﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Text

Public Class frmLoanScheme_AddEdit

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmLoanScheme_AddEdit"
    Private mblnCancel As Boolean = True
    Private objLoanScheme As clsLoan_Scheme
    Private menAction As enAction = enAction.ADD_ONE
    Private mintLoanSchemeunkid As Integer = -1

    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
    Private mstrSearchText As String = ""
    'Sohail (11 Apr 2018) -- End
    Private dvDocType As DataView
    Private mstrDocTypeIDs As String = String.Empty

    'Pinkal (10-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private dvEmployee As DataView = Nothing
    'Pinkal (10-Nov-2022) -- End

    'Pinkal (21-Jul-2023) -- Start
    '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
    Private dvExpiryOfTitleEmp As DataView = Nothing
    'Pinkal (21-Jul-2023) -- End

    'Pinkal (04-Aug-2023) -- Start
    '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
    Private dvLoanTrancheDocType As DataView
    Private mstrLoanTrancheDocTypeIDs As String = String.Empty
    'Pinkal (04-Aug-2023) -- End

    'Hemant (06 Dec 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
    Private dvFinalApprovalEmployee As DataView = Nothing
    'Hemant (06 Dec 2024) -- End


#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintLoanSchemeunkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintLoanSchemeunkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtName.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorOptional
            txtMinSalary.BackColor = GUI.ColorOptional
            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 52
            txtMaxLoanAmount.BackColor = GUI.ColorOptional
            'S.SANDEEP [20-SEP-2017] -- END

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            cboLoanCalcType.BackColor = GUI.ColorComp
            cboInterestCalcType.BackColor = GUI.ColorOptional
            txtLoanRate.BackColor = GUI.ColorOptional
            'S.SANDEEP [20-SEP-2017] -- END
            txtEMIExceedPerc.BackColor = GUI.ColorOptional 'Sohail (22 Sep 2017)
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            cboCostCenter.BackColor = GUI.ColorOptional
            'Sohail (14 Mar 2019) -- End
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            cboNetPay.BackColor = GUI.ColorOptional
            cboMappedHead.BackColor = GUI.ColorOptional
            'Sohail (29 Apr 2019) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-805 - Develop the logic for maximum loan amount
            txtMaxInstallmentAmtCalculation.BackColor = GUI.ColorOptional
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-818 - As a user, I want to be able to categorize a loan scheme as either secured,unsecured or Advance as radio buttons. A loan scheme can only be one at a time
            cboLoanSchemeCategory.BackColor = GUI.ColorOptional
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-819 - As a user, I want to have a field to configure the insurance amount on a loan scheme. Same idea we have with the interest rate
            txtInsuranceRate.BackColor = GUI.ColorOptional
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-821 - As a user, I want to be able to define maximum loan amount on a loan scheme as either a flat rate or a formula based on a payroll transaction head
            txtMaxLoanFormula.BackColor = GUI.ColorOptional
            'Hemant (24 Aug 2022) -- End
            'Hemant (03 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-914 - NMB - As a user, I want to have a field to specify the minimum amount on loan scheme. Same idea as the maximum amount on loan scheme. System should not allow application of figure below this amount
            txtMinLoanAmount.BackColor = GUI.ColorOptional
            'Hemant (03 Oct 2022) -- End
            'Hemant (07 Jul 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
            txtMaxInstallmentAmtForMortgage.BackColor = GUI.ColorOptional
            'Hemant (07 Jul 2023) -- End

            'Pinkal (11-Mar-2024) -- Start
            '(A1X-2505) NMB - Credit card integration.
            txtCreditCardPercentageDSR.BackColor = GUI.ColorOptional
            cboCreditCardAmountOnDSR.BackColor = GUI.ColorOptional
            cboCreditCardAmountForExpouser.BackColor = GUI.ColorOptional
            'Pinkal (11-Mar-2024) -- End



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Dim objExchangeRate As New clsExchangeRate
        objExchangeRate._ExchangeRateunkid = 1

        txtCode.Text = objLoanScheme._Code
        txtName.Text = objLoanScheme._Name
        txtDescription.Text = objLoanScheme._Description
        txtMinSalary.Text = Format(objLoanScheme._Minnetsalary, objExchangeRate._fmtCurrency)


        'Pinkal (14-Apr-2015) -- Start
        'Enhancement - WORKING ON REDESIGNING LOAN MODULE.
        chkShowonESS.Checked = objLoanScheme._IsShowonESS
        'Pinkal (14-Apr-2015) -- End

        'Nilay (19-Oct-2016) -- Start
        'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
        chkShowLoanBalOnPayslip.Checked = objLoanScheme._IsShowLoanBalOnPayslip
        'Nilay (19-Oct-2016) -- End

        'If objLoanScheme._Isinclude_Parttime = True Then
        '    chkIncludePartTimeEmp.Checked = True
        'Else
        '    chkIncludePartTimeEmp.Checked = False
        'End If

        'Hemant (24 Aug 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-821 - As a user, I want to be able to define maximum loan amount on a loan scheme as either a flat rate or a formula based on a payroll transaction head
        If objLoanScheme._MaxLoanAmountCalcTypeId <= 1 Then
            radFlatRate.Checked = True
            radFormula.Checked = False
        Else
            radFlatRate.Checked = False
            radFormula.Checked = True
        End If
        txtMaxLoanFormula.Text = objLoanScheme._MaxLoanAmountHeadFormula
        'Hemant (24 Aug 2022) -- End
        'S.SANDEEP [20-SEP-2017] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 52
        txtMaxLoanAmount.Text = Format(objLoanScheme._MaxLoanAmountLimit, objExchangeRate._fmtCurrency)
        'S.SANDEEP [20-SEP-2017] -- END

        'S.SANDEEP [20-SEP-2017] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 50
        cboLoanCalcType.SelectedValue = objLoanScheme._LoanCalcTypeId
        cboInterestCalcType.SelectedValue = objLoanScheme._InterestCalctypeId
        txtLoanRate.Text = CStr(objLoanScheme._InterestRate)
        'S.SANDEEP [20-SEP-2017] -- END
        txtEMIExceedPerc.Text = Format(objLoanScheme._EMI_NetPayPercentage, objExchangeRate._fmtCurrency) 'Sohail (22 Sep 2017)


        'Varsha (25 Nov 2017) -- Start
        'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
        txtMaxNoOfInstallment.Text = CStr(objLoanScheme._MaxNoOfInstallment)
        'Varsha (25 Nov 2017) -- End

        'Pinkal (23-Nov-2022) -- Start
        'NMB Loan Module Enhancement.
        txtMinNoOfInstallment.Decimal = CInt(objLoanScheme._MinNoOfInstallment)
        'Pinkal (23-Nov-2022) -- End


        'Sohail (11 Apr 2018) -- Start
        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
        Call txtEMIExceedPerc_Leave(txtEMIExceedPerc, New System.EventArgs)
        cboNetPay.SelectedValue = objLoanScheme._TranheadUnkid
        'Sohail (11 Apr 2018) -- End
        'Sohail (14 Mar 2019) -- Start
        'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
        cboCostCenter.SelectedValue = objLoanScheme._Costcenterunkid
        'Sohail (14 Mar 2019) -- End
        'Sohail (29 Apr 2019) -- Start
        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
        cboMappedHead.SelectedValue = objLoanScheme._Mapped_TranheadUnkid
        'Sohail (29 Apr 2019) -- End
        'Hemant (24 Aug 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-805 - Develop the logic for maximum loan amount
        txtMaxInstallmentAmtCalculation.Text = objLoanScheme._MaxInstallmentHeadFormula
        'Hemant (24 Aug 2022) -- End
        'Hemant (07 Jul 2023) -- Start
        'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
        txtMaxInstallmentAmtForMortgage.Text = objLoanScheme._MaxInstallmentHeadFormulaForMortgage
        'Hemant (07 Jul 2023) -- End

        'Hemant (24 Aug 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-818 - As a user, I want to be able to categorize a loan scheme as either secured,unsecured or Advance as radio buttons. A loan scheme can only be one at a time
        cboLoanSchemeCategory.SelectedValue = objLoanScheme._LoanSchemeCategoryId
        'Hemant (24 Aug 2022) -- End

        'Hemant (24 Aug 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-819 - As a user, I want be able to define the installment repayment schedule e.g monthly, quarterly etc
        txtRepaymentDays.Text = CStr(objLoanScheme._RepaymentDays)
        'Hemant (24 Aug 2022) -- End
        'Hemant (24 Aug 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-819 - As a user, I want to have a field to configure the insurance amount on a loan scheme. Same idea we have with the interest rate
        txtInsuranceRate.Text = CStr(objLoanScheme._InsuranceRate)
        'Hemant (24 Aug 2022) -- End
        'Hemant (24 Aug 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-822 - As a user I want to be able to define whether attachment on a loan scheme is mandatory or not
        chkAttachementRequired.Checked = objLoanScheme._IsAttachmentRequired
        chkAttachementRequired_CheckedChanged(Nothing, Nothing)
        mstrDocTypeIDs = objLoanScheme._DocumentTypeIDs
        'Hemant (24 Aug 2022) -- End


        'Pinkal (20-Sep-2022) -- Start
        'NMB Loan Module Enhancement.
        chkRequiredReportingToApproval.Checked = objLoanScheme._RequiredReportingToApproval
        If ConfigParameter._Object._RoleBasedLoanApproval AndAlso ConfigParameter._Object._LoanIntegration = enLoanIntegration.FlexCube Then
            chkRequiredReportingToApproval.Visible = True
        Else
            chkRequiredReportingToApproval.Visible = False
            chkRequiredReportingToApproval.Checked = False
        End If
        'Pinkal (20-Sep-2022) -- End

        'Hemant (03 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-914 - NMB - As a user, I want to have a field to specify the minimum amount on loan scheme. Same idea as the maximum amount on loan scheme. System should not allow application of figure below this amount
        txtMinLoanAmount.Text = Format(objLoanScheme._MinLoanAmountLimit, objExchangeRate._fmtCurrency)
        'Hemant (03 Oct 2022) -- End

        'Hemant (12 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : - As a user, I want to have option on loan scheme master to skip approval
        chkSkipApproval.Checked = objLoanScheme._IsSkipApproval
        If ConfigParameter._Object._RoleBasedLoanApproval AndAlso ConfigParameter._Object._LoanIntegration = enLoanIntegration.FlexCube Then
            chkSkipApproval.Visible = True
        Else
            chkSkipApproval.Visible = False
            chkSkipApproval.Checked = False
        End If
        'Hemant (12 Oct 2022) -- End
        'Hemant (12 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : - As a user, I want to have a checkbox option on the loan scheme master to define whether a loan scheme requires posting to flexcube or not. Not all loan schemes will be posting to flexcube after final approval  
        chkPostingToFlexcube.Checked = objLoanScheme._IsPostingToFlexcube

        'Pinkal (10-Nov-2022) -- Start
        'NMB Loan Module Enhancement.
        If ConfigParameter._Object._RoleBasedLoanApproval AndAlso ConfigParameter._Object._LoanIntegration = enLoanIntegration.FlexCube Then
        chkPostingToFlexcube_CheckedChanged(chkPostingToFlexcube, New EventArgs())
        End If
        'Pinkal (10-Nov-2022) -- End

        If ConfigParameter._Object._RoleBasedLoanApproval AndAlso ConfigParameter._Object._LoanIntegration = enLoanIntegration.FlexCube Then
            chkPostingToFlexcube.Visible = True
        Else
            chkPostingToFlexcube.Visible = False
            chkPostingToFlexcube.Checked = False
        End If
        'Hemant (12 Oct 2022) -- End
        'Hemant (12 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : - On loan scheme master, if a loan scheme has posting to flexcube enabled, as a user, I want to have the option of specifying the number of installments that need to be paid before a top-up can be done. 
        If chkPostingToFlexcube.Checked = True Then
            nudMinNoOfInstallmentPaid.Value = objLoanScheme._MinOfInstallmentPaid
            lblNoOfInstallmentPaid.Visible = True
            nudMinNoOfInstallmentPaid.Visible = True
        Else
            nudMinNoOfInstallmentPaid.Value = 0
            lblNoOfInstallmentPaid.Visible = False
            nudMinNoOfInstallmentPaid.Visible = False
        End If
        'Hemant (12 Oct 2022) -- End

        'Hemant (12 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : - As a user, I want to have an option (check box) on the loan scheme to indicate which loan schemes are eligible for top-up. If a loan scheme doesn’t have this option checked, user cannot do top-up on it   
        chkEligibleForTopup.Checked = objLoanScheme._IsEligibleForTopUp
        'Hemant (12 Oct 2022) -- End

        'Pinkal (10-Nov-2022) -- Start
        'NMB Loan Module Enhancement.
        chkEligibleForTopup_CheckedChanged(chkEligibleForTopup, New EventArgs())
        'Pinkal (10-Nov-2022) -- End


        'Sohail (02 Apr 2018) -- Start
        'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
        If objLoanScheme._RefLoanSchemeUnkid > 0 Then
            EnableControls(False)
        End If
        'Sohail (02 Apr 2018) -- End

        'Pinkal (10-Nov-2022) -- Start
        'NMB Loan Module Enhancement.
        chkLoanApprovalDailyReminder.Checked = objLoanScheme._LoanApproval_DailyReminder
        chkEscalationDays_CheckedChanged(chkEscalationDays, New EventArgs())

        nudEscalationDays.Value = CInt(objLoanScheme._EscalationDays)
        If CInt(nudEscalationDays.Value) > 0 Then
            chkEscalationDays.Checked = True
        Else
            chkEscalationDays.Checked = False
        End If
        If objLoanScheme._EscalatedToEmployeeIds.Trim.Length > 0 Then
            Dim drEmployee = CType(dgEmployee.DataSource, DataTable).Select("employeeunkid in (" & objLoanScheme._EscalatedToEmployeeIds.Trim & ")")
            If drEmployee IsNot Nothing AndAlso drEmployee.Count > 0 Then
                For i As Integer = 0 To drEmployee.Length - 1
                    drEmployee(i)("ischeck") = True
                    drEmployee(i).AcceptChanges()
                Next
            End If
            SetCheckBoxValue()
        End If
        'Pinkal (10-Nov-2022) -- End

        'Hemant (07 Jul 2023) -- Start
        'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
        If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then
            nudNoOfDaysBefereTitleExpiry.Value = objLoanScheme._NoOfDaysBefereTitleExpiry
            lblNoOfDaysBefereTitleExpiry.Enabled = True
            nudNoOfDaysBefereTitleExpiry.Enabled = True
        Else
            nudNoOfDaysBefereTitleExpiry.Value = 0
            lblNoOfDaysBefereTitleExpiry.Enabled = False
            nudNoOfDaysBefereTitleExpiry.Enabled = False
        End If
        'Hemant (07 July 2023) -- End

        'Pinkal (21-Jul-2023) -- Start
        '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
        nudNoOfDaysBefereTitleExpiry.Value = CInt(objLoanScheme._NoOfDaysBefereTitleExpiry)
        If CInt(nudNoOfDaysBefereTitleExpiry.Value) > 0 Then
            chkNtfForExpiryofTitle.Checked = True
        Else
            chkNtfForExpiryofTitle_CheckedChanged(chkNtfForExpiryofTitle, New EventArgs())
        End If
        If objLoanScheme._TitleExpiryToEmployeeIds.Trim.Length > 0 Then
            Dim drEmployee = CType(dgExpiryofTitleEmp.DataSource, DataTable).Select("employeeunkid in (" & objLoanScheme._TitleExpiryToEmployeeIds.Trim & ")")
            If drEmployee IsNot Nothing AndAlso drEmployee.Count > 0 Then
                For i As Integer = 0 To drEmployee.Length - 1
                    drEmployee(i)("ischeck") = True
                    drEmployee(i).AcceptChanges()
                Next
            End If
            SetExpiryTitleCheckBoxValue()
        End If
        'Pinkal (21-Jul-2023) -- End

        'Pinkal (04-Aug-2023) -- Start
        '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
        mstrLoanTrancheDocTypeIDs = objLoanScheme._LoanTrancheDocumentTypeIDs.Trim
        'Pinkal (04-Aug-2023) -- End

        'Pinkal (11-Mar-2024) -- Start
        '(A1X-2505) NMB - Credit card integration.
        txtCreditCardPercentageDSR.Decimal = objLoanScheme._CreditCardPercentageOnDSR
        cboCreditCardAmountOnDSR.SelectedValue = objLoanScheme._CreditCardAmountOnDSRFieldId
        cboCreditCardAmountForExpouser.SelectedValue = objLoanScheme._CreditCardAmountOnExposureFieldId
        'Pinkal (11-Mar-2024) -- End

        'Pinkal (17-May-2024) -- Start
        'NMB Enhancement For Mortgage Loan.
        chkMarketValueMandatory.Checked = objLoanScheme._MarketValueMandatory
        chkFSVValueMandatory.Checked = objLoanScheme._FSVValueMandatory
        chkBOQMandatory.Checked = objLoanScheme._BOQMandatory
        cboSeniorApproverLevel.SelectedValue = objLoanScheme._SeniorApproverLevelId.ToString()

        If objLoanScheme._ApproverLevelAlertIds.Trim.Length > 0 Then
            Dim drApproverLevel = CType(dgSeniorLoanApprovalAlert.DataSource, DataTable).Select("lnlevelunkid in (" & objLoanScheme._ApproverLevelAlertIds.Trim & ")")
            If drApproverLevel IsNot Nothing AndAlso drApproverLevel.Count > 0 Then
                For i As Integer = 0 To drApproverLevel.Length - 1
                    drApproverLevel(i)("IsChecked") = True
                    drApproverLevel(i).AcceptChanges()
                Next
            End If
        End If
        'Pinkal (17-May-2024) -- End

        'Hemant (22 Nov 2024) -- Start
        'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
        chkDisableEmployeeConfirm.Checked = objLoanScheme._IsDisableEmployeeConfirm
        'Hemant (22 Nov 2024) -- End

        'Hemant (06 Dec 2024) -- Start
        'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
        chkFlexcubeSuccessfulNotification.Checked = objLoanScheme._IsFinalApprovalNotify
        If objLoanScheme._FinalApprovalNotifyEmployeeIds.Trim.Length > 0 Then
            Dim drEmployee = CType(gvFlexcubeSuccessfulEmployeeList.DataSource, DataTable).Select("employeeunkid in (" & objLoanScheme._FinalApprovalNotifyEmployeeIds.Trim & ")")
            If drEmployee IsNot Nothing AndAlso drEmployee.Count > 0 Then
                For i As Integer = 0 To drEmployee.Length - 1
                    drEmployee(i)("ischeck") = True
                    drEmployee(i).AcceptChanges()
                Next
            End If
            SetFinalApprovalCheckBoxValue()
        End If
        'Hemant (06 Dec 2024) -- End


        FillDocomentTypeList()

        objExchangeRate = Nothing
    End Sub

    Private Sub SetValue()
        objLoanScheme._code = txtCode.Text
        objLoanScheme._name = txtName.Text
        objLoanScheme._Description = txtDescription.Text
        objLoanScheme._Minnetsalary = txtMinSalary.Decimal

        'Pinkal (14-Apr-2015) -- Start
        'Enhancement - WORKING ON REDESIGNING LOAN MODULE.
        objLoanScheme._IsShowonESS = chkShowonESS.Checked
        'Pinkal (14-Apr-2015) -- End

        'If chkIncludePartTimeEmp.Checked Then
        '    objLoanScheme._Isinclude_Parttime = True
        'Else
        '    objLoanScheme._Isinclude_Parttime = False
        'End If

        'Nilay (19-Oct-2016) -- Start
        'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
        objLoanScheme._IsShowLoanBalOnPayslip = chkShowLoanBalOnPayslip.Checked
        'Nilay (19-Oct-2016) -- End

        'Hemant (24 Aug 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-821 - As a user, I want to be able to define maximum loan amount on a loan scheme as either a flat rate or a formula based on a payroll transaction head
        If radFlatRate.Checked Then
            objLoanScheme._MaxLoanAmountCalcTypeId = 1
            txtMaxLoanFormula.Text = ""
        ElseIf radFormula.Checked Then
            objLoanScheme._MaxLoanAmountCalcTypeId = 2
            txtMaxLoanAmount.Text = "0"
        End If
        objLoanScheme._MaxLoanAmountHeadFormula = CStr(txtMaxLoanFormula.Text)
        'Hemant (24 Aug 2022) -- End

        'S.SANDEEP [20-SEP-2017] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 52
        objLoanScheme._MaxLoanAmountLimit = txtMaxLoanAmount.Decimal
        'S.SANDEEP [20-SEP-2017] -- END

        'S.SANDEEP [20-SEP-2017] -- START
        'ISSUE/ENHANCEMENT : REF-ID # 50
        objLoanScheme._LoanCalcTypeId = CInt(cboLoanCalcType.SelectedValue)
        objLoanScheme._InterestCalctypeId = CInt(cboInterestCalcType.SelectedValue)
        objLoanScheme._InterestRate = CDec(txtLoanRate.Text)
        'S.SANDEEP [20-SEP-2017] -- END
        'Sohail (22 Sep 2017) -- Start
        'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
        objLoanScheme._EMI_NetPayPercentage = txtEMIExceedPerc.Decimal
        'Sohail (22 Sep 2017) -- End

        'Varsha (25 Nov 2017) -- Start
        'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
        objLoanScheme._MaxNoOfInstallment = txtMaxNoOfInstallment.Int
        'Varsha (25 Nov 2017) -- End

        'Pinkal (23-Nov-2022) -- Start
        'NMB Loan Module Enhancement.
        objLoanScheme._MinNoOfInstallment = CInt(txtMinNoOfInstallment.Decimal)
        'Pinkal (23-Nov-2022) -- End

        'Sohail (11 Apr 2018) -- Start
        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
        objLoanScheme._TranheadUnkid = CInt(cboNetPay.SelectedValue)
        'Sohail (11 Apr 2018) -- End
        'Sohail (14 Mar 2019) -- Start
        'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
        objLoanScheme._Costcenterunkid = CInt(cboCostCenter.SelectedValue)
        'Sohail (14 Mar 2019) -- End
        'Sohail (29 Apr 2019) -- Start
        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
        objLoanScheme._Mapped_TranheadUnkid = CInt(cboMappedHead.SelectedValue)
        'Sohail (29 Apr 2019) -- End
        'Hemant (24 Aug 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-805 - Develop the logic for maximum loan amount
        objLoanScheme._MaxInstallmentHeadFormula = CStr(txtMaxInstallmentAmtCalculation.Text)
        'Hemant (24 Aug 2022) -- End
        'Hemant (07 Jul 2023) -- Start
        'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
        objLoanScheme._MaxInstallmentHeadFormulaForMortgage = CStr(txtMaxInstallmentAmtForMortgage.Text)
        'Hemant (07 Jul 2023) -- End
        'Hemant (24 Aug 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-818 - As a user, I want to be able to categorize a loan scheme as either secured,unsecured or Advance as radio buttons. A loan scheme can only be one at a time
        objLoanScheme._LoanSchemeCategoryId = CInt(cboLoanSchemeCategory.SelectedValue)
        'Hemant (24 Aug 2022) -- End
        'Hemant (24 Aug 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-819 - As a user, I want be able to define the installment repayment schedule e.g monthly, quarterly etc
        objLoanScheme._RepaymentDays = txtRepaymentDays.Int
        'Hemant (24 Aug 2022) -- End
        'Hemant (24 Aug 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-819 - As a user, I want to have a field to configure the insurance amount on a loan scheme. Same idea we have with the interest rate
        objLoanScheme._InsuranceRate = CDec(txtInsuranceRate.Text)
        'Hemant (24 Aug 2022) -- End
        'Hemant (24 Aug 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-822 - As a user I want to be able to define whether attachment on a loan scheme is mandatory or not
        objLoanScheme._IsAttachmentRequired = chkAttachementRequired.Checked
        If chkAttachementRequired.Checked = True Then
            mstrDocTypeIDs = String.Join(",", (From p In dvDocType.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("masterunkid").ToString)).ToArray)
        Else
            mstrDocTypeIDs = ""
        End If
        objLoanScheme._DocumentTypeIDs = mstrDocTypeIDs
        'Hemant (24 Aug 2022) -- End

        'Pinkal (20-Sep-2022) -- Start
        'NMB Loan Module Enhancement.
        objLoanScheme._RequiredReportingToApproval = chkRequiredReportingToApproval.Checked
        'Pinkal (20-Sep-2022) -- End


        'Hemant (03 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-914 - NMB - As a user, I want to have a field to specify the minimum amount on loan scheme. Same idea as the maximum amount on loan scheme. System should not allow application of figure below this amount
        objLoanScheme._MinLoanAmountLimit = txtMinLoanAmount.Decimal
        'Hemant (03 Oct 2022) -- End
        'Hemant (12 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : - As a user, I want to have option on loan scheme master to skip approval
        objLoanScheme._IsSkipApproval = chkSkipApproval.Checked
        'Hemant (12 Oct 2022) -- End
        'Hemant (12 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : - As a user, I want to have a checkbox option on the loan scheme master to define whether a loan scheme requires posting to flexcube or not. Not all loan schemes will be posting to flexcube after final approval  
        objLoanScheme._IsPostingToFlexcube = chkPostingToFlexcube.Checked
        'Hemant (12 Oct 2022) -- End
        'Hemant (12 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : - On loan scheme master, if a loan scheme has posting to flexcube enabled, as a user, I want to have the option of specifying the number of installments that need to be paid before a top-up can be done. 
        If chkPostingToFlexcube.Checked = True Then
            objLoanScheme._MinOfInstallmentPaid = CInt(nudMinNoOfInstallmentPaid.Value)
        Else
            objLoanScheme._MinOfInstallmentPaid = 0
        End If
        'Hemant (12 Oct 2022) -- End
        'Hemant (12 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : - As a user, I want to have an option (check box) on the loan scheme to indicate which loan schemes are eligible for top-up. If a loan scheme doesn’t have this option checked, user cannot do top-up on it   
        objLoanScheme._IsEligibleForTopUp = chkEligibleForTopup.Checked
        'Hemant (12 Oct 2022) -- End
        'Hemant (27 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-990 - As a user, I want to pass the principal balance contained in flexcube loan view to Aruti payroll to display on employee salary slip as the loan balance for loans set to post to flexcube

        'Pinkal (10-Nov-2022) -- Start
        'NMB Loan Module Enhancement.
        objLoanScheme._DatabaseName = FinancialYear._Object._DatabaseName
        'Pinkal (10-Nov-2022) -- End

        objLoanScheme._Userunkid = User._Object._Userunkid
        'Hemant (27 Oct 2022) -- End

        'Pinkal (10-Nov-2022) -- Start
        'NMB Loan Module Enhancement.
        objLoanScheme._LoanApproval_DailyReminder = chkLoanApprovalDailyReminder.Checked
        objLoanScheme._EscalationDays = CInt(nudEscalationDays.Value)
        Dim lstEmpIds As String = ""
        If CInt(nudEscalationDays.Value) > 0 Then
            lstEmpIds = String.Join(",", dgEmployee.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhEmpCheck.Index).Value) = True).Select(Function(x) x.Cells(objdgcolhEmployeeId.Index).Value.ToString()).ToArray())
        End If
        objLoanScheme._EscalatedToEmployeeIds = lstEmpIds
        'Pinkal (10-Nov-2022) -- End
        'Hemant (07 Jul 2023) -- Start
        'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
        If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then
            objLoanScheme._NoOfDaysBefereTitleExpiry = CInt(nudNoOfDaysBefereTitleExpiry.Value)
        Else
            objLoanScheme._NoOfDaysBefereTitleExpiry = 0
        End If
        'Hemant (07 July 2023) -- End

        'Pinkal (21-Jul-2023) -- Start
        '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
        Dim lstTitleExpiryEmpIds As String = ""
        If CInt(nudNoOfDaysBefereTitleExpiry.Value) > 0 Then
            lstTitleExpiryEmpIds = String.Join(",", dgExpiryofTitleEmp.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhExpiryTitleEmpCheck.Index).Value) = True).Select(Function(x) x.Cells(objdgcolhExpiryTitleEmployeeId.Index).Value.ToString()).ToArray())
        End If
        objLoanScheme._TitleExpiryToEmployeeIds = lstTitleExpiryEmpIds
        'Pinkal (21-Jul-2023) -- End

        'Pinkal (04-Aug-2023) -- Start
        '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
        If objLoanScheme._LoanSchemeCategoryId = enLoanSchemeCategories.MORTGAGE Then
            objLoanScheme._LoanTrancheDocumentTypeIDs = String.Join(",", (From p In dvLoanTrancheDocType.Table Where (CBool(p.Item("IsChecked")) = True) Select (p.Item("masterunkid").ToString)).ToArray)
        Else
            objLoanScheme._LoanTrancheDocumentTypeIDs = ""
        End If
        'Pinkal (04-Aug-2023) -- End

        'Pinkal (11-Mar-2024) -- Start
        '(A1X-2505) NMB - Credit card integration.
        objLoanScheme._CreditCardPercentageOnDSR = txtCreditCardPercentageDSR.Decimal
        objLoanScheme._CreditCardAmountOnDSRFieldId = CInt(cboCreditCardAmountOnDSR.SelectedValue)
        objLoanScheme._CreditCardAmountOnExposureFieldId = CInt(cboCreditCardAmountForExpouser.SelectedValue)
        'Pinkal (11-Mar-2024) -- End

        'Pinkal (17-May-2024) -- Start
        'NMB Enhancement For Mortgage Loan.
        objLoanScheme._MarketValueMandatory = chkMarketValueMandatory.Checked
        objLoanScheme._FSVValueMandatory = chkFSVValueMandatory.Checked
        objLoanScheme._BOQMandatory = chkBOQMandatory.Checked
        objLoanScheme._SeniorApproverLevelId = CInt(cboSeniorApproverLevel.SelectedValue)
        Dim lstApproverLevelIds As String = ""
        If dgSeniorLoanApprovalAlert.RowCount > 0 Then
            lstApproverLevelIds = String.Join(",", dgSeniorLoanApprovalAlert.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhLevelCheck.Index).Value) = True).Select(Function(x) x.Cells(objdgcolhLevelId.Index).Value.ToString()).ToArray())
        End If
        objLoanScheme._ApproverLevelAlertIds = lstApproverLevelIds
        'Pinkal (17-May-2024) -- End

        'Hemant (22 Nov 2024) -- Start
        'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
        objLoanScheme._IsDisableEmployeeConfirm = chkDisableEmployeeConfirm.Checked
        'Hemant (22 Nov 2024) -- End

        'Hemant (06 Dec 2024) -- Start
        'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
        objLoanScheme._IsFinalApprovalNotify = chkFlexcubeSuccessfulNotification.Checked
        Dim lstFinalApprovalEmpIds As String = ""
        lstFinalApprovalEmpIds = String.Join(",", gvFlexcubeSuccessfulEmployeeList.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhFinalApprovalEmpCheck.Index).Value) = True).Select(Function(x) x.Cells(objdgcolhFinalApprovalEmployeeId.Index).Value.ToString()).ToArray())
        objLoanScheme._FinalApprovalNotifyEmployeeIds = lstFinalApprovalEmpIds
        'Hemant (06 Dec 2024) -- End

    End Sub

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
    Private Sub FillCombo()
        Dim objLoan_Advance As New clsLoan_Advance
        'Sohail (11 Apr 2018) -- Start
        'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
        Dim objHead As New clsTransactionHead
        'Sohail (11 Apr 2018) -- End
        'Sohail (14 Mar 2019) -- Start
        'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
        Dim objCCetnter As New clscostcenter_master
        'Sohail (14 Mar 2019) -- End
        Dim dsList As New DataSet
        Try
            dsList = objLoan_Advance.GetLoanCalculationTypeList("List", True)
            With cboLoanCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            dsList = objLoan_Advance.GetLoan_Interest_Calculation_Type("List", True)
            With cboInterestCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With

            'Sohail (11 Apr 2018) -- Start
            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
            'Sohail (29 Apr 2019) -- Start
            'Isssue#0003774(Sport Pesa):  net pay transhead is not showing on the list. Optional basic salary head not working. when you map other transaction head on "basic salary head (Optional)" (loan setting->configuration) the system gives error during application
            'dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False, "calctype_id <> " & CInt(enCalcType.NET_PAY) & " ")
            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False)
            'Sohail (29 Apr 2019) -- End
            With cboNetPay
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboNetPay)
            End With
            'Sohail (11 Apr 2018) -- End

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            dsList = objHead.getComboList(FinancialYear._Object._DatabaseName, "List", True, 0, 0, -1, False, False, "calctype_id <> " & CInt(enCalcType.NET_PAY) & " ")
            With cboMappedHead
                .ValueMember = "tranheadunkid"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboMappedHead)
            End With
            'Sohail (29 Apr 2019) -- End

            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            dsList = objCCetnter.getComboList("List", True)
            With cboCostCenter
                .ValueMember = "costcenterunkid"
                .DisplayMember = "costcentername"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
                Call SetDefaultSearchText(cboCostCenter)
            End With
            'Sohail (14 Mar 2019) -- End

            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-818 - As a user, I want to be able to categorize a loan scheme as either secured,unsecured or Advance as radio buttons. A loan scheme can only be one at a time
            dsList = objLoan_Advance.GetLoan_Scheme_Categories("List", True)
            With cboLoanSchemeCategory
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsList.Tables("List")
                .SelectedValue = 0
            End With
            'Hemant (24 Aug 2022) -- End


            'Pinkal (11-Mar-2024) -- Start
            '(A1X-2505) NMB - Credit card integration.
            Dim dtTable As DataTable = Nothing
            Dim objCCIntegration As New clsCreditCardIntegration
            dtTable = objCCIntegration.GetCreditCardColumns(True, True, "DataType = " & clsCreditCardIntegration.enCreditCardColumnDataType.NUMERIC)

            With cboCreditCardAmountOnDSR
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable.Copy()
                .SelectedValue = "0"
                Call SetDefaultSearchText(cboCreditCardAmountOnDSR)
            End With

            With cboCreditCardAmountForExpouser
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dtTable.Copy()
                .SelectedValue = "0"
                Call SetDefaultSearchText(cboCreditCardAmountForExpouser)
            End With

            objCCIntegration = Nothing
            'Pinkal (11-Mar-2024) -- End



            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            dsList = Nothing
            Dim objEmployee As New clsEmployee_Master
            dsList = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                                    User._Object._Userunkid, _
                                                                    FinancialYear._Object._YearUnkid, _
                                                                    Company._Object._Companyunkid, _
                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                    eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                    ConfigParameter._Object._UserAccessModeSetting, _
                                                                    True, _
                                                                    ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", False, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, False, "", False, False, True, , , , False, False, "")


            If dsList.Tables(0).Columns.Contains("ischeck") = False Then
                Dim dcColumn As New DataColumn("ischeck")
                dcColumn.DataType = Type.GetType("System.Boolean")
                dcColumn.DefaultValue = False
                dsList.Tables(0).Columns.Add(dcColumn)
            End If

            dvEmployee = dsList.Tables(0).DefaultView

            dgEmployee.AutoGenerateColumns = False
            objdgcolhEmpCheck.DataPropertyName = "ischeck"
            objdgcolhEmployeeId.DataPropertyName = "employeeunkid"
            dgcolhEmployee.DataPropertyName = "EmpCodeName"
            dgEmployee.DataSource = dsList.Tables(0)
            objEmployee = Nothing
            'Pinkal (10-Nov-2022) -- End

            'Pinkal (21-Jul-2023) -- Start
            '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
            Dim dtTitleExpiry As DataTable = dsList.Tables(0).Copy()
            dvExpiryOfTitleEmp = dtTitleExpiry.DefaultView
            dgExpiryofTitleEmp.AutoGenerateColumns = False
            objdgcolhExpiryTitleEmpCheck.DataPropertyName = "ischeck"
            objdgcolhExpiryTitleEmployeeId.DataPropertyName = "employeeunkid"
            dgcolhExpiryTitleEmployee.DataPropertyName = "EmpCodeName"
            dgExpiryofTitleEmp.DataSource = dtTitleExpiry
            'Pinkal (21-Jul-2023) -- End

            'Hemant (06 Dec 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
            Dim dtFinalApproval As DataTable = dsList.Tables(0).Copy()
            dvFinalApprovalEmployee = dtFinalApproval.DefaultView
            gvFlexcubeSuccessfulEmployeeList.AutoGenerateColumns = False
            objdgcolhFinalApprovalEmpCheck.DataPropertyName = "ischeck"
            objdgcolhFinalApprovalEmployeeId.DataPropertyName = "employeeunkid"
            dgcolhFinalApprovalEmployee.DataPropertyName = "EmpCodeName"
            gvFlexcubeSuccessfulEmployeeList.DataSource = dtFinalApproval
            'Hemant (06 Dec 2024) -- End


            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            Dim objApproverLevel As New clslnapproverlevel_master
            Dim dsApproverLevel As DataSet = objApproverLevel.GetList("Level", False, True, "")
            cboSeniorApproverLevel.DisplayMember = "lnlevelname"
            cboSeniorApproverLevel.ValueMember = "lnlevelunkid"
            cboSeniorApproverLevel.DataSource = dsApproverLevel.Tables(0).Copy
            Call SetDefaultSearchText(cboSeniorApproverLevel)
            objApproverLevel = Nothing
            'Pinkal (17-May-2024) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objLoan_Advance = Nothing : dsList.Dispose()
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            objCCetnter = Nothing
            'Sohail (14 Mar 2019) -- End
        End Try
    End Sub
    'S.SANDEEP [20-SEP-2017] -- END

    'Sohail (02 Apr 2018) -- Start
    'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
    Private Sub EnableControls(ByVal blnEnable As Boolean)
        Try
            'Dim lst As IEnumerable(Of Control) = pnlMainInfo.Controls.OfType(Of Control)().Where(Function(t) t.Name <> objFooter.Name)
            'For Each ctrl In lst
            '    ctrl.Enabled = blnEnable
            'Next
            Dim lst As IEnumerable(Of Control) = gbLoanInfo.Controls.OfType(Of Control).Where(Function(x) x.Name <> txtCode.Name AndAlso x.Name <> txtName.Name)
            For Each ctrl In lst
                ctrl.Enabled = blnEnable
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EnableControls", mstrModuleName)
        End Try
    End Sub
    'Sohail (02 Apr 2018) -- End

    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 7, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefauSetRegularFontltSearchText", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 Apr 2018) -- End

    'Hemant (24 Aug 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-409 - As a user, I want to specify which loan schemes require attachments from the loan scheme master. I want to bind specific documents to loan schemes
    Private Sub FillDocomentTypeList()
        Dim objCommon As New clsCommon_Master
        Dim dsList As DataSet

        'Pinkal (04-Aug-2023) -- Start
        '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
        Dim dsLoanTrancheList As DataSet = Nothing
        'Pinkal (04-Aug-2023) -- End


        Try
            dsList = objCommon.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, False, "List")

            If dsList.Tables(0).Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dsList.Tables(0).Columns.Add(dtCol)
            End If

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
            dsLoanTrancheList = dsList.Copy()
            'Pinkal (04-Aug-2023) -- End

            If mstrDocTypeIDs IsNot Nothing AndAlso mstrDocTypeIDs.Trim <> "" Then
                Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (mstrDocTypeIDs.Split(CChar(",")).Contains(p.Item("masterunkid").ToString)) Select (p)).ToList
                For Each r As DataRow In lstRow
                    r.Item("IsChecked") = True
                Next
                dsList.Tables(0).AcceptChanges()
            End If

            dvDocType = dsList.Tables(0).DefaultView
            dgvDocumentType.AutoGenerateColumns = False
            objdgcolhCheck.DataPropertyName = "IsChecked"
            objdgcolhMasterunkid.DataPropertyName = "masterunkid"
            dgColhDocumentType.DataPropertyName = "name"
            dgvDocumentType.DataSource = dvDocType
            dvDocType.Sort = "IsChecked DESC, name "


            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
            If mstrLoanTrancheDocTypeIDs IsNot Nothing AndAlso mstrLoanTrancheDocTypeIDs.Trim <> "" Then
                Dim lstRow As List(Of DataRow) = (From p In dsLoanTrancheList.Tables(0) Where (mstrLoanTrancheDocTypeIDs.Split(CChar(",")).Contains(p.Item("masterunkid").ToString)) Select (p)).ToList
                For Each r As DataRow In lstRow
                    r.Item("IsChecked") = True
                Next
            End If
            dsLoanTrancheList.Tables(0).AcceptChanges()

            dvLoanTrancheDocType = dsLoanTrancheList.Tables(0).DefaultView
            dgLoanTrancheDocumentList.AutoGenerateColumns = False
            objdgcolhLoanTranCheck.DataPropertyName = "IsChecked"
            objdgcolhLoanTrancheMasterunkid.DataPropertyName = "masterunkid"
            dgcolhLoanTranchedocumentType.DataPropertyName = "name"
            dgLoanTrancheDocumentList.DataSource = dvLoanTrancheDocType
            dvLoanTrancheDocType.Sort = "IsChecked DESC, name "
            'Pinkal (04-Aug-2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDocomentTypeList", mstrModuleName)
        Finally
            objCommon = Nothing
        End Try
    End Sub
    'Hemant (24 Aug 2022) -- End

    'Pinkal (10-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = dvEmployee.Table.Select("ischeck = true")

            RemoveHandler chkSelectAllEmployee.CheckedChanged, AddressOf chkSelectAllEmployee_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAllEmployee.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgEmployee.Rows.Count Then
                chkSelectAllEmployee.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgEmployee.Rows.Count Then
                chkSelectAllEmployee.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAllEmployee.CheckedChanged, AddressOf chkSelectAllEmployee_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try
    End Sub
    'Pinkal (10-Nov-2022) -- End

    'Pinkal (21-Jul-2023) -- Start
    '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
    Private Sub SetExpiryTitleCheckBoxValue()
        Try
            Dim drRow As DataRow() = dvExpiryOfTitleEmp.Table.Select("ischeck = true")

            RemoveHandler chkAllExpiryofTitleEmp.CheckedChanged, AddressOf chkAllExpiryofTitleEmp_CheckedChanged

            If drRow.Length <= 0 Then
                chkAllExpiryofTitleEmp.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgExpiryofTitleEmp.Rows.Count Then
                chkAllExpiryofTitleEmp.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgExpiryofTitleEmp.Rows.Count Then
                chkAllExpiryofTitleEmp.CheckState = CheckState.Checked
            End If

            AddHandler chkAllExpiryofTitleEmp.CheckedChanged, AddressOf chkAllExpiryofTitleEmp_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetExpiryTitleCheckBoxValue", mstrModuleName)
        End Try
    End Sub
    'Pinkal (21-Jul-2023) -- End

    'Hemant (06 Dec 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
    Private Sub SetFinalApprovalCheckBoxValue()
        Try
            Dim drRow As DataRow() = dvFinalApprovalEmployee.Table.Select("ischeck = true")

            RemoveHandler chkSelectAllFinalApprovalEmployee.CheckedChanged, AddressOf chkSelectAllFinalApprovalEmployee_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAllFinalApprovalEmployee.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgEmployee.Rows.Count Then
                chkSelectAllFinalApprovalEmployee.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgEmployee.Rows.Count Then
                chkSelectAllFinalApprovalEmployee.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAllFinalApprovalEmployee.CheckedChanged, AddressOf chkSelectAllFinalApprovalEmployee_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetFinalApprovalCheckBoxValue", mstrModuleName)
        End Try
    End Sub
    'Hemant (06 Dec 2024) -- End

#End Region

#Region " Form's Events "

    Private Sub frmLoanScheme_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objLoanScheme = Nothing
    End Sub

    Private Sub frmLoanScheme_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmLoanScheme_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmLoanScheme_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLoanScheme = New clsLoan_Scheme
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call setColor()

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            Call FillCombo()
            'S.SANDEEP [20-SEP-2017] -- END

            If menAction = enAction.EDIT_ONE Then
                objLoanScheme._Loanschemeunkid = mintLoanSchemeunkid
            End If

            Call GetValue()

            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : - As a user, I want to have option on loan scheme master to skip approval
            'Hemant (27 Oct 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : to Stop pass that to  Payroll in Internal Loan and only deduct from what we fetch from the loan view in External Loan(For working both Normal Loan & Flexcube Loan parallelly )
            'If menAction = enAction.EDIT_ONE AndAlso chkSkipApproval.Checked = True Then
            '    chkSkipApproval.Enabled = False
            'End If
                chkSkipApproval.Enabled = False
            If ConfigParameter._Object._RoleBasedLoanApproval AndAlso ConfigParameter._Object._LoanIntegration = enLoanIntegration.FlexCube Then
            If menAction <> enAction.EDIT_ONE Then
                chkSkipApproval.Checked = True
            End If
            Else
                chkSkipApproval.Checked = False
            End If
            If menAction = enAction.EDIT_ONE AndAlso chkPostingToFlexcube.Checked = True Then
                chkPostingToFlexcube.Enabled = False
            End If
            'Hemant (27 Oct 2022) -- End
            'Hemant (12 Oct 2022) -- End

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanScheme_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsLoan_Scheme.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Scheme"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
#Region " Combobox Event(s) "

    Private Sub cboLoanCalcType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoanCalcType.SelectedIndexChanged
        Try
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            If CInt(cboLoanCalcType.SelectedValue) <> enLoanCalcId.No_Interest_With_Mapped_Head Then
                cboMappedHead.SelectedValue = 0
                cboMappedHead.Enabled = False
            End If
            'Sohail (29 Apr 2019) -- End
            If CInt(cboLoanCalcType.SelectedValue) > 0 Then
                Select Case CInt(cboLoanCalcType.SelectedValue)
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    'Case enLoanCalcId.No_Interest
                    Case enLoanCalcId.No_Interest, enLoanCalcId.No_Interest_With_Mapped_Head
                        'Sohail (29 Apr 2019) -- End
                        cboInterestCalcType.SelectedValue = 0
                        txtLoanRate.Text = CStr(0)
                        pnlCalcType.Enabled = False
                        'Sohail (29 Apr 2019) -- Start
                        'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                        If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                            cboMappedHead.Enabled = True
                        End If
                        'Sohail (29 Apr 2019) -- End
                    Case Else
                        pnlCalcType.Enabled = True
                End Select
            Else
                cboInterestCalcType.SelectedValue = 0
                txtLoanRate.Text = CStr(0)
                pnlCalcType.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanCalcType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
    Private Sub cboNetPay_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboNetPay.KeyPress, cboCostCenter.KeyPress, cboMappedHead.KeyPress _
                                                                                                                                                                                   , cboCreditCardAmountOnDSR.KeyPress, cboCreditCardAmountForExpouser.KeyPress _
                                                                                                                                                                                   , cboSeniorApproverLevel.KeyPress

        'Pinkal (17-May-2024) -- NMB Enhancement For Mortgage Loan.[cboSeniorApproverLevel.KeyPress]
        'Pinkal (11-Mar-2024) -- (A1X-2505) NMB - Credit card integration.[cboCreditCardAmountOnDSR.KeyPress, cboCreditCardAmountForExpouser.KeyPress]
        'Sohail (29 Apr 2019) - [cboMappedHead]
        'Sohail (14 Mar 2019) - [cboCostCenter.KeyPress]
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    'Sohail (14 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
                    '.CodeMember = "code"

                    If cbo.Name <> cboCreditCardAmountOnDSR.Name AndAlso cbo.Name <> cboCreditCardAmountForExpouser.Name Then
                    If cbo.Name = cboCostCenter.Name Then
                        .CodeMember = "costcentercode"
                    Else
                    .CodeMember = "code"
                    End If
                    End If

                    'Sohail (14 Mar 2019) -- End


                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNetPay_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNetPay.SelectedIndexChanged, cboCostCenter.SelectedIndexChanged _
                                                                                                                                                                 , cboMappedHead.SelectedIndexChanged, cboCreditCardAmountOnDSR.SelectedIndexChanged _
                                                                                                                                                                 , cboCreditCardAmountForExpouser.SelectedIndexChanged, cboSeniorApproverLevel.SelectedIndexChanged

        'Pinkal (17-May-2024) -- NMB Enhancement For Mortgage Loan.[ cboSeniorApproverLevel.SelectedIndexChanged]
        'Pinkal (11-Mar-2024) -- (A1X-2505) NMB - Credit card integration.[cboCreditCardAmountOnDSR.SelectedIndexChanged, cboCreditCardAmountForExpouser.SelectedIndexChanged]
        'Sohail (29 Apr 2019) - [cboMappedHead]
        'Sohail (14 Mar 2019) - [cboCostCenter.SelectedIndexChanged]
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNetPay_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNetPay.GotFocus, cboCostCenter.GotFocus, cboMappedHead.GotFocus _
                                                                                                                                              , cboCreditCardAmountOnDSR.GotFocus, cboCreditCardAmountForExpouser.GotFocus _
                                                                                                                                              , cboSeniorApproverLevel.GotFocus

        'Pinkal (17-May-2024) -- NMB Enhancement For Mortgage Loan.[ cboSeniorApproverLevel.GotFocus]
        'Pinkal (11-Mar-2024) -- (A1X-2505) NMB - Credit card integration.[cboCreditCardAmountOnDSR.GotFocus, cboCreditCardAmountForExpouser.GotFocus]
        'Sohail (29 Apr 2019) - [cboMappedHead]
        'Sohail (14 Mar 2019) - [cboCostCenter.GotFocus]
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboNetPay_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboNetPay.Leave, cboCostCenter.Leave, cboMappedHead.Leave _
                                                                                                                                        , cboCreditCardAmountOnDSR.Leave, cboCreditCardAmountForExpouser.Leave _
                                                                                                                                        , cboSeniorApproverLevel.Leave
        'Pinkal (17-May-2024) -- NMB Enhancement For Mortgage Loan.[ cboSeniorApproverLevel.Leave]
        'Pinkal (11-Mar-2024) -- (A1X-2505) NMB - Credit card integration.[cboCreditCardAmountOnDSR.Leave, cboCreditCardAmountForExpouser.Leave]
        'Sohail (29 Apr 2019) - [cboMappedHead]
        'Sohail (14 Mar 2019) - [cboCostCenter.Leave]
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try
            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub
    'Sohail (11 Apr 2018) -- End
    'Hemant (07 Jul 2023) -- Start
    'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
    Private Sub cboLoanSchemeCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoanSchemeCategory.SelectedIndexChanged
        Try
            'Pinkal (21-Jul-2023) -- Start
            '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then
                chkNtfForExpiryofTitle.Enabled = True
                chkNtfForExpiryofTitle.Checked = False
                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
                dgLoanTrancheDocumentList.Enabled = True
                'Pinkal (04-Aug-2023) -- End
            Else
                chkNtfForExpiryofTitle.Enabled = False
                chkNtfForExpiryofTitle.Checked = False
                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
                If dvLoanTrancheDocType IsNot Nothing Then
                    Dim lstRow As List(Of DataRow) = (From p In dvLoanTrancheDocType.Table().AsEnumerable().Where(Function(x) x.Field(Of Boolean)("IsChecked") = True) Select (p)).ToList()
                    For Each r As DataRow In lstRow
                        r.Item("IsChecked") = False
                    Next
                    dvLoanTrancheDocType.Table.AcceptChanges()
                End If
                mstrLoanTrancheDocTypeIDs = ""
                dgLoanTrancheDocumentList.Enabled = False
                'Pinkal (04-Aug-2023) -- End
            End If
            'Pinkal (21-Jul-2023) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanSchemeCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (07 July 2023) -- End

    'Pinkal (17-May-2024) -- Start
    'NMB Enhancement For Mortgage Loan.
    Private Sub cboSeniorApproverLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboSeniorApproverLevel.SelectedIndexChanged
        Dim dtTable As DataTable = Nothing
        Dim mstrSearch As String = ""
        Try
            dtTable = CType(cboSeniorApproverLevel.DataSource, DataTable)
            mstrSearch = "lnlevelunkid > 0 "

            If CInt(cboSeniorApproverLevel.SelectedValue) > 0 Then
                mstrSearch &= " AND lnlevelunkid <> " & CInt(cboSeniorApproverLevel.SelectedValue)
            End If

            dtTable = New DataView(dtTable, mstrSearch, "priority", DataViewRowState.CurrentRows).ToTable()

            If dtTable.Columns.Contains("IsChecked") = False Then
                Dim dtCol As DataColumn
                dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dtTable.Columns.Add(dtCol)
            End If

            dgSeniorLoanApprovalAlert.AutoGenerateColumns = False
            objdgcolhLevelCheck.DataPropertyName = "IsChecked"
            dgcolhApproverLevel.DataPropertyName = "lnlevelname"
            dgcolhPriority.DataPropertyName = "priority"
            objdgcolhLevelId.DataPropertyName = "lnlevelunkid"
            dgSeniorLoanApprovalAlert.DataSource = dtTable.Copy

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboSeniorApproverLevel_SelectedIndexChanged", mstrModuleName)
        Finally
            If dtTable IsNot Nothing Then dtTable.Clear()
            dtTable.Dispose()
            dtTable = Nothing
        End Try
    End Sub
    'Pinkal (17-May-2024) -- End



#End Region
    'S.SANDEEP [20-SEP-2017] -- END

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try


            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information) '?1
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Loan Scheme cannot be blank. Loan Scheme is required information."), enMsgBoxStyle.Information) '?1
                txtName.Focus()
                Exit Sub
            End If

            '--Vimal 28-Aug-2010 Start

            If Trim(txtMinSalary.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Minimum Salary cannot be blank. Please insert Minimum Salary."), enMsgBoxStyle.Information)
                txtMinSalary.Focus()
                Exit Sub
            End If

            '--Vimal 28-Aug-2010 End

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            If CInt(cboLoanCalcType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Sorry, Loan calculation type is mandatory information. Please select loan calculation type."), enMsgBoxStyle.Information)
                cboLoanCalcType.Focus()
                Exit Sub
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head AndAlso CInt(cboMappedHead.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Please select Mapped Head."), enMsgBoxStyle.Information)
                cboMappedHead.Focus()
                Exit Sub
                'Sohail (29 Apr 2019) -- End
            End If
            'S.SANDEEP [20-SEP-2017] -- END

            'Sohail (05 Oct 2017) -- Start
            'Enhancement - 70.1 - Do not allow to enter % more than 100 for EMI limitation on previous net pay.
            If txtEMIExceedPerc.Decimal < 0 OrElse txtEMIExceedPerc.Decimal > 100 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Sorry, Percentage should be in between 0 and 100."), enMsgBoxStyle.Information)
                txtEMIExceedPerc.Focus()
                Exit Sub
            End If
            'Sohail (05 Oct 2017) -- End

            'Sohail (02 Sep 2022) -- Start
            'Enhancement : AC2-815 : NMB - Give config option for mandatory "Max number of installments" on loan scheme add/edit.
            If ConfigParameter._Object._SetMaxNumberofInstallmentsMandatory = True AndAlso txtMaxNoOfInstallment.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, Maximum number of installment is mandatory."), enMsgBoxStyle.Information)
                txtMaxNoOfInstallment.Focus()
                Exit Sub
            End If
            'Sohail (02 Sep 2022) -- End

            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            If txtMaxNoOfInstallment.Int < 0 OrElse txtMaxNoOfInstallment.Int > 360 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Maximum number of installment should not be greater than 360."), enMsgBoxStyle.Information)
                txtMaxNoOfInstallment.Focus()
                Exit Sub
            End If
            'Varsha (25 Nov 2017) -- End

            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-409 - As a user, I want to specify which loan schemes require attachments from the loan scheme master. I want to bind specific documents to loan schemes
            If chkAttachementRequired.Checked = True Then
                Dim drDocType() As DataRow = dvDocType.Table.Select("Ischecked=true")
                If drDocType.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Document Type is mondatory information. Please select document type for Attachement"), enMsgBoxStyle.Information)
                    dgvDocumentType.Focus()
                    Exit Sub
                End If
            End If
            'Hemant (24 Aug 2022) -- End

            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : - On loan scheme master, if a loan scheme has posting to flexcube enabled, as a user, I want to have the option of specifying the number of installments that need to be paid before a top-up can be done. 
            If nudMinNoOfInstallmentPaid.Visible = True AndAlso nudMinNoOfInstallmentPaid.Value <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Sorry, Minimum No. of Installment to be Paid should not be greater than 0."), enMsgBoxStyle.Information)
                nudMinNoOfInstallmentPaid.Focus()
                Exit Sub
            End If
            'Hemant (12 Oct 2022) -- End


            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.

            If chkEscalationDays.Checked AndAlso CInt(nudEscalationDays.Value) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Please Set Recurrent Reminder Occurrence in day(s) to send reminder."), enMsgBoxStyle.Information)
                nudEscalationDays.Focus()
                Exit Sub
            End If

            Dim drEmpCheck = dgEmployee.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhEmpCheck.Index).Value) = True)
            If CInt(nudEscalationDays.Value) > 0 AndAlso (drEmpCheck IsNot Nothing AndAlso drEmpCheck.Count <= 0) Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select atleast one employee(s) to send reminder for loan approval."), enMsgBoxStyle.Information)
                dgEmployee.Focus()
                Exit Sub
            End If
            'Pinkal (10-Nov-2022) -- End

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then
                Dim drDocType() As DataRow = dvLoanTrancheDocType.Table.Select("Ischecked=true")
                If drDocType.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Document Type is mondatory information. Please select document type for Attachement"), enMsgBoxStyle.Information)
                    dgLoanTrancheDocumentList.Focus()
                    Exit Sub
                End If
            End If
            'Pinkal (04-Aug-2023) -- End

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            If CInt(cboSeniorApproverLevel.SelectedValue) > 0 AndAlso CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then
                Dim lstApproverLevelIds As String = ""
                If dgSeniorLoanApprovalAlert.RowCount > 0 Then
                    lstApproverLevelIds = String.Join(",", dgSeniorLoanApprovalAlert.Rows.Cast(Of DataGridViewRow).AsEnumerable().Where(Function(x) CBool(x.Cells(objdgcolhLevelCheck.Index).Value) = True).Select(Function(x) x.Cells(objdgcolhLevelId.Index).Value.ToString()).ToArray())
                End If
                If lstApproverLevelIds.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Please select atleast one approver level(s) to send notification to specific loan approver level."), enMsgBoxStyle.Information)
                    dgSeniorLoanApprovalAlert.Focus()
                    Exit Sub
                End If
            End If
            'Pinkal (17-May-2024) -- End



            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objLoanScheme.Update()
            Else
                blnFlag = objLoanScheme.Insert()
            End If

            If blnFlag = False And objLoanScheme._Message <> "" Then
                eZeeMsgBox.Show(objLoanScheme._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objLoanScheme = Nothing
                    objLoanScheme = New clsLoan_Scheme
                    Call GetValue()
                    'Hemant (27 Oct 2022) -- Start
                    'ISSUE/ENHANCEMENT(NMB) : to Stop pass that to  Payroll in Internal Loan and only deduct from what we fetch from the loan view in External Loan(For working both Normal Loan & Flexcube Loan parallelly )
                    If ConfigParameter._Object._RoleBasedLoanApproval AndAlso ConfigParameter._Object._LoanIntegration = enLoanIntegration.FlexCube Then
                    chkSkipApproval.Checked = True
                    Else
                        chkSkipApproval.Checked = False
                    End If
                    'Hemant (27 Oct 2022) -- End
                    txtCode.Focus()
                Else
                    mintLoanSchemeunkid = objLoanScheme._Loanschemeunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

    'Hemant (24 Aug 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-805 - Develop the logic for maximum loan amount
    Private Sub objbtnKeywordsMaxInstallment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnKeywordsMaxInstallment.Click
        Dim frm As New frmRemark
        Dim strB As New StringBuilder
        Try
            frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 12, "Available Keywords")
            frm.Text = frm.objgbRemarks.Text
            Dim strRemarks As String = ""
            strB.Length = 0

            strB.AppendLine("#headcode#    e.g. ((#101# + #102#)-(#201# + #202#)) * 0.33")
            strB.AppendLine("#totalflexcubeloanamount#    -- Provide Total Installment Amount From Flexcube ")
            'Pinkal (11-Mar-2024) -- Start
            '(A1X-2505) NMB - Credit card integration.
            strB.AppendLine("#creditcardamountondsr#    -- Provide Credit Card Amount On DSR")
            'Pinkal (11-Mar-2024) -- End
            strRemarks = strB.ToString

            frm.displayDialog(strRemarks, enArutiApplicatinType.Aruti_Payroll)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnKeywordsMaxInstallment_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (24 Aug 2022) -- End
    'Hemant (07 Jul 2023) -- Start
    'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
    Private Sub objbtnKeywordsMaxInstallmentMortgage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnKeywordsMaxInstallmentMortgage.Click
        Dim frm As New frmRemark
        Dim strB As New StringBuilder
        Try
            frm.objgbRemarks.Text = Language.getMessage(mstrModuleName, 12, "Available Keywords")
            frm.Text = frm.objgbRemarks.Text
            Dim strRemarks As String = ""
            strB.Length = 0

            strB.AppendLine("#headcode#    e.g. ((#101# + #102#)-(#201# + #202#)) *  0.50")
            strB.AppendLine("#totalflexcubeloanamount#    -- Provide Total Installment Amount From Flexcube ")
            'Pinkal (11-Mar-2024) -- Start
            '(A1X-2505) NMB - Credit card integration.
            strB.AppendLine("#creditcardamountondsr#    -- Provide Credit Card Amount On DSR")
            'Pinkal (11-Mar-2024) -- End
            strRemarks = strB.ToString

            frm.displayDialog(strRemarks, enArutiApplicatinType.Aruti_Payroll)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnKeywordsMaxInstallmentMortgage_Click", mstrModuleName)
        End Try
    End Sub
    'Hemant (07 Jul 2023) -- End


#End Region

#Region "Radio Button's Event"

    'Hemant (24 Aug 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-821 - As a user, I want to be able to define maximum loan amount on a loan scheme as either a flat rate or a formula based on a payroll transaction head
    Private Sub radFlatRate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radFlatRate.CheckedChanged
        Try
            If radFlatRate.Checked Then
                pnlMaxLoanAmount.BringToFront()
                pnlMaxLoanFormula.SendToBack()
            Else
                pnlMaxLoanFormula.BringToFront()
                pnlMaxLoanAmount.SendToBack()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radFlatRate_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (24 Aug 2022) -- End

#End Region

#Region "Checkbox's Events"

    'Hemant (24 Aug 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-409 - As a user, I want to specify which loan schemes require attachments from the loan scheme master. I want to bind specific documents to loan schemes
    Private Sub chkAttachementRequired_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAttachementRequired.CheckedChanged
        Try
            If chkAttachementRequired.Checked = True Then
                dgvDocumentType.Enabled = True
            Else
                dgvDocumentType.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkAttachementRequired_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (24 Aug 2022) -- End

    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : - On loan scheme master, if a loan scheme has posting to flexcube enabled, as a user, I want to have the option of specifying the number of installments that need to be paid before a top-up can be done. 
    Private Sub chkPostingToFlexcube_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPostingToFlexcube.CheckedChanged
        Try
            If chkPostingToFlexcube.Checked = True Then
                'Pinkal (10-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                'lblNoOfInstallmentPaid.Visible = True
                'nudMinNoOfInstallmentPaid.Visible = True
                'Pinkal (10-Nov-2022) -- End

                'Hemant (27 Oct 2022) -- Start
                'ISSUE/ENHANCEMENT(NMB) : to Stop pass that to  Payroll in Internal Loan and only deduct from what we fetch from the loan view in External Loan(For working both Normal Loan & Flexcube Loan parallelly )
                chkEligibleForTopup.Enabled = True
                chkEligibleForTopup.Checked = False
                chkSkipApproval.Checked = False
                'Hemant (27 Oct 2022) -- End
            Else
                'Pinkal (10-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                'lblNoOfInstallmentPaid.Visible = False
                'nudMinNoOfInstallmentPaid.Visible = False
                'Pinkal (10-Nov-2022) -- End

                'Hemant (27 Oct 2022) -- Start
                'ISSUE/ENHANCEMENT(NMB) : to Stop pass that to  Payroll in Internal Loan and only deduct from what we fetch from the loan view in External Loan(For working both Normal Loan & Flexcube Loan parallelly )
                chkEligibleForTopup.Enabled = False
                chkEligibleForTopup.Checked = False
                chkSkipApproval.Checked = True
                'Hemant (27 Oct 2022) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkPostingToFlexcube_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (12 Oct 2022) -- End

    'Pinkal (10-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private Sub chkEligibleForTopup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEligibleForTopup.CheckedChanged
        Try
            nudMinNoOfInstallmentPaid.Visible = chkEligibleForTopup.Checked
            lblNoOfInstallmentPaid.Visible = chkEligibleForTopup.Checked
            If chkEligibleForTopup.Checked = False Then nudMinNoOfInstallmentPaid.Value = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkEligibleForTopup_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkEscalationDays_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEscalationDays.CheckedChanged
        Try
            nudEscalationDays.Enabled = chkEscalationDays.Checked
            gbEmployeeList.Enabled = chkEscalationDays.Checked
            If chkEscalationDays.Checked = False Then
                nudEscalationDays.Value = 0
                Dim drEmployee = CType(dgEmployee.DataSource, DataTable).Select("ischeck = True")
                If drEmployee IsNot Nothing AndAlso drEmployee.Count > 0 Then
                    For i As Integer = 0 To drEmployee.Length - 1
                        drEmployee(i)("ischeck") = False
                        drEmployee(i).AcceptChanges()
                    Next
                    chkSelectAllEmployee.Checked = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkEscalationDays_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkSelectAllEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAllEmployee.CheckedChanged
        Try
            RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
            For Each dr As DataRowView In dvEmployee
                dr("ischeck") = chkSelectAllEmployee.Checked
                dr.EndEdit()
            Next
            dvEmployee.Table().AcceptChanges()
            AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAllEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Pinkal (10-Nov-2022) -- End

    'Pinkal (21-Jul-2023) -- Start
    '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
    Private Sub chkNtfForExpiryofTitle_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNtfForExpiryofTitle.CheckedChanged
        Try
            nudNoOfDaysBefereTitleExpiry.Enabled = chkNtfForExpiryofTitle.Checked
            gbExpiryofTitleEmployeeList.Enabled = chkNtfForExpiryofTitle.Checked
            chkAllExpiryofTitleEmp.Enabled = chkNtfForExpiryofTitle.Checked
            If chkNtfForExpiryofTitle.Checked = False Then
                chkAllExpiryofTitleEmp.Checked = False
                txtExpiryofTitleEmpSearch.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkNtfForExpiryofTitle_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkAllExpiryofTitleEmp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAllExpiryofTitleEmp.CheckedChanged
        Try
            RemoveHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
            For Each dr As DataRowView In dvExpiryOfTitleEmp
                dr("ischeck") = chkAllExpiryofTitleEmp.Checked
                dr.EndEdit()
            Next
            dvEmployee.Table().AcceptChanges()
            AddHandler dgEmployee.CellContentClick, AddressOf dgEmployee_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkAllExpiryofTitleEmp_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    'Pinkal (21-Jul-2023) -- End

    'Hemant (06 Dec 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
    Private Sub chkFlexcubeSuccessfulNotification_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFlexcubeSuccessfulNotification.CheckedChanged
        Try
            gbFlexcubeSuccessfulEmployeeList.Enabled = chkFlexcubeSuccessfulNotification.Checked
            If chkFlexcubeSuccessfulNotification.Checked = False Then
                Dim drEmployee = CType(gvFlexcubeSuccessfulEmployeeList.DataSource, DataTable).Select("ischeck = True")
                If drEmployee IsNot Nothing AndAlso drEmployee.Count > 0 Then
                    For i As Integer = 0 To drEmployee.Length - 1
                        drEmployee(i)("ischeck") = False
                        drEmployee(i).AcceptChanges()
                    Next
                    chkSelectAllFinalApprovalEmployee.Checked = False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkFlexcubeSuccessfulNotification_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkSelectAllFinalApprovalEmployee_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAllFinalApprovalEmployee.CheckedChanged
        Try
            RemoveHandler gvFlexcubeSuccessfulEmployeeList.CellContentClick, AddressOf gvFlexcubeSuccessfulEmployeeList_CellContentClick
            For Each dr As DataRowView In dvFinalApprovalEmployee
                dr("ischeck") = chkSelectAllFinalApprovalEmployee.Checked
                dr.EndEdit()
            Next
            dvFinalApprovalEmployee.Table().AcceptChanges()
            AddHandler gvFlexcubeSuccessfulEmployeeList.CellContentClick, AddressOf gvFlexcubeSuccessfulEmployeeList_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAllFinalApprovalEmployee_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (06 Dec 2024) -- End

#End Region

    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
#Region " Other Controls Events "


    Private Sub txtEMIExceedPerc_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEMIExceedPerc.Leave
        Try
            If txtEMIExceedPerc.Decimal = 0 Then
                cboNetPay.SelectedValue = 0
                cboNetPay.Enabled = False

            Else
                cboNetPay.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtEMIExceedPerc_Leave", mstrModuleName)
        End Try
    End Sub

    'Pinkal (10-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private Sub txtEmployeeSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEmployeeSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtEmployeeSearch.Text.Trim.Length > 0 Then
                strSearch = "[" & dgcolhEmployee.DataPropertyName & "] like '%" & txtEmployeeSearch.Text.Trim & "%'"
            End If
            dvEmployee.RowFilter = strSearch
            SetCheckBoxValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtEmployeeSearch_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Pinkal (10-Nov-2022) -- End

    'Pinkal (21-Jul-2023) -- Start
    '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
    Private Sub txtExpiryofTitleEmpSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExpiryofTitleEmpSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtExpiryofTitleEmpSearch.Text.Trim.Length > 0 Then
                strSearch = "[" & dgcolhExpiryTitleEmployee.DataPropertyName & "] like '%" & txtExpiryofTitleEmpSearch.Text.Trim & "%'"
            End If
            dvExpiryOfTitleEmp.RowFilter = strSearch
            SetExpiryTitleCheckBoxValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtExpiryofTitleEmpSearch_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Pinkal (21-Jul-2023) -- End


    'Hemant (06 Dec 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
    Private Sub txtFlexcubeSuccessfulEmployeeSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFlexcubeSuccessfulEmployeeSearch.TextChanged
        Try
            Dim strSearch As String = ""
            If txtFlexcubeSuccessfulEmployeeSearch.Text.Trim.Length > 0 Then
                strSearch = "[" & dgcolhFinalApprovalEmployee.DataPropertyName & "] like '%" & txtFlexcubeSuccessfulEmployeeSearch.Text.Trim & "%'"
            End If
            dvFinalApprovalEmployee.RowFilter = strSearch
            SetFinalApprovalCheckBoxValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtFlexcubeSuccessfulEmployeeSearch_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (06 Dec 2024) -- End


#End Region
    'Sohail (11 Apr 2018) -- End

    'Pinkal (10-Nov-2022) -- Start
    'NMB Loan Module Enhancement.

#Region "DataGrid Event"

    Private Sub dgEmployee_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgEmployee.CellContentClick, dgEmployee.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            RemoveHandler chkSelectAllEmployee.CheckedChanged, AddressOf chkSelectAllEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhEmpCheck.Index Then
                If dgEmployee.IsCurrentCellDirty Then
                    dgEmployee.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                dvEmployee.Table.AcceptChanges()
                SetCheckBoxValue()
            End If

            AddHandler chkSelectAllEmployee.CheckedChanged, AddressOf chkSelectAllEmployee_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellContentClick", mstrModuleName)
        End Try
    End Sub

    'Pinkal (21-Jul-2023) -- Start
    '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
    Private Sub dgExpiryofTitleEmp_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgExpiryofTitleEmp.CellContentClick, dgExpiryofTitleEmp.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            RemoveHandler chkAllExpiryofTitleEmp.CheckedChanged, AddressOf chkAllExpiryofTitleEmp_CheckedChanged

            If e.ColumnIndex = objdgcolhExpiryTitleEmpCheck.Index Then
                If dgExpiryofTitleEmp.IsCurrentCellDirty Then
                    dgExpiryofTitleEmp.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                dvExpiryOfTitleEmp.Table.AcceptChanges()
                SetExpiryTitleCheckBoxValue()
            End If

            AddHandler chkAllExpiryofTitleEmp.CheckedChanged, AddressOf chkAllExpiryofTitleEmp_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgExpiryofTitleEmp_CellContentClick", mstrModuleName)
        End Try
    End Sub
    'Pinkal (21-Jul-2023) -- End


    'Hemant (06 Dec 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
    Private Sub gvFlexcubeSuccessfulEmployeeList_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gvFlexcubeSuccessfulEmployeeList.CellContentClick, gvFlexcubeSuccessfulEmployeeList.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            RemoveHandler chkSelectAllFinalApprovalEmployee.CheckedChanged, AddressOf chkSelectAllFinalApprovalEmployee_CheckedChanged

            If e.ColumnIndex = objdgcolhFinalApprovalEmpCheck.Index Then
                If gvFlexcubeSuccessfulEmployeeList.IsCurrentCellDirty Then
                    gvFlexcubeSuccessfulEmployeeList.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                dvFinalApprovalEmployee.Table.AcceptChanges()
                SetFinalApprovalCheckBoxValue()
            End If

            AddHandler chkSelectAllFinalApprovalEmployee.CheckedChanged, AddressOf chkSelectAllFinalApprovalEmployee_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "gvFlexcubeSuccessfulEmployeeList_CellContentClick", mstrModuleName)
        End Try
    End Sub
    'Hemant (06 Dec 2024) -- End


#End Region

    'Pinkal (10-Nov-2022) -- End



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			Call SetLanguage()
			
			Me.gbLoanInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbLoanInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

            Me.gbDocumentTypeList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDocumentTypeList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbExpiryofTitleEmployeeList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbExpiryofTitleEmployeeList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbLoanTrancheDocumentList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLoanTrancheDocumentList.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbSeniorLoanApprovalAlert.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbSeniorLoanApprovalAlert.ForeColor = GUI._eZeeContainerHeaderForeColor


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.gbLoanInfo.Text = Language._Object.getCaption(Me.gbLoanInfo.Name, Me.gbLoanInfo.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.gbDocumentTypeList.Text = Language._Object.getCaption(Me.gbDocumentTypeList.Name, Me.gbDocumentTypeList.Text)
            Me.dgColhDocumentType.HeaderText = Language._Object.getCaption(Me.dgColhDocumentType.Name, Me.dgColhDocumentType.HeaderText)
            Me.chkAttachementRequired.Text = Language._Object.getCaption(Me.chkAttachementRequired.Name, Me.chkAttachementRequired.Text)
            Me.lblMaximumLoanAmount.Text = Language._Object.getCaption(Me.lblMaximumLoanAmount.Name, Me.lblMaximumLoanAmount.Text)
            Me.radFormula.Text = Language._Object.getCaption(Me.radFormula.Name, Me.radFormula.Text)
            Me.lblMaxLoanFormula.Text = Language._Object.getCaption(Me.lblMaxLoanFormula.Name, Me.lblMaxLoanFormula.Text)
            Me.radFlatRate.Text = Language._Object.getCaption(Me.radFlatRate.Name, Me.radFlatRate.Text)
            Me.lblMaxLoanCalcType.Text = Language._Object.getCaption(Me.lblMaxLoanCalcType.Name, Me.lblMaxLoanCalcType.Text)
            Me.lblInsuranceRate.Text = Language._Object.getCaption(Me.lblInsuranceRate.Name, Me.lblInsuranceRate.Text)
            Me.lblRepaymentDays.Text = Language._Object.getCaption(Me.lblRepaymentDays.Name, Me.lblRepaymentDays.Text)
            Me.lblLoanSchemeCategory.Text = Language._Object.getCaption(Me.lblLoanSchemeCategory.Name, Me.lblLoanSchemeCategory.Text)
            Me.lblMaxInstallmentAmtCalculation.Text = Language._Object.getCaption(Me.lblMaxInstallmentAmtCalculation.Name, Me.lblMaxInstallmentAmtCalculation.Text)
            Me.lblMappedHead.Text = Language._Object.getCaption(Me.lblMappedHead.Name, Me.lblMappedHead.Text)
            Me.lblCostCenter.Text = Language._Object.getCaption(Me.lblCostCenter.Name, Me.lblCostCenter.Text)
            Me.lblNetPay.Text = Language._Object.getCaption(Me.lblNetPay.Name, Me.lblNetPay.Text)
            Me.lblMaxNoOfInstallment.Text = Language._Object.getCaption(Me.lblMaxNoOfInstallment.Name, Me.lblMaxNoOfInstallment.Text)
            Me.lblEMIExceedPerc2.Text = Language._Object.getCaption(Me.lblEMIExceedPerc2.Name, Me.lblEMIExceedPerc2.Text)
            Me.lblEMIExceedPerc.Text = Language._Object.getCaption(Me.lblEMIExceedPerc.Name, Me.lblEMIExceedPerc.Text)
            Me.elDefaultInfo.Text = Language._Object.getCaption(Me.elDefaultInfo.Name, Me.elDefaultInfo.Text)
            Me.lblInterestCalcType.Text = Language._Object.getCaption(Me.lblInterestCalcType.Name, Me.lblInterestCalcType.Text)
            Me.lblLoanInterest.Text = Language._Object.getCaption(Me.lblLoanInterest.Name, Me.lblLoanInterest.Text)
            Me.lblLoanCalcType.Text = Language._Object.getCaption(Me.lblLoanCalcType.Name, Me.lblLoanCalcType.Text)
            Me.chkShowLoanBalOnPayslip.Text = Language._Object.getCaption(Me.chkShowLoanBalOnPayslip.Name, Me.chkShowLoanBalOnPayslip.Text)
            Me.chkShowonESS.Text = Language._Object.getCaption(Me.chkShowonESS.Name, Me.chkShowonESS.Text)
			Me.lblDescription.Text = Language._Object.getCaption(Me.lblDescription.Name, Me.lblDescription.Text)
			Me.lnEligibility.Text = Language._Object.getCaption(Me.lnEligibility.Name, Me.lnEligibility.Text)
			Me.lblMinSalary.Text = Language._Object.getCaption(Me.lblMinSalary.Name, Me.lblMinSalary.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblCode.Text = Language._Object.getCaption(Me.lblCode.Name, Me.lblCode.Text)
            Me.chkRequiredReportingToApproval.Text = Language._Object.getCaption(Me.chkRequiredReportingToApproval.Name, Me.chkRequiredReportingToApproval.Text)
            Me.lblMinimumLoanAmount.Text = Language._Object.getCaption(Me.lblMinimumLoanAmount.Name, Me.lblMinimumLoanAmount.Text)
            Me.chkSkipApproval.Text = Language._Object.getCaption(Me.chkSkipApproval.Name, Me.chkSkipApproval.Text)
            Me.chkPostingToFlexcube.Text = Language._Object.getCaption(Me.chkPostingToFlexcube.Name, Me.chkPostingToFlexcube.Text)
            Me.chkEligibleForTopup.Text = Language._Object.getCaption(Me.chkEligibleForTopup.Name, Me.chkEligibleForTopup.Text)
            Me.lblNoOfInstallmentPaid.Text = Language._Object.getCaption(Me.lblNoOfInstallmentPaid.Name, Me.lblNoOfInstallmentPaid.Text)
            Me.chkLoanApprovalDailyReminder.Text = Language._Object.getCaption(Me.chkLoanApprovalDailyReminder.Name, Me.chkLoanApprovalDailyReminder.Text)
            Me.gbEmployeeList.Text = Language._Object.getCaption(Me.gbEmployeeList.Name, Me.gbEmployeeList.Text)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.chkEscalationDays.Text = Language._Object.getCaption(Me.chkEscalationDays.Name, Me.chkEscalationDays.Text)
            Me.LblDays.Text = Language._Object.getCaption(Me.LblDays.Name, Me.LblDays.Text)
            Me.chkSelectAllEmployee.Text = Language._Object.getCaption(Me.chkSelectAllEmployee.Name, Me.chkSelectAllEmployee.Text)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.lblMinNoOfInstallment.Text = Language._Object.getCaption(Me.lblMinNoOfInstallment.Name, Me.lblMinNoOfInstallment.Text)
            Me.lblNoOfDaysBefereTitleExpiry.Text = Language._Object.getCaption(Me.lblNoOfDaysBefereTitleExpiry.Name, Me.lblNoOfDaysBefereTitleExpiry.Text)
            Me.lblMaxInstallmentAmtForMortgage.Text = Language._Object.getCaption(Me.lblMaxInstallmentAmtForMortgage.Name, Me.lblMaxInstallmentAmtForMortgage.Text)
            Me.chkNtfForExpiryofTitle.Text = Language._Object.getCaption(Me.chkNtfForExpiryofTitle.Name, Me.chkNtfForExpiryofTitle.Text)
            Me.gbExpiryofTitleEmployeeList.Text = Language._Object.getCaption(Me.gbExpiryofTitleEmployeeList.Name, Me.gbExpiryofTitleEmployeeList.Text)
            Me.chkAllExpiryofTitleEmp.Text = Language._Object.getCaption(Me.chkAllExpiryofTitleEmp.Name, Me.chkAllExpiryofTitleEmp.Text)
            Me.dgcolhExpiryTitleEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhExpiryTitleEmployee.Name, Me.dgcolhExpiryTitleEmployee.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.tbpLoanOtherOptions.Text = Language._Object.getCaption(Me.tbpLoanOtherOptions.Name, Me.tbpLoanOtherOptions.Text)
            Me.tbpLoanApprovelReminder.Text = Language._Object.getCaption(Me.tbpLoanApprovelReminder.Name, Me.tbpLoanApprovelReminder.Text)
            Me.tbpMortgageLoanOptions.Text = Language._Object.getCaption(Me.tbpMortgageLoanOptions.Name, Me.tbpMortgageLoanOptions.Text)
            Me.gbLoanTrancheDocumentList.Text = Language._Object.getCaption(Me.gbLoanTrancheDocumentList.Name, Me.gbLoanTrancheDocumentList.Text)
            Me.dgcolhLoanTranchedocumentType.HeaderText = Language._Object.getCaption(Me.dgcolhLoanTranchedocumentType.Name, Me.dgcolhLoanTranchedocumentType.HeaderText)
            Me.LblCreditCardPercentageDSR.Text = Language._Object.getCaption(Me.LblCreditCardPercentageDSR.Name, Me.LblCreditCardPercentageDSR.Text)
            Me.LblCreditCardAmtOnDSR.Text = Language._Object.getCaption(Me.LblCreditCardAmtOnDSR.Name, Me.LblCreditCardAmtOnDSR.Text)
            Me.LblCreditCardAmountForExpouser.Text = Language._Object.getCaption(Me.LblCreditCardAmountForExpouser.Name, Me.LblCreditCardAmountForExpouser.Text)
            Me.chkFSVValueMandatory.Text = Language._Object.getCaption(Me.chkFSVValueMandatory.Name, Me.chkFSVValueMandatory.Text)
            Me.chkMarketValueMandatory.Text = Language._Object.getCaption(Me.chkMarketValueMandatory.Name, Me.chkMarketValueMandatory.Text)
            Me.chkBOQMandatory.Text = Language._Object.getCaption(Me.chkBOQMandatory.Name, Me.chkBOQMandatory.Text)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.gbSeniorLoanApprovalAlert.Text = Language._Object.getCaption(Me.gbSeniorLoanApprovalAlert.Name, Me.gbSeniorLoanApprovalAlert.Text)
            Me.Label1.Text = Language._Object.getCaption(Me.Label1.Name, Me.Label1.Text)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
            Me.dgcolhApproverLevel.HeaderText = Language._Object.getCaption(Me.dgcolhApproverLevel.Name, Me.dgcolhApproverLevel.HeaderText)
            Me.dgcolhPriority.HeaderText = Language._Object.getCaption(Me.dgcolhPriority.Name, Me.dgcolhPriority.HeaderText)
            Me.chkDisableEmployeeConfirm.Text = Language._Object.getCaption(Me.chkDisableEmployeeConfirm.Name, Me.chkDisableEmployeeConfirm.Text)
            Me.chkFlexcubeSuccessfulNotification.Text = Language._Object.getCaption(Me.chkFlexcubeSuccessfulNotification.Name, Me.chkFlexcubeSuccessfulNotification.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Loan Scheme cannot be blank. Loan Scheme is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Minimum Salary cannot be blank. Please insert Minimum Salary.")
			Language.setMessage(mstrModuleName, 4, "Sorry, Loan calculation type is mandatory information. Please select loan calculation type.")
			Language.setMessage(mstrModuleName, 5, "Sorry, Percentage should be in between 0 and 100.")
			Language.setMessage(mstrModuleName, 6, "Sorry, Maximum number of installment should not be greater than 360.")
			Language.setMessage(mstrModuleName, 7, "Type to Search")
			Language.setMessage(mstrModuleName, 8, "Please select Mapped Head.")
			Language.setMessage(mstrModuleName, 9, "Sorry, Maximum number of installment is mandatory.")
            Language.setMessage(mstrModuleName, 10, "Document Type is mondatory information. Please select document type for Attachement")
            Language.setMessage(mstrModuleName, 11, "Sorry, Minimum No. of Installment to be Paid should not be greater than 0.")
            Language.setMessage(mstrModuleName, 12, "Available Keywords")
            Language.setMessage(mstrModuleName, 13, "Please Set Recurrent Reminder Occurrence in day(s) to send reminder.")
            Language.setMessage(mstrModuleName, 14, "Please select atleast one employee(s) to send reminder for loan approval.")
            Language.setMessage(mstrModuleName, 15, "Please select atleast one approver level(s) to send notification to specific loan approver level.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class