﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanScheme_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanScheme_AddEdit))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.gbLoanInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlLoanScheme = New System.Windows.Forms.Panel
        Me.cboCreditCardAmountForExpouser = New System.Windows.Forms.ComboBox
        Me.LblCreditCardAmountForExpouser = New System.Windows.Forms.Label
        Me.cboCreditCardAmountOnDSR = New System.Windows.Forms.ComboBox
        Me.LblCreditCardAmtOnDSR = New System.Windows.Forms.Label
        Me.txtCreditCardPercentageDSR = New eZee.TextBox.NumericTextBox
        Me.LblCreditCardPercentageDSR = New System.Windows.Forms.Label
        Me.tabLoanScheme = New System.Windows.Forms.TabControl
        Me.tbpLoanOtherOptions = New System.Windows.Forms.TabPage
        Me.pnlLoanOtherOptions = New System.Windows.Forms.Panel
        Me.chkDisableEmployeeConfirm = New System.Windows.Forms.CheckBox
        Me.chkShowLoanBalOnPayslip = New System.Windows.Forms.CheckBox
        Me.chkShowonESS = New System.Windows.Forms.CheckBox
        Me.chkAttachementRequired = New System.Windows.Forms.CheckBox
        Me.gbDocumentTypeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.dgvDocumentType = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhMasterunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgColhDocumentType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.chkRequiredReportingToApproval = New System.Windows.Forms.CheckBox
        Me.chkSkipApproval = New System.Windows.Forms.CheckBox
        Me.chkPostingToFlexcube = New System.Windows.Forms.CheckBox
        Me.chkEligibleForTopup = New System.Windows.Forms.CheckBox
        Me.nudMinNoOfInstallmentPaid = New System.Windows.Forms.NumericUpDown
        Me.lblNoOfInstallmentPaid = New System.Windows.Forms.Label
        Me.tbpLoanApprovelReminder = New System.Windows.Forms.TabPage
        Me.pnlLoanApprovalReminderOptions = New System.Windows.Forms.Panel
        Me.chkFlexcubeSuccessfulNotification = New System.Windows.Forms.CheckBox
        Me.gbFlexcubeSuccessfulEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtFlexcubeSuccessfulEmployeeSearch = New eZee.TextBox.AlphanumericTextBox
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.chkSelectAllFinalApprovalEmployee = New System.Windows.Forms.CheckBox
        Me.gvFlexcubeSuccessfulEmployeeList = New System.Windows.Forms.DataGridView
        Me.objdgcolhFinalApprovalEmpCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhFinalApprovalEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhFinalApprovalEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.chkLoanApprovalDailyReminder = New System.Windows.Forms.CheckBox
        Me.gbEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtEmployeeSearch = New eZee.TextBox.AlphanumericTextBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.chkSelectAllEmployee = New System.Windows.Forms.CheckBox
        Me.dgEmployee = New System.Windows.Forms.DataGridView
        Me.objdgcolhEmpCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.chkEscalationDays = New System.Windows.Forms.CheckBox
        Me.nudEscalationDays = New System.Windows.Forms.NumericUpDown
        Me.LblDays = New System.Windows.Forms.Label
        Me.tbpMortgageLoanOptions = New System.Windows.Forms.TabPage
        Me.pnlMortgageLoanOptions = New System.Windows.Forms.Panel
        Me.gbSeniorLoanApprovalAlert = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboSeniorApproverLevel = New System.Windows.Forms.ComboBox
        Me.pnlLoanApproverLevel = New System.Windows.Forms.Panel
        Me.dgSeniorLoanApprovalAlert = New System.Windows.Forms.DataGridView
        Me.objdgcolhLevelCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhApproverLevel = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPriority = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhLevelId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkBOQMandatory = New System.Windows.Forms.CheckBox
        Me.chkFSVValueMandatory = New System.Windows.Forms.CheckBox
        Me.chkMarketValueMandatory = New System.Windows.Forms.CheckBox
        Me.gbLoanTrancheDocumentList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.dgLoanTrancheDocumentList = New System.Windows.Forms.DataGridView
        Me.objdgcolhLoanTranCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhLoanTrancheMasterunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLoanTranchedocumentType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.nudNoOfDaysBefereTitleExpiry = New System.Windows.Forms.NumericUpDown
        Me.gbExpiryofTitleEmployeeList = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtExpiryofTitleEmpSearch = New eZee.TextBox.AlphanumericTextBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.chkAllExpiryofTitleEmp = New System.Windows.Forms.CheckBox
        Me.dgExpiryofTitleEmp = New System.Windows.Forms.DataGridView
        Me.objdgcolhExpiryTitleEmpCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhExpiryTitleEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhExpiryTitleEmployeeId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblNoOfDaysBefereTitleExpiry = New System.Windows.Forms.Label
        Me.chkNtfForExpiryofTitle = New System.Windows.Forms.CheckBox
        Me.objbtnKeywordsMaxInstallmentMortgage = New eZee.Common.eZeeGradientButton
        Me.txtMaxInstallmentAmtForMortgage = New System.Windows.Forms.TextBox
        Me.lblMaxInstallmentAmtForMortgage = New System.Windows.Forms.Label
        Me.txtMinNoOfInstallment = New eZee.TextBox.NumericTextBox
        Me.lblMinNoOfInstallment = New System.Windows.Forms.Label
        Me.lblMinimumLoanAmount = New System.Windows.Forms.Label
        Me.txtMinLoanAmount = New eZee.TextBox.NumericTextBox
        Me.pnlMaxLoanAmount = New System.Windows.Forms.Panel
        Me.lblMaximumLoanAmount = New System.Windows.Forms.Label
        Me.txtMaxLoanAmount = New eZee.TextBox.NumericTextBox
        Me.radFormula = New System.Windows.Forms.RadioButton
        Me.pnlMaxLoanFormula = New System.Windows.Forms.Panel
        Me.txtMaxLoanFormula = New System.Windows.Forms.TextBox
        Me.lblMaxLoanFormula = New System.Windows.Forms.Label
        Me.radFlatRate = New System.Windows.Forms.RadioButton
        Me.lblMaxLoanCalcType = New System.Windows.Forms.Label
        Me.txtInsuranceRate = New eZee.TextBox.NumericTextBox
        Me.lblInsuranceRate = New System.Windows.Forms.Label
        Me.txtRepaymentDays = New eZee.TextBox.NumericTextBox
        Me.lblRepaymentDays = New System.Windows.Forms.Label
        Me.cboLoanSchemeCategory = New System.Windows.Forms.ComboBox
        Me.lblLoanSchemeCategory = New System.Windows.Forms.Label
        Me.objbtnKeywordsMaxInstallment = New eZee.Common.eZeeGradientButton
        Me.txtMaxInstallmentAmtCalculation = New System.Windows.Forms.TextBox
        Me.lblMaxInstallmentAmtCalculation = New System.Windows.Forms.Label
        Me.cboMappedHead = New System.Windows.Forms.ComboBox
        Me.lblMappedHead = New System.Windows.Forms.Label
        Me.cboCostCenter = New System.Windows.Forms.ComboBox
        Me.lblCostCenter = New System.Windows.Forms.Label
        Me.cboNetPay = New System.Windows.Forms.ComboBox
        Me.lblNetPay = New System.Windows.Forms.Label
        Me.txtMaxNoOfInstallment = New eZee.TextBox.NumericTextBox
        Me.lblMaxNoOfInstallment = New System.Windows.Forms.Label
        Me.lblEMIExceedPerc2 = New System.Windows.Forms.Label
        Me.txtEMIExceedPerc = New eZee.TextBox.NumericTextBox
        Me.lblEMIExceedPerc = New System.Windows.Forms.Label
        Me.elDefaultInfo = New eZee.Common.eZeeLine
        Me.pnlCalcType = New System.Windows.Forms.Panel
        Me.cboInterestCalcType = New System.Windows.Forms.ComboBox
        Me.txtLoanRate = New eZee.TextBox.NumericTextBox
        Me.lblInterestCalcType = New System.Windows.Forms.Label
        Me.lblLoanInterest = New System.Windows.Forms.Label
        Me.cboLoanCalcType = New System.Windows.Forms.ComboBox
        Me.lblLoanCalcType = New System.Windows.Forms.Label
        Me.lblDescription = New System.Windows.Forms.Label
        Me.txtDescription = New eZee.TextBox.AlphanumericTextBox
        Me.lnEligibility = New eZee.Common.eZeeLine
        Me.txtMinSalary = New eZee.TextBox.NumericTextBox
        Me.lblMinSalary = New System.Windows.Forms.Label
        Me.txtName = New eZee.TextBox.AlphanumericTextBox
        Me.lblName = New System.Windows.Forms.Label
        Me.txtCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        Me.gbLoanInfo.SuspendLayout()
        Me.pnlLoanScheme.SuspendLayout()
        Me.tabLoanScheme.SuspendLayout()
        Me.tbpLoanOtherOptions.SuspendLayout()
        Me.pnlLoanOtherOptions.SuspendLayout()
        Me.gbDocumentTypeList.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvDocumentType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMinNoOfInstallmentPaid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpLoanApprovelReminder.SuspendLayout()
        Me.pnlLoanApprovalReminderOptions.SuspendLayout()
        Me.gbFlexcubeSuccessfulEmployeeList.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.gvFlexcubeSuccessfulEmployeeList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbEmployeeList.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudEscalationDays, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbpMortgageLoanOptions.SuspendLayout()
        Me.pnlMortgageLoanOptions.SuspendLayout()
        Me.gbSeniorLoanApprovalAlert.SuspendLayout()
        Me.pnlLoanApproverLevel.SuspendLayout()
        CType(Me.dgSeniorLoanApprovalAlert, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbLoanTrancheDocumentList.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.dgLoanTrancheDocumentList, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudNoOfDaysBefereTitleExpiry, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbExpiryofTitleEmployeeList.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgExpiryofTitleEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMaxLoanAmount.SuspendLayout()
        Me.pnlMaxLoanFormula.SuspendLayout()
        Me.pnlCalcType.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.gbLoanInfo)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(685, 579)
        Me.pnlMainInfo.TabIndex = 0
        '
        'gbLoanInfo
        '
        Me.gbLoanInfo.BorderColor = System.Drawing.Color.Black
        Me.gbLoanInfo.Checked = False
        Me.gbLoanInfo.CollapseAllExceptThis = False
        Me.gbLoanInfo.CollapsedHoverImage = Nothing
        Me.gbLoanInfo.CollapsedNormalImage = Nothing
        Me.gbLoanInfo.CollapsedPressedImage = Nothing
        Me.gbLoanInfo.CollapseOnLoad = False
        Me.gbLoanInfo.Controls.Add(Me.pnlLoanScheme)
        Me.gbLoanInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbLoanInfo.ExpandedHoverImage = Nothing
        Me.gbLoanInfo.ExpandedNormalImage = Nothing
        Me.gbLoanInfo.ExpandedPressedImage = Nothing
        Me.gbLoanInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLoanInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLoanInfo.HeaderHeight = 25
        Me.gbLoanInfo.HeaderMessage = ""
        Me.gbLoanInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbLoanInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLoanInfo.HeightOnCollapse = 0
        Me.gbLoanInfo.LeftTextSpace = 0
        Me.gbLoanInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbLoanInfo.Name = "gbLoanInfo"
        Me.gbLoanInfo.OpenHeight = 300
        Me.gbLoanInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLoanInfo.ShowBorder = True
        Me.gbLoanInfo.ShowCheckBox = False
        Me.gbLoanInfo.ShowCollapseButton = False
        Me.gbLoanInfo.ShowDefaultBorderColor = True
        Me.gbLoanInfo.ShowDownButton = False
        Me.gbLoanInfo.ShowHeader = True
        Me.gbLoanInfo.Size = New System.Drawing.Size(685, 529)
        Me.gbLoanInfo.TabIndex = 0
        Me.gbLoanInfo.Temp = 0
        Me.gbLoanInfo.Text = "Loan Scheme Information"
        Me.gbLoanInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlLoanScheme
        '
        Me.pnlLoanScheme.AutoScroll = True
        Me.pnlLoanScheme.Controls.Add(Me.cboCreditCardAmountForExpouser)
        Me.pnlLoanScheme.Controls.Add(Me.LblCreditCardAmountForExpouser)
        Me.pnlLoanScheme.Controls.Add(Me.cboCreditCardAmountOnDSR)
        Me.pnlLoanScheme.Controls.Add(Me.LblCreditCardAmtOnDSR)
        Me.pnlLoanScheme.Controls.Add(Me.txtCreditCardPercentageDSR)
        Me.pnlLoanScheme.Controls.Add(Me.LblCreditCardPercentageDSR)
        Me.pnlLoanScheme.Controls.Add(Me.tabLoanScheme)
        Me.pnlLoanScheme.Controls.Add(Me.objbtnKeywordsMaxInstallmentMortgage)
        Me.pnlLoanScheme.Controls.Add(Me.txtMaxInstallmentAmtForMortgage)
        Me.pnlLoanScheme.Controls.Add(Me.lblMaxInstallmentAmtForMortgage)
        Me.pnlLoanScheme.Controls.Add(Me.txtMinNoOfInstallment)
        Me.pnlLoanScheme.Controls.Add(Me.lblMinNoOfInstallment)
        Me.pnlLoanScheme.Controls.Add(Me.lblMinimumLoanAmount)
        Me.pnlLoanScheme.Controls.Add(Me.txtMinLoanAmount)
        Me.pnlLoanScheme.Controls.Add(Me.pnlMaxLoanAmount)
        Me.pnlLoanScheme.Controls.Add(Me.radFormula)
        Me.pnlLoanScheme.Controls.Add(Me.pnlMaxLoanFormula)
        Me.pnlLoanScheme.Controls.Add(Me.radFlatRate)
        Me.pnlLoanScheme.Controls.Add(Me.lblMaxLoanCalcType)
        Me.pnlLoanScheme.Controls.Add(Me.txtInsuranceRate)
        Me.pnlLoanScheme.Controls.Add(Me.lblInsuranceRate)
        Me.pnlLoanScheme.Controls.Add(Me.txtRepaymentDays)
        Me.pnlLoanScheme.Controls.Add(Me.lblRepaymentDays)
        Me.pnlLoanScheme.Controls.Add(Me.cboLoanSchemeCategory)
        Me.pnlLoanScheme.Controls.Add(Me.lblLoanSchemeCategory)
        Me.pnlLoanScheme.Controls.Add(Me.objbtnKeywordsMaxInstallment)
        Me.pnlLoanScheme.Controls.Add(Me.txtMaxInstallmentAmtCalculation)
        Me.pnlLoanScheme.Controls.Add(Me.lblMaxInstallmentAmtCalculation)
        Me.pnlLoanScheme.Controls.Add(Me.cboMappedHead)
        Me.pnlLoanScheme.Controls.Add(Me.lblMappedHead)
        Me.pnlLoanScheme.Controls.Add(Me.cboCostCenter)
        Me.pnlLoanScheme.Controls.Add(Me.lblCostCenter)
        Me.pnlLoanScheme.Controls.Add(Me.cboNetPay)
        Me.pnlLoanScheme.Controls.Add(Me.lblNetPay)
        Me.pnlLoanScheme.Controls.Add(Me.txtMaxNoOfInstallment)
        Me.pnlLoanScheme.Controls.Add(Me.lblMaxNoOfInstallment)
        Me.pnlLoanScheme.Controls.Add(Me.lblEMIExceedPerc2)
        Me.pnlLoanScheme.Controls.Add(Me.txtEMIExceedPerc)
        Me.pnlLoanScheme.Controls.Add(Me.lblEMIExceedPerc)
        Me.pnlLoanScheme.Controls.Add(Me.elDefaultInfo)
        Me.pnlLoanScheme.Controls.Add(Me.pnlCalcType)
        Me.pnlLoanScheme.Controls.Add(Me.cboLoanCalcType)
        Me.pnlLoanScheme.Controls.Add(Me.lblLoanCalcType)
        Me.pnlLoanScheme.Controls.Add(Me.lblDescription)
        Me.pnlLoanScheme.Controls.Add(Me.txtDescription)
        Me.pnlLoanScheme.Controls.Add(Me.lnEligibility)
        Me.pnlLoanScheme.Controls.Add(Me.txtMinSalary)
        Me.pnlLoanScheme.Controls.Add(Me.lblMinSalary)
        Me.pnlLoanScheme.Controls.Add(Me.txtName)
        Me.pnlLoanScheme.Controls.Add(Me.lblName)
        Me.pnlLoanScheme.Controls.Add(Me.txtCode)
        Me.pnlLoanScheme.Controls.Add(Me.lblCode)
        Me.pnlLoanScheme.Location = New System.Drawing.Point(0, 26)
        Me.pnlLoanScheme.Name = "pnlLoanScheme"
        Me.pnlLoanScheme.Size = New System.Drawing.Size(682, 498)
        Me.pnlLoanScheme.TabIndex = 391
        '
        'cboCreditCardAmountForExpouser
        '
        Me.cboCreditCardAmountForExpouser.DropDownWidth = 250
        Me.cboCreditCardAmountForExpouser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCreditCardAmountForExpouser.FormattingEnabled = True
        Me.cboCreditCardAmountForExpouser.Location = New System.Drawing.Point(164, 450)
        Me.cboCreditCardAmountForExpouser.Name = "cboCreditCardAmountForExpouser"
        Me.cboCreditCardAmountForExpouser.Size = New System.Drawing.Size(172, 21)
        Me.cboCreditCardAmountForExpouser.TabIndex = 466
        '
        'LblCreditCardAmountForExpouser
        '
        Me.LblCreditCardAmountForExpouser.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCreditCardAmountForExpouser.Location = New System.Drawing.Point(13, 450)
        Me.LblCreditCardAmountForExpouser.Name = "LblCreditCardAmountForExpouser"
        Me.LblCreditCardAmountForExpouser.Size = New System.Drawing.Size(146, 29)
        Me.LblCreditCardAmountForExpouser.TabIndex = 465
        Me.LblCreditCardAmountForExpouser.Text = "Credit Card Amount for Exposure Consideration"
        Me.LblCreditCardAmountForExpouser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCreditCardAmountOnDSR
        '
        Me.cboCreditCardAmountOnDSR.DropDownWidth = 250
        Me.cboCreditCardAmountOnDSR.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCreditCardAmountOnDSR.FormattingEnabled = True
        Me.cboCreditCardAmountOnDSR.Location = New System.Drawing.Point(508, 426)
        Me.cboCreditCardAmountOnDSR.Name = "cboCreditCardAmountOnDSR"
        Me.cboCreditCardAmountOnDSR.Size = New System.Drawing.Size(147, 21)
        Me.cboCreditCardAmountOnDSR.TabIndex = 464
        '
        'LblCreditCardAmtOnDSR
        '
        Me.LblCreditCardAmtOnDSR.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCreditCardAmtOnDSR.Location = New System.Drawing.Point(351, 428)
        Me.LblCreditCardAmtOnDSR.Name = "LblCreditCardAmtOnDSR"
        Me.LblCreditCardAmtOnDSR.Size = New System.Drawing.Size(151, 16)
        Me.LblCreditCardAmtOnDSR.TabIndex = 463
        Me.LblCreditCardAmtOnDSR.Text = "Credit Card Amount on DSR"
        Me.LblCreditCardAmtOnDSR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCreditCardPercentageDSR
        '
        Me.txtCreditCardPercentageDSR.AllowNegative = False
        Me.txtCreditCardPercentageDSR.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtCreditCardPercentageDSR.DigitsInGroup = 0
        Me.txtCreditCardPercentageDSR.Flags = 65536
        Me.txtCreditCardPercentageDSR.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCreditCardPercentageDSR.Location = New System.Drawing.Point(164, 423)
        Me.txtCreditCardPercentageDSR.MaxDecimalPlaces = 2
        Me.txtCreditCardPercentageDSR.MaxWholeDigits = 21
        Me.txtCreditCardPercentageDSR.Name = "txtCreditCardPercentageDSR"
        Me.txtCreditCardPercentageDSR.Prefix = ""
        Me.txtCreditCardPercentageDSR.RangeMax = 1.7976931348623157E+308
        Me.txtCreditCardPercentageDSR.RangeMin = -1.7976931348623157E+308
        Me.txtCreditCardPercentageDSR.Size = New System.Drawing.Size(172, 21)
        Me.txtCreditCardPercentageDSR.TabIndex = 462
        Me.txtCreditCardPercentageDSR.Text = "0"
        Me.txtCreditCardPercentageDSR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblCreditCardPercentageDSR
        '
        Me.LblCreditCardPercentageDSR.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCreditCardPercentageDSR.Location = New System.Drawing.Point(11, 425)
        Me.LblCreditCardPercentageDSR.Name = "LblCreditCardPercentageDSR"
        Me.LblCreditCardPercentageDSR.Size = New System.Drawing.Size(144, 16)
        Me.LblCreditCardPercentageDSR.TabIndex = 461
        Me.LblCreditCardPercentageDSR.Text = "Credit Card % on DSR"
        Me.LblCreditCardPercentageDSR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tabLoanScheme
        '
        Me.tabLoanScheme.Controls.Add(Me.tbpLoanOtherOptions)
        Me.tabLoanScheme.Controls.Add(Me.tbpLoanApprovelReminder)
        Me.tabLoanScheme.Controls.Add(Me.tbpMortgageLoanOptions)
        Me.tabLoanScheme.Location = New System.Drawing.Point(5, 552)
        Me.tabLoanScheme.Name = "tabLoanScheme"
        Me.tabLoanScheme.SelectedIndex = 0
        Me.tabLoanScheme.Size = New System.Drawing.Size(657, 313)
        Me.tabLoanScheme.TabIndex = 393
        '
        'tbpLoanOtherOptions
        '
        Me.tbpLoanOtherOptions.BackColor = System.Drawing.Color.Transparent
        Me.tbpLoanOtherOptions.Controls.Add(Me.pnlLoanOtherOptions)
        Me.tbpLoanOtherOptions.Location = New System.Drawing.Point(4, 22)
        Me.tbpLoanOtherOptions.Name = "tbpLoanOtherOptions"
        Me.tbpLoanOtherOptions.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpLoanOtherOptions.Size = New System.Drawing.Size(649, 287)
        Me.tbpLoanOtherOptions.TabIndex = 0
        Me.tbpLoanOtherOptions.Text = "Loan Other Options"
        Me.tbpLoanOtherOptions.UseVisualStyleBackColor = True
        '
        'pnlLoanOtherOptions
        '
        Me.pnlLoanOtherOptions.AutoScroll = True
        Me.pnlLoanOtherOptions.BackColor = System.Drawing.SystemColors.Control
        Me.pnlLoanOtherOptions.Controls.Add(Me.chkDisableEmployeeConfirm)
        Me.pnlLoanOtherOptions.Controls.Add(Me.chkShowLoanBalOnPayslip)
        Me.pnlLoanOtherOptions.Controls.Add(Me.chkShowonESS)
        Me.pnlLoanOtherOptions.Controls.Add(Me.chkAttachementRequired)
        Me.pnlLoanOtherOptions.Controls.Add(Me.gbDocumentTypeList)
        Me.pnlLoanOtherOptions.Controls.Add(Me.chkRequiredReportingToApproval)
        Me.pnlLoanOtherOptions.Controls.Add(Me.chkSkipApproval)
        Me.pnlLoanOtherOptions.Controls.Add(Me.chkPostingToFlexcube)
        Me.pnlLoanOtherOptions.Controls.Add(Me.chkEligibleForTopup)
        Me.pnlLoanOtherOptions.Controls.Add(Me.nudMinNoOfInstallmentPaid)
        Me.pnlLoanOtherOptions.Controls.Add(Me.lblNoOfInstallmentPaid)
        Me.pnlLoanOtherOptions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlLoanOtherOptions.Location = New System.Drawing.Point(3, 3)
        Me.pnlLoanOtherOptions.Name = "pnlLoanOtherOptions"
        Me.pnlLoanOtherOptions.Size = New System.Drawing.Size(643, 281)
        Me.pnlLoanOtherOptions.TabIndex = 0
        '
        'chkDisableEmployeeConfirm
        '
        Me.chkDisableEmployeeConfirm.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDisableEmployeeConfirm.Location = New System.Drawing.Point(14, 103)
        Me.chkDisableEmployeeConfirm.Name = "chkDisableEmployeeConfirm"
        Me.chkDisableEmployeeConfirm.Size = New System.Drawing.Size(226, 17)
        Me.chkDisableEmployeeConfirm.TabIndex = 440
        Me.chkDisableEmployeeConfirm.Text = "Disable Employee Confirmed Check Point"
        Me.chkDisableEmployeeConfirm.UseVisualStyleBackColor = True
        '
        'chkShowLoanBalOnPayslip
        '
        Me.chkShowLoanBalOnPayslip.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowLoanBalOnPayslip.Location = New System.Drawing.Point(14, 12)
        Me.chkShowLoanBalOnPayslip.Name = "chkShowLoanBalOnPayslip"
        Me.chkShowLoanBalOnPayslip.Size = New System.Drawing.Size(226, 17)
        Me.chkShowLoanBalOnPayslip.TabIndex = 405
        Me.chkShowLoanBalOnPayslip.Text = "Show Loan Balance On Payslip"
        Me.chkShowLoanBalOnPayslip.UseVisualStyleBackColor = True
        '
        'chkShowonESS
        '
        Me.chkShowonESS.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowonESS.Location = New System.Drawing.Point(14, 35)
        Me.chkShowonESS.Name = "chkShowonESS"
        Me.chkShowonESS.Size = New System.Drawing.Size(226, 17)
        Me.chkShowonESS.TabIndex = 404
        Me.chkShowonESS.Text = "Show on ESS"
        Me.chkShowonESS.UseVisualStyleBackColor = True
        '
        'chkAttachementRequired
        '
        Me.chkAttachementRequired.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAttachementRequired.Location = New System.Drawing.Point(14, 80)
        Me.chkAttachementRequired.Name = "chkAttachementRequired"
        Me.chkAttachementRequired.Size = New System.Drawing.Size(226, 17)
        Me.chkAttachementRequired.TabIndex = 430
        Me.chkAttachementRequired.Text = "Attachement Required"
        Me.chkAttachementRequired.UseVisualStyleBackColor = True
        '
        'gbDocumentTypeList
        '
        Me.gbDocumentTypeList.BorderColor = System.Drawing.Color.Black
        Me.gbDocumentTypeList.Checked = False
        Me.gbDocumentTypeList.CollapseAllExceptThis = False
        Me.gbDocumentTypeList.CollapsedHoverImage = Nothing
        Me.gbDocumentTypeList.CollapsedNormalImage = Nothing
        Me.gbDocumentTypeList.CollapsedPressedImage = Nothing
        Me.gbDocumentTypeList.CollapseOnLoad = False
        Me.gbDocumentTypeList.Controls.Add(Me.Panel2)
        Me.gbDocumentTypeList.ExpandedHoverImage = Nothing
        Me.gbDocumentTypeList.ExpandedNormalImage = Nothing
        Me.gbDocumentTypeList.ExpandedPressedImage = Nothing
        Me.gbDocumentTypeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbDocumentTypeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbDocumentTypeList.HeaderHeight = 25
        Me.gbDocumentTypeList.HeaderMessage = ""
        Me.gbDocumentTypeList.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbDocumentTypeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbDocumentTypeList.HeightOnCollapse = 0
        Me.gbDocumentTypeList.LeftTextSpace = 0
        Me.gbDocumentTypeList.Location = New System.Drawing.Point(10, 141)
        Me.gbDocumentTypeList.Name = "gbDocumentTypeList"
        Me.gbDocumentTypeList.OpenHeight = 300
        Me.gbDocumentTypeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbDocumentTypeList.ShowBorder = True
        Me.gbDocumentTypeList.ShowCheckBox = False
        Me.gbDocumentTypeList.ShowCollapseButton = False
        Me.gbDocumentTypeList.ShowDefaultBorderColor = True
        Me.gbDocumentTypeList.ShowDownButton = False
        Me.gbDocumentTypeList.ShowHeader = True
        Me.gbDocumentTypeList.Size = New System.Drawing.Size(608, 161)
        Me.gbDocumentTypeList.TabIndex = 431
        Me.gbDocumentTypeList.Temp = 0
        Me.gbDocumentTypeList.Text = "Document Type List"
        Me.gbDocumentTypeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.dgvDocumentType)
        Me.Panel2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(2, 26)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(603, 131)
        Me.Panel2.TabIndex = 64
        '
        'dgvDocumentType
        '
        Me.dgvDocumentType.AllowUserToAddRows = False
        Me.dgvDocumentType.AllowUserToDeleteRows = False
        Me.dgvDocumentType.AllowUserToResizeColumns = False
        Me.dgvDocumentType.AllowUserToResizeRows = False
        Me.dgvDocumentType.BackgroundColor = System.Drawing.Color.White
        Me.dgvDocumentType.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvDocumentType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvDocumentType.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.objdgcolhMasterunkid, Me.dgColhDocumentType})
        Me.dgvDocumentType.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvDocumentType.Location = New System.Drawing.Point(0, 0)
        Me.dgvDocumentType.Name = "dgvDocumentType"
        Me.dgvDocumentType.RowHeadersVisible = False
        Me.dgvDocumentType.Size = New System.Drawing.Size(603, 131)
        Me.dgvDocumentType.TabIndex = 63
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'objdgcolhMasterunkid
        '
        Me.objdgcolhMasterunkid.HeaderText = "Masterunkid"
        Me.objdgcolhMasterunkid.Name = "objdgcolhMasterunkid"
        Me.objdgcolhMasterunkid.Visible = False
        '
        'dgColhDocumentType
        '
        Me.dgColhDocumentType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgColhDocumentType.HeaderText = "Document Type"
        Me.dgColhDocumentType.Name = "dgColhDocumentType"
        Me.dgColhDocumentType.ReadOnly = True
        '
        'chkRequiredReportingToApproval
        '
        Me.chkRequiredReportingToApproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRequiredReportingToApproval.Location = New System.Drawing.Point(342, 35)
        Me.chkRequiredReportingToApproval.Name = "chkRequiredReportingToApproval"
        Me.chkRequiredReportingToApproval.Size = New System.Drawing.Size(227, 17)
        Me.chkRequiredReportingToApproval.TabIndex = 432
        Me.chkRequiredReportingToApproval.Text = "Required Reporting To Approval"
        Me.chkRequiredReportingToApproval.UseVisualStyleBackColor = True
        Me.chkRequiredReportingToApproval.Visible = False
        '
        'chkSkipApproval
        '
        Me.chkSkipApproval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSkipApproval.Location = New System.Drawing.Point(342, 12)
        Me.chkSkipApproval.Name = "chkSkipApproval"
        Me.chkSkipApproval.Size = New System.Drawing.Size(227, 17)
        Me.chkSkipApproval.TabIndex = 434
        Me.chkSkipApproval.Text = "Skip Approval"
        Me.chkSkipApproval.UseVisualStyleBackColor = True
        '
        'chkPostingToFlexcube
        '
        Me.chkPostingToFlexcube.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPostingToFlexcube.Location = New System.Drawing.Point(14, 58)
        Me.chkPostingToFlexcube.Name = "chkPostingToFlexcube"
        Me.chkPostingToFlexcube.Size = New System.Drawing.Size(227, 17)
        Me.chkPostingToFlexcube.TabIndex = 435
        Me.chkPostingToFlexcube.Text = "Posting To Flexcube"
        Me.chkPostingToFlexcube.UseVisualStyleBackColor = True
        '
        'chkEligibleForTopup
        '
        Me.chkEligibleForTopup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEligibleForTopup.Location = New System.Drawing.Point(342, 58)
        Me.chkEligibleForTopup.Name = "chkEligibleForTopup"
        Me.chkEligibleForTopup.Size = New System.Drawing.Size(226, 17)
        Me.chkEligibleForTopup.TabIndex = 437
        Me.chkEligibleForTopup.Text = "Eligible For Topup"
        Me.chkEligibleForTopup.UseVisualStyleBackColor = True
        '
        'nudMinNoOfInstallmentPaid
        '
        Me.nudMinNoOfInstallmentPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMinNoOfInstallmentPaid.Location = New System.Drawing.Point(561, 78)
        Me.nudMinNoOfInstallmentPaid.Name = "nudMinNoOfInstallmentPaid"
        Me.nudMinNoOfInstallmentPaid.Size = New System.Drawing.Size(59, 21)
        Me.nudMinNoOfInstallmentPaid.TabIndex = 438
        Me.nudMinNoOfInstallmentPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblNoOfInstallmentPaid
        '
        Me.lblNoOfInstallmentPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfInstallmentPaid.Location = New System.Drawing.Point(342, 80)
        Me.lblNoOfInstallmentPaid.Name = "lblNoOfInstallmentPaid"
        Me.lblNoOfInstallmentPaid.Size = New System.Drawing.Size(197, 16)
        Me.lblNoOfInstallmentPaid.TabIndex = 439
        Me.lblNoOfInstallmentPaid.Text = "Minimum No. Of Installment To be Paid"
        Me.lblNoOfInstallmentPaid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tbpLoanApprovelReminder
        '
        Me.tbpLoanApprovelReminder.BackColor = System.Drawing.Color.Transparent
        Me.tbpLoanApprovelReminder.Controls.Add(Me.pnlLoanApprovalReminderOptions)
        Me.tbpLoanApprovelReminder.Location = New System.Drawing.Point(4, 22)
        Me.tbpLoanApprovelReminder.Name = "tbpLoanApprovelReminder"
        Me.tbpLoanApprovelReminder.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpLoanApprovelReminder.Size = New System.Drawing.Size(649, 287)
        Me.tbpLoanApprovelReminder.TabIndex = 1
        Me.tbpLoanApprovelReminder.Text = "Loan Approval Notification Option(s)"
        Me.tbpLoanApprovelReminder.UseVisualStyleBackColor = True
        '
        'pnlLoanApprovalReminderOptions
        '
        Me.pnlLoanApprovalReminderOptions.AutoScroll = True
        Me.pnlLoanApprovalReminderOptions.BackColor = System.Drawing.SystemColors.Control
        Me.pnlLoanApprovalReminderOptions.Controls.Add(Me.chkFlexcubeSuccessfulNotification)
        Me.pnlLoanApprovalReminderOptions.Controls.Add(Me.gbFlexcubeSuccessfulEmployeeList)
        Me.pnlLoanApprovalReminderOptions.Controls.Add(Me.chkLoanApprovalDailyReminder)
        Me.pnlLoanApprovalReminderOptions.Controls.Add(Me.gbEmployeeList)
        Me.pnlLoanApprovalReminderOptions.Controls.Add(Me.chkEscalationDays)
        Me.pnlLoanApprovalReminderOptions.Controls.Add(Me.nudEscalationDays)
        Me.pnlLoanApprovalReminderOptions.Controls.Add(Me.LblDays)
        Me.pnlLoanApprovalReminderOptions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlLoanApprovalReminderOptions.Location = New System.Drawing.Point(3, 3)
        Me.pnlLoanApprovalReminderOptions.Name = "pnlLoanApprovalReminderOptions"
        Me.pnlLoanApprovalReminderOptions.Size = New System.Drawing.Size(643, 281)
        Me.pnlLoanApprovalReminderOptions.TabIndex = 0
        '
        'chkFlexcubeSuccessfulNotification
        '
        Me.chkFlexcubeSuccessfulNotification.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFlexcubeSuccessfulNotification.Location = New System.Drawing.Point(9, 290)
        Me.chkFlexcubeSuccessfulNotification.Name = "chkFlexcubeSuccessfulNotification"
        Me.chkFlexcubeSuccessfulNotification.Size = New System.Drawing.Size(442, 17)
        Me.chkFlexcubeSuccessfulNotification.TabIndex = 446
        Me.chkFlexcubeSuccessfulNotification.Text = "Send notification to selected employees on successfully to do disbursement in fle" & _
            "xcube"
        Me.chkFlexcubeSuccessfulNotification.UseVisualStyleBackColor = True
        '
        'gbFlexcubeSuccessfulEmployeeList
        '
        Me.gbFlexcubeSuccessfulEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbFlexcubeSuccessfulEmployeeList.Checked = False
        Me.gbFlexcubeSuccessfulEmployeeList.CollapseAllExceptThis = False
        Me.gbFlexcubeSuccessfulEmployeeList.CollapsedHoverImage = Nothing
        Me.gbFlexcubeSuccessfulEmployeeList.CollapsedNormalImage = Nothing
        Me.gbFlexcubeSuccessfulEmployeeList.CollapsedPressedImage = Nothing
        Me.gbFlexcubeSuccessfulEmployeeList.CollapseOnLoad = False
        Me.gbFlexcubeSuccessfulEmployeeList.Controls.Add(Me.txtFlexcubeSuccessfulEmployeeSearch)
        Me.gbFlexcubeSuccessfulEmployeeList.Controls.Add(Me.Panel5)
        Me.gbFlexcubeSuccessfulEmployeeList.ExpandedHoverImage = Nothing
        Me.gbFlexcubeSuccessfulEmployeeList.ExpandedNormalImage = Nothing
        Me.gbFlexcubeSuccessfulEmployeeList.ExpandedPressedImage = Nothing
        Me.gbFlexcubeSuccessfulEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFlexcubeSuccessfulEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFlexcubeSuccessfulEmployeeList.HeaderHeight = 25
        Me.gbFlexcubeSuccessfulEmployeeList.HeaderMessage = ""
        Me.gbFlexcubeSuccessfulEmployeeList.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFlexcubeSuccessfulEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFlexcubeSuccessfulEmployeeList.HeightOnCollapse = 0
        Me.gbFlexcubeSuccessfulEmployeeList.LeftTextSpace = 0
        Me.gbFlexcubeSuccessfulEmployeeList.Location = New System.Drawing.Point(8, 314)
        Me.gbFlexcubeSuccessfulEmployeeList.Name = "gbFlexcubeSuccessfulEmployeeList"
        Me.gbFlexcubeSuccessfulEmployeeList.OpenHeight = 300
        Me.gbFlexcubeSuccessfulEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFlexcubeSuccessfulEmployeeList.ShowBorder = True
        Me.gbFlexcubeSuccessfulEmployeeList.ShowCheckBox = False
        Me.gbFlexcubeSuccessfulEmployeeList.ShowCollapseButton = False
        Me.gbFlexcubeSuccessfulEmployeeList.ShowDefaultBorderColor = True
        Me.gbFlexcubeSuccessfulEmployeeList.ShowDownButton = False
        Me.gbFlexcubeSuccessfulEmployeeList.ShowHeader = True
        Me.gbFlexcubeSuccessfulEmployeeList.Size = New System.Drawing.Size(608, 243)
        Me.gbFlexcubeSuccessfulEmployeeList.TabIndex = 445
        Me.gbFlexcubeSuccessfulEmployeeList.Temp = 0
        Me.gbFlexcubeSuccessfulEmployeeList.Text = "Employee List"
        Me.gbFlexcubeSuccessfulEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFlexcubeSuccessfulEmployeeSearch
        '
        Me.txtFlexcubeSuccessfulEmployeeSearch.Flags = 0
        Me.txtFlexcubeSuccessfulEmployeeSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFlexcubeSuccessfulEmployeeSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtFlexcubeSuccessfulEmployeeSearch.Location = New System.Drawing.Point(2, 27)
        Me.txtFlexcubeSuccessfulEmployeeSearch.Name = "txtFlexcubeSuccessfulEmployeeSearch"
        Me.txtFlexcubeSuccessfulEmployeeSearch.Size = New System.Drawing.Size(604, 21)
        Me.txtFlexcubeSuccessfulEmployeeSearch.TabIndex = 393
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.chkSelectAllFinalApprovalEmployee)
        Me.Panel5.Controls.Add(Me.gvFlexcubeSuccessfulEmployeeList)
        Me.Panel5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel5.Location = New System.Drawing.Point(2, 52)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(603, 188)
        Me.Panel5.TabIndex = 64
        '
        'chkSelectAllFinalApprovalEmployee
        '
        Me.chkSelectAllFinalApprovalEmployee.AutoSize = True
        Me.chkSelectAllFinalApprovalEmployee.Location = New System.Drawing.Point(7, 6)
        Me.chkSelectAllFinalApprovalEmployee.Name = "chkSelectAllFinalApprovalEmployee"
        Me.chkSelectAllFinalApprovalEmployee.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAllFinalApprovalEmployee.TabIndex = 64
        Me.chkSelectAllFinalApprovalEmployee.UseVisualStyleBackColor = True
        '
        'gvFlexcubeSuccessfulEmployeeList
        '
        Me.gvFlexcubeSuccessfulEmployeeList.AllowUserToAddRows = False
        Me.gvFlexcubeSuccessfulEmployeeList.AllowUserToDeleteRows = False
        Me.gvFlexcubeSuccessfulEmployeeList.BackgroundColor = System.Drawing.Color.White
        Me.gvFlexcubeSuccessfulEmployeeList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.gvFlexcubeSuccessfulEmployeeList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.gvFlexcubeSuccessfulEmployeeList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhFinalApprovalEmpCheck, Me.dgcolhFinalApprovalEmployee, Me.objdgcolhFinalApprovalEmployeeId})
        Me.gvFlexcubeSuccessfulEmployeeList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gvFlexcubeSuccessfulEmployeeList.Location = New System.Drawing.Point(0, 0)
        Me.gvFlexcubeSuccessfulEmployeeList.Name = "gvFlexcubeSuccessfulEmployeeList"
        Me.gvFlexcubeSuccessfulEmployeeList.RowHeadersVisible = False
        Me.gvFlexcubeSuccessfulEmployeeList.Size = New System.Drawing.Size(603, 188)
        Me.gvFlexcubeSuccessfulEmployeeList.TabIndex = 65
        '
        'objdgcolhFinalApprovalEmpCheck
        '
        Me.objdgcolhFinalApprovalEmpCheck.HeaderText = ""
        Me.objdgcolhFinalApprovalEmpCheck.Name = "objdgcolhFinalApprovalEmpCheck"
        Me.objdgcolhFinalApprovalEmpCheck.Width = 25
        '
        'dgcolhFinalApprovalEmployee
        '
        Me.dgcolhFinalApprovalEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhFinalApprovalEmployee.HeaderText = "Employee"
        Me.dgcolhFinalApprovalEmployee.Name = "dgcolhFinalApprovalEmployee"
        Me.dgcolhFinalApprovalEmployee.ReadOnly = True
        Me.dgcolhFinalApprovalEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhFinalApprovalEmployeeId
        '
        Me.objdgcolhFinalApprovalEmployeeId.HeaderText = "objdgcolhEmployeeId"
        Me.objdgcolhFinalApprovalEmployeeId.Name = "objdgcolhFinalApprovalEmployeeId"
        Me.objdgcolhFinalApprovalEmployeeId.ReadOnly = True
        Me.objdgcolhFinalApprovalEmployeeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhFinalApprovalEmployeeId.Visible = False
        '
        'chkLoanApprovalDailyReminder
        '
        Me.chkLoanApprovalDailyReminder.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkLoanApprovalDailyReminder.Location = New System.Drawing.Point(9, 11)
        Me.chkLoanApprovalDailyReminder.Name = "chkLoanApprovalDailyReminder"
        Me.chkLoanApprovalDailyReminder.Size = New System.Drawing.Size(226, 17)
        Me.chkLoanApprovalDailyReminder.TabIndex = 440
        Me.chkLoanApprovalDailyReminder.Text = "Loan Approval Daily Reminder"
        Me.chkLoanApprovalDailyReminder.UseVisualStyleBackColor = True
        '
        'gbEmployeeList
        '
        Me.gbEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbEmployeeList.Checked = False
        Me.gbEmployeeList.CollapseAllExceptThis = False
        Me.gbEmployeeList.CollapsedHoverImage = Nothing
        Me.gbEmployeeList.CollapsedNormalImage = Nothing
        Me.gbEmployeeList.CollapsedPressedImage = Nothing
        Me.gbEmployeeList.CollapseOnLoad = False
        Me.gbEmployeeList.Controls.Add(Me.txtEmployeeSearch)
        Me.gbEmployeeList.Controls.Add(Me.Panel1)
        Me.gbEmployeeList.ExpandedHoverImage = Nothing
        Me.gbEmployeeList.ExpandedNormalImage = Nothing
        Me.gbEmployeeList.ExpandedPressedImage = Nothing
        Me.gbEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbEmployeeList.HeaderHeight = 25
        Me.gbEmployeeList.HeaderMessage = ""
        Me.gbEmployeeList.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbEmployeeList.HeightOnCollapse = 0
        Me.gbEmployeeList.LeftTextSpace = 0
        Me.gbEmployeeList.Location = New System.Drawing.Point(8, 35)
        Me.gbEmployeeList.Name = "gbEmployeeList"
        Me.gbEmployeeList.OpenHeight = 300
        Me.gbEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbEmployeeList.ShowBorder = True
        Me.gbEmployeeList.ShowCheckBox = False
        Me.gbEmployeeList.ShowCollapseButton = False
        Me.gbEmployeeList.ShowDefaultBorderColor = True
        Me.gbEmployeeList.ShowDownButton = False
        Me.gbEmployeeList.ShowHeader = True
        Me.gbEmployeeList.Size = New System.Drawing.Size(608, 243)
        Me.gbEmployeeList.TabIndex = 432
        Me.gbEmployeeList.Temp = 0
        Me.gbEmployeeList.Text = "Employee List"
        Me.gbEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployeeSearch
        '
        Me.txtEmployeeSearch.Flags = 0
        Me.txtEmployeeSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployeeSearch.Location = New System.Drawing.Point(2, 27)
        Me.txtEmployeeSearch.Name = "txtEmployeeSearch"
        Me.txtEmployeeSearch.Size = New System.Drawing.Size(604, 21)
        Me.txtEmployeeSearch.TabIndex = 393
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkSelectAllEmployee)
        Me.Panel1.Controls.Add(Me.dgEmployee)
        Me.Panel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(2, 52)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(603, 188)
        Me.Panel1.TabIndex = 64
        '
        'chkSelectAllEmployee
        '
        Me.chkSelectAllEmployee.AutoSize = True
        Me.chkSelectAllEmployee.Location = New System.Drawing.Point(7, 6)
        Me.chkSelectAllEmployee.Name = "chkSelectAllEmployee"
        Me.chkSelectAllEmployee.Size = New System.Drawing.Size(15, 14)
        Me.chkSelectAllEmployee.TabIndex = 64
        Me.chkSelectAllEmployee.UseVisualStyleBackColor = True
        '
        'dgEmployee
        '
        Me.dgEmployee.AllowUserToAddRows = False
        Me.dgEmployee.AllowUserToDeleteRows = False
        Me.dgEmployee.BackgroundColor = System.Drawing.Color.White
        Me.dgEmployee.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgEmployee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgEmployee.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhEmpCheck, Me.dgcolhEmployee, Me.objdgcolhEmployeeId})
        Me.dgEmployee.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgEmployee.Location = New System.Drawing.Point(0, 0)
        Me.dgEmployee.Name = "dgEmployee"
        Me.dgEmployee.RowHeadersVisible = False
        Me.dgEmployee.Size = New System.Drawing.Size(603, 188)
        Me.dgEmployee.TabIndex = 65
        '
        'objdgcolhEmpCheck
        '
        Me.objdgcolhEmpCheck.HeaderText = ""
        Me.objdgcolhEmpCheck.Name = "objdgcolhEmpCheck"
        Me.objdgcolhEmpCheck.Width = 25
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhEmployeeId
        '
        Me.objdgcolhEmployeeId.HeaderText = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.Name = "objdgcolhEmployeeId"
        Me.objdgcolhEmployeeId.ReadOnly = True
        Me.objdgcolhEmployeeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmployeeId.Visible = False
        '
        'chkEscalationDays
        '
        Me.chkEscalationDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEscalationDays.Location = New System.Drawing.Point(289, 11)
        Me.chkEscalationDays.Name = "chkEscalationDays"
        Me.chkEscalationDays.Size = New System.Drawing.Size(212, 17)
        Me.chkEscalationDays.TabIndex = 442
        Me.chkEscalationDays.Text = "Recurrent Reminder Occurrence after  "
        Me.chkEscalationDays.UseVisualStyleBackColor = True
        '
        'nudEscalationDays
        '
        Me.nudEscalationDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudEscalationDays.Location = New System.Drawing.Point(507, 9)
        Me.nudEscalationDays.Maximum = New Decimal(New Integer() {366, 0, 0, 0})
        Me.nudEscalationDays.Name = "nudEscalationDays"
        Me.nudEscalationDays.Size = New System.Drawing.Size(59, 21)
        Me.nudEscalationDays.TabIndex = 443
        Me.nudEscalationDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'LblDays
        '
        Me.LblDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDays.Location = New System.Drawing.Point(572, 11)
        Me.LblDays.Name = "LblDays"
        Me.LblDays.Size = New System.Drawing.Size(43, 16)
        Me.LblDays.TabIndex = 444
        Me.LblDays.Text = "Days."
        Me.LblDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tbpMortgageLoanOptions
        '
        Me.tbpMortgageLoanOptions.BackColor = System.Drawing.Color.Transparent
        Me.tbpMortgageLoanOptions.Controls.Add(Me.pnlMortgageLoanOptions)
        Me.tbpMortgageLoanOptions.Location = New System.Drawing.Point(4, 22)
        Me.tbpMortgageLoanOptions.Name = "tbpMortgageLoanOptions"
        Me.tbpMortgageLoanOptions.Size = New System.Drawing.Size(649, 287)
        Me.tbpMortgageLoanOptions.TabIndex = 2
        Me.tbpMortgageLoanOptions.Text = "Mortgage Loan Option(s)"
        Me.tbpMortgageLoanOptions.UseVisualStyleBackColor = True
        '
        'pnlMortgageLoanOptions
        '
        Me.pnlMortgageLoanOptions.AutoScroll = True
        Me.pnlMortgageLoanOptions.BackColor = System.Drawing.SystemColors.Control
        Me.pnlMortgageLoanOptions.Controls.Add(Me.gbSeniorLoanApprovalAlert)
        Me.pnlMortgageLoanOptions.Controls.Add(Me.chkBOQMandatory)
        Me.pnlMortgageLoanOptions.Controls.Add(Me.chkFSVValueMandatory)
        Me.pnlMortgageLoanOptions.Controls.Add(Me.chkMarketValueMandatory)
        Me.pnlMortgageLoanOptions.Controls.Add(Me.gbLoanTrancheDocumentList)
        Me.pnlMortgageLoanOptions.Controls.Add(Me.nudNoOfDaysBefereTitleExpiry)
        Me.pnlMortgageLoanOptions.Controls.Add(Me.gbExpiryofTitleEmployeeList)
        Me.pnlMortgageLoanOptions.Controls.Add(Me.lblNoOfDaysBefereTitleExpiry)
        Me.pnlMortgageLoanOptions.Controls.Add(Me.chkNtfForExpiryofTitle)
        Me.pnlMortgageLoanOptions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMortgageLoanOptions.Location = New System.Drawing.Point(0, 0)
        Me.pnlMortgageLoanOptions.Name = "pnlMortgageLoanOptions"
        Me.pnlMortgageLoanOptions.Size = New System.Drawing.Size(649, 287)
        Me.pnlMortgageLoanOptions.TabIndex = 0
        '
        'gbSeniorLoanApprovalAlert
        '
        Me.gbSeniorLoanApprovalAlert.BorderColor = System.Drawing.Color.Black
        Me.gbSeniorLoanApprovalAlert.Checked = False
        Me.gbSeniorLoanApprovalAlert.CollapseAllExceptThis = False
        Me.gbSeniorLoanApprovalAlert.CollapsedHoverImage = Nothing
        Me.gbSeniorLoanApprovalAlert.CollapsedNormalImage = Nothing
        Me.gbSeniorLoanApprovalAlert.CollapsedPressedImage = Nothing
        Me.gbSeniorLoanApprovalAlert.CollapseOnLoad = False
        Me.gbSeniorLoanApprovalAlert.Controls.Add(Me.cboSeniorApproverLevel)
        Me.gbSeniorLoanApprovalAlert.Controls.Add(Me.pnlLoanApproverLevel)
        Me.gbSeniorLoanApprovalAlert.Controls.Add(Me.Label1)
        Me.gbSeniorLoanApprovalAlert.ExpandedHoverImage = Nothing
        Me.gbSeniorLoanApprovalAlert.ExpandedNormalImage = Nothing
        Me.gbSeniorLoanApprovalAlert.ExpandedPressedImage = Nothing
        Me.gbSeniorLoanApprovalAlert.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbSeniorLoanApprovalAlert.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbSeniorLoanApprovalAlert.HeaderHeight = 25
        Me.gbSeniorLoanApprovalAlert.HeaderMessage = ""
        Me.gbSeniorLoanApprovalAlert.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbSeniorLoanApprovalAlert.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbSeniorLoanApprovalAlert.HeightOnCollapse = 0
        Me.gbSeniorLoanApprovalAlert.LeftTextSpace = 0
        Me.gbSeniorLoanApprovalAlert.Location = New System.Drawing.Point(10, 262)
        Me.gbSeniorLoanApprovalAlert.Name = "gbSeniorLoanApprovalAlert"
        Me.gbSeniorLoanApprovalAlert.OpenHeight = 300
        Me.gbSeniorLoanApprovalAlert.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbSeniorLoanApprovalAlert.ShowBorder = True
        Me.gbSeniorLoanApprovalAlert.ShowCheckBox = False
        Me.gbSeniorLoanApprovalAlert.ShowCollapseButton = False
        Me.gbSeniorLoanApprovalAlert.ShowDefaultBorderColor = True
        Me.gbSeniorLoanApprovalAlert.ShowDownButton = False
        Me.gbSeniorLoanApprovalAlert.ShowHeader = True
        Me.gbSeniorLoanApprovalAlert.Size = New System.Drawing.Size(611, 181)
        Me.gbSeniorLoanApprovalAlert.TabIndex = 469
        Me.gbSeniorLoanApprovalAlert.Temp = 0
        Me.gbSeniorLoanApprovalAlert.Text = "Senior Loan Approval Alert"
        Me.gbSeniorLoanApprovalAlert.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboSeniorApproverLevel
        '
        Me.cboSeniorApproverLevel.DropDownWidth = 250
        Me.cboSeniorApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSeniorApproverLevel.FormattingEnabled = True
        Me.cboSeniorApproverLevel.Location = New System.Drawing.Point(141, 31)
        Me.cboSeniorApproverLevel.Name = "cboSeniorApproverLevel"
        Me.cboSeniorApproverLevel.Size = New System.Drawing.Size(147, 21)
        Me.cboSeniorApproverLevel.TabIndex = 468
        '
        'pnlLoanApproverLevel
        '
        Me.pnlLoanApproverLevel.Controls.Add(Me.dgSeniorLoanApprovalAlert)
        Me.pnlLoanApproverLevel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlLoanApproverLevel.Location = New System.Drawing.Point(294, 27)
        Me.pnlLoanApproverLevel.Name = "pnlLoanApproverLevel"
        Me.pnlLoanApproverLevel.Size = New System.Drawing.Size(314, 151)
        Me.pnlLoanApproverLevel.TabIndex = 64
        '
        'dgSeniorLoanApprovalAlert
        '
        Me.dgSeniorLoanApprovalAlert.AllowUserToAddRows = False
        Me.dgSeniorLoanApprovalAlert.AllowUserToDeleteRows = False
        Me.dgSeniorLoanApprovalAlert.BackgroundColor = System.Drawing.Color.White
        Me.dgSeniorLoanApprovalAlert.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgSeniorLoanApprovalAlert.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgSeniorLoanApprovalAlert.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhLevelCheck, Me.dgcolhApproverLevel, Me.dgcolhPriority, Me.objdgcolhLevelId})
        Me.dgSeniorLoanApprovalAlert.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgSeniorLoanApprovalAlert.Location = New System.Drawing.Point(0, 0)
        Me.dgSeniorLoanApprovalAlert.Name = "dgSeniorLoanApprovalAlert"
        Me.dgSeniorLoanApprovalAlert.RowHeadersVisible = False
        Me.dgSeniorLoanApprovalAlert.Size = New System.Drawing.Size(314, 151)
        Me.dgSeniorLoanApprovalAlert.TabIndex = 65
        '
        'objdgcolhLevelCheck
        '
        Me.objdgcolhLevelCheck.HeaderText = ""
        Me.objdgcolhLevelCheck.Name = "objdgcolhLevelCheck"
        Me.objdgcolhLevelCheck.Width = 25
        '
        'dgcolhApproverLevel
        '
        Me.dgcolhApproverLevel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhApproverLevel.HeaderText = "Approver Level"
        Me.dgcolhApproverLevel.Name = "dgcolhApproverLevel"
        Me.dgcolhApproverLevel.ReadOnly = True
        Me.dgcolhApproverLevel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhPriority
        '
        Me.dgcolhPriority.HeaderText = "Priority"
        Me.dgcolhPriority.Name = "dgcolhPriority"
        Me.dgcolhPriority.ReadOnly = True
        Me.dgcolhPriority.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhPriority.Width = 70
        '
        'objdgcolhLevelId
        '
        Me.objdgcolhLevelId.HeaderText = "objdgcolhLevelId"
        Me.objdgcolhLevelId.Name = "objdgcolhLevelId"
        Me.objdgcolhLevelId.ReadOnly = True
        Me.objdgcolhLevelId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhLevelId.Visible = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(129, 16)
        Me.Label1.TabIndex = 467
        Me.Label1.Text = "Senior Approver Level"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkBOQMandatory
        '
        Me.chkBOQMandatory.Checked = True
        Me.chkBOQMandatory.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBOQMandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkBOQMandatory.Location = New System.Drawing.Point(11, 35)
        Me.chkBOQMandatory.Name = "chkBOQMandatory"
        Me.chkBOQMandatory.Size = New System.Drawing.Size(327, 17)
        Me.chkBOQMandatory.TabIndex = 467
        Me.chkBOQMandatory.Text = "Make BOQ Mandatory"
        Me.chkBOQMandatory.UseVisualStyleBackColor = True
        '
        'chkFSVValueMandatory
        '
        Me.chkFSVValueMandatory.Checked = True
        Me.chkFSVValueMandatory.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFSVValueMandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFSVValueMandatory.Location = New System.Drawing.Point(357, 13)
        Me.chkFSVValueMandatory.Name = "chkFSVValueMandatory"
        Me.chkFSVValueMandatory.Size = New System.Drawing.Size(264, 17)
        Me.chkFSVValueMandatory.TabIndex = 466
        Me.chkFSVValueMandatory.Text = "Make FSV Value Mandatory"
        Me.chkFSVValueMandatory.UseVisualStyleBackColor = True
        '
        'chkMarketValueMandatory
        '
        Me.chkMarketValueMandatory.Checked = True
        Me.chkMarketValueMandatory.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMarketValueMandatory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMarketValueMandatory.Location = New System.Drawing.Point(11, 13)
        Me.chkMarketValueMandatory.Name = "chkMarketValueMandatory"
        Me.chkMarketValueMandatory.Size = New System.Drawing.Size(327, 17)
        Me.chkMarketValueMandatory.TabIndex = 465
        Me.chkMarketValueMandatory.Text = "Make Market Value Mandatory"
        Me.chkMarketValueMandatory.UseVisualStyleBackColor = True
        '
        'gbLoanTrancheDocumentList
        '
        Me.gbLoanTrancheDocumentList.BorderColor = System.Drawing.Color.Black
        Me.gbLoanTrancheDocumentList.Checked = False
        Me.gbLoanTrancheDocumentList.CollapseAllExceptThis = False
        Me.gbLoanTrancheDocumentList.CollapsedHoverImage = Nothing
        Me.gbLoanTrancheDocumentList.CollapsedNormalImage = Nothing
        Me.gbLoanTrancheDocumentList.CollapsedPressedImage = Nothing
        Me.gbLoanTrancheDocumentList.CollapseOnLoad = False
        Me.gbLoanTrancheDocumentList.Controls.Add(Me.Panel4)
        Me.gbLoanTrancheDocumentList.ExpandedHoverImage = Nothing
        Me.gbLoanTrancheDocumentList.ExpandedNormalImage = Nothing
        Me.gbLoanTrancheDocumentList.ExpandedPressedImage = Nothing
        Me.gbLoanTrancheDocumentList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbLoanTrancheDocumentList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbLoanTrancheDocumentList.HeaderHeight = 25
        Me.gbLoanTrancheDocumentList.HeaderMessage = ""
        Me.gbLoanTrancheDocumentList.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbLoanTrancheDocumentList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbLoanTrancheDocumentList.HeightOnCollapse = 0
        Me.gbLoanTrancheDocumentList.LeftTextSpace = 0
        Me.gbLoanTrancheDocumentList.Location = New System.Drawing.Point(10, 452)
        Me.gbLoanTrancheDocumentList.Name = "gbLoanTrancheDocumentList"
        Me.gbLoanTrancheDocumentList.OpenHeight = 300
        Me.gbLoanTrancheDocumentList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbLoanTrancheDocumentList.ShowBorder = True
        Me.gbLoanTrancheDocumentList.ShowCheckBox = False
        Me.gbLoanTrancheDocumentList.ShowCollapseButton = False
        Me.gbLoanTrancheDocumentList.ShowDefaultBorderColor = True
        Me.gbLoanTrancheDocumentList.ShowDownButton = False
        Me.gbLoanTrancheDocumentList.ShowHeader = True
        Me.gbLoanTrancheDocumentList.Size = New System.Drawing.Size(612, 161)
        Me.gbLoanTrancheDocumentList.TabIndex = 464
        Me.gbLoanTrancheDocumentList.Temp = 0
        Me.gbLoanTrancheDocumentList.Text = "Loan Tranche Document Type List"
        Me.gbLoanTrancheDocumentList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.dgLoanTrancheDocumentList)
        Me.Panel4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel4.Location = New System.Drawing.Point(2, 26)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(606, 131)
        Me.Panel4.TabIndex = 64
        '
        'dgLoanTrancheDocumentList
        '
        Me.dgLoanTrancheDocumentList.AllowUserToAddRows = False
        Me.dgLoanTrancheDocumentList.AllowUserToDeleteRows = False
        Me.dgLoanTrancheDocumentList.AllowUserToResizeColumns = False
        Me.dgLoanTrancheDocumentList.AllowUserToResizeRows = False
        Me.dgLoanTrancheDocumentList.BackgroundColor = System.Drawing.Color.White
        Me.dgLoanTrancheDocumentList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgLoanTrancheDocumentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgLoanTrancheDocumentList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhLoanTranCheck, Me.objdgcolhLoanTrancheMasterunkid, Me.dgcolhLoanTranchedocumentType})
        Me.dgLoanTrancheDocumentList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgLoanTrancheDocumentList.Location = New System.Drawing.Point(0, 0)
        Me.dgLoanTrancheDocumentList.Name = "dgLoanTrancheDocumentList"
        Me.dgLoanTrancheDocumentList.RowHeadersVisible = False
        Me.dgLoanTrancheDocumentList.Size = New System.Drawing.Size(606, 131)
        Me.dgLoanTrancheDocumentList.TabIndex = 63
        '
        'objdgcolhLoanTranCheck
        '
        Me.objdgcolhLoanTranCheck.HeaderText = ""
        Me.objdgcolhLoanTranCheck.Name = "objdgcolhLoanTranCheck"
        Me.objdgcolhLoanTranCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhLoanTranCheck.Width = 25
        '
        'objdgcolhLoanTrancheMasterunkid
        '
        Me.objdgcolhLoanTrancheMasterunkid.HeaderText = "Masterunkid"
        Me.objdgcolhLoanTrancheMasterunkid.Name = "objdgcolhLoanTrancheMasterunkid"
        Me.objdgcolhLoanTrancheMasterunkid.Visible = False
        Me.objdgcolhLoanTrancheMasterunkid.Width = 575
        '
        'dgcolhLoanTranchedocumentType
        '
        Me.dgcolhLoanTranchedocumentType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhLoanTranchedocumentType.HeaderText = "Document Type"
        Me.dgcolhLoanTranchedocumentType.Name = "dgcolhLoanTranchedocumentType"
        Me.dgcolhLoanTranchedocumentType.ReadOnly = True
        '
        'nudNoOfDaysBefereTitleExpiry
        '
        Me.nudNoOfDaysBefereTitleExpiry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudNoOfDaysBefereTitleExpiry.Location = New System.Drawing.Point(279, 56)
        Me.nudNoOfDaysBefereTitleExpiry.Maximum = New Decimal(New Integer() {99999999, 0, 0, 0})
        Me.nudNoOfDaysBefereTitleExpiry.Name = "nudNoOfDaysBefereTitleExpiry"
        Me.nudNoOfDaysBefereTitleExpiry.Size = New System.Drawing.Size(59, 21)
        Me.nudNoOfDaysBefereTitleExpiry.TabIndex = 456
        Me.nudNoOfDaysBefereTitleExpiry.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'gbExpiryofTitleEmployeeList
        '
        Me.gbExpiryofTitleEmployeeList.BorderColor = System.Drawing.Color.Black
        Me.gbExpiryofTitleEmployeeList.Checked = False
        Me.gbExpiryofTitleEmployeeList.CollapseAllExceptThis = False
        Me.gbExpiryofTitleEmployeeList.CollapsedHoverImage = Nothing
        Me.gbExpiryofTitleEmployeeList.CollapsedNormalImage = Nothing
        Me.gbExpiryofTitleEmployeeList.CollapsedPressedImage = Nothing
        Me.gbExpiryofTitleEmployeeList.CollapseOnLoad = False
        Me.gbExpiryofTitleEmployeeList.Controls.Add(Me.txtExpiryofTitleEmpSearch)
        Me.gbExpiryofTitleEmployeeList.Controls.Add(Me.Panel3)
        Me.gbExpiryofTitleEmployeeList.ExpandedHoverImage = Nothing
        Me.gbExpiryofTitleEmployeeList.ExpandedNormalImage = Nothing
        Me.gbExpiryofTitleEmployeeList.ExpandedPressedImage = Nothing
        Me.gbExpiryofTitleEmployeeList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbExpiryofTitleEmployeeList.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbExpiryofTitleEmployeeList.HeaderHeight = 25
        Me.gbExpiryofTitleEmployeeList.HeaderMessage = ""
        Me.gbExpiryofTitleEmployeeList.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbExpiryofTitleEmployeeList.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbExpiryofTitleEmployeeList.HeightOnCollapse = 0
        Me.gbExpiryofTitleEmployeeList.LeftTextSpace = 0
        Me.gbExpiryofTitleEmployeeList.Location = New System.Drawing.Point(10, 86)
        Me.gbExpiryofTitleEmployeeList.Name = "gbExpiryofTitleEmployeeList"
        Me.gbExpiryofTitleEmployeeList.OpenHeight = 300
        Me.gbExpiryofTitleEmployeeList.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbExpiryofTitleEmployeeList.ShowBorder = True
        Me.gbExpiryofTitleEmployeeList.ShowCheckBox = False
        Me.gbExpiryofTitleEmployeeList.ShowCollapseButton = False
        Me.gbExpiryofTitleEmployeeList.ShowDefaultBorderColor = True
        Me.gbExpiryofTitleEmployeeList.ShowDownButton = False
        Me.gbExpiryofTitleEmployeeList.ShowHeader = True
        Me.gbExpiryofTitleEmployeeList.Size = New System.Drawing.Size(611, 170)
        Me.gbExpiryofTitleEmployeeList.TabIndex = 463
        Me.gbExpiryofTitleEmployeeList.Temp = 0
        Me.gbExpiryofTitleEmployeeList.Text = "Employee List"
        Me.gbExpiryofTitleEmployeeList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExpiryofTitleEmpSearch
        '
        Me.txtExpiryofTitleEmpSearch.Flags = 0
        Me.txtExpiryofTitleEmpSearch.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExpiryofTitleEmpSearch.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtExpiryofTitleEmpSearch.Location = New System.Drawing.Point(2, 27)
        Me.txtExpiryofTitleEmpSearch.Name = "txtExpiryofTitleEmpSearch"
        Me.txtExpiryofTitleEmpSearch.Size = New System.Drawing.Size(606, 21)
        Me.txtExpiryofTitleEmpSearch.TabIndex = 393
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.chkAllExpiryofTitleEmp)
        Me.Panel3.Controls.Add(Me.dgExpiryofTitleEmp)
        Me.Panel3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel3.Location = New System.Drawing.Point(2, 52)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(605, 114)
        Me.Panel3.TabIndex = 64
        '
        'chkAllExpiryofTitleEmp
        '
        Me.chkAllExpiryofTitleEmp.AutoSize = True
        Me.chkAllExpiryofTitleEmp.Location = New System.Drawing.Point(7, 6)
        Me.chkAllExpiryofTitleEmp.Name = "chkAllExpiryofTitleEmp"
        Me.chkAllExpiryofTitleEmp.Size = New System.Drawing.Size(15, 14)
        Me.chkAllExpiryofTitleEmp.TabIndex = 64
        Me.chkAllExpiryofTitleEmp.UseVisualStyleBackColor = True
        '
        'dgExpiryofTitleEmp
        '
        Me.dgExpiryofTitleEmp.AllowUserToAddRows = False
        Me.dgExpiryofTitleEmp.AllowUserToDeleteRows = False
        Me.dgExpiryofTitleEmp.BackgroundColor = System.Drawing.Color.White
        Me.dgExpiryofTitleEmp.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgExpiryofTitleEmp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgExpiryofTitleEmp.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhExpiryTitleEmpCheck, Me.dgcolhExpiryTitleEmployee, Me.objdgcolhExpiryTitleEmployeeId})
        Me.dgExpiryofTitleEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgExpiryofTitleEmp.Location = New System.Drawing.Point(0, 0)
        Me.dgExpiryofTitleEmp.Name = "dgExpiryofTitleEmp"
        Me.dgExpiryofTitleEmp.RowHeadersVisible = False
        Me.dgExpiryofTitleEmp.Size = New System.Drawing.Size(605, 114)
        Me.dgExpiryofTitleEmp.TabIndex = 65
        '
        'objdgcolhExpiryTitleEmpCheck
        '
        Me.objdgcolhExpiryTitleEmpCheck.HeaderText = ""
        Me.objdgcolhExpiryTitleEmpCheck.Name = "objdgcolhExpiryTitleEmpCheck"
        Me.objdgcolhExpiryTitleEmpCheck.Width = 25
        '
        'dgcolhExpiryTitleEmployee
        '
        Me.dgcolhExpiryTitleEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhExpiryTitleEmployee.HeaderText = "Employee"
        Me.dgcolhExpiryTitleEmployee.Name = "dgcolhExpiryTitleEmployee"
        Me.dgcolhExpiryTitleEmployee.ReadOnly = True
        Me.dgcolhExpiryTitleEmployee.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhExpiryTitleEmployeeId
        '
        Me.objdgcolhExpiryTitleEmployeeId.HeaderText = "objdgcolhExpiryTitleEmployeeId"
        Me.objdgcolhExpiryTitleEmployeeId.Name = "objdgcolhExpiryTitleEmployeeId"
        Me.objdgcolhExpiryTitleEmployeeId.ReadOnly = True
        Me.objdgcolhExpiryTitleEmployeeId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhExpiryTitleEmployeeId.Visible = False
        '
        'lblNoOfDaysBefereTitleExpiry
        '
        Me.lblNoOfDaysBefereTitleExpiry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoOfDaysBefereTitleExpiry.Location = New System.Drawing.Point(344, 58)
        Me.lblNoOfDaysBefereTitleExpiry.Name = "lblNoOfDaysBefereTitleExpiry"
        Me.lblNoOfDaysBefereTitleExpiry.Size = New System.Drawing.Size(55, 17)
        Me.lblNoOfDaysBefereTitleExpiry.TabIndex = 457
        Me.lblNoOfDaysBefereTitleExpiry.Text = "Day(s)."
        Me.lblNoOfDaysBefereTitleExpiry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkNtfForExpiryofTitle
        '
        Me.chkNtfForExpiryofTitle.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNtfForExpiryofTitle.Location = New System.Drawing.Point(11, 58)
        Me.chkNtfForExpiryofTitle.Name = "chkNtfForExpiryofTitle"
        Me.chkNtfForExpiryofTitle.Size = New System.Drawing.Size(264, 17)
        Me.chkNtfForExpiryofTitle.TabIndex = 462
        Me.chkNtfForExpiryofTitle.Text = "Send Notification For Expiry of Title Before"
        Me.chkNtfForExpiryofTitle.UseVisualStyleBackColor = True
        '
        'objbtnKeywordsMaxInstallmentMortgage
        '
        Me.objbtnKeywordsMaxInstallmentMortgage.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsMaxInstallmentMortgage.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsMaxInstallmentMortgage.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsMaxInstallmentMortgage.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsMaxInstallmentMortgage.BorderSelected = False
        Me.objbtnKeywordsMaxInstallmentMortgage.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsMaxInstallmentMortgage.Image = Global.Aruti.Main.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsMaxInstallmentMortgage.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsMaxInstallmentMortgage.Location = New System.Drawing.Point(436, 515)
        Me.objbtnKeywordsMaxInstallmentMortgage.Name = "objbtnKeywordsMaxInstallmentMortgage"
        Me.objbtnKeywordsMaxInstallmentMortgage.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsMaxInstallmentMortgage.TabIndex = 460
        '
        'txtMaxInstallmentAmtForMortgage
        '
        Me.txtMaxInstallmentAmtForMortgage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxInstallmentAmtForMortgage.Location = New System.Drawing.Point(163, 516)
        Me.txtMaxInstallmentAmtForMortgage.Name = "txtMaxInstallmentAmtForMortgage"
        Me.txtMaxInstallmentAmtForMortgage.Size = New System.Drawing.Size(267, 21)
        Me.txtMaxInstallmentAmtForMortgage.TabIndex = 459
        '
        'lblMaxInstallmentAmtForMortgage
        '
        Me.lblMaxInstallmentAmtForMortgage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxInstallmentAmtForMortgage.Location = New System.Drawing.Point(13, 512)
        Me.lblMaxInstallmentAmtForMortgage.Name = "lblMaxInstallmentAmtForMortgage"
        Me.lblMaxInstallmentAmtForMortgage.Size = New System.Drawing.Size(148, 27)
        Me.lblMaxInstallmentAmtForMortgage.TabIndex = 458
        Me.lblMaxInstallmentAmtForMortgage.Text = "Max. Installment Amount Formula For Mortgage Loan"
        Me.lblMaxInstallmentAmtForMortgage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMinNoOfInstallment
        '
        Me.txtMinNoOfInstallment.AllowNegative = False
        Me.txtMinNoOfInstallment.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMinNoOfInstallment.DigitsInGroup = 0
        Me.txtMinNoOfInstallment.Flags = 65536
        Me.txtMinNoOfInstallment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMinNoOfInstallment.Location = New System.Drawing.Point(508, 337)
        Me.txtMinNoOfInstallment.MaxDecimalPlaces = 0
        Me.txtMinNoOfInstallment.MaxWholeDigits = 21
        Me.txtMinNoOfInstallment.Name = "txtMinNoOfInstallment"
        Me.txtMinNoOfInstallment.Prefix = ""
        Me.txtMinNoOfInstallment.RangeMax = 1.7976931348623157E+308
        Me.txtMinNoOfInstallment.RangeMin = -1.7976931348623157E+308
        Me.txtMinNoOfInstallment.Size = New System.Drawing.Size(98, 21)
        Me.txtMinNoOfInstallment.TabIndex = 447
        Me.txtMinNoOfInstallment.Text = "0"
        Me.txtMinNoOfInstallment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMinNoOfInstallment
        '
        Me.lblMinNoOfInstallment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinNoOfInstallment.Location = New System.Drawing.Point(351, 339)
        Me.lblMinNoOfInstallment.Name = "lblMinNoOfInstallment"
        Me.lblMinNoOfInstallment.Size = New System.Drawing.Size(151, 16)
        Me.lblMinNoOfInstallment.TabIndex = 446
        Me.lblMinNoOfInstallment.Text = "Minimum No. Of Installment"
        Me.lblMinNoOfInstallment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMinimumLoanAmount
        '
        Me.lblMinimumLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinimumLoanAmount.Location = New System.Drawing.Point(340, 90)
        Me.lblMinimumLoanAmount.Name = "lblMinimumLoanAmount"
        Me.lblMinimumLoanAmount.Size = New System.Drawing.Size(118, 16)
        Me.lblMinimumLoanAmount.TabIndex = 433
        Me.lblMinimumLoanAmount.Text = "Minimum Loan Amount"
        Me.lblMinimumLoanAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMinLoanAmount
        '
        Me.txtMinLoanAmount.AllowNegative = True
        Me.txtMinLoanAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMinLoanAmount.DigitsInGroup = 0
        Me.txtMinLoanAmount.Flags = 0
        Me.txtMinLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMinLoanAmount.Location = New System.Drawing.Point(459, 90)
        Me.txtMinLoanAmount.MaxDecimalPlaces = 6
        Me.txtMinLoanAmount.MaxWholeDigits = 21
        Me.txtMinLoanAmount.Name = "txtMinLoanAmount"
        Me.txtMinLoanAmount.Prefix = ""
        Me.txtMinLoanAmount.RangeMax = 1.7976931348623157E+308
        Me.txtMinLoanAmount.RangeMin = -1.7976931348623157E+308
        Me.txtMinLoanAmount.Size = New System.Drawing.Size(159, 21)
        Me.txtMinLoanAmount.TabIndex = 432
        Me.txtMinLoanAmount.Text = "0"
        Me.txtMinLoanAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'pnlMaxLoanAmount
        '
        Me.pnlMaxLoanAmount.Controls.Add(Me.lblMaximumLoanAmount)
        Me.pnlMaxLoanAmount.Controls.Add(Me.txtMaxLoanAmount)
        Me.pnlMaxLoanAmount.Location = New System.Drawing.Point(8, 86)
        Me.pnlMaxLoanAmount.Name = "pnlMaxLoanAmount"
        Me.pnlMaxLoanAmount.Size = New System.Drawing.Size(327, 28)
        Me.pnlMaxLoanAmount.TabIndex = 428
        '
        'lblMaximumLoanAmount
        '
        Me.lblMaximumLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaximumLoanAmount.Location = New System.Drawing.Point(6, 6)
        Me.lblMaximumLoanAmount.Name = "lblMaximumLoanAmount"
        Me.lblMaximumLoanAmount.Size = New System.Drawing.Size(148, 16)
        Me.lblMaximumLoanAmount.TabIndex = 27
        Me.lblMaximumLoanAmount.Text = "Maximum Loan Amount"
        Me.lblMaximumLoanAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMaxLoanAmount
        '
        Me.txtMaxLoanAmount.AllowNegative = True
        Me.txtMaxLoanAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMaxLoanAmount.DigitsInGroup = 0
        Me.txtMaxLoanAmount.Flags = 0
        Me.txtMaxLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxLoanAmount.Location = New System.Drawing.Point(155, 4)
        Me.txtMaxLoanAmount.MaxDecimalPlaces = 6
        Me.txtMaxLoanAmount.MaxWholeDigits = 21
        Me.txtMaxLoanAmount.Name = "txtMaxLoanAmount"
        Me.txtMaxLoanAmount.Prefix = ""
        Me.txtMaxLoanAmount.RangeMax = 1.7976931348623157E+308
        Me.txtMaxLoanAmount.RangeMin = -1.7976931348623157E+308
        Me.txtMaxLoanAmount.Size = New System.Drawing.Size(171, 21)
        Me.txtMaxLoanAmount.TabIndex = 26
        Me.txtMaxLoanAmount.Text = "0"
        Me.txtMaxLoanAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'radFormula
        '
        Me.radFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radFormula.Location = New System.Drawing.Point(240, 66)
        Me.radFormula.Name = "radFormula"
        Me.radFormula.Size = New System.Drawing.Size(86, 17)
        Me.radFormula.TabIndex = 427
        Me.radFormula.Text = "Formula"
        Me.radFormula.UseVisualStyleBackColor = True
        '
        'pnlMaxLoanFormula
        '
        Me.pnlMaxLoanFormula.Controls.Add(Me.txtMaxLoanFormula)
        Me.pnlMaxLoanFormula.Controls.Add(Me.lblMaxLoanFormula)
        Me.pnlMaxLoanFormula.Location = New System.Drawing.Point(9, 86)
        Me.pnlMaxLoanFormula.Name = "pnlMaxLoanFormula"
        Me.pnlMaxLoanFormula.Size = New System.Drawing.Size(324, 28)
        Me.pnlMaxLoanFormula.TabIndex = 429
        '
        'txtMaxLoanFormula
        '
        Me.txtMaxLoanFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxLoanFormula.Location = New System.Drawing.Point(155, 4)
        Me.txtMaxLoanFormula.Name = "txtMaxLoanFormula"
        Me.txtMaxLoanFormula.Size = New System.Drawing.Size(157, 21)
        Me.txtMaxLoanFormula.TabIndex = 370
        '
        'lblMaxLoanFormula
        '
        Me.lblMaxLoanFormula.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxLoanFormula.Location = New System.Drawing.Point(2, 6)
        Me.lblMaxLoanFormula.Name = "lblMaxLoanFormula"
        Me.lblMaxLoanFormula.Size = New System.Drawing.Size(121, 16)
        Me.lblMaxLoanFormula.TabIndex = 27
        Me.lblMaxLoanFormula.Text = "Maximum Loan Formula"
        Me.lblMaxLoanFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radFlatRate
        '
        Me.radFlatRate.Checked = True
        Me.radFlatRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radFlatRate.Location = New System.Drawing.Point(165, 66)
        Me.radFlatRate.Name = "radFlatRate"
        Me.radFlatRate.Size = New System.Drawing.Size(69, 17)
        Me.radFlatRate.TabIndex = 426
        Me.radFlatRate.TabStop = True
        Me.radFlatRate.Text = "Flat Rate"
        Me.radFlatRate.UseVisualStyleBackColor = True
        '
        'lblMaxLoanCalcType
        '
        Me.lblMaxLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxLoanCalcType.Location = New System.Drawing.Point(14, 66)
        Me.lblMaxLoanCalcType.Name = "lblMaxLoanCalcType"
        Me.lblMaxLoanCalcType.Size = New System.Drawing.Size(148, 16)
        Me.lblMaxLoanCalcType.TabIndex = 407
        Me.lblMaxLoanCalcType.Text = "Max Loan Calc. Type"
        Me.lblMaxLoanCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInsuranceRate
        '
        Me.txtInsuranceRate.AllowNegative = False
        Me.txtInsuranceRate.BackColor = System.Drawing.SystemColors.Window
        Me.txtInsuranceRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInsuranceRate.DigitsInGroup = 0
        Me.txtInsuranceRate.Flags = 65536
        Me.txtInsuranceRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInsuranceRate.Location = New System.Drawing.Point(164, 290)
        Me.txtInsuranceRate.MaxDecimalPlaces = 6
        Me.txtInsuranceRate.MaxWholeDigits = 21
        Me.txtInsuranceRate.Name = "txtInsuranceRate"
        Me.txtInsuranceRate.Prefix = ""
        Me.txtInsuranceRate.RangeMax = 1.7976931348623157E+308
        Me.txtInsuranceRate.RangeMin = -1.7976931348623157E+308
        Me.txtInsuranceRate.Size = New System.Drawing.Size(170, 21)
        Me.txtInsuranceRate.TabIndex = 425
        Me.txtInsuranceRate.Text = "0"
        Me.txtInsuranceRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblInsuranceRate
        '
        Me.lblInsuranceRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInsuranceRate.Location = New System.Drawing.Point(13, 293)
        Me.lblInsuranceRate.Name = "lblInsuranceRate"
        Me.lblInsuranceRate.Size = New System.Drawing.Size(137, 16)
        Me.lblInsuranceRate.TabIndex = 424
        Me.lblInsuranceRate.Text = "Insurance Rate (%)"
        Me.lblInsuranceRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRepaymentDays
        '
        Me.txtRepaymentDays.AllowNegative = True
        Me.txtRepaymentDays.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtRepaymentDays.DigitsInGroup = 0
        Me.txtRepaymentDays.Flags = 0
        Me.txtRepaymentDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRepaymentDays.Location = New System.Drawing.Point(508, 394)
        Me.txtRepaymentDays.MaxDecimalPlaces = 0
        Me.txtRepaymentDays.MaxWholeDigits = 21
        Me.txtRepaymentDays.Name = "txtRepaymentDays"
        Me.txtRepaymentDays.Prefix = ""
        Me.txtRepaymentDays.RangeMax = 1.7976931348623157E+308
        Me.txtRepaymentDays.RangeMin = -1.7976931348623157E+308
        Me.txtRepaymentDays.Size = New System.Drawing.Size(98, 21)
        Me.txtRepaymentDays.TabIndex = 422
        Me.txtRepaymentDays.Text = "0"
        Me.txtRepaymentDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblRepaymentDays
        '
        Me.lblRepaymentDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRepaymentDays.Location = New System.Drawing.Point(352, 396)
        Me.lblRepaymentDays.Name = "lblRepaymentDays"
        Me.lblRepaymentDays.Size = New System.Drawing.Size(151, 16)
        Me.lblRepaymentDays.TabIndex = 423
        Me.lblRepaymentDays.Text = "Repayment Days"
        Me.lblRepaymentDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanSchemeCategory
        '
        Me.cboLoanSchemeCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanSchemeCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanSchemeCategory.FormattingEnabled = True
        Me.cboLoanSchemeCategory.Location = New System.Drawing.Point(458, 64)
        Me.cboLoanSchemeCategory.Name = "cboLoanSchemeCategory"
        Me.cboLoanSchemeCategory.Size = New System.Drawing.Size(160, 21)
        Me.cboLoanSchemeCategory.TabIndex = 421
        '
        'lblLoanSchemeCategory
        '
        Me.lblLoanSchemeCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanSchemeCategory.Location = New System.Drawing.Point(340, 66)
        Me.lblLoanSchemeCategory.Name = "lblLoanSchemeCategory"
        Me.lblLoanSchemeCategory.Size = New System.Drawing.Size(118, 16)
        Me.lblLoanSchemeCategory.TabIndex = 420
        Me.lblLoanSchemeCategory.Text = "Loan Scheme Category"
        Me.lblLoanSchemeCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnKeywordsMaxInstallment
        '
        Me.objbtnKeywordsMaxInstallment.BackColor = System.Drawing.Color.Transparent
        Me.objbtnKeywordsMaxInstallment.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsMaxInstallment.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnKeywordsMaxInstallment.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnKeywordsMaxInstallment.BorderSelected = False
        Me.objbtnKeywordsMaxInstallment.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnKeywordsMaxInstallment.Image = Global.Aruti.Main.My.Resources.Resources.Info_icons
        Me.objbtnKeywordsMaxInstallment.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnKeywordsMaxInstallment.Location = New System.Drawing.Point(436, 483)
        Me.objbtnKeywordsMaxInstallment.Name = "objbtnKeywordsMaxInstallment"
        Me.objbtnKeywordsMaxInstallment.Size = New System.Drawing.Size(21, 21)
        Me.objbtnKeywordsMaxInstallment.TabIndex = 419
        '
        'txtMaxInstallmentAmtCalculation
        '
        Me.txtMaxInstallmentAmtCalculation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxInstallmentAmtCalculation.Location = New System.Drawing.Point(163, 484)
        Me.txtMaxInstallmentAmtCalculation.Name = "txtMaxInstallmentAmtCalculation"
        Me.txtMaxInstallmentAmtCalculation.Size = New System.Drawing.Size(267, 21)
        Me.txtMaxInstallmentAmtCalculation.TabIndex = 418
        '
        'lblMaxInstallmentAmtCalculation
        '
        Me.lblMaxInstallmentAmtCalculation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxInstallmentAmtCalculation.Location = New System.Drawing.Point(13, 483)
        Me.lblMaxInstallmentAmtCalculation.Name = "lblMaxInstallmentAmtCalculation"
        Me.lblMaxInstallmentAmtCalculation.Size = New System.Drawing.Size(148, 27)
        Me.lblMaxInstallmentAmtCalculation.TabIndex = 417
        Me.lblMaxInstallmentAmtCalculation.Text = "Maximum Installment Amount Calculation"
        Me.lblMaxInstallmentAmtCalculation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboMappedHead
        '
        Me.cboMappedHead.DropDownWidth = 250
        Me.cboMappedHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboMappedHead.FormattingEnabled = True
        Me.cboMappedHead.Location = New System.Drawing.Point(455, 235)
        Me.cboMappedHead.Name = "cboMappedHead"
        Me.cboMappedHead.Size = New System.Drawing.Size(163, 21)
        Me.cboMappedHead.TabIndex = 416
        '
        'lblMappedHead
        '
        Me.lblMappedHead.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMappedHead.Location = New System.Drawing.Point(345, 238)
        Me.lblMappedHead.Name = "lblMappedHead"
        Me.lblMappedHead.Size = New System.Drawing.Size(99, 15)
        Me.lblMappedHead.TabIndex = 415
        Me.lblMappedHead.Text = "Mapped Head"
        Me.lblMappedHead.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCostCenter
        '
        Me.cboCostCenter.DropDownWidth = 250
        Me.cboCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCostCenter.FormattingEnabled = True
        Me.cboCostCenter.Location = New System.Drawing.Point(164, 186)
        Me.cboCostCenter.Name = "cboCostCenter"
        Me.cboCostCenter.Size = New System.Drawing.Size(171, 21)
        Me.cboCostCenter.TabIndex = 396
        '
        'lblCostCenter
        '
        Me.lblCostCenter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCostCenter.Location = New System.Drawing.Point(14, 188)
        Me.lblCostCenter.Name = "lblCostCenter"
        Me.lblCostCenter.Size = New System.Drawing.Size(148, 16)
        Me.lblCostCenter.TabIndex = 414
        Me.lblCostCenter.Text = "Cost Center"
        Me.lblCostCenter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboNetPay
        '
        Me.cboNetPay.DropDownWidth = 250
        Me.cboNetPay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboNetPay.FormattingEnabled = True
        Me.cboNetPay.Location = New System.Drawing.Point(164, 394)
        Me.cboNetPay.Name = "cboNetPay"
        Me.cboNetPay.Size = New System.Drawing.Size(171, 21)
        Me.cboNetPay.TabIndex = 402
        '
        'lblNetPay
        '
        Me.lblNetPay.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNetPay.Location = New System.Drawing.Point(14, 396)
        Me.lblNetPay.Name = "lblNetPay"
        Me.lblNetPay.Size = New System.Drawing.Size(142, 16)
        Me.lblNetPay.TabIndex = 413
        Me.lblNetPay.Text = "Net Pay head (Optional)"
        Me.lblNetPay.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMaxNoOfInstallment
        '
        Me.txtMaxNoOfInstallment.AllowNegative = False
        Me.txtMaxNoOfInstallment.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMaxNoOfInstallment.DigitsInGroup = 0
        Me.txtMaxNoOfInstallment.Flags = 65536
        Me.txtMaxNoOfInstallment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxNoOfInstallment.Location = New System.Drawing.Point(508, 366)
        Me.txtMaxNoOfInstallment.MaxDecimalPlaces = 0
        Me.txtMaxNoOfInstallment.MaxWholeDigits = 21
        Me.txtMaxNoOfInstallment.Name = "txtMaxNoOfInstallment"
        Me.txtMaxNoOfInstallment.Prefix = ""
        Me.txtMaxNoOfInstallment.RangeMax = 1.7976931348623157E+308
        Me.txtMaxNoOfInstallment.RangeMin = -1.7976931348623157E+308
        Me.txtMaxNoOfInstallment.Size = New System.Drawing.Size(98, 21)
        Me.txtMaxNoOfInstallment.TabIndex = 403
        Me.txtMaxNoOfInstallment.Text = "0"
        Me.txtMaxNoOfInstallment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMaxNoOfInstallment
        '
        Me.lblMaxNoOfInstallment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxNoOfInstallment.Location = New System.Drawing.Point(351, 368)
        Me.lblMaxNoOfInstallment.Name = "lblMaxNoOfInstallment"
        Me.lblMaxNoOfInstallment.Size = New System.Drawing.Size(151, 16)
        Me.lblMaxNoOfInstallment.TabIndex = 412
        Me.lblMaxNoOfInstallment.Text = "Maximum No. Of Installment"
        Me.lblMaxNoOfInstallment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEMIExceedPerc2
        '
        Me.lblEMIExceedPerc2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIExceedPerc2.Location = New System.Drawing.Point(230, 368)
        Me.lblEMIExceedPerc2.Name = "lblEMIExceedPerc2"
        Me.lblEMIExceedPerc2.Size = New System.Drawing.Size(127, 16)
        Me.lblEMIExceedPerc2.TabIndex = 411
        Me.lblEMIExceedPerc2.Text = "% of previous net pay"
        Me.lblEMIExceedPerc2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEMIExceedPerc
        '
        Me.txtEMIExceedPerc.AllowNegative = False
        Me.txtEMIExceedPerc.BackColor = System.Drawing.SystemColors.Window
        Me.txtEMIExceedPerc.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtEMIExceedPerc.DigitsInGroup = 0
        Me.txtEMIExceedPerc.Flags = 65536
        Me.txtEMIExceedPerc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEMIExceedPerc.Location = New System.Drawing.Point(164, 366)
        Me.txtEMIExceedPerc.MaxDecimalPlaces = 6
        Me.txtEMIExceedPerc.MaxWholeDigits = 21
        Me.txtEMIExceedPerc.Name = "txtEMIExceedPerc"
        Me.txtEMIExceedPerc.Prefix = ""
        Me.txtEMIExceedPerc.RangeMax = 1.7976931348623157E+308
        Me.txtEMIExceedPerc.RangeMin = -1.7976931348623157E+308
        Me.txtEMIExceedPerc.Size = New System.Drawing.Size(59, 21)
        Me.txtEMIExceedPerc.TabIndex = 400
        Me.txtEMIExceedPerc.Text = "0"
        Me.txtEMIExceedPerc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblEMIExceedPerc
        '
        Me.lblEMIExceedPerc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIExceedPerc.Location = New System.Drawing.Point(14, 366)
        Me.lblEMIExceedPerc.Name = "lblEMIExceedPerc"
        Me.lblEMIExceedPerc.Size = New System.Drawing.Size(148, 27)
        Me.lblEMIExceedPerc.TabIndex = 410
        Me.lblEMIExceedPerc.Text = "Monthly Installment Amount should not exceed"
        Me.lblEMIExceedPerc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'elDefaultInfo
        '
        Me.elDefaultInfo.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elDefaultInfo.Location = New System.Drawing.Point(4, 213)
        Me.elDefaultInfo.Name = "elDefaultInfo"
        Me.elDefaultInfo.Size = New System.Drawing.Size(623, 17)
        Me.elDefaultInfo.TabIndex = 409
        Me.elDefaultInfo.Text = "Default Calculation Type && Interest Rate"
        '
        'pnlCalcType
        '
        Me.pnlCalcType.Controls.Add(Me.cboInterestCalcType)
        Me.pnlCalcType.Controls.Add(Me.txtLoanRate)
        Me.pnlCalcType.Controls.Add(Me.lblInterestCalcType)
        Me.pnlCalcType.Controls.Add(Me.lblLoanInterest)
        Me.pnlCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlCalcType.Location = New System.Drawing.Point(9, 259)
        Me.pnlCalcType.Name = "pnlCalcType"
        Me.pnlCalcType.Size = New System.Drawing.Size(622, 25)
        Me.pnlCalcType.TabIndex = 408
        '
        'cboInterestCalcType
        '
        Me.cboInterestCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterestCalcType.FormattingEnabled = True
        Me.cboInterestCalcType.Location = New System.Drawing.Point(156, 2)
        Me.cboInterestCalcType.Name = "cboInterestCalcType"
        Me.cboInterestCalcType.Size = New System.Drawing.Size(171, 21)
        Me.cboInterestCalcType.TabIndex = 0
        '
        'txtLoanRate
        '
        Me.txtLoanRate.AllowNegative = False
        Me.txtLoanRate.BackColor = System.Drawing.SystemColors.Window
        Me.txtLoanRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanRate.DigitsInGroup = 0
        Me.txtLoanRate.Flags = 65536
        Me.txtLoanRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanRate.Location = New System.Drawing.Point(446, 2)
        Me.txtLoanRate.MaxDecimalPlaces = 6
        Me.txtLoanRate.MaxWholeDigits = 21
        Me.txtLoanRate.Name = "txtLoanRate"
        Me.txtLoanRate.Prefix = ""
        Me.txtLoanRate.RangeMax = 1.7976931348623157E+308
        Me.txtLoanRate.RangeMin = -1.7976931348623157E+308
        Me.txtLoanRate.Size = New System.Drawing.Size(163, 21)
        Me.txtLoanRate.TabIndex = 337
        Me.txtLoanRate.Text = "0"
        Me.txtLoanRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblInterestCalcType
        '
        Me.lblInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestCalcType.Location = New System.Drawing.Point(5, 4)
        Me.lblInterestCalcType.Name = "lblInterestCalcType"
        Me.lblInterestCalcType.Size = New System.Drawing.Size(137, 16)
        Me.lblInterestCalcType.TabIndex = 0
        Me.lblInterestCalcType.Text = "Int. Calc. Type"
        Me.lblInterestCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanInterest
        '
        Me.lblLoanInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanInterest.Location = New System.Drawing.Point(336, 5)
        Me.lblLoanInterest.Name = "lblLoanInterest"
        Me.lblLoanInterest.Size = New System.Drawing.Size(99, 15)
        Me.lblLoanInterest.TabIndex = 336
        Me.lblLoanInterest.Text = "Rate (%)"
        Me.lblLoanInterest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanCalcType
        '
        Me.cboLoanCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanCalcType.DropDownWidth = 250
        Me.cboLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanCalcType.FormattingEnabled = True
        Me.cboLoanCalcType.Location = New System.Drawing.Point(164, 235)
        Me.cboLoanCalcType.Name = "cboLoanCalcType"
        Me.cboLoanCalcType.Size = New System.Drawing.Size(171, 21)
        Me.cboLoanCalcType.TabIndex = 397
        '
        'lblLoanCalcType
        '
        Me.lblLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanCalcType.Location = New System.Drawing.Point(14, 237)
        Me.lblLoanCalcType.Name = "lblLoanCalcType"
        Me.lblLoanCalcType.Size = New System.Drawing.Size(137, 16)
        Me.lblLoanCalcType.TabIndex = 406
        Me.lblLoanCalcType.Text = "Loan Calc. Type"
        Me.lblLoanCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDescription
        '
        Me.lblDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDescription.Location = New System.Drawing.Point(14, 120)
        Me.lblDescription.Name = "lblDescription"
        Me.lblDescription.Size = New System.Drawing.Size(148, 16)
        Me.lblDescription.TabIndex = 395
        Me.lblDescription.Text = "Description"
        Me.lblDescription.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDescription
        '
        Me.txtDescription.Flags = 0
        Me.txtDescription.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDescription.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtDescription.Location = New System.Drawing.Point(164, 117)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDescription.Size = New System.Drawing.Size(454, 63)
        Me.txtDescription.TabIndex = 394
        '
        'lnEligibility
        '
        Me.lnEligibility.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.lnEligibility.Location = New System.Drawing.Point(4, 317)
        Me.lnEligibility.Name = "lnEligibility"
        Me.lnEligibility.Size = New System.Drawing.Size(623, 15)
        Me.lnEligibility.TabIndex = 399
        Me.lnEligibility.Text = "Loan Eligibility Info"
        '
        'txtMinSalary
        '
        Me.txtMinSalary.AllowNegative = True
        Me.txtMinSalary.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMinSalary.DigitsInGroup = 0
        Me.txtMinSalary.Flags = 0
        Me.txtMinSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMinSalary.Location = New System.Drawing.Point(164, 337)
        Me.txtMinSalary.MaxDecimalPlaces = 6
        Me.txtMinSalary.MaxWholeDigits = 21
        Me.txtMinSalary.Name = "txtMinSalary"
        Me.txtMinSalary.Prefix = ""
        Me.txtMinSalary.RangeMax = 1.7976931348623157E+308
        Me.txtMinSalary.RangeMin = -1.7976931348623157E+308
        Me.txtMinSalary.Size = New System.Drawing.Size(171, 21)
        Me.txtMinSalary.TabIndex = 398
        Me.txtMinSalary.Text = "0"
        Me.txtMinSalary.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblMinSalary
        '
        Me.lblMinSalary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinSalary.Location = New System.Drawing.Point(14, 339)
        Me.lblMinSalary.Name = "lblMinSalary"
        Me.lblMinSalary.Size = New System.Drawing.Size(145, 16)
        Me.lblMinSalary.TabIndex = 401
        Me.lblMinSalary.Text = "Minimum Net Monthly Salary"
        Me.lblMinSalary.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtName
        '
        Me.txtName.Flags = 0
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtName.Location = New System.Drawing.Point(164, 35)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(457, 21)
        Me.txtName.TabIndex = 392
        '
        'lblName
        '
        Me.lblName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblName.Location = New System.Drawing.Point(14, 37)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(148, 16)
        Me.lblName.TabIndex = 393
        Me.lblName.Text = "Loan Scheme Name"
        Me.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCode
        '
        Me.txtCode.Flags = 0
        Me.txtCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCode.Location = New System.Drawing.Point(164, 8)
        Me.txtCode.MaxLength = 255
        Me.txtCode.Name = "txtCode"
        Me.txtCode.Size = New System.Drawing.Size(171, 21)
        Me.txtCode.TabIndex = 390
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(14, 10)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(148, 16)
        Me.lblCode.TabIndex = 391
        Me.lblCode.Text = "Code"
        Me.lblCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 529)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(685, 50)
        Me.objFooter.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(580, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(94, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(480, 9)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(94, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn1.HeaderText = "tranheadunkid"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Visible = False
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn2.HeaderText = "Code"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Visible = False
        Me.DataGridViewTextBoxColumn2.Width = 575
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn3.HeaderText = "Head Name"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn4.HeaderText = "Document Type"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "Masterunkid"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Visible = False
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.HeaderText = "Document Type"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn7.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhExpiryTitleEmployeeId"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn8.Visible = False
        Me.DataGridViewTextBoxColumn8.Width = 575
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn9.HeaderText = "Document Type"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn10.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhExpiryTitleEmployeeId"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'frmLoanScheme_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(685, 579)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanScheme_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add / Edit Loan Scheme"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.gbLoanInfo.ResumeLayout(False)
        Me.pnlLoanScheme.ResumeLayout(False)
        Me.pnlLoanScheme.PerformLayout()
        Me.tabLoanScheme.ResumeLayout(False)
        Me.tbpLoanOtherOptions.ResumeLayout(False)
        Me.pnlLoanOtherOptions.ResumeLayout(False)
        Me.gbDocumentTypeList.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvDocumentType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMinNoOfInstallmentPaid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpLoanApprovelReminder.ResumeLayout(False)
        Me.pnlLoanApprovalReminderOptions.ResumeLayout(False)
        Me.gbFlexcubeSuccessfulEmployeeList.ResumeLayout(False)
        Me.gbFlexcubeSuccessfulEmployeeList.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.gvFlexcubeSuccessfulEmployeeList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbEmployeeList.ResumeLayout(False)
        Me.gbEmployeeList.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgEmployee, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudEscalationDays, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbpMortgageLoanOptions.ResumeLayout(False)
        Me.pnlMortgageLoanOptions.ResumeLayout(False)
        Me.gbSeniorLoanApprovalAlert.ResumeLayout(False)
        Me.pnlLoanApproverLevel.ResumeLayout(False)
        CType(Me.dgSeniorLoanApprovalAlert, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbLoanTrancheDocumentList.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        CType(Me.dgLoanTrancheDocumentList, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudNoOfDaysBefereTitleExpiry, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbExpiryofTitleEmployeeList.ResumeLayout(False)
        Me.gbExpiryofTitleEmployeeList.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.dgExpiryofTitleEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMaxLoanAmount.ResumeLayout(False)
        Me.pnlMaxLoanAmount.PerformLayout()
        Me.pnlMaxLoanFormula.ResumeLayout(False)
        Me.pnlMaxLoanFormula.PerformLayout()
        Me.pnlCalcType.ResumeLayout(False)
        Me.pnlCalcType.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents gbLoanInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlLoanScheme As System.Windows.Forms.Panel
    Friend WithEvents gbDocumentTypeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dgvDocumentType As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhMasterunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgColhDocumentType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkAttachementRequired As System.Windows.Forms.CheckBox
    Friend WithEvents pnlMaxLoanAmount As System.Windows.Forms.Panel
    Friend WithEvents lblMaximumLoanAmount As System.Windows.Forms.Label
    Friend WithEvents txtMaxLoanAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents radFormula As System.Windows.Forms.RadioButton
    Friend WithEvents pnlMaxLoanFormula As System.Windows.Forms.Panel
    Friend WithEvents txtMaxLoanFormula As System.Windows.Forms.TextBox
    Friend WithEvents lblMaxLoanFormula As System.Windows.Forms.Label
    Friend WithEvents radFlatRate As System.Windows.Forms.RadioButton
    Friend WithEvents lblMaxLoanCalcType As System.Windows.Forms.Label
    Friend WithEvents txtInsuranceRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblInsuranceRate As System.Windows.Forms.Label
    Friend WithEvents txtRepaymentDays As eZee.TextBox.NumericTextBox
    Friend WithEvents lblRepaymentDays As System.Windows.Forms.Label
    Friend WithEvents cboLoanSchemeCategory As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanSchemeCategory As System.Windows.Forms.Label
    Friend WithEvents objbtnKeywordsMaxInstallment As eZee.Common.eZeeGradientButton
    Friend WithEvents txtMaxInstallmentAmtCalculation As System.Windows.Forms.TextBox
    Friend WithEvents lblMaxInstallmentAmtCalculation As System.Windows.Forms.Label
    Friend WithEvents cboMappedHead As System.Windows.Forms.ComboBox
    Friend WithEvents lblMappedHead As System.Windows.Forms.Label
    Friend WithEvents cboCostCenter As System.Windows.Forms.ComboBox
    Friend WithEvents lblCostCenter As System.Windows.Forms.Label
    Friend WithEvents cboNetPay As System.Windows.Forms.ComboBox
    Friend WithEvents lblNetPay As System.Windows.Forms.Label
    Friend WithEvents txtMaxNoOfInstallment As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMaxNoOfInstallment As System.Windows.Forms.Label
    Friend WithEvents lblEMIExceedPerc2 As System.Windows.Forms.Label
    Friend WithEvents txtEMIExceedPerc As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEMIExceedPerc As System.Windows.Forms.Label
    Friend WithEvents elDefaultInfo As eZee.Common.eZeeLine
    Friend WithEvents pnlCalcType As System.Windows.Forms.Panel
    Friend WithEvents cboInterestCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents txtLoanRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblInterestCalcType As System.Windows.Forms.Label
    Friend WithEvents lblLoanInterest As System.Windows.Forms.Label
    Friend WithEvents cboLoanCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanCalcType As System.Windows.Forms.Label
    Friend WithEvents chkShowLoanBalOnPayslip As System.Windows.Forms.CheckBox
    Friend WithEvents chkShowonESS As System.Windows.Forms.CheckBox
    Friend WithEvents lblDescription As System.Windows.Forms.Label
    Friend WithEvents txtDescription As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lnEligibility As eZee.Common.eZeeLine
    Friend WithEvents txtMinSalary As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMinSalary As System.Windows.Forms.Label
    Friend WithEvents txtName As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents txtCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents chkRequiredReportingToApproval As System.Windows.Forms.CheckBox
    Friend WithEvents lblMinimumLoanAmount As System.Windows.Forms.Label
    Friend WithEvents txtMinLoanAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents chkSkipApproval As System.Windows.Forms.CheckBox
    Friend WithEvents chkPostingToFlexcube As System.Windows.Forms.CheckBox
    Friend WithEvents chkEligibleForTopup As System.Windows.Forms.CheckBox
    Friend WithEvents lblNoOfInstallmentPaid As System.Windows.Forms.Label
    Friend WithEvents nudMinNoOfInstallmentPaid As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkLoanApprovalDailyReminder As System.Windows.Forms.CheckBox
    Friend WithEvents gbEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nudEscalationDays As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkEscalationDays As System.Windows.Forms.CheckBox
    Friend WithEvents LblDays As System.Windows.Forms.Label
    Friend WithEvents txtEmployeeSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents chkSelectAllEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents dgEmployee As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhEmpCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblMinNoOfInstallment As System.Windows.Forms.Label
    Friend WithEvents txtMinNoOfInstallment As eZee.TextBox.NumericTextBox
    Friend WithEvents nudNoOfDaysBefereTitleExpiry As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblNoOfDaysBefereTitleExpiry As System.Windows.Forms.Label
    Friend WithEvents objbtnKeywordsMaxInstallmentMortgage As eZee.Common.eZeeGradientButton
    Friend WithEvents txtMaxInstallmentAmtForMortgage As System.Windows.Forms.TextBox
    Friend WithEvents lblMaxInstallmentAmtForMortgage As System.Windows.Forms.Label
    Friend WithEvents chkNtfForExpiryofTitle As System.Windows.Forms.CheckBox
    Friend WithEvents gbExpiryofTitleEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtExpiryofTitleEmpSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents chkAllExpiryofTitleEmp As System.Windows.Forms.CheckBox
    Friend WithEvents dgExpiryofTitleEmp As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhExpiryTitleEmpCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhExpiryTitleEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhExpiryTitleEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tabLoanScheme As System.Windows.Forms.TabControl
    Friend WithEvents tbpLoanOtherOptions As System.Windows.Forms.TabPage
    Friend WithEvents tbpLoanApprovelReminder As System.Windows.Forms.TabPage
    Friend WithEvents pnlLoanOtherOptions As System.Windows.Forms.Panel
    Friend WithEvents pnlLoanApprovalReminderOptions As System.Windows.Forms.Panel
    Friend WithEvents tbpMortgageLoanOptions As System.Windows.Forms.TabPage
    Friend WithEvents pnlMortgageLoanOptions As System.Windows.Forms.Panel
    Friend WithEvents gbLoanTrancheDocumentList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents dgLoanTrancheDocumentList As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhLoanTranCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhLoanTrancheMasterunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoanTranchedocumentType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtCreditCardPercentageDSR As eZee.TextBox.NumericTextBox
    Friend WithEvents LblCreditCardPercentageDSR As System.Windows.Forms.Label
    Friend WithEvents LblCreditCardAmtOnDSR As System.Windows.Forms.Label
    Friend WithEvents cboCreditCardAmountOnDSR As System.Windows.Forms.ComboBox
    Friend WithEvents cboCreditCardAmountForExpouser As System.Windows.Forms.ComboBox
    Friend WithEvents LblCreditCardAmountForExpouser As System.Windows.Forms.Label
    Friend WithEvents chkFSVValueMandatory As System.Windows.Forms.CheckBox
    Friend WithEvents chkMarketValueMandatory As System.Windows.Forms.CheckBox
    Friend WithEvents chkBOQMandatory As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbSeniorLoanApprovalAlert As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlLoanApproverLevel As System.Windows.Forms.Panel
    Friend WithEvents dgSeniorLoanApprovalAlert As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboSeniorApproverLevel As System.Windows.Forms.ComboBox
    Friend WithEvents objdgcolhLevelCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhApproverLevel As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPriority As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhLevelId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkDisableEmployeeConfirm As System.Windows.Forms.CheckBox
    Friend WithEvents chkFlexcubeSuccessfulNotification As System.Windows.Forms.CheckBox
    Friend WithEvents gbFlexcubeSuccessfulEmployeeList As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtFlexcubeSuccessfulEmployeeSearch As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents chkSelectAllFinalApprovalEmployee As System.Windows.Forms.CheckBox
    Friend WithEvents gvFlexcubeSuccessfulEmployeeList As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhFinalApprovalEmpCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhFinalApprovalEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhFinalApprovalEmployeeId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
