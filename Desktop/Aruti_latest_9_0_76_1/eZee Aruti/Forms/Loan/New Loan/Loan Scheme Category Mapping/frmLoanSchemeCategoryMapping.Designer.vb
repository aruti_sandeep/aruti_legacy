﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanSchemeCategoryMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanSchemeCategoryMapping))
        Me.eZeeHeader = New eZee.Common.eZeeHeader
        Me.gbMappingInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboOtherDocument = New System.Windows.Forms.ComboBox
        Me.lblOtherDocument = New System.Windows.Forms.Label
        Me.cboChargesStatus = New System.Windows.Forms.ComboBox
        Me.cboIdentityType = New System.Windows.Forms.ComboBox
        Me.cboLoanSchemeCategory = New System.Windows.Forms.ComboBox
        Me.lblChargesStatus = New System.Windows.Forms.Label
        Me.lblIdentityType = New System.Windows.Forms.Label
        Me.lblLoanSchemeCategory = New System.Windows.Forms.Label
        Me.objLine1 = New eZee.Common.eZeeLine
        Me.pnlMasterItemList = New System.Windows.Forms.Panel
        Me.lvMasterListInfo = New eZee.Common.eZeeListView(Me.components)
        Me.colhLoanSchemeCateogry = New System.Windows.Forms.ColumnHeader
        Me.colhIdentityType = New System.Windows.Forms.ColumnHeader
        Me.colhChargesStatus = New System.Windows.Forms.ColumnHeader
        Me.colhOtherDocument = New System.Windows.Forms.ColumnHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.colhCRBDocument = New System.Windows.Forms.ColumnHeader
        Me.cboCRBDocument = New System.Windows.Forms.ComboBox
        Me.LblCRBDocument = New System.Windows.Forms.Label
        Me.gbMappingInfo.SuspendLayout()
        Me.pnlMasterItemList.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'eZeeHeader
        '
        Me.eZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.eZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.eZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.eZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.eZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.eZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.eZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.eZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.eZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.eZeeHeader.Icon = Nothing
        Me.eZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.eZeeHeader.Message = ""
        Me.eZeeHeader.Name = "eZeeHeader"
        Me.eZeeHeader.Size = New System.Drawing.Size(653, 58)
        Me.eZeeHeader.TabIndex = 2
        Me.eZeeHeader.Title = "Loan Scheme Category Mapping"
        '
        'gbMappingInfo
        '
        Me.gbMappingInfo.BorderColor = System.Drawing.Color.Black
        Me.gbMappingInfo.Checked = False
        Me.gbMappingInfo.CollapseAllExceptThis = False
        Me.gbMappingInfo.CollapsedHoverImage = Nothing
        Me.gbMappingInfo.CollapsedNormalImage = Nothing
        Me.gbMappingInfo.CollapsedPressedImage = Nothing
        Me.gbMappingInfo.CollapseOnLoad = False
        Me.gbMappingInfo.Controls.Add(Me.cboCRBDocument)
        Me.gbMappingInfo.Controls.Add(Me.LblCRBDocument)
        Me.gbMappingInfo.Controls.Add(Me.cboOtherDocument)
        Me.gbMappingInfo.Controls.Add(Me.lblOtherDocument)
        Me.gbMappingInfo.Controls.Add(Me.cboChargesStatus)
        Me.gbMappingInfo.Controls.Add(Me.cboIdentityType)
        Me.gbMappingInfo.Controls.Add(Me.cboLoanSchemeCategory)
        Me.gbMappingInfo.Controls.Add(Me.lblChargesStatus)
        Me.gbMappingInfo.Controls.Add(Me.lblIdentityType)
        Me.gbMappingInfo.Controls.Add(Me.lblLoanSchemeCategory)
        Me.gbMappingInfo.Controls.Add(Me.objLine1)
        Me.gbMappingInfo.Controls.Add(Me.pnlMasterItemList)
        Me.gbMappingInfo.ExpandedHoverImage = Nothing
        Me.gbMappingInfo.ExpandedNormalImage = Nothing
        Me.gbMappingInfo.ExpandedPressedImage = Nothing
        Me.gbMappingInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbMappingInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbMappingInfo.HeaderHeight = 25
        Me.gbMappingInfo.HeaderMessage = ""
        Me.gbMappingInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbMappingInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbMappingInfo.HeightOnCollapse = 0
        Me.gbMappingInfo.LeftTextSpace = 0
        Me.gbMappingInfo.Location = New System.Drawing.Point(0, 61)
        Me.gbMappingInfo.Name = "gbMappingInfo"
        Me.gbMappingInfo.OpenHeight = 336
        Me.gbMappingInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbMappingInfo.ShowBorder = True
        Me.gbMappingInfo.ShowCheckBox = False
        Me.gbMappingInfo.ShowCollapseButton = False
        Me.gbMappingInfo.ShowDefaultBorderColor = True
        Me.gbMappingInfo.ShowDownButton = False
        Me.gbMappingInfo.ShowHeader = True
        Me.gbMappingInfo.Size = New System.Drawing.Size(653, 325)
        Me.gbMappingInfo.TabIndex = 3
        Me.gbMappingInfo.Temp = 0
        Me.gbMappingInfo.Text = "Mapping Info"
        Me.gbMappingInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOtherDocument
        '
        Me.cboOtherDocument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherDocument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherDocument.FormattingEnabled = True
        Me.cboOtherDocument.Location = New System.Drawing.Point(451, 35)
        Me.cboOtherDocument.Name = "cboOtherDocument"
        Me.cboOtherDocument.Size = New System.Drawing.Size(180, 21)
        Me.cboOtherDocument.TabIndex = 427
        '
        'lblOtherDocument
        '
        Me.lblOtherDocument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherDocument.Location = New System.Drawing.Point(324, 38)
        Me.lblOtherDocument.Name = "lblOtherDocument"
        Me.lblOtherDocument.Size = New System.Drawing.Size(121, 15)
        Me.lblOtherDocument.TabIndex = 426
        Me.lblOtherDocument.Text = "Other Document"
        Me.lblOtherDocument.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboChargesStatus
        '
        Me.cboChargesStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboChargesStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboChargesStatus.FormattingEnabled = True
        Me.cboChargesStatus.Location = New System.Drawing.Point(139, 90)
        Me.cboChargesStatus.Name = "cboChargesStatus"
        Me.cboChargesStatus.Size = New System.Drawing.Size(180, 21)
        Me.cboChargesStatus.TabIndex = 424
        '
        'cboIdentityType
        '
        Me.cboIdentityType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIdentityType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIdentityType.FormattingEnabled = True
        Me.cboIdentityType.Location = New System.Drawing.Point(139, 62)
        Me.cboIdentityType.Name = "cboIdentityType"
        Me.cboIdentityType.Size = New System.Drawing.Size(180, 21)
        Me.cboIdentityType.TabIndex = 423
        '
        'cboLoanSchemeCategory
        '
        Me.cboLoanSchemeCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanSchemeCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanSchemeCategory.FormattingEnabled = True
        Me.cboLoanSchemeCategory.Location = New System.Drawing.Point(139, 35)
        Me.cboLoanSchemeCategory.Name = "cboLoanSchemeCategory"
        Me.cboLoanSchemeCategory.Size = New System.Drawing.Size(180, 21)
        Me.cboLoanSchemeCategory.TabIndex = 422
        '
        'lblChargesStatus
        '
        Me.lblChargesStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChargesStatus.Location = New System.Drawing.Point(12, 90)
        Me.lblChargesStatus.Name = "lblChargesStatus"
        Me.lblChargesStatus.Size = New System.Drawing.Size(121, 15)
        Me.lblChargesStatus.TabIndex = 239
        Me.lblChargesStatus.Text = "Charges Status"
        Me.lblChargesStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblIdentityType
        '
        Me.lblIdentityType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdentityType.Location = New System.Drawing.Point(12, 65)
        Me.lblIdentityType.Name = "lblIdentityType"
        Me.lblIdentityType.Size = New System.Drawing.Size(121, 15)
        Me.lblIdentityType.TabIndex = 238
        Me.lblIdentityType.Text = "Identity Type"
        Me.lblIdentityType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanSchemeCategory
        '
        Me.lblLoanSchemeCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanSchemeCategory.Location = New System.Drawing.Point(12, 38)
        Me.lblLoanSchemeCategory.Name = "lblLoanSchemeCategory"
        Me.lblLoanSchemeCategory.Size = New System.Drawing.Size(121, 15)
        Me.lblLoanSchemeCategory.TabIndex = 237
        Me.lblLoanSchemeCategory.Text = "Loan Scheme Category"
        Me.lblLoanSchemeCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objLine1
        '
        Me.objLine1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objLine1.Location = New System.Drawing.Point(12, 114)
        Me.objLine1.Name = "objLine1"
        Me.objLine1.Size = New System.Drawing.Size(638, 12)
        Me.objLine1.TabIndex = 7
        Me.objLine1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlMasterItemList
        '
        Me.pnlMasterItemList.Controls.Add(Me.lvMasterListInfo)
        Me.pnlMasterItemList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlMasterItemList.Location = New System.Drawing.Point(11, 129)
        Me.pnlMasterItemList.Name = "pnlMasterItemList"
        Me.pnlMasterItemList.Size = New System.Drawing.Size(630, 184)
        Me.pnlMasterItemList.TabIndex = 11
        '
        'lvMasterListInfo
        '
        Me.lvMasterListInfo.BackColorOnChecked = True
        Me.lvMasterListInfo.ColumnHeaders = Nothing
        Me.lvMasterListInfo.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhLoanSchemeCateogry, Me.colhIdentityType, Me.colhChargesStatus, Me.colhOtherDocument, Me.colhCRBDocument})
        Me.lvMasterListInfo.CompulsoryColumns = ""
        Me.lvMasterListInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvMasterListInfo.FullRowSelect = True
        Me.lvMasterListInfo.GridLines = True
        Me.lvMasterListInfo.GroupingColumn = Nothing
        Me.lvMasterListInfo.HideSelection = False
        Me.lvMasterListInfo.Location = New System.Drawing.Point(0, 0)
        Me.lvMasterListInfo.MinColumnWidth = 50
        Me.lvMasterListInfo.MultiSelect = False
        Me.lvMasterListInfo.Name = "lvMasterListInfo"
        Me.lvMasterListInfo.OptionalColumns = ""
        Me.lvMasterListInfo.ShowMoreItem = False
        Me.lvMasterListInfo.ShowSaveItem = False
        Me.lvMasterListInfo.ShowSelectAll = True
        Me.lvMasterListInfo.ShowSizeAllColumnsToFit = True
        Me.lvMasterListInfo.Size = New System.Drawing.Size(630, 184)
        Me.lvMasterListInfo.Sortable = True
        Me.lvMasterListInfo.TabIndex = 0
        Me.lvMasterListInfo.UseCompatibleStateImageBehavior = False
        Me.lvMasterListInfo.View = System.Windows.Forms.View.Details
        '
        'colhLoanSchemeCateogry
        '
        Me.colhLoanSchemeCateogry.Text = "Loan Scheme Cateogry"
        Me.colhLoanSchemeCateogry.Width = 0
        '
        'colhIdentityType
        '
        Me.colhIdentityType.Tag = "colhIdentityType"
        Me.colhIdentityType.Text = "Identity Type"
        Me.colhIdentityType.Width = 150
        '
        'colhChargesStatus
        '
        Me.colhChargesStatus.Tag = "colhChargesStatus"
        Me.colhChargesStatus.Text = "Charges Status"
        Me.colhChargesStatus.Width = 150
        '
        'colhOtherDocument
        '
        Me.colhOtherDocument.Tag = "colhOtherDocument"
        Me.colhOtherDocument.Text = "Other Document"
        Me.colhOtherDocument.Width = 160
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 389)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(653, 55)
        Me.objFooter.TabIndex = 4
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(441, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 1
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(338, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(544, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'colhCRBDocument
        '
        Me.colhCRBDocument.Tag = "colhCRBDocument"
        Me.colhCRBDocument.Text = "CRB Document"
        Me.colhCRBDocument.Width = 160
        '
        'cboCRBDocument
        '
        Me.cboCRBDocument.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCRBDocument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCRBDocument.FormattingEnabled = True
        Me.cboCRBDocument.Location = New System.Drawing.Point(451, 62)
        Me.cboCRBDocument.Name = "cboCRBDocument"
        Me.cboCRBDocument.Size = New System.Drawing.Size(180, 21)
        Me.cboCRBDocument.TabIndex = 430
        '
        'LblCRBDocument
        '
        Me.LblCRBDocument.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblCRBDocument.Location = New System.Drawing.Point(324, 65)
        Me.LblCRBDocument.Name = "LblCRBDocument"
        Me.LblCRBDocument.Size = New System.Drawing.Size(121, 15)
        Me.LblCRBDocument.TabIndex = 429
        Me.LblCRBDocument.Text = "CRB Document"
        Me.LblCRBDocument.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmLoanSchemeCategoryMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(653, 444)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbMappingInfo)
        Me.Controls.Add(Me.eZeeHeader)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanSchemeCategoryMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Loan Scheme Category Mapping"
        Me.gbMappingInfo.ResumeLayout(False)
        Me.pnlMasterItemList.ResumeLayout(False)
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents eZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents gbMappingInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objLine1 As eZee.Common.eZeeLine
    Friend WithEvents pnlMasterItemList As System.Windows.Forms.Panel
    Friend WithEvents lvMasterListInfo As eZee.Common.eZeeListView
    Friend WithEvents colhLoanSchemeCateogry As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIdentityType As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhChargesStatus As System.Windows.Forms.ColumnHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblChargesStatus As System.Windows.Forms.Label
    Friend WithEvents lblIdentityType As System.Windows.Forms.Label
    Friend WithEvents lblLoanSchemeCategory As System.Windows.Forms.Label
    Friend WithEvents cboLoanSchemeCategory As System.Windows.Forms.ComboBox
    Friend WithEvents cboChargesStatus As System.Windows.Forms.ComboBox
    Friend WithEvents cboIdentityType As System.Windows.Forms.ComboBox
    Friend WithEvents cboOtherDocument As System.Windows.Forms.ComboBox
    Friend WithEvents lblOtherDocument As System.Windows.Forms.Label
    Friend WithEvents colhOtherDocument As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhCRBDocument As System.Windows.Forms.ColumnHeader
    Friend WithEvents cboCRBDocument As System.Windows.Forms.ComboBox
    Friend WithEvents LblCRBDocument As System.Windows.Forms.Label
End Class
