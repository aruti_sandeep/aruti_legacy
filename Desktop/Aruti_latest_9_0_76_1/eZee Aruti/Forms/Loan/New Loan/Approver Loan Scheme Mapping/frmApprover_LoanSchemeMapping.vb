﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmApprover_LoanSchemeMapping

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmApprover_LoanSchemeMapping"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objLoanMapping As clsapprover_scheme_mapping
    Friend mintMappingUnkid As Integer = -1
    Private mintApproverunkid As Integer = -1
    Private mstrEmployeeName As String = String.Empty
    Private mstrApproverLevel As String = String.Empty
    Private mdtLoanSchemeMapping As DataTable = Nothing
    Private dvLoanSchemeMapping As DataView = Nothing

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal _intApproverunkid As Integer) As Boolean
        Try
            mintMappingUnkid = intUnkId
            mintApproverunkid = _intApproverunkid
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintMappingUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Property"

    Public Property _EmployeeName() As String
        Get
            Return mstrEmployeeName
        End Get
        Set(ByVal value As String)
            mstrEmployeeName = value
        End Set
    End Property

    Public Property _ApproverLevel() As String
        Get
            Return mstrApproverLevel
        End Get
        Set(ByVal value As String)
            mstrApproverLevel = value
        End Set
    End Property

    Public Property _dtLoanSchemeMapping() As DataTable
        Get
            Return mdtLoanSchemeMapping
        End Get
        Set(ByVal value As DataTable)
            mdtLoanSchemeMapping = value
        End Set
    End Property

#End Region

#Region " Form's Events "

    Private Sub frmApprover_LoanSchemeMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objLoanMapping = New clsapprover_scheme_mapping
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            GetApproverInfo()
            FillLoanScheme()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_LoanSchemeMapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmApprover_LoanSchemeMapping_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_LoanSchemeMapping_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub ffrmApprover_LoanSchemeMapping_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objLoanMapping = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub GetApproverInfo()
        Try
            If mstrEmployeeName.Trim.ToString().Length <= 0 Then

                Dim objapprover As New clsLoanApprover_master
                objapprover._lnApproverunkid = mintApproverunkid

                Dim objEmployee As New clsEmployee_Master

                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objEmployee._Employeeunkid = objapprover._ApproverEmpunkid
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objapprover._ApproverEmpunkid
                'S.SANDEEP [04 JUN 2015] -- END

                objlblApprover.Text = objEmployee._Employeecode & " - " & objEmployee._Firstname & "  " & objEmployee._Surname
                objEmployee = Nothing

                Dim objLevel As New clslnapproverlevel_master
                objLevel._lnLevelunkid = objapprover._lnLevelunkid
                objlblApproverLevel.Text = objLevel._lnLevelname
                objLevel = Nothing
            Else
                objlblApprover.Text = mstrEmployeeName
                objlblApproverLevel.Text = mstrApproverLevel
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverInfo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillLoanScheme()
        Dim dsFill As DataSet = Nothing
        Try
            Dim objLoanScheme As New clsLoan_Scheme

            If mdtLoanSchemeMapping Is Nothing Then
                dsFill = objLoanScheme.getComboList(False, "List")
            Else
                Dim dsLoanScheme As DataSet = Nothing
                dsLoanScheme = New DataSet
                'Pinkal (26-Sep-2017) -- Start
                'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.
                'dsLoanScheme.Tables.Add(mdtLoanSchemeMapping)
                dsLoanScheme.Tables.Add(mdtLoanSchemeMapping.Copy())
                'Pinkal (26-Sep-2017) -- End
                dsFill = dsLoanScheme
            End If


            For Each dr As DataRow In dsFill.Tables(0).Rows

                Dim drRow As DataRow = objLoanMapping._dtLoanScheme.NewRow()
                drRow("Loanschememappingunkid") = objLoanMapping.GetLoanSchemeMappingUnkId(mintApproverunkid, CInt(dr("loanschemeunkid")))
                If mintApproverunkid > 0 AndAlso mdtLoanSchemeMapping Is Nothing Then
                    drRow("ischecked") = objLoanMapping.isExist(mintApproverunkid, CInt(dr("loanschemeunkid")))
                Else
                    If dsFill.Tables(0).Columns.Contains("ischecked") Then
                        drRow("ischecked") = dr("ischecked")
                    Else
                        drRow("ischecked") = False
                    End If
                End If

                drRow("loanschemeunkid") = dr("loanschemeunkid").ToString()
                drRow("code") = dr("Code").ToString()
                drRow("name") = dr("name").ToString()
                objLoanMapping._dtLoanScheme.Rows.Add(drRow)
            Next

            dvLoanSchemeMapping = objLoanMapping._dtLoanScheme.DefaultView

            dgLoanScheme.AutoGenerateColumns = False
            dgLoanScheme.DataSource = objLoanMapping._dtLoanScheme
            objdgcolhSelect.DataPropertyName = "ischecked"
            dgcolhSchemeCode.DataPropertyName = "code"
            dgcolhLoanScheme.DataPropertyName = "name"
            objdgcolhLoanSchemeMappingunkid.DataPropertyName = "Loanschememappingunkid"

            SetCheckBoxValue()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLoanScheme", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objLoanMapping._Approverunkid = mintApproverunkid
            objLoanMapping._Userunkid = User._Object._Userunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try

    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = objLoanMapping._dtLoanScheme.Select("ischecked = true")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgLoanScheme.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgLoanScheme.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try

    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try

            If objLoanMapping._dtLoanScheme Is Nothing Then Exit Sub

            Dim drRow As DataRow() = objLoanMapping._dtLoanScheme.Select("ischecked = true")
            If drRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Loan Scheme is compulsory information.Please Select one Loan Scheme."), CType(MsgBoxStyle.Information + MsgBoxStyle.OkOnly, enMsgBoxStyle))
                dgLoanScheme.Select()
                Exit Sub
            End If
            _dtLoanSchemeMapping = objLoanMapping._dtLoanScheme
            'Pinkal (26-Sep-2017) -- Start
            'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.
            mblnCancel = False
            'Pinkal (26-Sep-2017) -- End
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 

            'Pinkal (26-Sep-2017) -- Start
            'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.
            '_dtLoanSchemeMapping = Nothing
            'Pinkal (26-Sep-2017) -- End

            'Nilay (01-Mar-2016) -- End
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "CheckBox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try

            If objLoanMapping._dtLoanScheme.Rows.Count > 0 Then
                For Each dr As DataRow In objLoanMapping._dtLoanScheme.Rows
                    RemoveHandler dgLoanScheme.CellContentClick, AddressOf dgLoanScheme_CellContentClick
                    dr("ischecked") = chkSelectAll.Checked
                    AddHandler dgLoanScheme.CellContentClick, AddressOf dgLoanScheme_CellContentClick
                Next
                objLoanMapping._dtLoanScheme.AcceptChanges()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGridView Event"

    Private Sub dgLoanScheme_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgLoanScheme.CellContentClick, dgLoanScheme.CellContentDoubleClick
        Try
            If e.RowIndex < 0 Then Exit Sub

            If e.ColumnIndex = objdgcolhSelect.Index Then

                If dgLoanScheme.IsCurrentCellDirty Then
                    dgLoanScheme.CommitEdit(DataGridViewDataErrorContexts.Commit)
                    objLoanMapping._dtLoanScheme.AcceptChanges()
                End If

                SetCheckBoxValue()

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLoanScheme_CellContentClick", mstrModuleName)
        End Try

    End Sub

#End Region

#Region "Textbox Event"

    Private Sub txtSearchScheme_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchScheme.TextChanged
        Try
            Dim strSearch As String = ""
            If txtSearchScheme.Text.Trim.Length > 0 Then
                strSearch = dgcolhSchemeCode.DataPropertyName & " LIKE '%" & txtSearchScheme.Text & "%' OR " & _
                            dgcolhLoanScheme.DataPropertyName & " LIKE '%" & txtSearchScheme.Text & "%' "
            End If
            dvLoanSchemeMapping.RowFilter = strSearch
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchScheme_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbLeavedetail.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLeavedetail.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOK.GradientBackColor = GUI._ButttonBackColor
            Me.btnOK.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnOK.Text = Language._Object.getCaption(Me.btnOK.Name, Me.btnOK.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbLeavedetail.Text = Language._Object.getCaption(Me.gbLeavedetail.Name, Me.gbLeavedetail.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.lblApproverLevel.Text = Language._Object.getCaption(Me.lblApproverLevel.Name, Me.lblApproverLevel.Text)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.dgcolhSchemeCode.HeaderText = Language._Object.getCaption(Me.dgcolhSchemeCode.Name, Me.dgcolhSchemeCode.HeaderText)
            Me.dgcolhLoanScheme.HeaderText = Language._Object.getCaption(Me.dgcolhLoanScheme.Name, Me.dgcolhLoanScheme.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Loan Scheme is compulsory information.Please Select one Loan Scheme.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class