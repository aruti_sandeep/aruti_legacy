﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanApproval
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanApproval))
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbProcessPendingLoanInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.pnlScanAttachment = New System.Windows.Forms.Panel
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.dgvLoanApplicationAttachment = New System.Windows.Forms.DataGridView
        Me.lblDocumentType = New System.Windows.Forms.Label
        Me.lblAppliedAmnt = New System.Windows.Forms.Label
        Me.txtAppliedAmnt = New eZee.TextBox.NumericTextBox
        Me.txtBasicSal = New eZee.TextBox.AlphanumericTextBox
        Me.lblBasicSal = New System.Windows.Forms.Label
        Me.txtApprover = New eZee.TextBox.AlphanumericTextBox
        Me.lblApprover = New System.Windows.Forms.Label
        Me.lblExternalEntity = New System.Windows.Forms.Label
        Me.txtCurrency = New eZee.TextBox.AlphanumericTextBox
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.txtEmployee = New eZee.TextBox.AlphanumericTextBox
        Me.objvalPeriodDuration = New System.Windows.Forms.Label
        Me.objbtnSearchDeductionPeriod = New eZee.Common.eZeeGradientButton
        Me.nudDurationInMths = New System.Windows.Forms.NumericUpDown
        Me.lblDuration = New System.Windows.Forms.Label
        Me.cboDeductionPeriod = New System.Windows.Forms.ComboBox
        Me.txtEMIInstallments = New eZee.TextBox.NumericTextBox
        Me.lblDeductionPeriod = New System.Windows.Forms.Label
        Me.lblEMIInstallments = New System.Windows.Forms.Label
        Me.txtInstallmentAmt = New eZee.TextBox.NumericTextBox
        Me.lblEMIAmount = New System.Windows.Forms.Label
        Me.txtExternalEntity = New eZee.TextBox.AlphanumericTextBox
        Me.lblApproverRemarks = New System.Windows.Forms.Label
        Me.txtRemark = New eZee.TextBox.AlphanumericTextBox
        Me.dtpApplicationDate = New System.Windows.Forms.DateTimePicker
        Me.txtApplicationNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblApplicationDate = New System.Windows.Forms.Label
        Me.lblApplicationNo = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.lblAmt = New System.Windows.Forms.Label
        Me.txtLoanAmt = New eZee.TextBox.NumericTextBox
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.radAdvance = New System.Windows.Forms.RadioButton
        Me.radLoan = New System.Windows.Forms.RadioButton
        Me.lblLoanAdvance = New System.Windows.Forms.Label
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.sfdAttachment = New System.Windows.Forms.SaveFileDialog
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSize = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDownload = New System.Windows.Forms.DataGridViewLinkColumn
        Me.objcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhScanUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain.SuspendLayout()
        Me.gbProcessPendingLoanInfo.SuspendLayout()
        Me.pnlScanAttachment.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvLoanApplicationAttachment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDurationInMths, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbProcessPendingLoanInfo)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(521, 571)
        Me.pnlMain.TabIndex = 0
        '
        'gbProcessPendingLoanInfo
        '
        Me.gbProcessPendingLoanInfo.BorderColor = System.Drawing.Color.Black
        Me.gbProcessPendingLoanInfo.Checked = False
        Me.gbProcessPendingLoanInfo.CollapseAllExceptThis = False
        Me.gbProcessPendingLoanInfo.CollapsedHoverImage = Nothing
        Me.gbProcessPendingLoanInfo.CollapsedNormalImage = Nothing
        Me.gbProcessPendingLoanInfo.CollapsedPressedImage = Nothing
        Me.gbProcessPendingLoanInfo.CollapseOnLoad = False
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.pnlScanAttachment)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblAppliedAmnt)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtAppliedAmnt)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtBasicSal)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblBasicSal)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtApprover)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblApprover)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblExternalEntity)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtCurrency)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.cboStatus)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblStatus)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtEmployee)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.objvalPeriodDuration)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.objbtnSearchDeductionPeriod)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.nudDurationInMths)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblDuration)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.cboDeductionPeriod)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtEMIInstallments)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblDeductionPeriod)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblEMIInstallments)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtInstallmentAmt)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblEMIAmount)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtExternalEntity)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblApproverRemarks)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtRemark)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.dtpApplicationDate)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtApplicationNo)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblApplicationDate)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblApplicationNo)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.cboLoanScheme)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblAmt)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.txtLoanAmt)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblLoanScheme)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.radAdvance)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.radLoan)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblLoanAdvance)
        Me.gbProcessPendingLoanInfo.Controls.Add(Me.lblEmpName)
        Me.gbProcessPendingLoanInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbProcessPendingLoanInfo.ExpandedHoverImage = Nothing
        Me.gbProcessPendingLoanInfo.ExpandedNormalImage = Nothing
        Me.gbProcessPendingLoanInfo.ExpandedPressedImage = Nothing
        Me.gbProcessPendingLoanInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbProcessPendingLoanInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbProcessPendingLoanInfo.HeaderHeight = 25
        Me.gbProcessPendingLoanInfo.HeaderMessage = ""
        Me.gbProcessPendingLoanInfo.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbProcessPendingLoanInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbProcessPendingLoanInfo.HeightOnCollapse = 0
        Me.gbProcessPendingLoanInfo.LeftTextSpace = 0
        Me.gbProcessPendingLoanInfo.Location = New System.Drawing.Point(0, 0)
        Me.gbProcessPendingLoanInfo.Name = "gbProcessPendingLoanInfo"
        Me.gbProcessPendingLoanInfo.OpenHeight = 300
        Me.gbProcessPendingLoanInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbProcessPendingLoanInfo.ShowBorder = True
        Me.gbProcessPendingLoanInfo.ShowCheckBox = False
        Me.gbProcessPendingLoanInfo.ShowCollapseButton = False
        Me.gbProcessPendingLoanInfo.ShowDefaultBorderColor = True
        Me.gbProcessPendingLoanInfo.ShowDownButton = False
        Me.gbProcessPendingLoanInfo.ShowHeader = True
        Me.gbProcessPendingLoanInfo.Size = New System.Drawing.Size(521, 516)
        Me.gbProcessPendingLoanInfo.TabIndex = 1
        Me.gbProcessPendingLoanInfo.Temp = 0
        Me.gbProcessPendingLoanInfo.Text = "Loan Approval Info"
        Me.gbProcessPendingLoanInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlScanAttachment
        '
        Me.pnlScanAttachment.Controls.Add(Me.Panel3)
        Me.pnlScanAttachment.Controls.Add(Me.lblDocumentType)
        Me.pnlScanAttachment.Location = New System.Drawing.Point(105, 376)
        Me.pnlScanAttachment.Name = "pnlScanAttachment"
        Me.pnlScanAttachment.Size = New System.Drawing.Size(379, 131)
        Me.pnlScanAttachment.TabIndex = 410
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.dgvLoanApplicationAttachment)
        Me.Panel3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel3.Location = New System.Drawing.Point(5, 26)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(463, 97)
        Me.Panel3.TabIndex = 407
        '
        'dgvLoanApplicationAttachment
        '
        Me.dgvLoanApplicationAttachment.AllowUserToAddRows = False
        Me.dgvLoanApplicationAttachment.AllowUserToDeleteRows = False
        Me.dgvLoanApplicationAttachment.AllowUserToResizeRows = False
        Me.dgvLoanApplicationAttachment.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoanApplicationAttachment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvLoanApplicationAttachment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLoanApplicationAttachment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colhName, Me.colhSize, Me.objcolhDownload, Me.objcolhGUID, Me.objcolhScanUnkId})
        Me.dgvLoanApplicationAttachment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLoanApplicationAttachment.Location = New System.Drawing.Point(0, 0)
        Me.dgvLoanApplicationAttachment.Name = "dgvLoanApplicationAttachment"
        Me.dgvLoanApplicationAttachment.RowHeadersVisible = False
        Me.dgvLoanApplicationAttachment.Size = New System.Drawing.Size(463, 97)
        Me.dgvLoanApplicationAttachment.TabIndex = 0
        '
        'lblDocumentType
        '
        Me.lblDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocumentType.Location = New System.Drawing.Point(4, 6)
        Me.lblDocumentType.Name = "lblDocumentType"
        Me.lblDocumentType.Size = New System.Drawing.Size(296, 16)
        Me.lblDocumentType.TabIndex = 406
        Me.lblDocumentType.Text = "Document Type(.PDF and Images only)"
        Me.lblDocumentType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblAppliedAmnt
        '
        Me.lblAppliedAmnt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppliedAmnt.Location = New System.Drawing.Point(292, 187)
        Me.lblAppliedAmnt.Name = "lblAppliedAmnt"
        Me.lblAppliedAmnt.Size = New System.Drawing.Size(82, 17)
        Me.lblAppliedAmnt.TabIndex = 36
        Me.lblAppliedAmnt.Text = "Applied Amnt"
        Me.lblAppliedAmnt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAppliedAmnt
        '
        Me.txtAppliedAmnt.AllowNegative = False
        Me.txtAppliedAmnt.BackColor = System.Drawing.Color.White
        Me.txtAppliedAmnt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAppliedAmnt.DigitsInGroup = 0
        Me.txtAppliedAmnt.Flags = 65536
        Me.txtAppliedAmnt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAppliedAmnt.Location = New System.Drawing.Point(381, 186)
        Me.txtAppliedAmnt.MaxDecimalPlaces = 6
        Me.txtAppliedAmnt.MaxWholeDigits = 21
        Me.txtAppliedAmnt.Name = "txtAppliedAmnt"
        Me.txtAppliedAmnt.Prefix = ""
        Me.txtAppliedAmnt.RangeMax = 1.7976931348623157E+308
        Me.txtAppliedAmnt.RangeMin = -1.7976931348623157E+308
        Me.txtAppliedAmnt.ReadOnly = True
        Me.txtAppliedAmnt.Size = New System.Drawing.Size(105, 21)
        Me.txtAppliedAmnt.TabIndex = 37
        Me.txtAppliedAmnt.Text = "0"
        Me.txtAppliedAmnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBasicSal
        '
        Me.txtBasicSal.BackColor = System.Drawing.Color.White
        Me.txtBasicSal.Flags = 0
        Me.txtBasicSal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBasicSal.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBasicSal.Location = New System.Drawing.Point(374, 58)
        Me.txtBasicSal.Name = "txtBasicSal"
        Me.txtBasicSal.ReadOnly = True
        Me.txtBasicSal.Size = New System.Drawing.Size(111, 21)
        Me.txtBasicSal.TabIndex = 34
        Me.txtBasicSal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblBasicSal
        '
        Me.lblBasicSal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBasicSal.Location = New System.Drawing.Point(292, 60)
        Me.lblBasicSal.Name = "lblBasicSal"
        Me.lblBasicSal.Size = New System.Drawing.Size(76, 17)
        Me.lblBasicSal.TabIndex = 33
        Me.lblBasicSal.Text = "Basic Salary"
        '
        'txtApprover
        '
        Me.txtApprover.BackColor = System.Drawing.Color.White
        Me.txtApprover.Flags = 0
        Me.txtApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApprover.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApprover.Location = New System.Drawing.Point(108, 85)
        Me.txtApprover.Name = "txtApprover"
        Me.txtApprover.ReadOnly = True
        Me.txtApprover.Size = New System.Drawing.Size(377, 21)
        Me.txtApprover.TabIndex = 7
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(8, 87)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(93, 17)
        Me.lblApprover.TabIndex = 6
        Me.lblApprover.Text = "Approver"
        '
        'lblExternalEntity
        '
        Me.lblExternalEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExternalEntity.Location = New System.Drawing.Point(8, 134)
        Me.lblExternalEntity.Name = "lblExternalEntity"
        Me.lblExternalEntity.Size = New System.Drawing.Size(93, 17)
        Me.lblExternalEntity.TabIndex = 11
        Me.lblExternalEntity.Text = "External Entity"
        '
        'txtCurrency
        '
        Me.txtCurrency.BackColor = System.Drawing.Color.White
        Me.txtCurrency.Flags = 0
        Me.txtCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCurrency.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCurrency.Location = New System.Drawing.Point(218, 186)
        Me.txtCurrency.Name = "txtCurrency"
        Me.txtCurrency.ReadOnly = True
        Me.txtCurrency.Size = New System.Drawing.Size(65, 21)
        Me.txtCurrency.TabIndex = 17
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(108, 293)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(175, 21)
        Me.cboStatus.TabIndex = 29
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(8, 295)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(93, 17)
        Me.lblStatus.TabIndex = 28
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtEmployee
        '
        Me.txtEmployee.BackColor = System.Drawing.Color.White
        Me.txtEmployee.Flags = 0
        Me.txtEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployee.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEmployee.Location = New System.Drawing.Point(108, 58)
        Me.txtEmployee.Name = "txtEmployee"
        Me.txtEmployee.ReadOnly = True
        Me.txtEmployee.Size = New System.Drawing.Size(175, 21)
        Me.txtEmployee.TabIndex = 5
        '
        'objvalPeriodDuration
        '
        Me.objvalPeriodDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objvalPeriodDuration.Location = New System.Drawing.Point(108, 242)
        Me.objvalPeriodDuration.Name = "objvalPeriodDuration"
        Me.objvalPeriodDuration.Size = New System.Drawing.Size(375, 16)
        Me.objvalPeriodDuration.TabIndex = 23
        Me.objvalPeriodDuration.Text = "#periodDuration"
        Me.objvalPeriodDuration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchDeductionPeriod
        '
        Me.objbtnSearchDeductionPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDeductionPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeductionPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeductionPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDeductionPeriod.BorderSelected = False
        Me.objbtnSearchDeductionPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDeductionPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDeductionPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDeductionPeriod.Location = New System.Drawing.Point(492, 213)
        Me.objbtnSearchDeductionPeriod.Name = "objbtnSearchDeductionPeriod"
        Me.objbtnSearchDeductionPeriod.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchDeductionPeriod.TabIndex = 20
        '
        'nudDurationInMths
        '
        Me.nudDurationInMths.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDurationInMths.Location = New System.Drawing.Point(407, 109)
        Me.nudDurationInMths.Name = "nudDurationInMths"
        Me.nudDurationInMths.Size = New System.Drawing.Size(78, 21)
        Me.nudDurationInMths.TabIndex = 19
        Me.nudDurationInMths.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudDurationInMths.Visible = False
        '
        'lblDuration
        '
        Me.lblDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDuration.Location = New System.Drawing.Point(292, 110)
        Me.lblDuration.Name = "lblDuration"
        Me.lblDuration.Size = New System.Drawing.Size(112, 18)
        Me.lblDuration.TabIndex = 18
        Me.lblDuration.Text = "Duration In Months"
        Me.lblDuration.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblDuration.Visible = False
        '
        'cboDeductionPeriod
        '
        Me.cboDeductionPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeductionPeriod.FormattingEnabled = True
        Me.cboDeductionPeriod.Location = New System.Drawing.Point(108, 213)
        Me.cboDeductionPeriod.Name = "cboDeductionPeriod"
        Me.cboDeductionPeriod.Size = New System.Drawing.Size(378, 21)
        Me.cboDeductionPeriod.TabIndex = 22
        '
        'txtEMIInstallments
        '
        Me.txtEMIInstallments.AllowNegative = False
        Me.txtEMIInstallments.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtEMIInstallments.DigitsInGroup = 0
        Me.txtEMIInstallments.Flags = 65536
        Me.txtEMIInstallments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEMIInstallments.Location = New System.Drawing.Point(407, 266)
        Me.txtEMIInstallments.MaxDecimalPlaces = 0
        Me.txtEMIInstallments.MaxWholeDigits = 4
        Me.txtEMIInstallments.Name = "txtEMIInstallments"
        Me.txtEMIInstallments.Prefix = ""
        Me.txtEMIInstallments.RangeMax = 1.7976931348623157E+308
        Me.txtEMIInstallments.RangeMin = -1.7976931348623157E+308
        Me.txtEMIInstallments.Size = New System.Drawing.Size(78, 21)
        Me.txtEMIInstallments.TabIndex = 27
        Me.txtEMIInstallments.Text = "0"
        Me.txtEMIInstallments.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblDeductionPeriod
        '
        Me.lblDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDeductionPeriod.Location = New System.Drawing.Point(8, 215)
        Me.lblDeductionPeriod.Name = "lblDeductionPeriod"
        Me.lblDeductionPeriod.Size = New System.Drawing.Size(93, 17)
        Me.lblDeductionPeriod.TabIndex = 21
        Me.lblDeductionPeriod.Text = "Deduction Period"
        Me.lblDeductionPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEMIInstallments
        '
        Me.lblEMIInstallments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIInstallments.Location = New System.Drawing.Point(292, 267)
        Me.lblEMIInstallments.Name = "lblEMIInstallments"
        Me.lblEMIInstallments.Size = New System.Drawing.Size(112, 18)
        Me.lblEMIInstallments.TabIndex = 26
        Me.lblEMIInstallments.Text = "No. of Installments"
        Me.lblEMIInstallments.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInstallmentAmt
        '
        Me.txtInstallmentAmt.AllowNegative = False
        Me.txtInstallmentAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtInstallmentAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInstallmentAmt.DigitsInGroup = 0
        Me.txtInstallmentAmt.Flags = 65536
        Me.txtInstallmentAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInstallmentAmt.Location = New System.Drawing.Point(108, 266)
        Me.txtInstallmentAmt.MaxDecimalPlaces = 6
        Me.txtInstallmentAmt.MaxWholeDigits = 21
        Me.txtInstallmentAmt.Name = "txtInstallmentAmt"
        Me.txtInstallmentAmt.Prefix = ""
        Me.txtInstallmentAmt.RangeMax = 1.7976931348623157E+308
        Me.txtInstallmentAmt.RangeMin = -1.7976931348623157E+308
        Me.txtInstallmentAmt.Size = New System.Drawing.Size(105, 21)
        Me.txtInstallmentAmt.TabIndex = 25
        Me.txtInstallmentAmt.Text = "0"
        Me.txtInstallmentAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblEMIAmount
        '
        Me.lblEMIAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIAmount.Location = New System.Drawing.Point(8, 268)
        Me.lblEMIAmount.Name = "lblEMIAmount"
        Me.lblEMIAmount.Size = New System.Drawing.Size(93, 17)
        Me.lblEMIAmount.TabIndex = 24
        Me.lblEMIAmount.Text = "Installment Amt."
        Me.lblEMIAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExternalEntity
        '
        Me.txtExternalEntity.BackColor = System.Drawing.Color.White
        Me.txtExternalEntity.Flags = 0
        Me.txtExternalEntity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExternalEntity.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtExternalEntity.Location = New System.Drawing.Point(108, 132)
        Me.txtExternalEntity.Name = "txtExternalEntity"
        Me.txtExternalEntity.ReadOnly = True
        Me.txtExternalEntity.Size = New System.Drawing.Size(378, 21)
        Me.txtExternalEntity.TabIndex = 12
        '
        'lblApproverRemarks
        '
        Me.lblApproverRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApproverRemarks.Location = New System.Drawing.Point(8, 322)
        Me.lblApproverRemarks.Name = "lblApproverRemarks"
        Me.lblApproverRemarks.Size = New System.Drawing.Size(93, 17)
        Me.lblApproverRemarks.TabIndex = 30
        Me.lblApproverRemarks.Text = "Remarks"
        '
        'txtRemark
        '
        Me.txtRemark.Flags = 0
        Me.txtRemark.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemark.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(124)}
        Me.txtRemark.Location = New System.Drawing.Point(108, 320)
        Me.txtRemark.Multiline = True
        Me.txtRemark.Name = "txtRemark"
        Me.txtRemark.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtRemark.Size = New System.Drawing.Size(378, 50)
        Me.txtRemark.TabIndex = 31
        '
        'dtpApplicationDate
        '
        Me.dtpApplicationDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplicationDate.Checked = False
        Me.dtpApplicationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpApplicationDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpApplicationDate.Location = New System.Drawing.Point(374, 30)
        Me.dtpApplicationDate.Name = "dtpApplicationDate"
        Me.dtpApplicationDate.Size = New System.Drawing.Size(111, 21)
        Me.dtpApplicationDate.TabIndex = 3
        '
        'txtApplicationNo
        '
        Me.txtApplicationNo.BackColor = System.Drawing.Color.White
        Me.txtApplicationNo.Flags = 0
        Me.txtApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicationNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplicationNo.Location = New System.Drawing.Point(108, 30)
        Me.txtApplicationNo.Name = "txtApplicationNo"
        Me.txtApplicationNo.ReadOnly = True
        Me.txtApplicationNo.Size = New System.Drawing.Size(105, 21)
        Me.txtApplicationNo.TabIndex = 1
        '
        'lblApplicationDate
        '
        Me.lblApplicationDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicationDate.Location = New System.Drawing.Point(292, 33)
        Me.lblApplicationDate.Name = "lblApplicationDate"
        Me.lblApplicationDate.Size = New System.Drawing.Size(52, 15)
        Me.lblApplicationDate.TabIndex = 2
        Me.lblApplicationDate.Text = "Date"
        '
        'lblApplicationNo
        '
        Me.lblApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApplicationNo.Location = New System.Drawing.Point(8, 32)
        Me.lblApplicationNo.Name = "lblApplicationNo"
        Me.lblApplicationNo.Size = New System.Drawing.Size(93, 17)
        Me.lblApplicationNo.TabIndex = 0
        Me.lblApplicationNo.Text = "Application No."
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.BackColor = System.Drawing.Color.White
        Me.cboLoanScheme.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Items.AddRange(New Object() {"Select"})
        Me.cboLoanScheme.Location = New System.Drawing.Point(108, 160)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(378, 21)
        Me.cboLoanScheme.TabIndex = 14
        '
        'lblAmt
        '
        Me.lblAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmt.Location = New System.Drawing.Point(8, 187)
        Me.lblAmt.Name = "lblAmt"
        Me.lblAmt.Size = New System.Drawing.Size(93, 17)
        Me.lblAmt.TabIndex = 15
        Me.lblAmt.Text = "Approved Amnt"
        Me.lblAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLoanAmt
        '
        Me.txtLoanAmt.AllowNegative = False
        Me.txtLoanAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanAmt.DigitsInGroup = 0
        Me.txtLoanAmt.Flags = 65536
        Me.txtLoanAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanAmt.Location = New System.Drawing.Point(108, 186)
        Me.txtLoanAmt.MaxDecimalPlaces = 6
        Me.txtLoanAmt.MaxWholeDigits = 21
        Me.txtLoanAmt.Name = "txtLoanAmt"
        Me.txtLoanAmt.Prefix = ""
        Me.txtLoanAmt.RangeMax = 1.7976931348623157E+308
        Me.txtLoanAmt.RangeMin = -1.7976931348623157E+308
        Me.txtLoanAmt.Size = New System.Drawing.Size(105, 21)
        Me.txtLoanAmt.TabIndex = 16
        Me.txtLoanAmt.Text = "0"
        Me.txtLoanAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(8, 162)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(93, 17)
        Me.lblLoanScheme.TabIndex = 13
        Me.lblLoanScheme.Text = "Loan Scheme"
        '
        'radAdvance
        '
        Me.radAdvance.BackColor = System.Drawing.Color.Transparent
        Me.radAdvance.Enabled = False
        Me.radAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAdvance.Location = New System.Drawing.Point(210, 110)
        Me.radAdvance.Name = "radAdvance"
        Me.radAdvance.Size = New System.Drawing.Size(95, 17)
        Me.radAdvance.TabIndex = 10
        Me.radAdvance.Text = "Advance"
        Me.radAdvance.UseVisualStyleBackColor = False
        '
        'radLoan
        '
        Me.radLoan.BackColor = System.Drawing.Color.Transparent
        Me.radLoan.Enabled = False
        Me.radLoan.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radLoan.Location = New System.Drawing.Point(109, 110)
        Me.radLoan.Name = "radLoan"
        Me.radLoan.Size = New System.Drawing.Size(95, 17)
        Me.radLoan.TabIndex = 9
        Me.radLoan.Text = "Loan"
        Me.radLoan.UseVisualStyleBackColor = False
        '
        'lblLoanAdvance
        '
        Me.lblLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAdvance.Location = New System.Drawing.Point(8, 110)
        Me.lblLoanAdvance.Name = "lblLoanAdvance"
        Me.lblLoanAdvance.Size = New System.Drawing.Size(93, 17)
        Me.lblLoanAdvance.TabIndex = 8
        Me.lblLoanAdvance.Text = "Select"
        '
        'lblEmpName
        '
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(8, 60)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(93, 17)
        Me.lblEmpName.TabIndex = 4
        Me.lblEmpName.Text = "Employee"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 516)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(521, 55)
        Me.objFooter.TabIndex = 0
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(309, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(412, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "File Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn2.HeaderText = "File Size"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 120
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "objcolhGUID"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "objcolhScanUnkId"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'colhName
        '
        Me.colhName.HeaderText = "File Name"
        Me.colhName.Name = "colhName"
        Me.colhName.ReadOnly = True
        Me.colhName.Width = 150
        '
        'colhSize
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhSize.DefaultCellStyle = DataGridViewCellStyle5
        Me.colhSize.HeaderText = "File Size"
        Me.colhSize.Name = "colhSize"
        Me.colhSize.ReadOnly = True
        Me.colhSize.Width = 120
        '
        'objcolhDownload
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black
        Me.objcolhDownload.DefaultCellStyle = DataGridViewCellStyle6
        Me.objcolhDownload.HeaderText = "Download"
        Me.objcolhDownload.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objcolhDownload.Name = "objcolhDownload"
        Me.objcolhDownload.ReadOnly = True
        Me.objcolhDownload.Text = "Download"
        Me.objcolhDownload.UseColumnTextForLinkValue = True
        '
        'objcolhGUID
        '
        Me.objcolhGUID.HeaderText = "objcolhGUID"
        Me.objcolhGUID.Name = "objcolhGUID"
        Me.objcolhGUID.Visible = False
        '
        'objcolhScanUnkId
        '
        Me.objcolhScanUnkId.HeaderText = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Name = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Visible = False
        '
        'frmLoanApproval
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(521, 571)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanApproval"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Loan Approval"
        Me.pnlMain.ResumeLayout(False)
        Me.gbProcessPendingLoanInfo.ResumeLayout(False)
        Me.gbProcessPendingLoanInfo.PerformLayout()
        Me.pnlScanAttachment.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        CType(Me.dgvLoanApplicationAttachment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDurationInMths, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbProcessPendingLoanInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtEmployee As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objvalPeriodDuration As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchDeductionPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents nudDurationInMths As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblDuration As System.Windows.Forms.Label
    Friend WithEvents cboDeductionPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents txtEMIInstallments As eZee.TextBox.NumericTextBox
    Friend WithEvents lblDeductionPeriod As System.Windows.Forms.Label
    Friend WithEvents lblEMIInstallments As System.Windows.Forms.Label
    Friend WithEvents txtInstallmentAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEMIAmount As System.Windows.Forms.Label
    Friend WithEvents txtExternalEntity As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblApproverRemarks As System.Windows.Forms.Label
    Friend WithEvents txtRemark As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents dtpApplicationDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtApplicationNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblApplicationDate As System.Windows.Forms.Label
    Friend WithEvents lblApplicationNo As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents lblAmt As System.Windows.Forms.Label
    Friend WithEvents txtLoanAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents radAdvance As System.Windows.Forms.RadioButton
    Friend WithEvents radLoan As System.Windows.Forms.RadioButton
    Friend WithEvents lblLoanAdvance As System.Windows.Forms.Label
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents txtCurrency As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblExternalEntity As System.Windows.Forms.Label
    Friend WithEvents txtApprover As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents txtBasicSal As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBasicSal As System.Windows.Forms.Label
    Friend WithEvents lblAppliedAmnt As System.Windows.Forms.Label
    Friend WithEvents txtAppliedAmnt As eZee.TextBox.NumericTextBox
    Friend WithEvents pnlScanAttachment As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents dgvLoanApplicationAttachment As System.Windows.Forms.DataGridView
    Friend WithEvents lblDocumentType As System.Windows.Forms.Label
    Friend WithEvents sfdAttachment As System.Windows.Forms.SaveFileDialog
    Friend WithEvents colhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSize As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDownload As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents objcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhScanUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
