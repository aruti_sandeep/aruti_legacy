﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalApproveLoan
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalApproveLoan))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.gbInfo = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.txtRemarks = New eZee.TextBox.AlphanumericTextBox
        Me.lblRemarks = New System.Windows.Forms.Label
        Me.objbtnSearchApprover = New eZee.Common.eZeeGradientButton
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.cboApprover = New System.Windows.Forms.ComboBox
        Me.lblApprover = New System.Windows.Forms.Label
        Me.gbFilter = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objbtnSearchScheme = New eZee.Common.eZeeGradientButton
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.cboAmountCondition = New System.Windows.Forms.ComboBox
        Me.objbtnSearchDeductionPeriod = New eZee.Common.eZeeGradientButton
        Me.cboDeductionPeriod = New System.Windows.Forms.ComboBox
        Me.lblLoanAmount = New System.Windows.Forms.Label
        Me.LblDeductionPeriod = New System.Windows.Forms.Label
        Me.txtLoanAmount = New eZee.TextBox.NumericTextBox
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.gbPending = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.radAdvance = New System.Windows.Forms.RadioButton
        Me.radLoan = New System.Windows.Forms.RadioButton
        Me.pnlGrid = New System.Windows.Forms.Panel
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.objdgcolhCollapse = New System.Windows.Forms.DataGridViewImageColumn
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhEName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhBasicSal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAppNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhRAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDeductionPeriod = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.dgcolhDuration = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhInstallmentAmt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhNoOfInstallments = New eZee.Common.DataGridViewNumericTextBoxColumn
        Me.dgcolhAAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPendingUnkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhemployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnViewDiary = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.gbInfo.SuspendLayout()
        Me.gbFilter.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbPending.SuspendLayout()
        Me.pnlGrid.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.gbInfo)
        Me.pnlMain.Controls.Add(Me.gbFilter)
        Me.pnlMain.Controls.Add(Me.gbPending)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(862, 498)
        Me.pnlMain.TabIndex = 0
        '
        'gbInfo
        '
        Me.gbInfo.BorderColor = System.Drawing.Color.Black
        Me.gbInfo.Checked = False
        Me.gbInfo.CollapseAllExceptThis = False
        Me.gbInfo.CollapsedHoverImage = Nothing
        Me.gbInfo.CollapsedNormalImage = Nothing
        Me.gbInfo.CollapsedPressedImage = Nothing
        Me.gbInfo.CollapseOnLoad = False
        Me.gbInfo.Controls.Add(Me.txtRemarks)
        Me.gbInfo.Controls.Add(Me.lblRemarks)
        Me.gbInfo.Controls.Add(Me.objbtnSearchApprover)
        Me.gbInfo.Controls.Add(Me.cboStatus)
        Me.gbInfo.Controls.Add(Me.lblStatus)
        Me.gbInfo.Controls.Add(Me.cboApprover)
        Me.gbInfo.Controls.Add(Me.lblApprover)
        Me.gbInfo.ExpandedHoverImage = Nothing
        Me.gbInfo.ExpandedNormalImage = Nothing
        Me.gbInfo.ExpandedPressedImage = Nothing
        Me.gbInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbInfo.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbInfo.HeaderHeight = 25
        Me.gbInfo.HeaderMessage = ""
        Me.gbInfo.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbInfo.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbInfo.HeightOnCollapse = 0
        Me.gbInfo.LeftTextSpace = 0
        Me.gbInfo.Location = New System.Drawing.Point(501, 3)
        Me.gbInfo.Name = "gbInfo"
        Me.gbInfo.OpenHeight = 300
        Me.gbInfo.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbInfo.ShowBorder = True
        Me.gbInfo.ShowCheckBox = False
        Me.gbInfo.ShowCollapseButton = False
        Me.gbInfo.ShowDefaultBorderColor = True
        Me.gbInfo.ShowDownButton = False
        Me.gbInfo.ShowHeader = True
        Me.gbInfo.Size = New System.Drawing.Size(358, 140)
        Me.gbInfo.TabIndex = 1
        Me.gbInfo.Temp = 0
        Me.gbInfo.Text = "Information"
        Me.gbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtRemarks
        '
        Me.txtRemarks.Flags = 0
        Me.txtRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRemarks.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(124)}
        Me.txtRemarks.Location = New System.Drawing.Point(75, 87)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtRemarks.Size = New System.Drawing.Size(247, 44)
        Me.txtRemarks.TabIndex = 2
        '
        'lblRemarks
        '
        Me.lblRemarks.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRemarks.Location = New System.Drawing.Point(6, 88)
        Me.lblRemarks.Name = "lblRemarks"
        Me.lblRemarks.Size = New System.Drawing.Size(61, 15)
        Me.lblRemarks.TabIndex = 260
        Me.lblRemarks.Text = "Remarks"
        '
        'objbtnSearchApprover
        '
        Me.objbtnSearchApprover.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchApprover.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchApprover.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchApprover.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchApprover.BorderSelected = False
        Me.objbtnSearchApprover.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchApprover.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchApprover.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchApprover.Location = New System.Drawing.Point(328, 34)
        Me.objbtnSearchApprover.Name = "objbtnSearchApprover"
        Me.objbtnSearchApprover.Size = New System.Drawing.Size(21, 18)
        Me.objbtnSearchApprover.TabIndex = 258
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(75, 60)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(247, 21)
        Me.cboStatus.TabIndex = 1
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(6, 63)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(61, 15)
        Me.lblStatus.TabIndex = 7
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboApprover
        '
        Me.cboApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboApprover.DropDownWidth = 300
        Me.cboApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboApprover.FormattingEnabled = True
        Me.cboApprover.Location = New System.Drawing.Point(75, 33)
        Me.cboApprover.Name = "cboApprover"
        Me.cboApprover.Size = New System.Drawing.Size(247, 21)
        Me.cboApprover.TabIndex = 0
        '
        'lblApprover
        '
        Me.lblApprover.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprover.Location = New System.Drawing.Point(6, 36)
        Me.lblApprover.Name = "lblApprover"
        Me.lblApprover.Size = New System.Drawing.Size(61, 15)
        Me.lblApprover.TabIndex = 3
        Me.lblApprover.Text = "Approver"
        Me.lblApprover.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'gbFilter
        '
        Me.gbFilter.BorderColor = System.Drawing.Color.Black
        Me.gbFilter.Checked = False
        Me.gbFilter.CollapseAllExceptThis = False
        Me.gbFilter.CollapsedHoverImage = Nothing
        Me.gbFilter.CollapsedNormalImage = Nothing
        Me.gbFilter.CollapsedPressedImage = Nothing
        Me.gbFilter.CollapseOnLoad = False
        Me.gbFilter.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilter.Controls.Add(Me.objbtnSearchScheme)
        Me.gbFilter.Controls.Add(Me.objbtnReset)
        Me.gbFilter.Controls.Add(Me.objbtnSearch)
        Me.gbFilter.Controls.Add(Me.lnkAllocation)
        Me.gbFilter.Controls.Add(Me.cboAmountCondition)
        Me.gbFilter.Controls.Add(Me.objbtnSearchDeductionPeriod)
        Me.gbFilter.Controls.Add(Me.cboDeductionPeriod)
        Me.gbFilter.Controls.Add(Me.lblLoanAmount)
        Me.gbFilter.Controls.Add(Me.LblDeductionPeriod)
        Me.gbFilter.Controls.Add(Me.txtLoanAmount)
        Me.gbFilter.Controls.Add(Me.cboLoanScheme)
        Me.gbFilter.Controls.Add(Me.cboEmployee)
        Me.gbFilter.Controls.Add(Me.lblLoanScheme)
        Me.gbFilter.Controls.Add(Me.lblEmployee)
        Me.gbFilter.ExpandedHoverImage = Nothing
        Me.gbFilter.ExpandedNormalImage = Nothing
        Me.gbFilter.ExpandedPressedImage = Nothing
        Me.gbFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilter.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilter.HeaderHeight = 25
        Me.gbFilter.HeaderMessage = ""
        Me.gbFilter.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilter.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilter.HeightOnCollapse = 0
        Me.gbFilter.LeftTextSpace = 0
        Me.gbFilter.Location = New System.Drawing.Point(4, 3)
        Me.gbFilter.Name = "gbFilter"
        Me.gbFilter.OpenHeight = 300
        Me.gbFilter.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilter.ShowBorder = True
        Me.gbFilter.ShowCheckBox = False
        Me.gbFilter.ShowCollapseButton = False
        Me.gbFilter.ShowDefaultBorderColor = True
        Me.gbFilter.ShowDownButton = False
        Me.gbFilter.ShowHeader = True
        Me.gbFilter.Size = New System.Drawing.Size(493, 140)
        Me.gbFilter.TabIndex = 0
        Me.gbFilter.Temp = 0
        Me.gbFilter.Text = "Filter Criteria"
        Me.gbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(418, 115)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 18)
        Me.objbtnSearchEmployee.TabIndex = 243
        Me.objbtnSearchEmployee.Visible = False
        '
        'objbtnSearchScheme
        '
        Me.objbtnSearchScheme.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchScheme.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchScheme.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchScheme.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchScheme.BorderSelected = False
        Me.objbtnSearchScheme.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchScheme.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchScheme.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchScheme.Location = New System.Drawing.Point(391, 113)
        Me.objbtnSearchScheme.Name = "objbtnSearchScheme"
        Me.objbtnSearchScheme.Size = New System.Drawing.Size(21, 18)
        Me.objbtnSearchScheme.TabIndex = 247
        Me.objbtnSearchScheme.Visible = False
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(466, 1)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 256
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(443, 1)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 255
        Me.objbtnSearch.TabStop = False
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(345, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(94, 15)
        Me.lnkAllocation.TabIndex = 0
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocation"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboAmountCondition
        '
        Me.cboAmountCondition.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboAmountCondition.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAmountCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAmountCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAmountCondition.FormattingEnabled = True
        Me.cboAmountCondition.Location = New System.Drawing.Point(440, 88)
        Me.cboAmountCondition.Name = "cboAmountCondition"
        Me.cboAmountCondition.Size = New System.Drawing.Size(41, 21)
        Me.cboAmountCondition.TabIndex = 253
        '
        'objbtnSearchDeductionPeriod
        '
        Me.objbtnSearchDeductionPeriod.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchDeductionPeriod.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeductionPeriod.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchDeductionPeriod.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchDeductionPeriod.BorderSelected = False
        Me.objbtnSearchDeductionPeriod.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchDeductionPeriod.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchDeductionPeriod.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchDeductionPeriod.Location = New System.Drawing.Point(244, 89)
        Me.objbtnSearchDeductionPeriod.Name = "objbtnSearchDeductionPeriod"
        Me.objbtnSearchDeductionPeriod.Size = New System.Drawing.Size(21, 18)
        Me.objbtnSearchDeductionPeriod.TabIndex = 250
        '
        'cboDeductionPeriod
        '
        Me.cboDeductionPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeductionPeriod.DropDownWidth = 300
        Me.cboDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeductionPeriod.FormattingEnabled = True
        Me.cboDeductionPeriod.Location = New System.Drawing.Point(110, 88)
        Me.cboDeductionPeriod.Name = "cboDeductionPeriod"
        Me.cboDeductionPeriod.Size = New System.Drawing.Size(128, 21)
        Me.cboDeductionPeriod.TabIndex = 3
        '
        'lblLoanAmount
        '
        Me.lblLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAmount.Location = New System.Drawing.Point(271, 91)
        Me.lblLoanAmount.Name = "lblLoanAmount"
        Me.lblLoanAmount.Size = New System.Drawing.Size(73, 15)
        Me.lblLoanAmount.TabIndex = 252
        Me.lblLoanAmount.Text = "Loan Amount"
        Me.lblLoanAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LblDeductionPeriod
        '
        Me.LblDeductionPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDeductionPeriod.Location = New System.Drawing.Point(8, 91)
        Me.LblDeductionPeriod.Name = "LblDeductionPeriod"
        Me.LblDeductionPeriod.Size = New System.Drawing.Size(96, 15)
        Me.LblDeductionPeriod.TabIndex = 248
        Me.LblDeductionPeriod.Text = "Deduction Period"
        '
        'txtLoanAmount
        '
        Me.txtLoanAmount.AllowNegative = True
        Me.txtLoanAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanAmount.DigitsInGroup = 0
        Me.txtLoanAmount.Flags = 0
        Me.txtLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanAmount.Location = New System.Drawing.Point(350, 88)
        Me.txtLoanAmount.MaxDecimalPlaces = 6
        Me.txtLoanAmount.MaxWholeDigits = 21
        Me.txtLoanAmount.Name = "txtLoanAmount"
        Me.txtLoanAmount.Prefix = ""
        Me.txtLoanAmount.RangeMax = 1.7976931348623157E+308
        Me.txtLoanAmount.RangeMin = -1.7976931348623157E+308
        Me.txtLoanAmount.Size = New System.Drawing.Size(84, 21)
        Me.txtLoanAmount.TabIndex = 4
        Me.txtLoanAmount.Text = "0"
        Me.txtLoanAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownWidth = 300
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Location = New System.Drawing.Point(110, 60)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(371, 21)
        Me.cboLoanScheme.TabIndex = 2
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(110, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(371, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(8, 63)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(96, 15)
        Me.lblLoanScheme.TabIndex = 245
        Me.lblLoanScheme.Text = "Loan Scheme"
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(96, 15)
        Me.lblEmployee.TabIndex = 242
        Me.lblEmployee.Text = "Employee"
        '
        'gbPending
        '
        Me.gbPending.BorderColor = System.Drawing.Color.Black
        Me.gbPending.Checked = False
        Me.gbPending.CollapseAllExceptThis = False
        Me.gbPending.CollapsedHoverImage = Nothing
        Me.gbPending.CollapsedNormalImage = Nothing
        Me.gbPending.CollapsedPressedImage = Nothing
        Me.gbPending.CollapseOnLoad = False
        Me.gbPending.Controls.Add(Me.radAdvance)
        Me.gbPending.Controls.Add(Me.radLoan)
        Me.gbPending.Controls.Add(Me.pnlGrid)
        Me.gbPending.ExpandedHoverImage = Nothing
        Me.gbPending.ExpandedNormalImage = Nothing
        Me.gbPending.ExpandedPressedImage = Nothing
        Me.gbPending.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbPending.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbPending.HeaderHeight = 25
        Me.gbPending.HeaderMessage = ""
        Me.gbPending.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbPending.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbPending.HeightOnCollapse = 0
        Me.gbPending.LeftTextSpace = 0
        Me.gbPending.Location = New System.Drawing.Point(4, 145)
        Me.gbPending.Name = "gbPending"
        Me.gbPending.OpenHeight = 300
        Me.gbPending.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbPending.ShowBorder = True
        Me.gbPending.ShowCheckBox = False
        Me.gbPending.ShowCollapseButton = False
        Me.gbPending.ShowDefaultBorderColor = True
        Me.gbPending.ShowDownButton = False
        Me.gbPending.ShowHeader = True
        Me.gbPending.Size = New System.Drawing.Size(855, 297)
        Me.gbPending.TabIndex = 1
        Me.gbPending.Temp = 0
        Me.gbPending.Text = "Pending Loan/Advance Application(s)."
        Me.gbPending.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radAdvance
        '
        Me.radAdvance.BackColor = System.Drawing.Color.Transparent
        Me.radAdvance.Location = New System.Drawing.Point(756, 4)
        Me.radAdvance.Name = "radAdvance"
        Me.radAdvance.Size = New System.Drawing.Size(90, 17)
        Me.radAdvance.TabIndex = 1
        Me.radAdvance.TabStop = True
        Me.radAdvance.Text = "Advance"
        Me.radAdvance.UseVisualStyleBackColor = False
        '
        'radLoan
        '
        Me.radLoan.BackColor = System.Drawing.Color.Transparent
        Me.radLoan.Location = New System.Drawing.Point(653, 4)
        Me.radLoan.Name = "radLoan"
        Me.radLoan.Size = New System.Drawing.Size(90, 17)
        Me.radLoan.TabIndex = 0
        Me.radLoan.TabStop = True
        Me.radLoan.Text = "Loan"
        Me.radLoan.UseVisualStyleBackColor = False
        '
        'pnlGrid
        '
        Me.pnlGrid.Controls.Add(Me.dgvData)
        Me.pnlGrid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlGrid.Location = New System.Drawing.Point(2, 26)
        Me.pnlGrid.Name = "pnlGrid"
        Me.pnlGrid.Size = New System.Drawing.Size(850, 268)
        Me.pnlGrid.TabIndex = 2
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToResizeColumns = False
        Me.dgvData.AllowUserToResizeRows = False
        Me.dgvData.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.dgvData.ColumnHeadersHeight = 22
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCollapse, Me.objdgcolhCheck, Me.dgcolhEName, Me.dgcolhBasicSal, Me.dgcolhAppNo, Me.dgcolhRAmount, Me.dgcolhDeductionPeriod, Me.dgcolhDuration, Me.dgcolhInstallmentAmt, Me.dgcolhNoOfInstallments, Me.dgcolhAAmount, Me.objcolhPendingUnkid, Me.objdgcolhIsGrp, Me.objdgcolhGrpId, Me.objdgcolhemployeeunkid})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.MultiSelect = False
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(850, 268)
        Me.dgvData.TabIndex = 1
        '
        'objdgcolhCollapse
        '
        Me.objdgcolhCollapse.Frozen = True
        Me.objdgcolhCollapse.HeaderText = ""
        Me.objdgcolhCollapse.Name = "objdgcolhCollapse"
        Me.objdgcolhCollapse.ReadOnly = True
        Me.objdgcolhCollapse.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objdgcolhCollapse.Width = 25
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.Frozen = True
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhEName
        '
        Me.dgcolhEName.Frozen = True
        Me.dgcolhEName.HeaderText = "Employee"
        Me.dgcolhEName.Name = "dgcolhEName"
        Me.dgcolhEName.ReadOnly = True
        Me.dgcolhEName.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhEName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhEName.Width = 200
        '
        'dgcolhBasicSal
        '
        Me.dgcolhBasicSal.Frozen = True
        Me.dgcolhBasicSal.HeaderText = "Basic Salary"
        Me.dgcolhBasicSal.Name = "dgcolhBasicSal"
        Me.dgcolhBasicSal.ReadOnly = True
        Me.dgcolhBasicSal.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhBasicSal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhAppNo
        '
        Me.dgcolhAppNo.Frozen = True
        Me.dgcolhAppNo.HeaderText = "Application No"
        Me.dgcolhAppNo.Name = "dgcolhAppNo"
        Me.dgcolhAppNo.ReadOnly = True
        Me.dgcolhAppNo.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhAppNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhRAmount
        '
        Me.dgcolhRAmount.Frozen = True
        Me.dgcolhRAmount.HeaderText = "Applied Amnt"
        Me.dgcolhRAmount.Name = "dgcolhRAmount"
        Me.dgcolhRAmount.ReadOnly = True
        Me.dgcolhRAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhRAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'dgcolhDeductionPeriod
        '
        Me.dgcolhDeductionPeriod.HeaderText = "Deduction Period"
        Me.dgcolhDeductionPeriod.Name = "dgcolhDeductionPeriod"
        Me.dgcolhDeductionPeriod.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhDeductionPeriod.Width = 125
        '
        'dgcolhDuration
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle1.Format = "F0"
        Me.dgcolhDuration.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhDuration.HeaderText = "Duration"
        Me.dgcolhDuration.Name = "dgcolhDuration"
        Me.dgcolhDuration.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhDuration.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhDuration.Visible = False
        Me.dgcolhDuration.Width = 70
        '
        'dgcolhInstallmentAmt
        '
        DataGridViewCellStyle2.NullValue = Nothing
        Me.dgcolhInstallmentAmt.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhInstallmentAmt.HeaderText = "Installment Amount"
        Me.dgcolhInstallmentAmt.Name = "dgcolhInstallmentAmt"
        Me.dgcolhInstallmentAmt.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhInstallmentAmt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhInstallmentAmt.Width = 110
        '
        'dgcolhNoOfInstallments
        '
        DataGridViewCellStyle3.NullValue = Nothing
        Me.dgcolhNoOfInstallments.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgcolhNoOfInstallments.HeaderText = "No Of Installments"
        Me.dgcolhNoOfInstallments.Name = "dgcolhNoOfInstallments"
        Me.dgcolhNoOfInstallments.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhNoOfInstallments.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhNoOfInstallments.Width = 110
        '
        'dgcolhAAmount
        '
        Me.dgcolhAAmount.HeaderText = "Approved Amount"
        Me.dgcolhAAmount.Name = "dgcolhAAmount"
        Me.dgcolhAAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgcolhAAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhAAmount.Width = 110
        '
        'objcolhPendingUnkid
        '
        Me.objcolhPendingUnkid.HeaderText = "objcolhPendingUnkid"
        Me.objcolhPendingUnkid.Name = "objcolhPendingUnkid"
        Me.objcolhPendingUnkid.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhPendingUnkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objcolhPendingUnkid.Visible = False
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Visible = False
        '
        'objdgcolhemployeeunkid
        '
        Me.objdgcolhemployeeunkid.HeaderText = "objdgcolhemployeeunkid"
        Me.objdgcolhemployeeunkid.Name = "objdgcolhemployeeunkid"
        Me.objdgcolhemployeeunkid.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnViewDiary)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 443)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(862, 55)
        Me.objFooter.TabIndex = 1
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(669, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(765, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 200
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Basic Salary"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = "Application No"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.Frozen = True
        Me.DataGridViewTextBoxColumn4.HeaderText = "Applied Amnt"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'DataGridViewTextBoxColumn5
        '
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn5.HeaderText = "Installment Amount"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 110
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.HeaderText = "Approved Amount"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 110
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objcolhPendingUnkid"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhIsGrp"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objdgcolhGrpId"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhemployeeunkid"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'btnViewDiary
        '
        Me.btnViewDiary.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnViewDiary.BackColor = System.Drawing.Color.White
        Me.btnViewDiary.BackgroundImage = CType(resources.GetObject("btnViewDiary.BackgroundImage"), System.Drawing.Image)
        Me.btnViewDiary.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnViewDiary.BorderColor = System.Drawing.Color.Empty
        Me.btnViewDiary.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnViewDiary.FlatAppearance.BorderSize = 0
        Me.btnViewDiary.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnViewDiary.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnViewDiary.ForeColor = System.Drawing.Color.Black
        Me.btnViewDiary.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnViewDiary.GradientForeColor = System.Drawing.Color.Black
        Me.btnViewDiary.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewDiary.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnViewDiary.Location = New System.Drawing.Point(18, 13)
        Me.btnViewDiary.Name = "btnViewDiary"
        Me.btnViewDiary.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnViewDiary.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnViewDiary.Size = New System.Drawing.Size(90, 30)
        Me.btnViewDiary.TabIndex = 79
        Me.btnViewDiary.Text = "&View Diary"
        Me.btnViewDiary.UseVisualStyleBackColor = True
        '
        'frmGlobalApproveLoan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(862, 498)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalApproveLoan"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Approve Loan"
        Me.pnlMain.ResumeLayout(False)
        Me.gbInfo.ResumeLayout(False)
        Me.gbInfo.PerformLayout()
        Me.gbFilter.ResumeLayout(False)
        Me.gbFilter.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbPending.ResumeLayout(False)
        Me.pnlGrid.ResumeLayout(False)
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbPending As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents gbInfo As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents cboApprover As System.Windows.Forms.ComboBox
    Friend WithEvents lblApprover As System.Windows.Forms.Label
    Friend WithEvents pnlGrid As System.Windows.Forms.Panel
    Friend WithEvents radAdvance As System.Windows.Forms.RadioButton
    Friend WithEvents radLoan As System.Windows.Forms.RadioButton
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents gbFilter As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents objbtnSearchScheme As eZee.Common.eZeeGradientButton
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnSearchDeductionPeriod As eZee.Common.eZeeGradientButton
    Friend WithEvents cboDeductionPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents LblDeductionPeriod As System.Windows.Forms.Label
    Friend WithEvents lblLoanAmount As System.Windows.Forms.Label
    Friend WithEvents txtLoanAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents cboAmountCondition As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents lblRemarks As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents objbtnSearchApprover As eZee.Common.eZeeGradientButton
    Friend WithEvents objdgcolhCollapse As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhEName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhBasicSal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAppNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhRAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDeductionPeriod As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents dgcolhDuration As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhInstallmentAmt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhNoOfInstallments As eZee.Common.DataGridViewNumericTextBoxColumn
    Friend WithEvents dgcolhAAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPendingUnkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhemployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnViewDiary As eZee.Common.eZeeLightButton
End Class
