﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Math
Imports System.Threading
Imports System.IO

#End Region

Public Class frmLoanApproval

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmLoanApproval"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private objLoanApproval As clsloanapproval_process_Tran
    Private mintProcesspendingloanunkid As Integer = -1
    Private mintApproverID As Integer = -1
    Private mintPendingloantranunkid As Integer = -1
    Private mintPriority As Integer = -1
    'Hemant (30 Aug 2019) -- Start
    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
    Private trd As Thread
    Private objEmailList As New List(Of clsEmailCollection)
    'Hemant (30 Aug 2019) -- End
    'Hemant (19 Jul 2024) -- Start
    'ENHANCEMENT(FSDT): A1X - 2705 : Mandatory document attachment on non-Flexcube loans with an option for the approver to download/view
    Private mdtLoanApplicationDocument As DataTable
    Private mstrFolderName As String = ""
    Private objDocument As clsScan_Attach_Documents
    Private mdtFinalLoanApplicationDocument As DataTable
    Private mblnIsAttachmentRequired As Boolean = False
    'Hemant (19 Jul 2024) -- End
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal _ApproverID As Integer, ByVal _Processpendingloanunkid As Integer) As Boolean
        Try
            mintPendingloantranunkid = intUnkId
            menAction = eAction
            mintProcesspendingloanunkid = _Processpendingloanunkid
            mintApproverID = _ApproverID
            Me.ShowDialog()
            intUnkId = mintPendingloantranunkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmLoanApproval_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Hemant (19 Jul 2024) -- Start
        'ENHANCEMENT(FSDT): A1X - 2705 : Mandatory document attachment on non-Flexcube loans with an option for the approver to download/view
        objDocument = New clsScan_Attach_Documents
        'Hemant (19 Jul 2024) -- End
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Call SetVisibility()
            objLoanApproval = New clsloanapproval_process_Tran
            Call FillCombo()
            Getvalue()
            'Hemant (19 Jul 2024) -- Start
            'ENHANCEMENT(FSDT): A1X - 2705 : Mandatory document attachment on non-Flexcube loans with an option for the approver to download/view
            mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.LOAN_ADVANCE).Tables(0).Rows(0)("Name").ToString
            'Hemant (19 Jul 2024) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmLoanApproval_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmLoanApproval_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{TAB}")
            e.Handled = True
        End If
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsProcess_pending_loan.SetMessages()
            objfrm._Other_ModuleNames = "clsloanapproval_process_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Private Functions"

    Private Sub FillCombo()
        Dim objLoan As New clsProcess_pending_loan
        Dim objScheme As New clsLoan_Scheme
        Dim objPeriod As New clscommom_period_Tran
        Dim dsList As DataSet = Nothing
        Try

            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            'dsList = objScheme.getComboList(True, "List")
            dsList = objScheme.getComboList(True, "List", -1, "", False)
            'Pinkal (07-Dec-2017) -- End

            cboLoanScheme.ValueMember = "loanschemeunkid"
            cboLoanScheme.DisplayMember = "name"
            cboLoanScheme.DataSource = dsList.Tables(0)
            objScheme = Nothing

            dsList = Nothing
            'Nilay (10-Oct-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsList = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsList = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                               FinancialYear._Object._YearUnkid, _
                                               FinancialYear._Object._DatabaseName, _
                                               FinancialYear._Object._Database_Start_Date, _
                                               "Period", True, enStatusType.Open)
            'Nilay (10-Oct-2015) -- End


            cboDeductionPeriod.ValueMember = "periodunkid"
            cboDeductionPeriod.DisplayMember = "name"
            cboDeductionPeriod.DataSource = dsList.Tables(0)
            objPeriod = Nothing

            dsList = Nothing
            dsList = objLoan.GetLoan_Status("List", True, False)
            'Nilay (25-Jul-2016) -- Start
            Dim dtView As DataView = New DataView(dsList.Tables("List"), "Id <> 1", "", DataViewRowState.CurrentRows)
            'Nilay (25-Jul-2016) -- End
            cboStatus.ValueMember = "Id"
            cboStatus.DisplayMember = "NAME"
            cboStatus.DataSource = dtView 'Nilay (25-Jul-2016)
            cboStatus.SelectedIndex = 0
            objLoan = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If txtEmployee.Text.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                txtEmployee.Focus()
                Return False
            End If

            If radLoan.Checked Then
                If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), enMsgBoxStyle.Information)
                    cboLoanScheme.Focus()
                    Return False
                End If
            End If

            If txtLoanAmt.Decimal <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Loan/Advance Amount cannot be blank. Loan/Advance Amount is compulsory information."), enMsgBoxStyle.Information)
                txtLoanAmt.Focus()
                Return False
            End If

            If txtCurrency.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Currency is compulsory information.Please select currency."), enMsgBoxStyle.Information)
                txtCurrency.Select()
                Return False
            End If

            If CInt(cboDeductionPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Deduction Period is compulsory information.Please select deduction period."), enMsgBoxStyle.Information)
                cboDeductionPeriod.Focus()
                Return False
            End If

            If radLoan.Checked Then
                'Nilay (21-Oct-2015) -- Start
                'ENHANCEMENT : NEW LOAN Given By Rutta
                'If nudDurationInMths.Value <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Duration in Months cannot be 0.Please define duration in months greater than 0."), enMsgBoxStyle.Information)
                '    nudDurationInMths.Focus()
                '    Return False
                'End If
                'Nilay (21-Oct-2015) -- End

                If txtInstallmentAmt.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Installment Amount cannot be 0.Please define installment amount greater than 0."), enMsgBoxStyle.Information)
                    txtInstallmentAmt.Focus()
                    Return False
                End If

                'Nilay (15-Dec-2015) -- Start
                If txtInstallmentAmt.Decimal > txtLoanAmt.Decimal Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Installment Amount cannot be greater than Loan Amount."), enMsgBoxStyle.Information)
                    txtInstallmentAmt.Focus()
                    Return False
                End If
                'Nilay (15-Dec-2015) -- End

                If txtEMIInstallments.Decimal <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "No of Installment cannot be 0.Please define No of Installment greater than 0."), enMsgBoxStyle.Information)
                    txtInstallmentAmt.Focus()
                    Return False
                End If

                'Nilay (15-Dec-2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'If Format(txtLoanAmt.Decimal, GUI.fmtCurrency) <> Format((txtEMIInstallments.Decimal * CDec(txtInstallmentAmt.Tag)), GUI.fmtCurrency) Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "You are changing loan amount.We recommanded you that you have to change No of installments or Installment amount."), enMsgBoxStyle.Information)
                '    txtEMIInstallments.Focus()
                '    Return False
                'End If
                'Nilay (15-Dec-2015) -- End

            End If

            'Nilay (25-Jul-2016) -- Start
            If CInt(cboStatus.SelectedValue) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Please select atleast one Status from the list to perform further operation."), enMsgBoxStyle.Information)
                cboStatus.Focus()
                Return False
            End If
            'Nilay (25-Jul-2016) -- End

            'Nilay (20-Sept-2016) -- Start
            'Enhancement : Cancel feature for approved but not assigned loan application
            'If CInt(cboStatus.SelectedValue) = 3 AndAlso txtRemark.Text.Trim.Length <= 0 Then
            If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED AndAlso txtRemark.Text.Trim.Length <= 0 Then
                'Nilay (20-Sept-2016) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Remark cannot be blank.Remark is compulsory information."), enMsgBoxStyle.Information)
                txtRemark.Focus()
                Return False
            End If

            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            Dim objLoanScheme As New clsLoan_Scheme
            objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(txtEMIInstallments.Text) > objLoanScheme._MaxNoOfInstallment Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Installment months cannot be greater than ") & objLoanScheme._MaxNoOfInstallment & Language.getMessage(mstrModuleName, 16, " for ") & objLoanScheme._Name & Language.getMessage(mstrModuleName, 17, " Scheme."), enMsgBoxStyle.Information)
                txtEMIInstallments.Focus()
                Exit Function
            End If
            'Varsha (25 Nov 2017) -- End
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
        End Try
    End Function

    Private Sub Getvalue()
        Try
            Dim objLoanapplication As New clsProcess_pending_loan
            objLoanapplication._Processpendingloanunkid = mintProcesspendingloanunkid

            txtApplicationNo.Text = objLoanapplication._Application_No
            dtpApplicationDate.MinDate = objLoanapplication._Application_Date.Date
            dtpApplicationDate.Value = objLoanapplication._Application_Date.Date
            dtpApplicationDate.MaxDate = objLoanapplication._Application_Date.Date


            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = objLoanapplication._Employeeunkid
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLoanapplication._Employeeunkid
            'S.SANDEEP [04 JUN 2015] -- END

            txtEmployee.Tag = objLoanapplication._Employeeunkid
            txtEmployee.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmployee._Employeeunkid = -1
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = -1
            'S.SANDEEP [04 JUN 2015] -- END


            Dim objLoanApprover As New clsLoanApprover_master
            Dim objLoanLevel As New clslnapproverlevel_master
            objLoanApprover._lnApproverunkid = mintApproverID
            objLoanLevel._lnLevelunkid = objLoanApprover._lnLevelunkid
            mintPriority = objLoanLevel._Priority

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            ''S.SANDEEP [04 JUN 2015] -- START
            ''ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            ''objEmployee._Employeeunkid = objLoanApprover._ApproverEmpunkid
            'objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLoanApprover._ApproverEmpunkid
            ''S.SANDEEP [04 JUN 2015] -- END
            'txtApprover.Tag = objLoanApprover._ApproverEmpunkid
            'txtApprover.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " - " & objLoanLevel._lnLevelname

            If objLoanApprover._IsExternalApprover = True Then
                Dim objUser As New clsUserAddEdit
                objUser._Userunkid = objLoanApprover._ApproverEmpunkid
                Dim strAppName As String = objUser._Username & " - " & objLoanLevel._lnLevelname
                If strAppName.Trim.Length > 0 Then
                    txtApprover.Text = strAppName
                Else
                    txtApprover.Text = objUser._Username
                End If
                txtApprover.Tag = objUser._Userunkid
                objUser = Nothing
            Else
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLoanApprover._ApproverEmpunkid
            txtApprover.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname & " - " & objLoanLevel._lnLevelname
                txtApprover.Tag = objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            End If
            'Nilay (01-Mar-2016) -- End

            objLoanLevel = Nothing
            objLoanApprover = Nothing
            objEmployee = Nothing

            radLoan.Checked = objLoanapplication._Isloan
            radLoan_CheckedChanged(New Object(), New EventArgs())
            radAdvance.Checked = Not objLoanapplication._Isloan
            txtExternalEntity.Text = objLoanapplication._External_Entity_Name
            cboLoanScheme.SelectedValue = objLoanapplication._Loanschemeunkid

            'Nilay (21-Oct-2015) -- Start
            'ENHANCEMENT : NEW LOAN Given By Rutta
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran
            Dim dsScale As DataSet

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim intFirstOpenPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open, _
            '                                                                 FinancialYear._Object._YearUnkid, False, True)

            Dim intFirstOpenPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open, _
                                                                             False, True)
            'S.SANDEEP [04 JUN 2015] -- END
            
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intFirstOpenPeriodId

            dsScale = objMaster.Get_Current_Scale("Scale", objLoanapplication._Employeeunkid, objPeriod._End_Date.Date)
            If dsScale IsNot Nothing AndAlso dsScale.Tables("Scale").Rows.Count > 0 Then
                txtBasicSal.Text = Format(CDec(dsScale.Tables("Scale").Rows(0)("newscale")), GUI.fmtCurrency)
            End If
            'Nilay (21-Oct-2015) -- End

            'Sohail (15 May 2018) -- Start
            'CCK Enhancement - Ref # 179 : On loan approval screen on MSS, not all approvers should see the basic salary of the staff. in 72.1.
            lblBasicSal.Visible = User._Object.Privilege._AllowTo_View_Scale
            txtBasicSal.Visible = User._Object.Privilege._AllowTo_View_Scale
            'Sohail (15 May 2018) -- End

            If mintPendingloantranunkid > 0 Then
                objLoanApproval._Pendingloantranunkid = mintPendingloantranunkid

                txtLoanAmt.Text = Format(objLoanApproval._Loan_Amount, GUI.fmtCurrency)
                'Nilay (15-Dec-2015) -- Start
                txtAppliedAmnt.Text = Format(CDec(objLoanapplication._Loan_Amount), GUI.fmtCurrency)
                'Nilay (15-Dec-2015) -- End
                Dim objCurrency As New clsExchangeRate
                Dim dsList As DataSet = objCurrency.GetList("List", True, False, 0, objLoanApproval._Countryunkid)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    txtCurrency.Text = dsList.Tables(0).Rows(0)("currency_sign").ToString()
                    txtCurrency.Tag = CInt(dsList.Tables(0).Rows(0)("countryunkid").ToString())
                End If
                dsList = Nothing
                objCurrency = Nothing

                'Nilay (21-Oct-2015) -- Start
                'ENHANCEMENT : NEW LOAN Given By Rutta
                'nudDurationInMths.Value = objLoanApproval._Duration
                'Nilay (21-Oct-2015) -- End
                cboDeductionPeriod.SelectedValue = objLoanApproval._Deductionperiodunkid
                txtInstallmentAmt.Text = Format(objLoanApproval._Installmentamt, GUI.fmtCurrency)
                txtInstallmentAmt.Tag = objLoanApproval._Installmentamt
                txtEMIInstallments.Text = objLoanApproval._Noofinstallment.ToString()

                'Nilay (25-Jul-2016) -- Start
                'If menAction = enAction.EDIT_ONE Then
                '    cboStatus.SelectedValue = objLoanApproval._Statusunkid
                'Else
                '    cboStatus.SelectedValue = objLoanapplication._Loan_Statusunkid
                'End If
                'Nilay (25-Jul-2016) -- End

            ElseIf mintProcesspendingloanunkid > 0 Then
                'Nilay (21-Oct-2015) -- Start
                'NOTE: This block of code is no more use as now Loan Application Add/Edit and Loan Approval forms are seperated. 
                txtLoanAmt.Text = Format(objLoanapplication._Loan_Amount, GUI.fmtCurrency)
                Dim objCurrency As New clsExchangeRate
                Dim dsList As DataSet = objCurrency.GetList("List", True, False, 0, objLoanapplication._Countryunkid)
                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    txtCurrency.Text = dsList.Tables(0).Rows(0)("currency_sign").ToString()
                    txtCurrency.Tag = CInt(dsList.Tables(0).Rows(0)("countryunkid").ToString())
                End If
                dsList = Nothing
                objCurrency = Nothing

                'Nilay (21-Oct-2015) -- Start
                'ENHANCEMENT : NEW LOAN Given By Rutta
                'nudDurationInMths.Value = objLoanapplication._DurationInMonths
                'Nilay (21-Oct-2015) -- End

                cboDeductionPeriod.SelectedValue = objLoanapplication._DeductionPeriodunkid
                txtInstallmentAmt.Text = Format(objLoanapplication._InstallmentAmount, GUI.fmtCurrency)
                txtInstallmentAmt.Tag = objLoanapplication._InstallmentAmount
                txtEMIInstallments.Text = objLoanapplication._NoOfInstallment.ToString()
                'Nilay (25-Jul-2016) -- Start
                'cboStatus.SelectedValue = objLoanapplication._Loan_Statusunkid
                'Nilay (25-Jul-2016) -- End

                'Nilay (21-Oct-2015) -- End
            End If

            'Hemant (19 Jul 2024) -- Start
            'ENHANCEMENT(FSDT): A1X - 2705 : Mandatory document attachment on non-Flexcube loans with an option for the approver to download/view
            mdtFinalLoanApplicationDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.LOAN_ADVANCE, mintProcesspendingloanunkid, ConfigParameter._Object._Document_Path)
            mdtLoanApplicationDocument = mdtFinalLoanApplicationDocument.Clone()

            If mintProcesspendingloanunkid > 0 Then
                mdtLoanApplicationDocument = mdtFinalLoanApplicationDocument.Copy()
            End If

            mdtFinalLoanApplicationDocument.Rows.Clear()
            If menAction <> enAction.EDIT_ONE Then
                If mdtLoanApplicationDocument IsNot Nothing Then mdtLoanApplicationDocument.Rows.Clear()
            End If

            Call FillLoanApplicationAttachment()
            'Hemant (19 Jul 2024) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Getvalue", mstrModuleName)
        End Try
    End Sub

    Private Sub Setvalue()
        Try
            objLoanApproval._Processpendingloanunkid = mintProcesspendingloanunkid
            objLoanApproval._Employeeunkid = CInt(txtEmployee.Tag)
            objLoanApproval._Approvertranunkid = mintApproverID
            objLoanApproval._Approverempunkid = CInt(txtApprover.Tag)
            objLoanApproval._Priority = mintPriority
            objLoanApproval._Approvaldate = ConfigParameter._Object._CurrentDateAndTime
            objLoanApproval._Deductionperiodunkid = CInt(cboDeductionPeriod.SelectedValue)
            objLoanApproval._Loan_Amount = txtLoanAmt.Decimal
            objLoanApproval._Countryunkid = CInt(txtCurrency.Tag)
            'Nilay (21-Oct-2015) -- Start
            'ENHANCEMENT : NEW LOAN Given By Rutta
            'objLoanApproval._Duration = CInt(nudDurationInMths.Value)
            'Nilay (21-Oct-2015) -- End
            objLoanApproval._Installmentamt = CDec(txtInstallmentAmt.Tag)
            objLoanApproval._Noofinstallment = CInt(txtEMIInstallments.Decimal)
            objLoanApproval._Statusunkid = CInt(cboStatus.SelectedValue)
            objLoanApproval._Remark = txtRemark.Text.Trim
            objLoanApproval._Userunkid = User._Object._Userunkid
            objLoanApproval._Isvoid = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Setvalue", mstrModuleName)
        End Try
    End Sub

    'Hemant (30 Aug 2019) -- Start
    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
    Private Sub Send_Notification()
        Try
            If objEmailList.Count > 0 Then
                RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                clsApplicationIdleTimer.LastIdealTime.Stop()
                Dim objSendMail As New clsSendMail
                For Each obj In objEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendMail._UserUnkid = User._Object._Userunkid
                    objSendMail._SenderAddress = User._Object._Email
                    'Hemant (11 Jul 2022) -- Start
                    'objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
                    'Hemant (11 Jul 2022) -- Start
                    Try
                        objSendMail.SendMail(Company._Object._Companyunkid)
                    Catch ex As Exception

                    End Try
                Next
                objEmailList.Clear()
                AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
                clsApplicationIdleTimer.LastIdealTime.Start()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If objEmailList.Count > 0 Then
                objEmailList.Clear()
            End If
        End Try
    End Sub
    'Hemant (30 Aug 2019) -- End

    'Hemant (19 Jul 2024) -- Start
    'ENHANCEMENT(FSDT): A1X - 2705 : Mandatory document attachment on non-Flexcube loans with an option for the approver to download/view
    Private Sub FillLoanApplicationAttachment()
        Dim dtView As DataView
        Try
            If mdtLoanApplicationDocument Is Nothing Then Exit Sub

            dtView = New DataView(mdtLoanApplicationDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvLoanApplicationAttachment.AutoGenerateColumns = False
            colhName.DataPropertyName = "filename"
            colhSize.DataPropertyName = "filesize"
            objcolhGUID.DataPropertyName = "GUID"
            objcolhScanUnkId.DataPropertyName = "scanattachtranunkid"
            dgvLoanApplicationAttachment.DataSource = dtView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLoanApplicationAttachment", mstrModuleName)
        End Try
    End Sub
    'Hemant (19 Jul 2024) -- End


#End Region

#Region "Combobox Event"

    Private Sub cboDeductionPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDeductionPeriod.SelectedIndexChanged
        Try
            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objPeriod._Periodunkid = CInt(cboDeductionPeriod.SelectedValue)
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboDeductionPeriod.SelectedValue)
                'Nilay (10-Oct-2015) -- End
                objvalPeriodDuration.Text = Language.getMessage(mstrModuleName, 11, "Period Duration From :") & " " & objPeriod._Start_Date.Date & " " & Language.getMessage(mstrModuleName, 12, "To") & " " & objPeriod._End_Date.Date
                objPeriod = Nothing
            Else
                objvalPeriodDuration.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboDeductionPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboLoanScheme.KeyPress
        Try
            e.Handled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_KeyPress", mstrModuleName)
        End Try

    End Sub

    'Hemant (19 Jul 2024) -- Start
    'ENHANCEMENT(FSDT): A1X - 2705 : Mandatory document attachment on non-Flexcube loans with an option for the approver to download/view
    Private Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged
        Try
            If cboLoanScheme.SelectedValue IsNot Nothing AndAlso CInt(cboLoanScheme.SelectedValue) > 0 Then
                Dim objLScheme As New clsLoan_Scheme
                objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)

                mblnIsAttachmentRequired = objLScheme._IsAttachmentRequired

                If mblnIsAttachmentRequired = True Then
                    pnlScanAttachment.Visible = True
                Else
                    pnlScanAttachment.Visible = False
                End If

                objLScheme = Nothing
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (19 Jul 2024) -- End


#End Region

#Region "Textbox Events"

    'Nilay (15-Dec-2015) -- Start
    Private Sub txtLoanAmt_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLoanAmt.Validated
        Try
            RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
            If txtLoanAmt.Decimal > 0 AndAlso txtInstallmentAmt.Decimal > 0 Then
                'S.SANDEEP [09-May-2018] -- START
                'ISSUE/ENHANCEMENT : {Ref#204|#ARUTI-103}
                'txtEMIInstallments.Decimal = CInt(Math.Ceiling(txtLoanAmt.Decimal / txtInstallmentAmt.Decimal))
                txtInstallmentAmt.Decimal = CInt(Math.Ceiling(txtLoanAmt.Decimal / txtEMIInstallments.Decimal))
                'S.SANDEEP [09-May-2018] -- END
            End If
            AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtLoanAmt_Validated", mstrModuleName)
        End Try
    End Sub

    'Private Sub txtLoanAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLoanAmt.TextChanged
    '    Try
    '        RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
    '        If txtLoanAmt.Decimal > 0 AndAlso txtInstallmentAmt.Decimal > 0 Then
    '            txtEMIInstallments.Decimal = CInt(Math.Ceiling(txtLoanAmt.Decimal / txtInstallmentAmt.Decimal))
    '        End If
    '        AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtLoanAmt_TextChanged", mstrModuleName)
    '    End Try
    'End Sub
    'Nilay (15-Dec-2015) -- End

    Private Sub txtInstallmentAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            RemoveHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
            If txtLoanAmt.Decimal > 0 AndAlso txtInstallmentAmt.Decimal > 0 Then
                'Nilay (18-Nov-2015) -- Start
                'txtEMIInstallments.Decimal = CInt(txtLoanAmt.Decimal / txtInstallmentAmt.Decimal)
                If txtInstallmentAmt.Decimal > txtLoanAmt.Decimal Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Installment Amount cannot be greater than Loan Amount."), enMsgBoxStyle.Information)
                    txtInstallmentAmt.Focus()
                    Exit Sub
                Else
                    'Nilay (15-Dec-2015) -- Start
                    'txtEMIInstallments.Decimal = CInt(txtLoanAmt.Decimal / txtInstallmentAmt.Decimal)
                    txtEMIInstallments.Decimal = CInt(Math.Ceiling(txtLoanAmt.Decimal / txtInstallmentAmt.Decimal))
                    'Nilay (15-Dec-2015) -- End
                End If
                'Nilay (18-Nov-2015) -- End
            Else
                txtEMIInstallments.Decimal = 0
            End If
            txtInstallmentAmt.Tag = CDec(txtInstallmentAmt.Decimal)
            AddHandler txtEMIInstallments.TextChanged, AddressOf txtEMIInstallments_TextChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtInstallmentAmt_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtEMIInstallments_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtEMIInstallments.TextChanged
        Try
            RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            If txtLoanAmt.Decimal > 0 AndAlso txtEMIInstallments.Decimal > 0 Then
                txtInstallmentAmt.Tag = CDec(txtLoanAmt.Decimal / txtEMIInstallments.Decimal)
                txtInstallmentAmt.Text = Format(CDec(txtLoanAmt.Decimal / txtEMIInstallments.Decimal), GUI.fmtCurrency)
            Else
                txtInstallmentAmt.Tag = txtLoanAmt.Decimal
                txtInstallmentAmt.Text = Format(txtLoanAmt.Decimal, GUI.fmtCurrency)
            End If
            AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtEMIInstallments_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Radio Button Event"

    Private Sub radLoan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radLoan.CheckedChanged
        If radLoan.Checked Then
            cboLoanScheme.Enabled = True
            nudDurationInMths.Enabled = True
            txtInstallmentAmt.Enabled = True
            txtEMIInstallments.Enabled = True
        Else
            txtEMIInstallments.Enabled = False
            txtInstallmentAmt.Enabled = False
            nudDurationInMths.Enabled = False
            cboLoanScheme.Enabled = False
            cboLoanScheme.SelectedText = ""
        End If
    End Sub

#End Region

#Region "Button's Event"

    Private Sub objbtnSearchDeductionPeriod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchDeductionPeriod.Click
        Dim objFrm As New frmCommonSearch
        Try

            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboDeductionPeriod.ValueMember
                .DisplayMember = cboDeductionPeriod.DisplayMember
                .DataSource = CType(cboDeductionPeriod.DataSource, DataTable)
                'Nilay (23-Aug-2016) -- Start
                'Enhancement - Create New Loan Notification 
                .CodeMember = "code"
                'Nilay (23-Aug-2016) -- End
            End With

            If objFrm.DisplayDialog Then
                cboDeductionPeriod.SelectedValue = objFrm.SelectedValue
                cboDeductionPeriod.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchDeductionPeriod_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If IsValid() = False Then Exit Sub

            Setvalue()
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Me.Cursor = Cursors.WaitCursor
            'Shani (21-Jul-2016) -- End
            objEmailList = New List(Of clsEmailCollection) 'Hemant (30 Aug 2019)
            If menAction = enAction.ADD_ONE Then
                blnFlag = objLoanApproval.Insert(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, CInt(cboLoanScheme.SelectedValue))
            ElseIf menAction = enAction.EDIT_ONE Then
                'Pinkal (20-Sep-2022) -- Start
                'NMB Loan Module Enhancement.
                'blnFlag = objLoanApproval.Update(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, CInt(cboLoanScheme.SelectedValue))
                blnFlag = objLoanApproval.Update(Company._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                            , Company._Object._Companyunkid, ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date _
                                                                            , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp _
                                                                            , ConfigParameter._Object._IsLoanApprover_ForLoanScheme, CInt(cboLoanScheme.SelectedValue))
                'Pinkal (20-Sep-2022) -- End


            End If

            If blnFlag = False And objLoanApproval._Message <> "" Then
                eZeeMsgBox.Show(objLoanApproval._Message, enMsgBoxStyle.Information)
                'Shani (21-Jul-2016) -- Start
                'Enhancement - Create New Loan Notification 
            Else
                If blnFlag AndAlso menAction = enAction.EDIT_ONE Then
                    Dim objProcessLoan As New clsProcess_pending_loan
                    Dim oEmail As New List(Of clsEmailCollection) 'Hemant (30 Aug 2019)
                    'Nilay (10-Dec-2016) -- Start
                    'Issue #26: Setting to be included on configuration for Loan flow Approval process
                    If CBool(ConfigParameter._Object._IsSendLoanEmailFromDesktopMSS) = True Then
                    'Nilay (20-Sept-2016) -- Start
                    'Enhancement : Cancel feature for approved but not assigned loan application
                    'If CInt(cboStatus.SelectedValue) = 2 Then 'Approve
                    If CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.APPROVED Then
                        'Nilay (20-Sept-2016) -- End
                        If objLoanApproval.IsPendingLoanApplication(mintProcesspendingloanunkid, True) = False Then
                            If radLoan.Checked Then
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                    '                                      CInt(cboLoanScheme.SelectedValue), _
                                    '                                      CInt(txtEmployee.Tag), _
                                    '                                      mintPriority, _
                                    '                                      clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                    '                                      False, mintProcesspendingloanunkid.ToString, _
                                    '                                      ConfigParameter._Object._ArutiSelfServiceURL, True, enLogin_Mode.DESKTOP, 0, 0)
                                    'Hemant (30 Aug 2019) -- Start
                                    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                                    'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                    '                                                                          CInt(cboLoanScheme.SelectedValue), _
                                    '                                                                          CInt(txtEmployee.Tag), _
                                    '                                                                          mintPriority, _
                                    '                                                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                    '                                                                          False, mintProcesspendingloanunkid.ToString, _
                                    '                                                                             ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, True, enLogin_Mode.DESKTOP, 0, 0)
                                objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                      CInt(cboLoanScheme.SelectedValue), _
                                                                      CInt(txtEmployee.Tag), _
                                                                      mintPriority, _
                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                      False, mintProcesspendingloanunkid.ToString, _
                                                                              ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, True, enLogin_Mode.DESKTOP, 0, 0, _
                                                                              False, oEmail)
                                    objEmailList.AddRange(oEmail)
                                    'Hemant (30 Aug 2019) -- End
                                    'Sohail (30 Nov 2017) -- End
                            Else
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                    '                                      CInt(cboLoanScheme.SelectedValue), _
                                    '                                      CInt(txtEmployee.Tag), _
                                    '                                      mintPriority, _
                                    '                                      clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                    '                                      False, mintProcesspendingloanunkid.ToString, _
                                    '                                      ConfigParameter._Object._ArutiSelfServiceURL, True, enLogin_Mode.DESKTOP, 0, 0)
                                    'Hemant (30 Aug 2019) -- Start
                                    'ISSUE#0004110(ZURI) :  Error on global assigning loans..
                                    'objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                    '                                      CInt(cboLoanScheme.SelectedValue), _
                                    '                                      CInt(txtEmployee.Tag), _
                                    '                                      mintPriority, _
                                    '                                      clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                    '                                      False, mintProcesspendingloanunkid.ToString, _
                                    '                                         ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, True, enLogin_Mode.DESKTOP, 0, 0)
                                objProcessLoan.Send_Notification_Approver(ConfigParameter._Object._IsLoanApprover_ForLoanScheme, _
                                                                      CInt(cboLoanScheme.SelectedValue), _
                                                                      CInt(txtEmployee.Tag), _
                                                                      mintPriority, _
                                                                      clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                      False, mintProcesspendingloanunkid.ToString, _
                                                                          ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, True, enLogin_Mode.DESKTOP, 0, 0, _
                                                                          False, oEmail)
                                    objEmailList.AddRange(oEmail)
                                    'Hemant (30 Aug 2019) -- End                                    
                                    'Sohail (30 Nov 2017) -- End
                            End If
                            
                        Else 'Assign

                                'Nilay (08-Dec-2016) -- Start
                                'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
                                'If radLoan.Checked Then
                                'Nilay (08-Dec-2016) -- End

                                'Nilay (08-Dec-2016) -- Start
                                'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process	
                                If radLoan.Checked Then
                                    'Nilay (08-Dec-2016) -- End

                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Employee(CInt(txtEmployee.Tag), mintProcesspendingloanunkid, _
                                    '                                      clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                    '                                      clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                    '                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                                    '                                          enLogin_Mode.DESKTOP, 0, 0, False) 'Nilay (27-Dec-2016) -- [False]
                                objProcessLoan.Send_Notification_Employee(CInt(txtEmployee.Tag), mintProcesspendingloanunkid, _
                                                                          clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , _
                                                                              enLogin_Mode.DESKTOP, 0, 0, False)
                                    'Sohail (30 Nov 2017) -- End
                            Else
                                    'Sohail (30 Nov 2017) -- Start
                                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                    'objProcessLoan.Send_Notification_Employee(CInt(txtEmployee.Tag), mintProcesspendingloanunkid, _
                                    '                                          clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                    '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                    '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), , _
                                    '                                          enLogin_Mode.DESKTOP, 0, 0, False) 'Nilay (27-Dec-2016) -- [False]
                                    objProcessLoan.Send_Notification_Employee(CInt(txtEmployee.Tag), mintProcesspendingloanunkid, _
                                                                              clsProcess_pending_loan.enNoticationLoanStatus.APPROVE, _
                                                                              clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, , _
                                                                              enLogin_Mode.DESKTOP, 0, 0, False)
                                    'Sohail (30 Nov 2017) -- End
                            End If

                                'Nilay (23-Aug-2016) -- Start
                                'Enhancement - Create New Loan Notification 
                                objProcessLoan.Send_Notification_Assign(CInt(txtEmployee.Tag), _
                                                                        FinancialYear._Object._YearUnkid, _
                                                                        ConfigParameter._Object._ArutiSelfServiceURL, _
                                                                        mintProcesspendingloanunkid, _
                                                                        FinancialYear._Object._DatabaseName, _
                                                                        Company._Object._Companyunkid, _
                                                                        eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                        ConfigParameter._Object._UserAccessModeSetting, _
                                                                        ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                        False, enLogin_Mode.DESKTOP, 0, 0, , _
                                                                        ConfigParameter._Object._Notify_LoanAdvance_Users)
                                'Hemant (30 Aug 2019) -- [ConfigParameter._Object._Notify_LoanAdvance_Users]
                                'Nilay (23-Aug-2016) -- End
                        End If
                        'Nilay (20-Sept-2016) -- Start
                        'Enhancement : Cancel feature for approved but not assigned loan application
                        'ElseIf CInt(cboStatus.SelectedValue) = 3 Then ' Reject
                    ElseIf CInt(cboStatus.SelectedValue) = enLoanApplicationStatus.REJECTED Then
                        'Nilay (20-Sept-2016) -- End
                        If radLoan.Checked Then
                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objProcessLoan.Send_Notification_Employee(CInt(txtEmployee.Tag), mintProcesspendingloanunkid, _
                                '                                          clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                '                                          txtRemark.Text, enLogin_Mode.DESKTOP, 0, 0)
                                objProcessLoan.Send_Notification_Employee(CInt(txtEmployee.Tag), mintProcesspendingloanunkid, _
                                                                          clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                                                          clsProcess_pending_loan.enApproverEmailType.Loan_Approver, _
                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, _
                                                                          txtRemark.Text, enLogin_Mode.DESKTOP, 0, 0)
                                'Sohail (30 Nov 2017) -- End
                        Else
                                'Sohail (30 Nov 2017) -- Start
                                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                                'objProcessLoan.Send_Notification_Employee(CInt(txtEmployee.Tag), mintProcesspendingloanunkid, _
                                '                                          clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                '                                          clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                '                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                '                                          txtRemark.Text, enLogin_Mode.DESKTOP, 0, 0)
                                objProcessLoan.Send_Notification_Employee(CInt(txtEmployee.Tag), mintProcesspendingloanunkid, _
                                                                          clsProcess_pending_loan.enNoticationLoanStatus.REJECT, _
                                                                          clsProcess_pending_loan.enApproverEmailType.Loan_Advance, _
                                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, _
                                                                          txtRemark.Text, enLogin_Mode.DESKTOP, 0, 0)
                                'Sohail (30 Nov 2017) -- End
                        End If

            End If
                End If
                    'Nilay (10-Dec-2016) -- End

                    'Hemant (18 Aug 2023) -- Start
                    'ENHANCEMENT(ZSSF): A1X-1194 - Real time posting of approved loans and leave expenses to Navision
                    If ConfigParameter._Object._EFTIntegration = enEFTIntegration.DYNAMICS_NAVISION Then
                        objProcessLoan._Processpendingloanunkid = mintProcesspendingloanunkid
                        If objProcessLoan._Loan_Statusunkid = enLoanApplicationStatus.APPROVED Then
                            Dim objPeriod As New clscommom_period_Tran
                            
                            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboDeductionPeriod.SelectedValue)
                            objProcessLoan.SendToDynamicNavision(mintProcesspendingloanunkid, User._Object._Userunkid, objPeriod._Start_Date, objPeriod._End_Date, _
                                                                   ConfigParameter._Object._SQLDataSource, _
                                                                   ConfigParameter._Object._SQLDatabaseName, _
                                                                   ConfigParameter._Object._SQLDatabaseOwnerName, _
                                                                   ConfigParameter._Object._SQLUserName, _
                                                                   ConfigParameter._Object._SQLUserPassword, _
                                                                   FinancialYear._Object._FinancialYear_Name)

                            objPeriod = Nothing
                        End If

                    End If
                    'Hemant (18 Aug 2023) -- End
                End If
                'Shani (21-Jul-2016) -- End
            End If

            mblnCancel = False
            mintPendingloantranunkid = objLoanApproval._Pendingloantranunkid
            Me.Close()

            'Hemant (30 Aug 2019) -- Start
            'ISSUE#0004110(ZURI) :  Error on global assigning loans..
            trd = New Thread(AddressOf Send_Notification)
            trd.IsBackground = True
            trd.Start()
            'Hemant (30 Aug 2019) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            'Shani (21-Jul-2016) -- Start
            'Enhancement - Create New Loan Notification 
            Me.Cursor = Cursors.Default
            'Shani (21-Jul-2016) -- End
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Gridview's Event"

    'Hemant (19 Jul 2024) -- Start
    'ENHANCEMENT(FSDT): A1X - 2705 : Mandatory document attachment on non-Flexcube loans with an option for the approver to download/view
    Private Sub dgvLoanApplicationAttachment_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLoanApplicationAttachment.CellContentClick
        Try
            Cursor.Current = Cursors.WaitCursor
            If e.RowIndex >= 0 Then
                Dim xrow() As DataRow

                If CInt(dgvLoanApplicationAttachment.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) > 0 Then
                    xrow = mdtLoanApplicationDocument.Select("scanattachtranunkid = " & CInt(dgvLoanApplicationAttachment.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) & "")
                Else
                    xrow = mdtLoanApplicationDocument.Select("GUID = '" & dgvLoanApplicationAttachment.Rows(e.RowIndex).Cells(objcolhGUID.Index).Value.ToString & "'")
                End If

                If e.ColumnIndex = objcolhDownload.Index Then
                    Dim xPath As String = ""

                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("file_path").ToString
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If

                    Dim finfo As New IO.FileInfo(xPath)

                    sfdAttachment.Filter = "File Format (" & finfo.Extension & ")|*" & finfo.Extension & ""
                    sfdAttachment.FileName = xrow(0).Item("filename").ToString

                    If sfdAttachment.ShowDialog = Windows.Forms.DialogResult.OK Then

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            Dim strError As String = ""
                            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                            If blnIsIISInstalled Then

                                Dim filebytes As Byte() = clsFileUploadDownload.DownloadFile(xrow(0).Item("filepath").ToString, xrow(0).Item("fileuniquename").ToString, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)

                                If strError <> "" Then
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.OkOnly)
                                    Exit Sub
                                End If
                                If filebytes IsNot Nothing Then
                                    Dim ms As New MemoryStream(filebytes)
                                    Dim fs As New FileStream(sfdAttachment.FileName, FileMode.Create)
                                    ms.WriteTo(fs)

                                    ms.Close()
                                    fs.Close()
                                    fs.Dispose()
                                Else
                                    If System.IO.File.Exists(xrow(0).Item("filepath").ToString) Then
                                        xPath = xrow(0).Item("filepath").ToString

                                        Dim strFileUniqueName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & "_" & DateTime.Now.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(CStr(xrow(0).Item("filepath").ToString))

                                        If clsFileUploadDownload.UploadFile(xrow(0).Item("filepath").ToString, mstrFolderName, strFileUniqueName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then

                                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                            Exit Try
                                        Else
                                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                                strPath += "/"
                                            End If
                                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileUniqueName
                                            xrow(0).Item("AUD") = "U"
                                            xrow(0).Item("fileuniquename") = strFileUniqueName
                                            xrow(0).Item("filepath") = strPath
                                            xrow(0).Item("filesize") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).Item("filesize_kb") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).AcceptChanges()
                                            Dim objScanAttach As New clsScan_Attach_Documents
                                            objScanAttach._Datatable = mdtLoanApplicationDocument
                                            objScanAttach.InsertUpdateDelete_Documents()
                                            If objDocument._Message <> "" Then
                                                objScanAttach = Nothing
                                                eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
                                                Exit Try
                                            End If
                                            objScanAttach = Nothing
                                            xrow(0).Item("AUD") = ""
                                            xrow(0).AcceptChanges()
                                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                                        End If
                                    Else
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "sorry, file you are trying to access does not exists on Aruti self service application folder."), enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                End If
                            Else
                                IO.File.Copy(xPath, sfdAttachment.FileName, True)
                            End If
                        Else
                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvLoanApplicationAttachment_CellContentClick :", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
    'Hemant (19 Jul 2024) -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.gbProcessPendingLoanInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbProcessPendingLoanInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbProcessPendingLoanInfo.Text = Language._Object.getCaption(Me.gbProcessPendingLoanInfo.Name, Me.gbProcessPendingLoanInfo.Text)
			Me.lblDuration.Text = Language._Object.getCaption(Me.lblDuration.Name, Me.lblDuration.Text)
			Me.lblDeductionPeriod.Text = Language._Object.getCaption(Me.lblDeductionPeriod.Name, Me.lblDeductionPeriod.Text)
			Me.lblEMIInstallments.Text = Language._Object.getCaption(Me.lblEMIInstallments.Name, Me.lblEMIInstallments.Text)
			Me.lblEMIAmount.Text = Language._Object.getCaption(Me.lblEMIAmount.Name, Me.lblEMIAmount.Text)
			Me.lblApproverRemarks.Text = Language._Object.getCaption(Me.lblApproverRemarks.Name, Me.lblApproverRemarks.Text)
			Me.lblApplicationDate.Text = Language._Object.getCaption(Me.lblApplicationDate.Name, Me.lblApplicationDate.Text)
			Me.lblApplicationNo.Text = Language._Object.getCaption(Me.lblApplicationNo.Name, Me.lblApplicationNo.Text)
			Me.lblAmt.Text = Language._Object.getCaption(Me.lblAmt.Name, Me.lblAmt.Text)
			Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
			Me.radAdvance.Text = Language._Object.getCaption(Me.radAdvance.Name, Me.radAdvance.Text)
			Me.radLoan.Text = Language._Object.getCaption(Me.radLoan.Name, Me.radLoan.Text)
			Me.lblLoanAdvance.Text = Language._Object.getCaption(Me.lblLoanAdvance.Name, Me.lblLoanAdvance.Text)
			Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
			Me.lblStatus.Text = Language._Object.getCaption(Me.lblStatus.Name, Me.lblStatus.Text)
			Me.lblExternalEntity.Text = Language._Object.getCaption(Me.lblExternalEntity.Name, Me.lblExternalEntity.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.lblBasicSal.Text = Language._Object.getCaption(Me.lblBasicSal.Name, Me.lblBasicSal.Text)
            Me.lblAppliedAmnt.Text = Language._Object.getCaption(Me.lblAppliedAmnt.Name, Me.lblAppliedAmnt.Text)
			Me.lblDocumentType.Text = Language._Object.getCaption(Me.lblDocumentType.Name, Me.lblDocumentType.Text)
			Me.colhName.HeaderText = Language._Object.getCaption(Me.colhName.Name, Me.colhName.HeaderText)
			Me.colhSize.HeaderText = Language._Object.getCaption(Me.colhSize.Name, Me.colhSize.HeaderText)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
			Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
			Language.setMessage(mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue.")
			Language.setMessage(mstrModuleName, 3, "Loan/Advance Amount cannot be blank. Loan/Advance Amount is compulsory information.")
			Language.setMessage(mstrModuleName, 4, "Currency is compulsory information.Please select currency.")
			Language.setMessage(mstrModuleName, 6, "Deduction Period is compulsory information.Please select deduction period.")
			Language.setMessage(mstrModuleName, 7, "Installment Amount cannot be 0.Please define installment amount greater than 0.")
			Language.setMessage(mstrModuleName, 8, "No of Installment cannot be 0.Please define No of Installment greater than 0.")
			Language.setMessage(mstrModuleName, 10, "Remark cannot be blank.Remark is compulsory information.")
			Language.setMessage(mstrModuleName, 11, "Period Duration From :")
			Language.setMessage(mstrModuleName, 12, "To")
                        Language.setMessage(mstrModuleName, 13, "Installment Amount cannot be greater than Loan Amount.")
                        Language.setMessage(mstrModuleName, 14, "Please select atleast one Status from the list to perform further operation.")
			Language.setMessage(mstrModuleName, 15, "Installment months cannot be greater than")
			Language.setMessage(mstrModuleName, 16, " for")
			Language.setMessage(mstrModuleName, 17, " Scheme.")
			Language.setMessage(mstrModuleName, 18, "sorry, file you are trying to access does not exists on Aruti self service application folder.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class