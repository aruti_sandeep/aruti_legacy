﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System.IO

#End Region

Public Class frmOTPValidator

    Private ReadOnly mstrModuleName As String = "frmOTPValidator"
    Private mblnCancel As Boolean = True
    Dim ts As New Timers.Timer
    Dim objTOTPHelper As clsTOTPHelper = Nothing
    Dim mstrTOTPCode As String = ""
    Dim Counter As Integer = 0
    Dim mstrSecretKey As String = ""
    Dim mintEmployeeId As Integer = 0
    Dim mstrEmployeeName As String = ""
    Dim mstrEmpMobileNo As String = ""
    Dim mstrEmployeeEmail As String = ""


#Region " Display Dialog "

    Public Function displayDialog(ByVal _SecretKey As String, ByVal _EmployeeId As Integer, ByVal _EmployeeName As String, ByVal _EmpMobileNo As String, ByVal _EmployeeEmail As String) As Boolean
        Try
            mstrSecretKey = _SecretKey
            mintEmployeeId = _EmployeeId
            mstrEmployeeName = _EmployeeName
            mstrEmpMobileNo = _EmpMobileNo
            mstrEmployeeEmail = _EmployeeEmail
            Me.ShowDialog()
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Forms Events"

    Private Sub frmOTPValidator_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            clsApplicationIdleTimer.LastIdealTime.Enabled = False
            frmNewMDI.tmrReminder.Enabled = False

            objTOTPHelper = New clsTOTPHelper(Company._Object._Companyunkid)

            Counter = objTOTPHelper._TimeStepSeconds
            ts.Interval = 975
            ts.Enabled = False
            AddHandler ts.Elapsed, AddressOf timer_Elapsed

            mstrTOTPCode = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
            Dim mstrError As String = SendOTP()
            If mstrError.Trim.Length <= 0 Then
                ts.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOTPValidator_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmOTPValidator_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            ts.Dispose()
            clsApplicationIdleTimer.LastIdealTime.Enabled = True
            frmNewMDI.tmrReminder.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmOTPValidator_FormClosing", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Function SendOTP() As String
        Dim objLoan As New clsProcess_pending_loan
        Dim objSendMail As New clsSendMail
        Dim mstrError As String = ""
        Try
            'objSendMail._ToEmail = ConfigParameter._Object._SMSGatewayEmail
            objSendMail._ToEmail = mstrEmployeeEmail
            objSendMail._Subject = mstrEmpMobileNo
            objSendMail._Form_Name = ""
            objSendMail._LogEmployeeUnkid = -1
            objSendMail._OperationModeId = enLogin_Mode.DESKTOP
            objSendMail._UserUnkid = User._Object._Userunkid
            objSendMail._SenderAddress = User._Object._Email
            objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
            objSendMail._Message = objLoan.SetSMSForLoanApplication(mstrEmployeeName, mstrTOTPCode, Counter, False)
            mstrError = objSendMail.SendMail(Company._Object._Companyunkid)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SendOTP", mstrModuleName)
            mstrError = ex.Message.ToString()
        Finally
            objSendMail = Nothing
            objLoan = Nothing
        End Try
        Return mstrError
    End Function

    Private Sub InsertAttempts(ByRef mblnprompt As Boolean)
        Dim objAttempts As New clsloantotpattempts_tran
        Try
            Dim xAttempts As Integer = 0
            Dim xAttemptsunkid As Integer = 0
            Dim xOTPRetrivaltime As DateTime = Nothing

            Dim dsAttempts As DataSet = objAttempts.GetList(ConfigParameter._Object._CurrentDateAndTime.Date, User._Object._Userunkid, 0)

            If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                xAttempts = CInt(dsAttempts.Tables(0).Rows(0)("attempts"))
                xAttemptsunkid = CInt(dsAttempts.Tables(0).Rows(0)("attemptsunkid"))

                'If xAttempts >= ConfigParameter._Object._MaxTOTPUnSuccessfulAttempts Then
                '    objAttempts._Attemptsunkid = xAttemptsunkid
                '    objAttempts._Otpretrivaltime = ConfigParameter._Object._CurrentDateAndTime.AddMinutes(ConfigParameter._Object._TOTPRetryAfterMins)
                '    objAttempts._loginemployeeunkid = -1
                '    objAttempts._Userunkid = User._Object._Userunkid
                '    objAttempts._Ip = getIP()
                '    objAttempts._Machine_Name = getHostName()
                '    objAttempts._FormName = mstrModuleName
                '    objAttempts._Isweb = False
                '    If objAttempts.Update() = False Then
                '        eZeeMsgBox.Show(objAttempts._Message, enMsgBoxStyle.Information)
                '        Exit Sub
                '    End If

                '    If objAttempts._Otpretrivaltime > ConfigParameter._Object._CurrentDateAndTime Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You cannot save this loan application.Reason :  As You reached Maximum unsuccessful attempts.Please try after") & " " & ConfigParameter._Object._TOTPRetryAfterMins & " " & Language.getMessage(mstrModuleName, 4, "mintues") & ".", enMsgBoxStyle.Information)
                '        objAttempts = Nothing
                '        mblnprompt = True
                '        btnVerify.Enabled = False
                '        ts.Stop()
                '        ts.Enabled = False
                '        Exit Sub
                '    End If
                'End If

                If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                    xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))

                    If xOTPRetrivaltime > ConfigParameter._Object._CurrentDateAndTime Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "You cannot save this loan application.Reason :  As You reached Maximum unsuccessful attempts.Please try after") & " " & ConfigParameter._Object._TOTPRetryAfterMins & " " & Language.getMessage(mstrModuleName, 4, "mintues") & ".", enMsgBoxStyle.Information)
                        objAttempts = Nothing
                        mblnprompt = True
                        btnVerify.Enabled = False
                        ts.Stop()
                        ts.Enabled = False
                        Exit Sub
                    End If
                End If

                If xAttempts >= ConfigParameter._Object._MaxTOTPUnSuccessfulAttempts Then
                    xAttempts = 1
                    xAttemptsunkid = 0
                Else
                    xAttempts = xAttempts + 1
                End If
            Else
                xAttempts = 1
            End If


            objAttempts._Attemptsunkid = xAttemptsunkid
            objAttempts._Attempts = xAttempts
            objAttempts._Attemptdate = ConfigParameter._Object._CurrentDateAndTime
            objAttempts._Otpretrivaltime = Nothing
            objAttempts._loginemployeeunkid = -1
            objAttempts._Userunkid = User._Object._Userunkid
            objAttempts._Ip = getIP()
            objAttempts._Machine_Name = getHostName()
            objAttempts._FormName = mstrModuleName
            objAttempts._Isweb = False

            If xAttemptsunkid <= 0 Then
                If objAttempts.Insert() = False Then
                    eZeeMsgBox.Show(objAttempts._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            Else
                If xAttempts >= ConfigParameter._Object._MaxTOTPUnSuccessfulAttempts Then
                    objAttempts._Otpretrivaltime = ConfigParameter._Object._CurrentDateAndTime.AddMinutes(ConfigParameter._Object._TOTPRetryAfterMins)
                End If
                If objAttempts.Update() = False Then
                    eZeeMsgBox.Show(objAttempts._Message, enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            objAttempts = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAttempts", mstrModuleName)
        Finally
            objAttempts = Nothing
        End Try
    End Sub

#End Region

#Region "Button Events"

    Private Sub btnVerify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerify.Click
        Try
            If txtVerifyOTP.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "OTP is compulsory information.Please Enter OTP."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                txtVerifyOTP.Focus()
                Exit Sub
            End If

            If objTOTPHelper.ValidateTotpCode(mstrSecretKey, txtVerifyOTP.Text.Trim) = False Then
                Dim mblnprompt As Boolean = False
                InsertAttempts(mblnprompt)
                If mblnprompt = False Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Invalid OTP Code. Please try again."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                End If
                Exit Sub
            End If
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnVerify_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub LnkSendCodeAgain_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LnkSendCodeAgain.LinkClicked
        Try
            LnkSendCodeAgain.Enabled = False
            txtVerifyOTP.Text = ""
            txtVerifyOTP.ReadOnly = False
            txtVerifyOTP.Enabled = True
            btnVerify.Enabled = True
            Counter = 0
            Counter = objTOTPHelper._TimeStepSeconds
            mstrTOTPCode = objTOTPHelper.GenerateTOTPCode(mstrSecretKey)
            Dim mstrError As String = SendOTP()
            If mstrError.Trim.Length <= 0 Then
                ts.Enabled = True
            End If
            'txtVerifyOTP.Text = mstrTOTPCode
            'ts.Enabled = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "LnkSendCodeAgain_LinkClicked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            txtVerifyOTP.Text = ""
            LblMessage.Text = ""
            ts.Enabled = False
            ts.Stop()
            Counter = objTOTPHelper._TimeStepSeconds
            LblSeconds.Text = ""
            LnkSendCodeAgain.Enabled = False
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "Timer Events"

    Private Sub timer_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs)
        ts.Enabled = False
        Try
            Dim t As TimeSpan = TimeSpan.FromSeconds(Counter)
            LblSeconds.Text = t.Minutes.ToString("#00") & ":" & t.Seconds.ToString("#00")
            If Counter < 0 Then
                txtVerifyOTP.ReadOnly = True
                LnkSendCodeAgain.Enabled = True
                ts.Enabled = False
                Counter = 0
                t = TimeSpan.FromSeconds(Counter)
                LblSeconds.Text = t.Minutes.ToString("#00") & ":" & t.Seconds.ToString("#00")
                btnVerify.Enabled = False
                Exit Sub
                'Else
                '    If objTOTPHelper.ValidateTotpCode(mstrSecretKey, txtVerifyOTP.Text.Trim) Then
                '        LblMessage.Text = "Verify Successful."
                '        LblMessage.ForeColor = Color.Green
                '    Else
                '        LblMessage.Text = "Invalid Code."
                '        LblMessage.ForeColor = Color.Red
                '        Counter = 0
                '        t = TimeSpan.FromSeconds(Counter)
                '        LblSeconds.Text = t.Minutes.ToString("#00") & ":" & t.Seconds.ToString("#00")
                '        ts.Enabled = False
                '        LnkSendCodeAgain.Enabled = True
                '        Exit Sub
                '    End If
            End If
            Counter -= 1
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "timer_Elapsed", mstrModuleName)
        Finally
            If ts.Enabled = False Then ts.Enabled = True
        End Try
    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.gbTOTP.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbTOTP.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnVerify.GradientBackColor = GUI._ButttonBackColor
            Me.btnVerify.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbTOTP.Text = Language._Object.getCaption(Me.gbTOTP.Name, Me.gbTOTP.Text)
            Me.LblSeconds.Text = Language._Object.getCaption(Me.LblSeconds.Name, Me.LblSeconds.Text)
            Me.LnkSendCodeAgain.Text = Language._Object.getCaption(Me.LnkSendCodeAgain.Name, Me.LnkSendCodeAgain.Text)
            Me.LblOTP.Text = Language._Object.getCaption(Me.LblOTP.Name, Me.LblOTP.Text)
            Me.btnVerify.Text = Language._Object.getCaption(Me.btnVerify.Name, Me.btnVerify.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.LblMessage.Text = Language._Object.getCaption(Me.LblMessage.Name, Me.LblMessage.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "OTP is compulsory information.Please Enter OTP.")
            Language.setMessage(mstrModuleName, 2, "Invalid OTP Code. Please try again.")
            Language.setMessage(mstrModuleName, 3, "You cannot save this loan application.Reason :  As You reached Maximum unsuccessful attempts.Please try after")
            Language.setMessage(mstrModuleName, 4, "mintues")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class