﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
Imports System.Threading
Imports System.IO
Imports System.Text

#End Region

Public Class frmFlexcubeLoanApplication_AddEdit

#Region "Private Variables"

    Private ReadOnly mstrModuleName As String = "frmFlexcubeLoanApplication_AddEdit"
    Private objProcesspendingloan As clsProcess_pending_loan
    Private mintProcessPendingLoanUnkid As Integer = -1
    Private menAction As enAction = enAction.ADD_ONE
    Private mblnCancel As Boolean = True
    Private mintNewStatusId As Integer = 0
    Private mstrSearchText As String = String.Empty
    Private mdtPeriodStart As Date
    Private mdtPeriodEnd As Date
    Private mstrApplicationNo As String = String.Empty
    Private mdtApplcationDate As Date
    Private mstrMaxInstallmentHeadFormula As String = String.Empty
    Private mintMaxNoOfInstallmentLoanScheme As Integer = 1
    Private mdecMaxLoanAmountDefined As Decimal = 0
    Private mdecEstimateMaxInstallmentAmt As Decimal = 0
    Private mintMaxLoanCalcTypeId As Integer = 0
    Private mstrMaxLoanAmountHeadFormula As String = String.Empty

    Private mdecBaseExRate As Decimal
    Private mdecPaidExRate As Decimal
    Private mintPaidCurrId As Integer
    Private mstrBaseCurrSign As String
    Private mintBaseCurrId As Integer = 0

    Private mintLastCalcTypeId As Integer = 0
    Private mdecInstallmentAmount As Decimal = 0
    Private mintNoOfInstallment As Integer = 1
    Private mblnIsAttachmentRequired As Boolean = False
    Private mstrDocumentTypeIDs As String = String.Empty
    Private mintDeductionPeriodUnkId As Integer = 0
    Private mdecMaxLoanAmount As Decimal = 0

    Private mdtLoanApplicationDocument As DataTable
    Private objDocument As clsScan_Attach_Documents
    Private mstrFolderName As String = ""

    Private trd As Thread
    Private objEmailList As New List(Of clsEmailCollection)

    Private mdecMinLoanAmount As Decimal = 0

    'Pinkal (20-Sep-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mblnRequiredReportingToApproval As Boolean = False
    'Pinkal (20-Sep-2022) -- End

    'Pinkal (12-Oct-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mblnSkipApprovalFlow As Boolean = False
    'Pinkal (12-Oct-2022) -- End

    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    'Private mintOtherDocumentunkid As Integer = -1
    Private mstrOtherDocumentIds As String = ""
    'Pinkal (23-Nov-2022) -- End


    Private mdtFinalLoanApplicationDocument As DataTable
    Private mdtOtherLoanApplicationDocument As DataTable
    Dim objLoamEmailList As New List(Of clsEmailCollection)
    'Hemant (27 Oct 2022) -- End

    'Pinkal (10-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mblnLoanApproval_DailyReminder As Boolean = False
    Private mintEscalationDays As Integer = 0
    'Pinkal (10-Nov-2022) -- End

    'START REMOVE
    Dim mstrLoanSchemeCode As String = ""
    'END REMOVE
    'Hemant (22 Dec 2022) -- Start
    Private mintCurrentFlexcubeLoanCount As Integer
    'Hemant (22 Dec 2022) -- End


    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .
    Dim mstrSecretKey As String = ""
    Dim mstrEmpMobileNo As String = ""
    Dim mstrEmployeeName As String = ""
    Dim mstrEmployeeEmail As String = ""
    'Pinkal (02-Jun-2023) -- End

    'Pinkal (11-Mar-2024) -- Start
    '(A1X-2505) NMB - Credit card integration.
    Dim mdecCreditCardAmountOnExposure As Decimal = 0
    'Pinkal (11-Mar-2024) -- End
    'Hemant (18 Mar 2024) -- Start
    'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
    Dim mstrCompanyGroupName As String = ""
    'Hemant (18 Mar 2024) -- End
    'Hemant (22 Nov 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
    Dim mdecActualSalaryAdvancePendingPrincipalAmt As Decimal = 0
    Dim mdecFinalSalaryAdvancePendingPrincipalAmt As Decimal = 0
    'Hemant (22 Nov 2024) -- End
#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal intNewStatusId As Integer = 0) As Boolean
        Try
            mintProcessPendingLoanUnkid = intUnkId
            menAction = eAction
            mintNewStatusId = intNewStatusId

            Me.ShowDialog()

            intUnkId = mintProcessPendingLoanUnkid
            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Private Methods"

    Private Sub SetColor()
        Try
            cboEmpName.BackColor = GUI.ColorComp
            cboLoanScheme.BackColor = GUI.ColorComp
            cboLoanSchemeCategory.BackColor = GUI.ColorComp
            txtPurposeOfCredit.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Public Sub FillCombo()
        Dim dsCombos As New DataSet
        Dim objEmployee As New clsEmployee_Master
        Dim objLoanScheme As New clsLoan_Scheme
        Dim objLoan_Advance As New clsLoan_Advance

        Try

            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, _
                                                 ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                 "Employee", True)

            With cboEmpName
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Employee")
                .SelectedValue = 0
            End With

            Call SetDefaultSearchText(cboEmpName)

            dsCombos = objLoanScheme.getComboList(True, "LoanScheme", -1, " ISNULL(ispostingtoflexcube,0) = 1 ", False)
            With cboLoanScheme
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("LoanScheme")
                .SelectedValue = 0
            End With

            Call SetDefaultSearchText(cboLoanScheme)

            Dim objCurrency As New clsExchangeRate
            dsCombos = objCurrency.getComboList("Currency", True)
            With cboCurrency
                .ValueMember = "countryunkid"
                .DisplayMember = "currency_sign"
                .DataSource = dsCombos.Tables("Currency")

            End With

            Dim dtTable As DataTable = New DataView(dsCombos.Tables("Currency"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
            If dtTable.Rows.Count > 0 Then
                mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                mintBaseCurrId = CInt(dtTable.Rows(0).Item("countryunkid"))
            Else
                mstrBaseCurrSign = ""
                mintBaseCurrId = 0
            End If
            If mintBaseCurrId > 0 Then
                cboCurrency.SelectedValue = mintBaseCurrId
            Else
                If cboCurrency.Items.Count > 0 Then cboCurrency.SelectedIndex = 0
            End If

            dsCombos = objLoan_Advance.GetLoan_Scheme_Categories("List")

            With cboLoanSchemeCategory
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables(0)
                .SelectedValue = 0
            End With

            dsCombos = objLoan_Advance.GetLoanCalculationTypeList("List", True)
            With cboLoanCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With

            dsCombos = objLoan_Advance.GetLoan_Interest_Calculation_Type("List", True)
            With cboInterestCalcType
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("List")
                .SelectedValue = 0
            End With



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objEmployee = Nothing
            objLoanScheme = Nothing
            objLoan_Advance = Nothing
        End Try
    End Sub

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 41, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    'Function GetMaxLoanAmountDefinedLoanScheme(ByVal MaxLoanAmountDefined As Decimal) As Decimal
    '    Dim objLoan_Advance As New clsLoan_Advance
    '    Dim decMaxLoanAmount As Decimal = 0
    '    Try
    '        Dim decInstallmentAmount As Decimal = 0
    '        Dim decInstrAmount As Decimal = 0
    '        Dim decTotInstallmentAmount As Decimal = 0
    '        Dim decTotIntrstAmount As Decimal = 0
    '        Dim dtEndDate As Date = DateAdd(DateInterval.Month, nudDuration.Value, Now.Date)
    '        Dim dtFPeriodStart As Date = dtEndDate
    '        Dim dtFPeriodEnd As Date = dtEndDate
    '        If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
    '            Dim objPeriod As New clscommom_period_Tran
    '            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboDeductionPeriod.SelectedValue)
    '            dtFPeriodStart = objPeriod._Start_Date
    '            dtFPeriodEnd = objPeriod._End_Date
    '        End If
    '        Dim eIntRateCalcTypeID As enLoanInterestCalcType
    '        If CInt(cboInterestCalcType.SelectedValue) > 0 Then
    '            eIntRateCalcTypeID = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
    '        Else
    '            eIntRateCalcTypeID = enLoanInterestCalcType.MONTHLY
    '        End If
    '        objLoan_Advance.Calulate_Projected_Loan_Balance(MaxLoanAmountDefined _
    '                                                   , CInt(DateDiff(DateInterval.Day, Now.Date, dtEndDate.Date)) _
    '                                                   , txtLoanRate.Decimal _
    '                                                   , CType(CInt(cboLoanCalcType.SelectedValue), enLoanCalcId) _
    '                                                   , eIntRateCalcTypeID _
    '                                                   , CInt(nudDuration.Value) _
    '                                                   , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
    '                                                   , Convert.ToDecimal(IIf(CType(CInt(cboLoanCalcType.SelectedValue), enLoanCalcId) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, txtMaxLoanPrincipalAmt.Decimal, txtInstallmentAmt.Decimal)) _
    '                                                   , decInstrAmount _
    '                                                   , decInstallmentAmount _
    '                                                   , decTotIntrstAmount _
    '                                                   , decTotInstallmentAmount _
    '                                                   )

    '        txtMaxLoanPrincipalAmt.Text = Format(decInstallmentAmount - decInstrAmount, GUI.fmtCurrency)
    '        decMaxLoanAmount = MaxLoanAmountDefined + decTotIntrstAmount
    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "GetMaxLoanAmountDefinedLoanScheme", mstrModuleName)
    '    Finally
    '        objLoan_Advance = Nothing
    '    End Try
    '    Return decMaxLoanAmount
    'End Function

    Function GetAmountByHeadFormula(ByVal strHeadFormula As String, Optional ByRef strResultedFormulaExppr As String = "") As Decimal
        Dim objMaster As New clsMasterData
        Dim objPayrollProcess As New clsPayrollProcessTran
        Dim decAmount As Decimal = 0
        strResultedFormulaExppr = ""
        Try

            If CInt(cboEmpName.SelectedValue) > 0 AndAlso strHeadFormula.Trim.Length > 0 Then
                Dim intPrevPeriod As Integer = objMaster.getCurrentPeriodID(CInt(enModuleReference.Payroll), mdtPeriodStart.AddDays(-1), 0, 0, True, False, Nothing, False)

                'Hemant (22 Dec 2023) -- Start
                Dim objPeriod As New clscommom_period_Tran
                Dim objCompany As New clsCompany_Master
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriod
                Dim intyearid As Integer = objPeriod._Yearunkid
                Dim dsCompany As DataSet = objCompany.GetFinancialYearList(Company._Object._Companyunkid, -1, "List", intyearid)

                Dim strDatabaseName As String = FinancialYear._Object._DatabaseName
                If dsCompany IsNot Nothing AndAlso dsCompany.Tables(0).Rows.Count > 0 Then
                    strDatabaseName = dsCompany.Tables(0).Rows(0).Item("database_name").ToString
                End If

                objPeriod = Nothing
                objCompany = Nothing
                'Hemant (22 Dec 2023) -- End


                'Pinkal (11-Mar-2024) -- Start
                '(A1X-2505) NMB - Credit card integration.
                If strHeadFormula.Trim.Length > 0 AndAlso strHeadFormula.Contains("#creditcardamountondsr#") Then
                    Dim mstrBankACNo As String = ""
                    Dim objEmpBankTran As New clsEmployeeBanks
                    Dim dsEmpBank As DataSet = objEmpBankTran.GetList(strDatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting _
                                                                                                  , True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", , , CInt(cboEmpName.SelectedValue).ToString(), mdtPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

                    If dsEmpBank IsNot Nothing AndAlso dsEmpBank.Tables(0).Rows.Count > 0 Then
                        If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then
                            Dim objCreditCardIntegration As New clsCreditCardIntegration
                            objCreditCardIntegration.GetEmpCreditCardAmount(dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim(), mstrLoanSchemeCode, mdecCreditCardAmountOnExposure, True)
                            objCreditCardIntegration = Nothing
                        End If
                    End If 'If dsEmpBank.Tables(0).Rows.Count > 0 Then
                    objEmpBankTran = Nothing
                End If
                'Pinkal (11-Mar-2024) -- End


                Dim dt As DataTable = objPayrollProcess.GetFormulaBasePayslipAmount(strDatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", intPrevPeriod, CStr(cboEmpName.SelectedValue), strHeadFormula, "", _
                                                                                    ConfigParameter._Object._OracleHostName, ConfigParameter._Object._OraclePortNo, ConfigParameter._Object._OracleServiceName, ConfigParameter._Object._OracleUserName, ConfigParameter._Object._OracleUserPassword, mstrLoanSchemeCode)
                'Hemant (22 Dec 2023) -- [FinancialYear._Object._DatabaseName  --> strDatabaseName]
                'Hemant (03 Feb 2023) -- [mstrLoanSchemeCode]
                If dt.Rows.Count > 0 Then
                    decAmount = CDec(dt.Rows(0).Item("amount"))
                    strResultedFormulaExppr = dt.Rows(0).Item("formulaexpr").ToString
                End If
            End If
            Return decAmount
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "GetAmountByHeadFormula", mstrModuleName)
        Finally
            objMaster = Nothing
            objPayrollProcess = Nothing
        End Try
    End Function

    Private Sub FillPeriod()
        Dim objMaster As New clsMasterData
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombos As DataSet
        Dim mdtTable As DataTable
        Try


            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 FinancialYear._Object._DatabaseName, _
                                                 FinancialYear._Object._Database_Start_Date, _
                                                 "List", False, enStatusType.Open)

            If dsCombos.Tables(0).Rows.Count > 0 Then
                mintDeductionPeriodUnkId = CInt(dsCombos.Tables(0).Rows(0).Item("periodunkid"))
            End If

            If mintDeductionPeriodUnkId > 0 Then

                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintDeductionPeriodUnkId
                mdtPeriodStart = objPeriod._Start_Date
                mdtPeriodEnd = objPeriod._End_Date

                objvalPeriodDuration.Text = Language.getMessage(mstrModuleName, 39, "Period Duration From :") & " " & objPeriod._Start_Date.Date & " " & Language.getMessage(mstrModuleName, 40, "To") & " " & objPeriod._End_Date.Date

            Else
                objvalPeriodDuration.Text = ""
                mdtPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString())
                mdtPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate.ToString())
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillPeriod", mstrModuleName)
        Finally
            objMaster = Nothing
            objPeriod = Nothing
        End Try
    End Sub

    Public Sub GetValue()
        Try
            If Not (objProcesspendingloan._Application_Date = Nothing) Then
                mdtApplcationDate = objProcesspendingloan._Application_Date
            End If

            If Not (objProcesspendingloan._Application_No = Nothing) Then
                mstrApplicationNo = objProcesspendingloan._Application_No
            End If


            cboEmpName.SelectedValue = CStr(objProcesspendingloan._Employeeunkid)
            cboLoanScheme.SelectedValue = CInt(objProcesspendingloan._Loanschemeunkid)

            RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            txtLoanAmt.Decimal = CDec(objProcesspendingloan._Loan_Amount)
            txtLoanAmt.Text = Format(objProcesspendingloan._Loan_Amount, GUI.fmtCurrency)
            AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged

            RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
            nudDuration.Value = CDec(IIf(objProcesspendingloan._NoOfInstallment <= 0, 1, objProcesspendingloan._NoOfInstallment))
            AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
            mintDeductionPeriodUnkId = objProcesspendingloan._DeductionPeriodunkid
            'txtAppliedUptoPrincipalAmt.Text = Format(objProcesspendingloan._InstallmentAmount, GUI.fmtCurrency)

            txtPurposeOfCredit.Text = objProcesspendingloan._Emp_Remark

            objProcesspendingloan._Isvoid = objProcesspendingloan._Isvoid
            objProcesspendingloan._Voiddatetime = objProcesspendingloan._Voiddatetime
            objProcesspendingloan._Voiduserunkid = objProcesspendingloan._Voiduserunkid
            If menAction = enAction.EDIT_ONE Then
                cboCurrency.SelectedValue = objProcesspendingloan._Countryunkid
            End If

            RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            txtInstallmentAmt.Text = Format(objProcesspendingloan._InstallmentAmount, GUI.fmtCurrency)
            AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            'txtPurposeOfCredit.Text = objProcesspendingloan._Emp_Remark

            txtPlotNo.Text = objProcesspendingloan._PlotNo
            txtBlockNo.Text = objProcesspendingloan._BlockNo
            txtStreet.Text = objProcesspendingloan._Street
            txtTownCity.Text = objProcesspendingloan._TownCity
            txtMarketValue.Text = Format(objProcesspendingloan._MarketValue, GUI.fmtCurrency)
            txtCTNo.Text = objProcesspendingloan._CTNo
            txtLONo.Text = objProcesspendingloan._LONo
            txtFSV.Text = Format(objProcesspendingloan._FSV, GUI.fmtCurrency)

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            txtBOQ.Text = objProcesspendingloan._BillOfQty.Trim()
            'Pinkal (17-May-2024) -- End


            txtModel.Text = objProcesspendingloan._Model
            txtYOM.Text = objProcesspendingloan._YOM

            chkMakeExceptionalApplication.Checked = objProcesspendingloan._IsExceptionalApplication
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            Call chkMakeExceptionalApplication_CheckedChanged(chkMakeExceptionalApplication, New System.EventArgs)
            'Hemant (27 Oct 2022) -- End

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-998 - As a user, I want to have additional fields on application and approval pages where loan category = Secured. The below are the fields and they should be mandatory(Chassis number,Supplier,Colour)
            txtChassisNo.Text = objProcesspendingloan._ChassisNo
            txtSupplier.Text = objProcesspendingloan._Supplier
            txtColour.Text = objProcesspendingloan._Colour
            'Hemant (27 Oct 2022) -- End

            'Hemant (29 Mar 2024) -- Start
            'ENHANCEMENT(TADB): New loan application form
            txtTotalCIF.Text = Format(objProcesspendingloan._TotalCIF, GUI.fmtCurrency)
            txtEstimatedTax.Text = Format(objProcesspendingloan._EstimatedTax, GUI.fmtCurrency)
            txtInvoiceValue.Text = Format(objProcesspendingloan._InvoiceValue, GUI.fmtCurrency)
            txtModelNo.Text = objProcesspendingloan._ModelNo
            txtEngineNo.Text = objProcesspendingloan._EngineNo
            txtEngineCapacity.Text = Format(objProcesspendingloan._EngineCapacity, GUI.fmtCurrency)
            'Hemant (29 Mar 2024) -- End

            'Hemant (18 Mar 2024) -- Start
            'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
            txtRepaymentDays.Text = CStr(objProcesspendingloan._RepaymentDays)
            'Hemant (18 Mar 2024) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            chkSalaryAdvanceLiquidation.Checked = CBool(objProcesspendingloan._IsLiquidate)
            'Hemant (22 Nov 2024) -- End

            'Hemant (07 Jul 2023) -- Start
            'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
            If Not (objProcesspendingloan._TitleIssueDate = Nothing) Then
                dtpTitleIssueDate.Value = CDate(objProcesspendingloan._TitleIssueDate)
            End If
            nudTitleValidity.Value = CInt(objProcesspendingloan._TitleValidity)
            If Not (objProcesspendingloan._TitleExpiryDate = Nothing) Then
                dtpTitleExpiryDate.Value = CDate(objProcesspendingloan._TitleExpiryDate)
            End If
            'Hemant (07 July 2023) -- End

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            'mdtLoanApplicationDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.LOAN_ADVANCE, mintProcessPendingLoanUnkid, ConfigParameter._Object._Document_Path)            

            'If menAction <> enAction.EDIT_ONE Then
            '    If mdtLoanApplicationDocument IsNot Nothing Then mdtLoanApplicationDocument.Rows.Clear()
            'End If
            'Hemant (27 Oct 2022) -- End

            Call nudDuration_ValueChanged(nudDuration, New System.EventArgs)
            'Call txtInstallmentAmt_TextChanged(txtInstallmentAmt, New System.EventArgs)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Public Sub SetValue()
        Try
            objProcesspendingloan._Isloan = True

            objProcesspendingloan._Application_No = mstrApplicationNo
            objProcesspendingloan._Application_Date = mdtApplcationDate
            objProcesspendingloan._Loan_Amount = txtLoanAmt.Decimal

            objProcesspendingloan._Employeeunkid = CInt(cboEmpName.SelectedValue)
            objProcesspendingloan._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)

            objProcesspendingloan._Countryunkid = CInt(cboCurrency.SelectedValue)
            objProcesspendingloan._DeductionPeriodunkid = mintDeductionPeriodUnkId

            objProcesspendingloan._InstallmentAmount = CDec(txtInstallmentAmt.Decimal)
            objProcesspendingloan._NoOfInstallment = CInt(nudDuration.Value)
            objProcesspendingloan._Loan_Statusunkid = 1 'Pending
            objProcesspendingloan._Emp_Remark = ""
            objProcesspendingloan._Userunkid = User._Object._Userunkid
            objProcesspendingloan._Isvoid = False
            objProcesspendingloan._Voiddatetime = Nothing
            objProcesspendingloan._Voiduserunkid = -1
            objProcesspendingloan._IsLoanApprover_ForLoanScheme = ConfigParameter._Object._IsLoanApprover_ForLoanScheme

            objProcesspendingloan._IsImportedLoan = False

            objProcesspendingloan._Loan_Account_No = ""
            objProcesspendingloan._Emp_Remark = txtPurposeOfCredit.Text
            objProcesspendingloan._PlotNo = ""
            objProcesspendingloan._BlockNo = ""
            objProcesspendingloan._Street = ""
            objProcesspendingloan._TownCity = ""
            objProcesspendingloan._MarketValue = CDec(0)
            objProcesspendingloan._CTNo = ""
            objProcesspendingloan._LONo = ""
            objProcesspendingloan._FSV = CDec(0)
            objProcesspendingloan._Model = ""
            objProcesspendingloan._YOM = ""
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-998 - As a user, I want to have additional fields on application and approval pages where loan category = Secured. The below are the fields and they should be mandatory(Chassis number,Supplier,Colour)
            objProcesspendingloan._ChassisNo = ""
            objProcesspendingloan._Supplier = ""
            objProcesspendingloan._Colour = ""
            objProcesspendingloan._IsFlexcube = True
            'Hemant (27 Oct 2022) -- End
            'Hemant (29 Mar 2024) -- Start
            'ENHANCEMENT(TADB): New loan application form
            objProcesspendingloan._TotalCIF = CDec(0)
            objProcesspendingloan._EstimatedTax = CDec(0)
            objProcesspendingloan._InvoiceValue = CDec(0)
            objProcesspendingloan._ModelNo = ""
            objProcesspendingloan._EngineNo = ""
            objProcesspendingloan._EngineCapacity = CDec(0)
            'Hemant (29 Mar 2024) -- End
            'Hemant (07 Jul 2023) -- Start
            'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
            objProcesspendingloan._TitleIssueDate = Nothing
            objProcesspendingloan._TitleValidity = 0
            objProcesspendingloan._TitleExpiryDate = Nothing
            'Hemant (07 July 2023) -- End
            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then
                objProcesspendingloan._PlotNo = txtPlotNo.Text
                objProcesspendingloan._BlockNo = txtBlockNo.Text
                objProcesspendingloan._Street = txtStreet.Text
                objProcesspendingloan._TownCity = txtTownCity.Text
                objProcesspendingloan._MarketValue = CDec(txtMarketValue.Text)
                objProcesspendingloan._CTNo = txtCTNo.Text
                objProcesspendingloan._LONo = txtLONo.Text
                objProcesspendingloan._FSV = CDec(txtFSV.Text)
                'Hemant (07 Jul 2023) -- Start
                'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
                objProcesspendingloan._TitleIssueDate = CDate(dtpTitleIssueDate.Value)
                objProcesspendingloan._TitleValidity = CInt(nudTitleValidity.Value)
                objProcesspendingloan._TitleExpiryDate = CDate(dtpTitleExpiryDate.Value)
                'Hemant (07 July 2023) -- End

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                objProcesspendingloan._BillOfQty = txtBOQ.Text.Trim()
                'Pinkal (17-May-2024) -- End


            ElseIf CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.SECURED Then
                objProcesspendingloan._Model = txtModel.Text
                objProcesspendingloan._YOM = txtYOM.Text
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-998 - As a user, I want to have additional fields on application and approval pages where loan category = Secured. The below are the fields and they should be mandatory(Chassis number,Supplier,Colour)
                objProcesspendingloan._ChassisNo = txtChassisNo.Text
                objProcesspendingloan._Supplier = txtSupplier.Text
                objProcesspendingloan._Colour = txtColour.Text
                'Hemant (27 Oct 2022) -- End
                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                objProcesspendingloan._TotalCIF = CDec(txtTotalCIF.Text)
                objProcesspendingloan._EstimatedTax = CDec(txtEstimatedTax.Text)
                objProcesspendingloan._InvoiceValue = CDec(txtInvoiceValue.Text)
                objProcesspendingloan._ModelNo = txtModelNo.Text
                objProcesspendingloan._EngineNo = txtEngineNo.Text
                objProcesspendingloan._EngineCapacity = CDec(txtEngineCapacity.Text)
                'Hemant (29 Mar 2024) -- End
            End If
            objProcesspendingloan._IsExceptionalApplication = chkMakeExceptionalApplication.Checked

            'Pinkal (20-Sep-2022) -- Start
            'NMB Loan Module Enhancement.
            objProcesspendingloan._RequiredReportingToApproval = mblnRequiredReportingToApproval
            'Pinkal (20-Sep-2022) -- End

 'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-949 - As a user, on the application screen, if the loan scheme selected is eligible for top-up, my Take home should be calculated as Principal Amount – (Outstanding Principle + Outstanding Interest + Insurance Amount)
            objProcesspendingloan._OutstandingPrincipalAmount = CDec(txtOutStandingPrincipalAmt.Text)
            objProcesspendingloan._OutstandingInterestAmount = CDec(txtOutStandingInterestAmt.Text)
            objProcesspendingloan._NoOfInstallmentPaid = CInt(nudInstallmentPaid.Value)
            If CDec(txtOutStandingPrincipalAmt.Text) > 0 OrElse CDec(txtOutStandingInterestAmt.Text) > 0 Then
                objProcesspendingloan._IsTopUp = True
            Else
                objProcesspendingloan._IsTopUp = False
            End If
            objProcesspendingloan._TakeHomeAmount = CDec(txtTakeHome.Text)
            'Hemant (12 Oct 2022) -- End

            'Hemant (25 Nov 2022) -- Start
            'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
            objProcesspendingloan._InterestRate = CDec(txtLoanRate.Text)
            'Hemant (25 Nov 2022) -- End


            'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            objProcesspendingloan._SkipApproverFlow = mblnSkipApprovalFlow
            'Pinkal (12-Oct-2022) -- End

            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objProcesspendingloan._LoanApproval_DailyReminder = mblnLoanApproval_DailyReminder
            objProcesspendingloan._IssubmitApproval = True
            objProcesspendingloan._EscalationDays = mintEscalationDays
            'Pinkal (10-Nov-2022) -- End


            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement. 
            objProcesspendingloan._OtherLoanOutstandingPrincipalAmount = CDec(txtOutStandingPrincipalAmt.Tag) '[GETTING OUTSTANDING PRINCIPAL AMOUNT FROM OTHER LOAN SCHEME TO DEFINE THE APPROVER FLOW]
            objProcesspendingloan._PostedData = ""
            objProcesspendingloan._ReponseData = ""
            objProcesspendingloan._IsPostedError = False
            'Pinkal (23-Nov-2022) -- End

            'Pinkal (21-Jul-2023) -- Start
            '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
            objProcesspendingloan._NTFsendtitleexpiryemp = False
            objProcesspendingloan._NTFsendtitleexpiryusers = False
            'Pinkal (21-Jul-2023) -- End

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            objProcesspendingloan._SendEmpReminderForNextTrance = Nothing
            objProcesspendingloan._NtfSendEmpForNextTranche = False
            'Pinkal (04-Aug-2023) -- End

            'Pinkal (11-Mar-2024) -- Start
            '(A1X-2505) NMB - Credit card integration.
            objProcesspendingloan._CreditCardAmountOnExposure = mdecCreditCardAmountOnExposure
            'Pinkal (11-Mar-2024) -- End

            'Hemant (18 Mar 2024) -- Start
            'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
            objProcesspendingloan._RepaymentDays = CInt(txtRepaymentDays.Text)
            'Hemant (18 Mar 2024) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            If chkSalaryAdvanceLiquidation.Visible = True Then
                objProcesspendingloan._IsLiquidate = CBool(chkSalaryAdvanceLiquidation.Checked)
            Else
                objProcesspendingloan._IsLiquidate = False
            End If
            objProcesspendingloan._SalaryAdvancePrincipalAmount = mdecFinalSalaryAdvancePendingPrincipalAmt
            'Hemant (22 Nov 2024) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Dim objEmployee As New clsEmployee_Master
        Dim objEmployeeDates As New clsemployee_dates_tran
        Dim objDiscFileMaster As New clsDiscipline_file_master
        Dim objProceedingMaster As New clsdiscipline_proceeding_master
        Dim objLoanCategoryMapping As New clsLoanCategoryMapping
        'Hemant (27 Oct 2022) -- Start
        'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
        Dim objLoanScheme As New clsLoan_Scheme
        'Hemant (27 Oct 2022) -- End
        Dim strFilter As String = String.Empty
        Dim dsData As New DataSet

        Try
            objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmpName.SelectedValue)

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .

            mstrEmployeeName = objEmployee._Firstname & " " & objEmployee._Surname

            mstrEmployeeEmail = objEmployee._Email

            If ConfigParameter._Object._SMSGatewayEmailType = CInt(clsEmployee_Master.EmpColEnum.Col_Present_Mobile) Then
                mstrEmpMobileNo = objEmployee._Present_Mobile.ToString()
            ElseIf ConfigParameter._Object._SMSGatewayEmailType = CInt(clsEmployee_Master.EmpColEnum.Col_Domicile_Mobile) Then
                mstrEmpMobileNo = objEmployee._Domicile_Mobile.ToString()
            End If

            If mstrEmpMobileNo.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 60, "Mobile No is compulsory information. Please set Employee's Mobile No to send TOTP."), enMsgBoxStyle.Information)
                Return False
            End If

            'Pinkal (02-Jun-2023) -- End

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
            objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            'Hemant (27 Oct 2022) -- End
            If CInt(cboEmpName.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue."), enMsgBoxStyle.Information)
                cboEmpName.Focus()
                Return False
            End If

            If CInt(cboLoanScheme.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue."), enMsgBoxStyle.Information)
                cboLoanScheme.Focus()
                Return False
            End If

            If CDec(txtLoanAmt.Text) <= 0 Then
                Language.setLanguage(mstrModuleName)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Applied Amount cannot be blank. Applied Amount is compulsory information."), enMsgBoxStyle.Information)
                txtLoanAmt.Focus()
                Return False
            End If

            If CInt(cboCurrency.SelectedValue) <= 0 Then
                Language.setLanguage(mstrModuleName)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Currency is compulsory information.Please select currency."), enMsgBoxStyle.Information)
                cboCurrency.Focus()
                Return False
            End If

            If CDec(txtInstallmentAmt.Text) <= 0 Then
                Language.setLanguage(mstrModuleName)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Installment Amount cannot be 0.Please define installment amount greater than 0."), enMsgBoxStyle.Information)
                txtInstallmentAmt.Focus()
                Return False
            End If

            If CDec(txtInstallmentAmt.Text) > CDec(txtLoanAmt.Text) AndAlso nudDuration.Value > 1 Then
                'Hemant (07 Jul 2023) -- [nudDuration.Value > 1]
                Language.setLanguage(mstrModuleName)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Installment Amount cannot be greater than Loan Amount."), enMsgBoxStyle.Information)
                txtInstallmentAmt.Focus()
                Return False
            End If

            'Hemant (18 Mar 2024) -- Start
            'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
            If mstrCompanyGroupName = "TADB" And txtRepaymentDays.Enabled = True AndAlso (txtRepaymentDays.Text.Trim.Length <= 0 OrElse CInt(txtRepaymentDays.Text) <= 0) Then
                Language.setLanguage(mstrModuleName)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 74, "No of Repayment Days cannot be 0.Please define No of Repayment Days greater than 0."), enMsgBoxStyle.Information)
                txtRepaymentDays.Focus()
                Return False
            End If
            'Hemant (18 Mar 2024) -- End


            'Pinkal (28-Oct-2024) -- Start
            'NMB Enhancement : (A1X-2834) NMB - Credit card loan enhancements.
            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.CREDIT_CARD Then
                Dim mstrBankACNo As String = ""
                Dim objEmpBankTran As New clsEmployeeBanks

                Dim dsEmpBank As DataSet = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting _
                                                                                              , True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", , , CInt(cboEmpName.SelectedValue).ToString(), mdtPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

                If dsEmpBank IsNot Nothing AndAlso dsEmpBank.Tables(0).Rows.Count > 0 Then

                    If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then


                        Dim objCreditCardIntegration As New clsCreditCardIntegration
                        Dim dsList As DataSet = objCreditCardIntegration.GetList("List")

                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                            Dim dRRow As DataRow() = dsList.Tables(0).Select("directdebitaccno = '" & dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim() & "'")


                            If dRRow IsNot Nothing AndAlso dRRow.Length > 0 Then

                                Dim mstrDayssPastDue As String = "0.00"
                                Dim mstrWalletCreditLimit As String = "0.00"
                                Dim mstrProgramName As String = ""

                                If IsDBNull(dRRow(0)("programname")) = False AndAlso dRRow(0)("programname").ToString().Trim().Length > 0 Then
                                    Dim ar() As String = dRRow(0)("programname").ToString().Trim().Split(CChar("-"))
                                    If ar IsNot Nothing AndAlso ar.Length > 0 Then
                                        mstrProgramName = ar(0).ToString().Trim()
                                    End If
                                End If

                                If IsDBNull(dRRow(0)("dayspastdue")) = False AndAlso dRRow(0)("dayspastdue").ToString().Trim().Length > 0 Then
                                    If dRRow(0)("dayspastdue").ToString().Trim() = "-" Then
                                        mstrDayssPastDue = "0.00"
                                    ElseIf dRRow(0)("dayspastdue").ToString().Trim() = "" Then
                                        mstrDayssPastDue = "0.00"
                                    ElseIf dRRow(0)("dayspastdue").ToString().Trim() = " " Then
                                        mstrDayssPastDue = "0.00"
                                    Else
                                        mstrDayssPastDue = dRRow(0)("dayspastdue").ToString().Trim()
                                    End If
                                End If

                                If IsDBNull(dRRow(0)("walletcreditlimit")) = False AndAlso dRRow(0)("walletcreditlimit").ToString().Trim().Length > 0 Then
                                    If dRRow(0)("walletcreditlimit").ToString().Trim() = "-" Then
                                        mstrWalletCreditLimit = "0.00"
                                    ElseIf dRRow(0)("walletcreditlimit").ToString().Trim() = "" Then
                                        mstrWalletCreditLimit = "0.00"
                                    ElseIf dRRow(0)("walletcreditlimit").ToString().Trim() = " " Then
                                        mstrWalletCreditLimit = "0.00"
                                    Else
                                        mstrWalletCreditLimit = dRRow(0)("walletcreditlimit").ToString().Trim()
                                    End If
                                End If


                                If IsNumeric(mstrDayssPastDue) AndAlso CInt(mstrDayssPastDue) > 0 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 77, "You cannnot apply for this loan application.Reason : you have already past dues days."), enMsgBoxStyle.Information)
                                    Return False

                                ElseIf IsNumeric(mstrDayssPastDue) AndAlso CInt(mstrDayssPastDue) <= 0 AndAlso mstrProgramName.Trim.Length > 0 AndAlso mstrProgramName.Trim().ToUpper() = objLoanScheme._Code.Trim().ToUpper() Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 80, "You cannnot apply for this loan application.Reason : you have already past dues days entry for this loan scheme."), enMsgBoxStyle.Information)
                                    Return False

                                ElseIf IsNumeric(mstrWalletCreditLimit) AndAlso CDec(mstrWalletCreditLimit) < txtLoanAmt.Decimal Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 78, "You cannnot apply for this loan application.Reason : you have already exceeded with your credit card limit."), enMsgBoxStyle.Information)
                                    txtLoanAmt.Focus()
                                    Return False
                                End If 'If CInt(dRRow(0)("dayspastdue")) > 0 Then

                            End If  'If dRRow IsNot Nothing AndAlso dRRow.Length > 0 Then

                            dRRow = Nothing
                        Else
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 79, "You cannnot apply for this loan application.Reason : There is no record(s) related to credit card."), enMsgBoxStyle.Information)
                            Return False

                        End If  'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                        objCreditCardIntegration = Nothing

                    End If ' If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then

                End If 'If dsEmpBank.Tables(0).Rows.Count > 0 Then

                objEmpBankTran = Nothing

            End If  ' If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.CREDIT_CARD Then

            'Pinkal (28-Oct-2024) -- End


            If chkMakeExceptionalApplication.Checked = False Then


                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
                'Dim objLoanScheme As New clsLoan_Scheme
                'objLoanScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                'Hemant (27 Oct 2022) -- End


                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                If objLoanScheme._MinNoOfInstallment > 0 AndAlso CInt(nudDuration.Value) < objLoanScheme._MinNoOfInstallment Then
                    Language.setLanguage(mstrModuleName)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 56, "Installment months cannot be less than") & " " & objLoanScheme._MinNoOfInstallment & " " & Language.getMessage(mstrModuleName, 33, "for") & " " & objLoanScheme._Name & Language.getMessage(mstrModuleName, 34, " Scheme."), enMsgBoxStyle.Information)
                    nudDuration.Focus()
                    Return False
                End If
                'Pinkal (23-Nov-2022) -- End

                'Hemant (02 Feb 2024) -- Start
                'ISSUE/ENHANCEMENT(NMB): To exclude one month installment on application because during application, the system considers the first month in installment payments but in practice Approvers selects the 2nd month as deduction period. 
                'If objLoanScheme._MaxNoOfInstallment > 0 AndAlso CInt(nudDuration.Value) > objLoanScheme._MaxNoOfInstallment Then
                If CInt(nudMaxInstallment.Value) > 0 AndAlso CInt(nudDuration.Value) > CInt(nudMaxInstallment.Value) Then
                    'Hemant (02 Feb 2024) -- End
                    Language.setLanguage(mstrModuleName)
                    'Hemant (02 Feb 2024) -- Start
                    'ISSUE/ENHANCEMENT(NMB): To exclude one month installment on application because during application, the system considers the first month in installment payments but in practice Approvers selects the 2nd month as deduction period. 
                    'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Installment months cannot be greater than ") & " " & objLoanScheme._MaxNoOfInstallment & Language.getMessage(mstrModuleName, 33, "for") & " " & objLoanScheme._Name & Language.getMessage(mstrModuleName, 34, " Scheme."), enMsgBoxStyle.Information)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Installment months cannot be greater than ") & " " & nudMaxInstallment.Value & " " & Language.getMessage(mstrModuleName, 33, "for") & " " & objLoanScheme._Name & Language.getMessage(mstrModuleName, 34, " Scheme."), enMsgBoxStyle.Information)
                    'Hemant (02 Feb 2024) -- End
                    nudDuration.Focus()
                    Return False
                End If

                If CDec(txtLoanAmt.Text) > CDec(txtMaxLoanAmount.Text) Then
                    Language.setLanguage(mstrModuleName)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you can not apply for this loan scheme: Reason, Principal amount  is exceeding the Max Principal Amt") & " [" & Format(CDec(txtMaxLoanAmount.Text), GUI.fmtCurrency) & "] " & Language.getMessage(mstrModuleName, 42, " set for selected scheme."), enMsgBoxStyle.Information)
                    txtLoanAmt.Focus()
                    Return False
                End If

                If CDec(txtInstallmentAmt.Text) > CDec(txtMaxInstallmentAmt.Text) Then
                    Language.setLanguage(mstrModuleName)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you can not apply for this loan scheme: Reason, Installment amount  is exceeding the Max Installment amount") & " [" & Format(CDec(txtMaxInstallmentAmt.Text), GUI.fmtCurrency) & "] " & Language.getMessage(mstrModuleName, 42, " set for selected scheme."), enMsgBoxStyle.Information)
                    txtLoanAmt.Focus()
                    Return False
                End If

                If CDec(txtMinLoanAmount.Text) > CDec(txtLoanAmt.Text) Then
                    Language.setLanguage(mstrModuleName)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Sorry, you can not apply for this loan scheme: Reason, Principal amount  should be greater than the Minimum Principal amount") & " [" & Format(CDec(txtMinLoanAmount.Text), GUI.fmtCurrency) & "] " & Language.getMessage(mstrModuleName, 42, " set for selected scheme."), enMsgBoxStyle.Information)
                    txtLoanAmt.Focus()
                    Return False
                End If

                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-946 - As a user, I want to have an option (check box) on the loan scheme to indicate which loan schemes are eligible for top-up. If a loan scheme doesn’t have this option checked, user cannot do top-up on it   
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-994 - As a user, I want system to accept rejection of training if training budget is not sufficient. Currently system is refusing.
                'If CBool(objLoanScheme._IsEligibleForTopUp) = False AndAlso (CDec(txtOutStandingPrincipalAmt.Text) > 0 OrElse CDec(txtOutStandingInterestAmt.Text) > 0) Then
                '    Language.setLanguage(mstrModuleName)
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 46, "Sorry, you can not apply for Topup for this loan scheme: Reason, TopUp is not allowed ") & Language.getMessage(mstrModuleName, 42, " set for selected scheme."), enMsgBoxStyle.Information)
                '    Return False
                'End If
                'Hemant (27 Oct 2022) -- End
                'Hemant (12 Oct 2022) -- End

                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-950 - As a user, I don’t want the application to save if the number of paid installments is less than number of installments configured on loan scheme for top-up
                'Hemant (13 Dec 2022) -- Start
                'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - Top-up applications do not check the top-up condition is the number of paid installments in flex = 0
                'If CBool(objLoanScheme._IsPostingToFlexcube) = True AndAlso CInt(nudInstallmentPaid.Text) > 0 AndAlso CInt(nudInstallmentPaid.Text) <= CInt(objLoanScheme._MinOfInstallmentPaid) Then
                If CBool(objLoanScheme._IsPostingToFlexcube) = True AndAlso mintCurrentFlexcubeLoanCount > 0 AndAlso CInt(nudInstallmentPaid.Text) >= 0 AndAlso CInt(nudInstallmentPaid.Text) < CInt(objLoanScheme._MinOfInstallmentPaid) Then
                    'Hemant (13 Dec 2022) -- End
                    Language.setLanguage(mstrModuleName)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 47, "Sorry, you can not apply for Topup for this loan scheme: Reason, No of Installment Paid should be greater than minimum No of Installment Paid ") & " [" & objLoanScheme._MinOfInstallmentPaid & "] " & Language.getMessage(mstrModuleName, 42, " set for selected scheme."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Hemant (12 Oct 2022) -- End

                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
                'objLoanScheme = Nothing
                'Hemant (27 Oct 2022) -- End
            End If

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
            If CBool(objLoanScheme._IsPostingToFlexcube) = True AndAlso CBool(objLoanScheme._IsEligibleForTopUp) = False AndAlso (CDec(txtOutStandingPrincipalAmt.Text) > 0 OrElse CDec(txtOutStandingInterestAmt.Text) > 0) Then
                Language.setLanguage(mstrModuleName)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 46, "Sorry, you can not apply for Topup for this loan scheme: Reason, TopUp is not allowed ") & Language.getMessage(mstrModuleName, 42, " set for selected scheme."), enMsgBoxStyle.Information)
                Return False
            End If
            'Hemant (27 Oct 2022) -- End

            If CInt(cboLoanCalcType.SelectedValue) = CInt(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI) Then
                Dim decValue As Decimal
                Decimal.TryParse(txtAppliedUptoPrincipalAmt.Text, decValue)
                If decValue <= 0 Then
                    Language.setLanguage(mstrModuleName)
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Principal Amount cannot be 0. Please enter Principal amount."), enMsgBoxStyle.Information)
                    txtAppliedUptoPrincipalAmt.Focus()
                    Return False
                End If
            End If

            If txtPurposeOfCredit.Text.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Purpose Of Credit is compulsory information. Please enter Purpose Of Credit to continue."), enMsgBoxStyle.Information)
                txtPurposeOfCredit.Focus()
                Return False
            End If

            'Hemant (13 Dec 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - For top-up loan applications, system should not allow saving the application if The Take Home amount is 0 or negative amount.
            If CDec(txtTakeHome.Text) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 57, "Sorry, you do not qualify for a top-up at the moment. The take home amount should be greater than 0"), enMsgBoxStyle.Information)
                txtLoanAmt.Focus()
                Return False
            End If
            'Hemant (13 Dec 2022) -- End

            'Hemant (17 Mar 2023) -- Start
            'ISSUE/EHANCEMENT(NMB) : Please keep it for exceptional. Its causing us trouble in CRB
            Dim dsSchemeCategoryMappring As DataSet = objLoanCategoryMapping.GetList("List", CInt(cboLoanSchemeCategory.SelectedValue))
            If CInt(cboLoanSchemeCategory.SelectedValue) > 0 AndAlso dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 AndAlso CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("identitytypeunkid")) > 0 Then
                Dim strIdentityTypesIds As String = String.Join(",", dsSchemeCategoryMappring.Tables(0).AsEnumerable().Select(Function(x) x.Field(Of Integer)("identitytypeunkid").ToString).ToArray())
                Dim objIdentityTran As New clsIdentity_tran
                objIdentityTran._EmployeeUnkid = CInt(cboEmpName.SelectedValue)
                Dim dtIdentityType As DataTable = objIdentityTran._DataList
                Dim drIdentityType() As DataRow = dtIdentityType.Select("idtypeunkid IN (" & strIdentityTypesIds & ")")
                If drIdentityType.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, NIDA number is missing or in pending state"), enMsgBoxStyle.Information)
                    Return False
                End If
                objIdentityTran = Nothing
            End If
            'Hemant (17 Mar 2023) -- End 

            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then

                If txtPlotNo.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Plot No is compulsory information. Please enter Plot No to continue."), enMsgBoxStyle.Information)
                    txtPlotNo.Focus()
                    Return False
                End If

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                'If txtBlockNo.Text.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Block No is compulsory information. Please enter Block No to continue."), enMsgBoxStyle.Information)
                '    txtBlockNo.Focus()
                '    Return False
                'End If
                'Pinkal (17-May-2024) -- End

                If txtStreet.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Street is compulsory information. Please enter Street to continue."), enMsgBoxStyle.Information)
                    txtStreet.Focus()
                    Return False
                End If

                If txtTownCity.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Town/City is compulsory information. Please enter Town/City to continue."), enMsgBoxStyle.Information)
                    txtTownCity.Focus()
                    Return False
                End If

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                If objLoanScheme._MarketValueMandatory AndAlso CDec(txtMarketValue.Text) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Market Value cannot be 0. Please enter Market Value."), enMsgBoxStyle.Information)
                    txtMarketValue.Focus()
                    Return False
                End If
                'Pinkal (17-May-2024) -- End

                If txtCTNo.Text.Trim.Length <= 0 AndAlso objLoanScheme._Name.ToString.ToUpper.Contains("SEMI-FINISH MORTGAGE") = False Then
                    'Hemant (17 Jan 2025) -- [AndAlso objLoanScheme._Name.ToString.ToUpper.Contains("SEMI-FINISH MORTGAGE") = False]
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "CT No is compulsory information. Please enter CT No to continue."), enMsgBoxStyle.Information)
                    txtCTNo.Focus()
                    Return False
                End If


                'Pinkal (21-Mar-2024) -- Start
                'NMB - Mortgage UAT Enhancements.
                'If txtLONo.Text.Trim.Length <= 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "LO No is compulsory information. Please enter LO No to continue."), enMsgBoxStyle.Information)
                '    txtLONo.Focus()
                '    Return False
                'End If
                'Pinkal (21-Mar-2024) -- End

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                If objLoanScheme._FSVValueMandatory AndAlso CDec(txtFSV.Text) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 20, "FSV cannot be 0.Please enter FSV."), enMsgBoxStyle.Information)
                    txtFSV.Focus()
                    Return False
                End If

                If objLoanScheme._BOQMandatory AndAlso txtBOQ.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 75, "Bill of Quantity (BOQ) cannot be blank.Please enter Bill of Quantity (BOQ)."), enMsgBoxStyle.Information)
                    txtBOQ.Focus()
                    Return False
                End If
                'Pinkal (17-May-2024) -- End

                'Hemant (07 Jul 2023) -- Start
                'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
                If dtpTitleIssueDate.Checked = False OrElse dtpTitleIssueDate.Value = Nothing Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 64, "Title Issue Date is compulsory information. Please enter Title Issue Date to continue."), enMsgBoxStyle.Information)
                    dtpTitleIssueDate.Focus()
                    Return False
                End If

                If CInt(nudTitleValidity.Value) <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 65, "Title Validity cannot be 0.Please enter Title Validity."), enMsgBoxStyle.Information)
                    nudTitleValidity.Focus()
                    Return False
                End If

                If dtpTitleExpiryDate.Value = Nothing Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 66, "Title Expriry Date is compulsory information. Please enter Title Expriry Date to continue."), enMsgBoxStyle.Information)
                    dtpTitleExpiryDate.Focus()
                    Return False
                End If
                'Hemant (07 July 2023) -- End
                'Hemant (07 Jul 2023) -- Start
                'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
                If dtpTitleExpiryDate.Value.Date < eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date.AddDays(CInt(objLoanScheme._NoOfDaysBefereTitleExpiry)) Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 67, "Sorry, you can not apply for this Mortgage Loan: Reason, Title Expiry Date should not be less than Date :") & eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date.AddDays(CInt(objLoanScheme._NoOfDaysBefereTitleExpiry)).ToShortDateString, enMsgBoxStyle.Information)
                    dtpTitleExpiryDate.Focus()
                    Return False
                End If
                'Hemant (07 July 2023) -- End

            End If

            If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.SECURED Then


                If txtModel.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Model is compulsory information. Please enter Model to continue."), enMsgBoxStyle.Information)
                    txtModel.Focus()
                    Return False
                End If

                If txtYOM.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "YOM is compulsory information. Please enter YOM to continue."), enMsgBoxStyle.Information)
                    txtYOM.Focus()
                    Return False
                End If

                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-998 - As a user, I want to have additional fields on application and approval pages where loan category = Secured. The below are the fields and they should be mandatory(Chassis number,Supplier,Colour)
                If txtChassisNo.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 49, "Chassis No is compulsory information. Please enter Chassis No to continue."), enMsgBoxStyle.Information)
                    txtChassisNo.Focus()
                    Return False
                End If

                If txtSupplier.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 50, "Supplier is compulsory information. Please enter Supplier to continue."), enMsgBoxStyle.Information)
                    txtSupplier.Focus()
                    Return False
                End If

                If txtColour.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 51, "Colour is compulsory information. Please enter Colour to continue."), enMsgBoxStyle.Information)
                    txtColour.Focus()
                    Return False
                End If
                'Hemant (27 Oct 2022) -- End

                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form               
                If mstrCompanyGroupName = "TADB" Then
                    If CDec(txtTotalCIF.Text) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 68, "Total CIF cannot be 0.Please enter Total CIF."), enMsgBoxStyle.Information)
                        txtTotalCIF.Focus()
                        Return False
                    End If

                    If CDec(txtEstimatedTax.Text) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 69, "Estimated Tax cannot be 0.Please enter Estimated Tax."), enMsgBoxStyle.Information)
                        txtEstimatedTax.Focus()
                        Return False
                    End If

                    If CDec(txtInvoiceValue.Text) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 70, "Invoice Value cannot be 0.Please enter Invoice Value."), enMsgBoxStyle.Information)
                        txtInvoiceValue.Focus()
                        Return False
                    End If

                    If txtModelNo.Text.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 71, "Model No is compulsory information. Please enter Model No to continue."), enMsgBoxStyle.Information)
                        txtModelNo.Focus()
                        Return False
                    End If

                    If txtEngineNo.Text.Trim.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 72, "Engine No is compulsory information. Please enter Engine No to continue."), enMsgBoxStyle.Information)
                        txtEngineNo.Focus()
                        Return False
                    End If

                    If CDec(txtEngineCapacity.Text) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 73, "Engine Capacity cannot be 0.Please enter Engine Capacity."), enMsgBoxStyle.Information)
                        txtEngineCapacity.Focus()
                        Return False
                    End If
                End If
                'Hemant (29 Mar 2024) -- End

            End If

            If chkMakeExceptionalApplication.Checked = False Then

                If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then

                    'Hemant (17 Jan 2025) -- Start
                    'ISSUE/ENHANCEMENT(NMB): A1X - 2968 :  Semi finished mortgage loan changes
                    If objLoanScheme._Name.ToString.ToUpper.Contains("SEMI-FINISH MORTGAGE") = False Then
                        'Hemant (17 Jan 2025) -- End
                    'Pinkal (17-May-2024) -- Start
                    'NMB Enhancement For Mortgage Loan.

                    'Hemant (13 Dec 2022) -- Start
                    'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - For loan applications where loan category = mortgage, they don’t want the application to save if FSV < Market Value .
                    If (objLoanScheme._FSVValueMandatory OrElse CDec(IIf(txtFSV.Text.Trim.Length <= 0, 0, txtFSV.Text.Trim)) > 0) AndAlso (objLoanScheme._MarketValueMandatory OrElse CDec(IIf(txtMarketValue.Text.Trim.Length <= 0, 0, txtMarketValue.Text.Trim)) > 0) AndAlso CDec(txtFSV.Text) > CDec(txtMarketValue.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 76, "FSV cannot be more than the Market Value."), enMsgBoxStyle.Information)
                        txtFSV.Focus()
                        Return False
                    End If
                    'Hemant (13 Dec 2022) -- End
                    'Hemant (13 Dec 2022) -- Start
                    'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - For loan applications where loan category = mortgage, they don’t want system to save the application if the Principle Amount is greater than the FSV.
                    If (objLoanScheme._FSVValueMandatory OrElse CDec(IIf(txtFSV.Text.Trim.Length <= 0, 0, txtFSV.Text.Trim)) > 0) AndAlso CDec(txtLoanAmt.Text) > CDec(txtFSV.Text) Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 59, "Principle amount cannot be greater than the FSV"), enMsgBoxStyle.Information)
                        txtLoanAmt.Focus()
                        Return False
                    End If
                    'Hemant (13 Dec 2022) -- End

                    'Pinkal (17-May-2024) -- End
                    End If 'Hemant (17 Jan 2025)

                End If

                If objLoanScheme._IsDisableEmployeeConfirm = False AndAlso objEmployee._Confirmation_Date >= eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate) Then
                    'Hemant (22 Nov 2024) -- [objLoanScheme._IsDisableEmployeeConfirm = False]
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Sorry, you are not confirmed yet. Please contact HR support"), enMsgBoxStyle.Information)
                    Return False
                End If

                dsData = objEmployeeDates.GetList("List", enEmp_Dates_Transaction.DT_TERMINATION, _
                                                     FinancialYear._Object._Database_Start_Date.Date, _
                                                     CInt(cboEmpName.SelectedValue))

                If dsData IsNot Nothing AndAlso dsData.Tables("List").Rows.Count > 0 Then
                    If DateDiff(DateInterval.Day, CDate(dsData.Tables("List").Rows(0).Item("effectivedate")), CDate(dsData.Tables("List").Rows(0).Item("date1"))) <= 180 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry, your contract duration is less than 6 months. Please contact HR support"), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                'Hemant (17 Mar 2023) -- Start
                'ISSUE/EHANCEMENT(NMB) : Please keep it for exceptional. Its causing us trouble in CRB
                'Dim dsSchemeCategoryMappring As DataSet = objLoanCategoryMapping.GetList("List", CInt(cboLoanSchemeCategory.SelectedValue))
                'If CInt(cboLoanSchemeCategory.SelectedValue) > 0 AndAlso dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 AndAlso CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("identitytypeunkid")) > 0 Then
                '    Dim objIdentityTran As New clsIdentity_tran
                '    objIdentityTran._EmployeeUnkid = CInt(cboEmpName.SelectedValue)
                '    Dim dtIdentityType As DataTable = objIdentityTran._DataList
                '    Dim drIdentityType() As DataRow = dtIdentityType.Select("idtypeunkid=" & CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("identitytypeunkid")))
                '    If drIdentityType.Length <= 0 Then
                '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Sorry, NIDA number is missing or in pending state"), enMsgBoxStyle.Information)
                '        Return False
                '    End If
                '    objIdentityTran = Nothing
                'End If
                'Hemant (17 Mar 2023) -- End 


                If CInt(cboLoanSchemeCategory.SelectedValue) > 0 AndAlso dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 AndAlso CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("chargestatusunkid")) > 0 Then
                    Dim dsDisplinaryFile As New DataSet
                    dsDisplinaryFile = objDiscFileMaster.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", , " hrdiscipline_file_master.involved_employeeunkid = '" & CInt(cboEmpName.SelectedValue) & "' AND A.StatusId = " & CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("chargestatusunkid")) & " ", False)
                    If dsDisplinaryFile.Tables(0).Rows.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, you have a pending disciplinary issue. Kindly contact HR ER."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                Dim dsProceedingMaster As New DataSet
                dsProceedingMaster = objProceedingMaster.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, _
                                                     True, ConfigParameter._Object._IsIncludeInactiveEmp, "List", " hrdiscipline_file_master.involved_employeeunkid = " & CInt(cboEmpName.SelectedValue) & "", ConfigParameter._Object._AddProceedingAgainstEachCount)
                Dim drProceedingMaster() As DataRow = dsProceedingMaster.Tables(0).Select("penalty_expirydate >= '" & ConfigParameter._Object._CurrentDateAndTime.Date & "' ")
                If drProceedingMaster.Length > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, you have a pending disciplinary issue. Kindly contact HR ER."), enMsgBoxStyle.Information)
                    Return False
                End If

                If CInt(nudDuration.Value) > 1 Then
                    If IsDBNull(objEmployee._Empl_Enddate) = False AndAlso objEmployee._Empl_Enddate <> Nothing Then
                        Dim dtLoanEnd As Date = mdtPeriodStart.AddMonths(CInt(nudDuration.Value)).AddDays(-1)
                        'Hemant (27 Oct 2023) -- Start
                        'ISSUE(NMB): Showing Message "Sorry, installments cannot go beyond the end of contract date" While Approving Flexcube Loan
                        'Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, mdtPeriodStart, objEmployee._Empl_Enddate.Date.AddDays(1)))
                        Dim intEmpTenure As Integer = Math.Abs((mdtPeriodStart.Month - objEmployee._Empl_Enddate.Date.Month) + 12 * (mdtPeriodStart.Year - objEmployee._Empl_Enddate.Date.Year)) + 1
                        'Hemant (27 Oct 2023) -- End
                        If CInt(nudDuration.Value) > intEmpTenure Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 28, "Sorry, installments cannot go beyond the end of contract date"), enMsgBoxStyle.Information)
                            If nudDuration.Enabled = True Then nudDuration.Focus()
                            Return False

                        End If
                    End If

                    If IsDBNull(objEmployee._Termination_To_Date) = False AndAlso objEmployee._Termination_To_Date <> Nothing Then
                        Dim intEmpTenure As Integer = CInt(DateDiff(DateInterval.Month, mdtPeriodStart, objEmployee._Termination_To_Date.Date.AddMonths(-3).AddDays(1)))
                        If CInt(nudDuration.Value) > intEmpTenure Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, no of installments selected cannot go beyond the retirement date"), enMsgBoxStyle.Information)
                            If nudDuration.Enabled = True Then nudDuration.Focus()
                            Return False

                        End If
                    End If
                End If
            End If

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-996 -	As a user, I want to have only one pending loan application at a time. System should not allow application of another loan if there’s a pending entry in the system regardless of the loan scheme
            Dim dsProcessPendingLoan As New DataSet
            strFilter = " lnloan_process_pending_loan.employeeunkid = " & CInt(cboEmpName.SelectedValue) & " AND lnloan_process_pending_loan.loan_statusunkid = " & enLoanApplicationStatus.PENDING & ""
            If menAction = enAction.EDIT_ONE Then
                strFilter &= " AND lnloan_process_pending_loan.processpendingloanunkid <> " & mintProcessPendingLoanUnkid & " "
            End If
            dsProcessPendingLoan = objProcesspendingloan.GetList(FinancialYear._Object._DatabaseName, _
                                                                     User._Object._Userunkid, _
                                                                     FinancialYear._Object._YearUnkid, _
                                                                     Company._Object._Companyunkid, _
                                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                                     True, _
                                                                     ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                     "Loan", , strFilter)
            If dsProcessPendingLoan IsNot Nothing AndAlso dsProcessPendingLoan.Tables(0).Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 48, "Sorry, you can not apply for this loan scheme: Reason, Approval for Another Loan is still in Pending Stage."), enMsgBoxStyle.Information)
                Return False
            End If
            dsProcessPendingLoan = Nothing
            strFilter = ""
            'Hemant (27 Oct 2022) -- End

            If mstrCompanyGroupName = "NMB PLC" AndAlso objEmployee._EmpSignature Is Nothing Then
                'Hemant (18 Mar 2024) -- [mstrCompanyGroupName = "NMB PLC"]
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sorry, signature missing. Please update signature from your profile"), enMsgBoxStyle.Information)
                Return False
            End If


            If mstrCompanyGroupName = "NMB PLC" AndAlso chkconfirmSign.Checked = False Then
                'Hemant (18 Mar 2024) -- [mstrCompanyGroupName = "NMB PLC"]
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 31, "Sorry, Please confirm your signature first."), enMsgBoxStyle.Information)
                Return False
            End If

            'Pinkal (20-Sep-2022) -- Start 
            'NMB Loan Module Enhancement.
            If ConfigParameter._Object._LoanIntegration = enLoanIntegration.FlexCube AndAlso ConfigParameter._Object._RoleBasedLoanApproval Then
                Dim objRoleMapping As New clsloanscheme_role_mapping

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'Dim dsList As DataSet = objRoleMapping.GetList("List", True, " AND lnloanscheme_role_mapping.isexceptional = " & CInt(IIf(chkMakeExceptionalApplication.Checked, 1, 0)) & " AND lnloanscheme_role_mapping.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue), Nothing)
                Dim dsList As DataSet = objRoleMapping.GetList("List", True, False, " AND lnloanscheme_role_mapping.isexceptional = " & CInt(IIf(chkMakeExceptionalApplication.Checked, 1, 0)) & " AND lnloanscheme_role_mapping.loanschemeunkid = " & CInt(cboLoanScheme.SelectedValue), Nothing)
                'Pinkal (04-Aug-2023) -- End

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 45, "Loan Scheme is not mapped with Role and Level.please map Loan scheme with role and level."), enMsgBoxStyle.Information)
                    objRoleMapping = Nothing
                    Return False
                End If
                objRoleMapping = Nothing

                'Pinkal (12-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                Dim objLoanSchemeRoleMapping As New clsloanscheme_role_mapping

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'Dim dtTable As DataTable = objLoanSchemeRoleMapping.GetNextApprovers(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting _
                '                                                                                           , enUserPriviledge.AllowToApproveLoan, ConfigParameter._Object._CurrentDateAndTime.Date, User._Object._Userunkid, CInt(cboEmpName.SelectedValue), CInt(cboLoanScheme.SelectedValue) _
                '                                                                                           , chkMakeExceptionalApplication.Checked, CDec(txtOutStandingPrincipalAmt.Text) + CDec(txtLoanAmt.Text), Nothing, "")

                Dim dtTable As DataTable = objLoanSchemeRoleMapping.GetNextApprovers(FinancialYear._Object._DatabaseName, Company._Object._Companyunkid, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting _
                                                                                                            , enUserPriviledge.AllowToApproveLoan, ConfigParameter._Object._CurrentDateAndTime.Date, User._Object._Userunkid, CInt(cboEmpName.SelectedValue), CInt(cboLoanScheme.SelectedValue) _
                                                                                                           , chkMakeExceptionalApplication.Checked, CDec(txtOutStandingPrincipalAmt.Text) + CDec(txtLoanAmt.Text), False, Nothing, "")
                'Pinkal (04-Aug-2023) -- End


                If mblnRequiredReportingToApproval = False AndAlso mblnSkipApprovalFlow = False AndAlso dtTable IsNot Nothing AndAlso dtTable.Rows.Count <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 55, "You cannot apply for this loan application.Reason : Loan application's loan amount is not match with the approver level's minimum and maximum range. "), enMsgBoxStyle.Information)
                    objLoanSchemeRoleMapping = Nothing
                    Return False
                End If
                objLoanSchemeRoleMapping = Nothing
                'Pinkal (12-Oct-2022) -- End

            End If
            'Pinkal (20-Sep-2022) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            If mstrCompanyGroupName = "TADB" Then
                Dim intPrevPeriod As Integer = (New clsMasterData).getCurrentPeriodID(CInt(enModuleReference.Payroll), mdtPeriodStart.AddDays(-1), 0, 0, True, False, Nothing, False)
                Dim dtStart As Date = mdtPeriodStart
                Dim dtEnd As Date = mdtPeriodEnd
                Dim objPeriod As New clscommom_period_Tran
                Dim objCompany As New clsCompany_Master
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intPrevPeriod
                Dim intyearid As Integer = objPeriod._Yearunkid
                Dim dsCompany As DataSet = objCompany.GetFinancialYearList(Company._Object._Companyunkid, -1, "List", intyearid)

                Dim strDBName As String = FinancialYear._Object._DatabaseName
                If dsCompany IsNot Nothing AndAlso dsCompany.Tables(0).Rows.Count > 0 Then
                    strDBName = dsCompany.Tables(0).Rows(0).Item("database_name").ToString
                End If

                objPeriod = Nothing
                objCompany = Nothing

                Dim dsPayrollData As DataSet = (New clsPayrollProcessTran).GetList(strDBName, User._Object._Userunkid, intyearid, Company._Object._Companyunkid, dtStart, dtEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", CInt(cboEmpName.SelectedValue), intPrevPeriod)

                Dim decGrossPay As Decimal = 0

                Dim drGrossPay() As DataRow = dsPayrollData.Tables(0).Select(" trnheadcode = '061' ")

                If drGrossPay.Length > 0 Then
                    decGrossPay = CDec(drGrossPay.CopyToDataTable.Compute("SUM(amount)", ""))
                End If

                Dim decTotalDeduction As Decimal = 0
                Dim drTotalDeduction() As DataRow = dsPayrollData.Tables(0).Select(" trnheadcode = '060' ")
                If drTotalDeduction.Length > 0 Then
                    decTotalDeduction = CDec(drTotalDeduction.CopyToDataTable.Compute("SUM(amount)", ""))
                End If


                Dim decExcludeDeduction As Decimal = 0
                If objLoanScheme._Name.Trim.ToString() = "Personal Loan" AndAlso (CDec(txtOutStandingPrincipalAmt.Text) > 0 OrElse CDec(txtOutStandingInterestAmt.Text) > 0) Then
                    Dim decPersonalLoanInstallmentAmt As Decimal = 0
                    Dim drPersonalLoan() As DataRow = dsPayrollData.Tables(0).Select(" trnheadname = 'Personal Loan INSTALLMENT' ")
                    If drPersonalLoan.Length > 0 Then
                        decPersonalLoanInstallmentAmt = CDec(drPersonalLoan.CopyToDataTable.Compute("SUM(amount)", ""))
                    End If
                    decExcludeDeduction = decExcludeDeduction + decPersonalLoanInstallmentAmt

                    If CBool(chkSalaryAdvanceLiquidation.Checked) = True Then
                        Dim decSalaryAdvanceInstallmentAmt As Decimal = 0
                        Dim drSalaryAdvance() As DataRow = dsPayrollData.Tables(0).Select(" trnheadname = 'Salary Advance INSTALLMENT' ")
                        If drSalaryAdvance.Length > 0 Then
                            decSalaryAdvanceInstallmentAmt = CDec(drSalaryAdvance.CopyToDataTable.Compute("SUM(amount)", ""))
                        End If
                        decExcludeDeduction = decExcludeDeduction + decSalaryAdvanceInstallmentAmt
                    End If

                End If

                decTotalDeduction = decTotalDeduction + CDec(txtInstallmentAmt.Decimal) - decExcludeDeduction

                Dim decMonthlyDedPerc As Decimal = 0
                If decGrossPay > 0 Then
                    decMonthlyDedPerc = decTotalDeduction * 100 / decGrossPay
                End If

                If decMonthlyDedPerc > 66.67 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 81, "You cannot apply for this loan application.Reason : Percentage(%) of total monthly deduction to gross pay exceed 66.67%."), enMsgBoxStyle.Information)
                    Return False
                End If
            End If
            'Hemant (22 Nov 2024) -- End


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        Finally
            objEmployee = Nothing
            objEmployeeDates = Nothing
            objDiscFileMaster = Nothing
            objProceedingMaster = Nothing
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-988 - As a user, I don’t want the system to allow saving the application for loans marked as post to flexcube and no top-up option set If there’s an already running loan in flexcube for the same product code.
            objLoanScheme = Nothing
            'Hemant (27 Oct 2022) -- End
        End Try
    End Function

    Private Sub Send_Notification()
        Try
            If objLoamEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In objLoamEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = -1
                    objSendMail._OperationModeId = enLogin_Mode.DESKTOP
                    objSendMail._UserUnkid = User._Object._Userunkid
                    objSendMail._SenderAddress = User._Object._Email
                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.LOAN_MGT
                    Try
                        objSendMail.SendMail(Company._Object._Companyunkid)
                    Catch ex As Exception
                    End Try
                Next
                objLoamEmailList.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Send_Notification", mstrModuleName)
        Finally
            If objLoamEmailList.Count > 0 Then
                objLoamEmailList.Clear()
            End If
        End Try
    End Sub

    Private Sub Calculate_Projected_Loan_Principal(ByVal eLnCalcType As enLoanCalcId _
                                                 , ByVal eIntRateCalcTypeID As enLoanInterestCalcType _
                                                 , Optional ByVal blnIsDurationChange As Boolean = False _
                                                 )
        Try
            Dim objLoan_Advance As New clsLoan_Advance
            Dim decPrincipalAmount As Decimal = 0
            Dim decInstrAmount As Decimal = 0
            Dim decTotInstallmentAmount As Decimal = 0
            Dim decTotIntrstAmount As Decimal = 0
            Dim dtEndDate As Date = DateAdd(DateInterval.Month, nudDuration.Value, Now.Date)
            Dim dtFPeriodStart As Date = dtEndDate
            Dim dtFPeriodEnd As Date = dtEndDate
            If mintDeductionPeriodUnkId > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintDeductionPeriodUnkId
                dtFPeriodStart = objPeriod._Start_Date
                dtFPeriodEnd = objPeriod._End_Date
            End If
            objLoan_Advance.Calulate_Projected_Loan_Principal(txtMaxLoanAmount.Decimal _
                                                             , txtMaxInstallmentAmt.Decimal _
                                                            , CInt(DateDiff(DateInterval.Day, Now.Date, dtEndDate.Date)) _
                                                            , txtLoanRate.Decimal _
                                                            , eLnCalcType _
                                                            , eIntRateCalcTypeID _
                                                            , CInt(nudMaxInstallment.Value) _
                                                            , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
                                                            , Convert.ToDecimal(IIf(eLnCalcType = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, txtAppliedUptoPrincipalAmt.Decimal, txtMaxInstallmentAmt.Decimal)) _
                                                            , decInstrAmount _
                                                            , decPrincipalAmount _
                                                            , decTotIntrstAmount _
                                                            , decTotInstallmentAmount _
                                                            )

            'RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            'txtInstallmentAmt.Tag = decInstallmentAmount
            'txtInstallmentAmt.Decimal = CDec(Format(decInstallmentAmount, GUI.fmtCurrency))
            'AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            RemoveHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            If decPrincipalAmount > txtMaxLoanAmount.Decimal Then
                txtAppliedUptoPrincipalAmt.Text = Format(txtMaxLoanAmount.Decimal, GUI.fmtCurrency)
            Else
                If Math.Abs(decPrincipalAmount - txtMaxLoanAmount.Decimal) > 2 Then
                    txtAppliedUptoPrincipalAmt.Text = Format(decPrincipalAmount, GUI.fmtCurrency)
                End If
            End If
            AddHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
            RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            If decPrincipalAmount > txtMaxLoanAmount.Decimal Then
                txtLoanAmt.Text = Format(txtMaxLoanAmount.Decimal, GUI.fmtCurrency)
            Else
                If Math.Abs(decPrincipalAmount - txtMaxLoanAmount.Decimal) > 2 Then
                    txtLoanAmt.Text = Format(decPrincipalAmount, GUI.fmtCurrency)
                End If
            End If
            If Math.Abs(decPrincipalAmount - txtMaxLoanAmount.Decimal) > 2 Then
                Call txtLoanAmt_TextChanged(txtLoanAmt, New System.EventArgs)
            End If
            AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            txtIntAmt.Text = Format(CDec(txtLoanAmt.Decimal) - CDec(txtAppliedUptoPrincipalAmt.Text), GUI.fmtCurrency)

            objLoan_Advance = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Calculate_Projected_Loan_Balance", mstrModuleName)
        Finally
        End Try
    End Sub



    Private Sub Calculate_Projected_Loan_Balance(ByVal eLnCalcType As enLoanCalcId _
                                                 , ByVal eIntRateCalcTypeID As enLoanInterestCalcType _
                                                 , Optional ByVal blnIsDurationChange As Boolean = False _
                                                 )
        Try
            Dim objLoan_Advance As New clsLoan_Advance
            Dim decInstallmentAmount As Decimal = 0
            Dim decInstrAmount As Decimal = 0
            Dim decTotInstallmentAmount As Decimal = 0
            Dim decTotIntrstAmount As Decimal = 0
            Dim dtEndDate As Date = DateAdd(DateInterval.Month, nudDuration.Value, Now.Date)
            Dim dtFPeriodStart As Date = dtEndDate
            Dim dtFPeriodEnd As Date = dtEndDate
            If mintDeductionPeriodUnkId > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = mintDeductionPeriodUnkId
                dtFPeriodStart = objPeriod._Start_Date
                dtFPeriodEnd = objPeriod._End_Date
            End If
            objLoan_Advance.Calulate_Projected_Loan_Balance(txtLoanAmt.Decimal _
                                                            , CInt(DateDiff(DateInterval.Day, Now.Date, dtEndDate.Date)) _
                                                            , txtLoanRate.Decimal _
                                                            , eLnCalcType _
                                                            , eIntRateCalcTypeID _
                                                            , CInt(nudDuration.Value) _
                                                            , CInt(DateDiff(DateInterval.Day, dtFPeriodStart.Date, dtFPeriodEnd.Date.AddDays(1))) _
                                                            , txtInstallmentAmt.Decimal _
                                                            , decInstrAmount _
                                                            , decInstallmentAmount _
                                                            , decTotIntrstAmount _
                                                            , decTotInstallmentAmount _
                                                            )

            RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
            txtInstallmentAmt.Tag = decInstallmentAmount
            txtInstallmentAmt.Decimal = CDec(Format(decInstallmentAmount, GUI.fmtCurrency))
            AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged


            txtIntAmt.Text = Format(decInstrAmount, GUI.fmtCurrency)

            objLoan_Advance = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Calculate_Projected_Loan_Balance", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillDocumentTypeCombo()
        Dim objCommonMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Dim dt As New DataTable
        Dim strFilter As String = String.Empty
        Try
            dsCombos = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")
            If mstrDocumentTypeIDs.Trim.Length > 0 Then
                strFilter = "masterunkid in ( 0," & mstrDocumentTypeIDs & " )"
            Else
                strFilter = "masterunkid in ( 0 )"
            End If
            Dim drRow() As DataRow = dsCombos.Tables(0).Select(strFilter)
            If drRow.Length > 0 Then
                dt = drRow.CopyToDataTable
            End If
            With cboDocumentType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dt
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDocumentTypeCombo", mstrModuleName)
        Finally
            objCommonMaster = Nothing
        End Try
    End Sub

    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
    Private Sub FillOtherDocumentTypeCombo()
        Dim objCommonMaster As New clsCommon_Master
        Dim dsCombos As New DataSet
        Dim dt As New DataTable
        Dim strFilter As String = String.Empty
        Try
            dsCombos = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES, True, "List")

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            'If mintOtherDocumentunkid > 0 Then
            '    strFilter = "masterunkid in ( 0," & mintOtherDocumentunkid & " )"
            'Else
            '    strFilter = "masterunkid in ( 0 )"
            'End If
            If mstrOtherDocumentIds.Trim.Length > 0 Then
                strFilter = "masterunkid in ( 0," & mstrOtherDocumentIds & " )"
            Else
                strFilter = "masterunkid in ( 0 )"
            End If
            'Pinkal (23-Nov-2022) -- End


            Dim drRow() As DataRow = dsCombos.Tables(0).Select(strFilter)
            If drRow.Length > 0 Then
                dt = drRow.CopyToDataTable
            End If
            With cboOtherDocumentType
                .ValueMember = "masterunkid"
                .DisplayMember = "name"
                .DataSource = dt
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillOtherDocumentTypeCombo", mstrModuleName)
        Finally
            objCommonMaster = Nothing
        End Try
    End Sub
    'Hemant (27 Oct 2022) -- End


    Private Sub AddDocumentAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtLoanApplicationDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtLoanApplicationDocument.NewRow
                dRow("scanattachtranunkid") = -1

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'dRow("employeeunkid") = -1
                dRow("employeeunkid") = CInt(cboEmpName.SelectedValue)
                'Pinkal (04-Aug-2023) -- End

                dRow("documentunkid") = CInt(cboDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Loan_Balance
                dRow("scanattachrefid") = enScanAttactRefId.LOAN_ADVANCE
                dRow("form_name") = mstrForm_Name
                dRow("userunkid") = User._Object._Userunkid
                dRow("transactionunkid") = mintProcessPendingLoanUnkid
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = DateTime.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                dRow("attachment_type") = cboDocumentType.Text
                'Pinkal (17-May-2024) -- End


                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                Dim bytes() As Byte
                bytes = Encoding.ASCII.GetBytes(strfullpath)
                Dim xDocumentData As Byte() = bytes
                Try
                    dRow("DocBase64Value") = Convert.ToBase64String(xDocumentData)
                Catch ex As FormatException
                    DisplayError.Show("-1", ex.Message, "AddDocumentAttachment", mstrModuleName)
                End Try
                Dim objFile As New FileInfo(strfullpath)
                dRow("fileextension") = objFile.Extension.ToString().Replace(".", "")
                objFile = Nothing
                dRow("file_data") = xDocumentData
                'Pinkal (04-Aug-2023) -- End
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            mdtLoanApplicationDocument.Rows.Add(dRow)
            Call FillLoanApplicationAttachment()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddDocumentAttachment", mstrModuleName)
        End Try
    End Sub

    Private Sub FillLoanApplicationAttachment()
        Dim dtView As DataView
        Try
            If mdtLoanApplicationDocument Is Nothing Then Exit Sub

            dtView = New DataView(mdtLoanApplicationDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvLoanApplicationAttachment.AutoGenerateColumns = False

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            colhDocType.DataPropertyName = "attachment_type"
            'Pinkal (17-May-2024) -- End

            colhName.DataPropertyName = "filename"
            colhSize.DataPropertyName = "filesize"
            objcolhGUID.DataPropertyName = "GUID"
            objcolhScanUnkId.DataPropertyName = "scanattachtranunkid"
            dgvLoanApplicationAttachment.DataSource = dtView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLoanApplicationAttachment", mstrModuleName)
        End Try
    End Sub

    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
    Private Sub AddDocumentOtherAttachment(ByVal f As System.IO.FileInfo, ByVal strfullpath As String)
        Dim dRow As DataRow
        Try
            Dim dtRow() As DataRow = mdtOtherLoanApplicationDocument.Select("filename = '" & f.Name & "' AND AUD <> 'D' ")
            If dtRow.Length <= 0 Then
                dRow = mdtOtherLoanApplicationDocument.NewRow
                dRow("scanattachtranunkid") = -1

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
                'dRow("employeeunkid") = -1
                dRow("employeeunkid") = CInt(cboEmpName.SelectedValue)
                'Pinkal (04-Aug-2023) -- End

                dRow("documentunkid") = CInt(cboOtherDocumentType.SelectedValue)
                dRow("modulerefid") = enImg_Email_RefId.Loan_Balance
                dRow("scanattachrefid") = enScanAttactRefId.LOAN_ADVANCE
                dRow("form_name") = mstrForm_Name
                dRow("userunkid") = User._Object._Userunkid
                dRow("transactionunkid") = mintProcessPendingLoanUnkid
                dRow("file_path") = ""
                dRow("filename") = f.Name
                dRow("filesize") = objDocument.ConvertFileSize(f.Length)
                dRow("attached_date") = DateTime.Now.Date
                dRow("localpath") = strfullpath
                dRow("GUID") = Guid.NewGuid()
                dRow("AUD") = "A"
                dRow("filesize_kb") = f.Length / 1024

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                dRow("attachment_type") = cboOtherDocumentType.Text
                'Pinkal (17-May-2024) -- End

            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Selected information is already present for particular employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            mdtOtherLoanApplicationDocument.Rows.Add(dRow)
            Call FillLoanApplicationOtherAttachment()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AddDocumentAttachment", mstrModuleName)
        End Try
    End Sub

    Private Sub FillLoanApplicationOtherAttachment()
        Dim dtView As DataView
        Try
            If mdtOtherLoanApplicationDocument Is Nothing Then Exit Sub

            dtView = New DataView(mdtOtherLoanApplicationDocument, "AUD <> 'D' ", "", DataViewRowState.CurrentRows)
            dgvLoanApplicationOtherAttachment.AutoGenerateColumns = False

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            colhOtherDocType.DataPropertyName = "attachment_type"
            'Pinkal (17-May-2024) -- End


            colhOtherName.DataPropertyName = "filename"
            colhOtherSize.DataPropertyName = "filesize"
            objcolhOtherGUID.DataPropertyName = "GUID"
            objcolhOtherScanUnkId.DataPropertyName = "scanattachtranunkid"
            dgvLoanApplicationOtherAttachment.DataSource = dtView
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillLoanApplicationAttachment", mstrModuleName)
        End Try
    End Sub

    Private Sub AttachementLocationChange()
        Try
            If pnlScanAttachment.Visible = True AndAlso pnlOtherScanAttachment.Visible = True Then
                pnlScanAttachment.Location = New System.Drawing.Point(715, 30)
                pnlOtherScanAttachment.Location = New System.Drawing.Point(714, 192)
            ElseIf pnlScanAttachment.Visible = False AndAlso pnlOtherScanAttachment.Visible = True Then
                pnlOtherScanAttachment.Location = New System.Drawing.Point(715, 30)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AttachementLocationChange", mstrModuleName)
        End Try
    End Sub
    'Hemant (27 Oct 2022) -- End

    'Hemant (25 Nov 2022) -- Start
    'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
    Private Sub GetEmployeeHistoryData(ByVal intEmployeeId As Integer, ByRef intCategorizationTranunkid As Integer, ByRef intTransferunkid As Integer, ByRef intAtEmployeeunkid As Integer)
        Dim objCategorize As New clsemployee_categorization_Tran
        Dim objETaransfer As New clsemployee_transfer_tran
        Dim objEmployee As New clsEmployee_Master
        Dim dsCategorize As New DataSet
        Dim dsTransfer As New DataSet
        Dim dsMovement As New DataSet
        Try

            dsCategorize = objCategorize.Get_Current_Job(ConfigParameter._Object._CurrentDateAndTime.Date, intEmployeeId)
            If dsCategorize.Tables(0).Rows.Count > 0 Then
                intCategorizationTranunkid = CInt(dsCategorize.Tables(0).Rows(0).Item("categorizationtranunkid"))
            Else
                intCategorizationTranunkid = -1
            End If
            dsTransfer = objETaransfer.Get_Current_Allocation(ConfigParameter._Object._CurrentDateAndTime.Date, intEmployeeId)
            If dsTransfer.Tables(0).Rows.Count > 0 Then
                intTransferunkid = CInt(dsTransfer.Tables(0).Rows(0).Item("transferunkid"))
            Else
                intTransferunkid = -1
            End If

            Dim dtMovement As New DataTable
            dsMovement = objEmployee.GetMovement_Log(intEmployeeId, "Log")
            dtMovement = dsMovement.Tables(0).Copy
            dtMovement.DefaultView.Sort = "Date DESC, atemployeeunkid DESC"
            dtMovement = dtMovement.DefaultView.ToTable()
            If dtMovement.Rows.Count > 0 Then
                intAtEmployeeunkid = CInt(dtMovement.Rows(0).Item("atemployeeunkid"))
            Else
                intAtEmployeeunkid = -1
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeHistoryData", mstrModuleName)
        Finally
            objCategorize = Nothing
            objETaransfer = Nothing
            objEmployee = Nothing
        End Try
    End Sub
    'Hemant (25 Nov 2022) -- End

    'Pinkal (02-Jun-2023) -- Start
    'NMB Enhancement : TOTP Related Changes .
    Private Sub Save()
        Dim btnFlag As Boolean = False
        Try
            Call SetValue()

            If mblnIsAttachmentRequired = True Then
                If mdtLoanApplicationDocument Is Nothing OrElse mdtLoanApplicationDocument.Select("AUD <> 'D'").Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If

                Dim arrDocumentTypeID() As String = mstrDocumentTypeIDs.Split(CChar(","))
                For Each intDocumentTypeID As Integer In arrDocumentTypeID
                    'Pinkal (17-May-2024) -- Start
                    'NMB Enhancement For Mortgage Loan.
                    Dim mblnOptionalAttachmentForFlexcubeLoan As Boolean = False
                    If CInt(cboLoanSchemeCategory.SelectedValue) = enLoanSchemeCategories.MORTGAGE Then
                        Dim objCommon As New clsCommon_Master
                        objCommon._Masterunkid = intDocumentTypeID
                        mblnOptionalAttachmentForFlexcubeLoan = objCommon._OptionalAttachmentForFlexcubeLoanApplication
                        objCommon = Nothing
                    End If

                    If mblnOptionalAttachmentForFlexcubeLoan = False Then
                    Dim drDocumentRow() As DataRow = mdtLoanApplicationDocument.Select("AUD <> 'D' AND documentunkid = " & intDocumentTypeID & "")
                    If drDocumentRow.Length <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, "All Scan/Document(s) are mandatory information. Please attach all Scan/Document(s) in order to perform operation."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                    End If
                    'Pinkal (17-May-2024) -- End
                Next
            End If
            'Pinkal (17-May-2024) -- End

            'Hemant (13 Dec 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - NMB want the document attachment on exceptional applications made mandatory.
            If chkMakeExceptionalApplication.Checked = True AndAlso mstrOtherDocumentIds.Trim.Length > 0 Then
                If mdtOtherLoanApplicationDocument Is Nothing OrElse mdtOtherLoanApplicationDocument.Select("AUD <> 'D'").Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation."), enMsgBoxStyle.Information)
                    Exit Sub
                End If
            End If
            'Hemant (13 Dec 2022) -- End

            Me.Cursor = Cursors.WaitCursor

            'Hemant (25 Nov 2022) -- Start
            'ENHANCEMENT(NMB) : A1X-353 - As a user, I want to have offer letter reports for the loan scheme name - General loan
            Dim intCategorizationTranunkid As Integer
            Dim intTransferunkid As Integer
            Dim intAtEmployeeunkid As Integer
            GetEmployeeHistoryData(CInt(cboEmpName.SelectedValue), intCategorizationTranunkid, intTransferunkid, intAtEmployeeunkid)
            objProcesspendingloan._CategorizationTranunkid = intCategorizationTranunkid
            objProcesspendingloan._Transferunkid = intTransferunkid
            objProcesspendingloan._AtEmployeeunkid = intAtEmployeeunkid
            'Hemant (25 Nov 2022) -- End

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            'mdtLoanApplicationDocument.Merge(mdtOtherLoanApplicationDocument)
            mdtFinalLoanApplicationDocument.Rows.Clear()
            mdtFinalLoanApplicationDocument.Merge(mdtLoanApplicationDocument)
            mdtFinalLoanApplicationDocument.Merge(mdtOtherLoanApplicationDocument)
            'Hemant (27 Oct 2022) -- End

            If mdtFinalLoanApplicationDocument.Rows.Count > 0 Then

                Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                Dim strError As String = ""

                For Each drdoc_Row As DataRow In mdtFinalLoanApplicationDocument.Rows
                    If drdoc_Row("AUD").ToString = "A" AndAlso drdoc_Row("localpath").ToString <> "" Then
                        Dim strFileName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & Path.GetExtension(CStr(drdoc_Row("localpath")))
                        If blnIsIISInstalled Then
                            If clsFileUploadDownload.UploadFile(CStr(drdoc_Row("localpath")), mstrFolderName, strFileName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            Else
                                Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                drdoc_Row("fileuniquename") = strFileName
                                If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                    strPath += "/"
                                End If
                                strPath += "uploadimage/" & mstrFolderName & "/" + strFileName
                                drdoc_Row("filepath") = strPath
                                drdoc_Row.AcceptChanges()
                            End If
                        Else
                            If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If Directory.Exists(ConfigParameter._Object._Document_Path & "\" & mstrFolderName) = False Then
                                    Directory.CreateDirectory(ConfigParameter._Object._Document_Path & "\" & mstrFolderName)
                                End If
                                File.Copy(CStr(drdoc_Row("localpath")), strDocLocalPath, True)
                                drdoc_Row("fileuniquename") = strFileName
                                drdoc_Row("filepath") = strDocLocalPath
                                drdoc_Row.AcceptChanges()
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    ElseIf drdoc_Row("AUD").ToString = "D" AndAlso drdoc_Row("fileuniquename").ToString <> "" Then

                        Dim strFileName As String = drdoc_Row("fileuniquename").ToString
                        If blnIsIISInstalled Then
                            If clsFileUploadDownload.DeleteFile(drdoc_Row("filepath").ToString, strFileName, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL, ConfigParameter._Object._Companyunkid) = False Then
                                eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        Else
                            If Directory.Exists(ConfigParameter._Object._Document_Path) Then
                                Dim strDocLocalPath As String = ConfigParameter._Object._Document_Path & "\" & mstrFolderName & "\" & strFileName
                                If File.Exists(strDocLocalPath) Then
                                    File.Delete(strDocLocalPath)
                                End If
                            Else
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Configuration Path does not Exist."), enMsgBoxStyle.Information)
                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If

            objEmailList = New List(Of clsEmailCollection)
            If menAction = enAction.EDIT_ONE Then
                'Pinkal (20-Sep-2022) -- Start
                'NMB Loan Module Enhancement.
                'btnFlag = objProcesspendingloan.Update(True, , mdtLoanApplicationDocument)
                btnFlag = objProcesspendingloan.Update(Company._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid _
                                                                           , Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting _
                                                                           , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, Nothing, mdtFinalLoanApplicationDocument)
                'Pinkal (20-Sep-2022) -- End
            Else
                btnFlag = objProcesspendingloan.Insert(FinancialYear._Object._DatabaseName, _
                                                       User._Object._Userunkid, _
                                                       FinancialYear._Object._YearUnkid, _
                                                       Company._Object._Companyunkid, _
                                                       ConfigParameter._Object._LoanApplicationNoType, _
                                                       ConfigParameter._Object._LoanApplicationPrifix, _
                                                       , mdtFinalLoanApplicationDocument)
            End If

            If btnFlag = False And objProcesspendingloan._Message <> "" Then

                eZeeMsgBox.Show(objProcesspendingloan._Message, enMsgBoxStyle.Information)

            ElseIf btnFlag AndAlso menAction <> enAction.EDIT_ONE Then

                'Pinkal (27-Oct-2022) -- Start
                'NMB Loan Module Enhancement.
                If mblnSkipApprovalFlow = False Then

                    Dim objEmail As New List(Of clsEmailCollection)

                    objProcesspendingloan.Send_NotificationFlexCube_Employee(CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid, objProcesspendingloan._Application_No, _
                                                                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), Company._Object._Companyunkid, _
                                                                                                  cboLoanScheme.Text, objProcesspendingloan._Loan_Statusunkid, "", enLogin_Mode.DESKTOP, 0, User._Object._Userunkid, False, objEmail)

                    objLoamEmailList.AddRange(objEmail)
                    objEmail.Clear()
                    objEmail = Nothing

                    objEmail = New List(Of clsEmailCollection)

                    'Pinkal (21-Mar-2024) -- Start
                    'NMB - Mortgage UAT Enhancements.

                    'objProcesspendingloan.Send_NotificationFlexCube_Approver(Company._Object._DatabaseName, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, objProcesspendingloan._Application_Date.Date, _
                    '                                                                         CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid.ToString(), _
                    '                                                                         ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, enLogin_Mode.DESKTOP, 0, , False, objEmail)

                    objProcesspendingloan.Send_NotificationFlexCube_Approver(Company._Object._DatabaseName, FinancialYear._Object._YearUnkid, ConfigParameter._Object._UserAccessModeSetting, objProcesspendingloan._Application_Date.Date, _
                                                                                             CInt(cboLoanScheme.SelectedValue), CInt(cboEmpName.SelectedValue), objProcesspendingloan._Processpendingloanunkid.ToString(), _
                                                                                           ConfigParameter._Object._ArutiSelfServiceURL, Company._Object._Companyunkid, objProcesspendingloan._Loan_Statusunkid, enLogin_Mode.DESKTOP, 0, , False, objEmail, -999, "")
                    'Pinkal (21-Mar-2024) -- End


                    objLoamEmailList.AddRange(objEmail)
                    objEmail.Clear()
                    objEmail = Nothing

                End If
                'Pinkal (27-Oct-2022) -- End

            End If

            If btnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    'Hemant (27 Oct 2022) -- Start
                    'ENHANCEMENT(NMB) : AC2-991 - As a user, after saving a loan application, I want to have a prompt message captioned, Application submitted successfully.
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 54, "Loan Application Submitted successfully."), enMsgBoxStyle.Information)
                    'Hemant (27 Oct 2022) -- End
                    objProcesspendingloan = Nothing
                    objProcesspendingloan = New clsProcess_pending_loan
                    Call GetValue()
                    cboEmpName.Focus()
                Else
                    'Hemant (27 Oct 2022) -- Start
                    'ENHANCEMENT(NMB) : AC2-991 - As a user, after saving a loan application, I want to have a prompt message captioned, Application submitted successfully.
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 54, "Loan Application Submitted successfully."), enMsgBoxStyle.Information)
                    'Hemant (27 Oct 2022) -- End
                    mintProcessPendingLoanUnkid = objProcesspendingloan._Processpendingloanunkid

                    ''START For Testing Flexcube
                    'Dim objconfig As New clsConfigOptions
                    'Dim mstrOracleHostName As String = objconfig.GetKeyValue(Company._Object._Companyunkid, "OracleHostName", Nothing)
                    'Dim mstrOraclePortNo As String = objconfig.GetKeyValue(Company._Object._Companyunkid, "OraclePortNo", Nothing)
                    'Dim mstrOracleServiceName As String = objconfig.GetKeyValue(Company._Object._Companyunkid, "OracleServiceName", Nothing)
                    'Dim mstrOracleUserName As String = objconfig.GetKeyValue(Company._Object._Companyunkid, "OracleUserName", Nothing)
                    'Dim mstrOracleUserPassword As String = objconfig.GetKeyValue(Company._Object._Companyunkid, "OracleUserPassword", Nothing)
                    'Dim mstrNewLoanRequestFlexcubeURL As String = objconfig.GetKeyValue(Company._Object._Companyunkid, "NewLoanRequestFlexcubeURL", Nothing)
                    'Dim mstrTopupRequestFlexcubeURL As String = objconfig.GetKeyValue(Company._Object._Companyunkid, "TopupRequestFlexcubeURL", Nothing)

                    'If objconfig.IsKeyExist(Company._Object._Companyunkid, "Advance_CostCenterunkid") = True Then
                    '    mstrOracleUserPassword = clsSecurity.Decrypt(mstrOracleUserPassword, "ezee")
                    'End If

                    'objconfig = Nothing

                    'Dim mstrEmpBankAcNo As String = ""
                    'Dim objMasterData As New clsMasterData
                    'Dim mstrError As String = ""
                    'Dim mstrPostedData As String = ""
                    'Dim mstrResponseData As String = ""

                    'If objProcesspendingloan._DeductionPeriodunkid > 0 Then
                    '    Dim dsEmpBank As DataSet = Nothing
                    '    Dim objEmpBankTran As New clsEmployeeBanks
                    '    dsEmpBank = objEmpBankTran.GetList(Company._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStart, mdtPeriodEnd, ConfigParameter._Object._UserAccessModeSetting, True, False, "EmployeeBank", False, "", objProcesspendingloan._Employeeunkid.ToString(), mdtPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")
                    '    If dsEmpBank IsNot Nothing AndAlso dsEmpBank.Tables(0).Rows.Count > 0 Then
                    '        mstrEmpBankAcNo = dsEmpBank.Tables(0).Rows(0)("accountno").ToString()
                    '    End If
                    '    If dsEmpBank IsNot Nothing Then dsEmpBank.Clear()
                    '    dsEmpBank = Nothing
                    'End If

                    'MsgBox("mstrOracleUserName : " & mstrOracleUserName)
                    'MsgBox("mstrOracleUserPassword : " & mstrOracleUserPassword)

                    'Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(mstrOracleUserName.Trim & ":" & mstrOracleUserPassword)
                    'Dim mstrBase64Credential As String = Convert.ToBase64String(byt)

                    'MsgBox("Base 64 Password : " & mstrBase64Credential)
                    'MsgBox("mstrEmpBankAcNo : " & mstrEmpBankAcNo)
                    'MsgBox("mstrNewLoanRequestFlexcubeURL : " & mstrNewLoanRequestFlexcubeURL)
                    'MsgBox("IsTopup : " & objProcesspendingloan._IsTopUp)

                    'If mstrNewLoanRequestFlexcubeURL.Trim.Length > 0 AndAlso mstrBase64Credential.Trim.Length > 0 AndAlso objProcesspendingloan._IsTopUp = False AndAlso mstrEmpBankAcNo.Trim.Length > 0 Then   'NEW LOAN REQUEST
                    '    Dim objLoanFlexCube As New clsLoanFlexCubeIntegration
                    '    Dim objRoleBasedApproval As New clsroleloanapproval_process_Tran
                    '    Dim mstrCustomerNo As String = objRoleBasedApproval.GetFlexCubeEmpCustomerNo(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrEmpBankAcNo)

                    '    MsgBox("mstrCustomerNo : " & mstrCustomerNo)

                    '    objLoanFlexCube.service = "aruti-loan-automation"
                    '    objLoanFlexCube.type = "loan-request"
                    '    objLoanFlexCube.payload.transactionReference = objProcesspendingloan._Application_No
                    '    objLoanFlexCube.payload.applicationNumber = objProcesspendingloan._Application_No
                    '    objLoanFlexCube.payload.accountNumber = mstrEmpBankAcNo
                    '    objLoanFlexCube.payload.loanAccountNumber = Nothing
                    '    objLoanFlexCube.payload.productCode = mstrLoanSchemeCode
                    '    objLoanFlexCube.payload.productCategory = "STAFF"
                    '    objLoanFlexCube.payload.customerNumber = mstrCustomerNo
                    '    'objLoanFlexCube.payload.maturityDate = New Date(objProcesspendingloan._Application_Date.AddMonths(objProcesspendingloan._NoOfInstallment).Year, objProcesspendingloan._Application_Date.AddMonths(objProcesspendingloan._NoOfInstallment).Month, 1).ToString("yyyy-MM-dd")
                    '    objLoanFlexCube.payload.maturityDate = New Date(mdtPeriodStart.AddMonths(objProcesspendingloan._NoOfInstallment).Year, mdtPeriodStart.AddMonths(objProcesspendingloan._NoOfInstallment).Month, 1).ToString("yyyy-MM-dd")
                    '    objLoanFlexCube.payload.loanAmount = objProcesspendingloan._Loan_Amount
                    '    objLoanFlexCube.payload.currency = "TZS"
                    '    objLoanFlexCube.payload.remark = "Loan request"

                    '    objLoanFlexCube.payload.loanAccountNumber = Nothing
                    '    objLoanFlexCube.payload.outStandingPrincipal = Nothing
                    '    objLoanFlexCube.payload.outStandingInterest = Nothing
                    '    objLoanFlexCube.payload.totalOutstanding = Nothing
                    '    objLoanFlexCube.payload.valueDate = Nothing


                    '    If objMasterData.GetSetFlexCubeLoanRequest(mstrNewLoanRequestFlexcubeURL, mstrBase64Credential, objLoanFlexCube, mstrError, mstrPostedData, mstrResponseData) = False Then
                    '        MsgBox("New Loan Request Flexcube Error : " & mstrError)
                    '        Exit Sub
                    '    End If

                    '    objLoanFlexCube = Nothing
                    '    objRoleBasedApproval = Nothing

                    'ElseIf mstrTopupRequestFlexcubeURL.Trim.Length > 0 AndAlso mstrBase64Credential.Trim.Length > 0 AndAlso objProcesspendingloan._IsTopUp AndAlso mstrEmpBankAcNo.Trim.Length > 0 Then 'TOP UP LOAN REQUEST
                    '    Dim objLoanFlexCube As New clsLoanFlexCubeIntegration
                    '    Dim objRoleBasedApproval As New clsroleloanapproval_process_Tran
                    '    Dim objLoanAdvance As New clsLoan_Advance

                    '    Dim mstrCustomerNo As String = objRoleBasedApproval.GetFlexCubeEmpCustomerNo(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrEmpBankAcNo)

                    '    MsgBox("mstrCustomerNo : " & mstrCustomerNo)

                    '    Dim dsFlexcube As DataSet = objLoanAdvance.GetFlexcubeLoanData(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrEmpBankAcNo, mstrLoanSchemeCode, False)

                    '    If dsFlexcube IsNot Nothing AndAlso dsFlexcube.Tables(0).Rows.Count > 0 Then

                    '        objLoanFlexCube.service = "aruti-loan-automation"
                    '        objLoanFlexCube.type = "loan-roll-over"
                    '        objLoanFlexCube.payload.transactionReference = mstrLoanSchemeCode + objProcesspendingloan._Application_No
                    '        objLoanFlexCube.payload.applicationNumber = mstrLoanSchemeCode + objProcesspendingloan._Application_No
                    '        objLoanFlexCube.payload.accountNumber = mstrEmpBankAcNo
                    '        objLoanFlexCube.payload.loanAccountNumber = dsFlexcube.Tables(0).Rows(0)("ACCOUNT_NUMBER").ToString()
                    '        objLoanFlexCube.payload.productCode = mstrLoanSchemeCode
                    '        objLoanFlexCube.payload.customerNumber = mstrCustomerNo
                    '        objLoanFlexCube.payload.outStandingPrincipal = CDec(txtOutStandingPrincipalAmt.Text)
                    '        'objLoanFlexCube.payload.loanAmount =  CDec(txtOutStandingPrincipalAmt.Text) + objProcesspendingloan._Loan_Amount
                    '        objLoanFlexCube.payload.loanAmount = objProcesspendingloan._Loan_Amount
                    '        objLoanFlexCube.payload.totalOutstanding = CDec(txtOutStandingPrincipalAmt.Text) + CDec(txtOutStandingInterestAmt.Text)
                    '        objLoanFlexCube.payload.currency = "TZS"
                    '        'objLoanFlexCube.payload.maturityDate = CDate(dsFlexcube.Tables(0).Rows(0)("MATURITY_DATE").ToString()).ToString("yyyy-MM-dd")
                    '        objLoanFlexCube.payload.maturityDate = New Date(mdtPeriodStart.AddMonths(objProcesspendingloan._NoOfInstallment).Year, mdtPeriodStart.AddMonths(objProcesspendingloan._NoOfInstallment).Month, 1).ToString("yyyy-MM-dd")
                    '        objLoanFlexCube.payload.valueDate = Now.Date.ToString("yyyy-MM-dd")
                    '        objLoanFlexCube.payload.outStandingInterest = CDec(txtOutStandingInterestAmt.Text)
                    '        objLoanFlexCube.payload.remark = "Loan request"

                    '        objLoanFlexCube.payload.productCategory = Nothing


                    '        If objMasterData.GetSetFlexCubeLoanRequest(mstrNewLoanRequestFlexcubeURL, mstrBase64Credential, objLoanFlexCube, mstrError, mstrPostedData, mstrResponseData) = False Then
                    '            MsgBox("Top Up Loan Request Flexcube Error : " & mstrError)
                    '            Exit Sub
                    '        End If

                    '    Else
                    '        MsgBox("There is no data from Flexcube View")
                    '    End If  'If dsFlexcube IsNot Nothing AndAlso dsFlexcube.Tables(0).Rows.Count > 0 Then
                    '    objLoanFlexCube = Nothing
                    '    objRoleBasedApproval = Nothing
                    'End If
                    'objMasterData = Nothing
                    'END 
                    Me.Close()
                End If
                'ts.Enabled = False
                'ts.Stop()
            End If

            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            If objLoamEmailList IsNot Nothing AndAlso objLoamEmailList.Count > 0 Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                trd.Start()
            End If
            'Pinkal (27-Oct-2022) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Save", mstrModuleName)
        End Try
    End Sub
    'Pinkal (02-Jun-2023) -- End

    'Hemant (07 Jul 2023) -- Start
    'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
    Private Function IsNotMortgageLoanExist() As Boolean
        Dim objEmpBankTran As New clsEmployeeBanks
        Dim objloanAdvance As New clsLoan_Advance
        Dim objLoanScheme As New clsLoan_Scheme
        Try
            Dim dsEmpBank As New DataSet
            Dim xPrevPeriodStart As Date
            Dim xPrevPeriodEnd As Date
            If CInt(mintDeductionPeriodUnkId) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(mintDeductionPeriodUnkId)
                xPrevPeriodStart = objPeriod._Start_Date
                xPrevPeriodEnd = objPeriod._End_Date
                objPeriod = Nothing
            Else
                xPrevPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                xPrevPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            dsEmpBank = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", , , CStr(cboEmpName.SelectedValue), xPrevPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

            'call oracle function to get list of loan emis by passing bank account no.
            If dsEmpBank.Tables(0).Rows.Count > 0 Then
                Dim dsFlexLoanData As DataSet = objloanAdvance.GetFlexcubeLoanData(ConfigParameter._Object._OracleHostName, ConfigParameter._Object._OraclePortNo, ConfigParameter._Object._OracleServiceName, ConfigParameter._Object._OracleUserName, ConfigParameter._Object._OracleUserPassword, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString, "CL28", , False)
                'Hemant (29 Mar 2024) -- [dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString]
                'Hemant (01 Sep 2023) -- [blnUseOldView := False]
                If dsFlexLoanData IsNot Nothing AndAlso dsFlexLoanData.Tables(0).Rows.Count > 0 Then
                    Return False
                End If
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsMortgageLoanExist", mstrModuleName)
        Finally
            objEmpBankTran = Nothing
            objloanAdvance = Nothing
            objLoanScheme = Nothing
        End Try
    End Function
    'Hemant (07 Jul 2023) -- End
#End Region

#Region " Form's Events "

    Private Sub frmFlexcubeLoanApplication_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objProcesspendingloan = Nothing
    End Sub

    Private Sub frmFlexcubeLoanApplication_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.Control = True And e.KeyCode = Keys.S Then
            Call btnSave.PerformClick()
        End If
    End Sub

    Private Sub frmFlexcubeLoanApplication_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        If Asc(e.KeyChar) = 27 Then
            Me.Close()
        ElseIf Asc(e.KeyChar) = 13 Then
            Windows.Forms.SendKeys.Send("{Tab}")
            e.Handled = True
        End If
    End Sub

    Private Sub frmFlexcubeLoanApplication_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objProcesspendingloan = New clsProcess_pending_loan
        objDocument = New clsScan_Attach_Documents
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call SetColor()
            Call FillCombo()
            Call FillPeriod()
            Call FillDocumentTypeCombo()
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            Call FillOtherDocumentTypeCombo()
            'Hemant (27 Oct 2022) -- End


            Panel1.Visible = False
            Panel2.Visible = False
            pnlScanAttachment.Visible = False

            mstrApplicationNo = ""
            mdtApplcationDate = ConfigParameter._Object._CurrentDateAndTime.Date

            'Hemant (29 Mar 2024) -- Start
            'ENHANCEMENT(TADB): New loan application form
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            mstrCompanyGroupName = objGroup._Groupname.ToString().ToUpper()
            txtRepaymentDays.Enabled = False
            If mstrCompanyGroupName = "TADB" Then
                txtRepaymentDays.Enabled = True
                lblTotalCIF.Visible = True
                txtTotalCIF.Visible = True

                lblEstimatedTax.Visible = True
                txtEstimatedTax.Visible = True

                lblInvoiceValue.Visible = True
                txtInvoiceValue.Visible = True

                lblModelNo.Visible = True
                txtModelNo.Visible = True

                lblEngineNo.Visible = True
                txtEngineNo.Visible = True

                lblEngineCapacity.Visible = True
                txtEngineCapacity.Visible = True
            End If
            objGroup = Nothing
            'Hemant (29 Mar 2024) -- End

            If menAction = enAction.EDIT_ONE Then
                objProcesspendingloan._Processpendingloanunkid = mintProcessPendingLoanUnkid
                cboEmpName.Enabled = False
                objbtnSearchEmployee.Enabled = False
                chkMakeExceptionalApplication.Enabled = False
                Call GetValue()
            End If

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            'mdtLoanApplicationDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.LOAN_ADVANCE, mintProcessPendingLoanUnkid, ConfigParameter._Object._Document_Path)

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            'mdtFinalLoanApplicationDocument = objDocument.GetQulificationAttachment(-1, enScanAttactRefId.LOAN_ADVANCE, mintProcessPendingLoanUnkid, ConfigParameter._Object._Document_Path)
            mdtFinalLoanApplicationDocument = objDocument.GetQulificationAttachment(CInt(cboEmpName.SelectedValue), enScanAttactRefId.LOAN_ADVANCE, mintProcessPendingLoanUnkid, ConfigParameter._Object._Document_Path)
            'Pinkal (04-Aug-2023) -- End
            mdtLoanApplicationDocument = mdtFinalLoanApplicationDocument.Clone()
            mdtOtherLoanApplicationDocument = mdtFinalLoanApplicationDocument.Clone()

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            'Dim drLoanApplicationDocument() As DataRow = mdtFinalLoanApplicationDocument.Copy.Select("documentunkid <> " & mintOtherDocumentunkid & "")
            If mstrOtherDocumentIds.Trim.Length > 0 Then
                Dim drLoanApplicationDocument() As DataRow = mdtFinalLoanApplicationDocument.Copy.Select("documentunkid NOT IN (" & mstrOtherDocumentIds & ")")
            If drLoanApplicationDocument.Length > 0 Then
                mdtLoanApplicationDocument = drLoanApplicationDocument.CopyToDataTable()
            End If

                'Dim drOtherLoanApplicationDocument() As DataRow = mdtFinalLoanApplicationDocument.Copy.Select("documentunkid = " & mintOtherDocumentunkid & "")
                Dim drOtherLoanApplicationDocument() As DataRow = mdtFinalLoanApplicationDocument.Copy.Select("documentunkid  in (" & mstrOtherDocumentIds & ")")
            If drOtherLoanApplicationDocument.Length > 0 Then
                mdtOtherLoanApplicationDocument = drOtherLoanApplicationDocument.CopyToDataTable()
            End If
                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1158) NMB - Allow to configure up to 5 disbursement tranches for Mortgage loans on the loan approval page.
            Else
                mdtLoanApplicationDocument = mdtFinalLoanApplicationDocument.Copy()
                'Pinkal (04-Aug-2023) -- End
            End If
            'Pinkal (23-Nov-2022) -- End

            mdtFinalLoanApplicationDocument.Rows.Clear()
            'Hemant (27 Oct 2022) -- End
            If menAction <> enAction.EDIT_ONE Then
                If mdtLoanApplicationDocument IsNot Nothing Then mdtLoanApplicationDocument.Rows.Clear()
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                If mdtOtherLoanApplicationDocument IsNot Nothing Then mdtOtherLoanApplicationDocument.Rows.Clear()
                'Hemant (27 Oct 2022) -- End
            End If


            Call FillLoanApplicationAttachment()
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            Call FillLoanApplicationOtherAttachment()
            If chkMakeExceptionalApplication.Checked = True Then
                pnlOtherScanAttachment.Visible = True
            Else
                pnlOtherScanAttachment.Visible = False
            End If
            Call AttachementLocationChange()
            'Hemant (27 Oct 2022) -- End

            mstrFolderName = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.LOAN_ADVANCE).Tables(0).Rows(0)("Name").ToString

            If imgSignature.Image IsNot Nothing AndAlso imgSignature.Image.ToString.Trim.Length > 0 Then
                btnConfirmSign.Visible = True
            Else
                btnConfirmSign.Visible = False
            End If

            cboEmpName.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmFlexcubeLoanApplication_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsProcess_pending_loan.SetMessages()
            objfrm._Other_ModuleNames = "clsProcess_pending_loan"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region "Textbox Events"
    Private Sub txtPrincipalAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAppliedUptoPrincipalAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.No_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Dim decValue As Decimal = 0
                decValue = txtLoanAmt.Decimal / txtAppliedUptoPrincipalAmt.Decimal
                RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                nudDuration.Value = CDec(IIf(decValue <= 0, 1, CDec(Math.Ceiling(decValue))))
                AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.No_Interest_With_Mapped_Head, enIntCalcType)

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtPrincipalAmt_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtInstallmentAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then

                If txtInstallmentAmt.Decimal > 0 Then
                    RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                    Dim dblvalue As Double = 0
                    dblvalue = txtLoanAmt.Decimal / txtInstallmentAmt.Decimal
                    nudDuration.Value = CDec(IIf(dblvalue <= 0, 1, CDec(Math.Ceiling(dblvalue))))

                    AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                    RemoveHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtAppliedUptoPrincipalAmt.Text = Format(txtInstallmentAmt.Decimal, GUI.fmtCurrency)
                    AddHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtIntAmt.Text = Format(0, GUI.fmtCurrency)

                End If

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then

                If txtInstallmentAmt.Decimal > 0 Then
                    RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                    Dim dblvalue As Double = 0
                    dblvalue = txtLoanAmt.Decimal / txtInstallmentAmt.Decimal
                    nudDuration.Value = CDec(IIf(dblvalue <= 0, 1, CDec(Math.Ceiling(dblvalue))))

                    AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                    RemoveHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtAppliedUptoPrincipalAmt.Text = Format(txtInstallmentAmt.Decimal, GUI.fmtCurrency)
                    AddHandler txtAppliedUptoPrincipalAmt.TextChanged, AddressOf txtPrincipalAmt_TextChanged
                    txtIntAmt.Text = Format(0, GUI.fmtCurrency)

                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtInstallmentAmt_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtLoanAmt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanAmt.TextChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType

            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                Dim dblvalue As Double = 0
                nudDuration.Value = CDec(IIf(dblvalue <= 0, 1, CDec(Math.Ceiling(dblvalue))))
                AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                Call nudDuration_ValueChanged(nudDuration, New System.EventArgs)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                Call nudDuration_ValueChanged(nudDuration, New System.EventArgs)

            End If

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            'A1X-2587 NMB mortgage - Insurance deduction: The system should deduct insurance for the first year only and for the following year it will be done yearly. 
            Dim objLScheme As New clsLoan_Scheme
            objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            If objLScheme._LoanSchemeCategoryId = enLoanSchemeCategories.MORTGAGE Then
                If CInt(nudDuration.Value) > 12 Then
                    txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Decimal) * (CDec(txtInsuranceRate.Decimal) / 100), GUI.fmtCurrency)
                Else
                    txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Decimal) * (CDec(txtInsuranceRate.Decimal) / 100) * (CInt(nudDuration.Value) / 12), GUI.fmtCurrency)
                End If
            Else
            txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Decimal) * (CDec(txtInsuranceRate.Decimal) / 100) * (CInt(nudDuration.Value) / 12), GUI.fmtCurrency)
            End If
            objLScheme = Nothing
            'Pinkal (17-May-2024) -- End



            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-949 - As a user, on the application screen, if the loan scheme selected is eligible for top-up, my Take home should be calculated as Principal Amount – (Outstanding Principle + Outstanding Interest + Insurance Amount)
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - CDec(txtInsuranceAmt.Decimal), GUI.fmtCurrency)
            'Hemant (10 Nov 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : For the Take home field in top-up loans. Please return it to calculate as normal loan only.
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - (CDec(txtOutStandingPrincipalAmt.Decimal) + CDec(txtOutStandingInterestAmt.Decimal) + CDec(txtInsuranceAmt.Decimal)), GUI.fmtCurrency)
            'Hemant (13 Dec 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - For top-up loans, they want the Take Home formula to be as below : Take Home = Principle – (Current Outstanding Principle + Current Outstanding Interest + Insurance Amount).
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - CDec(txtInsuranceAmt.Decimal), GUI.fmtCurrency)
            txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - (CDec(txtOutStandingPrincipalAmt.Decimal) + CDec(txtOutStandingInterestAmt.Decimal) + CDec(txtInsuranceAmt.Decimal)) - mdecFinalSalaryAdvancePendingPrincipalAmt, GUI.fmtCurrency)
            'Hemant (22 Nov 2024) -- [- mdecFinalSalaryAdvancePendingPrincipalAmt]

            'Hemant (13 Dec 2022) -- End
            'Hemant (10 Nov 2022) -- End
            'Hemant (12 Oct 2022) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtLoanAmt_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub



    'Private Sub txtLoanAmt_LostFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoanAmt.LostFocus
    '    Try
    '        RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
    '        txtLoanAmt.Text = Format(CDec(txtLoanAmt.Text), GUI.fmtCurrency)
    '        AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtLoanAmt_LostFocus", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            'Pinkal (27-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            If objLoamEmailList.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage("frmNewMDI", 77, "Sending Email(s) process is in progress. Please wait for some time."), enMsgBoxStyle.Information)
                Exit Sub
            End If
            'Pinkal (27-Oct-2022) -- End
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        Try
            If IsValidate() = False Then Exit Sub

            'Pinkal (02-Jun-2023) -- Start
            'NMB Enhancement : TOTP Related Changes .

            If ConfigParameter._Object._ApplyTOTPForLoanApplicationApproval Then

                '/* START CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP
                Dim objAttempts As New clsloantotpattempts_tran
                Dim dsAttempts As DataSet = objAttempts.GetList(ConfigParameter._Object._CurrentDateAndTime.Date, User._Object._Userunkid, 0)
                Dim xOTPRetrivaltime As DateTime = Nothing
                If dsAttempts IsNot Nothing AndAlso dsAttempts.Tables(0).Rows.Count > 0 Then
                    If IsDBNull(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False AndAlso IsNothing(dsAttempts.Tables(0).Rows(0)("otpretrivaltime")) = False Then
                        xOTPRetrivaltime = CDate(dsAttempts.Tables(0).Rows(0)("otpretrivaltime"))
                        If xOTPRetrivaltime > ConfigParameter._Object._CurrentDateAndTime Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 62, "You cannot save this loan application.Reason : As You reached Maximum unsuccessful attempts.Please try after") & " " & ConfigParameter._Object._TOTPRetryAfterMins & " " & Language.getMessage(mstrModuleName, 63, "minutes") & ".", enMsgBoxStyle.Information)
                            objAttempts = Nothing
                    Exit Sub
                End If
                    End If

            End If
                objAttempts = Nothing
                '/* END CHECKING UNSUCCESSFUL ATTEMPTS OF TOTP

                If ConfigParameter._Object._SMSGatewayEmail.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 61, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan application."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If

                Dim objTOTPHelper As New clsTOTPHelper(Company._Object._Companyunkid)
                Dim mstrSecretKey As String = ""

                Dim objUser As New clsUserAddEdit
                Dim xUserId As Integer = objUser.Return_UserId(CInt(cboEmpName.SelectedValue), Company._Object._Companyunkid)
                objUser._Userunkid = xUserId

                If objUser._TOTPSecurityKey.Trim.Length > 0 Then
                    mstrSecretKey = objUser._TOTPSecurityKey
                            Else
                    mstrSecretKey = objTOTPHelper.GenerateSecretKey()
                    objUser._TOTPSecurityKey = mstrSecretKey
                    If objUser.Update(False) = False Then
                        eZeeMsgBox.Show(objUser._Message, enMsgBoxStyle.OkOnly)
                                Exit Sub
                            End If
                        End If
                objUser = Nothing

                Dim objfrm As New frmOTPValidator
                If objfrm.displayDialog(mstrSecretKey, CInt(cboEmpName.SelectedValue), mstrEmployeeName, mstrEmpMobileNo, mstrEmployeeEmail) Then
                    Save()
            End If
                objfrm = Nothing
                objTOTPHelper = Nothing

            Else
                Save()
            End If
            'Pinkal (02-Jun-2023) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub objbtnAddLoanScheme_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddLoanScheme.Click
        Dim frm As New frmLoanScheme_AddEdit
        Dim intLoan_SchemeId As Integer = -1
        Try

            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            If frm.displayDialog(intLoan_SchemeId, enAction.ADD_ONE) Then
                Dim dsCombo As New DataSet
                Dim objLoan As New clsLoan_Scheme
                dsCombo = objLoan.getComboList(True, "LoanScheme")
                With cboLoanScheme
                    .ValueMember = "loanschemeunkid"
                    .DisplayMember = "name"
                    .DataSource = dsCombo.Tables("LoanScheme")
                    .SelectedValue = intLoan_SchemeId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddLoanScheme_Click", mstrModuleName)
        End Try

    End Sub

    Private Sub objLoanSchemeSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objLoanSchemeSearch.Click
        Dim objFrm As New frmCommonSearch
        Try
            If User._Object._Isrighttoleft = True Then
                objFrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objFrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objFrm)
            End If

            With objFrm
                .ValueMember = cboLoanScheme.ValueMember
                .DisplayMember = cboLoanScheme.DisplayMember
                .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                .CodeMember = "Code"
            End With
            If objFrm.DisplayDialog Then
                cboLoanScheme.SelectedValue = objFrm.SelectedValue
                cboLoanScheme.Focus()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objLoanSchemeSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnAddAttachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddAttachment.Click
        Try
            'Hemant (13 Dec 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - When selecting document attachments on loan application page, the system loads for a long time before accepting the attachment.
            'If IsValidate() = False Then Exit Try
            'Hemant (13 Dec 2022) -- End

            If CInt(cboDocumentType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 43, "Document Type is compulsory information. Please select Document Type to continue."), enMsgBoxStyle.Information)
                cboDocumentType.Focus()
                Exit Sub
            End If

            ofdAttachment.FileName = ""
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-999 - As a user, I want to have both .pdf and image formats on document attachments.
            'ofdAttachment.Filter = "Pdf Files|*.pdf"
            ofdAttachment.Filter = "Pdf Files|*.pdf|Image Files|*.jpg;*.jpeg;*.png;*.gif;*.bmp;*.tif;"
            'Hemant (27 Oct 2022) -- End
            If (ofdAttachment.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                Dim f As New System.IO.FileInfo(ofdAttachment.FileName)
                Call AddDocumentAttachment(f, ofdAttachment.FileName.ToString)
                f = Nothing
            End If
            cboDocumentType.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddAttachment_Click :", mstrModuleName)
        End Try
    End Sub

    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
    Private Sub btnAddOtherAttachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddOtherAttachment.Click
        Try
            'Hemant (13 Dec 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - When selecting document attachments on loan application page, the system loads for a long time before accepting the attachment.
            'If IsValidate() = False Then Exit Try
            'Hemant (13 Dec 2022) -- End

            If CInt(cboOtherDocumentType.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 43, "Document Type is compulsory information. Please select Document Type to continue."), enMsgBoxStyle.Information)
                cboOtherDocumentType.Focus()
                Exit Sub
            End If

            ofdAttachment.FileName = ""
            ofdAttachment.Filter = "Pdf Files|*.pdf|Image Files|*.jpg;*.jpeg;*.png;*.gif;*.bmp;*.tif;"
            If (ofdAttachment.ShowDialog(Me) = System.Windows.Forms.DialogResult.OK) Then
                Dim f As New System.IO.FileInfo(ofdAttachment.FileName)
                Call AddDocumentOtherAttachment(f, ofdAttachment.FileName.ToString)
                f = Nothing
            End If
            cboOtherDocumentType.SelectedValue = 0
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAddOtherAttachment_Click :", mstrModuleName)
        End Try
    End Sub
    'Hemant (27 Oct 2022) -- End

    Private Sub btnConfirmSign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmSign.Click
        Try
            pnlSign.Visible = False
            If imgSignature.Image.ToString.Trim.Length > 0 Then
                pnlSign.Visible = True
                imgSignature.Visible = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnConfirmSign_Click :", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Combobox Event"

    Private Sub cboEmpName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboEmpName.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboEmpName.ValueMember
                    .DisplayMember = cboEmpName.DisplayMember
                    .DataSource = CType(cboEmpName.DataSource, DataTable)
                    .CodeMember = "employeecode"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboEmpName.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboEmpName.Text = ""
                End If
            End If



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpName_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmpName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpName.SelectedIndexChanged
        Dim objEmployee As New clsEmployee_Master
        Try
            If CInt(cboEmpName.SelectedValue) < 0 Then Call SetDefaultSearchText(cboEmpName)
            If CInt(cboEmpName.SelectedValue) > 0 Then
                With cboEmpName
                    .ForeColor = Color.Black
                    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                    .SelectionStart = cboEmpName.Text.Length
                    .SelectionLength = 0
                End With

                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-949 - As a user, on the application screen, if the loan scheme selected is eligible for top-up, my Take home should be calculated as Principal Amount – (Outstanding Principle + Outstanding Interest + Insurance Amount)
                'Dim decMaxLoanAmount As Decimal = 0
                'If mintMaxLoanCalcTypeId = 2 Then
                '    decMaxLoanAmount = GetAmountByHeadFormula(mstrMaxLoanAmountHeadFormula)
                'Else
                '    decMaxLoanAmount = mdecMaxLoanAmountDefined
                'End If

                'If mstrMaxInstallmentHeadFormula.Trim.Length <= 0 Then
                '    'txtMaxLoanAmount.Text = Format(GetMaxLoanAmountDefinedLoanScheme(decMaxLoanAmount), GUI.fmtCurrency)
                '    txtMaxLoanAmount.Text = Format(decMaxLoanAmount, GUI.fmtCurrency)
                'Else
                '    mdecEstimateMaxInstallmentAmt = GetAmountByHeadFormula(mstrMaxInstallmentHeadFormula)
                '    txtMaxLoanAmount.Text = Format(IIf(mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme > decMaxLoanAmount, decMaxLoanAmount, mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme), GUI.fmtCurrency)
                'End If
                'Hemant (12 Oct 2022) -- End


                'Call nudMaxLoanDuration_ValueChanged(nudMaxLoanDuration, New System.EventArgs)
                btnConfirmSign.Visible = False
                pnlSign.Visible = False
                objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = CInt(cboEmpName.SelectedValue)
                If objEmployee._EmpSignature IsNot Nothing Then
                    Dim bytSignature As Byte() = Nothing
                    bytSignature = objEmployee._EmpSignature
                    If bytSignature IsNot Nothing Then
                        Dim ms As System.IO.MemoryStream = Nothing
                        ms = New System.IO.MemoryStream(bytSignature, 0, bytSignature.Length)
                        If ms IsNot Nothing Then
                            imgSignature.Image = Image.FromStream(ms)
                            chkconfirmSign.Checked = False
                        End If
                    End If
                    btnConfirmSign.Visible = True
                End If
                'Hemant (07 Jul 2023) -- Start
                'Enhancement : NMB : Show the remaining number of pay periods & Dates to the end of applicant's contract, Retirement on loan application and approval screens
                Dim dtEOCDate As Date = Nothing
                'Hemant (02 Feb 2024) -- Start
                'ISSUE/ENHANCEMENT(NMB): To exclude one month installment on application because during application, the system considers the first month in installment payments but in practice Approvers selects the 2nd month as deduction period. 
                Dim blnIsEOCDate As Boolean = False
                'Hemant (02 Feb 2024) -- End
                If objEmployee._Termination_To_Date <> Nothing AndAlso IsDBNull(objEmployee._Termination_To_Date) = False Then
                    dtEOCDate = CDate(objEmployee._Termination_To_Date)
                    lblEmplDate.Text = "Till Retr. Date"
                End If

                If objEmployee._Empl_Enddate <> Nothing AndAlso IsDBNull(objEmployee._Empl_Enddate) = False Then
                    If dtEOCDate < CDate(objEmployee._Empl_Enddate) Then
                    Else
                        dtEOCDate = CDate(objEmployee._Empl_Enddate)
                    End If
                    lblEmplDate.Text = "Till EOC Date"
                    'Hemant (02 Feb 2024) -- Start
                    'ISSUE/ENHANCEMENT(NMB): To exclude one month installment on application because during application, the system considers the first month in installment payments but in practice Approvers selects the 2nd month as deduction period. 
                    blnIsEOCDate = True
                    'Hemant (02 Feb 2024) -- End
                End If
                If dtEOCDate <> Nothing Then
                    dtpEndEmplDate.Value = dtEOCDate
                    'Hemant (02 Feb 2024) -- Start
                    'ISSUE/ENHANCEMENT(NMB): To exclude one month installment on application because during application, the system considers the first month in installment payments but in practice Approvers selects the 2nd month as deduction period. 
                    If blnIsEOCDate = True Then
                        nudPayPeriodEOC.Value = Math.Abs((mdtPeriodStart.Month - dtEOCDate.Month) + 12 * (mdtPeriodStart.Year - dtEOCDate.Year))
                    Else
                        'Hemant (02 Feb 2024) -- End
                    nudPayPeriodEOC.Value = Math.Abs((mdtPeriodStart.Month - dtEOCDate.Month) + 12 * (mdtPeriodStart.Year - dtEOCDate.Year)) + 1
                    End If  'Hemant (02 Feb 2024) 
                    lblPayPeriodsEOC.Visible = True
                    nudPayPeriodEOC.Visible = True
                    lblEmplDate.Visible = True
                    dtpEndEmplDate.Visible = True
                Else
                    lblPayPeriodsEOC.Visible = False
                    nudPayPeriodEOC.Visible = False
                    lblEmplDate.Visible = False
                    dtpEndEmplDate.Visible = False
                End If
                'Hemant (07 July 2023) -- End
                ResetMax()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpName_SelectedIndexChanged", mstrModuleName)
        Finally
            objEmployee = Nothing
        End Try
    End Sub

    Private Sub ResetMax()
        Try
            If CInt(cboEmpName.SelectedValue) > 0 AndAlso CInt(cboLoanScheme.SelectedValue) > 0 Then
                Dim objLScheme As New clsLoan_Scheme
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : - On loan scheme master, if a loan scheme has posting to flexcube enabled, as a user, I want to have the option of specifying the number of installments that need to be paid before a top-up can be done. 
                Dim objEmpBankTran As New clsEmployeeBanks
                Dim objloanAdvance As New clsLoan_Advance
                'Hemant (12 Oct 2022) -- End
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                Dim objLoanCategoryMapping As New clsLoanCategoryMapping
                'Hemant (27 Oct 2022) -- End
                objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
                mdecMaxLoanAmountDefined = objLScheme._MaxLoanAmountLimit

                'Pinkal (20-Sep-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnRequiredReportingToApproval = objLScheme._RequiredReportingToApproval
                'Pinkal (20-Sep-2022) -- End

                'Pinkal (12-Oct-2022) -- Start 
                'NMB Loan Module Enhancement.
                mblnSkipApprovalFlow = objLScheme._IsSkipApproval
                'Pinkal (12-Oct-2022) -- End

                'Pinkal (10-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnLoanApproval_DailyReminder = objLScheme._LoanApproval_DailyReminder
                mintEscalationDays = objLScheme._EscalationDays
                'Pinkal (10-Nov-2022) -- End


                'START REMOVE
                mstrLoanSchemeCode = CType(cboLoanScheme.SelectedItem, DataRowView).Item("code").ToString
                'END REMOVE

                txtLoanRate.Text = CStr(objLScheme._InterestRate)
                mintMaxLoanCalcTypeId = CInt(objLScheme._MaxLoanAmountCalcTypeId)
                mstrMaxLoanAmountHeadFormula = CStr(objLScheme._MaxLoanAmountHeadFormula)
                cboLoanCalcType.SelectedValue = objLScheme._LoanCalcTypeId
                cboInterestCalcType.SelectedValue = objLScheme._InterestCalctypeId
                cboLoanSchemeCategory.SelectedValue = CStr(objLScheme._LoanSchemeCategoryId)
                'Hemant (18 Mar 2024) -- Start
                'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
                If mstrCompanyGroupName = "NMB PLC" Then
                    'Hemant (18 Mar 2024) -- END
                txtRepaymentDays.Text = CStr(objLScheme._RepaymentDays)
                End If 'Hemant (18 Mar 2024)


                'Hemant (07 Jul 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-1099 - Changes in maximum principal amount calculation to factor in remaining months to applicant's last employment date
                If nudPayPeriodEOC.Value < CInt(objLScheme._MaxNoOfInstallment) Then
                    mintMaxNoOfInstallmentLoanScheme = CInt(nudPayPeriodEOC.Value)
                Else
                    'Hemant (07 Jul 2023) -- End
                    mintMaxNoOfInstallmentLoanScheme = objLScheme._MaxNoOfInstallment
                End If 'Hemant (07 Jul 2023)


                'Hemant (29 Mar 2024) -- Start
                'ENHANCEMENT(TADB): New loan application form
                If mstrCompanyGroupName = "NMB PLC" Then
                    'Hemant (29 Mar 2024) -- End
                If IsNotMortgageLoanExist() = True Then 'Hemant (07 Jul 2023)
                mstrMaxInstallmentHeadFormula = objLScheme._MaxInstallmentHeadFormula
                    'Hemant (07 Jul 2023) -- Start
                    'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
                Else
                    mstrMaxInstallmentHeadFormula = objLScheme._MaxInstallmentHeadFormulaForMortgage
                    'Hemant (07 Jul 2023) -- End
                End If
                    'Hemant (29 Mar 2024) -- Start
                    'ENHANCEMENT(TADB): New loan application form
                Else
                    mstrMaxInstallmentHeadFormula = objLScheme._MaxInstallmentHeadFormula
                End If
                'Hemant (29 Mar 2024) -- End



                If mintMaxLoanCalcTypeId = 2 Then
                    mdecMaxLoanAmount = GetAmountByHeadFormula(mstrMaxLoanAmountHeadFormula)
                Else
                    mdecMaxLoanAmount = mdecMaxLoanAmountDefined
                End If
                If mstrMaxInstallmentHeadFormula.Trim.Length <= 0 Then
                    'txtMaxLoanAmount.Text = Format(GetMaxLoanAmountDefinedLoanScheme(decMaxLoanAmount), GUI.fmtCurrency)
                    txtMaxLoanAmount.Text = Format(mdecMaxLoanAmount, GUI.fmtCurrency)
                    txtFormulaExpr.Text = ""
                Else
                    Dim strResultFormulaExpr As String = ""
                    mdecEstimateMaxInstallmentAmt = GetAmountByHeadFormula(mstrMaxInstallmentHeadFormula, strResultFormulaExpr)
                    'Hemant (22 Nov 2024) -- Start
                    'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                    If mstrCompanyGroupName = "TADB" Then
                        txtMaxLoanAmount.Text = Format(mdecMaxLoanAmount, GUI.fmtCurrency)
                    Else
                        'Hemant (22 Nov 2024) -- End
                    txtMaxLoanAmount.Text = Format(IIf(mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme > mdecMaxLoanAmount, mdecMaxLoanAmount, mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme), GUI.fmtCurrency)
                    End If 'Hemant (22 Nov 2024)
                    'Hemant (22 Dec 2023) -- Start
                    'ENHANCEMENT(TADB): A1X-1607 - Oracle CBS JV enhancement to split payable amounts for contributions to different GL accounts
                    mdecMaxLoanAmount = CDec(txtMaxLoanAmount.Text)
                    'Hemant (22 Dec 2023) -- End
                    txtFormulaExpr.Text = strResultFormulaExpr
                End If


                mdecMinLoanAmount = objLScheme._MinLoanAmountLimit

                txtMinLoanAmount.Text = Format(mdecMinLoanAmount, GUI.fmtCurrency)

                Panel1.Visible = True
                Panel2.Visible = True
                If CInt(objLScheme._LoanSchemeCategoryId) = enLoanSchemeCategories.SECURED Then
                    Panel2.BringToFront()
                    Panel2.Visible = True
                ElseIf CInt(objLScheme._LoanSchemeCategoryId) = enLoanSchemeCategories.MORTGAGE Then
                    Panel2.SendToBack()
                    Panel2.Visible = False
                Else
                    Panel1.Visible = False
                    Panel2.Visible = False
                End If

                'Hemant (07 Jul 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-1099 - Changes in maximum principal amount calculation to factor in remaining months to applicant's last employment date
                If nudPayPeriodEOC.Value < CInt(objLScheme._MaxNoOfInstallment) Then
                    nudMaxInstallment.Value = nudPayPeriodEOC.Value
                Else
                    'Hemant (07 Jul 2023) -- End
                nudMaxInstallment.Value = objLScheme._MaxNoOfInstallment
                End If 'Hemant (07 Jul 2023)

                txtInsuranceRate.Text = CStr(objLScheme._InsuranceRate)



                'If menAction <> enAction.EDIT_ONE Then
                RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
                txtLoanAmt.Text = Format(mdecMaxLoanAmount, GUI.fmtCurrency)
                AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
                RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                nudDuration.Value = nudMaxInstallment.Value

                AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged

                txtMaxInstallmentAmt.Tag = mdecEstimateMaxInstallmentAmt
                txtMaxInstallmentAmt.Decimal = CDec(Format(mdecEstimateMaxInstallmentAmt, GUI.fmtCurrency))


                'Hemant (22 Nov 2024) -- Start
                'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                If mstrCompanyGroupName = "TADB" Then
                    txtMaxLoanAmount.Text = Format(mdecMaxLoanAmount, GUI.fmtCurrency)
                    txtLoanAmt_TextChanged(txtLoanAmt, New EventArgs())
                Else
                    'Hemant (22 Nov 2024) -- End
                txtInstallmentAmt.Tag = mdecEstimateMaxInstallmentAmt
                txtInstallmentAmt.Decimal = CDec(Format(mdecEstimateMaxInstallmentAmt, GUI.fmtCurrency))
                'txtInstallmentAmt.Text = Format(CDec(txtLoanAmt.Decimal) / CInt(nudDuration.Value), GUI.fmtCurrency)
                txtMaxLoanAmount.Text = Format(IIf(txtLoanAmt.Decimal > mdecMaxLoanAmount, mdecMaxLoanAmount, txtLoanAmt.Decimal), GUI.fmtCurrency)
                End If  'Hemant (22 Nov 2024)

                'End If

                'Call nudMaxLoanDuration_ValueChanged(nudMaxLoanDuration, New System.EventArgs)

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                'A1X-2587 NMB mortgage - Insurance deduction: The system should deduct insurance for the first year only and for the following year it will be done yearly. 
                If objLScheme._LoanSchemeCategoryId = enLoanSchemeCategories.MORTGAGE Then
                    If CInt(nudDuration.Value) > 12 Then
                        txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Decimal) * (CDec(txtInsuranceRate.Decimal) / 100), GUI.fmtCurrency)
                    Else
                        txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Decimal) * (CDec(txtInsuranceRate.Decimal) / 100) * (CInt(nudDuration.Value) / 12), GUI.fmtCurrency)
                    End If
                Else
                    txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Decimal) * (CDec(txtInsuranceRate.Decimal) / 100) * (CInt(nudDuration.Value) / 12), GUI.fmtCurrency)
                End If
                'Pinkal (17-May-2024) -- End

                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) :  AC2-949 - As a user, on the application screen, if the loan scheme selected is eligible for top-up, my Take home should be calculated as Principal Amount – (Outstanding Principle + Outstanding Interest + Insurance Amount)
                'txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - CDec(txtInsuranceAmt.Decimal), GUI.fmtCurrency)
                Dim dsEmpBank As New DataSet
                Dim decOutStandingPrincipalAmt As Decimal = 0
                Dim decOutStandingInterestAmt As Decimal = 0
                Dim intNoOfInstallmentPaid As Integer = 0
                'Hemant (11 Oct 2024) -- Start
                'ENHANCEMENT(NMB): A1X - 2822 :  Allow to apply another loan if employee has 1 installment left to clear
                Dim intTotalNoOfInstallment As Integer = 0
                'Hemant (11 Oct 2024) -- End
                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                Dim mdecOtherLoanOutStandingPrincipalAmt As Decimal = 0
                'Pinkal (23-Nov-2022) -- End

                Dim xPrevPeriodStart As Date
                Dim xPrevPeriodEnd As Date
                If CInt(mintDeductionPeriodUnkId) > 0 Then
                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(mintDeductionPeriodUnkId)
                    xPrevPeriodStart = objPeriod._Start_Date
                    xPrevPeriodEnd = objPeriod._End_Date
                    objPeriod = Nothing
                Else
                    xPrevPeriodStart = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                    xPrevPeriodEnd = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                End If
                dsEmpBank = objEmpBankTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "EmployeeBank", , , CStr(cboEmpName.SelectedValue), xPrevPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")

                'call oracle function to get list of loan emis by passing bank account no.
                If dsEmpBank.Tables(0).Rows.Count > 0 Then
                    If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then
                        Dim dsFlexLoanData As DataSet = objloanAdvance.GetFlexcubeLoanData(ConfigParameter._Object._OracleHostName, ConfigParameter._Object._OraclePortNo, ConfigParameter._Object._OracleServiceName, ConfigParameter._Object._OracleUserName, ConfigParameter._Object._OracleUserPassword, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString, objLScheme._Code, , False)
                        'Hemant (29 Mar 2024) -- [dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString]
                        'Hemant (01 Sep 2023) -- [blnUseOldView := False]
                        If dsFlexLoanData IsNot Nothing AndAlso dsFlexLoanData.Tables(0).Rows.Count > 0 Then
                            decOutStandingPrincipalAmt = (From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("PRINCIPALOUTSTANDING")))).Sum()
                            decOutStandingInterestAmt = (From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("CURRENT_OUTSTANDING_INTEREST")))).Sum()
                            intNoOfInstallmentPaid = CInt((From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("PAIDNOOFINSTALMENT")))).Sum())
                            'Hemant (11 Oct 2024) -- Start
                            'ENHANCEMENT(NMB): A1X - 2822 :  Allow to apply another loan if employee has 1 installment left to clear
                            If objLScheme._Code.ToString.Trim = "CL27" Then
                                intTotalNoOfInstallment = CInt((From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("INSTALMENTCOUNT")))).Sum())
                                If intTotalNoOfInstallment - intNoOfInstallmentPaid <= 1 Then
                                    decOutStandingPrincipalAmt = 0
                                    decOutStandingInterestAmt = 0
                                End If
                            End If
                            'Hemant (11 Oct 2024) -- End
                            'Hemant (22 Dec 2022) -- Start
                            mintCurrentFlexcubeLoanCount = dsFlexLoanData.Tables(0).Rows.Count
                        Else
                            mintCurrentFlexcubeLoanCount = 0
                            'Hemant (22 Dec 2022) -- End
                        End If

                        'Pinkal (23-Nov-2022) -- Start
                        'NMB Loan Module Enhancement. [GETTING OUTSTANDING PRINCIPAL AMOUNT FROM OTHER LOAN SCHEME ALSO TO DEFINE THE APPROVER FLOW]
                        dsFlexLoanData = Nothing
                        dsFlexLoanData = objloanAdvance.GetFlexcubeLoanData(ConfigParameter._Object._OracleHostName, ConfigParameter._Object._OraclePortNo, ConfigParameter._Object._OracleServiceName, ConfigParameter._Object._OracleUserName, ConfigParameter._Object._OracleUserPassword, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString, "", False, False)
                        'Hemant (29 Mar 2024) -- [dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString]
                        'Hemant (01 Sep 2023) -- [blnUseOldView := False]
                        If dsFlexLoanData IsNot Nothing AndAlso dsFlexLoanData.Tables(0).Rows.Count > 0 Then
                            Dim decOtherLoanOutStandingPrincipalAmt = (From p In dsFlexLoanData.Tables(0).AsEnumerable() Select (CDec(p.Item("PRINCIPALOUTSTANDING")))).DefaultIfEmpty.Sum()
                            If decOutStandingPrincipalAmt <= decOtherLoanOutStandingPrincipalAmt Then
                                mdecOtherLoanOutStandingPrincipalAmt = decOtherLoanOutStandingPrincipalAmt - decOutStandingPrincipalAmt
                            Else
                                mdecOtherLoanOutStandingPrincipalAmt = decOutStandingPrincipalAmt - decOtherLoanOutStandingPrincipalAmt
                    End If
                        End If '   If dsFlexLoanData IsNot Nothing AndAlso dsFlexLoanData.Tables(0).Rows.Count > 0 Then
                        'Pinkal (23-Nov-2022) -- End

                        'Hemant (22 Nov 2024) -- Start
                        'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                        If mstrCompanyGroupName = "TADB" AndAlso objLScheme._Code.Trim().ToString() = "56" AndAlso (decOutStandingPrincipalAmt > 0 OrElse decOutStandingInterestAmt > 0) Then
                            Dim dsSalaryAdvanceFlexcubeloan As DataSet = objloanAdvance.GetFlexcubeLoanData(ConfigParameter._Object._OracleHostName, ConfigParameter._Object._OraclePortNo, ConfigParameter._Object._OracleServiceName, ConfigParameter._Object._OracleUserName, ConfigParameter._Object._OracleUserPassword, dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString, dsEmpBank.Tables(0).Rows(0).Item("employeecode").ToString, "58", , False)
                            If dsSalaryAdvanceFlexcubeloan IsNot Nothing AndAlso dsSalaryAdvanceFlexcubeloan.Tables(0).Rows.Count > 0 Then
                                mdecActualSalaryAdvancePendingPrincipalAmt = (From p In dsSalaryAdvanceFlexcubeloan.Tables(0).AsEnumerable() Select (CDec(p.Item("PRINCIPALOUTSTANDING")))).DefaultIfEmpty.Sum()
                            End If
                            If mdecActualSalaryAdvancePendingPrincipalAmt > 0 Then
                                chkSalaryAdvanceLiquidation.Visible = True
                            Else
                                chkSalaryAdvanceLiquidation.Visible = False
                            End If

                        Else
                            chkSalaryAdvanceLiquidation.Visible = False
                            chkSalaryAdvanceLiquidation.Checked = False
                            mdecActualSalaryAdvancePendingPrincipalAmt = 0
                            mdecFinalSalaryAdvancePendingPrincipalAmt = 0
                        End If
                        'Hemant (22 Nov 2024) -- End

                    End If ' If dsEmpBank.Tables(0).Rows(0).Item("accountno").ToString.Trim.Length > 0 Then

                End If '    If dsEmpBank.Tables(0).Rows.Count > 0 Then

                txtOutStandingPrincipalAmt.Text = Format(decOutStandingPrincipalAmt, GUI.fmtCurrency)
                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement. [GETTING OUTSTANDING PRINCIPAL AMOUNT FROM OTHER LOAN SCHEME ALSO TO DEFINE THE APPROVER FLOW]
                txtOutStandingPrincipalAmt.Tag = mdecOtherLoanOutStandingPrincipalAmt
                'Pinkal (23-Nov-2022) -- End

                txtOutStandingInterestAmt.Text = Format(decOutStandingInterestAmt, GUI.fmtCurrency)
                nudInstallmentPaid.Value = intNoOfInstallmentPaid
                dsEmpBank = Nothing
                'Hemant (10 Nov 2022) -- Start
                'ISSUE/ENHANCEMENT(NMB) : For the Take home field in top-up loans. Please return it to calculate as normal loan only.
                'txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - (CDec(txtOutStandingPrincipalAmt.Decimal) + CDec(txtOutStandingInterestAmt.Decimal) + CDec(txtInsuranceAmt.Decimal)), GUI.fmtCurrency)
                'Hemant (13 Dec 2022) -- Start
                'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - For top-up loans, they want the Take Home formula to be as below : Take Home = Principle – (Current Outstanding Principle + Current Outstanding Interest + Insurance Amount).
                'txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - CDec(txtInsuranceAmt.Decimal), GUI.fmtCurrency)
                txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - (CDec(txtOutStandingPrincipalAmt.Decimal) + CDec(txtOutStandingInterestAmt.Decimal) + CDec(txtInsuranceAmt.Decimal)) - mdecFinalSalaryAdvancePendingPrincipalAmt, GUI.fmtCurrency)
                'Hemant (22 Nov 2024) -- [- mdecFinalSalaryAdvancePendingPrincipalAmt]


                'Hemant (13 Dec 2022) -- End
                'Hemant (10 Nov 2022) -- End
                'Hemant (12 Oct 2022) -- End


                mblnIsAttachmentRequired = objLScheme._IsAttachmentRequired
                mstrDocumentTypeIDs = CStr(objLScheme._DocumentTypeIDs)

                If mblnIsAttachmentRequired = True Then
                    pnlScanAttachment.Visible = True
                Else
                    pnlScanAttachment.Visible = False
                End If

                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                'mintOtherDocumentunkid = 0
                'Dim dsSchemeCategoryMappring As DataSet = objLoanCategoryMapping.GetList("List", CInt(cboLoanSchemeCategory.SelectedValue))
                'If CInt(cboLoanSchemeCategory.SelectedValue) > 0 AndAlso dsSchemeCategoryMappring IsNot Nothing AndAlso dsSchemeCategoryMappring.Tables(0).Rows.Count > 0 AndAlso CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("otherdocumentunkid")) > 0 Then
                '    mintOtherDocumentunkid = CInt(dsSchemeCategoryMappring.Tables(0).Rows(0).Item("otherdocumentunkid"))
                '    Dim lstDocumentTypeIDs As List(Of String) = mstrDocumentTypeIDs.Split(","c).ToList()
                '    lstDocumentTypeIDs.Remove(mintOtherDocumentunkid.ToString)
                '    mstrDocumentTypeIDs = String.Join(",", lstDocumentTypeIDs.ToArray())
                'End If
                mstrOtherDocumentIds = ""
                Dim dsSchemeCategoryMappring As DataSet = objLoanCategoryMapping.GetList("List", CInt(cboLoanSchemeCategory.SelectedValue))
                If dsSchemeCategoryMappring IsNot Nothing AndAlso CInt(cboLoanSchemeCategory.SelectedValue) > 0 Then
                    mstrOtherDocumentIds = String.Join(",", dsSchemeCategoryMappring.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("otherdocumentunkid") > 0).Select(Function(x) x.Field(Of Integer)("otherdocumentunkid").ToString()).ToArray())
                    Dim lstDocumentTypeIDs As List(Of String) = mstrDocumentTypeIDs.Split(","c).ToList()
                    If mstrOtherDocumentIds.Trim.Length > 0 Then
                        Dim arOtherDocIds() As String = mstrOtherDocumentIds.Trim.Split(CChar(","))
                        For i As Integer = 0 To arOtherDocIds.Length - 1
                            lstDocumentTypeIDs.Remove(arOtherDocIds(i))
                        Next
                        arOtherDocIds = Nothing
                    End If
                    mstrDocumentTypeIDs = String.Join(",", lstDocumentTypeIDs.ToArray())
                End If

                dsSchemeCategoryMappring = Nothing

                'If chkMakeExceptionalApplication.Checked = True AndAlso mintOtherDocumentunkid > 0 Then
                '    pnlOtherScanAttachment.Visible = True
                'Else
                '    pnlOtherScanAttachment.Visible = False
                'End If
                If chkMakeExceptionalApplication.Checked = True AndAlso mstrOtherDocumentIds.Trim.Length > 0 Then
                    pnlOtherScanAttachment.Visible = True
                Else
                    pnlOtherScanAttachment.Visible = False
                End If
                'Pinkal (23-Nov-2022) -- End

                AttachementLocationChange()
                FillOtherDocumentTypeCombo()
                'Hemant (27 Oct 2022) -- End
                Call FillDocumentTypeCombo()
                objLScheme = Nothing
                objEmpBankTran = Nothing
                objloanAdvance = Nothing
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                objLoanCategoryMapping = Nothing
                'Hemant (27 Oct 2022) -- End
            Else
                txtLoanRate.Text = Format(0, GUI.fmtCurrency)
                cboLoanCalcType.SelectedValue = CStr(0)
                cboInterestCalcType.SelectedValue = CStr(0)
                cboLoanSchemeCategory.SelectedValue = CStr(0)
                txtRepaymentDays.Text = CStr(0)
                txtMaxInstallmentAmt.Text = Format(0, GUI.fmtCurrency)
                txtMaxLoanAmount.Text = Format(0, GUI.fmtCurrency)
                txtFormulaExpr.Text = ""
                txtMinLoanAmount.Text = Format(0, GUI.fmtCurrency)

                Panel1.Visible = False
                Panel2.Visible = False

                nudMaxInstallment.Value = 1

                txtInsuranceRate.Text = Format(0, GUI.fmtCurrency)

                RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
                txtLoanAmt.Text = Format(0, GUI.fmtCurrency)
                AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
                RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
                nudDuration.Value = 1
                AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged

                txtInsuranceAmt.Text = Format(0, GUI.fmtCurrency)
                txtTakeHome.Text = Format(0, GUI.fmtCurrency)

                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                txtInstallmentAmt.Text = Format(0, GUI.fmtCurrency)
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged



                pnlScanAttachment.Visible = False
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                pnlOtherScanAttachment.Visible = False
                'Hemant (27 Oct 2022) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ResetMax", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmpName_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpName.GotFocus
        Try
            With cboEmpName
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpName_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboEmpName_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboEmpName.Leave
        Try
            If CInt(cboEmpName.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboEmpName)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmpName_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboLoanScheme.KeyPress
        Try
            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cboLoanScheme.ValueMember
                    .DisplayMember = cboLoanScheme.DisplayMember
                    .DataSource = CType(cboLoanScheme.DataSource, DataTable)
                    .CodeMember = "Code"
                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cboLoanScheme.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cboLoanScheme.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.SelectedIndexChanged
        Try
            If CInt(cboLoanScheme.SelectedValue) < 0 Then Call SetDefaultSearchText(cboLoanScheme)
            If CInt(cboLoanScheme.SelectedValue) > 0 Then
                With cboLoanScheme
                    .ForeColor = Color.Black
                    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                    .SelectionStart = cboLoanScheme.Text.Length
                    .SelectionLength = 0
                End With
            End If

            'Hemant (18 Mar 2024) -- Start
            'ENHANCEMENT(TADB): A1X-2515 - Add "Repayment Day" Field on loan application Screen
            'txtRepaymentDays.Text = "0"
            'Hemant (18 Mar 2024) -- End
            cboLoanSchemeCategory.SelectedValue = CStr(0)

            'Pinkal (20-Sep-2022) -- Start
            'NMB Loan Module Enhancement.
            mblnRequiredReportingToApproval = False
            'Pinkal (20-Sep-2022) -- End

'Pinkal (12-Oct-2022) -- Start
            'NMB Loan Module Enhancement.
            mblnSkipApprovalFlow = False
            'Pinkal (12-Oct-2022) -- End

            Call ResetMax()
            'If cboLoanScheme.SelectedValue IsNot Nothing AndAlso CInt(cboLoanScheme.SelectedValue) > 0 Then
            '    Dim objLScheme As New clsLoan_Scheme

            '    objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            '    mdecMaxLoanAmountDefined = objLScheme._MaxLoanAmountLimit

            '    txtLoanRate.Text = CStr(objLScheme._InterestRate)
            '    mintMaxLoanCalcTypeId = CInt(objLScheme._MaxLoanAmountCalcTypeId)
            '    mstrMaxLoanAmountHeadFormula = CStr(objLScheme._MaxLoanAmountHeadFormula)
            '    cboLoanCalcType.SelectedValue = objLScheme._LoanCalcTypeId
            '    cboInterestCalcType.SelectedValue = objLScheme._InterestCalctypeId
            '    cboLoanSchemeCategory.SelectedValue = CStr(objLScheme._LoanSchemeCategoryId)
            '    txtRepaymentDays.Text = CStr(objLScheme._RepaymentDays)

            '    mintMaxNoOfInstallmentLoanScheme = CInt(objLScheme._MaxNoOfInstallment)
            '    mstrMaxInstallmentHeadFormula = objLScheme._MaxInstallmentHeadFormula

            '    If mintMaxLoanCalcTypeId = 2 Then
            '        decMaxLoanAmount = GetAmountByHeadFormula(mstrMaxLoanAmountHeadFormula)
            '    Else
            '        decMaxLoanAmount = mdecMaxLoanAmountDefined
            '    End If
            '    If mstrMaxInstallmentHeadFormula.Trim.Length <= 0 Then
            '        'txtMaxLoanAmount.Text = Format(GetMaxLoanAmountDefinedLoanScheme(decMaxLoanAmount), GUI.fmtCurrency)
            '        txtMaxLoanAmount.Text = Format(decMaxLoanAmount, GUI.fmtCurrency)
            '        txtFormulaExpr.Text = ""
            '    Else
            '        Dim strResultFormulaExpr As String = ""
            '        mdecEstimateMaxInstallmentAmt = GetAmountByHeadFormula(mstrMaxInstallmentHeadFormula, strResultFormulaExpr)

            '        txtMaxLoanAmount.Text = Format(IIf(mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme > decMaxLoanAmount, decMaxLoanAmount, mdecEstimateMaxInstallmentAmt * mintMaxNoOfInstallmentLoanScheme), GUI.fmtCurrency)
            '        txtFormulaExpr.Text = strResultFormulaExpr
            '    End If


            '    Panel1.Visible = True
            '    Panel2.Visible = True
            '    If CInt(objLScheme._LoanSchemeCategoryId) = enLoanSchemeCategories.SECURED Then
            '        Panel2.BringToFront()
            '        Panel2.Visible = True
            '    ElseIf CInt(objLScheme._LoanSchemeCategoryId) = enLoanSchemeCategories.MORTGAGE Then
            '        Panel2.SendToBack()
            '        Panel2.Visible = False
            '    Else
            '        Panel1.Visible = False
            '        Panel2.Visible = False
            '    End If

            '    nudMaxInstallment.Value = objLScheme._MaxNoOfInstallment

            '    txtInsuranceRate.Text = CStr(objLScheme._InsuranceRate)

            '    txtMaxInstallmentAmt.Tag = mdecEstimateMaxInstallmentAmt
            '    txtMaxInstallmentAmt.Decimal = CDec(Format(mdecEstimateMaxInstallmentAmt, GUI.fmtCurrency))

            '    If menAction <> enAction.EDIT_ONE Then
            '        RemoveHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            '        txtLoanAmt.Text = Format(decMaxLoanAmount, GUI.fmtCurrency)
            '        AddHandler txtLoanAmt.TextChanged, AddressOf txtLoanAmt_TextChanged
            '        RemoveHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
            '        nudDuration.Value = nudMaxInstallment.Value

            '        AddHandler nudDuration.ValueChanged, AddressOf nudDuration_ValueChanged
            '        txtInstallmentAmt.Tag = mdecEstimateMaxInstallmentAmt
            '        txtInstallmentAmt.Decimal = CDec(Format(mdecEstimateMaxInstallmentAmt, GUI.fmtCurrency))
            '        'txtInstallmentAmt.Text = Format(CDec(txtLoanAmt.Decimal) / CInt(nudDuration.Value), GUI.fmtCurrency)
            '        txtMaxLoanAmount.Text = Format(IIf(txtLoanAmt.Decimal > decMaxLoanAmount, decMaxLoanAmount, txtLoanAmt.Decimal), GUI.fmtCurrency)

            '    End If



            '    'Call nudMaxLoanDuration_ValueChanged(nudMaxLoanDuration, New System.EventArgs)
            '    txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Decimal) * (CDec(txtInsuranceRate.Decimal) / 100) * (CInt(nudDuration.Value) / 12), GUI.fmtCurrency)
            '    txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - CDec(txtInsuranceAmt.Decimal), GUI.fmtCurrency)

            '    mblnIsAttachmentRequired = objLScheme._IsAttachmentRequired
            '    mstrDocumentTypeIDs = CStr(objLScheme._DocumentTypeIDs)

            '    If mblnIsAttachmentRequired = True Then
            '        pnlScanAttachment.Visible = True
            '    Else
            '        pnlScanAttachment.Visible = False
            '    End If
            '    Call FillDocumentTypeCombo()
            '    objLScheme = Nothing
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheme_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.GotFocus
        Try
            With cboLoanScheme
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanScheme_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanScheme.Leave
        Try
            If CInt(cboLoanScheme.SelectedValue) <= 0 Then Call SetDefaultSearchText(cboLoanScheme)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Private Sub cboLoanCalcType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanCalcType.SelectedIndexChanged, cboInterestCalcType.SelectedIndexChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) > 0 Then

                Select Case CInt(cboLoanCalcType.SelectedValue)

                    Case enLoanCalcId.Simple_Interest
                        nudDuration.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Simple_Interest

                        If menAction <> enAction.EDIT_ONE Then
                            Call Calculate_Projected_Loan_Principal(enLoanCalcId.Simple_Interest, enIntCalcType)
                            txtInstallmentAmt.ReadOnly = True
                            If txtLoanRate.Enabled = False Then
                                txtLoanRate.Enabled = True
                            End If
                        End If

                    Case enLoanCalcId.Reducing_Amount
                        nudDuration.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Reducing_Amount
                        If menAction <> enAction.EDIT_ONE Then
                            Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Amount, enIntCalcType)
                            txtInstallmentAmt.ReadOnly = True
                            If txtLoanRate.Enabled = False Then
                                txtLoanRate.Enabled = True
                            End If
                        End If
                    Case enLoanCalcId.No_Interest
                        RemoveHandler cboInterestCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
                        cboInterestCalcType.SelectedValue = 0
                        txtInstallmentAmt.ReadOnly = False
                        If menAction <> enAction.EDIT_ONE Then
                            nudDuration.Value = mintNoOfInstallment
                            nudDuration.Enabled = True
                            txtInstallmentAmt.Text = Format(mdecInstallmentAmount, GUI.fmtCurrency)
                            txtLoanRate.Decimal = 0
                            txtLoanRate.Enabled = False
                            Call txtLoanAmt_TextChanged(New Object, New EventArgs)
                        End If
                        mintLastCalcTypeId = enLoanCalcId.No_Interest
                    Case enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI
                        nudDuration.Enabled = True
                        mintLastCalcTypeId = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI
                        txtAppliedUptoPrincipalAmt.ReadOnly = False
                        If menAction <> enAction.EDIT_ONE Then
                            Call Calculate_Projected_Loan_Principal(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)
                            txtInstallmentAmt.ReadOnly = True
                            If txtLoanRate.Enabled = False Then
                                txtLoanRate.Enabled = True
                            End If
                        End If
                    Case enLoanCalcId.No_Interest_With_Mapped_Head
                        RemoveHandler cboInterestCalcType.SelectedIndexChanged, AddressOf cboLoanCalcType_SelectedIndexChanged
                        cboInterestCalcType.SelectedValue = 0
                        txtInstallmentAmt.ReadOnly = False
                        If menAction <> enAction.EDIT_ONE Then
                            nudDuration.Value = mintNoOfInstallment
                            nudDuration.Enabled = False
                            txtInstallmentAmt.Text = Format(mdecInstallmentAmount, GUI.fmtCurrency)
                            txtLoanRate.Decimal = 0
                            txtLoanRate.Enabled = False
                            Call txtLoanAmt_TextChanged(New Object, New EventArgs)
                        End If
                        mintLastCalcTypeId = enLoanCalcId.No_Interest_With_Mapped_Head
                End Select
            End If
            If CInt(cboInterestCalcType.SelectedValue) <= 0 Then
                cboInterestCalcType.Text = ""
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanCalcType_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExRate.Text = ""
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, mdtPeriodEnd, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Numeric Up Down Event "
    Private Sub nudDuration_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudDuration.ValueChanged
        Try
            Dim enIntCalcType As enLoanInterestCalcType
            If CInt(cboInterestCalcType.SelectedValue) > 0 Then
                enIntCalcType = CType(CInt(cboInterestCalcType.SelectedValue), enLoanInterestCalcType)
            Else
                enIntCalcType = enLoanInterestCalcType.MONTHLY
            End If
            If CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Simple_Interest Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Simple_Interest, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Amount Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Amount, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest Then
                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                txtInstallmentAmt.Text = Format(txtLoanAmt.Decimal / CDec(nudDuration.Value), GUI.fmtCurrency)
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI Then
                Call Calculate_Projected_Loan_Balance(enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI, enIntCalcType)

            ElseIf CInt(cboLoanCalcType.SelectedValue) = enLoanCalcId.No_Interest_With_Mapped_Head Then
                RemoveHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged
                txtInstallmentAmt.Text = Format(txtLoanAmt.Decimal / CDec(nudDuration.Value), GUI.fmtCurrency)
                AddHandler txtInstallmentAmt.TextChanged, AddressOf txtInstallmentAmt_TextChanged

            End If

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            'A1X-2587 NMB mortgage - Insurance deduction: The system should deduct insurance for the first year only and for the following year it will be done yearly. 
            Dim objLScheme As New clsLoan_Scheme
            objLScheme._Loanschemeunkid = CInt(cboLoanScheme.SelectedValue)
            If objLScheme._LoanSchemeCategoryId = enLoanSchemeCategories.MORTGAGE Then
                If CInt(nudDuration.Value) > 12 Then
                    txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Decimal) * (CDec(txtInsuranceRate.Decimal) / 100), GUI.fmtCurrency)
                Else
            txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Decimal) * (CDec(txtInsuranceRate.Decimal) / 100) * (CInt(nudDuration.Value) / 12), GUI.fmtCurrency)
                End If
            Else
                txtInsuranceAmt.Text = Format(CDec(txtLoanAmt.Decimal) * (CDec(txtInsuranceRate.Decimal) / 100) * (CInt(nudDuration.Value) / 12), GUI.fmtCurrency)
            End If
            objLScheme = Nothing
            'Pinkal (17-May-2024) -- End


            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) :  AC2-949 - As a user, on the application screen, if the loan scheme selected is eligible for top-up, my Take home should be calculated as Principal Amount – (Outstanding Principle + Outstanding Interest + Insurance Amount)
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - CDec(txtInsuranceAmt.Decimal), GUI.fmtCurrency)
            'Hemant (10 Nov 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : For the Take home field in top-up loans. Please return it to calculate as normal loan only.
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - (CDec(txtOutStandingPrincipalAmt.Decimal) + CDec(txtOutStandingInterestAmt.Decimal) + CDec(txtInsuranceAmt.Decimal)), GUI.fmtCurrency)
            'Hemant (13 Dec 2022) -- Start
            'ISSUE/ENHANCEMENT(NMB) : Final UAT Changes - For top-up loans, they want the Take Home formula to be as below : Take Home = Principle – (Current Outstanding Principle + Current Outstanding Interest + Insurance Amount).
            'txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - CDec(txtInsuranceAmt.Decimal), GUI.fmtCurrency)
            txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - (CDec(txtOutStandingPrincipalAmt.Decimal) + CDec(txtOutStandingInterestAmt.Decimal) + CDec(txtInsuranceAmt.Decimal)) - mdecFinalSalaryAdvancePendingPrincipalAmt, GUI.fmtCurrency)
            'Hemant (22 Nov 2024) -- [- mdecFinalSalaryAdvancePendingPrincipalAmt]
            'Hemant (13 Dec 2022) -- End
            'Hemant (10 Nov 2022) -- End
            'Hemant (12 Oct 2022) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "nudDuration_ValueChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    'Hemant (07 Jul 2023) -- Start
    'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
    Private Sub nudTitleValidity_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudTitleValidity.ValueChanged
        Try
            dtpTitleExpiryDate.Value = dtpTitleIssueDate.Value.Date.AddYears(CInt(nudTitleValidity.Value))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpTitleIssueDate_ValueChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (07 July 2023) -- End
#End Region

#Region "Gridview's Event"
    Private Sub dgvLoanApplicationAttachment_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLoanApplicationAttachment.CellContentClick
        Try
            Cursor.Current = Cursors.WaitCursor
            If e.RowIndex >= 0 Then
                Dim xrow() As DataRow

                If CInt(dgvLoanApplicationAttachment.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) > 0 Then
                    xrow = mdtLoanApplicationDocument.Select("scanattachtranunkid = " & CInt(dgvLoanApplicationAttachment.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) & "")
                Else
                    xrow = mdtLoanApplicationDocument.Select("GUID = '" & dgvLoanApplicationAttachment.Rows(e.RowIndex).Cells(objcolhGUID.Index).Value.ToString & "'")
                End If

                If e.ColumnIndex = objcohDelete.Index Then
                    If xrow.Length > 0 Then
                        If MessageBox.Show(Language.getMessage(mstrModuleName, 36, "Are you sure, you want to delete this attachment?"), "Aruti", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                            xrow(0).Item("AUD") = "D"
                            Call FillLoanApplicationAttachment()
                        End If
                    End If
                ElseIf e.ColumnIndex = objcolhDownload.Index Then
                    Dim xPath As String = ""

                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("file_path").ToString
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If

                    Dim finfo As New IO.FileInfo(xPath)

                    sfdAttachment.Filter = "File Format (" & finfo.Extension & ")|*" & finfo.Extension & ""
                    sfdAttachment.FileName = xrow(0).Item("filename").ToString

                    If sfdAttachment.ShowDialog = Windows.Forms.DialogResult.OK Then

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            Dim strError As String = ""
                            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                            If blnIsIISInstalled Then

                                Dim filebytes As Byte() = clsFileUploadDownload.DownloadFile(xrow(0).Item("filepath").ToString, xrow(0).Item("fileuniquename").ToString, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)

                                If strError <> "" Then
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.OkOnly)
                                    Exit Sub
                                End If
                                If filebytes IsNot Nothing Then
                                    Dim ms As New MemoryStream(filebytes)
                                    Dim fs As New FileStream(sfdAttachment.FileName, FileMode.Create)
                                    ms.WriteTo(fs)

                                    ms.Close()
                                    fs.Close()
                                    fs.Dispose()
                                Else
                                    If System.IO.File.Exists(xrow(0).Item("filepath").ToString) Then
                                        xPath = xrow(0).Item("filepath").ToString

                                        Dim strFileUniqueName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & "_" & DateTime.Now.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(CStr(xrow(0).Item("filepath").ToString))

                                        If clsFileUploadDownload.UploadFile(xrow(0).Item("filepath").ToString, mstrFolderName, strFileUniqueName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then

                                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                            Exit Try
                                        Else
                                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                                strPath += "/"
                                            End If
                                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileUniqueName
                                            xrow(0).Item("AUD") = "U"
                                            xrow(0).Item("fileuniquename") = strFileUniqueName
                                            xrow(0).Item("filepath") = strPath
                                            xrow(0).Item("filesize") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).Item("filesize_kb") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).AcceptChanges()
                                            Dim objScanAttach As New clsScan_Attach_Documents
                                            objScanAttach._Datatable = mdtLoanApplicationDocument
                                            objScanAttach.InsertUpdateDelete_Documents()
                                            If objDocument._Message <> "" Then
                                                objScanAttach = Nothing
                                                eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
                                                Exit Try
                                            End If
                                            objScanAttach = Nothing
                                            xrow(0).Item("AUD") = ""
                                            xrow(0).AcceptChanges()
                                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                                        End If
                                    Else
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "sorry, file you are trying to access does not exists on Aruti self service application folder."), enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                End If
                            Else
                                IO.File.Copy(xPath, sfdAttachment.FileName, True)
                            End If
                        Else
                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvLoanApplicationAttachment_CellContentClick :", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
    Private Sub dgvLoanApplicationOtherAttachment_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvLoanApplicationOtherAttachment.CellContentClick
        Try
            Cursor.Current = Cursors.WaitCursor
            If e.RowIndex >= 0 Then
                Dim xrow() As DataRow

                If CInt(dgvLoanApplicationOtherAttachment.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) > 0 Then
                    xrow = mdtOtherLoanApplicationDocument.Select("scanattachtranunkid = " & CInt(dgvLoanApplicationOtherAttachment.Rows(e.RowIndex).Cells(objcolhScanUnkId.Index).Value) & "")
                Else
                    xrow = mdtOtherLoanApplicationDocument.Select("GUID = '" & dgvLoanApplicationOtherAttachment.Rows(e.RowIndex).Cells(objcolhGUID.Index).Value.ToString & "'")
                End If

                If e.ColumnIndex = objcohDelete.Index Then
                    If xrow.Length > 0 Then
                        If MessageBox.Show(Language.getMessage(mstrModuleName, 36, "Are you sure, you want to delete this attachment?"), "Aruti", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                            xrow(0).Item("AUD") = "D"
                            Call FillLoanApplicationAttachment()
                        End If
                    End If
                ElseIf e.ColumnIndex = objcolhDownload.Index Then
                    Dim xPath As String = ""

                    If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                        xPath = xrow(0).Item("file_path").ToString
                    ElseIf xrow(0).Item("GUID").ToString.Trim <> "" Then
                        xPath = xrow(0).Item("localpath").ToString
                    End If

                    Dim finfo As New IO.FileInfo(xPath)

                    sfdAttachment.Filter = "File Format (" & finfo.Extension & ")|*" & finfo.Extension & ""
                    sfdAttachment.FileName = xrow(0).Item("filename").ToString

                    If sfdAttachment.ShowDialog = Windows.Forms.DialogResult.OK Then

                        If CInt(xrow(0).Item("scanattachtranunkid")) > 0 Then
                            Dim strError As String = ""
                            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                            If blnIsIISInstalled Then

                                Dim filebytes As Byte() = clsFileUploadDownload.DownloadFile(xrow(0).Item("filepath").ToString, xrow(0).Item("fileuniquename").ToString, mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)

                                If strError <> "" Then
                                    eZeeMsgBox.Show(strError, enMsgBoxStyle.OkOnly)
                                    Exit Sub
                                End If
                                If filebytes IsNot Nothing Then
                                    Dim ms As New MemoryStream(filebytes)
                                    Dim fs As New FileStream(sfdAttachment.FileName, FileMode.Create)
                                    ms.WriteTo(fs)

                                    ms.Close()
                                    fs.Close()
                                    fs.Dispose()
                                Else
                                    If System.IO.File.Exists(xrow(0).Item("filepath").ToString) Then
                                        xPath = xrow(0).Item("filepath").ToString

                                        Dim strFileUniqueName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & "_" & DateTime.Now.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(CStr(xrow(0).Item("filepath").ToString))

                                        If clsFileUploadDownload.UploadFile(xrow(0).Item("filepath").ToString, mstrFolderName, strFileUniqueName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then

                                            eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                            Exit Try
                                        Else
                                            Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                            If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                                strPath += "/"
                                            End If
                                            strPath += "uploadimage/" & mstrFolderName & "/" + strFileUniqueName
                                            xrow(0).Item("AUD") = "U"
                                            xrow(0).Item("fileuniquename") = strFileUniqueName
                                            xrow(0).Item("filepath") = strPath
                                            xrow(0).Item("filesize") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).Item("filesize_kb") = CInt((New System.IO.FileInfo(xPath)).Length / 1024)
                                            xrow(0).AcceptChanges()
                                            Dim objScanAttach As New clsScan_Attach_Documents
                                            objScanAttach._Datatable = mdtOtherLoanApplicationDocument
                                            objScanAttach.InsertUpdateDelete_Documents()
                                            If objDocument._Message <> "" Then
                                                objScanAttach = Nothing
                                                eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
                                                Exit Try
                                            End If
                                            objScanAttach = Nothing
                                            xrow(0).Item("AUD") = ""
                                            xrow(0).AcceptChanges()
                                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                                        End If
                                    Else
                                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "sorry, file you are trying to access does not exists on Aruti self service application folder."), enMsgBoxStyle.Information)
                                        Exit Sub
                                    End If
                                End If
                            Else
                                IO.File.Copy(xPath, sfdAttachment.FileName, True)
                            End If
                        Else
                            IO.File.Copy(xPath, sfdAttachment.FileName, True)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvLoanApplicationOtherAttachment_CellContentClick :", mstrModuleName)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub
    'Hemant (27 Oct 2022) -- End

#End Region

#Region "Label Events"
    Private Sub lblMaxInstallment_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblMaxInstallment.DoubleClick
        Try
            txtFormulaExpr.Visible = Not txtFormulaExpr.Visible
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lblMaxInstallment_DoubleClick", mstrModuleName)
        End Try
    End Sub
#End Region

#Region "Checkbox Events"

    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
    Private Sub chkMakeExceptionalApplication_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMakeExceptionalApplication.CheckedChanged
        Try
            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            'If chkMakeExceptionalApplication.Checked = True AndAlso mintOtherDocumentunkid > 0 Then
            If chkMakeExceptionalApplication.Checked = True AndAlso mstrOtherDocumentIds.Trim.Length > 0 Then
                'Pinkal (23-Nov-2022) -- End
                pnlOtherScanAttachment.Visible = True
            Else
                pnlOtherScanAttachment.Visible = False
            End If
            AttachementLocationChange()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkMakeExceptionalApplication_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (27 Oct 2022) -- End

    'Hemant (22 Nov 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
    Private Sub chkSalaryAdvanceLiquidation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSalaryAdvanceLiquidation.CheckedChanged
        Try
            If chkSalaryAdvanceLiquidation.Checked = True Then
                mdecFinalSalaryAdvancePendingPrincipalAmt = mdecActualSalaryAdvancePendingPrincipalAmt
            Else
                mdecFinalSalaryAdvancePendingPrincipalAmt = 0
            End If

            txtTakeHome.Text = Format(CDec(txtLoanAmt.Decimal) - (CDec(txtOutStandingPrincipalAmt.Decimal) + CDec(txtOutStandingInterestAmt.Decimal) + CDec(txtInsuranceAmt.Decimal)) - mdecFinalSalaryAdvancePendingPrincipalAmt, GUI.fmtCurrency)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSalaryAdvanceLiquidation_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (22 Nov 2024) -- End


#End Region

#Region "DateTimePicker Events"

    'Hemant (07 Jul 2023) -- Start
    'Enhancement : NMB : Add new fields Title Issue date , Title Validity, Title Expiry date on Mortgage loan application and approval screen
    Private Sub dtpTitleIssueDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpTitleIssueDate.ValueChanged
        Try
            dtpTitleExpiryDate.Value = dtpTitleIssueDate.Value.Date.AddYears(CInt(nudTitleValidity.Value))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtpTitleIssueDate_ValueChanged", mstrModuleName)
        End Try
    End Sub
    'Hemant (07 July 2023) -- End

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddAttachment.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddAttachment.GradientForeColor = GUI._ButttonFontColor

            Me.btnConfirmSign.GradientBackColor = GUI._ButttonBackColor
            Me.btnConfirmSign.GradientForeColor = GUI._ButttonFontColor

            Me.btnAddOtherAttachment.GradientBackColor = GUI._ButttonBackColor
            Me.btnAddOtherAttachment.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.lblLoanScheme.Text = Language._Object.getCaption(Me.lblLoanScheme.Name, Me.lblLoanScheme.Text)
            Me.lblEmpName.Text = Language._Object.getCaption(Me.lblEmpName.Name, Me.lblEmpName.Text)
            Me.lblLoanSchemeCategory.Text = Language._Object.getCaption(Me.lblLoanSchemeCategory.Name, Me.lblLoanSchemeCategory.Text)
            Me.lblRepaymentDays.Text = Language._Object.getCaption(Me.lblRepaymentDays.Name, Me.lblRepaymentDays.Text)
            Me.lblMaxLoanAmount.Text = Language._Object.getCaption(Me.lblMaxLoanAmount.Name, Me.lblMaxLoanAmount.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.lblBlockNo.Text = Language._Object.getCaption(Me.lblBlockNo.Name, Me.lblBlockNo.Text)
            Me.lblPlotNo.Text = Language._Object.getCaption(Me.lblPlotNo.Name, Me.lblPlotNo.Text)
            Me.lblFSV.Text = Language._Object.getCaption(Me.lblFSV.Name, Me.lblFSV.Text)
            Me.lblLONo.Text = Language._Object.getCaption(Me.lblLONo.Name, Me.lblLONo.Text)
            Me.lblCTNo.Text = Language._Object.getCaption(Me.lblCTNo.Name, Me.lblCTNo.Text)
            Me.lblMarketValue.Text = Language._Object.getCaption(Me.lblMarketValue.Name, Me.lblMarketValue.Text)
            Me.lblTownCity.Text = Language._Object.getCaption(Me.lblTownCity.Name, Me.lblTownCity.Text)
            Me.lblStreet.Text = Language._Object.getCaption(Me.lblStreet.Name, Me.lblStreet.Text)
            Me.lblAmt.Text = Language._Object.getCaption(Me.lblAmt.Name, Me.lblAmt.Text)
            Me.lblInterestCalcType.Text = Language._Object.getCaption(Me.lblInterestCalcType.Name, Me.lblInterestCalcType.Text)
            Me.lblIntAmt.Text = Language._Object.getCaption(Me.lblIntAmt.Name, Me.lblIntAmt.Text)
            Me.lblPrincipalAmt.Text = Language._Object.getCaption(Me.lblPrincipalAmt.Name, Me.lblPrincipalAmt.Text)
            Me.lblLoanInterest.Text = Language._Object.getCaption(Me.lblLoanInterest.Name, Me.lblLoanInterest.Text)
            Me.lblEMIInstallments.Text = Language._Object.getCaption(Me.lblEMIInstallments.Name, Me.lblEMIInstallments.Text)
            Me.lblEMIAmount.Text = Language._Object.getCaption(Me.lblEMIAmount.Name, Me.lblEMIAmount.Text)
            Me.elLoanAmountCalculation.Text = Language._Object.getCaption(Me.elLoanAmountCalculation.Name, Me.elLoanAmountCalculation.Text)
            Me.lblLoanCalcType.Text = Language._Object.getCaption(Me.lblLoanCalcType.Name, Me.lblLoanCalcType.Text)
            Me.lblYOM.Text = Language._Object.getCaption(Me.lblYOM.Name, Me.lblYOM.Text)
            Me.lblModel.Text = Language._Object.getCaption(Me.lblModel.Name, Me.lblModel.Text)
            Me.lblMaxInstallment.Text = Language._Object.getCaption(Me.lblMaxInstallment.Name, Me.lblMaxInstallment.Text)
            Me.lblInsuranceRate.Text = Language._Object.getCaption(Me.lblInsuranceRate.Name, Me.lblInsuranceRate.Text)
            Me.lblInsuranceAmt.Text = Language._Object.getCaption(Me.lblInsuranceAmt.Name, Me.lblInsuranceAmt.Text)
            Me.lblTakeHome.Text = Language._Object.getCaption(Me.lblTakeHome.Name, Me.lblTakeHome.Text)
            Me.lblPurposeOfCredit.Text = Language._Object.getCaption(Me.lblPurposeOfCredit.Name, Me.lblPurposeOfCredit.Text)
            Me.btnAddAttachment.Text = Language._Object.getCaption(Me.btnAddAttachment.Name, Me.btnAddAttachment.Text)
            Me.lblDocumentType.Text = Language._Object.getCaption(Me.lblDocumentType.Name, Me.lblDocumentType.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.chkconfirmSign.Text = Language._Object.getCaption(Me.chkconfirmSign.Name, Me.chkconfirmSign.Text)
            Me.lblempsign.Text = Language._Object.getCaption(Me.lblempsign.Name, Me.lblempsign.Text)
            Me.btnConfirmSign.Text = Language._Object.getCaption(Me.btnConfirmSign.Name, Me.btnConfirmSign.Text)
            Me.lblMaxInstallmentAmt.Text = Language._Object.getCaption(Me.lblMaxInstallmentAmt.Name, Me.lblMaxInstallmentAmt.Text)
            Me.lblMinLoanAmount.Text = Language._Object.getCaption(Me.lblMinLoanAmount.Name, Me.lblMinLoanAmount.Text)
            Me.chkMakeExceptionalApplication.Text = Language._Object.getCaption(Me.chkMakeExceptionalApplication.Name, Me.chkMakeExceptionalApplication.Text)
			Me.lblOutStandingPrincipalAmt.Text = Language._Object.getCaption(Me.lblOutStandingPrincipalAmt.Name, Me.lblOutStandingPrincipalAmt.Text)
			Me.lblOutStandingInterestAmt.Text = Language._Object.getCaption(Me.lblOutStandingInterestAmt.Name, Me.lblOutStandingInterestAmt.Text)
			Me.lblInstallmentPaid.Text = Language._Object.getCaption(Me.lblInstallmentPaid.Name, Me.lblInstallmentPaid.Text)
            Me.lblColour.Text = Language._Object.getCaption(Me.lblColour.Name, Me.lblColour.Text)
            Me.lblSupplier.Text = Language._Object.getCaption(Me.lblSupplier.Name, Me.lblSupplier.Text)
            Me.lblChassisNo.Text = Language._Object.getCaption(Me.lblChassisNo.Name, Me.lblChassisNo.Text)
            Me.btnAddOtherAttachment.Text = Language._Object.getCaption(Me.btnAddOtherAttachment.Name, Me.btnAddOtherAttachment.Text)
            Me.lblOtherDocumentType.Text = Language._Object.getCaption(Me.lblOtherDocumentType.Name, Me.lblOtherDocumentType.Text)
            Me.lblTitleIssueDate.Text = Language._Object.getCaption(Me.lblTitleIssueDate.Name, Me.lblTitleIssueDate.Text)
            Me.lblTitleExpriryDate.Text = Language._Object.getCaption(Me.lblTitleExpriryDate.Name, Me.lblTitleExpriryDate.Text)
            Me.lblTitleValidity.Text = Language._Object.getCaption(Me.lblTitleValidity.Name, Me.lblTitleValidity.Text)
            Me.lblEmplDate.Text = Language._Object.getCaption(Me.lblEmplDate.Name, Me.lblEmplDate.Text)
            Me.lblPayPeriodsEOC.Text = Language._Object.getCaption(Me.lblPayPeriodsEOC.Name, Me.lblPayPeriodsEOC.Text)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
			Me.lblTotalCIF.Text = Language._Object.getCaption(Me.lblTotalCIF.Name, Me.lblTotalCIF.Text)
			Me.lblModelNo.Text = Language._Object.getCaption(Me.lblModelNo.Name, Me.lblModelNo.Text)
			Me.lblInvoiceValue.Text = Language._Object.getCaption(Me.lblInvoiceValue.Name, Me.lblInvoiceValue.Text)
			Me.lblEstimatedTax.Text = Language._Object.getCaption(Me.lblEstimatedTax.Name, Me.lblEstimatedTax.Text)
			Me.lblEngineCapacity.Text = Language._Object.getCaption(Me.lblEngineCapacity.Name, Me.lblEngineCapacity.Text)
			Me.lblEngineNo.Text = Language._Object.getCaption(Me.lblEngineNo.Name, Me.lblEngineNo.Text)
            Me.LblBOQ.Text = Language._Object.getCaption(Me.LblBOQ.Name, Me.LblBOQ.Text)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.colhDocType.HeaderText = Language._Object.getCaption(Me.colhDocType.Name, Me.colhDocType.HeaderText)
            Me.colhName.HeaderText = Language._Object.getCaption(Me.colhName.Name, Me.colhName.HeaderText)
            Me.colhSize.HeaderText = Language._Object.getCaption(Me.colhSize.Name, Me.colhSize.HeaderText)
            Me.colhOtherDocType.HeaderText = Language._Object.getCaption(Me.colhOtherDocType.Name, Me.colhOtherDocType.HeaderText)
            Me.colhOtherName.HeaderText = Language._Object.getCaption(Me.colhOtherName.Name, Me.colhOtherName.HeaderText)
            Me.colhOtherSize.HeaderText = Language._Object.getCaption(Me.colhOtherSize.Name, Me.colhOtherSize.HeaderText)
            Me.chkSalaryAdvanceLiquidation.Text = Language._Object.getCaption(Me.chkSalaryAdvanceLiquidation.Name, Me.chkSalaryAdvanceLiquidation.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage("frmNewMDI", 77, "Sending Email(s) process is in progress. Please wait for some time.")
            Language.setMessage(mstrModuleName, 1, "Employee is compulsory information. Please select Employee to continue.")
            Language.setMessage(mstrModuleName, 2, "Loan Scheme is compulsory information. Please select Loan Scheme to continue.")
            Language.setMessage(mstrModuleName, 3, "Applied Amount cannot be blank. Applied Amount is compulsory information.")
            Language.setMessage(mstrModuleName, 4, "Currency is compulsory information.Please select currency.")
            Language.setMessage(mstrModuleName, 5, "Installment Amount cannot be 0.Please define installment amount greater than 0.")
            Language.setMessage(mstrModuleName, 6, "Installment Amount cannot be greater than Loan Amount.")
            Language.setMessage(mstrModuleName, 7, "Installment months cannot be greater than")
            Language.setMessage(mstrModuleName, 8, "Sorry, you can not apply for this loan scheme: Reason, Principal amount  is exceeding the Max Principal Amt")
            Language.setMessage(mstrModuleName, 9, "Sorry, you can not apply for this loan scheme: Reason, Installment amount  is exceeding the Max Installment amount")
            Language.setMessage(mstrModuleName, 10, "Sorry, you can not apply for this loan scheme: Reason, Principal amount  should be greater than the Minimum Principal amount")
            Language.setMessage(mstrModuleName, 11, "Principal Amount cannot be 0. Please enter Principal amount.")
            Language.setMessage(mstrModuleName, 12, "Purpose Of Credit is compulsory information. Please enter Purpose Of Credit to continue.")
            Language.setMessage(mstrModuleName, 13, "Plot No is compulsory information. Please enter Plot No to continue.")
            Language.setMessage(mstrModuleName, 15, "Street is compulsory information. Please enter Street to continue.")
            Language.setMessage(mstrModuleName, 16, "Town/City is compulsory information. Please enter Town/City to continue.")
            Language.setMessage(mstrModuleName, 17, "Market Value cannot be 0. Please enter Market Value.")
            Language.setMessage(mstrModuleName, 18, "CT No is compulsory information. Please enter CT No to continue.")
            Language.setMessage(mstrModuleName, 20, "FSV cannot be 0.Please enter FSV.")
            Language.setMessage(mstrModuleName, 21, "Model is compulsory information. Please enter Model to continue.")
            Language.setMessage(mstrModuleName, 22, "YOM is compulsory information. Please enter YOM to continue.")
            Language.setMessage(mstrModuleName, 23, "Sorry, you are not confirmed yet. Please contact HR support")
            Language.setMessage(mstrModuleName, 24, "Sorry, your contract duration is less than 6 months. Please contact HR support")
            Language.setMessage(mstrModuleName, 25, "Sorry, NIDA number is missing or in pending state")
            Language.setMessage(mstrModuleName, 26, "Sorry, you have a pending disciplinary issue. Kindly contact HR ER.")
            Language.setMessage(mstrModuleName, 27, "Scan/Document(s) is mandatory information.Please attach Scan/Document(s) in order to perform operation.")
            Language.setMessage(mstrModuleName, 28, "Sorry, installments cannot go beyond the end of contract date")
            Language.setMessage(mstrModuleName, 29, "Sorry, no of installments selected cannot go beyond the retirement date")
            Language.setMessage(mstrModuleName, 30, "Sorry, signature missing. Please update signature from your profile")
            Language.setMessage(mstrModuleName, 31, "Sorry, Please confirm your signature first.")
            Language.setMessage(mstrModuleName, 32, "Selected information is already present for particular employee.")
            Language.setMessage(mstrModuleName, 33, "for")
            Language.setMessage(mstrModuleName, 34, " Scheme.")
            Language.setMessage(mstrModuleName, 35, "Configuration Path does not Exist.")
            Language.setMessage(mstrModuleName, 36, "Are you sure, you want to delete this attachment?")
            Language.setMessage(mstrModuleName, 37, "sorry, file you are trying to access does not exists on Aruti self service application folder.")
            Language.setMessage(mstrModuleName, 38, "All Scan/Document(s) are mandatory information. Please attach all Scan/Document(s) in order to perform operation.")
            Language.setMessage(mstrModuleName, 39, "Period Duration From :")
            Language.setMessage(mstrModuleName, 40, "To")
            Language.setMessage(mstrModuleName, 41, "Type to Search")
            Language.setMessage(mstrModuleName, 42, " set for selected scheme.")
            Language.setMessage(mstrModuleName, 43, "Document Type is compulsory information. Please select Document Type to continue.")
            Language.setMessage(mstrModuleName, 45, "Loan Scheme is not mapped with Role and Level.please map Loan scheme with role and level.")
			Language.setMessage(mstrModuleName, 46, "Sorry, you can not apply for Topup for this loan scheme: Reason, TopUp is not allowed")
			Language.setMessage(mstrModuleName, 47, "Sorry, you can not apply for Topup for this loan scheme: Reason, No of Installment Paid should be greater than minimum No of Installment Paid")
            Language.setMessage(mstrModuleName, 48, "Sorry, you can not apply for this loan scheme: Reason, Approval for Another Loan is still in Pending Stage.")
            Language.setMessage(mstrModuleName, 49, "Chassis No is compulsory information. Please enter Chassis No to continue.")
            Language.setMessage(mstrModuleName, 50, "Supplier is compulsory information. Please enter Supplier to continue.")
            Language.setMessage(mstrModuleName, 51, "Colour is compulsory information. Please enter Colour to continue.")
            Language.setMessage(mstrModuleName, 54, "Loan Application Submitted successfully.")
            Language.setMessage(mstrModuleName, 55, "You cannot apply for this loan application.Reason : Loan application's loan amount is not match with the approver level's minimum and maximum range. ")
            Language.setMessage(mstrModuleName, 56, "Installment months cannot be less than")
            Language.setMessage(mstrModuleName, 57, "Sorry, you do not qualify for a top-up at the moment. The take home amount should be greater than 0")
            Language.setMessage(mstrModuleName, 59, "Principle amount cannot be greater than the FSV")
            Language.setMessage(mstrModuleName, 60, "Mobile No is compulsory information. Please set Employee's Mobile No to send TOTP.")
            Language.setMessage(mstrModuleName, 61, "Please Set SMS Gateway email as On configuration you set Apply TOTP on Loan application.")
            Language.setMessage(mstrModuleName, 62, "You cannot save this loan application.Reason : As You reached Maximum unsuccessful attempts.Please try after")
            Language.setMessage(mstrModuleName, 63, "minutes")
            Language.setMessage(mstrModuleName, 64, "Title Issue Date is compulsory information. Please enter Title Issue Date to continue.")
            Language.setMessage(mstrModuleName, 65, "Title Validity cannot be 0.Please enter Title Validity.")
            Language.setMessage(mstrModuleName, 66, "Title Expriry Date is compulsory information. Please enter Title Expriry Date to continue.")
            Language.setMessage(mstrModuleName, 67, "Sorry, you can not apply for this Mortgage Loan: Reason, Title Expiry Date should not be less than Date :")
			Language.setMessage(mstrModuleName, 68, "Total CIF cannot be 0.Please enter Total CIF.")
			Language.setMessage(mstrModuleName, 69, "Estimated Tax cannot be 0.Please enter Estimated Tax.")
			Language.setMessage(mstrModuleName, 70, "Invoice Value cannot be 0.Please enter Invoice Value.")
			Language.setMessage(mstrModuleName, 71, "Model No is compulsory information. Please enter Model No to continue.")
			Language.setMessage(mstrModuleName, 72, "Engine No is compulsory information. Please enter Engine No to continue.")
            Language.setMessage(mstrModuleName, 73, "Engine Capacity cannot be 0.Please enter Engine Capacity.")
            Language.setMessage(mstrModuleName, 74, "No of Repayment Days cannot be 0.Please define No of Repayment Days greater than 0.")
            Language.setMessage(mstrModuleName, 75, "Bill of Quantity (BOQ) cannot be blank.Please enter Bill of Quantity (BOQ).")
            Language.setMessage(mstrModuleName, 76, "FSV cannot be more than the Market Value.")
            Language.setMessage(mstrModuleName, 77, "You cannnot apply for this loan application.Reason : you have already past dues days.")
            Language.setMessage(mstrModuleName, 78, "You cannnot apply for this loan application.Reason : you have already exceeded with your credit card limit.")
            Language.setMessage(mstrModuleName, 79, "You cannnot apply for this loan application.Reason : There is no record(s) related to credit card.")
            Language.setMessage(mstrModuleName, 80, "You cannnot apply for this loan application.Reason : you have already past dues days entry for this loan scheme.")
            Language.setMessage(mstrModuleName, 81, "You cannot apply for this loan application.Reason : Percentage(%) of total monthly deduction to gross pay exceed 66.67%.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class