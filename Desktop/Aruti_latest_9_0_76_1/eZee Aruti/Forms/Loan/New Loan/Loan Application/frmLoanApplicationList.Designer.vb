﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanApplicationList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanApplicationList))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.lnkAllocation = New System.Windows.Forms.LinkLabel
        Me.cboAppAmountCondition = New System.Windows.Forms.ComboBox
        Me.objlnStLine3 = New eZee.Common.eZeeStraightLine
        Me.objlnStLine2 = New eZee.Common.eZeeStraightLine
        Me.cboAmountCondition = New System.Windows.Forms.ComboBox
        Me.objlnStLine1 = New eZee.Common.eZeeStraightLine
        Me.dtpAppDate = New System.Windows.Forms.DateTimePicker
        Me.lblAppDate = New System.Windows.Forms.Label
        Me.txtApplicationNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblAppNo = New System.Windows.Forms.Label
        Me.lblApprovedAmount = New System.Windows.Forms.Label
        Me.txtApprovedAmount = New eZee.TextBox.NumericTextBox
        Me.lblAmount = New System.Windows.Forms.Label
        Me.txtAmount = New eZee.TextBox.NumericTextBox
        Me.cboLoanAdvance = New System.Windows.Forms.ComboBox
        Me.lblLoanAdvance = New System.Windows.Forms.Label
        Me.cboStatus = New System.Windows.Forms.ComboBox
        Me.lblStatus = New System.Windows.Forms.Label
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.objbtnSearchTrnHead = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnOperations = New eZee.Common.eZeeSplitButton
        Me.cmnuOperation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuAssign = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalApprove = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalCancelApproved = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuGlobalAssign = New System.Windows.Forms.ToolStripMenuItem
        Me.btnDelete = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnEdit = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnNew = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.cmnuStatus = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.dgvLoanApplicationList = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn16 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApplicationNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAppDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLoanScheme = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhLoan_Advance = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhApp_Amount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhCancelRemark = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsLoan = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhStatusunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhEmpId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSchemeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhApprover = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsExternal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhprocesspendingloanunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsFlexcube = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhSkipApproval = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.cmnuOperation.SuspendLayout()
        CType(Me.dgvLoanApplicationList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.lnkAllocation)
        Me.gbFilterCriteria.Controls.Add(Me.cboAppAmountCondition)
        Me.gbFilterCriteria.Controls.Add(Me.objlnStLine3)
        Me.gbFilterCriteria.Controls.Add(Me.objlnStLine2)
        Me.gbFilterCriteria.Controls.Add(Me.cboAmountCondition)
        Me.gbFilterCriteria.Controls.Add(Me.objlnStLine1)
        Me.gbFilterCriteria.Controls.Add(Me.dtpAppDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblAppDate)
        Me.gbFilterCriteria.Controls.Add(Me.txtApplicationNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblAppNo)
        Me.gbFilterCriteria.Controls.Add(Me.lblApprovedAmount)
        Me.gbFilterCriteria.Controls.Add(Me.txtApprovedAmount)
        Me.gbFilterCriteria.Controls.Add(Me.lblAmount)
        Me.gbFilterCriteria.Controls.Add(Me.txtAmount)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanAdvance)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanAdvance)
        Me.gbFilterCriteria.Controls.Add(Me.cboStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblStatus)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.cboLoanScheme)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchTrnHead)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(7, 3)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(859, 90)
        Me.gbFilterCriteria.TabIndex = 1
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lnkAllocation
        '
        Me.lnkAllocation.BackColor = System.Drawing.Color.Transparent
        Me.lnkAllocation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnkAllocation.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.lnkAllocation.Location = New System.Drawing.Point(709, 6)
        Me.lnkAllocation.Name = "lnkAllocation"
        Me.lnkAllocation.Size = New System.Drawing.Size(94, 15)
        Me.lnkAllocation.TabIndex = 0
        Me.lnkAllocation.TabStop = True
        Me.lnkAllocation.Text = "Allocation"
        Me.lnkAllocation.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboAppAmountCondition
        '
        Me.cboAppAmountCondition.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboAppAmountCondition.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAppAmountCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAppAmountCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAppAmountCondition.FormattingEnabled = True
        Me.cboAppAmountCondition.Location = New System.Drawing.Point(809, 60)
        Me.cboAppAmountCondition.Name = "cboAppAmountCondition"
        Me.cboAppAmountCondition.Size = New System.Drawing.Size(42, 21)
        Me.cboAppAmountCondition.TabIndex = 10
        '
        'objlnStLine3
        '
        Me.objlnStLine3.BackColor = System.Drawing.Color.Transparent
        Me.objlnStLine3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnStLine3.ForeColor = System.Drawing.Color.DarkGray
        Me.objlnStLine3.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objlnStLine3.Location = New System.Drawing.Point(639, 24)
        Me.objlnStLine3.Name = "objlnStLine3"
        Me.objlnStLine3.Size = New System.Drawing.Size(3, 65)
        Me.objlnStLine3.TabIndex = 208
        '
        'objlnStLine2
        '
        Me.objlnStLine2.BackColor = System.Drawing.Color.Transparent
        Me.objlnStLine2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnStLine2.ForeColor = System.Drawing.Color.DarkGray
        Me.objlnStLine2.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objlnStLine2.Location = New System.Drawing.Point(463, 24)
        Me.objlnStLine2.Name = "objlnStLine2"
        Me.objlnStLine2.Size = New System.Drawing.Size(2, 65)
        Me.objlnStLine2.TabIndex = 207
        '
        'cboAmountCondition
        '
        Me.cboAmountCondition.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboAmountCondition.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboAmountCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAmountCondition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAmountCondition.FormattingEnabled = True
        Me.cboAmountCondition.Location = New System.Drawing.Point(809, 33)
        Me.cboAmountCondition.Name = "cboAmountCondition"
        Me.cboAmountCondition.Size = New System.Drawing.Size(42, 21)
        Me.cboAmountCondition.TabIndex = 8
        '
        'objlnStLine1
        '
        Me.objlnStLine1.BackColor = System.Drawing.Color.Transparent
        Me.objlnStLine1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlnStLine1.ForeColor = System.Drawing.Color.DarkGray
        Me.objlnStLine1.LineType = eZee.Common.StraightLineTypes.Vertical
        Me.objlnStLine1.Location = New System.Drawing.Point(256, 24)
        Me.objlnStLine1.Name = "objlnStLine1"
        Me.objlnStLine1.Size = New System.Drawing.Size(3, 65)
        Me.objlnStLine1.TabIndex = 206
        '
        'dtpAppDate
        '
        Me.dtpAppDate.Checked = False
        Me.dtpAppDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpAppDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpAppDate.Location = New System.Drawing.Point(536, 60)
        Me.dtpAppDate.Name = "dtpAppDate"
        Me.dtpAppDate.ShowCheckBox = True
        Me.dtpAppDate.Size = New System.Drawing.Size(94, 21)
        Me.dtpAppDate.TabIndex = 6
        '
        'lblAppDate
        '
        Me.lblAppDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppDate.Location = New System.Drawing.Point(469, 63)
        Me.lblAppDate.Name = "lblAppDate"
        Me.lblAppDate.Size = New System.Drawing.Size(61, 15)
        Me.lblAppDate.TabIndex = 203
        Me.lblAppDate.Text = "App. Date"
        '
        'txtApplicationNo
        '
        Me.txtApplicationNo.Flags = 0
        Me.txtApplicationNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApplicationNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtApplicationNo.Location = New System.Drawing.Point(536, 33)
        Me.txtApplicationNo.Name = "txtApplicationNo"
        Me.txtApplicationNo.Size = New System.Drawing.Size(94, 21)
        Me.txtApplicationNo.TabIndex = 5
        '
        'lblAppNo
        '
        Me.lblAppNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAppNo.Location = New System.Drawing.Point(469, 36)
        Me.lblAppNo.Name = "lblAppNo"
        Me.lblAppNo.Size = New System.Drawing.Size(61, 15)
        Me.lblAppNo.TabIndex = 167
        Me.lblAppNo.Text = "App. No."
        Me.lblAppNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblApprovedAmount
        '
        Me.lblApprovedAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApprovedAmount.Location = New System.Drawing.Point(647, 63)
        Me.lblApprovedAmount.Name = "lblApprovedAmount"
        Me.lblApprovedAmount.Size = New System.Drawing.Size(71, 15)
        Me.lblApprovedAmount.TabIndex = 165
        Me.lblApprovedAmount.Text = "App. Amount"
        Me.lblApprovedAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtApprovedAmount
        '
        Me.txtApprovedAmount.AllowNegative = True
        Me.txtApprovedAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtApprovedAmount.DigitsInGroup = 0
        Me.txtApprovedAmount.Flags = 0
        Me.txtApprovedAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtApprovedAmount.Location = New System.Drawing.Point(724, 60)
        Me.txtApprovedAmount.MaxDecimalPlaces = 6
        Me.txtApprovedAmount.MaxWholeDigits = 21
        Me.txtApprovedAmount.Name = "txtApprovedAmount"
        Me.txtApprovedAmount.Prefix = ""
        Me.txtApprovedAmount.RangeMax = 1.7976931348623157E+308
        Me.txtApprovedAmount.RangeMin = -1.7976931348623157E+308
        Me.txtApprovedAmount.Size = New System.Drawing.Size(83, 21)
        Me.txtApprovedAmount.TabIndex = 9
        Me.txtApprovedAmount.Text = "0"
        Me.txtApprovedAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblAmount
        '
        Me.lblAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmount.Location = New System.Drawing.Point(647, 36)
        Me.lblAmount.Name = "lblAmount"
        Me.lblAmount.Size = New System.Drawing.Size(71, 15)
        Me.lblAmount.TabIndex = 159
        Me.lblAmount.Text = "Loan Amount"
        Me.lblAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAmount
        '
        Me.txtAmount.AllowNegative = True
        Me.txtAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAmount.DigitsInGroup = 0
        Me.txtAmount.Flags = 0
        Me.txtAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmount.Location = New System.Drawing.Point(724, 33)
        Me.txtAmount.MaxDecimalPlaces = 6
        Me.txtAmount.MaxWholeDigits = 21
        Me.txtAmount.Name = "txtAmount"
        Me.txtAmount.Prefix = ""
        Me.txtAmount.RangeMax = 1.7976931348623157E+308
        Me.txtAmount.RangeMin = -1.7976931348623157E+308
        Me.txtAmount.Size = New System.Drawing.Size(83, 21)
        Me.txtAmount.TabIndex = 7
        Me.txtAmount.Text = "0"
        Me.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'cboLoanAdvance
        '
        Me.cboLoanAdvance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanAdvance.FormattingEnabled = True
        Me.cboLoanAdvance.Location = New System.Drawing.Point(347, 33)
        Me.cboLoanAdvance.Name = "cboLoanAdvance"
        Me.cboLoanAdvance.Size = New System.Drawing.Size(110, 21)
        Me.cboLoanAdvance.TabIndex = 3
        '
        'lblLoanAdvance
        '
        Me.lblLoanAdvance.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanAdvance.Location = New System.Drawing.Point(263, 36)
        Me.lblLoanAdvance.Name = "lblLoanAdvance"
        Me.lblLoanAdvance.Size = New System.Drawing.Size(78, 15)
        Me.lblLoanAdvance.TabIndex = 156
        Me.lblLoanAdvance.Text = "Loan/Advance"
        Me.lblLoanAdvance.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboStatus
        '
        Me.cboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboStatus.FormattingEnabled = True
        Me.cboStatus.Location = New System.Drawing.Point(347, 60)
        Me.cboStatus.Name = "cboStatus"
        Me.cboStatus.Size = New System.Drawing.Size(110, 21)
        Me.cboStatus.TabIndex = 4
        '
        'lblStatus
        '
        Me.lblStatus.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatus.Location = New System.Drawing.Point(263, 63)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(78, 15)
        Me.lblStatus.TabIndex = 152
        Me.lblStatus.Text = "Status"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(8, 63)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(73, 15)
        Me.lblLoanScheme.TabIndex = 148
        Me.lblLoanScheme.Text = "Loan Scheme"
        Me.lblLoanScheme.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.DropDownWidth = 300
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Location = New System.Drawing.Point(87, 60)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(163, 21)
        Me.cboLoanScheme.TabIndex = 2
        '
        'objbtnSearchTrnHead
        '
        Me.objbtnSearchTrnHead.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchTrnHead.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrnHead.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchTrnHead.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchTrnHead.BorderSelected = False
        Me.objbtnSearchTrnHead.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchTrnHead.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchTrnHead.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchTrnHead.Location = New System.Drawing.Point(229, 33)
        Me.objbtnSearchTrnHead.Name = "objbtnSearchTrnHead"
        Me.objbtnSearchTrnHead.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchTrnHead.TabIndex = 12
        Me.objbtnSearchTrnHead.Visible = False
        '
        'cboEmployee
        '
        Me.cboEmployee.DropDownWidth = 300
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(87, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(163, 21)
        Me.cboEmployee.TabIndex = 1
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(73, 15)
        Me.lblEmployee.TabIndex = 13
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(832, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(809, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnOperations)
        Me.objFooter.Controls.Add(Me.btnDelete)
        Me.objFooter.Controls.Add(Me.btnEdit)
        Me.objFooter.Controls.Add(Me.btnNew)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 416)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(873, 55)
        Me.objFooter.TabIndex = 2
        '
        'btnOperations
        '
        Me.btnOperations.BorderColor = System.Drawing.Color.Black
        Me.btnOperations.ContextMenuStrip = Me.cmnuOperation
        Me.btnOperations.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperations.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnOperations.GradientForeColor = System.Drawing.SystemColors.WindowText
        Me.btnOperations.Location = New System.Drawing.Point(12, 13)
        Me.btnOperations.Name = "btnOperations"
        Me.btnOperations.ShowDefaultBorderColor = True
        Me.btnOperations.Size = New System.Drawing.Size(102, 30)
        Me.btnOperations.SplitButtonMenu = Me.cmnuOperation
        Me.btnOperations.TabIndex = 0
        Me.btnOperations.Text = "&Operations"
        '
        'cmnuOperation
        '
        Me.cmnuOperation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAssign, Me.mnuGlobalApprove, Me.mnuGlobalCancelApproved, Me.mnuGlobalAssign})
        Me.cmnuOperation.Name = "cmnuOperation"
        Me.cmnuOperation.Size = New System.Drawing.Size(203, 92)
        '
        'mnuAssign
        '
        Me.mnuAssign.Name = "mnuAssign"
        Me.mnuAssign.Size = New System.Drawing.Size(202, 22)
        Me.mnuAssign.Tag = "mnuAssign"
        Me.mnuAssign.Text = "&Assign"
        '
        'mnuGlobalApprove
        '
        Me.mnuGlobalApprove.Name = "mnuGlobalApprove"
        Me.mnuGlobalApprove.Size = New System.Drawing.Size(202, 22)
        Me.mnuGlobalApprove.Tag = "mnuGlobalApprove"
        Me.mnuGlobalApprove.Text = "&Global Approve"
        '
        'mnuGlobalCancelApproved
        '
        Me.mnuGlobalCancelApproved.Name = "mnuGlobalCancelApproved"
        Me.mnuGlobalCancelApproved.Size = New System.Drawing.Size(202, 22)
        Me.mnuGlobalCancelApproved.Text = "Global &Cancel Approved"
        '
        'mnuGlobalAssign
        '
        Me.mnuGlobalAssign.Name = "mnuGlobalAssign"
        Me.mnuGlobalAssign.Size = New System.Drawing.Size(202, 22)
        Me.mnuGlobalAssign.Tag = "mnuGlobalAssign"
        Me.mnuGlobalAssign.Text = "Gl&obal Assign"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.BackColor = System.Drawing.Color.White
        Me.btnDelete.BackgroundImage = CType(resources.GetObject("btnDelete.BackgroundImage"), System.Drawing.Image)
        Me.btnDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnDelete.BorderColor = System.Drawing.Color.Empty
        Me.btnDelete.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnDelete.FlatAppearance.BorderSize = 0
        Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDelete.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDelete.ForeColor = System.Drawing.Color.Black
        Me.btnDelete.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnDelete.GradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Location = New System.Drawing.Point(661, 13)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnDelete.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnDelete.Size = New System.Drawing.Size(97, 30)
        Me.btnDelete.TabIndex = 3
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.BackColor = System.Drawing.Color.White
        Me.btnEdit.BackgroundImage = CType(resources.GetObject("btnEdit.BackgroundImage"), System.Drawing.Image)
        Me.btnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnEdit.BorderColor = System.Drawing.Color.Empty
        Me.btnEdit.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.Black
        Me.btnEdit.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnEdit.GradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Location = New System.Drawing.Point(558, 13)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnEdit.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnEdit.Size = New System.Drawing.Size(97, 30)
        Me.btnEdit.TabIndex = 2
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNew.BackColor = System.Drawing.Color.White
        Me.btnNew.BackgroundImage = CType(resources.GetObject("btnNew.BackgroundImage"), System.Drawing.Image)
        Me.btnNew.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnNew.BorderColor = System.Drawing.Color.Empty
        Me.btnNew.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnNew.FlatAppearance.BorderSize = 0
        Me.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNew.ForeColor = System.Drawing.Color.Black
        Me.btnNew.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnNew.GradientForeColor = System.Drawing.Color.Black
        Me.btnNew.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Location = New System.Drawing.Point(455, 13)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnNew.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnNew.Size = New System.Drawing.Size(97, 30)
        Me.btnNew.TabIndex = 1
        Me.btnNew.Text = "&New"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(764, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'cmnuStatus
        '
        Me.cmnuStatus.Name = "cmnuStatus"
        Me.cmnuStatus.Size = New System.Drawing.Size(61, 4)
        '
        'dgvLoanApplicationList
        '
        Me.dgvLoanApplicationList.AllowUserToAddRows = False
        Me.dgvLoanApplicationList.AllowUserToDeleteRows = False
        Me.dgvLoanApplicationList.AllowUserToResizeRows = False
        Me.dgvLoanApplicationList.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvLoanApplicationList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvLoanApplicationList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dgcolhApplicationNo, Me.dgcolhAppDate, Me.dgcolhEmployee, Me.dgcolhLoanScheme, Me.dgcolhLoan_Advance, Me.dgcolhAmount, Me.dgcolhApp_Amount, Me.dgcolhStatus, Me.dgcolhCancelRemark, Me.objdgcolhIsLoan, Me.objdgcolhStatusunkid, Me.objdgcolhEmpId, Me.objdgcolhSchemeID, Me.objdgcolhApprover, Me.objdgcolhIsExternal, Me.objdgcolhprocesspendingloanunkid, Me.objdgcolhIsFlexcube, Me.objdgcolhSkipApproval})
        Me.dgvLoanApplicationList.Location = New System.Drawing.Point(7, 99)
        Me.dgvLoanApplicationList.MultiSelect = False
        Me.dgvLoanApplicationList.Name = "dgvLoanApplicationList"
        Me.dgvLoanApplicationList.RowHeadersVisible = False
        Me.dgvLoanApplicationList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvLoanApplicationList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLoanApplicationList.Size = New System.Drawing.Size(859, 311)
        Me.dgvLoanApplicationList.TabIndex = 0
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Application No"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "App Date"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Employee"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 200
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Loan Scheme"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "Loan / Advance"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn6.HeaderText = "Loan Amount"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn7.HeaderText = "Approved Amount"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Cancel Remarks"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "IsLoan"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "Statusunkid"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "EmpID"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.HeaderText = "SchemeID"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn13.Visible = False
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.HeaderText = "Approver"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn14.Visible = False
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.HeaderText = "IsExternal"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn15.Visible = False
        '
        'DataGridViewTextBoxColumn16
        '
        Me.DataGridViewTextBoxColumn16.HeaderText = "objdgcolhprocesspendingloanunkid"
        Me.DataGridViewTextBoxColumn16.Name = "DataGridViewTextBoxColumn16"
        Me.DataGridViewTextBoxColumn16.ReadOnly = True
        Me.DataGridViewTextBoxColumn16.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn16.Visible = False
        '
        'dgcolhApplicationNo
        '
        Me.dgcolhApplicationNo.HeaderText = "Application No"
        Me.dgcolhApplicationNo.Name = "dgcolhApplicationNo"
        Me.dgcolhApplicationNo.ReadOnly = True
        '
        'dgcolhAppDate
        '
        Me.dgcolhAppDate.HeaderText = "App Date"
        Me.dgcolhAppDate.Name = "dgcolhAppDate"
        Me.dgcolhAppDate.ReadOnly = True
        '
        'dgcolhEmployee
        '
        Me.dgcolhEmployee.HeaderText = "Employee"
        Me.dgcolhEmployee.Name = "dgcolhEmployee"
        Me.dgcolhEmployee.ReadOnly = True
        Me.dgcolhEmployee.Width = 200
        '
        'dgcolhLoanScheme
        '
        Me.dgcolhLoanScheme.HeaderText = "Loan Scheme"
        Me.dgcolhLoanScheme.Name = "dgcolhLoanScheme"
        Me.dgcolhLoanScheme.ReadOnly = True
        Me.dgcolhLoanScheme.Width = 200
        '
        'dgcolhLoan_Advance
        '
        Me.dgcolhLoan_Advance.HeaderText = "Loan / Advance"
        Me.dgcolhLoan_Advance.Name = "dgcolhLoan_Advance"
        Me.dgcolhLoan_Advance.ReadOnly = True
        '
        'dgcolhAmount
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhAmount.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgcolhAmount.HeaderText = "Loan Amount"
        Me.dgcolhAmount.Name = "dgcolhAmount"
        Me.dgcolhAmount.ReadOnly = True
        '
        'dgcolhApp_Amount
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.dgcolhApp_Amount.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgcolhApp_Amount.HeaderText = "Approved Amount"
        Me.dgcolhApp_Amount.Name = "dgcolhApp_Amount"
        Me.dgcolhApp_Amount.ReadOnly = True
        '
        'dgcolhStatus
        '
        Me.dgcolhStatus.HeaderText = "Status"
        Me.dgcolhStatus.Name = "dgcolhStatus"
        Me.dgcolhStatus.ReadOnly = True
        '
        'dgcolhCancelRemark
        '
        Me.dgcolhCancelRemark.HeaderText = "Cancel Remarks"
        Me.dgcolhCancelRemark.Name = "dgcolhCancelRemark"
        Me.dgcolhCancelRemark.Visible = False
        '
        'objdgcolhIsLoan
        '
        Me.objdgcolhIsLoan.HeaderText = "IsLoan"
        Me.objdgcolhIsLoan.Name = "objdgcolhIsLoan"
        Me.objdgcolhIsLoan.ReadOnly = True
        Me.objdgcolhIsLoan.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsLoan.Visible = False
        '
        'objdgcolhStatusunkid
        '
        Me.objdgcolhStatusunkid.HeaderText = "Statusunkid"
        Me.objdgcolhStatusunkid.Name = "objdgcolhStatusunkid"
        Me.objdgcolhStatusunkid.ReadOnly = True
        Me.objdgcolhStatusunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhStatusunkid.Visible = False
        '
        'objdgcolhEmpId
        '
        Me.objdgcolhEmpId.HeaderText = "EmpID"
        Me.objdgcolhEmpId.Name = "objdgcolhEmpId"
        Me.objdgcolhEmpId.ReadOnly = True
        Me.objdgcolhEmpId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhEmpId.Visible = False
        '
        'objdgcolhSchemeID
        '
        Me.objdgcolhSchemeID.HeaderText = "SchemeID"
        Me.objdgcolhSchemeID.Name = "objdgcolhSchemeID"
        Me.objdgcolhSchemeID.ReadOnly = True
        Me.objdgcolhSchemeID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhSchemeID.Visible = False
        '
        'objdgcolhApprover
        '
        Me.objdgcolhApprover.HeaderText = "Approver"
        Me.objdgcolhApprover.Name = "objdgcolhApprover"
        Me.objdgcolhApprover.ReadOnly = True
        Me.objdgcolhApprover.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhApprover.Visible = False
        '
        'objdgcolhIsExternal
        '
        Me.objdgcolhIsExternal.HeaderText = "IsExternal"
        Me.objdgcolhIsExternal.Name = "objdgcolhIsExternal"
        Me.objdgcolhIsExternal.ReadOnly = True
        Me.objdgcolhIsExternal.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsExternal.Visible = False
        '
        'objdgcolhprocesspendingloanunkid
        '
        Me.objdgcolhprocesspendingloanunkid.HeaderText = "objdgcolhprocesspendingloanunkid"
        Me.objdgcolhprocesspendingloanunkid.Name = "objdgcolhprocesspendingloanunkid"
        Me.objdgcolhprocesspendingloanunkid.ReadOnly = True
        Me.objdgcolhprocesspendingloanunkid.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objdgcolhprocesspendingloanunkid.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhprocesspendingloanunkid.Visible = False
        '
        'objdgcolhIsFlexcube
        '
        Me.objdgcolhIsFlexcube.HeaderText = "IsFlexcube"
        Me.objdgcolhIsFlexcube.Name = "objdgcolhIsFlexcube"
        Me.objdgcolhIsFlexcube.ReadOnly = True
        Me.objdgcolhIsFlexcube.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhIsFlexcube.Visible = False
        '
        'objdgcolhSkipApproval
        '
        Me.objdgcolhSkipApproval.HeaderText = "SkipApproval"
        Me.objdgcolhSkipApproval.Name = "objdgcolhSkipApproval"
        Me.objdgcolhSkipApproval.ReadOnly = True
        Me.objdgcolhSkipApproval.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.objdgcolhSkipApproval.Visible = False
        '
        'frmLoanApplicationList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(873, 471)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.gbFilterCriteria)
        Me.Controls.Add(Me.dgvLoanApplicationList)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoanApplicationList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Loan Application List"
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.cmnuOperation.ResumeLayout(False)
        CType(Me.dgvLoanApplicationList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents lblAmount As System.Windows.Forms.Label
    Friend WithEvents txtAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents cboLoanAdvance As System.Windows.Forms.ComboBox
    Friend WithEvents lblLoanAdvance As System.Windows.Forms.Label
    Friend WithEvents cboStatus As System.Windows.Forms.ComboBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnSearchTrnHead As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnDelete As eZee.Common.eZeeLightButton
    Friend WithEvents btnEdit As eZee.Common.eZeeLightButton
    Friend WithEvents btnNew As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblApprovedAmount As System.Windows.Forms.Label
    Friend WithEvents txtApprovedAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents txtApplicationNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblAppNo As System.Windows.Forms.Label
    Friend WithEvents dtpAppDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblAppDate As System.Windows.Forms.Label
    Friend WithEvents objlnStLine2 As eZee.Common.eZeeStraightLine
    Friend WithEvents objlnStLine1 As eZee.Common.eZeeStraightLine
    Friend WithEvents objlnStLine3 As eZee.Common.eZeeStraightLine
    Friend WithEvents cmnuStatus As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents btnOperations As eZee.Common.eZeeSplitButton
    Friend WithEvents cmnuOperation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuAssign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGlobalApprove As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGlobalAssign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgvLoanApplicationList As System.Windows.Forms.DataGridView
    Friend WithEvents cboAmountCondition As System.Windows.Forms.ComboBox
    Friend WithEvents cboAppAmountCondition As System.Windows.Forms.ComboBox
    Friend WithEvents mnuGlobalCancelApproved As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lnkAllocation As System.Windows.Forms.LinkLabel
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn16 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApplicationNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAppDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoanScheme As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhLoan_Advance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhApp_Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhCancelRemark As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsLoan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhStatusunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhEmpId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSchemeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhApprover As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsExternal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhprocesspendingloanunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsFlexcube As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhSkipApproval As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
