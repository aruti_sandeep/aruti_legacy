﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFlexcubeLoanApplication_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFlexcubeLoanApplication_AddEdit))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.lblLoanScheme = New System.Windows.Forms.Label
        Me.cboLoanScheme = New System.Windows.Forms.ComboBox
        Me.objbtnAddLoanScheme = New eZee.Common.eZeeGradientButton
        Me.cboEmpName = New System.Windows.Forms.ComboBox
        Me.lblEmpName = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.objLoanSchemeSearch = New eZee.Common.eZeeGradientButton
        Me.lblLoanSchemeCategory = New System.Windows.Forms.Label
        Me.cboLoanSchemeCategory = New System.Windows.Forms.ComboBox
        Me.txtRepaymentDays = New eZee.TextBox.NumericTextBox
        Me.lblRepaymentDays = New System.Windows.Forms.Label
        Me.lblMaxLoanAmount = New System.Windows.Forms.Label
        Me.txtMaxLoanAmount = New eZee.TextBox.NumericTextBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnConfirmSign = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblExRate = New System.Windows.Forms.Label
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.objvalPeriodDuration = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtBOQ = New eZee.TextBox.AlphanumericTextBox
        Me.LblBOQ = New System.Windows.Forms.Label
        Me.lblTitleValidity = New System.Windows.Forms.Label
        Me.nudTitleValidity = New System.Windows.Forms.NumericUpDown
        Me.dtpTitleExpiryDate = New System.Windows.Forms.DateTimePicker
        Me.lblTitleExpriryDate = New System.Windows.Forms.Label
        Me.dtpTitleIssueDate = New System.Windows.Forms.DateTimePicker
        Me.lblTitleIssueDate = New System.Windows.Forms.Label
        Me.lblBlockNo = New System.Windows.Forms.Label
        Me.txtFSV = New eZee.TextBox.NumericTextBox
        Me.txtMarketValue = New eZee.TextBox.NumericTextBox
        Me.txtLONo = New eZee.TextBox.AlphanumericTextBox
        Me.lblLONo = New System.Windows.Forms.Label
        Me.txtCTNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblCTNo = New System.Windows.Forms.Label
        Me.lblMarketValue = New System.Windows.Forms.Label
        Me.txtTownCity = New eZee.TextBox.AlphanumericTextBox
        Me.lblTownCity = New System.Windows.Forms.Label
        Me.txtStreet = New eZee.TextBox.AlphanumericTextBox
        Me.lblStreet = New System.Windows.Forms.Label
        Me.txtBlockNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtPlotNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblPlotNo = New System.Windows.Forms.Label
        Me.lblFSV = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.txtEngineCapacity = New eZee.TextBox.NumericTextBox
        Me.lblEngineCapacity = New System.Windows.Forms.Label
        Me.txtEngineNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblEngineNo = New System.Windows.Forms.Label
        Me.txtModelNo = New eZee.TextBox.AlphanumericTextBox
        Me.lblModelNo = New System.Windows.Forms.Label
        Me.txtInvoiceValue = New eZee.TextBox.NumericTextBox
        Me.lblInvoiceValue = New System.Windows.Forms.Label
        Me.txtEstimatedTax = New eZee.TextBox.NumericTextBox
        Me.lblEstimatedTax = New System.Windows.Forms.Label
        Me.txtTotalCIF = New eZee.TextBox.NumericTextBox
        Me.lblTotalCIF = New System.Windows.Forms.Label
        Me.txtColour = New eZee.TextBox.AlphanumericTextBox
        Me.lblColour = New System.Windows.Forms.Label
        Me.txtChassisNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtSupplier = New eZee.TextBox.AlphanumericTextBox
        Me.lblSupplier = New System.Windows.Forms.Label
        Me.lblChassisNo = New System.Windows.Forms.Label
        Me.txtModel = New eZee.TextBox.AlphanumericTextBox
        Me.txtYOM = New eZee.TextBox.AlphanumericTextBox
        Me.lblYOM = New System.Windows.Forms.Label
        Me.lblModel = New System.Windows.Forms.Label
        Me.lblAmt = New System.Windows.Forms.Label
        Me.objpnlLoanProjection = New System.Windows.Forms.Panel
        Me.pnlInformation = New System.Windows.Forms.Panel
        Me.lblInstallmentPaid = New System.Windows.Forms.Label
        Me.nudInstallmentPaid = New System.Windows.Forms.NumericUpDown
        Me.txtOutStandingInterestAmt = New eZee.TextBox.NumericTextBox
        Me.lblOutStandingInterestAmt = New System.Windows.Forms.Label
        Me.txtOutStandingPrincipalAmt = New eZee.TextBox.NumericTextBox
        Me.lblOutStandingPrincipalAmt = New System.Windows.Forms.Label
        Me.cboCurrency = New System.Windows.Forms.ComboBox
        Me.txtLoanAmt = New eZee.TextBox.NumericTextBox
        Me.lblTakeHome = New System.Windows.Forms.Label
        Me.txtTakeHome = New eZee.TextBox.NumericTextBox
        Me.txtInsuranceAmt = New eZee.TextBox.NumericTextBox
        Me.lblInsuranceAmt = New System.Windows.Forms.Label
        Me.lblPrincipalAmt = New System.Windows.Forms.Label
        Me.lblEMIInstallments = New System.Windows.Forms.Label
        Me.nudDuration = New System.Windows.Forms.NumericUpDown
        Me.lblEMIAmount = New System.Windows.Forms.Label
        Me.txtInstallmentAmt = New eZee.TextBox.NumericTextBox
        Me.elLoanAmountCalculation = New eZee.Common.eZeeLine
        Me.txtAppliedUptoPrincipalAmt = New eZee.TextBox.NumericTextBox
        Me.lblIntAmt = New System.Windows.Forms.Label
        Me.txtIntAmt = New eZee.TextBox.NumericTextBox
        Me.lblPurposeOfCredit = New System.Windows.Forms.Label
        Me.txtPurposeOfCredit = New eZee.TextBox.AlphanumericTextBox
        Me.txtInsuranceRate = New eZee.TextBox.NumericTextBox
        Me.lblInsuranceRate = New System.Windows.Forms.Label
        Me.txtLoanRate = New eZee.TextBox.NumericTextBox
        Me.lblLoanInterest = New System.Windows.Forms.Label
        Me.cboLoanCalcType = New System.Windows.Forms.ComboBox
        Me.cboInterestCalcType = New System.Windows.Forms.ComboBox
        Me.lblInterestCalcType = New System.Windows.Forms.Label
        Me.lblLoanCalcType = New System.Windows.Forms.Label
        Me.lblMaxInstallment = New System.Windows.Forms.Label
        Me.nudMaxInstallment = New System.Windows.Forms.NumericUpDown
        Me.cboDocumentType = New System.Windows.Forms.ComboBox
        Me.btnAddAttachment = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblDocumentType = New System.Windows.Forms.Label
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.dgvLoanApplicationAttachment = New System.Windows.Forms.DataGridView
        Me.objcohDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhDocType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhSize = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhDownload = New System.Windows.Forms.DataGridViewLinkColumn
        Me.objcolhGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhScanUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ofdAttachment = New System.Windows.Forms.OpenFileDialog
        Me.sfdAttachment = New System.Windows.Forms.SaveFileDialog
        Me.pnlScanAttachment = New System.Windows.Forms.Panel
        Me.pnlSign = New System.Windows.Forms.Panel
        Me.lblempsign = New System.Windows.Forms.Label
        Me.imgSignature = New System.Windows.Forms.PictureBox
        Me.chkconfirmSign = New System.Windows.Forms.CheckBox
        Me.lblMaxInstallmentAmt = New System.Windows.Forms.Label
        Me.txtMaxInstallmentAmt = New eZee.TextBox.NumericTextBox
        Me.txtFormulaExpr = New System.Windows.Forms.TextBox
        Me.lblMinLoanAmount = New System.Windows.Forms.Label
        Me.txtMinLoanAmount = New eZee.TextBox.NumericTextBox
        Me.chkMakeExceptionalApplication = New System.Windows.Forms.CheckBox
        Me.pnlOtherScanAttachment = New System.Windows.Forms.Panel
        Me.btnAddOtherAttachment = New eZee.Common.eZeeLightButton(Me.components)
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.dgvLoanApplicationOtherAttachment = New System.Windows.Forms.DataGridView
        Me.objcohOtherDelete = New System.Windows.Forms.DataGridViewImageColumn
        Me.colhOtherDocType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhOtherName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhOtherSize = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhOtherDownload = New System.Windows.Forms.DataGridViewLinkColumn
        Me.objcolhOtherGUID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhOtherScanUnkId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lblOtherDocumentType = New System.Windows.Forms.Label
        Me.cboOtherDocumentType = New System.Windows.Forms.ComboBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.nudPayPeriodEOC = New System.Windows.Forms.NumericUpDown
        Me.lblPayPeriodsEOC = New System.Windows.Forms.Label
        Me.dtpEndEmplDate = New System.Windows.Forms.DateTimePicker
        Me.lblEmplDate = New System.Windows.Forms.Label
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.chkSalaryAdvanceLiquidation = New System.Windows.Forms.CheckBox
        Me.objFooter.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.nudTitleValidity, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.objpnlLoanProjection.SuspendLayout()
        Me.pnlInformation.SuspendLayout()
        CType(Me.nudInstallmentPaid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudDuration, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudMaxInstallment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.dgvLoanApplicationAttachment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlScanAttachment.SuspendLayout()
        Me.pnlSign.SuspendLayout()
        CType(Me.imgSignature, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlOtherScanAttachment.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.dgvLoanApplicationOtherAttachment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.nudPayPeriodEOC, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblLoanScheme
        '
        Me.lblLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanScheme.Location = New System.Drawing.Point(12, 37)
        Me.lblLoanScheme.Name = "lblLoanScheme"
        Me.lblLoanScheme.Size = New System.Drawing.Size(102, 17)
        Me.lblLoanScheme.TabIndex = 17
        Me.lblLoanScheme.Text = "Loan Scheme"
        '
        'cboLoanScheme
        '
        Me.cboLoanScheme.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanScheme.FormattingEnabled = True
        Me.cboLoanScheme.Items.AddRange(New Object() {"Select"})
        Me.cboLoanScheme.Location = New System.Drawing.Point(144, 37)
        Me.cboLoanScheme.Name = "cboLoanScheme"
        Me.cboLoanScheme.Size = New System.Drawing.Size(196, 21)
        Me.cboLoanScheme.TabIndex = 1
        '
        'objbtnAddLoanScheme
        '
        Me.objbtnAddLoanScheme.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddLoanScheme.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddLoanScheme.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddLoanScheme.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddLoanScheme.BorderSelected = False
        Me.objbtnAddLoanScheme.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddLoanScheme.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Add
        Me.objbtnAddLoanScheme.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddLoanScheme.Location = New System.Drawing.Point(344, 37)
        Me.objbtnAddLoanScheme.Name = "objbtnAddLoanScheme"
        Me.objbtnAddLoanScheme.Size = New System.Drawing.Size(20, 21)
        Me.objbtnAddLoanScheme.TabIndex = 18
        '
        'cboEmpName
        '
        Me.cboEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmpName.FormattingEnabled = True
        Me.cboEmpName.Items.AddRange(New Object() {"Select"})
        Me.cboEmpName.Location = New System.Drawing.Point(144, 10)
        Me.cboEmpName.Name = "cboEmpName"
        Me.cboEmpName.Size = New System.Drawing.Size(195, 21)
        Me.cboEmpName.TabIndex = 0
        '
        'lblEmpName
        '
        Me.lblEmpName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmpName.Location = New System.Drawing.Point(12, 10)
        Me.lblEmpName.Name = "lblEmpName"
        Me.lblEmpName.Size = New System.Drawing.Size(102, 17)
        Me.lblEmpName.TabIndex = 20
        Me.lblEmpName.Text = "Employee"
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(342, 8)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 21
        Me.objbtnSearchEmployee.Visible = False
        '
        'objLoanSchemeSearch
        '
        Me.objLoanSchemeSearch.BackColor = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objLoanSchemeSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objLoanSchemeSearch.BorderSelected = False
        Me.objLoanSchemeSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objLoanSchemeSearch.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objLoanSchemeSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objLoanSchemeSearch.Location = New System.Drawing.Point(344, 37)
        Me.objLoanSchemeSearch.Name = "objLoanSchemeSearch"
        Me.objLoanSchemeSearch.Size = New System.Drawing.Size(20, 21)
        Me.objLoanSchemeSearch.TabIndex = 22
        Me.objLoanSchemeSearch.Visible = False
        '
        'lblLoanSchemeCategory
        '
        Me.lblLoanSchemeCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanSchemeCategory.Location = New System.Drawing.Point(12, 64)
        Me.lblLoanSchemeCategory.Name = "lblLoanSchemeCategory"
        Me.lblLoanSchemeCategory.Size = New System.Drawing.Size(102, 17)
        Me.lblLoanSchemeCategory.TabIndex = 23
        Me.lblLoanSchemeCategory.Text = "Loan  Category"
        '
        'cboLoanSchemeCategory
        '
        Me.cboLoanSchemeCategory.Enabled = False
        Me.cboLoanSchemeCategory.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanSchemeCategory.FormattingEnabled = True
        Me.cboLoanSchemeCategory.Items.AddRange(New Object() {"Select"})
        Me.cboLoanSchemeCategory.Location = New System.Drawing.Point(144, 64)
        Me.cboLoanSchemeCategory.Name = "cboLoanSchemeCategory"
        Me.cboLoanSchemeCategory.Size = New System.Drawing.Size(73, 21)
        Me.cboLoanSchemeCategory.TabIndex = 2
        '
        'txtRepaymentDays
        '
        Me.txtRepaymentDays.AllowNegative = True
        Me.txtRepaymentDays.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtRepaymentDays.DigitsInGroup = 0
        Me.txtRepaymentDays.Enabled = False
        Me.txtRepaymentDays.Flags = 0
        Me.txtRepaymentDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRepaymentDays.Location = New System.Drawing.Point(144, 93)
        Me.txtRepaymentDays.MaxDecimalPlaces = 0
        Me.txtRepaymentDays.MaxWholeDigits = 21
        Me.txtRepaymentDays.Name = "txtRepaymentDays"
        Me.txtRepaymentDays.Prefix = ""
        Me.txtRepaymentDays.RangeMax = 1.7976931348623157E+308
        Me.txtRepaymentDays.RangeMin = -1.7976931348623157E+308
        Me.txtRepaymentDays.Size = New System.Drawing.Size(73, 21)
        Me.txtRepaymentDays.TabIndex = 3
        Me.txtRepaymentDays.Text = "0"
        Me.txtRepaymentDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblRepaymentDays
        '
        Me.lblRepaymentDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRepaymentDays.Location = New System.Drawing.Point(12, 93)
        Me.lblRepaymentDays.Name = "lblRepaymentDays"
        Me.lblRepaymentDays.Size = New System.Drawing.Size(128, 16)
        Me.lblRepaymentDays.TabIndex = 378
        Me.lblRepaymentDays.Text = "Repayment every (Days)"
        Me.lblRepaymentDays.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMaxLoanAmount
        '
        Me.lblMaxLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxLoanAmount.Location = New System.Drawing.Point(12, 123)
        Me.lblMaxLoanAmount.Name = "lblMaxLoanAmount"
        Me.lblMaxLoanAmount.Size = New System.Drawing.Size(96, 17)
        Me.lblMaxLoanAmount.TabIndex = 381
        Me.lblMaxLoanAmount.Text = "Max Principal Amt."
        Me.lblMaxLoanAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMaxLoanAmount
        '
        Me.txtMaxLoanAmount.AllowNegative = True
        Me.txtMaxLoanAmount.BackColor = System.Drawing.SystemColors.Window
        Me.txtMaxLoanAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMaxLoanAmount.DigitsInGroup = 3
        Me.txtMaxLoanAmount.Enabled = False
        Me.txtMaxLoanAmount.Flags = 0
        Me.txtMaxLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxLoanAmount.Location = New System.Drawing.Point(144, 120)
        Me.txtMaxLoanAmount.MaxDecimalPlaces = 6
        Me.txtMaxLoanAmount.MaxWholeDigits = 21
        Me.txtMaxLoanAmount.Name = "txtMaxLoanAmount"
        Me.txtMaxLoanAmount.Prefix = ""
        Me.txtMaxLoanAmount.RangeMax = 1.7976931348623157E+308
        Me.txtMaxLoanAmount.RangeMin = -1.7976931348623157E+308
        Me.txtMaxLoanAmount.ReadOnly = True
        Me.txtMaxLoanAmount.Size = New System.Drawing.Size(213, 21)
        Me.txtMaxLoanAmount.TabIndex = 4
        Me.txtMaxLoanAmount.Text = "0"
        Me.txtMaxLoanAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnConfirmSign)
        Me.objFooter.Controls.Add(Me.objlblExRate)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Controls.Add(Me.objvalPeriodDuration)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 555)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(1024, 55)
        Me.objFooter.TabIndex = 396
        '
        'btnConfirmSign
        '
        Me.btnConfirmSign.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnConfirmSign.BackColor = System.Drawing.Color.White
        Me.btnConfirmSign.BackgroundImage = CType(resources.GetObject("btnConfirmSign.BackgroundImage"), System.Drawing.Image)
        Me.btnConfirmSign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnConfirmSign.BorderColor = System.Drawing.Color.Empty
        Me.btnConfirmSign.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnConfirmSign.FlatAppearance.BorderSize = 0
        Me.btnConfirmSign.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConfirmSign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConfirmSign.ForeColor = System.Drawing.Color.Black
        Me.btnConfirmSign.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnConfirmSign.GradientForeColor = System.Drawing.Color.Black
        Me.btnConfirmSign.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnConfirmSign.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnConfirmSign.Location = New System.Drawing.Point(683, 13)
        Me.btnConfirmSign.Name = "btnConfirmSign"
        Me.btnConfirmSign.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnConfirmSign.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnConfirmSign.Size = New System.Drawing.Size(123, 30)
        Me.btnConfirmSign.TabIndex = 0
        Me.btnConfirmSign.Text = "&Confirm Signature"
        Me.btnConfirmSign.UseVisualStyleBackColor = True
        '
        'objlblExRate
        '
        Me.objlblExRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblExRate.Location = New System.Drawing.Point(9, 30)
        Me.objlblExRate.Name = "objlblExRate"
        Me.objlblExRate.Size = New System.Drawing.Size(592, 18)
        Me.objlblExRate.TabIndex = 29
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(812, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(97, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(915, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'objvalPeriodDuration
        '
        Me.objvalPeriodDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objvalPeriodDuration.Location = New System.Drawing.Point(9, 8)
        Me.objvalPeriodDuration.Name = "objvalPeriodDuration"
        Me.objvalPeriodDuration.Size = New System.Drawing.Size(597, 18)
        Me.objvalPeriodDuration.TabIndex = 24
        Me.objvalPeriodDuration.Text = "#periodDuration"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtBOQ)
        Me.Panel1.Controls.Add(Me.LblBOQ)
        Me.Panel1.Controls.Add(Me.lblTitleValidity)
        Me.Panel1.Controls.Add(Me.nudTitleValidity)
        Me.Panel1.Controls.Add(Me.dtpTitleExpiryDate)
        Me.Panel1.Controls.Add(Me.lblTitleExpriryDate)
        Me.Panel1.Controls.Add(Me.dtpTitleIssueDate)
        Me.Panel1.Controls.Add(Me.lblTitleIssueDate)
        Me.Panel1.Controls.Add(Me.lblBlockNo)
        Me.Panel1.Controls.Add(Me.txtFSV)
        Me.Panel1.Controls.Add(Me.txtMarketValue)
        Me.Panel1.Controls.Add(Me.txtLONo)
        Me.Panel1.Controls.Add(Me.lblLONo)
        Me.Panel1.Controls.Add(Me.txtCTNo)
        Me.Panel1.Controls.Add(Me.lblCTNo)
        Me.Panel1.Controls.Add(Me.lblMarketValue)
        Me.Panel1.Controls.Add(Me.txtTownCity)
        Me.Panel1.Controls.Add(Me.lblTownCity)
        Me.Panel1.Controls.Add(Me.txtStreet)
        Me.Panel1.Controls.Add(Me.lblStreet)
        Me.Panel1.Controls.Add(Me.txtBlockNo)
        Me.Panel1.Controls.Add(Me.txtPlotNo)
        Me.Panel1.Controls.Add(Me.lblPlotNo)
        Me.Panel1.Controls.Add(Me.lblFSV)
        Me.Panel1.Location = New System.Drawing.Point(3, 364)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(706, 185)
        Me.Panel1.TabIndex = 397
        '
        'txtBOQ
        '
        Me.txtBOQ.Flags = 0
        Me.txtBOQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBOQ.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBOQ.Location = New System.Drawing.Point(508, 122)
        Me.txtBOQ.Name = "txtBOQ"
        Me.txtBOQ.Size = New System.Drawing.Size(190, 21)
        Me.txtBOQ.TabIndex = 368
        '
        'LblBOQ
        '
        Me.LblBOQ.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblBOQ.Location = New System.Drawing.Point(380, 124)
        Me.LblBOQ.Name = "LblBOQ"
        Me.LblBOQ.Size = New System.Drawing.Size(123, 17)
        Me.LblBOQ.TabIndex = 367
        Me.LblBOQ.Text = "Bill Of Quantity (BOQ)"
        '
        'lblTitleValidity
        '
        Me.lblTitleValidity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitleValidity.Location = New System.Drawing.Point(380, 150)
        Me.lblTitleValidity.Name = "lblTitleValidity"
        Me.lblTitleValidity.Size = New System.Drawing.Size(121, 19)
        Me.lblTitleValidity.TabIndex = 365
        Me.lblTitleValidity.Text = "Title Validity (In Years)"
        '
        'nudTitleValidity
        '
        Me.nudTitleValidity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudTitleValidity.Location = New System.Drawing.Point(509, 149)
        Me.nudTitleValidity.Maximum = New Decimal(New Integer() {-402653185, -1613725636, 54210108, 0})
        Me.nudTitleValidity.Name = "nudTitleValidity"
        Me.nudTitleValidity.Size = New System.Drawing.Size(68, 21)
        Me.nudTitleValidity.TabIndex = 364
        Me.nudTitleValidity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudTitleValidity.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'dtpTitleExpiryDate
        '
        Me.dtpTitleExpiryDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTitleExpiryDate.Checked = False
        Me.dtpTitleExpiryDate.Enabled = False
        Me.dtpTitleExpiryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTitleExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTitleExpiryDate.Location = New System.Drawing.Point(139, 149)
        Me.dtpTitleExpiryDate.Name = "dtpTitleExpiryDate"
        Me.dtpTitleExpiryDate.Size = New System.Drawing.Size(101, 21)
        Me.dtpTitleExpiryDate.TabIndex = 363
        '
        'lblTitleExpriryDate
        '
        Me.lblTitleExpriryDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitleExpriryDate.Location = New System.Drawing.Point(17, 152)
        Me.lblTitleExpriryDate.Name = "lblTitleExpriryDate"
        Me.lblTitleExpriryDate.Size = New System.Drawing.Size(97, 15)
        Me.lblTitleExpriryDate.TabIndex = 362
        Me.lblTitleExpriryDate.Text = "Title Expriry Date"
        '
        'dtpTitleIssueDate
        '
        Me.dtpTitleIssueDate.CalendarFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTitleIssueDate.Checked = False
        Me.dtpTitleIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpTitleIssueDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpTitleIssueDate.Location = New System.Drawing.Point(139, 122)
        Me.dtpTitleIssueDate.Name = "dtpTitleIssueDate"
        Me.dtpTitleIssueDate.ShowCheckBox = True
        Me.dtpTitleIssueDate.Size = New System.Drawing.Size(101, 21)
        Me.dtpTitleIssueDate.TabIndex = 361
        '
        'lblTitleIssueDate
        '
        Me.lblTitleIssueDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTitleIssueDate.Location = New System.Drawing.Point(17, 125)
        Me.lblTitleIssueDate.Name = "lblTitleIssueDate"
        Me.lblTitleIssueDate.Size = New System.Drawing.Size(97, 15)
        Me.lblTitleIssueDate.TabIndex = 360
        Me.lblTitleIssueDate.Text = "Title Issue Date"
        '
        'lblBlockNo
        '
        Me.lblBlockNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBlockNo.Location = New System.Drawing.Point(380, 16)
        Me.lblBlockNo.Name = "lblBlockNo"
        Me.lblBlockNo.Size = New System.Drawing.Size(95, 17)
        Me.lblBlockNo.TabIndex = 347
        Me.lblBlockNo.Text = "Block No"
        '
        'txtFSV
        '
        Me.txtFSV.AllowNegative = True
        Me.txtFSV.BackColor = System.Drawing.SystemColors.Window
        Me.txtFSV.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtFSV.DigitsInGroup = 3
        Me.txtFSV.Flags = 0
        Me.txtFSV.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFSV.Location = New System.Drawing.Point(508, 95)
        Me.txtFSV.MaxDecimalPlaces = 6
        Me.txtFSV.MaxWholeDigits = 21
        Me.txtFSV.Name = "txtFSV"
        Me.txtFSV.Prefix = ""
        Me.txtFSV.RangeMax = 1.7976931348623157E+308
        Me.txtFSV.RangeMin = -1.7976931348623157E+308
        Me.txtFSV.Size = New System.Drawing.Size(190, 21)
        Me.txtFSV.TabIndex = 7
        Me.txtFSV.Text = "0"
        Me.txtFSV.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtMarketValue
        '
        Me.txtMarketValue.AllowNegative = True
        Me.txtMarketValue.BackColor = System.Drawing.SystemColors.Window
        Me.txtMarketValue.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMarketValue.DigitsInGroup = 3
        Me.txtMarketValue.Flags = 0
        Me.txtMarketValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMarketValue.Location = New System.Drawing.Point(139, 69)
        Me.txtMarketValue.MaxDecimalPlaces = 6
        Me.txtMarketValue.MaxWholeDigits = 21
        Me.txtMarketValue.Name = "txtMarketValue"
        Me.txtMarketValue.Prefix = ""
        Me.txtMarketValue.RangeMax = 1.7976931348623157E+308
        Me.txtMarketValue.RangeMin = -1.7976931348623157E+308
        Me.txtMarketValue.Size = New System.Drawing.Size(214, 21)
        Me.txtMarketValue.TabIndex = 2
        Me.txtMarketValue.Text = "0"
        Me.txtMarketValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLONo
        '
        Me.txtLONo.Flags = 0
        Me.txtLONo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLONo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtLONo.Location = New System.Drawing.Point(139, 95)
        Me.txtLONo.Name = "txtLONo"
        Me.txtLONo.Size = New System.Drawing.Size(214, 21)
        Me.txtLONo.TabIndex = 3
        '
        'lblLONo
        '
        Me.lblLONo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLONo.Location = New System.Drawing.Point(17, 99)
        Me.lblLONo.Name = "lblLONo"
        Me.lblLONo.Size = New System.Drawing.Size(100, 17)
        Me.lblLONo.TabIndex = 357
        Me.lblLONo.Text = "LO No"
        '
        'txtCTNo
        '
        Me.txtCTNo.Flags = 0
        Me.txtCTNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCTNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtCTNo.Location = New System.Drawing.Point(508, 68)
        Me.txtCTNo.Name = "txtCTNo"
        Me.txtCTNo.Size = New System.Drawing.Size(190, 21)
        Me.txtCTNo.TabIndex = 6
        '
        'lblCTNo
        '
        Me.lblCTNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCTNo.Location = New System.Drawing.Point(380, 72)
        Me.lblCTNo.Name = "lblCTNo"
        Me.lblCTNo.Size = New System.Drawing.Size(95, 17)
        Me.lblCTNo.TabIndex = 355
        Me.lblCTNo.Text = "CT No"
        '
        'lblMarketValue
        '
        Me.lblMarketValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMarketValue.Location = New System.Drawing.Point(17, 72)
        Me.lblMarketValue.Name = "lblMarketValue"
        Me.lblMarketValue.Size = New System.Drawing.Size(100, 17)
        Me.lblMarketValue.TabIndex = 353
        Me.lblMarketValue.Text = "Market Value"
        '
        'txtTownCity
        '
        Me.txtTownCity.Flags = 0
        Me.txtTownCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTownCity.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtTownCity.Location = New System.Drawing.Point(508, 40)
        Me.txtTownCity.Name = "txtTownCity"
        Me.txtTownCity.Size = New System.Drawing.Size(190, 21)
        Me.txtTownCity.TabIndex = 5
        '
        'lblTownCity
        '
        Me.lblTownCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTownCity.Location = New System.Drawing.Point(380, 44)
        Me.lblTownCity.Name = "lblTownCity"
        Me.lblTownCity.Size = New System.Drawing.Size(95, 17)
        Me.lblTownCity.TabIndex = 351
        Me.lblTownCity.Text = "Town/City"
        '
        'txtStreet
        '
        Me.txtStreet.Flags = 0
        Me.txtStreet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtStreet.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtStreet.Location = New System.Drawing.Point(139, 40)
        Me.txtStreet.Name = "txtStreet"
        Me.txtStreet.Size = New System.Drawing.Size(214, 21)
        Me.txtStreet.TabIndex = 1
        '
        'lblStreet
        '
        Me.lblStreet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStreet.Location = New System.Drawing.Point(17, 44)
        Me.lblStreet.Name = "lblStreet"
        Me.lblStreet.Size = New System.Drawing.Size(100, 17)
        Me.lblStreet.TabIndex = 349
        Me.lblStreet.Text = "Street"
        '
        'txtBlockNo
        '
        Me.txtBlockNo.Flags = 0
        Me.txtBlockNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBlockNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtBlockNo.Location = New System.Drawing.Point(508, 12)
        Me.txtBlockNo.Name = "txtBlockNo"
        Me.txtBlockNo.Size = New System.Drawing.Size(190, 21)
        Me.txtBlockNo.TabIndex = 4
        '
        'txtPlotNo
        '
        Me.txtPlotNo.Flags = 0
        Me.txtPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPlotNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtPlotNo.Location = New System.Drawing.Point(139, 12)
        Me.txtPlotNo.Name = "txtPlotNo"
        Me.txtPlotNo.Size = New System.Drawing.Size(214, 21)
        Me.txtPlotNo.TabIndex = 0
        '
        'lblPlotNo
        '
        Me.lblPlotNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlotNo.Location = New System.Drawing.Point(17, 16)
        Me.lblPlotNo.Name = "lblPlotNo"
        Me.lblPlotNo.Size = New System.Drawing.Size(100, 17)
        Me.lblPlotNo.TabIndex = 345
        Me.lblPlotNo.Text = "Plot No"
        '
        'lblFSV
        '
        Me.lblFSV.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFSV.Location = New System.Drawing.Point(380, 99)
        Me.lblFSV.Name = "lblFSV"
        Me.lblFSV.Size = New System.Drawing.Size(95, 17)
        Me.lblFSV.TabIndex = 359
        Me.lblFSV.Text = "FSV"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtEngineCapacity)
        Me.Panel2.Controls.Add(Me.lblEngineCapacity)
        Me.Panel2.Controls.Add(Me.txtEngineNo)
        Me.Panel2.Controls.Add(Me.lblEngineNo)
        Me.Panel2.Controls.Add(Me.txtModelNo)
        Me.Panel2.Controls.Add(Me.lblModelNo)
        Me.Panel2.Controls.Add(Me.txtInvoiceValue)
        Me.Panel2.Controls.Add(Me.lblInvoiceValue)
        Me.Panel2.Controls.Add(Me.txtEstimatedTax)
        Me.Panel2.Controls.Add(Me.lblEstimatedTax)
        Me.Panel2.Controls.Add(Me.txtTotalCIF)
        Me.Panel2.Controls.Add(Me.lblTotalCIF)
        Me.Panel2.Controls.Add(Me.txtColour)
        Me.Panel2.Controls.Add(Me.lblColour)
        Me.Panel2.Controls.Add(Me.txtChassisNo)
        Me.Panel2.Controls.Add(Me.txtSupplier)
        Me.Panel2.Controls.Add(Me.lblSupplier)
        Me.Panel2.Controls.Add(Me.lblChassisNo)
        Me.Panel2.Controls.Add(Me.txtModel)
        Me.Panel2.Controls.Add(Me.txtYOM)
        Me.Panel2.Controls.Add(Me.lblYOM)
        Me.Panel2.Controls.Add(Me.lblModel)
        Me.Panel2.Location = New System.Drawing.Point(3, 364)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(705, 185)
        Me.Panel2.TabIndex = 387
        '
        'txtEngineCapacity
        '
        Me.txtEngineCapacity.AllowNegative = True
        Me.txtEngineCapacity.BackColor = System.Drawing.SystemColors.Window
        Me.txtEngineCapacity.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtEngineCapacity.DigitsInGroup = 3
        Me.txtEngineCapacity.Flags = 0
        Me.txtEngineCapacity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEngineCapacity.Location = New System.Drawing.Point(141, 157)
        Me.txtEngineCapacity.MaxDecimalPlaces = 6
        Me.txtEngineCapacity.MaxWholeDigits = 21
        Me.txtEngineCapacity.Name = "txtEngineCapacity"
        Me.txtEngineCapacity.Prefix = ""
        Me.txtEngineCapacity.RangeMax = 1.7976931348623157E+308
        Me.txtEngineCapacity.RangeMin = -1.7976931348623157E+308
        Me.txtEngineCapacity.Size = New System.Drawing.Size(168, 21)
        Me.txtEngineCapacity.TabIndex = 405
        Me.txtEngineCapacity.Text = "0"
        Me.txtEngineCapacity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtEngineCapacity.Visible = False
        '
        'lblEngineCapacity
        '
        Me.lblEngineCapacity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEngineCapacity.Location = New System.Drawing.Point(11, 161)
        Me.lblEngineCapacity.Name = "lblEngineCapacity"
        Me.lblEngineCapacity.Size = New System.Drawing.Size(95, 17)
        Me.lblEngineCapacity.TabIndex = 406
        Me.lblEngineCapacity.Text = "Engine Capacity"
        Me.lblEngineCapacity.Visible = False
        '
        'txtEngineNo
        '
        Me.txtEngineNo.Flags = 0
        Me.txtEngineNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEngineNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtEngineNo.Location = New System.Drawing.Point(476, 131)
        Me.txtEngineNo.Name = "txtEngineNo"
        Me.txtEngineNo.Size = New System.Drawing.Size(189, 21)
        Me.txtEngineNo.TabIndex = 403
        Me.txtEngineNo.Visible = False
        '
        'lblEngineNo
        '
        Me.lblEngineNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEngineNo.Location = New System.Drawing.Point(378, 133)
        Me.lblEngineNo.Name = "lblEngineNo"
        Me.lblEngineNo.Size = New System.Drawing.Size(95, 17)
        Me.lblEngineNo.TabIndex = 404
        Me.lblEngineNo.Text = "Engine No"
        Me.lblEngineNo.Visible = False
        '
        'txtModelNo
        '
        Me.txtModelNo.Flags = 0
        Me.txtModelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtModelNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtModelNo.Location = New System.Drawing.Point(141, 131)
        Me.txtModelNo.Name = "txtModelNo"
        Me.txtModelNo.Size = New System.Drawing.Size(168, 21)
        Me.txtModelNo.TabIndex = 401
        Me.txtModelNo.Visible = False
        '
        'lblModelNo
        '
        Me.lblModelNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModelNo.Location = New System.Drawing.Point(11, 133)
        Me.lblModelNo.Name = "lblModelNo"
        Me.lblModelNo.Size = New System.Drawing.Size(95, 17)
        Me.lblModelNo.TabIndex = 402
        Me.lblModelNo.Text = "Model No"
        Me.lblModelNo.Visible = False
        '
        'txtInvoiceValue
        '
        Me.txtInvoiceValue.AllowNegative = True
        Me.txtInvoiceValue.BackColor = System.Drawing.SystemColors.Window
        Me.txtInvoiceValue.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInvoiceValue.DigitsInGroup = 3
        Me.txtInvoiceValue.Flags = 0
        Me.txtInvoiceValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInvoiceValue.Location = New System.Drawing.Point(476, 102)
        Me.txtInvoiceValue.MaxDecimalPlaces = 6
        Me.txtInvoiceValue.MaxWholeDigits = 21
        Me.txtInvoiceValue.Name = "txtInvoiceValue"
        Me.txtInvoiceValue.Prefix = ""
        Me.txtInvoiceValue.RangeMax = 1.7976931348623157E+308
        Me.txtInvoiceValue.RangeMin = -1.7976931348623157E+308
        Me.txtInvoiceValue.Size = New System.Drawing.Size(189, 21)
        Me.txtInvoiceValue.TabIndex = 399
        Me.txtInvoiceValue.Text = "0"
        Me.txtInvoiceValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtInvoiceValue.Visible = False
        '
        'lblInvoiceValue
        '
        Me.lblInvoiceValue.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInvoiceValue.Location = New System.Drawing.Point(378, 106)
        Me.lblInvoiceValue.Name = "lblInvoiceValue"
        Me.lblInvoiceValue.Size = New System.Drawing.Size(95, 17)
        Me.lblInvoiceValue.TabIndex = 400
        Me.lblInvoiceValue.Text = "Invoice Value"
        Me.lblInvoiceValue.Visible = False
        '
        'txtEstimatedTax
        '
        Me.txtEstimatedTax.AllowNegative = True
        Me.txtEstimatedTax.BackColor = System.Drawing.SystemColors.Window
        Me.txtEstimatedTax.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtEstimatedTax.DigitsInGroup = 3
        Me.txtEstimatedTax.Flags = 0
        Me.txtEstimatedTax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEstimatedTax.Location = New System.Drawing.Point(141, 102)
        Me.txtEstimatedTax.MaxDecimalPlaces = 6
        Me.txtEstimatedTax.MaxWholeDigits = 21
        Me.txtEstimatedTax.Name = "txtEstimatedTax"
        Me.txtEstimatedTax.Prefix = ""
        Me.txtEstimatedTax.RangeMax = 1.7976931348623157E+308
        Me.txtEstimatedTax.RangeMin = -1.7976931348623157E+308
        Me.txtEstimatedTax.Size = New System.Drawing.Size(168, 21)
        Me.txtEstimatedTax.TabIndex = 397
        Me.txtEstimatedTax.Text = "0"
        Me.txtEstimatedTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtEstimatedTax.Visible = False
        '
        'lblEstimatedTax
        '
        Me.lblEstimatedTax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstimatedTax.Location = New System.Drawing.Point(11, 106)
        Me.lblEstimatedTax.Name = "lblEstimatedTax"
        Me.lblEstimatedTax.Size = New System.Drawing.Size(95, 17)
        Me.lblEstimatedTax.TabIndex = 398
        Me.lblEstimatedTax.Text = "Estimated Tax"
        Me.lblEstimatedTax.Visible = False
        '
        'txtTotalCIF
        '
        Me.txtTotalCIF.AllowNegative = True
        Me.txtTotalCIF.BackColor = System.Drawing.SystemColors.Window
        Me.txtTotalCIF.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTotalCIF.DigitsInGroup = 3
        Me.txtTotalCIF.Flags = 0
        Me.txtTotalCIF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotalCIF.Location = New System.Drawing.Point(476, 72)
        Me.txtTotalCIF.MaxDecimalPlaces = 6
        Me.txtTotalCIF.MaxWholeDigits = 21
        Me.txtTotalCIF.Name = "txtTotalCIF"
        Me.txtTotalCIF.Prefix = ""
        Me.txtTotalCIF.RangeMax = 1.7976931348623157E+308
        Me.txtTotalCIF.RangeMin = -1.7976931348623157E+308
        Me.txtTotalCIF.Size = New System.Drawing.Size(189, 21)
        Me.txtTotalCIF.TabIndex = 395
        Me.txtTotalCIF.Text = "0"
        Me.txtTotalCIF.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtTotalCIF.Visible = False
        '
        'lblTotalCIF
        '
        Me.lblTotalCIF.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalCIF.Location = New System.Drawing.Point(378, 76)
        Me.lblTotalCIF.Name = "lblTotalCIF"
        Me.lblTotalCIF.Size = New System.Drawing.Size(95, 17)
        Me.lblTotalCIF.TabIndex = 396
        Me.lblTotalCIF.Text = "Total CIF"
        Me.lblTotalCIF.Visible = False
        '
        'txtColour
        '
        Me.txtColour.Flags = 0
        Me.txtColour.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtColour.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtColour.Location = New System.Drawing.Point(141, 75)
        Me.txtColour.Name = "txtColour"
        Me.txtColour.Size = New System.Drawing.Size(168, 21)
        Me.txtColour.TabIndex = 393
        '
        'lblColour
        '
        Me.lblColour.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColour.Location = New System.Drawing.Point(11, 75)
        Me.lblColour.Name = "lblColour"
        Me.lblColour.Size = New System.Drawing.Size(95, 17)
        Me.lblColour.TabIndex = 394
        Me.lblColour.Text = "Colour"
        '
        'txtChassisNo
        '
        Me.txtChassisNo.Flags = 0
        Me.txtChassisNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtChassisNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtChassisNo.Location = New System.Drawing.Point(141, 45)
        Me.txtChassisNo.Name = "txtChassisNo"
        Me.txtChassisNo.Size = New System.Drawing.Size(168, 21)
        Me.txtChassisNo.TabIndex = 389
        '
        'txtSupplier
        '
        Me.txtSupplier.Flags = 0
        Me.txtSupplier.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSupplier.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtSupplier.Location = New System.Drawing.Point(476, 45)
        Me.txtSupplier.Name = "txtSupplier"
        Me.txtSupplier.Size = New System.Drawing.Size(189, 21)
        Me.txtSupplier.TabIndex = 390
        '
        'lblSupplier
        '
        Me.lblSupplier.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSupplier.Location = New System.Drawing.Point(378, 45)
        Me.lblSupplier.Name = "lblSupplier"
        Me.lblSupplier.Size = New System.Drawing.Size(95, 17)
        Me.lblSupplier.TabIndex = 392
        Me.lblSupplier.Text = "Supplier"
        '
        'lblChassisNo
        '
        Me.lblChassisNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblChassisNo.Location = New System.Drawing.Point(11, 45)
        Me.lblChassisNo.Name = "lblChassisNo"
        Me.lblChassisNo.Size = New System.Drawing.Size(95, 17)
        Me.lblChassisNo.TabIndex = 391
        Me.lblChassisNo.Text = "Chassis No"
        '
        'txtModel
        '
        Me.txtModel.Flags = 0
        Me.txtModel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtModel.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtModel.Location = New System.Drawing.Point(141, 17)
        Me.txtModel.Name = "txtModel"
        Me.txtModel.Size = New System.Drawing.Size(168, 21)
        Me.txtModel.TabIndex = 0
        '
        'txtYOM
        '
        Me.txtYOM.Flags = 0
        Me.txtYOM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtYOM.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtYOM.Location = New System.Drawing.Point(476, 17)
        Me.txtYOM.MaxLength = 4
        Me.txtYOM.Name = "txtYOM"
        Me.txtYOM.Size = New System.Drawing.Size(189, 21)
        Me.txtYOM.TabIndex = 1
        '
        'lblYOM
        '
        Me.lblYOM.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblYOM.Location = New System.Drawing.Point(378, 17)
        Me.lblYOM.Name = "lblYOM"
        Me.lblYOM.Size = New System.Drawing.Size(95, 17)
        Me.lblYOM.TabIndex = 388
        Me.lblYOM.Text = "YOM"
        '
        'lblModel
        '
        Me.lblModel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblModel.Location = New System.Drawing.Point(11, 17)
        Me.lblModel.Name = "lblModel"
        Me.lblModel.Size = New System.Drawing.Size(95, 17)
        Me.lblModel.TabIndex = 387
        Me.lblModel.Text = "Model"
        '
        'lblAmt
        '
        Me.lblAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAmt.Location = New System.Drawing.Point(520, 301)
        Me.lblAmt.Name = "lblAmt"
        Me.lblAmt.Size = New System.Drawing.Size(57, 17)
        Me.lblAmt.TabIndex = 400
        Me.lblAmt.Text = "Applied Principal Amt"
        Me.lblAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblAmt.Visible = False
        '
        'objpnlLoanProjection
        '
        Me.objpnlLoanProjection.Controls.Add(Me.pnlInformation)
        Me.objpnlLoanProjection.Controls.Add(Me.elLoanAmountCalculation)
        Me.objpnlLoanProjection.Location = New System.Drawing.Point(367, 30)
        Me.objpnlLoanProjection.Name = "objpnlLoanProjection"
        Me.objpnlLoanProjection.Size = New System.Drawing.Size(341, 283)
        Me.objpnlLoanProjection.TabIndex = 401
        '
        'pnlInformation
        '
        Me.pnlInformation.Controls.Add(Me.chkSalaryAdvanceLiquidation)
        Me.pnlInformation.Controls.Add(Me.lblInstallmentPaid)
        Me.pnlInformation.Controls.Add(Me.nudInstallmentPaid)
        Me.pnlInformation.Controls.Add(Me.txtOutStandingInterestAmt)
        Me.pnlInformation.Controls.Add(Me.lblOutStandingInterestAmt)
        Me.pnlInformation.Controls.Add(Me.txtOutStandingPrincipalAmt)
        Me.pnlInformation.Controls.Add(Me.lblOutStandingPrincipalAmt)
        Me.pnlInformation.Controls.Add(Me.cboCurrency)
        Me.pnlInformation.Controls.Add(Me.txtLoanAmt)
        Me.pnlInformation.Controls.Add(Me.lblTakeHome)
        Me.pnlInformation.Controls.Add(Me.txtTakeHome)
        Me.pnlInformation.Controls.Add(Me.txtInsuranceAmt)
        Me.pnlInformation.Controls.Add(Me.lblInsuranceAmt)
        Me.pnlInformation.Controls.Add(Me.lblPrincipalAmt)
        Me.pnlInformation.Controls.Add(Me.lblEMIInstallments)
        Me.pnlInformation.Controls.Add(Me.nudDuration)
        Me.pnlInformation.Controls.Add(Me.lblEMIAmount)
        Me.pnlInformation.Controls.Add(Me.txtInstallmentAmt)
        Me.pnlInformation.Location = New System.Drawing.Point(6, 32)
        Me.pnlInformation.Name = "pnlInformation"
        Me.pnlInformation.Size = New System.Drawing.Size(328, 246)
        Me.pnlInformation.TabIndex = 2
        '
        'lblInstallmentPaid
        '
        Me.lblInstallmentPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInstallmentPaid.Location = New System.Drawing.Point(5, 140)
        Me.lblInstallmentPaid.Name = "lblInstallmentPaid"
        Me.lblInstallmentPaid.Size = New System.Drawing.Size(147, 15)
        Me.lblInstallmentPaid.TabIndex = 407
        Me.lblInstallmentPaid.Text = "No of Installment Paid"
        '
        'nudInstallmentPaid
        '
        Me.nudInstallmentPaid.Enabled = False
        Me.nudInstallmentPaid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudInstallmentPaid.Location = New System.Drawing.Point(153, 136)
        Me.nudInstallmentPaid.Maximum = New Decimal(New Integer() {-402653185, -1613725636, 54210108, 0})
        Me.nudInstallmentPaid.Name = "nudInstallmentPaid"
        Me.nudInstallmentPaid.ReadOnly = True
        Me.nudInstallmentPaid.Size = New System.Drawing.Size(73, 21)
        Me.nudInstallmentPaid.TabIndex = 406
        Me.nudInstallmentPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudInstallmentPaid.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtOutStandingInterestAmt
        '
        Me.txtOutStandingInterestAmt.AllowNegative = True
        Me.txtOutStandingInterestAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtOutStandingInterestAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOutStandingInterestAmt.DigitsInGroup = 3
        Me.txtOutStandingInterestAmt.Flags = 0
        Me.txtOutStandingInterestAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOutStandingInterestAmt.Location = New System.Drawing.Point(153, 109)
        Me.txtOutStandingInterestAmt.MaxDecimalPlaces = 6
        Me.txtOutStandingInterestAmt.MaxWholeDigits = 21
        Me.txtOutStandingInterestAmt.Name = "txtOutStandingInterestAmt"
        Me.txtOutStandingInterestAmt.Prefix = ""
        Me.txtOutStandingInterestAmt.RangeMax = 1.7976931348623157E+308
        Me.txtOutStandingInterestAmt.RangeMin = -1.7976931348623157E+308
        Me.txtOutStandingInterestAmt.ReadOnly = True
        Me.txtOutStandingInterestAmt.Size = New System.Drawing.Size(162, 21)
        Me.txtOutStandingInterestAmt.TabIndex = 404
        Me.txtOutStandingInterestAmt.Text = "0"
        Me.txtOutStandingInterestAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOutStandingInterestAmt
        '
        Me.lblOutStandingInterestAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOutStandingInterestAmt.Location = New System.Drawing.Point(5, 111)
        Me.lblOutStandingInterestAmt.Name = "lblOutStandingInterestAmt"
        Me.lblOutStandingInterestAmt.Size = New System.Drawing.Size(135, 16)
        Me.lblOutStandingInterestAmt.TabIndex = 405
        Me.lblOutStandingInterestAmt.Text = "OutStanding Interest Amt."
        Me.lblOutStandingInterestAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtOutStandingPrincipalAmt
        '
        Me.txtOutStandingPrincipalAmt.AllowNegative = True
        Me.txtOutStandingPrincipalAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtOutStandingPrincipalAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtOutStandingPrincipalAmt.DigitsInGroup = 3
        Me.txtOutStandingPrincipalAmt.Flags = 0
        Me.txtOutStandingPrincipalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtOutStandingPrincipalAmt.Location = New System.Drawing.Point(153, 82)
        Me.txtOutStandingPrincipalAmt.MaxDecimalPlaces = 6
        Me.txtOutStandingPrincipalAmt.MaxWholeDigits = 21
        Me.txtOutStandingPrincipalAmt.Name = "txtOutStandingPrincipalAmt"
        Me.txtOutStandingPrincipalAmt.Prefix = ""
        Me.txtOutStandingPrincipalAmt.RangeMax = 1.7976931348623157E+308
        Me.txtOutStandingPrincipalAmt.RangeMin = -1.7976931348623157E+308
        Me.txtOutStandingPrincipalAmt.ReadOnly = True
        Me.txtOutStandingPrincipalAmt.Size = New System.Drawing.Size(162, 21)
        Me.txtOutStandingPrincipalAmt.TabIndex = 402
        Me.txtOutStandingPrincipalAmt.Text = "0"
        Me.txtOutStandingPrincipalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblOutStandingPrincipalAmt
        '
        Me.lblOutStandingPrincipalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOutStandingPrincipalAmt.Location = New System.Drawing.Point(5, 85)
        Me.lblOutStandingPrincipalAmt.Name = "lblOutStandingPrincipalAmt"
        Me.lblOutStandingPrincipalAmt.Size = New System.Drawing.Size(134, 17)
        Me.lblOutStandingPrincipalAmt.TabIndex = 403
        Me.lblOutStandingPrincipalAmt.Text = "Outstanding Principal Amt."
        Me.lblOutStandingPrincipalAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboCurrency
        '
        Me.cboCurrency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCurrency.Enabled = False
        Me.cboCurrency.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCurrency.FormattingEnabled = True
        Me.cboCurrency.Location = New System.Drawing.Point(262, 55)
        Me.cboCurrency.Name = "cboCurrency"
        Me.cboCurrency.Size = New System.Drawing.Size(51, 21)
        Me.cboCurrency.TabIndex = 401
        '
        'txtLoanAmt
        '
        Me.txtLoanAmt.AllowNegative = True
        Me.txtLoanAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanAmt.DigitsInGroup = 3
        Me.txtLoanAmt.Flags = 0
        Me.txtLoanAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanAmt.Location = New System.Drawing.Point(153, 28)
        Me.txtLoanAmt.MaxDecimalPlaces = 6
        Me.txtLoanAmt.MaxWholeDigits = 21
        Me.txtLoanAmt.Name = "txtLoanAmt"
        Me.txtLoanAmt.Prefix = ""
        Me.txtLoanAmt.RangeMax = 1.7976931348623157E+308
        Me.txtLoanAmt.RangeMin = -1.7976931348623157E+308
        Me.txtLoanAmt.SelectionMode = False
        Me.txtLoanAmt.Size = New System.Drawing.Size(162, 21)
        Me.txtLoanAmt.TabIndex = 1
        Me.txtLoanAmt.Text = "0"
        Me.txtLoanAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTakeHome
        '
        Me.lblTakeHome.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTakeHome.Location = New System.Drawing.Point(5, 192)
        Me.lblTakeHome.Name = "lblTakeHome"
        Me.lblTakeHome.Size = New System.Drawing.Size(92, 17)
        Me.lblTakeHome.TabIndex = 34
        Me.lblTakeHome.Text = "Take Home"
        Me.lblTakeHome.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTakeHome
        '
        Me.txtTakeHome.AllowNegative = True
        Me.txtTakeHome.BackColor = System.Drawing.SystemColors.Window
        Me.txtTakeHome.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtTakeHome.DigitsInGroup = 3
        Me.txtTakeHome.Enabled = False
        Me.txtTakeHome.Flags = 0
        Me.txtTakeHome.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTakeHome.Location = New System.Drawing.Point(153, 188)
        Me.txtTakeHome.MaxDecimalPlaces = 6
        Me.txtTakeHome.MaxWholeDigits = 21
        Me.txtTakeHome.Name = "txtTakeHome"
        Me.txtTakeHome.Prefix = ""
        Me.txtTakeHome.RangeMax = 1.7976931348623157E+308
        Me.txtTakeHome.RangeMin = -1.7976931348623157E+308
        Me.txtTakeHome.ReadOnly = True
        Me.txtTakeHome.Size = New System.Drawing.Size(162, 21)
        Me.txtTakeHome.TabIndex = 4
        Me.txtTakeHome.Text = "0"
        Me.txtTakeHome.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtInsuranceAmt
        '
        Me.txtInsuranceAmt.AllowNegative = True
        Me.txtInsuranceAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtInsuranceAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInsuranceAmt.DigitsInGroup = 3
        Me.txtInsuranceAmt.Flags = 0
        Me.txtInsuranceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInsuranceAmt.Location = New System.Drawing.Point(153, 162)
        Me.txtInsuranceAmt.MaxDecimalPlaces = 6
        Me.txtInsuranceAmt.MaxWholeDigits = 21
        Me.txtInsuranceAmt.Name = "txtInsuranceAmt"
        Me.txtInsuranceAmt.Prefix = ""
        Me.txtInsuranceAmt.RangeMax = 1.7976931348623157E+308
        Me.txtInsuranceAmt.RangeMin = -1.7976931348623157E+308
        Me.txtInsuranceAmt.ReadOnly = True
        Me.txtInsuranceAmt.Size = New System.Drawing.Size(162, 21)
        Me.txtInsuranceAmt.TabIndex = 3
        Me.txtInsuranceAmt.Text = "0"
        Me.txtInsuranceAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblInsuranceAmt
        '
        Me.lblInsuranceAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInsuranceAmt.Location = New System.Drawing.Point(5, 168)
        Me.lblInsuranceAmt.Name = "lblInsuranceAmt"
        Me.lblInsuranceAmt.Size = New System.Drawing.Size(94, 17)
        Me.lblInsuranceAmt.TabIndex = 32
        Me.lblInsuranceAmt.Text = "Insurance Amt."
        Me.lblInsuranceAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblPrincipalAmt
        '
        Me.lblPrincipalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrincipalAmt.Location = New System.Drawing.Point(5, 32)
        Me.lblPrincipalAmt.Name = "lblPrincipalAmt"
        Me.lblPrincipalAmt.Size = New System.Drawing.Size(92, 17)
        Me.lblPrincipalAmt.TabIndex = 27
        Me.lblPrincipalAmt.Text = "Principal Amt."
        Me.lblPrincipalAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblEMIInstallments
        '
        Me.lblEMIInstallments.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIInstallments.Location = New System.Drawing.Point(5, 7)
        Me.lblEMIInstallments.Name = "lblEMIInstallments"
        Me.lblEMIInstallments.Size = New System.Drawing.Size(132, 19)
        Me.lblEMIInstallments.TabIndex = 19
        Me.lblEMIInstallments.Text = "Loan Tenure (In Months)"
        '
        'nudDuration
        '
        Me.nudDuration.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudDuration.Location = New System.Drawing.Point(153, 3)
        Me.nudDuration.Maximum = New Decimal(New Integer() {-402653185, -1613725636, 54210108, 0})
        Me.nudDuration.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudDuration.Name = "nudDuration"
        Me.nudDuration.Size = New System.Drawing.Size(68, 21)
        Me.nudDuration.TabIndex = 0
        Me.nudDuration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudDuration.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblEMIAmount
        '
        Me.lblEMIAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEMIAmount.Location = New System.Drawing.Point(5, 59)
        Me.lblEMIAmount.Name = "lblEMIAmount"
        Me.lblEMIAmount.Size = New System.Drawing.Size(92, 17)
        Me.lblEMIAmount.TabIndex = 23
        Me.lblEMIAmount.Text = "Installment Amt"
        Me.lblEMIAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtInstallmentAmt
        '
        Me.txtInstallmentAmt.AllowNegative = True
        Me.txtInstallmentAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtInstallmentAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInstallmentAmt.DigitsInGroup = 3
        Me.txtInstallmentAmt.Flags = 0
        Me.txtInstallmentAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInstallmentAmt.Location = New System.Drawing.Point(153, 55)
        Me.txtInstallmentAmt.MaxDecimalPlaces = 6
        Me.txtInstallmentAmt.MaxWholeDigits = 21
        Me.txtInstallmentAmt.Name = "txtInstallmentAmt"
        Me.txtInstallmentAmt.Prefix = ""
        Me.txtInstallmentAmt.RangeMax = 1.7976931348623157E+308
        Me.txtInstallmentAmt.RangeMin = -1.7976931348623157E+308
        Me.txtInstallmentAmt.Size = New System.Drawing.Size(105, 21)
        Me.txtInstallmentAmt.TabIndex = 2
        Me.txtInstallmentAmt.Text = "0"
        Me.txtInstallmentAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'elLoanAmountCalculation
        '
        Me.elLoanAmountCalculation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.elLoanAmountCalculation.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.elLoanAmountCalculation.Location = New System.Drawing.Point(8, 7)
        Me.elLoanAmountCalculation.Name = "elLoanAmountCalculation"
        Me.elLoanAmountCalculation.Size = New System.Drawing.Size(299, 17)
        Me.elLoanAmountCalculation.TabIndex = 34
        Me.elLoanAmountCalculation.Text = "Loan Amount Calculation"
        '
        'txtAppliedUptoPrincipalAmt
        '
        Me.txtAppliedUptoPrincipalAmt.AllowNegative = True
        Me.txtAppliedUptoPrincipalAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtAppliedUptoPrincipalAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtAppliedUptoPrincipalAmt.DigitsInGroup = 3
        Me.txtAppliedUptoPrincipalAmt.Enabled = False
        Me.txtAppliedUptoPrincipalAmt.Flags = 0
        Me.txtAppliedUptoPrincipalAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAppliedUptoPrincipalAmt.Location = New System.Drawing.Point(583, 298)
        Me.txtAppliedUptoPrincipalAmt.MaxDecimalPlaces = 6
        Me.txtAppliedUptoPrincipalAmt.MaxWholeDigits = 21
        Me.txtAppliedUptoPrincipalAmt.Name = "txtAppliedUptoPrincipalAmt"
        Me.txtAppliedUptoPrincipalAmt.Prefix = ""
        Me.txtAppliedUptoPrincipalAmt.RangeMax = 1.7976931348623157E+308
        Me.txtAppliedUptoPrincipalAmt.RangeMin = -1.7976931348623157E+308
        Me.txtAppliedUptoPrincipalAmt.ReadOnly = True
        Me.txtAppliedUptoPrincipalAmt.Size = New System.Drawing.Size(71, 21)
        Me.txtAppliedUptoPrincipalAmt.TabIndex = 0
        Me.txtAppliedUptoPrincipalAmt.Text = "0"
        Me.txtAppliedUptoPrincipalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtAppliedUptoPrincipalAmt.Visible = False
        '
        'lblIntAmt
        '
        Me.lblIntAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIntAmt.Location = New System.Drawing.Point(370, 298)
        Me.lblIntAmt.Name = "lblIntAmt"
        Me.lblIntAmt.Size = New System.Drawing.Size(72, 15)
        Me.lblIntAmt.TabIndex = 28
        Me.lblIntAmt.Text = "Interest Amt."
        Me.lblIntAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblIntAmt.Visible = False
        '
        'txtIntAmt
        '
        Me.txtIntAmt.AllowNegative = True
        Me.txtIntAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtIntAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtIntAmt.DigitsInGroup = 3
        Me.txtIntAmt.Flags = 0
        Me.txtIntAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIntAmt.Location = New System.Drawing.Point(464, 298)
        Me.txtIntAmt.MaxDecimalPlaces = 6
        Me.txtIntAmt.MaxWholeDigits = 21
        Me.txtIntAmt.Name = "txtIntAmt"
        Me.txtIntAmt.Prefix = ""
        Me.txtIntAmt.RangeMax = 1.7976931348623157E+308
        Me.txtIntAmt.RangeMin = -1.7976931348623157E+308
        Me.txtIntAmt.ReadOnly = True
        Me.txtIntAmt.Size = New System.Drawing.Size(50, 21)
        Me.txtIntAmt.TabIndex = 5
        Me.txtIntAmt.Text = "0"
        Me.txtIntAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIntAmt.Visible = False
        '
        'lblPurposeOfCredit
        '
        Me.lblPurposeOfCredit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPurposeOfCredit.Location = New System.Drawing.Point(9, 323)
        Me.lblPurposeOfCredit.Name = "lblPurposeOfCredit"
        Me.lblPurposeOfCredit.Size = New System.Drawing.Size(99, 17)
        Me.lblPurposeOfCredit.TabIndex = 402
        Me.lblPurposeOfCredit.Text = "Purpose of Credit"
        '
        'txtPurposeOfCredit
        '
        Me.txtPurposeOfCredit.Flags = 0
        Me.txtPurposeOfCredit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPurposeOfCredit.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92), Global.Microsoft.VisualBasic.ChrW(124)}
        Me.txtPurposeOfCredit.Location = New System.Drawing.Point(142, 320)
        Me.txtPurposeOfCredit.Multiline = True
        Me.txtPurposeOfCredit.Name = "txtPurposeOfCredit"
        Me.txtPurposeOfCredit.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtPurposeOfCredit.Size = New System.Drawing.Size(544, 35)
        Me.txtPurposeOfCredit.TabIndex = 11
        '
        'txtInsuranceRate
        '
        Me.txtInsuranceRate.AllowNegative = True
        Me.txtInsuranceRate.BackColor = System.Drawing.SystemColors.Window
        Me.txtInsuranceRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtInsuranceRate.DigitsInGroup = 0
        Me.txtInsuranceRate.Flags = 0
        Me.txtInsuranceRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtInsuranceRate.Location = New System.Drawing.Point(307, 206)
        Me.txtInsuranceRate.MaxDecimalPlaces = 6
        Me.txtInsuranceRate.MaxWholeDigits = 21
        Me.txtInsuranceRate.Name = "txtInsuranceRate"
        Me.txtInsuranceRate.Prefix = ""
        Me.txtInsuranceRate.RangeMax = 1.7976931348623157E+308
        Me.txtInsuranceRate.RangeMin = -1.7976931348623157E+308
        Me.txtInsuranceRate.ReadOnly = True
        Me.txtInsuranceRate.Size = New System.Drawing.Size(48, 21)
        Me.txtInsuranceRate.TabIndex = 10
        Me.txtInsuranceRate.Text = "0"
        Me.txtInsuranceRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblInsuranceRate
        '
        Me.lblInsuranceRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInsuranceRate.Location = New System.Drawing.Point(203, 210)
        Me.lblInsuranceRate.Name = "lblInsuranceRate"
        Me.lblInsuranceRate.Size = New System.Drawing.Size(103, 17)
        Me.lblInsuranceRate.TabIndex = 30
        Me.lblInsuranceRate.Text = "Insurance Rate (%)"
        Me.lblInsuranceRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtLoanRate
        '
        Me.txtLoanRate.AllowNegative = True
        Me.txtLoanRate.BackColor = System.Drawing.SystemColors.Window
        Me.txtLoanRate.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtLoanRate.DigitsInGroup = 0
        Me.txtLoanRate.Flags = 0
        Me.txtLoanRate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLoanRate.Location = New System.Drawing.Point(142, 208)
        Me.txtLoanRate.MaxDecimalPlaces = 6
        Me.txtLoanRate.MaxWholeDigits = 21
        Me.txtLoanRate.Name = "txtLoanRate"
        Me.txtLoanRate.Prefix = ""
        Me.txtLoanRate.RangeMax = 1.7976931348623157E+308
        Me.txtLoanRate.RangeMin = -1.7976931348623157E+308
        Me.txtLoanRate.ReadOnly = True
        Me.txtLoanRate.Size = New System.Drawing.Size(58, 21)
        Me.txtLoanRate.TabIndex = 9
        Me.txtLoanRate.Text = "0"
        Me.txtLoanRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblLoanInterest
        '
        Me.lblLoanInterest.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanInterest.Location = New System.Drawing.Point(12, 210)
        Me.lblLoanInterest.Name = "lblLoanInterest"
        Me.lblLoanInterest.Size = New System.Drawing.Size(94, 17)
        Me.lblLoanInterest.TabIndex = 17
        Me.lblLoanInterest.Text = "Loan Rate (%)"
        Me.lblLoanInterest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboLoanCalcType
        '
        Me.cboLoanCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboLoanCalcType.DropDownWidth = 250
        Me.cboLoanCalcType.Enabled = False
        Me.cboLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanCalcType.FormattingEnabled = True
        Me.cboLoanCalcType.Location = New System.Drawing.Point(142, 266)
        Me.cboLoanCalcType.Name = "cboLoanCalcType"
        Me.cboLoanCalcType.Size = New System.Drawing.Size(190, 21)
        Me.cboLoanCalcType.TabIndex = 7
        '
        'cboInterestCalcType
        '
        Me.cboInterestCalcType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple
        Me.cboInterestCalcType.Enabled = False
        Me.cboInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboInterestCalcType.FormattingEnabled = True
        Me.cboInterestCalcType.Location = New System.Drawing.Point(142, 293)
        Me.cboInterestCalcType.Name = "cboInterestCalcType"
        Me.cboInterestCalcType.Size = New System.Drawing.Size(190, 21)
        Me.cboInterestCalcType.TabIndex = 8
        '
        'lblInterestCalcType
        '
        Me.lblInterestCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInterestCalcType.Location = New System.Drawing.Point(12, 294)
        Me.lblInterestCalcType.Name = "lblInterestCalcType"
        Me.lblInterestCalcType.Size = New System.Drawing.Size(96, 17)
        Me.lblInterestCalcType.TabIndex = 334
        Me.lblInterestCalcType.Text = "Int. Calc. Type"
        Me.lblInterestCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoanCalcType
        '
        Me.lblLoanCalcType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoanCalcType.Location = New System.Drawing.Point(12, 267)
        Me.lblLoanCalcType.Name = "lblLoanCalcType"
        Me.lblLoanCalcType.Size = New System.Drawing.Size(96, 17)
        Me.lblLoanCalcType.TabIndex = 332
        Me.lblLoanCalcType.Text = "Loan Calc. Type"
        Me.lblLoanCalcType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblMaxInstallment
        '
        Me.lblMaxInstallment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxInstallment.Location = New System.Drawing.Point(12, 179)
        Me.lblMaxInstallment.Name = "lblMaxInstallment"
        Me.lblMaxInstallment.Size = New System.Drawing.Size(114, 15)
        Me.lblMaxInstallment.TabIndex = 403
        Me.lblMaxInstallment.Text = "Max No of Installment"
        '
        'nudMaxInstallment
        '
        Me.nudMaxInstallment.Enabled = False
        Me.nudMaxInstallment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudMaxInstallment.Location = New System.Drawing.Point(144, 177)
        Me.nudMaxInstallment.Maximum = New Decimal(New Integer() {-402653185, -1613725636, 54210108, 0})
        Me.nudMaxInstallment.Name = "nudMaxInstallment"
        Me.nudMaxInstallment.ReadOnly = True
        Me.nudMaxInstallment.Size = New System.Drawing.Size(73, 21)
        Me.nudMaxInstallment.TabIndex = 6
        Me.nudMaxInstallment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudMaxInstallment.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'cboDocumentType
        '
        Me.cboDocumentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDocumentType.FormattingEnabled = True
        Me.cboDocumentType.Location = New System.Drawing.Point(5, 25)
        Me.cboDocumentType.Name = "cboDocumentType"
        Me.cboDocumentType.Size = New System.Drawing.Size(201, 21)
        Me.cboDocumentType.TabIndex = 0
        '
        'btnAddAttachment
        '
        Me.btnAddAttachment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddAttachment.BackColor = System.Drawing.Color.White
        Me.btnAddAttachment.BackgroundImage = CType(resources.GetObject("btnAddAttachment.BackgroundImage"), System.Drawing.Image)
        Me.btnAddAttachment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddAttachment.BorderColor = System.Drawing.Color.Empty
        Me.btnAddAttachment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddAttachment.FlatAppearance.BorderSize = 0
        Me.btnAddAttachment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddAttachment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddAttachment.ForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddAttachment.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddAttachment.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.Location = New System.Drawing.Point(222, 20)
        Me.btnAddAttachment.Name = "btnAddAttachment"
        Me.btnAddAttachment.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddAttachment.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddAttachment.Size = New System.Drawing.Size(78, 29)
        Me.btnAddAttachment.TabIndex = 404
        Me.btnAddAttachment.Text = "&Browse"
        Me.btnAddAttachment.UseVisualStyleBackColor = True
        '
        'lblDocumentType
        '
        Me.lblDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDocumentType.Location = New System.Drawing.Point(4, 6)
        Me.lblDocumentType.Name = "lblDocumentType"
        Me.lblDocumentType.Size = New System.Drawing.Size(296, 16)
        Me.lblDocumentType.TabIndex = 406
        Me.lblDocumentType.Text = "Document Type(.PDF and Images only)"
        Me.lblDocumentType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.dgvLoanApplicationAttachment)
        Me.Panel3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel3.Location = New System.Drawing.Point(5, 52)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(295, 97)
        Me.Panel3.TabIndex = 407
        '
        'dgvLoanApplicationAttachment
        '
        Me.dgvLoanApplicationAttachment.AllowUserToAddRows = False
        Me.dgvLoanApplicationAttachment.AllowUserToDeleteRows = False
        Me.dgvLoanApplicationAttachment.AllowUserToResizeRows = False
        Me.dgvLoanApplicationAttachment.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoanApplicationAttachment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvLoanApplicationAttachment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLoanApplicationAttachment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcohDelete, Me.colhDocType, Me.colhName, Me.colhSize, Me.objcolhDownload, Me.objcolhGUID, Me.objcolhScanUnkId})
        Me.dgvLoanApplicationAttachment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLoanApplicationAttachment.Location = New System.Drawing.Point(0, 0)
        Me.dgvLoanApplicationAttachment.Name = "dgvLoanApplicationAttachment"
        Me.dgvLoanApplicationAttachment.RowHeadersVisible = False
        Me.dgvLoanApplicationAttachment.Size = New System.Drawing.Size(295, 97)
        Me.dgvLoanApplicationAttachment.TabIndex = 0
        '
        'objcohDelete
        '
        Me.objcohDelete.Frozen = True
        Me.objcohDelete.HeaderText = ""
        Me.objcohDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objcohDelete.Name = "objcohDelete"
        Me.objcohDelete.ReadOnly = True
        Me.objcohDelete.Width = 25
        '
        'colhDocType
        '
        Me.colhDocType.Frozen = True
        Me.colhDocType.HeaderText = "Document Type"
        Me.colhDocType.Name = "colhDocType"
        Me.colhDocType.ReadOnly = True
        Me.colhDocType.Width = 150
        '
        'colhName
        '
        Me.colhName.HeaderText = "File Name"
        Me.colhName.Name = "colhName"
        Me.colhName.ReadOnly = True
        Me.colhName.Width = 150
        '
        'colhSize
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhSize.DefaultCellStyle = DataGridViewCellStyle1
        Me.colhSize.HeaderText = "File Size"
        Me.colhSize.Name = "colhSize"
        Me.colhSize.ReadOnly = True
        Me.colhSize.Width = 120
        '
        'objcolhDownload
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        Me.objcolhDownload.DefaultCellStyle = DataGridViewCellStyle2
        Me.objcolhDownload.HeaderText = "Download"
        Me.objcolhDownload.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objcolhDownload.Name = "objcolhDownload"
        Me.objcolhDownload.ReadOnly = True
        Me.objcolhDownload.Text = "Download"
        Me.objcolhDownload.UseColumnTextForLinkValue = True
        '
        'objcolhGUID
        '
        Me.objcolhGUID.HeaderText = "objcolhGUID"
        Me.objcolhGUID.Name = "objcolhGUID"
        Me.objcolhGUID.Visible = False
        '
        'objcolhScanUnkId
        '
        Me.objcolhScanUnkId.HeaderText = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Name = "objcolhScanUnkId"
        Me.objcolhScanUnkId.Visible = False
        '
        'ofdAttachment
        '
        Me.ofdAttachment.FileName = "OpenFileDialog1"
        '
        'pnlScanAttachment
        '
        Me.pnlScanAttachment.Controls.Add(Me.btnAddAttachment)
        Me.pnlScanAttachment.Controls.Add(Me.Panel3)
        Me.pnlScanAttachment.Controls.Add(Me.lblDocumentType)
        Me.pnlScanAttachment.Controls.Add(Me.cboDocumentType)
        Me.pnlScanAttachment.Location = New System.Drawing.Point(715, 42)
        Me.pnlScanAttachment.Name = "pnlScanAttachment"
        Me.pnlScanAttachment.Size = New System.Drawing.Size(303, 156)
        Me.pnlScanAttachment.TabIndex = 408
        '
        'pnlSign
        '
        Me.pnlSign.Controls.Add(Me.lblempsign)
        Me.pnlSign.Controls.Add(Me.imgSignature)
        Me.pnlSign.Controls.Add(Me.chkconfirmSign)
        Me.pnlSign.Location = New System.Drawing.Point(715, 364)
        Me.pnlSign.Name = "pnlSign"
        Me.pnlSign.Size = New System.Drawing.Size(186, 184)
        Me.pnlSign.TabIndex = 409
        Me.pnlSign.Visible = False
        '
        'lblempsign
        '
        Me.lblempsign.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblempsign.Location = New System.Drawing.Point(4, 8)
        Me.lblempsign.Name = "lblempsign"
        Me.lblempsign.Size = New System.Drawing.Size(128, 17)
        Me.lblempsign.TabIndex = 1
        Me.lblempsign.Text = "Employee Signature:"
        '
        'imgSignature
        '
        Me.imgSignature.Location = New System.Drawing.Point(5, 28)
        Me.imgSignature.Name = "imgSignature"
        Me.imgSignature.Size = New System.Drawing.Size(176, 128)
        Me.imgSignature.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgSignature.TabIndex = 1
        Me.imgSignature.TabStop = False
        '
        'chkconfirmSign
        '
        Me.chkconfirmSign.AutoSize = True
        Me.chkconfirmSign.Location = New System.Drawing.Point(4, 162)
        Me.chkconfirmSign.Name = "chkconfirmSign"
        Me.chkconfirmSign.Size = New System.Drawing.Size(109, 17)
        Me.chkconfirmSign.TabIndex = 0
        Me.chkconfirmSign.Text = "Confirm Signature"
        Me.chkconfirmSign.UseVisualStyleBackColor = True
        '
        'lblMaxInstallmentAmt
        '
        Me.lblMaxInstallmentAmt.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaxInstallmentAmt.Location = New System.Drawing.Point(12, 151)
        Me.lblMaxInstallmentAmt.Name = "lblMaxInstallmentAmt"
        Me.lblMaxInstallmentAmt.Size = New System.Drawing.Size(129, 17)
        Me.lblMaxInstallmentAmt.TabIndex = 411
        Me.lblMaxInstallmentAmt.Text = "Max Installment Amt"
        Me.lblMaxInstallmentAmt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMaxInstallmentAmt
        '
        Me.txtMaxInstallmentAmt.AllowNegative = True
        Me.txtMaxInstallmentAmt.BackColor = System.Drawing.SystemColors.Window
        Me.txtMaxInstallmentAmt.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMaxInstallmentAmt.DigitsInGroup = 3
        Me.txtMaxInstallmentAmt.Enabled = False
        Me.txtMaxInstallmentAmt.Flags = 0
        Me.txtMaxInstallmentAmt.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtMaxInstallmentAmt.Location = New System.Drawing.Point(144, 147)
        Me.txtMaxInstallmentAmt.MaxDecimalPlaces = 6
        Me.txtMaxInstallmentAmt.MaxWholeDigits = 21
        Me.txtMaxInstallmentAmt.Name = "txtMaxInstallmentAmt"
        Me.txtMaxInstallmentAmt.Prefix = ""
        Me.txtMaxInstallmentAmt.RangeMax = 1.7976931348623157E+308
        Me.txtMaxInstallmentAmt.RangeMin = -1.7976931348623157E+308
        Me.txtMaxInstallmentAmt.Size = New System.Drawing.Size(213, 21)
        Me.txtMaxInstallmentAmt.TabIndex = 5
        Me.txtMaxInstallmentAmt.Text = "0"
        Me.txtMaxInstallmentAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtFormulaExpr
        '
        Me.txtFormulaExpr.Location = New System.Drawing.Point(223, 177)
        Me.txtFormulaExpr.Name = "txtFormulaExpr"
        Me.txtFormulaExpr.ReadOnly = True
        Me.txtFormulaExpr.Size = New System.Drawing.Size(134, 20)
        Me.txtFormulaExpr.TabIndex = 412
        Me.txtFormulaExpr.Visible = False
        '
        'lblMinLoanAmount
        '
        Me.lblMinLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMinLoanAmount.Location = New System.Drawing.Point(12, 239)
        Me.lblMinLoanAmount.Name = "lblMinLoanAmount"
        Me.lblMinLoanAmount.Size = New System.Drawing.Size(96, 17)
        Me.lblMinLoanAmount.TabIndex = 414
        Me.lblMinLoanAmount.Text = "Min Principal Amt."
        Me.lblMinLoanAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtMinLoanAmount
        '
        Me.txtMinLoanAmount.AllowNegative = True
        Me.txtMinLoanAmount.BackColor = System.Drawing.SystemColors.Window
        Me.txtMinLoanAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtMinLoanAmount.DigitsInGroup = 3
        Me.txtMinLoanAmount.Enabled = False
        Me.txtMinLoanAmount.Flags = 0
        Me.txtMinLoanAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMinLoanAmount.Location = New System.Drawing.Point(142, 236)
        Me.txtMinLoanAmount.MaxDecimalPlaces = 6
        Me.txtMinLoanAmount.MaxWholeDigits = 21
        Me.txtMinLoanAmount.Name = "txtMinLoanAmount"
        Me.txtMinLoanAmount.Prefix = ""
        Me.txtMinLoanAmount.RangeMax = 1.7976931348623157E+308
        Me.txtMinLoanAmount.RangeMin = -1.7976931348623157E+308
        Me.txtMinLoanAmount.ReadOnly = True
        Me.txtMinLoanAmount.Size = New System.Drawing.Size(213, 21)
        Me.txtMinLoanAmount.TabIndex = 413
        Me.txtMinLoanAmount.Text = "0"
        Me.txtMinLoanAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkMakeExceptionalApplication
        '
        Me.chkMakeExceptionalApplication.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMakeExceptionalApplication.Location = New System.Drawing.Point(493, 7)
        Me.chkMakeExceptionalApplication.Name = "chkMakeExceptionalApplication"
        Me.chkMakeExceptionalApplication.Size = New System.Drawing.Size(193, 17)
        Me.chkMakeExceptionalApplication.TabIndex = 415
        Me.chkMakeExceptionalApplication.Text = "Request for Exceptional Approval?"
        Me.chkMakeExceptionalApplication.UseVisualStyleBackColor = True
        '
        'pnlOtherScanAttachment
        '
        Me.pnlOtherScanAttachment.Controls.Add(Me.btnAddOtherAttachment)
        Me.pnlOtherScanAttachment.Controls.Add(Me.Panel5)
        Me.pnlOtherScanAttachment.Controls.Add(Me.lblOtherDocumentType)
        Me.pnlOtherScanAttachment.Controls.Add(Me.cboOtherDocumentType)
        Me.pnlOtherScanAttachment.Location = New System.Drawing.Point(714, 202)
        Me.pnlOtherScanAttachment.Name = "pnlOtherScanAttachment"
        Me.pnlOtherScanAttachment.Size = New System.Drawing.Size(303, 159)
        Me.pnlOtherScanAttachment.TabIndex = 416
        '
        'btnAddOtherAttachment
        '
        Me.btnAddOtherAttachment.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddOtherAttachment.BackColor = System.Drawing.Color.White
        Me.btnAddOtherAttachment.BackgroundImage = CType(resources.GetObject("btnAddOtherAttachment.BackgroundImage"), System.Drawing.Image)
        Me.btnAddOtherAttachment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAddOtherAttachment.BorderColor = System.Drawing.Color.Empty
        Me.btnAddOtherAttachment.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAddOtherAttachment.FlatAppearance.BorderSize = 0
        Me.btnAddOtherAttachment.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAddOtherAttachment.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddOtherAttachment.ForeColor = System.Drawing.Color.Black
        Me.btnAddOtherAttachment.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAddOtherAttachment.GradientForeColor = System.Drawing.Color.Black
        Me.btnAddOtherAttachment.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddOtherAttachment.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAddOtherAttachment.Location = New System.Drawing.Point(222, 20)
        Me.btnAddOtherAttachment.Name = "btnAddOtherAttachment"
        Me.btnAddOtherAttachment.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAddOtherAttachment.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAddOtherAttachment.Size = New System.Drawing.Size(78, 29)
        Me.btnAddOtherAttachment.TabIndex = 404
        Me.btnAddOtherAttachment.Text = "&Browse"
        Me.btnAddOtherAttachment.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.dgvLoanApplicationOtherAttachment)
        Me.Panel5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel5.Location = New System.Drawing.Point(5, 52)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(295, 99)
        Me.Panel5.TabIndex = 407
        '
        'dgvLoanApplicationOtherAttachment
        '
        Me.dgvLoanApplicationOtherAttachment.AllowUserToAddRows = False
        Me.dgvLoanApplicationOtherAttachment.AllowUserToDeleteRows = False
        Me.dgvLoanApplicationOtherAttachment.AllowUserToResizeRows = False
        Me.dgvLoanApplicationOtherAttachment.BackgroundColor = System.Drawing.Color.White
        Me.dgvLoanApplicationOtherAttachment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvLoanApplicationOtherAttachment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLoanApplicationOtherAttachment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcohOtherDelete, Me.colhOtherDocType, Me.colhOtherName, Me.colhOtherSize, Me.objcolhOtherDownload, Me.objcolhOtherGUID, Me.objcolhOtherScanUnkId})
        Me.dgvLoanApplicationOtherAttachment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvLoanApplicationOtherAttachment.Location = New System.Drawing.Point(0, 0)
        Me.dgvLoanApplicationOtherAttachment.Name = "dgvLoanApplicationOtherAttachment"
        Me.dgvLoanApplicationOtherAttachment.RowHeadersVisible = False
        Me.dgvLoanApplicationOtherAttachment.Size = New System.Drawing.Size(295, 99)
        Me.dgvLoanApplicationOtherAttachment.TabIndex = 0
        '
        'objcohOtherDelete
        '
        Me.objcohOtherDelete.Frozen = True
        Me.objcohOtherDelete.HeaderText = ""
        Me.objcohOtherDelete.Image = Global.Aruti.Main.My.Resources.Resources.remove
        Me.objcohOtherDelete.Name = "objcohOtherDelete"
        Me.objcohOtherDelete.ReadOnly = True
        Me.objcohOtherDelete.Width = 25
        '
        'colhOtherDocType
        '
        Me.colhOtherDocType.Frozen = True
        Me.colhOtherDocType.HeaderText = "Document Type"
        Me.colhOtherDocType.Name = "colhOtherDocType"
        Me.colhOtherDocType.ReadOnly = True
        Me.colhOtherDocType.Width = 150
        '
        'colhOtherName
        '
        Me.colhOtherName.HeaderText = "File Name"
        Me.colhOtherName.Name = "colhOtherName"
        Me.colhOtherName.ReadOnly = True
        Me.colhOtherName.Width = 150
        '
        'colhOtherSize
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.colhOtherSize.DefaultCellStyle = DataGridViewCellStyle3
        Me.colhOtherSize.HeaderText = "File Size"
        Me.colhOtherSize.Name = "colhOtherSize"
        Me.colhOtherSize.ReadOnly = True
        Me.colhOtherSize.Width = 120
        '
        'objcolhOtherDownload
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        Me.objcolhOtherDownload.DefaultCellStyle = DataGridViewCellStyle4
        Me.objcolhOtherDownload.HeaderText = "Download"
        Me.objcolhOtherDownload.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline
        Me.objcolhOtherDownload.Name = "objcolhOtherDownload"
        Me.objcolhOtherDownload.ReadOnly = True
        Me.objcolhOtherDownload.Text = "Download"
        Me.objcolhOtherDownload.UseColumnTextForLinkValue = True
        '
        'objcolhOtherGUID
        '
        Me.objcolhOtherGUID.HeaderText = "objcolhGUID"
        Me.objcolhOtherGUID.Name = "objcolhOtherGUID"
        Me.objcolhOtherGUID.Visible = False
        '
        'objcolhOtherScanUnkId
        '
        Me.objcolhOtherScanUnkId.HeaderText = "objcolhScanUnkId"
        Me.objcolhOtherScanUnkId.Name = "objcolhOtherScanUnkId"
        Me.objcolhOtherScanUnkId.Visible = False
        '
        'lblOtherDocumentType
        '
        Me.lblOtherDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOtherDocumentType.Location = New System.Drawing.Point(4, 5)
        Me.lblOtherDocumentType.Name = "lblOtherDocumentType"
        Me.lblOtherDocumentType.Size = New System.Drawing.Size(296, 16)
        Me.lblOtherDocumentType.TabIndex = 406
        Me.lblOtherDocumentType.Text = "Other Document Type(.PDF and Images only)"
        Me.lblOtherDocumentType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboOtherDocumentType
        '
        Me.cboOtherDocumentType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOtherDocumentType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboOtherDocumentType.FormattingEnabled = True
        Me.cboOtherDocumentType.Location = New System.Drawing.Point(5, 25)
        Me.cboOtherDocumentType.Name = "cboOtherDocumentType"
        Me.cboOtherDocumentType.Size = New System.Drawing.Size(201, 21)
        Me.cboOtherDocumentType.TabIndex = 0
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.nudPayPeriodEOC)
        Me.Panel4.Controls.Add(Me.lblPayPeriodsEOC)
        Me.Panel4.Controls.Add(Me.dtpEndEmplDate)
        Me.Panel4.Controls.Add(Me.lblEmplDate)
        Me.Panel4.Location = New System.Drawing.Point(715, 2)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(302, 35)
        Me.Panel4.TabIndex = 417
        '
        'nudPayPeriodEOC
        '
        Me.nudPayPeriodEOC.Enabled = False
        Me.nudPayPeriodEOC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudPayPeriodEOC.Location = New System.Drawing.Point(70, 7)
        Me.nudPayPeriodEOC.Maximum = New Decimal(New Integer() {-402653185, -1613725636, 54210108, 0})
        Me.nudPayPeriodEOC.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nudPayPeriodEOC.Name = "nudPayPeriodEOC"
        Me.nudPayPeriodEOC.Size = New System.Drawing.Size(61, 21)
        Me.nudPayPeriodEOC.TabIndex = 34
        Me.nudPayPeriodEOC.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudPayPeriodEOC.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblPayPeriodsEOC
        '
        Me.lblPayPeriodsEOC.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriodsEOC.Location = New System.Drawing.Point(4, 7)
        Me.lblPayPeriodsEOC.Name = "lblPayPeriodsEOC"
        Me.lblPayPeriodsEOC.Size = New System.Drawing.Size(64, 17)
        Me.lblPayPeriodsEOC.TabIndex = 32
        Me.lblPayPeriodsEOC.Text = "Pay Periods"
        Me.lblPayPeriodsEOC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpEndEmplDate
        '
        Me.dtpEndEmplDate.Checked = False
        Me.dtpEndEmplDate.Enabled = False
        Me.dtpEndEmplDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpEndEmplDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpEndEmplDate.Location = New System.Drawing.Point(191, 7)
        Me.dtpEndEmplDate.Name = "dtpEndEmplDate"
        Me.dtpEndEmplDate.Size = New System.Drawing.Size(106, 21)
        Me.dtpEndEmplDate.TabIndex = 30
        '
        'lblEmplDate
        '
        Me.lblEmplDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmplDate.Location = New System.Drawing.Point(136, 10)
        Me.lblEmplDate.Name = "lblEmplDate"
        Me.lblEmplDate.Size = New System.Drawing.Size(50, 17)
        Me.lblEmplDate.TabIndex = 29
        Me.lblEmplDate.Text = "Til EOC Date"
        Me.lblEmplDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "File Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 150
        '
        'DataGridViewTextBoxColumn2
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn2.HeaderText = "File Size"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 120
        '
        'DataGridViewTextBoxColumn3
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn3.HeaderText = "objcolhGUID"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Visible = False
        Me.DataGridViewTextBoxColumn3.Width = 120
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "objcolhScanUnkId"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Visible = False
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.HeaderText = "File Name"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Visible = False
        Me.DataGridViewTextBoxColumn5.Width = 150
        '
        'DataGridViewTextBoxColumn6
        '
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle7
        Me.DataGridViewTextBoxColumn6.Frozen = True
        Me.DataGridViewTextBoxColumn6.HeaderText = "File Size"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 120
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.HeaderText = "objcolhGUID"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        Me.DataGridViewTextBoxColumn7.Width = 150
        '
        'DataGridViewTextBoxColumn8
        '
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle8
        Me.DataGridViewTextBoxColumn8.HeaderText = "objcolhScanUnkId"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        Me.DataGridViewTextBoxColumn8.Width = 120
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "objcolhGUID"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objcolhScanUnkId"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'chkSalaryAdvanceLiquidation
        '
        Me.chkSalaryAdvanceLiquidation.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSalaryAdvanceLiquidation.Location = New System.Drawing.Point(153, 219)
        Me.chkSalaryAdvanceLiquidation.Name = "chkSalaryAdvanceLiquidation"
        Me.chkSalaryAdvanceLiquidation.Size = New System.Drawing.Size(155, 17)
        Me.chkSalaryAdvanceLiquidation.TabIndex = 416
        Me.chkSalaryAdvanceLiquidation.Text = "Salary Advance Liquidation"
        Me.chkSalaryAdvanceLiquidation.UseVisualStyleBackColor = True
        Me.chkSalaryAdvanceLiquidation.Visible = False
        '
        'frmFlexcubeLoanApplication_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1024, 610)
        Me.Controls.Add(Me.objpnlLoanProjection)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.pnlOtherScanAttachment)
        Me.Controls.Add(Me.chkMakeExceptionalApplication)
        Me.Controls.Add(Me.lblMinLoanAmount)
        Me.Controls.Add(Me.txtMinLoanAmount)
        Me.Controls.Add(Me.txtFormulaExpr)
        Me.Controls.Add(Me.lblMaxInstallmentAmt)
        Me.Controls.Add(Me.txtMaxInstallmentAmt)
        Me.Controls.Add(Me.pnlSign)
        Me.Controls.Add(Me.lblAmt)
        Me.Controls.Add(Me.pnlScanAttachment)
        Me.Controls.Add(Me.txtAppliedUptoPrincipalAmt)
        Me.Controls.Add(Me.lblPurposeOfCredit)
        Me.Controls.Add(Me.txtPurposeOfCredit)
        Me.Controls.Add(Me.lblIntAmt)
        Me.Controls.Add(Me.cboLoanCalcType)
        Me.Controls.Add(Me.lblMaxInstallment)
        Me.Controls.Add(Me.txtIntAmt)
        Me.Controls.Add(Me.cboInterestCalcType)
        Me.Controls.Add(Me.nudMaxInstallment)
        Me.Controls.Add(Me.txtInsuranceRate)
        Me.Controls.Add(Me.lblInsuranceRate)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.lblInterestCalcType)
        Me.Controls.Add(Me.lblLoanCalcType)
        Me.Controls.Add(Me.objFooter)
        Me.Controls.Add(Me.txtLoanRate)
        Me.Controls.Add(Me.lblLoanInterest)
        Me.Controls.Add(Me.lblMaxLoanAmount)
        Me.Controls.Add(Me.txtMaxLoanAmount)
        Me.Controls.Add(Me.txtRepaymentDays)
        Me.Controls.Add(Me.lblRepaymentDays)
        Me.Controls.Add(Me.cboLoanSchemeCategory)
        Me.Controls.Add(Me.lblLoanSchemeCategory)
        Me.Controls.Add(Me.objLoanSchemeSearch)
        Me.Controls.Add(Me.cboEmpName)
        Me.Controls.Add(Me.lblEmpName)
        Me.Controls.Add(Me.objbtnSearchEmployee)
        Me.Controls.Add(Me.lblLoanScheme)
        Me.Controls.Add(Me.cboLoanScheme)
        Me.Controls.Add(Me.objbtnAddLoanScheme)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFlexcubeLoanApplication_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add / Edit Loan Application"
        Me.objFooter.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.nudTitleValidity, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.objpnlLoanProjection.ResumeLayout(False)
        Me.pnlInformation.ResumeLayout(False)
        Me.pnlInformation.PerformLayout()
        CType(Me.nudInstallmentPaid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudDuration, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudMaxInstallment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        CType(Me.dgvLoanApplicationAttachment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlScanAttachment.ResumeLayout(False)
        Me.pnlSign.ResumeLayout(False)
        Me.pnlSign.PerformLayout()
        CType(Me.imgSignature, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlOtherScanAttachment.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        CType(Me.dgvLoanApplicationOtherAttachment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        CType(Me.nudPayPeriodEOC, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLoanScheme As System.Windows.Forms.Label
    Friend WithEvents cboLoanScheme As System.Windows.Forms.ComboBox
    Friend WithEvents objbtnAddLoanScheme As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmpName As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmpName As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents objLoanSchemeSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents lblLoanSchemeCategory As System.Windows.Forms.Label
    Friend WithEvents cboLoanSchemeCategory As System.Windows.Forms.ComboBox
    Friend WithEvents txtRepaymentDays As eZee.TextBox.NumericTextBox
    Friend WithEvents lblRepaymentDays As System.Windows.Forms.Label
    Friend WithEvents lblMaxLoanAmount As System.Windows.Forms.Label
    Friend WithEvents txtMaxLoanAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents objlblExRate As System.Windows.Forms.Label
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents objvalPeriodDuration As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtBlockNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblBlockNo As System.Windows.Forms.Label
    Friend WithEvents txtPlotNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblPlotNo As System.Windows.Forms.Label
    Friend WithEvents lblFSV As System.Windows.Forms.Label
    Friend WithEvents txtLONo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblLONo As System.Windows.Forms.Label
    Friend WithEvents txtCTNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblCTNo As System.Windows.Forms.Label
    Friend WithEvents lblMarketValue As System.Windows.Forms.Label
    Friend WithEvents txtTownCity As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblTownCity As System.Windows.Forms.Label
    Friend WithEvents txtStreet As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblStreet As System.Windows.Forms.Label
    Friend WithEvents txtMarketValue As eZee.TextBox.NumericTextBox
    Friend WithEvents lblAmt As System.Windows.Forms.Label
    Friend WithEvents objpnlLoanProjection As System.Windows.Forms.Panel
    Friend WithEvents cboLoanCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents cboInterestCalcType As System.Windows.Forms.ComboBox
    Friend WithEvents lblInterestCalcType As System.Windows.Forms.Label
    Friend WithEvents pnlInformation As System.Windows.Forms.Panel
    Friend WithEvents lblIntAmt As System.Windows.Forms.Label
    Friend WithEvents lblPrincipalAmt As System.Windows.Forms.Label
    Friend WithEvents txtIntAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents txtAppliedUptoPrincipalAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents txtLoanRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblLoanInterest As System.Windows.Forms.Label
    Friend WithEvents lblEMIInstallments As System.Windows.Forms.Label
    Friend WithEvents nudDuration As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblEMIAmount As System.Windows.Forms.Label
    Friend WithEvents txtInstallmentAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents elLoanAmountCalculation As eZee.Common.eZeeLine
    Friend WithEvents lblLoanCalcType As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtYOM As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblYOM As System.Windows.Forms.Label
    Friend WithEvents lblModel As System.Windows.Forms.Label
    Friend WithEvents txtModel As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtFSV As eZee.TextBox.NumericTextBox
    Friend WithEvents lblMaxInstallment As System.Windows.Forms.Label
    Friend WithEvents nudMaxInstallment As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtInsuranceRate As eZee.TextBox.NumericTextBox
    Friend WithEvents lblInsuranceRate As System.Windows.Forms.Label
    Friend WithEvents txtInsuranceAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblInsuranceAmt As System.Windows.Forms.Label
    Friend WithEvents lblTakeHome As System.Windows.Forms.Label
    Friend WithEvents txtTakeHome As eZee.TextBox.NumericTextBox
    Friend WithEvents lblPurposeOfCredit As System.Windows.Forms.Label
    Friend WithEvents txtPurposeOfCredit As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboDocumentType As System.Windows.Forms.ComboBox
    Friend WithEvents btnAddAttachment As eZee.Common.eZeeLightButton
    Friend WithEvents lblDocumentType As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents dgvLoanApplicationAttachment As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ofdAttachment As System.Windows.Forms.OpenFileDialog
    Friend WithEvents sfdAttachment As System.Windows.Forms.SaveFileDialog
    Friend WithEvents pnlScanAttachment As System.Windows.Forms.Panel
    Friend WithEvents pnlSign As System.Windows.Forms.Panel
    Friend WithEvents imgSignature As System.Windows.Forms.PictureBox
    Friend WithEvents chkconfirmSign As System.Windows.Forms.CheckBox
    Friend WithEvents lblempsign As System.Windows.Forms.Label
    Friend WithEvents btnConfirmSign As eZee.Common.eZeeLightButton
    Friend WithEvents cboCurrency As System.Windows.Forms.ComboBox
    Friend WithEvents lblMaxInstallmentAmt As System.Windows.Forms.Label
    Friend WithEvents txtMaxInstallmentAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents txtLoanAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents txtFormulaExpr As System.Windows.Forms.TextBox
    Friend WithEvents lblMinLoanAmount As System.Windows.Forms.Label
    Friend WithEvents txtMinLoanAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents chkMakeExceptionalApplication As System.Windows.Forms.CheckBox
    Friend WithEvents txtOutStandingPrincipalAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblOutStandingPrincipalAmt As System.Windows.Forms.Label
    Friend WithEvents txtOutStandingInterestAmt As eZee.TextBox.NumericTextBox
    Friend WithEvents lblOutStandingInterestAmt As System.Windows.Forms.Label
    Friend WithEvents lblInstallmentPaid As System.Windows.Forms.Label
    Friend WithEvents nudInstallmentPaid As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtColour As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblColour As System.Windows.Forms.Label
    Friend WithEvents txtChassisNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents txtSupplier As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblSupplier As System.Windows.Forms.Label
    Friend WithEvents lblChassisNo As System.Windows.Forms.Label
    Friend WithEvents pnlOtherScanAttachment As System.Windows.Forms.Panel
    Friend WithEvents btnAddOtherAttachment As eZee.Common.eZeeLightButton
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents dgvLoanApplicationOtherAttachment As System.Windows.Forms.DataGridView
    Friend WithEvents lblOtherDocumentType As System.Windows.Forms.Label
    Friend WithEvents cboOtherDocumentType As System.Windows.Forms.ComboBox
    Friend WithEvents dtpTitleIssueDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTitleIssueDate As System.Windows.Forms.Label
    Friend WithEvents dtpTitleExpiryDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTitleExpriryDate As System.Windows.Forms.Label
    Friend WithEvents lblTitleValidity As System.Windows.Forms.Label
    Friend WithEvents nudTitleValidity As System.Windows.Forms.NumericUpDown
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents dtpEndEmplDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblEmplDate As System.Windows.Forms.Label
    Friend WithEvents nudPayPeriodEOC As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblPayPeriodsEOC As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents txtTotalCIF As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTotalCIF As System.Windows.Forms.Label
    Friend WithEvents txtModelNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblModelNo As System.Windows.Forms.Label
    Friend WithEvents txtInvoiceValue As eZee.TextBox.NumericTextBox
    Friend WithEvents lblInvoiceValue As System.Windows.Forms.Label
    Friend WithEvents txtEstimatedTax As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEstimatedTax As System.Windows.Forms.Label
    Friend WithEvents txtEngineCapacity As eZee.TextBox.NumericTextBox
    Friend WithEvents lblEngineCapacity As System.Windows.Forms.Label
    Friend WithEvents txtEngineNo As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents lblEngineNo As System.Windows.Forms.Label
    Friend WithEvents txtBOQ As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents LblBOQ As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcohDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhDocType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhSize As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhDownload As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents objcolhGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhScanUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcohOtherDelete As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents colhOtherDocType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhOtherName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhOtherSize As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhOtherDownload As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents objcolhOtherGUID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhOtherScanUnkId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents chkSalaryAdvanceLiquidation As System.Windows.Forms.CheckBox
End Class
