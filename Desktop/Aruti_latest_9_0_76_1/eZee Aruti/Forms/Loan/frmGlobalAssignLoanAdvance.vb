﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmGlobalAssignLoanAdvance

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmGlobalAssignLoanAdvance"
    Private mdtTran As DataTable
    Private objLoan_Advance As clsLoan_Advance
    Private objPendingLoan As clsProcess_pending_loan
    Private mblnIsFormLoad As Boolean = False
    Private dtChkRow() As DataRow = Nothing

    Private mdecInterest_Amount As Decimal = 0
    Private mdecNetAmount As Decimal = 0
    Private mdecInstallmentAmount As Decimal = 0
    Private mintTotalEMI As Integer = 0
    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage


#End Region

#Region " Private Functions & Procedure "

    Private Sub FillCombo()
        Dim dsCombo As New DataSet
        Dim objBranch As New clsStation
        Dim objDept As New clsDepartment
        Dim objSection As New clsSections
        Dim objJob As New clsJobs
        Dim objEmp As New clsEmployee_Master
        Dim objApprover As New clsLoan_Approver
        Dim objScheme As New clsLoan_Scheme
        Dim objPeriod As New clscommom_period_Tran
        Dim objCMaster As New clsMasterData
        Try

            dsCombo = objBranch.getComboList("List", True)
            With cboBranch
                .ValueMember = "stationunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objDept.getComboList("List", True)
            With cboDepartment
                .ValueMember = "departmentunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objSection.getComboList("List", True)
            With cboSection
                .ValueMember = "sectionunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objJob.getComboList("List", True)
            With cboJob
                .ValueMember = "jobunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombo = objEmp.GetEmployeeList("List", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dsCombo = objEmp.GetEmployeeList("List", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            dsCombo = objEmp.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                             User._Object._Userunkid, _
                                             FinancialYear._Object._YearUnkid, _
                                             Company._Object._Companyunkid, _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                             ConfigParameter._Object._UserAccessModeSetting, _
                                             True, False, "List", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objApprover.GetList("List", True, True)
            With cboApprover
                .ValueMember = "approverunkid"
                .DisplayMember = "approvername"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objScheme.getComboList(True, "List")
            With cboLoan
                .ValueMember = "loanschemeunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            dsCombo = objCMaster.GetLoanScheduling("List")
            With cboLoanScheduleBy
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "List", True, enStatusType.Open)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List").Copy
                .SelectedValue = 0
            End With

            With cboDeductionPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedValue = 0
            End With

            cboMode.Items.Clear()
            cboMode.Items.Add(Language.getMessage(mstrModuleName, 1, "Loan"))
            cboMode.Items.Add(Language.getMessage(mstrModuleName, 2, "Advance"))
            cboMode.SelectedIndex = 0

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
            dsCombo.Dispose() : objBranch = Nothing : objDept = Nothing : objSection = Nothing _
          : objJob = Nothing : objEmp = Nothing : objApprover = Nothing : objScheme = Nothing _
          : objCMaster = Nothing : objPeriod = Nothing
        End Try
    End Sub

    Private Sub FillGrid()
        mdtTran = objPendingLoan.GetToAssignList(2, _
                                                 CInt(cboDepartment.SelectedValue), _
                                                 CInt(cboBranch.SelectedValue), _
                                                 CInt(cboSection.SelectedValue), _
                                                 CInt(cboJob.SelectedValue), _
                                                 CInt(cboEmployee.SelectedValue), _
                                                 CInt(cboApprover.SelectedValue), _
                                                 CInt(cboMode.SelectedIndex), _
                                                 CInt(cboLoan.SelectedValue), "mData")

        dgvDataList.AutoGenerateColumns = False

        dgcolhAmount.DataPropertyName = "Amount"
        dgcolhApprover.DataPropertyName = "Approver"
        dgcolhCode.DataPropertyName = "ECode"
        dgcolhEmployee.DataPropertyName = "Employee"
        objdgcolhSelect.DataPropertyName = "IsCheck"
        objdgcolhGrpId.DataPropertyName = "GrpId"
        objdgcolhIsEx.DataPropertyName = "IsEx"
        objdgcolhIsGrp.DataPropertyName = "IsGrp"
        objdgcolhPendingunkid.DataPropertyName = "Pendingunkid"
        objdgcolhApprId.DataPropertyName = "ApprId"
        objdgcolhEmpId.DataPropertyName = "EmpId"

        dgvDataList.DataSource = mdtTran

        dgcolhAmount.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleRight
        dgcolhAmount.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        dgcolhAmount.DefaultCellStyle.Format = GUI.fmtCurrency

        Call SetGridColor()

        Call SetCollapseValue()

    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.ForeColor = Color.Black
            dgvcsChild.SelectionForeColor = Color.Black
            dgvcsChild.SelectionBackColor = Color.White
            dgvcsChild.BackColor = Color.White


            For i As Integer = 0 To dgvDataList.RowCount - 1
                If CBool(dgvDataList.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    dgvDataList.Rows(i).DefaultCellStyle = dgvcsHeader
                    'Else
                    '    dgvDataList.Rows(i).DefaultCellStyle = dgvcsChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgvDataList.Font.FontFamily, 13, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim objdgvsCollapseChild As New DataGridViewCellStyle
            objdgvsCollapseChild.Font = New Font(dgvDataList.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseChild.ForeColor = Color.Black
            objdgvsCollapseChild.SelectionBackColor = Color.White
            objdgvsCollapseChild.SelectionBackColor = Color.White
            objdgvsCollapseChild.Alignment = DataGridViewContentAlignment.MiddleCenter


            For i As Integer = 0 To dgvDataList.RowCount - 1
                If CBool(dgvDataList.Rows(i).Cells(objdgcolhIsGrp.Index).Value) = True Then
                    If dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value Is Nothing Then
                        'dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = "▬"
                        dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                    End If
                    dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseHeader
                Else
                    dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Value = imgBlankIcon
                    '    dgvDataList.Rows(i).Cells(objdgcolhCollapse.Index).Style = objdgvsCollapseChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidData() As Boolean
        Try
            'If ConfigParameter._Object._LoanVocNoType = 0 Then
            '    If txtVoucherNo.Text.Trim.Length <= 0 Then
            '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Voucher No. cannot be blank. Voucher No. is compulsory information."), enMsgBoxStyle.Information)
            '        txtVoucherNo.Focus()
            '        Return False
            '    End If
            'End If

            dtChkRow = mdtTran.Select("IsCheck= true AND IsGrp = false")

            If dtChkRow.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please check atleast one employee in order to assign Loan or Advance."), enMsgBoxStyle.Information)
                Return False
            End If

            If CInt(cboPayPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Assigned Period is compulsory information. Please select Assigned Period."), enMsgBoxStyle.Information)
                cboPayPeriod.Focus()
                Return False
            End If

            If CInt(cboMode.SelectedIndex) > 0 Then
                Select Case CInt(cboMode.SelectedIndex)
                    Case 0
                        If Trim(txtLoanInterest.Text) = "" Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Loan Interest cannot be blank. Loan Interest is compulsory information."), enMsgBoxStyle.Information)
                            txtLoanInterest.Focus()
                            Return False
                        ElseIf CDbl(Trim(txtLoanInterest.Text)) > 100 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Loan Interest cannot be greater than 100%. Please provide proper Loan Interest."), enMsgBoxStyle.Information)
                            txtLoanInterest.Focus()
                            Return False
                        End If

                        If CInt(cboLoanScheduleBy.SelectedValue) <= 0 Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Loan Schedule is compulsory information. Please select Loan Schedule to continue."), enMsgBoxStyle.Information)
                            cboLoanScheduleBy.Focus()
                            Return False
                        End If

                        Select Case CInt(cboLoanScheduleBy.SelectedValue)
                            Case 1  'AMOUNT WISE
                                If txtInstallmentAmt.Decimal = 0 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Installment Amount cannot be blank. Installment Amount is compulsory information."), enMsgBoxStyle.Information)
                                    txtInstallmentAmt.Focus()
                                    Return False
                                End If
                            Case 2  'PERIOD WISE
                                If txtInstallmentNo.Decimal = 0 Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "No. of Installments cannot be blank. No. of Installments is compulsory information."), enMsgBoxStyle.Information)
                                    txtInstallmentNo.Focus()
                                    Return False
                                End If
                        End Select
                End Select
            End If


            '<TODO -- NET AMOUNT VALIDATION IS TO BE KEPT LIKE IN LOAN/ADVANCE ADD EDIT> 

            Dim objPeriod As New clscommom_period_Tran
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objPeriod._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPayPeriod.SelectedValue)
            'Sohail (21 Aug 2015) -- End

            If dtpDate.Value.Date > objPeriod._End_Date Or dtpDate.Value.Date < objPeriod._Start_Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Effective date should be in between ") & _
                                objPeriod._Start_Date & Language.getMessage(mstrModuleName, 13, " And ") & _
                                objPeriod._End_Date, enMsgBoxStyle.Information)
                dtpDate.Focus()
                Return False
            End If

            If CInt(cboDeductionPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Deduction period is compulsory information. Please select Deduction period to continue."), enMsgBoxStyle.Information)
                cboDeductionPeriod.Focus()
                Return False
            End If

            If CInt(cboDeductionPeriod.SelectedValue) > 0 Then
                Dim objDPeriod As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objDPeriod._Periodunkid = CInt(cboDeductionPeriod.SelectedValue)
                objDPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboDeductionPeriod.SelectedValue)
                'Sohail (21 Aug 2015) -- End

                If objDPeriod._Start_Date < objPeriod._Start_Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Deduction period cannot be less than the Loan Period."), enMsgBoxStyle.Information)
                    cboDeductionPeriod.Focus()
                    Return False
                End If

                objDPeriod = Nothing
            End If

            objPeriod = Nothing

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidData", mstrModuleName)
        Finally
        End Try
    End Function

    Private Sub SetVisibility()
        Try
            If ConfigParameter._Object._LoanVocNoType = 1 Then
                nudManualNo.Enabled = False
            Else
                nudManualNo.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetColor()
        Try
            txtInstallmentAmt.BackColor = GUI.ColorComp
            txtInstallmentNo.BackColor = GUI.ColorComp
            txtLoanInterest.BackColor = GUI.ColorComp
            txtPurpose.BackColor = GUI.ColorOptional
            txtPrefix.BackColor = GUI.ColorComp
            nudManualNo.BackColor = GUI.ColorComp
            nudDuration.BackColor = GUI.ColorComp
            cboApprover.BackColor = GUI.ColorOptional
            cboBranch.BackColor = GUI.ColorOptional
            cboDeductionPeriod.BackColor = GUI.ColorComp
            cboDepartment.BackColor = GUI.ColorOptional
            cboEmployee.BackColor = GUI.ColorOptional
            cboJob.BackColor = GUI.ColorOptional
            cboLoan.BackColor = GUI.ColorOptional
            cboLoanScheduleBy.BackColor = GUI.ColorComp
            cboMode.BackColor = GUI.ColorComp
            cboPayPeriod.BackColor = GUI.ColorComp
            cboSection.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function SetLoanAdvanceValue(ByVal intValue As Integer) As Boolean
        Try
            objLoan_Advance = New clsLoan_Advance
            Call Calculate_Loan_Amount(intValue)
            Select Case CInt(cboMode.SelectedIndex)
                Case 0  'LOAN ASSIGNMENT
                    objLoan_Advance._Loan_Amount = CDec(dtChkRow(intValue)("Amount"))
                    objLoan_Advance._Loan_Duration = CInt(nudDuration.Value)
                    objLoan_Advance._Loanscheduleunkid = CInt(cboLoanScheduleBy.SelectedValue)
                    objLoan_Advance._Loanschemeunkid = CInt(dtChkRow(intValue)("GrpId"))
                    objLoan_Advance._Net_Amount = mdecNetAmount
                    objLoan_Advance._Isloan = True
                    'S.SANDEEP [ 18 JAN 2014 ] -- START
                    objLoan_Advance._LoanBF_Amount = mdecNetAmount
                    'S.SANDEEP [ 18 JAN 2014 ] -- END
                Case 1  'ADVANCE ASSIGNMENT
                    objLoan_Advance._Advance_Amount = CDec(dtChkRow(intValue)("Amount"))
                    objLoan_Advance._Loanscheduleunkid = 0
                    objLoan_Advance._Loanschemeunkid = 0
                    objLoan_Advance._Net_Amount = 0
                    objLoan_Advance._Isloan = False
            End Select
            objLoan_Advance._Approverunkid = CInt(dtChkRow(intValue)("ApprId"))

            If CBool(dtChkRow(intValue)("IsEx")) = True Then
                Select Case CInt(cboMode.SelectedIndex)
                    Case 0
                        objLoan_Advance._Balance_Amount = mdecNetAmount
                    Case 1
                        objLoan_Advance._Balance_Amount = CDec(dtChkRow(intValue)("Amount"))
                End Select
            Else
                objLoan_Advance._Balance_Amount = 0
            End If

            Select Case CInt(cboMode.SelectedIndex)
                Case 0
                    If radSimpleInterest.Checked Then
                        objLoan_Advance._Calctype_Id = enLoanCalcId.Simple_Interest '1
                    ElseIf radCompoundInterest.Checked Then
                        objLoan_Advance._Calctype_Id = enLoanCalcId.Compound_Interest   '2
                    ElseIf radReduceBalInterest.Checked Then
                        objLoan_Advance._Calctype_Id = enLoanCalcId.Reducing_Amount '3
                    End If
                Case Else
                    objLoan_Advance._Calctype_Id = 0
            End Select
            objLoan_Advance._Deductionperiodunkid = CInt(cboDeductionPeriod.SelectedValue)
            objLoan_Advance._Effective_Date = dtpDate.Value

            Select Case CInt(cboMode.SelectedIndex)
                Case 0
                    Calculate_Loan_EMI()
                    If CInt(cboLoanScheduleBy.SelectedValue) > 0 Then
                        Select Case CInt(cboLoanScheduleBy.SelectedValue)
                            Case 1  'AMOUNT WISE
                                If txtInstallmentAmt.Decimal > mdecNetAmount Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "The Installments Amount cannot be greater than the Net Amount."), enMsgBoxStyle.Information)
                                    Return False
                                End If

                                If mintTotalEMI > CInt(nudDuration.Value) Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Total Installment No. cannot be greater than loan duration."), enMsgBoxStyle.Information)
                                    Return False
                                End If

                                If radReduceBalInterest.Checked Then
                                    objLoan_Advance._Emi_Amount = mdecInstallmentAmount
                                Else
                                    objLoan_Advance._Emi_Amount = txtInstallmentAmt.Decimal
                                End If
                                objLoan_Advance._Emi_Tenure = mintTotalEMI
                            Case 2  'PERIOD WISE
                                If mdecInstallmentAmount > mdecNetAmount Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "The Installments Amount cannot be greater than the Net Amount."), enMsgBoxStyle.Information)
                                    Return False
                                End If
                                If txtInstallmentNo.Int > CInt(nudDuration.Value) Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Total Installment No. cannot be greater than loan duration."), enMsgBoxStyle.Information)
                                    Return False
                                End If
                                objLoan_Advance._Emi_Amount = mdecInstallmentAmount
                                objLoan_Advance._Emi_Tenure = CInt(txtInstallmentNo.Text)
                        End Select
                    End If
            End Select

            objLoan_Advance._Employeeunkid = CInt(dtChkRow(intValue)("EmpId"))
            Select Case CInt(cboMode.SelectedIndex)
                Case 0
                    objLoan_Advance._Interest_Amount = mdecInterest_Amount
                    objLoan_Advance._Interest_Rate = txtLoanInterest.Decimal
                Case Else
                    objLoan_Advance._Interest_Amount = 0
                    objLoan_Advance._Interest_Rate = 0
            End Select
            objLoan_Advance._Isbrought_Forward = False
            objLoan_Advance._Loan_Purpose = txtPurpose.Text
            If ConfigParameter._Object._LoanVocNoType = 1 Then
                objLoan_Advance._Loanvoucher_No = ""
            Else
                objLoan_Advance._Loanvoucher_No = txtPrefix.Text.Trim & nudManualNo.Value.ToString
            End If
            objLoan_Advance._Periodunkid = CInt(cboPayPeriod.SelectedValue)
            objLoan_Advance._Userunkid = User._Object._Userunkid
            objLoan_Advance._Voiduserunkid = -1
            objLoan_Advance._Voiddatetime = Nothing
            objLoan_Advance._Isvoid = False
            objLoan_Advance._LoanStatus = 1
            objLoan_Advance._Processpendingloanunkid = CInt(dtChkRow(intValue)("Pendingunkid"))


            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLoanAdvanceValue", mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    Private Sub Calculate_Loan_Amount(ByVal iCnt As Integer)
        Try
            If radSimpleInterest.Checked Then
                mdecNetAmount = 0 : mdecInterest_Amount = 0
                objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Simple_Interest, CDec(dtChkRow(iCnt)("Amount")), CInt(nudDuration.Value), txtLoanInterest.Decimal, mdecNetAmount, mdecInterest_Amount)
            ElseIf radCompoundInterest.Checked Then
                mdecNetAmount = 0 : mdecInterest_Amount = 0
                objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Compound_Interest, CDec(dtChkRow(iCnt)("Amount")), CInt(nudDuration.Value), txtLoanInterest.Decimal, mdecNetAmount, mdecInterest_Amount)
            ElseIf radReduceBalInterest.Checked Then
                mdecNetAmount = 0 : mdecInterest_Amount = 0
                objLoan_Advance.Calculate_LoanInfo(enLoanCalcId.Reducing_Amount, CDec(dtChkRow(iCnt)("Amount")), CInt(nudDuration.Value), txtLoanInterest.Decimal, mdecNetAmount, mdecInterest_Amount, mdecInstallmentAmount)
                'If radReduceBalInterest.Checked Then
                '    cboLoanScheduleBy.SelectedValue = 1
                '    cboLoanScheduleBy.Enabled = False
                '    txtInstallmentAmt.Text = CStr(mdecEMIAmt)
                'End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Calculate_Loan_EMI()
        Select Case CInt(cboLoanScheduleBy.SelectedValue)
            Case 1
                If radReduceBalInterest.Checked Then
                    objLoan_Advance.Calculate_LoanEMI(CInt(cboLoanScheduleBy.SelectedValue), mdecNetAmount, mdecInstallmentAmount, mintTotalEMI)
                Else
                    objLoan_Advance.Calculate_LoanEMI(CInt(cboLoanScheduleBy.SelectedValue), mdecNetAmount, txtInstallmentAmt.Decimal, mintTotalEMI)
                End If
            Case 2
                objLoan_Advance.Calculate_LoanEMI(CInt(cboLoanScheduleBy.SelectedValue), mdecNetAmount, mdecInstallmentAmount, txtInstallmentNo.Int)
        End Select
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmGlobalAssignLoanAdvance_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objLoan_Advance = Nothing : objPendingLoan = Nothing : dtChkRow = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalAssignLoanAdvance_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmGlobalAssignLoanAdvance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objLoan_Advance = New clsLoan_Advance : objPendingLoan = New clsProcess_pending_loan
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            Call SetVisibility()
            Call SetColor()
            Call FillCombo()
            Call FillGrid()
            mblnIsFormLoad = True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalAssignLoanAdvance_Load", mstrModuleName)
        Finally
        End Try
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            clsLoan_Advance.SetMessages()
            objfrm._Other_ModuleNames = "clsLoan_Advance"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Button's Events "

    Private Sub btnAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        Dim blnFlag As Boolean = False
        Dim blnFail As Boolean = False
        Try
            If IsValidData() = False Then Exit Sub

            If dtChkRow.Length > 0 Then
                For i As Integer = 0 To dtChkRow.Length - 1
                    Select Case CInt(cboMode.SelectedIndex)
                        Case 0  'LOAN ASSIGNMENT
                            If SetLoanAdvanceValue(i) Then
                                If objLoan_Advance.Insert Then
                                    blnFlag = True
                                Else
                                    dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.BackColor = Color.Red
                                    dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.SelectionBackColor = Color.Red

                                    blnFail = True
                                    blnFlag = False

                                End If
                            Else
                                dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.BackColor = Color.Red
                                dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.SelectionBackColor = Color.Red

                                blnFail = True

                            End If
                        Case 1  'ADVANCE ASSIGNMENT
                            If SetLoanAdvanceValue(i) Then
                                If objLoan_Advance.Insert Then
                                    blnFlag = True
                                Else
                                    dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.BackColor = Color.Red
                                    dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.SelectionBackColor = Color.Red

                                    blnFail = True
                                    blnFlag = False

                                End If
                            Else
                                dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.BackColor = Color.Red
                                dgvDataList.Rows(mdtTran.Rows.IndexOf(dtChkRow(i))).DefaultCellStyle.SelectionBackColor = Color.Red

                                blnFail = True

                            End If
                    End Select
                Next
            End If

            If blnFlag = True And blnFail = False Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Loan/Advance successfully assigned."), enMsgBoxStyle.Information)
                Call FillGrid()
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Some Loan/Advance assignment failed and marked with red color."), enMsgBoxStyle.Information)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAssign_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Controls "

    Private Sub dgvDataList_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDataList.CellContentClick, dgvDataList.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgvDataList.IsCurrentCellDirty Then
                Me.dgvDataList.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhIsGrp.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case 0
                        'If dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value.ToString = "▬" Then
                        '    dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "+"
                        'Else
                        '    dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = "▬"
                        'End If

                        If dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value Is imgMinusIcon Then
                            dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgPlusIcon
                        Else
                            dgvDataList.Rows(e.RowIndex).Cells(objdgcolhCollapse.Index).Value = imgMinusIcon
                        End If

                        For i = e.RowIndex + 1 To dgvDataList.RowCount - 1
                            If CInt(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvDataList.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                If dgvDataList.Rows(i).Visible = False Then
                                    dgvDataList.Rows(i).Visible = True
                                Else
                                    dgvDataList.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next
                    Case 1
                        For i = e.RowIndex + 1 To dgvDataList.RowCount - 1
                            Dim blnFlg As Boolean = CBool(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhSelect.Index).Value)
                            If CInt(dgvDataList.Rows(e.RowIndex).Cells(objdgcolhGrpId.Index).Value) = CInt(dgvDataList.Rows(i).Cells(objdgcolhGrpId.Index).Value) Then
                                dgvDataList.Rows(i).Cells(objdgcolhSelect.Index).Value = blnFlg
                            End If
                        Next
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvDataList_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged, _
                                                                                                                   cboDepartment.SelectedIndexChanged, _
                                                                                                                   cboSection.SelectedIndexChanged, _
                                                                                                                   cboJob.SelectedIndexChanged, _
                                                                                                                   cboEmployee.SelectedIndexChanged, _
                                                                                                                   cboApprover.SelectedIndexChanged, _
                                                                                                                   cboMode.SelectedIndexChanged, _
                                                                                                                   cboLoan.SelectedIndexChanged
        If mblnIsFormLoad = False Then Exit Sub

        Select Case CType(sender, ComboBox).Name.ToUpper
            Case "CBOMODE"
                Select Case cboMode.SelectedIndex
                    Case 0
                        cboLoan.SelectedValue = 0
                        cboLoan.Enabled = True
                        gbLoanInfo.Enabled = True
                    Case 1
                        cboLoan.SelectedValue = 0
                        cboLoan.Enabled = False
                        gbLoanInfo.Enabled = False
                End Select
        End Select

        Call FillGrid()
    End Sub

    Private Sub cboLoanScheduleBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboLoanScheduleBy.SelectedIndexChanged
        Try
            Select Case CInt(cboLoanScheduleBy.SelectedValue)
                Case 1  'AMOUNT WISE
                    lblEMIAmount.Visible = True
                    txtInstallmentAmt.Visible = True
                    lblNoOfInstallment.Visible = False
                    txtInstallmentNo.Visible = False
                Case 2  'PERIOD WISE
                    lblEMIAmount.Visible = False
                    txtInstallmentAmt.Visible = False
                    lblNoOfInstallment.Visible = True
                    txtInstallmentNo.Visible = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboLoanScheduleBy_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtInstallmentAmt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInstallmentAmt.TextChanged
        Try
            If txtInstallmentAmt.Text = "" Or txtInstallmentAmt.Text = "." Then Exit Sub
            Call Calculate_Loan_EMI()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtInstallmentAmt_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub txtInstallmentNo_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtInstallmentNo.TextChanged
        Try
            If txtInstallmentNo.Text = "" Or txtInstallmentNo.Text = "." Then Exit Sub
            Call Calculate_Loan_EMI()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtInstallmentNo_TextChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub radReduceBalInterest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radReduceBalInterest.CheckedChanged
        Try
            If radReduceBalInterest.Checked Then
                cboLoanScheduleBy.SelectedValue = 1 : cboLoanScheduleBy.Enabled = False
                txtInstallmentAmt.Enabled = False
                txtInstallmentNo.Enabled = False
            Else
                cboLoanScheduleBy.SelectedValue = 0 : cboLoanScheduleBy.Enabled = True
                txtInstallmentAmt.Enabled = True
                txtInstallmentNo.Enabled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "radReduceBalInterest_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbLoanInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbLoanInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbBasicInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbBasicInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnAssign.GradientBackColor = GUI._ButttonBackColor
            Me.btnAssign.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblDepartment.Text = Language._Object.getCaption(Me.lblDepartment.Name, Me.lblDepartment.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblJob.Text = Language._Object.getCaption(Me.lblJob.Name, Me.lblJob.Text)
            Me.lblSection.Text = Language._Object.getCaption(Me.lblSection.Name, Me.lblSection.Text)
            Me.lblLoan.Text = Language._Object.getCaption(Me.lblLoan.Name, Me.lblLoan.Text)
            Me.lblMode.Text = Language._Object.getCaption(Me.lblMode.Name, Me.lblMode.Text)
            Me.lblApprover.Text = Language._Object.getCaption(Me.lblApprover.Name, Me.lblApprover.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.btnAssign.Text = Language._Object.getCaption(Me.btnAssign.Name, Me.btnAssign.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbLoanInfo.Text = Language._Object.getCaption(Me.gbLoanInfo.Name, Me.gbLoanInfo.Text)
            Me.gbBasicInfo.Text = Language._Object.getCaption(Me.gbBasicInfo.Name, Me.gbBasicInfo.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
            Me.lblEffectiveDate.Text = Language._Object.getCaption(Me.lblEffectiveDate.Name, Me.lblEffectiveDate.Text)
            Me.lblDeductionPeriod.Text = Language._Object.getCaption(Me.lblDeductionPeriod.Name, Me.lblDeductionPeriod.Text)
            Me.lblPurpose.Text = Language._Object.getCaption(Me.lblPurpose.Name, Me.lblPurpose.Text)
            Me.lblLoanInterest.Text = Language._Object.getCaption(Me.lblLoanInterest.Name, Me.lblLoanInterest.Text)
            Me.lblDuration.Text = Language._Object.getCaption(Me.lblDuration.Name, Me.lblDuration.Text)
            Me.radReduceBalInterest.Text = Language._Object.getCaption(Me.radReduceBalInterest.Name, Me.radReduceBalInterest.Text)
            Me.radCompoundInterest.Text = Language._Object.getCaption(Me.radCompoundInterest.Name, Me.radCompoundInterest.Text)
            Me.radSimpleInterest.Text = Language._Object.getCaption(Me.radSimpleInterest.Name, Me.radSimpleInterest.Text)
            Me.lnInterestCalcType.Text = Language._Object.getCaption(Me.lnInterestCalcType.Name, Me.lnInterestCalcType.Text)
            Me.lnInstallmentInfo.Text = Language._Object.getCaption(Me.lnInstallmentInfo.Name, Me.lnInstallmentInfo.Text)
            Me.lblLoanSchedule.Text = Language._Object.getCaption(Me.lblLoanSchedule.Name, Me.lblLoanSchedule.Text)
            Me.lblEMIAmount.Text = Language._Object.getCaption(Me.lblEMIAmount.Name, Me.lblEMIAmount.Text)
            Me.lblNoOfInstallment.Text = Language._Object.getCaption(Me.lblNoOfInstallment.Name, Me.lblNoOfInstallment.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.DataGridViewTextBoxColumn6.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn6.Name, Me.DataGridViewTextBoxColumn6.HeaderText)
            Me.DataGridViewTextBoxColumn7.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn7.Name, Me.DataGridViewTextBoxColumn7.HeaderText)
            Me.DataGridViewTextBoxColumn8.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn8.Name, Me.DataGridViewTextBoxColumn8.HeaderText)
            Me.DataGridViewTextBoxColumn9.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn9.Name, Me.DataGridViewTextBoxColumn9.HeaderText)
            Me.DataGridViewTextBoxColumn10.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn10.Name, Me.DataGridViewTextBoxColumn10.HeaderText)
            Me.DataGridViewTextBoxColumn11.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn11.Name, Me.DataGridViewTextBoxColumn11.HeaderText)
			Me.lblVoucherNo.Text = Language._Object.getCaption(Me.lblVoucherNo.Name, Me.lblVoucherNo.Text)
            Me.dgcolhCode.HeaderText = Language._Object.getCaption(Me.dgcolhCode.Name, Me.dgcolhCode.HeaderText)
            Me.dgcolhEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhEmployee.Name, Me.dgcolhEmployee.HeaderText)
            Me.dgcolhApprover.HeaderText = Language._Object.getCaption(Me.dgcolhApprover.Name, Me.dgcolhApprover.HeaderText)
            Me.dgcolhAmount.HeaderText = Language._Object.getCaption(Me.dgcolhAmount.Name, Me.dgcolhAmount.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Loan")
            Language.setMessage(mstrModuleName, 2, "Advance")
            Language.setMessage(mstrModuleName, 3, "Deduction period is compulsory information. Please select Deduction period to continue.")
            Language.setMessage(mstrModuleName, 4, "Please check atleast one employee in order to assign Loan or Advance.")
            Language.setMessage(mstrModuleName, 5, "Total Installment No. cannot be greater than loan duration.")
            Language.setMessage(mstrModuleName, 6, "Assigned Period is compulsory information. Please select Assigned Period.")
            Language.setMessage(mstrModuleName, 7, "Loan Interest cannot be blank. Loan Interest is compulsory information.")
            Language.setMessage(mstrModuleName, 8, "Loan Interest cannot be greater than 100%. Please provide proper Loan Interest.")
            Language.setMessage(mstrModuleName, 9, "Loan Schedule is compulsory information. Please select Loan Schedule to continue.")
            Language.setMessage(mstrModuleName, 10, "Installment Amount cannot be blank. Installment Amount is compulsory information.")
            Language.setMessage(mstrModuleName, 11, "No. of Installments cannot be blank. No. of Installments is compulsory information.")
            Language.setMessage(mstrModuleName, 12, "Effective date should be in between")
            Language.setMessage(mstrModuleName, 13, " And")
            Language.setMessage(mstrModuleName, 14, "Deduction period cannot be less than the Loan Period.")
            Language.setMessage(mstrModuleName, 15, "Loan/Advance successfully assigned.")
            Language.setMessage(mstrModuleName, 16, "Some Loan/Advance assignment failed and marked with red color.")
            Language.setMessage(mstrModuleName, 17, "The Installments Amount cannot be greater than the Net Amount.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class