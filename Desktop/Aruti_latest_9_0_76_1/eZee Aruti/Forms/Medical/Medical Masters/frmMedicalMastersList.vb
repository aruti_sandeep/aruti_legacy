﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
'Last Message Index = 3

Public Class frmMedicalMastersList

#Region "Private Variable"

    Private objMedicalMaster As clsmedical_master
    Private ReadOnly mstrModuleName As String = "frmMedicalMastersList"

#End Region

#Region "Form's Event"

    Private Sub frmMedicalMastersList_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objMedicalMaster = New clsmedical_master
        Try
            Call Set_Logo(Me, gApplicationType)

            Call SetVisibility()

            cboMasterList.Items.Clear()

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes
            'cboMasterList.Items.Add(enMedicalMasterType.Category)
            cboMasterList.Items.Add(enMedicalMasterType.Medical_Category.ToString().Replace("_", " "))
            'Pinkal (12-Oct-2011) -- End

            cboMasterList.Items.Add(enMedicalMasterType.Cover)
            cboMasterList.Items.Add(enMedicalMasterType.Treatment)
            cboMasterList.Items.Add(enMedicalMasterType.Service)
            cboMasterList.SelectedIndex = 0
            Call Language.setLanguage(Me.Name)
            fillList()
            If lvMastercodes.Items.Count > 0 Then lvMastercodes.Items(0).Selected = True
            lvMastercodes.Select()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalMastersList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsmedical_master.SetMessages()
            objfrm._Other_ModuleNames = "clsmedical_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmMedicalMaster_AddEdit As New frmMedicalMaster_AddEdit
            If objfrmMedicalMaster_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE, cboMasterList.SelectedIndex + 1) Then
                Call fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        Dim strName As String = GetMedicalMasterTypeName()

        If lvMastercodes.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select ") & strName & Language.getMessage(mstrModuleName, 2, "  from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvMastercodes.Select()
            Exit Sub
        End If
        Dim objfrmMedicalMaster_AddEdit As New frmMedicalMaster_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvMastercodes.SelectedItems(0).Index
            If objfrmMedicalMaster_AddEdit.displayDialog(CInt(lvMastercodes.SelectedItems(0).Tag), enAction.EDIT_ONE, cboMasterList.SelectedIndex + 1) Then
                Call fillList()
            End If
            objfrmMedicalMaster_AddEdit = Nothing

            lvMastercodes.Items(intSelectedIndex).Selected = True
            lvMastercodes.EnsureVisible(intSelectedIndex)
            lvMastercodes.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmMedicalMaster_AddEdit IsNot Nothing Then objfrmMedicalMaster_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim strName As String = GetMedicalMasterTypeName()
        If lvMastercodes.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select ") & strName & Language.getMessage(mstrModuleName, 2, "  from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvMastercodes.Select()
            Exit Sub
        End If
        If objMedicalMaster.isUsed(CInt(lvMastercodes.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete this ") & strName & Language.getMessage(mstrModuleName, 4, " . Reason: This ") & strName & Language.getMessage(mstrModuleName, 5, " is in use."), enMsgBoxStyle.Information) '?2
            lvMastercodes.Select()
            Exit Sub
        End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvMastercodes.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Are you sure you want to delete this ") & strName & Language.getMessage(mstrModuleName, 7, " ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objMedicalMaster.Delete(CInt(lvMastercodes.SelectedItems(0).Tag))
                lvMastercodes.SelectedItems(0).Remove()

                If lvMastercodes.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvMastercodes.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvMastercodes.Items.Count - 1
                    lvMastercodes.Items(intSelectedIndex).Selected = True
                    lvMastercodes.EnsureVisible(intSelectedIndex)
                ElseIf lvMastercodes.Items.Count <> 0 Then
                    lvMastercodes.Items(intSelectedIndex).Selected = True
                    lvMastercodes.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvMastercodes.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboMasterList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMasterList.SelectedIndexChanged
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboMasterList_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsMedicalMasterList As New DataSet
        Dim strSearching As String = String.Empty
        Dim dtMasterTable As New DataTable
        Try

            If User._Object.Privilege._AllowToViewMedicalMasterList = True Then                'Pinkal (02-Jul-2012) -- Start

                dsMedicalMasterList = objMedicalMaster.GetList("List")

                If CInt(cboMasterList.SelectedIndex) > -1 Then
                    strSearching = "AND mdmastertype_id =" & CInt(cboMasterList.SelectedIndex + 1)
                End If

                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtMasterTable = New DataView(dsMedicalMasterList.Tables("List"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtMasterTable = dsMedicalMasterList.Tables("List")
                End If

                Dim lvItem As ListViewItem

                lvMastercodes.Items.Clear()
                For Each drRow As DataRow In dtMasterTable.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("mdmastercode").ToString
                    lvItem.Tag = drRow("mdmasterunkid")
                    lvItem.SubItems.Add(drRow("mdmastername").ToString)
                    lvItem.SubItems.Add(drRow("mddescription").ToString)
                    lvMastercodes.Items.Add(lvItem)
                Next

                If lvMastercodes.Items.Count > 16 Then
                    colhDescription.Width = 405 - 18
                Else
                    colhDescription.Width = 405
                End If

            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsMedicalMasterList.Dispose()
        End Try
    End Sub

    Private Function GetMedicalMasterTypeName() As String
        Try
            If cboMasterList.SelectedIndex > -1 Then


                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes

                Select Case cboMasterList.SelectedIndex + 1
                    'Case CInt(enMedicalMasterType.Category)
                    '    Return enMedicalMasterType.Category.ToString()
                    Case CInt(enMedicalMasterType.Medical_Category)
                        Return enMedicalMasterType.Medical_Category.ToString().Replace("_", " ")
                    Case CInt(enMedicalMasterType.Cover)
                        Return enMedicalMasterType.Cover.ToString()
                    Case CInt(enMedicalMasterType.Treatment)
                        Return enMedicalMasterType.Treatment.ToString()
                    Case CInt(enMedicalMasterType.Service)
                        Return enMedicalMasterType.Service.ToString()
                End Select

                'Pinkal (12-Oct-2011) -- End

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMedicalMasterTypeName", mstrModuleName)
        End Try
        Return "Master Type"
    End Function

    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AddMedicalMasters
            btnEdit.Enabled = User._Object.Privilege._EditMedicalMasters
            btnDelete.Enabled = User._Object.Privilege._DeleteMedicalMasters

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbMasterList.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbMasterList.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.gbMasterList.Text = Language._Object.getCaption(Me.gbMasterList.Name, Me.gbMasterList.Text)
            Me.lblMasterList.Text = Language._Object.getCaption(Me.lblMasterList.Name, Me.lblMasterList.Text)
            Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
            Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
            Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select")
            Language.setMessage(mstrModuleName, 2, "  from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 3, "Sorry, You cannot delete this")
            Language.setMessage(mstrModuleName, 4, " . Reason: This")
            Language.setMessage(mstrModuleName, 5, " is in use.")
            Language.setMessage(mstrModuleName, 6, "Are you sure you want to delete this")
            Language.setMessage(mstrModuleName, 7, " ?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class