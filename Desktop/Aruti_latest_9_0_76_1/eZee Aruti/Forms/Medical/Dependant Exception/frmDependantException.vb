﻿Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmDependantException

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmDependantException"
    Private objException As clsdependant_exception
    Private mstrAdvanceFilter As String = ""
    Private mdtTable As DataTable = Nothing


    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
    Dim imgMedical As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.NextReservation_16)
    Dim dtMedicalDate As MonthCalendar
    Dim imgLeave As Drawing.Bitmap = New Drawing.Bitmap(My.Resources.NextReservation_16)
    Dim dtLeaveDate As MonthCalendar
    'Pinkal (21-Jul-2014) -- End


#End Region

#Region " Private Methods "

    Private Function Validation() As Boolean
        Try
            If CInt(cboEmployee.SelectedValue) <= 0 AndAlso CInt(cboRelation.SelectedValue) <= 0 And mstrAdvanceFilter.Trim.Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select Employee or Relation to do futher operation on it."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                cboEmployee.Select()
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
        Return True
    End Function

    Private Sub FillList()
        Dim dtTable As DataTable = Nothing
        Dim StrSearching As String = String.Empty
        Try
            Dim objDependant As New clsDependants_Beneficiary_tran

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsDependant As DataSet = objDependant.GetList("Dependant")

            'If CInt(cboEmployee.SelectedValue) > 0 Then
            '    StrSearching = "AND EmpId = " & CInt(cboEmployee.SelectedValue) & " "
            'End If

            'If CInt(cboDependents.SelectedValue) > 0 Then
            '    StrSearching &= "AND DpndtTranId = " & CInt(cboDependents.SelectedValue) & " "
            'End If

            'If CInt(cboRelation.SelectedValue) > 0 Then
            '    StrSearching &= "AND RelationId = " & CInt(cboRelation.SelectedValue) & " "
            'End If

            'If mstrAdvanceFilter.Length > 0 Then
            '    StrSearching &= "AND " & mstrAdvanceFilter
            'End If

            'If StrSearching.Trim.Length > 0 Then
            '    StrSearching = StrSearching.Substring(3)
            'End If

            If CInt(cboEmployee.SelectedValue) > 0 Then
                StrSearching = "AND hrdependants_beneficiaries_tran.employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
            End If

            If CInt(cboDependents.SelectedValue) > 0 Then
                StrSearching &= "AND hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = " & CInt(cboDependents.SelectedValue) & " "
            End If

            If CInt(cboRelation.SelectedValue) > 0 Then
                StrSearching &= "AND cfcommon_master.masterunkid = " & CInt(cboRelation.SelectedValue) & " "
            End If

            If mstrAdvanceFilter.Length > 0 Then
                StrSearching &= "AND " & mstrAdvanceFilter
            End If

            If StrSearching.Trim.Length > 0 Then
                StrSearching = StrSearching.Substring(3)
            End If

            Dim dsDependant As DataSet = objDependant.GetList(FinancialYear._Object._DatabaseName, _
                                                              User._Object._Userunkid, _
                                                              FinancialYear._Object._YearUnkid, _
                                                              Company._Object._Companyunkid, _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                                              ConfigParameter._Object._IsIncludeInactiveEmp, "Dependant", , , StrSearching, , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Sohail (18 May 2019) - [eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)]
            'S.SANDEEP [04 JUN 2015] -- END





            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            If dsDependant IsNot Nothing AndAlso dsDependant.Tables("Dependant").Columns.Contains("MedicalStopdate") = False Then
                dsDependant.Tables("Dependant").Columns.Add("IsMedical", Type.GetType("System.Boolean"))
                dsDependant.Tables("Dependant").Columns.Add("MedicalStopdate", Type.GetType("System.DateTime"))
                dsDependant.Tables("Dependant").Columns("MedicalStopdate").DefaultValue = DBNull.Value
                dsDependant.Tables("Dependant").Columns.Add("MedicalImage", Type.GetType("System.Object"))
                dsDependant.Tables("Dependant").Columns("MedicalImage").DefaultValue = Nothing
            End If

            If dsDependant IsNot Nothing AndAlso dsDependant.Tables("Dependant").Columns.Contains("LeaveStopdate") = False Then
                dsDependant.Tables("Dependant").Columns.Add("IsLeave", Type.GetType("System.Boolean"))
                dsDependant.Tables("Dependant").Columns.Add("LeaveStopdate", Type.GetType("System.DateTime"))
                dsDependant.Tables("Dependant").Columns("LeaveStopdate").DefaultValue = DBNull.Value
                dsDependant.Tables("Dependant").Columns.Add("LeaveImage", Type.GetType("System.Object"))
                dsDependant.Tables("Dependant").Columns("LeaveImage").DefaultValue = Nothing
            End If
            'Pinkal (21-Jul-2014) -- End


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'dtTable = New DataView(dsDependant.Tables("Dependant"), StrSearching, "EmpName,EmpId", DataViewRowState.CurrentRows).ToTable()
            dtTable = New DataView(dsDependant.Tables("Dependant"), "", "EmpName,EmpId", DataViewRowState.CurrentRows).ToTable()
            'S.SANDEEP [04 JUN 2015] -- END

            dgDependants.DataSource = Nothing
            mdtTable.Rows.Clear()

            If dtTable.Rows.Count > 0 Then
                Dim mintEmpID As Integer = -1
                Dim drNewRow As DataRow = Nothing
                Dim intRowCount As Integer = -1
                For Each dr As DataRow In dtTable.Rows

                    If mintEmpID <> CInt(dr("EmpId")) Then
                        mintEmpID = CInt(dr("EmpId"))
                        drNewRow = mdtTable.NewRow
                        drNewRow("Particular") = dr("EmpName").ToString()
                        drNewRow("Isgroup") = True
                        drNewRow("Employeeunkid") = mintEmpID
                        drNewRow("Exceptionunkid") = -1
                        drNewRow("Ischecked") = False
                        drNewRow("Isvoid") = False
                        drNewRow("Relationunkid") = -1
                        drNewRow("Dependantunkid") = -1
                        drNewRow("Relation") = ""

                        'Pinkal (21-Jul-2014) -- Start
                        'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                        drNewRow("IsMedical") = False
                        drNewRow("MedicalStopdate") = DBNull.Value
                        drNewRow("MedicalImage") = New Drawing.Bitmap(16, 16)
                        drNewRow("IsLeave") = False
                        drNewRow("LeaveStopdate") = DBNull.Value
                        drNewRow("LeaveImage") = New Drawing.Bitmap(16, 16)
                        drNewRow("birthdate") = DBNull.Value
                        'Pinkal (21-Jul-2014) -- End
                        mdtTable.Rows.Add(drNewRow)
                        intRowCount += 1
                    End If

                    drNewRow = mdtTable.NewRow
                    drNewRow("Particular") = Space(10) & dr("DpndtBefName").ToString()
                    drNewRow("Dependantunkid") = CInt(dr("DpndtTranId"))
                    drNewRow("Relationunkid") = CInt(dr("RelationId"))
                    drNewRow("Relation") = dr("Relation")
                    drNewRow("Employeeunkid") = mintEmpID

                    'Pinkal (21-Jul-2014) -- Start
                    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                    'drNewRow("Exceptionunkid") = -1
                    drNewRow("Exceptionunkid") = objException.GetExceptionId(mintEmpID, CInt(dr("DpndtTranId")), CInt(dr("RelationId")))
                    'Pinkal (21-Jul-2014) -- End


                    drNewRow("Ischecked") = objException.isExist(mintEmpID, CInt(dr("DpndtTranId")), CInt(dr("RelationId")))
                    drNewRow("Isvoid") = False
                    drNewRow("Isgroup") = False

                    'Pinkal (21-Jul-2014) -- Start
                    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

                    drNewRow("IsMedical") = objException.GetDependantExceptionModule(enModuleReference.Medical, mintEmpID, CInt(dr("DpndtTranId")), CInt(dr("RelationId")))
                    Dim mdtMedicalStopDate As DateTime = objException.GetDependantModuleWiseStopDate(enModuleReference.Medical, mintEmpID, CInt(dr("DpndtTranId")), CInt(dr("RelationId")))
                    drNewRow("MedicalStopdate") = IIf(mdtMedicalStopDate <> Nothing, mdtMedicalStopDate, DBNull.Value)
                    drNewRow("MedicalImage") = imgMedical

                    drNewRow("IsLeave") = objException.GetDependantExceptionModule(enModuleReference.Leave, mintEmpID, CInt(dr("DpndtTranId")), CInt(dr("RelationId")))
                    Dim mdtLeaveStopDate As DateTime = objException.GetDependantModuleWiseStopDate(enModuleReference.Leave, mintEmpID, CInt(dr("DpndtTranId")), CInt(dr("RelationId")))
                    drNewRow("LeaveStopdate") = IIf(mdtLeaveStopDate <> Nothing, mdtLeaveStopDate, DBNull.Value)
                    drNewRow("LeaveImage") = imgLeave

                    If dr("birthdate").ToString.Trim.Length > 0 Then
                        drNewRow("birthdate") = eZeeDate.convertDate(dr("birthdate").ToString()).Date
                    Else
                        drNewRow("birthdate") = DBNull.Value
                    End If
                    'Pinkal (21-Jul-2014) -- End

                    mdtTable.Rows.Add(drNewRow)
                    intRowCount += 1
                    'SetParentChecked(intRowCount, mintEmpID)
                Next

            End If

            dgDependants.AutoGenerateColumns = False
            objcolhIschecked.DataPropertyName = "Ischecked"
            objdgcolhIsGroup.DataPropertyName = "Isgroup"
            objdgcolhEmpId.DataPropertyName = "Employeeunkid"
            dgcolhParticulars.DataPropertyName = "Particular"
            dgcolhRelation.DataPropertyName = "Relation"


            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            dgcolhBirthdate.DataPropertyName = "birthdate"
            dgDependants.Columns(dgcolhMdStopdate.Index).DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToString()
            objcolhIsMedical.DataPropertyName = "IsMedical"
            dgcolhMdStopdate.DataPropertyName = "MedicalStopdate"
            objdgcolhMdImage.DataPropertyName = "MedicalImage"

            dgDependants.Columns(dgcolhLvStopdate.Index).DefaultCellStyle.Format = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToString()
            objcolhIsLeave.DataPropertyName = "IsLeave"
            dgcolhLvStopdate.DataPropertyName = "LeaveStopdate"
            objdgcolhLvImage.DataPropertyName = "LeaveImage"

            'Pinkal (21-Jul-2014) -- End


            dgDependants.DataSource = mdtTable

            SetCheckBoxValue()

            If dgDependants.RowCount > 0 Then
                chkSelectAll.Enabled = True
            Else
                chkSelectAll.Checked = False
                chkSelectAll.Enabled = False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim DsFill As New DataSet
        Try
            'FOR EMPLOYEE 
            Dim objEmployee As New clsEmployee_Master

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    DsFill = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    DsFill = objEmployee.GetEmployeeList("Employee", True)
            'End If

            DsFill = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                 User._Object._Userunkid, _
                                                 FinancialYear._Object._YearUnkid, _
                                                 Company._Object._Companyunkid, _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                 ConfigParameter._Object._UserAccessModeSetting, _
                                                 True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END


            cboEmployee.DisplayMember = "employeename"
            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DataSource = DsFill.Tables("Employee")


            'FOR RELATION
            DsFill = Nothing
            Dim objRelation As New clsCommon_Master
            DsFill = objRelation.getComboList(clsCommon_Master.enCommonMaster.RELATIONS, True, "Relation")
            cboRelation.DisplayMember = "name"
            cboRelation.ValueMember = "masterunkid"
            cboRelation.DataSource = DsFill.Tables(0)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            btnSave.Enabled = User._Object.Privilege._AllowToAddMedicalDependantException
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

    Private Sub SetGridColor()
        Try
            Dim dgvcsHeader As New DataGridViewCellStyle
            dgvcsHeader.ForeColor = Color.White
            dgvcsHeader.SelectionBackColor = Color.Gray
            dgvcsHeader.SelectionForeColor = Color.White
            dgvcsHeader.BackColor = Color.Gray

            Dim dgvcsChild As New DataGridViewCellStyle
            dgvcsChild.ForeColor = Color.Black
            dgvcsChild.SelectionForeColor = Color.Black
            dgvcsChild.SelectionBackColor = Color.White
            dgvcsChild.BackColor = Color.White


            For i As Integer = 0 To dgDependants.RowCount - 1
                If CBool(dgDependants.Rows(i).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    dgDependants.Rows(i).DefaultCellStyle = dgvcsHeader
                Else
                    dgDependants.Rows(i).DefaultCellStyle = dgvcsChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetGridColor", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCollapseValue()
        Try
            Dim objdgvsCollapseHeader As New DataGridViewCellStyle
            objdgvsCollapseHeader.Font = New Font(dgDependants.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseHeader.ForeColor = Color.White
            objdgvsCollapseHeader.BackColor = Color.Gray
            objdgvsCollapseHeader.SelectionBackColor = Color.Gray
            objdgvsCollapseHeader.Alignment = DataGridViewContentAlignment.MiddleCenter

            Dim objdgvsCollapseChild As New DataGridViewCellStyle
            objdgvsCollapseChild.Font = New Font(dgDependants.Font.FontFamily, 12, FontStyle.Bold)
            objdgvsCollapseChild.ForeColor = Color.Black
            objdgvsCollapseChild.SelectionBackColor = Color.White
            objdgvsCollapseChild.SelectionBackColor = Color.White
            objdgvsCollapseChild.Alignment = DataGridViewContentAlignment.MiddleCenter


            For i As Integer = 0 To dgDependants.RowCount - 1
                If CBool(dgDependants.Rows(i).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    If dgDependants.Rows(i).Cells(objdgcolhCollaps.Index).Value Is Nothing Then
                        dgDependants.Rows(i).Cells(objdgcolhCollaps.Index).Value = "-"
                    End If
                    dgDependants.Rows(i).Cells(objdgcolhCollaps.Index).Style = objdgvsCollapseHeader
                Else
                    dgDependants.Rows(i).Cells(objdgcolhCollaps.Index).Value = ""
                    dgDependants.Rows(i).Cells(objdgcolhCollaps.Index).Style = objdgvsCollapseChild
                End If
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCollapseValue", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub SetCheckBoxValue()
        Try
            Dim drRow As DataRow() = mdtTable.Select("ischecked = true")

            RemoveHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If drRow.Length <= 0 Then
                chkSelectAll.CheckState = CheckState.Unchecked
            ElseIf drRow.Length < dgDependants.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Indeterminate
            ElseIf drRow.Length = dgDependants.Rows.Count Then
                chkSelectAll.CheckState = CheckState.Checked
            End If

            AddHandler chkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCheckBoxValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetParentChecked(ByVal intRowindex As Integer, ByVal intEmployeeId As Integer)
        Try
            Dim drRow() As DataRow = mdtTable.Select("employeeunkid = " & intEmployeeId)
            Dim drCheckRow() As DataRow = mdtTable.Select("employeeunkid = " & intEmployeeId & " AND dependantunkid > 0 AND ischecked = true")

            If drRow.Length - 1 = drCheckRow.Length Then
                drRow(0)("ischecked") = CheckState.Checked
            Else
                drRow(0)("ischecked") = CheckState.Unchecked
            End If
            SetCheckBoxValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetParentChecked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmDependantExceptionList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objException = New clsdependant_exception
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            Call SetVisibility()
            FillCombo()
            mdtTable = objException._dtDependant.Clone
            If dgDependants.RowCount > 0 Then
                chkSelectAll.Enabled = True
            Else
                chkSelectAll.Checked = False
                chkSelectAll.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDependantExceptionList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDependantExceptionList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmDependantExceptionList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmDependantExceptionList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objException = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsdependant_exception.SetMessages()
            objfrm._Other_ModuleNames = "clsdependant_exception"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If Validation() = False Then Exit Sub

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            If mdtTable IsNot Nothing Then
                Dim mblnFlag As Boolean = False
                Dim drRow() As DataRow = mdtTable.Select("ischecked = True AND isgroup =False")
                If drRow.Length > 0 Then
                    For i As Integer = 0 To drRow.Length - 1
                        If CBool(drRow(i)("IsMedical")) = False AndAlso CBool(drRow(i)("IsLeave")) = False Then
                            Dim mintIndex As Integer = mdtTable.Rows.IndexOf(drRow(i))
                            dgDependants.Rows(mintIndex).Cells(dgcolhParticulars.Index).Style.ForeColor = Color.Red
                            dgDependants.Rows(mintIndex).Cells(dgcolhRelation.Index).Style.ForeColor = Color.Red
                            mblnFlag = True
                        End If
                    Next
                End If

                If mblnFlag Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please assign a module to checked dependent(s) highlighted in red color."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Exit Sub
                End If
            End If

            'Pinkal (21-Jul-2014) -- End


            objException._Userunkid = User._Object._Userunkid
            objException._dtDependant = mdtTable

            blnFlag = objException.Insert()

            If blnFlag = False And objException._Message <> "" Then
                eZeeMsgBox.Show(objException._Message)
            Else
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Medical dependant exception saved successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                mdtTable.Rows.Clear()
                cboEmployee.SelectedIndex = 0
                cboDependents.SelectedIndex = 0
                cboRelation.SelectedIndex = 0
                mstrAdvanceFilter = ""
                If dgDependants.RowCount > 0 Then
                    chkSelectAll.Enabled = True
                Else
                    chkSelectAll.Checked = False
                    chkSelectAll.Enabled = False
                End If

                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                If dgDependants.Controls.Contains(dtMedicalDate) Then
                    dgDependants.Controls.Remove(dtMedicalDate)
                    dtMedicalDate.Dispose()
                End If
                'Pinkal (21-Jul-2014) -- End

                cboEmployee.Select()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            objfrm.DataSource = CType(cboEmployee.DataSource, DataTable)
            objfrm.ValueMember = "employeeunkid"
            objfrm.DisplayMember = "employeename"
            objfrm.CodeMember = "employeecode"
            If objfrm.DisplayDialog Then
                cboEmployee.SelectedValue = objfrm.SelectedValue
            End If
            cboEmployee.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchDependants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchDependants.Click
        Dim objfrm As New frmCommonSearch
        Try
            objfrm.DataSource = CType(cboDependents.DataSource, DataTable)
            objfrm.ValueMember = cboDependents.ValueMember
            objfrm.DisplayMember = cboDependents.DisplayMember
            If objfrm.DisplayDialog Then
                cboDependents.SelectedValue = objfrm.SelectedValue
            End If
            cboDependents.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchDependants_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchRelation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchRelation.Click
        Dim objfrm As New frmCommonSearch
        Try
            objfrm.DataSource = CType(cboRelation.DataSource, DataTable)
            objfrm.ValueMember = cboRelation.ValueMember
            objfrm.DisplayMember = cboRelation.DisplayMember
            If objfrm.DisplayDialog Then
                cboRelation.SelectedValue = objfrm.SelectedValue
            End If
            cboRelation.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchRelation_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            If Validation() Then
                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                If dgDependants.Controls.Contains(dtMedicalDate) Then
                    dtMedicalDate.Visible = False
                    dgDependants.Controls.Remove(dtMedicalDate)
                End If
                If dgDependants.Controls.Contains(dtLeaveDate) Then
                    dtLeaveDate.Visible = False
                    dgDependants.Controls.Remove(dtLeaveDate)
                End If

                Me.Cursor = Cursors.WaitCursor
                FillList()
                Me.Cursor = Cursors.Default
                'Pinkal (21-Jul-2014) -- End
            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
            If dgDependants.Controls.Contains(dtMedicalDate) Then
                dtMedicalDate.Visible = False
                dgDependants.Controls.Remove(dtMedicalDate)
            End If

            If dgDependants.Controls.Contains(dtLeaveDate) Then
                dtLeaveDate.Visible = False
                dgDependants.Controls.Remove(dtLeaveDate)
            End If
            'Pinkal (21-Jul-2014) -- End

            cboEmployee.SelectedIndex = 0
            cboDependents.SelectedIndex = 0
            cboRelation.SelectedIndex = 0
            chkSelectAll.Checked = False
            mstrAdvanceFilter = ""
            mdtTable.Rows.Clear()
            If dgDependants.RowCount > 0 Then
                chkSelectAll.Enabled = True
            Else
                chkSelectAll.Checked = False
                chkSelectAll.Enabled = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboEmployee_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEmployee.SelectedIndexChanged
        Dim dsFill As DataSet = Nothing
        Try
            'FOR DEPENDENTS
            Dim objDependant As New clsDependants_Beneficiary_tran
            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsList As DataSet = objDependant.GetList("Dependant", "", "", "", CInt(cboEmployee.SelectedValue))
            Dim dsList As DataSet = objDependant.GetList(FinancialYear._Object._DatabaseName, _
                                                         User._Object._Userunkid, _
                                                         FinancialYear._Object._YearUnkid, _
                                                         Company._Object._Companyunkid, _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                         eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                         ConfigParameter._Object._UserAccessModeSetting, True, _
                                                         ConfigParameter._Object._IsIncludeInactiveEmp, "Dependant", , CInt(cboEmployee.SelectedValue), , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Sohail (18 May 2019) - [eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)]
            'S.SANDEEP [04 JUN 2015] -- END

            cboDependents.ValueMember = "DpndtTranId"
            cboDependents.DisplayMember = "DpndtBefName"

            Dim drRow As DataRow = dsList.Tables(0).NewRow
            drRow("DpndtTranId") = 0
            drRow("DpndtBefName") = Language.getMessage(mstrModuleName, 1, "Select")
            dsList.Tables(0).Rows.InsertAt(drRow, 0)

            cboDependents.DataSource = dsList.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboEmployee_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "LinkLabel Event"

    Private Sub lnkAllocation_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAllocation.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAllocation_LinkClicked", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "DataGrid Event"

    Private Sub dgDependants_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgDependants.CellContentClick, dgDependants.CellContentDoubleClick
        Try
            Dim i As Integer
            If e.RowIndex = -1 Then Exit Sub

            If Me.dgDependants.IsCurrentCellDirty Then
                Me.dgDependants.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            If CBool(dgDependants.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True Then
                Select Case CInt(e.ColumnIndex)
                    Case objdgcolhCollaps.Index
                        If dgDependants.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value.ToString = "-" Then
                            dgDependants.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "+"
                        Else
                            dgDependants.Rows(e.RowIndex).Cells(objdgcolhCollaps.Index).Value = "-"
                        End If

                        For i = e.RowIndex + 1 To dgDependants.RowCount - 1
                            If CInt(dgDependants.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value) = CInt(dgDependants.Rows(i).Cells(objdgcolhEmpId.Index).Value) Then
                                If dgDependants.Rows(i).Visible = False Then
                                    dgDependants.Rows(i).Visible = True
                                Else
                                    dgDependants.Rows(i).Visible = False
                                End If
                            Else
                                Exit For
                            End If
                        Next

                    Case objcolhIschecked.Index

                        Dim blnFlg As Boolean = CBool(dgDependants.Rows(e.RowIndex).Cells(objcolhIschecked.Index).Value)
                        Dim drRow() As DataRow = mdtTable.Select("Employeeunkid = " & CInt(dgDependants.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value))

                        If drRow.Length > 0 Then
                            RemoveHandler dgDependants.DataBindingComplete, AddressOf dgDependants_DataBindingComplete

                            For k As Integer = 1 To drRow.Length - 1
                                dgDependants.Rows(e.RowIndex + k).Cells(objcolhIschecked.Index).Value = blnFlg
                                mdtTable.Rows(e.RowIndex + k).AcceptChanges()
                            Next
                            AddHandler dgDependants.DataBindingComplete, AddressOf dgDependants_DataBindingComplete

                            SetCheckBoxValue()

                        End If

                    Case objcolhIsMedical.Index

                        Dim blnFlg As Boolean = CBool(dgDependants.Rows(e.RowIndex).Cells(objcolhIsMedical.Index).Value)
                        Dim drRow() As DataRow = mdtTable.Select("Employeeunkid = " & CInt(dgDependants.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value))

                        If drRow.Length > 0 Then
                            For k As Integer = 1 To drRow.Length - 1
                                dgDependants.Rows(e.RowIndex + k).Cells(objcolhIsMedical.Index).Value = blnFlg
                                If blnFlg = False Then dgDependants.Rows(e.RowIndex + k).Cells(dgcolhMdStopdate.Index).Value = DBNull.Value
                                mdtTable.Rows(e.RowIndex + k).AcceptChanges()
                            Next
                        End If

                    Case objcolhIsLeave.Index

                        Dim blnFlg As Boolean = CBool(dgDependants.Rows(e.RowIndex).Cells(objcolhIsLeave.Index).Value)
                        Dim drRow() As DataRow = mdtTable.Select("Employeeunkid = " & CInt(dgDependants.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value))

                        If drRow.Length > 0 Then
                            For k As Integer = 1 To drRow.Length - 1
                                dgDependants.Rows(e.RowIndex + k).Cells(objcolhIsLeave.Index).Value = blnFlg
                                If blnFlg = False Then dgDependants.Rows(e.RowIndex + k).Cells(dgcolhLvStopdate.Index).Value = DBNull.Value
                                mdtTable.Rows(e.RowIndex + k).AcceptChanges()
                            Next
                        End If

                End Select



                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16
                If dtMedicalDate IsNot Nothing AndAlso dtMedicalDate.Visible Then dtMedicalDate.Visible = False
                If dtLeaveDate IsNot Nothing AndAlso dtLeaveDate.Visible Then dtLeaveDate.Visible = False
                'Pinkal (21-Jul-2014) -- End


            ElseIf e.ColumnIndex = objcolhIschecked.Index Then
                mdtTable.Rows(e.RowIndex).AcceptChanges()
                SetParentChecked(e.RowIndex, CInt(dgDependants.Rows(e.RowIndex).Cells(objdgcolhEmpId.Index).Value))


                'Pinkal (21-Jul-2014) -- Start
                'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            ElseIf e.ColumnIndex = objcolhIsMedical.Index Then

                If CBool(dgDependants.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                    dgDependants.Rows(e.RowIndex).Cells(dgcolhMdStopdate.Index).Value = DBNull.Value
                End If

            ElseIf e.ColumnIndex = objcolhIsLeave.Index Then

                If CBool(dgDependants.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                    dgDependants.Rows(e.RowIndex).Cells(dgcolhLvStopdate.Index).Value = DBNull.Value
                End If

            ElseIf e.ColumnIndex = objdgcolhMdImage.Index AndAlso CBool(dgDependants.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = False Then

                If dgDependants.Contains(dtMedicalDate) Then
                    dgDependants.Controls.Remove(dtMedicalDate)
                End If

                If CBool(dgDependants.CurrentRow.Cells(objcolhIsMedical.Index).Value) = False Then Exit Sub

                dtMedicalDate = New MonthCalendar
                dtMedicalDate.MaxSelectionCount = 1
                dtMedicalDate.MinDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtMedicalDate.Visible = False

                If IsDBNull(dgDependants.CurrentRow.Cells(dgcolhMdStopdate.Index).Value) = False Then
                    dtMedicalDate.SetDate(CDate(dgDependants.CurrentRow.Cells(dgcolhMdStopdate.Index).Value))
                End If

                dtMedicalDate.Size = New Size(100, 100)

                Dim Location As Point = dgDependants.GetCellDisplayRectangle(dgcolhMdStopdate.Index, e.RowIndex, False).Location

                If (dgDependants.Bottom - 100) > Location.Y AndAlso Location.Y > 200 Then
                    Location = (New Point(Location.X - 55, Location.Y + 10 - 150))
                Else
                    Location = (New Point(Location.X - 55, Location.Y + 10))
                End If

                dtMedicalDate.Location = Location
                Me.dgDependants.Controls.Add(dtMedicalDate)
                dtMedicalDate.Visible = True
                dtMedicalDate.Select()

                AddHandler dtMedicalDate.DateSelected, AddressOf dtMedicalDate_DateSelected
                AddHandler dtMedicalDate.KeyDown, AddressOf dtMedicalDate_KeyDown

            ElseIf e.ColumnIndex = objdgcolhLvImage.Index AndAlso CBool(dgDependants.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = False Then

                If dgDependants.Contains(dtLeaveDate) Then
                    dgDependants.Controls.Remove(dtLeaveDate)
                End If

                If CBool(dgDependants.CurrentRow.Cells(objcolhIsLeave.Index).Value) = False Then Exit Sub

                dtLeaveDate = New MonthCalendar
                dtLeaveDate.MaxSelectionCount = 1
                dtLeaveDate.MinDate = ConfigParameter._Object._CurrentDateAndTime.Date
                dtLeaveDate.Visible = False

                If IsDBNull(dgDependants.CurrentRow.Cells(dgcolhLvStopdate.Index).Value) = False Then
                    dtLeaveDate.SetDate(CDate(dgDependants.CurrentRow.Cells(dgcolhLvStopdate.Index).Value))
                End If

                dtLeaveDate.Size = New Size(100, 100)

                Dim Location As Point = dgDependants.GetCellDisplayRectangle(dgcolhLvStopdate.Index, e.RowIndex, False).Location

                If (dgDependants.Bottom - 100) > Location.Y AndAlso Location.Y > 200 Then
                    Location = (New Point(Location.X - 55, Location.Y + 10 - 150))
                Else
                    Location = (New Point(Location.X - 55, Location.Y + 10))
                End If

                dtLeaveDate.Location = Location
                Me.dgDependants.Controls.Add(dtLeaveDate)
                dtLeaveDate.Visible = True
                dtLeaveDate.Select()

                AddHandler dtLeaveDate.DateSelected, AddressOf dtLeaveDate_DateSelected
                AddHandler dtLeaveDate.KeyDown, AddressOf dtLeaveDate_KeyDown

                'Pinkal (21-Jul-2014) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgvUserAccess_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgDependants_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgDependants.DataBindingComplete
        Try
            Call SetGridColor()
            Call SetCollapseValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgDependants_DataBindingComplete", mstrModuleName)
        Finally
        End Try
    End Sub


    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

    Private Sub dgDependants_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgDependants.Click
        Try
            If dgDependants.Controls.Contains(dtMedicalDate) AndAlso dtMedicalDate.Visible Then
                dtMedicalDate.Visible = False
            End If
            If dgDependants.Controls.Contains(dtLeaveDate) AndAlso dtLeaveDate.Visible Then
                dtLeaveDate.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgDependants_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub dgDependants_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles dgDependants.Scroll
        Try
            If dgDependants.Controls.Contains(dtMedicalDate) AndAlso dtMedicalDate.Visible Then
                dtMedicalDate.Visible = False
            End If
            If dgDependants.Controls.Contains(dtLeaveDate) AndAlso dtLeaveDate.Visible Then
                dtLeaveDate.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("", ex.Message, "dgDependants_Scroll", mstrModuleName)
        End Try
    End Sub

    Private Sub dgDependants_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgDependants.KeyDown
        Try
            If e.KeyData = Keys.Delete Then
                If dgDependants.CurrentCell.ColumnIndex = dgcolhMdStopdate.Index Then
                    dgDependants.CurrentRow.Cells(dgcolhMdStopdate.Index).Value = DBNull.Value
                    mdtTable.Rows(dgDependants.CurrentRow.Index)("MedicalStopdate") = DBNull.Value
                ElseIf dgDependants.CurrentCell.ColumnIndex = dgcolhLvStopdate.Index Then
                    dgDependants.CurrentRow.Cells(dgcolhLvStopdate.Index).Value = DBNull.Value
                    mdtTable.Rows(dgDependants.CurrentRow.Index)("LeaveStopdate") = DBNull.Value
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgDependants_KeyDown", mstrModuleName)
        End Try
    End Sub


    'Pinkal (21-Jul-2014) -- End

#End Region

#Region "Checkbox Event"

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            If mdtTable.Rows.Count > 0 Then
                RemoveHandler dgDependants.CellContentClick, AddressOf dgDependants_CellContentClick
                RemoveHandler dgDependants.CellContentDoubleClick, AddressOf dgDependants_CellContentClick
                RemoveHandler dgDependants.DataBindingComplete, AddressOf dgDependants_DataBindingComplete

                For Each dr As DataRow In mdtTable.Rows
                    dr("ischecked") = chkSelectAll.Checked
                    dr.EndEdit()
                Next
                mdtTable.AcceptChanges()
                Call SetGridColor()
                Call SetCollapseValue()
                AddHandler dgDependants.CellContentClick, AddressOf dgDependants_CellContentClick
                AddHandler dgDependants.CellContentDoubleClick, AddressOf dgDependants_CellContentClick
                AddHandler dgDependants.DataBindingComplete, AddressOf dgDependants_DataBindingComplete
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub


#End Region

    'Pinkal (21-Jul-2014) -- Start
    'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

#Region "Datepicker Event"

    Private Sub dtMedicalDate_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try
            If e.KeyCode = Keys.Escape Then
                dgDependants.CurrentCell = dgDependants.CurrentRow.Cells(objcolhIschecked.Index)
                dtMedicalDate.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtMedicalDate_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub dtMedicalDate_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs)
        Try
            If dgDependants.CurrentCell IsNot Nothing Then
                dgDependants.Rows(dgDependants.CurrentRow.Index).Cells(dgcolhMdStopdate.Index).Value = dtMedicalDate.SelectionStart.Date
                mdtTable.Rows(dgDependants.CurrentRow.Index)("MedicalStopdate") = dtMedicalDate.SelectionStart.Date
                mdtTable.AcceptChanges()
            End If
            dtMedicalDate.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtMedicalDate_DateSelected", mstrModuleName)
        End Try
    End Sub

    Private Sub dtLeaveDate_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        Try
            If e.KeyCode = Keys.Escape Then
                dgDependants.CurrentCell = dgDependants.CurrentRow.Cells(objcolhIschecked.Index)
                dtLeaveDate.Visible = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtLeaveDate_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub dtLeaveDate_DateSelected(ByVal sender As Object, ByVal e As System.Windows.Forms.DateRangeEventArgs)
        Try
            If dgDependants.CurrentCell IsNot Nothing Then
                dgDependants.Rows(dgDependants.CurrentRow.Index).Cells(dgcolhLvStopdate.Index).Value = dtLeaveDate.SelectionStart.Date
                mdtTable.Rows(dgDependants.CurrentRow.Index)("LeaveStopdate") = dtLeaveDate.SelectionStart.Date
                mdtTable.AcceptChanges()
            End If
            dtLeaveDate.Visible = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dtLeaveDate_DateSelected", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (21-Jul-2014) -- End






    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lbldependants.Text = Language._Object.getCaption(Me.lbldependants.Name, Me.lbldependants.Text)
            Me.LblRelation.Text = Language._Object.getCaption(Me.LblRelation.Name, Me.LblRelation.Text)
            Me.lnkAllocation.Text = Language._Object.getCaption(Me.lnkAllocation.Name, Me.lnkAllocation.Text)
            Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
            Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
            Me.DataGridViewTextBoxColumn3.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn3.Name, Me.DataGridViewTextBoxColumn3.HeaderText)
            Me.DataGridViewTextBoxColumn4.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn4.Name, Me.DataGridViewTextBoxColumn4.HeaderText)
            Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
            Me.DataGridViewTextBoxColumn5.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn5.Name, Me.DataGridViewTextBoxColumn5.HeaderText)
            Me.dgcolhParticulars.HeaderText = Language._Object.getCaption(Me.dgcolhParticulars.Name, Me.dgcolhParticulars.HeaderText)
            Me.dgcolhBirthdate.HeaderText = Language._Object.getCaption(Me.dgcolhBirthdate.Name, Me.dgcolhBirthdate.HeaderText)
            Me.dgcolhRelation.HeaderText = Language._Object.getCaption(Me.dgcolhRelation.Name, Me.dgcolhRelation.HeaderText)
            Me.dgcolhMdStopdate.HeaderText = Language._Object.getCaption(Me.dgcolhMdStopdate.Name, Me.dgcolhMdStopdate.HeaderText)
            Me.dgcolhLvStopdate.HeaderText = Language._Object.getCaption(Me.dgcolhLvStopdate.Name, Me.dgcolhLvStopdate.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Please Select Employee or Relation to do futher operation on it.")
            Language.setMessage(mstrModuleName, 3, "Medical dependant exception saved successfully.")
            Language.setMessage(mstrModuleName, 4, "Please assign a module to checked dependent(s) highlighted in red color.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

