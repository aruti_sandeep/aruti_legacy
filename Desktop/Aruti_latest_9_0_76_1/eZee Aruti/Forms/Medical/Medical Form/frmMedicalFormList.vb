﻿
Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 2

Public Class frmMedicalFormList

#Region "Private Variable"

    Private objInjuryMaster As clsmedical_injury_master
    Private ReadOnly mstrModuleName As String = "frmMedicalFormList"

#End Region

#Region "Form's Event"

    Private Sub frmMedicalFormList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objInjuryMaster = New clsmedical_injury_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)

            Call SetVisibility()

            FillEmployee()
            FillMedicalYear()
            fillList()

            'Pinkal (03-Jan-2011) -- Start

            dtpClaimFromDate.MinDate = FinancialYear._Object._Database_Start_Date.Date
            dtpClaimFromDate.MaxDate = FinancialYear._Object._Database_End_Date.Date

            'Pinkal (03-Jan-2011) -- End


            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes
            dtpClaimTodate.MinDate = FinancialYear._Object._Database_Start_Date.Date
            dtpClaimTodate.MaxDate = FinancialYear._Object._Database_End_Date.Date
            dtpClaimFromDate.Checked = False
            dtpClaimTodate.Checked = False
            'Pinkal (07-Jan-2012) -- End


            If lvInjuryList.Items.Count > 0 Then lvInjuryList.Items(0).Selected = True
            lvInjuryList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalFormList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalFormList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvInjuryList.Focused = True Then
                'Sohail (28 Jun 2011) -- Start
                'Issue : Delete event fired even if there is no delete privilege
                'btnDelete_Click(sender, e)
                Call btnDelete.PerformClick()
                'Sohail (28 Jun 2011) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalFormList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalFormList_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmMedicalFormList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmMedicalFormList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objInjuryMaster = Nothing
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsmedical_injury_master.SetMessages()
            objfrm._Other_ModuleNames = "clsmedical_injury_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region "Dropdown's Event"

    Private Sub cboYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboYear.SelectedIndexChanged
        Try
            ' FillMedicalPeriod()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboYear_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim ObjFrm As New frmMedicalForm_AddEdit
            If ObjFrm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvInjuryList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee Injury from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvInjuryList.Select()
            Exit Sub
        End If
        Dim objfrmMedicalForm_AddEdit As New frmMedicalForm_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvInjuryList.SelectedItems(0).Index
            If objfrmMedicalForm_AddEdit.displayDialog(CInt(lvInjuryList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            objfrmMedicalForm_AddEdit = Nothing
            If lvInjuryList.Items.Count > 0 Then
                lvInjuryList.Items(intSelectedIndex).Selected = True
                lvInjuryList.EnsureVisible(intSelectedIndex)
            End If
            lvInjuryList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If objfrmMedicalForm_AddEdit IsNot Nothing Then objfrmMedicalForm_AddEdit.Dispose()
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvInjuryList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Employee Injury from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvInjuryList.Select()
            Exit Sub
        End If
        'If objInjuryMaster.isUsed(CInt(lvInjuryList.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Employee Injury. Reason: This Employee Injury is in use."), enMsgBoxStyle.Information) '?2
        '    lvInjuryList.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvInjuryList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Employee Injury ?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objInjuryMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
                objInjuryMaster._Voiduserunkid = User._Object._Userunkid
                objInjuryMaster.Delete(CInt(lvInjuryList.SelectedItems(0).Tag))
                lvInjuryList.SelectedItems(0).Remove()

                If lvInjuryList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvInjuryList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvInjuryList.Items.Count - 1
                    lvInjuryList.Items(intSelectedIndex).Selected = True
                    lvInjuryList.EnsureVisible(intSelectedIndex)
                ElseIf lvInjuryList.Items.Count <> 0 Then
                    lvInjuryList.Items(intSelectedIndex).Selected = True
                    lvInjuryList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvInjuryList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Try
            Dim frm As New frmCommonSearch
            frm.DataSource = CType(cboEmployee.DataSource, DataTable)
            frm.DisplayMember = cboEmployee.DisplayMember
            frm.ValueMember = cboEmployee.ValueMember
            frm.CodeMember = "employeecode"
            If frm.DisplayDialog() Then
                cboEmployee.SelectedValue = frm.SelectedValue
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedIndex = 0
            cboYear.SelectedIndex = 0
            'cboPeriod.SelectedIndex = 0
            txtClaimNo.Text = ""
            txtServiceNo.Text = ""

            'Pinkal (03-Jan-2011) -- Start

            If FinancialYear._Object._Database_End_Date.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpClaimFromDate.Value = FinancialYear._Object._Database_End_Date.Date
            ElseIf FinancialYear._Object._Database_End_Date.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpClaimFromDate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            End If

            'Pinkal (03-Jan-2011) -- End
            dtpClaimFromDate.Checked = False


            'Pinkal (07-Jan-2012) -- Start
            'Enhancement : TRA Changes

            If FinancialYear._Object._Database_End_Date.Date < ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpClaimTodate.Value = FinancialYear._Object._Database_End_Date.Date
            ElseIf FinancialYear._Object._Database_End_Date.Date > ConfigParameter._Object._CurrentDateAndTime.Date Then
                dtpClaimTodate.Value = ConfigParameter._Object._CurrentDateAndTime.Date
            End If

            dtpClaimTodate.Checked = False
            'Pinkal (07-Jan-2012) -- End

            txtRemark.Text = ""
            fillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub FillMedicalYear()
        Try

            'Sandeep [ 21 Aug 2010 ] -- Start
            'Dim dsYear As DataSet = objInjuryMaster.getListForYearCombo("Year", True)
            'cboYear.DisplayMember = "name"
            'cboYear.ValueMember = "yearunkid"
            Dim objMaster As New clsMasterData

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim dsYear As DataSet = objMaster.getComboListPAYYEAR("Year", True)
            Dim dsYear As DataSet = objMaster.getComboListPAYYEAR(FinancialYear._Object._YearUnkid, FinancialYear._Object._FinancialYear_Name, Company._Object._Companyunkid, "Year", True)
            'S.SANDEEP [04 JUN 2015] -- END

            cboYear.DisplayMember = "name"
            cboYear.ValueMember = "Id"
            'Sandeep [ 21 Aug 2010 ] -- End 
            cboYear.DataSource = dsYear.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillMedicalYear", mstrModuleName)
        End Try
    End Sub

    'Private Sub FillMedicalPeriod()
    '    Try
    '        Dim objPeriod As New clscommom_period_Tran
    '        Dim dsPeriod As DataSet = Nothing
    '        If CInt(cboYear.SelectedValue) > 0 Then
    '            dsPeriod = objPeriod.getListForCombo(CInt(enModuleRefenence.Medical), CInt(cboYear.SelectedValue), "Period", False)
    '        Else
    '            dsPeriod = objPeriod.getListForCombo(CInt(enModuleRefenence.Medical), CInt(cboYear.SelectedValue), "Period", True)
    '        End If
    '        cboPeriod.ValueMember = "periodunkid"
    '        cboPeriod.DisplayMember = "name"
    '        cboPeriod.DataSource = dsPeriod.Tables(0)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillMedicalPeriod", mstrModuleName)
    '    End Try
    'End Sub


    Private Sub FillEmployee()
        Try
            Dim objEmployee As New clsEmployee_Master


            'Pinkal (24-Jun-2011) -- Start
            'ISSUE : CHECK FOR ACTIVE EMPLOYEE
            'Dim dsEmployee As DataSet = objEmployee.GetEmployeeList("Employee", True, True)
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim dsEmployee As DataSet = objEmployee.GetEmployeeList("Employee", True, Not ConfigParameter._Object._IsIncludeInactiveEmp)
            Dim dsEmployee As DataSet

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsEmployee = objEmployee.GetEmployeeList("Employee", True, , , , , , , , , , , , , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
            'Else
            '    dsEmployee = objEmployee.GetEmployeeList("Employee", True)
            'End If
            dsEmployee = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                     User._Object._Userunkid, _
                                                     FinancialYear._Object._YearUnkid, _
                                                     Company._Object._Companyunkid, _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                     ConfigParameter._Object._UserAccessModeSetting, _
                                                     True, ConfigParameter._Object._IsIncludeInactiveEmp, "Employee", True)
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (06 Jan 2012) -- End
            'Pinkal (24-Jun-2011) -- End

            cboEmployee.ValueMember = "employeeunkid"
            cboEmployee.DisplayMember = "employeename"
            cboEmployee.DataSource = dsEmployee.Tables(0)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub fillList()
        Dim dsMedicalInjury As New DataSet
        Dim strSearching As String = ""
        Dim dtMedicalInjury As New DataTable
        Try

            If User._Object.Privilege._AllowToViewEmpInjuryList = True Then   'Pinkal (02-Jul-2012) -- Start

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsMedicalInjury = objInjuryMaster.GetList("Medical Injury")
                dsMedicalInjury = objInjuryMaster.GetList(FinancialYear._Object._DatabaseName, _
                                                          User._Object._Userunkid, _
                                                          FinancialYear._Object._YearUnkid, _
                                                          Company._Object._Companyunkid, _
                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                          ConfigParameter._Object._UserAccessModeSetting, _
                                                          True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                          "Medical Injury")
                'Shani(24-Aug-2015) -- End

                If CInt(cboEmployee.SelectedValue) > 0 Then
                    strSearching = "AND employeeunkid =" & CInt(cboEmployee.SelectedValue) & " "
                End If

                If CInt(cboYear.SelectedValue) > 0 Then
                    strSearching &= "AND yearunkid =" & CInt(cboYear.SelectedValue) & " "
                End If

                'If CInt(cboPeriod.SelectedValue) > 0 Then
                '    strSearching &= "AND  periodunkid=" & CInt(cboPeriod.SelectedValue) & " "
                'End If

                If txtClaimNo.Text.Trim.Length > 0 Then
                    strSearching &= "AND claim_no like '%" & txtClaimNo.Text.Trim & "%' "
                End If

                If txtServiceNo.Text.Trim.Length > 0 Then
                    strSearching &= "AND serviceno like '%" & txtServiceNo.Text.Trim & "%' "
                End If

                If txtRemark.Text.Trim.Length > 0 Then
                    strSearching &= "AND remark like '%" & txtRemark.Text.Trim & "%' "
                End If

                If dtpClaimFromDate.Checked Then
                    strSearching &= "AND claim_date >= '" & eZeeDate.convertDate(dtpClaimFromDate.Value) & "' "
                End If


                'Pinkal (07-Jan-2012) -- Start
                'Enhancement : TRA Changes

                If dtpClaimTodate.Checked Then
                    strSearching &= "AND claim_date <='" & eZeeDate.convertDate(dtpClaimTodate.Value) & "' "
                End If

                'Pinkal (07-Jan-2012) -- End


                If strSearching.Length > 0 Then
                    strSearching = strSearching.Substring(3)
                    dtMedicalInjury = New DataView(dsMedicalInjury.Tables("Medical Injury"), strSearching, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtMedicalInjury = dsMedicalInjury.Tables("Medical Injury")
                End If


                Dim lvItem As ListViewItem

                lvInjuryList.Items.Clear()
                For Each drRow As DataRow In dtMedicalInjury.Rows
                    lvItem = New ListViewItem
                    lvItem.Text = drRow("employee").ToString
                    lvItem.Tag = drRow("injuryunkid")
                    lvItem.SubItems.Add(drRow("year").ToString)
                    lvItem.SubItems.Add(drRow("claim_no").ToString)
                    lvItem.SubItems.Add(eZeeDate.convertDate(drRow("claim_date").ToString).ToShortDateString)
                    lvItem.SubItems.Add(drRow("serviceno").ToString)
                    lvItem.SubItems.Add(drRow("remark").ToString)
                    lvInjuryList.Items.Add(lvItem)
                Next

                If lvInjuryList.Items.Count > 13 Then
                    colhRemark.Width = 215 - 18
                Else
                    colhRemark.Width = 215
                End If

            End If


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsMedicalInjury.Dispose()
        End Try
    End Sub

    Private Sub SetVisibility()

        Try

            btnNew.Enabled = User._Object.Privilege._AddMedicalInjuries
            btnEdit.Enabled = User._Object.Privilege._EditMedicalInjuries
            btnDelete.Enabled = User._Object.Privilege._DeleteMedicalInjuries

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor


           
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.lblYears.Text = Language._Object.getCaption(Me.lblYears.Name, Me.lblYears.Text)
            Me.lblServiceNo.Text = Language._Object.getCaption(Me.lblServiceNo.Name, Me.lblServiceNo.Text)
            Me.lblRemark.Text = Language._Object.getCaption(Me.lblRemark.Name, Me.lblRemark.Text)
            Me.lblClaimFromDate.Text = Language._Object.getCaption(Me.lblClaimFromDate.Name, Me.lblClaimFromDate.Text)
            Me.lblClaimNo.Text = Language._Object.getCaption(Me.lblClaimNo.Name, Me.lblClaimNo.Text)
            Me.colhEmployee.Text = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.Text)
            Me.colhYear.Text = Language._Object.getCaption(CStr(Me.colhYear.Tag), Me.colhYear.Text)
            Me.colhClaimNo.Text = Language._Object.getCaption(CStr(Me.colhClaimNo.Tag), Me.colhClaimNo.Text)
            Me.colhClaimDate.Text = Language._Object.getCaption(CStr(Me.colhClaimDate.Tag), Me.colhClaimDate.Text)
            Me.colhServiceNo.Text = Language._Object.getCaption(CStr(Me.colhServiceNo.Tag), Me.colhServiceNo.Text)
            Me.colhRemark.Text = Language._Object.getCaption(CStr(Me.colhRemark.Tag), Me.colhRemark.Text)
            Me.lblClaimTodate.Text = Language._Object.getCaption(Me.lblClaimTodate.Name, Me.lblClaimTodate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Employee Injury from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Employee Injury ?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class