﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmAction_ReasonList

#Region " Private Varaibles "
    Private objAction_Reason As clsAction_Reason
    Private ReadOnly mstrModuleName As String = "frmAction_ReasonList"
    Private mblnIsAction As Boolean = False
#End Region

#Region " Property "
    Public WriteOnly Property _IsAction() As Boolean
        Set(ByVal value As Boolean)
            mblnIsAction = value
        End Set
    End Property
#End Region

#Region " Private Function "

    Private Sub fillList()
        Dim dsAccess As New DataSet
        Try


            'Pinkal (09-Jul-2012) -- Start
            'Enhancement : TRA Changes

            If User._Object.Privilege._AllowToViewReasonMasterList = False AndAlso mblnIsAction = False Then Exit Sub

            If User._Object.Privilege._AllowToViewDisciplinaryPenaltiesList = False AndAlso mblnIsAction = True Then Exit Sub

            'Pinkal (09-Jul-2012) -- End



            dsAccess = objAction_Reason.GetList("Action_Reason", mblnIsAction)


            Dim lvItem As ListViewItem

            lvAction_Reason.Items.Clear()
            For Each drRow As DataRow In dsAccess.Tables(0).Rows
                lvItem = New ListViewItem
                If CBool(drRow.Item("isreason")) = False Then
                    If CInt(drRow("actionreasonunkid")) <= 2 Then
                        lvItem.ForeColor = Color.Gray
                    End If
                End If
                lvItem.Text = drRow("code").ToString
                lvItem.Tag = drRow("actionreasonunkid")
                lvItem.SubItems.Add(drRow("reason_action").ToString)
                lvAction_Reason.Items.Add(lvItem)
            Next

            If lvAction_Reason.Items.Count > 16 Then
                colhAction.Width = 555 - 18
            Else
                colhAction.Width = 555
            End If

            'Call setAlternateColor(lvAirline)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsAccess.Dispose()
        End Try
    End Sub

    Private Sub SetCaptions()
        Try
            If mblnIsAction = False Then
                Me.Text = Language.getMessage(mstrModuleName, 4, "Reason List")
                eZeeHeader.Title = Language.getMessage(mstrModuleName, 4, "Reason List")
                colhAction.Text = Language.getMessage(mstrModuleName, 5, "Reason")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetCaptions", mstrModuleName)
        End Try
    End Sub


    Private Sub SetVisibility()

        Try
            If mblnIsAction = True Then
                btnNew.Enabled = User._Object.Privilege._AddDisciplinaryAction
                btnEdit.Enabled = User._Object.Privilege._EditDisciplinaryAction
                btnDelete.Enabled = User._Object.Privilege._DeleteDisciplinaryAction
            Else
                btnNew.Enabled = User._Object.Privilege._AddTerminationReason
                btnEdit.Enabled = User._Object.Privilege._EditTerminationReason
                btnDelete.Enabled = User._Object.Privilege._DeleteTerminationReason
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub



    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "

    Private Sub frmAction_ReasonList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvAction_Reason.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAction_ReasonList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAction_ReasonList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAction_ReasonList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAction_ReasonList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAction_Reason = New clsAction_Reason
        Try

            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes
            If mblnIsAction Then
                Me.Name = "frmDisciplinaryActionList"
                Me.Text = Language.getMessage(mstrModuleName, 7, "Disciplinary Penalties List")
            Else
                Me.Name = "frmTerminationReasonList"
                Me.Text = Language.getMessage(mstrModuleName, 8, "Reason List")
            End If
            'Pinkal (24-Jul-2012) -- End

            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            Call SetVisibility()

            Call SetCaptions()

            Call fillList()

            If lvAction_Reason.Items.Count > 0 Then lvAction_Reason.Items(0).Selected = True
            lvAction_Reason.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAction_ReasonList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAction_ReasonList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objAction_Reason = Nothing
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAction_Reason.SetMessages()
            objfrm._Other_ModuleNames = "clsAction_Reason"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvAction_Reason.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Action/Reason from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvAction_Reason.Select()
            Exit Sub
        End If
        If objAction_Reason.isUsed(CInt(lvAction_Reason.SelectedItems(0).Tag)) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Action/Reason. Reason: This Action/Reason is in use."), enMsgBoxStyle.Information) '?2
            lvAction_Reason.Select()
            Exit Sub
        End If

        If lvAction_Reason.SelectedItems(0).ForeColor = Color.Gray Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, you cannot delete predefined action."), enMsgBoxStyle.Information)
            lvAction_Reason.Select()
            Exit Sub
        End If

        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAction_Reason.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to delete this Action/Reason?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objAction_Reason.Delete(CInt(lvAction_Reason.SelectedItems(0).Tag))
                lvAction_Reason.SelectedItems(0).Remove()

                If lvAction_Reason.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvAction_Reason.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvAction_Reason.Items.Count - 1
                    lvAction_Reason.Items(intSelectedIndex).Selected = True
                    lvAction_Reason.EnsureVisible(intSelectedIndex)
                ElseIf lvAction_Reason.Items.Count <> 0 Then
                    lvAction_Reason.Items(intSelectedIndex).Selected = True
                    lvAction_Reason.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvAction_Reason.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvAction_Reason.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Action/Reason from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvAction_Reason.Select()
            Exit Sub
        End If
        Dim frm As New frmAction_Reason_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvAction_Reason.SelectedItems(0).Index

            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END


            'Pinkal (24-Jul-2012) -- Start
            'Enhancement : TRA Changes

            If frm.displayDialog(CInt(lvAction_Reason.SelectedItems(0).Tag), enAction.EDIT_ONE, mblnIsAction) Then
                Call fillList()
            End If

            'Pinkal (24-Jul-2012) -- End

            frm = Nothing

            lvAction_Reason.Items(intSelectedIndex).Selected = True
            lvAction_Reason.EnsureVisible(intSelectedIndex)
            lvAction_Reason.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmAction_Reason_AddEdit
        Try
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'S.SANDEEP [ 20 AUG 2011 ] -- END
            If frm.displayDialog(-1, enAction.ADD_CONTINUE, mblnIsAction) Then
                Call fillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()

            Call SetLanguage()
		
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
			Me.colhAction.Text = Language._Object.getCaption(CStr(Me.colhAction.Tag), Me.colhAction.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select Action/Reason from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Action/Reason. Reason: This Action/Reason is in use.")
			Language.setMessage(mstrModuleName, 3, "Are you sure you want to delete this Action/Reason?")
			Language.setMessage(mstrModuleName, 4, "Reason List")
			Language.setMessage(mstrModuleName, 5, "Reason")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot delete predefined action.")
			Language.setMessage(mstrModuleName, 7, "Disciplinary Penalties List")
			Language.setMessage(mstrModuleName, 8, "Reason List")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class