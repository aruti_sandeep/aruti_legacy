﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmAction_Reason_AddEdit

#Region " Private Variables "
    Private mstrModuleName As String = ""
    Private mblnCancel As Boolean = True
    Private objAction_Reason As clsAction_Reason
    Private menAction As enAction = enAction.ADD_ONE
    Private mintActionReasonUnkid As Integer = -1
    Private mblnIsAction_Reason As Boolean = False
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, Optional ByVal isAction As Boolean = False) As Boolean
        Try
            mintActionReasonUnkid = intUnkId
            menAction = eAction
            mblnIsAction_Reason = isAction
            Select Case mblnIsAction_Reason
                Case False
                    Me.Name = "frmTerminationReasonAddEdit"
                    Me.Text = Language.getMessage(mstrModuleName, 3, "Add/Edit Termination Action")
                    gbAccessInfo.Text = Language.getMessage(mstrModuleName, 4, "Termination Action")
                Case True
                    Me.Name = "frmDisciplinaryActionAddEdit"
                    Me.Text = Language.getMessage(mstrModuleName, 5, "Add/Edit Disciplinary Penalties")
                    gbAccessInfo.Text = Language.getMessage(mstrModuleName, 6, "Disciplinary Penalties")
            End Select
            mstrModuleName = Me.Name
            Me.ShowDialog()

            intUnkId = mintActionReasonUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "
    Private Sub setColor()
        Try
            txtCode.BackColor = GUI.ColorComp
            txtDescription.BackColor = GUI.ColorComp
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        txtCode.Text = objAction_Reason._Code
        txtDescription.Text = objAction_Reason._Reason_Action
    End Sub

    Private Sub SetValue()
        objAction_Reason._Code = txtCode.Text
        objAction_Reason._Reason_Action = txtDescription.Text
        If menAction = enAction.EDIT_ONE Then
            objAction_Reason._Isreason = objAction_Reason._Isreason
        Else
            If mblnIsAction_Reason = True Then
                objAction_Reason._Isreason = False
            Else
                objAction_Reason._Isreason = True
            End If
        End If
    End Sub
#End Region

#Region " Form's Events "
    Private Sub frmAction_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objAction_Reason = Nothing
    End Sub

    Private Sub frmAction_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAction_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAction_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAction_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAction_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objAction_Reason = New clsAction_Reason
        Try
            Call Set_Logo(Me, gApplicationType)
            'S.SANDEEP [ 20 AUG 2011 ] -- START
            'ENHANCEMENT : LANGUAGES IMPLEMENTATION
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'S.SANDEEP [ 20 AUG 2011 ] -- END

            Call setColor()

            If menAction = enAction.EDIT_ONE Then
                objAction_Reason._Actionreasonunkid = mintActionReasonUnkid
            End If

            Call GetValue()

            txtCode.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAction_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 20 AUG 2011 ] -- START
    'ENHANCEMENT : LANGUAGES IMPLEMENTATION
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsAction_Reason.SetMessages()
            objfrm._Other_ModuleNames = "clsAction_Reason"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'S.SANDEEP [ 20 AUG 2011 ] -- END
#End Region

#Region " Button's Events "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try
            If Trim(txtCode.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information."), enMsgBoxStyle.Information)
                txtCode.Focus()
                Exit Sub
            End If

            If Trim(txtDescription.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Action/Reason cannot be blank. Action/Reason is required information."), enMsgBoxStyle.Information)
                txtDescription.Focus()
                Exit Sub
            End If

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objAction_Reason.Update()
            Else
                blnFlag = objAction_Reason.Insert()
            End If

            If blnFlag = False And objAction_Reason._Message <> "" Then
                eZeeMsgBox.Show(objAction_Reason._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objAction_Reason = Nothing
                    objAction_Reason = New clsAction_Reason
                    Call GetValue()
                    txtDescription.Focus()
                Else
                    mintActionReasonUnkid = objAction_Reason._Actionreasonunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try

    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbAccessInfo.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbAccessInfo.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbAccessInfo.Text = Language._Object.getCaption(Me.gbAccessInfo.Name, Me.gbAccessInfo.Text)
			Me.lblAction.Text = Language._Object.getCaption(Me.lblAction.Name, Me.lblAction.Text)
			Me.lblAccCode.Text = Language._Object.getCaption(Me.lblAccCode.Name, Me.lblAccCode.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Action/Reason cannot be blank. Action/Reason is required information.")
			Language.setMessage(mstrModuleName, 2, "Code cannot be blank. Code is required information.")
			Language.setMessage(mstrModuleName, 3, "Add/Edit Termination Action")
			Language.setMessage(mstrModuleName, 4, "Termination Action")
            Language.setMessage(mstrModuleName, 5, "Add/Edit Disciplinary Penalties")
            Language.setMessage(mstrModuleName, 6, "Disciplinary Penalties")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class