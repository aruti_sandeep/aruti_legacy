﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.clsPayment_tran

Public Class frmGlobalVoidPayment

#Region " Private Varaibles "
    Private objPaymentTran As New clsPayment_tran
    Private ReadOnly mstrModuleName As String = "frmGlobalVoidPayment"
    Private mblnCancel As Boolean = True
    Private mintTransactonId As Integer = -1
    Private mintReferenceId As Integer = -1
    Private mintPayTypeId As Integer = -1

    'Sohail (22 May 2014) -- Start
    'Enhancement - Show Progress on Global Void Payment.
    Private mintCheckedEmployee As Integer = 0
    Private mblnProcessFailed As Boolean = False
    'Sohail (22 May 2014) -- End

    'Sohail (22 May 2014) -- Start
    'Enhancement - Show Progress on Global Void Payment.
    Private mstrVoidReason As String = String.Empty
    Private strVoidIds As String = ""
    Private mstrAdvanceFilter As String = ""
    'Sohail (22 May 2014) -- End

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mdtPeriod_StartDate As Date
    Private mdtPeriod_EndDate As Date
    'Sohail (21 Aug 2015) -- End
    Private dtActAdj As New DataTable 'Sohail (23 May 2017)
    Private dtStart As Date
    'Hemant (09 Jun 2023) -- Start
    'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
    Private dvEmployee As DataView
    'Hemant (09 Jun 2023) -- End
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByVal intRefUnkid As Integer, _
                                  ByVal intPayTypeId As Integer) As Boolean
        Try
            mintReferenceId = intRefUnkid
            mintPayTypeId = intPayTypeId

            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Method & Function "
    Private Sub FillList()
        Dim dsPayment As New DataSet
        Dim dtTable As New DataTable
        Dim StrSearching As String = String.Empty
        Dim lvItem As ListViewItem
        Dim objExrate As clsExchangeRate

        Try

            'Hemant (09 Jun 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
            'lvPayment.Items.Clear() 'Sohail (02 Jan 2017)
            'Hemant (09 Jun 2023) -- End
            If User._Object.Privilege._AllowToViewGlobalVoidPaymentList = True Then  'Pinkal (02-Jul-2012) -- Start

                'Sohail (02 Jan 2017) -- Start
                'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                'lvPayment.Items.Clear() 
                Cursor.Current = Cursors.WaitCursor
                'Hemant (09 Jun 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
                'lvPayment.BeginUpdate()
                'Hemant (09 Jun 2023) -- End

                'Sohail (02 Jan 2017) -- End

                'Sohail (24 Sep 2012) -- Start
                'TRA - ENHANCEMENT
                'dsPayment = objPaymentTran.GetListByPeriod("Payment", mintReferenceId, CInt(cboPayPeriod.SelectedValue), mintPayTypeId)

                'If CInt(cboEmployee.SelectedValue) > 0 Then
                '    StrSearching &= "AND employeeunkid = " & CInt(cboEmployee.SelectedValue) & " "
                'End If
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsPayment = objPaymentTran.GetListByPeriod("Payment", mintReferenceId, CInt(cboPayPeriod.SelectedValue), mintPayTypeId, cboEmployee.SelectedValue.ToString, , mstrAdvanceFilter)
                dsPayment = objPaymentTran.GetListByPeriod(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_StartDate, mdtPeriod_EndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Payment", mintReferenceId, CInt(cboPayPeriod.SelectedValue), mintPayTypeId, cboEmployee.SelectedValue.ToString, True, mstrAdvanceFilter)
                'Sohail (21 Aug 2015) -- End
                'Sohail (24 Sep 2012) -- End

                If txtPaidAmount.Decimal <> 0 AndAlso txtPaidAmountTo.Decimal <> 0 Then
                    StrSearching &= "AND Amount >= " & txtPaidAmount.Decimal & " AND Amount <= " & txtPaidAmountTo.Decimal & " "
                End If

                'Sohail (03 Jul 2014) -- Start
                'Enhancement - Advance Filter and Voucher Combobox on Global Void Payment list.
                'If txtVoucherno.Text.Trim <> "" Then
                '    'Sohail (08 Nov 2011) -- Start
                '    'StrSearching &= "AND Voc LIKE '%" & CStr(txtVoucherno.Text) & "%'" & " " 
                '    StrSearching &= "AND voucherno LIKE '%" & CStr(txtVoucherno.Text) & "%'" & " "
                '    'Sohail (08 Nov 2011) -- End
                'End If
                If CInt(cboPmtVoucher.SelectedIndex) > 0 Then
                    StrSearching &= "AND voucherno = '" & cboPmtVoucher.Text & "'" & " "
                    'Sohail (08 Nov 2011) -- End
                End If
                'Sohail (03 Jul 2014) -- End

                If dtpPaymentDate.Checked = True Then
                    StrSearching &= "AND PaymentDate = '" & eZeeDate.convertDate(dtpPaymentDate.Value) & "'" & "  "
                End If

                If StrSearching.Length > 0 Then
                    StrSearching = StrSearching.Substring(3)
                    'Sohail (13 Dec 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'dtTable = New DataView(dsPayment.Tables("Payment"), StrSearching, "", DataViewRowState.CurrentRows).ToTable
                    dtTable = New DataView(dsPayment.Tables("Payment"), StrSearching, "voucherno, empname", DataViewRowState.CurrentRows).ToTable
                    'Sohail (13 Dec 2017) -- End
                Else
                    'Sohail (13 Dec 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'dtTable = dsPayment.Tables("Payment")
                    dtTable = New DataView(dsPayment.Tables("Payment"), "", "voucherno, empname", DataViewRowState.CurrentRows).ToTable
                    'Sohail (13 Dec 2017) -- End
                End If


                'Hemant (09 Jun 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
                'For Each dtRow As DataRow In dtTable.Rows
                '    lvItem = New ListViewItem

                '    lvItem.Text = ""
                '    lvItem.Tag = dtRow.Item("paymenttranunkid")

                '    lvItem.SubItems.Add(eZeeDate.convertDate(dtRow.Item("PaymentDate").ToString).ToShortDateString)
                '    lvItem.SubItems.Add(dtRow.Item("VoucherNo").ToString)
                '    'Sohail (28 Jan 2012) -- Start
                '    'TRA - ENHANCEMENT
                '    lvItem.SubItems.Add(dtRow.Item("employeecode").ToString)
                '    'Sohail (28 Jan 2012) -- End
                '    lvItem.SubItems(colhEmpCode.Index).Tag = CDec(dtRow.Item("roundingadjustment")) 'Sohail (21 Mar 2014)
                '    lvItem.SubItems.Add(dtRow.Item("EmpName").ToString)
                '    'Sohail (23 May 2017) -- Start
                '    'Enhancement - 67.1 - Link budget with Payroll.
                '    lvItem.SubItems(colhEmployee.Index).Tag = CInt(dtRow.Item("employeeunkid"))
                '    'Sohail (23 May 2017) -- End
                '    lvItem.SubItems.Add(dtRow.Item("PeriodName").ToString)

                '    lvItem.SubItems.Add(Format(CDec(dtRow.Item("expaidamt").ToString), GUI.fmtCurrency))
                '    objExrate = New clsExchangeRate
                '    objExrate._ExchangeRateunkid = CInt(dtRow.Item("paidcurrencyid"))
                '    lvItem.SubItems.Add(objExrate._Currency_Sign)

                '    RemoveHandler lvPayment.ItemChecked, AddressOf lvPayment_ItemChecked
                '    lvPayment.Items.Add(lvItem)
                '    AddHandler lvPayment.ItemChecked, AddressOf lvPayment_ItemChecked
                '    lvItem = Nothing
                'Next
                'Hemant (09 Jun 2023) -- End

                'Hemant (09 Jun 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
                If dtTable.Columns.Contains("IsChecked") = False Then
                    Dim dtCol As DataColumn
                    dtCol = New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                    dtCol.DefaultValue = False
                    dtCol.AllowDBNull = False
                    dtTable.Columns.Add(dtCol)
                End If

                If dtTable.Columns.Contains("IsGrp") = False Then
                    Dim dtCol As New DataColumn
                    dtCol = New DataColumn
                    dtCol.ColumnName = "IsGrp"
                    dtCol.Caption = "IsGrp"
                    dtCol.DataType = System.Type.GetType("System.Boolean")
                    dtCol.DefaultValue = False
                    dtTable.Columns.Add(dtCol)
                End If

                Dim dtVoucher As DataTable = dtTable.DefaultView.ToTable(True, "voucherno", "PaymentDate")
                For Each drVoucher As DataRow In dtVoucher.Rows
                    Dim drRow As DataRow = dtTable.NewRow
                    drRow.Item("PaymentDate") = drVoucher.Item("PaymentDate")
                    drRow.Item("VoucherNo") = drVoucher.Item("VoucherNo")
                    drRow.Item("paymenttranunkid") = -1
                    drRow.Item("IsGrp") = 1
                    dtTable.Rows.Add(drRow)
                Next

                Dim dtTemp As New DataTable
                dtTemp = New DataView(dtTable, "", "voucherno, paymenttranunkid", DataViewRowState.CurrentRows).ToTable

                dvEmployee = dtTemp.DefaultView()

                GvVoidPayment.AutoGenerateColumns = False
                objcolhCheck1.DataPropertyName = "IsChecked"
                objdgcolhIsGroup.DataPropertyName = "IsGrp"
                colhDate.DataPropertyName = "PaymentDate"
                colhVoucher.DataPropertyName = "VoucherNo"
                colhEmpCode.DataPropertyName = "employeecode"
                colhEmployee.DataPropertyName = "EmpName"
                colhPayPeriod.DataPropertyName = "PeriodName"
                colhPaidAmount.DataPropertyName = "expaidamt"
                colhPaidAmount.DefaultCellStyle.Format = GUI.fmtCurrency
                colhCurrency.DataPropertyName = "paidcurrencysign"
                objdgcolhPaymenttranunkid.DataPropertyName = "paymenttranunkid"
                objcolhEmployeeunkid.DataPropertyName = "employeeunkid"
                GvVoidPayment.DataSource = dvEmployee
                'Hemant (09 Jun 2023) -- End

                'Sohail (08 Nov 2011) -- Start

                'Hemant (09 Jun 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
                'lvPayment.GridLines = False
                'lvPayment.GroupingColumn = colhVoucher
                'lvPayment.DisplayGroups(True)
                'Hemant (09 Jun 2023) -- End                
                'Sohail (08 Nov 2011) -- End

                'Hemant (09 Jun 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
                'If colhPaidAmount1.Width > 0 Then 'Sohail (16 Oct 2019)
                '    If lvPayment.Items.Count > 4 Then
                '        colhEmployee1.Width = 170 - 18
                '    Else
                '        colhEmployee1.Width = 170
                '    End If
                'End If 'Sohail (16 Oct 2019)
                'Hemant (09 Jun 2023) -- End

                'Sohail (16 Oct 2019) -- Start
                'NMB Enhancement # : if user does not have privilege to see amount/salary, system should disable checkbox in list, he can either tick or untick all on Approval / Authorize / Payslip / Global Void Payment Screen for Automatic Integration between Payroll and Finance modules,Online Approval workflow for Payroll runs.
                If User._Object.Privilege._AllowToViewPaidAmount = False Then
                    colhEmployee.Width += colhPaidAmount.Width
                    colhPaidAmount.Width = 0
                End If
                'Sohail (16 Oct 2019) -- End

                'Sohail (22 May 2014) -- Start

                    'Hemant (09 Jun 2023) -- Start
                    'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
                    'Call objbtnSearch.ShowResult(CStr(lvPayment.Items.Count))
                    Call objbtnSearch.ShowResult(CStr(dvEmployee.Table.Select("IsGrp = 0 AND IsChecked = 1 ").Length))
                    'Hemant (09 Jun 2023) -- End
                'Sohail (22 May 2014) -- End

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
        Finally
            objExrate = Nothing
            'Sohail (18 May 2013) -- End
            'Sohail (02 Jan 2017) -- Start
            'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
            Cursor.Current = Cursors.Default

            'Hemant (09 Jun 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
            'lvPayment.EndUpdate()
            'Hemant (09 Jun 2023) -- End
            'Sohail (02 Jan 2017) -- End
        End Try
    End Sub

    Private Sub FillCombo()
        Dim objEmployee As New clsEmployee_Master
        Dim objPeriod As New clscommom_period_Tran
        Dim dsCombos As DataSet
        Try
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmployee.GetEmployeeList("Emp", True, True)
            'Sohail (23 Nov 2012) -- Start
            'TRA - ENHANCEMENT
            'dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True, , , , , , , , , , , , , ConfigParameter._Object._CurrentDateAndTime.Date, ConfigParameter._Object._CurrentDateAndTime.Date)
            'Else
            '    dsCombos = objEmployee.GetEmployeeList("Emp", True)
            'End If

            dsCombos = objEmployee.GetEmployeeList(FinancialYear._Object._DatabaseName, _
                                                   User._Object._Userunkid, _
                                                   FinancialYear._Object._YearUnkid, _
                                                   Company._Object._Companyunkid, _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                   ConfigParameter._Object._UserAccessModeSetting, _
                                                   True, ConfigParameter._Object._IsIncludeInactiveEmp, "Emp", True)
            'S.SANDEEP [04 JUN 2015] -- END


            'Sohail (23 Nov 2012) -- End
            'Sohail (06 Jan 2012) -- End
            With cboEmployee
                .ValueMember = "employeeunkid"
                .DisplayMember = "employeename"
                .DataSource = dsCombos.Tables("Emp")
                .SelectedValue = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", False, enStatusType.Open)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", False, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            With cboPayPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Period")
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
            'Sohail (18 May 2013) -- Start
            'TRA - ENHANCEMENT
        Finally
            objEmployee = Nothing
            objPeriod = Nothing
            'Sohail (18 May 2013) -- End
        End Try
    End Sub


    Private Sub CheckAllPayment(ByVal blnCheckAll As Boolean)
        Try
            'Hemant (09 Jun 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
            'For Each lvItem As ListViewItem In lvPayment.Items
            '    RemoveHandler lvPayment.ItemChecked, AddressOf lvPayment_ItemChecked
            '    lvItem.Checked = blnCheckAll
            '    AddHandler lvPayment.ItemChecked, AddressOf lvPayment_ItemChecked
            'Next
            If dvEmployee IsNot Nothing Then
                For Each dr As DataRowView In dvEmployee
                    dr.Item("IsChecked") = blnCheckAll
                    dr.EndEdit()
            Next
                dvEmployee.ToTable.AcceptChanges()
            End If
            'Hemant (09 Jun 2023) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckAllEmployee", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisibility()

        Try
            If mintReferenceId = enPaymentRefId.LOAN OrElse mintReferenceId = enPaymentRefId.ADVANCE Then
                If mintPayTypeId = enPayTypeId.PAYMENT Then
                    btnVoid.Enabled = User._Object.Privilege._DeleteLoanAdvancePayment
                ElseIf mintPayTypeId = enPayTypeId.RECEIVED Then
                    btnVoid.Enabled = User._Object.Privilege._DeleteLoanAdvanceReceived
                    Me.Text = Language.getMessage(mstrModuleName, 4, "List of Receipts")
                    EZeeHeader.Title = Language.getMessage(mstrModuleName, 5, "Receipt List")
                End If
            ElseIf mintReferenceId = enPaymentRefId.SAVINGS Then
                btnVoid.Enabled = User._Object.Privilege._DeleteSavingsPayment
            Else
                btnVoid.Enabled = User._Object.Privilege._DeletePayment
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try

    End Sub
#End Region

#Region " Form's Events "

    Private Sub frmGlobalVoidPayment_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPaymentTran = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalVoidPayment_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGlobalVoidPayment_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Keys.Escape Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalVoidPayment_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmGlobalVoidPayment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objPaymentTran = New clsPayment_tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call FillCombo()
            'Call FillList()

            'Hemant (09 Jun 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
            'If lvPayment.Items.Count > 0 Then lvPayment.Items(0).Selected = True
            'lvPayment.Select()
            If GvVoidPayment.Rows.Count > 0 Then GvVoidPayment.Rows(0).Selected = True
            GvVoidPayment.Select()
            'Hemant (09 Jun 2023) -- End
            
            Call SetVisibility()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmGlobalVoidPayment_Load", mstrModuleName)
        End Try
    End Sub

    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayment_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsPayment_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- End

#End Region

#Region " Button's Events "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnVoid_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVoid.Click
        'Sohail (17 Oct 2012) -- Start
        'TRA - ENHANCEMENT
        Dim dsList As DataSet
        'Dim strVoidIds As String = "" 'Sohail (22 May 2014)
        Dim strFilter As String = ""
        'Sohail (17 Oct 2012) -- End
        Try
            'Hemant (09 Jun 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
            'If lvPayment.CheckedItems.Count <= 0 Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Payment from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            '    lvPayment.Select()
            '    Exit Sub
            'End If
            If dvEmployee.Table.Select("IsGrp = 0 AND IsChecked = 1 ").Length <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Payment from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                GvVoidPayment.Select()
                Exit Sub
            End If
            'Hemant (09 Jun 2023) -- End

            'Sohail (17 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (21 Mar 2014) -- Start
            'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
            'For Each lvItem As ListViewItem In lvPayment.CheckedItems
            '    strVoidIds &= ", " & lvItem.Tag.ToString
            'Next
            'strVoidIds = strVoidIds.Substring(2)

            'Hemant (09 Jun 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
            'Dim lstIDs As List(Of String) = (From p In lvPayment.CheckedItems.Cast(Of ListViewItem)() Select (p.Tag.ToString)).ToList
            Dim lstIDs As List(Of String) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("paymenttranunkid").ToString)).ToList
            'Hemant (09 Jun 2023) -- End
            strVoidIds = String.Join(",", lstIDs.ToArray)
            'Sohail (21 Mar 2014) -- End

            strFilter = " prpayment_tran.periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " AND isauthorized = 1 AND prpayment_tran.paymenttranunkid IN (" & strVoidIds & ") "
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsList = objPaymentTran.GetList("Authorized", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, strFilter)
            dsList = objPaymentTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_StartDate, mdtPeriod_EndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Authorized", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, True, strFilter)
            'Sohail (21 Aug 2015) -- End
            If dsList.Tables("Authorized").Rows.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry! Some of the payments are already Authorized. Please Void Authorized payment for selected employees."), enMsgBoxStyle.Information) '?1
                'Hemant (09 Jun 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
                'lvPayment.Select()
                GvVoidPayment.Select()
                'Hemant (09 Jun 2023) -- End
                Exit Try
            End If
            'Sohail (17 Oct 2012) -- End

            'Sohail (13 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            If ConfigParameter._Object._SetPayslipPaymentApproval = True Then
                strFilter = " prpayment_tran.periodunkid = " & CInt(cboPayPeriod.SelectedValue) & " AND prpayment_tran.isapproved = 1 AND prpayment_tran.paymenttranunkid IN (" & strVoidIds & ") "
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'dsList = objPaymentTran.GetList("Authorized", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, strFilter)
                dsList = objPaymentTran.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_StartDate, mdtPeriod_EndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, "Authorized", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, True, strFilter)
                'Sohail (21 Aug 2015) -- End
                If dsList.Tables("Authorized").Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Some of the payments are Final Approved."), enMsgBoxStyle.Information)
                    'Hemant (09 Jun 2023) -- Start
                    'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
                    'lvPayment.Select()
                    GvVoidPayment.Select()
                    'Hemant (09 Jun 2023) -- End
                    Exit Try
                End If

                Dim objPaymentApproval As New clsPayment_approval_tran
                dsList = objPaymentApproval.GetList("Approved", True, strVoidIds, , , "levelunkid <> -1 AND priority <> -1 ")
                If dsList.Tables("Approved").Rows.Count > 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, Some of the Payments are Approved by some Approvers."), enMsgBoxStyle.Information) '?1
                    'Hemant (09 Jun 2023) -- Start
                    'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
                    'lvPayment.Select()
                    GvVoidPayment.Select()
                    'Hemant (09 Jun 2023) -- End
                    Exit Sub
                End If
                objPaymentApproval = Nothing
            End If
            'Sohail (13 Feb 2013) -- End

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            Dim objBudget As New clsBudget_MasterNew
            Dim intBudgetId As Integer = 0
            dtActAdj = New DataTable
            dsList = objBudget.GetComboList("List", False, True, )
            If dsList.Tables(0).Rows.Count > 0 Then
                intBudgetId = CInt(dsList.Tables(0).Rows(0).Item("budgetunkid"))
            End If
            If intBudgetId > 0 Then
                Dim mstrAnalysis_Fields As String = ""
                Dim mstrAnalysis_Join As String = ""
                Dim mstrAnalysis_OrderBy As String = "hremployee_master.employeeunkid"
                Dim mstrReport_GroupName As String = ""
                Dim mstrAnalysis_TableName As String = ""
                Dim mstrAnalysis_CodeField As String = ""
                objBudget._Budgetunkid = intBudgetId
                Dim strEmpList As String = ""
                'Hemant (09 Jun 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
                'Dim allEmp As List(Of String) = (From p In lvPayment.CheckedItems.Cast(Of ListViewItem)() Select (p.SubItems(colhEmployee.Index).Tag.ToString)).ToList
                Dim allEmp As List(Of String) = (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("employeeunkid").ToString)).ToList
                'Hemant (09 Jun 2023) -- End              
                strEmpList = String.Join(",", allEmp.ToArray())
                Dim objBudgetCodes As New clsBudgetcodes_master
                Dim dsEmp As DataSet = Nothing
                If objBudget._Viewbyid = enBudgetViewBy.Allocation Then
                    Dim objEmp As New clsEmployee_Master
                    Dim objBudget_Tran As New clsBudget_TranNew
                    Dim mstrAllocationTranUnkIDs As String = objBudget_Tran.GetAllocationTranUnkIDs(intBudgetId)

                    frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", objBudget._Allocationbyid, mstrAllocationTranUnkIDs, objBudget._Budget_date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)

                    dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPayPeriod.SelectedValue), intBudgetId, 0, 0, mstrAllocationTranUnkIDs, 1)
                    dsEmp = objEmp.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_StartDate, mdtPeriod_EndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", , , , "hremployee_master.employeeunkid IN (" & strEmpList & ")")
                Else
                    dsList = objBudgetCodes.GetEmployeeActivityPercentage(CInt(cboPayPeriod.SelectedValue), intBudgetId, 0, 0, strEmpList, 1)
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    dtActAdj.Columns.Add("fundactivityunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                    dtActAdj.Columns.Add("transactiondate", System.Type.GetType("System.String")).DefaultValue = ""
                    dtActAdj.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
                    dtActAdj.Columns.Add("isexeed", System.Type.GetType("System.Boolean")).DefaultValue = False
                    dtActAdj.Columns.Add("Activity Code", System.Type.GetType("System.String")).DefaultValue = ""
                    dtActAdj.Columns.Add("Current Balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
                    dtActAdj.Columns.Add("Actual Salary", System.Type.GetType("System.Decimal")).DefaultValue = 0

                    Dim mdicActivity As New Dictionary(Of Integer, Decimal)
                    Dim mdicActivityCode As New Dictionary(Of Integer, String)
                    Dim objActivity As New clsfundactivity_Tran
                    Dim dsAct As DataSet = objActivity.GetList("Activity")
                    Dim objActAdjust As New clsFundActivityAdjustment_Tran
                    Dim dsActAdj As DataSet = objActAdjust.GetLastCurrentBalance("List", 0, mdtPeriod_EndDate)
                    mdicActivity = (From p In dsActAdj.Tables("List") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Total = CDec(p.Item("newbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                    mdicActivityCode = (From p In dsAct.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activity_code").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                    Dim objPayment As New clsPayment_tran
                    Dim dsBalance As DataSet = objPayment.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_StartDate, mdtPeriod_EndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", clsPayment_tran.enPaymentRefId.PAYSLIP, 0, clsPayment_tran.enPayTypeId.PAYMENT, True, " prpayment_tran.paymenttranunkid IN (" & strVoidIds & ") ", Nothing)
                    If objBudget._Viewbyid = enBudgetViewBy.Allocation AndAlso dsEmp IsNot Nothing Then
                        dsBalance.Tables(0).PrimaryKey = New DataColumn() {dsBalance.Tables(0).Columns("employeeunkid")}
                        dsBalance.Tables(0).Merge(dsEmp.Tables(0))
                    End If
                    Dim intActivityId As Integer
                    Dim decTotal As Decimal = 0
                    Dim decActBal As Decimal = 0
                    Dim blnShowValidationList As Boolean = False
                    Dim blnIsExeed As Boolean = False
                    For Each pair In mdicActivityCode
                        intActivityId = pair.Key
                        decTotal = 0
                        decActBal = 0
                        blnIsExeed = False

                        If mdicActivity.ContainsKey(intActivityId) = True Then
                            decActBal = mdicActivity.Item(intActivityId)
                        End If

                        Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundactivityunkid")) = intActivityId AndAlso CDec(p.Item("percentage")) > 0) Select (p)).ToList
                        If lstRow.Count > 0 Then
                            For Each dRow As DataRow In lstRow
                                Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                                Dim decNetPay As Decimal = (From p In dsBalance.Tables(0) Where (CInt(p.Item(mstrAnalysis_OrderBy.Split(CChar("."))(1).Trim)) = intAllocUnkId AndAlso IsDBNull(p.Item("Amount")) = False) Select (CDec(p.Item("Amount")))).Sum
                                decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                            Next

                            'If decTotal > decActBal Then
                            '    blnShowValidationList = True
                            '    blnIsExeed = True
                            'End If
                            Dim dr As DataRow = dtActAdj.NewRow
                            dr.Item("fundactivityunkid") = intActivityId
                            dr.Item("transactiondate") = eZeeDate.convertDate(mdtPeriod_EndDate).ToString()
                            dr.Item("remark") = Language.getMessage(mstrModuleName, 9, "Salary payments Voided for the period of") & " " & cboPayPeriod.Text
                            dr.Item("isexeed") = blnIsExeed
                            dr.Item("Activity Code") = pair.Value
                            dr.Item("Current Balance") = Format(decActBal, GUI.fmtCurrency)
                            dr.Item("Actual Salary") = Format(decTotal, GUI.fmtCurrency)
                            dtActAdj.Rows.Add(dr)
                        End If
                    Next

                    If blnShowValidationList = True Then
                        Dim dr() As DataRow = dtActAdj.Select("isexeed = 0 ")
                        For Each r In dr
                            dtActAdj.Rows.Remove(r)
                        Next
                        dtActAdj.AcceptChanges()
                        dtActAdj.Columns.Remove("fundactivityunkid")
                        dtActAdj.Columns.Remove("transactiondate")
                        dtActAdj.Columns.Remove("remark")
                        dtActAdj.Columns.Remove("isexeed")
                        Dim objValid As New frmCommonValidationList
                        objValid.displayDialog(False, Language.getMessage(mstrModuleName, 10, "Sorry, Actual Salary Payment will be exceeding to the Activity Current Balance."), dtActAdj)
                        Exit Try
                    End If
                End If
            End If
            'Sohail (23 May 2017) -- End

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Are you sure you want to delete selected Payment(s)?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                Exit Try
            End If


            'Hemant (09 Jun 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
            'Dim intSelectedIndex As Integer
            'intSelectedIndex = lvPayment.SelectedItems(0).Index
            'Hemant (09 Jun 2023) -- End

            Dim frm As New frmReasonSelection
            'Sohail (22 May 2014) -- Start
            'Enhancement - Show Progress on Global Void Payment.
            'Dim mstrVoidReason As String = String.Empty
            mstrVoidReason = ""
            'Sohail (22 May 2014)
            frm.displayDialog(enVoidCategoryType.PAYROLL, mstrVoidReason)
            If mstrVoidReason.Length <= 0 Then
                Exit Sub
            End If

            'Sohail (17 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim strVoidIds As String = ""
            'For Each lvItem As ListViewItem In lvPayment.CheckedItems
            '    strVoidIds &= ", " & lvItem.Tag.ToString
            'Next
            'strVoidIds = strVoidIds.Substring(2)
            'Sohail (17 Oct 2012) -- End

            'Sohail (22 May 2014) -- Start
            'Enhancement - Show Progress on Global Void Payment.
            'If objPaymentTran.VoidAll(strVoidIds, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason) = True Then
            '    lvPayment.SelectedItems(0).Remove()

            '    Call FillList()

            '    mblnCancel = False
            'End If

            'Hemant (09 Jun 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
            'mintCheckedEmployee = lvPayment.CheckedItems.Count
            mintCheckedEmployee = dvEmployee.Table.Select("IsGrp = 0 AND IsChecked = 1 ").Length
            'Hemant (09 Jun 2023) -- End
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            objbgwVoidPayment.RunWorkerAsync()
            'Sohail (22 May 2014) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnVoid_Click", mstrModuleName)
            'Sohail (22 May 2014) -- Start
            'Enhancement - Show Progress on Global Void Payment.
            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            'Sohail (22 May 2014) -- End
        End Try
    End Sub
#End Region

    'Sohail (03 Jul 2014) -- Start
    'Enhancement - Advance Filter and Voucher Combobox on Global Void Payment list.
#Region " Combobox Events "
    Private Sub cboPayPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPayPeriod.SelectedIndexChanged
        Dim dsCombo As DataSet
        Try
            dsCombo = objPaymentTran.Get_DIST_VoucherNo("Voucher", True, clsPayment_tran.enPaymentRefId.PAYSLIP, clsPayment_tran.enPayTypeId.PAYMENT, FinancialYear._Object._DatabaseName, CInt(cboPayPeriod.SelectedValue))
            With cboPmtVoucher
                .ValueMember = "voucherno"
                .DisplayMember = "voucherno"
                .DataSource = dsCombo.Tables("Voucher")
                .SelectedIndex = 0
            End With

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            If CInt(cboPayPeriod.SelectedValue) > 0 Then
                mdtPeriod_StartDate = eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
                mdtPeriod_EndDate = eZeeDate.convertDate(CType(cboPayPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
            Else
                mdtPeriod_StartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
                mdtPeriod_EndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            End If
            'Sohail (21 Aug 2015) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPayPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
#End Region
    'Sohail (03 Jul 2014) -- End

#Region " Listview's Events "

    'Hemant (09 Jun 2023) -- Start
    'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
    'Private Sub lvPayment_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvPayment.ItemChecked
    '    Try
    '        If lvPayment.CheckedItems.Count <= 0 Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Unchecked
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvPayment.CheckedItems.Count < lvPayment.Items.Count Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Indeterminate
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        ElseIf lvPayment.CheckedItems.Count = lvPayment.Items.Count Then
    '            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '            objchkSelectAll.CheckState = CheckState.Checked
    '            AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvPayment_ItemChecked", mstrModuleName)
    '    End Try
    'End Sub
    'Hemant (09 Jun 2023) -- End

    Private Sub GvVoidPayment_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles GvVoidPayment.CellPainting
        Try
            If e.RowIndex < 0 Then Exit Sub
            If e.RowIndex >= 0 AndAlso e.RowIndex < GvVoidPayment.RowCount - 1 AndAlso CBool(GvVoidPayment.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True AndAlso e.ColumnIndex > 0 Then
                If (e.ColumnIndex = colhDate.Index) Then
                    Dim backColorBrush As Brush = New SolidBrush(Color.Gray)
                    Dim totWidth As Integer = 0
                    For i As Integer = 1 To GvVoidPayment.Columns.Count - 1
                        totWidth += GvVoidPayment.Columns(i).Width
                    Next
                    Dim r As New RectangleF(e.CellBounds.Left, e.CellBounds.Top, totWidth, e.CellBounds.Height)
                    e.Graphics.FillRectangle(backColorBrush, r)

                    e.Graphics.DrawString("Voucher No. : " & CType(GvVoidPayment.Rows(e.RowIndex).Cells(colhVoucher.Index).Value.ToString, String), e.CellStyle.Font, Brushes.White, e.CellBounds.X, e.CellBounds.Y + 5)

                End If

                e.Handled = True
            End If

            If GvVoidPayment.Columns(e.ColumnIndex).DataPropertyName = "PaymentDate" Then
                If GvVoidPayment.Rows(e.RowIndex).Cells(e.ColumnIndex).Value IsNot Nothing AndAlso IsDBNull(GvVoidPayment.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False AndAlso GvVoidPayment.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString().Trim.Length > 0 AndAlso IsDate(GvVoidPayment.Rows(e.RowIndex).Cells(e.ColumnIndex).Value) = False Then
                    GvVoidPayment.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = eZeeDate.convertDate(GvVoidPayment.Rows(e.RowIndex).Cells(e.ColumnIndex).Value.ToString()).ToShortDateString
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgEmployee_CellPainting", mstrModuleName)
        End Try
    End Sub

    Private Sub GvVoidPayment_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles GvVoidPayment.CellContentClick
        Try
                RemoveHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

            If e.ColumnIndex = objcolhCheck1.Index Then
                If Me.GvVoidPayment.IsCurrentCellDirty Then
                    Me.GvVoidPayment.CommitEdit(DataGridViewDataErrorContexts.Commit)
                End If
                Dim drRow As DataRow() = Nothing
                If CBool(GvVoidPayment.Rows(e.RowIndex).Cells(objdgcolhIsGroup.Index).Value) = True Then
                    drRow = dvEmployee.Table.Select(colhVoucher.DataPropertyName & " = '" & GvVoidPayment.Rows(e.RowIndex).Cells(colhVoucher.Index).Value.ToString() & "'", "")
                    If drRow.Length > 0 Then
                        For index As Integer = 0 To drRow.Length - 1
                            drRow(index)("isChecked") = CBool(GvVoidPayment.Rows(e.RowIndex).Cells(objcolhCheck1.Index).Value)
                        Next
                    End If
                End If


                drRow = dvEmployee.ToTable.Select("isChecked = true", "")

                If drRow.Length > 0 Then
                    If dvEmployee.ToTable.Rows.Count = drRow.Length Then
                        objchkSelectAll.CheckState = CheckState.Checked
                    Else
                        objchkSelectAll.CheckState = CheckState.Indeterminate
                    End If
                Else
                objchkSelectAll.CheckState = CheckState.Unchecked
                End If

            End If

                AddHandler objchkSelectAll.CheckedChanged, AddressOf chkSelectAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GvVoidPayment_CellContentClick", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " CheckBox's Events "
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Dim decTotAmt As Decimal = 0
        Try
            Call CheckAllPayment(objchkSelectAll.Checked)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Other Control's Events "
    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            cboEmployee.SelectedValue = 0
            If cboPayPeriod.Items.Count > 0 Then cboPayPeriod.SelectedIndex = 0
            txtPaidAmount.Text = ""
            txtPaidAmountTo.Text = ""
            'Sohail (03 Jul 2014) -- Start
            'Enhancement - Advance Filter and Voucher Combobox on Global Void Payment list.
            'txtVoucherno.Text = ""
            cboPmtVoucher.SelectedIndex = 0
            'Sohail (03 Jul 2014) -- End
            dtpPaymentDate.Checked = False
            mstrAdvanceFilter = "" 'Sohail (22 May 2014)
            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchEmployee.Click
        Dim objfrm As New frmCommonSearch
        Try
            With objfrm
                .ValueMember = cboEmployee.ValueMember
                .DisplayMember = cboEmployee.DisplayMember
                .DataSource = CType(cboEmployee.DataSource, DataTable)
                .CodeMember = "employeecode"
            End With

            If objfrm.DisplayDialog Then
                cboEmployee.SelectedValue = objfrm.SelectedValue
                cboEmployee.Focus()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchEmployee_Click", mstrModuleName)
        End Try
    End Sub

    'Sohail (22 May 2014) -- Start
    'Enhancement - Show Progress on Global Void Payment.
    Private Sub objbgwVoidPayment_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles objbgwVoidPayment.DoWork
        Try
            mblnProcessFailed = False
            Me.ControlBox = False
            Me.Enabled = False

            GC.Collect()
            dtStart = DateAndTime.Now

            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            'Hemant (09 Jun 2023) -- Start
            'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
            'Dim strEmpIDs As String = String.Join(",", (From p In lvPayment.CheckedItems.Cast(Of ListViewItem)() Select (p.SubItems(colhEmployee.Index).Tag.ToString)).ToArray)
            Dim strEmpIDs As String = String.Join(",", (From p In dvEmployee.Table Where (CBool(p.Item("IsChecked")) = True AndAlso CBool(p.Item("IsGrp")) = False) Select (p.Item("employeeunkid").ToString)).ToArray)
            'Hemant (09 Jun 2023) -- End
            'Sohail (30 Oct 2018) -- End

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            'If objPaymentTran.VoidAll(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_StartDate, mdtPeriod_EndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, strVoidIds, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, True, objbgwVoidPayment) = True Then
            'Sohail (30 Oct 2018) -- Start
            'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
            'If objPaymentTran.VoidAll(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_StartDate, mdtPeriod_EndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, strVoidIds, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, True, objbgwVoidPayment, "", dtActAdj) = True Then
            If objPaymentTran.VoidAll(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriod_StartDate, mdtPeriod_EndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, strVoidIds, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, mstrVoidReason, CInt(clsPayment_tran.enPaymentRefId.PAYSLIP), CInt(clsPayment_tran.enPayTypeId.PAYMENT), True, objbgwVoidPayment, "", dtActAdj, strEmpIDs, ConfigParameter._Object._CurrencyFormat) = True Then
                'Sohail (26 Oct 2020) - [strFmtCurrency]
                'Sohail (30 Oct 2018) -- End
                'Sohail (23 May 2017) -- End
                'Hemant (09 Jun 2023) -- Start
                'ISSUE/ENHANCEMENT(TRA) : A1X-979 - Optimize Payment Voiding operation
                'lvPayment.SelectedItems(0).Remove()
                'Hemant (09 Jun 2023) -- End
                Call FillList()

                mblnCancel = False
            End If
        Catch ex As Exception
            mblnProcessFailed = True
            DisplayError.Show("-1", ex.Message, "objbgwVoidPayment_DoWork", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgwVoidPayment_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles objbgwVoidPayment.ProgressChanged
        Try
            objlblProgress.Text = "[ " & e.ProgressPercentage.ToString & " / " & mintCheckedEmployee & " ]"
            objlblProgress.Refresh()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwVoidPayment_ProgressChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objbgwVoidPayment_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles objbgwVoidPayment.RunWorkerCompleted
        Try

            RemoveHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick
            AddHandler gfrmMDI.tmrReminder.Tick, AddressOf gfrmMDI.tmrReminder_Tick

            'Me.Text = CInt((Now - dtStart).TotalSeconds).ToString

            If e.Cancelled = True Then

            ElseIf e.Error IsNot Nothing Then
                eZeeMsgBox.Show(e.Error.ToString, enMsgBoxStyle.Information)
            ElseIf mblnProcessFailed = True Then

            Else
                'Sohail (30 Oct 2018) -- Start
                'FDRC Enhancement - Global Payment performance issue on FDRC database in 75.1.
                'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Void Payment Process completed successfully."), enMsgBoxStyle.Information)
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Void Payment Process completed successfully."), enMsgBoxStyle.Information, eZeeMsgBox.MessageBoxButton.DefaultTitle & " [in " & CInt((DateAndTime.Now - dtStart).TotalSeconds).ToString & " Seconds.]")
                'Sohail (30 Oct 2018) -- End
                objchkSelectAll.Checked = False
                cboPayPeriod.SelectedValue = 0
                objlblProgress.Text = ""
            End If

            Me.ControlBox = True
            Me.Enabled = True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbgwVoidPayment_RunWorkerCompleted", mstrModuleName)
        End Try
    End Sub

    Private Sub lnkAdvanceFilter_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnkAdvanceFilter.LinkClicked
        Try
            Dim frm As New frmAdvanceSearch
            frm.ShowDialog()
            mstrAdvanceFilter = frm._GetFilterString
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lnkAdvanceFilter_LinkClicked", mstrModuleName)
        End Try
    End Sub
    'Sohail (22 May 2014) -- End

#End Region

#Region " Messages "
    '1, "Please select Payment from the list to perform further operation on it."
    '2, "Are you sure you want to delete selected Payment(s)?"
#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.EZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.EZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.EZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.EZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.EZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnVoid.GradientBackColor = GUI._ButttonBackColor
            Me.btnVoid.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.EZeeHeader.Title = Language._Object.getCaption(Me.EZeeHeader.Name & "_Title", Me.EZeeHeader.Title)
            Me.EZeeHeader.Message = Language._Object.getCaption(Me.EZeeHeader.Name & "_Message", Me.EZeeHeader.Message)
            Me.colhDate.HeaderText = Language._Object.getCaption(CStr(Me.colhDate.Tag), Me.colhDate.HeaderText)
            Me.colhVoucher.HeaderText = Language._Object.getCaption(CStr(Me.colhVoucher.Tag), Me.colhVoucher.HeaderText)
            Me.colhEmployee.HeaderText = Language._Object.getCaption(CStr(Me.colhEmployee.Tag), Me.colhEmployee.HeaderText)
            Me.colhPayPeriod.HeaderText = Language._Object.getCaption(CStr(Me.colhPayPeriod.Tag), Me.colhPayPeriod.HeaderText)
            Me.colhPaidAmount.HeaderText = Language._Object.getCaption(CStr(Me.colhPaidAmount.Tag), Me.colhPaidAmount.HeaderText)
            Me.colhCurrency.HeaderText = Language._Object.getCaption(CStr(Me.colhCurrency.Tag), Me.colhCurrency.HeaderText)
            Me.btnVoid.Text = Language._Object.getCaption(Me.btnVoid.Name, Me.btnVoid.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.lblTo.Text = Language._Object.getCaption(Me.lblTo.Name, Me.lblTo.Text)
            Me.lblPaidAmount.Text = Language._Object.getCaption(Me.lblPaidAmount.Name, Me.lblPaidAmount.Text)
            Me.lblPaymentDate.Text = Language._Object.getCaption(Me.lblPaymentDate.Name, Me.lblPaymentDate.Text)
            Me.lblVoucherno.Text = Language._Object.getCaption(Me.lblVoucherno.Name, Me.lblVoucherno.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.lblEmployee.Text = Language._Object.getCaption(Me.lblEmployee.Name, Me.lblEmployee.Text)
            Me.colhEmpCode.HeaderText = Language._Object.getCaption(CStr(Me.colhEmpCode.Tag), Me.colhEmpCode.HeaderText)
            Me.lnkAdvanceFilter.Text = Language._Object.getCaption(Me.lnkAdvanceFilter.Name, Me.lnkAdvanceFilter.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Payment from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Sorry! Some of the payments are already Authorized. Please Void Authorized payment for selected employees.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Some of the payments are Final Approved.")
            Language.setMessage(mstrModuleName, 4, "List of Receipts")
            Language.setMessage(mstrModuleName, 5, "Receipt List")
            Language.setMessage(mstrModuleName, 6, "Sorry, Some of the Payments are Approved by some Approvers.")
            Language.setMessage(mstrModuleName, 7, "Are you sure you want to delete selected Payment(s)?")
            Language.setMessage(mstrModuleName, 8, "Void Payment Process completed successfully.")
			Language.setMessage(mstrModuleName, 9, "Salary payments Voided for the period of")
			Language.setMessage(mstrModuleName, 10, "Sorry, Actual Salary Payment will be exceeding to the Activity Current Balance.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class