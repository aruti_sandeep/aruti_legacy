﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmPayment_AddEdit

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmPayment_AddEdit"

#Region " Common Varibales "
    Private mintTranUnkid As Integer = -1
    Private mblnCancel As Boolean = True
    Private objPaymentTran As New clsPayment_tran
    Private objMaster As clsMasterData
    Private mintReferenceUnkid As Integer = -1
    Private menAction As enAction = enAction.ADD_ONE
    Private objEmployee As clsEmployee_Master
    Private objCommon As clscommom_period_Tran
    Private mintSelectedIndex As Integer = -1
    Private mintPaymentTranId As Integer = -1
    Private mintPayTypeId As Integer = -1
    Private decReceived As Decimal = 0 'Sohail (11 May 2011)
    Private mintRefSelectedIndex As Integer = -1
    'Savings
    Private mdtScheme_EffDate As DateTime = Nothing
    Private mdecBalanceAmt As Decimal = 0 'Sohail (11 May 2011)
    Private mdecFinalBalanceAmt As Decimal = 0 'Sohail (11 May 2011)
    Dim objInterestTran As clsInterestRate_tran
    ' Dim objRedemptionTran As clsRedemption_tran

    'Sandeep [ 17 DEC 2010 ] -- Start
    Private dtDenometable As DataTable
    'Sandeep [ 17 DEC 2010 ] -- End 

    'Sohail (08 Oct 2011) -- Start
    'For base currency Balance Amount
    Private mstrBaseCurrSign As String
    Private mintBaseCurrId As Integer
    Private mintPaidCurrId As Integer
    Private mdecBaseExRate As Decimal
    Private mdecPaidExRate As Decimal
    Private mdecBaseCurrBalanceAmt As Decimal
    Private mdecPaidCurrBalanceAmt As Decimal
    Private mdecPaidCurrBalFormatedAmt As Decimal 'displayed on txtAmount textbox.
    'Sohail (08 Oct 2011) -- End

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    Private mdtPeriodStartDate As Date = Nothing
    Private mdtPeriodEndDate As Date = Nothing
    'Sohail (21 Aug 2015) -- End
    Private mintBaseCountryId As Integer = 0 'Sohail (07 May 2015)

    'Sohail (12 Dec 2015) -- Start
    'Enhancement - Provide Deposit feaure in Employee Saving.
    Private mdtLoanSavingEffectiveDate As Date
    'Sohail (12 Dec 2015) -- End
#End Region

#Region " Loan / Advance "
    Private objLoan_Advance As clsLoan_Advance
    Private objLoan_Master As clsLoan_Scheme
    Private mstrLoan_Status_Data As String = String.Empty
    Private mblnIsFromStatus As Boolean = False
    'Nilay (01-Apr-2016) -- Start
    'ENHANCEMENT - Approval Process in Loan Other Operations
    Private mintEmployeeunkid As Integer = 0
    'Nilay (01-Apr-2016) -- End

#End Region

#Region " Saving "
    Private objEmployee_Saving As clsSaving_Tran
    Private objSaving_Master As clsSavingScheme
    Private objSavingStatus As clsSaving_Status_Tran
    Private mintPaymentListCount As Integer = 0
#End Region

#Region "PaySlip"
    Dim objPayslip As clsTnALeaveTran
#End Region

#End Region

#Region " Properties "
    Public WriteOnly Property _IsRefSelectionId() As Integer
        Set(ByVal value As Integer)
            mintRefSelectedIndex = value
        End Set
    End Property

    Public WriteOnly Property _Loan_Status_data() As String
        Set(ByVal value As String)
            mstrLoan_Status_Data = value
        End Set
    End Property
    Public WriteOnly Property _IsFrom_Status() As Boolean
        Set(ByVal value As Boolean)
            mblnIsFromStatus = value
        End Set
    End Property

    Public WriteOnly Property _PaymentListCount() As Integer
        Set(ByVal value As Integer)
            mintPaymentListCount = value
        End Set
    End Property

    'Sohail (12 Dec 2015) -- Start
    'Enhancement - Provide Deposit feaure in Employee Saving.
    Public WriteOnly Property _LoanSavingEffectiveDate() As Date
        Set(ByVal value As Date)
            mdtLoanSavingEffectiveDate = value
        End Set
    End Property
    'Sohail (12 Dec 2015) -- End
#End Region

#Region " Display Dialog "
    Public Function displayDialog(ByRef intUnkId As Integer, _
                                  ByVal eAction As enAction, _
                                  ByVal intRefUnkid As Integer, _
                                  ByVal intTransactionId As Integer, _
                                  ByVal intPaymentTypeId As Integer) As Boolean
        Try
            mintPaymentTranId = intUnkId
            mintTranUnkid = intTransactionId
            mintReferenceUnkid = intRefUnkid
            mintPayTypeId = intPaymentTypeId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintPaymentTranId

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Private Methods "

    Private Sub FillInfo()
        Try
            txtEmployee.Text = objEmployee._Firstname & " " & objEmployee._Othername & " " & objEmployee._Surname
            Select Case mintReferenceUnkid
                Case 1, 2       'Loan / Advance 
                    txtPaymentOf.Text = CStr(IIf(objLoan_Advance._Isloan = True, Language.getMessage("frmLoanAdvanceList", 2, "Loan"), _
                                                                                 Language.getMessage("frmLoanAdvanceList", 3, "Advance")))
                    txtVoucher.Text = objLoan_Advance._Loanvoucher_No

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'txtPayYear.Text = objMaster.GetFinancialYear_Name(CInt(objCommon._Yearunkid))
                    'Sohail (24 Jun 2017) -- Start
                    'Issue - 68.1 - There is no row at position 0.
                    'txtPayYear.Text = objMaster.GetFinancialYear_Name(CInt(objCommon._Yearunkid), Company._Object._Companyunkid)
                    Try
                    txtPayYear.Text = objMaster.GetFinancialYear_Name(CInt(objCommon._Yearunkid), Company._Object._Companyunkid)
                    Catch ex As Exception
                        'Do Nothing to ignore error if closed year period database does not exist
                    End Try
                    'Sohail (24 Jun 2017) -- End
                    'S.SANDEEP [04 JUN 2015] -- END

                    txtPayPeriod.Text = objCommon._Period_Name
                    If mintPayTypeId = clsPayment_tran.enPayTypeId.PAYMENT Then
                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        'txtBalanceDue.Text = Format(IIf(objLoan_Advance._Isloan = True, objLoan_Advance._Loan_Amount, objLoan_Advance._Advance_Amount), GUI.fmtCurrency)
                        'txtBalanceDue.Tag = CDec(IIf(objLoan_Advance._Isloan = True, objLoan_Advance._Loan_Amount, objLoan_Advance._Advance_Amount)) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
                        txtBalanceDue.Text = Format(objLoan_Advance._Basecurrency_amount, GUI.fmtCurrency)
                        txtBalanceDue.Tag = objLoan_Advance._Basecurrency_amount
                        'Sohail (07 May 2015) -- End
                    ElseIf mintPayTypeId = clsPayment_tran.enPayTypeId.RECEIVED Then
                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                        'txtBalanceDue.Text = Format(objLoan_Advance._Balance_Amount, GUI.fmtCurrency) 'Sohail (04 Mar 2011)
                        'txtBalanceDue.Tag = CDec(objLoan_Advance._Balance_Amount) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
                        If mintReferenceUnkid = clsPayment_tran.enPaymentRefId.ADVANCE Then
                            txtBalanceDue.Text = Format(objLoan_Advance._Balance_Amount, GUI.fmtCurrency)
                            txtBalanceDue.Tag = CDec(objLoan_Advance._Balance_Amount)
                        Else
                            Dim decBalanceAmt As Decimal = 0
                            Dim objLoan As New clsLoan_Advance
                            'Nilay (25-Mar-2016) -- Start
                            'Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo("List", dtpPaymentDate.Value.Date, "", mintTranUnkid)
                            Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo(FinancialYear._Object._DatabaseName, _
                                                                                      User._Object._Userunkid, _
                                                                                      FinancialYear._Object._YearUnkid, _
                                                                                      Company._Object._Companyunkid, _
                                                                                      mdtPeriodStartDate, _
                                                                                      mdtPeriodEndDate, _
                                                                                      ConfigParameter._Object._UserAccessModeSetting, _
                                                                                      True, "List", dtpPaymentDate.Value.Date, "", mintTranUnkid)
                            'Nilay (25-Mar-2016) -- End

                            If dsLoan.Tables("List").Rows.Count > 0 Then
                                With dsLoan.Tables("List").Rows(0)
                                    txtBalanceDue.Text = Format(CDec(.Item("LastProjectedBalance")), GUI.fmtCurrency)
                                    txtBalanceDue.Tag = CDec(.Item("LastProjectedBalance"))
                                End With
                            End If
                        End If
                        'Sohail (07 May 2015) -- End
                    End If
                    txtScheme.Text = CStr(IIf(objLoan_Advance._Isloan = True, objLoan_Master._Name, ""))
                Case 3          'Payslip
                    txtPaymentOf.Text = CStr(Language.getMessage("frmPayslipList", 4, "Payslip"))
                    txtVoucher.Text = objPayslip._Voucherno

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'txtPayYear.Text = objMaster.GetFinancialYear_Name(CInt(objCommon._Yearunkid))
                    txtPayYear.Text = objMaster.GetFinancialYear_Name(CInt(objCommon._Yearunkid), Company._Object._Companyunkid)
                    'S.SANDEEP [04 JUN 2015] -- END

                    txtPayPeriod.Text = objCommon._Period_Name
                    txtBalanceDue.Text = objPayslip._Balanceamount.ToString(GUI.fmtCurrency) 'Sohail (04 Mar 2011)
                    txtBalanceDue.Tag = objPayslip._Balanceamount 'Sohail (16 Oct 2010)
                    txtAmount.Text = txtBalanceDue.Text

                Case 4          'Saving
                    txtPaymentOf.Text = "Savings"
                    txtVoucher.Text = objEmployee_Saving._Voucherno

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'txtPayYear.Text = objMaster.GetFinancialYear_Name(CInt(objCommon._Yearunkid))
                    txtPayYear.Text = objMaster.GetFinancialYear_Name(CInt(objCommon._Yearunkid), Company._Object._Companyunkid)
                    'S.SANDEEP [04 JUN 2015] -- END

                    txtPayPeriod.Text = objCommon._Period_Name
                    ' txtBalanceDue.Text = ""
                    txtScheme.Text = objSaving_Master._Savingschemename
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillInfo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Dim dsCombos As New DataSet
        'Sohail (16 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objBankGrp As New clspayrollgroup_master
        'Dim objBranch As New clsbankbranch_master
        Dim objCompanyBank As New clsCompany_Bank_tran
        'Sohail (16 Jul 2012) -- End
        Dim objExRate As New clsExchangeRate
        Dim objPeriod As New clscommom_period_Tran 'Sohail (09 Mar 2015)
        Try
            dsCombos = objMaster.GetPaymentMode("PayMode")
            With cboPaymentMode
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("PayMode")
                .SelectedValue = 0
            End With

            dsCombos = objMaster.GetPaymentBy("PayBy")
            With cboPaymentBy
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("PayBy")
                .SelectedValue = 0
            End With

            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT - Now Show bank from Company master
            'dsCombos = objBankGrp.getListForCombo(enGroupType.BankGroup, "BankGrp", True)
            'With cboBankGroup
            '    .ValueMember = "groupmasterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("BankGrp")
            '    .SelectedValue = 0
            'End With
            dsCombos = objCompanyBank.GetComboList(Company._Object._Companyunkid, 1, "BankGrp")
            With cboBankGroup
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("BankGrp")
                .SelectedValue = 0
            End With
            'Sohail (16 Jul 2012) -- End

            'Sohail (08 Oct 2011) -- Start
            dsCombos = objExRate.getComboList("ExRate", False)
            If dsCombos.Tables(0).Rows.Count > 0 Then
                Dim dtTable As DataTable = New DataView(dsCombos.Tables("ExRate"), "isbasecurrency = 1 ", "", DataViewRowState.CurrentRows).ToTable
                If dtTable.Rows.Count > 0 Then
                    mstrBaseCurrSign = dtTable.Rows(0).Item("currency_sign").ToString
                    mintBaseCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    mintBaseCountryId = CInt(dtTable.Rows(0).Item("countryunkid"))
                Else
                    mintBaseCountryId = 0
                    'Sohail (07 May 2015) -- End
                End If
                With cboCurrency
                    'Sohail (03 Sep 2012) -- Start
                    'TRA - ENHANCEMENT
                    '.ValueMember = "exchangerateunkid"
                    .ValueMember = "countryunkid"
                    'Sohail (03 Sep 2012) -- End
                    .DisplayMember = "currency_sign"
                    .DataSource = dsCombos.Tables("ExRate")
                    'Sohail (07 May 2015) -- Start
                    'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    '.SelectedIndex = 0
                    If mintBaseCountryId > 0 Then
                        .SelectedValue = mintBaseCountryId
                    Else
                        .SelectedIndex = 0
                    End If
                    'Sohail (07 May 2015) -- End
                End With
            End If

            'Sohail (08 Oct 2011) -- End

            'Sohail (09 Mar 2015) -- Start
            'Enhancement - Provide Multi Currency on Saving scheme contribution.
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, "Period", True, enStatusType.Open)
            dsCombos = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "Period", True, enStatusType.Open)
            'Sohail (21 Aug 2015) -- End
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt - Allow to take receipt, withdrawal in first open period only as interest and balance are re-calculated on end date process payroll.
            Dim mdtTable As DataTable = Nothing
            If mintPayTypeId = clsPayment_tran.enPayTypeId.PAYMENT Then
                mdtTable = New DataView(dsCombos.Tables("Period")).ToTable
            Else
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open, FinancialYear._Object._YearUnkid)
                Dim intFirstPeriodId As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
                'Nilay (10-Oct-2015) -- End

                If intFirstPeriodId > 0 Then
                    mdtTable = New DataView(dsCombos.Tables("Period"), "periodunkid = " & intFirstPeriodId & " ", "", DataViewRowState.CurrentRows).ToTable
                Else
                    mdtTable = New DataView(dsCombos.Tables("Period"), "1=2", "", DataViewRowState.CurrentRows).ToTable
                End If
            End If
            'Sohail (07 May 2015) -- End
            With cboPeriod
                .ValueMember = "periodunkid"
                .DisplayMember = "name"
                'Sohail (07 May 2015) -- Start
                'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                '.DataSource = dsCombos.Tables("Period")
                '.SelectedValue = 0
                .DataSource = mdtTable
                If .Items.Count > 0 Then .SelectedIndex = 0
                'Sohail (07 May 2015) -- End
            End With
            'Sohail (09 Mar 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub SetColor()
        Try
            txtAgainstVoucher.BackColor = GUI.ColorComp
            txtAmount.BackColor = GUI.ColorComp
            txtBalanceDue.BackColor = GUI.ColorComp
            txtChequeNo.BackColor = GUI.ColorComp
            txtEmployee.BackColor = GUI.ColorComp
            txtPaymentOf.BackColor = GUI.ColorComp
            txtPayPeriod.BackColor = GUI.ColorComp
            txtScheme.BackColor = GUI.ColorComp
            'txtTotal.BackColor = GUI.ColorComp
            txtValue.BackColor = GUI.ColorComp
            txtVoucher.BackColor = GUI.ColorComp
            txtPayYear.BackColor = GUI.ColorComp
            cboBankGroup.BackColor = GUI.ColorComp
            cboPaymentBy.BackColor = GUI.ColorComp
            cboPaymentMode.BackColor = GUI.ColorComp
            cboBranch.BackColor = GUI.ColorComp
            cboCurrency.BackColor = GUI.ColorComp 'Sohail (08 Oct 2011)
            cboAccountNo.BackColor = GUI.ColorComp 'Sohail (21 Jul 2012)
            txtRemarks.BackColor = GUI.ColorOptional 'Sohail (24 Dec 2014)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            objPaymentTran._Percentage = CDec(IIf(txtValue.Text.Trim = "", 0, txtValue.Text))
            'Sohail (16 Oct 2010) -- Start
            'objPaymentTran._Amount = cdec(txtAmount.Text)
            'Sohail (08 Oct 2011) -- Start
            'If txtBalanceDue.Decimal = txtAmount.Decimal Then 'Sohail (11 May 2011)
            '    objPaymentTran._Amount = CDec(txtBalanceDue.Tag) 'Sohail (11 May 2011)
            'Else
            '    objPaymentTran._Amount = txtAmount.Decimal
            'End If
            'Sohail (08 Oct 2011) -- End
            'Sohail (16 Oct 2010) -- End

            objPaymentTran._Branchunkid = CInt(cboBranch.SelectedValue)
            objPaymentTran._Chequeno = txtChequeNo.Text

            If CInt(cboAccountNo.SelectedValue) > 0 Then
                objPaymentTran._Accountno = cboAccountNo.Text 'Sohail (21 Jul 2012)
            Else
                objPaymentTran._Accountno = "" 'Sohail (21 Jul 2012)
            End If

            Select Case mintReferenceUnkid
                Case 1, 2       'Loan / Advance
                    objPaymentTran._Employeeunkid = objLoan_Advance._Employeeunkid
                Case 3          'Payslip
                    objPaymentTran._Employeeunkid = objPayslip._Employeeunkid
                Case 4    'Saving
                    objPaymentTran._Employeeunkid = objEmployee_Saving._Employeeunkid

            End Select
            'Sohail (21 Nov 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            'If mintPayTypeId = clsPayment_tran.enPayTypeId.RECEIVED Then   '<TODO Consider Saving Condition>
            If mintPayTypeId = clsPayment_tran.enPayTypeId.RECEIVED OrElse mintPayTypeId = clsPayment_tran.enPayTypeId.DEPOSIT Then   '<TODO Consider Saving Condition>
                'Sohail (21 Nov 2015) -- End
                objPaymentTran._Is_Receipt = True
            Else
                objPaymentTran._Is_Receipt = False
            End If
            objPaymentTran._Paymentbyid = CInt(cboPaymentBy.SelectedValue)
            objPaymentTran._Paymentdate = dtpPaymentDate.Value
            objPaymentTran._Paymentmodeid = CInt(cboPaymentMode.SelectedValue)
            objPaymentTran._Paymentrefid = mintReferenceUnkid
            objPaymentTran._Payreftranunkid = mintTranUnkid
            objPaymentTran._PaymentTypeId = mintPayTypeId

            Select Case mintReferenceUnkid
                Case 1, 2       'Loan / Advance
                    objPaymentTran._Periodunkid = objLoan_Advance._Periodunkid
                Case 3          'Payslip
                    objPaymentTran._Periodunkid = objPayslip._Payperiodunkid
                Case 4          'Saving
                    objPaymentTran._Periodunkid = objEmployee_Saving._Payperiodunkid

            End Select
            objPaymentTran._Userunkid = User._Object._Userunkid
            If mintPaymentTranId = -1 Then
                objPaymentTran._Isvoid = False
                objPaymentTran._Voiddatetime = Nothing
                objPaymentTran._Voiduserunkid = -1
            Else
                objPaymentTran._Isvoid = objPaymentTran._Isvoid
                objPaymentTran._Voiddatetime = objPaymentTran._Voiddatetime
                objPaymentTran._Voiduserunkid = objPaymentTran._Voiduserunkid
            End If
            objPaymentTran._Voucherno = txtAgainstVoucher.Text
            objPaymentTran._Voucherref = txtVoucher.Text
            If mintPaymentListCount > 0 Then
                objPaymentTran._IsInsertStatus = False
            Else
                objPaymentTran._IsInsertStatus = True
            End If
            objPaymentTran._Isglobalpayment = False 'Sohail (16 Oct 2010)


            'Sandeep [ 25 APRIL 2011 ] -- Start
            objPaymentTran._strData = mstrLoan_Status_Data
            objPaymentTran._IsFromStatus = mblnIsFromStatus
            'Sandeep [ 25 APRIL 2011 ] -- End 

            'Sohail (08 Oct 2011) -- Start
            objPaymentTran._Basecurrencyid = mintBaseCurrId
            objPaymentTran._Baseexchangerate = mdecBaseExRate
            objPaymentTran._Paidcurrencyid = mintPaidCurrId
            objPaymentTran._Expaidrate = mdecPaidExRate
            If mintReferenceUnkid = clsPayment_tran.enPaymentRefId.PAYSLIP Then 'Sohail (17 Mar 2020)
            If mdecPaidCurrBalFormatedAmt = CDec(Format(txtAmount.Decimal, GUI.fmtCurrency)) Then
                'Sohail (21 Mar 2014) -- Start
                'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                If ConfigParameter._Object._CFRoundingAbove > 0 AndAlso Math.Abs(CDec(txtBalanceDue.Tag) - mdecBaseCurrBalanceAmt) <= ConfigParameter._Object._CFRoundingAbove Then
                    objPaymentTran._RoundingAdjustment = CDec(txtBalanceDue.Tag) - mdecBaseCurrBalanceAmt '*** Rounding Adjustment
                Else
                    objPaymentTran._RoundingAdjustment = 0 '*** Rounding Adjustment
                End If
                'Sohail (21 Mar 2014) -- End
                If mdecPaidExRate <> 0 Then
                    objPaymentTran._Amount = mdecPaidCurrBalanceAmt * mdecBaseExRate / mdecPaidExRate
                Else
                    objPaymentTran._Amount = mdecPaidCurrBalanceAmt * mdecBaseExRate
                End If
                objPaymentTran._Expaidamt = mdecPaidCurrBalanceAmt
            Else
                'Sohail (21 Mar 2014) -- Start
                'Enhancement - Set Limit for C/F Netpay balance and Payment JV Report
                If ConfigParameter._Object._CFRoundingAbove > 0 AndAlso Math.Abs(CDec(txtBalanceDue.Tag) - mdecBaseCurrBalanceAmt) <= ConfigParameter._Object._CFRoundingAbove Then
                        'Sohail (17 Mar 2020) -- Start
                        'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
                        'objPaymentTran._RoundingAdjustment = CDec(txtBalanceDue.Tag) - txtAmount.Decimal '*** Rounding Adjustment
                        objPaymentTran._RoundingAdjustment = CDec(txtBalanceDue.Tag) - mdecBaseCurrBalanceAmt '*** Rounding Adjustment
                        'Sohail (17 Mar 2020) -- End
                Else
                    objPaymentTran._RoundingAdjustment = 0 '*** Rounding Adjustment
                End If
                'Sohail (21 Mar 2014) -- End
                If mdecPaidExRate <> 0 Then
                    objPaymentTran._Amount = txtAmount.Decimal * mdecBaseExRate / mdecPaidExRate
                Else
                    objPaymentTran._Amount = txtAmount.Decimal * mdecBaseExRate
                End If
                objPaymentTran._Expaidamt = txtAmount.Decimal
            End If
                'Sohail (17 Mar 2020) -- Start
                'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
            Else
                If mdecPaidExRate <> 0 Then
                    objPaymentTran._Amount = txtAmount.Decimal * mdecBaseExRate / mdecPaidExRate
                Else
                    objPaymentTran._Amount = txtAmount.Decimal * mdecBaseExRate
                End If
                objPaymentTran._Expaidamt = txtAmount.Decimal
            End If
            'Sohail (17 Mar 2020) -- End
            'Sohail (08 Oct 2011) -- End
            objPaymentTran._Countryunkid = CInt(cboCurrency.SelectedValue) 'Sohail (03 Sep 2012)
            objPaymentTran._Isapproved = False 'Sohail (13 Feb 2013)

            'Sohail (22 Oct 2013) -- Start
            'TRA - ENHANCEMENT 
            If mintReferenceUnkid = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                objPaymentTran._Roundingtypeid = ConfigParameter._Object._PaymentRoundingType
                objPaymentTran._Roundingmultipleid = ConfigParameter._Object._PaymentRoundingMultiple
            Else
                objPaymentTran._Roundingtypeid = 0
                objPaymentTran._Roundingmultipleid = 0
            End If
            'Sohail (22 Oct 2013) -- End
            objPaymentTran._Remarks = txtRemarks.Text.Trim 'Sohail (24 Dec 2014)
            objPaymentTran._PaymentDate_Periodunkid = CInt(cboPeriod.SelectedValue) 'Sohail (07 May 2015)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            'cboCurrency.SelectedValue = objPaymentTran._Paidcurrencyid 'Sohail (08 Oct 2011)
            cboCurrency.SelectedValue = objPaymentTran._Countryunkid
            'S.SANDEEP [ 29 May 2013 ] -- END
            If mintReferenceUnkid <> clsPayment_tran.enPaymentRefId.PAYSLIP Then
                txtValue.Text = CStr(objPaymentTran._Percentage)
                'Sohail (08 Oct 2011) -- Start
                'txtAmount.Text = Format(objPaymentTran._Amount, GUI.fmtCurrency)
                txtAmount.Text = Format(objPaymentTran._Expaidamt, GUI.fmtCurrency)
                'Sohail (08 Oct 2011) -- End
                cboPaymentBy.SelectedValue = objPaymentTran._Paymentbyid
            End If

            'Sandeep [ 21 Aug 2010 ] -- Start
            cboBranch.SelectedValue = objPaymentTran._Branchunkid
            Dim objBankGrp As New clspayrollgroup_master
            Dim objBranch As New clsbankbranch_master
            objBranch._Branchunkid = CInt(objPaymentTran._Branchunkid)
            objBankGrp._Groupmasterunkid = CInt(objBranch._Bankgroupunkid)
            cboBankGroup.SelectedValue = CInt(objBankGrp._Groupmasterunkid)
            objBankGrp = Nothing
            objBranch = Nothing
            'Sandeep [ 21 Aug 2010 ] -- End 

            txtChequeNo.Text = objPaymentTran._Chequeno
            cboAccountNo.Text = objPaymentTran._Accountno
            Select Case mintReferenceUnkid
                Case 1, 2       'Loan / Advance
                    objPaymentTran._Employeeunkid = objPaymentTran._Employeeunkid
                Case 3          'Payslip

                Case 4          'Saving
                    objPaymentTran._Employeeunkid = objPaymentTran._Employeeunkid

            End Select
            objPaymentTran._Isvoid = objPaymentTran._Isvoid
            objPaymentTran._Is_Receipt = objPaymentTran._Is_Receipt
            'cboPaymentBy.SelectedValue = objPaymentTran._Paymentbyid
            If objPaymentTran._Paymentdate = Nothing Then

            Else
                dtpPaymentDate.Value = objPaymentTran._Paymentdate
            End If
            cboPaymentMode.SelectedValue = objPaymentTran._Paymentmodeid
            If mintPaymentTranId = -1 Then
                objPaymentTran._Paymentrefid = objPaymentTran._Paymentrefid
                objPaymentTran._Payreftranunkid = objPaymentTran._Payreftranunkid
                objPaymentTran._PaymentTypeId = objPaymentTran._PaymentTypeId
            Else
                mintReferenceUnkid = objPaymentTran._Paymentrefid
                mintTranUnkid = objPaymentTran._Payreftranunkid
                mintPayTypeId = objPaymentTran._PaymentTypeId
            End If
            mintPaymentTranId = objPaymentTran._Paymenttranunkid
            Select Case mintReferenceUnkid
                Case 1, 2       'Loan / Advance
                    If mintPaymentTranId = -1 Then
                        objLoan_Advance._Periodunkid = objPaymentTran._Periodunkid
                    Else
                        objPaymentTran._Periodunkid = objPaymentTran._Periodunkid
                    End If
                Case 3          'Payslip

                    If mintPaymentTranId = -1 Then
                        objPayslip._Payperiodunkid = objPaymentTran._Periodunkid
                    Else
                        objPaymentTran._Periodunkid = objPaymentTran._Periodunkid
                    End If

                Case 4              'Saving
                    If mintPaymentTranId = -1 Then
                        objEmployee_Saving._Payperiodunkid = objPaymentTran._Periodunkid
                    Else
                        objPaymentTran._Periodunkid = objPaymentTran._Periodunkid
                    End If

            End Select
            objPaymentTran._Userunkid = objPaymentTran._Userunkid
            objPaymentTran._Voiddatetime = objPaymentTran._Voiddatetime
            objPaymentTran._Voiduserunkid = objPaymentTran._Voiduserunkid
            txtAgainstVoucher.Text = objPaymentTran._Voucherno
            If objPaymentTran._Voucherref <> "" Then
                txtVoucher.Text = objPaymentTran._Voucherref
            End If
            txtRemarks.Text = objPaymentTran._Remarks 'Sohail (24 Dec 2014)

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            If menAction <> enAction.EDIT_ONE Then
                Select Case mintReferenceUnkid
                    'Nilay (28-Aug-2015) -- Start
                    'Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.LOAN
                    Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                        'Nilay (28-Aug-2015) -- End
                        If CInt(cboPeriod.SelectedValue) > 0 Then
                            dtpPaymentDate.Value = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("start_date").ToString)
                        End If
                    Case clsPayment_tran.enPaymentRefId.PAYSLIP
                        If CInt(cboPeriod.SelectedValue) > 0 Then
                            dtpPaymentDate.Value = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
                        End If
                    Case clsPayment_tran.enPaymentRefId.SAVINGS
                        If CInt(cboPeriod.SelectedValue) > 0 Then
                            dtpPaymentDate.Value = eZeeDate.convertDate(CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString)
                        End If
                End Select

            End If
            'Sohail (07 May 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetVisbibility()
        Try
            'Sandeep [ 16 Oct 2010 ] -- Start
            'Issue : Auto No. Generation
            If ConfigParameter._Object._PaymentVocNoType = 1 Then
                txtAgainstVoucher.Enabled = False
            Else
                txtAgainstVoucher.Enabled = True
            End If
            'Sandeep [ 16 Oct 2010 ] -- End 

            Select Case mintPayTypeId
                Case clsPayment_tran.enPayTypeId.PAYMENT  'PAYMENT
                    lblReceivedFrom.Visible = False
                    lblPaidTo.Visible = True
                    lblLoanAmount.Visible = True
                    lblBalanceDue.Visible = False
                Case clsPayment_tran.enPayTypeId.RECEIVED  'RECEIVED
                    'Anjan (23 Feb 2011)-Start
                    Me.Text = Language.getMessage(mstrModuleName, 6, "Add / Edit Receipt")
                    gbPayment.Text = Language.getMessage(mstrModuleName, 7, "Receipt Information")
                    'Anjan (23 Feb 2011)-End
                    lblReceivedFrom.Visible = True
                    lblPaidTo.Visible = False
                    lblBalanceDue.Visible = True
                    lblLoanAmount.Visible = False
                    'Sohail (21 Nov 2015) -- Start
                    'Enhancement - Provide Deposit feaure in Employee Saving.
                    'Case 3  'WITHDRAWAL
                    'Case 4  'REDEMPTION
                    '    lblLoanAmount.Text = Language.getMessage(mstrModuleName, 5, "Savings")
                    'Case 5  'REPAYMENT
                Case clsPayment_tran.enPayTypeId.WITHDRAWAL, clsPayment_tran.enPayTypeId.REPAYMENT, clsPayment_tran.enPayTypeId.DEPOSIT
                    lblLoanAmount.Text = Language.getMessage(mstrModuleName, 5, "Savings")
                    'Sohail (21 Nov 2015) -- End
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisbibility", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        'Dim blnIsZeroSalary As Boolean = False 'Sohail (18 Dec 2010), 'Sohail (04 Mar 2011) - [Commented]
        Dim dtTable As DataTable 'Sohail (29 Jun 2012)
        'Sohail (16 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        Dim objEmpBank As New clsEmployeeBanks
        Dim ds As DataSet
        'Sohail (16 Jul 2012) -- End
        Try
            If CDec(txtBalanceDue.Text.Trim) <= 0 Or txtBalanceDue.Text.Trim = "" Then 'Sohail (11 May 2011)
                'Select Case mintPayTypeId
                '    Case clsPayment_tran.enPayTypeId.PAYMENT
                '        Select Case mintReferenceUnkid
                '            Case clsPayment_tran.enPaymentRefId.LOAN
                '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot make new payment. Reason : Loan is already paid to particular employee."), enMsgBoxStyle.Information)
                '                Return False
                '            Case clsPayment_tran.enPaymentRefId.ADVANCE
                '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot make new payment. Reason : Due amount is alread paid by employee."), enMsgBoxStyle.Information)
                '                Return False
                '            Case clsPayment_tran.enPaymentRefId.PAYSLIP
                '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot make new payment. Reason : Payslip amount is alread paid by employee."), enMsgBoxStyle.Information)
                '                Return False
                '            Case clsPayment_tran.enPaymentRefId.SAVINGS
                '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot make new payment. Reason : Saving Balance is Zero."), enMsgBoxStyle.Information)
                '                Return False
                '        End Select
                'End Select

                'Sohail (18 Dec 2010) -- Start
                'Select Case mintReferenceUnkid
                'Case clsPayment_tran.enPayTypeId.PAYMENT
                'Select Case mintPayTypeId
                'Sohail (18 Dec 2010) -- End
                Select Case mintReferenceUnkid
                    Case clsPayment_tran.enPaymentRefId.LOAN
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 8, "Sorry, you cannot make new payment. Reason : Loan is already paid to particular employee."), enMsgBoxStyle.Information)
                        Return False
                    Case clsPayment_tran.enPaymentRefId.ADVANCE
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 9, "Sorry, you cannot make new payment. Reason : Due amount is already paid by employee."), enMsgBoxStyle.Information) 'Sohail (18 Dec 2010)
                        Return False
                    Case clsPayment_tran.enPaymentRefId.PAYSLIP
                        'Sohail (18 Dec 2010) -- Start
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot make new payment. Reason : Payslip amount is alread paid by employee."), enMsgBoxStyle.Information)
                        'Return False
                        'Sohail (04 Mar 2011) -- Start
                        'Changes : From now Payment should not be done for zero or negative balance.
                        'If (New clsPayment_tran).IsPaymentDone(clsPayment_tran.enPayTypeId.PAYMENT, mintTranUnkid) = True Then
                        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot make new payment. Reason : Payslip amount is already paid by employee."), enMsgBoxStyle.Information) 'Sohail (18 Dec 2010)
                        '    Return False
                        'Else
                        '    If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CHEQUE OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.TRANSFER Then
                        '        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry, you can not make Cheque or Transafer Payment of Zero Amount."), enMsgBoxStyle.Information)
                        '        Return False
                        '    End If
                        '    blnIsZeroSalary = True
                        'End If
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 26, "Sorry, you cannot make payment. Reason : Payslip amount is either Zero or Negative."), enMsgBoxStyle.Information)
                        Return False
                        'Sohail (04 Mar 2011) -- End
                        'Sohail (18 Dec 2010) -- End

                    Case clsPayment_tran.enPaymentRefId.SAVINGS
                        'Sohail (21 Nov 2015) -- Start
                        'Enhancement - Provide Deposit feaure in Employee Saving.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot make new payment. Reason : Saving Balance is Zero."), enMsgBoxStyle.Information)
                        'Return False
                        If mintPayTypeId <> clsPayment_tran.enPayTypeId.DEPOSIT Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 18, "Sorry, you cannot make new payment. Reason : Saving Balance is Zero."), enMsgBoxStyle.Information)
                            Return False
                        End If
                        'Sohail (21 Nov 2015) -- End
                End Select

                'End Select 'Sohail (18 Dec 2010)
            End If


            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            Select Case mintReferenceUnkid
                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                    If mintPayTypeId = clsPayment_tran.enPayTypeId.PAYMENT Then
                        'Sohail (25 Mar 2015) -- Start
                        'B.C.Patel & Co Enhancement - New Custom CSV and XLS EFT for Advance module Payment and distribute payment as per employee banks.
                        'If txtAmount.Decimal < txtBalanceDue.Decimal Then
                        If txtAmount.Decimal < mdecPaidCurrBalFormatedAmt Then
                            'Sohail (25 Mar 2015) -- End
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 29, "Sorry, you cannot do any partial payment. Please do full payment."), enMsgBoxStyle.Information)
                            Return False
                        End If
                        'Sohail (07 May 2015) -- Start
                        'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
                    ElseIf mintPayTypeId = clsPayment_tran.enPayTypeId.RECEIVED Then
                        Dim objLoan As New clsLoan_Advance
                        Dim dsLoan As DataSet = objLoan.GetLastLoanBalance("List", mintTranUnkid)
                        If dsLoan.Tables("List").Rows.Count > 0 Then
                            'Sohail (06 Sep 2017) -- Start
                            'Issue - 69.1 - Not able to make loan receipt on loan effective date.
                            'If dsLoan.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(dtpPaymentDate.Value.Date) Then
                            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Payment date should not be less than last transaction date") & " " & Format(eZeeDate.convertDate(dsLoan.Tables("List").Rows(0).Item("end_date").ToString).AddDays(1), "dd-MMM-yyyy") & ".", enMsgBoxStyle.Information)
                            '    Return False
                            'End If
                            objLoan._Loanadvancetranunkid = mintTranUnkid
                            If CDate(objLoan._Effective_Date).Date = eZeeDate.convertDate(dsLoan.Tables(0).Rows(0).Item("end_date").ToString).Date Then
                                If dsLoan.Tables("List").Rows(0).Item("end_date").ToString > eZeeDate.convertDate(dtpPaymentDate.Value.Date) Then
                                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Payment date should not be less than last transaction date") & " " & Format(eZeeDate.convertDate(dsLoan.Tables("List").Rows(0).Item("end_date").ToString), "dd-MMM-yyyy") & ".", enMsgBoxStyle.Information)
                                    Return False
                                End If
                            Else
                            If dsLoan.Tables("List").Rows(0).Item("end_date").ToString >= eZeeDate.convertDate(dtpPaymentDate.Value.Date) Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 35, "Sorry, Payment date should not be less than last transaction date") & " " & Format(eZeeDate.convertDate(dsLoan.Tables("List").Rows(0).Item("end_date").ToString).AddDays(1), "dd-MMM-yyyy") & ".", enMsgBoxStyle.Information)
                                Return False
                            End If
                        End If
                            'Sohail (06 Sep 2017) -- End
                        End If
                        'Sohail (07 May 2015) -- End

                        'Nilay (01-Apr-2016) -- Start
                        'ENHANCEMENT - Approval Process in Loan Other Operations
                        Dim objlnOtherOpApprovalTran As New clsloanotherop_approval_tran
                        Dim blnFlag As Boolean
                        blnFlag = objlnOtherOpApprovalTran.IsLoanParameterApprovalPending(CInt(cboPeriod.SelectedValue), mintEmployeeunkid.ToString, dtpPaymentDate.Value)
                        If blnFlag = False Then
                            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 39, "Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process in current period. In order to Receipt, please either Approve or Reject pending operations for current period."), enMsgBoxStyle.Information)
                            Return False
                        End If
                        'Nilay (01-Apr-2016) -- End
                    End If

                    'Sohail (12 Dec 2015) -- Start
                    'Enhancement - Provide Deposit feaure in Employee Saving.
                    If mdtLoanSavingEffectiveDate <> Nothing AndAlso dtpPaymentDate.Value.Date < mdtLoanSavingEffectiveDate.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 37, "Sorry, Date should not be less than Loan/Advance Effective date") & " " & Format(mdtLoanSavingEffectiveDate, "dd-MMM-yyyy") & ".", enMsgBoxStyle.Information)
                        Return False
                    End If

                Case clsPayment_tran.enPaymentRefId.SAVINGS
                    If mdtLoanSavingEffectiveDate <> Nothing AndAlso dtpPaymentDate.Value.Date < mdtLoanSavingEffectiveDate.Date Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 38, "Sorry, Date should not be less than Saving Effective date") & " " & Format(mdtLoanSavingEffectiveDate, "dd-MMM-yyyy") & ".", enMsgBoxStyle.Information)
                        Return False
                    End If
                    'Sohail (12 Dec 2015) -- End

            End Select
            'S.SANDEEP [ 29 May 2013 ] -- END

            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            If mdecBaseExRate = 0 AndAlso mdecPaidExRate = 0 Then '[No exchange rate found for current currency for current payment date.]
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 24, "Sorry! No Exchange rate defined for selected currency for the given date."))
                dtpPaymentDate.Focus()
                Return False
            End If
            'Sohail (03 Sep 2012) -- End

            'Sandeep [ 16 Oct 2010 ] -- Start
            'Issue : Auto No. Generation
            'If txtAgainstVoucher.Text.Trim = "" Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Voucher No. cannot be blank. Voucher No. is compulsory information."))
            '    txtAgainstVoucher.Focus()
            '    Return False
            'End If
            If ConfigParameter._Object._PaymentVocNoType = 0 Then
                If txtAgainstVoucher.Text.Trim = "" Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 10, "Voucher No. cannot be blank. Voucher No. is compulsory information."))
                    txtAgainstVoucher.Focus()
                    Return False
                End If
            End If
            'Sandeep [ 16 Oct 2010 ] -- End 

            If CInt(cboPaymentMode.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 11, "Payment Mode is compulsory information. Please select Payment Mode to continue."), enMsgBoxStyle.Information)
                cboPaymentMode.Focus()
                Return False
            End If

            If CInt(cboPaymentBy.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 12, "Payment By is compulsory information. Please select Payment By to continue.", ), enMsgBoxStyle.Information)
                cboPaymentBy.Focus()
                Return False
            End If

            Select Case CInt(cboPaymentMode.SelectedValue)
                Case enPaymentMode.CHEQUE
                    If CInt(cboBankGroup.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Bank Group is compulsory information. Please select Bank Group to continue."), enMsgBoxStyle.Information)
                        cboBankGroup.Focus()
                        Return False
                    End If
                    If CInt(cboBranch.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Branch is compulsory information. Please select Branch to continue."), enMsgBoxStyle.Information)
                        cboBranch.Focus()
                        Return False
                        'Sohail (21 Jul 2012) -- Start
                        'TRA - ENHANCEMENT
                    ElseIf CInt(cboAccountNo.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 25, "Please select Bank Account. Bank Account is mandatory information."), enMsgBoxStyle.Information)
                        cboAccountNo.Focus()
                        Return False
                        'Sohail (21 Jul 2012) -- End
                    End If

                    'Sohail (16 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    Select Case mintReferenceUnkid
                        Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                            If objEmpBank.isEmployeeBankExist(objLoan_Advance._Employeeunkid) = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), enMsgBoxStyle.Information)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'ds = objEmpBank.GetEmployeeWithOverDistribution("List", objLoan_Advance._Employeeunkid.ToString)
                            ds = objEmpBank.GetEmployeeWithOverDistribution(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                                                            FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                            ConfigParameter._Object._UserAccessModeSetting, True, True, False, _
                                                                            "", "List", objLoan_Advance._Employeeunkid.ToString)
                            'Sohail (21 Aug 2015) -- End
                            If ds.Tables("List").Rows.Count > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), enMsgBoxStyle.Information)
                                cboPaymentMode.Focus()
                                Return False
                            End If

                        Case clsPayment_tran.enPaymentRefId.PAYSLIP
                            If objEmpBank.isEmployeeBankExist(objPayslip._Employeeunkid) = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), enMsgBoxStyle.Information)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'ds = objEmpBank.GetEmployeeWithOverDistribution("List", objPayslip._Employeeunkid.ToString)
                            ds = objEmpBank.GetEmployeeWithOverDistribution(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                            eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                            ConfigParameter._Object._UserAccessModeSetting, _
                                                                            True, True, False, "", "List", objPayslip._Employeeunkid.ToString)
                            'Sohail (21 Aug 2015) -- End
                            If ds.Tables("List").Rows.Count > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), enMsgBoxStyle.Information)
                                cboPaymentMode.Focus()
                                Return False
                            End If

                        Case clsPayment_tran.enPaymentRefId.SAVINGS
                            If objEmpBank.isEmployeeBankExist(objEmployee_Saving._Employeeunkid) = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), enMsgBoxStyle.Information)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'ds = objEmpBank.GetEmployeeWithOverDistribution("List", objEmployee_Saving._Employeeunkid.ToString)
                            ds = objEmpBank.GetEmployeeWithOverDistribution(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, False, "", "List", objEmployee_Saving._Employeeunkid.ToString)
                            'Sohail (21 Aug 2015) -- End
                            If ds.Tables("List").Rows.Count > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), enMsgBoxStyle.Information)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                    End Select
                    'Sohail (16 Jul 2012) -- End

                    If txtChequeNo.Text = "" Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 17, "Cheque No. cannot be blank. Please enter the cheque information."), enMsgBoxStyle.Information)
                        txtChequeNo.Focus()
                        Return False
                    End If
                Case enPaymentMode.TRANSFER
                    If CInt(cboBankGroup.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 13, "Bank Group is compulsory information. Please select Bank Group to continue."), enMsgBoxStyle.Information)
                        cboBankGroup.Focus()
                        Return False
                    End If
                    If CInt(cboBranch.SelectedValue) <= 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 14, "Branch is compulsory information. Please select Branch to continue."), enMsgBoxStyle.Information)
                        cboBranch.Focus()
                        Return False
                    End If

                    'Sohail (16 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    Select Case mintReferenceUnkid
                        Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                            If objEmpBank.isEmployeeBankExist(objLoan_Advance._Employeeunkid) = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), enMsgBoxStyle.Information)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'ds = objEmpBank.GetEmployeeWithOverDistribution("List", objLoan_Advance._Employeeunkid.ToString)
                            ds = objEmpBank.GetEmployeeWithOverDistribution(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, False, "", "List", objLoan_Advance._Employeeunkid.ToString)
                            'Sohail (21 Aug 2015) -- End
                            If ds.Tables("List").Rows.Count > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), enMsgBoxStyle.Information)
                                cboPaymentMode.Focus()
                                Return False
                            End If

                        Case clsPayment_tran.enPaymentRefId.PAYSLIP
                            If objEmpBank.isEmployeeBankExist(objPayslip._Employeeunkid) = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), enMsgBoxStyle.Information)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'ds = objEmpBank.GetEmployeeWithOverDistribution("List", objPayslip._Employeeunkid.ToString)
                            ds = objEmpBank.GetEmployeeWithOverDistribution(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, False, "", "List", objPayslip._Employeeunkid.ToString)
                            'Sohail (21 Aug 2015) -- End
                            If ds.Tables("List").Rows.Count > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), enMsgBoxStyle.Information)
                                cboPaymentMode.Focus()
                                Return False
                            End If

                            'Nilay (01-Apr-2016) -- Start
                            'ENHANCEMENT - Approval Process in Loan Other Operations
                            Dim objlnOtherOpApprovalTran As New clsloanotherop_approval_tran
                            Dim blnFlag As Boolean
                            blnFlag = objlnOtherOpApprovalTran.IsLoanParameterApprovalPending(CInt(cboPeriod.SelectedValue), objPayslip._Employeeunkid.ToString, dtpPaymentDate.Value)
                            If blnFlag = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 40, "Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process in current period. In order to make Payment, please either Approve or Reject pending operations for current period."), enMsgBoxStyle.Information)
                                Return False
                            End If
                            'Nilay (01-Apr-2016) -- End

                        Case clsPayment_tran.enPaymentRefId.SAVINGS
                            If objEmpBank.isEmployeeBankExist(objEmployee_Saving._Employeeunkid) = False Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank."), enMsgBoxStyle.Information)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'ds = objEmpBank.GetEmployeeWithOverDistribution("List", objEmployee_Saving._Employeeunkid.ToString)
                            ds = objEmpBank.GetEmployeeWithOverDistribution(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), ConfigParameter._Object._UserAccessModeSetting, True, True, False, "", "List", objEmployee_Saving._Employeeunkid.ToString)
                            'Sohail (21 Aug 2015) -- End
                            If ds.Tables("List").Rows.Count > 0 Then
                                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank."), enMsgBoxStyle.Information)
                                cboPaymentMode.Focus()
                                Return False
                            End If
                    End Select
                    'Sohail (16 Jul 2012) -- End
            End Select

            Select Case CInt(cboPaymentBy.SelectedValue)
                Case 1
                    'Sohail (18 Dec 2010) -- Start
                    'Sohail (04 Mar 2011) -- Start
                    If txtAmount.Decimal <= 0 Then 'Sohail (11 May 2011)
                        'If txtAmount.Decimal = 0 AndAlso blnIsZeroSalary = False Then
                        'Sohail (04 Mar 2011) -- End
                        'If txtAmount.Text.Trim = "" Or txtAmount.Text.Trim = "0" Then
                        'Sohail (18 Dec 2010) -- End
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 15, "Amount cannot be blank .Amount is compulsory information."), enMsgBoxStyle.Information)
                        txtAmount.Focus()
                        Return False
                    End If
                Case 2
                    'Sohail (18 Dec 2010) -- Start
                    'Sohail (04 Mar 2011) -- Start
                    If txtValue.Decimal <= 0 Then 'Sohail (11 May 2011)
                        'If txtValue.Decimal = 0 AndAlso blnIsZeroSalary = False Then
                        'Sohail (04 Mar 2011) -- End
                        'If txtValue.Text.Trim = "" Or txtValue.Text.Trim = "0" Then
                        'Sohail (18 Dec 2010) -- End
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 16, "Percent cannot be blank .Percent is compulsory information."), enMsgBoxStyle.Information)
                        txtValue.Focus()
                        Return False
                    End If
            End Select

            'Sohail (08 Oct 2011) -- Start
            Dim decBalAmt As Decimal
            If mdecBaseExRate <> 0 Then
                decBalAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
            Else
                decBalAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate
            End If
            'If txtAmount.Decimal > txtBalanceDue.Decimal Then 'Sohail (11 May 2011)
            'Sohail (08 Nov 2011) -- Start
            'Sohail (21 Nov 2015) -- Start
            'Enhancement - Provide Deposit feaure in Employee Saving.
            'If txtAmount.Decimal > CDec(Format(decBalAmt, GUI.fmtCurrency)) Then
            If txtAmount.Decimal > CDec(Format(decBalAmt, GUI.fmtCurrency)) AndAlso Not (mintPayTypeId = clsPayment_tran.enPayTypeId.DEPOSIT) Then
                'Sohail (21 Nov 2015) -- End
                'If txtAmount.Decimal > decBalAmt Then
                'Sohail (08 Nov 2011) -- End
                'Sohail (08 Oct 2011) -- End
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 19, "Amount cannot be greater than the Due Amount ( ") & decBalAmt.ToString & Language.getMessage(mstrModuleName, 20, " )."), enMsgBoxStyle.Information)
                txtAmount.Focus()
                Return False
            End If

            'Sohail (09 Mar 2015) -- Start
            'Enhancement - Provide Multi Currency on Saving scheme contribution.
            If CInt(cboPeriod.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 33, "Please select period."), enMsgBoxStyle.Information)
                cboPeriod.Focus()
                Return False
            End If
            'Sohail (09 Mar 2015) -- End

            'Sohail (11 May 2011) -- Start
            'Issue : Payment can be done in any previuos year date
            If dtpPaymentDate.Value.Date < objCommon._Start_Date OrElse dtpPaymentDate.Value.Date > FinancialYear._Object._Database_End_Date.Date Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 23, "Payment Date should be between ") & objCommon._Start_Date.ToShortDateString & Language.getMessage(mstrModuleName, 28, " And ") & FinancialYear._Object._Database_End_Date.ToShortDateString & ".", enMsgBoxStyle.Information)
                dtpPaymentDate.Focus()
                Return False
            End If
            'Sohail (11 May 2011) -- End

            'Sohail (12 Jan 2015) -- Start
            'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
            Dim objMaster As New clsMasterData
            Dim objPeriod As New clscommom_period_Tran

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim intFirstOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, enStatusType.Open, FinancialYear._Object._YearUnkid)
            Dim intFirstOpenPeriod As Integer = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.Open)
            'S.SANDEEP [04 JUN 2015] -- END

            If intFirstOpenPeriod > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPeriod._Periodunkid = intFirstOpenPeriod
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = intFirstOpenPeriod
                'Sohail (21 Aug 2015) -- End
                If dtpPaymentDate.Value.Date < objPeriod._Start_Date Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 32, "Sorry, Payment Date should be between open periods and should not be less than") & " " & objPeriod._Start_Date.ToShortDateString, enMsgBoxStyle.Information)
                    dtpPaymentDate.Focus()
                    Return False
                End If
            End If

            'Sohail (09 Mar 2015) -- Start
            'Enhancement - Provide Multi Currency on Saving scheme contribution.
            If eZeeDate.convertDate(dtpPaymentDate.Value.Date) < CType(cboPeriod.SelectedItem, DataRowView).Item("start_date").ToString OrElse eZeeDate.convertDate(dtpPaymentDate.Value.Date) > CType(cboPeriod.SelectedItem, DataRowView).Item("end_date").ToString Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 34, "Sorry, Payment Date should be between selected period start date and end date."), enMsgBoxStyle.Information)
                dtpPaymentDate.Focus()
                Return False
            End If
            'Sohail (09 Mar 2015) -- End

            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            If mintPayTypeId = clsPayment_tran.enPayTypeId.RECEIVED Then
                Dim strEmpID As String = ""
                Select Case mintReferenceUnkid
                    Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                        strEmpID = objLoan_Advance._Employeeunkid.ToString
                    Case clsPayment_tran.enPaymentRefId.SAVINGS
                        strEmpID = objEmployee_Saving._Employeeunkid.ToString
                End Select
                If strEmpID.Trim <> "" Then
                    Dim objTnA As New clsTnALeaveTran
                    If objTnA.IsPayrollProcessDone(CInt(cboPeriod.SelectedValue), strEmpID, objPeriod._End_Date, enModuleReference.Payroll) = True Then
                        'Sohail (19 Apr 2019) -- Start
                        'Enhancement - 76.1 - Option to open process payroll screen if process payroll is already done message come.
                        'eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Sorry, You cannot do this transaction. Reason : Payroll process is already done for the last date of selected period for selected employee."), enMsgBoxStyle.Information)
                        If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 36, "Sorry, You cannot do this transaction. Reason : Payroll process is already done for the last date of selected period for selected employee.") & vbCrLf & vbCrLf & Language.getMessage(mstrModuleName, 43, "Do you want to void Payroll?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                            Dim objFrm As New frmProcessPayroll
                            objFrm.ShowDialog()
                        End If
                        'Sohail (19 Apr 2019) -- End
                        cboPeriod.Focus()
                        Return False
                    End If
                End If
            End If
            'Sohail (07 May 2015) -- End

            objMaster = Nothing
            objPeriod = Nothing
            'Sohail (12 Jan 2015) -- End

            'Sohail (29 Jun 2012) -- Start
            'TRA - ENHANCEMENT
            If mintReferenceUnkid = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                If ConfigParameter._Object._DoNotAllowOverDeductionForPayment = True Then
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'dtTable = (New clsTnALeaveTran).GetOverDeductionList("List", objCommon._Periodunkid, False) 'CInt(cboPayPeriod.SelectedValue)
                    dtTable = (New clsTnALeaveTran).GetOverDeductionList("List", objCommon._Periodunkid(FinancialYear._Object._DatabaseName), False)
                    'Sohail (21 Aug 2015) -- End
                    If dtTable.Rows.Count > 0 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 27, "Sorry! You can not do Payment now. Reason : Some of the employees have been Over Deducted this month."), enMsgBoxStyle.Information)
                        Return False
                    End If
                End If

                'Sohail (25 Aug 2022) -- Start
                'Enhancement : AC2-793 : VFT - As a user, I want to be able to stop over deduction where net pay goes below a certain percentage of employee gross or basic.
                If ConfigParameter._Object._DontAllowPaymentIfNetPayGoesBelow.Trim.Length > 0 Then
                    Dim objPayroll As New clsPayrollProcessTran
                    Dim strEmpIDs As String = objPayslip._Employeeunkid.ToString

                    Dim dtEmp As DataTable = objPayroll.GetFormulaBasePayslipAmount(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", CInt(cboPeriod.SelectedValue), strEmpIDs, ConfigParameter._Object._DontAllowPaymentIfNetPayGoesBelow, "")
                    Dim dtFinal As DataTable = New DataView(dtEmp, "netpay < amount", "", DataViewRowState.CurrentRows).ToTable
                    If dtFinal.Rows.Count > 0 Then
                        Dim sid As String = String.Join(",", (From p In dtFinal Select (p.Item("employeeunkid").ToString)).ToArray)
                        dtFinal.Columns.Remove("employeeunkid")
                        Dim objValid As New frmCommonValidationList
                        objValid.displayDialog(False, Language.getMessage(mstrModuleName, 44, "Sorry, Net Pay went below as per the formula set on configuration for some employees."), dtFinal)
                        Return False
                    End If
                End If
                'Sohail (25 Aug 2022) -- End

                'Sohail (28 May 2014) -- Start
                'Enhancement - Staff Requisition.
                If gobjEmailList.Count > 0 AndAlso ConfigParameter._Object._SetPayslipPaymentApproval = True Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 30, "Sending Email(s) process is in progress from other module. Please wait."), enMsgBoxStyle.Information)
                    Return False
                End If
                'Sohail (28 May 2014) -- End

            End If
            'Sohail (29 Jun 2012) -- End

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function

    'Private Sub FinalRedemptionAmt()
    '    Dim days As Integer = 0
    '    Dim dblYears As Double = 0
    '    Dim dblInterestRate As Double = 0
    '    Dim Rate As Double = 0
    '    Dim Charge As Double = 0
    '    Dim dblRedemptionAmt As Double = 0
    '    Dim dblInterestAmt As Double = 0
    '    Try
    '        objInterestTran = New clsInterestRate_tran
    '        objRedemptionTran = New clsRedemption_tran


    '        If menAction = enAction.EDIT_ONE Then
    '            objPaymentTran.Get_Payment_Total(mintTranUnkid, mintReferenceUnkid, mintPayTypeId, dblReceived, False, True)
    '        Else
    '            objPaymentTran.Get_Payment_Total(mintTranUnkid, mintReferenceUnkid, mintPayTypeId, dblReceived)
    '        End If
    '        days = CInt(DateDiff(DateInterval.Day, CDate(mdtScheme_EffDate).Date, Now.Date))
    '        dblYears = cdec(days / 365)

    '        'Redemption calculation
    '        objRedemptionTran.GetRedemptionCharge(CInt(objEmployee_Saving._Savingschemeunkid), dblYears)

    '        Charge = cdec(objRedemptionTran._Charge)


    '        dblRedemptionAmt = (mdblBalanceAmt * Charge) / 100
    '        mdblFinalBalanceAmt = (mdblBalanceAmt - dblRedemptionAmt)


    '        'Interest Calculation
    '        objInterestTran.GetInterestRate(CInt(objEmployee_Saving._Savingschemeunkid), dblYears)

    '        Rate = cdec(objInterestTran._Rate)
    '        dblInterestAmt = (mdblFinalBalanceAmt * Rate) / 100

    '        mdblFinalBalanceAmt = (mdblFinalBalanceAmt + dblInterestAmt)

    '        'due balance calculations for partial payments of saving
    '        If mintRefSelectedIndex = 0 Then
    '            dblReceived = 0
    '            mdblFinalBalanceAmt = mdblFinalBalanceAmt - dblReceived
    '        Else
    '            mdblFinalBalanceAmt = mdblFinalBalanceAmt - dblReceived
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FinalRedemptionAmt", mstrModuleName)
    '    End Try

    'End Sub
    Private Sub FinalRepaymentAmt()

        Try

            'If mintPayTypeId = enPayTypeId.REPAYMENT Then
            ' mdtMaturityDate = CDate(objEmployee_Saving._Maturitydate)
            'mdblFinalBalanceAmt = cdec(objEmployee_Saving._Maturity_Amou)
            'End If

            If menAction = enAction.EDIT_ONE Then
                objPaymentTran.Get_Payment_Total(mintTranUnkid, mintReferenceUnkid, mintPayTypeId, decReceived, False, True)
                txtBalanceDue.Text = Format(mdecBalanceAmt + decReceived, GUI.fmtCurrency) 'Sohail (04 Mar 2011)
                txtBalanceDue.Tag = CDec(mdecBalanceAmt + decReceived) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
            Else
                objPaymentTran.Get_Payment_Total(mintTranUnkid, mintReferenceUnkid, mintPayTypeId, decReceived)
                txtBalanceDue.Text = Format((mdecBalanceAmt + decReceived) - decReceived, GUI.fmtCurrency)
                txtBalanceDue.Tag = CDec((mdecBalanceAmt + decReceived) - decReceived) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
            End If


            'If mintRefSelectedIndex = 0 Then
            '    ' dblReceived = 0
            '    mdblFinalBalanceAmt = mdblBalanceAmt + dblReceived
            'Else
            '    mdblFinalBalanceAmt = mdblBalanceAmt
            'End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FinalRepaymentAmt", mstrModuleName)
        End Try

    End Sub

    'Private Sub FillList()
    '    Try
    '        Dim dblTotal As Double = 0
    '        lvPayment.Items.Clear()
    '        Dim lvItem As ListViewItem
    '        For Each dtRow As DataRow In mdtTran.Rows
    '            If CStr(IIf(IsDBNull(dtRow.Item("AUD")), "A", dtRow.Item("AUD"))) <> "D" Then
    '                lvItem = New ListViewItem
    '                lvItem.Text = dtRow.Item("voucherno").ToString  'Voucher No
    '                cboPaymentMode.SelectedValue = CInt(dtRow.Item("paymentmodeid").ToString)
    '                lvItem.SubItems.Add(cboPaymentMode.Text.ToString)    'Payment Mode

    '                Dim objBank As New clspayrollgroup_master
    '                Dim objBranch As New clsbankbranch_master
    '                objBank._Groupmasterunkid = CInt(IIf(IsDBNull(dtRow.Item("BankUnkid")), 0, dtRow.Item("BankUnkid")))
    '                objBranch._Branchunkid = CInt(dtRow.Item("branchunkid"))
    '                lvItem.SubItems.Add(objBank._Groupname.ToString)    'Bank Group
    '                lvItem.SubItems.Add(objBranch._Branchname.ToString)    'Branch
    '                lvItem.SubItems.Add(dtRow.Item("chequeno").ToString)    'Cheque

    '                cboPaymentBy.SelectedValue = CInt(dtRow.Item("paymentbyid").ToString)

    '                lvItem.SubItems.Add(cboPaymentBy.Text.ToString)    'Payment By
    '                lvItem.SubItems.Add(dtRow.Item("percentage").ToString)    'Percentage
    '                lvItem.SubItems.Add(dtRow.Item("amount").ToString)    'Amount
    '                lvItem.SubItems.Add(dtRow.Item("paymentmodeid").ToString)    'PayModeUnkid
    '                lvItem.SubItems.Add(dtRow.Item("paymentbyid").ToString)    'PayByUnkid
    '                lvItem.SubItems.Add(dtRow.Item("BankUnkid").ToString)    'BankUnkid
    '                lvItem.SubItems.Add(dtRow.Item("branchunkid").ToString)    'BranchUnkid
    '                lvItem.SubItems.Add(dtRow.Item("GUID").ToString)    'GUID

    '                lvItem.Tag = dtRow.Item("paymenttranunkid")

    '                dblTotal += cdec(dtRow.Item("amount"))

    '                lvPayment.Items.Add(lvItem)

    '                lvItem = Nothing

    '            End If
    '        Next
    '        txtTotal.Text = CStr(dblTotal)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub ResetValue()
    '    Try
    '        txtAgainstVoucher.Text = ""
    '        txtAmount.Text = ""
    '        txtChequeNo.Text = ""
    '        txtValue.Text = ""
    '        cboBankGroup.SelectedValue = 0
    '        cboBranch.SelectedValue = 0
    '        cboPaymentBy.SelectedValue = 0
    '        cboPaymentMode.SelectedValue = 0
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ResetValue", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "
    Private Sub frmPayment_AddEdit_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objEmployee = New clsEmployee_Master
        objPaymentTran = New clsPayment_tran
        objMaster = New clsMasterData
        objCommon = New clscommom_period_Tran

        Try
            Call Set_Logo(Me, gApplicationType)

            Call Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call SetColor()

            'Nilay (25-Mar-2016) -- Start
            mdtPeriodStartDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            mdtPeriodEndDate = eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)
            'Nilay (25-Mar-2016) -- End

            Call FillCombo()
            Call SetVisbibility()

            Select Case mintReferenceUnkid
                Case 1, 2       'Loan / Advance
                    objLoan_Advance = New clsLoan_Advance
                    objLoan_Master = New clsLoan_Scheme
                    objLoan_Advance._Loanadvancetranunkid = mintTranUnkid

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmployee._Employeeunkid = objLoan_Advance._Employeeunkid
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objLoan_Advance._Employeeunkid
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Nilay (01-Apr-2016) -- Start
                    'ENHANCEMENT - Approval Process in Loan Other Operations
                    mintEmployeeunkid = objLoan_Advance._Employeeunkid
                    'Nilay (01-Apr-2016) -- End

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objCommon._Periodunkid = objLoan_Advance._Periodunkid
                    'Sohail (29 Apr 2017) -- Start
                    'CCK Issue - 66.1 - entry was not getting inserted in loan balance tran table for the employees which were not active on loan start period date due to active employee concept changes and loan start period is assigned.
                    'objCommon._Periodunkid(FinancialYear._Object._DatabaseName) = objLoan_Advance._Periodunkid
                    'Sohail (24 Jun 2017) -- Start
                    'Issue - 68.1 - There is no row at position 0.
                    'objCommon._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                    'Sohail (30 Jan 2018) -- Start
                    'TRA - issue #0001938: Loan balance issue still continues (written off loan)TRA Issue - 70.1 - entry was not getting inserted in loan balance tran table for the employees which were not active on loan start period date due to active employee concept changes and loan start period is assigned in 70.1.
                    'objCommon._Periodunkid(FinancialYear._Object._DatabaseName) = objLoan_Advance._Periodunkid
                    If mstrLoan_Status_Data.Trim.Length > 0 Then
                        Dim StrStatus() As String = mstrLoan_Status_Data.Split(CChar("|"))
                        objCommon._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(StrStatus(8))
                    Else
                    objCommon._Periodunkid(FinancialYear._Object._DatabaseName) = objLoan_Advance._Periodunkid
                    End If
                    'Sohail (30 Jan 2018) -- End
                    'Sohail (24 Jun 2017) -- End
                    'Sohail (29 Apr 2017) -- End
                    'Sohail (21 Aug 2015) -- End
                    objLoan_Master._Loanschemeunkid = objLoan_Advance._Loanschemeunkid

                    'Sohail (15 Dec 2015) -- Start
                    'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                    dtpPaymentDate.Enabled = True
                    If mintPayTypeId = clsPayment_tran.enPayTypeId.RECEIVED Then
                        'Sohail (14 Mar 2017) -- Start
                        'PACRA Enhancement - 65.1 - New Interest Calculation type "By Tenure" for PACRA.
                        'If objLoan_Advance._Interest_Calctype_Id = enLoanInterestCalcType.MONTHLY Then
                        'Sohail (23 May 2017) -- Start
                        'Telematics Enhancement - 66.1 - New Interest Calculation type "Simple Interest" for Telematics.
                        'If objLoan_Advance._Interest_Calctype_Id = enLoanInterestCalcType.MONTHLY OrElse objLoan_Advance._Interest_Calctype_Id = enLoanInterestCalcType.BY_TENURE Then
                        If objLoan_Advance._Interest_Calctype_Id = enLoanInterestCalcType.MONTHLY OrElse objLoan_Advance._Interest_Calctype_Id = enLoanInterestCalcType.BY_TENURE OrElse objLoan_Advance._Interest_Calctype_Id = enLoanInterestCalcType.SIMPLE_INTEREST Then
                            'Sohail (23 May 2017) -- End
                            'Sohail (14 Mar 2017) -- End
                            dtpPaymentDate.Enabled = False
                        End If
                    End If

                    'Sohail (15 Dec 2015) -- End

                Case 3          'Payslip
                    objPayslip = New clsTnALeaveTran
                    'Sohail (02 Jan 2017) -- Start
                    'TRA Enhancement - 64.1 - Performance enhancement in process payroll, and global void payment.
                    'objPayslip._Tnaleavetranunkid = mintTranUnkid
                    objPayslip._Tnaleavetranunkid(Nothing) = mintTranUnkid
                    'Sohail (02 Jan 2017) -- End

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmployee._Employeeunkid = objPayslip._Employeeunkid
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objPayslip._Employeeunkid
                    'S.SANDEEP [04 JUN 2015] -- END


                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objCommon._Periodunkid = objPayslip._Payperiodunkid
                    objCommon._Periodunkid(FinancialYear._Object._DatabaseName) = objPayslip._Payperiodunkid
                    'Sohail (21 Aug 2015) -- End
                    lblPayment.Visible = True
                    cboPaymentBy.SelectedValue = 1
                    'Sohail (21 Apr 2020) -- Start
                    'CCBRT Enhancement # 0004659 : Assist to Enable to do Partial Payment by using Both modes of Payment i.e cheque,cash and Transfe.
                    'cboPaymentBy.Enabled = False
                    'txtValue.Enabled = False
                    'txtAmount.ReadOnly = True
                    cboPaymentBy.Enabled = True
                    txtValue.Enabled = True
                    txtAmount.ReadOnly = False
                    'Sohail (21 Apr 2020) -- End

                Case 4          'Savings

                    objEmployee_Saving = New clsSaving_Tran
                    objSaving_Master = New clsSavingScheme
                    objSavingStatus = New clsSaving_Status_Tran

                    objEmployee_Saving._Savingtranunkid = mintTranUnkid

                    'S.SANDEEP [04 JUN 2015] -- START
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objEmployee._Employeeunkid = objEmployee_Saving._Employeeunkid
                    objEmployee._Employeeunkid(eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate)) = objEmployee_Saving._Employeeunkid
                    'S.SANDEEP [04 JUN 2015] -- END

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objCommon._Periodunkid = objEmployee_Saving._Payperiodunkid
                    objCommon._Periodunkid(FinancialYear._Object._DatabaseName) = objEmployee_Saving._Payperiodunkid
                    'Sohail (21 Aug 2015) -- End
                    objSaving_Master._Savingschemeunkid = objEmployee_Saving._Savingschemeunkid
                    mdtScheme_EffDate = CDate(objEmployee_Saving._Effectivedate)
                    'Sohail (12 Dec 2015) -- Start
                    'Enhancement - Provide Deposit feaure in Employee Saving.
                    'mdecBalanceAmt = CDec(objEmployee_Saving._Balance_Amount) 'Sohail (11 May 2011)
                    If mintPayTypeId = clsPayment_tran.enPayTypeId.DEPOSIT Then
                        mdecBalanceAmt = 0
                    Else
                        mdecBalanceAmt = CDec(objEmployee_Saving._Balance_Amount) 'Sohail (11 May 2011)
                    End If
                    'Sohail (12 Dec 2015) -- End
            End Select

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            mdtPeriodStartDate = objCommon._Start_Date
            mdtPeriodEndDate = objCommon._End_Date
            'Sohail (21 Aug 2015) -- End

            'Call SetColor()
            Call FillInfo()
            'Call FillCombo()
            'Call SetVisbibility()
            If menAction = enAction.EDIT_ONE Then
                objPaymentTran._Paymenttranunkid = mintPaymentTranId
                cboPaymentMode.Enabled = False
                cboPaymentBy.Enabled = False
                'objPaymentTran.mdblPreviousAmount = cdec(txtAmount.Text)
                cboCurrency.Enabled = False 'Sohail (08 Oct 2011)
            End If

            'Anjan (25 Mar 2011)-Start
            If menAction = enAction.ADD_ONE Then
                'Sohail (17 Jun 2011) -- Start
                'Issue : can't change payment date.
                'dtpPaymentDate.MaxDate = ConfigParameter._Object._CurrentDateAndTime
                dtpPaymentDate.MaxDate = FinancialYear._Object._Database_End_Date
                'Sohail (17 Jun 2011) -- End
                'dtpPaymentDate.MinDate = ConfigParameter._Object._CurrentDateAndTime
            ElseIf menAction = enAction.EDIT_ONE Then
                dtpPaymentDate.MaxDate = objPaymentTran._Paymentdate
                dtpPaymentDate.MinDate = objPaymentTran._Paymentdate
            End If
            'Anjan (25 Mar 2011)-End

            Select Case mintReferenceUnkid
                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                    If mintPayTypeId = clsPayment_tran.enPayTypeId.PAYMENT Then
                        If menAction = enAction.EDIT_ONE Then
                            objPaymentTran.Get_Payment_Total(mintTranUnkid, mintReferenceUnkid, mintPayTypeId, decReceived, , True)
                            If mintRefSelectedIndex = 0 Then
                                decReceived = 0
                                txtBalanceDue.Text = CStr(txtBalanceDue.Decimal - decReceived) 'Sohail (11 May 2011)
                                txtBalanceDue.Tag = CDec(txtBalanceDue.Decimal - decReceived) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
                            Else
                                txtBalanceDue.Text = CStr((txtBalanceDue.Decimal + decReceived) - decReceived) 'Sohail (11 May 2011)
                                txtBalanceDue.Tag = CDec(txtBalanceDue.Decimal + decReceived) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
                            End If

                        Else
                            objPaymentTran.Get_Payment_Total(mintTranUnkid, mintReferenceUnkid, mintPayTypeId, decReceived)
                            txtBalanceDue.Text = CStr(txtBalanceDue.Decimal - decReceived) 'Sohail (11 May 2011)
                            txtBalanceDue.Tag = ((txtBalanceDue.Decimal + decReceived) - decReceived) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
                        End If


                    Else

                        If menAction = enAction.EDIT_ONE Then
                            objPaymentTran.Get_Payment_Total(mintTranUnkid, mintReferenceUnkid, mintPayTypeId, decReceived, True, True)
                            If mintRefSelectedIndex = 0 Then
                                decReceived = 0
                                txtBalanceDue.Text = CStr(txtBalanceDue.Decimal - decReceived) 'Sohail (11 May 2011)
                                txtBalanceDue.Tag = CDec(txtBalanceDue.Decimal - decReceived) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
                            Else
                                txtBalanceDue.Text = CStr(txtBalanceDue.Decimal + decReceived) 'Sohail (11 May 2011)
                                txtBalanceDue.Tag = CDec(txtBalanceDue.Decimal + decReceived) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
                            End If
                        Else
                            objPaymentTran.Get_Payment_Total(mintTranUnkid, mintReferenceUnkid, mintPayTypeId, decReceived, True)
                            txtBalanceDue.Text = CStr((txtBalanceDue.Decimal + decReceived) - decReceived) 'Sohail (11 May 2011)
                            txtBalanceDue.Tag = ((txtBalanceDue.Decimal + decReceived) - decReceived) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
                        End If
                    End If
                Case clsPayment_tran.enPaymentRefId.PAYSLIP

                    If menAction = enAction.EDIT_ONE Then
                        objPaymentTran.Get_Payment_Total(mintTranUnkid, mintReferenceUnkid, mintPayTypeId, decReceived, True, True)
                        If mintRefSelectedIndex = 0 Then
                            decReceived = 0
                            txtBalanceDue.Text = CStr(txtBalanceDue.Decimal - decReceived) 'Sohail (11 May 2011)
                            txtBalanceDue.Tag = CDec(txtBalanceDue.Decimal - decReceived) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
                        Else
                            txtBalanceDue.Text = CStr(txtBalanceDue.Decimal - decReceived) 'Sohail (11 May 2011)
                            txtBalanceDue.Tag = CDec(txtBalanceDue.Decimal - decReceived) 'Sohail (16 Oct 2010), 'Sohail (11 May 2011)
                        End If
                    End If

                Case clsPayment_tran.enPaymentRefId.SAVINGS
                    'Sohail (12 Jan 2015) -- Start
                    'Saving Redesign - Allow to change Contribution and Interest Rate And allow to Withdraw in Saving.
                    'If mintPayTypeId = clsPayment_tran.enPayTypeId.REPAYMENT Then
                    '    Call FinalRepaymentAmt()
                    'End If
                    'Sohail (21 Nov 2015) -- Start
                    'Enhancement - Provide Deposit feaure in Employee Saving.
                    'If mintPayTypeId = clsPayment_tran.enPayTypeId.REPAYMENT OrElse mintPayTypeId = clsPayment_tran.enPayTypeId.WITHDRAWAL Then
                    If mintPayTypeId = clsPayment_tran.enPayTypeId.REPAYMENT OrElse mintPayTypeId = clsPayment_tran.enPayTypeId.WITHDRAWAL OrElse mintPayTypeId = clsPayment_tran.enPayTypeId.DEPOSIT Then
                        'Sohail (21 Nov 2015) -- End
                        Call FinalRepaymentAmt()
                    End If
                    'Sohail (12 Jan 2015) -- End

            End Select

            'Sohail (08 Oct 2011) -- Start
            mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag)
            'Sohail (22 Oct 2013) -- Start
            'TRA - ENHANCEMENT
            objlblMessage.Text = ""
            'Sohail (17 Mar 2020) -- Start
            'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
            'If mintReferenceUnkid = clsPayment_tran.enPaymentRefId.PAYSLIP Then
            '    Dim decRondingMultile As Decimal = 0
            '    Select Case CInt(ConfigParameter._Object._PaymentRoundingMultiple)
            '        Case enPaymentRoundingMultiple._1
            '            decRondingMultile = 1
            '        Case enPaymentRoundingMultiple._5
            '            decRondingMultile = 5
            '        Case enPaymentRoundingMultiple._10
            '            decRondingMultile = 10
            '        Case enPaymentRoundingMultiple._50
            '            decRondingMultile = 50
            '        Case enPaymentRoundingMultiple._100
            '            decRondingMultile = 100
            '            'Sohail (08 Apr 2016) -- Start
            '            'Issue - 58.1 - Allow Rounding to 200 for Payslip Payment Rounding Multiple option for Mabibo.
            '        Case enPaymentRoundingMultiple._200
            '            decRondingMultile = 200
            '            'Sohail (08 Apr 2016) -- End
            '        Case enPaymentRoundingMultiple._500
            '            decRondingMultile = 500
            '        Case enPaymentRoundingMultiple._1000
            '            decRondingMultile = 1000
            '    End Select
            '    Select Case CInt(ConfigParameter._Object._PaymentRoundingType)
            '        Case enPaymentRoundingType.NONE
            '            mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag)
            '            objlblMessage.Text = ""
            '        Case enPaymentRoundingType.AUTOMATIC
            '            If decRondingMultile / 2 > CDec(txtBalanceDue.Tag) Mod decRondingMultile Then
            '                mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag) - CDec(CDec(txtBalanceDue.Tag) Mod decRondingMultile)
            '            Else
            '                If CDec(CDec(txtBalanceDue.Tag) Mod decRondingMultile) <> 0 Then
            '                    mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag) - CDec(CDec(txtBalanceDue.Tag) Mod decRondingMultile) + decRondingMultile
            '                End If
            '            End If
            '            objlblMessage.Text = "Payment Rounding Type : Automatic, Rounding To : " & decRondingMultile.ToString
            '        Case enPaymentRoundingType.UP
            '            If CDec(CDec(txtBalanceDue.Tag) Mod decRondingMultile) <> 0 Then
            '                mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag) - CDec(CDec(txtBalanceDue.Tag) Mod decRondingMultile) + decRondingMultile
            '            End If
            '            objlblMessage.Text = "Payment Rounding Type : Up, Rounding To : " & decRondingMultile.ToString
            '        Case enPaymentRoundingType.DOWN
            '            mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag) - CDec(CDec(txtBalanceDue.Tag) Mod decRondingMultile)
            '            objlblMessage.Text = "Payment Rounding Type : Down, Rounding To : " & decRondingMultile.ToString
            '    End Select
            'End If
            'Sohail (17 Mar 2020) -- End
            'Sohail (22 Oct 2013) -- End
            Call cboCurrency_SelectedIndexChanged(Nothing, Nothing)
            'Sohail (17 Mar 2020) -- Start
            'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
            'If mdecBaseExRate <> 0 Then
            '    mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
            'Else
            '    mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate
            'End If
            mdecPaidCurrBalFormatedAmt = CDec(Format(mdecPaidCurrBalanceAmt, GUI.fmtCurrency))
            'Sohail (17 Mar 2020) -- End
            If menAction <> enAction.EDIT_ONE Then
                'cboCurrency.SelectedIndex = 0 'Sohail (07 May 2015) 
                txtAmount.Text = Format(mdecPaidCurrBalFormatedAmt, GUI.fmtCurrency)
            End If
            'Sohail (08 Oct 2011) -- End

            Call GetValue()

            'Sohail (15 Dec 2015) -- Start
            'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
            If menAction <> enAction.EDIT_ONE Then
                If mintBaseCountryId > 0 Then
                    cboCurrency.SelectedValue = mintBaseCountryId
                Else
                    cboCurrency.SelectedIndex = 0
                End If
                txtAmount.Text = Format(mdecPaidCurrBalFormatedAmt, GUI.fmtCurrency)
            End If
            'Sohail (15 Dec 2015) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayment_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayment_AddEdit_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objPaymentTran = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayment_AddEdit_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayment_AddEdit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayment_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmPayment_AddEdit_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPayment_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    'Sohail (10 Jul 2014) -- Start
    'Enhancement - Custom Language.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsPayment_authorize_tran.SetMessages()
            objfrm._Other_ModuleNames = "clsPayment_authorize_tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Sohail (10 Jul 2014) -- End
#End Region

#Region " Buttons "
    'Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If txtAgainstVoucher.Text.Trim = "" Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Voucher No. cannot be blank. Voucher No. is compulsory information."))
    '            txtAgainstVoucher.Focus()
    '            Exit Sub
    '        End If

    '        If CInt(cboPaymentMode.SelectedValue) > 0 Then
    '            Select Case CInt(cboPaymentMode.SelectedValue)
    '                Case 2
    '                    If CInt(cboBankGroup.SelectedValue) <= 0 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Bank Group is compulsory infomation. Please select Bank Group to continue."))
    '                        Exit Sub
    '                    End If

    '                    If CInt(cboBranch.SelectedValue) <= 0 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Bank Branch is compulsory infomation. Please select Bank Branch to continue."))
    '                        Exit Sub
    '                    End If

    '                    If txtChequeNo.Text.Trim = "" Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Cheque No. cannot be blank. Cheque No. is compulsory information."))
    '                        txtChequeNo.Focus()
    '                        Exit Sub
    '                    End If
    '                Case 3
    '                    If CInt(cboBankGroup.SelectedValue) <= 0 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Bank Group is compulsory infomation. Please select Bank Group to continue."))
    '                        Exit Sub
    '                    End If
    '                    If CInt(cboBranch.SelectedValue) <= 0 Then
    '                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Bank Branch is compulsory infomation. Please select Bank Branch to continue."))
    '                        Exit Sub
    '                    End If
    '            End Select
    '        Else
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Payment Mode is compulsory infomation. Please select Payment Mode to continue."))
    '            cboPaymentMode.Focus()
    '            Exit Sub
    '        End If

    '        If CInt(cboPaymentBy.SelectedValue) <= 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Payment By is compulsory infomation. Please select Payment By to continue."))
    '            cboPaymentBy.Focus()
    '            Exit Sub
    '        End If

    '        Dim dtRow As DataRow() = mdtTran.Select("voucherno = '" & txtAgainstVoucher.Text & "' AND AUD <> 'D' ")

    '        If dtRow.Length > 0 Then
    '            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 7, "Voucher No. is already added to the list for Payment."), enMsgBoxStyle.Information)
    '            Exit Sub
    '        End If

    '        Dim dtPayRow As DataRow
    '        dtPayRow = mdtTran.NewRow

    '        dtPayRow.Item("paymenttranunkid") = -1
    '        dtPayRow.Item("voucherno") = txtAgainstVoucher.Text
    '        dtPayRow.Item("periodunkid") = objLoan_Advance._Periodunkid
    '        dtPayRow.Item("paymentdate") = dtpPaymentDate.Value
    '        dtPayRow.Item("employeeunkid") = objEmployee._Employeeunkid
    '        dtPayRow.Item("paymentrefid") = mintReferenceUnkid
    '        dtPayRow.Item("paymentmodeid") = cboPaymentMode.SelectedValue
    '        dtPayRow.Item("branchunkid") = cboBranch.SelectedValue
    '        dtPayRow.Item("chequeno") = txtChequeNo.Text
    '        dtPayRow.Item("paymentbyid") = cboPaymentBy.SelectedValue
    '        dtPayRow.Item("percentage") = cdec(IIf(txtValue.Text.Trim = "", 0, (txtValue.Text.Trim)))
    '        dtPayRow.Item("amount") = cdec(IIf(txtAmount.Text.Trim = "", 0, (txtAmount.Text.Trim)))
    '        dtPayRow.Item("voucherref") = txtVoucher.Text
    '        dtPayRow.Item("payreftranunkid") = mintTranUnkid
    '        dtPayRow.Item("BankUnkid") = cboBankGroup.SelectedValue
    '        dtPayRow.Item("AUD") = "A"
    '        dtPayRow.Item("GUID") = Guid.NewGuid().ToString

    '        mdtTran.Rows.Add(dtPayRow)

    '        Call FillList()

    '        Call ResetValue()

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnAdd_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If lvPayment.SelectedItems.Count > 0 Then
    '            If mintSelectedIndex > -1 Then
    '                Dim drTemp As DataRow()
    '                If CInt(lvPayment.Items(mintSelectedIndex).Tag) = -1 Then
    '                    drTemp = mdtTran.Select("GUID = '" & lvPayment.Items(mintSelectedIndex).SubItems(objcolhGUID.Index).Text & "'")
    '                Else
    '                    drTemp = mdtTran.Select("paymenttranunkid = " & CInt(lvPayment.Items(mintSelectedIndex).Tag))
    '                End If
    '                If drTemp.Length > 0 Then
    '                    With drTemp(0)
    '                        .Item("voucherno") = txtAgainstVoucher.Text
    '                        .Item("periodunkid") = objLoan_Advance._Periodunkid
    '                        .Item("paymentdate") = dtpPaymentDate.Value
    '                        .Item("employeeunkid") = objEmployee._Employeeunkid
    '                        .Item("paymentrefid") = mintReferenceUnkid
    '                        .Item("paymentmodeid") = cboPaymentMode.SelectedValue
    '                        .Item("branchunkid") = cboBranch.SelectedValue
    '                        .Item("chequeno") = txtChequeNo.Text
    '                        .Item("paymentbyid") = cboPaymentBy.SelectedValue
    '                        .Item("percentage") = IIf(txtValue.Text.Trim = "", 0, (txtValue.Text.Trim))
    '                        .Item("amount") = IIf(txtAmount.Text.Trim = "", 0, (txtAmount.Text.Trim))
    '                        .Item("voucherref") = txtVoucher.Text
    '                        .Item("payreftranunkid") = mintTranUnkid
    '                        .Item("BankUnkid") = cboBankGroup.SelectedValue
    '                        If IsDBNull(.Item("AUD")) Or CStr(.Item("AUD")).ToString.Trim = "" Then
    '                            .Item("AUD") = "U"
    '                        End If
    '                        .Item("GUID") = Guid.NewGuid().ToString
    '                        .AcceptChanges()
    '                    End With
    '                    Call FillList()
    '                End If
    '            End If
    '            Call ResetValue()
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        If mintSelectedIndex > -1 Then
    '            Dim drTemp As DataRow()
    '            If CInt(lvPayment.Items(mintSelectedIndex).Tag) = -1 Then
    '                drTemp = mdtTran.Select("GUID = '" & lvPayment.Items(mintSelectedIndex).SubItems(objcolhGUID.Index).Text & "'")
    '            Else
    '                drTemp = mdtTran.Select("paymenttranunkid = '" & CInt(lvPayment.Items(mintSelectedIndex).Tag) & "'")
    '            End If
    '            If drTemp.Length > 0 Then
    '                drTemp(0).Item("AUD") = "D"
    '                Call FillList()
    '            End If
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValidate() = False Then
                Exit Sub
            End If

            'Sohail (17 Jun 2011) -- Start
            'Issue : can't change payment date.
            'If dtpPaymentDate.Value > ConfigParameter._Object._CurrentDateAndTime Then
            '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 118, "Payment date cannot be greater than system date."), enMsgBoxStyle.Information)
            '    Exit Sub
            'End If
            'Sohail (17 Jun 2011) -- End

            Call SetValue()
            'Sohail (18 Dec 2010) -- Start

            'Sohail (23 May 2017) -- Start
            'Enhancement - 67.1 - Link budget with Payroll.
            Dim dtActAdj As New DataTable
            If menAction <> enAction.EDIT_ONE AndAlso mintReferenceUnkid = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                Dim objBudget As New clsBudget_MasterNew
                Dim intBudgetId As Integer = 0
                Dim dsList As DataSet
                dsList = objBudget.GetComboList("List", False, True, )
                If dsList.Tables(0).Rows.Count > 0 Then
                    intBudgetId = CInt(dsList.Tables(0).Rows(0).Item("budgetunkid"))
                End If
                If intBudgetId > 0 Then
                    Dim mstrAnalysis_Fields As String = ""
                    Dim mstrAnalysis_Join As String = ""
                    Dim mstrAnalysis_OrderBy As String = "hremployee_master.employeeunkid"
                    Dim mstrReport_GroupName As String = ""
                    Dim mstrAnalysis_TableName As String = ""
                    Dim mstrAnalysis_CodeField As String = ""
                    objBudget._Budgetunkid = intBudgetId
                    Dim strEmpList As String = objPaymentTran._Employeeunkid.ToString()
                    Dim objBudgetCodes As New clsBudgetcodes_master
                    Dim dsEmp As DataSet = Nothing
                    If objBudget._Viewbyid = enBudgetViewBy.Allocation Then
                        Dim objEmp As New clsEmployee_Master
                        Dim objBudget_Tran As New clsBudget_TranNew
                        Dim mstrAllocationTranUnkIDs As String = objBudget_Tran.GetAllocationTranUnkIDs(intBudgetId)

                        frmViewAnalysis.GetAnalysisByDetails("frmViewAnalysis", objBudget._Allocationbyid, mstrAllocationTranUnkIDs, objBudget._Budget_date, "", mstrAnalysis_Fields, mstrAnalysis_Join, mstrAnalysis_OrderBy, "", mstrReport_GroupName, mstrAnalysis_TableName, mstrAnalysis_CodeField)

                        dsList = objBudgetCodes.GetEmployeeActivityPercentage(objPaymentTran._Periodunkid, intBudgetId, 0, 0, mstrAllocationTranUnkIDs, 1)
                        dsEmp = objEmp.GetList(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, "List", , , , "hremployee_master.employeeunkid IN (" & strEmpList & ")")
                    Else
                        dsList = objBudgetCodes.GetEmployeeActivityPercentage(objPaymentTran._Periodunkid, intBudgetId, 0, 0, strEmpList, 1)
                    End If

                    If dsList.Tables(0).Rows.Count > 0 Then
                        dtActAdj.Columns.Add("fundactivityunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
                        dtActAdj.Columns.Add("transactiondate", System.Type.GetType("System.String")).DefaultValue = ""
                        dtActAdj.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
                        dtActAdj.Columns.Add("isexeed", System.Type.GetType("System.Boolean")).DefaultValue = False
                        dtActAdj.Columns.Add("Activity Code", System.Type.GetType("System.String")).DefaultValue = ""
                        dtActAdj.Columns.Add("Current Balance", System.Type.GetType("System.Decimal")).DefaultValue = 0
                        dtActAdj.Columns.Add("Actual Salary", System.Type.GetType("System.Decimal")).DefaultValue = 0

                        Dim mdicActivity As New Dictionary(Of Integer, Decimal)
                        Dim mdicActivityCode As New Dictionary(Of Integer, String)
                        Dim objActivity As New clsfundactivity_Tran
                        Dim dsAct As DataSet = objActivity.GetList("Activity")
                        Dim objActAdjust As New clsFundActivityAdjustment_Tran
                        Dim dsActAdj As DataSet = objActAdjust.GetLastCurrentBalance("List", 0, mdtPeriodEndDate)
                        mdicActivity = (From p In dsActAdj.Tables("List") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Total = CDec(p.Item("newbalance"))}).ToDictionary(Function(x) x.Id, Function(y) y.Total)
                        mdicActivityCode = (From p In dsAct.Tables("Activity") Select New With {.Id = CInt(p.Item("fundactivityunkid")), .Code = p.Item("activity_code").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Code)
                        'Dim dsBalance As DataSet = objTnALeave.Get_Balance_List(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, mdtPayPeriodStartDate, mdtPayPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, False, True, "prtnaleave_tran.balanceamount > 0", "list", strEmpList, CInt(cboPayPeriod.SelectedValue), "")
                        If objBudget._Viewbyid = enBudgetViewBy.Allocation AndAlso dsEmp IsNot Nothing Then
                            'mdtTable.PrimaryKey = New DataColumn() {mdtTable.Columns("employeeunkid")}
                            'mdtTable.Merge(dsEmp.Tables(0))
                        End If
                        Dim intActivityId As Integer
                        Dim decTotal As Decimal = 0
                        Dim decActBal As Decimal = 0
                        Dim blnShowValidationList As Boolean = False
                        Dim blnIsExeed As Boolean = False
                        For Each pair In mdicActivityCode
                            intActivityId = pair.Key
                            decTotal = 0
                            decActBal = 0
                            blnIsExeed = False

                            If mdicActivity.ContainsKey(intActivityId) = True Then
                                decActBal = mdicActivity.Item(intActivityId)
                            End If

                            Dim lstRow As List(Of DataRow) = (From p In dsList.Tables(0) Where (CInt(p.Item("fundactivityunkid")) = intActivityId AndAlso CDec(p.Item("percentage")) > 0) Select (p)).ToList
                            If lstRow.Count > 0 Then
                                For Each dRow As DataRow In lstRow
                                    Dim intAllocUnkId As Integer = CInt(dRow.Item("Employeeunkid"))
                                    Dim decNetPay As Decimal = objPaymentTran._Amount
                                    decTotal += decNetPay * CDec(dRow.Item("percentage")) / 100
                                Next

                                If decTotal > decActBal Then
                                    blnShowValidationList = True
                                    blnIsExeed = True
                                End If
                                Dim dr As DataRow = dtActAdj.NewRow
                                dr.Item("fundactivityunkid") = intActivityId
                                dr.Item("transactiondate") = eZeeDate.convertDate(mdtPeriodEndDate).ToString()
                                dr.Item("remark") = Language.getMessage(mstrModuleName, 41, "Salary payment for the period of") & " " & txtPayPeriod.Text
                                dr.Item("isexeed") = blnIsExeed
                                dr.Item("Activity Code") = pair.Value
                                dr.Item("Current Balance") = Format(decActBal, GUI.fmtCurrency)
                                dr.Item("Actual Salary") = Format(decTotal, GUI.fmtCurrency)
                                dtActAdj.Rows.Add(dr)
                            End If
                        Next

                        If blnShowValidationList = True Then
                            Dim dr() As DataRow = dtActAdj.Select("isexeed = 0 ")
                            For Each r In dr
                                dtActAdj.Rows.Remove(r)
                            Next
                            dtActAdj.AcceptChanges()
                            dtActAdj.Columns.Remove("fundactivityunkid")
                            dtActAdj.Columns.Remove("transactiondate")
                            dtActAdj.Columns.Remove("remark")
                            dtActAdj.Columns.Remove("isexeed")
                            Dim objValid As New frmCommonValidationList
                            objValid.displayDialog(False, Language.getMessage(mstrModuleName, 42, "Sorry, Actual Salary Payment will be exceeding to the Activity Current Balance."), dtActAdj)
                            Exit Try
                        End If
                    End If
                End If
            End If
            'Sohail (23 May 2017) -- End

            If ConfigParameter._Object._IsDenominationCompulsory = True AndAlso User._Object.Privilege._AddCashDenomination = True Then 'Anjan (12 Feb 2011)-Start 'Issue : Included privilege for the adding cash denomination.
                If mintReferenceUnkid = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                    If txtAmount.Decimal > 0 AndAlso (CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE) Then 'Sohail (11 May 2011)
                        If menAction = enAction.ADD_ONE Then
                            Dim frm As New frmCashDenomination
                            'Sohail (12 Oct 2011) -- Start
                            'If frm.displayDialog(-1, objPaymentTran._Periodunkid, objPaymentTran._Employeeunkid.ToString, 100, enAction.ADD_ONE) = False Then

                            'Sohail (03 Sep 2012) -- Start
                            'TRA - ENHANCEMENT
                            'If frm.displayDialog(-1, objPaymentTran._Periodunkid, objPaymentTran._Employeeunkid.ToString, 100, enAction.ADD_ONE, CInt(cboCurrency.SelectedValue), objPaymentTran._Baseexchangerate, objPaymentTran._Expaidrate) = False Then
                            '    'Sohail (12 Oct 2011) -- End
                            '    Exit Sub
                            'End If

                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'If frm.displayDialog(-1, objPaymentTran._Periodunkid, objPaymentTran._Employeeunkid.ToString, 100, enAction.ADD_ONE, mintPaidCurrId, objPaymentTran._Baseexchangerate, objPaymentTran._Expaidrate, CInt(cboCurrency.SelectedValue)) = False Then
                            If frm.displayDialog(-1, objPaymentTran._Periodunkid, objPaymentTran._Employeeunkid.ToString, 100, enAction.ADD_ONE, mdtPeriodStartDate, mdtPeriodEndDate, mintPaidCurrId, objPaymentTran._Baseexchangerate, objPaymentTran._Expaidrate, CInt(cboCurrency.SelectedValue)) = False Then
                                'Sohail (21 Aug 2015) -- End
                                Exit Sub
                            End If
                            'Sohail (03 Sep 2012) -- End


                            dtDenometable = frm._CashDataSource
                            objPaymentTran._Denom_Table = dtDenometable
                        End If
                    End If
                End If
            End If
            'Sohail (18 Dec 2010) -- End

            If menAction = enAction.EDIT_ONE Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objPaymentTran.Update()

                'Sohail (15 Dec 2015) -- Start
                'Enhancement - New Loan Calculation type Reducing balance with Fixed Principal EMI and new interest calculation Monthly apart from Daily for KBC.
                'blnFlag = objPaymentTran.Update(ConfigParameter._Object._CurrentDateAndTime)

                'Nilay (25-Mar-2016) -- Start
                'blnFlag = objPaymentTran.Update(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, _
                '                                ConfigParameter._Object._CurrentDateAndTime)

                blnFlag = objPaymentTran.Update(FinancialYear._Object._DatabaseName, _
                                                User._Object._Userunkid, _
                                                FinancialYear._Object._YearUnkid, _
                                                Company._Object._Companyunkid, _
                                                mdtPeriodStartDate, _
                                                mdtPeriodEndDate, _
                                                ConfigParameter._Object._UserAccessModeSetting, _
                                                True, _
                                                ConfigParameter._Object._CurrentDateAndTime)
                'Nilay (25-Mar-2016) -- End

                'Sohail (15 Dec 2015) -- End

                'Sohail (21 Aug 2015) -- End
            Else
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'blnFlag = objPaymentTran.Insert()

                'Nilay (25-Mar-2016) -- Start
                'blnFlag = objPaymentTran.Insert(FinancialYear._Object._DatabaseName, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                '                               mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, _
                '                               ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PaymentVocNoType, _
                '                               ConfigParameter._Object._PaymentVocPrefix, ConfigParameter._Object._CurrentDateAndTime, True, "")
                'Sohail (23 May 2017) -- Start
                'Enhancement - 67.1 - Link budget with Payroll.
                'blnFlag = objPaymentTran.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                '                                mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, _
                '                                ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PaymentVocNoType, _
                '                                ConfigParameter._Object._PaymentVocPrefix, ConfigParameter._Object._CurrentDateAndTime, True, "")
                blnFlag = objPaymentTran.Insert(FinancialYear._Object._DatabaseName, User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, _
                                                ConfigParameter._Object._IsIncludeInactiveEmp, ConfigParameter._Object._PaymentVocNoType, _
                                                ConfigParameter._Object._PaymentVocPrefix, ConfigParameter._Object._CurrentDateAndTime, True, "", dtActAdj)
                'Sohail (23 May 2017) -- End
                'Nilay (25-Mar-2016) -- End

                'Sohail (21 Aug 2015) -- End

            End If

            Select Case mintReferenceUnkid
                Case clsPayment_tran.enPaymentRefId.LOAN, clsPayment_tran.enPaymentRefId.ADVANCE
                    If blnFlag = False And objPaymentTran._Message <> "" Then
                        eZeeMsgBox.Show(objPaymentTran._Message, enMsgBoxStyle.Information)
                    End If
                Case clsPayment_tran.enPaymentRefId.PAYSLIP
                    If blnFlag = False And objPaymentTran._Message <> "" Then
                        eZeeMsgBox.Show(objPaymentTran._Message, enMsgBoxStyle.Information)
                        'Sohail (20 Feb 2013) -- Start
                        'TRA - ENHANCEMENT
                    Else
                        If ConfigParameter._Object._SetPayslipPaymentApproval = True Then
                            Dim intBankPaid As Integer = 0
                            Dim intCashPaid As Integer = 0
                            Dim decBankPaid As Decimal = 0
                            Dim decCashPaid As Decimal = 0
                            If CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CASH_AND_CHEQUE Then
                                intCashPaid = 1
                                decCashPaid = CDec(Format(objPaymentTran._Expaidamt, GUI.fmtCurrency))
                            ElseIf CInt(cboPaymentMode.SelectedValue) = enPaymentMode.CHEQUE OrElse CInt(cboPaymentMode.SelectedValue) = enPaymentMode.TRANSFER Then
                                intBankPaid = 1
                                decBankPaid = CDec(Format(objPaymentTran._Expaidamt, GUI.fmtCurrency))
                            End If
                            'S.SANDEEP [ 28 JAN 2014 ] -- START
                            'objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, objPaymentTran._Employeeunkid.ToString, objPaymentTran._Periodunkid, False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, "", GUI.fmtCurrency)
                            'Sohail (17 Dec 2014) -- Start
                            'Enhancement - Provide Link for Payroll approval/Authorize Payslip Payment
                            'objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, objPaymentTran._Employeeunkid.ToString, objPaymentTran._Periodunkid, False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, "", GUI.fmtCurrency, , , , , enLogin_Mode.DESKTOP, 0)
                            'Sohail (24 Dec 2014) -- Start
                            'Enhancement - Provide Remark option on Payslip Payment and Global Payslip Payment.
                            'objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, objPaymentTran._Employeeunkid.ToString, objPaymentTran._Periodunkid, False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, "", GUI.fmtCurrency, , , , , enLogin_Mode.DESKTOP, 0, CInt(cboCurrency.SelectedValue), ConfigParameter._Object._ArutiSelfServiceURL)
                            'Sohail (21 Aug 2015) -- Start
                            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                            'objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, objPaymentTran._Employeeunkid.ToString, objPaymentTran._Periodunkid, False, intBankPaid, intCashPaid, (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, , , , , enLogin_Mode.DESKTOP, 0, CInt(cboCurrency.SelectedValue), ConfigParameter._Object._ArutiSelfServiceURL)
                            'Sohail (16 Nov 2016) -- Start
                            'Enhancement - 64.1 - Get User Access Level Employees from new employee transfer table and employee categorization table.
                            'objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, objPaymentTran._Employeeunkid.ToString, _
                            '                                  objPaymentTran._Periodunkid, False, intBankPaid, intCashPaid, _
                            '                                  (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), _
                            '                                  cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, _
                            '                                  FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, , _
                            '                                  enLogin_Mode.DESKTOP, 0, CInt(cboCurrency.SelectedValue), _
                            '                                  ConfigParameter._Object._ArutiSelfServiceURL)
                            objPaymentTran.SendMailToApprover(True, User._Object._Userunkid, objPaymentTran._Employeeunkid.ToString, _
                                                              objPaymentTran._Periodunkid, False, intBankPaid, intCashPaid, _
                                                              (intBankPaid + intCashPaid), decBankPaid, decCashPaid, (decBankPaid + decCashPaid), _
                                                              cboCurrency.Text, txtRemarks.Text.Trim, GUI.fmtCurrency, Company._Object._Companyunkid, _
                                                              FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, mdtPeriodStartDate, mdtPeriodEndDate, ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, , _
                                                              enLogin_Mode.DESKTOP, 0, CInt(cboCurrency.SelectedValue), _
                                                              ConfigParameter._Object._ArutiSelfServiceURL)
                            'Sohail (16 Nov 2016) -- End
                            'Sohail (21 Aug 2015) -- End
                            'Sohail (24 Dec 2014) -- End
                            'Sohail (17 Dec 2014) -- End
                            'S.SANDEEP [ 28 JAN 2014 ] -- END
                        End If
                        'Sohail (20 Feb 2013) -- End
                    End If

                Case clsPayment_tran.enPaymentRefId.SAVINGS
                    If blnFlag = False And objPaymentTran._Message <> "" Then
                        eZeeMsgBox.Show(objPaymentTran._Message, enMsgBoxStyle.Information)
                    End If
            End Select

            '   Sandeep -> to update balance in progress 
            'If mintPayTypeId = 2 Then   'Only For Received
            '    objLoan_Advance._Balance_Amount = objLoan_Advance._Balance_Amount - txtAmount.Decimal
            '    objLoan_Advance.Update()
            'ElseIf mintPayTypeId = 1 Then   'Only For Paid
            '    objLoan_Advance._Balance_Amount = objLoan_Advance._Net_Amount
            '    objLoan_Advance.Update()
            'End If


            'Savings



            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objPaymentTran = Nothing
                    objPaymentTran = New clsPayment_tran
                    Call GetValue()
                    txtAgainstVoucher.Focus()
                Else
                    mintPaymentTranId = objPaymentTran._Paymenttranunkid
                    Me.Close()
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Controls "
    Private Sub cboBankGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBankGroup.SelectedIndexChanged
        Dim dsCombos As New DataSet
        'Sohail (16 Jul 2012) -- Start
        'TRA - ENHANCEMENT
        'Dim objBranch As New clsbankbranch_master
        Dim objCompanyBank As New clsCompany_Bank_tran
        'Sohail (16 Jul 2012) -- End
        Try
            'If CInt(cboBankGroup.SelectedValue) > 0 Then
            'Sohail (16 Jul 2012) -- Start
            'TRA - ENHANCEMENT - Now Show bank from Company master
            'dsCombos = objBranch.getListForCombo("Branch", True, CInt(cboBankGroup.SelectedValue))
            'With cboBranch
            '    .ValueMember = "branchunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsCombos.Tables("Branch")
            '    'Sandeep [ 21 Aug 2010 ] -- Start
            '    '.SelectedValue = 0
            '    If mintPaymentTranId = -1 Then
            '        .SelectedValue = 0
            '    Else
            '        .SelectedValue = objPaymentTran._Branchunkid
            '    End If
            '    'Sandeep [ 21 Aug 2010 ] -- End 
            'End With
            dsCombos = objCompanyBank.GetComboList(Company._Object._Companyunkid, 2, "Branch", " cfbankbranch_master.bankgroupunkid = " & CInt(cboBankGroup.SelectedValue) & " ")
            With cboBranch
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsCombos.Tables("Branch")
                If mintPaymentTranId = -1 Then
                    .SelectedValue = 0
                Else
                    .SelectedValue = objPaymentTran._Branchunkid
                End If
            End With
            'Sohail (16 Jul 2012) -- End
            'End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBankGroup_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (21 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub cboBranch_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBranch.SelectedIndexChanged
        Dim objCompanyBank As New clsCompany_Bank_tran
        Dim dsCombos As New DataSet
        Try
            dsCombos = objCompanyBank.GetComboListBankAccount("Account", Company._Object._Companyunkid, True, CInt(cboBankGroup.SelectedValue), CInt(cboBranch.SelectedValue))
            With cboAccountNo
                .ValueMember = "id"
                .DisplayMember = "Name"
                .DataSource = dsCombos.Tables("Account")
                If mintPaymentTranId = -1 Then
                    .SelectedValue = 0
                Else
                    .SelectedValue = mintPaymentTranId
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboBranch_SelectedIndexChanged", mstrModuleName)
        Finally
            objCompanyBank = Nothing
        End Try
    End Sub
    'Sohail (21 Jul 2012) -- End

    Private Sub cboPaymentBy_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPaymentBy.SelectedIndexChanged
        Try

            'S.SANDEEP [ 29 May 2013 ] -- START
            'ENHANCEMENT : ATLAS COPCO WEB CHANGES
            'txtValue.Text = "" 'Sohail (08 Oct 2011)
            If menAction <> enAction.EDIT_ONE Then
                txtValue.Text = ""
            End If
            'S.SANDEEP [ 29 May 2013 ] -- END
            Select Case CInt(cboPaymentBy.SelectedValue)
                Case 1  ' Value
                    'txtValue.Text = "" 'Sohail (08 Oct 2011)
                    txtValue.Enabled = False
                    txtAmount.ReadOnly = False
                Case 2  'Percentage
                    txtValue.Enabled = True
                    txtAmount.ReadOnly = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPaymentBy_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sandeep -> not in use.
    'Private Sub txtTotal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim dblDueBalance As Double = 0
    '        Dim dblPaidTotal As Double = 0
    '        dblDueBalance = cdec(IIf(objLoan_Advance._Isloan = True, objLoan_Advance._Net_Amount, objLoan_Advance._Advance_Amount))
    '        'dblPaidTotal = cdec(txtTotal.Text)
    '        txtBalanceDue.Text = CStr(dblDueBalance - dblPaidTotal)
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "txtTotal_TextChanged", mstrModuleName)
    '    End Try
    'End Sub

    'Private Sub lvPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        If lvPayment.SelectedItems.Count > 0 Then
    '            mintSelectedIndex = CInt(lvPayment.SelectedItems(0).Index)
    '            cboBankGroup.SelectedValue = 0
    '            cboBranch.SelectedValue = 0
    '            cboPaymentBy.SelectedValue = 0
    '            cboPaymentMode.SelectedValue = 0

    '            cboBankGroup.SelectedValue = CInt(IIf(lvPayment.SelectedItems(0).SubItems(objcolhBankUnkid.Index).Text = "", 0, lvPayment.SelectedItems(0).SubItems(objcolhBankUnkid.Index).Text))
    '            cboBranch.SelectedValue = CInt(IIf(lvPayment.SelectedItems(0).SubItems(objcolhBranchUnkid.Index).Text = "", 0, lvPayment.SelectedItems(0).SubItems(objcolhBranchUnkid.Index).Text))
    '            cboPaymentBy.SelectedValue = CInt(IIf(lvPayment.SelectedItems(0).SubItems(objcolhPayByUnkid.Index).Text = "", 0, lvPayment.SelectedItems(0).SubItems(objcolhPayByUnkid.Index).Text))
    '            cboPaymentMode.SelectedValue = CInt(IIf(lvPayment.SelectedItems(0).SubItems(objcolhPayModeUnkid.Index).Text = "", 0, lvPayment.SelectedItems(0).SubItems(objcolhPayModeUnkid.Index).Text))
    '            txtAgainstVoucher.Text = lvPayment.SelectedItems(0).SubItems(colhVoucher.Index).Text
    '            txtChequeNo.Text = lvPayment.SelectedItems(0).SubItems(colhCheque.Index).Text
    '            txtValue.Text = lvPayment.SelectedItems(0).SubItems(colhPercent.Index).Text
    '            txtAmount.Text = lvPayment.SelectedItems(0).SubItems(colhAmount.Index).Text
    '        End If
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "lvPayment_Click", mstrModuleName)
    '    End Try
    'End Sub


    Private Sub cboPaymentMode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPaymentMode.SelectedIndexChanged
        Try
            Select Case CInt(cboPaymentMode.SelectedValue)
                Case 1
                    cboBankGroup.SelectedValue = 0
                    cboBankGroup.Enabled = False
                    cboBranch.SelectedValue = 0
                    cboBranch.Enabled = False
                    txtChequeNo.Text = ""
                    txtChequeNo.Enabled = False
                    'Sohail (21 Jul 2012) -- Start
                    'TRA - ENHANCEMENT
                    cboAccountNo.SelectedValue = 0
                    cboAccountNo.Enabled = False
                    'Sohail (21 Jul 2012) -- End
                Case Else
                    cboBankGroup.Enabled = True
                    cboBranch.Enabled = True
                    txtChequeNo.Enabled = True
                    cboAccountNo.Enabled = True 'Sohail (21 Jul 2012)
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPaymentMode_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub txtValue_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtValue.TextChanged
        Dim decPercentAmount As Decimal = 0 'Sohail (11 May 2011)
        Try
            If txtValue.Text.Trim <> "" Then
                If txtValue.Text.Trim <> "." Then
                    If CDec(txtValue.Text) > 100 Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please enter correct Percentage."))
                        txtValue.Text = CStr(0)
                        Exit Sub
                    Else
                        'Sohail (08 Oct 2011) -- Start
                        'decPercentAmount = (txtBalanceDue.Decimal * txtValue.Decimal) / 100 'Sohail (11 May 2011)
                        If mdecBaseExRate <> 0 Then
                            decPercentAmount = mdecBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
                        Else
                            decPercentAmount = mdecBaseCurrBalanceAmt * mdecPaidExRate
                        End If
                        decPercentAmount = (decPercentAmount * txtValue.Decimal) / 100
                        'Sohail (08 Oct 2011) -- End
                        txtAmount.Decimal = decPercentAmount 'Sohail (11 May 2011)
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtValue_TextChanged", mstrModuleName)
        End Try
    End Sub

    'Sohail (08 Oct 2011) -- Start
    Private Sub cboCurrency_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCurrency.SelectedIndexChanged _
                                                                                                            , dtpPaymentDate.ValueChanged
        Dim objExRate As New clsExchangeRate
        Dim dsList As DataSet
        Dim dtTable As DataTable
        Try
            objlblExRate.Text = ""
            lblAmount.Text = Language.getMessage(mstrModuleName, 31, "Amount")
            'Sohail (03 Sep 2012) -- Start
            'TRA - ENHANCEMENT
            'dsList = objExRate.GetList("ExRate", True)
            'dtTable = New DataView(dsList.Tables("ExRate"), "exchangerateunkid = " & CInt(cboCurrency.SelectedValue) & " ", "", DataViewRowState.CurrentRows).ToTable
            dsList = objExRate.GetList("ExRate", True, , , CInt(cboCurrency.SelectedValue), True, dtpPaymentDate.Value, True)
            dtTable = New DataView(dsList.Tables("ExRate")).ToTable
            'Sohail (03 Sep 2012) -- End
            If dtTable.Rows.Count > 0 Then
                mdecBaseExRate = CDec(dtTable.Rows(0).Item("exchange_rate1"))
                mdecPaidExRate = CDec(dtTable.Rows(0).Item("exchange_rate2"))
                mintPaidCurrId = CInt(dtTable.Rows(0).Item("exchangerateunkid"))
                objlblExRate.Text = Format(mdecBaseExRate, GUI.fmtCurrency) & " " & mstrBaseCurrSign & " = " & mdecPaidExRate.ToString & " " & dtTable.Rows(0).Item("currency_sign").ToString & " "
                lblAmount.Text &= " (" & dtTable.Rows(0).Item("currency_sign").ToString & ")"
                'Sohail (03 Sep 2012) -- Start
                'TRA - ENHANCEMENT
            Else
                mdecBaseExRate = 0
                mdecPaidExRate = 0
                'Sohail (03 Sep 2012) -- End
            End If
            'Sohail (07 May 2015) -- Start
            'Enhancement - New Loan Redesign with allow to change interest rate, tenure, topup, receipt.
            If mintReferenceUnkid = clsPayment_tran.enPaymentRefId.LOAN AndAlso mintPayTypeId = clsPayment_tran.enPayTypeId.RECEIVED Then
                Dim objLoan As New clsLoan_Advance
                'Nilay (25-Mar-2016) -- Start
                'Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo("List", dtpPaymentDate.Value.Date, "", mintTranUnkid)
                Dim dsLoan As DataSet = objLoan.Calculate_LoanBalanceInfo(FinancialYear._Object._DatabaseName, _
                                                                          User._Object._Userunkid, _
                                                                          FinancialYear._Object._YearUnkid, _
                                                                          Company._Object._Companyunkid, _
                                                                          mdtPeriodStartDate, _
                                                                          mdtPeriodEndDate, _
                                                                          ConfigParameter._Object._UserAccessModeSetting, _
                                                                          True, "List", dtpPaymentDate.Value.Date, "", mintTranUnkid)
                'Nilay (25-Mar-2016) -- End

                If dsLoan.Tables("List").Rows.Count > 0 Then
                    With dsLoan.Tables("List").Rows(0)
                        txtBalanceDue.Text = Format(CDec(.Item("LastProjectedBalance")), GUI.fmtCurrency)
                        txtBalanceDue.Tag = CDec(.Item("LastProjectedBalance"))
                        mdecBaseCurrBalanceAmt = CDec(.Item("LastProjectedBalance"))
                    End With

                    'Sohail (17 Mar 2020) -- Start
                    'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
                    'mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag)
                    'objlblMessage.Text = ""
                    'If mintReferenceUnkid = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                    '    Dim decRondingMultile As Decimal = 0
                    '    Select Case CInt(ConfigParameter._Object._PaymentRoundingMultiple)
                    '        Case enPaymentRoundingMultiple._1
                    '            decRondingMultile = 1
                    '        Case enPaymentRoundingMultiple._5
                    '            decRondingMultile = 5
                    '        Case enPaymentRoundingMultiple._10
                    '            decRondingMultile = 10
                    '        Case enPaymentRoundingMultiple._50
                    '            decRondingMultile = 50
                    '        Case enPaymentRoundingMultiple._100
                    '            decRondingMultile = 100
                    '            'Sohail (08 Apr 2016) -- Start
                    '            'Issue - 58.1 - Allow Rounding to 200 for Payslip Payment Rounding Multiple option for Mabibo.
                    '        Case enPaymentRoundingMultiple._200
                    '            decRondingMultile = 200
                    '            'Sohail (08 Apr 2016) -- End
                    '        Case enPaymentRoundingMultiple._500
                    '            decRondingMultile = 500
                    '        Case enPaymentRoundingMultiple._1000
                    '            decRondingMultile = 1000
                    '    End Select
                    '    Select Case CInt(ConfigParameter._Object._PaymentRoundingType)
                    '        Case enPaymentRoundingType.NONE
                    '            mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag)
                    '            objlblMessage.Text = ""
                    '        Case enPaymentRoundingType.AUTOMATIC
                    '            If decRondingMultile / 2 > CDec(txtBalanceDue.Tag) Mod decRondingMultile Then
                    '                mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag) - CDec(CDec(txtBalanceDue.Tag) Mod decRondingMultile)
                    '            Else
                    '                If CDec(CDec(txtBalanceDue.Tag) Mod decRondingMultile) <> 0 Then
                    '                    mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag) - CDec(CDec(txtBalanceDue.Tag) Mod decRondingMultile) + decRondingMultile
                    '                End If
                    '            End If
                    '            objlblMessage.Text = "Payment Rounding Type : Automatic, Rounding To : " & decRondingMultile.ToString
                    '        Case enPaymentRoundingType.UP
                    '            If CDec(CDec(txtBalanceDue.Tag) Mod decRondingMultile) <> 0 Then
                    '                mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag) - CDec(CDec(txtBalanceDue.Tag) Mod decRondingMultile) + decRondingMultile
                    '            End If
                    '            objlblMessage.Text = "Payment Rounding Type : Up, Rounding To : " & decRondingMultile.ToString
                    '        Case enPaymentRoundingType.DOWN
                    '            mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag) - CDec(CDec(txtBalanceDue.Tag) Mod decRondingMultile)
                    '            objlblMessage.Text = "Payment Rounding Type : Down, Rounding To : " & decRondingMultile.ToString
                    '    End Select
                    'End If
                    'Sohail (17 Mar 2020) -- End
                End If
            End If
            'Sohail (07 May 2015) -- End
            'Sohail (17 Mar 2020) -- Start
            'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
                    mdecBaseCurrBalanceAmt = CDec(txtBalanceDue.Tag)
                    objlblMessage.Text = ""
            'Sohail (17 Mar 2020) -- End
            If mdecBaseExRate <> 0 Then
                mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate / mdecBaseExRate
            Else
                mdecPaidCurrBalanceAmt = mdecBaseCurrBalanceAmt * mdecPaidExRate
            End If
            'Sohail (17 Mar 2020) -- Start
            'PLASCO LTD Enhancement # 0004621 : Assist to put Rounding off in both base currency and Usd payment net payment.
                    If mintReferenceUnkid = clsPayment_tran.enPaymentRefId.PAYSLIP Then
                        Dim decRondingMultile As Decimal = 0
                        Select Case CInt(ConfigParameter._Object._PaymentRoundingMultiple)
                            Case enPaymentRoundingMultiple._1
                                decRondingMultile = 1
                            Case enPaymentRoundingMultiple._5
                                decRondingMultile = 5
                            Case enPaymentRoundingMultiple._10
                                decRondingMultile = 10
                            Case enPaymentRoundingMultiple._50
                                decRondingMultile = 50
                            Case enPaymentRoundingMultiple._100
                                decRondingMultile = 100
                            Case enPaymentRoundingMultiple._200
                                decRondingMultile = 200
                            Case enPaymentRoundingMultiple._500
                                decRondingMultile = 500
                            Case enPaymentRoundingMultiple._1000
                                decRondingMultile = 1000
                        End Select
                Dim dicBalanceAmt As Decimal = mdecPaidCurrBalanceAmt
                        Select Case CInt(ConfigParameter._Object._PaymentRoundingType)
                            Case enPaymentRoundingType.NONE
                        mdecPaidCurrBalanceAmt = dicBalanceAmt
                                objlblMessage.Text = ""
                            Case enPaymentRoundingType.AUTOMATIC
                        If decRondingMultile / 2 > dicBalanceAmt Mod decRondingMultile Then
                            mdecPaidCurrBalanceAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile)
                                Else
                            If CDec(dicBalanceAmt Mod decRondingMultile) <> 0 Then
                                mdecPaidCurrBalanceAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile) + decRondingMultile
                                    End If
                                End If
                                objlblMessage.Text = "Payment Rounding Type : Automatic, Rounding To : " & decRondingMultile.ToString
                            Case enPaymentRoundingType.UP
                        If CDec(dicBalanceAmt Mod decRondingMultile) <> 0 Then
                            mdecPaidCurrBalanceAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile) + decRondingMultile
                                End If
                                objlblMessage.Text = "Payment Rounding Type : Up, Rounding To : " & decRondingMultile.ToString
                            Case enPaymentRoundingType.DOWN
                        mdecPaidCurrBalanceAmt = dicBalanceAmt - CDec(dicBalanceAmt Mod decRondingMultile)
                                objlblMessage.Text = "Payment Rounding Type : Down, Rounding To : " & decRondingMultile.ToString
                        End Select
            If mdecBaseExRate <> 0 Then
                    mdecBaseCurrBalanceAmt = mdecPaidCurrBalanceAmt * mdecBaseExRate / mdecPaidExRate
            Else
                    mdecBaseCurrBalanceAmt = mdecPaidCurrBalanceAmt * mdecBaseExRate
                End If
            End If
            'Sohail (17 Mar 2020) -- End
            mdecPaidCurrBalFormatedAmt = CDec(Format(mdecPaidCurrBalanceAmt, GUI.fmtCurrency))
            If CInt(cboPaymentBy.SelectedValue) = enPaymentBy.Percentage Then
                txtAmount.Tag = mdecPaidCurrBalanceAmt * txtValue.Decimal / 100
            Else
                txtAmount.Tag = mdecPaidCurrBalanceAmt
            End If
            txtAmount.Text = Format(CDec(txtAmount.Tag), GUI.fmtCurrency)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCurrency_SelectedIndexChanged", mstrModuleName)
        End Try

    End Sub
    'Sohail (08 Oct 2011) -- End

    'Nilay (04-Nov-2016) -- Start
    'Enhancements: Global Change Status feature requested by {Rutta}
    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Try
            If CInt(cboPeriod.SelectedValue) > 0 Then
                Dim objPeriod As New clscommom_period_Tran
                objPeriod._Periodunkid(FinancialYear._Object._DatabaseName) = CInt(cboPeriod.SelectedValue)
                'Sohail (24 Jun 2017) -- Start
                'Issue - 68.1 - There is no row at position 0.
                mdtPeriodStartDate = objPeriod._Start_Date
                mdtPeriodEndDate = objPeriod._End_Date
                'Sohail (24 Jun 2017) -- End
                'Sohail (14 Dec 2018) -- Start
                'Twaweza Issue : Payment Time not coming on payroll report prepared by.
                'dtpPaymentDate.Value = objPeriod._Start_Date
                dtpPaymentDate.Value = objPeriod._Start_Date.AddMinutes(DateDiff(DateInterval.Minute, DateAndTime.Now.Date, Now))
                'Sohail (14 Dec 2018) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboPeriod_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub
    'Nilay (04-Nov-2016) -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

            Me.gbPayment.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbPayment.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnSave.GradientBackColor = GUI._ButttonBackColor
            Me.btnSave.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbPayment.Text = Language._Object.getCaption(Me.gbPayment.Name, Me.gbPayment.Text)
            Me.lblPaymentOf.Text = Language._Object.getCaption(Me.lblPaymentOf.Name, Me.lblPaymentOf.Text)
            Me.lblPayPeriod.Text = Language._Object.getCaption(Me.lblPayPeriod.Name, Me.lblPayPeriod.Text)
            Me.lblPayYear.Text = Language._Object.getCaption(Me.lblPayYear.Name, Me.lblPayYear.Text)
            Me.lblPaidTo.Text = Language._Object.getCaption(Me.lblPaidTo.Name, Me.lblPaidTo.Text)
            Me.lblPaymentDate.Text = Language._Object.getCaption(Me.lblPaymentDate.Name, Me.lblPaymentDate.Text)
            Me.lblPaymentMode.Text = Language._Object.getCaption(Me.lblPaymentMode.Name, Me.lblPaymentMode.Text)
            Me.lblBankGroup.Text = Language._Object.getCaption(Me.lblBankGroup.Name, Me.lblBankGroup.Text)
            Me.lblBranch.Text = Language._Object.getCaption(Me.lblBranch.Name, Me.lblBranch.Text)
            Me.lblCheque.Text = Language._Object.getCaption(Me.lblCheque.Name, Me.lblCheque.Text)
            Me.lblPaymentBy.Text = Language._Object.getCaption(Me.lblPaymentBy.Name, Me.lblPaymentBy.Text)
            Me.lblAmount.Text = Language._Object.getCaption(Me.lblAmount.Name, Me.lblAmount.Text)
            Me.lblVoucher.Text = Language._Object.getCaption(Me.lblVoucher.Name, Me.lblVoucher.Text)
            Me.lblAgainstVoucher.Text = Language._Object.getCaption(Me.lblAgainstVoucher.Name, Me.lblAgainstVoucher.Text)
            Me.lblValue.Text = Language._Object.getCaption(Me.lblValue.Name, Me.lblValue.Text)
            Me.lblBalanceDue.Text = Language._Object.getCaption(Me.lblBalanceDue.Name, Me.lblBalanceDue.Text)
            Me.lblLoanAmount.Text = Language._Object.getCaption(Me.lblLoanAmount.Name, Me.lblLoanAmount.Text)
            Me.lblReceivedFrom.Text = Language._Object.getCaption(Me.lblReceivedFrom.Name, Me.lblReceivedFrom.Text)
            Me.lblScheme.Text = Language._Object.getCaption(Me.lblScheme.Name, Me.lblScheme.Text)
            Me.lblPayment.Text = Language._Object.getCaption(Me.lblPayment.Name, Me.lblPayment.Text)
            Me.lblCurrency.Text = Language._Object.getCaption(Me.lblCurrency.Name, Me.lblCurrency.Text)
            Me.lblAccountNo.Text = Language._Object.getCaption(Me.lblAccountNo.Name, Me.lblAccountNo.Text)
            Me.lblRemarks.Text = Language._Object.getCaption(Me.lblRemarks.Name, Me.lblRemarks.Text)
            Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
			Language.setMessage(mstrModuleName, 1, "Please enter correct Percentage.")
            Language.setMessage("frmLoanAdvanceList", 2, "Loan")
            Language.setMessage("frmLoanAdvanceList", 3, "Advance")
            Language.setMessage("frmPayslipList", 4, "Payslip")
            Language.setMessage(mstrModuleName, 1, "Please enter correct Percentage.")
            Language.setMessage(mstrModuleName, 5, "Savings")
            Language.setMessage(mstrModuleName, 6, "Add / Edit Receipt")
            Language.setMessage(mstrModuleName, 7, "Receipt Information")
            Language.setMessage(mstrModuleName, 8, "Sorry, you cannot make new payment. Reason : Loan is already paid to particular employee.")
            Language.setMessage(mstrModuleName, 9, "Sorry, you cannot make new payment. Reason : Due amount is already paid by employee.")
            Language.setMessage(mstrModuleName, 10, "Voucher No. cannot be blank. Voucher No. is compulsory information.")
            Language.setMessage(mstrModuleName, 11, "Payment Mode is compulsory information. Please select Payment Mode to continue.")
            Language.setMessage(mstrModuleName, 12, "Payment By is compulsory information. Please select Payment By to continue.")
            Language.setMessage(mstrModuleName, 13, "Bank Group is compulsory information. Please select Bank Group to continue.")
            Language.setMessage(mstrModuleName, 14, "Branch is compulsory information. Please select Branch to continue.")
            Language.setMessage(mstrModuleName, 15, "Amount cannot be blank .Amount is compulsory information.")
            Language.setMessage(mstrModuleName, 16, "Percent cannot be blank .Percent is compulsory information.")
            Language.setMessage(mstrModuleName, 17, "Cheque No. cannot be blank. Please enter the cheque information.")
            Language.setMessage(mstrModuleName, 18, "Sorry, you cannot make new payment. Reason : Saving Balance is Zero.")
            Language.setMessage(mstrModuleName, 19, "Amount cannot be greater than the Due Amount (")
            Language.setMessage(mstrModuleName, 20, " ).")
            Language.setMessage(mstrModuleName, 21, "Sorry! This employee does not have any bank account. Please add bank account from Employee Bank.")
            Language.setMessage(mstrModuleName, 22, "Sorry! This employee is having Total Salary Distribution percentage Not Equal to 100 in Employee Bank.")
            Language.setMessage(mstrModuleName, 23, "Payment Date should be between")
            Language.setMessage(mstrModuleName, 24, "Sorry! No Exchange rate defined for selected currency for the given date.")
            Language.setMessage(mstrModuleName, 25, "Please select Bank Account. Bank Account is mandatory information.")
            Language.setMessage(mstrModuleName, 26, "Sorry, you cannot make payment. Reason : Payslip amount is either Zero or Negative.")
            Language.setMessage(mstrModuleName, 27, "Sorry! You can not do Payment now. Reason : Some of the employees have been Over Deducted this month.")
            Language.setMessage(mstrModuleName, 28, " And")
            Language.setMessage(mstrModuleName, 29, "Sorry, you cannot do any partial payment. Please do full payment.")
            Language.setMessage(mstrModuleName, 30, "Sending Email(s) process is in progress from other module. Please wait.")
            Language.setMessage(mstrModuleName, 31, "Amount")
            Language.setMessage(mstrModuleName, 32, "Sorry, Payment Date should be between open periods and should not be less than")
            Language.setMessage(mstrModuleName, 33, "Please select period.")
            Language.setMessage(mstrModuleName, 34, "Sorry, Payment Date should be between selected period start date and end date.")
            Language.setMessage(mstrModuleName, 35, "Sorry, Payment date should not be less than last transaction date")
            Language.setMessage(mstrModuleName, 36, "Sorry, You cannot do this transaction. Reason : Payroll process is already done for the last date of selected period for selected employee.")
            Language.setMessage(mstrModuleName, 37, "Sorry, Date should not be less than Loan/Advance Effective date")
            Language.setMessage(mstrModuleName, 38, "Sorry, Date should not be less than Saving Effective date")
            Language.setMessage(mstrModuleName, 39, "Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process in current period. In order to Receipt, please either Approve or Reject pending operations for current period.")
            Language.setMessage(mstrModuleName, 40, "Sorry. Some of Operation(s) either Interest Rate/EMI Tenure/Topup Amount are still pending for Approval process in current period. In order to make Payment, please either Approve or Reject pending operations for current period.")
	    Language.setMessage(mstrModuleName, 41, "Salary payment for the period of")
	    Language.setMessage(mstrModuleName, 42, "Sorry, Actual Salary Payment will be exceeding to the Activity Current Balance.")
			Language.setMessage(mstrModuleName, 43, "Do you want to void Payroll?")
			Language.setMessage(mstrModuleName, 44, "Sorry, Net Pay went below as per the formula set on configuration for some employees.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class