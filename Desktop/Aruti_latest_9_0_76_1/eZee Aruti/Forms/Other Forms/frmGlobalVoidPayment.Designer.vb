﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGlobalVoidPayment
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGlobalVoidPayment))
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.objchkSelectAll = New System.Windows.Forms.CheckBox
        Me.GvVoidPayment = New System.Windows.Forms.DataGridView
        Me.EZeeHeader = New eZee.Common.eZeeHeader
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.objlblProgress = New System.Windows.Forms.Label
        Me.btnVoid = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.cboPmtVoucher = New System.Windows.Forms.ComboBox
        Me.lnkAdvanceFilter = New System.Windows.Forms.LinkLabel
        Me.txtPaidAmountTo = New eZee.TextBox.NumericTextBox
        Me.lblTo = New System.Windows.Forms.Label
        Me.txtPaidAmount = New eZee.TextBox.NumericTextBox
        Me.lblPaidAmount = New System.Windows.Forms.Label
        Me.dtpPaymentDate = New System.Windows.Forms.DateTimePicker
        Me.lblPaymentDate = New System.Windows.Forms.Label
        Me.lblVoucherno = New System.Windows.Forms.Label
        Me.cboPayPeriod = New System.Windows.Forms.ComboBox
        Me.lblPayPeriod = New System.Windows.Forms.Label
        Me.objbtnSearchEmployee = New eZee.Common.eZeeGradientButton
        Me.cboEmployee = New System.Windows.Forms.ComboBox
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.objbtnReset = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbtnSearch = New eZee.Common.eZeeSearchResetButton(Me.components)
        Me.objbgwVoidPayment = New System.ComponentModel.BackgroundWorker
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhCheck1 = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colhDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhVoucher = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhEmployeeunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhEmployee = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPayPeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhPaidAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhPaidAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhCurrency = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhPaymenttranunkid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.GvVoidPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.objchkSelectAll)
        Me.pnlMainInfo.Controls.Add(Me.GvVoidPayment)
        Me.pnlMainInfo.Controls.Add(Me.EZeeHeader)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Controls.Add(Me.gbFilterCriteria)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(797, 445)
        Me.pnlMainInfo.TabIndex = 1
        '
        'objchkSelectAll
        '
        Me.objchkSelectAll.AutoSize = True
        Me.objchkSelectAll.Location = New System.Drawing.Point(21, 168)
        Me.objchkSelectAll.Name = "objchkSelectAll"
        Me.objchkSelectAll.Size = New System.Drawing.Size(15, 14)
        Me.objchkSelectAll.TabIndex = 288
        Me.objchkSelectAll.UseVisualStyleBackColor = True
        '
        'GvVoidPayment
        '
        Me.GvVoidPayment.AllowUserToAddRows = False
        Me.GvVoidPayment.AllowUserToDeleteRows = False
        Me.GvVoidPayment.AllowUserToResizeRows = False
        Me.GvVoidPayment.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GvVoidPayment.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.GvVoidPayment.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.GvVoidPayment.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal
        Me.GvVoidPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.GvVoidPayment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhCheck1, Me.colhDate, Me.colhVoucher, Me.objcolhEmployeeunkid, Me.colhEmpCode, Me.colhEmployee, Me.colhPayPeriod, Me.colhPaidAmount, Me.objcolhPaidAmount, Me.colhCurrency, Me.objdgcolhPaymenttranunkid, Me.objdgcolhIsGroup})
        Me.GvVoidPayment.Location = New System.Drawing.Point(12, 163)
        Me.GvVoidPayment.Name = "GvVoidPayment"
        Me.GvVoidPayment.RowHeadersVisible = False
        Me.GvVoidPayment.RowHeadersWidth = 5
        Me.GvVoidPayment.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.GvVoidPayment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.GvVoidPayment.Size = New System.Drawing.Size(773, 222)
        Me.GvVoidPayment.TabIndex = 287
        '
        'EZeeHeader
        '
        Me.EZeeHeader.BackColor = System.Drawing.SystemColors.Control
        Me.EZeeHeader.BorderColor = System.Drawing.SystemColors.ControlDark
        Me.EZeeHeader.DescriptionForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.EZeeHeader.Dock = System.Windows.Forms.DockStyle.Top
        Me.EZeeHeader.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeHeader.GradiantStyle = eZee.Common.eZeeHeader.GradientStyle.Central
        Me.EZeeHeader.GradientColor1 = System.Drawing.SystemColors.Window
        Me.EZeeHeader.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeHeader.HeaderTextForeColor = System.Drawing.SystemColors.ControlText
        Me.EZeeHeader.Icon = Nothing
        Me.EZeeHeader.Location = New System.Drawing.Point(0, 0)
        Me.EZeeHeader.Message = ""
        Me.EZeeHeader.Name = "EZeeHeader"
        Me.EZeeHeader.Size = New System.Drawing.Size(797, 60)
        Me.EZeeHeader.TabIndex = 277
        Me.EZeeHeader.Title = "Global Void Payment"
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.objlblProgress)
        Me.objFooter.Controls.Add(Me.btnVoid)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 390)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(797, 55)
        Me.objFooter.TabIndex = 2
        '
        'objlblProgress
        '
        Me.objlblProgress.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblProgress.Location = New System.Drawing.Point(409, 22)
        Me.objlblProgress.Name = "objlblProgress"
        Me.objlblProgress.Size = New System.Drawing.Size(141, 13)
        Me.objlblProgress.TabIndex = 15
        '
        'btnVoid
        '
        Me.btnVoid.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVoid.BackColor = System.Drawing.Color.White
        Me.btnVoid.BackgroundImage = CType(resources.GetObject("btnVoid.BackgroundImage"), System.Drawing.Image)
        Me.btnVoid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnVoid.BorderColor = System.Drawing.Color.Empty
        Me.btnVoid.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnVoid.FlatAppearance.BorderSize = 0
        Me.btnVoid.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVoid.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnVoid.ForeColor = System.Drawing.Color.Black
        Me.btnVoid.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnVoid.GradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoid.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnVoid.Location = New System.Drawing.Point(585, 13)
        Me.btnVoid.Name = "btnVoid"
        Me.btnVoid.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnVoid.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnVoid.Size = New System.Drawing.Size(97, 30)
        Me.btnVoid.TabIndex = 2
        Me.btnVoid.Text = "&Void"
        Me.btnVoid.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnClose.Location = New System.Drawing.Point(688, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(97, 30)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.cboPmtVoucher)
        Me.gbFilterCriteria.Controls.Add(Me.lnkAdvanceFilter)
        Me.gbFilterCriteria.Controls.Add(Me.txtPaidAmountTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblTo)
        Me.gbFilterCriteria.Controls.Add(Me.txtPaidAmount)
        Me.gbFilterCriteria.Controls.Add(Me.lblPaidAmount)
        Me.gbFilterCriteria.Controls.Add(Me.dtpPaymentDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblPaymentDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblVoucherno)
        Me.gbFilterCriteria.Controls.Add(Me.cboPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.lblPayPeriod)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearchEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.cboEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.lblEmployee)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(12, 66)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(773, 91)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPmtVoucher
        '
        Me.cboPmtVoucher.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPmtVoucher.DropDownWidth = 150
        Me.cboPmtVoucher.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPmtVoucher.FormattingEnabled = True
        Me.cboPmtVoucher.Location = New System.Drawing.Point(93, 60)
        Me.cboPmtVoucher.Name = "cboPmtVoucher"
        Me.cboPmtVoucher.Size = New System.Drawing.Size(105, 21)
        Me.cboPmtVoucher.TabIndex = 312
        '
        'lnkAdvanceFilter
        '
        Me.lnkAdvanceFilter.BackColor = System.Drawing.Color.Transparent
        Me.lnkAdvanceFilter.Location = New System.Drawing.Point(581, 6)
        Me.lnkAdvanceFilter.Name = "lnkAdvanceFilter"
        Me.lnkAdvanceFilter.Size = New System.Drawing.Size(128, 13)
        Me.lnkAdvanceFilter.TabIndex = 310
        Me.lnkAdvanceFilter.TabStop = True
        Me.lnkAdvanceFilter.Text = "Advance Filter"
        Me.lnkAdvanceFilter.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'txtPaidAmountTo
        '
        Me.txtPaidAmountTo.AllowNegative = False
        Me.txtPaidAmountTo.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPaidAmountTo.DigitsInGroup = 0
        Me.txtPaidAmountTo.Flags = 65536
        Me.txtPaidAmountTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaidAmountTo.Location = New System.Drawing.Point(645, 60)
        Me.txtPaidAmountTo.MaxDecimalPlaces = 6
        Me.txtPaidAmountTo.MaxWholeDigits = 21
        Me.txtPaidAmountTo.Name = "txtPaidAmountTo"
        Me.txtPaidAmountTo.Prefix = ""
        Me.txtPaidAmountTo.RangeMax = 1.7976931348623157E+308
        Me.txtPaidAmountTo.RangeMin = -1.7976931348623157E+308
        Me.txtPaidAmountTo.Size = New System.Drawing.Size(104, 21)
        Me.txtPaidAmountTo.TabIndex = 6
        Me.txtPaidAmountTo.Text = "0"
        Me.txtPaidAmountTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblTo
        '
        Me.lblTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTo.Location = New System.Drawing.Point(578, 63)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(61, 15)
        Me.lblTo.TabIndex = 160
        Me.lblTo.Text = "To"
        Me.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPaidAmount
        '
        Me.txtPaidAmount.AllowNegative = False
        Me.txtPaidAmount.Decimal = New Decimal(New Integer() {0, 0, 0, 0})
        Me.txtPaidAmount.DigitsInGroup = 0
        Me.txtPaidAmount.Flags = 65536
        Me.txtPaidAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPaidAmount.Location = New System.Drawing.Point(468, 60)
        Me.txtPaidAmount.MaxDecimalPlaces = 6
        Me.txtPaidAmount.MaxWholeDigits = 21
        Me.txtPaidAmount.Name = "txtPaidAmount"
        Me.txtPaidAmount.Prefix = ""
        Me.txtPaidAmount.RangeMax = 1.7976931348623157E+308
        Me.txtPaidAmount.RangeMin = -1.7976931348623157E+308
        Me.txtPaidAmount.Size = New System.Drawing.Size(104, 21)
        Me.txtPaidAmount.TabIndex = 5
        Me.txtPaidAmount.Text = "0"
        Me.txtPaidAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblPaidAmount
        '
        Me.lblPaidAmount.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaidAmount.Location = New System.Drawing.Point(383, 63)
        Me.lblPaidAmount.Name = "lblPaidAmount"
        Me.lblPaidAmount.Size = New System.Drawing.Size(79, 15)
        Me.lblPaidAmount.TabIndex = 158
        Me.lblPaidAmount.Text = "Amount From"
        Me.lblPaidAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpPaymentDate
        '
        Me.dtpPaymentDate.Checked = False
        Me.dtpPaymentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpPaymentDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpPaymentDate.Location = New System.Drawing.Point(645, 33)
        Me.dtpPaymentDate.Name = "dtpPaymentDate"
        Me.dtpPaymentDate.ShowCheckBox = True
        Me.dtpPaymentDate.Size = New System.Drawing.Size(104, 21)
        Me.dtpPaymentDate.TabIndex = 2
        '
        'lblPaymentDate
        '
        Me.lblPaymentDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaymentDate.Location = New System.Drawing.Point(578, 36)
        Me.lblPaymentDate.Name = "lblPaymentDate"
        Me.lblPaymentDate.Size = New System.Drawing.Size(61, 15)
        Me.lblPaymentDate.TabIndex = 155
        Me.lblPaymentDate.Text = "Date"
        Me.lblPaymentDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVoucherno
        '
        Me.lblVoucherno.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVoucherno.Location = New System.Drawing.Point(8, 63)
        Me.lblVoucherno.Name = "lblVoucherno"
        Me.lblVoucherno.Size = New System.Drawing.Size(79, 15)
        Me.lblVoucherno.TabIndex = 153
        Me.lblVoucherno.Text = "Voucher No."
        Me.lblVoucherno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboPayPeriod
        '
        Me.cboPayPeriod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPayPeriod.FormattingEnabled = True
        Me.cboPayPeriod.Location = New System.Drawing.Point(467, 33)
        Me.cboPayPeriod.Name = "cboPayPeriod"
        Me.cboPayPeriod.Size = New System.Drawing.Size(105, 21)
        Me.cboPayPeriod.TabIndex = 4
        '
        'lblPayPeriod
        '
        Me.lblPayPeriod.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayPeriod.Location = New System.Drawing.Point(386, 35)
        Me.lblPayPeriod.Name = "lblPayPeriod"
        Me.lblPayPeriod.Size = New System.Drawing.Size(78, 16)
        Me.lblPayPeriod.TabIndex = 145
        Me.lblPayPeriod.Text = "Pay Period"
        Me.lblPayPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnSearchEmployee
        '
        Me.objbtnSearchEmployee.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearchEmployee.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearchEmployee.BorderSelected = False
        Me.objbtnSearchEmployee.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearchEmployee.Image = Global.Aruti.Main.My.Resources.Resources.Mini_Search
        Me.objbtnSearchEmployee.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearchEmployee.Location = New System.Drawing.Point(356, 33)
        Me.objbtnSearchEmployee.Name = "objbtnSearchEmployee"
        Me.objbtnSearchEmployee.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearchEmployee.TabIndex = 88
        '
        'cboEmployee
        '
        Me.cboEmployee.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.cboEmployee.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboEmployee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmployee.FormattingEnabled = True
        Me.cboEmployee.Location = New System.Drawing.Point(93, 33)
        Me.cboEmployee.Name = "cboEmployee"
        Me.cboEmployee.Size = New System.Drawing.Size(257, 21)
        Me.cboEmployee.TabIndex = 0
        '
        'lblEmployee
        '
        Me.lblEmployee.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEmployee.Location = New System.Drawing.Point(8, 36)
        Me.lblEmployee.Name = "lblEmployee"
        Me.lblEmployee.Size = New System.Drawing.Size(79, 15)
        Me.lblEmployee.TabIndex = 85
        Me.lblEmployee.Text = "Employee"
        '
        'objbtnReset
        '
        Me.objbtnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnReset.BackColor = System.Drawing.Color.Transparent
        Me.objbtnReset.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Reset
        Me.objbtnReset.Image = CType(resources.GetObject("objbtnReset.Image"), System.Drawing.Image)
        Me.objbtnReset.Location = New System.Drawing.Point(746, 0)
        Me.objbtnReset.Name = "objbtnReset"
        Me.objbtnReset.ResultMessage = ""
        Me.objbtnReset.SearchMessage = ""
        Me.objbtnReset.Size = New System.Drawing.Size(24, 24)
        Me.objbtnReset.TabIndex = 69
        Me.objbtnReset.TabStop = False
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.ButtonType = eZee.Common.eZeeSearchResetButton.EnumButtonType.Search
        Me.objbtnSearch.Image = CType(resources.GetObject("objbtnSearch.Image"), System.Drawing.Image)
        Me.objbtnSearch.Location = New System.Drawing.Point(723, 0)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.ResultMessage = ""
        Me.objbtnSearch.SearchMessage = ""
        Me.objbtnSearch.Size = New System.Drawing.Size(24, 24)
        Me.objbtnSearch.TabIndex = 68
        Me.objbtnSearch.TabStop = False
        '
        'objbgwVoidPayment
        '
        Me.objbgwVoidPayment.WorkerReportsProgress = True
        Me.objbgwVoidPayment.WorkerSupportsCancellation = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Payment Date"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn1.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Voucher #"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn2.Width = 90
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "employeeunkid"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn3.Visible = False
        Me.DataGridViewTextBoxColumn3.Width = 90
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.DataGridViewTextBoxColumn4.HeaderText = "Emp. code"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn4.Visible = False
        Me.DataGridViewTextBoxColumn4.Width = 90
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn5.HeaderText = "Employee Name"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.DataGridViewTextBoxColumn6.HeaderText = "Pay Period"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'DataGridViewTextBoxColumn7
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.DataGridViewTextBoxColumn7.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn7.HeaderText = "Paid Amount"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn7.Width = 130
        '
        'DataGridViewTextBoxColumn8
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.DataGridViewTextBoxColumn8.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn8.HeaderText = "objdgcolhAmount"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn8.Visible = False
        Me.DataGridViewTextBoxColumn8.Width = 130
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.HeaderText = "Currency"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn9.Visible = False
        Me.DataGridViewTextBoxColumn9.Width = 65
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.HeaderText = "objdgcolhPaymenttranunkid"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridViewTextBoxColumn10.Visible = False
        Me.DataGridViewTextBoxColumn10.Width = 65
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.HeaderText = "objdgcolhPaymenttranunkid"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.HeaderText = "IsGroup"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'objcolhCheck1
        '
        Me.objcolhCheck1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.objcolhCheck1.HeaderText = ""
        Me.objcolhCheck1.Name = "objcolhCheck1"
        Me.objcolhCheck1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.objcolhCheck1.Width = 30
        '
        'colhDate
        '
        Me.colhDate.HeaderText = "Payment Date"
        Me.colhDate.Name = "colhDate"
        Me.colhDate.ReadOnly = True
        Me.colhDate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhDate.Width = 90
        '
        'colhVoucher
        '
        Me.colhVoucher.HeaderText = "Voucher #"
        Me.colhVoucher.Name = "colhVoucher"
        Me.colhVoucher.ReadOnly = True
        Me.colhVoucher.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhVoucher.Width = 90
        '
        'objcolhEmployeeunkid
        '
        Me.objcolhEmployeeunkid.HeaderText = "employeeunkid"
        Me.objcolhEmployeeunkid.Name = "objcolhEmployeeunkid"
        Me.objcolhEmployeeunkid.ReadOnly = True
        Me.objcolhEmployeeunkid.Visible = False
        '
        'colhEmpCode
        '
        Me.colhEmpCode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None
        Me.colhEmpCode.HeaderText = "Emp. code"
        Me.colhEmpCode.Name = "colhEmpCode"
        Me.colhEmpCode.ReadOnly = True
        Me.colhEmpCode.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhEmpCode.Width = 90
        '
        'colhEmployee
        '
        Me.colhEmployee.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colhEmployee.HeaderText = "Employee"
        Me.colhEmployee.Name = "colhEmployee"
        Me.colhEmployee.ReadOnly = True
        '
        'colhPayPeriod
        '
        Me.colhPayPeriod.HeaderText = "Pay Period"
        Me.colhPayPeriod.Name = "colhPayPeriod"
        Me.colhPayPeriod.ReadOnly = True
        Me.colhPayPeriod.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        '
        'colhPaidAmount
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopRight
        Me.colhPaidAmount.DefaultCellStyle = DataGridViewCellStyle6
        Me.colhPaidAmount.HeaderText = "Paid Amount"
        Me.colhPaidAmount.Name = "colhPaidAmount"
        Me.colhPaidAmount.ReadOnly = True
        Me.colhPaidAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhPaidAmount.Width = 130
        '
        'objcolhPaidAmount
        '
        Me.objcolhPaidAmount.HeaderText = "objdgcolhAmount"
        Me.objcolhPaidAmount.Name = "objcolhPaidAmount"
        Me.objcolhPaidAmount.ReadOnly = True
        Me.objcolhPaidAmount.Visible = False
        '
        'colhCurrency
        '
        Me.colhCurrency.HeaderText = "Currency"
        Me.colhCurrency.Name = "colhCurrency"
        Me.colhCurrency.ReadOnly = True
        Me.colhCurrency.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colhCurrency.Width = 65
        '
        'objdgcolhPaymenttranunkid
        '
        Me.objdgcolhPaymenttranunkid.HeaderText = "objdgcolhPaymenttranunkid"
        Me.objdgcolhPaymenttranunkid.Name = "objdgcolhPaymenttranunkid"
        Me.objdgcolhPaymenttranunkid.ReadOnly = True
        Me.objdgcolhPaymenttranunkid.Visible = False
        '
        'objdgcolhIsGroup
        '
        Me.objdgcolhIsGroup.HeaderText = "IsGroup"
        Me.objdgcolhIsGroup.Name = "objdgcolhIsGroup"
        Me.objdgcolhIsGroup.ReadOnly = True
        Me.objdgcolhIsGroup.Visible = False
        '
        'frmGlobalVoidPayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(797, 445)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmGlobalVoidPayment"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Global Void Payment"
        Me.pnlMainInfo.ResumeLayout(False)
        Me.pnlMainInfo.PerformLayout()
        CType(Me.GvVoidPayment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbFilterCriteria.PerformLayout()
        CType(Me.objbtnReset, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.objbtnSearch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents EZeeHeader As eZee.Common.eZeeHeader
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnVoid As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtPaidAmountTo As eZee.TextBox.NumericTextBox
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents txtPaidAmount As eZee.TextBox.NumericTextBox
    Friend WithEvents lblPaidAmount As System.Windows.Forms.Label
    Friend WithEvents dtpPaymentDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPaymentDate As System.Windows.Forms.Label
    Friend WithEvents lblVoucherno As System.Windows.Forms.Label
    Friend WithEvents cboPayPeriod As System.Windows.Forms.ComboBox
    Friend WithEvents lblPayPeriod As System.Windows.Forms.Label
    Friend WithEvents objbtnSearchEmployee As eZee.Common.eZeeGradientButton
    Friend WithEvents cboEmployee As System.Windows.Forms.ComboBox
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents objbtnReset As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbtnSearch As eZee.Common.eZeeSearchResetButton
    Friend WithEvents objbgwVoidPayment As System.ComponentModel.BackgroundWorker
    Friend WithEvents objlblProgress As System.Windows.Forms.Label
    Friend WithEvents lnkAdvanceFilter As System.Windows.Forms.LinkLabel
    Friend WithEvents cboPmtVoucher As System.Windows.Forms.ComboBox
    Friend WithEvents GvVoidPayment As System.Windows.Forms.DataGridView
    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhCheck1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colhDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhVoucher As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhEmployeeunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmpCode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhEmployee As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPayPeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhPaidAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhPaidAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhCurrency As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhPaymenttranunkid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGroup As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
