﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.Sql
Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmNewWizard
    Private mstrType As String = ""
    Private mstrXmlPath As String = ""
    Private mblnFinish As Boolean = False
    Private mdtMaster As New DataSet
    Private mintCnt As Integer = 1
    Private tlpWizard As New ToolTip
    Private mConnection As New SqlClient.SqlConnection

    'Dipti (08 June 2012) - Start
    Private mstrQuery As String = ""
    Private mstrModuleName As String = "frmNewWizard"

#Region " Form Properties "

    Public Property _Query() As String
        Get
            Return mstrQuery
        End Get
        Set(ByVal value As String)
            mstrQuery = value
        End Set
    End Property

    Public Property _Type() As String
        Get
            Return mstrType
        End Get
        Set(ByVal value As String)
            mstrType = value
        End Set
    End Property

    Public Property _XmlPath() As String
        Get
            Return mstrXmlPath
        End Get
        Set(ByVal value As String)
            mstrXmlPath = value
        End Set
    End Property

    Public Property _MasterTable() As DataSet
        Get
            Return mdtMaster
        End Get
        Set(ByVal value As DataSet)
            mdtMaster = value
        End Set
    End Property

    Public Property _IsFinish() As Boolean
        Get
            Return mblnFinish
        End Get
        Set(ByVal value As Boolean)
            mblnFinish = value
        End Set
    End Property

    'S.SANDEEP [ 19 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstr_Database_Name As String = String.Empty
    Public Property _Database_Name() As String
        Get
            Return mstr_Database_Name
        End Get
        Set(ByVal value As String)
            mstr_Database_Name = value
        End Set
    End Property
    'S.SANDEEP [ 19 JUNE 2012 ] -- END


#End Region
    'Dipti (08 June 2012) - End

#Region " Private Methods "

    Private Sub ShowStep1_TemplateSeletion()
        Try
            lblNote.Text = "Step 1 : Set the proper template name."

            btnTestConnection.Visible = False
            btnPrevious.Visible = False
            btnFinish.Visible = False
            btnNext.Enabled = True
            btnNext.Visible = True

            pnlStep1_TemplateName.Visible = True
            pnlStep2_SelectionType.Visible = False
            pnlStep3_ServerSelection.Visible = False
            pnlStep3_XmlSelection.Visible = False
            pnlStep5_Query.Visible = False
            pnlStep4_SelectDatabase.Visible = False

            mintCnt = 1
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "ShowStep1_TemplateSeletion", mstrModuleName)
        End Try
    End Sub

    Private Sub ShowStep2_SelectionType()
        Try
            lblNote.Text = "Step 2 : Select the appropriate type of data for creating template."

            btnTestConnection.Visible = False
            btnPrevious.Visible = True
            btnFinish.Visible = False
            btnNext.Enabled = True
            btnNext.Visible = True

            pnlStep1_TemplateName.Visible = False
            pnlStep1_TemplateName.SendToBack()
            pnlStep2_SelectionType.Visible = True
            pnlStep2_SelectionType.BringToFront()
            pnlStep3_ServerSelection.Visible = False
            pnlStep3_XmlSelection.Visible = False
            pnlStep5_Query.Visible = False
            pnlStep4_SelectDatabase.Visible = False

            mintCnt = 2
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "ShowStep2_SelectionType", mstrModuleName)
        End Try
    End Sub

    Private Sub ShowStep3_XmlSeletion()
        Try
            lblNote.Text = "Step 3 : Select the appropriate XML for the template."

            btnTestConnection.Visible = False
            btnPrevious.Visible = True
            btnFinish.Visible = True
            btnNext.Visible = False

            pnlStep1_TemplateName.Visible = False
            pnlStep2_SelectionType.Visible = False
            pnlStep3_ServerSelection.Visible = False
            pnlStep3_XmlSelection.Visible = True
            pnlStep5_Query.Visible = False
            pnlStep4_SelectDatabase.Visible = False

            mintCnt = 3
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "ShowStep3_XmlSeletion", mstrModuleName)
        End Try
    End Sub

    Private Sub ShowStep3_ServerSelection()
        Try
            lblNote.Text = "Step 3 : Select the appropriate database server."

            btnTestConnection.Visible = True
            btnPrevious.Visible = True
            btnFinish.Visible = False
            btnNext.Visible = True
            btnNext.Enabled = False


            pnlStep1_TemplateName.Visible = False
            pnlStep2_SelectionType.Visible = False
            pnlStep3_ServerSelection.Visible = True
            pnlStep3_XmlSelection.Visible = False
            pnlStep5_Query.Visible = False
            pnlStep4_SelectDatabase.Visible = False

            mintCnt = 3
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "ShowStep3_ServerSelection", mstrModuleName)
        End Try
    End Sub

    Private Sub ShowStep4_DbSelection()
        Try
            lblNote.Text = "Step 4 : Select the appropriate database."

            btnTestConnection.Visible = True
            btnPrevious.Visible = True
            btnFinish.Visible = False
            btnNext.Enabled = True
            btnNext.Visible = True

            pnlStep1_TemplateName.Visible = False
            pnlStep2_SelectionType.Visible = False
            pnlStep3_ServerSelection.Visible = False
            pnlStep3_XmlSelection.Visible = False
            pnlStep5_Query.Visible = False
            pnlStep4_SelectDatabase.Visible = True
            mintCnt = 4
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "ShowStep4_DbSelection", mstrModuleName)
        End Try
    End Sub

    Private Sub ShowStep5_Query()
        Try
            lblNote.Text = "Step 5 : Write the appropriate valid sql query creating the template."

            btnTestConnection.Visible = False
            btnPrevious.Visible = True
            btnFinish.Visible = True
            btnNext.Visible = False

            pnlStep1_TemplateName.Visible = False
            pnlStep2_SelectionType.Visible = False
            pnlStep3_ServerSelection.Visible = False
            pnlStep3_XmlSelection.Visible = False
            pnlStep5_Query.Visible = True
            pnlStep4_SelectDatabase.Visible = False

            mintCnt = 5
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "ShowStep5_Query", mstrModuleName)
        End Try
    End Sub

    Private Sub fillCombo()
        Try
            cmbSeverName.Items.Clear()

            Dim SqlEnumerator As SqlDataSourceEnumerator
            SqlEnumerator = SqlDataSourceEnumerator.Instance
            Dim dTable As DataTable = SqlEnumerator.GetDataSources()
            Dim dr() As DataRow = dTable.Select("", "ServerName")
            For Each db As DataRow In dr
                cmbSeverName.Items.Add(db("ServerName").ToString() + "\" + db("InstanceName").ToString())
            Next
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillCombo", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form "

    Private Sub frmNewWizard_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            mConnection.Close()
            mConnection.Dispose()
            Dispose()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmNewWizard_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmNewWizard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Set_Logo(Me, gApplicationType)
            txtType.Text = ""
            lblNote.Text = "Step 1 : Set the proper template name."
            'S.SANDEEP [ 19 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'txtType.Text = mstrType
            'Call fillCombo()
            CheckForIllegalCrossThreadCalls = False
            bgFillDataSources.RunWorkerAsync()
            'S.SANDEEP [ 19 JUNE 2012 ] -- END
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmNewWizard_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        mblnFinish = False
        Me.Close()
    End Sub

    'Private Sub btnNextFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNextFinish.Click
    '    Try
    '        'If btnNextFinish.Text = "&Finish" Then
    '        '    If mintCnt = 2 Then
    '        '        If txtXmlPath.Text.Trim = "" Then
    '        '            eZeeMsgBox.Show("XML cannot be empty...", "eZee Message")
    '        '            txtXmlPath.Focus()
    '        '            Exit Sub
    '        '        End If
    '        '    Else
    '        '        Dim ds As New DataSet
    '        '        Dim daAdapter As New SqlClient.SqlDataAdapter(txtQuery.Text, mConnection)
    '        '        daAdapter.Fill(ds)
    '        '        If ds.Tables(0).Columns.Count > 0 Then
    '        '            If Not IO.Directory.Exists(My.Application.Info.DirectoryPath + "\DataXML") Then
    '        '                IO.Directory.CreateDirectory(My.Application.Info.DirectoryPath + "\DataXML")
    '        '            End If
    '        '            ds.WriteXml(My.Application.Info.DirectoryPath + "\DataXML\" + txtType.Text, XmlWriteMode.WriteSchema)
    '        '            txtXmlPath.Text = My.Application.Info.DirectoryPath + "\DataXML\" + txtType.Text
    '        '        End If
    '        '    End If

    '        '    mstrQuery = txtQuery.Text
    '        '    mstrType = txtType.Text
    '        '    mstrXmlPath = txtXmlPath.Text
    '        '    mblnFinish = True

    '        '    txtType.Text = ""
    '        '    txtXmlPath.Text = ""

    '        '    Me.Close()
    '        'Else

    '        If mintCnt = 0 Then
    '            If txtType.Text.Trim = "" Then
    '                'MessageBox.Show("Print Type cannot be empty...", "eZee Message")
    '                eZeeMsgBox.Show("Template name cannot be blank. Please enter the template name.", enMsgBoxStyle.Information)
    '                txtType.Focus()
    '                Exit Sub
    '            Else
    '                If mdtMaster.Tables.Count > 0 Then
    '                    Dim drow() As DataRow = mdtMaster.Tables(0).Select(" printtype = '" & txtType.Text & "'")
    '                    If drow.Length > 0 Then
    '                        'MessageBox.Show("Print Type already exists...", "eZee Message")
    '                        eZeeMsgBox.Show("Template name already exists. Please enter another template name.", enMsgBoxStyle.Information)
    '                        txtType.Focus()
    '                        Exit Sub
    '                    End If
    '                End If
    '            End If
    '        ElseIf mintCnt = 3 Then
    '            If cmbSeverName.Text.Trim = "" Then
    '                'MessageBox.Show("Database Name cannot be empty....", "eZee Message")
    '                eZeeMsgBox.Show("Please select atleast one database from the dropdown list.", enMsgBoxStyle.Information)
    '                cmbSeverName.Focus()
    '                Exit Sub
    '            Else
    '                Dim cmd As New SqlCommand("use " + cmbSeverName.Text, mConnection)
    '                cmd.ExecuteNonQuery()
    '                cmd.Dispose()
    '            End If
    '        End If

    '        mintCnt += 1
    '        If mintCnt = 1 Then
    '            '0
    '            'lblTemplateName.Visible = False
    '            'txtType.Visible = False
    '            pnlStep1_TemplateName.Visible = False

    '            '2 XML
    '            'txtXmlPath.Visible = False
    '            'btnOpen.Visible = False
    '            pnlStep3_XmlSelection.Visible = False

    '            '2 Sql
    '            'cmbSeverName.Visible = False
    '            'txtUserName.Visible = False
    '            'txtPassword.Visible = False
    '            btnTestConn.Visible = False
    '            'lblUserName.Visible = False
    '            'lblSeverName.Visible = False
    '            'lblPassword.Visible = False

    '            pnlStep3_ServerSelection.Visible = False

    '            '4 Query
    '            'lblQuery.Visible = False
    '            'txtQuery.Visible = False

    '            pnlStep4_Query.Visible = False

    '            '1
    '            'rbQuery.Visible = True
    '            'rbXML.Visible = True
    '            pnlStep3_XmlSelection.Visible = False
    '            pnlStep2_SelectionType.Visible = True
    '            pnlStep2_SelectionType.BringToFront()


    '            btnPrevious.Visible = True

    '            lblNote.Text = " This wizard will help you to accept the type of Data you would like to enter." + vbCrLf + _
    '                           " Note : XML Data will only select the xml. " + vbCrLf + _
    '                           "           Query selection will help you to write your own query "
    '        ElseIf mintCnt = 2 Then

    '            If rbXML.Checked Then
    '                '0
    '                'txtType.Visible = False
    '                pnlStep1_TemplateName.Visible = False

    '                '1
    '                'rbQuery.Visible = False
    '                'rbXML.Visible = False
    '                pnlStep2_SelectionType.Visible = False
    '                '2 XML
    '                'txtXmlPath.Visible = True
    '                'btnOpen.Visible = True
    '                'lblTemplateName.Visible = True
    '                pnlStep3_XmlSelection.Visible = True

    '                '2 Sql
    '                'cmbSeverName.Visible = False
    '                'txtUserName.Visible = False
    '                'txtPassword.Visible = False
    '                btnTestConn.Visible = False
    '                'lblUserName.Visible = False
    '                'lblSeverName.Visible = False
    '                'lblPassword.Visible = False
    '                pnlStep3_ServerSelection.Visible = False

    '                '4 Query
    '                'lblQuery.Visible = False
    '                'txtQuery.Visible = False
    '                pnlStep4_Query.Visible = False

    '                'txtXmlPath.Location = New System.Drawing.Point(txtXmlPath.Location.X, 59)
    '                'btnOpen.Location = New System.Drawing.Point(btnOpen.Location.X, 58)

    '                'lblTemplateName.Text = "New XML Path"
    '                lblNote.Text = " This wizard will help you to give any XML to our program." + vbCrLf + _
    '                               " Note : XML Data should be there otherwise it may give problem. "

    '                '   btnNextFinish.Text = "&Finish"
    '            Else


    '                '0
    '                'txtType.Visible = False
    '                pnlStep1_TemplateName.Visible = True
    '                '1
    '                'rbQuery.Visible = False
    '                'rbXML.Visible = False
    '                pnlStep2_SelectionType.Visible = False
    '                '2 XML
    '                'txtXmlPath.Visible = False
    '                'btnOpen.Visible = False
    '                'lblTemplateName.Visible = False
    '                pnlStep3_XmlSelection.Visible = False

    '                '2 Sql
    '                'cmbSeverName.Visible = True
    '                'txtUserName.Visible = True
    '                'txtPassword.Visible = True
    '                btnTestConn.Visible = True
    '                'lblUserName.Visible = True
    '                'lblSeverName.Visible = True
    '                'lblPassword.Visible = True
    '                pnlStep3_ServerSelection.Visible = True

    '                '4 Query
    '                'lblQuery.Visible = False
    '                'txtQuery.Visible = False
    '                pnlStep4_Query.Visible = False

    '                'lblSeverName.Text = "Server Name"
    '                If txtServerName.Text = "" Then
    '                    cmbSeverName.Text = txtServerName.Text
    '                End If
    '                btnNextFinish.Enabled = False



    '                Dim SqlEnumerator As SqlDataSourceEnumerator
    '                SqlEnumerator = SqlDataSourceEnumerator.Instance
    '                'Dim data As DataTable = SmoApplication.EnumAvailableSqlServers(False)
    '                Dim dTable As DataTable = SqlEnumerator.GetDataSources()
    '                Dim dr() As DataRow = dTable.Select("", "ServerName")
    '                cmbSeverName.Items.Clear()
    '                For Each db As DataRow In dr
    '                    cmbSeverName.Items.Add(db("ServerName").ToString() + "\" + db("InstanceName").ToString())
    '                Next
    '                lblNote.Text = " This wizard will help you to select server name." + vbCrLf + _
    '                               " Note : User Name and Password should be correct. "
    '            End If
    '        ElseIf mintCnt = 3 Then
    '            Dim dt As New DataTable
    '            Dim daAdapter As New SqlClient.SqlDataAdapter("Select Name from sys.databases", mConnection)
    '            daAdapter.Fill(dt)
    '            If dt.Rows.Count > 0 Then
    '                cmbSeverName.Items.Clear()

    '                lblSeverName.Text = "Database"

    '                lblSeverName.Visible = True
    '                cmbSeverName.Visible = True

    '                '0
    '                'txtType.Visible = False
    '                pnlStep1_TemplateName.Visible = False
    '                '1
    '                'rbQuery.Visible = False
    '                'rbXML.Visible = False
    '                pnlStep2_SelectionType.Visible = False
    '                '2 XML
    '                'txtXmlPath.Visible = False
    '                'btnOpen.Visible = False
    '                'lblTemplateName.Visible = False
    '                pnlStep3_XmlSelection.Visible = False
    '                '2 SQL
    '                'lblUserName.Visible = False
    '                'txtUserName.Visible = False
    '                'lblPassword.Visible = False
    '                'txtPassword.Visible = False
    '                btnTestConn.Visible = False
    '                pnlStep3_ServerSelection.Visible = False

    '                '4 Query
    '                'lblQuery.Visible = False
    '                'txtQuery.Visible = False
    '                pnlStep4_Query.Visible = False

    '                For i As Integer = 0 To dt.Rows.Count - 1
    '                    cmbSeverName.Items.Add(dt.Rows(i)(0))
    '                Next

    '                cmbSeverName.Text = txtDatabase.Name

    '                lblNote.Text = " This wizard will help you to select Database name." + vbCrLf + _
    '                               " Note : One Database should be selected. "

    '            End If
    '        ElseIf mintCnt = 4 Then
    '            '0
    '            'txtType.Visible = False
    '            pnlStep1_TemplateName.Visible = False
    '            '1
    '            'rbQuery.Visible = False
    '            'rbXML.Visible = False
    '            pnlStep2_SelectionType.Visible = False
    '            '2 XML
    '            'txtXmlPath.Visible = False
    '            'btnOpen.Visible = False
    '            'lblTemplateName.Visible = False
    '            pnlStep3_XmlSelection.Visible = False

    '            '2 Sql
    '            'cmbSeverName.Visible = False
    '            'txtUserName.Visible = False
    '            'txtPassword.Visible = False
    '            btnTestConn.Visible = False
    '            'lblUserName.Visible = False
    '            'lblSeverName.Visible = False
    '            'lblPassword.Visible = False
    '            pnlStep3_ServerSelection.Visible = False


    '            '4 Query
    '            'lblQuery.Visible = True
    '            'txtQuery.Visible = True
    '            pnlStep4_Query.Visible = True

    '            'txtXmlPath.Location = New System.Drawing.Point(txtXmlPath.Location.X, 59)
    '            'btnOpen.Location = New System.Drawing.Point(btnOpen.Location.X, 58)

    '            ' lblTemplateName.Text = "New XML Path"
    '            lblNote.Text = " This wizard will help you to write query." + vbCrLf + _
    '                           " Note : Query should be right. "

    '            ' btnNextFinish.Text = "&Finish"
    '        End If
    '        ' End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, My.Application.Info.Title, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub

    Private Sub btnNextFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Try
            If mintCnt = 1 Then
                If txtType.Text.Trim = "" Then
                    eZeeMsgBox.Show("Template name cannot be blank. Please enter the template name.", enMsgBoxStyle.Information)
                    txtType.Focus()
                    Exit Sub
                Else
                    If mdtMaster.Tables.Count > 0 Then
                        Dim drow() As DataRow = mdtMaster.Tables(0).Select(" printtype = '" & txtType.Text & "'")
                        If drow.Length > 0 Then
                            eZeeMsgBox.Show("Template name already exists. Please enter another template name.", enMsgBoxStyle.Information)
                            txtType.Focus()
                            Exit Sub
                        End If
                    End If
                End If

                Call ShowStep2_SelectionType()

            ElseIf mintCnt = 2 Then
                If rbXML.Checked Then
                    Call ShowStep3_XmlSeletion()
                Else
                    Call ShowStep3_ServerSelection()
                End If
            ElseIf mintCnt = 3 Then

                Dim dt As New DataTable
                Dim daAdapter As New SqlClient.SqlDataAdapter("Select Name from sys.databases", mConnection)
                daAdapter.Fill(dt)

                If dt.Rows.Count > 0 Then
                    cboDatabase.Items.Clear()
                    For i As Integer = 0 To dt.Rows.Count - 1
                        cboDatabase.Items.Add(dt.Rows(i)(0))
                    Next

                    cboDatabase.Text = txtDatabase.Name
                End If

                Call ShowStep4_DbSelection()
            ElseIf mintCnt = 4 Then
                If cboDatabase.Text.Trim = "" Then
                    eZeeMsgBox.Show("Please select atleast one database from the dropdown list.", enMsgBoxStyle.Information)
                    cboDatabase.Focus()
                    Exit Sub
                Else
                    Dim cmd As New SqlCommand("use " + cboDatabase.Text, mConnection)
                    cmd.ExecuteNonQuery()
                    cmd.Dispose()
                End If

                Call ShowStep5_Query()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    'Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
    '    mintCnt -= 1
    '    If mintCnt = 0 Then
    '        '0
    '        'lblTemplateName.Visible = True
    '        'txtType.Visible = True
    '        pnlStep1_TemplateName.Visible = True
    '        '1
    '        'rbQuery.Visible = False
    '        'rbXML.Visible = False
    '        pnlStep2_SelectionType.Visible = False
    '        '2 XML
    '        'txtXmlPath.Visible = False
    '        'btnOpen.Visible = False
    '        pnlStep3_XmlSelection.Visible = False
    '        '2 Sql
    '        'cmbSeverName.Visible = False
    '        'txtUserName.Visible = False
    '        'txtPassword.Visible = False
    '        btnTestConn.Visible = False
    '        'lblUserName.Visible = False
    '        'lblSeverName.Visible = False
    '        'lblPassword.Visible = False

    '        pnlStep3_ServerSelection.Visible = False

    '        btnPrevious.Visible = False

    '        ' lblTemplateName.Text = "New Print Type"
    '        lblNote.Text = " Welcome to Print Type wizard. It help's you to give a new Name of Print Type." + vbCrLf + _
    '                       " Note : Name already exists will return you message of incorrect entry. "
    '    ElseIf mintCnt = 1 Then
    '        '0
    '        lblTemplateName.Visible = False
    '        txtType.Visible = False
    '        '2 XML
    '        txtXmlPath.Visible = False
    '        btnOpen.Visible = False


    '        '2 Sql
    '        cmbSeverName.Visible = False
    '        txtUserName.Visible = False
    '        txtPassword.Visible = False
    '        btnTestConn.Visible = False
    '        lblUserName.Visible = False
    '        lblSeverName.Visible = False
    '        lblPassword.Visible = False

    '        '4 Query
    '        lblQuery.Visible = False
    '        txtQuery.Visible = False

    '        '1
    '        rbQuery.Visible = True
    '        rbXML.Visible = True
    '        btnNextFinish.Text = "&Next >>"

    '        btnNextFinish.Enabled = True

    '        lblNote.Text = " This wizard will help you to accept the type of Data you would like to enter." + vbCrLf + _
    '                       " Note : XML Data will only select the xml. " + vbCrLf + _
    '                       "          Query selection will help you to write your own query "

    '    ElseIf mintCnt = 2 Then
    '        '0
    '        txtType.Visible = False
    '        '1
    '        rbQuery.Visible = False
    '        rbXML.Visible = False
    '        '2 XML
    '        txtXmlPath.Visible = False
    '        btnOpen.Visible = False
    '        lblTemplateName.Visible = False

    '        '4 Query
    '        lblQuery.Visible = False
    '        txtQuery.Visible = False

    '        '2 Sql
    '        cmbSeverName.Visible = True
    '        txtUserName.Visible = True
    '        txtPassword.Visible = True
    '        btnTestConn.Visible = True
    '        lblUserName.Visible = True
    '        lblSeverName.Visible = True
    '        lblPassword.Visible = True

    '        lblSeverName.Text = "Server Name"

    '        btnNextFinish.Enabled = False

    '        Dim SqlEnumerator As SqlDataSourceEnumerator
    '        SqlEnumerator = SqlDataSourceEnumerator.Instance
    '        'Dim data As DataTable = SmoApplication.EnumAvailableSqlServers(False)
    '        Dim dTable As DataTable = SqlEnumerator.GetDataSources()
    '        Dim dr() As DataRow = dTable.Select("", "ServerName")
    '        cmbSeverName.Items.Clear()
    '        For Each db As DataRow In dr
    '            cmbSeverName.Items.Add(db("ServerName").ToString() + "\" + db("InstanceName").ToString())
    '        Next

    '        cmbSeverName.Text = txtServerName.Text
    '        lblNote.Text = " This wizard will help you to select server name." + vbCrLf + _
    '                       " Note : User Name and Password should be correct. "
    '    ElseIf mintCnt = 3 Then
    '        Dim dt As New DataTable
    '        Dim daAdapter As New SqlClient.SqlDataAdapter("Select Name from sys.databases", mConnection)
    '        daAdapter.Fill(dt)
    '        If dt.Rows.Count > 0 Then
    '            cmbSeverName.Items.Clear()

    '            lblSeverName.Text = "Database"
    '            lblSeverName.Visible = True
    '            cmbSeverName.Visible = True

    '            '0
    '            txtType.Visible = False
    '            '1
    '            rbQuery.Visible = False
    '            rbXML.Visible = False
    '            '2 XML
    '            txtXmlPath.Visible = False
    '            btnOpen.Visible = False
    '            lblTemplateName.Visible = False
    '            '2 SQL
    '            lblUserName.Visible = False
    '            txtUserName.Visible = False
    '            lblPassword.Visible = False
    '            txtPassword.Visible = False
    '            btnTestConn.Visible = False

    '            '4 Query
    '            lblQuery.Visible = False
    '            txtQuery.Visible = False



    '            For i As Integer = 0 To dt.Rows.Count - 1
    '                cmbSeverName.Items.Add(dt.Rows(i)(0))
    '            Next
    '            lblNote.Text = " This wizard will help you to select Database name." + vbCrLf + _
    '                           " Note : One Database should be selected. "

    '            btnNextFinish.Text = "&Next >>"
    '            cmbSeverName.Text = txtDatabase.Text
    '        End If
    '    End If
    'End Sub

    Private Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        Try
            If pnlStep3_XmlSelection.Visible Then
                If txtXmlPath.Text.Trim = "" Then
                    eZeeMsgBox.Show("XML cannot be empty...", enMsgBoxStyle.Information)
                    txtXmlPath.Focus()
                    Exit Sub
                End If
            Else
                If txtQuery.Text.Trim.Length <= 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 100, "SQL Query is mandatory information. Please provide Sql Query."), enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    If txtQuery.Text.Trim.ToUpper.Contains("INSERT ") Or _
                       txtQuery.Text.Trim.ToUpper.Contains("DELETE ") Or _
                       txtQuery.Text.Trim.ToUpper.Contains("UPDATE ") Or _
                       txtQuery.Text.Trim.ToUpper.Contains("TRUNCATE ") Or _
                       txtQuery.Text.Trim.ToUpper.Contains("CREATE ") Or _
                       txtQuery.Text.Trim.ToUpper.Contains("ALTER ") Or _
                       txtQuery.Text.Trim.ToUpper.Contains("DROP ") Or _
                       txtQuery.Text.Trim.ToUpper.Contains("INTO ") Or _
                       txtQuery.Text.Trim.ToUpper.Contains("EXEC ") Or _
                       txtQuery.Text.Trim.ToUpper.Contains("EXECUTE ")Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 101, "Sorry, you cannot change data. Please write only proper query."), enMsgBoxStyle.Information)
                        Exit Sub
                    End If
                End If
                Dim strDetail As String = "Select 1 AS id"
                Dim strQuery As String = strDetail & vbCrLf & txtQuery.Text
                'txtQuery.Text = strQuery
                Dim ds As New DataSet
                Dim daAdapter As New SqlClient.SqlDataAdapter(strQuery, mConnection)
                daAdapter.Fill(ds)
                If ds.Tables(0).Columns.Count > 0 Then
                    If Not IO.Directory.Exists(AppSettings._Object._ApplicationPath + "DataXML") Then
                        IO.Directory.CreateDirectory(AppSettings._Object._ApplicationPath & "DataXML")
                    End If
                    ds.WriteXml(AppSettings._Object._ApplicationPath & "DataXML\" + txtType.Text, XmlWriteMode.WriteSchema)
                    'txtXmlPath.Text = My.Application.Info.DirectoryPath + "\DataXML\" + txtType.Text
                    txtXmlPath.Text = txtType.Text
                End If
            End If

            mstrQuery = txtQuery.Text
            mstrType = txtType.Text
            mstrXmlPath = txtXmlPath.Text
            'S.SANDEEP [ 19 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            mstr_Database_Name = cboDatabase.Text
            'S.SANDEEP [ 19 JUNE 2012 ] -- END
            mblnFinish = True

            txtType.Text = ""
            txtXmlPath.Text = ""

            Me.Close()
        Catch ex As SqlException
            Dim StrMessage As String = String.Empty
            Select Case ex.Number
                Case 102
                    StrMessage = Language.getMessage(mstrModuleName, 501, "Sorry, Your Query has ") & ex.Message
                Case 207, 208, 209
                    StrMessage = Language.getMessage(mstrModuleName, 500, "Sorry, You have provided ") & ex.Message
                Case Else
                    StrMessage = ex.Message
            End Select
            eZeeMsgBox.Show(StrMessage, enMsgBoxStyle.Information)
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnFinish_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnTestConn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTestConnection.Click
        Try
            If cmbSeverName.Text.Trim = "" Then
                eZeeMsgBox.Show("Server Name cannot be empty", enMsgBoxStyle.Information)
                Exit Sub
            End If

            Dim strConnection As String = ""
            If txtUserName.Text = "" Then
                strConnection = "Data Source=" & cmbSeverName.Text & ";Integrated Security=True"
            Else
                strConnection = "Data Source=" & cmbSeverName.Text & ";User ID=" & txtUserName.Text & ";Password=" & txtPassword.Text
            End If

            mConnection = New SqlClient.SqlConnection(strConnection)
            mConnection.Open()

            If mConnection.State = ConnectionState.Open Then
                eZeeMsgBox.Show("Test Completed!", enMsgBoxStyle.Information)
                btnNext.Enabled = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, My.Application.Info.Title)
        End Try
    End Sub

    Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click
        Try
            Select Case mintCnt
                Case 2
                    Call ShowStep1_TemplateSeletion()
                Case 3
                    Call ShowStep2_SelectionType()
                Case 4
                    Call ShowStep3_ServerSelection()
                Case 5
                    Call ShowStep4_DbSelection()
            End Select
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnPrevious_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Other Controls "

    Private Sub txtXmlPath_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtXmlPath.KeyDown
        Try
            e.Handled = True
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "txtXmlPath_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub txtXmlPath_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtXmlPath.KeyPress
        Try
            e.Handled = True
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "txtXmlPath_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOpen_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOpen.Click
        Try
            Dim opFileDialog As New OpenFileDialog
            opFileDialog.Filter = "XML(*.XML)|*.XML|All(*.*)|*.*"
            opFileDialog.ShowDialog()

            If opFileDialog.FileName.Trim <> "" Then
                txtXmlPath.Text = opFileDialog.FileName
                tlpWizard.SetToolTip(txtXmlPath, txtXmlPath.Text)
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnOpen_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub cmbSeverName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSeverName.TextChanged
        Try
            txtServerName.Text = sender.text
            txtUserName.Text = ""
            txtPassword.Text = ""
            ' btnNextFinish.Enabled = False
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cmbSeverName_TextChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboDatabase_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDatabase.TextChanged
        Try
            txtDatabase.Text = sender.text
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "cboDatabase_TextChanged", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [ 19 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub bgFillDataSources_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgFillDataSources.DoWork
        Call fillCombo()
    End Sub
    'S.SANDEEP [ 19 JUNE 2012 ] -- END

#End Region

End Class