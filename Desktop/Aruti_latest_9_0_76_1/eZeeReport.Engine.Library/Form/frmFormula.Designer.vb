﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFormula
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Add (x+y)")
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Subtract (x-y)")
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Multiply (x*y)")
        Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Divide (x/y)")
        Dim TreeNode5 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Arithmetic", New System.Windows.Forms.TreeNode() {TreeNode1, TreeNode2, TreeNode3, TreeNode4})
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFormula))
        Me.tvReportFields = New System.Windows.Forms.TreeView
        Me.txtExpression = New System.Windows.Forms.TextBox
        Me.lblTree = New System.Windows.Forms.Label
        Me.txtExpback = New System.Windows.Forms.TextBox
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.splMain = New System.Windows.Forms.SplitContainer
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer
        Me.tblFields = New System.Windows.Forms.TableLayoutPanel
        Me.tblOperators = New System.Windows.Forms.TableLayoutPanel
        Me.tvOperators = New System.Windows.Forms.TreeView
        Me.lblOperators = New System.Windows.Forms.Label
        Me.tblExpression = New System.Windows.Forms.TableLayoutPanel
        Me.lblExpression = New System.Windows.Forms.Label
        Me.tblTop = New System.Windows.Forms.TableLayoutPanel
        Me.tsMain = New System.Windows.Forms.ToolStrip
        Me.btnExecute = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep1 = New System.Windows.Forms.ToolStripSeparator
        Me.btnIsNumeric = New System.Windows.Forms.ToolStripButton
        Me.objtsmiSep2 = New System.Windows.Forms.ToolStripSeparator
        Me.chkContinousSum = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.chkDecimal = New System.Windows.Forms.ToolStripButton
        Me.cboDecimalPlaces = New System.Windows.Forms.ToolStripComboBox
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.chkInWord = New System.Windows.Forms.ToolStripButton
        Me.cboWordDec = New System.Windows.Forms.ToolStripComboBox
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.btnClear = New System.Windows.Forms.ToolStripButton
        Me.tsSaveClose = New System.Windows.Forms.ToolStrip
        Me.btnNew = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator
        Me.btnSave = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.btnCancel = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator
        Me.pnlMain.SuspendLayout()
        Me.splMain.Panel1.SuspendLayout()
        Me.splMain.Panel2.SuspendLayout()
        Me.splMain.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.tblFields.SuspendLayout()
        Me.tblOperators.SuspendLayout()
        Me.tblExpression.SuspendLayout()
        Me.tblTop.SuspendLayout()
        Me.tsMain.SuspendLayout()
        Me.tsSaveClose.SuspendLayout()
        Me.SuspendLayout()
        '
        'tvReportFields
        '
        Me.tvReportFields.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvReportFields.Location = New System.Drawing.Point(6, 29)
        Me.tvReportFields.Name = "tvReportFields"
        Me.tvReportFields.ShowNodeToolTips = True
        Me.tvReportFields.Size = New System.Drawing.Size(303, 209)
        Me.tvReportFields.TabIndex = 0
        '
        'txtExpression
        '
        Me.txtExpression.AllowDrop = True
        Me.txtExpression.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtExpression.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExpression.Location = New System.Drawing.Point(6, 29)
        Me.txtExpression.Multiline = True
        Me.txtExpression.Name = "txtExpression"
        Me.txtExpression.Size = New System.Drawing.Size(645, 128)
        Me.txtExpression.TabIndex = 1
        '
        'lblTree
        '
        Me.lblTree.AutoSize = True
        Me.lblTree.BackColor = System.Drawing.Color.Silver
        Me.lblTree.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblTree.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTree.ForeColor = System.Drawing.Color.Black
        Me.lblTree.Location = New System.Drawing.Point(3, 3)
        Me.lblTree.Margin = New System.Windows.Forms.Padding(0)
        Me.lblTree.Name = "lblTree"
        Me.lblTree.Size = New System.Drawing.Size(309, 20)
        Me.lblTree.TabIndex = 2
        Me.lblTree.Text = "Report Fields"
        Me.lblTree.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtExpback
        '
        Me.txtExpback.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtExpback.Location = New System.Drawing.Point(710, 321)
        Me.txtExpback.Multiline = True
        Me.txtExpback.Name = "txtExpback"
        Me.txtExpback.Size = New System.Drawing.Size(358, 26)
        Me.txtExpback.TabIndex = 8
        Me.txtExpback.Visible = False
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.splMain)
        Me.pnlMain.Controls.Add(Me.txtExpback)
        Me.pnlMain.Controls.Add(Me.tblTop)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(657, 479)
        Me.pnlMain.TabIndex = 14
        '
        'splMain
        '
        Me.splMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.splMain.Location = New System.Drawing.Point(0, 68)
        Me.splMain.Name = "splMain"
        Me.splMain.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'splMain.Panel1
        '
        Me.splMain.Panel1.Controls.Add(Me.SplitContainer2)
        '
        'splMain.Panel2
        '
        Me.splMain.Panel2.Controls.Add(Me.tblExpression)
        Me.splMain.Size = New System.Drawing.Size(657, 411)
        Me.splMain.SplitterDistance = 244
        Me.splMain.TabIndex = 16
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.tblFields)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.tblOperators)
        Me.SplitContainer2.Size = New System.Drawing.Size(657, 244)
        Me.SplitContainer2.SplitterDistance = 315
        Me.SplitContainer2.TabIndex = 0
        '
        'tblFields
        '
        Me.tblFields.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble
        Me.tblFields.ColumnCount = 1
        Me.tblFields.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblFields.Controls.Add(Me.tvReportFields, 0, 1)
        Me.tblFields.Controls.Add(Me.lblTree, 0, 0)
        Me.tblFields.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblFields.Location = New System.Drawing.Point(0, 0)
        Me.tblFields.Name = "tblFields"
        Me.tblFields.RowCount = 2
        Me.tblFields.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblFields.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblFields.Size = New System.Drawing.Size(315, 244)
        Me.tblFields.TabIndex = 0
        '
        'tblOperators
        '
        Me.tblOperators.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble
        Me.tblOperators.ColumnCount = 1
        Me.tblOperators.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblOperators.Controls.Add(Me.tvOperators, 0, 1)
        Me.tblOperators.Controls.Add(Me.lblOperators, 0, 0)
        Me.tblOperators.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblOperators.Location = New System.Drawing.Point(0, 0)
        Me.tblOperators.Name = "tblOperators"
        Me.tblOperators.RowCount = 2
        Me.tblOperators.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblOperators.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblOperators.Size = New System.Drawing.Size(338, 244)
        Me.tblOperators.TabIndex = 2
        '
        'tvOperators
        '
        Me.tvOperators.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvOperators.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tvOperators.Location = New System.Drawing.Point(6, 29)
        Me.tvOperators.Name = "tvOperators"
        TreeNode1.Name = "nAdd"
        TreeNode1.Tag = "+"
        TreeNode1.Text = "Add (x+y)"
        TreeNode2.Name = "nSubtract"
        TreeNode2.Tag = "-"
        TreeNode2.Text = "Subtract (x-y)"
        TreeNode3.Name = "nMultiply"
        TreeNode3.Tag = "*"
        TreeNode3.Text = "Multiply (x*y)"
        TreeNode4.Name = "nDivide"
        TreeNode4.Tag = "/"
        TreeNode4.Text = "Divide (x/y)"
        TreeNode5.Name = "ndArithmetic"
        TreeNode5.Text = "Arithmetic"
        Me.tvOperators.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode5})
        Me.tvOperators.ShowNodeToolTips = True
        Me.tvOperators.Size = New System.Drawing.Size(326, 209)
        Me.tvOperators.TabIndex = 0
        '
        'lblOperators
        '
        Me.lblOperators.AutoSize = True
        Me.lblOperators.BackColor = System.Drawing.Color.Silver
        Me.lblOperators.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblOperators.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperators.ForeColor = System.Drawing.Color.Black
        Me.lblOperators.Location = New System.Drawing.Point(3, 3)
        Me.lblOperators.Margin = New System.Windows.Forms.Padding(0)
        Me.lblOperators.Name = "lblOperators"
        Me.lblOperators.Size = New System.Drawing.Size(332, 20)
        Me.lblOperators.TabIndex = 2
        Me.lblOperators.Text = "Operators"
        Me.lblOperators.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tblExpression
        '
        Me.tblExpression.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble
        Me.tblExpression.ColumnCount = 1
        Me.tblExpression.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblExpression.Controls.Add(Me.lblExpression, 0, 0)
        Me.tblExpression.Controls.Add(Me.txtExpression, 0, 1)
        Me.tblExpression.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblExpression.Location = New System.Drawing.Point(0, 0)
        Me.tblExpression.Name = "tblExpression"
        Me.tblExpression.RowCount = 2
        Me.tblExpression.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblExpression.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblExpression.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblExpression.Size = New System.Drawing.Size(657, 163)
        Me.tblExpression.TabIndex = 17
        '
        'lblExpression
        '
        Me.lblExpression.AutoSize = True
        Me.lblExpression.BackColor = System.Drawing.Color.Silver
        Me.lblExpression.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblExpression.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExpression.ForeColor = System.Drawing.Color.Black
        Me.lblExpression.Location = New System.Drawing.Point(3, 3)
        Me.lblExpression.Margin = New System.Windows.Forms.Padding(0)
        Me.lblExpression.Name = "lblExpression"
        Me.lblExpression.Size = New System.Drawing.Size(651, 20)
        Me.lblExpression.TabIndex = 3
        Me.lblExpression.Text = "Expression"
        Me.lblExpression.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tblTop
        '
        Me.tblTop.BackColor = System.Drawing.Color.White
        Me.tblTop.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.OutsetDouble
        Me.tblTop.ColumnCount = 1
        Me.tblTop.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblTop.Controls.Add(Me.tsMain, 0, 0)
        Me.tblTop.Controls.Add(Me.tsSaveClose, 0, 1)
        Me.tblTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.tblTop.Location = New System.Drawing.Point(0, 0)
        Me.tblTop.Name = "tblTop"
        Me.tblTop.RowCount = 2
        Me.tblTop.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblTop.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblTop.Size = New System.Drawing.Size(657, 68)
        Me.tblTop.TabIndex = 17
        '
        'tsMain
        '
        Me.tsMain.AutoSize = False
        Me.tsMain.BackColor = System.Drawing.Color.White
        Me.tsMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tsMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnExecute, Me.objtsmiSep1, Me.btnIsNumeric, Me.objtsmiSep2, Me.chkContinousSum, Me.ToolStripSeparator1, Me.chkDecimal, Me.cboDecimalPlaces, Me.ToolStripSeparator2, Me.chkInWord, Me.cboWordDec, Me.ToolStripSeparator3, Me.btnClear})
        Me.tsMain.Location = New System.Drawing.Point(3, 3)
        Me.tsMain.Name = "tsMain"
        Me.tsMain.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tsMain.Size = New System.Drawing.Size(651, 29)
        Me.tsMain.TabIndex = 0
        Me.tsMain.Text = "ToolStrip1"
        '
        'btnExecute
        '
        Me.btnExecute.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.OK_24x24
        Me.btnExecute.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnExecute.Name = "btnExecute"
        Me.btnExecute.Size = New System.Drawing.Size(56, 26)
        Me.btnExecute.Text = "&Check"
        '
        'objtsmiSep1
        '
        Me.objtsmiSep1.Name = "objtsmiSep1"
        Me.objtsmiSep1.Size = New System.Drawing.Size(6, 29)
        '
        'btnIsNumeric
        '
        Me.btnIsNumeric.Enabled = False
        Me.btnIsNumeric.Image = CType(resources.GetObject("btnIsNumeric.Image"), System.Drawing.Image)
        Me.btnIsNumeric.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnIsNumeric.Name = "btnIsNumeric"
        Me.btnIsNumeric.Size = New System.Drawing.Size(74, 26)
        Me.btnIsNumeric.Text = "IsNumeric"
        '
        'objtsmiSep2
        '
        Me.objtsmiSep2.Name = "objtsmiSep2"
        Me.objtsmiSep2.Size = New System.Drawing.Size(6, 29)
        '
        'chkContinousSum
        '
        Me.chkContinousSum.CheckOnClick = True
        Me.chkContinousSum.Enabled = False
        Me.chkContinousSum.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.GroupBy_16
        Me.chkContinousSum.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.chkContinousSum.Name = "chkContinousSum"
        Me.chkContinousSum.Size = New System.Drawing.Size(114, 26)
        Me.chkContinousSum.Text = "Do Continous Sum"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 29)
        '
        'chkDecimal
        '
        Me.chkDecimal.CheckOnClick = True
        Me.chkDecimal.Enabled = False
        Me.chkDecimal.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.CheckBoxNone_24
        Me.chkDecimal.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.chkDecimal.Name = "chkDecimal"
        Me.chkDecimal.Size = New System.Drawing.Size(105, 26)
        Me.chkDecimal.Text = "Decimal Settings"
        '
        'cboDecimalPlaces
        '
        Me.cboDecimalPlaces.AutoSize = False
        Me.cboDecimalPlaces.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDecimalPlaces.DropDownWidth = 30
        Me.cboDecimalPlaces.Enabled = False
        Me.cboDecimalPlaces.FlatStyle = System.Windows.Forms.FlatStyle.Standard
        Me.cboDecimalPlaces.IntegralHeight = False
        Me.cboDecimalPlaces.Items.AddRange(New Object() {"0", "1", "2", "3", "4"})
        Me.cboDecimalPlaces.Name = "cboDecimalPlaces"
        Me.cboDecimalPlaces.Size = New System.Drawing.Size(50, 21)
        Me.cboDecimalPlaces.Sorted = True
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 29)
        '
        'chkInWord
        '
        Me.chkInWord.CheckOnClick = True
        Me.chkInWord.Enabled = False
        Me.chkInWord.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.CheckBoxNone_24
        Me.chkInWord.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.chkInWord.Name = "chkInWord"
        Me.chkInWord.Size = New System.Drawing.Size(97, 26)
        Me.chkInWord.Text = "Show InWords"
        '
        'cboWordDec
        '
        Me.cboWordDec.AutoSize = False
        Me.cboWordDec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboWordDec.DropDownWidth = 50
        Me.cboWordDec.Enabled = False
        Me.cboWordDec.FlatStyle = System.Windows.Forms.FlatStyle.Standard
        Me.cboWordDec.Items.AddRange(New Object() {"0", "1", "2", "3", "4"})
        Me.cboWordDec.Name = "cboWordDec"
        Me.cboWordDec.Size = New System.Drawing.Size(50, 21)
        Me.cboWordDec.Sorted = True
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 29)
        '
        'btnClear
        '
        Me.btnClear.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Refresh
        Me.btnClear.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(52, 26)
        Me.btnClear.Text = "Clear"
        '
        'tsSaveClose
        '
        Me.tsSaveClose.AllowDrop = True
        Me.tsSaveClose.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.tsSaveClose.AutoSize = False
        Me.tsSaveClose.BackColor = System.Drawing.Color.White
        Me.tsSaveClose.Dock = System.Windows.Forms.DockStyle.None
        Me.tsSaveClose.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNew, Me.ToolStripSeparator5, Me.btnSave, Me.ToolStripSeparator4, Me.btnCancel, Me.ToolStripSeparator6})
        Me.tsSaveClose.Location = New System.Drawing.Point(3, 35)
        Me.tsSaveClose.Name = "tsSaveClose"
        Me.tsSaveClose.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.tsSaveClose.Size = New System.Drawing.Size(200, 30)
        Me.tsSaveClose.TabIndex = 14
        Me.tsSaveClose.Text = "ToolStrip1"
        '
        'btnNew
        '
        Me.btnNew.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.New_16
        Me.btnNew.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(48, 27)
        Me.btnNew.Text = "&New"
        Me.btnNew.Visible = False
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 30)
        Me.ToolStripSeparator5.Visible = False
        '
        'btnSave
        '
        Me.btnSave.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Save_24
        Me.btnSave.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(51, 27)
        Me.btnSave.Text = "&Save"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(6, 30)
        '
        'btnCancel
        '
        Me.btnCancel.Image = Global.ArutiReport.Engine.Library.My.Resources.Resources.Exit_App_16
        Me.btnCancel.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(53, 27)
        Me.btnCancel.Text = "&Close"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 30)
        '
        'frmFormula
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.ClientSize = New System.Drawing.Size(657, 479)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(665, 513)
        Me.Name = "frmFormula"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Formula Editor"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.splMain.Panel1.ResumeLayout(False)
        Me.splMain.Panel2.ResumeLayout(False)
        Me.splMain.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        Me.tblFields.ResumeLayout(False)
        Me.tblFields.PerformLayout()
        Me.tblOperators.ResumeLayout(False)
        Me.tblOperators.PerformLayout()
        Me.tblExpression.ResumeLayout(False)
        Me.tblExpression.PerformLayout()
        Me.tblTop.ResumeLayout(False)
        Me.tsMain.ResumeLayout(False)
        Me.tsMain.PerformLayout()
        Me.tsSaveClose.ResumeLayout(False)
        Me.tsSaveClose.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tvReportFields As System.Windows.Forms.TreeView
    Friend WithEvents txtExpression As System.Windows.Forms.TextBox
    Public WithEvents lblTree As System.Windows.Forms.Label
    Friend WithEvents txtExpback As System.Windows.Forms.TextBox
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents tsMain As System.Windows.Forms.ToolStrip
    Friend WithEvents btnExecute As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtsmiSep1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnIsNumeric As System.Windows.Forms.ToolStripButton
    Friend WithEvents objtsmiSep2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents chkContinousSum As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsSaveClose As System.Windows.Forms.ToolStrip
    Friend WithEvents btnSave As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnCancel As System.Windows.Forms.ToolStripButton
    Friend WithEvents splMain As System.Windows.Forms.SplitContainer
    Friend WithEvents tblExpression As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tblFields As System.Windows.Forms.TableLayoutPanel
    Public WithEvents lblExpression As System.Windows.Forms.Label
    Friend WithEvents tblOperators As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tvOperators As System.Windows.Forms.TreeView
    Public WithEvents lblOperators As System.Windows.Forms.Label
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents chkDecimal As System.Windows.Forms.ToolStripButton
    Friend WithEvents cboDecimalPlaces As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents chkInWord As System.Windows.Forms.ToolStripButton
    Friend WithEvents cboWordDec As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnClear As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnNew As System.Windows.Forms.ToolStripButton
    Friend WithEvents tblTop As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
End Class
