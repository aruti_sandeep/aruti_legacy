﻿Public Class frmGrouping
    Public dtTable As New DataTable
    Public lOk As Boolean = False
    Public strSelect As String = ""
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
        lOk = False
    End Sub
    Public Sub ShowMeNow(ByVal trNode As TreeNode)
        Dim nCtr As Integer = 0
        tvControl.Font = New Font("Arial", 8, FontStyle.Bold)
        tvGrouping.Font = New Font("Arial", 8, FontStyle.Bold)
        For i As Int16 = 0 To trNode.Nodes.Count - 1
            If Not trNode.Nodes(i).Text Like ".Formula_*" Then
                tvControl.Nodes.Add(trNode.Nodes(i).Text)
                tvControl.Nodes(nCtr).Tag = trNode.Nodes(i).Tag
                tvControl.Nodes(nCtr).ToolTipText = trNode.Nodes(i).ToolTipText
                nCtr += 1
            End If
        Next
        If strSelect.Length > 0 Then
            For i As Integer = 0 To strSelect.Split(",").Length - 1
                If strSelect.Split(",")(i) <> "" Then
                    tvGrouping.Nodes.Add(strSelect.Split(",")(i))
                End If
            Next
        End If
        'If tvGrouping.Nodes.Count > 0 Then
        '    btnOk.Enabled = True
        'End If
        ShowDialog()
    End Sub
    Private Sub AddDetailPart()
        Dim lFlag As Boolean = False
        For i As Integer = 0 To tvGrouping.Nodes.Count - 1
            If tvGrouping.Nodes(i).Text = tvControl.SelectedNode.Text Then
                lFlag = True
                Exit For
            End If
        Next
        If lFlag Then
            MessageBox.Show("The cuurent item already exists", "eZee Message")
        Else
            tvGrouping.Nodes.Add(tvControl.SelectedNode.Text)
            tvGrouping.Nodes(tvGrouping.Nodes.Count - 1).Tag = tvControl.Nodes(tvControl.SelectedNode.Index).Tag
            tvGrouping.Nodes(tvGrouping.Nodes.Count - 1).ToolTipText = tvControl.Nodes(tvControl.SelectedNode.Index).ToolTipText
            btnOk.Enabled = True
        End If
    End Sub
    Private Sub tvControl_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvControl.DoubleClick
        AddDetailPart()
    End Sub

    Private Sub tvGrouping_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvGrouping.DoubleClick
        Remove()
    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        AddDetailPart()
    End Sub

    Private Sub btnReject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Remove()
    End Sub
    Private Sub Remove()
        If tvGrouping.Nodes.Count > 0 Then
            tvGrouping.Nodes.Remove(tvGrouping.SelectedNode)
        End If
        'If tvGrouping.Nodes.Count = 0 Then
        '    btnOk.Enabled = False
        'Else
        '    btnOk.Enabled = True
        'End If
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        strSelect = ""
        If tvGrouping.Nodes.Count > 0 Then
            For i As Integer = 0 To tvGrouping.Nodes.Count - 1
                strSelect += tvGrouping.Nodes(i).Text + ","
            Next
        End If
        If strSelect.Length > 0 Then
            strSelect = strSelect.Substring(0, strSelect.Length - 1)
        End If
        lOk = True
        Close()
    End Sub
End Class