﻿Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.IO
Imports System.Drawing
Imports System.Data
Imports System.Windows.Forms
Imports System.Windows.Forms.Design
Imports System.Runtime.Serialization
Imports System.Runtime.Serialization.Formatters.Binary

Public Class TemplatePropertyGrid
    Inherits PropertyGrid

    Private components As System.ComponentModel.Container = Nothing

    Public Sub New()
        InitializeComponent()
    End Sub

    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If components IsNot Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Codice generato da Progettazione componenti"
    ''' <summary> 
    ''' Metodo necessario per il supporto della finestra di progettazione. Non modificare 
    ''' il contenuto del metodo con l'editor di codice. 
    ''' </summary> 
    Private Sub InitializeComponent()
        ' 
        ' UserControl1 
        ' 

        Me.Name = "myPropertyGrid"
    End Sub
#End Region

    Protected Overloads Overrides Function CreatePropertyTab(ByVal tabType As Type) As PropertyTab
        Dim t As New CustomPropertyGridTab()
        Return t
    End Function
End Class

Public Class CustomPropertyGridTab
    Inherits PropertyTab

    Public Sub New()

    End Sub

    ' get the properties of the selected component 
    Public Overloads Overrides Function GetProperties(ByVal component As Object, ByVal attributes As System.Attribute()) As System.ComponentModel.PropertyDescriptorCollection
        Dim properties As PropertyDescriptorCollection
        If attributes IsNot Nothing Then
            properties = TypeDescriptor.GetProperties(component, attributes)
        Else
            properties = TypeDescriptor.GetProperties(component)
        End If

        If TypeOf component Is System.Drawing.Font Then
            Return Nothing
        End If
        If TypeOf component Is System.Drawing.Bitmap Then
            Return Nothing
        End If
        'Componet must implement the ICUSTOMCLASS interface. 
        Dim bclass As ITemplatePropertyClass = DirectCast(component, ITemplatePropertyClass)

        'The new array of properties, based on the PublicProperties properties of "model" 
        Dim arrProp As PropertyDescriptor() = New PropertyDescriptor(bclass.PublicProperties.Count - 1) {}

        For i As Integer = 0 To bclass.PublicProperties.Count - 1
            'Find the properties in the array of the propertis which neme is in the PubliCProperties 
            Dim prop As PropertyDescriptor = properties.Find(bclass.PublicProperties(i).Name, True)
            'Build a new properties 
            If prop IsNot Nothing Then
                Dim attribCol(1) As Attribute
                If bclass.PublicProperties(i).DisplayName <> "" Then
                    attribCol(0) = New DisplayNameAttribute(bclass.PublicProperties(i).DisplayName)
                End If
                attribCol(1) = New CategoryAttribute(bclass.PublicProperties(i).Category)
                arrProp(i) = TypeDescriptor.CreateProperty(prop.ComponentType, prop, attribCol)

            End If
        Next
        Return New PropertyDescriptorCollection(arrProp)
    End Function

    Public Overloads Overrides Function GetProperties(ByVal component As Object) As System.ComponentModel.PropertyDescriptorCollection
        Return Me.GetProperties(component, Nothing)
    End Function

    ' PropertyTab Name 
    Public Overloads Overrides ReadOnly Property TabName() As String
        Get
            Return "Properties"
        End Get
    End Property

    'Image of the property tab (return a blank 16x16 Bitmap) 
    Public Overloads Overrides ReadOnly Property Bitmap() As System.Drawing.Bitmap
        Get
            Return New Bitmap(32, 32)
        End Get
    End Property
End Class


