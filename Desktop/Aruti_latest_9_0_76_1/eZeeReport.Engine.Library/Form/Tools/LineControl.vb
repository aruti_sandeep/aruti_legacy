Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Runtime.CompilerServices
Imports System.Windows.Forms

Public Delegate Sub MovedResizedLineEventHandler(ByVal sender As Object, ByVal e As EventArgs)
Public Delegate Sub SelectedLineEventHandler(ByVal sender As Object, ByVal e As EventArgs)

Public Interface ICommonLineControl
    Property CommonLineProperty() As LineControl
End Interface

<Serializable()> _
Public Class LineControl
    Inherits UserControl

#Region "Events Declaration"
    ' Events
    Public Event MovedResized As MovedResizedLineEventHandler
    Public Event Selected As SelectedLineEventHandler
#End Region

#Region "Enum"
    Public Enum DragMode
        Move = 0
        ResizeLeft = 8
        ResizeRight = 4
        ResizeBottom = 6
        ResizeTop = 2
    End Enum
#End Region

#Region "Variables"
    Private components As Container
    Private _dragMode As DragMode
    Private _textAlign As ContentAlignment
    Private _formatSting As StringFormat
    Private _horizontalMid As Single
    Private _isDragging As Boolean
    Private _isSelected As Boolean
    Private _lastMouseX As Integer
    Private _lastMouseY As Integer
    Private _minHeight As Integer = 5
    Private _minWidth As Integer = 5
    Private _sizerWidth As Integer = 6
    Private _verticalMid As Single
    Private _leftRect As Rectangle
    Private _topRect As Rectangle
    Private _bottomRect As Rectangle
    Private _rightRect As Rectangle
    Private _resizedRect As Rectangle
    Private _screenRect As Rectangle
    Private _sizerRectangles As ArrayList

    Public _lineStyle As System.Drawing.Drawing2D.DashStyle
    Public _lineColor As Color = Color.Black
    Public _isvertical As Boolean = False
    Public _lineThickness As Integer = 1

    Public intFieldType As Integer

    'Public components As Container
    'Public _dragMode As DragMode
    'Public _textAlign As ContentAlignment
    'Public _formatSting As StringFormat
    'Public _horizontalMid As Single
    'Public _isDragging As Boolean
    'Public _isSelected As Boolean
    'Public _lastMouseX As Integer
    'Public _lastMouseY As Integer
    'Public _minHeight As Integer = 5
    'Public _minWidth As Integer = 5
    'Public _sizerWidth As Integer = 6
    'Public _verticalMid As Single
    'Public _isvertical As Boolean = False
    'Public _leftRect As Rectangle
    'Public _topRect As Rectangle
    'Public _bottomRect As Rectangle
    'Public _rightRect As Rectangle
    'Public _resizedRect As Rectangle
    'Public _screenRect As Rectangle
    'Public _sizerRectangles As ArrayList
    'Public _lineStyle As System.Drawing.Drawing2D.DashStyle
    'Public _lineColor As Color = Color.Black
#End Region

#Region "Properties"
    'Issue: For Transparent Line Background
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ExStyle = cp.ExStyle Or &H20
            Return cp
        End Get
    End Property

    <Browsable(True), DefaultValue("Text"), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)> _
    Public Overrides Property Text() As String
        Get
            Return MyBase.Text
        End Get
        Set(ByVal value As String)
            MyBase.Text = value
            Me.Invalidate()
        End Set
    End Property

    <Description("Minimum height of resizable control"), Category("Appearance")> _
       Public Property MinHeight() As Integer
        Get
            Return Me._minHeight
        End Get
        Set(ByVal value As Integer)
            'ArgumentValidation.CheckCondition(value, "MinHeight", (value > 0))
            Me._minHeight = value
        End Set
    End Property

    <Description("Minimum width of resizable control"), Category("Appearance")> _
    Public Property MinWidth() As Integer
        Get
            Return Me._minWidth
        End Get
        Set(ByVal value As Integer)
            'ArgumentValidation.CheckCondition(value, "MinWidth", (value > 0))
            Me._minWidth = value
        End Set
    End Property

    <Description("Width of resize handles"), Category("Appearance")> _
    Public Property SizerWidth() As Integer
        Get
            Return Me._sizerWidth
        End Get
        Set(ByVal value As Integer)
            'ArgumentValidation.CheckCondition(value, "SizerWidth", (value > 0))
            Me._sizerWidth = value
            Me.UpdateSizers()
        End Set
    End Property

    <DefaultValue(&H10), DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)> _
   Public Property TextAlign() As ContentAlignment
        Get
            Return Me._textAlign
        End Get
        Set(ByVal value As ContentAlignment)
            Me._textAlign = value
            Select Case Me._textAlign
                Case ContentAlignment.TopLeft
                    _formatSting.LineAlignment = StringAlignment.Near
                    _formatSting.Alignment = StringAlignment.Near
                Case ContentAlignment.TopCenter
                    _formatSting.LineAlignment = StringAlignment.Near
                    _formatSting.Alignment = StringAlignment.Center
                Case ContentAlignment.TopRight
                    _formatSting.LineAlignment = StringAlignment.Near
                    _formatSting.Alignment = StringAlignment.Far

                Case ContentAlignment.MiddleLeft
                    _formatSting.LineAlignment = StringAlignment.Center
                    _formatSting.Alignment = StringAlignment.Near
                Case ContentAlignment.MiddleCenter
                    _formatSting.LineAlignment = StringAlignment.Center
                    _formatSting.Alignment = StringAlignment.Center
                Case ContentAlignment.MiddleRight
                    _formatSting.LineAlignment = StringAlignment.Center
                    _formatSting.Alignment = StringAlignment.Far

                Case ContentAlignment.BottomLeft
                    _formatSting.LineAlignment = StringAlignment.Far
                    _formatSting.Alignment = StringAlignment.Near
                Case ContentAlignment.BottomCenter
                    _formatSting.LineAlignment = StringAlignment.Far
                    _formatSting.Alignment = StringAlignment.Center
                Case ContentAlignment.BottomRight
                    _formatSting.LineAlignment = StringAlignment.Far
                    _formatSting.Alignment = StringAlignment.Far
            End Select
            Me.Invalidate()
        End Set
    End Property

    <Description("Line Vertical"), Category("Appearance")> _
   Public Property Vertical() As Boolean
        Get
            Return Me._isvertical
        End Get
        Set(ByVal value As Boolean)
            Me._isvertical = value
            'If Me.DesignMode Then
            If Me._isvertical = True Then
                If MyBase.Width > MyBase.Height Then
                    Me.Size = New System.Drawing.Size(MyBase.Height, MyBase.Width)
                End If
            Else
                If MyBase.Height > MyBase.Width Then
                    Me.Size = New System.Drawing.Size(MyBase.Height, MyBase.Width)
                End If
            End If
            'End If
            Me.UpdateSizers()
            Me.Refresh()
        End Set
    End Property

    <Description("Line Style"), Category("Appearance")> _
      Public Property LineStyle() As System.Drawing.Drawing2D.DashStyle
        Get
            Return Me._lineStyle
        End Get
        Set(ByVal value As System.Drawing.Drawing2D.DashStyle)
            Me._lineStyle = value
            'Invalidate()
            Me.Refresh()
        End Set
    End Property

    <Description("Line Thickness"), Category("Appearance")> _
       Public Property LineThickness() As Integer
        Get
            Return Me._lineThickness
        End Get
        Set(ByVal value As Integer)
            'ArgumentValidation.CheckCondition(value, "MinHeight", (value > 0))
            If value > 6 Then
                value = 6
                Me._lineThickness = value
            Else
                Me._lineThickness = value
            End If
        End Set
    End Property

    <Description("Line Color"), Category("Appearance")> _
  Public Property LineColor() As Color
        Get
            Return Me._lineColor
        End Get
        Set(ByVal value As Color)
            Me._lineColor = value
            'Invalidate()
            Me.Refresh()
        End Set
    End Property

    Public Property FieldType() As Integer
        Get
            Return intFieldType
        End Get
        Set(ByVal value As Integer)
            intFieldType = value
        End Set
    End Property
    Private m_Suppress As Boolean = False
    <DefaultValue(False)> _
  Public Property Suppress() As Boolean
        Get
            Return m_Suppress
        End Get
        Set(ByVal value As Boolean)
            m_Suppress = value
            Me.Refresh()
        End Set
    End Property
    Dim Texts As String = "Lines"
    Public ReadOnly Property NamesText() As String
        Get
            Return Texts
        End Get
    End Property
#End Region

#Region "Constructor"
    Public Sub New()
        Me.InitializeComponent()
        'Issue: For Transparent Line Background
        'MyBase.SetStyle((ControlStyles.OptimizedDoubleBuffer Or _
        '                (ControlStyles.AllPaintingInWmPaint Or _
        '                ControlStyles.UserPaint)), True)
        MyBase.SetStyle(ControlStyles.Opaque, True)
        MyBase.SetStyle(ControlStyles.OptimizedDoubleBuffer, False)

        Me._resizedRect = MyBase.Bounds
        Me._topRect = New Rectangle(0, 0, 0, 0)
        Me._bottomRect = New Rectangle(0, 0, 0, 0)
        Me._leftRect = New Rectangle(0, 0, 0, 0)
        Me._rightRect = New Rectangle(0, 0, 0, 0)

        Me._sizerRectangles = New ArrayList
        Me._sizerRectangles.Add(New Sizer((Me._topRect), Cursors.SizeNS, DragMode.ResizeTop))
        Me._sizerRectangles.Add(New Sizer((Me._bottomRect), Cursors.SizeNS, DragMode.ResizeBottom))
        Me._sizerRectangles.Add(New Sizer((Me._leftRect), Cursors.SizeWE, DragMode.ResizeLeft))
        Me._sizerRectangles.Add(New Sizer((Me._rightRect), Cursors.SizeWE, DragMode.ResizeRight))


        Me._textAlign = ContentAlignment.MiddleLeft
        Me._formatSting = New StringFormat
        Me.UpdateSizers()
    End Sub
#End Region

#Region "Methods"
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If (disposing AndAlso (Not Me.components Is Nothing)) Then
            Me.components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'ResizingControl
        '
        Me.DoubleBuffered = True
        Me.Name = "ResizeLineControl"

        Me.Size = New System.Drawing.Size(100, 6)

        Me.ResumeLayout(False)
    End Sub
#End Region
    
#Region "Keyboard key Function"
    Protected Overrides Function ProcessCmdKey(ByRef msg As Message, ByVal keyData As Keys) As Boolean
        If Me._isSelected = True Then
            Dim intWidth As Integer
            If mblnSnapToGrid = True Then
                intWidth = 6
            Else
                intWidth = 1
            End If

            Select Case keyData
                '**********For Change Height and Width.************
                Case Keys.Right + Keys.Shift
                    If Me._isvertical = False Then
                        MyBase.Width += intWidth
                        Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                        Me._screenRect = New Rectangle(0, 0, 0, 0)
                        Me.UpdateSizers()
                        MyBase.Refresh()
                    End If
                Case Keys.Left + Keys.Shift
                    If Me._isvertical = False Then
                        If MyBase.Width <> 12 Then
                            MyBase.Width -= intWidth
                            Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                            Me._screenRect = New Rectangle(0, 0, 0, 0)
                            Me.UpdateSizers()
                            MyBase.Refresh()
                        End If
                    End If
                Case Keys.Down + Keys.Shift
                    If Me._isvertical = True Then
                        MyBase.Height += intWidth
                        Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                        Me._screenRect = New Rectangle(0, 0, 0, 0)
                        Me.UpdateSizers()
                        MyBase.Refresh()
                    End If
                Case Keys.Up + Keys.Shift
                    If Me._isvertical = True Then
                        If MyBase.Height <> 12 Then
                            MyBase.Height -= intWidth
                            Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                            Me._screenRect = New Rectangle(0, 0, 0, 0)
                            Me.UpdateSizers()
                            MyBase.Refresh()
                        End If
                    End If
                    '*********For Change Location.***********
                Case Keys.Right
                    MyBase.Left += intWidth
                    'Me._resizedRect = New Rectangle(MyBase.Left, 0, MyBase.Width, MyBase.Height)
                    Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                    Me._screenRect = New Rectangle(0, 0, 0, 0)
                    Me.UpdateSizers()
                    MyBase.Refresh()
                Case Keys.Left
                    MyBase.Left -= intWidth
                    'Me._resizedRect = New Rectangle(MyBase.Left, 0, MyBase.Width, MyBase.Height)
                    Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                    Me._screenRect = New Rectangle(0, 0, 0, 0)
                    Me.UpdateSizers()
                    MyBase.Refresh()
                Case Keys.Up
                    MyBase.Top -= intWidth
                    'Me._resizedRect = New Rectangle(0, MyBase.Top, MyBase.Width, MyBase.Height)
                    Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                    Me._screenRect = New Rectangle(0, 0, 0, 0)
                    Me.UpdateSizers()
                    MyBase.Refresh()
                Case Keys.Down
                    MyBase.Top += intWidth
                    'Me._resizedRect = New Rectangle(0, MyBase.Top, MyBase.Width, MyBase.Height)
                    Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
                    Me._screenRect = New Rectangle(0, 0, 0, 0)
                    Me.UpdateSizers()
                    MyBase.Refresh()
                    '**********For Delete Field************
                Case Keys.Delete
                    'MyBase.Parent.Controls.Remove(Me)
            End Select
        End If
            Me.Refresh()
    End Function
#End Region
  
#Region "Mouse Events"
    Protected Overrides Sub OnMouseDown(ByVal e As MouseEventArgs)
        MyBase.OnMouseDown(e)
        Me.BringToFront()

        Dim flag As Boolean = False
        'Me._isSelected = True
        Me._isDragging = True

        Dim sizer As Sizer
        For Each sizer In Me._sizerRectangles
            If sizer._rect.Contains(e.X, e.Y) Then
                Me._dragMode = sizer._dragMode
                flag = True
                If _isvertical = True Then
                    Exit For
                End If
            End If
        Next

        If Not flag Then
            Me._dragMode = DragMode.Move
        End If

        Me._lastMouseX = e.X
        Me._lastMouseY = e.Y
        MyBase.Invalidate()
    End Sub

    Protected Overrides Sub OnMouseEnter(ByVal e As EventArgs)
        MyBase.OnMouseEnter(e)
        If Not Me._isSelected Then
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal e As EventArgs)
        MyBase.OnMouseLeave(e)
        Me._isDragging = False
    End Sub

    Protected Overrides Sub OnMouseMove(ByVal e As MouseEventArgs)
        MyBase.OnMouseMove(e)

        If Me._isSelected Then
            Dim flag As Boolean = False
            Dim sizer As Sizer
            For Each sizer In Me._sizerRectangles
                If sizer._rect.Contains(e.X, e.Y) Then
                    flag = True
                    Me.Cursor = sizer._cursor
                    If _isvertical = True Then
                        Exit For
                    End If

                End If
            Next
            If Not flag Then
                Me.Cursor = Cursors.SizeAll
            End If
        End If

        If Me._isDragging Then
            Dim num As Integer = (e.X - Me._lastMouseX)
            Dim num2 As Integer = (e.Y - Me._lastMouseY)

            MyBase.RectangleToClient(MyBase.Parent.RectangleToScreen(MyBase.Parent.ClientRectangle))
            Dim originalRect As Rectangle = MyBase.RectangleToScreen(MyBase.ClientRectangle)

            Select Case Me._dragMode
                Case DragMode.Move
                    Me._resizedRect.X = (Me._resizedRect.X + num)
                    Me._resizedRect.Y = (Me._resizedRect.Y + num2)
                    Exit Select
                Case DragMode.ResizeRight
                    Me._resizedRect.Width = (Me._resizedRect.Width + num)
                    Exit Select
                Case DragMode.ResizeLeft
                    Me._resizedRect.X = (Me._resizedRect.X + num)
                    Me._resizedRect.Width = (Me._resizedRect.Width - num)
                    Exit Select
                Case DragMode.ResizeTop
                    Me._resizedRect.Y = (Me._resizedRect.Y + num2)
                    Me._resizedRect.Height = (Me._resizedRect.Height - num2)
                    Exit Select
                Case DragMode.ResizeBottom
                    Me._resizedRect.Height = (Me._resizedRect.Height + num2)
                    Exit Select
            End Select

            Dim bounds As Rectangle = MyBase.Parent.RectangleToScreen(MyBase.Parent.ClientRectangle)
            If (TypeOf MyBase.Parent Is ScrollableControl AndAlso (Not MyBase.Parent.Parent Is Nothing)) Then
                bounds.Intersect(MyBase.Parent.Parent.RectangleToScreen(MyBase.Parent.Parent.ClientRectangle))
            End If

            'Me.Size = New System.Drawing.Size(Me._resizedRect.Width, Me._resizedRect.Height)
            'MyBase.Refresh()

            Me._lastMouseX = e.X
            Me._lastMouseY = e.Y

        End If
        MyBase.Refresh()
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal e As MouseEventArgs)
        MyBase.OnMouseUp(e)
        Me._isDragging = False
        If (((Me._resizedRect.X <> 0) OrElse (Me._resizedRect.Y <> 0)) OrElse ((Me._resizedRect.Width <> MyBase.Bounds.Width) OrElse (Me._resizedRect.Height <> MyBase.Bounds.Height))) Then
            MyBase.SetBounds((MyBase.Bounds.X + Me._resizedRect.X), (MyBase.Bounds.Y + Me._resizedRect.Y), Me._resizedRect.Width, Me._resizedRect.Height, BoundsSpecified.All)
            MyBase.Invalidate()
            Me.OnMovedResized()
        End If

        If mblnSnapToGrid = True Then
            If ((MyBase.Location.X) Mod 6) <> 0 Or ((MyBase.Location.Y) Mod 3) <> 0 Then
                MyBase.Location = New Point(MyBase.Location.X + (6 - ((MyBase.Location.X) Mod 6)), MyBase.Location.Y + (3 - ((MyBase.Location.Y) Mod 3)))
            End If

        End If

        'Vimal (16 Mar 2011) -- End
    End Sub
#End Region

#Region "Other Events"
    Protected Overrides Sub OnMove(ByVal e As EventArgs)
        Me.UpdateSizingData()
        MyBase.OnMove(e)
    End Sub

    Protected Overridable Sub OnMovedResized()
        'If (Not Me._MovedResized Is Nothing) Then
        RaiseEvent MovedResized(Me, EventArgs.Empty)
        'End If
    End Sub

    Protected Overrides Sub OnSizeChanged(ByVal e As EventArgs)
        Me.UpdateSizingData()
        MyBase.OnSizeChanged(e)
    End Sub

    Protected Overridable Sub OnSelected()
        RaiseEvent Selected(Me, EventArgs.Empty)
    End Sub

    Protected Overrides Sub [Select](ByVal directed As Boolean, ByVal forward As Boolean)
        Me._isSelected = True
        Me._isDragging = False
        'Vimal (16 Mar 2011) -- Start 
        'MyBase.Select(directed, forward)
        'Vimal (16 Mar 2011) -- End
        Me.OnSelected()
    End Sub
#End Region
    
#Region "Other Fuctions"
    Public Sub Unselect()
        Me._isSelected = False
        Me._isDragging = False
        MyBase.Invalidate()
    End Sub

    Private Sub UpdateSizers()
        If (Not Me._sizerRectangles Is Nothing) Then
            If Me._isvertical Then
                Dim sizer As Sizer = DirectCast(Me._sizerRectangles.Item(0), Sizer)  ' Top Rectangle
                sizer._rect.X = 0
                sizer._rect.Y = 0
                sizer._rect.Width = Me.SizerWidth
                sizer._rect.Height = Me.SizerWidth

                sizer = DirectCast(Me._sizerRectangles.Item(1), Sizer)  ' Bottom Rctangle
                sizer._rect.X = 0
                sizer._rect.Y = (MyBase.Bounds.Height - Me.SizerWidth)
                sizer._rect.Width = Me.SizerWidth
                sizer._rect.Height = Me.SizerWidth

            Else
                Dim sizer As Sizer = DirectCast(Me._sizerRectangles.Item(2), Sizer)  ' Left Rctangle
                sizer._rect.X = 0
                Sizer._rect.Y = 0
                Sizer._rect.Width = Me.SizerWidth
                sizer._rect.Height = Me.SizerWidth

                Sizer = DirectCast(Me._sizerRectangles.Item(3), Sizer)  ' Right Rctangle
                Sizer._rect.X = (MyBase.Bounds.Width - Me.SizerWidth)
                Sizer._rect.Y = 0
                Sizer._rect.Width = Me.SizerWidth
                sizer._rect.Height = Me.SizerWidth
            End If
        End If
    End Sub

    Protected Sub UpdateSizingData()
        Me._resizedRect = New Rectangle(0, 0, MyBase.Width, MyBase.Height)
        Me._screenRect = New Rectangle(0, 0, 0, 0)
        Me.UpdateSizers()
    End Sub
#End Region

#Region "Paint Events and Function"
    Private Sub DrawHandles(ByVal g As Graphics)
        Dim sizer As Sizer
        For Each sizer In Me._sizerRectangles
            ControlPaint.DrawGrabHandle(g, sizer._rect, True, True)
        Next
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        MyBase.OnPaint(e)
        Dim g As Graphics = e.Graphics
        'Using p As New Pen(_lineColor)
        Using p As New Pen(_lineColor, _lineThickness)
            p.DashStyle = _lineStyle
            If Me._isSelected Then
                If Me._isvertical Then
                    'g.DrawLine(p, 3, 0, 3, MyBase.Height) 'Issue: Sizer rect. draw on line.
                    g.DrawLine(p, 3, 6, 3, MyBase.Height - 6)
                Else
                    'g.DrawLine(p, 0, 3, MyBase.Width, 3) 'Issue: Sizer rect. draw on line.
                    g.DrawLine(p, 6, 3, MyBase.Width - 6, 3)
                End If

                Me.DrawHandles(e.Graphics)
            Else
                If Me._isvertical Then
                    'g.DrawLine(p, 3, 0, 3, MyBase.Height) 'Issue: Sizer rect. draw on line.
                    g.DrawLine(p, 3, 6, 3, MyBase.Height - 6)
                Else
                    'g.DrawLine(p, 0, 3, MyBase.Width, 3)  'Issue: Sizer rect. draw on line.
                    g.DrawLine(p, 6, 3, MyBase.Width - 6, 3)
                End If
            End If
            If m_Suppress Then
                For i As Integer = 0 To MyBase.Width - 1
                    i += 7
                    e.Graphics.DrawLine(Pens.LightGray, i, 0, i, MyBase.Height - 1)
                Next
                For i As Integer = 0 To MyBase.Height - 1
                    i += 7
                    e.Graphics.DrawLine(Pens.LightGray, 0, i, MyBase.Width - 1, i)
                Next
            End If
        End Using
    End Sub
#End Region

    Private Class Sizer
        Public _cursor As Cursor
        Public _dragMode As DragMode
        Public _rect As Rectangle

        Public Sub New(ByRef rect As Rectangle, ByVal cursor As Cursor, ByVal dragMode As DragMode)
            Me._rect = rect
            Me._cursor = cursor
            Me._dragMode = dragMode
        End Sub
    End Class
End Class

