Imports eZeeCommonLib
Public Class clsCommonControl_Language
    Private Shared ReadOnly mstrModuleName As String = "clsCommonControl_Language"
    Public Shared Sub Refresh()
        eZee.Common.Ui.Language.eZeeSearchResetButton.Caption_Reset = Language.getMessage("eZeeSearchResetButton_Caption", 101, "Reset")
        eZee.Common.Ui.Language.eZeeSearchResetButton.Caption_Search = Language.getMessage("eZeeSearchResetButton_Caption", 102, "Search")

        eZee.Common.Ui.Language.ListView.Caption_More = Language.getMessage("eZeeListView_Caption", 101, "Search")
        eZee.Common.Ui.Language.ListView.Caption_Save = Language.getMessage("eZeeListView_Caption", 102, "Search")
        eZee.Common.Ui.Language.ListView.Caption_ShowAll = Language.getMessage("eZeeListView_Caption", 103, "Search")
        eZee.Common.Ui.Language.ListView.Caption_SizeAllColumnsToFit = Language.getMessage("eZeeListView_Caption", 104, "Search")
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.[4/6/2010]
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("eZeeSearchResetButton_Caption", 101, "Reset")
            Language.setMessage("eZeeSearchResetButton_Caption", 102, "Search")
            Language.setMessage("eZeeListView_Caption", 103, "Search")
            Language.setMessage("eZeeListView_Caption", 104, "Search")
            Language.setMessage("eZeeListView_Caption", 101, "Search")
            Language.setMessage("eZeeListView_Caption", 102, "Search")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
