Public Class IColumn
    Implements ICloneable

    Public Sub New()
    End Sub

    Public Sub New(ByVal Name As String, ByVal DisplayName As String)
        'Sohail (18 Feb 2012) -- Start
        'TRA - ENHANCEMENT
        'Call Me.New(Name, DisplayName, True)
        Call Me.New(Name, DisplayName, True, False)
        'Sohail (18 Feb 2012) -- End
    End Sub

    'Sohail (18 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    'Public Sub New(ByVal Name As String, ByVal DisplayName As String, ByVal ShowInOrderBy As Boolean)
    '    m_ColName = Name
    '    m_ColDisplayName = DisplayName
    '    m_ShowInOrderBy = ShowInOrderBy
    'End Sub
    Public Sub New(ByVal Name As String, ByVal DisplayName As String, ByVal ShowInOrderBy As Boolean, ByVal OrderByDescending As Boolean)
        m_ColName = Name
        m_ColDisplayName = DisplayName
        m_ShowInOrderBy = ShowInOrderBy
        m_OrderByDescending = OrderByDescending
    End Sub

    Public Sub New(ByVal Name As String, ByVal DisplayName As String, ByVal DescendigOrder As Boolean)
        Call Me.New(Name, DisplayName, True, DescendigOrder)
    End Sub
    'Sohail (18 Feb 2012) -- End

    Public Overrides Function ToString() As String
        Return m_ColDisplayName
    End Function

    Public Function Clone() As Object Implements System.ICloneable.Clone
        Return MemberwiseClone()
    End Function

#Region " Property "
    Private m_ColName As String
    Public Property Name() As String
        Get
            Return m_ColName
        End Get
        Set(ByVal value As String)
            m_ColName = value
        End Set
    End Property

    Private m_ColDisplayName As String
    Public Property DisplayName() As String
        Get
            Return m_ColDisplayName
        End Get
        Set(ByVal value As String)
            m_ColDisplayName = value
        End Set
    End Property

    Private m_ShowInOrderBy As Boolean = True
    Public Property ShowInOrderBy() As Boolean
        Get
            Return m_ShowInOrderBy
        End Get
        Set(ByVal value As Boolean)
            m_ShowInOrderBy = value
        End Set
    End Property

    Private m_intWidth As Integer = 100
    Public Property _Width() As Integer
        Get
            Return m_intWidth
        End Get
        Set(ByVal value As Integer)
            m_intWidth = value
        End Set
    End Property

    Private m_intLocation As Integer = 0
    Public Property _Location() As Integer
        Get
            Return m_intLocation
        End Get
        Set(ByVal value As Integer)
            m_intLocation = value
        End Set
    End Property

    Private m_Alignment As CrystalDecisions.Shared.Alignment = CrystalDecisions.Shared.Alignment.LeftAlign
    Public Property _Alignment() As CrystalDecisions.Shared.Alignment
        Get
            Return m_Alignment
        End Get
        Set(ByVal value As CrystalDecisions.Shared.Alignment)
            m_Alignment = value
        End Set
    End Property

    'Sohail (18 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Private m_OrderByDescending As Boolean = False
    Public Property OrderByDescending() As Boolean
        Get
            Return m_OrderByDescending
        End Get
        Set(ByVal value As Boolean)
            m_OrderByDescending = value
        End Set
    End Property
    'Sohail (18 Feb 2012) -- End

#End Region
End Class
