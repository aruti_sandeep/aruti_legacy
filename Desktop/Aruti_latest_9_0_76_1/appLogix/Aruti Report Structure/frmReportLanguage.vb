﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class frmReportLanguage

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmReportLanguage"
    Private mdtList As DataTable
    Private mstrSearchText As String = ""

#End Region

#Region " Private Methods "

    Private Sub FillList(ByVal intChangeLangForId As Integer, ByVal intCategoryId As Integer)
        Dim objReport As New clsArutiReportClass
        Dim dsList As DataSet = Nothing
        Try
            dsList = objReport.getList(intChangeLangForId, intCategoryId)

            dgLanguage.AutoGenerateColumns = False

            dgColLanguage.DataPropertyName = "name"
            dgcollanguage1.DataPropertyName = "name1"
            dgcolLanguage2.DataPropertyName = "name2"
            dgcollangunkid.DataPropertyName = "reportunkid"

            mdtList = dsList.Tables(0)

            dgLanguage.DataSource = mdtList.DefaultView

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objReport = Nothing
        End Try
    End Sub

#End Region

#Region " Form Event(s) "

    Private Sub frmReportLanguage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)

            Call OtherSettings()

            With cboFormFilter
                .Items.Clear()
                .Items.Add(dgColLanguage.HeaderText)
                .Items.Add(dgcollanguage1.HeaderText)
                .Items.Add(dgcolLanguage2.HeaderText)
                .SelectedIndex = 0
            End With

            With cboFillType
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 100, "Report Category(s)"))
                .Items.Add(Language.getMessage(mstrModuleName, 101, "Report(s)"))
                .SelectedIndex = 0
            End With

            Dim objRptCls As New clsArutiReportClass
            Dim ds As New DataSet
            ds = objRptCls.getcategoryList(True, User._Object._Languageunkid)
            With cboCategory
                .ValueMember = "Id"
                .DisplayMember = "Name"
                .DataSource = ds.Tables(0)
                .SelectedValue = 0
            End With
            objRptCls = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReportLanguage_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event "
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Textbox Events "

    Private Sub txtSearch_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.GotFocus
        Try
            With txtSearch
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Clear()
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.Leave
        Try
            If txtSearch.Text.Trim = "" Then
                With txtSearch
                    .ForeColor = Color.Gray
                    .Text = mstrSearchText
                    .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If txtSearch.Text.Trim = mstrSearchText Then
                mdtList.DefaultView.RowFilter = ""
                dgLanguage.Refresh()
            Else
                If mdtList IsNot Nothing AndAlso mdtList.Rows.Count > 0 Then
                    Select Case cboFormFilter.SelectedIndex
                        Case 0
                            mdtList.DefaultView.RowFilter = "name LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' "
                        Case 1
                            mdtList.DefaultView.RowFilter = "name1 LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' "
                        Case 2
                            mdtList.DefaultView.RowFilter = "name2 LIKE '%" & txtSearch.Text.Replace("'", "''") & "%' "
                    End Select
                Else
                    mdtList.DefaultView.RowFilter = ""
                End If
                dgLanguage.Refresh()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearch_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid Events "

    Private Sub dgLanguage_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgLanguage.CellEnter
        Try
            If e.RowIndex <= -1 Then Exit Sub
            Select Case e.ColumnIndex
                Case dgcollanguage1.Index, dgcolLanguage2.Index
                    SendKeys.Send("{F2}")
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLanguage_CellEnter", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgLanguage_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgLanguage.EditingControlShowing
        Try
            Select Case dgLanguage.CurrentCell.ColumnIndex
                Case dgcollanguage1.Index, dgcolLanguage2.Index
                    If e.Control IsNot Nothing Then
                        Dim txt As Windows.Forms.TextBox = CType(e.Control, Windows.Forms.TextBox)
                    End If
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLanguage_EditingControlShowing", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgLanguage_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgLanguage.CellValidating
        Try
            If e.RowIndex <= -1 Then Exit Sub
            If dgLanguage.Focused = False Then Exit Sub

            Select Case e.ColumnIndex
                Case dgcollanguage1.Index, dgcolLanguage2.Index
                    If e.FormattedValue.ToString.Trim = "" Then
                        eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Report name is mandatory. Please enter report name."), enMsgBoxStyle.Information)
                        e.Cancel = True
                        Exit Sub
                    End If

            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLanguage_CellValidating", mstrModuleName)
        Finally

        End Try
    End Sub

    Private Sub dgLanguage_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgLanguage.CellValueChanged
        Try
            If e.RowIndex <= -1 Then Exit Sub

            RemoveHandler dgLanguage.CellValueChanged, AddressOf dgLanguage_CellValueChanged

            Select Case e.ColumnIndex
                Case dgcollanguage1.Index, dgcolLanguage2.Index
                    Select Case cboFillType.SelectedIndex
                        Case 0
                            If dgLanguage.Rows(e.RowIndex).Cells(dgcollanguage1.Index).Value.ToString.Trim <> "" AndAlso dgLanguage.Rows(e.RowIndex).Cells(dgcolLanguage2.Index).Value.ToString.Trim <> "" Then
                                Dim objReport As New clsArutiReportClass
                                If objReport.UpdateCategoryValue(CInt(dgLanguage.Rows(e.RowIndex).Cells(dgcollangunkid.Index).Value), _
                                                                 dgLanguage.Rows(e.RowIndex).Cells(dgcollanguage1.Index).Value.ToString, _
                                                                 dgLanguage.Rows(e.RowIndex).Cells(dgcolLanguage2.Index).Value.ToString, _
                                                                 User._Object._Userunkid, Company._Object._Companyunkid) = False Then
                                    Throw New Exception()
                                End If
                            End If
                        Case 1
                            If dgLanguage.Rows(e.RowIndex).Cells(dgcollanguage1.Index).Value.ToString.Trim <> "" AndAlso dgLanguage.Rows(e.RowIndex).Cells(dgcolLanguage2.Index).Value.ToString.Trim <> "" Then
                                Dim objReport As New clsArutiReportClass
                                objReport._Reportunkid = CInt(dgLanguage.Rows(e.RowIndex).Cells(dgcollangunkid.Index).Value)
                                objReport._Name1 = dgLanguage.Rows(e.RowIndex).Cells(dgcollanguage1.Index).Value.ToString
                                objReport._Name2 = dgLanguage.Rows(e.RowIndex).Cells(dgcolLanguage2.Index).Value.ToString
                                If objReport.Update(User._Object._Userunkid, Company._Object._Companyunkid) = False Then
                                    Throw New Exception()
                                End If
                            End If
                    End Select
            End Select

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgLanguage_CellValueChanged", mstrModuleName)
        Finally
            RemoveHandler dgLanguage.CellValueChanged, AddressOf dgLanguage_CellValueChanged
            AddHandler dgLanguage.CellValueChanged, AddressOf dgLanguage_CellValueChanged
        End Try
    End Sub

    Private Sub dgLanguage_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgLanguage.DataError

    End Sub
#End Region

#Region " Combobox Event(s) "

    Private Sub cboFillType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFillType.SelectedIndexChanged
        Try
            If cboFillType.SelectedIndex <= 0 Then
                cboCategory.SelectedValue = 0
                cboCategory.Enabled = False
            Else
                cboCategory.Enabled = True
                cboCategory_SelectedIndexChanged(cboCategory, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFillType_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub cboCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCategory.SelectedIndexChanged
        Try
            Call FillList(cboFillType.SelectedIndex, CInt(cboCategory.SelectedValue))
            dgLanguage.Focus()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCategory_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.dgColLanguage.HeaderText = Language._Object.getCaption(Me.dgColLanguage.Name, Me.dgColLanguage.HeaderText)
            Me.dgcollanguage1.HeaderText = Language._Object.getCaption(Me.dgcollanguage1.Name, Me.dgcollanguage1.HeaderText)
            Me.dgcolLanguage2.HeaderText = Language._Object.getCaption(Me.dgcolLanguage2.Name, Me.dgcolLanguage2.HeaderText)
            Me.dgcollangunkid.HeaderText = Language._Object.getCaption(Me.dgcollangunkid.Name, Me.dgcollangunkid.HeaderText)
            Me.lblSearchBy.Text = Language._Object.getCaption(Me.lblSearchBy.Name, Me.lblSearchBy.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Report name is mandatory. Please enter report name.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>

    
End Class