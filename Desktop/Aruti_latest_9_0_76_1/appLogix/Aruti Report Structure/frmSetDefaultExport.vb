
Imports eZeeCommonLib
Imports Aruti.Data
Public Class frmSetDefaultExport

    Private ReadOnly mstrModuleName As String = "frmSetDefaultExport"
    Private mblnCancel As Boolean = True
    Private mintDefaultExport As Integer = -1
    'Private mintNewDefaultExport As Integer = -1

#Region " From Properties "
    'Public ReadOnly Property _DefaultExportOption() As Integer
    '    Get
    '        Return mintDefaultExport
    '    End Get

    'End Property

    'Public ReadOnly Property _NewDefaultOption() As Integer
    '    Get
    '        Return mintNewDefaultExport
    '    End Get
    'End Property

#End Region

#Region " Display Dialog "

    Public Function displayDialog() As Boolean
        Try
            Me.ShowDialog()
            Return Not mblnCancel

        Catch ex As System.Exception
            Call DisplayError.Show(-1, ex.Message, "displayDialog", mstrModuleName)
        Finally

        End Try
    End Function

#End Region

#Region " Private Method "

    Private Sub DefaultOpt()
        Dim objAppSetting As New clsApplicationSettings
        Try
            Select Case objAppSetting._SetDefaultExportAction
                Case enExportAction.Excel
                    rabExcel.Checked = True
                Case enExportAction.HTML, enExportAction.None
                    rabHTML.Checked = True
                Case enExportAction.PDF
                    rabPDF.Checked = True
                Case enExportAction.RichText
                    rabRichText.Checked = True
                Case enExportAction.Word
                    rabWord.Checked = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objAppSetting", mstrModuleName)
        Finally
            objAppSetting = Nothing
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmSetDefaultExport_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        If e.Control And e.KeyCode = Windows.Forms.Keys.S Then
            Call btnOK.PerformClick()
        End If
    End Sub

    Private Sub frmSetDefaultExport_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        If Asc(e.KeyChar) = 27 Then
            btnCancel.PerformClick()
        ElseIf Asc(e.KeyChar) = 13 Then
            Call btnOK.PerformClick()
        End If
    End Sub

    Private Sub frmSetDefaultExport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            Call DefaultOpt()

        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "frmOrderBy_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Buttons "

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.click
        Dim objAppSetting As New clsApplicationSettings
        Try
            If rabRichText.Checked = True Then
                objAppSetting._SetDefaultExportAction = enExportAction.RichText
            ElseIf rabWord.Checked = True Then
                objAppSetting._SetDefaultExportAction = enExportAction.Word
            ElseIf rabExcel.Checked = True Then
                objAppSetting._SetDefaultExportAction = enExportAction.Excel
            ElseIf rabPDF.Checked = True Then
                objAppSetting._SetDefaultExportAction = enExportAction.PDF
            ElseIf rabHTML.Checked = True Then
                objAppSetting._SetDefaultExportAction = enExportAction.HTML
            End If
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        Finally
            objAppSetting = Nothing
        End Try
    End Sub

    'Private Sub objLanguage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
    '    Try
    '        Dim frm As New frmLanguage
    '        If User._Object._RightToLeft = True Then
    '            frm.RightToLeft = Windows.Forms.RightToLeft.Yes
    '            frm.RightToLeftLayout = True
    '            Call Language.ctlRightToLeftlayOut(frm)
    '        End If
    '        Call SetMessages()

    '        frm.displayDialog(Me)
    '        frm = Nothing

    '        Call Language.setLanguage(Me.Name)
    '        Call SetLanguage()

    '    Catch ex As Exception
    '        Call DisplayError.Show(-1, ex.Message, "objLanguage_Click", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            mblnCancel = True
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show(-1, ex.Message, "btnCancel_Click", mstrModuleName)
        End Try
    End Sub

#End Region

End Class