﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHTMLReportView
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHTMLReportView))
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnPrint = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.brReportView = New System.Windows.Forms.WebBrowser
        Me.pnlControl = New System.Windows.Forms.Panel
        Me.EZeeFooter1.SuspendLayout()
        Me.pnlControl.SuspendLayout()
        Me.SuspendLayout()
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnPrint)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 540)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(1144, 50)
        Me.EZeeFooter1.TabIndex = 34
        '
        'btnPrint
        '
        Me.btnPrint.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint.BackColor = System.Drawing.Color.White
        Me.btnPrint.BackgroundImage = CType(resources.GetObject("btnPrint.BackgroundImage"), System.Drawing.Image)
        Me.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnPrint.BorderColor = System.Drawing.Color.Empty
        Me.btnPrint.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnPrint.FlatAppearance.BorderSize = 0
        Me.btnPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPrint.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.ForeColor = System.Drawing.Color.Black
        Me.btnPrint.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnPrint.GradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrint.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.Location = New System.Drawing.Point(947, 11)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnPrint.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnPrint.Size = New System.Drawing.Size(90, 30)
        Me.btnPrint.TabIndex = 33
        Me.btnPrint.Text = "&Print"
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(1043, 11)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(90, 30)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'brReportView
        '
        Me.brReportView.AllowWebBrowserDrop = False
        Me.brReportView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.brReportView.IsWebBrowserContextMenuEnabled = False
        Me.brReportView.Location = New System.Drawing.Point(0, 0)
        Me.brReportView.Margin = New System.Windows.Forms.Padding(0)
        Me.brReportView.MinimumSize = New System.Drawing.Size(20, 20)
        Me.brReportView.Name = "brReportView"
        Me.brReportView.Size = New System.Drawing.Size(1144, 540)
        Me.brReportView.TabIndex = 35
        Me.brReportView.WebBrowserShortcutsEnabled = False
        '
        'pnlControl
        '
        Me.pnlControl.Controls.Add(Me.brReportView)
        Me.pnlControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlControl.Location = New System.Drawing.Point(0, 0)
        Me.pnlControl.Name = "pnlControl"
        Me.pnlControl.Size = New System.Drawing.Size(1144, 540)
        Me.pnlControl.TabIndex = 36
        '
        'frmHTMLReportView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1144, 590)
        Me.Controls.Add(Me.pnlControl)
        Me.Controls.Add(Me.EZeeFooter1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHTMLReportView"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Report View"
        Me.EZeeFooter1.ResumeLayout(False)
        Me.pnlControl.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnPrint As eZee.Common.eZeeLightButton
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents brReportView As System.Windows.Forms.WebBrowser
    Friend WithEvents pnlControl As System.Windows.Forms.Panel
End Class
