﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Badge
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlBadge = New Aruti.Data.RoundedPanel
        Me.lblCaption = New System.Windows.Forms.Label
        Me.lblNumber = New System.Windows.Forms.Label
        Me.pnlBadge.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlBadge
        '
        Me.pnlBadge.BackColor = System.Drawing.Color.DimGray
        Me.pnlBadge.BorderColor = System.Drawing.Color.White
        Me.pnlBadge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlBadge.Controls.Add(Me.lblCaption)
        Me.pnlBadge.Controls.Add(Me.lblNumber)
        Me.pnlBadge.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBadge.Edge = 5
        Me.pnlBadge.Location = New System.Drawing.Point(0, 0)
        Me.pnlBadge.Margin = New System.Windows.Forms.Padding(0)
        Me.pnlBadge.Name = "pnlBadge"
        Me.pnlBadge.Size = New System.Drawing.Size(103, 50)
        Me.pnlBadge.TabIndex = 0
        '
        'lblCaption
        '
        Me.lblCaption.BackColor = System.Drawing.Color.Aquamarine
        Me.lblCaption.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCaption.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCaption.Location = New System.Drawing.Point(0, 31)
        Me.lblCaption.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.lblCaption.Name = "lblCaption"
        Me.lblCaption.Size = New System.Drawing.Size(101, 17)
        Me.lblCaption.TabIndex = 2
        Me.lblCaption.Text = "abc"
        Me.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblNumber
        '
        Me.lblNumber.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblNumber.BackColor = System.Drawing.Color.DimGray
        Me.lblNumber.Font = New System.Drawing.Font("Tahoma", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumber.ForeColor = System.Drawing.Color.White
        Me.lblNumber.Location = New System.Drawing.Point(0, 0)
        Me.lblNumber.Name = "lblNumber"
        Me.lblNumber.Size = New System.Drawing.Size(100, 30)
        Me.lblNumber.TabIndex = 1
        Me.lblNumber.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Badge
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.pnlBadge)
        Me.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "Badge"
        Me.Size = New System.Drawing.Size(103, 50)
        Me.pnlBadge.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlBadge As RoundedPanel
    Friend WithEvents lblNumber As System.Windows.Forms.Label
    Friend WithEvents lblCaption As System.Windows.Forms.Label

End Class
