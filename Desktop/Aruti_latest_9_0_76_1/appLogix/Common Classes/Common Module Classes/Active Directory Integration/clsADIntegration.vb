﻿Imports eZeeCommonLib
Imports System.DirectoryServices
Imports System.DirectoryServices.Protocols
Imports System.Net
Imports System.Security.Cryptography.X509Certificates

Public Class clsADIntegration

    Private Shared mstrModuleName As String = "clsADIntegration"


    Public Shared Sub GetADConnection(ByRef mstrADIPAddress As String, ByRef mstrADDomain As String, ByRef mstrADUserName As String, ByRef mstrADUserPwd As String, ByRef mstrADPortNo As String, Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim objConfig As New clsConfigOptions
        Try
            objConfig.IsValue_Changed("ADIPAddress", "-999", mstrADIPAddress, objDoOperation)
            objConfig.IsValue_Changed("ADDomain", "-999", mstrADDomain, objDoOperation)
            objConfig.IsValue_Changed("ADDomainUser", "-999", mstrADUserName, objDoOperation)
            objConfig.IsValue_Changed("ADDomainUserPwd", "-999", mstrADUserPwd, objDoOperation)
            objConfig.IsValue_Changed("ADPortNo", "-999", mstrADPortNo)

            If mstrADUserPwd.Trim.Length > 0 Then
                mstrADUserPwd = clsSecurity.Decrypt(mstrADUserPwd, "ezee")
            End If

            Dim ar() As String = Nothing
            If mstrADDomain.Trim.Length > 0 AndAlso mstrADDomain.Trim.Contains(".") Then
                ar = mstrADDomain.Trim.Split(CChar("."))
                mstrADDomain = ""
                If ar.Length > 0 Then
                    For i As Integer = 0 To ar.Length - 1
                        mstrADDomain &= ",DC=" & ar(i)
                    Next
                End If
            End If

            If mstrADDomain.Trim.Length > 0 Then
                mstrADDomain = mstrADDomain.Trim.Substring(1)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetADConnection ; Module Name : " & mstrModuleName)
        Finally
            objConfig = Nothing
        End Try
    End Sub

    Public Shared Function IsADUserExist(ByVal mstrEmpDisplayName As String, ByVal mstrADIPAddress As String, ByVal mstrADDomain As String, ByVal mstrADUserName As String, ByVal mstrADUserPwd As String, ByVal mstrADPortNo As String, ByVal mstrAttributes As String(), ByVal objLDap As LdapConnection, ByRef objresult As SearchResultEntry) As Boolean
        Dim exForce As Exception = Nothing
        Dim mblnFlag As Boolean = True
        Try
            '/* START FOR CHECK WHETHER EMPLOYEE IS EXIST IN A.D OR NOT
            Dim objSSLSearch As New SearchRequest(mstrADDomain, "(SAMAccountName=" & mstrEmpDisplayName & ")", Protocols.SearchScope.Subtree, mstrAttributes)
            Dim objResponse As SearchResponse = DirectCast(objLDap.SendRequest(objSSLSearch), SearchResponse)

            If objResponse.ResultCode = ResultCode.Success Then
                If objResponse.Entries.Count > 0 Then
                    objresult = objResponse.Entries(0)
                    mblnFlag = True
                Else
                    mblnFlag = False
                End If
            Else
                mblnFlag = False
                'exForce = New Exception(objResponse.ErrorMessage)
                'Throw exForce
            End If
            '/* END FOR CHECK WHETHER EMPLOYEE IS EXIST IN A.D OR NOT
        Catch ex As Exception
            mblnFlag = False
            If ex.Message.ToString().Contains("The server is not operational.") Then
                Throw New Exception("System cannot connect with Active Directory at the moment. Please check Active Directory connection. Also username or password (Aruti to AD Server) may be incorrect.")
            Else
                Throw New Exception(ex.Message & "; Procedure Name: IsADUserExist; Module Name: " & mstrModuleName)
            End If
        Finally
        End Try
        Return mblnFlag
    End Function

    Public Shared Sub SetADProperty(ByVal objLDap As LdapConnection, ByVal result As SearchResultEntry, ByVal pAttribute As String, ByVal pAttributeValue As String, ByRef mstrProperty As String)
        Dim exForce As Exception = Nothing
        Try
            If pAttribute IsNot Nothing Then
                mstrProperty = pAttribute
                If result.Attributes.Contains(pAttribute) Then 'The SearchResultEntry contains this attribute
                    Dim objModifyrequest As ModifyRequest = New ModifyRequest()
                    objModifyrequest.DistinguishedName = result.DistinguishedName
                    Dim objModifyDir As DirectoryAttributeModification = New DirectoryAttributeModification()
                    objModifyDir.Operation = DirectoryAttributeOperation.Replace
                    objModifyDir.Name = pAttribute
                    objModifyDir.Add(pAttributeValue)
                    objModifyrequest.Controls.Add(New PermissiveModifyControl())
                    objModifyrequest.Modifications.Add(objModifyDir)

                    Dim objModifyResponse As ModifyResponse = CType(objLDap.SendRequest(objModifyrequest), ModifyResponse)
                    If objModifyResponse.ResultCode <> ResultCode.Success Then
                        exForce = New Exception(objModifyResponse.ErrorMessage)
                        Throw exForce
                    End If   ' If objModifyResponse.ResultCode <> ResultCode.Success Then

                    objModifyResponse = Nothing
                    objModifyDir = Nothing
                    objModifyrequest = Nothing

                Else    'attribute doesnt exist
                    Dim objAddRequest As AddRequest = New AddRequest()    'Add the attribute and set it's value
                    objAddRequest.DistinguishedName = result.DistinguishedName
                    Dim objAddDir As DirectoryAttribute = New DirectoryAttribute()
                    objAddDir.Name = pAttribute
                    objAddDir.Add(pAttributeValue)
                    objAddRequest.Attributes.Add(objAddDir)

                    Dim objAddResponse As AddResponse = CType(objLDap.SendRequest(objAddRequest), AddResponse)
                    If objAddResponse.ResultCode <> ResultCode.Success Then
                        exForce = New Exception(objAddResponse.ErrorMessage)
                        Throw exForce
                    End If   ' If objAddResponse.ResultCode <> ResultCode.Success Then

                    objAddResponse = Nothing
                    objAddDir = Nothing
                    objAddRequest = Nothing

                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & " " & "[" & pAttribute & "]" & "; Procedure Name: SetADProperty; Module Name: " & mstrModuleName)
        End Try
    End Sub

    Public Shared Sub EnableDisableActiveDirectoryUser(ByVal blnEnable As Boolean, ByVal mstrUserName As String, ByVal mstrADIPAddress As String, ByVal mstrADDomain As String, ByVal mstrADDomainUser As String, ByVal mstrADDomainUserPwd As String, ByVal mstrADPortNo As String)
        Dim exForce As Exception = Nothing
        Try
            Dim mstrAttributes() As String = New String() {"userAccountControl", "SamAccountName", "cn"}

            Dim objLDAP As LdapConnection = clsADIntegration.SetADConnection(mstrADIPAddress.Trim, mstrADDomain.Trim, mstrADPortNo.Trim, mstrADDomainUser.Trim, mstrADDomainUserPwd.Trim)
            Dim objSSLSearch As New SearchRequest(mstrADDomain, "(SAMAccountName=" & mstrUserName & ")", Protocols.SearchScope.Subtree, mstrAttributes)
            Dim objResponse As SearchResponse = DirectCast(objLDAP.SendRequest(objSSLSearch), SearchResponse)

            If objResponse.ResultCode = ResultCode.Success Then

                If objResponse.Entries.Count > 0 Then

                    Dim srcResult As SearchResultEntry = objResponse.Entries(0)

                    If srcResult.Attributes.Count > 0 Then

                        If srcResult.Attributes.Contains("userAccountControl") Then
                            Dim iValue As Integer = Convert.ToInt32(srcResult.Attributes("userAccountControl")(0))
                            Dim objModifyrequest As ModifyRequest = New ModifyRequest()
                            objModifyrequest.DistinguishedName = srcResult.DistinguishedName
                            Dim objModifyDir As DirectoryAttributeModification = New DirectoryAttributeModification()
                            objModifyDir.Operation = DirectoryAttributeOperation.Replace
                            objModifyDir.Name = "userAccountControl"
                            Dim mstrValue As String = ""
                            If blnEnable Then
                                mstrValue = CStr(iValue And Not 514)
                            Else
                                mstrValue = CStr(iValue Or 514)
                            End If
                            objModifyDir.Add(mstrValue)
                            objModifyrequest.Modifications.Add(objModifyDir)

                            Dim objModifyResponse As ModifyResponse = CType(objLDAP.SendRequest(objModifyrequest), ModifyResponse)
                            If objModifyResponse.ResultCode <> ResultCode.Success Then
                                exForce = New Exception(objModifyResponse.ErrorMessage)
                                Throw exForce
                            End If  ' If objModifyResponse.ResultCode <> ResultCode.Success Then

                        End If  '   If srcResult.Attributes.Contains("userAccountControl") Then

                    End If   '  If srcResult.Attributes.Count > 0 Then

                End If    'If objResponse.Entries.Count > 0 Then
            Else
                exForce = New Exception(objResponse.ErrorMessage)
                Throw exForce
            End If  ' If objResponse.ResultCode = ResultCode.Success Then

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: EnableDisableActiveDirectoryUser; Module Name: " & mstrModuleName)
        End Try
    End Sub

    'Public Shared Function SetADConnection(ByVal mstrADIPAddress As String, ByVal mstrADDomain As String, ByVal mstrADPortNo As String, ByVal mstrADUserName As String, ByVal mstrADUserPwd As String, ByVal mstrADFilter As String, ByVal mstrAttributes As String()) As SearchResultEntryCollection
    '    Dim result As SearchResultEntryCollection = Nothing
    '    Dim objLDAP As LdapConnection = Nothing
    '    Try
    '        If mstrADPortNo.Trim.Length > 0 Then
    '            objLDAP = New LdapConnection(New LdapDirectoryIdentifier(mstrADIPAddress.Trim, CInt(mstrADPortNo.Trim)))
    '            objLDAP.SessionOptions.SecureSocketLayer = True
    '            objLDAP.SessionOptions.ProtocolVersion = 3
    '        Else
    '            objLDAP = New LdapConnection(New LdapDirectoryIdentifier(mstrADIPAddress.Trim, 389))
    '        End If
    '        objLDAP.AuthType = AuthType.Basic
    '        objLDAP.Credential = New NetworkCredential(mstrADUserName.Trim, mstrADUserPwd.Trim)
    '        objLDAP.SessionOptions.VerifyServerCertificate = AddressOf ServerCallBack
    '        objLDAP.SessionOptions.ReferralChasing = ReferralChasingOptions.All
    '        objLDAP.Bind()

    '        Dim objSSLSearch As New SearchRequest(mstrADDomain, mstrADFilter, Protocols.SearchScope.Subtree, mstrAttributes)
    '        Dim objpageRequestControl As PageResultRequestControl = New PageResultRequestControl(1000)
    '        objSSLSearch.Controls.Add(objpageRequestControl)
    '        Dim objsortRequest As SortRequestControl = New SortRequestControl("SamAccountName", True)
    '        objSSLSearch.Controls.Add(objsortRequest)

    '        Dim objResponse As SearchResponse = DirectCast(objLDAP.SendRequest(objSSLSearch), SearchResponse)

    '        If objResponse.Entries.Count > 0 Then
    '            result = objResponse.Entries
    '        End If
    '        Return result
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: SetADConnection; Module Name: " & mstrModuleName)
    '    End Try
    'End Function

    Public Shared Function SetADConnection(ByVal mstrADIPAddress As String, ByVal mstrADDomain As String, ByVal mstrADPortNo As String, ByVal mstrADUserName As String, ByVal mstrADUserPwd As String) As LdapConnection
        Dim objLDAP As LdapConnection = Nothing
        Try
            If mstrADPortNo.Trim.Length > 0 Then
                objLDAP = New LdapConnection(New LdapDirectoryIdentifier(mstrADIPAddress.Trim, CInt(mstrADPortNo.Trim)))
                objLDAP.SessionOptions.SecureSocketLayer = True
                objLDAP.SessionOptions.ProtocolVersion = 3
            Else
                objLDAP = New LdapConnection(New LdapDirectoryIdentifier(mstrADIPAddress.Trim, 389))
                objLDAP.SessionOptions.SecureSocketLayer = False
            End If
            objLDAP.AuthType = AuthType.Negotiate
            objLDAP.Credential = New NetworkCredential(mstrADUserName.Trim, mstrADUserPwd.Trim)
            objLDAP.SessionOptions.VerifyServerCertificate = AddressOf ServerCallBack
            objLDAP.SessionOptions.ReferralChasing = ReferralChasingOptions.All
            objLDAP.Bind()
            Return objLDAP
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetADConnection; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Shared Function ServerCallBack(ByVal objConn As LdapConnection, ByVal objCerti As X509Certificate) As Boolean
        Return True
    End Function

    Public Shared Function ADDirectoryPathExists(ByVal objLDap As LdapConnection, ByVal mstrIPAddress As String, ByVal mstrADDomain As String, ByVal mstrPath As String, ByVal mstrAttributes As String()) As Boolean
        Dim mblnFlag As Boolean = True
        Dim exForce As Exception = Nothing
        Try
            If mstrPath.Trim.Length <= 0 Then Return True

            If mstrPath.Trim.Contains("LDAP://" & mstrIPAddress & "/") Then
                mstrPath = mstrPath.Replace("LDAP://" & mstrIPAddress & "/", "")
            End If

            Dim objSSLSearch As New SearchRequest(mstrADDomain, "(distinguishedname=" & mstrPath & ")", Protocols.SearchScope.Subtree, mstrAttributes)
            Dim objResponse As SearchResponse = DirectCast(objLDap.SendRequest(objSSLSearch), SearchResponse)
            Dim objresult As SearchResultEntry = Nothing
            If objResponse.ResultCode = ResultCode.Success Then
                If objResponse.Entries.Count > 0 Then
                    objresult = objResponse.Entries(0)
                    mblnFlag = True
                Else
                    mblnFlag = False
                End If
            Else
                mblnFlag = False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ADDirectoryPathExists; Module Name: " & mstrModuleName)
            mblnFlag = False
        End Try
        Return mblnFlag
    End Function

    Public Shared Function IsAuthenticated(ByVal mstrIPAddress As String, ByVal mstrDomain As String, ByVal mstrPortNo As String, ByVal mstrUsername As String, ByVal mstrpwd As String) As Boolean
        Dim mblnFlag As Boolean = True
        Try
            If mstrDomain.Trim.Length <= 0 Then Return False
            Dim ar() As String = Nothing
            If mstrDomain.Trim.Length > 0 AndAlso mstrDomain.Trim.Contains(".") Then
                ar = mstrDomain.Trim.Split(CChar("."))
                mstrDomain = ""
                If ar.Length > 0 Then
                    For i As Integer = 0 To ar.Length - 1
                        mstrDomain &= ",DC=" & ar(i)
                    Next
                End If
            End If

            If mstrDomain.Trim.Length > 0 Then
                mstrDomain = mstrDomain.Trim.Substring(1)
            End If

            Dim objLdap As LdapConnection = SetADConnection(mstrIPAddress, mstrDomain, mstrPortNo, mstrUsername, mstrpwd)

            '/* START FOR CHECK WHETHER EMPLOYEE IS EXIST IN A.D OR NOT
            Dim objresult As SearchResultEntry = Nothing
            Dim mstrAttributes() As String = New String() {"SAMAccountName"}
            Dim objSSLSearch As New SearchRequest(mstrDomain, "(SAMAccountName=" & mstrUsername & ")", Protocols.SearchScope.Subtree, mstrAttributes)
            Dim objResponse As SearchResponse = DirectCast(objLdap.SendRequest(objSSLSearch), SearchResponse)

            If objResponse.ResultCode = ResultCode.Success Then
                If objResponse.Entries.Count > 0 Then
                    objresult = objResponse.Entries(0)
                    mblnFlag = True
                Else
                    mblnFlag = False
                End If
            Else
                mblnFlag = False
            End If
            '/* END FOR CHECK WHETHER EMPLOYEE IS EXIST IN A.D OR NOT

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : IsAuthenticated ; Module Name : " & mstrModuleName)
            mblnFlag = False
        End Try
        Return mblnFlag
    End Function

End Class
