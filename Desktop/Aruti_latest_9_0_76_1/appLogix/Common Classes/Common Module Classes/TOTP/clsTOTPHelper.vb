﻿Imports System
Imports System.Security.Cryptography
Imports System.Text
Imports System.Net.NetworkInformation

Public Class clsTOTPHelper

    Private Shared ReadOnly mstrModuleName As String = "TOTPHelper"
    Dim CodeDigits As Integer = 6
    Private mintTimeStepSeconds As Integer = 30
    Private TimeSkewTolerance As Integer = 1

    Public Sub New(ByVal xCompanyId As Integer)
        Dim objConfig As New clsConfigOptions
        mintTimeStepSeconds = objConfig.GetKeyValue(xCompanyId, "TOTPTimeLimitInSeconds", Nothing)
        objConfig = Nothing
    End Sub

    Public ReadOnly Property _TimeStepSeconds() As Integer
        Get
            Return mintTimeStepSeconds
        End Get
    End Property

    Public Function GenerateSecretKey() As String
        Try
            Const secretKeyLength As Integer = 16
            Const allowedChars As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
            Dim random = New Random()
            Dim secretKey = New Char(secretKeyLength - 1) {}
            For i As Integer = 0 To secretKeyLength - 1
                secretKey(i) = allowedChars.Chars(random.Next(allowedChars.Length))
            Next i
            Return New String(secretKey)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateSecretKey; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function GenerateTOTPCode(ByVal secretKey As String, ByVal timestamp As Long) As String
        Try
            Dim keyBytes As Byte() = Encoding.ASCII.GetBytes(secretKey)
            Dim timestampBytes As Byte() = BitConverter.GetBytes(timestamp)
            If BitConverter.IsLittleEndian Then
                Array.Reverse(timestampBytes)
            End If

            Using hmac As New HMACSHA1(keyBytes, True)
                Dim hashBytes As Byte() = hmac.ComputeHash(timestampBytes)
                Dim offset As Integer = hashBytes(hashBytes.Length - 1) And &HF
                Dim truncatedHash As Integer = ExtractTruncatedHash(hashBytes, offset)
                Dim totpCode As String = truncatedHash.ToString().PadLeft(CodeDigits, "0"c)
                Return totpCode
            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateTOTPCode; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function GenerateTOTPCode(ByVal secretKey As String) As String
        Try
            Dim finaltimestamp As Long = GetCurrentTimestamp()
            Return GenerateTOTPCode(secretKey, finaltimestamp)
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateTOTPCode; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function GetCurrentTimestamp() As Long
        Try
            Dim unixEpoch As New DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
            Dim timestamp As Long = (DateTime.UtcNow - unixEpoch).TotalSeconds
            Return timestamp / mintTimeStepSeconds
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCurrentTimestamp; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function VerifyTOTPCode(ByVal secretKey As String, ByVal enteredCode As String) As Boolean
        Try
            Dim generatedCode As String = GenerateTOTPCode(secretKey)
            If generatedCode.Trim() = enteredCode.Trim() Then Return True Else Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VerifyTOTPCode; Module Name: " & mstrModuleName)
        End Try
    End Function

    Private Function ExtractTruncatedHash(ByVal hashBytes As Byte(), ByVal offset As Integer) As Integer
        Try
            Dim binaryCode As Integer = (hashBytes(offset) And &H7F) << 24 Or _
                                   (hashBytes(offset + 1) And &HFF) << 16 Or _
                                   (hashBytes(offset + 2) And &HFF) << 8 Or _
                                   (hashBytes(offset + 3) And &HFF)

            Return binaryCode Mod CInt(Math.Pow(10, CodeDigits))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ExtractTruncatedHash; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function ValidateTotpCode(ByVal secretKey As String, ByVal totpCode As String) As Boolean
        Try
            If totpCode = GenerateTOTPCode(secretKey) Then Return True

            For i As Integer = 1 To TimeSkewTolerance
                If totpCode = GenerateTOTPCode(secretKey, GetCurrentTimestamp() + i) Then
                    Return True
                ElseIf totpCode = GenerateTOTPCode(secretKey, GetCurrentTimestamp() - i) Then
                    Return True
                End If
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ValidateTotpCode; Module Name: " & mstrModuleName)
        End Try
        Return False ' OTP code is invalid
    End Function

End Class
