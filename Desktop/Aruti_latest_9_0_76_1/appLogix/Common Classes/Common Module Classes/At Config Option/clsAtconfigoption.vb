﻿'************************************************************************************************************************************
'Class Name : clsAtconfigoption.vb
'Purpose    :
'Date       :18/04/2013
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsAtconfigoption
    Private Shared ReadOnly mstrModuleName As String = "clsAtconfigoption"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAtoptionunkid As Integer
    Private mintOptiongroupid As Integer
    Private mintConfigunkid As Integer
    Private mintCompanyunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mintAudituserunkid As Integer
    Private mdtAuditdatetime As Date
    Private mstrIp As String = String.Empty
    Private mstrMachine_Name As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set atoptionunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Atoptionunkid() As Integer
        Get
            Return mintAtoptionunkid
        End Get
        Set(ByVal value As Integer)
            mintAtoptionunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set optiongroupid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Optiongroupid() As Integer
        Get
            Return mintOptiongroupid
        End Get
        Set(ByVal value As Integer)
            mintOptiongroupid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set configunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Configunkid() As Integer
        Get
            Return mintConfigunkid
        End Get
        Set(ByVal value As Integer)
            mintConfigunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set machine_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Machine_Name() As String
        Get
            Return mstrMachine_Name
        End Get
        Set(ByVal value As String)
            mstrMachine_Name = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  atoptionunkid " & _
              ", optiongroupid " & _
              ", configunkid " & _
              ", companyunkid " & _
              ", description " & _
              ", audituserunkid " & _
              ", auditdatetime " & _
              ", ip " & _
              ", machine_name " & _
             "FROM atcfconfiguration " & _
             "WHERE atoptionunkid = @atoptionunkid "

            objDataOperation.AddParameter("@atoptionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAtoptionunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAtoptionunkid = CInt(dtRow.Item("atoptionunkid"))
                mintOptiongroupid = CInt(dtRow.Item("optiongroupid"))
                mintConfigunkid = CInt(dtRow.Item("configunkid"))
                mintCompanyunkid = CInt(dtRow.Item("companyunkid"))
                mstrDescription = dtRow.Item("description").ToString
                mintAudituserunkid = CInt(dtRow.Item("audituserunkid"))
                mdtAuditdatetime = dtRow.Item("auditdatetime")
                mstrIp = dtRow.Item("ip").ToString
                mstrMachine_Name = dtRow.Item("machine_name").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT atoptionunkid " & _
                      ", optiongroupid	" & _
                      ", CASE optiongroupid WHEN 1 THEN @General " & _
                      "                               WHEN 2 THEN @Paths   " & _
                      "                               WHEN 3 THEN @Integration " & _
                      "                               WHEN 4 THEN @Settings " & _
                      "                               WHEN 5 THEN @Notification " & _
                      " END [Operation Mode]  " & _
                      ", configunkid " & _
                      ", atcfconfiguration.companyunkid " & _
                      ", cfcompany_master.code	" & _
                      ", cfcompany_master.name AS Company " & _
                      ", Description " & _
                      ", audituserunkid " & _
                      ", cfuser_master.username as [User] " & _
                      ", Auditdatetime " & _
                      ", IP " & _
                      ", machine_name as Machine " & _
                      " FROM atcfconfiguration " & _
                      " JOIN cfcompany_master ON atcfconfiguration.companyunkid = cfcompany_master.companyunkid " & _
                      " JOIN cfuser_master ON cfuser_master.userunkid = atcfconfiguration.audituserunkid " & _
                      " WHERE 1 = 1"

            If mstrFilter.Trim.Length > 0 Then
                strQ &= mstrFilter
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@General", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 362, "General"))
            objDataOperation.AddParameter("@Paths", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 361, "Paths"))
            objDataOperation.AddParameter("@Integration", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 360, "Integration"))
            objDataOperation.AddParameter("@Settings", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 359, "Settings"))
            objDataOperation.AddParameter("@Notification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 358, "Notification"))
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (atcfconfiguration) </purpose>
    Public Function Insert() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@optiongroupid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOptiongroupid.ToString)
            objDataOperation.AddParameter("@configunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConfigunkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.VarChar, mstrDescription.Trim.Length, mstrDescription.ToString)
            'Hemant (28 Jul 2021) -- Start             
            'ENHANCEMENT : OLD-293 - Training Evaluation
            If mintAudituserunkid > 0 Then
                objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            Else
                'Hemant (28 Jul 2021) -- End
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            End If  'Hemant (28 Jul 2021)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP().ToString())
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName().ToString())


            'Pinkal (01-Feb-2021) -- Start
            'Enhancement NMB - Working Training Enhancements for New UI.


            strQ = "INSERT INTO hrmsconfiguration..atcfconfiguration ( " & _
              "  optiongroupid " & _
              ", configunkid " & _
              ", companyunkid " & _
              ", description " & _
              ", audituserunkid " & _
              ", auditdatetime " & _
              ", ip " & _
              ", machine_name" & _
            ") VALUES (" & _
              "  @optiongroupid " & _
              ", @configunkid " & _
              ", @companyunkid " & _
              ", @description " & _
              ", @audituserunkid " & _
              ", @auditdatetime " & _
              ", @ip " & _
              ", @machine_name" & _
            "); SELECT @@identity"


            'Pinkal (01-Feb-2021) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAtoptionunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (atcfconfiguration) </purpose>
    Public Function Update() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@atoptionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAtoptionunkid.ToString)
            objDataOperation.AddParameter("@optiongroupid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOptiongroupid.ToString)
            objDataOperation.AddParameter("@configunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConfigunkid.ToString)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCompanyunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditdatetime.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachine_Name.ToString)

            strQ = "UPDATE atcfconfiguration SET " & _
              "  optiongroupid = @optiongroupid" & _
              ", configunkid = @configunkid" & _
              ", companyunkid = @companyunkid" & _
              ", description = @description" & _
              ", audituserunkid = @audituserunkid" & _
              ", auditdatetime = @auditdatetime" & _
              ", ip = @ip" & _
              ", machine_name = @machine_name " & _
            "WHERE atoptionunkid = @atoptionunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  atoptionunkid " & _
              ", optiongroupid " & _
              ", configunkid " & _
              ", companyunkid " & _
              ", description " & _
              ", audituserunkid " & _
              ", auditdatetime " & _
              ", ip " & _
              ", machine_name " & _
             "FROM atcfconfiguration " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND atoptionunkid <> @atoptionunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@atoptionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage("clsMasterData", 358, "Notification")
			Language.setMessage("clsMasterData", 359, "Settings")
			Language.setMessage("clsMasterData", 360, "Integration")
			Language.setMessage("clsMasterData", 361, "Paths")
			Language.setMessage("clsMasterData", 362, "General")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class