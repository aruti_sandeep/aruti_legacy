﻿'************************************************************************************************************************************
'Class Name : clsstapproverlevel_role_mapping.vb
'Purpose    :
'Date       :03-Jul-2023
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsstapproverlevel_role_mapping
    Private Shared ReadOnly mstrModuleName As String = "clsstapproverlevel_role_mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintStmappingunkid As Integer
    Private mintLevelunkid As Integer
    Private mintRoleunkid As Integer
    Private mblnIsuseraccess As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stmappingunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stmappingunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintStmappingunkid
        End Get
        Set(ByVal value As Integer)
            mintStmappingunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set roleunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Roleunkid() As Integer
        Get
            Return mintRoleunkid
        End Get
        Set(ByVal value As Integer)
            mintRoleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isuseraccess
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isuseraccess() As Boolean
        Get
            Return mblnIsuseraccess
        End Get
        Set(ByVal value As Boolean)
            mblnIsuseraccess = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  stmappingunkid " & _
                      ", levelunkid " & _
                      ", roleunkid " & _
                      ", isuseraccess " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      " FROM stapproverlevel_role_mapping " & _
                      " WHERE stmappingunkid = @stmappingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@stmappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStmappingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStmappingunkid = CInt(dtRow.Item("stmappingunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintRoleunkid = CInt(dtRow.Item("roleunkid"))
                mblnIsuseraccess = CBool(dtRow.Item("isuseraccess"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False AndAlso dtRow.Item("voiddatetime") <> Nothing Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True _
                                    , Optional ByVal objDoOperation As clsDataOperation = Nothing, Optional ByVal mstrFilter As String = "") As DataSet

        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  stapproverlevel_role_mapping.stmappingunkid " & _
                      ", stapproverlevel_role_mapping.levelunkid " & _
                      ", ISNULL(stapproverlevel_master.stlevelname,'') AS stlevelname " & _
                      ", stapproverlevel_master.priority " & _
                      ", stapproverlevel_role_mapping.roleunkid " & _
                      ", ISNULL(cr.name,'') AS Role " & _
                      ", stapproverlevel_role_mapping.isuseraccess " & _
                      ", stapproverlevel_role_mapping.userunkid " & _
                      ", stapproverlevel_role_mapping.isvoid " & _
                      ", stapproverlevel_role_mapping.voiddatetime " & _
                      ", stapproverlevel_role_mapping.voiduserunkid " & _
                      ", stapproverlevel_role_mapping.voidreason " & _
                      " FROM stapproverlevel_role_mapping " & _
                      " LEFT JOIN stapproverlevel_master on stapproverlevel_master.stlevelunkid = stapproverlevel_role_mapping.levelunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfrole_master cr ON cr.roleunkid = stapproverlevel_role_mapping.roleunkid "

            If blnOnlyActive Then
                strQ &= " WHERE stapproverlevel_role_mapping.isvoid = 0 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (stapproverlevel_role_mapping) </purpose>
    Public Function Insert(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        If isExist(mintRoleunkid, mintLevelunkid, mintStmappingunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Role already exists for this level. Please mapped different Role.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@isuseraccess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsuseraccess.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO stapproverlevel_role_mapping ( " & _
                      "  levelunkid " & _
                      ", roleunkid " & _
                      ", isuseraccess " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @levelunkid " & _
                      ", @roleunkid " & _
                      ", @isuseraccess " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStmappingunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailLevelRoleMapping(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (stapproverlevel_role_mapping) </purpose>
    Public Function Update(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        If isExist(mintRoleunkid, mintLevelunkid, mintStmappingunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Role already exists for this level. Please mapped different Role.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@stmappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStmappingunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@isuseraccess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsuseraccess.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE stapproverlevel_role_mapping SET " & _
                      "  levelunkid = @levelunkid" & _
                      ", roleunkid = @roleunkid" & _
                      ", isuseraccess = @isuseraccess" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      " WHERE isvoid = 0 AND stmappingunkid = @stmappingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailLevelRoleMapping(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = " UPDATE  stapproverlevel_role_mapping SET isvoid = @isvoid,voiduserunkid  = @voiduserunkid,voiddatetime = getdate(),voidreason = @voidreason " & _
                      " WHERE stmappingunkid = @stmappingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@stmappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Stmappingunkid(objDataOperation) = intUnkid

            If InsertAuditTrailLevelRoleMapping(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT ISNULL(stmappingunkid,0) AS stmappingunkid FROM sttransfer_approval_tran where stmappingunkid = @stmappingunkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@stmappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xRoleId As Integer, ByVal xApproverLevelId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  stmappingunkid " & _
                      ", levelunkid " & _
                      ", roleunkid " & _
                      ", isuseraccess " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM stapproverlevel_role_mapping " & _
                      " WHERE isvoid = 0 AND roleunkid = @roleunkid " & _
                      " AND levelunkid = @levelunkid "

            If intUnkid > 0 Then
                strQ &= " AND stmappingunkid <> @stmappingunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRoleId)
            objDataOperation.AddParameter("@stmappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xApproverLevelId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function InsertAuditTrailLevelRoleMapping(ByVal objDataOperation As clsDataOperation, ByVal AuditType As enAuditType) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO atstapproverlevel_role_mapping ( " & _
                      "  stmappingunkid " & _
                      ", levelunkid " & _
                      ", roleunkid " & _
                      ", isuseraccess " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip" & _
                      ", machine_name" & _
                      ", form_name " & _
                      ", isweb " & _
                      " ) VALUES (" & _
                      "  @stmappingunkid " & _
                      ", @levelunkid " & _
                      ", @roleunkid " & _
                      ", @isuseraccess " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip" & _
                      ", @machine_name" & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@stmappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStmappingunkid)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@isuseraccess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsuseraccess)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailLevelRoleMapping; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function


    '''' <summary>
    '''' Modify By: Pinkal Jariwala
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    '''' 
    'Public Function GetNextApprovers(ByVal strDatabaseName As String _
    '                                                              , ByVal xCompanyId As Integer _
    '                                                              , ByVal xYearId As Integer _
    '                                                              , ByVal strUserAccessMode As String _
    '                                                              , ByVal xPrivilegeId As Integer _
    '                                                              , ByVal xEmployeeAsOnDate As Date _
    '                                                              , ByVal intUserId As Integer _
    '                                                              , ByVal intEmployeeID As Integer _
    '                                                              , ByVal mblnIsUserAccess As Boolean _
    '                                                              , Optional ByVal objDataOpr As clsDataOperation = Nothing _
    '                                                              , Optional ByVal mstrFilterString As String = "" _
    '                                                              , Optional ByVal mdctStaffAllocations As Dictionary(Of Integer, Integer) = Nothing _
    '                                                              ) As DataTable
    '    Dim StrQ As String = String.Empty
    '    Dim dtList As DataTable = Nothing
    '    Dim objDataOperation As New clsDataOperation
    '    If objDataOpr IsNot Nothing Then
    '        objDataOperation = objDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Try
    '        Dim strFilter As String = String.Empty
    '        Dim strJoin As String = String.Empty
    '        Dim strSelect As String = String.Empty
    '        Dim strAccessJoin As String = String.Empty
    '        Dim strOuterJoin As String = String.Empty
    '        Dim strFinalString As String = ""


    '        If mblnIsUserAccess Then

    '            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
    '            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
    '            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xEmployeeAsOnDate, xEmployeeAsOnDate, , , strDatabaseName)
    '            Call GetAdvanceFilterQry(xAdvanceJoinQry, xEmployeeAsOnDate, strDatabaseName)

    '            StrQ = "IF OBJECT_ID('tempdb..#STAFFUSR') IS NOT NULL " & _
    '                 "DROP TABLE #STAFFUSR "
    '            StrQ &= "SELECT " & _
    '                    "* " & _
    '                    "INTO #STAFFUSR " & _
    '                    "FROM " & _
    '                    "( "

    '            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
    '            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
    '            For index As Integer = 0 To strvalues.Length - 1
    '                Dim xStrJoinColName As String = ""
    '                Dim xIntAllocId As Integer = 0
    '                Select Case CInt(strvalues(index))
    '                    Case enAllocation.BRANCH
    '                        xStrJoinColName = "stationunkid"
    '                        xIntAllocId = CInt(enAllocation.BRANCH)
    '                    Case enAllocation.DEPARTMENT_GROUP
    '                        xStrJoinColName = "deptgroupunkid"
    '                        xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
    '                    Case enAllocation.DEPARTMENT
    '                        xStrJoinColName = "departmentunkid"
    '                        xIntAllocId = CInt(enAllocation.DEPARTMENT)
    '                    Case enAllocation.SECTION_GROUP
    '                        xStrJoinColName = "sectiongroupunkid"
    '                        xIntAllocId = CInt(enAllocation.SECTION_GROUP)
    '                    Case enAllocation.SECTION
    '                        xStrJoinColName = "sectionunkid"
    '                        xIntAllocId = CInt(enAllocation.SECTION)
    '                    Case enAllocation.UNIT_GROUP
    '                        xStrJoinColName = "unitgroupunkid"
    '                        xIntAllocId = CInt(enAllocation.UNIT_GROUP)
    '                    Case enAllocation.UNIT
    '                        xStrJoinColName = "unitunkid"
    '                        xIntAllocId = CInt(enAllocation.UNIT)
    '                    Case enAllocation.TEAM
    '                        xStrJoinColName = "teamunkid"
    '                        xIntAllocId = CInt(enAllocation.TEAM)
    '                    Case enAllocation.JOB_GROUP
    '                        xStrJoinColName = "jobgroupunkid"
    '                        xIntAllocId = CInt(enAllocation.JOB_GROUP)
    '                    Case enAllocation.JOBS
    '                        xStrJoinColName = "jobunkid"
    '                        xIntAllocId = CInt(enAllocation.JOBS)
    '                    Case enAllocation.CLASS_GROUP
    '                        xStrJoinColName = "classgroupunkid"
    '                        xIntAllocId = CInt(enAllocation.CLASS_GROUP)
    '                    Case enAllocation.CLASSES
    '                        xStrJoinColName = "classunkid"
    '                        xIntAllocId = CInt(enAllocation.CLASSES)
    '                End Select
    '                StrQ &= "SELECT DISTINCT " & _
    '                        "    B" & index.ToString() & ".userunkid AS mapuserunkid " & _
    '                        "   ,A.employeeunkid " & _
    '                        "FROM " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        AEM.employeeunkid " & _
    '                        "       ,ISNULL(T.departmentunkid, 0) AS departmentunkid " & _
    '                        "       ,ISNULL(J.jobunkid, 0) AS jobunkid " & _
    '                        "       ,ISNULL(T.classgroupunkid, 0) AS classgroupunkid " & _
    '                        "       ,ISNULL(T.classunkid, 0) AS classunkid " & _
    '                        "       ,ISNULL(T.stationunkid, 0) AS stationunkid " & _
    '                        "       ,ISNULL(T.deptgroupunkid, 0) AS deptgroupunkid " & _
    '                        "       ,ISNULL(T.sectiongroupunkid, 0) AS sectiongroupunkid " & _
    '                        "       ,ISNULL(T.sectionunkid, 0) AS sectionunkid " & _
    '                        "       ,ISNULL(T.unitgroupunkid, 0) AS unitgroupunkid " & _
    '                        "       ,ISNULL(T.unitunkid, 0) AS unitunkid " & _
    '                        "       ,ISNULL(T.teamunkid, 0) AS teamunkid " & _
    '                        "       ,ISNULL(J.jobgroupunkid, 0) AS jobgroupunkid " & _
    '                        "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
    '                        "   LEFT JOIN " & _
    '                        "   ( " & _
    '                        "       SELECT " & _
    '                        "            stationunkid " & _
    '                        "           ,deptgroupunkid " & _
    '                        "           ,departmentunkid " & _
    '                        "           ,sectiongroupunkid " & _
    '                        "           ,sectionunkid " & _
    '                        "           ,unitgroupunkid " & _
    '                        "           ,unitunkid " & _
    '                        "           ,teamunkid " & _
    '                        "           ,classgroupunkid " & _
    '                        "           ,classunkid " & _
    '                        "           ,employeeunkid " & _
    '                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                        "       FROM " & strDatabaseName & "..hremployee_transfer_tran " & _
    '                        "       WHERE isvoid = 0 " & _
    '                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
    '                        "   ) AS T ON T.employeeunkid = AEM.employeeunkid " & _
    '                        "   AND T.Rno = 1 " & _
    '                        "   LEFT JOIN " & _
    '                        "   ( " & _
    '                        "       SELECT " & _
    '                        "            jobgroupunkid " & _
    '                        "           ,jobunkid " & _
    '                        "           ,employeeunkid " & _
    '                        "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
    '                        "       FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
    '                        "       WHERE isvoid = 0 " & _
    '                        "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
    '                        "   ) AS J ON J.employeeunkid = AEM.employeeunkid " & _
    '                        "   AND J.Rno = 1 " & _
    '                        ") AS A " & _
    '                        "JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        UPM.userunkid " & _
    '                        "       ,UPT.allocationunkid " & _
    '                        "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                        "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                        "   WHERE UPM.companyunkid = " & xCompanyId & " AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
    '                        ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "

    '                StrQ &= " AND A.employeeunkid = " & intEmployeeID & " "
    '                If index < strvalues.Length - 1 Then
    '                    StrQ &= " INTERSECT "
    '                End If
    '            Next

    '            StrQ &= ") AS Fl "

    '            StrQ &= "SELECT " & _
    '                       "    mapuserunkid " & _
    '                       ",   employeeunkid " & _
    '                       ",   roleunkid " & _
    '                       ",   levelunkid " & _
    '                       ",   priority " & _
    '                       ",   stmappingunkid " & _
    '                       "  FROM ( " & _
    '                       "              SELECT " & _
    '                       "                #STAFFUSR.mapuserunkid " & _
    '                       "               ,#STAFFUSR.employeeunkid " & _
    '                       "               ,UM.roleunkid " & _
    '                       "               ,TAM.levelunkid " & _
    '                       "               ,TLM.priority " & _
    '                       "               ,TAM.stmappingunkid  " & _
    '                       "               ,DENSE_RANK() OVER (PARTITION BY #STAFFUSR.mapuserunkid ORDER BY TAM.roleunkid ASC, TLM.priority ASC, TAM.levelunkid ASC, TAM.stmappingunkid ASC) AS rowno " & _
    '                       "             FROM #STAFFUSR " & _
    '                       "             JOIN hrmsConfiguration..cfuser_master UM ON UM.userunkid = #STAFFUSR.mapuserunkid " & _
    '                       "             AND UM.employeeunkid <> " & intEmployeeID & _
    '                       "             JOIN stapproverlevel_role_mapping TAM  ON TAM.roleunkid = UM.roleunkid  AND TAM.isvoid = 0 AND TAM.isuseraccess = 1" & _
    '                       "             JOIN stapproverlevel_master TLM ON TLM.stlevelunkid = TAM.levelunkid AND TLM.isactive = 1 " & _
    '                       "             JOIN " & _
    '                       "             ( " & _
    '                       "                    SELECT DISTINCT " & _
    '                       "                            cfuser_master.userunkid " & _
    '                       "                           ,cfuser_master.firstname + ' ' +cfuser_master.lastname as username " & _
    '                       "                           ,cfuser_master.email " & _
    '                       "                           ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
    '                       "                           ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
    '                       "                    FROM hrmsConfiguration..cfuser_master " & _
    '                       "                    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
    '                       "                    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
    '                       "                    WHERE cfuser_master.isactive = 1  AND yearunkid = @Y AND privilegeunkid = @P  " & _
    '                       "            ) AS Fn ON #STAFFUSR.mapuserunkid = Fn.userunkid  " & _
    '                       " LEFT JOIN hrmsConfiguration..cfuser_master on cfuser_master.userunkid = #STAFFUSR.mapuserunkid " & _
    '                       " LEFT JOIN hremployee_master UserEmp ON UserEmp.employeeunkid = hrmsConfiguration..cfuser_master.employeeunkid "

    '            If xDateJoinQry.Trim.Length > 0 Then
    '                StrQ &= xDateJoinQry.Replace("hremployee_master", "UserEmp")
    '            End If

    '            If xAdvanceJoinQry.Trim.Length > 0 Then
    '                StrQ &= xAdvanceJoinQry.Replace("hremployee_master", "UserEmp")
    '            End If

    '            StrQ &= " WHERE #STAFFUSR.employeeunkid = " & intEmployeeID & " " & " AND UM.roleunkid > 0 "

    '            If xDateFilterQry.Trim.Length > 0 Then
    '                StrQ &= xDateFilterQry.Replace("hremployee_master", "UserEmp")
    '            End If

    '            StrQ &= " ) AS Y " & _
    '                         " WHERE Y.rowno = 1 "

    '            If mstrFilterString.Trim.Length > 0 Then
    '                StrQ &= " AND " & mstrFilterString.Trim
    '            End If

    '            StrQ &= " ORDER BY priority "

    '            StrQ &= " DROP TABLE #STAFFUSR "

    '            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
    '            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearId)
    '            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, xPrivilegeId)

    '        Else

    '            If mdctStaffAllocations IsNot Nothing AndAlso mdctStaffAllocations.Count > 0 Then


    '                StrQ = " IF OBJECT_ID('tempdb..#STAFFUSR') IS NOT NULL " & _
    '                           " DROP TABLE #STAFFUSR  "

    '                StrQ &= " SELECT  * " & _
    '                             "      INTO #STAFFUSR " & _
    '                             "  FROM " & _
    '                             " ( "

    '                Dim index As Integer = 0
    '                For Each sKey As String In mdctStaffAllocations.Keys

    '                    Dim xStrJoinColName As String = ""
    '                    Dim xIntRefId As Integer = 0

    '                    Select Case CInt(sKey)

    '                        Case enAllocation.BRANCH
    '                            xStrJoinColName = "stationunkid"

    '                        Case enAllocation.DEPARTMENT_GROUP
    '                            xStrJoinColName = "deptgroupunkid"

    '                        Case enAllocation.DEPARTMENT
    '                            xStrJoinColName = "departmentunkid"

    '                        Case enAllocation.SECTION_GROUP
    '                            xStrJoinColName = "sectiongroupunkid"

    '                        Case enAllocation.SECTION
    '                            xStrJoinColName = "sectionunkid"

    '                        Case enAllocation.UNIT_GROUP
    '                            xStrJoinColName = "unitgroupunkid"

    '                        Case enAllocation.UNIT
    '                            xStrJoinColName = "unitunkid"

    '                        Case enAllocation.TEAM
    '                            xStrJoinColName = "teamunkid"

    '                        Case enAllocation.JOB_GROUP
    '                            xStrJoinColName = "jobgroupunkid"

    '                        Case enAllocation.JOBS
    '                            xStrJoinColName = "jobunkid"

    '                        Case enAllocation.CLASS_GROUP
    '                            xStrJoinColName = "classgroupunkid"

    '                        Case enAllocation.CLASSES
    '                            xStrJoinColName = "classunkid"

    '                    End Select

    '                    index += 1

    '                    xIntRefId = mdctStaffAllocations(sKey)

    '                    StrQ &= "SELECT DISTINCT " & _
    '                       "    B" & index.ToString() & ".userunkid AS mapuserunkid " & _
    '                       "   , " & intEmployeeID & " AS employeeunkid " & _
    '                       "FROM " & _
    '                       "( " & _
    '                       "   SELECT " & _
    '                       "        UPM.userunkid " & _
    '                       "       ,UPT.allocationunkid " & _
    '                       "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                       "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                       "   WHERE UPM.companyunkid = " & xCompanyId & " AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & sKey & ") " & _
    '                       "   AND UPT.allocationunkid = " & mdctStaffAllocations(sKey) & _
    '                       ") AS B" & index.ToString()

    '                    If index < mdctStaffAllocations.Count Then
    '                        StrQ &= " INTERSECT "
    '                    End If

    '                Next

    '                StrQ &= ") AS Fl "

    '                StrQ &= "SELECT " & _
    '                     "    mapuserunkid " & _
    '                     ",   employeeunkid " & _
    '                     ",   roleunkid " & _
    '                     ",   levelunkid " & _
    '                     ",   priority " & _
    '                     ",   stmappingunkid " & _
    '                     "  FROM ( " & _
    '                     "              SELECT " & _
    '                     "                #STAFFUSR.mapuserunkid " & _
    '                     "               ,#STAFFUSR.employeeunkid " & _
    '                     "               ,UM.roleunkid " & _
    '                     "               ,TAM.levelunkid " & _
    '                     "               ,TLM.priority " & _
    '                     "               ,TAM.stmappingunkid  " & _
    '                     "               ,DENSE_RANK() OVER (PARTITION BY #STAFFUSR.mapuserunkid ORDER BY TAM.roleunkid ASC, TLM.priority ASC, TAM.levelunkid ASC, TAM.stmappingunkid ASC) AS rowno " & _
    '                     "             FROM #STAFFUSR " & _
    '                     "             JOIN hrmsConfiguration..cfuser_master UM ON UM.userunkid = #STAFFUSR.mapuserunkid " & _
    '                     "             JOIN stapproverlevel_role_mapping TAM  ON TAM.roleunkid = UM.roleunkid  AND TAM.isvoid = 0 AND TAM.isuseraccess = 0 " & _
    '                     "             JOIN stapproverlevel_master TLM ON TLM.stlevelunkid = TAM.levelunkid AND TLM.isactive = 1 " & _
    '                     "             JOIN " & _
    '                     "             ( " & _
    '                     "                    SELECT DISTINCT " & _
    '                     "                            cfuser_master.userunkid " & _
    '                     "                           ,cfuser_master.firstname + ' ' +cfuser_master.lastname as username " & _
    '                     "                           ,cfuser_master.email " & _
    '                     "                           ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
    '                     "                           ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
    '                     "                    FROM hrmsConfiguration..cfuser_master " & _
    '                     "                    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
    '                     "                    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
    '                     "                    WHERE cfuser_master.isactive = 1  AND yearunkid = @Y AND privilegeunkid = @P  " & _
    '                     "            ) AS Fn ON #STAFFUSR.mapuserunkid = Fn.userunkid  " & _
    '                     " LEFT JOIN hrmsConfiguration..cfuser_master on cfuser_master.userunkid = #STAFFUSR.mapuserunkid " & _
    '                     " WHERE UM.roleunkid > 0 ) AS Y WHERE Y.rowno = 1 "

    '                If mstrFilterString.Trim.Length > 0 Then
    '                    StrQ &= " AND " & mstrFilterString.Trim
    '                End If

    '                StrQ &= " ORDER BY priority "

    '                StrQ &= " DROP TABLE #STAFFUSR "

    '                objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
    '                objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearId)
    '                objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, xPrivilegeId)

    '            End If

    '        End If

    '        Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '        dtList = dsList.Tables("List").Copy()

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetNextApprovers; Module Name: " & mstrModuleName)
    '    Finally
    '        If objDataOpr Is Nothing Then objDataOperation = Nothing
    '    End Try
    '    Return dtList
    'End Function


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetNextApprovers(ByVal strDatabaseName As String _
                                                                  , ByVal xCompanyId As Integer _
                                                                  , ByVal xYearId As Integer _
                                                                  , ByVal strUserAccessMode As String _
                                                                  , ByVal xPrivilegeId As Integer _
                                                                  , ByVal xEmployeeAsOnDate As Date _
                                                                  , ByVal intUserId As Integer _
                                                                  , ByVal intEmployeeID As Integer _
                                                                  , ByVal mblnIsUserAccess As Boolean _
                                                                  , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                                                  , Optional ByVal mstrFilterString As String = "" _
                                                                  , Optional ByVal mdctStaffAllocations As Dictionary(Of Integer, Integer) = Nothing _
                                                                  ) As DataTable
        Dim StrQ As String = String.Empty
        Dim dtList As DataTable = Nothing
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""


            If mblnIsUserAccess Then

                Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xEmployeeAsOnDate, xEmployeeAsOnDate, , , strDatabaseName)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, xEmployeeAsOnDate, strDatabaseName)

                StrQ = "IF OBJECT_ID('tempdb..#STAFFUSR') IS NOT NULL " & _
                     "DROP TABLE #STAFFUSR "
                StrQ &= "SELECT " & _
                        "* " & _
                        "INTO #STAFFUSR " & _
                        "FROM " & _
                        "( "

                If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
                Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
                For index As Integer = 0 To strvalues.Length - 1
                    Dim xStrJoinColName As String = ""
                    Dim xIntAllocId As Integer = 0
                    Select Case CInt(strvalues(index))
                        Case enAllocation.BRANCH
                            xStrJoinColName = "stationunkid"
                            xIntAllocId = CInt(enAllocation.BRANCH)
                        Case enAllocation.DEPARTMENT_GROUP
                            xStrJoinColName = "deptgroupunkid"
                            xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
                        Case enAllocation.DEPARTMENT
                            xStrJoinColName = "departmentunkid"
                            xIntAllocId = CInt(enAllocation.DEPARTMENT)
                        Case enAllocation.SECTION_GROUP
                            xStrJoinColName = "sectiongroupunkid"
                            xIntAllocId = CInt(enAllocation.SECTION_GROUP)
                        Case enAllocation.SECTION
                            xStrJoinColName = "sectionunkid"
                            xIntAllocId = CInt(enAllocation.SECTION)
                        Case enAllocation.UNIT_GROUP
                            xStrJoinColName = "unitgroupunkid"
                            xIntAllocId = CInt(enAllocation.UNIT_GROUP)
                        Case enAllocation.UNIT
                            xStrJoinColName = "unitunkid"
                            xIntAllocId = CInt(enAllocation.UNIT)
                        Case enAllocation.TEAM
                            xStrJoinColName = "teamunkid"
                            xIntAllocId = CInt(enAllocation.TEAM)
                        Case enAllocation.JOB_GROUP
                            xStrJoinColName = "jobgroupunkid"
                            xIntAllocId = CInt(enAllocation.JOB_GROUP)
                        Case enAllocation.JOBS
                            xStrJoinColName = "jobunkid"
                            xIntAllocId = CInt(enAllocation.JOBS)
                        Case enAllocation.CLASS_GROUP
                            xStrJoinColName = "classgroupunkid"
                            xIntAllocId = CInt(enAllocation.CLASS_GROUP)
                        Case enAllocation.CLASSES
                            xStrJoinColName = "classunkid"
                            xIntAllocId = CInt(enAllocation.CLASSES)
                    End Select
                    StrQ &= "SELECT DISTINCT " & _
                            "    B" & index.ToString() & ".userunkid AS mapuserunkid " & _
                            "   ,A.employeeunkid " & _
                            "FROM " & _
                            "( " & _
                            "   SELECT " & _
                            "        AEM.employeeunkid " & _
                            "       ,ISNULL(T.departmentunkid, 0) AS departmentunkid " & _
                            "       ,ISNULL(J.jobunkid, 0) AS jobunkid " & _
                            "       ,ISNULL(T.classgroupunkid, 0) AS classgroupunkid " & _
                            "       ,ISNULL(T.classunkid, 0) AS classunkid " & _
                            "       ,ISNULL(T.stationunkid, 0) AS stationunkid " & _
                            "       ,ISNULL(T.deptgroupunkid, 0) AS deptgroupunkid " & _
                            "       ,ISNULL(T.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                            "       ,ISNULL(T.sectionunkid, 0) AS sectionunkid " & _
                            "       ,ISNULL(T.unitgroupunkid, 0) AS unitgroupunkid " & _
                            "       ,ISNULL(T.unitunkid, 0) AS unitunkid " & _
                            "       ,ISNULL(T.teamunkid, 0) AS teamunkid " & _
                            "       ,ISNULL(J.jobgroupunkid, 0) AS jobgroupunkid " & _
                            "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                            "   LEFT JOIN " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            stationunkid " & _
                            "           ,deptgroupunkid " & _
                            "           ,departmentunkid " & _
                            "           ,sectiongroupunkid " & _
                            "           ,sectionunkid " & _
                            "           ,unitgroupunkid " & _
                            "           ,unitunkid " & _
                            "           ,teamunkid " & _
                            "           ,classgroupunkid " & _
                            "           ,classunkid " & _
                            "           ,employeeunkid " & _
                            "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                            "       FROM " & strDatabaseName & "..hremployee_transfer_tran " & _
                            "       WHERE isvoid = 0 " & _
                            "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                            "   ) AS T ON T.employeeunkid = AEM.employeeunkid " & _
                            "   AND T.Rno = 1 " & _
                            "   LEFT JOIN " & _
                            "   ( " & _
                            "       SELECT " & _
                            "            jobgroupunkid " & _
                            "           ,jobunkid " & _
                            "           ,employeeunkid " & _
                            "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                            "       FROM " & strDatabaseName & "..hremployee_categorization_tran " & _
                            "       WHERE isvoid = 0 " & _
                            "       AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(xEmployeeAsOnDate) & "' " & _
                            "   ) AS J ON J.employeeunkid = AEM.employeeunkid " & _
                            "   AND J.Rno = 1 " & _
                            ") AS A " & _
                            "JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        UPM.userunkid " & _
                            "       ,UPT.allocationunkid " & _
                            "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                            "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                            "   WHERE UPM.companyunkid = " & xCompanyId & " AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
                            ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "

                    StrQ &= " AND A.employeeunkid = " & intEmployeeID & " "
                    If index < strvalues.Length - 1 Then
                        StrQ &= " INTERSECT "
                    End If
                Next

                StrQ &= ") AS Fl "

                StrQ &= "SELECT " & _
                           "    mapuserunkid " & _
                           ",   employeeunkid " & _
                           ",   roleunkid " & _
                           ",   levelunkid " & _
                           ",   priority " & _
                           ",   stmappingunkid " & _
                           "  FROM ( " & _
                           "              SELECT " & _
                           "                #STAFFUSR.mapuserunkid " & _
                           "               ,#STAFFUSR.employeeunkid " & _
                           "               ,UM.roleunkid " & _
                           "               ,TAM.levelunkid " & _
                           "               ,TLM.priority " & _
                           "               ,TAM.stmappingunkid  " & _
                           "               ,DENSE_RANK() OVER (PARTITION BY #STAFFUSR.mapuserunkid ORDER BY TAM.roleunkid ASC, TLM.priority ASC, TAM.levelunkid ASC, TAM.stmappingunkid ASC) AS rowno " & _
                           "             FROM #STAFFUSR " & _
                           "             JOIN hrmsConfiguration..cfuser_master UM ON UM.userunkid = #STAFFUSR.mapuserunkid " & _
                           "             AND UM.employeeunkid <> " & intEmployeeID & _
                           "             JOIN stapproverlevel_role_mapping TAM  ON TAM.roleunkid = UM.roleunkid  AND TAM.isvoid = 0 AND TAM.isuseraccess = 1" & _
                           "             JOIN stapproverlevel_master TLM ON TLM.stlevelunkid = TAM.levelunkid AND TLM.isactive = 1 " & _
                           "             JOIN " & _
                           "             ( " & _
                           "                    SELECT DISTINCT " & _
                           "                            cfuser_master.userunkid " & _
                           "                           ,cfuser_master.firstname + ' ' +cfuser_master.lastname as username " & _
                           "                           ,cfuser_master.email " & _
                           "                           ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                           "                           ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                           "                    FROM hrmsConfiguration..cfuser_master " & _
                           "                    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                           "                    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                           "                    WHERE cfuser_master.isactive = 1  AND yearunkid = @Y AND privilegeunkid = @P  " & _
                           "            ) AS Fn ON #STAFFUSR.mapuserunkid = Fn.userunkid  " & _
                           " LEFT JOIN hrmsConfiguration..cfuser_master on cfuser_master.userunkid = #STAFFUSR.mapuserunkid " & _
                           " LEFT JOIN hremployee_master UserEmp ON UserEmp.employeeunkid = hrmsConfiguration..cfuser_master.employeeunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry.Replace("hremployee_master", "UserEmp")
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry.Replace("hremployee_master", "UserEmp")
                End If

                StrQ &= " WHERE #STAFFUSR.employeeunkid = " & intEmployeeID & " " & " AND UM.roleunkid > 0 "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry.Replace("hremployee_master", "UserEmp")
                End If

                StrQ &= " ) AS Y " & _
                             " WHERE Y.rowno = 1 "

                If mstrFilterString.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrFilterString.Trim
                End If

                StrQ &= " ORDER BY priority "

                StrQ &= " DROP TABLE #STAFFUSR "

                objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
                objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearId)
                objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, xPrivilegeId)

            Else

                If mdctStaffAllocations IsNot Nothing AndAlso mdctStaffAllocations.Count > 0 Then

                    Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
                    xDateJoinQry = "" : xDateFilterQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xEmployeeAsOnDate, xEmployeeAsOnDate, , , strDatabaseName)


                    StrQ = " IF OBJECT_ID('tempdb..#STAFFUSR') IS NOT NULL " & _
                               " DROP TABLE #STAFFUSR  "

                    StrQ &= " SELECT  * " & _
                                 "      INTO #STAFFUSR " & _
                                 "  FROM " & _
                                 " ( "

                    Dim index As Integer = 0
                    For Each sKey As String In mdctStaffAllocations.Keys

                        Dim xStrJoinColName As String = ""
                        Dim xIntRefId As Integer = 0

                        Select Case CInt(sKey)

                            Case enAllocation.BRANCH
                                xStrJoinColName = "stationunkid"

                            Case enAllocation.DEPARTMENT_GROUP
                                xStrJoinColName = "deptgroupunkid"

                            Case enAllocation.DEPARTMENT
                                xStrJoinColName = "departmentunkid"

                            Case enAllocation.SECTION_GROUP
                                xStrJoinColName = "sectiongroupunkid"

                            Case enAllocation.SECTION
                                xStrJoinColName = "sectionunkid"

                            Case enAllocation.UNIT_GROUP
                                xStrJoinColName = "unitgroupunkid"

                            Case enAllocation.UNIT
                                xStrJoinColName = "unitunkid"

                            Case enAllocation.TEAM
                                xStrJoinColName = "teamunkid"

                            Case enAllocation.JOB_GROUP
                                xStrJoinColName = "jobgroupunkid"

                            Case enAllocation.JOBS
                                xStrJoinColName = "jobunkid"

                            Case enAllocation.CLASS_GROUP
                                xStrJoinColName = "classgroupunkid"

                            Case enAllocation.CLASSES
                                xStrJoinColName = "classunkid"

                        End Select

                        index += 1

                        xIntRefId = mdctStaffAllocations(sKey)

                        StrQ &= "SELECT DISTINCT " & _
                           "    B" & index.ToString() & ".userunkid AS mapuserunkid " & _
                           "   , " & intEmployeeID & " AS employeeunkid " & _
                           "FROM " & _
                           "( " & _
                           "   SELECT " & _
                           "        UPM.userunkid " & _
                           "       ,UPT.allocationunkid " & _
                           "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                           "       JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                           "   WHERE UPM.companyunkid = " & xCompanyId & " AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & sKey & ") " & _
                           "   AND UPT.allocationunkid = " & mdctStaffAllocations(sKey) & _
                           ") AS B" & index.ToString()

                        If index < mdctStaffAllocations.Count Then
                            StrQ &= " INTERSECT "
                        End If

                    Next

                    StrQ &= ") AS Fl "

                    StrQ &= "SELECT " & _
                         "    mapuserunkid " & _
                         ",   employeeunkid " & _
                         ",   roleunkid " & _
                         ",   levelunkid " & _
                         ",   priority " & _
                         ",   stmappingunkid " & _
                         "  FROM ( " & _
                         "              SELECT " & _
                         "                #STAFFUSR.mapuserunkid " & _
                         "               ,#STAFFUSR.employeeunkid " & _
                         "               ,UM.roleunkid " & _
                         "               ,TAM.levelunkid " & _
                         "               ,TLM.priority " & _
                         "               ,TAM.stmappingunkid  " & _
                         "               ,DENSE_RANK() OVER (PARTITION BY #STAFFUSR.mapuserunkid ORDER BY TAM.roleunkid ASC, TLM.priority ASC, TAM.levelunkid ASC, TAM.stmappingunkid ASC) AS rowno " & _
                         "             FROM #STAFFUSR " & _
                         "             JOIN hrmsConfiguration..cfuser_master UM ON UM.userunkid = #STAFFUSR.mapuserunkid " & _
                         "             JOIN stapproverlevel_role_mapping TAM  ON TAM.roleunkid = UM.roleunkid  AND TAM.isvoid = 0 AND TAM.isuseraccess = 0 " & _
                         "             JOIN stapproverlevel_master TLM ON TLM.stlevelunkid = TAM.levelunkid AND TLM.isactive = 1 " & _
                         "             JOIN " & _
                         "             ( " & _
                         "                    SELECT DISTINCT " & _
                         "                            cfuser_master.userunkid " & _
                         "                           ,cfuser_master.firstname + ' ' +cfuser_master.lastname as username " & _
                         "                           ,cfuser_master.email " & _
                         "                           ,hrmsConfiguration..cfuser_master.employeeunkid AS uempid " & _
                         "                           ,hrmsConfiguration..cfuser_master.companyunkid AS ecompid " & _
                         "                    FROM hrmsConfiguration..cfuser_master " & _
                         "                    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                         "                    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                         "                    WHERE cfuser_master.isactive = 1  AND yearunkid = @Y AND privilegeunkid = @P  " & _
                         "            ) AS Fn ON #STAFFUSR.mapuserunkid = Fn.userunkid  " & _
                         "          LEFT JOIN hremployee_master UserEmp ON UserEmp.employeeunkid = UM.employeeunkid "

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= xDateJoinQry.Replace("hremployee_master", "UserEmp")
                    End If

                    StrQ &= " WHERE UM.roleunkid > 0  "

                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry.Replace("hremployee_master", "UserEmp")
                    End If

                    StrQ &= " ) AS Y WHERE Y.rowno = 1 "

                    If mstrFilterString.Trim.Length > 0 Then
                        StrQ &= " AND " & mstrFilterString.Trim
                    End If

                    StrQ &= " ORDER BY priority "

                    StrQ &= " DROP TABLE #STAFFUSR "

                    objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, xCompanyId)
                    objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, xYearId)
                    objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, xPrivilegeId)

                End If

            End If

            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            dtList = dsList.Tables("List").Copy()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextApprovers; Module Name: " & mstrModuleName)
        Finally
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return dtList
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Role already exists for this level. Please mapped different Role.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class