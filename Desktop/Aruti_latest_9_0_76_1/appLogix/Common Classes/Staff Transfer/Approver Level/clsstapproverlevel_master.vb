﻿'************************************************************************************************************************************
'Class Name : clsstapproverlevel_master.vb
'Purpose    :
'Date       :03-Jul-2023
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsstapproverlevel_master
    Private Shared ReadOnly mstrModuleName As String = "clsStapproverlevel_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintStlevelunkid As Integer
    Private mstrstLevelcode As String = String.Empty
    Private mstrstlevelname As String = String.Empty
    Private mintPriority As Integer
    Private mblnIsactive As Boolean = True
    Private mstrStlevelname1 As String = String.Empty
    Private mstrStlevelname2 As String = String.Empty
    Private mintUserunkid As Integer = 0
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mblnIsWeb As Boolean = False
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stlevelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _stlevelunkid() As Integer
        Get
            Return mintStlevelunkid
        End Get
        Set(ByVal value As Integer)
            mintStlevelunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stlevelcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _stLevelcode() As String
        Get
            Return mstrstLevelcode
        End Get
        Set(ByVal value As String)
            mstrstLevelcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stlevelname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stlevelname() As String
        Get
            Return mstrstlevelname
        End Get
        Set(ByVal value As String)
            mstrstlevelname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stlevelname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stlevelname1() As String
        Get
            Return mstrStlevelname1
        End Get
        Set(ByVal value As String)
            mstrStlevelname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stlevelname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stlevelname2() As String
        Get
            Return mstrStlevelname2
        End Get
        Set(ByVal value As String)
            mstrStlevelname2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webclientip
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webhostname
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = " SELECT " & _
                      "  stlevelunkid " & _
                      ", stlevelcode " & _
                      ", stlevelname " & _
                      ", priority " & _
                      ", isactive " & _
                      ", stlevelname1 " & _
                      ", stlevelname2 " & _
                      " FROM stapproverlevel_master " & _
                      " WHERE stlevelunkid = @stlevelunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@stlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStlevelunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStlevelunkid = CInt(dtRow.Item("stlevelunkid"))
                mstrstLevelcode = dtRow.Item("stlevelcode").ToString
                mstrstlevelname = dtRow.Item("stlevelname").ToString
                mintPriority = CInt(dtRow.Item("priority"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrStlevelname1 = dtRow.Item("stlevelname1").ToString
                mstrStlevelname2 = dtRow.Item("stlevelname2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  stlevelunkid " & _
                      ", stlevelcode " & _
                      ", stlevelname " & _
                      ", priority " & _
                      ", isactive " & _
                      ", stlevelname1 " & _
                      ", stlevelname2 " & _
                      " FROM stapproverlevel_master " & _
                      " WHERE 1 = 1"

            If blnOnlyActive Then
                strQ &= " AND isactive = 1 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (stapproverlevel_master) </purpose>
    Public Function Insert(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        If isExist(mstrstLevelcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist("", mstrstlevelname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintPriority) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@stlevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrstLevelcode.ToString)
            objDataOperation.AddParameter("@stlevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrstlevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@stlevelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStlevelname1.ToString)
            objDataOperation.AddParameter("@stlevelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStlevelname2.ToString)

            strQ = "INSERT INTO stapproverlevel_master ( " & _
                      "  stlevelcode " & _
                      ", stlevelname " & _
                      ", priority " & _
                      ", isactive " & _
                      ", stlevelname1 " & _
                      ", stlevelname2" & _
                    ") VALUES (" & _
                      "  @stlevelcode " & _
                      ", @stlevelname " & _
                      ", @priority " & _
                      ", @isactive " & _
                      ", @stlevelname1 " & _
                      ", @stlevelname2" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStlevelunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForApproverLevel(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (stapproverlevel_master) </purpose>
    Public Function Update(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        If isExist(mstrstLevelcode, "", mintStlevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist("", mstrstlevelname, mintStlevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintPriority, mintStlevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@stlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStlevelunkid.ToString)
            objDataOperation.AddParameter("@stlevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrstLevelcode.ToString)
            objDataOperation.AddParameter("@stlevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrstlevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@stlevelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStlevelname1.ToString)
            objDataOperation.AddParameter("@stlevelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStlevelname2.ToString)

            strQ = "UPDATE stapproverlevel_master SET " & _
                      "  stlevelcode = @stlevelcode" & _
                      ", stlevelname = @stlevelname" & _
                      ", priority = @priority" & _
                      ", isactive = @isactive" & _
                      ", stlevelname1 = @stlevelname1" & _
                      ", stlevelname2 = @stlevelname2 " & _
                      " WHERE stlevelunkid = @stlevelunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForApproverLevel(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvapproverlevel_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, You cannot delete selected Approver Level. Reason: This Approver Level is in use.")
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = " Update stapproverlevel_master set isactive = 0 " & _
                      " WHERE stlevelunkid = @stlevelunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@stlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ClearParameters()
            mintStlevelunkid = intUnkid
            GetData(objDataOperation)

            If InsertAuditTrailForApproverLevel(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT ISNULL(levelunkid,0) AS levelunkid FROM stapproverlevel_role_mapping WHERE levelunkid = @levelunkid " & _
                      " UNION " & _
                       "SELECT ISNULL(levelunkid,0) AS levelunkid FROM sttransfer_approval_tran WHERE levelunkid = @levelunkid "

            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  stlevelunkid " & _
                      ", stlevelcode " & _
                      ", stlevelname " & _
                      ", priority " & _
                      ", isactive " & _
                      ", stlevelname1 " & _
                      ", stlevelname2 " & _
                      " FROM stapproverlevel_master " & _
                      " WHERE 1=1 "


            If strCode.Length > 0 Then
                strQ &= " AND stlevelcode = @stlevelcode "
            End If
            If strName.Length > 0 Then
                strQ &= " AND stlevelname = @stlevelname "
            End If

            If intUnkid > 0 Then
                strQ &= " AND stlevelunkid <> @stlevelunkid"
            End If

            objDataOperation.AddParameter("@stlevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@stlevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@stlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isPriorityExist(ByVal mintPriority As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  stlevelunkid " & _
                      ", stlevelcode " & _
                      ", stlevelname " & _
                      ", priority " & _
                      ", isactive " & _
                      ", stlevelname1 " & _
                      ", stlevelname2 " & _
                      " FROM stapproverlevel_master " & _
                      " WHERE priority = @priority"


            If intUnkid > 0 Then
                strQ &= " AND stlevelunkid <> @stlevelunkid"
            End If

            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
            objDataOperation.AddParameter("@stlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPriorityExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as stlevelunkid, ' ' +  @name  as name UNION "
            End If
            strQ &= "SELECT stlevelunkid, stlevelname as name FROM stapproverlevel_master where isactive = 1 ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (stapproverlevel_master) </purpose>
    Public Function InsertAuditTrailForApproverLevel(ByVal objDoOperation As clsDataOperation, ByVal intAuditType As enAuditType) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDoOperation.ClearParameters()
            objDoOperation.AddParameter("@stlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStlevelunkid)
            objDoOperation.AddParameter("@stlevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrstLevelcode.ToString)
            objDoOperation.AddParameter("@stlevelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrstlevelname.ToString)
            objDoOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDoOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDoOperation.AddParameter("@stlevelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStlevelname1.ToString)
            objDoOperation.AddParameter("@stlevelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStlevelname2.ToString)
            objDoOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDoOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDoOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP.Trim))
            objDoOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName.Trim))
            objDoOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebFormName)
            objDoOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = " INSERT INTO atstapproverlevel_master ( " & _
                      "  stlevelunkid " & _
                      ", stlevelcode " & _
                      ", stlevelname " & _
                      ", priority " & _
                      ", isactive " & _
                      ", stlevelname1 " & _
                      ", stlevelname2" & _
                       ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name" & _
                      ", form_name " & _
                      ", isweb" & _
                      " ) VALUES (" & _
                      "  @stlevelunkid " & _
                      ", @stlevelcode " & _
                      ", @stlevelname " & _
                      ", @priority " & _
                      ", @isactive " & _
                      ", @stlevelname1 " & _
                      ", @stlevelname2" & _
                       ",@audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @machine_name" & _
                      ", @form_name " & _
                      ", @isweb" & _
                      " ); SELECT @@identity"

            dsList = objDoOperation.ExecQuery(strQ, "List")

            If objDoOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Language.setMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Language.setMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "Sorry, You cannot delete selected Approver Level. Reason: This Approver Level is in use.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class