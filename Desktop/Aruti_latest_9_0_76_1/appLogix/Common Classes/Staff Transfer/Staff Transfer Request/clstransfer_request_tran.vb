﻿'************************************************************************************************************************************
'Class Name : clstransfer_request_Tran.vb
'Purpose    :
'Date       :26-Jun-2023
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Web

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clstransfer_request_tran
    Private Shared ReadOnly mstrModuleName As String = "clstransfer_request_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintTransferrequestunkid As Integer
    Private mdtRequestdate As Date
    Private mintEmployeeunkid As Integer
    Private mintStationunkid As Integer
    Private mintDeptgroupunkid As Integer
    Private mintDepartmentunkid As Integer
    Private mintSectiongroupunkid As Integer
    Private mintSectionunkid As Integer
    Private mintUnitgroupunkid As Integer
    Private mintUnitunkid As Integer
    Private mintTeamunkid As Integer
    Private mintClassgroupunkid As Integer
    Private mintClassunkid As Integer
    Private mintJobgroupunkid As Integer
    Private mintJobunkid As Integer
    Private mintGradegroupunkid As Integer
    Private mintGradeunkid As Integer
    Private mintGradelevelunkid As Integer
    Private mintReasonunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mintStatusunkid As Integer
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mblnIsWeb As Boolean = False
#End Region

#Region "ENUM"

    Public Enum enTransferRequestStatus
        Pending = 1
        Approved = 2
        PushedBack = 3
    End Enum

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transferrequestunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Transferrequestunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintTransferrequestunkid
        End Get
        Set(ByVal value As Integer)
            mintTransferrequestunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set requestdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Requestdate() As Date
        Get
            Return mdtRequestdate
        End Get
        Set(ByVal value As Date)
            mdtRequestdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stationunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stationunkid() As Integer
        Get
            Return mintStationunkid
        End Get
        Set(ByVal value As Integer)
            mintStationunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deptgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Deptgroupunkid() As Integer
        Get
            Return mintDeptgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintDeptgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Departmentunkid() As Integer
        Get
            Return mintDepartmentunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectiongroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Sectiongroupunkid() As Integer
        Get
            Return mintSectiongroupunkid
        End Get
        Set(ByVal value As Integer)
            mintSectiongroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sectionunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Sectionunkid() As Integer
        Get
            Return mintSectionunkid
        End Get
        Set(ByVal value As Integer)
            mintSectionunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Unitgroupunkid() As Integer
        Get
            Return mintUnitgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintUnitgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unitunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Unitunkid() As Integer
        Get
            Return mintUnitunkid
        End Get
        Set(ByVal value As Integer)
            mintUnitunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set teamunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Teamunkid() As Integer
        Get
            Return mintTeamunkid
        End Get
        Set(ByVal value As Integer)
            mintTeamunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set classgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Classgroupunkid() As Integer
        Get
            Return mintClassgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintClassgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set classunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Classunkid() As Integer
        Get
            Return mintClassunkid
        End Get
        Set(ByVal value As Integer)
            mintClassunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Jobgroupunkid() As Integer
        Get
            Return mintJobgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintJobgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradegroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Gradegroupunkid() As Integer
        Get
            Return mintGradegroupunkid
        End Get
        Set(ByVal value As Integer)
            mintGradegroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Gradeunkid() As Integer
        Get
            Return mintGradeunkid
        End Get
        Set(ByVal value As Integer)
            mintGradeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradelevelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Gradelevelunkid() As Integer
        Get
            Return mintGradelevelunkid
        End Get
        Set(ByVal value As Integer)
            mintGradelevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reasonunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Reasonunkid() As Integer
        Get
            Return mintReasonunkid
        End Get
        Set(ByVal value As Integer)
            mintReasonunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webclientip
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webhostname
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  transferrequestunkid " & _
                      ", requestdate " & _
                      ", employeeunkid " & _
                      ", stationunkid " & _
                      ", deptgroupunkid " & _
                      ", departmentunkid " & _
                      ", sectiongroupunkid " & _
                      ", sectionunkid " & _
                      ", unitgroupunkid " & _
                      ", unitunkid " & _
                      ", teamunkid " & _
                      ", classgroupunkid " & _
                      ", classunkid " & _
                      ", jobgroupunkid " & _
                      ", jobunkid " & _
                      ", gradegroupunkid " & _
                      ", gradeunkid " & _
                      ", gradelevelunkid " & _
                      ", reasonunkid " & _
                      ", remark " & _
                      ", statusunkid " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      " FROM sttransfer_request_tran " & _
                      " WHERE transferrequestunkid = @transferrequestunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferrequestunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintTransferrequestunkid = CInt(dtRow.Item("transferrequestunkid"))
                mdtRequestdate = dtRow.Item("requestdate")
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintStationunkid = CInt(dtRow.Item("stationunkid"))
                mintDeptgroupunkid = CInt(dtRow.Item("deptgroupunkid"))
                mintDepartmentunkid = CInt(dtRow.Item("departmentunkid"))
                mintSectiongroupunkid = CInt(dtRow.Item("sectiongroupunkid"))
                mintSectionunkid = CInt(dtRow.Item("sectionunkid"))
                mintUnitgroupunkid = CInt(dtRow.Item("unitgroupunkid"))
                mintUnitunkid = CInt(dtRow.Item("unitunkid"))
                mintTeamunkid = CInt(dtRow.Item("teamunkid"))
                mintClassgroupunkid = CInt(dtRow.Item("classgroupunkid"))
                mintClassunkid = CInt(dtRow.Item("classunkid"))
                mintJobgroupunkid = CInt(dtRow.Item("jobgroupunkid"))
                mintJobunkid = CInt(dtRow.Item("jobunkid"))
                mintGradegroupunkid = CInt(dtRow.Item("gradegroupunkid"))
                mintGradeunkid = CInt(dtRow.Item("gradeunkid"))
                mintGradelevelunkid = CInt(dtRow.Item("gradelevelunkid"))
                mintReasonunkid = CInt(dtRow.Item("reasonunkid"))
                mstrRemark = dtRow.Item("remark").ToString
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False AndAlso dtRow.Item("voiddatetime") <> Nothing Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal objDoOperation As clsDataOperation = Nothing, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim objDataOperation As clsDataOperation
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  sttransfer_request_tran.transferrequestunkid " & _
                      ", sttransfer_request_tran.requestdate " & _
                      ", sttransfer_request_tran.employeeunkid " & _
                      ", ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') AS Employee" & _
                      ", sttransfer_request_tran.stationunkid " & _
                      ", ISNULL(hrstation_master.name,'') AS Branch" & _
                      ", sttransfer_request_tran.deptgroupunkid " & _
                      ", ISNULL(hrdepartment_group_master.name,'') AS DeptGrp" & _
                      ", sttransfer_request_tran.departmentunkid " & _
                      ", ISNULL(hrdepartment_master.name,'') AS Department" & _
                      ", sttransfer_request_tran.sectiongroupunkid " & _
                      ", ISNULL(hrsectiongroup_master.name,'') AS SectionGrp" & _
                      ", sttransfer_request_tran.sectionunkid " & _
                      ", ISNULL(hrsection_master.name,'') AS Section" & _
                      ", sttransfer_request_tran.unitgroupunkid " & _
                      ", ISNULL(hrunitgroup_master.name,'') AS UnitGroup" & _
                      ", sttransfer_request_tran.unitunkid " & _
                      ", ISNULL(hrunit_master.name,'') AS Unit" & _
                      ", sttransfer_request_tran.teamunkid " & _
                      ", ISNULL(hrteam_master.name,'') AS Team" & _
                      ", sttransfer_request_tran.classgroupunkid " & _
                      ", ISNULL(hrclassgroup_master.name,'') AS ClassGrp" & _
                      ", sttransfer_request_tran.classunkid " & _
                      ", ISNULL(hrclasses_master.name,'') AS Class" & _
                      ", sttransfer_request_tran.jobgroupunkid " & _
                      ", ISNULL(hrjobgroup_master.name,'') AS JobGrp" & _
                      ", sttransfer_request_tran.jobunkid " & _
                      ", ISNULL(hrjob_master.job_name,'') AS Job" & _
                      ", sttransfer_request_tran.gradegroupunkid " & _
                      ", sttransfer_request_tran.gradeunkid " & _
                      ", sttransfer_request_tran.gradelevelunkid " & _
                      ", sttransfer_request_tran.reasonunkid " & _
                      ", ISNULL(cfcommon_master.name,'') AS Reason " & _
                      ", sttransfer_request_tran.Remark " & _
                      ", sttransfer_request_tran.statusunkid " & _
                      ", CASE WHEN sttransfer_request_tran.statusunkid = 1 THEN @Pending " & _
                      "           WHEN sttransfer_request_tran.statusunkid = 2 THEN @Approved " & _
                      "           WHEN sttransfer_request_tran.statusunkid = 3 THEN @PushedBack " & _
                      " END Status " & _
                      ", sttransfer_request_tran.userunkid " & _
                      ", sttransfer_request_tran.loginemployeeunkid " & _
                      ", sttransfer_request_tran.isvoid " & _
                      ", sttransfer_request_tran.voiddatetime " & _
                      ", sttransfer_request_tran.voiduserunkid " & _
                      ", sttransfer_request_tran.voidreason " & _
                      " FROM sttransfer_request_tran " & _
                      " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = sttransfer_request_tran.employeeunkid " & _
                      " LEFT JOIN hrstation_master ON hrstation_master.stationunkid = sttransfer_request_tran.stationunkid " & _
                      " LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = sttransfer_request_tran.deptgroupunkid " & _
                      " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = sttransfer_request_tran.departmentunkid " & _
                      " LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = sttransfer_request_tran.sectiongroupunkid " & _
                      " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = sttransfer_request_tran.sectionunkid " & _
                      " LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = sttransfer_request_tran.unitgroupunkid " & _
                      " LEFT JOIN hrunit_master ON hrunit_master.unitunkid = sttransfer_request_tran.unitunkid " & _
                      " LEFT JOIN hrteam_master ON hrteam_master.teamunkid = sttransfer_request_tran.teamunkid " & _
                      " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = sttransfer_request_tran.jobgroupunkid " & _
                      " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = sttransfer_request_tran.jobunkid " & _
                      " LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = sttransfer_request_tran.classgroupunkid " & _
                      " LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = sttransfer_request_tran.classunkid " & _
                      " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = sttransfer_request_tran.reasonunkid AND cfcommon_master.mastertype = @mastertype  "

            If blnOnlyActive Then
                strQ &= " WHERE sttransfer_request_tran.isvoid = 0 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mastertype", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(clsCommon_Master.enCommonMaster.STAFF_TRANSFER))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Approved"))
            objDataOperation.AddParameter("@PushedBack", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Pushed Back"))
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (sttransfer_request_tran) </purpose>
    Public Function Insert(ByVal xCompanyUnkid As Integer, ByVal xDatabaseName As String, ByVal xYearUnkid As Integer _
                                 , Optional ByVal objDoOperation As clsDataOperation = Nothing, Optional ByVal mdtDocuments As DataTable = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objConfig As New clsConfigOptions
        Dim mstrUserAccessModeSetting As String = objConfig.GetKeyValue(xCompanyUnkid, "UserAccessModeSetting", Nothing)
        Dim mstrStaffTransferRequestAllocations As String = objConfig.GetKeyValue(xCompanyUnkid, "StaffTransferRequestAllocations", Nothing)
        objConfig = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@requestdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRequestdate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectiongroupunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If


            strQ = "INSERT INTO sttransfer_request_tran ( " & _
                      "  requestdate " & _
                      ", employeeunkid " & _
                      ", stationunkid " & _
                      ", deptgroupunkid " & _
                      ", departmentunkid " & _
                      ", sectiongroupunkid " & _
                      ", sectionunkid " & _
                      ", unitgroupunkid " & _
                      ", unitunkid " & _
                      ", teamunkid " & _
                      ", classgroupunkid " & _
                      ", classunkid " & _
                      ", jobgroupunkid " & _
                      ", jobunkid " & _
                      ", gradegroupunkid " & _
                      ", gradeunkid " & _
                      ", gradelevelunkid " & _
                      ", reasonunkid " & _
                      ", remark " & _
                      ", statusunkid " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @requestdate " & _
                      ", @employeeunkid " & _
                      ", @stationunkid " & _
                      ", @deptgroupunkid " & _
                      ", @departmentunkid " & _
                      ", @sectiongroupunkid " & _
                      ", @sectionunkid " & _
                      ", @unitgroupunkid " & _
                      ", @unitunkid " & _
                      ", @teamunkid " & _
                      ", @classgroupunkid " & _
                      ", @classunkid " & _
                      ", @jobgroupunkid " & _
                      ", @jobunkid " & _
                      ", @gradegroupunkid " & _
                      ", @gradeunkid " & _
                      ", @gradelevelunkid " & _
                      ", @reasonunkid " & _
                      ", @remark " & _
                      ", @statusunkid " & _
                      ", @userunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintTransferrequestunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailStaffTransferRequest(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtDocuments IsNot Nothing Then
                For Each drow As DataRow In mdtDocuments.Rows
                    drow("transactionunkid") = mintTransferrequestunkid
                    drow("userunkid") = mintUserunkid
                Next

                Dim objDocument As New clsScan_Attach_Documents
                objDocument._Datatable = mdtDocuments
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objDocument = Nothing
            End If


            If InsertTransferApprovalDetails(objDataOperation, xDatabaseName, xCompanyUnkid, xYearUnkid, mstrUserAccessModeSetting, mstrStaffTransferRequestAllocations) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (sttransfer_request_tran) </purpose>
    Public Function Update(ByVal xDatabaseName As String, ByVal xYearId As Integer, ByVal xCompanyId As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing _
                                            , Optional ByVal mdtDocuments As DataTable = Nothing, Optional ByVal mblnEditFromESS As Boolean = True) As Boolean
        'If isExist(mstrName, mintTransferrequestunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mblnIncludeInactiveEmployee As Boolean = False

        Dim objConfig As New clsConfigOptions
        Dim mstrUserAccessModeSetting As String = objConfig.GetKeyValue(xCompanyId, "UserAccessModeSetting", Nothing)
        Dim mstrStaffTransferRequestAllocations As String = objConfig.GetKeyValue(xCompanyId, "StaffTransferRequestAllocations", Nothing)
        Dim mstrIncludeInactiveEmployee As String = objConfig.GetKeyValue(xCompanyId, "IncludeInactiveEmployee", Nothing)
        If mstrIncludeInactiveEmployee.Trim.Length > 0 Then mblnIncludeInactiveEmployee = CBool(mstrIncludeInactiveEmployee)
        objConfig = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferrequestunkid.ToString)
            objDataOperation.AddParameter("@requestdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRequestdate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectiongroupunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            strQ = "UPDATE sttransfer_request_tran SET " & _
                      "  requestdate = @requestdate" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", stationunkid = @stationunkid" & _
                      ", deptgroupunkid = @deptgroupunkid" & _
                      ", departmentunkid = @departmentunkid" & _
                      ", sectiongroupunkid = @sectiongroupunkid" & _
                      ", sectionunkid = @sectionunkid" & _
                      ", unitgroupunkid = @unitgroupunkid" & _
                      ", unitunkid = @unitunkid" & _
                      ", teamunkid = @teamunkid" & _
                      ", classgroupunkid = @classgroupunkid" & _
                      ", classunkid = @classunkid" & _
                      ", jobgroupunkid = @jobgroupunkid" & _
                      ", jobunkid = @jobunkid" & _
                      ", gradegroupunkid = @gradegroupunkid" & _
                      ", gradeunkid = @gradeunkid" & _
                      ", gradelevelunkid = @gradelevelunkid" & _
                      ", reasonunkid = @reasonunkid" & _
                      ", remark = @remark" & _
                      ", statusunkid = @statusunkid" & _
                      ", userunkid = @userunkid" & _
                      ", loginemployeeunkid = @loginemployeeunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      " WHERE isvoid = 0 AND  transferrequestunkid = @transferrequestunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailStaffTransferRequest(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtDocuments IsNot Nothing Then
                For Each drow As DataRow In mdtDocuments.Rows
                    drow("transactionunkid") = mintTransferrequestunkid
                    drow("userunkid") = mintUserunkid
                Next

                Dim objDocument As New clsScan_Attach_Documents
                objDocument._Datatable = mdtDocuments
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objDocument = Nothing
            End If

            If mblnEditFromESS Then

                Dim objTransferApproval As New clssttransfer_approval_Tran
                Dim dsAppovalList As DataSet = objTransferApproval.GetTrasferApprovalList(xDatabaseName, xCompanyId, mdtRequestdate, mdtRequestdate, mstrUserAccessModeSetting, True, mblnIncludeInactiveEmployee, "List", "sta.transferrequestunkid = " & mintTransferrequestunkid, False, objDataOperation)
                If dsAppovalList IsNot Nothing AndAlso dsAppovalList.Tables(0).Rows.Count > 0 Then

                    For Each dr As DataRow In dsAppovalList.Tables(0).Rows

                        objTransferApproval._Userunkid = mintUserunkid
                        objTransferApproval._ClientIP = mstrWebClientIP
                        objTransferApproval._HostName = mstrWebHostName
                        objTransferApproval._FormName = mstrWebFormName
                        objTransferApproval._IsWeb = mblnIsWeb

                        If objTransferApproval.Delete(CInt(dr("transferapprovalunkid")), objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                End If
                If dsAppovalList IsNot Nothing Then dsAppovalList.Clear()
                dsAppovalList = Nothing
                objTransferApproval = Nothing

                If InsertTransferApprovalDetails(objDataOperation, xDatabaseName, xCompanyId, xYearId, mstrUserAccessModeSetting, mstrStaffTransferRequestAllocations) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objTransferApproval = Nothing

            End If

            'objTransferApproval._Transferrequestunkid = mintTransferrequestunkid
            'objTransferApproval._Employeeunkid = mintEmployeeunkid
            'objTransferApproval._Stationunkid = mintStationunkid
            'objTransferApproval._Deptgroupunkid = mintDeptgroupunkid
            'objTransferApproval._Departmentunkid = mintDepartmentunkid
            'objTransferApproval._Sectiongroupunkid = mintSectiongroupunkid
            'objTransferApproval._Sectionunkid = mintSectionunkid
            'objTransferApproval._Unitgroupunkid = mintUnitgroupunkid
            'objTransferApproval._Unitunkid = mintUnitunkid
            'objTransferApproval._Teamunkid = mintTeamunkid
            'objTransferApproval._Jobgroupunkid = mintJobgroupunkid
            'objTransferApproval._Jobunkid = mintJobunkid
            'objTransferApproval._Classgroupunkid = mintClassgroupunkid
            'objTransferApproval._Classunkid = mintClassunkid
            'objTransferApproval._Gradegroupunkid = mintGradegroupunkid
            'objTransferApproval._Gradeunkid = mintGradeunkid
            'objTransferApproval._Gradelevelunkid = mintGradelevelunkid
            'objTransferApproval._Reasonunkid = mintReasonunkid


            'objTransferApproval._Isvoid = False
            'objTransferApproval._Voiddatetime = mdtVoiddatetime
            'objTransferApproval._Voiduserunkid = mintVoiduserunkid
            'objTransferApproval._Voidreason = mstrVoidreason
            'objTransferApproval._FormName = mstrWebFormName
            'objTransferApproval._AuditUserId = mintUserunkid
            'objTransferApproval._ClientIP = mstrWebClientIP
            'objTransferApproval._HostName = mstrWebHostName
            'objTransferApproval._IsWeb = mblnIsWeb

            'If objTransferApproval.UpdateTransferApproval(objDataOperation) = False Then
            '    objTransferApproval = Nothing
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If


            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (sttransfer_request_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing _
                                  , Optional ByVal mdtDocuments As DataTable = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try

            strQ = "SELECT ISNULL(transferapprovalunkid,0) AS transferapprovalunkid FROM sttransfer_approval_tran  WHERE transferrequestunkid = @transferrequestunkid AND isvoid =0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim objAppoval As New clssttransfer_approval_Tran
                For Each dr As DataRow In dsList.Tables(0).Rows
                    objAppoval._ClientIP = mstrWebClientIP
                    objAppoval._HostName = mstrWebHostName
                    objAppoval._FormName = mstrWebFormName
                    objAppoval._IsWeb = mblnIsWeb
                    objAppoval._Voidreason = mstrVoidreason
                    If objAppoval.Delete(CInt(dr("transferapprovalunkid")), objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
                objAppoval = Nothing
            End If

            strQ = " UPDATE sttransfer_request_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voidreason = @voidreason " & _
                     " WHERE transferrequestunkid = @transferrequestunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ClearParameters()
            _Transferrequestunkid(objDataOperation) = intUnkid

            If InsertAuditTrailStaffTransferRequest(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtDocuments IsNot Nothing AndAlso mdtDocuments.Rows.Count > 0 Then
                Dim objDocument As New clsScan_Attach_Documents
                For Each iRow As DataRow In mdtDocuments.Rows
                    iRow("AUD") = "D"
                Next
                objDocument._Datatable = mdtDocuments
                objDocument.InsertUpdateDelete_Documents(objDataOperation)
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objDocument = Nothing
            End If


            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  transferrequestunkid " & _
              ", requestdate " & _
              ", employeeunkid " & _
              ", stationunkid " & _
              ", deptgroupunkid " & _
              ", departmentunkid " & _
              ", sectiongroupunkid " & _
              ", sectionunkid " & _
              ", unitgroupunkid " & _
              ", unitunkid " & _
              ", teamunkid " & _
              ", classgroupunkid " & _
              ", classunkid " & _
              ", jobgroupunkid " & _
              ", jobunkid " & _
              ", gradegroupunkid " & _
              ", gradeunkid " & _
              ", gradelevelunkid " & _
              ", reasonunkid " & _
              ", remark " & _
              ", statusunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             "FROM sttransfer_request_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND transferrequestunkid <> @transferrequestunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (sttransfer_request_tran) </purpose>
    Public Function InsertAuditTrailStaffTransferRequest(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            strQ = "INSERT INTO atsttransfer_request_tran ( " & _
                      "  transferrequestunkid " & _
                      ", requestdate " & _
                      ", employeeunkid " & _
                      ", stationunkid " & _
                      ", deptgroupunkid " & _
                      ", departmentunkid " & _
                      ", sectiongroupunkid " & _
                      ", sectionunkid " & _
                      ", unitgroupunkid " & _
                      ", unitunkid " & _
                      ", teamunkid " & _
                      ", classgroupunkid " & _
                      ", classunkid " & _
                      ", jobgroupunkid " & _
                      ", jobunkid " & _
                      ", gradegroupunkid " & _
                      ", gradeunkid " & _
                      ", gradelevelunkid " & _
                      ", reasonunkid " & _
                      ", remark " & _
                      ", statusunkid " & _
                      ", loginemployeeunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name" & _
                      ", form_name " & _
                      ", isweb" & _
                    ") VALUES (" & _
                      "  @transferrequestunkid " & _
                      ", @requestdate " & _
                      ", @employeeunkid " & _
                      ", @stationunkid " & _
                      ", @deptgroupunkid " & _
                      ", @departmentunkid " & _
                      ", @sectiongroupunkid " & _
                      ", @sectionunkid " & _
                      ", @unitgroupunkid " & _
                      ", @unitunkid " & _
                      ", @teamunkid " & _
                      ", @classgroupunkid " & _
                      ", @classunkid " & _
                      ", @jobgroupunkid " & _
                      ", @jobunkid " & _
                      ", @gradegroupunkid " & _
                      ", @gradeunkid " & _
                      ", @gradelevelunkid " & _
                      ", @reasonunkid " & _
                      ", @remark " & _
                      ", @statusunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @machine_name" & _
                      ", @form_name " & _
                      ", @isweb" & _
                    "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@transferrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferrequestunkid)
            objDataOperation.AddParameter("@requestdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRequestdate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@deptgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeptgroupunkid.ToString)
            objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentunkid.ToString)
            objDataOperation.AddParameter("@sectiongroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectiongroupunkid.ToString)
            objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSectionunkid.ToString)
            objDataOperation.AddParameter("@unitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitgroupunkid.ToString)
            objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUnitunkid.ToString)
            objDataOperation.AddParameter("@teamunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTeamunkid.ToString)
            objDataOperation.AddParameter("@classgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassgroupunkid.ToString)
            objDataOperation.AddParameter("@classunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintClassunkid.ToString)
            objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobgroupunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@reasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReasonunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP.Trim))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName.Trim))
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailStaffTransferRequest; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (sttransfer_request_tran) </purpose>
    ''' 
    Public Function GetTransferRequest_Status(Optional ByVal strListName As String = "List", Optional ByVal blnFlag As Boolean = False, Optional ByVal blnIncludePushedBack As Boolean = False) As DataSet
        Dim strQ As String = String.Empty
        Dim objDataOperation As clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try


            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            If blnFlag = True Then
                strQ = "SELECT 0 AS Id,@Select AS NAME UNION  "
            End If

            strQ &= " SELECT " & enTransferRequestStatus.Pending & " AS Id,@Pending AS NAME " & _
                         " UNION SELECT " & enTransferRequestStatus.Approved & " AS Id,@Approved AS NAME "

            If blnIncludePushedBack Then
                strQ &= " UNION SELECT " & enTransferRequestStatus.PushedBack & " AS Id,@PushedBack AS NAME "
                objDataOperation.AddParameter("@PushedBack", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Pushed Back"))
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Approved"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTransferRequest_Status; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (sttransfer_request_tran) </purpose>
    ''' 
    Private Function InsertTransferApprovalDetails(ByVal objDataOperation As clsDataOperation, ByVal xDatabaseName As String, ByVal xCompanyUnkid As Integer, ByVal xYearUnkid As Integer, ByVal mstrUserAccessModeSetting As String, ByVal mstrStaffTransferRequestAllocations As String) As Boolean
        Dim mblnFlag As Boolean = True
        Dim exForce As Exception
        Try
            Dim objLevelRoleMapping As New clsstapproverlevel_role_mapping
            Dim dsRoleMapping As DataSet = objLevelRoleMapping.GetList("List", True, objDataOperation, "stapproverlevel_role_mapping.isuseraccess = 0")

            Dim dtNoUserAccess As DataTable = Nothing
            If dsRoleMapping IsNot Nothing AndAlso dsRoleMapping.Tables(0).Rows.Count > 0 Then

                Dim mdctStaffAllocations As New Dictionary(Of Integer, Integer)
                Dim ar() As String = mstrStaffTransferRequestAllocations.Split(CChar("|"))

                If ar IsNot Nothing AndAlso ar.Length > 0 Then
                    For i As Integer = 0 To ar.Length - 1

                        Select Case CInt(ar(i))

                            Case enAllocation.BRANCH
                                If mintStationunkid > 0 AndAlso mdctStaffAllocations.ContainsKey(enAllocation.BRANCH) = False Then
                                    mdctStaffAllocations.Add(enAllocation.BRANCH, mintStationunkid)
                                End If

                            Case enAllocation.DEPARTMENT_GROUP
                                If mintDeptgroupunkid > 0 AndAlso mdctStaffAllocations.ContainsKey(enAllocation.DEPARTMENT_GROUP) = False Then
                                    mdctStaffAllocations.Add(enAllocation.DEPARTMENT_GROUP, mintDeptgroupunkid)
                                End If

                            Case enAllocation.DEPARTMENT
                                If mintDepartmentunkid > 0 AndAlso mdctStaffAllocations.ContainsKey(enAllocation.DEPARTMENT) = False Then
                                    mdctStaffAllocations.Add(enAllocation.DEPARTMENT, mintDepartmentunkid)
                                End If

                            Case enAllocation.SECTION_GROUP
                                If mintSectiongroupunkid > 0 AndAlso mdctStaffAllocations.ContainsKey(enAllocation.SECTION_GROUP) = False Then
                                    mdctStaffAllocations.Add(enAllocation.SECTION_GROUP, mintSectiongroupunkid)
                                End If

                            Case enAllocation.SECTION
                                If mintSectionunkid > 0 AndAlso mdctStaffAllocations.ContainsKey(enAllocation.SECTION) = False Then
                                    mdctStaffAllocations.Add(enAllocation.SECTION, mintSectionunkid)
                                End If

                            Case enAllocation.UNIT_GROUP
                                If mintUnitgroupunkid > 0 AndAlso mdctStaffAllocations.ContainsKey(enAllocation.UNIT_GROUP) = False Then
                                    mdctStaffAllocations.Add(enAllocation.UNIT_GROUP, mintUnitgroupunkid)
                                End If

                            Case enAllocation.UNIT
                                If mintUnitunkid > 0 AndAlso mdctStaffAllocations.ContainsKey(enAllocation.UNIT) = False Then
                                    mdctStaffAllocations.Add(enAllocation.UNIT, mintUnitunkid)
                                End If

                            Case enAllocation.TEAM
                                If mintTeamunkid > 0 AndAlso mdctStaffAllocations.ContainsKey(enAllocation.TEAM) = False Then
                                    mdctStaffAllocations.Add(enAllocation.TEAM, mintTeamunkid)
                                End If

                            Case enAllocation.JOB_GROUP
                                If mintJobgroupunkid > 0 AndAlso mdctStaffAllocations.ContainsKey(enAllocation.JOB_GROUP) = False Then
                                    mdctStaffAllocations.Add(enAllocation.JOB_GROUP, mintJobgroupunkid)
                                End If

                            Case enAllocation.JOBS
                                If mintJobunkid > 0 AndAlso mdctStaffAllocations.ContainsKey(enAllocation.JOBS) = False Then
                                    mdctStaffAllocations.Add(enAllocation.JOBS, mintJobunkid)
                                End If

                            Case enAllocation.CLASS_GROUP
                                If mintClassgroupunkid > 0 AndAlso mdctStaffAllocations.ContainsKey(enAllocation.CLASS_GROUP) = False Then
                                    mdctStaffAllocations.Add(enAllocation.CLASS_GROUP, mintClassgroupunkid)
                                End If

                            Case enAllocation.CLASSES
                                If mintClassunkid > 0 AndAlso mdctStaffAllocations.ContainsKey(enAllocation.CLASSES) = False Then
                                    mdctStaffAllocations.Add(enAllocation.CLASSES, mintClassunkid)
                                End If

                        End Select
                    Next

                End If
                dtNoUserAccess = objLevelRoleMapping.GetNextApprovers(xDatabaseName, xCompanyUnkid, xYearUnkid, mstrUserAccessModeSetting, enUserPriviledge.AllowToApproveStaffTransferApproval, mdtRequestdate, mintUserunkid, mintEmployeeunkid, False, objDataOperation, "", mdctStaffAllocations)
            End If

            Dim dtTable As DataTable = objLevelRoleMapping.GetNextApprovers(xDatabaseName, xCompanyUnkid, xYearUnkid, mstrUserAccessModeSetting, enUserPriviledge.AllowToApproveStaffTransferApproval, mdtRequestdate, mintUserunkid, mintEmployeeunkid, True, objDataOperation, "", Nothing)
            objLevelRoleMapping = Nothing

            If dtNoUserAccess IsNot Nothing AndAlso dtNoUserAccess.Rows.Count > 0 Then
                dtTable.Merge(dtNoUserAccess)
                dtTable = New DataView(dtTable, "", "priority", DataViewRowState.CurrentRows).ToTable()
            End If

            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim objTransferApproval As New clssttransfer_approval_Tran

                Dim intMinPriority As Integer = CInt(dtTable.Compute("MIN(priority)", "1=1"))

                For Each drRow As DataRow In dtTable.Rows
                    objTransferApproval._Transferrequestunkid = mintTransferrequestunkid
                    objTransferApproval._Employeeunkid = mintEmployeeunkid
                    objTransferApproval._Stmappingunkid = CInt(drRow("mapuserunkid"))
                    objTransferApproval._Roleunkid = CInt(drRow("roleunkid"))
                    objTransferApproval._Levelunkid = CInt(drRow("levelunkid"))
                    objTransferApproval._Priority = CInt(drRow("priority"))
                    objTransferApproval._Stmappingunkid = CInt(drRow("stmappingunkid"))
                    objTransferApproval._Mapuserunkid = CInt(drRow("mapuserunkid"))
                    objTransferApproval._Approvaldate = Nothing
                    objTransferApproval._Stationunkid = mintStationunkid
                    objTransferApproval._Deptgroupunkid = mintDeptgroupunkid
                    objTransferApproval._Departmentunkid = mintDepartmentunkid
                    objTransferApproval._Sectiongroupunkid = mintSectiongroupunkid
                    objTransferApproval._Sectionunkid = mintSectionunkid
                    objTransferApproval._Unitgroupunkid = mintUnitgroupunkid
                    objTransferApproval._Unitunkid = mintUnitunkid
                    objTransferApproval._Teamunkid = mintTeamunkid
                    objTransferApproval._Jobgroupunkid = mintJobgroupunkid
                    objTransferApproval._Jobunkid = mintJobunkid
                    objTransferApproval._Classgroupunkid = mintClassgroupunkid
                    objTransferApproval._Classunkid = mintClassunkid
                    objTransferApproval._Gradegroupunkid = mintGradegroupunkid
                    objTransferApproval._Gradeunkid = mintGradeunkid
                    objTransferApproval._Gradelevelunkid = mintGradelevelunkid
                    objTransferApproval._Reasonunkid = mintReasonunkid

                    If intMinPriority = CInt(drRow("priority")) Then
                        objTransferApproval._Statusunkid = clstransfer_request_tran.enTransferRequestStatus.Pending
                        objTransferApproval._Visibleunkid = clstransfer_request_tran.enTransferRequestStatus.Pending
                    Else
                        objTransferApproval._Statusunkid = clstransfer_request_tran.enTransferRequestStatus.Pending
                        objTransferApproval._Visibleunkid = -1
                    End If

                    objTransferApproval._Isvoid = False
                    objTransferApproval._Voiddatetime = mdtVoiddatetime
                    objTransferApproval._Voiduserunkid = mintVoiduserunkid
                    objTransferApproval._Voidreason = mstrVoidreason
                    objTransferApproval._FormName = mstrWebFormName
                    objTransferApproval._ClientIP = mstrWebClientIP
                    objTransferApproval._HostName = mstrWebHostName
                    objTransferApproval._IsWeb = mblnIsWeb

                    If objTransferApproval.Insert(objDataOperation) = False Then
                        objTransferApproval = Nothing
                        mblnFlag = False
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next
                objTransferApproval = Nothing
            End If
        Catch ex As Exception
            mblnFlag = False
            Throw New Exception(ex.Message & "; Procedure Name: InsertTransferApprovalDetails; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (sttransfer_request_tran) </purpose>
    ''' 
    Public Sub Send_NotificationEmployee(ByVal iEmployeeId As Integer, _
                                                           ByVal dtEmployeeAsOnDate As Date, _
                                                           ByVal intCompanyUnkId As Integer, _
                                                           ByVal xApplicationStatus As Integer, _
                                                           Optional ByVal strRemark As String = "", _
                                                           Optional ByVal iLoginTypeId As Integer = 0, _
                                                           Optional ByVal iLoginEmployeeId As Integer = 0, _
                                                           Optional ByVal iUserId As Integer = 0)

        Dim dtApprover As DataTable = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub
            Dim objEmp As New clsEmployee_Master

            objEmp._Employeeunkid(dtEmployeeAsOnDate) = iEmployeeId

            Dim objMail As New clsSendMail

            If objEmp._Email = "" Then Exit Sub

            Dim strMessage As String = ""
            Dim strSubject As String = ""


            If xApplicationStatus = clstransfer_request_tran.enTransferRequestStatus.Pending Then
                strSubject = Language.getMessage(mstrModuleName, 5, "Staff Transfer Request Submission")
            ElseIf xApplicationStatus = clstransfer_request_tran.enTransferRequestStatus.Approved Then
                strSubject = Language.getMessage(mstrModuleName, 6, "Staff Transfer Request Approval")
            ElseIf xApplicationStatus = clstransfer_request_tran.enTransferRequestStatus.PushedBack Then
                strSubject = Language.getMessage(mstrModuleName, 7, "Staff Transfer Request Pushed back")
            End If

            strMessage = "<HTML> <BODY>"

            Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
            strMessage &= Language.getMessage(mstrModuleName, 8, "Dear") & " " & info1.ToTitleCase(objEmp._Firstname & "  " & objEmp._Surname) & ", <BR><BR>"
            info1 = Nothing

            Select Case xApplicationStatus
                Case clstransfer_request_tran.enTransferRequestStatus.Pending
                    strMessage &= Language.getMessage(mstrModuleName, 9, "Your self-initiated transfer request has been submitted successfully.")
                Case clstransfer_request_tran.enTransferRequestStatus.Approved
                    strMessage &= Language.getMessage(mstrModuleName, 11, "Your self-initiated transfer request has been finally approved.")
                Case clstransfer_request_tran.enTransferRequestStatus.PushedBack
                    strMessage &= Language.getMessage(mstrModuleName, 17, "This is to notify you that, your self initiated transfer request has been pushed back with the below remarks.") & "<BR></BR><b>" & strRemark & "</b>"
            End Select

            strMessage &= "<BR></BR><BR></BR>"
            strMessage &= Language.getMessage(mstrModuleName, 10, "Regards.")

            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"


            objMail._Subject = strSubject
            objMail._Message = strMessage
            objMail._ToEmail = objEmp._Email
            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrWebFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrWebFormName
            End If
            objMail._LogEmployeeUnkid = iLoginEmployeeId
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
            objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
            objMail.SendMail(intCompanyUnkId)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_NotificationEmployee; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (sttransfer_request_tran) </purpose>
    ''' 
    Public Sub Send_NotificationApprover(ByVal xDatabaseName As String, _
                                                         ByVal xCompanyUnkid As Integer, _
                                                         ByVal xYearId As Integer, _
                                                         ByVal mdtRequestDate As Date, _
                                                         ByVal xApplicationStatus As Integer, _
                                                         ByVal xUserAccessMode As String, _
                                                         ByVal iEmployeeId As Integer, _
                                                         ByVal strUnkId As String, _
                                                         ByVal strArutiSelfServiceURL As String, _
                                                         Optional ByVal xPriority As Integer = -1, _
                                                         Optional ByVal strRemark As String = "", _
                                                         Optional ByVal iLoginTypeId As Integer = 0, _
                                                         Optional ByVal iLoginEmployeeId As Integer = 0, _
                                                         Optional ByVal iUserId As Integer = 0)

        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        objDataOperation = New clsDataOperation
        Try
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

            Dim objApproval As New clssttransfer_approval_Tran
            Dim mstrSearch As String = "st.employeeunkid = " & iEmployeeId & " AND st.transferrequestunkid in (" & strUnkId & ") AND st.statusunkid = " & clstransfer_request_tran.enTransferRequestStatus.Pending & " AND sta.statusunkid = " & clstransfer_request_tran.enTransferRequestStatus.Pending & " AND sta.visibleunkid = " & clstransfer_request_tran.enTransferRequestStatus.Pending

            If xApplicationStatus = clstransfer_request_tran.enTransferRequestStatus.PushedBack AndAlso xPriority > -1 Then
                mstrSearch &= " AND sta.priority < " & xPriority
            End If

            Dim dsList As DataSet = objApproval.GetTrasferApprovalList(xDatabaseName, xCompanyUnkid, mdtRequestDate, mdtRequestDate, xUserAccessMode, True, False, "List", mstrSearch, False, Nothing)
            objApproval = Nothing

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then Exit Sub

            Dim dtTable As DataTable = Nothing
            If xApplicationStatus = clstransfer_request_tran.enTransferRequestStatus.PushedBack AndAlso xPriority > -1 Then
                dtTable = New DataView(dsList.Tables(0), "priority = MAX(priority)", "", DataViewRowState.CurrentRows).ToTable()
            Else
                dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable()
            End If

            Dim strSubject As String = ""

            If xApplicationStatus = clstransfer_request_tran.enTransferRequestStatus.PushedBack AndAlso xPriority > -1 Then
                strSubject = Language.getMessage(mstrModuleName, 16, "Staff Transfer Request Pushed Back")
            ElseIf xApplicationStatus = clstransfer_request_tran.enTransferRequestStatus.Pending Then
                strSubject = Language.getMessage(mstrModuleName, 12, "Staff Transfer Request Review/Decision")
            End If


            Dim objMail As New clsSendMail
            For Each dtRow As DataRow In dtTable.Rows

                If dtRow.Item("UserEmail") = "" Then Continue For
                Dim strMessage As String = ""
                Dim strContain As String = ""

                strLink = strArutiSelfServiceURL & "/Staff_Transfer/Transfer_Approval/wPgTransferApproval.aspx?" & HttpUtility.UrlEncode(clsCrypto.Encrypt(dtRow.Item("transferapprovalunkid").ToString & "|" & dtRow.Item("mapuserunkid").ToString & "|" & dtRow.Item("transferrequestunkid").ToString & "|" & iEmployeeId.ToString() & "|" & xCompanyUnkid.ToString()))

                strMessage = "<HTML> <BODY>"

                Dim info1 As System.Globalization.TextInfo = System.Globalization.CultureInfo.InvariantCulture.TextInfo
                strMessage &= Language.getMessage(mstrModuleName, 8, "Dear") & " " & info1.ToTitleCase(CStr(IIf(dtRow.Item("UserName").ToString().Trim.Length > 0, dtRow.Item("UserName").ToString().Trim, dtRow.Item("loginuser").ToString()))) & ", <BR><BR>"
                info1 = Nothing

                If xApplicationStatus = clstransfer_request_tran.enTransferRequestStatus.Pending Then
                    strMessage &= Language.getMessage(mstrModuleName, 13, "A self-initiated transfer request I.F.O") & " " & "<b>(" & dtRow("Employee").ToString() & ")</b>" & " " & Language.getMessage(mstrModuleName, 14, "has been submitted for your approval")
                    If strRemark.Trim.Length > 0 Then
                        strMessage &= " " & Language.getMessage(mstrModuleName, 20, "with the below remarks.") & "<BR></BR><b>" & strRemark & "</b>"
                    Else
                        strMessage &= "."
                    End If
                    strMessage &= "<BR></BR><BR></BR>"
                    strMessage &= Language.getMessage(mstrModuleName, 15, "Please click on the link below for the approve/decline.") & " <BR></BR><a href='" & strLink & "'>" & strLink & "</a>"
                    strMessage &= "<BR></BR><BR></BR>"
                ElseIf xApplicationStatus = clstransfer_request_tran.enTransferRequestStatus.PushedBack Then
                    strMessage &= Language.getMessage(mstrModuleName, 18, "This is to notify you that, self initiated transfer request I.F.O") & " " & "<b>(" & dtRow("Employee").ToString() & ")</b>" & " " & Language.getMessage(mstrModuleName, 19, "has been pushed back") & " " & Language.getMessage(mstrModuleName, 20, "with the below remarks.") & "<BR></BR><b>" & strRemark & "</b>"
                    strMessage &= "<BR></BR><BR></BR>"
                    strMessage &= Language.getMessage(mstrModuleName, 15, "Please click on the link below for the approve/decline.") & " <BR></BR><a href='" & strLink & "'>" & strLink & "</a>"
                    strMessage &= "<BR></BR><BR></BR>"
                End If

                strMessage &= Language.getMessage(mstrModuleName, 10, "Regards.")

                strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"
                strMessage &= "</BODY></HTML>"

                objMail._Subject = strSubject
                objMail._Message = strMessage
                objMail._ToEmail = dtRow.Item("UserEmail")

                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrWebFormName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrWebFormName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = IIf(iUserId <= 0, User._Object._Userunkid, iUserId)
                objMail._SenderAddress = CStr(IIf(dtRow.Item("UserName").ToString().Trim.Length > 0, dtRow.Item("UserName").ToString().Trim, dtRow.Item("loginuser").ToString()))
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                objMail.SendMail(xCompanyUnkid)

            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_NotificationApprover; Module Name: " & mstrModuleName)
        Finally

        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function getListForRequestDate(ByVal iEmpId As Integer, _
                                                            Optional ByVal iStatusId As Integer = 0, _
                                                            Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as transferrequestunkid, ' ' +  @name as name  UNION "
            End If

            strQ &= " SELECT DISTINCT sttransfer_request_tran.transferrequestunkid, CONVERT(CHAR(8),sttransfer_request_tran.requestdate,112) AS name " & _
                        " FROM sttransfer_request_tran  " & _
                        " WHERE sttransfer_request_tran.isvoid = 0 "

            If iStatusId > 0 Then
                strQ &= " AND sttransfer_request_tran.statusunkid = @statusunkid "
            End If

            If iEmpId > 0 Then
                strQ &= " AND sttransfer_request_tran.employeeunkid = '" & iEmpId & "'"
            End If

            strQ &= " ORDER BY name "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iStatusId)
            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dr As DataRow In dsList.Tables(0).Rows
                If CInt(dr("transferrequestunkid")) <= 0 Then Continue For
                dr("name") = eZeeDate.convertDate(dr("name").ToString()).ToShortDateString()
            Next

            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForRequestDate", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Pending")
            Language.setMessage(mstrModuleName, 3, "Approved")
            Language.setMessage(mstrModuleName, 4, "Pushed Back")
            Language.setMessage(mstrModuleName, 5, "Staff Transfer Request Submission")
            Language.setMessage(mstrModuleName, 6, "Staff Transfer Request Approval")
            Language.setMessage(mstrModuleName, 7, "Staff Transfer Request Pushed back")
            Language.setMessage(mstrModuleName, 8, "Dear")
            Language.setMessage(mstrModuleName, 9, "Your self-initiated transfer request has been submitted successfully.")
            Language.setMessage(mstrModuleName, 10, "Regards.")
            Language.setMessage(mstrModuleName, 11, "Your self-initiated transfer request has been finally approved.")
            Language.setMessage(mstrModuleName, 12, "Staff Transfer Request Review/Decision")
            Language.setMessage(mstrModuleName, 13, "A self-initiated transfer request I.F.O")
            Language.setMessage(mstrModuleName, 14, "has been submitted for your approval")
            Language.setMessage(mstrModuleName, 15, "Please click on the link below for the approve/decline.")
            Language.setMessage(mstrModuleName, 16, "Staff Transfer Request Pushed Back")
            Language.setMessage(mstrModuleName, 17, "This is to notify you that, your self initiated transfer request has been pushed back with the below remarks.")
            Language.setMessage(mstrModuleName, 18, "This is to notify you that, self initiated transfer request I.F.O")
            Language.setMessage(mstrModuleName, 19, "has been pushed back")
            Language.setMessage(mstrModuleName, 20, "with the below remarks.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class