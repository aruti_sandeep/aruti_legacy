﻿'************************************************************************************************************************************
'Class Name : clsaccountconfig_employee.vb
'Purpose    :
'Date       :06-08-2011
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib

Public Class clsaccountconfig_employee
    Private Shared ReadOnly mstrModuleName As String = "clsaccountconfig_employee"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAccountconfigEmpunkid As Integer
    Private mintTranheadunkid As Integer
    Private mintEmployeeunkid As Integer = 0
    Private mintAccountunkid As Integer
    Private mblnIsactive As Boolean = True
    Private mintTransactiontype_Id As Integer
    'Sohail (14 Nov 2011) -- Start
    Private mintReferencecodeid As Integer
    Private mintReferencenameid As Integer
    Private mintReferencetypeid As Integer = -1
    'Sohail (14 Nov 2011) -- End
    Private mstrShortname As String = String.Empty 'Sohail (13 Mar 2013)
    Private mstrShortname2 As String = String.Empty 'Sohail (08 Jul 2017)
    Private mstrShortname3 As String = String.Empty 'Sohail (03 Mar 2020)

    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Private mblnIsInactive As Boolean = False
    'Gajanan [24-Aug-2020] -- End
    'Hemant (22 Dec 2023) -- Start
    'ENHANCEMENT(TADB): A1X-1607 - Oracle CBS JV enhancement to split payable amounts for contributions to different GL accounts
    Private mdblPercentage As Double
    'Hemant (22 Dec 2023) -- End
    'Hemant (02 Feb 2024) -- Start
    'ENHANCEMENT(KCMUCO): A1X-2429 - Sage JV modification
    Private mintOppositeAccountunkid As Integer
    'Hemant (02 Feb 2024) -- End
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accountconfigempunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _AccountconfigEmpunkid() As Integer
        Get
            Return mintAccountconfigEmpunkid
        End Get
        Set(ByVal value As Integer)
            mintAccountconfigEmpunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accountunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Accountunkid() As Integer
        Get
            Return mintAccountunkid
        End Get
        Set(ByVal value As Integer)
            mintAccountunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiontype_Id
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Transactiontype_Id() As Integer
        Get
            Return mintTransactiontype_Id
        End Get
        Set(ByVal value As Integer)
            mintTransactiontype_Id = value
        End Set
    End Property

    'Sohail (14 Nov 2011) -- Start
    ''' <summary>
    ''' Purpose: Get or Set referencecodeid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Referencecodeid() As Integer
        Get
            Return mintReferencecodeid
        End Get
        Set(ByVal value As Integer)
            mintReferencecodeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referencenameid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Referencenameid() As Integer
        Get
            Return mintReferencenameid
        End Get
        Set(ByVal value As Integer)
            mintReferencenameid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referencetypeid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Referencetypeid() As Integer
        Get
            Return mintReferencetypeid
        End Get
        Set(ByVal value As Integer)
            mintReferencetypeid = value
        End Set
    End Property
    'Sohail (14 Nov 2011) -- End

    'Sohail (13 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set shortname
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Shortname() As String
        Get
            Return mstrShortname
        End Get
        Set(ByVal value As String)
            mstrShortname = value
        End Set
    End Property
    'Sohail (13 Mar 2013) -- End

    'Sohail (08 Jul 2017) -- Start
    'TANAPA Enhancement - 68.1 - New JV Integration with Dynamics Nav.
    Public Property _Shortname2() As String
        Get
            Return mstrShortname2
        End Get
        Set(ByVal value As String)
            mstrShortname2 = value
        End Set
    End Property
    'Sohail (08 Jul 2017) -- End

    'Sohail (03 Mar 2020) -- Start
    'NMB Enhancement # : Need another option like short name on account configuration screens to set keywords to get concern value on JV.
    Public Property _Shortname3() As String
        Get
            Return mstrShortname3
        End Get
        Set(ByVal value As String)
            mstrShortname3 = value
        End Set
    End Property
    'Sohail (03 Mar 2020) -- End

    'Sohail (03 Jul 2020) -- Start
    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
    Private mintPeriodunkid As Integer
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    Private mblnUsedefaultmapping As Boolean
    Public Property _Usedefaultmapping() As Boolean
        Get
            Return mblnUsedefaultmapping
        End Get
        Set(ByVal value As Boolean)
            mblnUsedefaultmapping = value
        End Set
    End Property
    'Sohail (03 Jul 2020) -- End


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Public Property _IsInactive() As Boolean
        Get
            Return mblnIsInactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsInactive = value
        End Set
    End Property

    'Gajanan [24-Aug-2020] -- End
    'Hemant (22 Dec 2023) -- Start
    'ENHANCEMENT(TADB): A1X-1607 - Oracle CBS JV enhancement to split payable amounts for contributions to different GL accounts
    Public Property _Percentage() As Double
        Get
            Return mdblPercentage
        End Get
        Set(ByVal value As Double)
            mdblPercentage = value
        End Set
    End Property
    'Hemant (22 Dec 2023) -- End

    'Hemant (02 Feb 2024) -- Start
    'ENHANCEMENT(KCMUCO): A1X-2429 - Sage JV modification
    Public Property _OppositeAccountunkid() As Integer
        Get
            Return mintOppositeAccountunkid
        End Get
        Set(ByVal value As Integer)
            mintOppositeAccountunkid = value
        End Set
    End Property
    'Hemant (02 Feb 2024) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  accountconfigempunkid " & _
              ", tranheadunkid " & _
              ", employeeunkid " & _
              ", accountunkid " & _
              ", isactive " & _
              ", transactiontype_Id " & _
              ", referencecodeid " & _
              ", referencenameid " & _
              ", referencetypeid " & _
              ", ISNULL(shortname, '') AS shortname " & _
              ", ISNULL(praccount_configuration_employee.shortname2, '') AS shortname2 " & _
              ", ISNULL(praccount_configuration_employee.shortname3, '') AS shortname3 " & _
              ", ISNULL(praccount_configuration_employee.periodunkid, 0) AS periodunkid " & _
              ", ISNULL(praccount_configuration_employee.usedefaultmapping, 0) AS usedefaultmapping " & _
              ", ISNULL(praccount_configuration_employee.isinactive, 0) AS isinactive " & _
              ", ISNULL(praccount_configuration_employee.percentage, 0) AS percentage " & _
              ", ISNULL(praccount_configuration_employee.opposite_accountunkid, 0) AS opposite_accountunkid " & _
             "FROM praccount_configuration_employee " & _
             "WHERE accountconfigempunkid = @accountconfigempunkid "
            'Hemant (02 Feb 2024) -- [opposite_accountunkid]
            'Hemant (22 Dec 2023) -- [percentage]
            'Sohail (03 Jul 2020) - [periodunkid, usedefaultmapping]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]
            'Sohail (08 Jul 2017) - [shortname2]
            'Sohail (13 Mar 2013) - [shortname]

            objDataOperation.AddParameter("@accountconfigempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountconfigEmpunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAccountconfigEmpunkid = CInt(dtRow.Item("accountconfigempunkid"))
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintAccountunkid = CInt(dtRow.Item("accountunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mintTransactiontype_Id = CInt(dtRow.Item("transactiontype_Id"))
                'Sohail (14 Nov 2011) -- Start
                mintReferencecodeid = CInt(dtRow.Item("referencecodeid"))
                mintReferencenameid = CInt(dtRow.Item("referencenameid"))
                mintReferencetypeid = CInt(dtRow.Item("referencetypeid"))
                'Sohail (14 Nov 2011) -- End
                mstrShortname = dtRow.Item("shortname").ToString 'Sohail (13 Mar 2013)
                mstrShortname2 = dtRow.Item("shortname2").ToString 'Sohail (08 Jul 2017)
                mstrShortname3 = dtRow.Item("shortname3").ToString 'Sohail (03 Mar 2020)
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mblnUsedefaultmapping = CBool(dtRow.Item("usedefaultmapping"))
                'Sohail (03 Jul 2020) -- End

                'Gajanan [24-Aug-2020] -- Start
                'NMB Enhancement : Allow to set account configuration mapping 
                'inactive for closed period to allow to map head on other account 
                'configuration screen from new period
                mblnIsInactive = CBool(dtRow.Item("isinactive"))
                'Gajanan [24-Aug-2020] -- End
                'Hemant (22 Dec 2023) -- Start
                'ENHANCEMENT(TADB): A1X-1607 - Oracle CBS JV enhancement to split payable amounts for contributions to different GL accounts
                mdblPercentage = CDec(dtRow.Item("percentage"))
                'Hemant (22 Dec 2023) -- End
                'Hemant (02 Feb 2024) -- Start
                'ENHANCEMENT(KCMUCO): A1X-2429 - Sage JV modification
                mintOppositeAccountunkid = CInt(dtRow.Item("opposite_accountunkid"))
                'Hemant (02 Feb 2024) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal blnApplyUserAccessFilter As Boolean _
                            , ByVal strFilerString As String _
                            , ByVal strTableName As String _
                            , Optional ByVal blnOnlyActive As Boolean = True _
                            , Optional ByVal blnIncludeInactiveEmployees As Boolean = False _
                            , Optional ByVal blnAddGrouping As Boolean = False _
                            , Optional ByVal dtAsOnDate As Date = Nothing _
                            , Optional ByVal strFilterOuter As String = "" _
                            , Optional ByVal mintOnlyInActive As Integer = enTranHeadActiveInActive.ACTIVE _
                            ) As DataSet
        'Sohail (03 Jul 2020) - [dtAsOnDate, strFilterOuter]
        'Sohail (02 Nov 2019) - [blnAddGrouping]
        'Sohail (21 Aug 2015) - [xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, strFilerString]
        'Sohail (12 Jul 2013) - [blnIncludeInactiveEmployees]
        'Gajanan [24-Aug-2020] -- [mintOnlyInActive]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (21 Aug 2015) -- Start
        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
        'Sohail (21 Aug 2015) -- End

        objDataOperation = New clsDataOperation

        Try

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            'Sohail (03 Jul 2020) -- End

            'Sohail (06 Aug 2016) -- Start
            'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
            Dim objMaster As New clsMasterData
            Dim ds As DataSet = objMaster.getComboListJVTransactionType("TranType")
            Dim dicTranType As Dictionary(Of Integer, String) = (From p In ds.Tables("TranType") Select New With {.id = CInt(p.Item("id")), .name = p.Item("name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)
            'Sohail (06 Aug 2016) -- End

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            strQ = "SELECT  0 AS employeeunkid " & _
                         ", ' ' + @defaultcodeforunmapped AS employeecode " & _
                         ", ' ' + @defaultnameforunmapped AS employeename " & _
                   "INTO #TableEmp " & _
                   "UNION " & _
                   "SELECT  hremployee_master.employeeunkid " & _
                         ", hremployee_master.employeecode " & _
                         ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
                   "FROM    hremployee_master"

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE 1 = 1 "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If blnIncludeInactiveEmployees = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            strQ &= " SELECT * FROM ( "
            'Sohail (03 Jul 2020) -- End

            'Sohail (14 Nov 2011) -- Start
            'Changes : Loan Schme name and Saving Scheme name added.
            'strQ = "SELECT  accountconfigempunkid , " & _
            '          " praccount_configuration_employee.tranheadunkid , " & _
            '          " prtranhead_master.trnheadname, " & _
            '          " praccount_configuration_employee.employeeunkid , " & _
            '          " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename, " & _
            '          " praccount_configuration_employee.accountunkid , " & _
            '          " praccount_master.account_code, " & _
            '          " praccount_master.account_name, " & _
            '          " praccount_configuration_employee.isactive , " & _
            '          " transactiontype_Id " & _
            '          " FROM  praccount_configuration_employee " & _
            '          " LEFT JOIN prtranhead_master ON praccount_configuration_employee.tranheadunkid = prtranhead_master.tranheadunkid AND prtranhead_master.isvoid = 0 " & _
            '          " LEFT JOIN hremployee_master ON praccount_configuration_employee.employeeunkid = hremployee_master.employeeunkid AND hremployee_master.isactive = 1 " & _
            '          " LEFT JOIN praccount_master ON praccount_configuration_employee.accountunkid = praccount_master.accountunkid AND praccount_master.isactive = 1 " & _
            '          " WHERE 1=1 " 'Sohail (08 Nov 2011) - [account_code]
            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            'strQ = "SELECT  accountconfigempunkid , " & _
            '          " praccount_configuration_employee.tranheadunkid , " & _
            '          " ISNULL(CASE praccount_configuration_employee.transactiontype_Id " & _
            '                     "WHEN " & enJVTransactionType.TRANSACTION_HEAD & " THEN prtranhead_master.trnheadname " & _
            '                     "WHEN " & enJVTransactionType.LOAN & " THEN lnloan_scheme_master.name " & _
            '                     "WHEN " & enJVTransactionType.SAVINGS & " THEN svsavingscheme_master.savingschemename " & _
            '                     "WHEN " & enJVTransactionType.CASH & " THEN '" & If(dicTranType.ContainsKey(enJVTransactionType.CASH) = True, dicTranType.Item(enJVTransactionType.CASH), "CASH").ToString & "' " & _
            '                     "WHEN " & enJVTransactionType.BANK & " THEN cfbankbranch_master.branchname " & _
            '                     "WHEN " & enJVTransactionType.COST_CENTER & " THEN '" & If(dicTranType.ContainsKey(enJVTransactionType.COST_CENTER) = True, dicTranType.Item(enJVTransactionType.COST_CENTER), "COST CENTER").ToString & "' " & _
            '                     "WHEN " & enJVTransactionType.PAY_PER_ACTIVITY & " THEN practivity_master.name " & _
            '                     "WHEN " & enJVTransactionType.CR_EXPENSE & " THEN cmexpense_master.name " & _
            '                     "ELSE '' " & _
            '                   "END, '') AS trnheadname, " & _
            '          " praccount_configuration_employee.employeeunkid , " & _
            '          " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename, " & _
            '          " praccount_configuration_employee.accountunkid , " & _
            '          " praccount_master.account_code, " & _
            '          " praccount_master.account_name, " & _
            '          " praccount_configuration_employee.isactive , " & _
            '          " transactiontype_Id, " & _
            '          " praccount_configuration_employee.referencecodeid, " & _
            '          " praccount_configuration_employee.referencenameid, " & _
            '          " praccount_configuration_employee.referencetypeid " & _
            '          ", ISNULL(praccount_configuration_employee.shortname, '') AS shortname " & _
            '          ", ISNULL(praccount_configuration_employee.shortname2, '') AS shortname2 "
            strQ &= "SELECT  accountconfigempunkid , " & _
                      " praccount_configuration_employee.tranheadunkid , " & _
                      " ISNULL(CASE praccount_configuration_employee.transactiontype_Id " & _
                                 "WHEN " & CInt(enJVTransactionType.TRANSACTION_HEAD) & " THEN prtranhead_master.trnheadcode " & _
                                 "WHEN " & CInt(enJVTransactionType.LOAN) & " THEN lnloan_scheme_master.code " & _
                                 "WHEN " & CInt(enJVTransactionType.SAVINGS) & " THEN svsavingscheme_master.savingschemecode " & _
                                 "WHEN " & CInt(enJVTransactionType.CASH) & " THEN '" & If(dicTranType.ContainsKey(CInt(enJVTransactionType.CASH)) = True, dicTranType.Item(CInt(enJVTransactionType.CASH)), "CASH").ToString & "' " & _
                                 "WHEN " & CInt(enJVTransactionType.BANK) & " THEN cfbankbranch_master.branchcode " & _
                                 "WHEN " & CInt(enJVTransactionType.COST_CENTER) & " THEN '" & If(dicTranType.ContainsKey(CInt(enJVTransactionType.COST_CENTER)) = True, dicTranType.Item(CInt(enJVTransactionType.COST_CENTER)), "COST CENTER").ToString & "' " & _
                                 "WHEN " & CInt(enJVTransactionType.PAY_PER_ACTIVITY) & " THEN practivity_master.code " & _
                                 "WHEN " & CInt(enJVTransactionType.CR_EXPENSE) & " THEN cmexpense_master.code " & _
                                 "ELSE '' " & _
                               "END, '') AS trnheadcode, " & _
                      " ISNULL(CASE praccount_configuration_employee.transactiontype_Id " & _
                                 "WHEN " & CInt(enJVTransactionType.TRANSACTION_HEAD) & " THEN prtranhead_master.trnheadname " & _
                                 "WHEN " & CInt(enJVTransactionType.LOAN) & " THEN lnloan_scheme_master.name " & _
                                 "WHEN " & CInt(enJVTransactionType.SAVINGS) & " THEN svsavingscheme_master.savingschemename " & _
                                 "WHEN " & CInt(enJVTransactionType.CASH) & " THEN '" & If(dicTranType.ContainsKey(CInt(enJVTransactionType.CASH)) = True, dicTranType.Item(CInt(enJVTransactionType.CASH)), "CASH").ToString & "' " & _
                                 "WHEN " & CInt(enJVTransactionType.BANK) & " THEN cfbankbranch_master.branchname " & _
                                 "WHEN " & CInt(enJVTransactionType.COST_CENTER) & " THEN '" & If(dicTranType.ContainsKey(CInt(enJVTransactionType.COST_CENTER)) = True, dicTranType.Item(CInt(enJVTransactionType.COST_CENTER)), "COST CENTER").ToString & "' " & _
                                 "WHEN " & CInt(enJVTransactionType.PAY_PER_ACTIVITY) & " THEN practivity_master.name " & _
                                 "WHEN " & CInt(enJVTransactionType.CR_EXPENSE) & " THEN cmexpense_master.name " & _
                                 "WHEN " & enJVTransactionType.COMPANY_BANK & " THEN CompBranch.branchname " & _
                                 "ELSE '' " & _
                               "END, '') AS trnheadname, " & _
                      " praccount_configuration_employee.employeeunkid , " & _
                      " #TableEmp.employeecode , " & _
                      " #TableEmp.employeename, " & _
                      " praccount_configuration_employee.accountunkid , " & _
                      " praccount_master.account_code, " & _
                      " praccount_master.account_name, " & _
                      " praccount_configuration_employee.isactive , " & _
                      " ISNULL(praccount_configuration_employee.isinactive, 0) AS isinactive , " & _
                      " transactiontype_Id, " & _
                      " praccount_configuration_employee.referencecodeid, " & _
                      " praccount_configuration_employee.referencenameid, " & _
                      " praccount_configuration_employee.referencetypeid " & _
                      ", ISNULL(praccount_configuration_employee.shortname, '') AS shortname " & _
                      ", ISNULL(praccount_configuration_employee.shortname2, '') AS shortname2 " & _
                      ", ISNULL(praccount_configuration_employee.shortname3, '') AS shortname3 " & _
                      ", CAST(0 AS BIT) AS IsGrp " & _
                      ", CAST(0 AS BIT) AS IsChecked " & _
                      ", ISNULL(praccount_configuration_employee.periodunkid, 0) AS periodunkid " & _
                      ", ISNULL(cfcommon_period_tran.period_code, '') AS period_code " & _
                      ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
                      ", ISNULL(praccount_configuration_employee.usedefaultmapping, 0) AS usedefaultmapping " & _
                      ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                      ", ISNULL(praccount_configuration_employee.percentage, 0) AS percentage " & _
                      ", ISNULL(praccount_configuration_employee.opposite_accountunkid, 0) AS opposite_accountunkid "
            'Hemant (02 Feb 2024) -- [opposite_accountunkid]
            'Hemant (22 Dec 2023) -- [percentage]
            'Sohail (03 Jul 2020) - [periodunkid, period_code, period_name, usedefaultmapping, ROWNO]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (02 Nov 2019) - [IsGrp, IsChecked]
            'Sohail (03 Jan 2019) - [CompBranch.branchname]
            'Sohail (02 Aug 2017) -- End
            'Sohail (08 Jul 2017) - [shortname2]

            'Sohail (06 Aug 2016) -- Start
            'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
            '"WHEN 5 THEN 'CASH' " & _
            '"WHEN 6 THEN 'BANK' " & _
            '"WHEN 7 THEN 'COST CENTER' " & _
            strQ &= ", CASE praccount_configuration_employee.transactiontype_Id "
            For Each pair In dicTranType
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS transactiontype_name "
            'Sohail (06 Aug 2016) -- End

            strQ &= "   FROM  praccount_configuration_employee " & _
                      " LEFT JOIN prtranhead_master ON praccount_configuration_employee.tranheadunkid = prtranhead_master.tranheadunkid AND prtranhead_master.isvoid = 0 " & _
                      " JOIN #TableEmp ON praccount_configuration_employee.employeeunkid = #TableEmp.employeeunkid " & _
                      " LEFT JOIN praccount_master ON praccount_configuration_employee.accountunkid = praccount_master.accountunkid AND praccount_master.isactive = 1 " & _
                      " LEFT JOIN lnloan_scheme_master ON praccount_configuration_employee.tranheadunkid = lnloan_scheme_master.loanschemeunkid " & _
                      " LEFT JOIN svsavingscheme_master ON praccount_configuration_employee.tranheadunkid = svsavingscheme_master.savingschemeunkid " & _
                      "LEFT JOIN practivity_master ON praccount_configuration_employee.tranheadunkid = practivity_master.activityunkid " & _
                      "LEFT JOIN cmexpense_master ON praccount_configuration_employee.tranheadunkid = cmexpense_master.expenseunkid " & _
                      "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON praccount_configuration_employee.tranheadunkid = cfbankbranch_master.branchunkid " & _
                      "LEFT JOIN hrmsConfiguration..cfcompanybank_tran ON cfcompanybank_tran.companybanktranunkid = praccount_configuration_employee.tranheadunkid " & _
                      "LEFT JOIN hrmsConfiguration..cfbankbranch_master AS CompBranch ON cfcompanybank_tran.branchunkid = CompBranch.branchunkid " & _
                      "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid "
            'Sohail (03 Jul 2020) - [LEFT JOIN cfcommon_period_tran], [LEFT JOIN hremployee_master]=[JOIN #TableEmp]
            'Sohail (21 May 2020) - [LEFT JOIN hrmsConfiguration..cfcompanybank_tran ON cfcompanybank_tran.companybanktranunkid = praccount_configuration_employee.tranheadunkid]
            'Sohail (03 Jan 2019) - [LEFT JOIN hrmsConfiguration..cfbankbranch_master AS CompBranch ON praccount_configuration_employee.tranheadunkid = CompBranch.branchunkid]
            'Sohail (06 Aug 2016) - [LEFT JOIN cfbankbranch_master ON praccount_configuration_employee.tranheadunkid = cfbankbranch_master.branchunkid]

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If
            'Sohail (03 Jul 2020) -- End

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            'If blnApplyUserAccessFilter = True Then
            '    If xUACQry.Trim.Length > 0 Then
            '        strQ &= xUACQry
            '    End If
            'End If
            'Sohail (03 Jul 2020) -- End
            'S.SANDEEP [15 NOV 2016] -- END

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If
            'Sohail (03 Jul 2020) -- End
            'Sohail (21 Aug 2015) -- End

            strQ &= " WHERE 1 = 1 "
            '         'Sohail (12 Nov 2014) - [cmexpense_master.name]
            '         'Sohail (21 Jun 2013) - [CASH, BANK, COST CENTER, practivity_master.name]
            '         'Sohail (13 Mar 2013) - [shortname]
            'Sohail (14 Nov 2011) -- End

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Sohail (12 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    strQ += " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            'End If
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If blnIncludeInactiveEmployees = False Then
            '    strQ += " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'Sohail (12 May 2012) - [empl_enddate]

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            'End If
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If blnIncludeInactiveEmployees = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry
            '    End If
            'End If
            'Sohail (03 Jul 2020) -- End

            If strFilerString.Trim.Length > 0 Then
                strQ &= " AND " & strFilerString
            End If
            'Sohail (21 Aug 2015) -- End

            'Sohail (12 Jul 2013) -- End
            'Sohail (06 Jan 2012) -- End

            If blnOnlyActive Then
                strQ &= " AND praccount_configuration_employee.isactive = 1 "
            End If

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            If dtAsOnDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
                objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If
            'Sohail (03 Jul 2020) -- End

            'Sohail (06 Aug 2016) -- Start
            'Enhancement - 63.1 - Bank Branch in mapping account configuration for Flex Cube Payment JV.
            'strQ &= " ORDER BY praccount_configuration_employee.employeeunkid "
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            'strQ &= " ORDER BY A.employeename " & _
            '                ", A.transactiontype_Id "
            strQ &= " ) AS A " & _
                    " WHERE 1 = 1 "

            If dtAsOnDate <> Nothing Then
                strQ &= " AND A.ROWNO = 1 "
            End If

            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            If mintOnlyInActive = enTranHeadActiveInActive.INACTIVE Then
                strQ &= "AND A.isinactive = 1 "
            Else
                strQ &= "AND A.isinactive = 0 "
            End If
            'Gajanan [24-Aug-2020] -- End

            If strFilterOuter.Trim <> "" Then
                strQ &= " " & strFilterOuter & " "
            End If

            strQ &= " ORDER BY A.employeename " & _
                           ", A.transactiontype_Id "

            strQ &= " DROP TABLE #TableEmp "

            objDataOperation.AddParameter("@defaultcodeforunmapped", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 66, "Default Account for ALL unmapped Employees"))
            objDataOperation.AddParameter("@defaultnameforunmapped", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 67, "Default Account for ALL unmapped Employees"))
            'Sohail (03 Jul 2020) -- End
            'Sohail (06 Aug 2016) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (02 Nov 2019) -- Start
            'TANAPA Enhancement # 4254 : Slowness on opening the Employee Acc. & Cost Center Acc. Config screens..
            If blnAddGrouping = True Then

                Dim dt As DataTable = New DataView(dsList.Tables(0)).ToTable(True, "employeename", "employeecode", "employeeunkid")
                Dim dtCol As New DataColumn("IsChecked", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = False
                dtCol.AllowDBNull = False
                dt.Columns.Add(dtCol)

                dtCol = New DataColumn("IsGrp", System.Type.GetType("System.Boolean"))
                dtCol.DefaultValue = True
                dtCol.AllowDBNull = False
                dt.Columns.Add(dtCol)

                dt.Merge(dsList.Tables(0), False)
                dt.DefaultView.Sort = "employeename, employeecode, employeeunkid, IsGrp DESC"

                dsList.Tables.Clear()
                dsList.Tables.Add(dt.DefaultView.ToTable)
            End If
            'Sohail (02 Nov 2019) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (praccount_configuration_employee) </purpose>
    Public Function Insert(ByVal blnIsOverWrite As Boolean, ByVal intTranHeadTypeID As Integer, ByVal mstrTranheadunkid As String, ByVal mstrEmployeeIDs As String, ByVal objDataop As clsDataOperation, ByVal dtPeriodEnd As Date) As Boolean 'Sohail (14 Nov 2011) - [mstrEmployeeIDs]
        'Sohail (25 Jul 2020) - [dtPeriodEnd]
        'Sohail (02 Aug 2017) - [objDataop]
        'Public Function Insert(ByVal blnIsOverWrite As Boolean, ByVal intTranHeadTypeID As Integer, Optional ByVal mstrTranheadunkid As String = "") As Boolean
        'Dim objDataOperation As clsDataOperation 'Sohail (02 Aug 2017) 
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objTranHead As New clsTransactionHead 'Sohail (02 Aug 2017

        'Sohail (02 Aug 2017) -- Start
        'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If objDataop IsNot Nothing Then
            objDataOperation = objDataop
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Sohail (02 Aug 2017) -- End
        Try


            strQ = "INSERT INTO praccount_configuration_employee ( " & _
                  "  tranheadunkid " & _
                  ", employeeunkid " & _
                  ", accountunkid " & _
                  ", isactive " & _
                  ", transactiontype_Id" & _
                  ", referencecodeid " & _
                  ", referencenameid " & _
                  ", referencetypeid " & _
                  ", shortname " & _
                  ", shortname2 " & _
                  ", shortname3 " & _
                  ", periodunkid " & _
                  ", usedefaultmapping " & _
                  ", isinactive " & _
                  ", percentage " & _
              ", opposite_accountunkid " & _
                ") VALUES (" & _
                  "  @tranheadunkid " & _
                  ", @employeeunkid " & _
                  ", @accountunkid " & _
                  ", @isactive " & _
                  ", @transactiontype_Id" & _
                  ", @referencecodeid " & _
                  ", @referencenameid " & _
                  ", @referencetypeid " & _
                  ", @shortname " & _
                  ", @shortname2 " & _
                  ", @shortname3 " & _
                  ", @periodunkid " & _
                  ", @usedefaultmapping " & _
                  ", @isinactive " & _
                  ", @percentage " & _
              ", @opposite_accountunkid " & _
                "); SELECT @@identity"
            'Hemant (02 Feb 2024) -- [opposite_accountunkid]
            'Hemant (22 Dec 2023) -- [percentage]
            'Sohail (03 Jul 2020) = [periodunkid, usedefaultmapping]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]
            'Sohail (08 Jul 2017) - [shortname2]
            'Sohail (13 Mar 2013) - [shortname]
            'Gajanan [24-Aug-2020] - [isinactive]


            If mstrTranheadunkid <> "" Then

                Dim arTranheadunkid As String() = mstrTranheadunkid.Split(",")

                If arTranheadunkid.Length <= 0 Then Return False

                'Sohail (14 Nov 2011) -- Start
                Dim arEmpUnkid As String() = mstrEmployeeIDs.Split(",")

                For j As Integer = 0 To arEmpUnkid.Length - 1
                    mintEmployeeunkid = CInt(arEmpUnkid(j))
                    'Sohail (14 Nov 2011) -- End

                    For i As Integer = 0 To arTranheadunkid.Length - 1

                        'Sohail (02 Aug 2017) -- Start
                        'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
                        If objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, CType(mintTransactiontype_Id, enJVTransactionType), dtPeriodEnd, CInt(arTranheadunkid(i))) = True Then Continue For
                        'Sohail (25 Jul 2020) - [dtPeriodEnd]
                        'Sohail (02 Aug 2017) -- End

                        'Sohail (31 Aug 2017) -- Start
                        'Issue - 69.1 - Bind transaction issue in inserting employee account configuration.
                        'If isExist(intTranHeadTypeID, CInt(arTranheadunkid(i)), mintEmployeeunkid) Then
                        'Sohail (03 Jul 2020) -- Start
                        'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                        'If isExist(intTranHeadTypeID, CInt(arTranheadunkid(i)), mintEmployeeunkid, -1, -1, objDataOperation) Then
                        If isExist(intTranHeadTypeID, CInt(arTranheadunkid(i)), mintEmployeeunkid, mintPeriodunkid, mintAccountunkid, -1, objDataOperation) Then
                            'Hemant (22 Dec 2023) -- [ -1 --> mintAccountunkid]
                            'Sohail (03 Jul 2020) -- End
                            'Sohail (31 Aug 2017) -- End
                            If blnIsOverWrite Then
                                'Sohail (02 Aug 2011) -- Start
                                'mintAccountconfigEmpunkid = GetEmpAccountConfigUnkId(objDataOperation, CInt(arTranheadunkid(i)))
                                'Sohail (09 Jul 2013) -- Start
                                'TRA - ENHANCEMENT
                                'mintAccountconfigEmpunkid = GetEmpAccountConfigUnkId(intTranHeadTypeID, CInt(arTranheadunkid(i)))
                                'Sohail (31 Aug 2017) -- Start
                                'Issue - 69.1 - Bind transaction issue in inserting employee account configuration.
                                'mintAccountconfigEmpunkid = GetEmpAccountConfigUnkId(intTranHeadTypeID, CInt(arTranheadunkid(i)), mintEmployeeunkid)
                                'Sohail (03 Jul 2020) -- Start
                                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                                'mintAccountconfigEmpunkid = GetEmpAccountConfigUnkId(intTranHeadTypeID, CInt(arTranheadunkid(i)), mintEmployeeunkid, objDataOperation)
                                mintAccountconfigEmpunkid = GetEmpAccountConfigUnkId(intTranHeadTypeID, CInt(arTranheadunkid(i)), mintEmployeeunkid, mintPeriodunkid, objDataOperation)
                                'Sohail (03 Jul 2020) -- End
                                'Sohail (31 Aug 2017) -- End
                                'Sohail (09 Jul 2013) -- End
                                'Sohail (02 Aug 2011) -- End
                                mintTranheadunkid = CInt(arTranheadunkid(i))
                                Update(objDataOperation)
                                mintTranheadunkid = -1
                                mintAccountconfigEmpunkid = -1
                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                Continue For
                            Else
                                Continue For
                            End If
                        End If

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                        objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
                        objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                        objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
                        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, arTranheadunkid(i).ToString)
                        'Sohail (14 Nov 2011) -- Start
                        objDataOperation.AddParameter("@referencecodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencecodeid.ToString)
                        objDataOperation.AddParameter("@referencenameid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencenameid.ToString)
                        objDataOperation.AddParameter("@referencetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencetypeid.ToString)
                        'Sohail (14 Nov 2011) -- End
                        objDataOperation.AddParameter("@shortname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname.ToString) 'Sohail (13 Mar 2013)
                        objDataOperation.AddParameter("@shortname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname2.ToString) 'Sohail (08 Jul 2017)
                        objDataOperation.AddParameter("@shortname3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname3.ToString) 'Sohail (03 Mar 2020)
                        'Sohail (03 Jul 2020) -- Start
                        'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
                        objDataOperation.AddParameter("@usedefaultmapping", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnUsedefaultmapping)
                        'Sohail (03 Jul 2020) -- End

                        'Gajanan [24-Aug-2020] -- Start
                        'NMB Enhancement : Allow to set account configuration mapping 
                        'inactive for closed period to allow to map head on other account 
                        'configuration screen from new period
                        objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        'Gajanan [24-Aug-2020] -- End
                    'Hemant (22 Dec 2023) -- Start
                    'ENHANCEMENT(TADB): A1X-1607 - Oracle CBS JV enhancement to split payable amounts for contributions to different GL accounts
                    objDataOperation.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdblPercentage.ToString)
                    'Hemant (22 Dec 2023) -- End
                    'Hemant (02 Feb 2024) -- Start
                    'ENHANCEMENT(KCMUCO): A1X-2429 - Sage JV modification
                    objDataOperation.AddParameter("@opposite_accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOppositeAccountunkid.ToString)
                    'Hemant (02 Feb 2024) -- End

                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'Sohail (12 Oct 2011) -- Start
                        mintAccountconfigEmpunkid = dsList.Tables(0).Rows(0).Item(0)
                        If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "praccount_configuration_employee", "accountconfigempunkid", mintAccountconfigEmpunkid) = False Then
                            'Sohail (02 Aug 2017) -- Start
                            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
                            'objDataOperation.ReleaseTransaction(False)
                            If objDataop Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            'Sohail (02 Aug 2017) -- End
                            Return False
                        End If
                        'Sohail (12 Oct 2011) -- End
                    Next

                Next 'Sohail (14 Nov 2011)
            Else
                'Sohail (14 Nov 2011) -- Start
                Dim arEmpUnkid As String() = mstrEmployeeIDs.Split(",")

                For j As Integer = 0 To arEmpUnkid.Length - 1
                    mintEmployeeunkid = CInt(arEmpUnkid(j))
                    'Sohail (14 Nov 2011) -- End

                    'Sohail (02 Aug 2017) -- Start
                    'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
                    If objTranHead.IsTranHeadIDUsedInOtherJV(enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION, CType(mintTransactiontype_Id, enJVTransactionType), dtPeriodEnd, mintTranheadunkid) = True Then Continue For
                    'Sohail (25 Jul 2020) - [dtPeriodEnd]
                    'Sohail (02 Aug 2017) -- End

                    'Sohail (02 Aug 2011) -- Start
                    'Sohail (31 Aug 2017) -- Start
                    'Issue - 69.1 - Bind transaction issue in inserting employee account configuration.
                    'If isExist(intTranHeadTypeID, 0, mintEmployeeunkid) = True Then
                    'Sohail (03 Jul 2020) -- Start
                    'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                    'If isExist(intTranHeadTypeID, 0, mintEmployeeunkid, -1, -1, objDataOperation) = True Then
                    If isExist(intTranHeadTypeID, 0, mintEmployeeunkid, mintPeriodunkid, mintAccountunkid, -1, objDataOperation) = True Then
                        'Hemant (22 Dec 2023) -- [ -1 ---> mintAccountunkid]
                        'Sohail (03 Jul 2020) -- End
                        'Sohail (31 Aug 2017) -- End
                        If blnIsOverWrite Then
                            'Sohail (09 Jul 2013) -- Start
                            'TRA - ENHANCEMENT
                            'mintAccountconfigEmpunkid = GetEmpAccountConfigUnkId(intTranHeadTypeID, 0)
                            'Sohail (31 Aug 2017) -- Start
                            'Issue - 69.1 - Bind transaction issue in inserting employee account configuration.
                            'mintAccountconfigEmpunkid = GetEmpAccountConfigUnkId(intTranHeadTypeID, 0, mintEmployeeunkid)
                            'Sohail (03 Jul 2020) -- Start
                            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                            'mintAccountconfigEmpunkid = GetEmpAccountConfigUnkId(intTranHeadTypeID, 0, mintEmployeeunkid, objDataOperation)
                            mintAccountconfigEmpunkid = GetEmpAccountConfigUnkId(intTranHeadTypeID, 0, mintEmployeeunkid, mintPeriodunkid, objDataOperation)
                            'Sohail (03 Jul 2020) -- End
                            'Sohail (31 Aug 2017) -- End
                            'Sohail (09 Jul 2013) -- End
                            'Sohail (12 Oct 2011) -- Start
                            'Call Update(True)
                            If Update(True) = False Then
                                'Sohail (02 Aug 2017) -- Start
                                'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
                                'objDataOperation.ReleaseTransaction(False)
                                If objDataop Is Nothing Then objDataOperation.ReleaseTransaction(False)
                                'Sohail (02 Aug 2017) -- End
                                Return False
                            End If

                            'Sohail (12 Oct 2011) -- End
                        End If
                    Else
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                        objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
                        objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                        objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
                        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid)
                        'Sohail (14 Nov 2011) -- Start
                        objDataOperation.AddParameter("@referencecodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencecodeid.ToString)
                        objDataOperation.AddParameter("@referencenameid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencenameid.ToString)
                        objDataOperation.AddParameter("@referencetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencetypeid.ToString)
                        'Sohail (14 Nov 2011) -- End
                        objDataOperation.AddParameter("@shortname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname.ToString) 'Sohail (13 Mar 2013)
                        objDataOperation.AddParameter("@shortname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname2.ToString) 'Sohail (08 Jul 2017)
                        objDataOperation.AddParameter("@shortname3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname3.ToString) 'Sohail (03 Mar 2020)
                        'Sohail (03 Jul 2020) -- Start
                        'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
                        objDataOperation.AddParameter("@usedefaultmapping", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnUsedefaultmapping)
                        'Sohail (03 Jul 2020) -- End

                        'Gajanan [24-Aug-2020] -- Start
                        'NMB Enhancement : Allow to set account configuration mapping 
                        'inactive for closed period to allow to map head on other account 
                        'configuration screen from new period
                        objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        'Gajanan [24-Aug-2020] -- End
                        'Hemant (22 Dec 2023) -- Start
                        'ENHANCEMENT(TADB): A1X-1607 - Oracle CBS JV enhancement to split payable amounts for contributions to different GL accounts
                        objDataOperation.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdblPercentage.ToString)
                        'Hemant (22 Dec 2023) -- End
                    'Hemant (02 Feb 2024) -- Start
                    'ENHANCEMENT(KCMUCO): A1X-2429 - Sage JV modification
                    objDataOperation.AddParameter("@opposite_accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOppositeAccountunkid.ToString)
                    'Hemant (02 Feb 2024) -- End
                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'Sohail (12 Oct 2011) -- Start
                        mintAccountconfigEmpunkid = dsList.Tables(0).Rows(0).Item(0)
                        If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "praccount_configuration_employee", "accountconfigempunkid", mintAccountconfigEmpunkid) = False Then
                            'Sohail (02 Aug 2017) -- Start
                            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
                            'objDataOperation.ReleaseTransaction(False)
                            If objDataop Is Nothing Then objDataOperation.ReleaseTransaction(False)
                            'Sohail (02 Aug 2017) -- End
                            Return False
                        End If
                        'Sohail (12 Oct 2011) -- End
                    End If
                    'Sohail (02 Aug 2011) -- End

                Next 'Sohail (14 Nov 2011)
            End If

            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            'objDataOperation.ReleaseTransaction(True)
            If objDataop Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Sohail (02 Aug 2017) -- End
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            objDataOperation.ReleaseTransaction(False)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            'objDataOperation = Nothing
            If objDataop Is Nothing Then objDataOperation = Nothing
            'Sohail (02 Aug 2017) -- End
        End Try
    End Function

    'Sohail (02 Aug 2017) -- Start
    'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
    Public Function InsertAll(ByVal dtTable As DataTable, ByVal blnIsOverWrite As Boolean, ByVal dtPeriodEnd As Date) As Boolean
        'Sohail (25 Jul 2020) - [dtPeriodEnd]

        Dim objDataOperation As clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try


            For Each dtRow As DataRow In dtTable.Rows
                'Sohail (03 Jul 2020) -- Start
                'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mblnUsedefaultmapping = CBool(dtRow.Item("usedefaultmapping"))
                'Sohail (03 Jul 2020) -- End
                mintTransactiontype_Id = CInt(dtRow.Item("transactiontype_id"))
                mintTranheadunkid = CInt(dtRow.Item("headunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintAccountunkid = CInt(dtRow.Item("accountunkid"))
                mintReferencecodeid = CInt(dtRow.Item("referencecodeid"))
                mintReferencenameid = CInt(dtRow.Item("referencenameid"))
                mintReferencetypeid = CInt(dtRow.Item("referencetypeid"))
                mstrShortname = dtRow.Item("shortname").ToString
                mstrShortname2 = dtRow.Item("shortname2").ToString
                mstrShortname3 = dtRow.Item("shortname3").ToString
                'Sohail (03 Mar 2020) - [shortname3]

                mblnIsactive = True

                'Gajanan [24-Aug-2020] -- Start
                'NMB Enhancement : Allow to set account configuration mapping 
                'inactive for closed period to allow to map head on other account 
                'configuration screen from new period
                mblnIsInactive = False
                'Gajanan [24-Aug-2020] -- End
                'Hemant (22 Dec 2023) -- Start
                'ENHANCEMENT(TADB): A1X-1607 - Oracle CBS JV enhancement to split payable amounts for contributions to different GL accounts
                mdblPercentage = CDec(dtRow.Item("percentage").ToString)
                'Hemant (22 Dec 2023) -- End

                If Insert(blnIsOverWrite, mintTransactiontype_Id, mintTranheadunkid.ToString(), mintEmployeeunkid.ToString(), objDataOperation, dtPeriodEnd) = False Then
                    'Sohail (25 Jul 2020) - [mdtPeriodEnd]
                    objDataOperation.ReleaseTransaction(False)
                    If mstrMessage.Trim.Length > 0 Then Throw New Exception(mstrMessage)
                    Return False
                    Exit For
                End If
            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            objDataOperation.ReleaseTransaction(False)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (02 Aug 2017) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (praccount_configuration_employee) </purpose>
    Public Function Update(ByVal blnIsOverWrite As Boolean) As Boolean
        Dim objDataOperation As clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            'If isExist(mintTranheadunkid, mintEmployeeunkid, mintAccountunkid, mintAccountconfigEmpunkid) Then
            If isExist(mintTransactiontype_Id, mintTranheadunkid, mintEmployeeunkid, mintPeriodunkid, mintAccountunkid, mintAccountconfigEmpunkid, objDataOperation) Then
                'Sohail (03 Jul 2020) -- End
                If blnIsOverWrite Then
                    GoTo OverWrite
                Else
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Account Configuration for this Transaction Head is already defined. Please define new Cost Center Account Configuration.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

OverWrite:
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@accountconfigempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountconfigEmpunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
            'Sohail (14 Nov 2011) -- Start
            objDataOperation.AddParameter("@referencecodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencecodeid.ToString)
            objDataOperation.AddParameter("@referencenameid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencenameid.ToString)
            objDataOperation.AddParameter("@referencetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencetypeid.ToString)
            'Sohail (14 Nov 2011) -- End
            objDataOperation.AddParameter("@shortname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname.ToString) 'Sohail (13 Mar 2013)
            objDataOperation.AddParameter("@shortname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname2.ToString) 'Sohail (08 Jul 2017)
            objDataOperation.AddParameter("@shortname3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname3.ToString) 'Sohail (03 Mar 2020)
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            objDataOperation.AddParameter("@usedefaultmapping", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnUsedefaultmapping)
            'Sohail (03 Jul 2020) -- End
            'Hemant (22 Dec 2023) -- Start
            'ENHANCEMENT(TADB): A1X-1607 - Oracle CBS JV enhancement to split payable amounts for contributions to different GL accounts
            objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInactive.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdblPercentage.ToString)
            'Hemant (22 Dec 2023) -- End
            'Hemant (02 Feb 2024) -- Start
            'ENHANCEMENT(KCMUCO): A1X-2429 - Sage JV modification
            objDataOperation.AddParameter("@opposite_accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOppositeAccountunkid.ToString)
            'Hemant (02 Feb 2024) -- End

            strQ = "UPDATE praccount_configuration_employee SET " & _
              "  tranheadunkid = @tranheadunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", accountunkid = @accountunkid" & _
              ", isactive = @isactive" & _
              ", transactiontype_Id = @transactiontype_Id " & _
              ", referencecodeid = @referencecodeid" & _
              ", referencenameid = @referencenameid" & _
              ", referencetypeid = @referencetypeid " & _
              ", shortname = @shortname " & _
              ", shortname2 = @shortname2 " & _
              ", shortname3 = @shortname3 " & _
              ", periodunkid = @periodunkid " & _
              ", usedefaultmapping = @usedefaultmapping " & _
              ", isinactive = @isinactive " & _
              ", percentage = @percentage " & _
              ", opposite_accountunkid = @opposite_accountunkid " & _
            "WHERE accountconfigempunkid = @accountconfigempunkid "
            'Hemant (02 Feb 2024) -- [opposite_accountunkid]
            'Hemant (22 Dec 2023) -- [percentage]
            'Sohail (03 Jul 2020) - [periodunkid, usedefaultmapping]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]
            'Sohail (08 Jul 2017) - [shortname2]
            'Sohail (13 Mar 2013) - [shortname]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            Dim blnToInsert As Boolean
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "praccount_configuration_employee", mintAccountconfigEmpunkid, "accountconfigempunkid", 2) Then
                blnToInsert = True
            End If
            If blnToInsert = True AndAlso clsCommonATLog.Insert_AtLog(objDataOperation, 2, "praccount_configuration_employee", "accountconfigempunkid", mintAccountconfigEmpunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (12 Oct 2011) -- End
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (praccount_configuration_employee) </purpose>
    Public Function Update(ByVal objDataOperation As clsDataOperation) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@accountconfigempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountconfigEmpunkid.ToString)
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccountunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransactiontype_Id.ToString)
            'Sohail (14 Nov 2011) -- Start
            objDataOperation.AddParameter("@referencecodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencecodeid.ToString)
            objDataOperation.AddParameter("@referencenameid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencenameid.ToString)
            objDataOperation.AddParameter("@referencetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferencetypeid.ToString)
            'Sohail (14 Nov 2011) -- End
            objDataOperation.AddParameter("@shortname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname.ToString) 'Sohail (13 Mar 2013)
            objDataOperation.AddParameter("@shortname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname2.ToString) 'Sohail (08 Jul 2017)
            objDataOperation.AddParameter("@shortname3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrShortname3.ToString) 'Sohail (03 Mar 2020)
            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid)
            objDataOperation.AddParameter("@usedefaultmapping", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnUsedefaultmapping)
            'Sohail (03 Jul 2020) -- End


            'Gajanan [24-Aug-2020] -- Start
            'NMB Enhancement : Allow to set account configuration mapping 
            'inactive for closed period to allow to map head on other account 
            'configuration screen from new period
            objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInactive.ToString)
            'Gajanan [24-Aug-2020] -- End
            'Hemant (22 Dec 2023) -- Start
            'ENHANCEMENT(TADB): A1X-1607 - Oracle CBS JV enhancement to split payable amounts for contributions to different GL accounts
            objDataOperation.AddParameter("@percentage", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdblPercentage.ToString)
            'Hemant (22 Dec 2023) -- End
            'Hemant (02 Feb 2024) -- Start
            'ENHANCEMENT(KCMUCO): A1X-2429 - Sage JV modification
            objDataOperation.AddParameter("@opposite_accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOppositeAccountunkid.ToString)
            'Hemant (02 Feb 2024) -- End


            strQ = "UPDATE praccount_configuration_employee SET " & _
              "  tranheadunkid = @tranheadunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", accountunkid = @accountunkid" & _
              ", isactive = @isactive" & _
              ", transactiontype_Id = @transactiontype_Id " & _
              ", referencecodeid = @referencecodeid" & _
              ", referencenameid = @referencenameid" & _
              ", referencetypeid = @referencetypeid " & _
              ", shortname = @shortname " & _
              ", shortname2 = @shortname2 " & _
              ", shortname3 = @shortname3 " & _
              ", periodunkid = @periodunkid " & _
              ", usedefaultmapping = @usedefaultmapping " & _
              ", isinactive = @isinactive " & _
              ", percentage = @percentage " & _
              ", opposite_accountunkid = @opposite_accountunkid " & _
            "WHERE accountconfigempunkid = @accountconfigempunkid "
            'Hemant (02 Feb 2024) -- [opposite_accountunkid]
            'Hemant (22 Dec 2023) -- [percentage]
            'Sohail (03 Jul 2020) - [periodunkid, usedefaultmapping]
            'Sohail (03 Mar 2020) - [shortname3]
            'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]
            'Sohail (08 Jul 2017) - [shortname2]
            'Sohail (13 Mar 2013) - [shortname]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            Dim blnToInsert As Boolean
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "praccount_configuration_employee", mintAccountconfigEmpunkid, "accountconfigempunkid", 2) Then
                blnToInsert = True
            End If
            If blnToInsert = True AndAlso clsCommonATLog.Insert_AtLog(objDataOperation, 2, "praccount_configuration_employee", "accountconfigempunkid", mintAccountconfigEmpunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Sohail (12 Oct 2011) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (praccount_configuration_employee) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation 'Sohail (12 Oct 2011)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction() 'Sohail (12 Oct 2011)

        Try
            strQ = "Update  praccount_configuration_employee set isactive = 0" & _
            "WHERE accountconfigempunkid = @accountconfigempunkid "

            objDataOperation.AddParameter("@accountconfigempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (12 Oct 2011) -- Start
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "praccount_configuration_employee", "accountconfigempunkid", intUnkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
                Return True
            End If
            'Sohail (12 Oct 2011) -- End
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (30 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function VoidAll(ByVal strUnkIDs As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE  praccount_configuration_employee " & _
                   "SET     isactive = 0 " & _
                   "WHERE   accountconfigempunkid IN ( " & strUnkIDs & " ) "


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim arrID() As String = strUnkIDs.Split(",")
            For i = 0 To arrID.Count - 1
                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "praccount_configuration_employee", "accountconfigempunkid", CInt(arrID(i))) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (30 Aug 2013) -- End

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@accountconfigempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intTranHeadTypeID As Integer, ByVal Tranheadunkid As Integer, ByVal employeeunkid As Integer, ByVal intPeriodUnkId As Integer, Optional ByVal Accountunkid As Integer = -1, Optional ByVal intunkid As Integer = -1, Optional ByVal objDataop As clsDataOperation = Nothing) As Boolean
        'Sohail (03 Jul 2020) - [intPeriodUnkId]
        'Sohail (31 Aug 2017) - [objDataop]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            'Sohail (31 Aug 2017) -- Start
            'Issue - 69.1 - Bind transaction issue in inserting employee account configuration.
            'objDataOperation = New clsDataOperation
            If objDataop IsNot Nothing Then
                objDataOperation = objDataop
            Else
                objDataOperation = New clsDataOperation
                'objDataOperation.BindTransaction() 'Sohail (07 Mar 2018) - [Bind transaction issue : not needed in isExist method] in 70.1
            End If
            'Sohail (31 Aug 2017) -- End
            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
              "  accountconfigempunkid " & _
              ", tranheadunkid " & _
              ", employeeunkid " & _
              ", accountunkid " & _
              ", isactive " & _
              ", transactiontype_Id " & _
              ", referencecodeid " & _
              ", referencenameid " & _
              ", referencetypeid " & _
             "FROM praccount_configuration_employee " & _
             "WHERE transactiontype_Id = @transactiontype_Id " & _
             "AND  isactive = 1 " 'Sohail (02 Aug 2011), 'Sohail (14 Nov 2011) - [referencecodeid, referencenameid, referencetypeid]

            'Sohail (02 Aug 2011) -- Start
            If Tranheadunkid > 0 Then
                strQ &= " AND tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Tranheadunkid)
            End If
            'Sohail (14 Nov 2011) -- Start
            'If employeeunkid > 0 Then 'Sohail (25 Jul 2020)
            'If intTranHeadTypeID = enJVTransactionType.TRANSACTION_HEAD Then
            'Sohail (14 Nov 2011) -- End
            strQ &= " AND employeeunkid = @employeeunkid "
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, employeeunkid)
            'End If 'Sohail (25 Jul 2020)
            'Sohail (02 Aug 2011) -- End

            If Accountunkid > 0 Then
                strQ &= " AND accountunkid = @accountunkid"
                objDataOperation.AddParameter("@accountunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Accountunkid)
            End If

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            If intPeriodUnkId > 0 Then
                strQ &= " AND praccount_configuration_employee.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If
            'Sohail (03 Jul 2020) -- End

            If intunkid > 0 Then
                strQ &= " AND accountconfigempunkid <> @accountconfigempunkid"
                objDataOperation.AddParameter("@accountconfigempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)
            End If


            'Sohail (02 Aug 2011) -- Start
            'objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, Tranheadunkid)
            'objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, employeeunkid)
            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadTypeID)
            'Sohail (02 Aug 2011) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetEmpAccountConfigUnkId(ByVal intTranHeadTypeID As Integer, ByVal TranHeadunkid As Integer, ByVal intEmployeeunkid As Integer, ByVal intPeriodUnkId As Integer, Optional ByVal objDataop As clsDataOperation = Nothing) As Integer
        'Sohail (03 Jul 2020) - [intPeriodUnkId]
        'Sohail (31 Aug 2017) - [objDataop]
        'Sohail (09 Jul 2013) - [intEmployeeunkid]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = -1

        'Sohail (31 Aug 2017) -- Start
        'Issue - 69.1 - Bind transaction issue in inserting employee account configuration.
        'objDataOperation = New clsDataOperation
        If objDataop IsNot Nothing Then
            objDataOperation = objDataop
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        'Sohail (31 Aug 2017) -- End
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
              "  accountconfigempunkid " & _
             "FROM praccount_configuration_employee " & _
             "WHERE transactiontype_Id = @transactiontype_Id " & _
             "AND isactive = 1"

            'Sohail (02 Aug 2011) -- Start
            If TranHeadunkid > 0 Then
                strQ &= " AND tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, TranHeadunkid)
            End If
            'If intTranHeadTypeID = enJVTransactionType.TRANSACTION_HEAD Then
            '    strQ &= " AND employeeunkid = @employeeunkid "
            '    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            'End If
            'Sohail (02 Aug 2011) -- End

            'Sohail (09 Jul 2013) -- Start
            'TRA - ENHANCEMENT
            If intEmployeeunkid > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            End If
            'Sohail (09 Jul 2013) -- End

            'Sohail (03 Jul 2020) -- Start
            'NMB Enhancement # : Effective period option on company, employee and cost center account configuration for historical account configuration.
            If intPeriodUnkId > 0 Then
                strQ &= " AND praccount_configuration_employee.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)
            End If
            'Sohail (03 Jul 2020) -- End

            'Sohail (02 Aug 2011) -- Start
            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadTypeID)
            'objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, TranHeadunkid)
            'objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
            'Sohail (02 Aug 2011) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0)("accountconfigempunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpAccountConfigUnkId; Module Name: " & mstrModuleName)
        End Try
        Return intUnkId
    End Function


    'Gajanan [24-Aug-2020] -- Start
    'NMB Enhancement : Allow to set account configuration mapping 
    'inactive for closed period to allow to map head on other account 
    'configuration screen from new period
    Public Function InactiveAll(ByVal strUnkIDs As String, ByVal intPeriodid As Integer) As Boolean
        If isInactive(strUnkIDs) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Some of the account of selected transactions are already inactive.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim newIds As String = ""

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "INSERT INTO  praccount_configuration_employee (" & _
                   " tranheadunkid " & _
                   ",employeeunkid " & _
                   ",accountunkid " & _
                   ",isactive " & _
                   ",transactiontype_Id  " & _
                   ",referencecodeid " & _
                   ",referencenameid " & _
                   ",referencetypeid " & _
                   ",shortname " & _
                   ",shortname2 " & _
                   ",shortname3 " & _
                   ",periodunkid " & _
                   ",usedefaultmapping " & _
                   ",isInactive " & _
                   ",percentage " & _
                   ") select " & _
                   " tranheadunkid " & _
                   ",employeeunkid " & _
                   ",accountunkid " & _
                   ",@isactive " & _
                   ",transactiontype_Id  " & _
                   ",referencecodeid " & _
                   ",referencenameid " & _
                   ",referencetypeid " & _
                   ",shortname " & _
                   ",shortname2 " & _
                   ",shortname3 " & _
                   ",@periodunkid " & _
                   ",usedefaultmapping " & _
                   ",@isInactive " & _
                   ",percentage " & _
                   " from praccount_configuration_employee " & _
                   "WHERE accountconfigempunkid = @accountconfigempunkid " & _
                   "; SELECT @@identity"
                   'Hemant (22 Dec 2023) -- [percentage]
            For Each id As String In strUnkIDs.Split(",")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodid)
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@accountconfigempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(id))

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If newIds.Trim.Length <= 0 Then
                    newIds = dsList.Tables(0).Rows(0).Item(0)
                Else
                    newIds &= "," & dsList.Tables(0).Rows(0).Item(0)
                End If
                dsList = Nothing
            Next


            Dim arrID() As String = newIds.Split(",")
            For i = 0 To arrID.Count - 1
                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "praccount_configuration_employee", "accountconfigempunkid", CInt(arrID(i))) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InactiveAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isInactive(ByVal strUnkIDs As String) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  accountconfigempunkid " & _
              "FROM praccount_configuration_employee " & _
              "WHERE isactive = 1 and isinactive = 1 and accountconfigempunkid IN ( " & strUnkIDs & " ) "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isInactive; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Gajanan [24-Aug-2020] -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Account Configuration for this Transaction Head is already defined. Please define new Cost Center Account Configuration.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Some of the account of selected transactions are already inactive.")

        Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
