﻿Imports eZeeCommonLib
Imports Aruti.Data
Imports ExcelWriter

Public Class clsExportPPA
    Inherits IReportData

    Private Shared ReadOnly mstrModuleName As String = "clsExportPPA"
    Private mstrReportId As String = enArutiReport.Pay_Per_Activity_Report

#Region " Private Variables "

    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mintViewIndex As Integer = -1
    Private mstrViewByIds As String = ""
    Private mstrViewByName As String = ""
    Private mstrAnalysis_Fields As String = ""
    Private mstrAnalysis_Join As String = ""
    Private mstrAnalysis_OrderBy As String = ""
    Private mstrReport_GroupName As String = ""
    Private mstrUserAccessFilter As String = String.Empty
    Private mstrExportReportPath As String = ConfigParameter._Object._ExportReportPath
    Private mblnOpenAfterExport As Boolean = ConfigParameter._Object._OpenAfterExport
    Private mblnFirstNamethenSurname As Boolean = ConfigParameter._Object._FirstNamethenSurname
    Dim Rpt As CrystalDecisions.CrystalReports.Engine.ReportClass
    Private mintUserUnkid As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mstrCompanyName As String = String.Empty
#End Region

#Region " Properties "


    Public WriteOnly Property _StartDate() As Date
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    Public WriteOnly Property _EndDate() As Date
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    Public WriteOnly Property _ViewIndex() As Integer
        Set(ByVal value As Integer)
            mintViewIndex = value
        End Set
    End Property

    Public WriteOnly Property _ViewByIds() As String
        Set(ByVal value As String)
            mstrViewByIds = value
        End Set
    End Property

    Public WriteOnly Property _ViewByName() As String
        Set(ByVal value As String)
            mstrViewByName = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Fields() As String
        Set(ByVal value As String)
            mstrAnalysis_Fields = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_Join() As String
        Set(ByVal value As String)
            mstrAnalysis_Join = value
        End Set
    End Property

    Public WriteOnly Property _Analysis_OrderBy() As String
        Set(ByVal value As String)
            mstrAnalysis_OrderBy = value
        End Set
    End Property

    Public WriteOnly Property _Report_GroupName() As String
        Set(ByVal value As String)
            mstrReport_GroupName = value
        End Set
    End Property

    Public WriteOnly Property _ExportReportPath() As String
        Set(ByVal value As String)
            mstrExportReportPath = value
        End Set
    End Property

    Public WriteOnly Property _OpenAfterExport() As Boolean
        Set(ByVal value As Boolean)
            mblnOpenAfterExport = value
        End Set
    End Property

    Public WriteOnly Property _UserAccessFilter() As String
        Set(ByVal value As String)
            mstrUserAccessFilter = value
        End Set
    End Property

    Public WriteOnly Property _FirstNamethenSurname() As Boolean
        Set(ByVal value As Boolean)
            mblnFirstNamethenSurname = value
        End Set
    End Property

    Public ReadOnly Property _Rpt() As CrystalDecisions.CrystalReports.Engine.ReportClass
        Get
            Return Rpt
        End Get
    End Property

    Public WriteOnly Property _CompanyUnkId() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _Company() As String
        Set(ByVal value As String)
            mstrCompanyName = value
        End Set
    End Property

    Public WriteOnly Property _UserUnkId() As Integer
        Set(ByVal value As Integer)
            mintUserUnkid = value
        End Set
    End Property


#End Region

    Public Overrides Sub generateReport(ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None)

        'Pinkal (24-Aug-2015) -- Start
        'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

        'Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        'Dim strReportExportFile As String = ""
        'Try

        '    If mintCompanyUnkid <= 0 Then
        '        mintCompanyUnkid = Company._Object._Companyunkid
        '    End If

        '    Company._Object._Companyunkid = mintCompanyUnkid
        '    ConfigParameter._Object._Companyunkid = mintCompanyUnkid

        '    If mintUserUnkid <= 0 Then
        '        mintUserUnkid = User._Object._Userunkid
        '    End If

        '    User._Object._Userunkid = mintUserUnkid

        '    objRpt = ExportWeeklyPPATemplate()

        '    If Not IsNothing(objRpt) Then
        '        Call ReportExecute(objRpt, PrintAction, ExportAction, mstrExportReportPath, mblnOpenAfterExport)
        '    End If

        'Catch ex As Exception
        '    DisplayError.Show(-1, ex.Message, "generateReport", mstrModuleName)
        'End Try
        'Pinkal (24-Aug-2015) -- End
    End Sub

    Public Overrides Sub generateReportNew(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xExportReportPath As String, ByVal xOpenReportAfterExport As Boolean, ByVal pintReportType As Integer, Optional ByVal PrintAction As enPrintAction = enPrintAction.Preview, Optional ByVal ExportAction As enExportAction = enExportAction.None, Optional ByVal xBaseCurrencyId As Integer = 0)
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim strReportExportFile As String = ""
        Try

            If mintCompanyUnkid <= 0 Then
                mintCompanyUnkid = Company._Object._Companyunkid
            End If

            Company._Object._Companyunkid = mintCompanyUnkid
            ConfigParameter._Object._Companyunkid = mintCompanyUnkid

            If mintUserUnkid <= 0 Then
                mintUserUnkid = User._Object._Userunkid
            End If

            User._Object._Userunkid = mintUserUnkid

            objRpt = ExportWeeklyPPATemplate(xDatabaseName, xUserUnkid, xYearUnkid _
                                                                  , xCompanyUnkid, xPeriodStart, xPeriodEnd _
                                                                  , xOnlyApproved, xUserModeSetting)

            If Not IsNothing(objRpt) Then
                Call ReportExecute(objRpt, PrintAction, ExportAction, mstrExportReportPath, mblnOpenAfterExport)
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "generateReport", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setDefaultOrderBy(ByVal intReportType As Integer)
        Try
            mdtStartDate = Nothing
            mdtEndDate = Nothing
            mintViewIndex = -1
            mstrViewByIds = ""
            mstrViewByName = ""
            mstrAnalysis_Fields = ""
            mstrAnalysis_Join = ""
            mstrAnalysis_OrderBy = ""
            mstrReport_GroupName = ""
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "SetDefaultValue", mstrModuleName)
        End Try
    End Sub

    Public Overrides Sub setOrderBy(ByVal intReportType As Integer)

    End Sub

    Public Function Export_PPA_Activity(ByVal mdtTran As DataTable, ByVal sName As String, Optional ByVal mstrGroupColumns() As String = Nothing) As Boolean 'Pinkal (06-Mar-2014) -- Start [mstrGroupColumns]
        Try
            Dim row As WorksheetRow
            Dim wcell As WorksheetCell
            Dim rowsArrayFooter As New ArrayList



            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes
            Dim mdtTotal As DataTable = Nothing
            mdtTotal = New DataView(mdtTran, "EmpId <= 0", "", DataViewRowState.CurrentRows).ToTable
            mdtTran = New DataView(mdtTran, "EmpId > 0", "", DataViewRowState.CurrentRows).ToTable
            mdtTotal.Columns.Remove("EmpId")
            mdtTran.Columns.Remove("EmpId")

            If mstrGroupColumns IsNot Nothing AndAlso mstrGroupColumns.Length > 0 Then
                mdtTran.Columns.Remove("Id")
                mdtTran = New DataView(mdtTran, "", "GName asc", DataViewRowState.CurrentRows).ToTable
                mdtTotal.Columns.Remove("GName")
            End If

            'Pinkal (06-Mar-2014) -- End


            Dim intArrayColumnWidth As Integer() = Nothing
            ReDim intArrayColumnWidth(mdtTran.Columns.Count - 1)
            For i As Integer = 0 To intArrayColumnWidth.Length - 1
                Select Case i
                    Case Is < 2
                        intArrayColumnWidth(i) = 120
                    Case Else
                        intArrayColumnWidth(i) = 85
                End Select
            Next

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes
            'Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, "", True, mdtTran, intArrayColumnWidth, True, True, False, Nothing, sName, "", " ", Nothing, "", True, Nothing, Nothing, Nothing)


            '*******   REPORT FOOTER   ******
            row = New WorksheetRow()
            wcell = New WorksheetCell("", "s8w")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTran.Columns.Count - 1
            rowsArrayFooter.Add(row)
            '----------------------

            row = New WorksheetRow()
            wcell = New WorksheetCell(Language.getMessage(mstrModuleName, 3, "Activity Wise Total"), "s8b")
            row.Cells.Add(wcell)
            wcell.MergeAcross = mdtTran.Columns.Count - 1
            rowsArrayFooter.Add(row)


            For i As Integer = 0 To mdtTotal.Rows.Count - 1
                row = New WorksheetRow()
                For j As Integer = 0 To mdtTotal.Columns.Count - 2

                    If mdtTotal.Columns(j).ColumnName.StartsWith("Column") OrElse mdtTotal.Columns(j).DataType Is Type.GetType("System.Decimal") Then
                        wcell = New WorksheetCell(mdtTotal.Rows(i)(j).ToString(), DataType.Number, "n8")
                    Else
                        wcell = New WorksheetCell(mdtTotal.Rows(i)(j).ToString, DataType.String, "s8")
                    End If

                    If j = mdtTotal.Columns.Count - 2 Then
                        wcell.MergeAcross = 1
                    End If

                    row.Cells.Add(wcell)
                Next
                rowsArrayFooter.Add(row)
            Next


            Call ReportExecute(Nothing, enPrintAction.None, enExportAction.ExcelExtra, "", True, mdtTran, intArrayColumnWidth, True, True, False, mstrGroupColumns, sName, "", " ", Nothing, "", True, Nothing, rowsArrayFooter, Nothing, Nothing, False)
            'Pinkal (06-Mar-2014) -- End

            Return True
        Catch ex As Exception
            Throw New Exception("-1;" & ex.Message & ";" & mstrModuleName)
        End Try
    End Function


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    'Public Function ExportWeeklyPPATemplate() As CrystalDecisions.CrystalReports.Engine.ReportClass
    '    Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
    '    Dim StrQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim rpt_Data As ArutiReport.Designer.dsArutiReport
    '    Try

    '        Dim objDataOperation As New clsDataOperation

    '        StrQ = " SELECT ISNULL(hremployee_master.employeecode,'') AS Employeecode "

    '        If mblnFirstNamethenSurname = True Then
    '            StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee "
    '        Else
    '            StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS Employee "
    '        End If


    '        If mintViewIndex > 0 Then
    '            StrQ &= mstrAnalysis_Fields
    '        Else
    '            StrQ &= ", 0 AS Id, '' AS GName "
    '        End If


    '        StrQ &= " FROM  hremployee_master "

    '        StrQ &= mstrAnalysis_Join

    '        StrQ &= " WHERE  CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate ORDER BY ISNULL(hremployee_master.employeecode,'') "

    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
    '        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
    '        dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

    '        If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
    '            Me._FilterTitle = Language.getMessage(mstrModuleName, 1, "From Date : ") & mdtStartDate.Date & "  "
    '            Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "To Date : ") & mdtEndDate.Date
    '        End If


    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '        End If


    '        rpt_Data = New ArutiReport.Designer.dsArutiReport

    '        For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
    '            Dim rpt_Rows As DataRow
    '            rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
    '            rpt_Rows.Item("Column1") = dtRow.Item("Employeecode")
    '            rpt_Rows.Item("Column2") = dtRow.Item("Employee")
    '            rpt_Rows.Item("Column3") = dtRow.Item("GName")
    '            rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
    '        Next


    '        objRpt = New ArutiReport.Designer.rptWeeklyExportPPATemplate

    '        ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
    '        Dim arrImageRow As DataRow = Nothing
    '        arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

    '        ReportFunction.Logo_Display(objRpt, _
    '                                    ConfigParameter._Object._IsDisplayLogo, _
    '                                    ConfigParameter._Object._ShowLogoRightSide, _
    '                                    "arutiLogo1", _
    '                                    "arutiLogo2", _
    '                                    arrImageRow, _
    '                                    "txtCompanyName", _
    '                                    "txtReportName", _
    '                                    "txtFilterDescription", _
    '                                    ConfigParameter._Object._GetLeftMargin, _
    '                                    ConfigParameter._Object._GetRightMargin)

    '        rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

    '        If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
    '            rpt_Data.Tables("ArutiTable").Rows.Add("")
    '        End If

    '        If mintViewIndex <= 0 Then
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
    '            Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowPreparedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 4, "Prepared By :"))
    '            Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
    '        End If

    '        If ConfigParameter._Object._IsShowCheckedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 5, "Checked By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
    '        End If

    '        If ConfigParameter._Object._IsShowApprovedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 6, "Approved By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
    '        End If

    '        If ConfigParameter._Object._IsShowReceivedBy = True Then
    '            Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 7, "Received By :"))
    '        Else
    '            Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
    '        End If

    '        objRpt.SetDataSource(rpt_Data)

    '        For i As Integer = 0 To DateDiff(DateInterval.Day, mdtStartDate.Date, mdtEndDate.Date)
    '            Call ReportFunction.TextChange(objRpt, "txtDate" & i + 1, mdtStartDate.AddDays(i).Date)
    '            Call ReportFunction.TextChange(objRpt, "txtDay" & i + 1, WeekdayName(Weekday(mdtStartDate.AddDays(i).Date, FirstDayOfWeek.Sunday), False, FirstDayOfWeek.Sunday))
    '        Next

    '        Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 8, "Code"))
    '        Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 9, "Employee"))
    '        Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

    '        Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 10, "Printed By :"))
    '        Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))

    '        Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
    '        Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

    '        Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
    '        Call ReportFunction.TextChange(objRpt, "txtCompanyName", mstrCompanyName)
    '        Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

    '        Return objRpt
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "ExportWeeklyPPATemplate", mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function

    Public Function ExportWeeklyPPATemplate(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                                    , ByVal xCompanyUnkid As Integer, ByVal xStartDate As Date, ByVal xEndDate As Date _
                                                                    , ByVal xOnlyApproved As Boolean, ByVal xUserModeSetting As String) As CrystalDecisions.CrystalReports.Engine.ReportClass
        Dim objRpt As CrystalDecisions.CrystalReports.Engine.ReportClass = Nothing
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim rpt_Data As ArutiReport.Designer.dsArutiReport
        Try

            Dim objDataOperation As New clsDataOperation



            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xStartDate, xEndDate, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xEndDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xEndDate, xDatabaseName)

            StrQ = " SELECT ISNULL(hremployee_master.employeecode,'') AS Employeecode "

            If mblnFirstNamethenSurname = True Then
                StrQ &= ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee "
            Else

                StrQ &= ", ISNULL(hremployee_master.surname, '') + ' ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') AS Employee "
            End If


            If mintViewIndex > 0 Then
                StrQ &= mstrAnalysis_Fields
            Else
                StrQ &= ", 0 AS Id, '' AS GName "
            End If


            StrQ &= " FROM  hremployee_master "

            StrQ &= mstrAnalysis_Join


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If


            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'StrQ &= " WHERE  CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
            '        " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate ORDER BY ISNULL(hremployee_master.employeecode,'') "

            StrQ &= " WHERE 1 = 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                StrQ &= " AND " & xUACFiltrQry
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry
            End If

            'Pinkal (24-Aug-2015) -- End



            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If mdtStartDate <> Nothing AndAlso mdtEndDate <> Nothing Then
                Me._FilterTitle = Language.getMessage(mstrModuleName, 1, "From Date : ") & mdtStartDate.Date & "  "
                Me._FilterTitle &= Language.getMessage(mstrModuleName, 2, "To Date : ") & mdtEndDate.Date
            End If


            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            End If


            rpt_Data = New ArutiReport.Designer.dsArutiReport

            For Each dtRow As DataRow In dsList.Tables("DataTable").Rows
                Dim rpt_Rows As DataRow
                rpt_Rows = rpt_Data.Tables("ArutiTable").NewRow
                rpt_Rows.Item("Column1") = dtRow.Item("Employeecode")
                rpt_Rows.Item("Column2") = dtRow.Item("Employee")
                rpt_Rows.Item("Column3") = dtRow.Item("GName")
                rpt_Data.Tables("ArutiTable").Rows.Add(rpt_Rows)
            Next


            objRpt = New ArutiReport.Designer.rptWeeklyExportPPATemplate

            ConfigParameter._Object.GetReportSettings(CInt(mstrReportId))
            Dim arrImageRow As DataRow = Nothing
            arrImageRow = rpt_Data.Tables("ArutiImage").NewRow()

            ReportFunction.Logo_Display(objRpt, _
                                        ConfigParameter._Object._IsDisplayLogo, _
                                        ConfigParameter._Object._ShowLogoRightSide, _
                                        "arutiLogo1", _
                                        "arutiLogo2", _
                                        arrImageRow, _
                                        "txtCompanyName", _
                                        "txtReportName", _
                                        "txtFilterDescription", _
                                        ConfigParameter._Object._GetLeftMargin, _
                                        ConfigParameter._Object._GetRightMargin)

            rpt_Data.Tables("ArutiImage").Rows.Add(arrImageRow)

            If rpt_Data.Tables("ArutiTable").Rows.Count <= 0 Then
                rpt_Data.Tables("ArutiTable").Rows.Add("")
            End If

            If mintViewIndex <= 0 Then
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupHeaderSection1", True)
                Call ReportFunction.EnableSuppressSection(objRpt, "GroupFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowPreparedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblPreparedBy", Language.getMessage(mstrModuleName, 4, "Prepared By :"))
                Call ReportFunction.TextChange(objRpt, "txtUPreparedBy", User._Object._Username)
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "Section4", True)
            End If

            If ConfigParameter._Object._IsShowCheckedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblCheckedBy", Language.getMessage(mstrModuleName, 5, "Checked By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection1", True)
            End If

            If ConfigParameter._Object._IsShowApprovedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblApprovedBy", Language.getMessage(mstrModuleName, 6, "Approved By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection2", True)
            End If

            If ConfigParameter._Object._IsShowReceivedBy = True Then
                Call ReportFunction.TextChange(objRpt, "lblReceivedBy", Language.getMessage(mstrModuleName, 7, "Received By :"))
            Else
                Call ReportFunction.EnableSuppressSection(objRpt, "ReportFooterSection3", True)
            End If

            objRpt.SetDataSource(rpt_Data)

            For i As Integer = 0 To DateDiff(DateInterval.Day, mdtStartDate.Date, mdtEndDate.Date)
                Call ReportFunction.TextChange(objRpt, "txtDate" & i + 1, mdtStartDate.AddDays(i).Date)
                Call ReportFunction.TextChange(objRpt, "txtDay" & i + 1, WeekdayName(Weekday(mdtStartDate.AddDays(i).Date, FirstDayOfWeek.Sunday), False, FirstDayOfWeek.Sunday))
            Next

            Call ReportFunction.TextChange(objRpt, "txtEmpCode", Language.getMessage(mstrModuleName, 8, "Code"))
            Call ReportFunction.TextChange(objRpt, "txtEmpName", Language.getMessage(mstrModuleName, 9, "Employee"))
            Call ReportFunction.TextChange(objRpt, "txtGroupName", mstrReport_GroupName)

            Call ReportFunction.TextChange(objRpt, "lblPrintedBy", Language.getMessage(mstrModuleName, 10, "Printed By :"))
            Call ReportFunction.TextChange(objRpt, "lblPrintedDate", Language.getMessage(mstrModuleName, 11, "Printed Date :"))

            Call ReportFunction.TextChange(objRpt, "txtPrintedDate", Me._PrintDate)
            Call ReportFunction.TextChange(objRpt, "txtPrintedBy", Me._UserName)

            Call ReportFunction.TextChange(objRpt, "txtReportName", Me._ReportName)
            Call ReportFunction.TextChange(objRpt, "txtCompanyName", mstrCompanyName)
            Call ReportFunction.TextChange(objRpt, "txtFilterDescription", Me._FilterTitle)

            Return objRpt
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportWeeklyPPATemplate", mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (24-Aug-2015) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "From Date :")
            Language.setMessage(mstrModuleName, 2, "To Date :")
            Language.setMessage(mstrModuleName, 3, "Activity Wise Total")
            Language.setMessage(mstrModuleName, 4, "Prepared By :")
            Language.setMessage(mstrModuleName, 5, "Checked By :")
            Language.setMessage(mstrModuleName, 6, "Approved By :")
            Language.setMessage(mstrModuleName, 7, "Received By :")
            Language.setMessage(mstrModuleName, 8, "Code")
            Language.setMessage(mstrModuleName, 9, "Employee")
            Language.setMessage(mstrModuleName, 10, "Printed By :")
            Language.setMessage(mstrModuleName, 11, "Printed Date :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


End Class
