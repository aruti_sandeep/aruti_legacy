﻿'************************************************************************************************************************************
'Class Name : clsexpense_category_master.vb
'Purpose    :
'Date       :24-Jun-2024
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsexpense_category_master
    Private Shared ReadOnly mstrModuleName As String = "clsexpense_category_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintCategoryunkid As Integer
    Private mstrCategorycode As String = String.Empty
    Private mstrCategoryname As String = String.Empty
    Private mblnIspredefine As Boolean
    Private mblnIsactive As Boolean = True
    Private mstrCategoryname1 As String = String.Empty
    Private mstrCategoryname2 As String = String.Empty
    Private mintUserunkid As Integer = 0
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mblnIsWeb As Boolean = False
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Categoryunkid() As Integer
        Get
            Return mintCategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintCategoryunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categorycode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Categorycode() As String
        Get
            Return mstrCategorycode
        End Get
        Set(ByVal value As String)
            mstrCategorycode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Categoryname() As String
        Get
            Return mstrCategoryname
        End Get
        Set(ByVal value As String)
            mstrCategoryname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ispredefine
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ispredefine() As Boolean
        Get
            Return mblnIspredefine
        End Get
        Set(ByVal value As Boolean)
            mblnIspredefine = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Categoryname1() As String
        Get
            Return mstrCategoryname1
        End Get
        Set(ByVal value As String)
            mstrCategoryname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Categoryname2() As String
        Get
            Return mstrCategoryname2
        End Get
        Set(ByVal value As String)
            mstrCategoryname2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webclientip
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webhostname
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

#End Region

    Public Sub New()
        Dim objDataOperation As clsDataOperation = Nothing
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
            objDataOperation.ExecNonQuery("INSERT INTO cmexpense_category_master (categorycode, categoryname, ispredefine, isactive,categoryname1,categoryname2) SELECT 'Leave','Leave',1,1,'Leave','Leave' WHERE NOT EXISTS (SELECT * FROM cmexpense_category_master WHERE categoryunkid = " & enExpenseType.EXP_LEAVE & ")")
            objDataOperation.ExecNonQuery("INSERT INTO cmexpense_category_master (categorycode, categoryname, ispredefine, isactive,categoryname1,categoryname2) SELECT 'Medical','Medical',1,1,'Medical','Medical' WHERE NOT EXISTS (SELECT * FROM cmexpense_category_master WHERE categoryunkid = " & enExpenseType.EXP_MEDICAL & ")")
            objDataOperation.ExecNonQuery("INSERT INTO cmexpense_category_master (categorycode, categoryname, ispredefine, isactive,categoryname1,categoryname2) SELECT 'Training','Training',1,1,'Training','Training' WHERE NOT EXISTS (SELECT * FROM cmexpense_category_master WHERE categoryunkid = " & enExpenseType.EXP_TRAINING & ")")
            objDataOperation.ExecNonQuery("INSERT INTO cmexpense_category_master (categorycode, categoryname, ispredefine, isactive,categoryname1,categoryname2) SELECT 'Miscellaneous','Miscellaneous',1,1,'Miscellaneous','Miscellaneous' WHERE NOT EXISTS (SELECT * FROM cmexpense_category_master WHERE categoryunkid = " & enExpenseType.EXP_MISCELLANEOUS & ")")
            objDataOperation.ExecNonQuery("INSERT INTO cmexpense_category_master (categorycode, categoryname, ispredefine, isactive,categoryname1,categoryname2) SELECT 'Imprest','Imprest',1,1,'Imprest','Imprest' WHERE NOT EXISTS (SELECT * FROM cmexpense_category_master WHERE categoryunkid = " & enExpenseType.EXP_IMPREST & ")")
            objDataOperation.ExecNonQuery("INSERT INTO cmexpense_category_master (categorycode, categoryname, ispredefine, isactive,categoryname1,categoryname2) SELECT 'Rebate Privilege','Rebate Privilege',1,1,'Rebate Privilege','Rebate Privilege' WHERE NOT EXISTS (SELECT * FROM cmexpense_category_master WHERE categoryunkid = " & enExpenseType.EXP_REBATE_PRIVILEGE & ")")
            objDataOperation.ExecNonQuery("INSERT INTO cmexpense_category_master (categorycode, categoryname, ispredefine, isactive,categoryname1,categoryname2) SELECT 'Rebate Duty','Rebate Duty',1,1,'Rebate Duty','Rebate Duty' WHERE NOT EXISTS (SELECT * FROM cmexpense_category_master WHERE categoryunkid = " & enExpenseType.EXP_REBATE_DUTY & ")")
            objDataOperation.ExecNonQuery("INSERT INTO cmexpense_category_master (categorycode, categoryname, ispredefine, isactive,categoryname1,categoryname2) SELECT 'Rebate Privilege Dependant','Rebate Privilege Dependant',1,1,'Rebate Privilege Dependant','Rebate Privilege Dependant' WHERE NOT EXISTS (SELECT * FROM cmexpense_category_master WHERE categoryunkid = " & enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT & ")")
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  categoryunkid " & _
                      ", categorycode " & _
                      ", categoryname " & _
                      ", ispredefine " & _
                      ", isactive " & _
                      ", categoryname1 " & _
                      ", categoryname2 " & _
                      " FROM cmexpense_category_master " & _
                      " WHERE categoryunkid = @categoryunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCategoryunkid = CInt(dtRow.Item("categoryunkid"))
                mstrCategorycode = dtRow.Item("categorycode").ToString
                mstrCategoryname = dtRow.Item("categoryname").ToString
                mblnIspredefine = CBool(dtRow.Item("ispredefine"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrCategoryname1 = dtRow.Item("categoryname1").ToString
                mstrCategoryname2 = dtRow.Item("categoryname2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  categoryunkid " & _
                      ", categorycode " & _
                      ", categoryname " & _
                      ", ispredefine " & _
                      ", isactive " & _
                      ", categoryname1 " & _
                      ", categoryname2 " & _
                      " FROM cmexpense_category_master " & _
                      " WHERE 1 = 1"

            If blnOnlyActive Then
                strQ &= " AND isactive = 1 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cmexpense_category_master) </purpose>
    Public Function Insert(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        If isExist(mstrCategorycode, "", mintCategoryunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Expense Category Code is already defined. Please define new Expense Category Code.")
            Return False
        ElseIf isExist("", mstrCategoryname, mintCategoryunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Expense Category Name is already defined. Please define new Expense Category Name.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categorycode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategorycode.ToString)
            objDataOperation.AddParameter("@categoryname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategoryname.ToString)
            objDataOperation.AddParameter("@ispredefine", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspredefine.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@categoryname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategoryname1.ToString)
            objDataOperation.AddParameter("@categoryname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategoryname2.ToString)

            strQ = "INSERT INTO cmexpense_category_master ( " & _
                      "  categorycode " & _
                      ", categoryname " & _
                      ", ispredefine " & _
                      ", isactive " & _
                      ", categoryname1 " & _
                      ", categoryname2" & _
                    ") VALUES (" & _
                      "  @categorycode " & _
                      ", @categoryname " & _
                      ", @ispredefine " & _
                      ", @isactive " & _
                      ", @categoryname1 " & _
                      ", @categoryname2" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCategoryunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForExpenseCategory(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cmexpense_category_master) </purpose>
    Public Function Update(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        If isExist(mstrCategorycode, "", mintCategoryunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Expense Category Code is already defined. Please define new Expense Category Code.")
            Return False
        ElseIf isExist("", mstrCategoryname, mintCategoryunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Expense Category Name is already defined. Please define new Expense Category Name.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid.ToString)
            objDataOperation.AddParameter("@categorycode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategorycode.ToString)
            objDataOperation.AddParameter("@categoryname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategoryname.ToString)
            objDataOperation.AddParameter("@ispredefine", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspredefine.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@categoryname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategoryname1.ToString)
            objDataOperation.AddParameter("@categoryname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategoryname2.ToString)

            strQ = "UPDATE cmexpense_category_master SET " & _
                      "  categorycode = @categorycode" & _
                      ", categoryname = @categoryname" & _
                      ", ispredefine = @ispredefine" & _
                      ", isactive = @isactive" & _
                      ", categoryname1 = @categoryname1" & _
                      ", categoryname2 = @categoryname2 " & _
                      " WHERE categoryunkid = @categoryunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForExpenseCategory(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cmexpense_category_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation = Nothing
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = " UPDATE cmexpense_category_master set isactive = 0 " & _
                      " WHERE categoryunkid = @categoryunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ClearParameters()
            mintCategoryunkid = intUnkid
            GetData(objDataOperation)

            If InsertAuditTrailForExpenseCategory(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  categoryunkid " & _
                      ", categorycode " & _
                      ", categoryname " & _
                      ", ispredefine " & _
                      ", isactive " & _
                      ", categoryname1 " & _
                      ", categoryname2 " & _
                      " FROM cmexpense_category_master " & _
                      " WHERE 1 = 1 "

            If strCode.Length > 0 Then
                strQ &= " AND categorycode = @categorycode "
            End If
            If strName.Length > 0 Then
                strQ &= " AND categoryname = @categoryname "
            End If


            If intUnkid > 0 Then
                strQ &= " AND categoryunkid <> @categoryunkid"
            End If

            objDataOperation.AddParameter("@categorycode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@categoryname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (stapproverlevel_master) </purpose>
    Public Function InsertAuditTrailForExpenseCategory(ByVal objDoOperation As clsDataOperation, ByVal intAuditType As enAuditType) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDoOperation.ClearParameters()
            objDoOperation.AddParameter("@categoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategoryunkid)
            objDoOperation.AddParameter("@categorycode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategorycode.ToString)
            objDoOperation.AddParameter("@categoryname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategoryname.ToString)
            objDoOperation.AddParameter("@ispredefine", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspredefine.ToString)
            objDoOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDoOperation.AddParameter("@categoryname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategoryname1.ToString)
            objDoOperation.AddParameter("@categoryname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCategoryname2.ToString)
            objDoOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDoOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDoOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP.Trim))
            objDoOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName.Trim))
            objDoOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebFormName)
            objDoOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = " INSERT INTO atcmexpense_category_master ( " & _
                      "  categoryunkid " & _
                      ", categorycode " & _
                      ", categoryname " & _
                      ", ispredefine " & _
                      ", isactive " & _
                      ", categoryname1 " & _
                      ", categoryname2" & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name" & _
                      ", form_name " & _
                      ", isweb" & _
                      " ) VALUES (" & _
                      "  @categoryunkid " & _
                      ", @categorycode " & _
                      ", @categoryname " & _
                      ", @ispredefine " & _
                      ", @isactive " & _
                      ", @categoryname1 " & _
                      ", @categoryname2" & _
                       ",@audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @machine_name" & _
                      ", @form_name " & _
                      ", @isweb" & _
                      " ); SELECT @@identity"

            dsList = objDoOperation.ExecQuery(strQ, "List")

            If objDoOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (stapproverlevel_master) </purpose>
    Public Function GetExpenseCategory(ByVal mstrDataBaseName As String, Optional ByVal IncludeMedical As Boolean = False, Optional ByVal IncludeTraining As Boolean = False, _
                                                        Optional ByVal iFlag As Boolean = False, _
                                                        Optional ByVal iList As String = "List", Optional ByVal IncludeMiscellenous As Boolean = False, _
                                                        Optional ByVal blnIncludeImprest As Boolean = False, _
                                                        Optional ByVal blnIncludeRebatePrivilege As Boolean = True, _
                                                        Optional ByVal blnIncludeRebateDuty As Boolean = True, _
                                                        Optional ByVal blnIncludeRebatePrivilegeDpndt As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        Dim strGrpName As String = String.Empty
        Dim objGrp As New clsGroup_Master
        objGrp._Groupunkid = 1
        strGrpName = objGrp._Groupname
        objGrp = Nothing
        Try

            If iFlag Then
                strQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            strQ &= " SELECT categoryunkid AS Id , categoryname AS Name FROM " & mstrDataBaseName & "..cmexpense_category_master WHERE isactive = 1 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim drRow() As DataRow = Nothing

            If IncludeMedical = False Then
                drRow = Nothing
                drRow = dsList.Tables(0).Select("Id = " & enExpenseType.EXP_MEDICAL)
                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                End If
            End If

            If IncludeTraining = False Then
                drRow = Nothing
                drRow = dsList.Tables(0).Select("Id = " & enExpenseType.EXP_TRAINING)
                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                End If
            End If

            If IncludeMiscellenous = False Then
                drRow = Nothing
                drRow = dsList.Tables(0).Select("Id = " & enExpenseType.EXP_MISCELLANEOUS)
                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                End If
            End If

            If blnIncludeImprest = False Then
                drRow = Nothing
                drRow = dsList.Tables(0).Select("Id = " & enExpenseType.EXP_IMPREST)
                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                End If
            End If

            If strGrpName.ToUpper() <> "PW" Then

                drRow = Nothing
                drRow = dsList.Tables(0).Select("Id = " & enExpenseType.EXP_REBATE_PRIVILEGE)
                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                End If

                drRow = Nothing
                drRow = dsList.Tables(0).Select("Id = " & enExpenseType.EXP_REBATE_DUTY)
                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                End If

                drRow = Nothing
                drRow = dsList.Tables(0).Select("Id = " & enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT)
                If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                    dsList.Tables(0).Rows.Remove(drRow(0))
                End If

            ElseIf strGrpName.ToUpper() = "PW" Then

                If blnIncludeRebatePrivilege = False Then
                    drRow = Nothing
                    drRow = dsList.Tables(0).Select("Id = " & enExpenseType.EXP_REBATE_PRIVILEGE)
                    If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                        dsList.Tables(0).Rows.Remove(drRow(0))
                    End If
                End If

                If blnIncludeRebateDuty = False Then
                    drRow = Nothing
                    drRow = dsList.Tables(0).Select("Id = " & enExpenseType.EXP_REBATE_DUTY)
                    If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                        dsList.Tables(0).Rows.Remove(drRow(0))
                    End If
                End If

                If blnIncludeRebatePrivilegeDpndt = False Then
                    drRow = Nothing
                    drRow = dsList.Tables(0).Select("Id = " & enExpenseType.EXP_REBATE_PRIVILEGE_DPNDT)
                    If drRow IsNot Nothing AndAlso drRow.Length > 0 Then
                        dsList.Tables(0).Rows.Remove(drRow(0))
                    End If
                End If

            End If

            dsList.AcceptChanges()

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExpenseCategory; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Expense Category Code is already defined. Please define new Expense Category Code.")
            Language.setMessage(mstrModuleName, 2, "This Expense Category Name is already defined. Please define new Expense Category Name.")
            Language.setMessage(mstrModuleName, 3, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class