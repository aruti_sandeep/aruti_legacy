﻿Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Gajanan
''' </summary>
Public Class clsAsset_businessdealT2depn_tran
    Private Const mstrModuleName = "clsAsset_businessdealT2depn_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintassetbusinessdealt2depntranunkid As Integer
    Private mintassetdeclarationt2unkid As Integer
    Private mintassetsectorunkid As Integer = 0
    Private mstrcompany_org_name As String = String.Empty
    Private mstrcompany_tin_no As String = String.Empty
    Private mdtregistration_date As DateTime
    Private mstraddress_location As String = String.Empty
    Private mstrbusiness_type As String = String.Empty
    Private mstrposition_held As String = String.Empty
    Private mintissupplierclient As Integer = 0
    Private mintcountryunkid As Integer = 0
    Private mintcurrencyunkid As Integer = 0
    Private mintbasecurrencyunkid As Integer = 0
    Private mdecbaseexchangerate As Decimal = 0
    Private mdecexchangerate As Decimal = 0
    Private mdecmonthlyannualearnings As Decimal = 0
    Private mdecbasemonthlyannualearnings As Decimal = 0
    Private mstrbusiness_contact_no As String = String.Empty
    Private mstrshareholders_names As String = String.Empty
    Private mblnisfinalsaved As Boolean = False
    Private mdttransactiondate As DateTime
    Private mintuserunkid As Integer = 0
    Private mblnisvoid As Boolean = False
    Private mintvoiduserunkid As Integer = 0
    Private mdtvoiddatetime As DateTime
    Private mstrvoidreason As String = String.Empty

    Private minAuditUserid As Integer = 0
    Private minAuditDate As DateTime
    Private minClientIp As String = String.Empty
    Private minloginemployeeunkid As Integer = 0
    Private mstrHostName As String = String.Empty
    Private mstrFormName As String = String.Empty
    Private blnIsFromWeb As Boolean = False

    Private mdtTable As DataTable
    Private mstrDatabaseName As String = String.Empty
    'Hemant (03 Dec 2018) -- Start
    'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
    Private mintDependentunkid As Integer
    Private mintRelationshipunkid As Integer
    'Hemant (03 Dec 2018) -- End
#End Region

#Region " Properties "
    Public Property _Datasource() As DataTable
        Get
            Return mdtTable
        End Get
        Set(ByVal value As DataTable)
            mdtTable = value
        End Set
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public Property _Assetbusinessdealt2depntranunkid() As Integer
        Get
            Return mintassetbusinessdealt2depntranunkid
        End Get
        Set(ByVal value As Integer)
            mintassetbusinessdealt2depntranunkid = value
        End Set
    End Property

    Public Property _Assetdeclarationt2unkid(ByVal xDatabase As String) As Integer
        Get
            Return mintassetdeclarationt2unkid
        End Get
        Set(ByVal value As Integer)
            mintassetdeclarationt2unkid = value
            Call GetData(xDatabase)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assetsectorunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Assetsectorunkid() As Integer
        Get
            Return mintAssetsectorunkid
        End Get
        Set(ByVal value As Integer)
            mintAssetsectorunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set company_org_name
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Company_Org_Name() As String
        Get
            Return mstrCompany_Org_Name
        End Get
        Set(ByVal value As String)
            mstrCompany_Org_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set company_tin_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Company_Tin_No() As String
        Get
            Return mstrCompany_Tin_No
        End Get
        Set(ByVal value As String)
            mstrCompany_Tin_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set registration_date
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Registration_Date() As Date
        Get
            Return mdtRegistration_Date
        End Get
        Set(ByVal value As Date)
            mdtRegistration_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address_location
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Address_Location() As String
        Get
            Return mstrAddress_Location
        End Get
        Set(ByVal value As String)
            mstrAddress_Location = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set business_type
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Business_Type() As String
        Get
            Return mstrBusiness_Type
        End Get
        Set(ByVal value As String)
            mstrBusiness_Type = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set position_held
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Position_Held() As String
        Get
            Return mstrPosition_Held
        End Get
        Set(ByVal value As String)
            mstrPosition_Held = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set is_supplier_client
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Is_Supplier_Client() As Integer
        Get
            Return mintissupplierclient
        End Get
        Set(ByVal value As Integer)
            mintissupplierclient = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Currencyunkid() As Integer
        Get
            Return mintCurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintCurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set basecurrencyunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Basecurrencyunkid() As Integer
        Get
            Return mintBasecurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintBasecurrencyunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set baseexchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Baseexchangerate() As Decimal
        Get
            Return mdecbaseexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecbaseexchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set exchangerate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Exchangerate() As Decimal
        Get
            Return mdecexchangerate
        End Get
        Set(ByVal value As Decimal)
            mdecexchangerate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set monthly_annual_earnings
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Monthly_Annual_Earnings() As Decimal
        Get
            Return mdecmonthlyannualearnings
        End Get
        Set(ByVal value As Decimal)
            mdecmonthlyannualearnings = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set basemonthly_annual_earnings
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Basemonthly_Annual_Earnings() As Decimal
        Get
            Return mdecbasemonthlyannualearnings
        End Get
        Set(ByVal value As Decimal)
            mdecbasemonthlyannualearnings = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set business_contact_no
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Business_Contact_No() As String
        Get
            Return mstrBusiness_Contact_No
        End Get
        Set(ByVal value As String)
            mstrBusiness_Contact_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shareholders_names
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Shareholders_Names() As String
        Get
            Return mstrShareholders_Names
        End Get
        Set(ByVal value As String)
            mstrShareholders_Names = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinalsaved
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isfinalsaved() As Boolean
        Get
            Return mblnIsfinalsaved
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinalsaved = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dependantunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Dependantunkid() As Integer
        Get
            Return mintDependentunkid
        End Get
        Set(ByVal value As Integer)
            mintDependentunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set relationshipunkid
    ''' Modify By: Gajanan
    ''' </summary>
    Public Property _Relationshipunkid() As Integer
        Get
            Return mintRelationshipunkid
        End Get
        Set(ByVal value As Integer)
            mintRelationshipunkid = Value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintuserunkid
        End Get
        Set(ByVal value As Integer)
            mintuserunkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public Property _AuditUserid() As Integer
        Get
            Return minAuditUserid
        End Get
        Set(ByVal value As Integer)
            minAuditUserid = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return minAuditDate
        End Get
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public Property _ClientIp() As String
        Get
            Return minClientIp
        End Get
        Set(ByVal value As String)
            minClientIp = value
        End Set
    End Property

    Public Property _Loginemployeeunkid() As Integer
        Get
            Return minloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            minloginemployeeunkid = value
        End Set
    End Property

    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTable = New DataTable("BusinessdealT2depn")

        Try
            mdtTable.Columns.Add("assetbusinessdealt2depntranunkid", System.Type.GetType("System.Int32")).DefaultValue = -2
            mdtTable.Columns.Add("assetdeclarationt2unkid", System.Type.GetType("System.Int32")).DefaultValue = -2
            mdtTable.Columns.Add("assetsectorunkid", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("company_org_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("company_tin_no", System.Type.GetType("System.String")).DefaultValue = 0
            mdtTable.Columns.Add("registration_date", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("address_location", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("business_type", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("position_held", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("is_supplier_client", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("countryunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("currencyunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("basecurrencyunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("baseexchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("exchangerate", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("monthly_annual_earnings", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("basemonthly_annual_earnings", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("business_contact_no", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("shareholders_names", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("isfinalsaved", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("transactiondate", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("userunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            mdtTable.Columns.Add("dependantunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTable.Columns.Add("relationshipunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            'Hemant (03 Dec 2018) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    Public Sub GetData(ByVal xDatabase As String)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        objDataOperation = New clsDataOperation

        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
              "  assetbusinessdealt2depntranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", assetsectorunkid " & _
              ", company_org_name " & _
              ", company_tin_no " & _
              ", ISNULL(registration_date, Getdate()) AS registration_date  " & _
              ", address_location " & _
              ", business_type " & _
              ", position_held " & _
              ", is_supplier_client " & _
              ", countryunkid " & _
              ", ISNULL(currencyunkid, 0) as currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) as basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) as baseexchangerate " & _
              ", ISNULL(exchangerate, 0) as exchangerate " & _
              ", ISNULL(monthly_annual_earnings, 0) as monthly_annual_earnings " & _
              ", ISNULL(basemonthly_annual_earnings, 0) as basemonthly_annual_earnings " & _
              ", business_contact_no " & _
              ", shareholders_names " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", ISNULL(dependantunkid, 0) as dependantunkid " & _
              ", ISNULL(relationshipunkid, 0) as relationshipunkid " & _
             "FROM " & mstrDatabaseName & "..hrasset_businessdealT2depn_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            'Hemant (03 Dec 2018) -- [dependantunkid,relationshipunkid]
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetdeclarationt2unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()
                drRow.Item("assetbusinessdealt2depntranunkid") = CInt(dtRow.Item("assetbusinessdealt2depntranunkid"))
                drRow.Item("assetdeclarationt2unkid") = CInt(dtRow.Item("assetdeclarationt2unkid"))
                drRow.Item("assetsectorunkid") = dtRow.Item("assetsectorunkid").ToString
                drRow.Item("company_org_name") = dtRow.Item("company_org_name").ToString
                drRow.Item("company_tin_no") = dtRow.Item("company_tin_no").ToString
                If IsDBNull(dtRow.Item("registration_date")) = True Then
                    drRow.Item("registration_date") = DBNull.Value
                Else
                    drRow.Item("registration_date") = dtRow.Item("registration_date")
                End If
                drRow.Item("address_location") = dtRow.Item("address_location").ToString
                drRow.Item("business_type") = dtRow.Item("business_type").ToString
                drRow.Item("position_held") = dtRow.Item("position_held").ToString
                drRow.Item("is_supplier_client") = CInt(dtRow.Item("is_supplier_client"))
                drRow.Item("countryunkid") = CInt(dtRow.Item("countryunkid"))
                drRow.Item("currencyunkid") = CInt(dtRow.Item("currencyunkid"))
                drRow.Item("basecurrencyunkid") = CInt(dtRow.Item("basecurrencyunkid"))
                drRow.Item("baseexchangerate") = CDec(dtRow.Item("baseexchangerate"))
                drRow.Item("exchangerate") = CDec(dtRow.Item("exchangerate"))
                drRow.Item("monthly_annual_earnings") = CDec(dtRow.Item("monthly_annual_earnings"))
                drRow.Item("basemonthly_annual_earnings") = CDec(dtRow.Item("basemonthly_annual_earnings"))
                drRow.Item("business_contact_no") = dtRow.Item("business_contact_no").ToString
                drRow.Item("shareholders_names") = dtRow.Item("shareholders_names").ToString
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved").ToString)
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    drRow.Item("voiddatetime") = DBNull.Value
                Else
                    drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                End If
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                drRow.Item("AUD") = dtRow.Item("AUD").ToString
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                drRow.Item("dependantunkid") = CInt(dtRow.Item("dependantunkid"))
                drRow.Item("relationshipunkid") = CInt(dtRow.Item("relationshipunkid"))
                'Hemant (03 Dec 2018) -- End
                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2
    Public Sub GetData(ByVal xDatabase As String, ByVal intAssetdeclarationt2Unkid As Integer, ByVal xDataOp As clsDataOperation)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drRow As DataRow

        'Gajanan (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Gajanan (14 Nov 2018) -- End


        mintassetdeclarationt2unkid = intAssetdeclarationt2Unkid

        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
              "  assetbusinessdealt2depntranunkid " & _
              ", assetdeclarationt2unkid " & _
              ", assetsectorunkid " & _
              ", company_org_name " & _
              ", company_tin_no " & _
              ", ISNULL(registration_date, Getdate()) AS registration_date  " & _
              ", address_location " & _
              ", business_type " & _
              ", position_held " & _
              ", is_supplier_client " & _
              ", countryunkid " & _
              ", ISNULL(currencyunkid, 0) as currencyunkid " & _
              ", ISNULL(basecurrencyunkid, 0) as basecurrencyunkid " & _
              ", ISNULL(baseexchangerate, 0) as baseexchangerate " & _
              ", ISNULL(exchangerate, 0) as exchangerate " & _
              ", ISNULL(monthly_annual_earnings, 0) as monthly_annual_earnings " & _
              ", ISNULL(basemonthly_annual_earnings, 0) as basemonthly_annual_earnings " & _
              ", business_contact_no " & _
              ", shareholders_names " & _
              ", isfinalsaved " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' AS AUD " & _
              ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
              ", ISNULL(dependantunkid, 0) as dependantunkid " & _
              ", ISNULL(relationshipunkid, 0) as relationshipunkid " & _
             "FROM " & mstrDatabaseName & "..hrasset_businessdealT2depn_tran " & _
             "WHERE ISNULL(isvoid, 0 ) = 0 " & _
             "AND assetdeclarationt2unkid = @assetdeclarationt2unkid "

            'Hemant (03 Dec 2018) -- [dependantunkid,relationshipunkid]
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetdeclarationt2unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drRow = mdtTable.NewRow()
                drRow.Item("assetbusinessdealt2depntranunkid") = CInt(dtRow.Item("assetbusinessdealt2depntranunkid"))
                drRow.Item("assetdeclarationt2unkid") = CInt(dtRow.Item("assetdeclarationt2unkid"))
                drRow.Item("assetsectorunkid") = dtRow.Item("assetsectorunkid").ToString
                drRow.Item("company_org_name") = dtRow.Item("company_org_name").ToString
                drRow.Item("company_tin_no") = dtRow.Item("company_tin_no").ToString
                If IsDBNull(dtRow.Item("registration_date")) = True Then
                    drRow.Item("registration_date") = DBNull.Value
                Else
                    drRow.Item("registration_date") = dtRow.Item("registration_date")
                End If
                drRow.Item("address_location") = dtRow.Item("address_location").ToString
                drRow.Item("business_type") = dtRow.Item("business_type").ToString
                drRow.Item("position_held") = dtRow.Item("position_held").ToString
                drRow.Item("is_supplier_client") = CInt(dtRow.Item("is_supplier_client"))
                drRow.Item("countryunkid") = CInt(dtRow.Item("countryunkid"))
                drRow.Item("currencyunkid") = CInt(dtRow.Item("currencyunkid"))
                drRow.Item("basecurrencyunkid") = CInt(dtRow.Item("basecurrencyunkid"))
                drRow.Item("baseexchangerate") = CDec(dtRow.Item("baseexchangerate"))
                drRow.Item("exchangerate") = CDec(dtRow.Item("exchangerate"))
                drRow.Item("monthly_annual_earnings") = CDec(dtRow.Item("monthly_annual_earnings"))
                drRow.Item("basemonthly_annual_earnings") = CDec(dtRow.Item("basemonthly_annual_earnings"))
                drRow.Item("business_contact_no") = dtRow.Item("business_contact_no").ToString
                drRow.Item("shareholders_names") = dtRow.Item("shareholders_names").ToString
                drRow.Item("isfinalsaved") = CBool(dtRow.Item("isfinalsaved").ToString)
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    drRow.Item("transactiondate") = DBNull.Value
                Else
                    drRow.Item("transactiondate") = dtRow.Item("transactiondate")
                End If
                drRow.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    drRow.Item("voiddatetime") = DBNull.Value
                Else
                    drRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                End If
                drRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                drRow.Item("AUD") = dtRow.Item("AUD").ToString
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                drRow.Item("dependantunkid") = CInt(dtRow.Item("dependantunkid"))
                drRow.Item("relationshipunkid") = CInt(dtRow.Item("relationshipunkid"))
                'Hemant (03 Dec 2018) -- End

                mdtTable.Rows.Add(drRow)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan (14 Nov 2018) -- End
        End Try
    End Sub
    'Gajanan (14 Nov 2018) -- End

    'Hemant (14 Nov 2018) -- End

    'Gajanan (07 Dec 2018) -- Start
    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
    Public Sub GetDataByUnkId(ByVal xDatabase As String, ByVal intAssetBusinessDealT2DepntranUnkid As Integer, ByVal xDataOp As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        mstrDatabaseName = xDatabase

        Try
            strQ = "SELECT " & _
               "  assetbusinessdealt2depntranunkid " & _
               ", assetdeclarationt2unkid " & _
               ", assetsectorunkid " & _
               ", company_org_name " & _
               ", company_tin_no " & _
               ", ISNULL(registration_date, Getdate()) AS registration_date  " & _
               ", address_location " & _
               ", business_type " & _
               ", position_held " & _
               ", is_supplier_client " & _
               ", countryunkid " & _
               ", ISNULL(currencyunkid, 0) as currencyunkid " & _
               ", ISNULL(basecurrencyunkid, 0) as basecurrencyunkid " & _
               ", ISNULL(baseexchangerate, 0) as baseexchangerate " & _
               ", ISNULL(exchangerate, 0) as exchangerate " & _
               ", ISNULL(monthly_annual_earnings, 0) as monthly_annual_earnings " & _
               ", ISNULL(basemonthly_annual_earnings, 0) as basemonthly_annual_earnings " & _
               ", business_contact_no " & _
               ", shareholders_names " & _
               ", isfinalsaved " & _
               ", userunkid " & _
               ", isvoid " & _
               ", voiduserunkid " & _
               ", voiddatetime " & _
               ", voidreason " & _
               ", '' AS AUD " & _
               ", ISNULL(transactiondate, Getdate()) AS transactiondate " & _
               ", ISNULL(dependantunkid, 0) as dependantunkid " & _
               ", ISNULL(relationshipunkid, 0) as relationshipunkid " & _
              "FROM " & mstrDatabaseName & "..hrasset_businessdealT2depn_tran " & _
              "WHERE assetbusinessdealt2depntranunkid = @assetbusinessdealt2depntranunkid "

            objDataOperation.AddParameter("@assetbusinessdealt2depntranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetBusinessDealT2DepntranUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintassetbusinessdealt2depntranunkid = CInt(dtRow.Item("assetbusinessdealt2depntranunkid"))
                mintassetdeclarationt2unkid = CInt(dtRow.Item("assetdeclarationt2unkid"))
                mintassetsectorunkid = dtRow.Item("assetsectorunkid").ToString
                mstrcompany_org_name = dtRow.Item("company_org_name").ToString
                mstrcompany_tin_no = dtRow.Item("company_tin_no").ToString
                If IsDBNull(dtRow.Item("registration_date")) = True Then
                    mdtregistration_date = Nothing
                Else
                    mdtregistration_date = dtRow.Item("registration_date")
                End If
                mstraddress_location = dtRow.Item("address_location").ToString
                mstrbusiness_type = dtRow.Item("business_type").ToString
                mstrposition_held = dtRow.Item("position_held").ToString
                mblnisfinalsaved = CInt(dtRow.Item("is_supplier_client"))
                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
                mintcurrencyunkid = CInt(dtRow.Item("currencyunkid"))
                mintbasecurrencyunkid = CInt(dtRow.Item("basecurrencyunkid"))
                mdecbaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mdecexchangerate = CDec(dtRow.Item("exchangerate"))
                mdecmonthlyannualearnings = CDec(dtRow.Item("monthly_annual_earnings"))
                mdecbasemonthlyannualearnings = CDec(dtRow.Item("basemonthly_annual_earnings"))
                mstrbusiness_contact_no = dtRow.Item("business_contact_no").ToString
                mstrshareholders_names = dtRow.Item("shareholders_names").ToString
                mblnisfinalsaved = CBool(dtRow.Item("isfinalsaved").ToString)
                If IsDBNull(dtRow.Item("transactiondate")) = True Then
                    mdttransactiondate = Nothing
                Else
                    mdttransactiondate = dtRow.Item("transactiondate")
                End If
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtvoiddatetime = Nothing
                Else
                    mdtvoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString
                mintDependentunkid = CInt(dtRow.Item("dependantunkid"))
                mintRelationshipunkid = CInt(dtRow.Item("relationshipunkid"))

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDataByUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub
    'Gajanan (07 Dec 2018) -- End



    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2
    'Public Function InserByDataTable(ByRef blnChildTableChanged As Boolean, ByVal dtOld As DataTable, ByVal xCurrentDatetTime As DateTime) As Boolean

    Public Function InserByDataTable(ByRef blnChildTableChanged As Boolean, ByVal dtOld As DataTable, ByVal xCurrentDatetTime As DateTime, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Gajanan (14 Nov 2018) -- End

        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim strUnkIDs As String = ""
        Dim decTotal As Decimal = 0

        Try
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = New clsDataOperation
            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
            objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()
            'Gajanan (14 Nov 2018) -- End


            For Each dtRow As DataRow In mdtTable.Rows

                mintassetbusinessdealt2depntranunkid = CInt(dtRow.Item("assetbusinessdealt2depntranunkid").ToString)
                mintassetsectorunkid = CInt(dtRow.Item("assetsectorunkid").ToString)
                mstrcompany_org_name = dtRow.Item("company_org_name").ToString
                mstrcompany_tin_no = dtRow.Item("company_tin_no").ToString
                mdtregistration_date = Convert.ToDateTime(dtRow.Item("registration_date"))
                mstraddress_location = dtRow.Item("address_location").ToString
                mstrbusiness_type = dtRow.Item("business_type").ToString
                mstrposition_held = dtRow.Item("position_held").ToString
                mintissupplierclient = CInt(dtRow.Item("is_supplier_client").ToString)
                mintcountryunkid = CInt(dtRow.Item("countryunkid"))
                mintcurrencyunkid = CInt(dtRow.Item("currencyunkid"))
                mintbasecurrencyunkid = CInt(dtRow.Item("basecurrencyunkid"))
                mdecbaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                mdecexchangerate = CDec(dtRow.Item("exchangerate"))
                mdecmonthlyannualearnings = CDec(dtRow.Item("monthly_annual_earnings"))
                mdecbasemonthlyannualearnings = CDec(dtRow.Item("basemonthly_annual_earnings"))
                mstrbusiness_contact_no = dtRow.Item("business_contact_no").ToString
                mstrshareholders_names = dtRow.Item("shareholders_names").ToString
                mblnisfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                mdttransactiondate = dtRow.Item("transactiondate")
                'mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtvoiddatetime = Nothing
                mstrvoidreason = dtRow.Item("voidreason").ToString
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                mintDependentunkid = CInt(dtRow.Item("dependantunkid"))
                mintRelationshipunkid = CInt(dtRow.Item("relationshipunkid"))
                'Hemant (03 Dec 2018) -- End

                If mintassetbusinessdealt2depntranunkid <= 0 Then

                    blnChildTableChanged = True

                    'Gajanan (14 Nov 2018) -- Start
                    'Enhancement : Changes for Asset Declaration template2
                    'If Insert() = False Then
                    If Insert(objDataOperation) = False Then
                        'Gajanan (14 Nov 2018) -- End
                        Return False
                    End If
                Else
                    If strUnkIDs.Trim = "" Then
                        strUnkIDs = mintassetbusinessdealt2depntranunkid.ToString
                    Else
                        strUnkIDs &= "," & mintassetbusinessdealt2depntranunkid.ToString
                    End If
                    'Gajanan (14 Nov 2018) -- Start
                    'Enhancement : Changes for Asset Declaration template2
                    'If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged) = False Then
                    If dtRow.Item("AUD").ToString() = "U" AndAlso Update(blnChildTableChanged, objDataOperation) = False Then
                        'Gajanan (14 Nov 2018) -- End
                        Return False
                    End If
                End If

            Next



            If dtOld IsNot Nothing AndAlso dtOld.Rows.Count > 0 Then
                Dim dRow() As DataRow
                If strUnkIDs.Trim <> "" Then
                    dRow = dtOld.Select("assetbusinessdealt2depntranunkid NOT IN (" & strUnkIDs & ") ")
                Else
                    dRow = dtOld.Select()
                End If

                For Each dtRow In dRow

                    mintassetbusinessdealt2depntranunkid = CInt(dtRow.Item("assetbusinessdealt2depntranunkid").ToString)
                    mintassetsectorunkid = CInt(dtRow.Item("assetsectorunkid").ToString)
                    mstrcompany_org_name = dtRow.Item("company_org_name").ToString
                    mstrcompany_tin_no = dtRow.Item("company_tin_no").ToString
                    mdtregistration_date = Convert.ToDateTime(dtRow.Item("registration_date"))
                    mstraddress_location = dtRow.Item("address_location").ToString
                    mstrbusiness_type = dtRow.Item("business_type").ToString
                    mstrposition_held = dtRow.Item("position_held").ToString
                    mintissupplierclient = CInt(dtRow.Item("is_supplier_client").ToString)
                    mintcountryunkid = CInt(dtRow.Item("countryunkid"))
                    mintcurrencyunkid = CInt(dtRow.Item("currencyunkid"))
                    mintbasecurrencyunkid = CInt(dtRow.Item("basecurrencyunkid"))
                    mdecbaseexchangerate = CDec(dtRow.Item("baseexchangerate"))
                    mdecexchangerate = CDec(dtRow.Item("exchangerate"))
                    mdecmonthlyannualearnings = CDec(dtRow.Item("monthly_annual_earnings"))
                    mdecbasemonthlyannualearnings = CDec(dtRow.Item("basemonthly_annual_earnings"))
                    mstrbusiness_contact_no = dtRow.Item("business_contact_no").ToString
                    mstrshareholders_names = dtRow.Item("shareholders_names").ToString
                    mblnisfinalsaved = CBool(dtRow.Item("isfinalsaved"))
                    mdttransactiondate = dtRow.Item("transactiondate")
                    'mintuserunkid = CInt(dtRow.Item("userunkid"))
                    mblnisvoid = CBool(dtRow.Item("isvoid"))
                    mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                    mdtvoiddatetime = Nothing
                    mstrvoidreason = dtRow.Item("voidreason").ToString

                    'Hemant (03 Dec 2018) -- Start
                    'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                    mintDependentunkid = CInt(dtRow.Item("dependantunkid"))
                    mintRelationshipunkid = CInt(dtRow.Item("relationshipunkid"))
                    'Hemant (03 Dec 2018) -- End

                    'Gajanan (14 Nov 2018) -- Start
                    'Enhancement : Changes for Asset Declaration template2
                    'If Void(CInt(dtRow.Item("assetbusinessdealt2depntranunkid")), mintuserunkid, xCurrentDatetTime, "") = False Then
                    If Void(CInt(dtRow.Item("assetbusinessdealt2depntranunkid")), mintuserunkid, xCurrentDatetTime, "", objDataOperation) = False Then
                        'Gajanan (14 Nov 2018) -- End

                        Return False
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InserByDataTable; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan (14 Nov 2018) -- End
        End Try
    End Function

    Public Function GetList(ByVal strTableName As String, Optional ByVal intAssetDeclarationt2UnkID As Integer = 0, Optional ByVal dtAsonDate As Date = Nothing) As DataSet
        'Sohail (18 May 2019) - [dtAsonDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            Dim objMaster As New clsMasterData
            Dim dsSuppBorr As DataSet = objMaster.getComboListAssetsupplierborrowerList("List")
            Dim dicSuppBorr As Dictionary(Of Integer, String) = (From p In dsSuppBorr.Tables(0) Select New With {.Id = CInt(p.Item("Id")), .Name = p.Item("name").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
            Dim objExRate As New clsExchangeRate
            Dim dsExRate As DataSet = objExRate.GetList("Currency", True, False, 0, 0, False, Nothing, True)
            Dim dicExRate As Dictionary(Of Integer, String) = (From p In dsExRate.Tables(0) Select New With {.Id = CInt(p.Item("countryunkid")), .Name = p.Item("currency_sign").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
            'Sohail (07 Dec 2018) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ = "SELECT * " & _
                       "INTO #TableDepn " & _
                       "FROM " & _
                       "( " & _
                           "SELECT hrdependant_beneficiaries_status_tran.* " & _
                                ", DENSE_RANK() OVER (PARTITION BY hrdependant_beneficiaries_status_tran.dpndtbeneficetranunkid ORDER BY hrdependant_beneficiaries_status_tran.effective_date DESC, hrdependant_beneficiaries_status_tran.dpndtbeneficestatustranunkid DESC ) AS ROWNO " & _
                           "FROM hrdependant_beneficiaries_status_tran "

            strQ &= "WHERE hrdependant_beneficiaries_status_tran.isvoid = 0 "

            If dtAsonDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= @dtAsOnDate "
                objDataOperation.AddParameter("@dtAsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsonDate))
            Else
                'StrQ &= "AND CONVERT(CHAR(8), hrdependant_beneficiaries_status_tran.effective_date, 112) <= GETDATE() "
            End If

            strQ &= ") AS A " & _
                    "WHERE 1 = 1 " & _
                    " AND A.ROWNO = 1 "
            'Sohail (18 May 2019) -- End

            strQ &= "SELECT " & _
              "  hrasset_businessdealT2depn_tran.assetbusinessdealt2depntranunkid " & _
              ", hrasset_businessdealT2depn_tran.assetdeclarationt2unkid " & _
              ", hrasset_businessdealT2depn_tran.assetsectorunkid " & _
              ", ISNULL(sector.name, '') AS assetsectorname " & _
              ", hrasset_businessdealT2depn_tran.company_org_name " & _
              ", hrasset_businessdealT2depn_tran.company_tin_no " & _
              ", hrasset_businessdealT2depn_tran.registration_date " & _
              ", hrasset_businessdealT2depn_tran.address_location " & _
              ", hrasset_businessdealT2depn_tran.business_type " & _
              ", hrasset_businessdealT2depn_tran.position_held " & _
              ", hrasset_businessdealT2depn_tran.is_supplier_client " & _
              ", ISNULL(hrasset_businessdealT2depn_tran.countryunkid, 0) AS countryunkid " & _
              ", ISNULL(hrasset_businessdealT2depn_tran.currencyunkid, 0) AS currencyunkid " & _
              ", ISNULL(hrasset_businessdealT2depn_tran.basecurrencyunkid, 0) AS basecurrencyunkid " & _
              ", ISNULL(hrasset_businessdealT2depn_tran.baseexchangerate, 0) AS baseexchangerate " & _
              ", ISNULL(hrasset_businessdealT2depn_tran.exchangerate, 0) AS exchangerate " & _
              ", ISNULL(hrasset_businessdealT2depn_tran.monthly_annual_earnings, 0) AS monthly_annual_earnings " & _
              ", ISNULL(hrasset_businessdealT2depn_tran.basemonthly_annual_earnings, 0) AS basemonthly_annual_earnings " & _
              ", hrasset_businessdealT2depn_tran.business_contact_no " & _
              ", hrasset_businessdealT2depn_tran.shareholders_names " & _
              ", hrasset_businessdealT2depn_tran.isfinalsaved " & _
              ", hrasset_businessdealT2depn_tran.transactiondate " & _
              ", hrasset_businessdealT2depn_tran.userunkid " & _
              ", hrasset_businessdealT2depn_tran.isvoid " & _
              ", hrasset_businessdealT2depn_tran.voiduserunkid " & _
              ", hrasset_businessdealT2depn_tran.voiddatetime " & _
              ", hrasset_businessdealT2depn_tran.voidreason " & _
              ", ISNULL(hrasset_businessdealT2depn_tran.dependantunkid, 0) as dependantunkid " & _
              ", ISNULL(hrdependants_beneficiaries_tran.first_name,'') +  ' ' + ISNULL(hrdependants_beneficiaries_tran.middle_name,'') +' '+ISNULL(hrdependants_beneficiaries_tran.last_name,'') as dependantname " & _
              ", ISNULL(hrasset_businessdealT2depn_tran.relationshipunkid, 0) as relationshipunkid " & _
              ", ISNULL(relation.name, '') AS relationshipname "

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            strQ &= ", CASE hrasset_businessdealT2depn_tran.is_supplier_client "
            For Each pair In dicSuppBorr
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS is_supplier_clientname "

            strQ &= ", CASE hrasset_businessdealT2depn_tran.countryunkid "
            For Each pair In dicExRate
                strQ &= " WHEN " & pair.Key & "  THEN '" & pair.Value & "' "
            Next
            strQ &= " END AS currency_sign "
            'Sohail (07 Dec 2018) -- End

            strQ &= "FROM hrasset_businessdealT2depn_tran " & _
                        "LEFT JOIN cfcommon_master AS sector ON sector.masterunkid = hrasset_businessdealT2depn_tran.assetsectorunkid AND sector.mastertype = " & CInt(clsCommon_Master.enCommonMaster.ASSET_SECTOR) & " " & _
                        "LEFT JOIN cfcommon_master AS relation ON relation.masterunkid = hrasset_businessdealT2depn_tran.relationshipunkid AND relation.mastertype = " & CInt(clsCommon_Master.enCommonMaster.RELATIONS) & " " & _
                        "LEFT JOIN hrdependants_beneficiaries_tran ON dpndtbeneficetranunkid = hrasset_businessdealT2depn_tran.dependantunkid " & _
                        "LEFT JOIN #TableDepn ON hrdependants_beneficiaries_tran.dpndtbeneficetranunkid = #TableDepn.dpndtbeneficetranunkid " & _
                    "WHERE ISNULL(hrasset_businessdealT2depn_tran.isvoid, 0 ) = 0 " & _
                    "AND #TableDepn.isactive = 1 "
            'Sohail (18 May 2019) - [JOIN #TableDepn, isactive]
            'Sohail (07 Dec 2018) - [LEFT JOIN cfcommon_master AS sector, LEFT JOIN cfcommon_master AS relation, LEFT JOIN hrdependants_beneficiaries_tran]
            'Hemant (03 Dec 2018) -- [dependantunkid,relationshipunkid]

            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'If intAssetDeclarationt2UnkID > 0 Then
            '    strQ &= " AND assetdeclarationt2unkid = @assetdeclarationt2unkid "
            '    objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            'End If
            If intAssetDeclarationt2UnkID > 0 Then
                strQ &= " AND hrasset_businessdealT2depn_tran.assetdeclarationt2unkid = @assetdeclarationt2unkid "
                objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            Else
                strQ &= " AND 1 = 2 "
            End If
            'Sohail (07 Dec 2018) -- End

            'Sohail (18 May 2019) -- Start
            'NMB Enhancement - REF # - 76.1 - On dependants menu, provide a way for user to set status of dependant as deceased etc. Allow user to deactivate a dependant with a reason instead of deleting.
            strQ &= " DROP TABLE #TableDepn "
            'Sohail (18 May 2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2
    'Public Function Insert() As Boolean
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_MasterT2 As clsAssetdeclaration_masterT2 = Nothing) As Boolean
        'Gajanan (07 Dec 2018) - [objAD_MasterT2]
        'Gajanan (14 Nov 2018) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Gajanan (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End        
        End If
        objDataOperation.ClearParameters()
        'Gajanan (14 Nov 2018) -- End


        Try

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If mintassetdeclarationt2unkid <= 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_MasterT2.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintassetdeclarationt2unkid = intNewUnkId
                End If
            ElseIf mintassetdeclarationt2unkid > 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                If objAD_MasterT2.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If

            objDataOperation.ClearParameters()
            'Gajanan (07 Dec 2018) -- End

            objDataOperation.AddParameter("@assetbusinessdealt2depntranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetbusinessdealt2depntranunkid)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetdeclarationt2unkid)
            objDataOperation.AddParameter("@assetsectorunkid", SqlDbType.NVarChar, eZeeDataType.INT_SIZE, mintassetsectorunkid.ToString)
            objDataOperation.AddParameter("@company_org_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrcompany_org_name.ToString)
            objDataOperation.AddParameter("@company_tin_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrcompany_tin_no.ToString)
            If mdtregistration_date = Nothing Then
                objDataOperation.AddParameter("@registration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@registration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtregistration_date.ToString)
            End If

            objDataOperation.AddParameter("@address_location", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstraddress_location.ToString)
            objDataOperation.AddParameter("@business_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrbusiness_type.ToString)
            objDataOperation.AddParameter("@position_held", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrposition_held.ToString)
            objDataOperation.AddParameter("@is_supplier_client", SqlDbType.Int, eZeeDataType.INT_SIZE, mintissupplierclient.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcurrencyunkid.ToString)
            objDataOperation.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintbasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseexchangerate.ToString)
            objDataOperation.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseexchangerate.ToString)
            objDataOperation.AddParameter("@monthly_annual_earnings", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecmonthlyannualearnings.ToString)
            objDataOperation.AddParameter("@basemonthly_annual_earnings", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbasemonthlyannualearnings.ToString)
            objDataOperation.AddParameter("@business_contact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrbusiness_contact_no.ToString)
            objDataOperation.AddParameter("@shareholders_names", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrshareholders_names.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisfinalsaved.ToString)
            If mdttransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdttransactiondate.ToString)
            End If

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            If mdtvoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependentunkid.ToString)
            objDataOperation.AddParameter("@relationshipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationshipunkid.ToString)
            'Hemant (03 Dec 2018) -- End

            strQ = "INSERT INTO hrasset_businessdealT2depn_tran ( " & _
                   "  assetdeclarationt2unkid " & _
                   ", assetsectorunkid " & _
                   ", company_org_name " & _
                   ", company_tin_no " & _
                   ", registration_date  " & _
                   ", address_location " & _
                   ", business_type " & _
                   ", position_held   " & _
                   ", is_supplier_client " & _
                   ", countryunkid " & _
                   ", currencyunkid " & _
                   ", basecurrencyunkid " & _
                   ", exchangerate " & _
                   ", monthly_annual_earnings " & _
                   ", basemonthly_annual_earnings " & _
                   ", business_contact_no " & _
                   ", shareholders_names " & _
                   ", isfinalsaved  " & _
                   ", transactiondate  " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   ", dependantunkid " & _
                   ", relationshipunkid " & _
            ") VALUES (" & _
                   "  @assetdeclarationt2unkid " & _
                   ", @assetsectorunkid " & _
                   ", @company_org_name " & _
                   ", @company_tin_no " & _
                   ", @registration_date  " & _
                   ", @address_location " & _
                   ", @business_type " & _
                   ", @position_held   " & _
                   ", @is_supplier_client " & _
                   ", @countryunkid " & _
                   ", @currencyunkid " & _
                   ", @basecurrencyunkid " & _
                   ", @exchangerate " & _
                   ", @monthly_annual_earnings " & _
                   ", @basemonthly_annual_earnings " & _
                   ", @business_contact_no " & _
                   ", @shareholders_names " & _
                   ", @isfinalsaved  " & _
                   ", @transactiondate  " & _
                   ", @userunkid " & _
                   ", @isvoid " & _
                   ", @voiduserunkid " & _
                   ", @voiddatetime " & _
                   ", @voidreason " & _
                   ", @dependantunkid " & _
                   ", @relationshipunkid " & _
            "); SELECT @@identity"

            'Hemant (03 Dec 2018) -- [dependantunkid,relationshipunkid]
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintassetbusinessdealt2depntranunkid = dsList.Tables(0).Rows(0).Item(0)

            If Insert_AtTranLog(objDataOperation, 1) = False Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                Return False
            End If
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan (14 Nov 2018) -- End
        End Try
    End Function
    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2

    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2
    'Public Function Update(ByRef blnChildTableChanged As Boolean) As Boolean
    Public Function Update(ByRef blnChildTableChanged As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal objAD_MasterT2 As clsAssetdeclaration_masterT2 = Nothing) As Boolean
        'Sohail (07 Dec 2018) - [objAD_MasterT2]
        'Gajanan (14 Nov 2018) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End          
        End If
        objDataOperation.ClearParameters()
        'Gajanan (14 Nov 2018) -- End



        Try
            'Sohail (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            If mintassetdeclarationt2unkid <= 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If objAD_MasterT2.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintassetdeclarationt2unkid = intNewUnkId
                End If
            ElseIf mintassetdeclarationt2unkid > 0 AndAlso objAD_MasterT2 IsNot Nothing Then
                If objAD_MasterT2.Update(enAction.EDIT_ONE, objDataOperation) = False Then
                    Return False
                End If
            End If
            objDataOperation.ClearParameters()
            'Sohail (07 Dec 2018) -- End
            objDataOperation.AddParameter("@assetbusinessdealt2depntranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetbusinessdealt2depntranunkid)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetdeclarationt2unkid)
            objDataOperation.AddParameter("@assetsectorunkid", SqlDbType.NVarChar, eZeeDataType.INT_SIZE, mintassetsectorunkid.ToString)
            objDataOperation.AddParameter("@company_org_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrcompany_org_name.ToString)
            objDataOperation.AddParameter("@company_tin_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrcompany_tin_no.ToString)
            If mdtregistration_date = Nothing Then
                objDataOperation.AddParameter("@registration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@registration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtregistration_date.ToString)
            End If

            objDataOperation.AddParameter("@address_location", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstraddress_location.ToString)
            objDataOperation.AddParameter("@business_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrbusiness_type.ToString)
            objDataOperation.AddParameter("@position_held", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrposition_held.ToString)
            objDataOperation.AddParameter("@is_supplier_client", SqlDbType.Int, eZeeDataType.INT_SIZE, mintissupplierclient.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcurrencyunkid.ToString)
            objDataOperation.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintbasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseexchangerate.ToString)
            objDataOperation.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseexchangerate.ToString)
            objDataOperation.AddParameter("@monthly_annual_earnings", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecmonthlyannualearnings.ToString)
            objDataOperation.AddParameter("@basemonthly_annual_earnings", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbasemonthlyannualearnings.ToString)
            objDataOperation.AddParameter("@business_contact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrbusiness_contact_no.ToString)
            objDataOperation.AddParameter("@shareholders_names", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrshareholders_names.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisfinalsaved.ToString)
            If mdttransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdttransactiondate.ToString)
            End If

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            If mdtvoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtvoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependentunkid.ToString)
            objDataOperation.AddParameter("@relationshipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationshipunkid.ToString)
            'Hemant (03 Dec 2018) -- End

            strQ = "UPDATE hrasset_businessdealT2depn_tran SET " & _
                   "  assetdeclarationt2unkid = @assetdeclarationt2unkid" & _
                   ", assetsectorunkid = @assetsectorunkid" & _
                   ", company_org_name = @company_org_name" & _
                   ", company_tin_no = @company_tin_no " & _
                   ", registration_date = @registration_date " & _
                   ", address_location= @address_location" & _
                   ", business_type= @business_type " & _
                   ", position_held   = @position_held " & _
                   ", is_supplier_client= @is_supplier_client " & _
                   ", countryunkid= @countryunkid " & _
                   ", currencyunkid= @currencyunkid " & _
                   ", basecurrencyunkid= @basecurrencyunkid " & _
                   ", exchangerate= @exchangerate " & _
                   ", monthly_annual_earnings= @monthly_annual_earnings " & _
                   ", basemonthly_annual_earnings= @basemonthly_annual_earnings " & _
                   ", business_contact_no= @business_contact_no " & _
                   ", shareholders_names= @shareholders_names " & _
                   ", isfinalsaved = @isfinalsaved " & _
                   ", transactiondate = @transactiondate " & _
                   ", userunkid = @userunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   ", dependantunkid = @dependantunkid " & _
                   ", relationshipunkid = @relationshipunkid " & _
                   "WHERE assetbusinessdealt2depntranunkid = @assetbusinessdealt2depntranunkid "

            'Hemant (03 Dec 2018) -- [dependantunkid,relationshipunkid]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintassetbusinessdealt2depntranunkid, objDataOperation) = False Then
            If Insert_AtTranLog(objDataOperation, 2) = False Then
                    'Sohail (07 Dec 2018) -- Start
                    'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    'Sohail (07 Dec 2018) -- End
                Return False
            End If
            End If

            blnChildTableChanged = True
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan (14 Nov 2018) -- End
        End Try
    End Function

    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2
    'Public Function VoidByAssetDeclarationt2UnkID(ByVal intAssetDeclarationt2UnkID As Integer, _
    '                                    ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, ByVal intParentAuditType As Integer) As Boolean
    Public Function VoidByAssetDeclarationt2UnkID(ByVal intAssetDeclarationt2UnkID As Integer, _
                                        ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, ByVal intParentAuditType As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Gajanan (14 Nov 2018) -- End
        Try
            'If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "hrassetdeclarationT2_master", "assetdeclarationt2unkid", intAssetDeclarationt2UnkID, "hrasset_bankT2_tran", "assetbankt2tranunkid", intParentAuditType, 3, , " ISNULL(isvoid, 0) = 0 ", , intVoidUserID) = False Then
            '    Return False
            'End If
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            strQ = "INSERT INTO athrasset_businessdealT2depn_tran ( " & _
                     "  assetbusinessdealt2depntranunkid " & _
                     ", assetdeclarationt2unkid " & _
                     ", assetsectorunkid " & _
                     ", company_org_name " & _
                     ", company_tin_no " & _
                     ", registration_date " & _
                     ", address_location " & _
                     ", business_type " & _
                     ", position_held " & _
                     ", is_supplier_client " & _
                     ", countryunkid " & _
                     ", currencyunkid " & _
                     ", basecurrencyunkid " & _
                     ", baseexchangerate " & _
                     ", exchangerate " & _
                     ", monthly_annual_earnings " & _
                     ", basemonthly_annual_earnings " & _
                     ", business_contact_no " & _
                     ", shareholders_names " & _
                     ", isfinalsaved " & _
                     ", transactiondate " & _
                     ", auditdatetime " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", loginemployeeunkid " & _
                     ", ip " & _
                     ", host " & _
                     ", form_name " & _
                     ", isweb " & _
                     ", dependantunkid " & _
                     ", relationshipunkid " & _
                     " )" & _
                     "SELECT " & _
                     "  assetbusinessdealt2depntranunkid " & _
                     ", assetdeclarationt2unkid " & _
                     ", assetsectorunkid " & _
                     ", company_org_name " & _
                     ", company_tin_no " & _
                     ", registration_date " & _
                     ", address_location " & _
                     ", business_type " & _
                     ", position_held " & _
                     ", is_supplier_client " & _
                     ", countryunkid " & _
                     ", currencyunkid " & _
                     ", basecurrencyunkid " & _
                     ", baseexchangerate " & _
                     ", exchangerate " & _
                     ", monthly_annual_earnings " & _
                     ", basemonthly_annual_earnings " & _
                     ", business_contact_no " & _
                     ", shareholders_names " & _
                     ", isfinalsaved " & _
                     ", transactiondate " & _
                     ", GETDATE() " & _
                     ", 3 " & _
                     ", @audituserunkid " & _
                     ", @loginemployeeunkid " & _
                     ", @ip " & _
                     ", @host " & _
                     ", @form_name " & _
                     ", @isweb " & _
                     ", dependantunkid " & _
                     ", relationshipunkid " & _
                     "  FROM hrasset_businessdealT2depn_tran " & _
                     "  WHERE isvoid = 0 " & _
                     "  AND assetdeclarationt2unkid = @assetdeclarationt2unkid "
            'Hemant (03 Dec 2018) -- [dependantunkid,relationshipunkid]
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = "UPDATE hrasset_businessdealT2depn_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetdeclarationt2unkid = @assetdeclarationt2unkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssetDeclarationt2UnkID)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If Insert_AtTranLog(objDataOperation, 3) = False Then
            '    Return False
            'End If
            'Gajanan (14 Nov 2018) -- End
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidByAssetDeclarationt2UnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan (14 Nov 2018) -- End
        End Try
    End Function

    'Gajanan (14 Nov 2018) -- Start
    'Enhancement : Changes for Asset Declaration template2
    'Public Function Void(ByVal intUnkid As Integer, _
    '                     ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean
    Public Function Void(ByVal intUnkid As Integer, _
                       ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Gajanan (14 Nov 2018) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan (14 Nov 2018) -- Start
        'Enhancement : Changes for Asset Declaration template2
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
            'Gajanan (07 Dec 2018) -- Start
            objDataOperation.BindTransaction()
            'Gajanan (07 Dec 2018) -- End
        End If
        objDataOperation.ClearParameters()
        'Gajanan (14 Nov 2018) -- End

        Try
           
          

            strQ = "UPDATE hrasset_businessdealT2depn_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE assetbusinessdealt2depntranunkid = @assetbusinessdealt2depntranunkid "

            objDataOperation.AddParameter("@assetbusinessdealt2depntranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If Insert_AtTranLog(objDataOperation, 3) = False Then
                'Sohail (07 Dec 2018) -- Start
                'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                'Sohail (07 Dec 2018) -- End
                Return False
            End If
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Gajanan (07 Dec 2018) -- End
            Return True
        Catch ex As Exception
            'Gajanan (07 Dec 2018) -- Start
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan (07 Dec 2018) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan (14 Nov 2018) -- Start
            'Enhancement : Changes for Asset Declaration template2
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Gajanan (14 Nov 2018) -- End
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
           strQ = "<Query>"


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Insert_AtTranLog(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO athrasset_businessdealT2depn_tran ( " & _
                     "  assetbusinessdealt2depntranunkid " & _
                     ", assetdeclarationt2unkid " & _
                     ", assetsectorunkid " & _
                     ", company_org_name " & _
                     ", company_tin_no " & _
                     ", registration_date " & _
                     ", address_location " & _
                     ", business_type " & _
                     ", position_held " & _
                     ", is_supplier_client " & _
                     ", countryunkid " & _
                     ", currencyunkid " & _
                     ", basecurrencyunkid " & _
                     ", baseexchangerate " & _
                     ", exchangerate " & _
                     ", monthly_annual_earnings " & _
                     ", basemonthly_annual_earnings " & _
                     ", business_contact_no " & _
                     ", shareholders_names " & _
                     ", isfinalsaved " & _
                     ", transactiondate " & _
                     ", auditdatetime " & _
                     ", audittype " & _
                     ", audituserunkid " & _
                     ", loginemployeeunkid " & _
                     ", ip " & _
                     ", host " & _
                     ", form_name " & _
                     ", isweb " & _
                     ", dependantunkid " & _
                     ", relationshipunkid " & _
                  ") VALUES (" & _
                     "  @assetbusinessdealt2depntranunkid " & _
                     ", @assetdeclarationt2unkid " & _
                     ", @assetsectorunkid " & _
                     ", @company_org_name " & _
                     ", @company_tin_no " & _
                     ", @registration_date " & _
                     ", @address_location " & _
                     ", @business_type " & _
                     ", @position_held " & _
                     ", @is_supplier_client " & _
                     ", @countryunkid " & _
                     ", @currencyunkid " & _
                     ", @basecurrencyunkid " & _
                     ", @baseexchangerate " & _
                     ", @exchangerate " & _
                     ", @monthly_annual_earnings " & _
                     ", @basemonthly_annual_earnings " & _
                     ", @business_contact_no " & _
                     ", @shareholders_names " & _
                     ", @isfinalsaved " & _
                     ", @transactiondate " & _
                     ", GETDATE() " & _
                     ", @audittype " & _
                     ", @audituserunkid " & _
                     ", @loginemployeeunkid " & _
                     ", @ip " & _
                     ", @host " & _
                     ", @form_name " & _
                     ", @isweb " & _
                     ", @dependantunkid " & _
                     ", @relationshipunkid " & _
                     " ) "
            'Hemant (03 Dec 2018) -- [dependantunkid,relationshipunkid]
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetbusinessdealt2depntranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetbusinessdealt2depntranunkid)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetdeclarationt2unkid)
            objDataOperation.AddParameter("@assetsectorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintassetsectorunkid.ToString)
            objDataOperation.AddParameter("@company_org_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrcompany_org_name.ToString)
            objDataOperation.AddParameter("@company_tin_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrcompany_tin_no.ToString)
            If mdtregistration_date = Nothing Then
                objDataOperation.AddParameter("@registration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@registration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtregistration_date.ToString)
            End If
            objDataOperation.AddParameter("@address_location", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstraddress_location.ToString)
            objDataOperation.AddParameter("@business_type", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrbusiness_type.ToString)
            objDataOperation.AddParameter("@position_held", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrposition_held.ToString)
            objDataOperation.AddParameter("@is_supplier_client", SqlDbType.Int, eZeeDataType.INT_SIZE, mintissupplierclient.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcountryunkid.ToString)
            objDataOperation.AddParameter("@currencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintcurrencyunkid.ToString)
            objDataOperation.AddParameter("@basecurrencyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintbasecurrencyunkid.ToString)
            objDataOperation.AddParameter("@baseexchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseexchangerate.ToString)
            objDataOperation.AddParameter("@exchangerate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbaseexchangerate.ToString)
            objDataOperation.AddParameter("@monthly_annual_earnings", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecmonthlyannualearnings.ToString)
            objDataOperation.AddParameter("@basemonthly_annual_earnings", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecbasemonthlyannualearnings.ToString)
            objDataOperation.AddParameter("@business_contact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrbusiness_contact_no.ToString)
            objDataOperation.AddParameter("@shareholders_names", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrshareholders_names.ToString)
            objDataOperation.AddParameter("@isfinalsaved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnisfinalsaved.ToString)
            If mdttransactiondate = Nothing Then
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdttransactiondate.ToString)
            End If
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minloginemployeeunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, minClientIp)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)
            'Hemant (03 Dec 2018) -- Start
            'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
            objDataOperation.AddParameter("@dependantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependentunkid.ToString)
            objDataOperation.AddParameter("@relationshipunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRelationshipunkid.ToString)
            'Hemant (03 Dec 2018) -- End
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                extForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw extForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_AtTranLog; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            'Gajanan (07 Dec 2018) -- Start
            'NMB Enhancement - Performance Enhancement in Asset Declaration T2 in 75.1.
            'strQ = "select TOP 1 * from athrasset_businessdealT2depn_tran where assetbusinessdealt2depntranunkid = @assetbusinessdealt2depntranunkid and assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype = 2 ORDER BY auditdatetime DESC"
            strQ = "select TOP 1 * from athrasset_businessdealT2depn_tran where assetbusinessdealt2depntranunkid = @assetbusinessdealt2depntranunkid and assetdeclarationt2unkid = @assetdeclarationt2unkid and audittype <> 3 ORDER BY auditdatetime DESC"
            'Gajanan (07 Dec 2018) -- End

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assetbusinessdealt2depntranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)
            objDataOperation.AddParameter("@assetdeclarationt2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssetdeclarationt2unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows
                'Hemant (03 Dec 2018) -- Start
                'NMB Enhancement : On dependant screens, add a field on Dependant Name and Relationship,Should provide option to select from employee dependants - Change to be included on dependant business, dependant shares , dependant bank and dependant real properties. On the relationship, should be dropdrown reading from relation master
                If IsDBNull(dr("dependantunkid")) = True Or IsDBNull(dr("relationshipunkid")) = True Then
                    Return False
                    'Hemant (03 Dec 2018) -- End
                ElseIf dr("assetsectorunkid").ToString() = mintassetsectorunkid AndAlso dr("company_org_name").ToString() = mstrcompany_org_name AndAlso _
                   dr("company_tin_no").ToString() = mstrcompany_tin_no AndAlso CDate(dr("registration_date")) = mdtregistration_date _
                   AndAlso dr("address_location").ToString() = mstraddress_location AndAlso dr("business_type").ToString() = mstrbusiness_type _
                   AndAlso dr("position_held").ToString() = mstrposition_held AndAlso dr("is_supplier_client").ToString() = mintissupplierclient _
                   AndAlso dr("monthly_annual_earnings").ToString() = mdecmonthlyannualearnings AndAlso dr("countryunkid").ToString() = mintcountryunkid _
                       AndAlso dr("business_contact_no").ToString() = mstrbusiness_contact_no AndAlso dr("shareholders_names").ToString() = mstrshareholders_names _
                        AndAlso dr("dependantunkid").ToString() = mintDependentunkid AndAlso dr("relationshipunkid").ToString() = mintRelationshipunkid Then
                    'Hemant (03 Dec 2018) -- [dependantunkid,relationshipunkid]

                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class
