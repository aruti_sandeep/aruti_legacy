﻿'************************************************************************************************************************************
'Class Name : clspdpaction_plan_category
'Purpose    :
'Date       : 12-Jan-2021
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clspdpaction_plan_category
    Private Shared ReadOnly mstrModuleName As String = "clspdpaction_plan_category"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation


#Region " Private variables "
    Private mintActionPlanCategoryunkid As Integer
    Private mstrCategory As String = String.Empty
    Private mintSortOrder As Integer
    Private mblnIsactive As Boolean = True
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actionplancategoryunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ActionPlanCategoryunkid() As Integer
        Get
            Return mintActionPlanCategoryunkid
        End Get
        Set(ByVal value As Integer)
            mintActionPlanCategoryunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set category
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Category() As String
        Get
            Return mstrCategory
        End Get
        Set(ByVal value As String)
            mstrCategory = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sortorder
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _SortOrder() As Integer
        Get
            Return mintSortOrder
        End Get
        Set(ByVal value As Integer)
            mintSortOrder = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  actionplancategoryunkid " & _
              ", category " & _
              ", sortorder " & _
              ", isactive " & _
             "FROM " & mstrDatabaseName & "..pdpaction_plan_category " & _
             "WHERE actionplancategoryunkid = @actionplancategoryunkid "

            objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionPlanCategoryunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintActionPlanCategoryunkid = CInt(dtRow.Item("actionplancategoryunkid"))
                mstrCategory = dtRow.Item("category").ToString
                mintSortOrder = CInt(dtRow.Item("sortorder"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    'Pinkal (12-Dec-2020) -- Start
    'Enhancement  -  Working on Talent Issue which is given by Andrew.
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        'Pinkal (12-Dec-2020) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  actionplancategoryunkid " & _
              ", category " & _
              ", pdpaction_plan_category.sortorder " & _
              ", pdpaction_plan_category.isactive " & _
             " FROM " & mstrDatabaseName & "..pdpaction_plan_category " & _
             " WHERE pdpaction_plan_category.isactive = 1 order by pdpaction_plan_category.sortorder "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pdpaction_plan_category) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCategory, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This category is already defined. Please define new category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@category", SqlDbType.NVarChar, mstrCategory.Trim.Length, mstrCategory.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "INSERT INTO " & mstrDatabaseName & "..pdpaction_plan_category ( " & _
              " category " & _
              ", sortorder " & _
              ", isactive" & _
            ") VALUES (" & _
              " @category " & _
              ", @sortorder " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintActionPlanCategoryunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pdpaction_plan_category) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCategory, mintActionPlanCategoryunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This category is already defined. Please define new category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionPlanCategoryunkid.ToString)
            objDataOperation.AddParameter("@category", SqlDbType.NVarChar, mstrCategory.Trim.Length, mstrCategory.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "UPDATE " & mstrDatabaseName & "..pdpaction_plan_category SET " & _
              "  category = @category" & _
              ", sortorder = @sortorder" & _
              ", isactive = @isactive " & _
            "WHERE actionplancategoryunkid = @actionplancategoryunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pdpaction_plan_category) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        _ActionPlanCategoryunkid = intUnkid
        mblnIsactive = False

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE " & mstrDatabaseName & "..pdpaction_plan_category SET " & _
                   " isactive = 0 " & _
                   "WHERE actionplancategoryunkid = @actionplancategoryunkid "

            objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            Dim Tables() As String = {"pdpgoals_master"}
            For Each value As String In Tables
                Select Case value
                    Case "pdpgoals_master"
                        strQ = "SELECT actionplancategoryunkid FROM " & value & " WHERE actionplancategoryunkid = @actionplancategoryunkid "

                    Case Else
                End Select
            objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

                dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    Return True
                    Exit For
                End If

            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCategory As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "  actionplancategoryunkid " & _
                    ", category " & _
                    ", sortorder " & _
                    ", isactive " & _
                    "FROM " & mstrDatabaseName & "..pdpaction_plan_category " & _
                    "WHERE isactive = 1 " & _
                    " AND category = @category "

            If intUnkid > 0 Then
                strQ &= " AND actionplancategoryunkid <> @actionplancategoryunkid"
            End If

            objDataOperation.AddParameter("@category", SqlDbType.NVarChar, strCategory.Trim.Length, strCategory)
            objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    Public Function GetCategoryList(ByVal strTableName As String, Optional ByVal blnFlag As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            If blnFlag = True Then
            strQ = "SELECT " & _
                             "a.* " & _
                        "FROM (SELECT " & _
                                  "0 AS actionplancategoryunkid " & _
                                ",@Select AS category " & _
                                ",1 AS isactive " & _
                                ",-1 AS sortorder " & _
                             "UNION " & _
                             "SELECT " & _
                                  "actionplancategoryunkid " & _
                                ",category " & _
                                ",pdpaction_plan_category.isactive " & _
                                ",pdpaction_plan_category.sortorder " & _
                             "FROM pdpaction_plan_category " & _
                             "WHERE pdpaction_plan_category.isactive = 1) AS a " & _
                        "ORDER BY sortorder "

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            Else
                strQ &= "SELECT " & _
              "  actionplancategoryunkid " & _
              ", category " & _
              ", pdpaction_plan_category.isactive " & _
             "FROM " & mstrDatabaseName & "..pdpaction_plan_category " & _
             "WHERE pdpaction_plan_category.isactive = 1 order by pdpaction_plan_category.sortorder "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCategoryList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpdpaction_plan_category ( " & _
                    "  tranguid " & _
                    ", actionplancategoryunkid " & _
                    ", category " & _
                    ", sortorder " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                  ") VALUES (" & _
                    "  LOWER(NEWID()) " & _
                    ", @actionplancategoryunkid " & _
                    ", @category " & _
                    ", @sortorder " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActionPlanCategoryunkid.ToString)
            objDataOperation.AddParameter("@category", SqlDbType.NVarChar, mstrCategory.Trim.Length, mstrCategory.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function isSortOrderExist(ByVal intSortOrder As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOper As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                    "  actionplancategoryunkid " & _
                    ", category " & _
                    ", sortorder " & _
                    "FROM pdpaction_plan_category " & _
                    "WHERE isactive = 1 " & _
                    " AND sortorder = @sortorder "

            If intUnkid > 0 Then
                strQ &= " and actionplancategoryunkid <> @actionplancategoryunkid "
                objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, intSortOrder)
            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isSortOrderExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOper Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isPDPTransectionStarted(ByVal intUnkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                         "pdpaction_plan_category.actionplancategoryunkid " & _
                    "FROM pdpgoals_master " & _
                    "LEFT JOIN pdpaction_plan_category " & _
                         "ON pdpgoals_master.actionplancategoryunkid = pdpaction_plan_category.actionplancategoryunkid " & _
                    "WHERE pdpgoals_master.actionplancategoryunkid = @actionplancategoryunkid "

            objDataOperation.AddParameter("@actionplancategoryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "Used")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, This category is already defined. Please define new category.")
			Language.setMessage(mstrModuleName, 2, "Select")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
