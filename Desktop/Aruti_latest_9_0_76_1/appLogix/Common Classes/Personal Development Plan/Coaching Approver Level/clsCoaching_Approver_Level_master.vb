﻿Option Strict On
'************************************************************************************************************************************
'Class Name : clsCoaching_Approver_Level_master.vb
'Purpose    :
'Date       : 22-Sep-2023
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant Morker
''' </summary>
Public Class clsCoaching_Approver_Level_master
    Private Shared ReadOnly mstrModuleName As String = "clsCoaching_Approver_Level_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintLevelunkid As Integer
    Private mstrLevelcode As String = String.Empty
    Private mstrLevelname As String = String.Empty
    Private mintPriority As Integer
    Private mstrLevelname1 As String = String.Empty
    Private mstrLevelname2 As String = String.Empty
    Private mintFormTypeId As Integer
    Private mblnIsactive As Boolean = True

    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Levelunkid(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelcode
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Levelcode() As String
        Get
            Return mstrLevelcode
        End Get
        Set(ByVal value As String)
            mstrLevelcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Levelname() As String
        Get
            Return mstrLevelname
        End Get
        Set(ByVal value As String)
            mstrLevelname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname1
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Levelname1() As String
        Get
            Return mstrLevelname1
        End Get
        Set(ByVal value As String)
            mstrLevelname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname2
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Levelname2() As String
        Get
            Return mstrLevelname2
        End Get
        Set(ByVal value As String)
            mstrLevelname2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formtypeid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FormTypeId() As Integer
        Get
            Return mintFormTypeId
        End Get
        Set(ByVal value As Integer)
            mintFormTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", levelname1 " & _
              ", levelname2 " & _
              ", ISNULL(formtypeid, 0) AS formtypeid " & _
              ", isactive " & _
             "FROM pdpcoaching_approverlevel_master " & _
             "WHERE levelunkid = @levelunkid "

            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mstrLevelcode = dtRow.Item("levelcode").ToString
                mstrLevelname = dtRow.Item("levelname").ToString
                If dtRow.Item("priority") IsNot DBNull.Value Then mintPriority = CInt(dtRow.Item("priority"))
                mstrLevelname1 = dtRow.Item("levelname1").ToString
                mstrLevelname2 = dtRow.Item("levelname2").ToString
                mintFormTypeId = CInt(dtRow.Item("formtypeid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblnblank As Boolean = False, Optional ByVal strfilter As String = "") As DataSet
        Dim objMaster As New clsMasterData
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            Dim dsFormType As DataSet = objMaster.getComboListForPDPCoachingFormType("List", False)
            Dim dicFormType As Dictionary(Of Integer, String) = (From p In dsFormType.Tables("List") Select New With {.id = CInt(p.Item("Id")), .name = p.Item("Name").ToString}).ToDictionary(Function(x) x.id, Function(x) x.name)

            If mblnblank Then
                strQ = "SELECT " & _
                       "  0 AS levelunkid " & _
                       ", '' AS levelcode " & _
                       ", '' AS levelname " & _
                       ", 0 AS priority " & _
                       ", '' AS levelname1 " & _
                       ", '' AS levelname2 " & _
                       ", 0 AS formtypeid " & _
                       ", 0 AS isactive " & _
                       ", '' AS formtype " & _
                       " UNION ALL "
            End If

            strQ &= "SELECT " & _
                    "  levelunkid " & _
                    ", levelcode " & _
                    ", levelname " & _
                    ", priority " & _
                    ", levelname1 " & _
                    ", levelname2 " & _
                    ", formtypeid " & _
                    ", isactive "

            strQ &= ", CASE formtypeid "
            For Each pair In dicFormType
                strQ &= " WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            Next
            strQ &= " END AS formtype "

            strQ &= " FROM pdpcoaching_approverlevel_master "

            If blnOnlyActive Then
                strQ &= " WHERE pdpcoaching_approverlevel_master.isactive = 1 "
            End If

            If strfilter <> "" Then
                strQ &= "And " & strfilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            objMaster = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pdpcoaching_approverlevel_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintFormTypeId, mstrLevelcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Level Code is already defined. Please define new Level Code.")
            Return False
        ElseIf isExist(mintFormTypeId, "", mstrLevelname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Level Name is already defined. Please define new Level Name.")
            Return False
        ElseIf isPriorityExist(mintFormTypeId, mintPriority) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Level Priority is already assigned. Please assign new Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@levelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname1.ToString)
            objDataOperation.AddParameter("@levelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname2.ToString)
            objDataOperation.AddParameter("@formtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormTypeId.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "INSERT INTO pdpcoaching_approverlevel_master ( " & _
              "  levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", levelname1 " & _
              ", levelname2 " & _
              ", formtypeid " & _
              ", isactive" & _
            ") VALUES (" & _
              "  @levelcode " & _
              ", @levelname " & _
              ", @priority " & _
              ", @levelname1 " & _
              ", @levelname2 " & _
              ", @formtypeid " & _
              ", @isactive" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLevelunkid = CInt(dsList.Tables(0).Rows(0).Item(0))

            If InsertAuditTrailForLevel(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pdpcoaching_approverlevel_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mintFormTypeId, mstrLevelcode, "", mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Level Code is already defined. Please define new Level Code.")
            Return False
        ElseIf isExist(mintFormTypeId, "", mstrLevelname, mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Level Name is already defined. Please define new Level Name.")
            Return False
        ElseIf isPriorityExist(mintFormTypeId, mintPriority, mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Level Priority is already assigned. Please assign new Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@levelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname1.ToString)
            objDataOperation.AddParameter("@levelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname2.ToString)
            objDataOperation.AddParameter("@formtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormTypeId.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "UPDATE pdpcoaching_approverlevel_master SET " & _
                   "  levelcode = @levelcode" & _
                   ", levelname = @levelname" & _
                   ", priority = @priority" & _
                   ", levelname1 = @levelname1" & _
                   ", levelname2 = @levelname2" & _
                   ", formtypeid = @formtypeid " & _
                   ", isactive = @isactive " & _
                   "WHERE levelunkid = @levelunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForLevel(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pdpcoaching_approverlevel_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, you cannot delete this Level. Reason : This Level is already linked with some transaction.")
            Return False
        End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = " Update pdpcoaching_approverlevel_master set isactive = 0 " & _
                   " WHERE levelunkid  = @levelunkid  "

            objDataOperation.AddParameter("@levelunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Levelunkid(objDataOperation) = intUnkid

            If InsertAuditTrailForLevel(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "select isnull(levelunkid,0) FROM pdpcoaching_role_mapping WHERE levelunkid = @levelunkid AND isvoid = 0 "
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intFormTypeId As Integer, Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  levelunkid " & _
                   ", levelcode " & _
                   ", levelname " & _
                   ", priority " & _
                   ", levelname1 " & _
                   ", levelname2 " & _
                   ", formtypeid " & _
                   ", isactive " & _
                   "FROM pdpcoaching_approverlevel_master " & _
                   " WHERE 1=1 AND isactive = 1 "

            If strCode.Length > 0 Then
                strQ &= " AND levelcode = @levelcode "
            End If

            If strName.Length > 0 Then
                strQ &= " AND levelname = @levelname "
            End If

            If intUnkid > 0 Then
                strQ &= " AND levelunkid <> @levelunkid"
            End If

            If intFormTypeId > 0 Then
                strQ &= " AND formtypeid = @formtypeid"
            End If

            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@formtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormTypeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isPriorityExist(ByVal intFormTypeId As Integer, ByVal mintPriority As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                      "  levelunkid " & _
                      ", levelcode " & _
                      ", levelName " & _
                      ", priority " & _
                      ", isactive " & _
                      ", levelName1 " & _
                      ", levelName2 " & _
                      ", formtypeid " & _
                     " FROM pdpcoaching_approverlevel_master " & _
                     " WHERE priority = @priority AND isactive = 1 "

            If intUnkid > 0 Then
                strQ &= " AND levelunkid <> @levelunkid"
            End If

            If intFormTypeId > 0 Then
                strQ &= " AND formtypeid = @formtypeid"
            End If

            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@formtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormTypeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPriorityExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrailForLevel(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO atpdpcoaching_approverlevel_master ( " & _
                       "  tranguid " & _
                       ", levelunkid " & _
                       ", levelcode " & _
                       ", levelname " & _
                       ", priority " & _
                       ", levelname1 " & _
                       ", levelname2 " & _
                       ", formtypeid " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip" & _
                       ", hostname" & _
                       ", form_name " & _
                       ", isweb " & _
                   ") VALUES (" & _
                       "  @tranguid " & _
                       ", @levelunkid " & _
                       ", @levelcode " & _
                       ", @levelname " & _
                       ", @priority " & _
                       ", @levelname1 " & _
                       ", @levelname2 " & _
                       ", @formtypeid " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip" & _
                       ", @hostname" & _
                       ", @form_name " & _
                       ", @isweb " & _
                   ");"

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid)
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@levelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname1.ToString)
            objDataOperation.AddParameter("@levelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname2.ToString)
            objDataOperation.AddParameter("@formtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormTypeId.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForLevel; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(ByVal intFormTypeId As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal xDataOper As clsDataOperation = Nothing) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation
        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        objDataOperation.ClearParameters()
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as levelunkid, ' ' +  @name  as name, 0 AS priority UNION "
            End If
            strQ &= "SELECT " & _
                    "  levelunkid " & _
                    " ,levelname as name " & _
                    " ,priority AS priority " & _
                    "FROM pdpcoaching_approverlevel_master " & _
                    "WHERE isactive = 1  "

            objDataOperation.ClearParameters()

            If intFormTypeId > 0 Then
                strQ &= " AND formtypeid = @formtypeid "
                objDataOperation.AddParameter("@formtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormTypeId)
            End If

            strQ &= " ORDER BY priority "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getListForCombo; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If xDataOper Is Nothing Then objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Level Code is already defined. Please define new Level Code.")
			Language.setMessage(mstrModuleName, 2, "This Level Name is already defined. Please define new Level Name.")
			Language.setMessage(mstrModuleName, 3, "This Level Priority is already assigned. Please assign new Level Priority.")
			Language.setMessage(mstrModuleName, 4, "Select")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot delete this Level. Reason : This Level is already linked with some transaction.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
