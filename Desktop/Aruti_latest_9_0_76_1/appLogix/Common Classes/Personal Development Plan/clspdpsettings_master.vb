﻿'************************************************************************************************************************************
'Class Name : clspdpsettings_master
'Purpose    :
'Date       : 12-Jan-2021
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clspdpsettings_master
    Private Shared ReadOnly mstrModuleName As String = "clspdpsettings_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSettingunkid As Integer
    Private mintSettingkeyid As Integer
    Private mstrSetting_Value As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

    Private mdicPDPSetting As Dictionary(Of enPDPConfiguration, String)

    Public Enum enPDPConfiguration
        SELF = 1
        LINE_MANAGER = 2
        PEERS = 3
        CategoryItemMapping = 4
        INSTRUCTION = 5
    End Enum

    Public Enum enPDPEvalutorTypeId
        SELF = 1
        LINE_MANAGER = 2
        PEERS = 3
        MENTOR = 4
        OTHER = 5
    End Enum
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set settingunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Settingunkid() As Integer
        Get
            Return mintSettingunkid
        End Get
        Set(ByVal value As Integer)
            mintSettingunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set settingkeyid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Settingkeyid() As Integer
        Get
            Return mintSettingkeyid
        End Get
        Set(ByVal value As Integer)
            mintSettingkeyid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set setting_value
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Setting_Value() As String
        Get
            Return mstrSetting_Value
        End Get
        Set(ByVal value As String)
            mstrSetting_Value = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _PDPSetting() As Dictionary(Of enPDPConfiguration, String)
        Set(ByVal value As Dictionary(Of enPDPConfiguration, String))
            mdicPDPSetting = value
        End Set
    End Property



#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  settingunkid " & _
              ", settingkeyid " & _
              ", setting_value " & _
             "FROM " & mstrDatabaseName & "..pdpsettings_master " & _
             "WHERE settingunkid = @settingunkid "

            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintSettingunkid = CInt(dtRow.Item("settingunkid"))
                mintSettingkeyid = CInt(dtRow.Item("settingkeyid"))
                mstrSetting_Value = dtRow.Item("setting_value").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetSetting(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Dictionary(Of enPDPConfiguration, String)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  settingunkid " & _
              ", settingkeyid " & _
              ", setting_value " & _
             "FROM " & mstrDatabaseName & "..pdpsettings_master "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            mdicPDPSetting = New Dictionary(Of enPDPConfiguration, String)
            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then

                Dim SELF As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enPDPConfiguration.SELF)).FirstOrDefault()
                If IsNothing(SELF) = False Then
                    mdicPDPSetting.Add(enPDPConfiguration.SELF, SELF.Field(Of String)("setting_value").ToString())
                End If

                Dim LINE_MANAGER As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enPDPConfiguration.LINE_MANAGER)).FirstOrDefault()
                If IsNothing(LINE_MANAGER) = False Then
                    mdicPDPSetting.Add(enPDPConfiguration.LINE_MANAGER, LINE_MANAGER.Field(Of String)("setting_value").ToString())
                End If

                Dim PEERS As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enPDPConfiguration.PEERS)).FirstOrDefault()
                If IsNothing(PEERS) = False Then
                    mdicPDPSetting.Add(enPDPConfiguration.PEERS, PEERS.Field(Of String)("setting_value").ToString())
                End If

                Dim CategoryItemMapping As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enPDPConfiguration.CategoryItemMapping)).FirstOrDefault()
                If IsNothing(CategoryItemMapping) = False Then
                    mdicPDPSetting.Add(enPDPConfiguration.CategoryItemMapping, CategoryItemMapping.Field(Of String)("setting_value").ToString())
                End If

                Dim Instruction As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enPDPConfiguration.INSTRUCTION)).FirstOrDefault()
                If IsNothing(Instruction) = False Then
                    mdicPDPSetting.Add(enPDPConfiguration.INSTRUCTION, Instruction.Field(Of String)("setting_value").ToString())
                End If


                Return mdicPDPSetting

            End If

            Return Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSetting; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    Public Function GetSettingValueFromKey(ByVal settingkeyid As enPDPConfiguration, Optional ByVal xDataOpr As clsDataOperation = Nothing) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              " setting_value " & _
             "FROM " & mstrDatabaseName & "..pdpsettings_master " & _
             "WHERE settingkeyid = @settingkeyid "

            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(settingkeyid))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)("setting_value").ToString()
            End If

            Return String.Empty


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSettingValueFromKey; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  settingunkid " & _
              ", settingkeyid " & _
              ", setting_value " & _
             "FROM " & mstrDatabaseName & "..pdpsettings_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function SavePDPSetting(ByVal xmdicSetting As Dictionary(Of enPDPConfiguration, String), Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        objDataOperation.ClearParameters()
        Try

            If xmdicSetting.Count > 0 Then
                For Each kvp As KeyValuePair(Of enPDPConfiguration, String) In xmdicSetting

                    mintSettingkeyid = kvp.Key
                    mstrSetting_Value = kvp.Value

                    If isExist(kvp.Key, objDataOperation) Then
                        If Update(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                            Return False
                        End If
                    Else
                        If Insert(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                            Return False
                        End If
                    End If

                Next
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: SavePDPSetting; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal intSettingTypeId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT setting_value,settingunkid " & _
             "FROM " & mstrDatabaseName & "..pdpsettings_master " & _
             " where  settingkeyid= @settingkeyid  "

            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSettingTypeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mintSettingunkid = CInt(dsList.Tables(0).Rows(0)("settingunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pdpsettings_master) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingkeyid.ToString)
            objDataOperation.AddParameter("@setting_value", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrSetting_Value.ToString)

            strQ = "INSERT INTO " & mstrDatabaseName & "..pdpsettings_master ( " & _
              " settingkeyid " & _
              ", setting_value" & _
            ") VALUES (" & _
              " @settingkeyid " & _
              ", @setting_value" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSettingunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pdpsettings_master) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingunkid.ToString)
            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingkeyid.ToString)
            objDataOperation.AddParameter("@setting_value", SqlDbType.NVarChar, mstrSetting_Value.Length, mstrSetting_Value.ToString)

            strQ = "UPDATE " & mstrDatabaseName & "..pdpsettings_master SET " & _
                  " settingkeyid = @settingkeyid" & _
                  ", setting_value = @setting_value " & _
                "WHERE settingunkid = @settingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pdpsettings_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        _Settingunkid = intUnkid

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()
        Try

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "DELETE FROM " & mstrDatabaseName & "..pdpsettings_master WHERE settingunkid = @settingunkid "

            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isAllPDPSettingExist(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objtlstages_master As New clstlstages_master
        Dim objtlscreener_master As New clstlscreener_master
        Dim objtlratings_master As New clstlratings_master
        Dim objtlquestionnaire_master As New clstlquestionnaire_master
        Dim mdtSetting As New Dictionary(Of enPDPConfiguration, String)
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            mdtSetting = Me.GetSetting()
            If mdtSetting.Count <= 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, PDP qualification setting not define, so please complete this to continue.")
                Return False
            End If

            objtlstages_master._DatabaseName = mstrDatabaseName
            dsList = objtlstages_master.GetList("stage")
            If dsList.Tables("stage").Rows.Count <= 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, PDP stages are not define,so please complete this to continue.")
                Return False
            End If

            objtlscreener_master._DatabaseName = mstrDatabaseName
            dsList = objtlscreener_master.GetList("screener")
            If dsList.Tables("screener").Rows.Count <= 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, PDP screeners are not define,so please complete this to continue.")
                Return False
            End If

            objtlratings_master._DatabaseName = mstrDatabaseName
            dsList = objtlratings_master.GetList("rating")
            If dsList.Tables("rating").Rows.Count <= 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, PDP rating are not define,so please complete this to continue.")
                Return False
            End If

            objtlquestionnaire_master._DatabaseName = mstrDatabaseName
            dsList = objtlquestionnaire_master.GetList("question", True)
            If dsList.Tables("question").Rows.Count <= 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, PDP questions are not define,so please complete this to continue.")
                Return False
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isAllPDPSettingExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpdpsettings_master ( " & _
                    "  tranguid " & _
                    ", settingunkid " & _
                    ", settingkeyid " & _
                    ", setting_value " & _
                    ", audittypeid " & _
                    ", audtuserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb" & _
                  ") VALUES (" & _
                    "  LOWER(NEWID()) " & _
                    ", @settingunkid " & _
                    ", @settingkeyid " & _
                    ", @setting_value " & _
                    ", @audittypeid " & _
                    ", @audtuserunkid " & _
                    ", GETDATE() " & _
                    ", @formname " & _
                    ", @ip " & _
                    ", @host " & _
                    ", @isweb" & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingunkid.ToString)
            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingkeyid.ToString)
            objDataOperation.AddParameter("@setting_value", SqlDbType.NVarChar, mstrSetting_Value.Length, mstrSetting_Value.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audtuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, PDP qualification setting not define, so please complete this to continue.")
			Language.setMessage(mstrModuleName, 2, "Sorry, PDP stages are not define,so please complete this to continue.")
			Language.setMessage(mstrModuleName, 3, "Sorry, PDP screeners are not define,so please complete this to continue.")
			Language.setMessage(mstrModuleName, 4, "Sorry, PDP rating are not define,so please complete this to continue.")
			Language.setMessage(mstrModuleName, 5, "Sorry, PDP questions are not define,so please complete this to continue.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
