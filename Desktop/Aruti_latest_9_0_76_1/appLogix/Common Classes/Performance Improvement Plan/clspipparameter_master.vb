﻿'************************************************************************************************************************************
'Class Name : clspipparameter_master
'Purpose    :
'Date       : 01-Dec-2023
'Written By : Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clspipparameter_master
    Private Shared ReadOnly mstrModuleName As String = "clspipparameter_master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation


#Region " Private variables "
    Private mintParameterunkid As Integer
    Private mstrParameter As String = String.Empty
    Private mintViewTypeId As Integer
    Private mintSortOrder As Integer
    Private mblnIsactive As Boolean = True
    Private mintApplicableId As Integer = 0
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mstrFormName As String = ""
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mblnIsWeb As Boolean = False
    Private mstrDatabaseName As String = ""
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categoryunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Parameterunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintParameterunkid
        End Get
        Set(ByVal value As Integer)
            mintParameterunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set category
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Parameter() As String
        Get
            Return mstrParameter
        End Get
        Set(ByVal value As String)
            mstrParameter = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set viewtypeid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ViewTypeId() As Integer
        Get
            Return mintViewTypeId
        End Get
        Set(ByVal value As Integer)
            mintViewTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set applicableid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ApplicableId() As Integer
        Get
            Return mintApplicableId
        End Get
        Set(ByVal value As Integer)
            mintApplicableId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sortorder
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _SortOrder() As Integer
        Get
            Return mintSortOrder
        End Get
        Set(ByVal value As Integer)
            mintSortOrder = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set clientip
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fromweb
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

#Region " ENUMS "

    Public Enum enPIPApplicableMode
        EMPLOYEE = 1
        MANAGER = 2
    End Enum


#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If


        Try
            strQ = "SELECT " & _
                      "  parameterunkid " & _
                      ", parameter " & _
                      ", viewtype " & _
                      ", applicableid " & _
                      ", sortorder " & _
                      ", isactive " & _
                      " FROM " & mstrDatabaseName & "..pipparameter_master " & _
                      " WHERE parameterunkid = @parameterunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintParameterunkid = CInt(dtRow.Item("parameterunkid"))
                mstrParameter = dtRow.Item("parameter").ToString
                mintViewTypeId = CInt(dtRow.Item("viewtype"))
                mintApplicableId = CInt(dtRow.Item("applicableid"))
                mintSortOrder = CInt(dtRow.Item("sortorder"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList_ApplicableMode(Optional ByVal iList As String = "List", Optional ByVal iAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOpr As New clsDataOperation
        Try

            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            StrQ &= " SELECT '" & enPIPApplicableMode.EMPLOYEE & "' AS Id, @Employee AS Name UNION " & _
                         " SELECT '" & enPIPApplicableMode.MANAGER & "' AS Id, @Manager AS Name "


            objDataOpr.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            objDataOpr.AddParameter("@Employee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Employee"))
            objDataOpr.AddParameter("@Manager", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Manager"))

            If iList.Trim.Length <= 0 Then iList = "List"

            dsList = objDataOpr.ExecQuery(StrQ, iList)

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetList_ApplicableMode", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetList(ByVal strTableName As String, _
                                     Optional ByVal blnOnlyActive As Boolean = True, _
                                     Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdicApplicableMode As New Dictionary(Of Integer, String)

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            Dim ds As DataSet = GetList_ApplicableMode("List", False)
            mdicApplicableMode = (From p In ds.Tables("List") Select New With {Key .Id = CInt(p.Item("Id")), Key .Name = p.Item("name").ToString}).ToDictionary(Function(x) CInt(x.Id), Function(y) y.Name)

            strQ = "SELECT " & _
                      "  parameterunkid " & _
                      ", parameter " & _
                      ", pipparameter_master.viewtype as viewtypeid " & _
                      ", CASE WHEN pipparameter_master.viewtype  = " & CInt(enPDPCustomItemViewType.Table) & " then @table " & _
                      "       WHEN pipparameter_master.viewtype  = " & CInt(enPDPCustomItemViewType.List) & " then @list " & _
                      "  ELSE '' END ViewType " & _
                      ", pipparameter_master.applicableid "

            If mdicApplicableMode.Count > 0 Then
                strQ &= ", CASE "
                For Each key In mdicApplicableMode
                    strQ &= " WHEN applicableid = " & key.Key & " THEN '" & key.Value & "' "
                Next
                strQ &= " END AS applicablemode "
            End If

            strQ &= ", pipparameter_master.sortorder " & _
                      ", pipparameter_master.isactive " & _
                      " FROM pipparameter_master " & _
                      " WHERE pipparameter_master.isactive = 1 "

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter & " "
            End If

            strQ &= "ORDER BY pipparameter_master.sortorder "

            objDataOperation.AddParameter("@table", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Table"))
            objDataOperation.AddParameter("@list", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "List"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pdpcategory_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrParameter, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, This parameter is already defined. Please define new parameter.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@parameter", SqlDbType.NVarChar, mstrParameter.Trim.Length, mstrParameter.ToString)
            objDataOperation.AddParameter("@viewtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintViewTypeId.ToString())
            objDataOperation.AddParameter("@applicableid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicableId.ToString())
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)


            strQ = "INSERT INTO " & mstrDatabaseName & "..pipparameter_master ( " & _
                      "  parameter " & _
                      ", viewtype " & _
                      ", applicableid " & _
                      ", sortorder " & _
                      ", isactive" & _
                    ") VALUES (" & _
                      "  @parameter " & _
                      ", @viewtype " & _
                      ", @applicableid " & _
                      ", @sortorder " & _
                      ", @isactive" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintParameterunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pdpcategory_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrParameter, mintParameterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, This parameter is already defined. Please define new parameter.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@parameter", SqlDbType.NVarChar, mstrParameter.Trim.Length, mstrParameter.ToString)
            objDataOperation.AddParameter("@viewtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintViewTypeId.ToString)
            objDataOperation.AddParameter("@applicableid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicableId.ToString())
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)


            strQ = "UPDATE " & mstrDatabaseName & "..pipparameter_master SET " & _
                      " parameter = @parameter" & _
                      ", viewtype = @viewtype " & _
                      ", applicableid = @applicableid " & _
                      ", sortorder = @sortorder" & _
                      ", isactive = @isactive " & _
                      " WHERE parameterunkid = @parameterunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pdpcategory_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE " & mstrDatabaseName & "..pipparameter_master SET " & _
                      " isactive = 0 " & _
                      " WHERE parameterunkid = @parameterunkid "

            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Parameterunkid(objDataOperation) = intUnkid

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            Dim Tables() As String = {"pipitem_master", "pipconcern_expectationtran", "pipimprovement_plantran", "pipmonitoring_tran"}
            For Each value As String In Tables
                Select Case value
                    Case "pipitem_master"
                        strQ = "SELECT parameterunkid FROM " & value & " WHERE parameterunkid = @parameterunkid "

                    Case "pipconcern_expectationtran"
                        strQ = "SELECT parameterunkid FROM " & value & " WHERE parameterunkid = @parameterunkid"

                    Case "pipimprovement_plantran"
                        strQ = "SELECT parameterunkid FROM " & value & " WHERE parameterunkid = @parameterunkid "

                    Case "pipmonitoring_tran"
                        strQ = "SELECT parameterunkid FROM " & value & " WHERE parameterunkid = @parameterunkid "
                    Case Else
                End Select
                objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    Return True
                    Exit For
                End If

            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCategory As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "  parameterunkid " & _
                    ", parameter " & _
                    ", viewtype " & _
                    ", applicableid " & _
                    ", sortorder " & _
                    ", isactive " & _
                    " FROM " & mstrDatabaseName & "..pipparameter_master " & _
                    " WHERE isactive = 1 AND parameter = @parameter "

            If intUnkid > 0 Then
                strQ &= " AND parameter <> @parameter"
            End If

            objDataOperation.AddParameter("@parameter", SqlDbType.NVarChar, strCategory.Trim.Length, strCategory)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isSortOrderExist(ByVal intSortOrder As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOper As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                    "  parameterunkid " & _
                    ", parameter " & _
                    ", viewtype " & _
                    ", applicableid " & _
                    ", sortorder " & _
                    ", isactive " & _
                    " FROM pipparameter_master " & _
                    " WHERE isactive = 1  AND sortorder = @sortorder "

            If intUnkid > 0 Then
                strQ &= " and parameterunkid <> @parameterunkid "
                objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, intSortOrder)
            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isSortOrderExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOper Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strTableName As String = "List", _
                                    Optional ByVal mblFlag As Boolean = False, _
                                    Optional ByVal xDataOper As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as parameterunkid, ' ' +  @Select  as parameter UNION "
            End If

            strQ &= "SELECT " & _
                         "  parameterunkid " & _
                         ", parameter " & _
                         " FROM " & mstrDatabaseName & "..pipparameter_master " & _
                         " WHERE pipparameter_master.isactive = 1 "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "Select")

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getListForCombo; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOper Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpipparameter_master ( " & _
                    "  parameterunkid " & _
                    ", parameter " & _
                    ", viewtype " & _
                    ", applicableid " & _
                    ", sortorder " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", form_name " & _
                    ", ip " & _
                    ", machine_name " & _
                    ", isweb" & _
                  ") VALUES (" & _
                    " @parameterunkid " & _
                    ", @parameter " & _
                    ", @viewtype " & _
                    ", @applicableid " & _
                    ", @sortorder " & _
                    ", @audittype " & _
                    ", @audituserunkid " & _
                    ", GETDATE() " & _
                    ", @form_name " & _
                    ", @ip " & _
                    ", @machine_name " & _
                    ", @isweb" & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@parameter", SqlDbType.NVarChar, mstrParameter.Trim.Length, mstrParameter.ToString)
            objDataOperation.AddParameter("@viewtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintViewTypeId.ToString)
            objDataOperation.AddParameter("@applicableid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicableId.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Table")
            Language.setMessage(mstrModuleName, 2, "List")
            Language.setMessage(mstrModuleName, 3, "Sorry, This parameter is already defined. Please define new parameter.")
            Language.setMessage(mstrModuleName, 4, "Select")
            Language.setMessage(mstrModuleName, 5, "Employee")
            Language.setMessage(mstrModuleName, 6, "Manager")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
