﻿'************************************************************************************************************************************
'Class Name : clspipconcern_expectationTran.vb
'Purpose    :
'Date       :06-Dec-2023
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clspipconcern_expectationTran
    Private Const mstrModuleName = "clspipconcern_expectationTran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintConcernunkid As Integer
    Private mintPipformunkid As Integer
    Private mintParameterunkid As Integer
    Private mintItemunkid As Integer
    Private mintWorkprocessunkid As Integer
    Private mstrItemGrpGuId As String = ""
    Private mstrConcern_Value As String = String.Empty
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mintAuditUserId As Integer = 0
    Private mstrFormName As String = ""
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mblnIsWeb As Boolean = False

    Private mintCompanyUnkid As Integer = 0
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set concernunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Concernunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintConcernunkid
        End Get
        Set(ByVal value As Integer)
            mintConcernunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pipformunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Pipformunkid() As Integer
        Get
            Return mintPipformunkid
        End Get
        Set(ByVal value As Integer)
            mintPipformunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set parameterunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Parameterunkid() As Integer
        Get
            Return mintParameterunkid
        End Get
        Set(ByVal value As Integer)
            mintParameterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Itemunkid() As Integer
        Get
            Return mintItemunkid
        End Get
        Set(ByVal value As Integer)
            mintItemunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set workprocessunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Workprocessunkid() As Integer
        Get
            Return mintWorkprocessunkid
        End Get
        Set(ByVal value As Integer)
            mintWorkprocessunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemgrpguid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ItemGrpGuId() As String
        Get
            Return mstrItemGrpGuId
        End Get
        Set(ByVal value As String)
            mstrItemGrpGuId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set concern_value
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Concern_Value() As String
        Get
            Return mstrConcern_Value
        End Get
        Set(ByVal value As String)
            mstrConcern_Value = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set clienip
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property



#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  concernunkid " & _
                      ", pipformunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", workprocessunkid " & _
                      ", itemgrpguid " & _
                      ", concern_value " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM pipconcern_expectationtran " & _
                      " WHERE concernunkid = @concernunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@concernunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConcernunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintConcernunkid = CInt(dtRow.Item("concernunkid"))
                mintPipformunkid = CInt(dtRow.Item("pipformunkid"))
                mintParameterunkid = CInt(dtRow.Item("parameterunkid"))
                mintItemunkid = CInt(dtRow.Item("itemunkid"))
                mintWorkprocessunkid = CInt(dtRow.Item("workprocessunkid"))
                mstrItemGrpGuId = dtRow.Item("itemunkid").ToString()
                mstrConcern_Value = dtRow.Item("concern_value").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If


                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  concernunkid " & _
                      ", pipformunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", workprocessunkid " & _
                      ", itemgrpguid " & _
                      ", concern_value " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM pipconcern_expectationtran "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pipconcern_expectationtran) </purpose>
    Public Function Insert(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPipformunkid.ToString)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@workprocessunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkprocessunkid.ToString)
            objDataOperation.AddParameter("@itemgrpguid", SqlDbType.NVarChar, mstrItemGrpGuId.Trim.Length, mstrItemGrpGuId.ToString)
            objDataOperation.AddParameter("@concern_value", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrConcern_Value.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO pipconcern_expectationtran ( " & _
                      "  pipformunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", workprocessunkid " & _
                      ", itemgrpguid " & _
                      ", concern_value " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @pipformunkid " & _
                      ", @parameterunkid " & _
                      ", @itemunkid " & _
                      ", @workprocessunkid " & _
                      ", @itemgrpguid " & _
                      ", @concern_value " & _
                      ", @userunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voidloginemployeeunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintConcernunkid = dsList.Tables(0).Rows(0).Item(0)


            'If InsertAudiTrails(objDataOperation, enAuditType.ADD) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pipconcern_expectationtran) </purpose>
    Public Function Update(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@concernunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintConcernunkid.ToString)
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPipformunkid.ToString)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@workprocessunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkprocessunkid.ToString)
            objDataOperation.AddParameter("@itemgrpguid", SqlDbType.NVarChar, mstrItemGrpGuId.Trim.Length, mstrItemGrpGuId.ToString)
            objDataOperation.AddParameter("@concern_value", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrConcern_Value.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE pipconcern_expectationtran SET " & _
                      "  pipformunkid = @pipformunkid" & _
                      ", parameterunkid = @parameterunkid" & _
                      ", itemunkid = @itemunkid" & _
                      ", workprocessunkid = @workprocessunkid " & _
                      ", itemgrpguid = @itemgrpguid" & _
                      ", concern_value = @concern_value" & _
                      ", userunkid = @userunkid" & _
                      ", loginemployeeunkid = @loginemployeeunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      " WHERE isvoid = 0 AND itemgrpguid = @itemgrpguid and parameterunkid = @parameterunkid and itemunkid = @itemunkid and pipformunkid = @pipformunkid "


            If mintConcernunkid > 0 Then
                strQ &= " AND concernunkid = @concernunkid "
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'If InsertAudiTrails(objDataOperation, enAuditType.EDIT) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pipconcern_expectationtran) </purpose>
    Public Function Delete(ByVal intTranunkid As Integer, ByVal strItemgrpguid As String, ByVal intcategoryunkid As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = " UPDATE  pipconcern_expectationtran SET  " & _
                      "  isvoid = @isvoid " & _
                      ", voiddatetime = GETDATE() " & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                      ", voidreason = @voidreason " & _
                      " WHERE itemgrpguid = @itemgrpguid and parameterunkid=@parameterunkid and pipformunkid = @pipformunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@itemgrpguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strItemgrpguid.ToString)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intcategoryunkid.ToString)
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPipformunkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, mintPipformunkid, intTranunkid, strItemgrpguid.ToString, intcategoryunkid, -1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Deletw; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, Optional ByVal strGuid As String = "", Optional ByVal isFromTable As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        objDataOperation = New clsDataOperation
        Try

            'Dim Tables() As String = {"pipmonitoring_tran"}
            'For Each value As String In Tables
            '    Select Case value
            '        Case "pipmonitoring_tran"
            '            If isFromTable Then
            '                strQ = " SELECT pipconcern_expectationtran.concernunkid FROM " & value & " " & _
            '                          " LEFT JOIN pipconcern_expectationtran ON " & value & ".itemunkid = pipconcern_expectationtran.itemunkid " & _
            '                          " WHERE pipconcern_expectationtran.itemgrpguid = '" & strGuid & "' "
            '            Else
            '                strQ = "SELECT itemunkid FROM " & value & " WHERE itemunkid = @itemunkid "
            '            End If
            '    End Select
            '    objDataOperation.ClearParameters()
            '    objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            '    dsList = objDataOperation.ExecQuery(strQ, "Used")

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            '    If dsList.Tables("Used").Rows.Count > 0 Then
            '        Return True
            '        Exit For
            '    End If

            'Next
            'Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strItemgrpguid As String, _
                                    ByVal intParameterId As Integer, _
                                    ByVal intItemunkid As Integer, _
                                    ByVal intpipformunkid As Integer, _
                                    ByVal intconcernunkid As Integer, _
                                    Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If

        Try
            strQ = "SELECT " & _
                      "  concernunkid " & _
                      ", pipformunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", workprocessunkid " & _
                      ", itemgrpguid " & _
                      ", concern_value " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM pipconcern_expectationtran " & _
                      " WHERE itemgrpguid = @itemgrpguid " & _
                      " AND parameterunkid = @parameterunkid " & _
                      " AND pipformunkid = @pipformunkid " & _
                      " AND itemunkid = @itemunkid "

            If intconcernunkid > 0 Then
                strQ &= " AND concernunkid = @concernunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intpipformunkid)
            objDataOperation.AddParameter("@itemgrpguid", SqlDbType.NVarChar, strItemgrpguid.Trim.Length, strItemgrpguid)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intParameterId)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intItemunkid)
            objDataOperation.AddParameter("@concernunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intconcernunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Public Function InsertAudiTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try

    '        strQ = "INSERT INTO atpipconcern_expectationtran ( " & _
    '                  "  concernunkid " & _
    '                  ", pipformunkid " & _
    '                  ", parameterunkid " & _
    '                  ", itemunkid " & _
    '                  ", itemgrpguid " & _
    '                  ", concern_value " & _
    '                  ", audittype " & _
    '                  ", audituserunkid " & _
    '                  ", loginemployeeunkid " & _
    '                  ", auditdatetime " & _
    '                  ", ip " & _
    '                  ", machine_name " & _
    '                  ", form_name " & _
    '                  ", isweb" & _
    '                ") VALUES (" & _
    '                  "  @concernunkid " & _
    '                  ", @pipformunkid " & _
    '                  ", @parameterunkid " & _
    '                  ", @itemunkid " & _
    '                  ", @itemgrpguid " & _
    '                  ", @concern_value " & _
    '                  ", @audittype " & _
    '                  ", @audituserunkid " & _
    '                  ", @loginemployeeunkid " & _
    '                  ", GETDATE() " & _
    '                  ", @ip " & _
    '                  ", @machine_name " & _
    '                  ", @form_name " & _
    '                  ", @isweb" & _
    '                "); SELECT @@identity"


    '        objDataOperation.ClearParameters()



    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: InsertAudiTrails; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pipconcern_expectationtran) </purpose>
    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, _
                                                  ByVal eAuditType As enAuditType, _
                                                  ByVal intPIPformunkid As Integer, _
                                                  ByVal intConcernUnkid As Integer, _
                                                  ByVal strItemgrpguid As String, _
                                                  ByVal intParameterunkid As Integer, _
                                                  ByVal intItemunkid As Integer) As Boolean

        Dim StrQ As String = ""
        Try
            StrQ = " INSERT INTO atpipconcern_expectationtran ( " & _
                      "  concernunkid " & _
                      ", pipformunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", workprocessunkid " & _
                      ", itemgrpguid " & _
                      ", concern_value " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", loginemployeeunkid " & _
                      ", auditdatetime " & _
                      ", form_name " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", isweb" & _
                     " ) SELECT " & _
                     "  concernunkid " & _
                     ", pipformunkid " & _
                     ", parameterunkid " & _
                     ", itemunkid " & _
                     ", workprocessunkid " & _
                     ", itemgrpguid " & _
                     ", concern_value " & _
                     ", @audittype " & _
                     ", @audituserunkid " & _
                     ", @loginemployeeunkid " & _
                     ", GETDATE() " & _
                     ", @form_name " & _
                     ", @ip " & _
                     ", @machine_name " & _
                     ", @isweb " & _
                     "  FROM pipconcern_expectationtran WHERE 1=1 AND pipconcern_expectationtran.parameterunkid = @parameterunkid and pipformunkid=@pipformunkid AND pipconcern_expectationtran.itemgrpguid = @itemgrpguid "


            objDataOperation.ClearParameters()

            If intConcernUnkid > 0 Then
                StrQ &= " AND concernunkid = @concernunkid "
                objDataOperation.AddParameter("@concernunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intConcernUnkid)
            End If

            If intItemunkid > 0 Then
                StrQ &= " AND pipconcern_expectationtran.Itemunkid = @Itemunkid "
                objDataOperation.AddParameter("@Itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intItemunkid)
            End If

            objDataOperation.AddParameter("@itemgrpguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strItemgrpguid)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intParameterunkid)
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPIPformunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pipconcern_expectationtran) </purpose>
    Public Function isValueExist(ByVal strFieldValue As String, _
                                            ByVal strGuid As String, _
                                            ByVal intParameterunkid As Integer, _
                                            ByVal intItemunkid As Integer, _
                                            ByVal intpipformunkid As Integer, _
                                            Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT concernunkid FROM pipconcern_expectationtran  " & _
                      " WHERE itemunkid = @Itemunkid " & _
                      " and isvoid = 0 " & _
                      " and concern_value = @concern_value " & _
                      " and parameterunkid = @parameterunkid " & _
                      " and pipformunkid = @pipformunkid " & _
                      " and itemgrpguid <> @itemgrpguid "

            objDataOperation.AddParameter("@itemgrpguid", SqlDbType.NVarChar, mstrItemGrpGuId.Length, strGuid)
            objDataOperation.AddParameter("@concern_value", SqlDbType.NVarChar, mstrItemGrpGuId.Length, strFieldValue)
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intpipformunkid)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intItemunkid)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intParameterunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isValueExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


End Class