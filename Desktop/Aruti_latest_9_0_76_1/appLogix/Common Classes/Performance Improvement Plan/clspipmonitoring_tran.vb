﻿'************************************************************************************************************************************
'Class Name : clspipmonitoring_tran.vb
'Purpose    :
'Date       :07-Dec-2023
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clspipmonitoring_tran
    Private Shared ReadOnly mstrModuleName As String = "clspipmonitoring_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintPipmoniteringunkid As Integer
    Private mintPipformunkid As Integer
    Private mintImprovementplanunkid As Integer
    Private mintParameterunkid As Integer
    Private mintItemunkid As Integer
    Private mdtProgressDate As Date
    Private mintStatusunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mblnIsSubmit As Boolean = False
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mintAuditUserId As Integer = 0
    Private mstrFormName As String = ""
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mblnIsWeb As Boolean = False

    Private mintCompanyUnkid As Integer = 0
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pipmoniteringunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Pipmoniteringunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintPipmoniteringunkid
        End Get
        Set(ByVal value As Integer)
            mintPipmoniteringunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pipformunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Pipformunkid() As Integer
        Get
            Return mintPipformunkid
        End Get
        Set(ByVal value As Integer)
            mintPipformunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set improvementplanunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Improvementplanunkid() As Integer
        Get
            Return mintImprovementplanunkid
        End Get
        Set(ByVal value As Integer)
            mintImprovementplanunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set parameterunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Parameterunkid() As Integer
        Get
            Return mintParameterunkid
        End Get
        Set(ByVal value As Integer)
            mintParameterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Itemunkid() As Integer
        Get
            Return mintItemunkid
        End Get
        Set(ByVal value As Integer)
            mintItemunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set progressdate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ProgressDate() As DateTime
        Get
            Return mdtProgressDate
        End Get
        Set(ByVal value As DateTime)
            mdtProgressDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issubmit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsSubmit() As Boolean
        Get
            Return mblnIsSubmit
        End Get
        Set(ByVal value As Boolean)
            mblnIsSubmit = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set clienip
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  pipmoniteringunkid " & _
                      ", pipformunkid " & _
                      ", improvementplanunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", progressdate " & _
                      ", statusunkid " & _
                      ", remark " & _
                      ", issubmit " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM pipmonitoring_tran " & _
                      " WHERE pipmoniteringunkid = @pipmoniteringunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipmoniteringunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPipmoniteringunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPipmoniteringunkid = CInt(dtRow.Item("pipmoniteringunkid"))
                mintPipformunkid = CInt(dtRow.Item("pipformunkid"))
                mintImprovementplanunkid = CInt(dtRow.Item("improvementplanunkid"))
                mintParameterunkid = CInt(dtRow.Item("parameterunkid"))
                mintItemunkid = CInt(dtRow.Item("itemunkid"))

                If IsDBNull(dtRow.Item("progressdate")) = False Then
                    mdtProgressDate = dtRow.Item("progressdate")
                End If

                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrRemark = dtRow.Item("remark").ToString
                mblnIsSubmit = CBool(dtRow.Item("issubmit"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xPIPFormunkid As Integer, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal xImprovementPlanId As Integer = 0, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT * FROM " & _
                      " ( " & _
                      " SELECT " & _
                      "  ISNULL(pipmonitoring_tran.pipmoniteringunkid,0) AS pipmoniteringunkid " & _
                      ", ISNULL(pipmonitoring_tran.pipformunkid,pipimprovement_plantran.pipformunkid) AS pipformunkid " & _
                      ", ISNULL(pipmonitoring_tran.improvementplanunkid,pipimprovement_plantran.improvementplanunkid) AS improvementplanunkid " & _
                      ", ISNULL(pipmonitoring_tran.parameterunkid,pipimprovement_plantran.parameterunkid)  AS parameterunkid" & _
                      ", ISNULL(pipmonitoring_tran.itemunkid,pipimprovement_plantran.itemunkid) AS itemunkid " & _
                      ", ROW_NUMBER()OVER(PARTITION BY ISNULL(pipmonitoring_tran.pipformunkid,pipimprovement_plantran.pipformunkid),ISNULL(pipmonitoring_tran.improvementplanunkid,pipimprovement_plantran.improvementplanunkid) ORDER BY pipmonitoring_tran.progressdate DESC) AS RowNo " & _
                      ", pipmonitoring_tran.progressdate " & _
                      ", pipimprovement_plantran.improvement_goal " & _
                      ", pipimprovement_plantran.improvement_value " & _
                      ", pipimprovement_plantran.duedate " & _
                      ", ISNULL(pipmonitoring_tran.statusunkid,0) AS statusunkid " & _
                      ", ISNULL(piprating_master.optionvalue,'') AS status " & _
                      ", ISNULL(pipmonitoring_tran.remark,'') AS remarks " & _
                      ", ISNULL(pipmonitoring_tran.issubmit,0) AS issubmit " & _
                      ", pipmonitoring_tran.userunkid " & _
                      ", pipmonitoring_tran.isvoid " & _
                      ", pipmonitoring_tran.voiduserunkid " & _
                      ", pipmonitoring_tran.voiddatetime " & _
                      ", pipmonitoring_tran.voidreason " & _
                      " FROM pipimprovement_plantran " & _
                      " LEFT JOIN pipmonitoring_tran ON pipimprovement_plantran.improvementplanunkid = pipmonitoring_tran.improvementplanunkid AND ISNULL(pipmonitoring_tran.isvoid,0) = 0 " & _
                      " LEFT JOIN piprating_master ON piprating_master.pipratingunkid = pipmonitoring_tran.statusunkid AND piprating_master.isvoid = 0 " & _
                      " WHERE ISNULL(pipimprovement_plantran.issubmit,0) = 1 AND ISNULL(pipmonitoring_tran.pipformunkid,pipimprovement_plantran.pipformunkid) = @pipformunkid "

            If xImprovementPlanId > 0 Then
                strQ &= " AND ISNULL(pipmonitoring_tran.improvementplanunkid,pipimprovement_plantran.improvementplanunkid) = @improvementplanunkid "
            End If

            If blnOnlyActive Then
                strQ &= "  AND pipimprovement_plantran.isvoid = 0 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If

            strQ &= " ) AS A WHERE A.RowNo = 1 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPIPFormunkid)
            objDataOperation.AddParameter("@improvementplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xImprovementPlanId)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pipmonitoring_tran) </purpose>
    Public Function Insert(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPipformunkid.ToString)
            objDataOperation.AddParameter("@improvementplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImprovementplanunkid.ToString)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)

            If IsDBNull(mdtProgressDate) = False AndAlso mdtProgressDate <> Nothing Then
                objDataOperation.AddParameter("@progressdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProgressDate.ToString)
            Else
                objDataOperation.AddParameter("@progressdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmit.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO pipmonitoring_tran ( " & _
                      "  pipformunkid " & _
                      ", improvementplanunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", progressdate " & _
                      ", statusunkid " & _
                      ", issubmit " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @pipformunkid " & _
                      ", @improvementplanunkid " & _
                      ", @parameterunkid " & _
                      ", @itemunkid " & _
                      ", @progressdate " & _
                      ", @statusunkid " & _
                      ", @issubmit " & _
                      ", @remark " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPipmoniteringunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAudiTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pipmonitoring_tran) </purpose>
    Public Function Update(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintPipmoniteringunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipmoniteringunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPipmoniteringunkid.ToString)
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPipformunkid.ToString)
            objDataOperation.AddParameter("@improvementplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImprovementplanunkid.ToString)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)

            If IsDBNull(mdtProgressDate) = False AndAlso mdtProgressDate <> Nothing Then
                objDataOperation.AddParameter("@progressdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProgressDate.ToString)
            Else
                objDataOperation.AddParameter("@progressdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmit.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = " UPDATE pipmonitoring_tran SET " & _
                      "  pipformunkid = @pipformunkid" & _
                      ", improvementplanunkid = @improvementplanunkid" & _
                      ", parameterunkid = @parameterunkid" & _
                      ", itemunkid = @itemunkid" & _
                      ", progressdate = @progressdate " & _
                      ", statusunkid = @statusunkid" & _
                      ", issubmit = @issubmit " & _
                      ", remark = @remark" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      " WHERE isvoid = 0 AND pipmoniteringunkid = @pipmoniteringunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAudiTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pipmonitoring_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = " UPDATE pipmonitoring_tran SET " & _
                      " isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = GETDATE()" & _
                      ", voidreason = @voidreason " & _
                      " WHERE isvoid = 0 AND pipmoniteringunkid = @pipmoniteringunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@pipmoniteringunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Pipmoniteringunkid(objDataOperation) = intUnkid

            If InsertAudiTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@pipmoniteringunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iPIPFormId As Integer, Optional ByVal iImprovementPlanId As Integer = 0, Optional ByVal iMonitoringId As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  pipmoniteringunkid " & _
                      ", pipformunkid " & _
                      ", improvementplanunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", statusunkid " & _
                      ", progressdate " & _
                      ", issubmit " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM pipmonitoring_tran " & _
                      " WHERE isvoid = 0 AND pipformunkid = @pipformunkid AND issubmit = 1 "

            If iImprovementPlanId > 0 Then
                strQ &= " AND improvementplanunkid = @improvementplanunkid"
            End If

            If iMonitoringId > 0 Then
                strQ &= " AND pipmoniteringunkid = @pipmoniteringunkid"
            End If

            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iPIPFormId)
            objDataOperation.AddParameter("@pipmoniteringunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iImprovementPlanId)
            objDataOperation.AddParameter("@improvementplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iMonitoringId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pipconcern_expectationtran) </purpose>
    Public Function InsertAudiTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = " INSERT INTO atpipmonitoring_tran ( " & _
                      " pipmoniteringunkid " & _
                      ", pipformunkid " & _
                      ", improvementplanunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", progressdate " & _
                      ", statusunkid " & _
                      ", issubmit " & _
                      ", remark " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb" & _
                    ") VALUES (" & _
                       " @pipmoniteringunkid " & _
                      ", @pipformunkid " & _
                      ", @improvementplanunkid " & _
                      ", @parameterunkid " & _
                      ", @itemunkid " & _
                      ", @progressdate " & _
                      ", @statusunkid " & _
                      ", @issubmit " & _
                      ", @remark " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @isweb" & _
                    "); SELECT @@identity"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipmoniteringunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPipmoniteringunkid.ToString)
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPipformunkid.ToString)
            objDataOperation.AddParameter("@improvementplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImprovementplanunkid.ToString)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)

            If IsDBNull(mdtProgressDate) = False AndAlso mdtProgressDate <> Nothing Then
                objDataOperation.AddParameter("@progressdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtProgressDate.ToString)
            Else
                objDataOperation.AddParameter("@progressdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@issubmit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmit.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)

            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAudiTrails; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pipconcern_expectationtran) </purpose>
    ''' 
    Public Sub SendNotificationForFinalAssessment(ByVal mintPIPFormId As Integer, ByVal mstrPIPFormNo As String, ByVal mintEmployeeId As Integer, ByVal mstrEmployeeCode As String, ByVal mstrEmployee As String _
                                                                           , ByVal mintApproverUserId As Integer, ByVal mstrApproverName As String, ByVal mstrApproverEmail As String _
                                                                           , Optional ByVal intCompanyUnkId As Integer = 0, Optional ByVal strArutiSelfServiceURL As String = "", Optional ByVal iLoginTypeId As Integer = 0)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objMail As New clsSendMail
        Dim strLink As String = ""
        Try

            If mstrPIPFormNo.Trim.Length <= 0 OrElse mstrEmployeeCode.Trim.Length <= 0 OrElse mstrEmployee.Trim.Length <= 0 OrElse mintApproverUserId <= 0 OrElse mstrApproverEmail.Trim.Length <= 0 Then Exit Sub

            If strArutiSelfServiceURL.Trim.Length > 0 Then
                strLink = strArutiSelfServiceURL & "/PIP/wPgEmployeePIPForm.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(CInt(clspipsettings_master.enPIPConfiguration.ASSESSOR_MAPPING) & "|" & intCompanyUnkId.ToString & "|" & mintEmployeeId.ToString & "|" & mintApproverUserId.ToString() & "|" & mintPIPFormId.ToString))
            End If


            Dim mstrSubject As String = Language.getMessage(mstrModuleName, 1, "Notification for PIP Final Assessment")

            Dim strMessage As String = ""

            strMessage = "<HTML> <BODY>"

            strMessage &= Language.getMessage(mstrModuleName, 2, "Dear") & " " & getTitleCase(mstrApproverName) & ", <BR><BR>"

            'strMessage &= Language.getMessage(mstrModuleName, 3, "Please note that the progress review for") & " <B>" & mstrEmployeeCode.Trim() & " - " & getTitleCase(mstrEmployee) & "</B>'s " & Language.getMessage(mstrModuleName, 4, "PIP Form No") & " <B>(" & mstrPIPFormNo.Trim() & ")" & "</B> " & Language.getMessage(mstrModuleName, 5, "is complete.")
            strMessage &= Language.getMessage(mstrModuleName, 3, "Please note that the progress review for") & " <B> " & getTitleCase(mstrEmployee) & "</B>'s " & Language.getMessage(mstrModuleName, 4, "PIP Form No") & " <B>(" & mstrPIPFormNo.Trim() & ")" & "</B> " & Language.getMessage(mstrModuleName, 5, "is complete.")
            strMessage &= "<BR></BR><BR></BR>"
            strMessage &= Language.getMessage(mstrModuleName, 9, "Click on the link below to review progress.")
            strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"


            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"

            objMail._Subject = mstrSubject
            objMail._Message = strMessage
            objMail._ToEmail = mstrApproverEmail.Trim

            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrFormName
                objMail._WebClientIP = mstrClientIP
                objMail._WebHostName = mstrHostName
            End If
            objMail._LogEmployeeUnkid = 0
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = mintUserunkid
            objMail._SenderAddress = IIf(mstrApproverEmail.Trim.Length < 0, mstrApproverName.Trim, mstrApproverEmail.Trim)
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PIP_MGT
            objMail.SendMail(intCompanyUnkId)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotificationForProgressMonitoring; Module Name: " & mstrModuleName)
        Finally
            objMail = Nothing
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Notification for PIP Final Assessment")
            Language.setMessage(mstrModuleName, 2, "Dear")
            Language.setMessage(mstrModuleName, 3, "Please note that the progress review for")
            Language.setMessage(mstrModuleName, 4, "PIP Form No")
            Language.setMessage(mstrModuleName, 5, "is complete.")
            Language.setMessage(mstrModuleName, 9, "Click on the link below to review progress.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class