﻿Imports eZeeCommonLib
Public Class clspipform_master

    Private Shared ReadOnly mstrModuleName As String = "clspipform_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintPIPFormunkid As Integer = -1
    Private mstrPIPFormNo As String = ""
    Private mdtPIPFormDate As Date = Nothing
    Private mintEmployeeunkid As Integer = 0
    Private mdtStartDate As Date = Nothing
    Private mdtEndDate As Date = Nothing
    Private mintStatusunkid As Integer = enPIPStatus.Pending    'Pending
    Private mstrFinalAssessmentRemark As String = ""
    Private mintUserunkid As Integer = 0
    Private mintLoginEmployeeunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mintVoidLoginEmployeeunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private menViewType As enPDPCustomItemViewType = enPDPCustomItemViewType.List

    Private mdtAreaofConcernItem As DataTable = Nothing
    Private mstrItemgrpguid As String = ""
    Private mstrDatabaseName As String = ""
    Private mblnConcenData As Boolean = False
    Private mintConcernunkid As Integer = -1
    Private mintParameterunkid As Integer = -1
    Private mintConcernItemunkid As Integer = 0
    Private mstrConcernvalue As String = ""
    Private mintWorkprocessunkid As Integer
#End Region

#Region "Enum"

    Public Enum enPIPStatus
        Pending = -999
    End Enum

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pipformunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _PIPFormunkid() As Integer
        Get
            Return mintPIPFormunkid
        End Get
        Set(ByVal value As Integer)
            mintPIPFormunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pipformno
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _PIPFormNo() As String
        Get
            Return mstrPIPFormNo
        End Get
        Set(ByVal value As String)
            mstrPIPFormNo = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pipformdate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _PIPFormDate() As Date
        Get
            Return mdtPIPFormDate
        End Get
        Set(ByVal value As Date)
            mdtPIPFormDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _StartDate() As Date
        Get
            Return mdtStartDate
        End Get
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _EndDate() As Date
        Get
            Return mdtEndDate
        End Get
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set finalAssessmentRemark
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FinalAssessmentRemark() As String
        Get
            Return mstrFinalAssessmentRemark
        End Get
        Set(ByVal value As String)
            mstrFinalAssessmentRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VoidLoginEmployeeunkid() As Integer
        Get
            Return mintVoidLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _DtAreaofConcernItems() As DataTable
        Get
            Return mdtAreaofConcernItem
        End Get
        Set(ByVal value As DataTable)
            mdtAreaofConcernItem = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set databasename
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isconcerndata
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _IsConcernData() As Boolean
        Set(ByVal value As Boolean)
            mblnConcenData = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set concerunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Concernunkid() As Integer
        Get
            Return mintConcernunkid
        End Get
        Set(ByVal value As Integer)
            mintConcernunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set parameterid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Parameterid() As Integer
        Get
            Return mintParameterunkid
        End Get
        Set(ByVal value As Integer)
            mintParameterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set concernitemunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ConcernItemunkid() As Integer
        Get
            Return mintConcernItemunkid
        End Get
        Set(ByVal value As Integer)
            mintConcernItemunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set concernvalue
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Concernvalue() As String
        Get
            Return mstrConcernvalue
        End Get
        Set(ByVal value As String)
            mstrConcernvalue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemgrpguid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _Itemgrpguid() As String
        Set(ByVal value As String)
            mstrItemgrpguid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemgrpguid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ViewType() As enPDPCustomItemViewType
        Get
            Return menViewType
        End Get
        Set(ByVal value As enPDPCustomItemViewType)
            menViewType = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set workprocessunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Workprocessunkid() As Integer
        Get
            Return mintWorkprocessunkid
        End Get
        Set(ByVal value As Integer)
            mintWorkprocessunkid = value
        End Set
    End Property

#End Region

#Region " Constuctor "

    Public Sub New()
        mdtAreaofConcernItem = New DataTable()

        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("itemunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtAreaofConcernItem.Columns.Add(dCol)

            dCol = New DataColumn("fieldvalue")
            dCol.DataType = System.Type.GetType("System.String")
            mdtAreaofConcernItem.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       " pipformunkid " & _
                       ",pipformno " & _
                       ",pipformdate " & _
                       ",employeeunkid " & _
                       ",startdate " & _
                       ",enddate " & _
                       ",statusunkid " & _
                       ",assessment_remark " & _
                       ",userunkid " & _
                       ",loginemployeeunkid " & _
                       ",isvoid " & _
                       ",voiduserunkid " & _
                       ",voidloginemployeeunkid " & _
                       ",voiddatetime " & _
                       ",voidreason " & _
                       " FROM pipform_master " & _
                       " WHERE pipformunkid = @pipformunkid "

            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPIPFormunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPIPFormunkid = CInt(dtRow.Item("pipformunkid"))
                mstrPIPFormNo = dtRow.Item("pipformno").ToString
                mdtPIPFormDate = CDate(dtRow.Item("pipformdate"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtStartDate = CDate(dtRow.Item("startdate"))
                mdtEndDate = CDate(dtRow.Item("enddate"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrFinalAssessmentRemark = dtRow.Item("assessment_remark").ToString()
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginEmployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintVoidLoginEmployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>    ''' 
    Public Function GetList(ByVal xDatabaseName As String _
                                    , ByVal xUserUnkid As Integer _
                                    , ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer _
                                    , ByVal xPeriodStart As DateTime _
                                    , ByVal xPeriodEnd As DateTime _
                                    , ByVal xUserModeSetting As String _
                                    , ByVal xOnlyApproved As Boolean _
                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                    , ByVal strTableName As String _
                                    , ByVal mblnIncludeGrp As Boolean _
                                    , Optional ByVal mstrFilter As String = "" _
                                    , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                    ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            If blnApplyUserAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)


            If mblnIncludeGrp Then

                strQ = "SELECT DISTINCT  " & _
                       " CAST(1 AS BIT) AS IsGrp " & _
                       ", 0 AS pipformunkid " & _
                       ", '' AS pipformno " & _
                       ", NULL AS pipformdate " & _
                       ", pipform_master.employeeunkid " & _
                       ", ISNULL(hremployee_master.employeecode,'') AS EmployeeCode " & _
                       ", ISNULL(hremployee_master.firstname,'') + '  ' +ISNULL(hremployee_master.surname,'') AS Employee " & _
                       ", ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                       ", 0 AS LineManagerId " & _
                       ", '' AS LineManager " & _
                       ", 0 AS LMUserId " & _
                       ", 0 AS coach_employeeunkid " & _
                       ", '' AS coach " & _
                       ", 0 AS CoachUserId " & _
                       ", NULL AS startdate " & _
                       ", NULL  AS enddate " & _
                       ", 0 AS statusunkid " & _
                       ", '' AS status " & _
                       ", '' AS assessment_remark " & _
                       ", 0 AS userunkid " & _
                       ", 0 AS loginemployeeunkid " & _
                       ", CAST(0 AS BIT) isvoid " & _
                       ", 0 AS voiduserunkid " & _
                       ", 0 AS voidloginemployeeunkid " & _
                       ", NULL AS voiddatetime " & _
                       ", '' AS voidreason " & _
                       " FROM pipform_master " & _
                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = pipform_master.employeeunkid "


                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry
                End If

                If blnApplyUserAccessFilter = True Then
                    If xUACQry.Trim.Length > 0 Then
                        strQ &= xUACQry
                    End If
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry
                End If

                strQ &= " WHERE ISNULL(pipform_master.isvoid,0) = 0 "

                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry
                    End If
                End If

                If mstrFilter.Trim.Length > 0 Then
                    strQ &= " AND " & mstrFilter
                End If

                strQ &= " UNION "

            End If


            strQ &= "SELECT " & _
                       " CAST(0 AS BIT) AS IsGrp " & _
                       ", pipformunkid " & _
                       ", pipformno " & _
                       ",pipformdate " & _
                       ",pipform_master.employeeunkid " & _
                       ",ISNULL(hremployee_master.employeecode,'') AS EmployeeCode " & _
                       ",ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                       ",ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                       ", ISNULL(RepTable.reporttoemployeeunkid,0) AS LineManagerId " & _
                       ", ISNULL(RepTable.LineManager,'') AS LineManager " & _
                       ", ISNULL(RepTable.LMUserId,0) AS LMUserId " & _
                       ", ISNULL(A.coach_employeeunkid,0) AS  coach_employeeunkid " & _
                       ", ISNULL(A.coach,'') AS coach " & _
                       ", ISNULL(A.CoachUserId,0) AS CoachUserId " & _
                       ", startdate " & _
                       ", enddate " & _
                       ", statusunkid " & _
                       ", CASE WHEN pipform_master.statusunkid > 0 THEN  ISNULL(piprating_master.optionvalue,'') ELSE @Pending END AS status " & _
                       ", pipform_master.assessment_remark " & _
                       ", pipform_master.userunkid " & _
                       ", pipform_master.loginemployeeunkid " & _
                       ", pipform_master.isvoid " & _
                       ", pipform_master.voiduserunkid " & _
                       ", pipform_master.voidloginemployeeunkid " & _
                       ", pipform_master.voiddatetime " & _
                       ", pipform_master.voidreason " & _
                       " FROM pipform_master " & _
                       " LEFT JOIN piprating_master ON piprating_master.pipratingunkid = pipform_master.statusunkid AND piprating_master.isvoid = 0 AND ratingtypeunkid = @ratingtypeunkid " & _
                       " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = pipform_master.employeeunkid " & _
                       " LEFT JOIN (SELECT " & _
                       " 		             coachee_employeeunkid " & _
                       " 	                ,coach_employeeunkid " & _
                       " 	                ,ISNULL(u.userunkid,0) AS CoachUserId " & _
                       " 	                ,ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS coach " & _
                       " 	                ,ROW_NUMBER() OVER (PARTITION BY coachee_employeeunkid ORDER BY effective_date DESC) AS ROWNO " & _
                       " 	                FROM pdpcoaching_nomination_form " & _
                       " 	                LEFT JOIN hremployee_master	ON hremployee_master.employeeunkid = pdpcoaching_nomination_form.coach_employeeunkid " & _
                       "                    LEFT JOIN hrmsconfiguration..cfuser_master AS u ON u.employeeunkid = pdpcoaching_nomination_form.coach_employeeunkid AND u.isactive = 1 " & _
                       " 	                WHERE pdpcoaching_nomination_form.isvoid = 0 " & _
                       "                ) AS A	ON A.coachee_employeeunkid = hremployee_master.employeeunkid	AND A.ROWNO = 1 " & _
                       " LEFT JOIN ( " & _
                       "                    SELECT " & _
                       "                        hremployee_reportto.employeeunkid AS employeeunkid " & _
                       "                        ,hremployee_reportto.employeeunkid AS reporttoemployeeunkid " & _
                       "                        ,ISNULL(u.userunkid,0) AS LMUserId " & _
                       "                        ,ISNULL(R_Emp.firstname,'')+ ' ' + ISNULL(R_Emp.surname,'') As LineManager " & _
                       "                        ,ROW_NUMBER() OVER (PARTITION BY hremployee_reportto.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                       "                    FROM hremployee_reportto " & _
                       "                    LEFT JOIN hremployee_master AS R_Emp ON hremployee_reportto.reporttoemployeeunkid = R_Emp.employeeunkid " & _
                       "                    LEFT JOIN hrmsconfiguration..cfuser_master AS u ON u.employeeunkid = R_Emp.employeeunkid AND u.isactive = 1 " & _
                       "                    WHERE ishierarchy=1 AND hremployee_reportto.isvoid=0  AND CONVERT(CHAR(8), effectivedate, 112) <= @effectivedate " & _
                       " ) AS RepTable ON RepTable.employeeunkid=hremployee_master.employeeunkid AND RepTable.Rno = 1  "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE ISNULL(pipform_master.isvoid,0) = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " ORDER BY ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,''), IsGrp DESC , pipformunkid DESC  "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@ratingtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, clspiprating_master.enPIPRatingType.Final_Assessment)
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Pending"))
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, xPeriodEnd)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function SavePIPEmployeeData(ByVal xCompanyUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception
        Dim objConcern As New clspipconcern_expectationTran
        Dim objImprovement As New clspipimprovement_plantran

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If isExist(mstrPIPFormNo, mintPIPFormunkid, objDataOperation) Then
                If Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                mstrItemgrpguid = Guid.NewGuid().ToString()
                If Insert(xCompanyUnkid, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mblnConcenData Then

                If menViewType = enPDPCustomItemViewType.Table Then

                    If IsNothing(mdtAreaofConcernItem) = False AndAlso mdtAreaofConcernItem.Rows.Count > 0 Then
                        objConcern._Concernunkid = mintConcernunkid
                        objConcern._ItemGrpGuId = mstrItemgrpguid
                        objConcern._Pipformunkid = mintPIPFormunkid
                        objConcern._Parameterunkid = mintParameterunkid
                        objConcern._Workprocessunkid = mintWorkprocessunkid
                        objConcern._Isvoid = False
                        objConcern._Userunkid = mintUserunkid
                        objConcern._AuditUserId = mintAuditUserId
                        objConcern._Loginemployeeunkid = mintLoginEmployeeunkid
                        objConcern._HostName = mstrHostName
                        objConcern._ClientIP = mstrClientIP
                        objConcern._IsWeb = mblnIsWeb
                        objConcern._DatabaseName = mstrDatabaseName
                        objConcern._FormName = mstrFormName

                        For Each drow As DataRow In mdtAreaofConcernItem.Rows
                            objConcern._Itemunkid = drow("itemunkid").ToString()
                            objConcern._Concern_Value = drow("fieldvalue").ToString()

                            If objConcern.isExist(mstrItemgrpguid, mintParameterunkid, CInt(drow("itemunkid")), mintPIPFormunkid, mintConcernunkid, objDataOperation) = False Then
                                If objConcern.Insert(objDataOperation) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If objConcern.InsertAuditTrails(objDataOperation, enAuditType.ADD, mintPIPFormunkid, mintConcernunkid, mstrItemgrpguid, mintParameterunkid, CInt(drow("itemunkid"))) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Else
                                If objConcern.Update(objDataOperation) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If objConcern.InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintPIPFormunkid, mintConcernunkid, mstrItemgrpguid, mintParameterunkid, CInt(drow("itemunkid"))) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            End If

                        Next

                    End If

                ElseIf menViewType = enPDPCustomItemViewType.List Then

                    objConcern._Concernunkid = mintConcernunkid
                    objConcern._ItemGrpGuId = mstrItemgrpguid
                    objConcern._Pipformunkid = mintPIPFormunkid
                    objConcern._Parameterunkid = mintParameterunkid
                    objConcern._Workprocessunkid = mintWorkprocessunkid
                    objConcern._Isvoid = False
                    objConcern._Userunkid = mintUserunkid
                    objConcern._AuditUserId = mintAuditUserId
                    objConcern._Loginemployeeunkid = mintLoginEmployeeunkid
                    objConcern._HostName = mstrHostName
                    objConcern._ClientIP = mstrClientIP
                    objConcern._IsWeb = mblnIsWeb
                    objConcern._DatabaseName = mstrDatabaseName
                    objConcern._FormName = mstrFormName
                    objConcern._Itemunkid = mintConcernItemunkid
                    objConcern._Concern_Value = mstrConcernvalue

                    If objConcern.isExist(mstrItemgrpguid, mintParameterunkid, mintConcernItemunkid, mintPIPFormunkid, mintConcernunkid, objDataOperation) = False Then
                        If objConcern.Insert(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If objConcern.InsertAuditTrails(objDataOperation, enAuditType.ADD, mintPIPFormunkid, mintConcernunkid, mstrItemgrpguid, mintParameterunkid, mintConcernItemunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Else
                        If objConcern.Update(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If objConcern.InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintPIPFormunkid, mintConcernunkid, mstrItemgrpguid, mintParameterunkid, mintConcernItemunkid) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If

                End If  ' If menViewType = enPDPCustomItemViewType.Table Then

            End If  'If mblnConcenData Then

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: SavePIPEmployeeData; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pipform_master) </purpose>
    Public Function Insert(ByVal xCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Dim intFormNoType As Integer = 0
        Dim mstrPIPformNotPrefix As String = ""
        Dim objConfig As New clsConfigOptions
        intFormNoType = objConfig.GetKeyValue(xCompanyId, "PIPFormNoType", xDataOpr)
        mstrPIPformNotPrefix = objConfig.GetKeyValue(xCompanyId, "PIPFormNoPrefix", xDataOpr)
        objConfig = Nothing

        If intFormNoType = 0 Then
            If isExist(mstrPIPFormNo) Then
                mstrMessage = Language.getMessage(mstrModuleName, 3, "This Form No is already defined. Please define new Form No.")
                Return False
            End If
        End If

        Try
            objDataOperation.AddParameter("@pipformno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPIPFormNo.ToString)
            objDataOperation.AddParameter("@pipformdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPIPFormDate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate.ToString)
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@assessment_remark", SqlDbType.NVarChar, mstrFinalAssessmentRemark.Length, mstrFinalAssessmentRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            strQ = "INSERT INTO  pipform_master ( " & _
                      "  pipformno " & _
                      ", pipformdate " & _
                      ", employeeunkid " & _
                      ", startdate " & _
                      ", enddate " & _
                      ", statusunkid " & _
                      ", assessment_remark " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                  ") VALUES (" & _
                      "  @pipformno " & _
                      ", @pipformdate " & _
                      ", @employeeunkid " & _
                      ", @startdate " & _
                      ", @enddate " & _
                      ", @statusunkid " & _
                      ", @assessment_remark " & _
                      ", @userunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voidloginemployeeunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPIPFormunkid = dsList.Tables(0).Rows(0).Item(0)


            If intFormNoType = 1 Then
                If Set_AutoNumber(objDataOperation, mintPIPFormunkid, "pipform_master", "pipformno", "pipformunkid", "NextPIPFormNo", mstrPIPformNotPrefix, xCompanyId) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
                If Get_Saved_Number(objDataOperation, mintPIPFormunkid, "pipform_master", "pipformno", "pipformunkid", mstrPIPFormNo) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If


            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pipform_master) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPIPFormunkid.ToString)
            objDataOperation.AddParameter("@pipformno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPIPFormNo.ToString)
            objDataOperation.AddParameter("@pipformdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPIPFormDate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate.ToString)
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@assessment_remark", SqlDbType.NVarChar, mstrFinalAssessmentRemark.Length, mstrFinalAssessmentRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            strQ = "UPDATE pipform_master SET " & _
                  "  pipformno = @pipformno " & _
                  ", pipformdate = @pipformdate " & _
                  ", employeeunkid = @employeeunkid " & _
                  ", startdate = @startdate " & _
                  ", enddate = @enddate " & _
                  ", statusunkid = @statusunkid " & _
                  ", assessment_remark = @assessment_remark " & _
                  ", userunkid = @userunkid " & _
                  ", loginemployeeunkid = @loginemployeeunkid " & _
                  ", isvoid = @isvoid" & _
                  ", voiduserunkid = @voiduserunkid" & _
                  ", voiddatetime = @voiddatetime" & _
                  ", voidreason = @voidreason " & _
                  "  WHERE pipformunkid = @pipformunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pipform_master) </purpose>
    ''' 
    Public Function Delete(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPIPFormunkid.ToString)

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE pipform_master SET " & _
                      " isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      "  WHERE pipformunkid = @pipformunkid "

            _PIPFormunkid = mintPIPFormunkid

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strFormNo As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                       " pipformunkid " & _
                       ",pipformno " & _
                       ",pipformdate " & _
                       ",employeeunkid " & _
                       ",startdate " & _
                       ",enddate " & _
                       ",statusunkid " & _
                       ",assessment_remark " & _
                       ",userunkid " & _
                       ",loginemployeeunkid " & _
                       ",isvoid " & _
                       ",voiduserunkid " & _
                       ",voidloginemployeeunkid " & _
                       ",voiddatetime " & _
                       ",voidreason " & _
                       " FROM pipform_master " & _
                       " WHERE  isvoid = 0 AND pipformno = @pipformno "

            objDataOperation.ClearParameters()

            If intUnkid > 0 Then
                strQ &= " AND pipformunkid = @pipformunkid"
            End If

            objDataOperation.AddParameter("@pipformno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strFormNo)
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPIPFormunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO atpipform_master ( " & _
                    "  pipformunkid " & _
                    ", pipformno " & _
                    ", pipformdate " & _
                    ", employeeunkid " & _
                    ", startdate " & _
                    ", enddate " & _
                    ", statusunkid " & _
                    ", assessment_remark " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", loginemployeeunkid " & _
                    ", auditdatetime " & _
                    ", ip " & _
                    ", machine_name " & _
                    ", form_name " & _
                    ", isweb " & _
                    ") VALUES (" & _
                    "  @pipformunkid " & _
                    ", @pipformno " & _
                    ", @pipformdate " & _
                    ", @employeeunkid " & _
                    ", @startdate " & _
                    ", @enddate " & _
                    ", @statusunkid " & _
                    ", @assessment_remark " & _
                    ", @audittype " & _
                    ", @audituserunkid " & _
                    ", @loginemployeeunkid " & _
                    ", GETDATE() " & _
                    ", @ip " & _
                    ", @machine_name " & _
                    ", @form_name " & _
                    ", @isweb " & _
                    "  ) "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPIPFormunkid.ToString)
            objDataOperation.AddParameter("@pipformno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPIPFormNo.ToString)
            objDataOperation.AddParameter("@pipformdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPIPFormDate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate.ToString)
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@assessment_remark", SqlDbType.NVarChar, mstrFinalAssessmentRemark.Length, mstrFinalAssessmentRemark.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function Get_CListItems_ForAddEdit(ByVal xConcernId As Integer) As DataTable

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation = New clsDataOperation
        Try
            StrQ = " SELECT " & _
                       " item " & _
                       ", ISNULL(CASE WHEN itemtypeid IN (" & CInt(clsassess_custom_items.enCustomType.FREE_TEXT) & "," & CInt(clsassess_custom_items.enCustomType.DATE_SELECTION) & "," & CInt(clsassess_custom_items.enCustomType.NUMERIC_DATA) & ") THEN ISNULL(pipconcern_expectationtran.concern_value, '') " & _
                       "                     WHEN itemtypeid = " & clsassess_custom_items.enCustomType.SELECTION & " AND  selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES) & ", " & CInt(clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES) & ") THEN cfcommon_master.name " & _
                       "            END, '') AS custom_value " & _
                       ", itemtypeid " & _
                       ", selectionmodeid " & _
                       ", CAST(NULL AS DATETIME) AS ddate " & _
                       ", 0 AS selectedid " & _
                       ", ISNULL(pipconcern_expectationtran.itemunkid, pipitem_master.itemunkid) AS customitemunkid " & _
                       ", pipitem_master.parameterunkid " & _
                       ", ISNULL(pipconcern_expectationtran.concern_value, '') AS customvalueId " & _
                       " FROM pipitem_master " & _
                       " LEFT JOIN pipconcern_expectationtran ON pipitem_master.itemunkid = pipconcern_expectationtran.itemunkid " & _
                       " LEFT JOIN cfcommon_master ON pipconcern_expectationtran.concern_value = CAST(masterunkid AS NVARCHAR(MAX)) " & _
                       "  AND itemtypeid = " & CInt(clsassess_custom_items.enCustomType.SELECTION) & " " & _
                       "  AND selectionmodeid IN (" & CInt(clsassess_custom_items.enSelectionMode.CAREER_DEVELOPMENT_COURSES) & ", " & CInt(clsassess_custom_items.enSelectionMode.JOB_CAPABILITIES_COURSES) & ") " & _
                       "  AND cfcommon_master.isactive = 1 " & _
                       " WHERE pipitem_master.isactive = 1 AND pipitem_master.isimprovementgoal = 0 AND pipconcern_expectationtran.concernunkid = '" & xConcernId & "'  "


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For index As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                If dsList.Tables(0).Rows(index)("itemtypeid") = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    If dsList.Tables(0).Rows(index)("custom_value").ToString().Length > 0 Then
                        Dim dt As Date = eZeeDate.convertDate(dsList.Tables(0).Rows(index)("custom_value").ToString).Date
                        dsList.Tables(0).Rows(index)("custom_value") = eZeeDate.convertDate(dsList.Tables(0).Rows(index)("custom_value").ToString).ToShortDateString()
                        dsList.Tables(0).Rows(index)("ddate") = dt
                    End If
                ElseIf dsList.Tables(0).Rows(index)("itemtypeid") = clsassess_custom_items.enCustomType.SELECTION Then
                    If Val(dsList.Tables(0).Rows(index)("customvalueId")) > 0 Then dsList.Tables(0).Rows(index)("selectedid") = CInt(Val(dsList.Tables(0).Rows(index)("customvalueId")))
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_CListItems_ForAddEdit; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList.Tables(0)
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function Get_Custom_Items_List(ByVal xPIPFormId As Integer, ByVal xParameterId As Integer, ByVal xItemId As Integer, ByVal xEmployeeId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation

        Try

            StrQ = " SELECT " & _
                         " pipconcern_expectationtran.concernunkid " & _
                         ", pipconcern_expectationtran.itemunkid AS customitemunkid " & _
                         ",pipconcern_expectationtran.concern_value AS custom_value " & _
                         ",pipitem_master.itemtypeid " & _
                         ",pipitem_master.selectionmodeid " & _
                         ",pipitem_master.isapplyworkprocess " & _
                         ",pipconcern_expectationtran.pipformunkid  " & _
                         ",pipconcern_expectationtran.itemunkid  " & _
                         ",pipconcern_expectationtran.parameterunkid  " & _
                         ",pipconcern_expectationtran.itemgrpguid  " & _
                         ",ISNULL(pipconcern_expectationtran.workprocessunkid,0) AS workprocessunkid  " & _
                         ",ISNULL(pipworkprocess_master.name,'') AS workprocess  " & _
                         " FROM pipconcern_expectationtran " & _
                         " JOIN atpipconcern_expectationtran ON atpipconcern_expectationtran.concernunkid = pipconcern_expectationtran.concernunkid AND atpipconcern_expectationtran.audittype = @audittype " & _
                         " LEFT JOIN hrmsConfiguration..cfuser_master ON atpipconcern_expectationtran.audituserunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                         " JOIN pipitem_master ON pipitem_master.itemunkid = atpipconcern_expectationtran.itemunkid AND pipitem_master.isactive = 1 " & _
                         " JOIN pipform_master ON pipform_master.pipformunkid = pipconcern_expectationtran.pipformunkid " & _
                         " LEFT JOIN pipworkprocess_master ON pipworkprocess_master.workprocessunkid = pipconcern_expectationtran.workprocessunkid " & _
                         " WHERE pipconcern_expectationtran.isvoid = 0 AND pipitem_master.isimprovementgoal = 0 AND pipform_master.employeeunkid = @employeeunkid" & _
                         " AND pipitem_master.itemunkid = @itemunkid AND pipitem_master.parameterunkid = @parameterunkid " & _
                         " AND pipform_master.pipformunkid = @pipformunkid " & _
                         " ORDER BY pipconcern_expectationtran.concernunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPIPFormId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xItemId)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xParameterId)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, enAuditType.ADD)
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows

                    Select Case CInt(dRow.Item("itemtypeid"))
                        Case clspdpitem_master.enPdpCustomType.SELECTION
                            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then

                                If CInt(dRow.Item("selectionmodeid")) = clspipitem_master.enPIPSelectionMode.JOB_CAPABILITIES_COURSES OrElse CInt(dRow.Item("selectionmodeid")) = clspipitem_master.enPIPSelectionMode.CAREER_DEVELOPMENT_COURSES Then
                                    Dim objCMaster As New clsCommon_Master
                                    objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                    dRow.Item("custom_value") = objCMaster._Name
                                    objCMaster = Nothing
                                End If
                            End If
                        Case clspdpitem_master.enPdpCustomType.DATE_SELECTION
                            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                dRow.Item("custom_value") = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                            End If
                        Case clspdpitem_master.enPdpCustomType.NUMERIC_DATA
                            If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                If IsNumeric(dRow.Item("custom_value")) Then
                                    dRow.Item("custom_value") = CDbl(dRow.Item("custom_value")).ToString
                                End If
                            End If
                    End Select

                Next
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Custom_Items_List; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function Get_CTableItems_ForAddEdit(ByVal xParameterId As Integer, Optional ByVal strEditGUID As String = "") As DataTable

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation = New clsDataOperation
        Try

            StrQ = " SELECT " & _
                       " pipitem_master.item " & _
                       " ,ISNULL(CASE  WHEN itemtypeid = " & clspipitem_master.enPIPCustomType.SELECTION & " AND selectionmodeid IN (" & CInt(clspipitem_master.enPIPSelectionMode.CAREER_DEVELOPMENT_COURSES) & ", " & CInt(clspipitem_master.enPIPSelectionMode.JOB_CAPABILITIES_COURSES) & ") THEN cfcommon_master.name " & _
                       "                       WHEN itemtypeid IN (" & CInt(clspipitem_master.enPIPCustomType.FREE_TEXT) & "," & CInt(clspipitem_master.enPIPCustomType.DATE_SELECTION) & "," & CInt(clspipitem_master.enPIPCustomType.NUMERIC_DATA) & ") THEN  pipconcern_expectationtran.concern_value " & _
                       "             END, '') AS custom_value " & _
                       ", itemtypeid " & _
                       ", selectionmodeid " & _
                       ", CAST(NULL AS DATETIME) AS ddate " & _
                       ", 0 AS selectedid " & _
                       ", ISNULL(pipconcern_expectationtran.itemunkid, pipitem_master.itemunkid) AS customitemunkid " & _
                       ", pipitem_master.parameterunkid " & _
                       ", ISNULL(pipconcern_expectationtran.concern_value, '') AS customvalueId " & _
                       ", pipitem_master.isapplyworkprocess " & _
                       ", ISNULL(pipworkprocess_master.workprocessunkid, 0) AS workprocessunkid " & _
                       ", ISNULL(pipworkprocess_master.name, '') AS workprocessname " & _
                       " FROM pipitem_master " & _
                       " LEFT JOIN pipconcern_expectationtran ON pipitem_master.itemunkid = pipconcern_expectationtran.itemunkid  AND pipconcern_expectationtran.itemgrpguid = '" & strEditGUID & "' " & _
                       " LEFT JOIN cfcommon_master ON pipconcern_expectationtran.concern_value = CAST(masterunkid AS NVARCHAR(MAX)) AND itemtypeid = " & CInt(clspipitem_master.enPIPCustomType.SELECTION) & " " & _
                       " AND selectionmodeid IN (" & CInt(clspipitem_master.enPIPSelectionMode.CAREER_DEVELOPMENT_COURSES) & ", " & CInt(clspipitem_master.enPIPSelectionMode.JOB_CAPABILITIES_COURSES) & ") " & _
                       " AND cfcommon_master.isactive = 1 " & _
                       " LEFT JOIN pipworkprocess_master ON pipworkprocess_master.workprocessunkid = pipconcern_expectationtran.workprocessunkid " & _
                       " WHERE pipitem_master.parameterunkid = @parameterunkid " & _
                       " AND pipitem_master.isactive = 1 AND pipitem_master.isimprovementgoal = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xParameterId)
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For index As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                If dsList.Tables(0).Rows(index)("itemtypeid") = clsassess_custom_items.enCustomType.DATE_SELECTION Then
                    If dsList.Tables(0).Rows(index)("custom_value").ToString().Length > 0 Then
                        Dim dt As Date = eZeeDate.convertDate(dsList.Tables(0).Rows(index)("custom_value").ToString).Date
                        dsList.Tables(0).Rows(index)("custom_value") = eZeeDate.convertDate(dsList.Tables(0).Rows(index)("custom_value").ToString).ToShortDateString()
                        dsList.Tables(0).Rows(index)("ddate") = dt
                    End If
                ElseIf dsList.Tables(0).Rows(index)("itemtypeid") = clsassess_custom_items.enCustomType.SELECTION Then
                    If Val(dsList.Tables(0).Rows(index)("customvalueId")) > 0 Then dsList.Tables(0).Rows(index)("selectedid") = CInt(Val(dsList.Tables(0).Rows(index)("customvalueId")))
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_CTableItems_ForAddEdit; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList.Tables(0)
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function Get_Custom_Items_Table(ByVal iPIPFormId As Integer, ByVal iParameterId As Integer, ByVal iEmployeeId As Integer, ByVal mblnIsImprovementGoal As Boolean) As DataTable
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim mdtFinal As DataTable = Nothing
        Dim dCol As DataColumn
        Dim objDataOperation As New clsDataOperation
        Try
            mdtFinal = New DataTable("CTList")

            Dim objCHeader As New clspipparameter_master
            objCHeader._Parameterunkid = iParameterId
            dCol = New DataColumn
            With dCol
                .ColumnName = "Header_Name"
                .Caption = Language.getMessage(mstrModuleName, 1, "Custom Header")
                .DataType = System.Type.GetType("System.String")
                .DefaultValue = objCHeader._Parameter
            End With
            mdtFinal.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Header_Id"
                .Caption = ""
                .DataType = System.Type.GetType("System.Int32")
                .DefaultValue = objCHeader._Parameterunkid
            End With
            mdtFinal.Columns.Add(dCol)


            objCHeader = Nothing

            Dim objCustomItems As New clspipitem_master
            dsList = objCustomItems.getComboList("List", mblnIsImprovementGoal, False, iParameterId)
            objCustomItems = Nothing

            If dsList.Tables(0).Rows.Count > 0 Then

                For Each drow As DataRow In dsList.Tables(0).Rows

                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "CItem_" & drow.Item("Id").ToString
                        .Caption = drow.Item("Name").ToString
                        .DataType = System.Type.GetType("System.String")
                        .DefaultValue = ""
                    End With
                    mdtFinal.Columns.Add(dCol)

                    dCol = New DataColumn
                    With dCol
                        .ColumnName = "CItem_" & drow.Item("Id").ToString & "Id"
                        .Caption = ""
                        .DataType = System.Type.GetType("System.Int32")
                        .DefaultValue = -1
                    End With
                    mdtFinal.Columns.Add(dCol)

                Next

                dCol = New DataColumn
                With dCol
                    .ColumnName = "concernunkid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = -1
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "itemtypeid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = -1
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "selectionmodeid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = -1
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "GUID"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.String")
                    .DefaultValue = ""
                End With
                mdtFinal.Columns.Add(dCol)

                dCol = New DataColumn
                With dCol
                    .ColumnName = "pipformunkid"
                    .Caption = ""
                    .DataType = System.Type.GetType("System.Int32")
                    .DefaultValue = -1
                End With
                mdtFinal.Columns.Add(dCol)

                StrQ = "SELECT " & _
                          " pipconcern_expectationtran.itemunkid as customitemunkid " & _
                          ",pipconcern_expectationtran.concern_value AS custom_value" & _
                          ",pipitem_master.itemtypeid " & _
                          ",pipitem_master.selectionmodeid " & _
                          ",pipconcern_expectationtran.itemgrpguid " & _
                          ",pipconcern_expectationtran.pipformunkid " & _
                          " FROM pipconcern_expectationtran " & _
                          " LEFT JOIN pipitem_master ON pipitem_master.itemunkid = pipconcern_expectationtran.itemunkid " & _
                          " LEFT JOIN pipworkprocess_master ON pipworkprocess_master.workprocessunkid = pipconcern_expectationtran.workprocessunkid " & _
                          " LEFT JOIN pipform_master ON pipform_master.pipformunkid = pipconcern_expectationtran.pipformunkid " & _
                          " WHERE pipconcern_expectationtran.isvoid = 0 AND pipitem_master.isimprovementgoal = 0 AND pipform_master.employeeunkid = '" & iEmployeeId & "' " & _
                          " AND pipitem_master.parameterunkid = '" & iParameterId & "'  AND pipform_master.pipformunkid = '" & iPIPFormId & "' "

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                If dsList.Tables(0).Rows.Count > 0 Then
                    Dim dFRow As DataRow = Nothing
                    Dim iCustomId, iNewCustomId As Integer : iCustomId = 0 : iNewCustomId = 0
                    Dim iGuid As String = ""
                    For Each dRow As DataRow In dsList.Tables(0).Rows
                        If iCustomId <= 0 Then iCustomId = dRow.Item("customitemunkid")
                        iNewCustomId = dRow.Item("customitemunkid")

                        If iCustomId = iNewCustomId Then
                            dFRow = mdtFinal.NewRow
                            mdtFinal.Rows.Add(dFRow)
                        End If

                        Select Case CInt(dRow.Item("itemtypeid"))
                            Case clspipitem_master.enPIPCustomType.FREE_TEXT
                                dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = dRow.Item("custom_value")
                            Case clspipitem_master.enPIPCustomType.SELECTION
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    Select Case CInt(dRow.Item("selectionmodeid"))

                                        Case clspipitem_master.enPIPSelectionMode.JOB_CAPABILITIES_COURSES, clspipitem_master.enPIPSelectionMode.CAREER_DEVELOPMENT_COURSES
                                            Dim objCMaster As New clsCommon_Master
                                            objCMaster._Masterunkid = CInt(dRow.Item("custom_value"))
                                            dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = objCMaster._Name
                                            objCMaster = Nothing
                                    End Select
                                End If
                            Case clspipitem_master.enPIPCustomType.DATE_SELECTION
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = eZeeDate.convertDate(dRow.Item("custom_value").ToString).ToShortDateString
                                End If
                            Case clspipitem_master.enPIPCustomType.NUMERIC_DATA
                                If dRow.Item("custom_value").ToString.Trim.Length > 0 Then
                                    If IsNumeric(dRow.Item("custom_value")) Then
                                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString) = CDbl(dRow.Item("custom_value")).ToString
                                    End If
                                End If
                        End Select

                        dFRow.Item("CItem_" & dRow.Item("customitemunkid").ToString & "Id") = dRow.Item("customitemunkid")
                        dFRow.Item("itemtypeid") = dRow.Item("itemtypeid")
                        dFRow.Item("selectionmodeid") = dRow.Item("selectionmodeid")
                        dFRow.Item("GUID") = dRow.Item("itemgrpguid")
                        dFRow.Item("pipformunkid") = dRow.Item("pipformunkid")
                    Next
                Else
                    mdtFinal.Rows.Add(mdtFinal.NewRow)
                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Custom_Items_Table; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return mdtFinal
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function getComboList(ByVal xEmployeeId As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False _
                                            , Optional ByVal mblnIncludeDate As Boolean = False, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try

            If mblFlag = True Then
                strQ = "SELECT 0 as pipformunkid, ' ' +  @name as formno, '' AS SDate, '' AS EDate UNION "
            End If

            strQ &= " SELECT pipformunkid,pipformno AS formno  , CONVERT(CHAR(8),startdate,112) AS SDate " & _
                        ", CONVERT(CHAR(8),enddate,112) AS EDate " & _
                        " FROM pipform_master "

            If xEmployeeId > 0 Then
                strQ &= " WHERE employeeunkid = @employeeunkid "
            End If

            strQ &= " ORDER BY  pipformunkid "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mblnIncludeDate = True Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If dRow.Item("pipformunkid") <= 0 Then Continue For
                    dRow.Item("formno") = dRow.Item("formno").ToString.Trim & " ( " & eZeeDate.convertDate(dRow.Item("SDate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dRow.Item("EDate").ToString).ToShortDateString & " ) "
                Next
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function getLastEmployeePIPForm(ByVal xEmployeeId As Integer, ByVal xFormId As Integer, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try

            strQ &= " SELECT TOP 1 pipformunkid,pipformno,pipformdate, startdate " & _
                        ", enddate " & _
                        " FROM pipform_master " & _
                        " WHERE isvoid = 0 AND employeeunkid = @employeeunkid "

            If xFormId > 0 Then
                strQ &= " AND pipformunkid <> @pipformunkid "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " ORDER BY pipformdate DESC "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId.ToString)
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFormId.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getLastEmployeePIPForm; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Custom Header")
            Language.setMessage(mstrModuleName, 2, "Pending")
            Language.setMessage(mstrModuleName, 3, "This Form No is already defined. Please define new Form No.")
            Language.setMessage(mstrModuleName, 4, "Select")
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
