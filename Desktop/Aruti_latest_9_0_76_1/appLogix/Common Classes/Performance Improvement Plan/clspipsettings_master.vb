﻿'************************************************************************************************************************************
'Class Name : clspipsettings_master
'Purpose    :
'Date       : 30-Nov-2023
'Written By : Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clspipsettings_master
    Private Shared ReadOnly mstrModuleName As String = "clspipsettings_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSettingunkid As Integer
    Private mintSettingkeyid As Integer
    Private mstrSetting_Value As String = String.Empty
    Private mstrroleunkids As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

    Private mdicPDPSetting As Dictionary(Of enPIPConfiguration, String)

#End Region

#Region "Enum"

    Public Enum enPIPConfiguration
        NONE = 0
        INSTRUCTION = 1
        PROGRESS_MONITORING = 2
        ASSESSOR_MAPPING = 3
    End Enum

    Public Enum enPIPProgressMappingId
        LINE_MANAGER = 1
        NOMINATED_COACH = 2
        ROLE_BASED = 3
    End Enum


#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set settingunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Settingunkid() As Integer
        Get
            Return mintSettingunkid
        End Get
        Set(ByVal value As Integer)
            mintSettingunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set settingkeyid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Settingkeyid() As Integer
        Get
            Return mintSettingkeyid
        End Get
        Set(ByVal value As Integer)
            mintSettingkeyid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set setting_value
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Setting_Value() As String
        Get
            Return mstrSetting_Value
        End Get
        Set(ByVal value As String)
            mstrSetting_Value = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Roleunkids
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Roleunkids() As String
        Get
            Return mstrroleunkids
        End Get
        Set(ByVal value As String)
            mstrroleunkids = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FromWeb
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set CompanyUnkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DatabaseName
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set PIPSetting
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _PIPSetting() As Dictionary(Of enPIPConfiguration, String)
        Set(ByVal value As Dictionary(Of enPIPConfiguration, String))
            mdicPDPSetting = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  settingunkid " & _
                      ", settingkeyid " & _
                      ", setting_value " & _
                      ", roleunkids " & _
                      " FROM " & mstrDatabaseName & "..pipsettings_master " & _
                      " WHERE settingunkid = @settingunkid "

            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintSettingunkid = CInt(dtRow.Item("settingunkid"))
                mintSettingkeyid = CInt(dtRow.Item("settingkeyid"))
                mstrSetting_Value = dtRow.Item("setting_value").ToString
                mstrroleunkids = dtRow.Item("roleunkids").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetSetting(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Dictionary(Of enPIPConfiguration, String)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  settingunkid " & _
              ", settingkeyid " & _
              ", setting_value " & _
             "FROM " & mstrDatabaseName & "..pipsettings_master "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            mdicPDPSetting = New Dictionary(Of enPIPConfiguration, String)
            If IsNothing(dsList) = False AndAlso dsList.Tables(0).Rows.Count > 0 Then

                Dim drInstruction As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enPIPConfiguration.INSTRUCTION)).FirstOrDefault()
                If IsNothing(drInstruction) = False Then
                    mdicPDPSetting.Add(enPIPConfiguration.INSTRUCTION, drInstruction.Field(Of String)("setting_value").ToString())
                End If

                Dim drProgress_Monitoring As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enPIPConfiguration.PROGRESS_MONITORING)).FirstOrDefault()
                If IsNothing(drProgress_Monitoring) = False Then
                    mdicPDPSetting.Add(enPIPConfiguration.PROGRESS_MONITORING, drProgress_Monitoring.Field(Of String)("setting_value").ToString())
                End If

                Dim drAssessor_Mapping As DataRow = dsList.Tables(0).AsEnumerable.Where(Function(x) x.Field(Of Integer)("settingkeyid") = CInt(enPIPConfiguration.ASSESSOR_MAPPING)).FirstOrDefault()
                If IsNothing(drAssessor_Mapping) = False Then
                    mdicPDPSetting.Add(enPIPConfiguration.ASSESSOR_MAPPING, drAssessor_Mapping.Field(Of String)("setting_value").ToString())
                End If

                Return mdicPDPSetting

            End If

            Return Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSetting; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetSettingValueFromKey(ByVal settingkeyid As enPIPConfiguration, Optional ByVal xDataOpr As clsDataOperation = Nothing) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              " setting_value " & _
             "FROM " & mstrDatabaseName & "..pipsettings_master " & _
             "WHERE settingkeyid = @settingkeyid "

            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(settingkeyid))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)("setting_value").ToString()
            End If

            Return String.Empty


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetSettingValueFromKey; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xSettingKeyId As enPIPConfiguration) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
                      "  settingunkid " & _
                      ", settingkeyid " & _
                      ", setting_value " & _
                      ", roleunkids " & _
                      " FROM " & mstrDatabaseName & "..pipsettings_master " & _
                      " WHERE 1 = 1 "

            If xSettingKeyId > 0 Then
                strQ &= " AND settingkeyid = @settingkeyid "
                objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, xSettingKeyId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function SavePIPSetting(ByVal xmdicSetting As Dictionary(Of enPIPConfiguration, String), Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        objDataOperation.ClearParameters()
        Try

            If xmdicSetting.Count > 0 Then

                Dim xRoldunkids As String = mstrroleunkids

                For Each kvp As KeyValuePair(Of enPIPConfiguration, String) In xmdicSetting

                    mintSettingkeyid = kvp.Key
                    mstrSetting_Value = kvp.Value

                    If kvp.Key <> enPIPConfiguration.ASSESSOR_MAPPING Then
                        mstrroleunkids = ""
                    Else
                        mstrroleunkids = xRoldunkids
                    End If

                    If isExist(kvp.Key, objDataOperation) Then
                        If Update(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                            Return False
                        End If
                    Else
                        If Insert(objDataOperation) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                            Return False
                        End If
                    End If

                Next
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: SavePIPSetting; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intSettingTypeId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT settingunkid,setting_value,roleunkids " & _
                     " FROM " & mstrDatabaseName & "..pipsettings_master " & _
                     " WHERE  settingkeyid= @settingkeyid  "

            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSettingTypeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mintSettingunkid = CInt(dsList.Tables(0).Rows(0)("settingunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingkeyid.ToString)
            objDataOperation.AddParameter("@setting_value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSetting_Value.ToString)
            objDataOperation.AddParameter("@roleunkids", SqlDbType.NVarChar, mstrroleunkids.Trim.Length, mstrroleunkids.ToString)

            strQ = "INSERT INTO " & mstrDatabaseName & "..pipsettings_master ( " & _
                      " settingkeyid " & _
                      ", setting_value" & _
                      ", roleunkids " & _
                      " ) VALUES (" & _
                      " @settingkeyid " & _
                      ", @setting_value" & _
                      ", @roleunkids " & _
                      " ); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSettingunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingunkid.ToString)
            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingkeyid.ToString)
            objDataOperation.AddParameter("@setting_value", SqlDbType.NVarChar, mstrSetting_Value.Length, mstrSetting_Value.ToString)
            objDataOperation.AddParameter("@roleunkids", SqlDbType.NVarChar, mstrroleunkids.Trim.Length, mstrroleunkids.ToString)

            strQ = "UPDATE " & mstrDatabaseName & "..pipsettings_master SET " & _
                      " settingkeyid = @settingkeyid" & _
                      ", setting_value = @setting_value " & _
                      ", roleunkids = @roleunkids " & _
                      " WHERE settingunkid = @settingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        _Settingunkid = intUnkid

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()
        Try

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "DELETE FROM " & mstrDatabaseName & "..pipsettings_master WHERE settingunkid = @settingunkid "

            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpipsettings_master ( " & _
                    "  settingunkid " & _
                    ", settingkeyid " & _
                    ", setting_value " & _
                    ", roleunkids " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", form_name " & _
                    ", ip " & _
                    ", machine_name " & _
                    ", isweb" & _
                  ") VALUES (" & _
                    "  @settingunkid " & _
                    ", @settingkeyid " & _
                    ", @setting_value " & _
                    ", @roleunkids " & _
                    ", @audittype " & _
                    ", @audituserunkid " & _
                    ", GETDATE() " & _
                    ", @form_name " & _
                    ", @ip " & _
                    ", @machine_name " & _
                    ", @isweb" & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@settingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingunkid.ToString)
            objDataOperation.AddParameter("@settingkeyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSettingkeyid.ToString)
            objDataOperation.AddParameter("@setting_value", SqlDbType.NVarChar, mstrSetting_Value.Length, mstrSetting_Value.ToString)
            objDataOperation.AddParameter("@roleunkids", SqlDbType.NVarChar, mstrroleunkids.Length, mstrroleunkids.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

End Class
