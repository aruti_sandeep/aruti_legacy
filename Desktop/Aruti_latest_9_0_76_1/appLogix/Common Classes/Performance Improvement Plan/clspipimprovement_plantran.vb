﻿'************************************************************************************************************************************
'Class Name : clspipimprovement_plantran.vb
'Purpose    :
'Date       :07-Dec-2023
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clspipimprovement_plantran
    Private Shared ReadOnly mstrModuleName As String = "clspipimprovement_plantran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintImprovementplanunkid As Integer
    Private mintPipformunkid As Integer
    Private mintParameterunkid As Integer
    Private mintItemunkid As Integer
    Private mstrImprovement_Goal As String = String.Empty
    Private mstrImprovement_Value As String = String.Empty
    Private mdtDuedate As Date = Nothing
    Private mblnIsSubmit As Boolean = False
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mintAuditUserId As Integer = 0
    Private mstrFormName As String = ""
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mblnIsWeb As Boolean = False

    Private mintCompanyUnkid As Integer = 0
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set improvementplanunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Improvementplanunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintImprovementplanunkid
        End Get
        Set(ByVal value As Integer)
            mintImprovementplanunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pipformunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Pipformunkid() As Integer
        Get
            Return mintPipformunkid
        End Get
        Set(ByVal value As Integer)
            mintPipformunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set parameterunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Parameterunkid() As Integer
        Get
            Return mintParameterunkid
        End Get
        Set(ByVal value As Integer)
            mintParameterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Itemunkid() As Integer
        Get
            Return mintItemunkid
        End Get
        Set(ByVal value As Integer)
            mintItemunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set improvement_goal
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Improvement_Goal() As String
        Get
            Return mstrImprovement_Goal
        End Get
        Set(ByVal value As String)
            mstrImprovement_Goal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set improvement_value
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Improvement_Value() As String
        Get
            Return mstrImprovement_Value
        End Get
        Set(ByVal value As String)
            mstrImprovement_Value = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set duedate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Duedate() As Date
        Get
            Return mdtDuedate
        End Get
        Set(ByVal value As Date)
            mdtDuedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issubmit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsSubmit() As Boolean
        Get
            Return mblnIsSubmit
        End Get
        Set(ByVal value As Boolean)
            mblnIsSubmit = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set clienip
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If


        Try
            strQ = "SELECT " & _
                      "  improvementplanunkid " & _
                      ", pipformunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", improvement_goal " & _
                      ", improvement_value " & _
                      ", duedate " & _
                      ", issubmit " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM pipimprovement_plantran " & _
                      " WHERE improvementplanunkid = @improvementplanunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@improvementplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImprovementplanunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintImprovementplanunkid = CInt(dtRow.Item("improvementplanunkid"))
                mintPipformunkid = CInt(dtRow.Item("pipformunkid"))
                mintParameterunkid = CInt(dtRow.Item("parameterunkid"))
                mintItemunkid = CInt(dtRow.Item("itemunkid"))
                mstrImprovement_Goal = dtRow.Item("improvement_goal").ToString
                mstrImprovement_Value = dtRow.Item("improvement_value").ToString

                If IsDBNull(dtRow.Item("duedate")) = False Then
                    mdtDuedate = CDate(dtRow.Item("duedate"))
                End If

                mblnIsSubmit = CBool(dtRow.Item("issubmit"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintVoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xPIPFormunkid As Integer, Optional ByVal blnOnlyActive As Boolean = True _
                                    , Optional ByVal xParameterunkid As Integer = 0, Optional ByVal xItemunkid As Integer = 0, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  pipimprovement_plantran.improvementplanunkid " & _
                      ", pipimprovement_plantran.pipformunkid " & _
                      ", pipimprovement_plantran.parameterunkid " & _
                      ", pipimprovement_plantran.itemunkid " & _
                      ", pipitem_master.item " & _
                      ", pipimprovement_plantran.improvement_goal " & _
                      ", pipimprovement_plantran.improvement_value " & _
                      ", pipimprovement_plantran.duedate " & _
                      ", pipimprovement_plantran.issubmit " & _
                      ", pipimprovement_plantran.userunkid " & _
                      ", pipimprovement_plantran.loginemployeeunkid " & _
                      ", pipimprovement_plantran.isvoid " & _
                      ", pipimprovement_plantran.voiduserunkid " & _
                      ", pipimprovement_plantran.voidloginemployeeunkid " & _
                      ", pipimprovement_plantran.voiddatetime " & _
                      ", pipimprovement_plantran.voidreason " & _
                      " FROM pipimprovement_plantran " & _
                      " LEFT JOIN pipitem_master ON pipitem_master.itemunkid = pipimprovement_plantran.itemunkid AND pipitem_master.isimprovementgoal = 1 " & _
                      " WHERE pipimprovement_plantran.pipformunkid = @pipformunkid "


            If blnOnlyActive Then
                strQ &= " AND pipimprovement_plantran.isvoid  = 0 "
            End If

            If xParameterunkid > 0 Then
                strQ &= " AND pipimprovement_plantran.parameterunkid = @parameterunkid "
            End If

            If xItemunkid > 0 Then
                strQ &= " AND pipimprovement_plantran.itemunkid = @itemunkid "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPIPFormunkid)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xParameterunkid)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xItemunkid)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pipimprovement_plantran) </purpose>
    Public Function Insert(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing


        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPipformunkid.ToString)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@improvement_goal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImprovement_Goal.ToString)
            objDataOperation.AddParameter("@improvement_value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImprovement_Value.ToString)

            If IsDBNull(mdtDuedate) = False AndAlso mdtDuedate <> Nothing Then
                objDataOperation.AddParameter("@duedate", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, mdtDuedate.ToString)
            Else
                objDataOperation.AddParameter("@duedate", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@issubmit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmit.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO pipimprovement_plantran ( " & _
                      "  pipformunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", improvement_goal " & _
                      ", improvement_value " & _
                      ", duedate " & _
                      ", issubmit " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @pipformunkid " & _
                      ", @parameterunkid " & _
                      ", @itemunkid " & _
                      ", @improvement_goal " & _
                      ", @improvement_value " & _
                      ", @duedate " & _
                      ", @issubmit " & _
                      ", @userunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voidloginemployeeunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintImprovementplanunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAudiTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pipimprovement_plantran) </purpose>
    Public Function Update(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@improvementplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImprovementplanunkid.ToString)
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPipformunkid.ToString)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@improvement_goal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImprovement_Goal.ToString)
            objDataOperation.AddParameter("@improvement_value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImprovement_Value.ToString)

            If IsDBNull(mdtDuedate) = False AndAlso mdtDuedate <> Nothing Then
                objDataOperation.AddParameter("@duedate", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, mdtDuedate.ToString)
            Else
                objDataOperation.AddParameter("@duedate", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@issubmit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmit.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = " UPDATE pipimprovement_plantran SET " & _
                      "  pipformunkid = @pipformunkid" & _
                      ", parameterunkid = @parameterunkid" & _
                      ", itemunkid = @itemunkid" & _
                      ", improvement_goal = @improvement_goal" & _
                      ", improvement_value = @improvement_value" & _
                      ", duedate = @duedate" & _
                      ", issubmit = @issubmit " & _
                      ", userunkid = @userunkid" & _
                      ", loginemployeeunkid = @loginemployeeunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      " WHERE isvoid = 0 AND itemunkid = @itemunkid and pipformunkid = @pipformunkid  "


            If mintImprovementplanunkid > 0 Then
                strQ &= " AND improvementplanunkid = @improvementplanunkid "
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAudiTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pipimprovement_plantran) </purpose>
    Public Function Delete(ByVal xImprovementPlanId As Integer, ByVal xImprovementItemId As Integer, ByVal xFormId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            strQ = " UPDATE pipimprovement_plantran SET " & _
                      "  isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
                      ", voiddatetime = GETDATE()" & _
                      ", voidreason = @voidreason " & _
                      " WHERE isvoid = 0 AND itemunkid = @itemunkid AND pipformunkid = @pipformunkid AND improvementplanunkid = @improvementplanunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@improvementplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xImprovementPlanId)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xImprovementItemId)
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFormId)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Improvementplanunkid(objDataOperation) = xImprovementPlanId

            If InsertAudiTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, Optional ByVal isFromTable As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        objDataOperation = New clsDataOperation
        Try

            Dim Tables() As String = {"pipmonitoring_tran"}
            For Each value As String In Tables
                Select Case value
                    Case "pipmonitoring_tran"
                        If isFromTable Then
                            strQ = " SELECT pipimprovement_plantran.improvementplanunkid FROM " & value & " " & _
                                      " LEFT JOIN pipimprovement_plantran ON " & value & ".improvementplanunkid = pipimprovement_plantran.improvementplanunkid " & _
                                      " WHERE " & value & ".improvementplanunkid = @improvementplanunkid "
                        Else
                            strQ = "SELECT improvementplanunkid FROM " & value & " WHERE improvementplanunkid = @improvementplanunkid "
                        End If
                End Select
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@improvementplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    Return True
                    Exit For
                End If

            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intItemunkid As Integer, ByVal intpipformunkid As Integer, ByVal intImprovementPlanId As Integer, ByVal xImprovementGoal As String, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If

        Try
            strQ = "SELECT " & _
                      "  improvementplanunkid " & _
                      ", pipformunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", improvement_goal " & _
                      ", improvement_value " & _
                      ", duedate " & _
                      ", issubmit " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM pipimprovement_plantran " & _
                      " WHERE isvoid = 0 AND  pipformunkid = @pipformunkid " & _
                      " AND itemunkid = @itemunkid "

            If xImprovementGoal.Trim.Length > 0 Then
                strQ &= " AND improvement_goal = @improvement_goal "
            End If

            If intImprovementPlanId > 0 Then
                strQ &= " AND improvementplanunkid = @improvementplanunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intpipformunkid)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intItemunkid)
            objDataOperation.AddParameter("@improvement_goal", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, xImprovementGoal)
            objDataOperation.AddParameter("@improvementplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intImprovementPlanId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pipconcern_expectationtran) </purpose>
    Public Function InsertAudiTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO atpipimprovement_plantran ( " & _
                      " improvementplanunkid " & _
                      ", pipformunkid " & _
                      ", parameterunkid " & _
                      ", itemunkid " & _
                      ", improvement_goal " & _
                      ", improvement_value " & _
                      ", duedate " & _
                      ", issubmit " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", loginemployeeunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb" & _
                    ") VALUES (" & _
                      "  @improvementplanunkid " & _
                      ", @pipformunkid " & _
                      ", @parameterunkid " & _
                      ", @itemunkid " & _
                      ", @improvement_goal " & _
                      ", @improvement_value " & _
                      ", @duedate " & _
                      ", @issubmit " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @loginemployeeunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @isweb" & _
                    "); SELECT @@identity"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@improvementplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintImprovementplanunkid.ToString)
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPipformunkid.ToString)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@improvement_goal", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImprovement_Goal.ToString)
            objDataOperation.AddParameter("@improvement_value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImprovement_Value.ToString)

            If IsDBNull(mdtDuedate) = False AndAlso mdtDuedate <> Nothing Then
                objDataOperation.AddParameter("@duedate", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, mdtDuedate.ToString)
            Else
                objDataOperation.AddParameter("@duedate", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@issubmit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmit.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAudiTrails; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pipconcern_expectationtran) </purpose>
    Public Function GetImprovementItemList(ByVal strTableName As String, ByVal xPIPFormunkid As Integer, ByVal xEmployeeunkid As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  pipimprovement_plantran.improvementplanunkid " & _
                      ", pipimprovement_plantran.pipformunkid " & _
                      ", pipimprovement_plantran.parameterunkid " & _
                      ", pipimprovement_plantran.itemunkid " & _
                      ", pipitem_master.item " & _
                      ", pipimprovement_plantran.improvement_goal " & _
                      ", pipimprovement_plantran.improvement_value " & _
                      ", pipimprovement_plantran.duedate " & _
                      ", pipimprovement_plantran.issubmit " & _
                      ", pipimprovement_plantran.userunkid " & _
                      ", pipimprovement_plantran.loginemployeeunkid " & _
                      ", pipimprovement_plantran.isvoid " & _
                      ", pipimprovement_plantran.voiduserunkid " & _
                      ", pipimprovement_plantran.voidloginemployeeunkid " & _
                      ", pipimprovement_plantran.voiddatetime " & _
                      ", pipimprovement_plantran.voidreason " & _
                      " FROM pipimprovement_plantran " & _
                      " LEFT JOIN pipform_master ON pipform_master.pipformunkid = pipimprovement_plantran.pipformunkid AND pipform_master.isvoid = 0" & _
                      " LEFT JOIN pipitem_master ON pipitem_master.itemunkid = pipimprovement_plantran.itemunkid AND pipitem_master.isimprovementgoal = 1 " & _
                      " WHERE pipimprovement_plantran.isvoid = 0 AND  pipimprovement_plantran.pipformunkid = @pipformunkid "

            If xEmployeeunkid > 0 Then
                strQ &= " AND pipform_master.employeeunkid = @employeeunkid "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipformunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPIPFormunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pipconcern_expectationtran) </purpose>
    '''  Public Function GetImprovementItemList(ByVal strTableName As String, ByVal xPIPFormunkid As Integer, ByVal xEmployeeunkid As Integer) As DataSet
    Public Sub SendNotificationForProgressMonitoring(ByVal mintPIPFormId As Integer, ByVal mstrPIPFormNo As String, ByVal mintEmployeeId As Integer, ByVal mstrEmployeeCode As String, ByVal mstrEmployee As String _
                                                                            , ByVal mdtPIPFormStartDate As Date, ByVal mdtPIPFormEndDate As Date, ByVal mintApproverUserId As Integer, ByVal mstrApproverName As String, ByVal mstrApproverEmail As String _
                                                                            , Optional ByVal intCompanyUnkId As Integer = 0, Optional ByVal strArutiSelfServiceURL As String = "", Optional ByVal iLoginTypeId As Integer = 0)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objMail As New clsSendMail
        Dim strLink As String = ""
        Try

            If mstrPIPFormNo.Trim.Length <= 0 OrElse mstrEmployeeCode.Trim.Length <= 0 OrElse mstrEmployee.Trim.Length <= 0 OrElse mintApproverUserId <= 0 OrElse mstrApproverEmail.Trim.Length <= 0 Then Exit Sub

            If strArutiSelfServiceURL.Trim.Length > 0 Then
                strLink = strArutiSelfServiceURL & "/PIP/wPgEmployeePIPForm.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(CInt(clspipsettings_master.enPIPConfiguration.PROGRESS_MONITORING) & "|" & intCompanyUnkId.ToString & "|" & mintEmployeeId.ToString & "|" & mintApproverUserId.ToString() & "|" & mintPIPFormId.ToString))
            End If


            Dim mstrSubject As String = Language.getMessage(mstrModuleName, 1, "Notification for PIP Review")

            Dim strMessage As String = ""

            strMessage = "<HTML> <BODY>"

            strMessage &= Language.getMessage(mstrModuleName, 2, "Dear") & " " & getTitleCase(mstrApproverName) & ", <BR><BR>"

            strMessage &= Language.getMessage(mstrModuleName, 3, "This is to notify you that employee") & " <B>" & mstrEmployeeCode.Trim() & " - " & getTitleCase(mstrEmployee) & "</B> " & Language.getMessage(mstrModuleName, 4, "has submitted PIP form no") & " <B>(" & mstrPIPFormNo.Trim() & ")" & "</B> "
            strMessage &= Language.getMessage(mstrModuleName, 5, "having start date :") & " <B> " & mdtPIPFormStartDate.ToShortDateString() & "</B> " & Language.getMessage(mstrModuleName, 6, "and End Date :") & " <B> " & mdtPIPFormEndDate.ToShortDateString() & "</B> " & Language.getMessage(mstrModuleName, 7, "for progress review.")
            strMessage &= "<BR>"
            strMessage &= Language.getMessage(mstrModuleName, 8, "It is highly recommended that you review the PIP through a one on one discussion with the employee.")
            strMessage &= "<BR><BR>"

            strMessage &= Language.getMessage(mstrModuleName, 9, "Click on the link below to review progress.")
            strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"


            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"

            objMail._Subject = mstrSubject
            objMail._Message = strMessage
            objMail._ToEmail = mstrApproverEmail.Trim

            If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
            If mstrFormName.Trim.Length > 0 Then
                objMail._Form_Name = mstrFormName
                objMail._WebClientIP = mstrClientIP
                objMail._WebHostName = mstrHostName
            End If
            objMail._LogEmployeeUnkid = mintLoginemployeeunkid
            objMail._OperationModeId = iLoginTypeId
            objMail._UserUnkid = mintUserunkid
            objMail._SenderAddress = IIf(mstrApproverEmail.Trim.Length < 0, mstrApproverName.Trim, mstrApproverEmail.Trim)
            objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PIP_MGT
            objMail.SendMail(intCompanyUnkId)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendNotificationForProgressMonitoring; Module Name: " & mstrModuleName)
        Finally
            objMail = Nothing
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Notification for PIP Review")
            Language.setMessage(mstrModuleName, 2, "Dear")
            Language.setMessage(mstrModuleName, 3, "This is to notify you that employee")
            Language.setMessage(mstrModuleName, 4, "has submitted PIP form no")
            Language.setMessage(mstrModuleName, 5, "having start date :")
            Language.setMessage(mstrModuleName, 6, "and End Date :")
            Language.setMessage(mstrModuleName, 7, "for progress review.")
            Language.setMessage(mstrModuleName, 8, "It is highly recommended that you review the PIP through a one on one discussion with the employee.")
            Language.setMessage(mstrModuleName, 9, "Click on the link below to review progress.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class