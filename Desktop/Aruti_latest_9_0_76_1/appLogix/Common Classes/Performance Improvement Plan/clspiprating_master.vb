﻿'************************************************************************************************************************************
'Class Name : clspiprating_master.vb
'Purpose    :
'Date       :04-Dec-2023
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clspiprating_master
    Private Shared ReadOnly mstrModuleName As String = "clspiprating_master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintPIPRatingunkid As Integer = 0
    Private mintRatingTypeunkid As Integer = 0
    Private mstrOptionValue As String = String.Empty
    Private mstrColor As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date

    Private mintAuditUserId As Integer = 0
    Private mstrFormName As String = ""
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mblnIsWeb As Boolean = False

    Private mintCompanyUnkid As Integer = 0
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pipratingunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _PIPRatingunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintPIPRatingunkid
        End Get
        Set(ByVal value As Integer)
            mintPIPRatingunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ratingtypeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _RatingTypeunkid() As Integer
        Get
            Return mintRatingTypeunkid
        End Get
        Set(ByVal value As Integer)
            mintRatingTypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set optionvalue
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _OptionValue() As String
        Get
            Return mstrOptionValue
        End Get
        Set(ByVal value As String)
            mstrOptionValue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set color
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Color() As String
        Get
            Return mstrColor
        End Get
        Set(ByVal value As String)
            mstrColor = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set clienip
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

#Region " ENUMS "

    Public Enum enPIPRatingType
        Final_Assessment = 1
        Progress_Monitoring = 2
    End Enum

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  pipratingunkid " & _
                      ", ratingtypeunkid " & _
                      ", optionvalue " & _
                      ", color " & _
                      ", description " & _
                      ", color " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      " FROM " & mstrDatabaseName & "..piprating_master " & _
                      " WHERE pipratingunkid = @pipratingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipratingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPIPRatingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPIPRatingunkid = CInt(dtRow.Item("pipratingunkid"))
                mintRatingTypeunkid = CInt(dtRow.Item("ratingtypeunkid"))
                mstrOptionValue = dtRow.Item("optionvalue").ToString()
                mstrColor = dtRow.Item("color").ToString
                mstrDescription = dtRow.Item("description").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList_RatingTypes(Optional ByVal iList As String = "List", Optional ByVal iAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOpr As New clsDataOperation
        Try
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            StrQ &= "SELECT '" & enPIPRatingType.Final_Assessment & "' AS Id, @FA AS Name UNION " & _
                        "SELECT '" & enPIPRatingType.Progress_Monitoring & "' AS Id, @PM AS Name "


            objDataOpr.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOpr.AddParameter("@FA", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Final Assessment"))
            objDataOpr.AddParameter("@PM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Progress Monitoring"))


            If iList.Trim.Length <= 0 Then iList = "List"

            dsList = objDataOpr.ExecQuery(StrQ, iList)

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetList_RatingTypes", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal mblnIncludeGrp As Boolean, Optional ByVal intRatingId As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            If mblnIncludeGrp Then

            strQ = "SELECT " & _
                         " CAST(1 AS BIT) AS IsGrp " & _
                         ",  0 AS pipratingunkid " & _
                         ", 0 AS ratingtypeunkid " & _
                         ", CASE WHEN piprating_master.ratingtypeunkid = 1 THEN @FinalAssessment  " & _
                         "           WHEN piprating_master.ratingtypeunkid = 2 THEN @ProgressMonitoring END RatingType" & _
                         ", '' AS optionvalue " & _
                         ", '' AS description " & _
                         ", '' AS color " & _
                         ", CAST(0 AS BIT) AS isvoid " & _
                         ", 0 voiduserunkid " & _
                         ", NULL AS voiddatetime " & _
                         " FROM " & mstrDatabaseName & "..piprating_master " & _
                         " WHERE piprating_master.isvoid = 0 "

                If intRatingId > 0 Then
                    strQ &= " AND piprating_master.pipratingunkid = @pipratingunkid"
                End If

                strQ &= " UNION "

            End If

            strQ &= "SELECT " & _
                          " CAST(0 AS BIT) AS IsGrp " & _
                          ", piprating_master.pipratingunkid " & _
                      ", piprating_master.ratingtypeunkid " & _
                      ", CASE WHEN piprating_master.ratingtypeunkid = 1 THEN @FinalAssessment  " & _
                      "           WHEN piprating_master.ratingtypeunkid = 2 THEN @ProgressMonitoring END RatingType" & _
                      ", piprating_master.optionvalue " & _
                      ", piprating_master.description " & _
                      ", piprating_master.color " & _
                      ", piprating_master.isvoid " & _
                      ", piprating_master.voiduserunkid " & _
                      ", piprating_master.voiddatetime " & _
                      " FROM " & mstrDatabaseName & "..piprating_master " & _
                      " WHERE piprating_master.isvoid = 0 "

            If intRatingId > 0 Then
                strQ &= " AND piprating_master.pipratingunkid = @pipratingunkid"
            End If

            strQ &= " ORDER BY RatingType,optionvalue,IsGrp DESC"

            objDataOperation.AddParameter("@pipratingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intRatingId)
            objDataOperation.AddParameter("@FinalAssessment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Final Assessment"))
            objDataOperation.AddParameter("@ProgressMonitoring", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Progress Monitoring"))
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tlratings_master) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@ratingtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRatingTypeunkid.ToString)
            objDataOperation.AddParameter("@optionvalue", SqlDbType.NVarChar, mstrOptionValue.Trim.Length, mstrOptionValue.ToString)
            objDataOperation.AddParameter("@color", SqlDbType.NVarChar, mstrColor.Length, mstrColor.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            strQ = "INSERT INTO " & mstrDatabaseName & "..piprating_master ( " & _
                      "  ratingtypeunkid " & _
                      ", optionvalue " & _
                      ", color " & _
                      ", description " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                    ") VALUES (" & _
                      "  @ratingtypeunkid " & _
                      ", @optionvalue " & _
                      ", @color " & _
                      ", @description " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPIPRatingunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tlratings_master) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@pipratingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPIPRatingunkid.ToString)
            objDataOperation.AddParameter("@ratingtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRatingTypeunkid.ToString)
            objDataOperation.AddParameter("@optionvalue", SqlDbType.NVarChar, mstrOptionValue.Trim.Length, mstrOptionValue.ToString)
            objDataOperation.AddParameter("@color", SqlDbType.NVarChar, mstrColor.Length, mstrColor.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            strQ = "UPDATE " & mstrDatabaseName & "..piprating_master SET " & _
                      "  ratingtypeunkid = @ratingtypeunkid" & _
                      ", optionvalue = @optionvalue" & _
                      ", color = @color" & _
                      ", description = @description" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      " WHERE pipratingunkid = @pipratingunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tlratings_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "UPDATE " & mstrDatabaseName & "..piprating_master SET " & _
                       "  isvoid = @isvoid " & _
                       ", voiduserunkid = @voiduserunkid " & _
                       ", voiddatetime = @voiddatetime " & _
                       " WHERE pipratingunkid = @pipratingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipratingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _PIPRatingunkid(objDataOperation) = intUnkid

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            Dim Tables() As String = {"pipform_master", "pipmonitoring_tran"}
            For Each value As String In Tables
                Select Case value
                    Case "pipform_master"
                        strQ = "SELECT statusunkid FROM " & value & " WHERE statusunkid = @statusunkid "

                    Case "pipmonitoring_tran"
                        strQ = "SELECT statusunkid FROM " & value & " WHERE statusunkid = @statusunkid "
                    Case Else
                End Select
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    Return True
                    Exit For
                End If
            Next
            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpiprating_master ( " & _
                    "  pipratingunkid " & _
                    ", ratingtypeunkid " & _
                    ", optionvalue " & _
                    ", color " & _
                    ", description " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", form_name " & _
                    ", ip " & _
                    ", machine_name " & _
                    ", isweb" & _
                  ") VALUES (" & _
                    "  @pipratingunkid " & _
                    ", @ratingtypeunkid " & _
                    ", @optionvalue " & _
                    ", @color " & _
                    ", @description " & _
                    ", @audittype " & _
                    ", @audituserunkid " & _
                    ", GETDATE() " & _
                    ", @form_name " & _
                    ", @ip " & _
                    ", @machine_name " & _
                    ", @isweb" & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@pipratingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPIPRatingunkid.ToString)
            objDataOperation.AddParameter("@ratingtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRatingTypeunkid.ToString)
            objDataOperation.AddParameter("@optionvalue", SqlDbType.NVarChar, mstrOptionValue.Trim.Length, mstrOptionValue.ToString)
            objDataOperation.AddParameter("@color", SqlDbType.NVarChar, mstrColor.Length, mstrColor.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(ByVal strList As String, Optional ByVal blnSelect As Boolean = False, Optional ByVal xRatingType As Integer = 0) As DataSet
        Dim StrQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            If blnSelect Then
                StrQ = "SELECT 0 AS Id, @SE AS Name UNION ALL "
            End If
            StrQ &= "SELECT " & _
                        "  pipratingunkid AS Id " & _
                        ", optionvalue AS Name " & _
                        " FROM " & mstrDatabaseName & "..piprating_master " & _
                        " WHERE isvoid = 0 "

            If xRatingType > 0 Then
                StrQ &= " AND ratingtypeunkid = @ratingtypeunkid"
                objDataOperation.AddParameter("@ratingtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xRatingType)
            End If

            StrQ &= " ORDER BY Id "

            objDataOperation.AddParameter("@SE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "Final Assessment")
            Language.setMessage(mstrModuleName, 3, "Progress Monitoring")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class