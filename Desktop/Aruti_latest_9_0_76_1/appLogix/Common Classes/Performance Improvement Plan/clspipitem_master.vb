﻿'************************************************************************************************************************************
'Class Name : clspipitem_master
'Purpose    :
'Date       : 01-Dec-2023
'Written By : Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clspipitem_master
    Private Shared ReadOnly mstrModuleName As String = "clspipitem_master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "

    Private mintItemunkid As Integer
    Private mstrItem As String = String.Empty
    Private mintParameterunkid As Integer
    Private mintItemTypeid As Integer
    Private mintSelectionModeid As Integer
    Private mintSortOrder As Integer
    Private mblnIsApplyWorkProcess As Boolean = False
    Private mblnIsImprovementGoal As Boolean = False
    Private mblnIsactive As Boolean = True
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Itemunkid(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer
        Get
            Return mintItemunkid
        End Get
        Set(ByVal value As Integer)
            mintItemunkid = value
            Call GetData(xDataOpr)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set item
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Item() As String
        Get
            Return mstrItem
        End Get
        Set(ByVal value As String)
            mstrItem = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set parameterunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Parameterunkid() As Integer
        Get
            Return mintParameterunkid
        End Get
        Set(ByVal value As Integer)
            mintParameterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set itemtype
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ItemTypeId() As Integer
        Get
            Return mintItemTypeid
        End Get
        Set(ByVal value As Integer)
            mintItemTypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set selectionmodeid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _SelectionModeId() As Integer
        Get
            Return mintSelectionModeid
        End Get
        Set(ByVal value As Integer)
            mintSelectionModeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isApplyWorkProcess
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsApplyWorkProcess() As Boolean
        Get
            Return mblnIsApplyWorkProcess
        End Get
        Set(ByVal value As Boolean)
            mblnIsApplyWorkProcess = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isimprovementgoal
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _IsImprovementGoal() As Boolean
        Get
            Return mblnIsImprovementGoal
        End Get
        Set(ByVal value As Boolean)
            mblnIsImprovementGoal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set sortorder
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _SortOrder() As Integer
        Get
            Return mintSortOrder
        End Get
        Set(ByVal value As Integer)
            mintSortOrder = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set clientip
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fromweb
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set companyunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set databasename
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

#Region " ENUMS "

    Public Enum enPIPCustomType
        FREE_TEXT = 1
        SELECTION = 2
        DATE_SELECTION = 3
        NUMERIC_DATA = 4
    End Enum

    Public Enum enPIPSelectionMode
        JOB_CAPABILITIES_COURSES = 1
        CAREER_DEVELOPMENT_COURSES = 2
    End Enum

#End Region


    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  itemunkid " & _
                      ", item " & _
                      ", parameterunkid " & _
                      ", itemtypeid " & _
                      ", selectionmodeid " & _
                      ", isapplyworkprocess " & _
                      ", isimprovementgoal " & _
                      ", sortorder " & _
                      ", isactive " & _
                      " FROM " & mstrDatabaseName & "..pipitem_master " & _
                      " WHERE itemunkid = @itemunkid "

            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintItemunkid = CInt(dtRow.Item("itemunkid"))
                mstrItem = dtRow.Item("item").ToString
                mintParameterunkid = CInt(dtRow.Item("parameterunkid"))
                mintItemTypeid = CInt(dtRow.Item("itemtypeid"))
                mintSelectionModeid = CInt(dtRow.Item("selectionmodeid"))
                mintSortOrder = CInt(dtRow.Item("sortorder"))
                mblnIsApplyWorkProcess = CBool(dtRow.Item("isapplyworkprocess"))
                mblnIsImprovementGoal = CBool(dtRow.Item("isimprovementgoal"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList_CustomTypes(Optional ByVal iList As String = "List", Optional ByVal iAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOpr As New clsDataOperation
        Try
            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            StrQ &= "SELECT '" & enPIPCustomType.FREE_TEXT & "' AS Id, @FT AS Name UNION " & _
                   "SELECT '" & enPIPCustomType.SELECTION & "' AS Id, @ST AS Name UNION " & _
                   "SELECT '" & enPIPCustomType.DATE_SELECTION & "' AS Id, @DS AS Name UNION " & _
                   "SELECT '" & enPIPCustomType.NUMERIC_DATA & "' AS Id, @ND AS Name "

            objDataOpr.AddParameter("@FT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Free Text"))
            objDataOpr.AddParameter("@ST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Selection"))
            objDataOpr.AddParameter("@DS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Date"))
            objDataOpr.AddParameter("@ND", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Numeric Data"))
            objDataOpr.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Select"))

            If iList.Trim.Length <= 0 Then iList = "List"

            dsList = objDataOpr.ExecQuery(StrQ, iList)

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetList_CustomTypes", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList_SelectionMode(Optional ByVal iList As String = "List", Optional ByVal iAddSelect As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOpr As New clsDataOperation
        Try

            If iAddSelect = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If

            StrQ &= " SELECT '" & enPIPSelectionMode.JOB_CAPABILITIES_COURSES & "' AS Id, @JCC AS Name UNION " & _
                         " SELECT '" & enPIPSelectionMode.CAREER_DEVELOPMENT_COURSES & "' AS Id, @CDC AS Name "


            objDataOpr.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Select"))
            objDataOpr.AddParameter("@JCC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Job Capability Courses"))
            objDataOpr.AddParameter("@CDC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Career Development Courses"))

            If iList.Trim.Length <= 0 Then iList = "List"

            dsList = objDataOpr.ExecQuery(StrQ, iList)

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetList_SelectionMode", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal mblnIncludeGrp As Boolean, Optional ByVal xParameterId As Integer = 0, Optional ByVal xImprovementGoal As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objCItems As New clsassess_custom_items
        Dim mdicItemType As New Dictionary(Of Integer, String)
        Dim mdicSelectionMode As New Dictionary(Of Integer, String)
        Dim mdicApplicableMode As New Dictionary(Of Integer, String)

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Dim ds As DataSet = GetList_CustomTypes("List", True)
        mdicItemType = (From p In ds.Tables("List") Select New With {Key .Id = CInt(p.Item("Id")), Key .Name = p.Item("name").ToString}).ToDictionary(Function(x) CInt(x.Id), Function(y) y.Name)

        ds = GetList_SelectionMode("List", False)
        mdicSelectionMode = (From p In ds.Tables("List") Select New With {Key .Id = CInt(p.Item("Id")), Key .Name = p.Item("name").ToString}).ToDictionary(Function(x) CInt(x.Id), Function(y) y.Name)

        Try

            objDataOperation.ClearParameters()

            If mblnIncludeGrp Then

            strQ = "SELECT " & _
                          " CAST(1 AS BIT) AS IsGrp " & _
                          ",  0 AS itemunkid " & _
                          ", '' AS item " & _
                          ", pipparameter_master.parameterunkid " & _
                          ", 0 AS itemtypeid " & _
                          ", 0 AS selectionmodeid " & _
                          ", 0 AS sortorder " & _
                          ", CAST(0 AS BIT) AS isapplyworkprocess " & _
                          ", CAST(0 AS BIT) AS isimprovementgoal " & _
                          ", pipparameter_master.isactive " & _
                          ", ISNULL(pipparameter_master.parameter, '') as parameter " & _
                          ", '' AS  itemtype " & _
                          ", ''  AS selectionmode " & _
                          " FROM " & mstrDatabaseName & "..pipitem_master " & _
                          " LEFT JOIN " & mstrDatabaseName & ".. pipparameter_master ON pipparameter_master.parameterunkid = pipitem_master.parameterunkid " & _
                          " WHERE pipitem_master.isactive = 1 "

                If xImprovementGoal >= 0 Then
                    strQ &= " AND pipitem_master.isimprovementgoal = @isimprovementgoal "
                    objDataOperation.AddParameter("@isimprovementgoal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, xImprovementGoal)
                End If

                If xParameterId > 0 Then
                    strQ &= " AND pipparameter_master.parameterunkid = @parameterunkid "
                    objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xParameterId)
                End If

                strQ &= " UNION "

            End If


            strQ &= "SELECT " & _
                      "  CAST(0 AS BIT) AS IsGrp " & _
                      ", itemunkid " & _
                      ", ISNULL(item, '') AS item " & _
                      ", pipparameter_master.parameterunkid " & _
                      ", itemtypeid " & _
                      ", selectionmodeid " & _
                      ", pipitem_master.sortorder " & _
                      ", pipitem_master.isapplyworkprocess " & _
                      ", pipitem_master.isimprovementgoal " & _
                      ", pipparameter_master.isactive " & _
                      ", isnull(pipparameter_master.parameter, '') as parameter "

            If mdicItemType.Count > 0 Then
                strQ &= ", CASE "
                For Each key In mdicItemType
                    strQ &= " WHEN itemtypeid = " & key.Key & " THEN '" & key.Value & "' "
                Next
                strQ &= " END AS itemtype "
            End If

            If mdicSelectionMode.Count > 0 Then
                strQ &= ", CASE "
                For Each key In mdicSelectionMode
                    strQ &= " WHEN selectionmodeid = " & key.Key & " THEN '" & key.Value & "' "
                Next
                strQ &= " END AS selectionmode "
            End If

            strQ &= " FROM " & mstrDatabaseName & "..pipitem_master " & _
                        " LEFT JOIN " & mstrDatabaseName & ".. pipparameter_master ON pipparameter_master.parameterunkid = pipitem_master.parameterunkid "


            strQ &= " WHERE pipitem_master.isactive = 1 "


            If xImprovementGoal >= 0 Then
                strQ &= " AND pipitem_master.isimprovementgoal = @isimprovementgoal "
                objDataOperation.AddParameter("@isimprovementgoal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, xImprovementGoal)
            End If

            If xParameterId > 0 Then
                strQ &= " AND pipparameter_master.parameterunkid = @parameterunkid "
                objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xParameterId)
            End If

            strQ &= " ORDER BY parameter,sortorder,item, IsGrp DESC "


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (pdpitem_master) </purpose>
    Public Function Insert(Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean

        If isExist(mstrItem, mintParameterunkid, -1, objDataOpr) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Item is already defined. Please define new Item.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strDtFilter As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@item", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrItem.ToString)
            objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemTypeid.ToString)
            objDataOperation.AddParameter("@selectionmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelectionModeid.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isapplyworkprocess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsApplyWorkProcess.ToString)
            objDataOperation.AddParameter("@isimprovementgoal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsImprovementGoal.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)


            strQ = "INSERT INTO " & mstrDatabaseName & "..pipitem_master ( " & _
                      "  parameterunkid " & _
                      ", item " & _
                      ", itemtypeid " & _
                      ", selectionmodeid " & _
                      ", sortorder " & _
                      ", isapplyworkprocess " & _
                      ", isimprovementgoal" & _
                      ", isactive" & _
                    ") VALUES (" & _
                      "  @parameterunkid " & _
                      ", @item " & _
                      ", @itemtypeid " & _
                      ", @selectionmodeid " & _
                      ", @sortorder " & _
                      ", @isapplyworkprocess " & _
                      ", @isimprovementgoal" & _
                      ", @isactive" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintItemunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (pdpitem_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrItem, mintParameterunkid, mintItemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Item is already defined. Please define new Item.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@item", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrItem.ToString)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemTypeid.ToString)
            objDataOperation.AddParameter("@selectionmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelectionModeid.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isapplyworkprocess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsApplyWorkProcess.ToString)
            objDataOperation.AddParameter("@isimprovementgoal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsImprovementGoal.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)


            strQ = "UPDATE " & mstrDatabaseName & "..pipitem_master SET " & _
                      " item = @item" & _
                      ", parameterunkid = @parameterunkid" & _
                      ", itemtypeid = @itemtypeid" & _
                      ", selectionmodeid = @selectionmodeid" & _
                      ", sortorder = @sortorder" & _
                      ", isapplyworkprocess = @isapplyworkprocess " & _
                      ", isimprovementgoal = @isimprovementgoal" & _
                      ", isactive = @isactive " & _
                      " WHERE itemunkid = @itemunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (pdpitem_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, _
                           Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal dtStages As DataTable = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        _Itemunkid(xDataOpr) = intUnkid
        mblnIsactive = False

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = " UPDATE " & mstrDatabaseName & "..pipitem_master SET " & _
                      " isactive = 0 " & _
                      " WHERE itemunkid = @itemunkid "

            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            Dim Tables() As String = {"pipconcern_expectationtran", "pipimprovement_plantran", "pipmonitoring_tran"}
            For Each value As String In Tables
                Select Case value
                    Case "pipconcern_expectationtran"
                        strQ = "SELECT itemunkid FROM " & value & " WHERE itemunkid = @itemunkid "

                    Case "pipimprovement_plantran"
                        strQ = "SELECT itemunkid FROM " & value & " WHERE itemunkid = @itemunkid "

                    Case "pipmonitoring_tran"
                        strQ = "SELECT itemunkid FROM " & value & " WHERE itemunkid = @itemunkid "
                    Case Else
                End Select
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    Return True
                    Exit For
                End If

            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strItem As String, ByVal xParameterId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal objDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "  itemunkid " & _
                   ", item " & _
                   ", parameterunkid " & _
                   ", itemtypeid " & _
                   ", selectionmodeid " & _
                   ", sortorder " & _
                   ", isapplyworkprocess " & _
                   ", isimprovementgoal " & _
                   ", isactive " & _
                   " FROM " & mstrDatabaseName & "..pipitem_master " & _
                   " WHERE isactive = 1 " & _
                   " AND parameterunkid = @parameterunkid " & _
                   " AND item = @item "

            If intUnkid > 0 Then
                strQ &= " AND itemunkid <> @itemunkid"
            End If

            objDataOperation.AddParameter("@item", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strItem)
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xParameterId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try

            StrQ = "INSERT INTO " & mstrDatabaseName & "..atpipitem_master ( " & _
                    "  itemunkid " & _
                    ", item " & _
                    ", parameterunkid " & _
                    ", itemtypeid " & _
                    ", selectionmodeid " & _
                    ", sortorder " & _
                    ", isapplyworkprocess " & _
                    ", isimprovementgoal " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", form_name " & _
                    ", ip " & _
                    ", machine_name " & _
                    ", isweb" & _
                  ") VALUES (" & _
                    "  @itemunkid " & _
                    ", @item " & _
                    ", @parameterunkid " & _
                    ", @itemtypeid " & _
                    ", @selectionmodeid " & _
                    ", @isapplyworkprocess " & _
                    ", @isimprovementgoal " & _
                    ", @sortorder " & _
                    ", @audittype " & _
                    ", @audituserunkid " & _
                    ", GETDATE() " & _
                    ", @form_name " & _
                    ", @ip " & _
                    ", @machine_name " & _
                    ", @isweb" & _
                  ")"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemunkid.ToString)
            objDataOperation.AddParameter("@item", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrItem.ToString)
            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintParameterunkid.ToString)
            objDataOperation.AddParameter("@itemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintItemTypeid.ToString)
            objDataOperation.AddParameter("@selectionmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelectionModeid.ToString)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSortOrder.ToString)
            objDataOperation.AddParameter("@isapplyworkprocess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsApplyWorkProcess.ToString)
            objDataOperation.AddParameter("@isimprovementgoal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsImprovementGoal.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(ByVal strList As String, ByVal mblnIsImprovementGoal As Boolean, Optional ByVal blnSelect As Boolean = False, Optional ByVal intCategoryunkid As Integer = 0) As DataSet
        Dim StrQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            If blnSelect Then
                StrQ = "SELECT 0 AS Id, @SE AS Name, CAST(0 AS BIT) AS isapplyworkprocess , -1 AS sortorder UNION ALL "
            End If
            StrQ &= "SELECT " & _
                        "  itemunkid AS Id " & _
                        ", item AS Name " & _
                        ", isapplyworkprocess " & _
                        ", sortorder " & _
                        " FROM " & mstrDatabaseName & "..pipitem_master " & _
                        " WHERE isactive = 1 AND isimprovementgoal = @isimprovementgoal "

            If intCategoryunkid > 0 Then
                StrQ &= " AND parameterunkid = @parameterunkid"
                objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCategoryunkid)
            End If

            StrQ &= " ORDER BY sortorder "

            objDataOperation.AddParameter("@SE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Select"))
            objDataOperation.AddParameter("@isimprovementgoal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsImprovementGoal)

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isSortOrderExist(ByVal intSortOrder As Integer, ByVal xParameterId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOper As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT " & _
                    "  itemunkid " & _
                    ", item " & _
                    ", sortorder " & _
                    ", parameterunkid " & _
                    " FROM  " & mstrDatabaseName & "..pipitem_master " & _
                    " WHERE isactive = 1 " & _
                    " AND sortorder = @sortorder and parameterunkid=@parameterunkid "

            If intUnkid > 0 Then
                strQ &= " and itemunkid <> @itemunkid "
                objDataOperation.AddParameter("@itemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@parameterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xParameterId)
            objDataOperation.AddParameter("@sortorder", SqlDbType.Int, eZeeDataType.INT_SIZE, intSortOrder)
            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isSortOrderExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOper Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, This Item is already defined. Please define new Item.")
            Language.setMessage(mstrModuleName, 2, "Free Text")
            Language.setMessage(mstrModuleName, 3, "Selection")
            Language.setMessage(mstrModuleName, 4, "Date")
            Language.setMessage(mstrModuleName, 5, "Numeric Data")
            Language.setMessage(mstrModuleName, 6, "Select")
            Language.setMessage(mstrModuleName, 7, "Job Capability Courses")
            Language.setMessage(mstrModuleName, 8, "Career Development Courses")
            Language.setMessage(mstrModuleName, 9, "Employee")
            Language.setMessage(mstrModuleName, 10, "Manager")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
