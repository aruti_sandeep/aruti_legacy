﻿'************************************************************************************************************************************
'Class Name : clsholdemployee_master.vb
'Purpose    :
'Date       :08-Oct-2016
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm


''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsholdemployee_master
    Private Shared ReadOnly mstrModuleName As String = "clsholdemployee_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintHoldunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtTransactiondate As Date
    Private mdtEffectivedate As Date
    Private mintOperationtype As Integer
    Private mstrRemark As String = String.Empty
    Private mblnIspost As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set holdunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Holdunkid(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintHoldunkid
        End Get
        Set(ByVal value As Integer)
            mintHoldunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set operationtype
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Operationtype() As Integer
        Get
            Return mintOperationtype
        End Get
        Set(ByVal value As Integer)
            mintOperationtype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ispost
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ispost() As Boolean
        Get
            Return mblnIspost
        End Get
        Set(ByVal value As Boolean)
            mblnIspost = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If


        Try
            strQ = "SELECT " & _
              "  holdunkid " & _
              ", employeeunkid " & _
              ", transactiondate " & _
              ", effectivedate " & _
              ", operationtype " & _
              ", remark " & _
              ", ispost " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM tnaholdemployee_master " & _
             "WHERE holdunkid = @holdunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintHoldunkid = CInt(dtRow.Item("holdunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtTransactiondate = dtRow.Item("transactiondate")
                mdtEffectivedate = dtRow.Item("effectivedate")
                mintOperationtype = CInt(dtRow.Item("operationtype"))
                mstrRemark = dtRow.Item("remark").ToString
                mblnIspost = CBool(dtRow.Item("ispost"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = IIf(dtRow.Item("voiddatetime") Is Nothing, Nothing, dtRow.Item("voiddatetime"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                                      , ByVal xDatabaseName As String _
                                      , ByVal xUserUnkid As Integer _
                                      , ByVal xYearUnkid As Integer _
                                      , ByVal xCompanyUnkid As Integer _
                                      , ByVal xStartDate As DateTime _
                                      , ByVal xEndDate As DateTime _
                                      , ByVal xUserModeSetting As String _
                                      , ByVal xOnlyApproved As Boolean _
                                      , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                      , Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = "" : xUACQry = "" : xUACFiltrQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xStartDate.Date, xEndDate.Date, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xEndDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xEndDate.Date, xDatabaseName)

            strQ = " SELECT " & _
                      " CAST(0 AS bit) As ischeck " & _
                      ", -1 As holdunkid " & _
                      ", hremployee_master.employeeunkid " & _
                      ", ISNULL(hremployee_master.employeecode,'') AS Employeecode " & _
                      ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                      ", '' as transactiondate " & _
                      ", '' as effectivedate " & _
                      ", 0 as  operationtypeid " & _
                      ", '' as  operationtype " & _
                      ", '' as  Remark " & _
                      ", 1 as IsGrp " & _
                      "  FROM tnaholdemployee_master " & _
                      " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnaholdemployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If


            strQ &= " WHERE tnaholdemployee_master.isvoid = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If


            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If


            strQ &= " UNION " & _
                        " SELECT " & _
                        " CAST(0 AS bit) As ischeck " & _
                        ", tnaholdemployee_master.holdunkid " & _
                        ", hremployee_master.employeeunkid " & _
                        ", '' AS Employeecode " & _
                        ", '' AS Employee " & _
                        ", CONVERT(CHAR(8),tnaholdemployee_master.transactiondate,112) AS transactiondate " & _
                        ",  CONVERT(CHAR(8),tnaholdemployee_master.effectivedate,112) AS effectivedate " & _
                        ", tnaholdemployee_master.operationtype AS operationtypeid " & _
                        ", CASE WHEN tnaholdemployee_master.operationtype = 1 THEN @hold WHEN  tnaholdemployee_master.operationtype = 2 THEN @unhold END AS operationtype  " & _
                        ", tnaholdemployee_master.remark " & _
                        ", 0 as IsGrp " & _
                        "  FROM tnaholdemployee_master " & _
                        " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = tnaholdemployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If


            strQ &= " WHERE tnaholdemployee_master.isvoid = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If


            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            strQ &= " ORDER BY employeeunkid,isgrp DESC, effectivedate DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@hold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Hold"))
            objDataOperation.AddParameter("@unhold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Unhold"))
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnaholdemployee_master) </purpose>
    Public Function Insert() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtTransactiondate <> Nothing, mdtTransactiondate, DBNull.Value))
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEffectivedate <> Nothing, mdtEffectivedate, DBNull.Value))
            objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationtype.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@ispost", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspost.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO tnaholdemployee_master ( " & _
                      "  employeeunkid " & _
                      ", transactiondate " & _
                      ", effectivedate " & _
                      ", operationtype " & _
                      ", remark " & _
                      ", ispost " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @employeeunkid " & _
                      ", @transactiondate " & _
                      ", @effectivedate " & _
                      ", @operationtype " & _
                      ", @remark " & _
                      ", @ispost " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintHoldunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForHoldUnHold(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tnaholdemployee_master) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintHoldunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationtype.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@ispost", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspost.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE tnaholdemployee_master SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", transactiondate = @transactiondate" & _
              ", effectivedate = @effectivedate" & _
              ", operationtype = @operationtype" & _
              ", remark = @remark" & _
              ", ispost = @ispost" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE holdunkid = @holdunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnaholdemployee_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = " UPDATE tnaholdemployee_master SET " & _
                       " isvoid = 1,voiddatetime = GetDate() " & _
                       ", voiduserunkid = @voiduserunkid,voidreason = @voidreason " & _
                       " WHERE holdunkid = @holdunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Holdunkid(objDataOperation) = intUnkid

            If InsertAuditTrailForHoldUnHold(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isOperationExist(ByVal dtDate As Date, ByVal xEmployeeID As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mblnFlag As Boolean = False
        Dim objDataOperation As New clsDataOperation
        Dim xFirstOperationType As Integer = 0
        Dim xLastOperationType As Integer = 0

        Try
            strQ = "SELECT TOP 1 effectivedate,operationType From tnaholdemployee_master WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND CONVERT(char(8),effectivedate,112) < @date Order by effectivedate desc"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeID)
            objDataOperation.AddParameter("@date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                xFirstOperationType = CInt(dsList.Tables(0).Rows(0)("operationtype"))
            End If

            strQ = "SELECT TOP 1 effectivedate,operationType From tnaholdemployee_master WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND CONVERT(char(8),effectivedate,112) > @date Order by effectivedate asc"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeID)
            objDataOperation.AddParameter("@date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                xLastOperationType = CInt(dsList.Tables(0).Rows(0)("operationtype"))
            End If

            If (xFirstOperationType > 0 AndAlso xLastOperationType > 0) AndAlso (xFirstOperationType = xLastOperationType) Then mblnFlag = True Else mblnFlag = False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isOperationExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return mblnFlag
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  holdunkid " & _
              ", employeeunkid " & _
              ", transactiondate " & _
              ", effectivedate " & _
              ", operationtype " & _
              ", remark " & _
              ", ispost " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM tnaholdemployee_master " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND holdunkid <> @holdunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeFromOperation(ByVal xDatabaseName As String _
                                                                    , ByVal xUserUnkid As Integer _
                                                                    , ByVal xYearUnkid As Integer _
                                                                    , ByVal xCompanyUnkid As Integer _
                                                                    , ByVal xDate As DateTime _
                                                                    , ByVal xUserModeSetting As String _
                                                                    , ByVal xOnlyApproved As Boolean _
                                                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                                    , ByVal xOperationType As Integer _
                                                                    , Optional ByVal blnExcludeTermEmp_PayProcess As Boolean = False _
                                                                    , Optional ByVal blnAddApprovalCondition As Boolean = True _
                                                                    , Optional ByVal xFilterString As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xDate, xDate, , blnExcludeTermEmp_PayProcess, xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xDate, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , blnAddApprovalCondition)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xDate, xDatabaseName)

            strQ = " SELECT " & _
                      " hremployee_master.employeeunkid " & _
                      ", hremployee_master.employeecode " & _
                      ",ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                      ", ISNULL(a.holdunkid,-1) as holdunkid " & _
                      ",CASE WHEN ISNULL(a.operationtype,-1) <= 0 THEN '' " & _
                      "           WHEN a.operationtype = 1 THEN @hold " & _
                      "           WHEN a.operationtype = 2 THEN @unhold " & _
                      "  End AS Operation " & _
                      " FROM hremployee_master "

            If xOperationType = 2 Then 'Unhold 
                strQ &= "  JOIN  "
            Else
                strQ &= "  LEFT JOIN  "
            End If


            strQ &= " ( " & _
                        "          SELECT " & _
                        "          holdunkid " & _
                        "         ,employeeunkid " & _
                        "         ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY operationtype DESC,effectivedate DESC) AS RowNo " & _
                        "         ,operationtype " & _
                        "          FROM tnaholdemployee_master " & _
                        "          WHERE tnaholdemployee_master.isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112)  <= @Date " & _
                        "  ) " & _
                        "  AS A  ON A.employeeunkid = hremployee_master.employeeunkid AND a.RowNo = 1  "

            If xOperationType > 0 Then
                strQ &= " AND A.operationtype <> @operationtype "
            End If

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE 1= 1 "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If xFilterString.Trim.Length > 0 Then
                strQ &= " AND " & xFilterString
            End If

            objDataOperation.AddParameter("@hold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Hold"))
            objDataOperation.AddParameter("@unhold", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Unhold"))
            objDataOperation.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, xOperationType)
            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeFromOperation; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeExistForHoldUnhold(ByVal xDate As Date, ByVal xEmployeeId As Integer) As Boolean
        Dim mblnFlag As Boolean = False
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation


            strQ = " SELECT ISNULL(holdunkid,0) AS holdunkid FROM tnaholdemployee_master " & _
                      " WHERE isvoid = 0 AND Employeeunkid = @employeeunkid AND Convert(Char(8),effectivedate,112) = @date "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate))

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                mblnFlag = True
            ElseIf dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mblnFlag = False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeExistForHoldUnhold; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrailForHoldUnHold(ByVal objDoOpration As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = " INSERT INTO attnaholdemployee_master ( " & _
                      "  holdunkid  " & _
                      ", employeeunkid " & _
                      ", transactiondate " & _
                      ", effectivedate " & _
                      ", operationtype " & _
                      ", remark " & _
                      ", ispost " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip" & _
                      ", machine_name" & _
                      ", form_name " & _
                      ", module_name1 " & _
                      ", module_name2 " & _
                      ", module_name3 " & _
                      ", module_name4 " & _
                      ", module_name5 " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      "  @holdunkid  " & _
                      ", @employeeunkid " & _
                      ", @transactiondate " & _
                      ", @effectivedate " & _
                      ", @operationtype " & _
                      ", @remark " & _
                      ", @ispost " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GetDate() " & _
                      ", @ip" & _
                      ", @machine_name" & _
                      ", @form_name " & _
                      ", @module_name1 " & _
                      ", @module_name2 " & _
                      ", @module_name3 " & _
                      ", @module_name4 " & _
                      ", @module_name5 " & _
                      ", @isweb " & _
                    "); SELECT @@identity"


            objDoOpration.ClearParameters()
            objDoOpration.AddParameter("@holdunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHoldunkid.ToString)
            objDoOpration.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDoOpration.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtTransactiondate <> Nothing, mdtTransactiondate, DBNull.Value))
            objDoOpration.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEffectivedate <> Nothing, mdtEffectivedate, DBNull.Value))
            objDoOpration.AddParameter("@operationtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationtype.ToString)
            objDoOpration.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDoOpration.AddParameter("@ispost", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIspost.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDoOpration.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            If mstrWebClientIP.ToString().Length <= 0 Then
                mstrWebClientIP = getIP()
            End If

            If mstrWebHostName.ToString().Length <= 0 Then
                mstrWebHostName = getHostName()
            End If

            objDoOpration.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            objDoOpration.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDoOpration.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDoOpration.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDoOpration.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDoOpration.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDoOpration.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDoOpration.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If

            objDoOpration.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDoOpration.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDoOpration.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDoOpration.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            dsList = objDoOpration.ExecQuery(strQ, "List")

            If objDoOpration.ErrorMessage <> "" Then
                exForce = New Exception(objDoOpration.ErrorNumber & ": " & objDoOpration.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForHoldUnHold; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "WEB")
            Language.setMessage(mstrModuleName, 2, "Hold")
            Language.setMessage(mstrModuleName, 3, "Unhold")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
