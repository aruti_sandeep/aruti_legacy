﻿'************************************************************************************************************************************
'Class Name : clspolicy_master.vb
'Purpose    :
'Date       :27/09/2013
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clspolicy_master
    Private Shared ReadOnly mstrModuleName As String = "clspolicy_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintPolicyunkid As Integer
    Private mstrPolicycode As String = String.Empty
    Private mstrPolicyname As String = String.Empty
    Private mstrPolicyname1 As String = String.Empty
    Private mstrPolicyname2 As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private objPolicyTran As New clsPolicy_tran


    'Pinkal (06-May-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
    Dim mdtOTPenalty As DataTable = Nothing
    'Pinkal (06-May-2016) -- End


#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set policyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Policyunkid() As Integer
        Get
            Return mintPolicyunkid
        End Get
        Set(ByVal value As Integer)
            mintPolicyunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set policycode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Policycode() As String
        Get
            Return mstrPolicycode
        End Get
        Set(ByVal value As String)
            mstrPolicycode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set policyname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Policyname() As String
        Get
            Return mstrPolicyname
        End Get
        Set(ByVal value As String)
            mstrPolicyname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set policyname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Policyname1() As String
        Get
            Return mstrPolicyname1
        End Get
        Set(ByVal value As String)
            mstrPolicyname1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set policyname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Policyname2() As String
        Get
            Return mstrPolicyname2
        End Get
        Set(ByVal value As String)
            mstrPolicyname2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property


    'Pinkal (06-May-2016) -- Start
    'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.

    ''' <summary>
    ''' Purpose: Get or Set dtOTPenalty
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtOTPenalty() As DataTable
        Get
            Return mdtOTPenalty
        End Get
        Set(ByVal value As DataTable)
            mdtOTPenalty = value
        End Set
    End Property

    'Pinkal (06-May-2016) -- End


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  policyunkid " & _
              ", policycode " & _
              ", policyname " & _
              ", policyname1 " & _
              ", policyname2 " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             "FROM tnapolicy_master " & _
             "WHERE policyunkid = @policyunkid "

            objDataOperation.AddParameter("@policyunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintPolicyUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintpolicyunkid = CInt(dtRow.Item("policyunkid"))
                mstrpolicycode = dtRow.Item("policycode").ToString
                mstrpolicyname = dtRow.Item("policyname").ToString
                mstrpolicyname1 = dtRow.Item("policyname1").ToString
                mstrpolicyname2 = dtRow.Item("policyname2").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mstrFilter As String = "") As DataSet
        'Pinkal (09-Jun-2022)-- Enhancment : Shift and policy are compulsory created before importing any employee.
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  policyunkid " & _
              ", policycode " & _
              ", policyname " & _
              ", policyname1 " & _
              ", policyname2 " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             "FROM tnapolicy_master "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            'Pinkal (09-Jun-2022)-- Start
            'Enhancment : Shift and policy are compulsory created before importing any employee.
            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If
            'Pinkal (09-Jun-2022)-- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnapolicy_master) </purpose>
    Public Function Insert(ByVal mdtDays As DataTable) As Boolean

        If isExist(mstrPolicycode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Policy Code is already defined. Please define new Policy Code.")
            Return False
        ElseIf isExist("", mstrPolicyname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Policy Name is already defined. Please define new Policy Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@policycode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPolicycode.ToString)
            objDataOperation.AddParameter("@policyname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPolicyname.ToString)
            objDataOperation.AddParameter("@policyname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPolicyname1.ToString)
            objDataOperation.AddParameter("@policyname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPolicyname2.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO tnapolicy_master ( " & _
                      "  policycode " & _
                      ", policyname " & _
                      ", policyname1 " & _
                      ", policyname2 " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @policycode " & _
                      ", @policyname " & _
                      ", @policyname1 " & _
                      ", @policyname2 " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPolicyunkid = dsList.Tables(0).Rows(0).Item(0)

            objPolicyTran._DataList = mdtDays
            objPolicyTran._Policyunkid = mintPolicyunkid
            objPolicyTran._Userunkid = mintUserunkid
            If objPolicyTran.InsertUpdatePolicyDays(objDataOperation) = False Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (06-May-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.

            If mdtOTPenalty IsNot Nothing AndAlso mdtOTPenalty.Rows.Count > 0 Then
                Dim objOTPenalty As New clsOT_Penalty
                objOTPenalty._Policyunkid = mintPolicyunkid
                objOTPenalty._Userunkid = mintUserunkid
                If objOTPenalty.InsertOTPenalty(objDataOperation, mdtOTPenalty) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOTPenalty = Nothing
            End If

            'Pinkal (06-May-2016) -- End


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tnapolicy_master) </purpose>
    Public Function Update(ByVal mdtDays As DataTable) As Boolean

        If isExist(mstrPolicycode, "", mintPolicyunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Policy Code is already defined. Please define new Policy Code.")
            Return False
        ElseIf isExist("", mstrPolicyname, mintPolicyunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Policy Name is already defined. Please define new Policy Name.")
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPolicyunkid.ToString)
            objDataOperation.AddParameter("@policycode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPolicycode.ToString)
            objDataOperation.AddParameter("@policyname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPolicyname.ToString)
            objDataOperation.AddParameter("@policyname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPolicyname1.ToString)
            objDataOperation.AddParameter("@policyname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPolicyname2.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = " UPDATE tnapolicy_master SET " & _
                      "  policycode = @policycode" & _
                      ", policyname = @policyname" & _
                      ", policyname1 = @policyname1" & _
                      ", policyname2 = @policyname2" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                    "WHERE policyunkid = @policyunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objPolicyTran._DataList = mdtDays
            objPolicyTran._Policyunkid = mintPolicyunkid
            objPolicyTran._Userunkid = mintUserunkid

            Dim dt() As DataRow = objPolicyTran._DataList.Select("AUD=''")
            If dt.Length = objPolicyTran._DataList.Rows.Count Then

                If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "tnapolicy_master", mintPolicyunkid, "policyunkid", 2) Then

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "tnapolicy_master", "policyunkid", mintPolicyunkid, "tnapolicy_tran", "policytranunkid", -1, 2, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If

            Else
                If objPolicyTran.InsertUpdatePolicyDays(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            'Pinkal (06-May-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
            If mdtOTPenalty IsNot Nothing AndAlso mdtOTPenalty.Rows.Count > 0 Then
                Dim objOTPenalty As New clsOT_Penalty
                objOTPenalty._Policyunkid = mintPolicyunkid
                objOTPenalty._Userunkid = mintUserunkid
                If objOTPenalty.InsertOTPenalty(objDataOperation, mdtOTPenalty) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objOTPenalty = Nothing
            End If
            'Pinkal (06-May-2016) -- End


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnapolicy_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
     

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE tnapolicy_master SET isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                      " WHERE policyunkid = @policyunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objPolicyTran._Userunkid = mintVoiduserunkid
            If objPolicyTran.DeletePolicyDays(objDataOperation, intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "Select isnull(policyunkid,0) from hremployee_policy_tran where policyunkid = @policyunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            strQ = "Select isnull(policyunkid,0) from tnalogin_summary where policyunkid = @policyunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            'Pinkal (06-May-2016) -- Start
            'Enhancement - WORKING ON ENHANCEMENT ON OT PENALTY FEATURE FOR ASP.
            strQ = "Select isnull(policyunkid,0) from tnaot_penalty where policyunkid = @policyunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True
            'Pinkal (06-May-2016) -- End


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal 
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  policyunkid " & _
              ", policycode " & _
              ", policyname " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             "FROM tnapolicy_master " & _
             "WHERE 1=1 AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND policyunkid <> @policyunkid"
            End If

            If strCode.Length > 0 Then
                strQ &= " AND policycode = @code "
            End If
            If strName.Length > 0 Then
                strQ &= " AND policyname = @name "
            End If


            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try

            If mblFlag = True Then
                strQ = "SELECT 0 as policyunkid, ' ' +  @name as name, '' AS policycode UNION "
            End If
            strQ &= " SELECT tnapolicy_master.policyunkid, tnapolicy_master.policyname as name, policycode AS policycode " & _
                        " FROM tnapolicy_master " & _
                        " WHERE tnapolicy_master.isvoid = 0 "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Public Function GetPolicyUnkid(ByVal strPolicyName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intPolicyId As Integer = 0
        objDataOperation = New clsDataOperation
        Try
            strQ = " SELECT " & _
                        " policyunkid " & _
                   " FROM tnapolicy_master " & _
                   " WHERE policyname=@policyname AND isvoid=0"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@policyname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strPolicyName.Trim())

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intPolicyId = CInt(dsList.Tables("List").Rows(0).Item(0))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPolicyUnkid; Module Name: " & mstrModuleName)
        End Try
        Return intPolicyId
    End Function
    'S.SANDEEP [14-JUN-2018] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Policy Code is already defined. Please define new Policy Code.")
			Language.setMessage(mstrModuleName, 2, "This Policy Name is already defined. Please define new Policy Name.")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class