﻿'************************************************************************************************************************************
'Class Name : clsPolicy_tran.vb
'Purpose    :
'Date       :27/09/2013
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>

Public Class clsPolicy_tran

    Private Shared ReadOnly mstrModuleName As String = "clsPolicy_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintPolicyTranunkid As Integer = -1
    Private mintPolicyunkid As Integer = -1
    Private mintUserunkid As Integer = -1
    Private mblnIsvoid As Boolean = False
    Private mintVoidUserunkid As Integer = -1
    Private mdtVoiddatetime As DateTime = Nothing
    Private mstrVoidReason As String = ""
    Private mdtTran As DataTable
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get or Set PolicyTranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _PolicyTranunkid() As Integer
        Get
            Return mintPolicyTranunkid
        End Get
        Set(ByVal value As Integer)
            mintPolicyTranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Policyunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Policyunkid() As Integer
        Get
            Return mintPolicyunkid
        End Get
        Set(ByVal value As Integer)
            mintPolicyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DataList
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DataList() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set VoidUserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _VoidUserunkid() As Integer
        Get
            Return mintVoidUserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As DateTime
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As DateTime)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set VoidReason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _VoidReason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property


#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("PolicyTran")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("policytranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("policyunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("dayid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isweekend")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("basehours")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot1")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot2")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot3")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot4")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("elrcoming_grace")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ltcoming_grace")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("elrgoing_grace")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ltgoing_grace")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("basehoursinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot1insec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot2insec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot3insec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ot4insec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("elrcome_graceinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ltcome_graceinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("elrgoing_graceinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ltgoing_graceinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            'S.SANDEEP [14-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {NMB}
            mdtTran.Columns.Add("isvoid", Type.GetType("System.Boolean"))
            mdtTran.Columns.Add("voiduserunkid", Type.GetType("System.Int32"))
            mdtTran.Columns.Add("voiddatetime", Type.GetType("System.DateTime"))
            mdtTran.Columns.Add("voidreason", Type.GetType("System.String"))
            'S.SANDEEP [14-JUN-2018] -- END


            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'S.SANDEEP [ 07 JAN 2014 ] -- START
            dCol = New DataColumn("breaktime")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("breaktimeinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("countbreaktimeafter")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("countbreaktimeaftersec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP [ 07 JAN 2014 ] -- END


            'Pinkal (28-Jan-2014) -- Start
            'Enhancement : Oman Changes

            dCol = New DataColumn("teatime")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("teatimeinsec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("countteatimeafter")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("countteatimeaftersec")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            'Pinkal (28-Jan-2014) -- End


            'S.SANDEEP [14-JUN-2018] -- START {NMB} [Please add new defined/added column in method for default value {GenerateDefaultDays}] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetPolicyTran(Optional ByVal intPolicyId As Integer = 0, Optional ByVal dayId As Integer = -1, Optional ByVal objDooperation As clsDataOperation = Nothing)

        'Pinkal (25-Jan-2018) -- 'Bug - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.[Optional ByVal objDooperation As clsDataOperation = Nothing]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation

        'Pinkal (25-Jan-2018) -- Start
        'Bug - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
        If objDooperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDooperation
        End If
        'Pinkal (25-Jan-2018) -- End

        objDataOperation.ClearParameters()
        Try

            strQ = " SELECT  " & _
                       " tnapolicy_tran.policytranunkid " & _
                       ",tnapolicy_tran.policyunkid " & _
                       ",tnapolicy_tran.dayid " & _
                       ",'' AS DAYS " & _
                       ",0.00 as basehours " & _
                       ",tnapolicy_tran.isweekend " & _
                       ",0.00 as ot1 " & _
                       ",0.00 as ot2 " & _
                       ",0.00 as ot3 " & _
                       ",0.00 as ot4 " & _
                       ",0 as elrcoming_grace " & _
                       ",0 as ltcoming_grace " & _
                       ",0 as elrgoing_grace " & _
                       ",0 as ltgoing_grace " & _
                       ",tnapolicy_tran.basehours as  basehoursinsec" & _
                       ",tnapolicy_tran.ot1 as ot1insec " & _
                       ",tnapolicy_tran.ot2 as ot2insec " & _
                       ",tnapolicy_tran.ot3 as ot3insec  " & _
                       ",tnapolicy_tran.ot4 as ot4insec  " & _
                       ",tnapolicy_tran.elrcoming_grace as  elrcome_graceinsec " & _
                       ",tnapolicy_tran.ltcoming_grace  as ltcome_graceinsec " & _
                       ",tnapolicy_tran.elrgoing_grace as elrgoing_graceinsec " & _
                       ",tnapolicy_tran.ltgoing_grace  as ltgoing_graceinsec " & _
                       ",isvoid " & _
                       ",voiduserunkid " & _
                       ",voiddatetime " & _
                       ",voidreason " & _
                       " ,'' AS AUD " & _
                       ",0 AS breaktime " & _
                       ",tnapolicy_tran.breaktime AS breaktimeinsec " & _
                       ",0 AS countbreaktimeafter " & _
                       ",tnapolicy_tran.countbreaktimeafter AS countbreaktimeaftersec " & _
                       ",0 AS teatime " & _
                       ",tnapolicy_tran.teatime AS teatimeinsec " & _
                       ",0 AS countteatimeafter " & _
                       ",tnapolicy_tran.countteatimeafter AS countteatimeaftersec " & _
                       " FROM tnapolicy_tran" 'S.SANDEEP [ 07 JAN 2014 ] -- START {breaktime,countbreaktimeafter} -- END

            'Pinkal (28-Jan-2014) -- Start  [teatime,teatimeinsec,countteatimeafter,countteatimeaftersec] -- End
            'S.SANDEEP [14-JUN-2018] -- START {NMB} (isvoid,voiduserunkid,voiddatetime,voidreason) -- END


            If intPolicyId > 0 Then
                strQ &= " WHERE tnapolicy_tran.policyunkid = @policyunkid"
                objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPolicyId)
            End If

            If dayId > 0 Then
                strQ &= "  AND tnapolicy_tran.dayid = @dayid"
                objDataOperation.AddParameter("@dayid", SqlDbType.Int, eZeeDataType.INT_SIZE, dayId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")
            mdtTran = dsList.Tables("List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPolicyTran; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (25-Jan-2018) -- Start
            'Bug - 0001937 Error when recalculating timing Description: Error when recalculating timing and importing device attendance.
            If objDooperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (25-Jan-2018) -- End
        End Try

    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_tran) </purpose>
    Public Function InsertUpdatePolicyDays(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            If mdtTran Is Nothing Then Return True

            For i = 0 To mdtTran.Rows.Count - 1

                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()

                    If Not IsDBNull(.Item("AUD")) Then

                        Select Case .Item("AUD")

                            Case "A"

                                strQ = " INSERT INTO tnapolicy_tran " & _
                                          "  ( policyunkid " & _
                                          "  , dayid " & _
                                          "  , basehours " & _
                                          "  , ot1 " & _
                                          "  , ot2 " & _
                                          "  , ot3 " & _
                                          "  , ot4 " & _
                                          "  , elrcoming_grace " & _
                                          "  , ltcoming_grace " & _
                                          "  , elrgoing_grace " & _
                                          "  , ltgoing_grace " & _
                                          "  , isweekend " & _
                                          "  , userunkid " & _
                                          "  , isvoid " & _
                                          "  , voiduserunkid " & _
                                          "  , voiddatetime " & _
                                          "  , voidreason " & _
                                          "  , breaktime " & _
                                          "  , countbreaktimeafter " & _
                                          "  , teatime " & _
                                          "  , countteatimeafter " & _
                                          ") " & _
                                          "    VALUES (" & _
                                          "   @policyunkid " & _
                                          "  , @dayid " & _
                                          "  , @basehours " & _
                                          "  , @ot1 " & _
                                          "  , @ot2 " & _
                                          "  , @ot3 " & _
                                          "  , @ot4 " & _
                                          "  , @elrcoming_grace " & _
                                          "  , @ltcoming_grace " & _
                                          "  , @elrgoing_grace " & _
                                          "  , @ltgoing_grace " & _
                                          "  , @isweekend " & _
                                          "  , @userunkid " & _
                                          "  , @isvoid " & _
                                          "  , @voiduserunkid " & _
                                          "  , @voiddatetime " & _
                                          "  , @voidreason " & _
                                          "  , @breaktime " & _
                                          "  , @countbreaktimeafter " & _
                                          "  , @teatime " & _
                                          "  , @countteatimeafter " & _
                                          "); SELECT @@identity" 'S.SANDEEP [ 07 JAN 2014 ] -- START {breaktime,countbreaktimeafter} -- END

                                'Pinkal (28-Jan-2014) -- Start  [teatime,countteatimeafter] -- End

                                objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPolicyunkid.ToString)
                                objDataOperation.AddParameter("@dayid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dayid").ToString)
                                objDataOperation.AddParameter("@isweekend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isweekend").ToString)
                                objDataOperation.AddParameter("@basehours", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("basehoursinsec").ToString)
                                objDataOperation.AddParameter("@ot1", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("ot1insec").ToString)
                                objDataOperation.AddParameter("@ot2", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("ot2insec").ToString)
                                objDataOperation.AddParameter("@ot3", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("ot3insec").ToString)
                                objDataOperation.AddParameter("@ot4", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("ot4insec").ToString)
                                objDataOperation.AddParameter("@elrcoming_grace", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("elrcome_graceinsec").ToString)
                                objDataOperation.AddParameter("@ltcoming_grace", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ltcome_graceinsec").ToString)
                                objDataOperation.AddParameter("@elrgoing_grace", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("elrgoing_graceinsec").ToString)
                                objDataOperation.AddParameter("@ltgoing_grace", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ltgoing_graceinsec").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserunkid)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason)
                                'S.SANDEEP [ 07 JAN 2014 ] -- START
                                objDataOperation.AddParameter("@breaktime", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("breaktimeinsec").ToString)
                                objDataOperation.AddParameter("@countbreaktimeafter", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("countbreaktimeaftersec").ToString)
                                'S.SANDEEP [ 07 JAN 2014 ] -- END


                                'Pinkal (28-Jan-2014) -- Start
                                'Enhancement : Oman Changes
                                objDataOperation.AddParameter("@teatime", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("teatimeinsec").ToString)
                                objDataOperation.AddParameter("@countteatimeafter", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("countteatimeaftersec").ToString)
                                'Pinkal (28-Jan-2014) -- End


                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintPolicyTranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("policyunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "tnapolicy_master", "policyunkid", mintPolicyunkid, "tnapolicy_tran", "policytranunkid", mintPolicyTranunkid, 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "tnapolicy_master", "policyunkid", mintPolicyunkid, "tnapolicy_tran", "policytranunkid", mintPolicyTranunkid, 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "U"

                                strQ = " UPDATE tnapolicy_tran SET " & _
                                          "  policyunkid = @policyunkid " & _
                                          " ,dayid = @dayid" & _
                                          " ,basehours =  @basehours" & _
                                          " ,ot1 = @ot1" & _
                                          " ,ot2 = @ot2" & _
                                          " ,ot3 = @ot3" & _
                                          " ,ot4 = @ot4" & _
                                          " ,elrcoming_grace =  @elrcoming_grace" & _
                                          " ,ltcoming_grace =  @ltcoming_grace" & _
                                          " ,elrgoing_grace = @elrgoing_grace" & _
                                          " ,ltgoing_grace =  @ltgoing_grace" & _
                                          " ,isweekend =  @isweekend" & _
                                          " ,breaktime = @breaktime " & _
                                          " ,countbreaktimeafter = @countbreaktimeafter " & _
                                          " ,teatime = @teatime " & _
                                          " ,countteatimeafter = @countteatimeafter " & _
                                          " WHERE policytranunkid = @policytranunkid " 'S.SANDEEP [ 07 JAN 2014 ] -- START {breaktime,countbreaktimeafter} -- END

                                objDataOperation.AddParameter("@policytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("policytranunkid").ToString)
                                objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPolicyunkid.ToString)
                                objDataOperation.AddParameter("@dayid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dayid").ToString)
                                objDataOperation.AddParameter("@isweekend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isweekend").ToString)
                                objDataOperation.AddParameter("@basehours", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("basehoursinsec").ToString)
                                objDataOperation.AddParameter("@ot1", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("ot1insec").ToString)
                                objDataOperation.AddParameter("@ot2", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("ot2insec").ToString)
                                objDataOperation.AddParameter("@ot3", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("ot3insec").ToString)
                                objDataOperation.AddParameter("@ot4", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("ot4insec").ToString)
                                objDataOperation.AddParameter("@elrcoming_grace", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("elrcome_graceinsec").ToString)
                                objDataOperation.AddParameter("@ltcoming_grace", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ltcome_graceinsec").ToString)
                                objDataOperation.AddParameter("@elrgoing_grace", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("elrgoing_graceinsec").ToString)
                                objDataOperation.AddParameter("@ltgoing_grace", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("ltgoing_graceinsec").ToString)
                                'S.SANDEEP [ 07 JAN 2014 ] -- START
                                objDataOperation.AddParameter("@breaktime", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("breaktimeinsec").ToString)
                                objDataOperation.AddParameter("@countbreaktimeafter", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("countbreaktimeaftersec").ToString)
                                'S.SANDEEP [ 07 JAN 2014 ] -- END


                                'Pinkal (28-Jan-2014) -- Start
                                'Enhancement : Oman Changes
                                objDataOperation.AddParameter("@teatime", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("teatimeinsec").ToString)
                                objDataOperation.AddParameter("@countteatimeafter", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("countteatimeaftersec").ToString)
                                'Pinkal (28-Jan-2014) -- End

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "tnapolicy_master", "policyunkid", mintPolicyunkid, "tnapolicy_tran", "policytranunkid", CInt(.Item("policytranunkid").ToString), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select

                    End If

                End With

            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertUpdatePolicyDays", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function DeletePolicyDays(ByVal objDataOperation As clsDataOperation, ByVal intPolicyId As Integer) As Boolean
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            strQ = " SELECT ISNULL(policytranunkid,-1) AS policytranunkid FROM tnapolicy_tran WHERE policyunkid  = @policyunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPolicyId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " UPDATE tnapolicy_tran SET " & _
                      " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                      " WHERE policyunkid = @policyunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@policyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPolicyId)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For j As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "tnapolicy_master", "policyunkid", intPolicyId, "tnapolicy_tran", "policytranunkid", CInt(dsList.Tables(0).Rows(j)("policytranunkid")), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "DeletePolicyDays", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'S.SANDEEP [14-JUN-2018] -- START
    'ISSUE/ENHANCEMENT : {NMB}
    Public Sub GenerateDefaultDays(ByVal dtTable As DataTable, ByVal mintPolicyMasterUnkid As Integer)
        Try
            Dim days() As String = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.DayNames
            For i As Integer = 0 To 6
                Dim dr As DataRow = dtTable.NewRow
                dr("policytranunkid") = -1
                dr("policyunkid") = mintPolicyMasterUnkid
                dr("dayId") = i
                dr("Days") = days(i).ToString()
                dr("isweekend") = False
                dr("basehours") = 0
                dr("ot1") = 0.0
                dr("ot2") = 0.0
                dr("ot3") = 0.0
                dr("ot4") = 0.0
                dr("elrcoming_grace") = 0
                dr("ltcoming_grace") = 0
                dr("elrgoing_grace") = 0
                dr("ltgoing_grace") = 0
                dr("basehoursinsec") = 0
                dr("ot1insec") = 0.0
                dr("ot2insec") = 0.0
                dr("ot3insec") = 0.0
                dr("ot4insec") = 0.0
                dr("elrcome_graceinsec") = 0
                dr("ltcome_graceinsec") = 0
                dr("elrgoing_graceinsec") = 0
                dr("ltgoing_graceinsec") = 0
                dr("isvoid") = False
                dr("voiduserunkid") = -1
                dr("voiddatetime") = DBNull.Value
                dr("voidreason") = ""
                dr("AUD") = "A"
                dr("breaktime") = 0
                dr("breaktimeinsec") = 0
                dr("countbreaktimeafter") = 0
                dr("countbreaktimeaftersec") = 0
                dr("teatime") = 0
                dr("teatimeinsec") = 0
                dr("countteatimeafter") = 0
                dr("countteatimeaftersec") = 0
                dtTable.Rows.Add(dr)
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateDefaultDays; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [14-JUN-2018] -- END

End Class
