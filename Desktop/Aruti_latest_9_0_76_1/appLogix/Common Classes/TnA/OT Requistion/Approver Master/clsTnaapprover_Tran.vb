﻿'************************************************************************************************************************************
'Class Name :clsTnaapprover_Tran.vb
'Purpose    :
'Date       :05-Jul-2019
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsTnaapprover_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsTnaapprover_Tran"
    Dim mstrMessage As String = ""

#Region " Private Variables "

    Private mintTnAapprovertranunkid As Integer = 0
    Private mintTnamappingunkid As Integer = 0
    Private mdtTran As DataTable
    Private mdtEmployeeAsonDate As Date = Nothing
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mintLoginEmployeeunkid As Integer
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing
    Private mintUserunkid As Integer

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TnAapprovertranunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _TnAapprovertranunkid() As Integer
        Get
            Return mintTnAapprovertranunkid
        End Get
        Set(ByVal value As Integer)
            mintTnAapprovertranunkid = value
        End Set
    End Property


    'Pinkal (03-Mar-2020) -- Start
    'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.

    ''' <summary>
    ''' Purpose: Get or Set TnAMappingUnkId
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _TnAMappingUnkId(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintTnamappingunkid
        End Get
        Set(ByVal value As Integer)
            mintTnamappingunkid = value
            Call Get_Data(objDoOperation)
        End Set
    End Property

    'Pinkal (03-Mar-2020) -- End


    ''' <summary>
    ''' Purpose: Get or Set Datatable
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Date
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _EmployeeAsonDate() As Date
        Get
            Return mdtEmployeeAsonDate
        End Get
        Set(ByVal value As Date)
            mdtEmployeeAsonDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        mdtTran = New DataTable("List")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("ischeck")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("tnaapprovertranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("tnamappingunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("edept")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ejob")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Public & Private Methods "


    'Pinkal (03-Mar-2020) -- Start
    'ENHANCEMENT NMB:  Working on OT Requisition Approver Migration and transfer and recategorization migration enforcement screen.

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Sub Get_Data(Optional ByVal objDoOperation As clsDataOperation = Nothing, _
                        Optional ByVal xDatabaseName As String = "", _
                        Optional ByVal mblnIncludeInActiveEmployee As Boolean = True)


        'Gajanan [23-May-2020] -- ADD [xDatabaseName,mblnIncludeInActiveEmployee]
        'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.


        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objData As clsDataOperation
        Try

            If objDoOperation Is Nothing Then
                objData = New clsDataOperation
            Else
                objData = objDoOperation
            End If


            'Gajanan [23-May-2020] -- Start
            'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            If mblnIncludeInActiveEmployee = False Then
                xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtEmployeeAsonDate, mdtEmployeeAsonDate, , , xDatabaseName)
                Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtEmployeeAsonDate, xDatabaseName)
            End If
            'Gajanan [23-May-2020] -- End

            objData.ClearParameters()

            StrQ = "SELECT " & _
                   "	 CAST(0 AS BIT) AS ischeck " & _
                   "	,tnaapprover_tran.tnaapprovertranunkid " & _
                   "	,tnaapprover_tran.tnamappingunkid " & _
                   "	,tnaapprover_tran.employeeunkid " & _
                   "	,tnaapprover_tran.userunkid " & _
                   "	,tnaapprover_tran.isvoid " & _
                   "	,tnaapprover_tran.voiddatetime " & _
                   "	,tnaapprover_tran.voidreason " & _
                   "	,tnaapprover_tran.voiduserunkid " & _
                   "	,'' AS AUD " & _
                   "	, ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.surname,'')  AS ename " & _
                   "	,name AS edept " & _
                   "	,job_name AS ejob " & _
                   " FROM tnaapprover_tran " & _
                   "	JOIN hremployee_master ON tnaapprover_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   "    LEFT JOIN " & _
                    "   ( " & _
                    "        SELECT " & _
                    "             jobunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmployeeAsonDate" & _
                    " ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                    "	JOIN hrjob_master ON  hrjob_master.jobunkid = Jobs.jobunkid " & _
                    "   LEFT JOIN " & _
                    "   ( " & _
                    "        SELECT " & _
                    "         departmentunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "    FROM hremployee_transfer_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmployeeAsonDate" & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "	JOIN hrdepartment_master ON  hrdepartment_master.departmentunkid = Alloc.departmentunkid " & _
                   "	JOIN tnaapprover_master ON tnaapprover_tran.tnamappingunkid = tnaapprover_master.tnamappingunkid "


            If mblnIncludeInActiveEmployee = False Then
                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If
            End If


            StrQ &= "WHERE tnaapprover_master.tnamappingunkid = @tnamappingunkid AND tnaapprover_tran.isvoid = 0 "

            If mblnIncludeInActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If
            End If



            objData.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnamappingunkid)
            objData.AddParameter("@EmployeeAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsonDate))
            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                exForce = New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()

            For Each dRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(dRow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Data; Module Name: " & mstrModuleName)
        Finally
            If objDoOperation Is Nothing Then objData = Nothing
        End Try
    End Sub

    'Pinkal (03-Mar-2020) -- End

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function Insert_Update_Delete(ByVal objData As clsDataOperation) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objData.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO tnaapprover_tran ( " & _
                                           "  tnamappingunkid " & _
                                           ", employeeunkid " & _
                                           ", userunkid " & _
                                           ", isvoid " & _
                                           ", voiddatetime " & _
                                           ", voidreason " & _
                                           ", voiduserunkid " & _
                                       ") VALUES (" & _
                                           "  @tnamappingunkid " & _
                                           ", @employeeunkid " & _
                                           ", @userunkid " & _
                                           ", @isvoid " & _
                                           ", @voiddatetime " & _
                                           ", @voidreason " & _
                                           ", @voiduserunkid " & _
                                       "); SELECT @@identity "

                                objData.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnamappingunkid)
                                objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                                objData.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid"))
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))


                                dsList = objData.ExecQuery(StrQ, "List")

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                mintTnAapprovertranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If InsertAuditTrailApproverTran(objData, 1, CInt(.Item("employeeunkid"))) = False Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "U"

                                StrQ = " UPDATE tnaapprover_tran SET " & _
                                           "  tnamappingunkid = @tnamappingunkid" & _
                                           ", employeeunkid = @employeeunkid" & _
                                           ", userunkid = @userunkid" & _
                                           ", isvoid = @isvoid" & _
                                           ", voiddatetime = @voiddatetime" & _
                                           ", voidreason = @voidreason" & _
                                           ", voiduserunkid = @voiduserunkid " & _
                                           " WHERE tnaapprovertranunkid = @tnaapprovertranunkid "

                                objData.AddParameter("@tnaapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tnaapprovertranunkid"))
                                objData.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnamappingunkid)
                                objData.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                                objData.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid"))
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))

                                Call objData.ExecNonQuery(StrQ)

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                mintTnAapprovertranunkid = CInt(.Item("tnaapprovertranunkid"))

                                If InsertAuditTrailApproverTran(objData, 2, CInt(.Item("employeeunkid"))) = False Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"


                                StrQ = " UPDATE tnaapprover_tran SET " & _
                                           "  isvoid = @isvoid " & _
                                           ", voiddatetime = @voiddatetime " & _
                                           ", voidreason = @voidreason " & _
                                           ", voiduserunkid = @voiduserunkid " & _
                                           " WHERE tnaapprovertranunkid = @tnaapprovertranunkid "

                                objData.AddParameter("@tnaapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tnaapprovertranunkid"))
                                objData.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objData.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objData.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objData.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))

                                Call objData.ExecNonQuery(StrQ)

                                If objData.ErrorMessage <> "" Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                                mintTnAapprovertranunkid = CInt(.Item("tnaapprovertranunkid"))

                                If InsertAuditTrailApproverTran(objData, 3, CInt(.Item("employeeunkid"))) = False Then
                                    exForce = New Exception(objData.ErrorNumber & ": " & objData.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_Update_Delete; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function InsertAuditTrailApproverTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal xEmployeeId As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO attnaapprover_tran ( " & _
                      "  tnaapprovertranunkid " & _
                      ", tnamappingunkid " & _
                      ", employeeunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip" & _
                      ", hostname" & _
                      ", form_name " & _
                      ", isweb " & _
                      " ) VALUES (" & _
                      "  @tnaapprovertranunkid " & _
                      ", @tnamappingunkid " & _
                      ", @employeeunkid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip" & _
                      ", @hostname" & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tnaapprovertranunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mintTnAapprovertranunkid)
            objDataOperation.AddParameter("@tnamappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTnamappingunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailApproverTran; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function IsUserMapped(ByVal iUserId As Integer, ByRef sMsg As String) As Boolean
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Dim objData As New clsDataOperation

            StrQ = "SELECT " & _
                   "	 employeecode AS ECode " & _
                   "	,firstname+' '+surname AS EName " & _
                   "FROM hrapprover_usermapping " & _
                   "	JOIN cmexpapprover_master ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid " & _
                   "	JOIN hremployee_master ON cmexpapprover_master.employeeunkid = hremployee_master.employeeunkid " & _
                   "WHERE usertypeid = '" & enUserType.crApprover & "' AND hrapprover_usermapping.userunkid = '" & iUserId & "' "

            dsList = objData.ExecQuery(StrQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                sMsg = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot map selected user with this employee." & vbCrLf & _
                                           "Reason user is already mapped with [ Code : ") & dsList.Tables(0).Rows(0).Item("ECode") & _
                Language.getMessage(mstrModuleName, 2, " Employee : ") & dsList.Tables(0).Rows(0).Item("EName") & Language.getMessage(mstrModuleName, 3, " ]. Please select new user to map.")
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsUserMapped; Module Name: " & mstrModuleName)
            Return True
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    'Public Function Perform_Migration(ByVal iFromApproverUnkid As Integer, ByVal iToApproverUnkid As Integer, ByVal iDataTable As DataTable, ByVal iUserId As Integer, ByVal iToApproverEmpID As Integer, ByVal dtCurrentDateAndTime As DateTime) As Boolean
    '    'Shani(24-Aug-2015) -- End

    '    Dim exForce As Exception
    '    Dim iblnVoidMaster As Boolean = False
    '    Dim objDataOperation As New clsDataOperation
    '    Dim iMigratedEmpId As String = String.Empty
    '    Dim strQ As String = ""
    '    Try
    '        _ExpApproverMasterId = iFromApproverUnkid
    '        If mdtTran.Rows.Count = iDataTable.Rows.Count Then iblnVoidMaster = True
    '        objDataOperation.BindTransaction()
    '        For Each dRow As DataRow In iDataTable.Rows
    '            mdtTran.Rows.Clear()
    '            With dRow
    '                .Item("AUD") = "D"

    '                'Shani(24-Aug-2015) -- Start
    '                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '                '.Item("voiddatetime") = ConfigParameter._Object._CurrentDateAndTime
    '                .Item("voiddatetime") = dtCurrentDateAndTime
    '                'Shani(24-Aug-2015) -- End

    '                .Item("isvoid") = True
    '                .Item("voidreason") = Language.getMessage(mstrModuleName, 4, "Migrated to other Approver.")
    '                .Item("voiduserunkid") = iUserId
    '                .Item("crapprovertranunkid") = dRow.Item("crapprovertranunkid")
    '                .Item("crapproverunkid") = iFromApproverUnkid
    '            End With
    '            mdtTran.ImportRow(dRow)

    '            mintExpApproverMasterId = iFromApproverUnkid

    '            If Insert_Update_Delete(objDataOperation) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            mdtTran.Rows.Clear()
    '            With dRow
    '                .Item("AUD") = "A"
    '                .Item("voiddatetime") = DBNull.Value
    '                .Item("isvoid") = False
    '                .Item("voidreason") = ""
    '                .Item("voiduserunkid") = 0
    '                .Item("crapproverunkid") = iToApproverUnkid
    '            End With
    '            mdtTran.ImportRow(dRow)

    '            mintExpApproverMasterId = iToApproverUnkid

    '            If Insert_Update_Delete(objDataOperation) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If


    '            'Pinkal (19-Mar-2015) -- Start
    '            'Enhancement - IMPLEMENTING LEAVE APPROVER SWAPING.

    '            Dim objClaim As New clsclaim_request_master
    '            Dim mstrFormId As String = objClaim.GetApproverPendingClaimForm(iFromApproverUnkid, dRow.Item("employeeunkid").ToString(), objDataOperation)

    '            If mstrFormId.Trim.Length > 0 Then
    '                strQ = " Update cmclaim_approval_tran set approveremployeeunkid  = @newapproveremployeeunkid,crapproverunkid = @newcrapproverunkid  WHERE crmasterunkid in (" & mstrFormId & ") AND crapproverunkid = @oldcrapproverunkid AND cmclaim_approval_tran.isvoid = 0 "
    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@newapproveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iToApproverEmpID)
    '                objDataOperation.AddParameter("@newcrapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iToApproverUnkid)
    '                objDataOperation.AddParameter("@oldcrapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iFromApproverUnkid)
    '                objDataOperation.ExecNonQuery(strQ)

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If


    '                strQ = "Select ISNULL(crapprovaltranunkid,0) crapprovaltranunkid FROM cmclaim_approval_tran WHERE crapproverunkid = @crapproverunkid AND approveremployeeunkid = @approveremployeeunkid AND crmasterunkid  in (" & mstrFormId & ") AND isvoid = 0"
    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iToApproverUnkid)
    '                objDataOperation.AddParameter("@approveremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iToApproverEmpID)
    '                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If


    '                If dsList.Tables(0).Rows.Count > 0 Then

    '                    For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
    '                        If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "cmclaim_approval_tran", "crapprovaltranunkid", dsList.Tables(0).Rows(k)("crapprovaltranunkid").ToString(), False) = False Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If
    '                    Next

    '                End If

    '            End If

    '            'Pinkal (19-Mar-2015) -- End


    '            iMigratedEmpId &= "," & dRow.Item("employeeunkid")
    '        Next


    '        'Pinkal (06-Jan-2016) -- Start
    '        'Enhancement - Working on Changes in SS for Leave Module.
    '        strQ = " SELECT count(*) as Empcount FROM cmexpapprover_tran WHERE crapproverunkid = @crapproverunkid AND isvoid = 0  "
    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@crapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iFromApproverUnkid)
    '        Dim dsCount As DataSet = objDataOperation.ExecQuery(strQ, "List")
    '        If dsCount IsNot Nothing AndAlso dsCount.Tables(0).Rows.Count > 0 Then
    '            If dsCount.Tables(0).Rows(0)("Empcount") <= 0 Then
    '                iblnVoidMaster = True
    '            End If
    '        Else
    '            iblnVoidMaster = True
    '        End If
    '        'Pinkal (06-Jan-2016) -- End


    '        If iblnVoidMaster = True Then
    '            Dim objExApprMaster As New clsExpenseApprover_Master
    '            objExApprMaster._Isvoid = True

    '            'Shani(24-Aug-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'objExApprMaster._Voiddatetime = ConfigParameter._Object._CurrentDateAndTime
    '            objExApprMaster._Voiddatetime = dtCurrentDateAndTime
    '            'Shani(24-Aug-2015) -- End

    '            objExApprMaster._Voidreason = Language.getMessage(mstrModuleName, 5, "Employee Migrated")
    '            objExApprMaster._Voiduserunkid = iUserId
    '            If objExApprMaster.Delete(iFromApproverUnkid, objDataOperation) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If
    '            objExApprMaster = Nothing
    '        End If

    '        If iMigratedEmpId.Trim.Length > 0 Then
    '            iMigratedEmpId = Mid(iMigratedEmpId, 2)
    '            Dim objMigration As New clsMigration
    '            objMigration._Migratedemployees = iMigratedEmpId

    '            'Shani(24-Aug-2015) -- Start
    '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '            'objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
    '            objMigration._Migrationdate = dtCurrentDateAndTime
    '            'Shani(24-Aug-2015) -- End

    '            objMigration._Migrationfromunkid = iFromApproverUnkid
    '            objMigration._Migrationtounkid = iToApproverUnkid
    '            objMigration._Usertypeid = enUserType.crApprover
    '            objMigration._Userunkid = iUserId

    '            If objMigration.Insert(objDataOperation) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            objMigration = Nothing

    '        End If
    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        DisplayError.Show("-1", ex.Message, "Perform_Migration", mstrModuleName)
    '    Finally
    '    End Try
    'End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, you cannot map selected user with this employee." & vbCrLf & _
                                                    "Reason user is already mapped with [ Code :")
            Language.setMessage(mstrModuleName, 2, " Employee :")
            Language.setMessage(mstrModuleName, 3, " ]. Please select new user to map.")
            Language.setMessage(mstrModuleName, 4, "Migrated to other Approver.")
            Language.setMessage(mstrModuleName, 5, "Employee Migrated")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
