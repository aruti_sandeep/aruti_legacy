﻿'************************************************************************************************************************************
'Class Name : clstnaotrequisition_process_Tran.vb
'Purpose    :
'Date       :31-Jan-2020
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clstnaotrequisition_process_Tran
    Private Const mstrModuleName = "clstnaotrequisition_process_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintOtrequisitionprocesstranunkid As Integer
    Private mintOtrequestapprovaltranunkid As Integer
    Private mintOtrequisitiontranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtRequestdate As Date
    Private mintPeriodunkid As Integer
    Private mdtActualstart_Time As Date
    Private mdtActualend_Time As Date
    Private mintActualot_Hours As Integer
    Private mintActualnight_Hrs As Integer
    Private mintFinalmappedapproverunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mdtVoiddatetime As Date
    Private mblnIsposted As Boolean
    Private mblnIsprocessed As Boolean
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientID As String = ""
    Private mstrWebhostName As String = ""
    Private mblnIsWeb As Boolean
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otrequisitionprocesstranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Otrequisitionprocesstranunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintOtrequisitionprocesstranunkid
        End Get
        Set(ByVal value As Integer)
            mintOtrequisitionprocesstranunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otrequestapprovaltranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Otrequestapprovaltranunkid() As Integer
        Get
            Return mintOtrequestapprovaltranunkid
        End Get
        Set(ByVal value As Integer)
            mintOtrequestapprovaltranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otrequisitiontranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Otrequisitiontranunkid() As Integer
        Get
            Return mintOtrequisitiontranunkid
        End Get
        Set(ByVal value As Integer)
            mintOtrequisitiontranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set requestdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Requestdate() As Date
        Get
            Return mdtRequestdate
        End Get
        Set(ByVal value As Date)
            mdtRequestdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actualstart_time
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Actualstart_Time() As Date
        Get
            Return mdtActualstart_Time
        End Get
        Set(ByVal value As Date)
            mdtActualstart_Time = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actualend_time
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Actualend_Time() As Date
        Get
            Return mdtActualend_Time
        End Get
        Set(ByVal value As Date)
            mdtActualend_Time = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actualot_hours
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Actualot_Hours() As Integer
        Get
            Return mintActualot_Hours
        End Get
        Set(ByVal value As Integer)
            mintActualot_Hours = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set actualnight_hrs
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Actualnight_Hrs() As Integer
        Get
            Return mintActualnight_Hrs
        End Get
        Set(ByVal value As Integer)
            mintActualnight_Hrs = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set finalmappedapproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Finalmappedapproverunkid() As Integer
        Get
            Return mintFinalmappedapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintFinalmappedapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isposted
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isposted() As Boolean
        Get
            Return mblnIsposted
        End Get
        Set(ByVal value As Boolean)
            mblnIsposted = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isprocessed
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isprocessed() As Boolean
        Get
            Return mblnIsprocessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsprocessed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Webhostname
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _Isweb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If


        Try
            strQ = "SELECT " & _
                      "  otrequisitionprocesstranunkid " & _
                      ", otrequestapprovaltranunkid " & _
                      ", otrequisitiontranunkid " & _
                      ", employeeunkid " & _
                      ", requestdate " & _
                      ", periodunkid " & _
                      ", actualstart_time " & _
                      ", actualend_time " & _
                      ", actualot_hours " & _
                      ", actualnight_hrs " & _
                      ", finalmappedapproverunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", voiddatetime " & _
                      ", isposted " & _
                      ", isprocessed " & _
                      " FROM tnaotrequisition_process_tran " & _
                      " WHERE otrequisitionprocesstranunkid = @otrequisitionprocesstranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@otrequisitionprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitionprocesstranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintOtrequisitionprocesstranunkid = CInt(dtRow.Item("otrequisitionprocesstranunkid"))
                mintOtrequestapprovaltranunkid = CInt(dtRow.Item("otrequestapprovaltranunkid"))
                mintOtrequisitiontranunkid = CInt(dtRow.Item("otrequisitiontranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtRequestdate = dtRow.Item("requestdate")
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mdtActualstart_Time = dtRow.Item("actualstart_time")
                mdtActualend_Time = dtRow.Item("actualend_time")
                mintActualot_Hours = CInt(dtRow.Item("actualot_hours"))
                mintActualnight_Hrs = CInt(dtRow.Item("actualnight_hrs"))
                mintFinalmappedapproverunkid = CInt(dtRow.Item("finalmappedapproverunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString

                If mdtVoiddatetime <> Nothing Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mblnIsposted = CBool(dtRow.Item("isposted"))
                mblnIsprocessed = CBool(dtRow.Item("isprocessed"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    'Pinkal (16-Jul-2020) -- Start
    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True _
                                  , Optional ByVal intIsposted As Integer = -1, Optional ByVal mstrFilter As String = "", Optional ByVal mblnAddGrp As Boolean = True _
                                  , Optional ByVal objDoOperation As clsDataOperation = Nothing) As DataSet

        'Pinkal (12-Oct-2021)-- NMB OT Requisition Performance Issue.[Optional ByVal objDoOperation As clsDataOperation = Nothing]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2021)-- Start
        'NMB OT Requisition Performance Issue.
        If objDoOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        'Pinkal (12-Oct-2021)-- End

        Try

            If mblnAddGrp Then

                strQ &= " SELECT " & _
                              "  CAST(1 AS BIT) As Isgroup " & _
                              ", CAST(0 AS BIT) As Ischecked " & _
                              ", CAST(0 AS BIT) As ischange " & _
                              ",  0 As otrequisitionprocesstranunkid " & _
                              ",  0 As otrequestapprovaltranunkid " & _
                              ", tnaotrequisition_process_tran.otrequisitiontranunkid " & _
                              ", tnaotrequisition_process_tran.employeeunkid " & _
                              ", ISNULL(hremployee_master.employeecode ,'') AS EmployeeCode" & _
                              ", ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'')  + '  ' +  ISNULL(hremployee_master.surname,'') AS Employee " & _
                               " , ' [ '  + @RequestDate + ' : '  + REPLACE(RTRIM(CONVERT(char(15),tnaot_requisition_tran.requestdate,106)),' ','-') + ' ]' + ' : ' + ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Particulars " & _
                              ", tnaot_requisition_tran.requestdate " & _
                              ", CONVERT(Char(8),tnaot_requisition_tran.requestdate,112) AS RDate " & _
                              ", 0 AS periodunkid " & _
                              ", '' AS Period " & _
                              ", NULL AS actualstart_time " & _
                              ", NULL AS actualend_time " & _
                              ", 0 AS actualot_hoursinsec " & _
                              " ,'' AS actualot_hours " & _
                              ", 0 AS actualnight_hrsinsec " & _
                              ", 0 AS finalmappedapproverunkid " & _
                              ", 0 AS userunkid " & _
                              ", CAST(0 AS BIT) AS isvoid " & _
                              ", 0 AS voiduserunkid " & _
                              ", '' AS voidreason " & _
                              ", NULL AS voiddatetime " & _
                              ",  CAST(0 AS BIT) AS isposted " & _
                              ",  CAST(0 AS BIT) AS isprocessed " & _
                              " FROM tnaotrequisition_process_tran " & _
                              " JOIN tnaot_requisition_tran ON tnaot_requisition_tran.otrequisitiontranunkid = tnaotrequisition_process_tran.otrequisitiontranunkid AND tnaot_requisition_tran.isvoid = 0 " & _
                              " JOIN hremployee_master ON hremployee_master.employeeunkid = tnaotrequisition_process_tran.employeeunkid " & _
                              " WHERE tnaotrequisition_process_tran.isprocessed = 0 "

                If blnOnlyActive Then
                    strQ &= " AND tnaotrequisition_process_tran.isvoid  = 0  "
                End If

                If intIsposted = 0 OrElse intIsposted = 1 Then
                    strQ &= " AND tnaotrequisition_process_tran.isposted = @isposted"
                End If

                If mstrFilter.Trim.Length > 0 Then
                    strQ &= " AND " & mstrFilter
                End If

                strQ &= "UNION "


            End If


            strQ &= " SELECT " & _
                      "  CAST(0 AS BIT) As Isgroup " & _
                      ", CAST(0 AS BIT) As Ischecked " & _
                      ", CAST(0 AS BIT) As ischange " & _
                      ",  tnaotrequisition_process_tran.otrequisitionprocesstranunkid " & _
                      ", tnaotrequisition_process_tran.otrequestapprovaltranunkid " & _
                      ", tnaotrequisition_process_tran.otrequisitiontranunkid " & _
                      ", tnaotrequisition_process_tran.employeeunkid " & _
                      ", ISNULL(hremployee_master.employeecode ,'') AS EmployeeCode" & _
                      ", ISNULL(hremployee_master.firstname,'') + '  ' + ISNULL(hremployee_master.othername,'')  + '  ' +  ISNULL(hremployee_master.surname,'') AS Employee " & _
                      ", '' AS Particulars " & _
                      ", tnaot_requisition_tran.requestdate " & _
                      ", CONVERT(Char(8),tnaot_requisition_tran.requestdate,112) AS RDate " & _
                      ", tnaotrequisition_process_tran.periodunkid " & _
                      ", ISNULL(cfcommon_period_tran.period_name,'') AS Period " & _
                      ", tnaotrequisition_process_tran.actualstart_time " & _
                      ", tnaotrequisition_process_tran.actualend_time " & _
                      ", tnaotrequisition_process_tran.actualot_hours AS actualot_hoursinsec " & _
                      " , ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),tnaotrequisition_process_tran.actualot_hours / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (tnaotrequisition_process_tran.actualot_hours  % 3600) / 60 ), 2), '00:00') AS actualot_hours " & _
                      ", tnaotrequisition_process_tran.actualnight_hrs AS actualnight_hrsinsec " & _
                      ", tnaotrequisition_process_tran.finalmappedapproverunkid " & _
                      ", tnaotrequisition_process_tran.userunkid " & _
                      ", tnaotrequisition_process_tran.isvoid " & _
                      ", tnaotrequisition_process_tran.voiduserunkid " & _
                      ", tnaotrequisition_process_tran.voidreason " & _
                      ", tnaotrequisition_process_tran.voiddatetime " & _
                      ", tnaotrequisition_process_tran.isposted " & _
                      ", tnaotrequisition_process_tran.isprocessed " & _
                      " FROM tnaotrequisition_process_tran " & _
                      " JOIN tnaot_requisition_tran ON tnaot_requisition_tran.otrequisitiontranunkid = tnaotrequisition_process_tran.otrequisitiontranunkid AND tnaot_requisition_tran.isvoid = 0 " & _
                      " JOIN hremployee_master ON hremployee_master.employeeunkid = tnaotrequisition_process_tran.employeeunkid " & _
                      " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid  = tnaotrequisition_process_tran.periodunkid " & _
                      " WHERE tnaotrequisition_process_tran.isprocessed = 0 "

            If blnOnlyActive Then
                strQ &= " AND tnaotrequisition_process_tran.isvoid  = 0  "
            End If

            If intIsposted = 0 OrElse intIsposted = 1 Then
                strQ &= " AND tnaotrequisition_process_tran.isposted = @isposted"
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            'strQ &= " ORDER BY  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,''),RDate DESC "
            strQ &= " ORDER BY Employee,RDate DESC,Isgroup DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isposted", SqlDbType.Int, eZeeDataType.INT_SIZE, intIsposted)
            objDataOperation.AddParameter("@RequestDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsTnaotrequisition_approval_Tran", 1, "Request Date"))
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (12-Oct-2021)-- Start
            'NMB OT Requisition Performance Issue.
            If objDoOperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (12-Oct-2021)-- End
        End Try
        Return dsList
    End Function

    'Pinkal (16-Jul-2020) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnaotrequisition_process_tran) </purpose>
    Public Function Insert(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@otrequestapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequestapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitiontranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@requestdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRequestdate.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualstart_Time.ToString)
            objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualend_Time.ToString)
            objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualot_Hours.ToString)
            objDataOperation.AddParameter("@actualnight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualnight_Hrs.ToString)
            objDataOperation.AddParameter("@finalmappedapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalmappedapproverunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)

            strQ = "INSERT INTO tnaotrequisition_process_tran ( " & _
                      "  otrequestapprovaltranunkid " & _
                      ", otrequisitiontranunkid " & _
                      ", employeeunkid " & _
                      ", requestdate " & _
                      ", periodunkid " & _
                      ", actualstart_time " & _
                      ", actualend_time " & _
                      ", actualot_hours " & _
                      ", actualnight_hrs " & _
                      ", finalmappedapproverunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", voiddatetime " & _
                      ", isposted " & _
                      ", isprocessed" & _
                    ") VALUES (" & _
                      "  @otrequestapprovaltranunkid " & _
                      ", @otrequisitiontranunkid " & _
                      ", @employeeunkid " & _
                      ", @requestdate " & _
                      ", @periodunkid " & _
                      ", @actualstart_time " & _
                      ", @actualend_time " & _
                      ", @actualot_hours " & _
                      ", @actualnight_hrs " & _
                      ", @finalmappedapproverunkid " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voidreason " & _
                      ", @voiddatetime " & _
                      ", @isposted " & _
                      ", @isprocessed" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintOtrequisitionprocesstranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAudiTrailForOTRequisitionProcessTran(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tnaotrequisition_process_tran) </purpose>
    Public Function Update(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@otrequisitionprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitionprocesstranunkid.ToString)
            objDataOperation.AddParameter("@otrequestapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequestapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitiontranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@requestdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRequestdate.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualstart_Time.ToString)
            objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualend_Time.ToString)
            objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualot_Hours.ToString)
            objDataOperation.AddParameter("@actualnight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualnight_Hrs.ToString)
            objDataOperation.AddParameter("@finalmappedapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalmappedapproverunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)

            strQ = "UPDATE tnaotrequisition_process_tran SET " & _
                      "  otrequestapprovaltranunkid = @otrequestapprovaltranunkid" & _
                      ", otrequisitiontranunkid = @otrequisitiontranunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", requestdate = @requestdate" & _
                      ", periodunkid = @periodunkid" & _
                      ", actualstart_time = @actualstart_time" & _
                      ", actualend_time = @actualend_time" & _
                      ", actualot_hours = @actualot_hours" & _
                      ", actualnight_hrs = @actualnight_hrs" & _
                      ", finalmappedapproverunkid = @finalmappedapproverunkid" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", isposted = @isposted" & _
                      ", isprocessed = @isprocessed " & _
                      " WHERE otrequisitionprocesstranunkid = @otrequisitionprocesstranunkid AND isvoid = 0 "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If InsertAudiTrailForOTRequisitionProcessTran(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (tnaotrequisition_process_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "DELETE FROM tnaotrequisition_process_tran " & _
            "WHERE otrequisitionprocesstranunkid = @otrequisitionprocesstranunkid "

            objDataOperation.AddParameter("@otrequisitionprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@otrequisitionprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  otrequisitionprocesstranunkid " & _
              ", otrequestapprovaltranunkid " & _
              ", otrequisitiontranunkid " & _
              ", employeeunkid " & _
              ", requestdate " & _
              ", periodunkid " & _
              ", actualstart_time " & _
              ", actualend_time " & _
              ", actualot_hours " & _
              ", actualnight_hrs " & _
              ", finalmappedapproverunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", voiddatetime " & _
              ", isposted " & _
              ", isprocessed " & _
             "FROM tnaotrequisition_process_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND otrequisitionprocesstranunkid <> @otrequisitionprocesstranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@otrequisitionprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Pinkal (16-Jul-2020) -- Start
    'ENHANCEMENT NMB:  Working on Employee Approver Calibrator Report

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    'Public Function Posting_Unposting_OTRequisition(ByVal blnIsPosted As Boolean, ByVal dtTable As DataTable) As Boolean

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try
    '        Dim objDataOperation As New clsDataOperation
    '        objDataOperation.BindTransaction()


    '        strQ = " UPDATE tnaotrequisition_process_tran SET periodunkid = @periodunkid,isposted = @isposted " & _
    '                  " WHERE employeeunkid  = @employeeunkid AND otrequisitionprocesstranunkid = @otrequisitionprocesstranunkid AND isvoid = 0 "

    '        For Each dr As DataRow In dtTable.Rows
    '            If CBool(dr("ischange")) = False Then Continue For
    '            objDataOperation.ClearParameters()
    '            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("periodunkid"))
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("employeeunkid"))
    '            objDataOperation.AddParameter("@otrequisitionprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dr("otrequisitionprocesstranunkid"))
    '            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsPosted)
    '            objDataOperation.ExecNonQuery(strQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If


    '            'Pinkal (29-Apr-2020) -- Start
    '            'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.
    '            Dim xUserId As Integer = mintUserunkid
    '            'Hemant (29-Apr-2020) -- End

    '            _Otrequisitionprocesstranunkid(objDataOperation) = CInt(dr("otrequisitionprocesstranunkid"))

    '            'Pinkal (29-Apr-2020) -- Start
    '            'ISSUE/ENHANCEMENT : {Audit Trails} in 77.1.                
    '            mintUserunkid = xUserId
    '            'Pinkal (29-Apr-2020) -- End

    '            If InsertAudiTrailForOTRequisitionProcessTran(objDataOperation, 2) = False Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '        Next

    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Posting_Unposting_OTRequisition; Module Name: " & mstrModuleName)
    '        Return False
    '    End Try
    'End Function


    'Pinkal (03-Sep-2020) -- Start
    'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
    'Public Function Posting_Unposting_OTRequisition(ByVal blnIsPosted As Boolean, ByVal xPeriodId As Integer, ByVal strOtRequisitionProcessIds As String) As Boolean
    Public Function Posting_Unposting_OTRequisition(ByVal blnIsPosted As Boolean, ByVal xPeriodId As Integer, ByVal arrListProcessIds As ArrayList) As Boolean
        'Pinkal (03-Sep-2020) -- End
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            objDataOperation.BindTransaction()


            'Pinkal (03-Sep-2020) -- Start
            'Optimzation OT NMB:  Working on Optimzing Post To Payroll.
            'If strOtRequisitionProcessIds.Trim.Length > 0 Then
            If arrListProcessIds IsNot Nothing AndAlso arrListProcessIds.Count > 0 Then

                For i As Integer = 0 To arrListProcessIds.Count - 1

                    Dim strOtRequisitionProcessIds As String = arrListProcessIds(i)

                    If strOtRequisitionProcessIds.Trim.Length <= 0 Then Continue For

                strQ = "IF OBJECT_ID(N'tempdb..#OtRequisitionProcessIds') IS NOT NULL " & _
                         " DROP TABLE #OtRequisitionProcessIds " & _
                         " CREATE TABLE #OtRequisitionProcessIds (otrequisitionprocessunkid int default(0)) " & _
                         " DECLARE @DATA NVARCHAR(MAX) = '" & strOtRequisitionProcessIds & "' " & _
                         " DECLARE @CNT INT " & _
                         " DECLARE @SEP AS CHAR(1) " & _
                         " SET @SEP = ',' " & _
                         " SET @CNT = 1 " & _
                         " WHILE (CHARINDEX(@SEP,@DATA)>0) " & _
                         "    BEGIN " & _
                         "        INSERT INTO #OtRequisitionProcessIds (otrequisitionprocessunkid) " & _
                         "        SELECT DATA = LTRIM(RTRIM(SUBSTRING(@DATA,1,CHARINDEX(@SEP,@DATA)-1))) " & _
                         "        SET @DATA = SUBSTRING(@DATA,CHARINDEX(@SEP,@DATA)+1,LEN(@DATA)) " & _
                         "        SET @CNT = @CNT + 1 " & _
                         "    END " & _
                         " INSERT INTO #OtRequisitionProcessIds (otrequisitionprocessunkid) SELECT DATA = LTRIM(RTRIM(@DATA)) " & _
                         " UPDATE tnaotrequisition_process_tran  SET " & _
                         " periodunkid = @periodunkid,isposted = @isposted " & _
                         " FROM " & _
                         "( " & _
                         "    SELECT " & _
                         "        #OtRequisitionProcessIds.otrequisitionprocessunkid " & _
                         "    FROM #OtRequisitionProcessIds " & _
                         "    JOIN tnaotrequisition_process_tran AS OPT ON #OtRequisitionProcessIds.otrequisitionprocessunkid = OPT.otrequisitionprocesstranunkid " & _
                         "    WHERE OPT.isvoid = 0 " & _
                         " ) AS OT WHERE OT.otrequisitionprocessunkid = tnaotrequisition_process_tran.otrequisitionprocesstranunkid " & _
                         " INSERT INTO attnaotrequisition_process_tran (otrequisitionprocesstranunkid,otrequestapprovaltranunkid,otrequisitiontranunkid,employeeunkid,requestdate,periodunkid,actualstart_time " & _
                         " ,actualend_time,actualot_hours,actualnight_hrs,finalmappedapproverunkid,isposted,isprocessed,audittype,audituserunkid,auditdatetime,ip,hostname,form_name,isweb) " & _
                         " SELECT otrequisitionprocesstranunkid,otrequestapprovaltranunkid,otrequisitiontranunkid,employeeunkid,requestdate,periodunkid,actualstart_time " & _
                         " ,actualend_time,actualot_hours,actualnight_hrs,finalmappedapproverunkid,isposted,isprocessed,@audittype,@audituserunkid,Getdate(),@ip,@hostname,@form_name,@isweb " & _
                         " FROM #OtRequisitionProcessIds " & _
                         " JOIN tnaotrequisition_process_tran ON #OtRequisitionProcessIds.otrequisitionprocessunkid = tnaotrequisition_process_tran.otrequisitionprocesstranunkid " & _
                         " WHERE tnaotrequisition_process_tran.isvoid = 0 " & _
                         " " & _
                         " IF OBJECT_ID(N'tempdb..#OtRequisitionProcessIds') IS NOT NULL " & _
                         " DROP TABLE #OtRequisitionProcessIds "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodId)
                objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsPosted)
                objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, 2)
                objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebClientID)
                objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Next

                'Pinkal (03-Sep-2020) -- End

                End If
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Posting_Unposting_OTRequisition; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    'Pinkal (16-Jul-2020) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (tnaotrequisition_process_tran) </purpose>
    Public Function InsertAudiTrailForOTRequisitionProcessTran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@otrequisitionprocesstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitionprocesstranunkid.ToString)
            objDataOperation.AddParameter("@otrequestapprovaltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequestapprovaltranunkid.ToString)
            objDataOperation.AddParameter("@otrequisitiontranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtrequisitiontranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@requestdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtRequestdate.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@actualstart_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualstart_Time.ToString)
            objDataOperation.AddParameter("@actualend_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActualend_Time.ToString)
            objDataOperation.AddParameter("@actualot_hours", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualot_Hours.ToString)
            objDataOperation.AddParameter("@actualnight_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActualnight_Hrs.ToString)
            objDataOperation.AddParameter("@finalmappedapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalmappedapproverunkid.ToString)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsprocessed.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebClientID)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO attnaotrequisition_process_tran ( " & _
                      "  otrequisitionprocesstranunkid " & _
                      ", otrequestapprovaltranunkid " & _
                      ", otrequisitiontranunkid " & _
                      ", employeeunkid " & _
                      ", requestdate " & _
                      ", periodunkid " & _
                      ", actualstart_time " & _
                      ", actualend_time " & _
                      ", actualot_hours " & _
                      ", actualnight_hrs " & _
                      ", finalmappedapproverunkid " & _
                      ", isposted " & _
                      ", isprocessed" & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", hostname " & _
                      ", form_name " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      "  @otrequisitionprocesstranunkid " & _
                      ", @otrequestapprovaltranunkid " & _
                      ", @otrequisitiontranunkid " & _
                      ", @employeeunkid " & _
                      ", @requestdate " & _
                      ", @periodunkid " & _
                      ", @actualstart_time " & _
                      ", @actualend_time " & _
                      ", @actualot_hours " & _
                      ", @actualnight_hrs " & _
                      ", @finalmappedapproverunkid " & _
                      ", @isposted " & _
                      ", @isprocessed" & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", Getdate() " & _
                      ", @ip " & _
                      ", @hostname " & _
                      ", @form_name " & _
                      ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintOtrequisitionprocesstranunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAudiTrailForOTRequisitionProcessTran; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

End Class