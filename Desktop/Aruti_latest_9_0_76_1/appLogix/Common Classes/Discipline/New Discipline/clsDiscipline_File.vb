﻿'************************************************************************************************************************************
'Class Name : clsDiscipline_File.vb
'Purpose    :
'Date       :28/11/2011
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsDiscipline_File
    Private Shared ReadOnly mstrModuleName As String = "clsDiscipline_File"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintDisciplinefileunkid As Integer
    Private mdtTrandate As Date
    Private mintDisciplinetypeunkid As Integer
    Private mintInvolved_Employeeunkid As Integer
    Private mintAgainst_Employeeunkid As Integer
    Private mstrIncident As String = String.Empty
    Private mstrRemark As String = String.Empty
    Private mblnIsapproved As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'S.SANDEEP [ 20 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
    Private mintDisciplinestatusunkid As Integer
    'S.SANDEEP [ 20 MARCH 2012 ] -- END

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsExternal As Boolean = False
    Private mblnIsFromResolution As Boolean = False
    Private mintEmailtranunkid As Integer = 0
    Private mdtEmaildate As Date = Nothing
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

    'S.SANDEEP [ 01 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA DISCIPLINE CHANGES
    Private mdtInterdictdate As Date
    Private mstrReference_No As String = String.Empty
    'S.SANDEEP [ 01 JUNE 2012 ] -- END

    'S.SANDEEP [ 16 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdtResponse_Date As Date
    Private mstrResponse_Remark As String = String.Empty
    'S.SANDEEP [ 16 JAN 2013 ] -- END
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinefileunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Disciplinefileunkid() As Integer
        Get
            Return mintDisciplinefileunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinefileunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trandate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Trandate() As Date
        Get
            Return mdtTrandate
        End Get
        Set(ByVal value As Date)
            mdtTrandate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinetypeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Disciplinetypeunkid() As Integer
        Get
            Return mintDisciplinetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinetypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set involved_employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Involved_Employeeunkid() As Integer
        Get
            Return mintInvolved_Employeeunkid
        End Get
        Set(ByVal value As Integer)
            mintInvolved_Employeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set against_employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Against_Employeeunkid() As Integer
        Get
            Return mintAgainst_Employeeunkid
        End Get
        Set(ByVal value As Integer)
            mintAgainst_Employeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set incident
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Incident() As String
        Get
            Return mstrIncident
        End Get
        Set(ByVal value As String)
            mstrIncident = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isapproved
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isapproved() As Boolean
        Get
            Return mblnIsapproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsapproved = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'S.SANDEEP [ 20 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
    ''' <summary>
    ''' Purpose: Get or Set disciplinestatusunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Disciplinestatusunkid() As Integer
        Get
            Return mintDisciplinestatusunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinestatusunkid = value
        End Set
    End Property
    'S.SANDEEP [ 20 MARCH 2012 ] -- END

    'S.SANDEEP [ 20 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _IsExternal() As Boolean
        Set(ByVal value As Boolean)
            mblnIsExternal = value
        End Set
    End Property

    Public WriteOnly Property _IsFromResolution() As Boolean
        Set(ByVal value As Boolean)
            mblnIsFromResolution = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set emailtranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Emailtranunkid() As Integer
        Set(ByVal value As Integer)
            mintEmailtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set emaildate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _Emaildate() As Date
        Set(ByVal value As Date)
            mdtEmaildate = value
        End Set
    End Property
    'S.SANDEEP [ 20 APRIL 2012 ] -- END

    'S.SANDEEP [ 01 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA DISCIPLINE CHANGES
    ''' <summary>
    ''' Purpose: Get or Set interdictdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Interdictdate() As Date
        Get
            Return mdtInterdictdate
        End Get
        Set(ByVal value As Date)
            mdtInterdictdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reference_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reference_No() As String
        Get
            Return mstrReference_No
        End Get
        Set(ByVal value As String)
            mstrReference_No = value
        End Set
    End Property
    'S.SANDEEP [ 01 JUNE 2012 ] -- END

    'S.SANDEEP [ 16 JAN 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set response_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Response_Date() As Date
        Get
            Return mdtResponse_Date
        End Get
        Set(ByVal value As Date)
            mdtResponse_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set response_remark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Response_Remark() As String
        Get
            Return mstrResponse_Remark
        End Get
        Set(ByVal value As String)
            mstrResponse_Remark = value
        End Set
    End Property
    'S.SANDEEP [ 16 JAN 2013 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  disciplinefileunkid " & _
              ", trandate " & _
              ", disciplinetypeunkid " & _
              ", involved_employeeunkid " & _
              ", against_employeeunkid " & _
              ", incident " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", disciplinestatusunkid " & _
              ", emailtranunkid " & _
              ", emaildate " & _
              ", interdictdate " & _
              ", reference_no " & _
              ", response_date " & _
              ", response_remark " & _
             "FROM hrdiscipline_file " & _
             "WHERE disciplinefileunkid = @disciplinefileunkid "

            'S.SANDEEP [ 20 MARCH 2012 disciplinestatusunkid ] -- START -- END
            'S.SANDEEP [ 20 APRIL 2012 emailtranunkid,emaildate] -- START -- END
            'S.SANDEEP [ 01 JUNE 2012  interdictdate,reference_no] -- START -- END
            'S.SANDEEP [ 16 JAN 2013  response_date,response_remark] -- START -- END




            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
                mdtTrandate = dtRow.Item("trandate")
                mintDisciplinetypeunkid = CInt(dtRow.Item("disciplinetypeunkid"))
                mintInvolved_Employeeunkid = CInt(dtRow.Item("involved_employeeunkid"))
                mintAgainst_Employeeunkid = CInt(dtRow.Item("against_employeeunkid"))
                mstrIncident = dtRow.Item("incident").ToString
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'S.SANDEEP [ 20 MARCH 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
                mintDisciplinestatusunkid = CInt(dtRow.Item("disciplinestatusunkid"))
                'S.SANDEEP [ 20 MARCH 2012 ] -- END

                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mintEmailtranunkid = CInt(dtRow.Item("emailtranunkid"))
                If IsDBNull(dtRow.Item("emaildate")) = False Then
                    mdtEmaildate = dtRow.Item("emaildate")
                Else
                    mdtEmaildate = Nothing
                End If
                'S.SANDEEP [ 20 APRIL 2012 ] -- END

                'S.SANDEEP [ 01 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA DISCIPLINE CHANGES
                If IsDBNull(dtRow.Item("interdictdate")) = True Then
                    mdtInterdictdate = Nothing
                Else
                    mdtInterdictdate = dtRow.Item("interdictdate")
                End If
                mstrReference_No = dtRow.Item("reference_no").ToString
                'S.SANDEEP [ 01 JUNE 2012 ] -- END

                'S.SANDEEP [ 16 JAN 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If IsDBNull(dtRow.Item("response_date")) = True Then
                    mdtResponse_Date = Nothing
                Else
                    mdtResponse_Date = dtRow.Item("response_date")
                End If
                mstrResponse_Remark = dtRow.Item("response_remark").ToString
                'S.SANDEEP [ 16 JAN 2013 ] -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal intEmpId As Integer = -1, _
                            Optional ByVal xFilterString As String = "") As DataSet 'Sohail (23 Apr 2012) - [strIncludeInactiveEmployee,strEmployeeAsOnDate,strUserAccessLevelFilterString]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        Try
            strQ = "SELECT " & _
                   "  hrdiscipline_file.disciplinefileunkid " & _
                   ", hrdiscipline_file.trandate " & _
                   ", hrdiscipline_file.disciplinetypeunkid " & _
                   ", hrdiscipline_file.involved_employeeunkid " & _
                   ", hrdiscipline_file.against_employeeunkid " & _
                   ", hrdiscipline_file.incident " & _
                   ", hrdiscipline_file.remark " & _
                   ", hrdiscipline_file.userunkid " & _
                   ", hrdiscipline_file.isvoid " & _
                   ", hrdiscipline_file.voiduserunkid " & _
                   ", hrdiscipline_file.voiddatetime " & _
                   ", hrdiscipline_file.voidreason " & _
                   ", ISNULL(against.firstname+' '+against.surname+' '+against.othername,'')AS AgainstPerson " & _
                   ", ISNULL(involved.firstname+' '+involved.surname+' '+involved.othername,'')AS InvolvedPerson " & _
                   ", ISNULL(against.employeecode,'')AS AgainstPersoncode " & _
                   ", ISNULL(involved.employeecode,'')AS InvolvedPersoncode " & _
                   ", ISNULL(hrdisciplinetype_master.name,'') AS DisciplineType " & _
                   ", ISNULL(hrdisciplinetype_master.severity,'') AS Severity " & _
                   ", CONVERT(CHAR(8),hrdiscipline_file.trandate,112) AS FileDate " & _
                   ", hrdiscipline_file.disciplinestatusunkid " & _
                   ", hrdisciplinestatus_master.name AS Status " & _
                   ", ISNULL(Category,'') AS Category " & _
                   ", ISNULL(CategoryId,0) AS CategoryId " & _
                   ", ISNULL(involved.email,'') AS Email " & _
                   ", emailtranunkid " & _
                   ", emaildate " & _
                   ", interdictdate " & _
                   ", reference_no " & _
                   ", response_date " & _
                   ", response_remark " & _
                   "FROM hrdiscipline_file " & _
                   " JOIN hrdisciplinestatus_master ON hrdiscipline_file.disciplinestatusunkid = hrdisciplinestatus_master.disciplinestatusunkid " & _
                   " LEFT JOIN hrdisciplinetype_master on hrdisciplinetype_master.disciplinetypeunkid = hrdiscipline_file.disciplinetypeunkid " & _
                   " JOIN hremployee_master AS involved ON hrdiscipline_file.involved_employeeunkid = involved.employeeunkid "

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry.Replace("hremployee_master", "involved")
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry.Replace("hremployee_master", "involved")
                strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bTRM\b", "ITRM")
                strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bRET\b", "IRET")
                strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bHIRE\b", "IHIRE")
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry.Replace("hremployee_master", "Involved")
                    strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bTRM\b", "ITRM")
                    strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bRET\b", "IRET")
                    strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bHIRE\b", "IHIRE")
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & System.Text.RegularExpressions.Regex.Replace(xUACFiltrQry, "\bhremployee_master\b", "involved") & " "
            End If

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= " LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "       CASE WHEN isexternal = 0 THEN @INTERNAL " & _
                    "            WHEN isexternal = 1 THEN @EXTERNAL " & _
                    "    END AS Category " & _
                    "   ,disciplinefileunkid AS FileUnkid " & _
                    "   ,disciplinestatusunkid AS StatusId " & _
                    "   ,statustranunkid AS StatusTranId " & _
                    "   ,CASE WHEN isexternal = 0 THEN 0 " & _
                    "            WHEN isexternal = 1 THEN 1 " & _
                    "    END AS CategoryId " & _
                    "FROM " & _
                    "( " & _
                    "   SELECT " & _
                    "     isexternal " & _
                    "    ,disciplinefileunkid " & _
                    "    ,disciplinestatusunkid " & _
                    "    ,statustranunkid " & _
                    "    ,ROW_NUMBER() OVER(PARTITION BY disciplinefileunkid ORDER BY statustranunkid DESC) AS RNO " & _
                    "   FROM hrdiscipline_status_tran " & _
                    "   WHERE isvoid = 0 " & _
                    ") AS Category " & _
                    "WHERE RNO = 1 " & _
                    ") AS A ON A.FileUnkid = hrdiscipline_file.disciplinefileunkid "

            objDataOperation.AddParameter("@INTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Internal"))
            objDataOperation.AddParameter("@EXTERNAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "External"))
            'S.SANDEEP [ 20 APRIL 2012 ] -- END


            strQ &= " LEFT JOIN hremployee_master AS against ON hrdiscipline_file.against_employeeunkid = against.employeeunkid "

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry.Replace("hremployee_master", "against")
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry.Replace("hremployee_master", "against")
                strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bTRM\b", "ATRM")
                strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bRET\b", "ARET")
                strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bHIRE\b", "AHIRE")
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry.Replace("hremployee_master", "against")
                    strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bTRM\b", "ATRM")
                    strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bRET\b", "ARET")
                    strQ = System.Text.RegularExpressions.Regex.Replace(strQ, "\bHIRE\b", "AHIRE")
                End If
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & System.Text.RegularExpressions.Regex.Replace(xUACFiltrQry, "\bhremployee_master\b", "against") & " "
            End If

            strQ &= "WHERE 1 = 1 "

            

            If blnOnlyActive Then
                strQ &= " AND hrdiscipline_file.isvoid = 0 "
            End If



            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If intEmpId > 0 Then
                strQ &= " AND involved_employeeunkid = '" & intEmpId & "'"
            End If
            'S.SANDEEP [ 04 FEB 2012 ] -- END


            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    strQ &= " AND ISNULL(involved.isactive,0) = 1 AND ISNULL(against.isactive,0) = 1 "
            'End If

            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    If blnOnlyActive Then
            '        strQ &= " AND involved.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND against.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '    Else
            '        strQ &= " WHERE involved.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND against.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '    End If
            'End If


            'S.SANDEEP [12 MAY 2015] -- START
            If xFilterString.Trim.Length > 0 Then
                strQ &= " AND " & xFilterString
            End If
            'S.SANDEEP [12 MAY 2015] -- END



            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Sohail (23 Apr 2012) -- Start
            'TRA - ENHANCEMENT
            'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            'Sohail (23 Apr 2012) -- End
            'S.SANDEEP [ 04 FEB 2012 ] -- END


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrdiscipline_file) </purpose>
    Public Function Insert() As Boolean
        If isExist(mdtTrandate, mintDisciplinetypeunkid, mintInvolved_Employeeunkid, mstrIncident) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This discipline is already filed for selected employee. Please add new discipline.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@trandate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTrandate.ToString)
            objDataOperation.AddParameter("@disciplinetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinetypeunkid.ToString)
            objDataOperation.AddParameter("@involved_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInvolved_Employeeunkid.ToString)
            objDataOperation.AddParameter("@against_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAgainst_Employeeunkid.ToString)
            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'objDataOperation.AddParameter("@incident", SqlDbType.VarChar, 8000, mstrIncident.ToString)
            objDataOperation.AddParameter("@incident", SqlDbType.VarChar, mstrIncident.Length, mstrIncident.ToString)
            'S.SANDEEP [ 01 JUNE 2012 ] -- END
            objDataOperation.AddParameter("@remark", SqlDbType.VarChar, 8000, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinestatusunkid.ToString)
            'S.SANDEEP [ 20 MARCH 2012 ] -- END

            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@emailtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmailtranunkid.ToString)
            If mdtEmaildate <> Nothing Then
                objDataOperation.AddParameter("@emaildate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmaildate)
            Else
                objDataOperation.AddParameter("@emaildate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            If mdtInterdictdate <> Nothing Then
                objDataOperation.AddParameter("@interdictdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterdictdate)
            Else
                objDataOperation.AddParameter("@interdictdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference_No.ToString)
            'S.SANDEEP [ 01 JUNE 2012 ] -- END

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdtResponse_Date <> Nothing Then
                objDataOperation.AddParameter("@response_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtResponse_Date)
            Else
                objDataOperation.AddParameter("@response_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@response_remark", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrResponse_Remark.ToString)
            'S.SANDEEP [ 16 JAN 2013 ] -- END

            strQ = "INSERT INTO hrdiscipline_file ( " & _
                  "  trandate " & _
                  ", disciplinetypeunkid " & _
                  ", involved_employeeunkid " & _
                  ", against_employeeunkid " & _
                  ", incident " & _
                  ", remark " & _
                  ", userunkid " & _
                  ", isvoid " & _
                  ", voiduserunkid " & _
                  ", voiddatetime " & _
                  ", voidreason" & _
                  ", disciplinestatusunkid" & _
                  ", emailtranunkid " & _
                  ", emaildate" & _
                  ", interdictdate " & _
                  ", reference_no " & _
                  ", response_date " & _
                  ", response_remark" & _
                ") VALUES (" & _
                  "  @trandate " & _
                  ", @disciplinetypeunkid " & _
                  ", @involved_employeeunkid " & _
                  ", @against_employeeunkid " & _
                  ", @incident " & _
                  ", @remark " & _
                  ", @userunkid " & _
                  ", @isvoid " & _
                  ", @voiduserunkid " & _
                  ", @voiddatetime " & _
                  ", @voidreason" & _
                  ", @disciplinestatusunkid" & _
                  ", @emailtranunkid " & _
                  ", @emaildate" & _
                  ", @interdictdate " & _
                  ", @reference_no " & _
                  ", @response_date " & _
                  ", @response_remark" & _
                "); SELECT @@identity"

            'S.SANDEEP [ 16 JAN 2013 response_date,response_remark] -- START -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintDisciplinefileunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrdiscipline_file", "disciplinefileunkid", mintDisciplinefileunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            Dim objStatusTran As New clsDiscipline_StatusTran

            objStatusTran._Disciplinefileunkid = mintDisciplinefileunkid
            objStatusTran._Disciplinestatusunkid = mintDisciplinestatusunkid
            objStatusTran._Isvoid = False
            objStatusTran._Remark = ""
            objStatusTran._Statusdate = mdtTrandate
            objStatusTran._Userunkid = User._Object._Userunkid
            objStatusTran._Voiddatetime = Nothing
            objStatusTran._Voidreason = ""
            objStatusTran._Voiduserunkid = -1
            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objStatusTran._Isexternal = mblnIsExternal
            'S.SANDEEP [ 20 APRIL 2012 ] -- END



            If objStatusTran.Insert() = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objStatusTran = Nothing

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 20 MARCH 2012 ] -- END




            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrdiscipline_file) </purpose>
    Public Function Update(Optional ByVal blnInsert_Status As Boolean = False) As Boolean
        If isExist(mdtTrandate, mintDisciplinetypeunkid, mintInvolved_Employeeunkid, mstrIncident, mintDisciplinefileunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This discipline is already filed for selected employee. Please add new discipline.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)
            objDataOperation.AddParameter("@trandate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTrandate.ToString)
            objDataOperation.AddParameter("@disciplinetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinetypeunkid.ToString)
            objDataOperation.AddParameter("@involved_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInvolved_Employeeunkid.ToString)
            objDataOperation.AddParameter("@against_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAgainst_Employeeunkid.ToString)
            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            'objDataOperation.AddParameter("@incident", SqlDbType.VarChar, 8000, mstrIncident.ToString)
            objDataOperation.AddParameter("@incident", SqlDbType.VarChar, mstrIncident.Length, mstrIncident.ToString)
            'S.SANDEEP [ 01 JUNE 2012 ] -- END
            objDataOperation.AddParameter("@remark", SqlDbType.VarChar, 8000, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            objDataOperation.AddParameter("@disciplinestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinestatusunkid.ToString)
            'S.SANDEEP [ 20 MARCH 2012 ] -- END


            'S.SANDEEP [ 20 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@emailtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmailtranunkid.ToString)
            If mdtEmaildate <> Nothing Then
                objDataOperation.AddParameter("@emaildate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEmaildate)
            Else
                objDataOperation.AddParameter("@emaildate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP [ 20 APRIL 2012 ] -- END

            'S.SANDEEP [ 01 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA DISCIPLINE CHANGES
            If mdtInterdictdate <> Nothing Then
                objDataOperation.AddParameter("@interdictdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtInterdictdate.ToString)
            Else
                objDataOperation.AddParameter("@interdictdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@reference_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReference_No.ToString)
            'S.SANDEEP [ 01 JUNE 2012 ] -- END

            'S.SANDEEP [ 16 JAN 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdtResponse_Date <> Nothing Then
                objDataOperation.AddParameter("@response_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtResponse_Date)
            Else
                objDataOperation.AddParameter("@response_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@response_remark", SqlDbType.Text, eZeeDataType.NAME_SIZE, mstrResponse_Remark.ToString)
            'S.SANDEEP [ 16 JAN 2013 ] -- END

            strQ = "UPDATE hrdiscipline_file SET " & _
                   "  trandate = @trandate" & _
                   ", disciplinetypeunkid = @disciplinetypeunkid" & _
                   ", involved_employeeunkid = @involved_employeeunkid" & _
                   ", against_employeeunkid = @against_employeeunkid" & _
                   ", incident = @incident" & _
                   ", remark = @remark" & _
                   ", userunkid = @userunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   ", disciplinestatusunkid = @disciplinestatusunkid " & _
                   ", emailtranunkid = @emailtranunkid " & _
                   ", emaildate = @emaildate " & _
                   ", interdictdate = @interdictdate" & _
                   ", reference_no = @reference_no " & _
                   ", response_date = @response_date" & _
                   ", response_remark = @response_remark " & _
                   "WHERE disciplinefileunkid = @disciplinefileunkid "

            'S.SANDEEP [ 16 JAN 2013 response_date,response_remark] -- START -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrdiscipline_file", mintDisciplinefileunkid, "disciplinefileunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrdiscipline_file", "disciplinefileunkid", mintDisciplinefileunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'S.SANDEEP [ 20 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {DISCIPLINE MODULE}
            If blnInsert_Status = True Then
                Dim objStatusTran As New clsDiscipline_StatusTran

                objStatusTran._Disciplinefileunkid = mintDisciplinefileunkid
                objStatusTran._Disciplinestatusunkid = mintDisciplinestatusunkid
                objStatusTran._Isvoid = False
                objStatusTran._Remark = ""
                objStatusTran._Statusdate = ConfigParameter._Object._CurrentDateAndTime
                objStatusTran._Userunkid = User._Object._Userunkid
                objStatusTran._Voiddatetime = Nothing
                objStatusTran._Voidreason = ""
                objStatusTran._Voiduserunkid = -1
                'S.SANDEEP [ 20 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If mblnIsFromResolution = True Then
                    objStatusTran._Isexternal = mblnIsExternal
                End If
                'S.SANDEEP [ 20 APRIL 2012 ] -- END

                If objStatusTran.Insert() = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objStatusTran = Nothing
            End If
            'S.SANDEEP [ 20 MARCH 2012 ] -- END



            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrdiscipline_file) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, you cannot void this discipline. Reason : This discipline already linked to some transactions.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE hrdiscipline_file SET " & _
                     " isvoid = 1 " & _
                     ",voiddatetime = @voiddatetime " & _
                     ",voidreason = @voidreason " & _
                     ",voiduserunkid = @voiduserunkid " & _
                   "WHERE disciplinefileunkid = @disciplinefileunkid "

            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrdiscipline_file", "disciplinefileunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            mstrMessage = ""

            strQ = " SELECT * FROM hrdiscipline_resolutions WHERE disciplinefileunkid = @disciplinefileunkid "

            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal dtDisciplineDate As DateTime, ByVal intDisciplineTypeunkid As Integer, ByVal intPersonInvolvedunkid As Integer, _
                            ByVal strIncident As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  disciplinefileunkid " & _
                   ", trandate " & _
                   ", disciplinetypeunkid " & _
                   ", involved_employeeunkid " & _
                   ", against_employeeunkid " & _
                   ", incident " & _
                   ", remark " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   "FROM hrdiscipline_file " & _
                   " WHERE trandate = @trandate " & _
                   " AND disciplinetypeunkid = @disciplinetypeunkid " & _
                   " AND involved_employeeunkid = @involved_employeeunkid " & _
                   " AND incident = @incident and isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND disciplinefileunkid <> @disciplinefileunkid"
                objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@trandate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtDisciplineDate)
            objDataOperation.AddParameter("@disciplinetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineTypeunkid)
            objDataOperation.AddParameter("@involved_employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPersonInvolvedunkid)
            objDataOperation.AddParameter("@incident", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strIncident)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP [ 01 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA DISCIPLINE CHANGES
    Public Function IsInterdictionDateExists(ByVal dtDate As Date, ByVal strRefNo As String, ByRef dtOrginalDate As Date) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Try
            objDataOperation = New clsDataOperation

            If strRefNo.Trim.Length > 0 Then
                StrQ = "SELECT ISNULL(CONVERT(CHAR(8),interdictdate,112),'') AS IDate FROM hrdiscipline_file WHERE reference_no = @RefNo AND interdictdate IS NOT NULL"
                objDataOperation.AddParameter("@RefNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strRefNo)
                dsList = objDataOperation.ExecQuery(StrQ, "List")
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If dsList.Tables("List").Rows.Count > 0 Then
                    If eZeeDate.convertDate(dtDate) <> dsList.Tables("List").Rows(0)("IDate") Then
                        dtOrginalDate = eZeeDate.convertDate(dsList.Tables("List").Rows(0)("IDate").ToString).ToShortDateString
                        blnFlag = False
                    Else
                        blnFlag = True
                    End If
                Else
                    blnFlag = True
                End If
            Else
                blnFlag = True
            End If
            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsInterdictionDateExists; Module Name: " & mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 01 JUNE 2012 ] -- END

    Public Function GetData_ForDiary(ByVal intEmployeeId As Integer, Optional ByVal StrListName As String = "List") As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsGrpData As New DataSet
        Dim dsTrnData As New DataSet
        Dim dtFinalTable As New DataTable(StrListName)
        Try
            objDataOperation = New clsDataOperation

            StrQ &= "SELECT " & _
                    "   CONVERT(CHAR(8),hrdiscipline_file.trandate,112) AS ChargeDate " & _
                    ",  hrdisciplinetype_master.name AS Offence " & _
                    ",  incident AS incident " & _
                    ",  hrdisciplinestatus_master.name AS DStatus " & _
                    ",  disciplinefileunkid AS GrpId " & _
                    ",  1 AS IsGrp " & _
                    "FROM hrdiscipline_file " & _
                    "   JOIN hrdisciplinestatus_master ON hrdiscipline_file.disciplinestatusunkid = hrdisciplinestatus_master.disciplinestatusunkid " & _
                    "   JOIN hrdisciplinetype_master ON hrdiscipline_file.disciplinetypeunkid = hrdisciplinetype_master.disciplinetypeunkid " & _
                    "WHERE involved_employeeunkid = '" & intEmployeeId & "' "

            dsGrpData = objDataOperation.ExecQuery(StrQ, "GrpList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsGrpData.Tables("GrpList").Rows.Count > 0 Then
                dtFinalTable.Columns.Add("Column1", System.Type.GetType("System.String")).DefaultValue = "" 'RESOLUTION STEP
                dtFinalTable.Columns.Add("Column2", System.Type.GetType("System.String")).DefaultValue = "" 'ACTION TAKEN
                dtFinalTable.Columns.Add("Column3", System.Type.GetType("System.String")).DefaultValue = "" 'ACTION START DATE
                dtFinalTable.Columns.Add("Column4", System.Type.GetType("System.String")).DefaultValue = "" 'ACTION END DATE
                dtFinalTable.Columns.Add("Column5", System.Type.GetType("System.String")).DefaultValue = "" 'RESOLUTION STEP STATUS
                dtFinalTable.Columns.Add("Column6", System.Type.GetType("System.String")).DefaultValue = "" 'APPROVAL STATUS
                dtFinalTable.Columns.Add("Column7", System.Type.GetType("System.String")).DefaultValue = "" 'APPROVAL DATE
                dtFinalTable.Columns.Add("Column8", System.Type.GetType("System.Boolean")).DefaultValue = False 'IS GROUP
                dtFinalTable.Columns.Add("Column9", System.Type.GetType("System.Int32")).DefaultValue = -1 'GROUP ID
                dtFinalTable.Columns.Add("Column10", System.Type.GetType("System.String")).DefaultValue = "" 'INCIDENT
                dtFinalTable.Columns.Add("Column11", System.Type.GetType("System.String")).DefaultValue = "" 'LINK

                Dim dFRow As DataRow = Nothing

                For Each dGRow As DataRow In dsGrpData.Tables("GrpList").Rows
                    dFRow = dtFinalTable.NewRow
                    dFRow.Item("Column1") = "Charge Date : " & eZeeDate.convertDate(dGRow.Item("ChargeDate").ToString).ToShortDateString & " || Offence : " & dGRow.Item("Offence") & _
                                            " || Status : " & dGRow.Item("DStatus")
                    dFRow.Item("Column8") = True
                    dFRow.Item("Column9") = dGRow.Item("GrpId")
                    dFRow.Item("Column10") = dGRow.Item("incident")
                    dFRow.Item("Column11") = Language.getMessage(mstrModuleName, 7, "Show Incident")
                    dtFinalTable.Rows.Add(dFRow)

                    StrQ = "SELECT " & _
                           "    hrdiscipline_resolutions.disciplinefileunkid " & _
                           ",   resolutionstep AS RStep " & _
                           ",   ISNULL(hraction_reason_master.reason_action,'') AS ActionTaken " & _
                           ",   ISNULL(CONVERT(CHAR(8),hrdiscipline_resolutions.action_start_date,112),'') AS AStartDate " & _
                           ",   ISNULL(CONVERT(CHAR(8),hrdiscipline_resolutions.action_end_date,112),'')AS AEndDate " & _
                           ",   ISNULL(RStatus.name,'') AS SStatus " & _
                           ",   CASE WHEN hrdiscipline_resolutions.isapproved = 0 THEN @Unapproved " & _
                           "         WHEN hrdiscipline_resolutions.isapproved = 1 THEN @Approved " & _
                           "    END AS App_Status " & _
                           ",   ISNULL(CONVERT(CHAR(8),hrdiscipline_resolutions.approvaldate,112),'') AS App_Date " & _
                           "FROM hrdiscipline_resolutions " & _
                           "    JOIN hrdisciplinestatus_master AS RStatus ON hrdiscipline_resolutions.disciplinestatusunkid = RStatus.disciplinestatusunkid " & _
                           "    JOIN hraction_reason_master ON hraction_reason_master.actionreasonunkid = hrdiscipline_resolutions.disciplinaryactionunkid " & _
                           "WHERE disciplinefileunkid = @disciplinefileunkid "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dGRow.Item("GrpId"))
                    objDataOperation.AddParameter("@Unapproved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Unapproved"))
                    objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Approved"))

                    dsTrnData = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    For Each dTRow As DataRow In dsTrnData.Tables("List").Rows
                        dFRow = dtFinalTable.NewRow

                        dFRow.Item("Column1") = Space(5) & dTRow.Item("RStep")
                        dFRow.Item("Column2") = dTRow.Item("ActionTaken")
                        If dTRow.Item("AStartDate").ToString.Trim.Length <= 0 Then
                            dFRow.Item("Column3") = ""
                        Else
                            dFRow.Item("Column3") = eZeeDate.convertDate(dTRow.Item("AStartDate").ToString).ToShortDateString
                        End If

                        If dTRow.Item("AEndDate").ToString.Trim.Length <= 0 Then
                            dFRow.Item("Column4") = ""
                        Else
                            dFRow.Item("Column4") = eZeeDate.convertDate(dTRow.Item("AEndDate").ToString).ToShortDateString
                        End If

                        dFRow.Item("Column5") = dTRow.Item("SStatus")
                        dFRow.Item("Column6") = dTRow.Item("App_Status")

                        If dTRow.Item("App_Date").ToString.Trim.Length <= 0 Then
                            dFRow.Item("Column7") = ""
                        Else
                            dFRow.Item("Column7") = eZeeDate.convertDate(dTRow.Item("App_Date").ToString).ToShortDateString
                        End If

                        dFRow.Item("Column8") = False
                        dFRow.Item("Column9") = dGRow.Item("GrpId")

                        dtFinalTable.Rows.Add(dFRow)
                    Next

                Next

            End If

            Return dtFinalTable

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData_ForDiary; Module Name: " & mstrModuleName)
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This discipline is already filed for selected employee. Please add new discipline.")
            Language.setMessage(mstrModuleName, 2, "Sorry, you cannot void this discipline. Reason : This discipline already linked to some transactions.")
            Language.setMessage(mstrModuleName, 3, "Internal")
            Language.setMessage(mstrModuleName, 4, "External")
            Language.setMessage(mstrModuleName, 5, "Unapproved")
            Language.setMessage(mstrModuleName, 6, "Approved")
            Language.setMessage(mstrModuleName, 7, "Show Incident")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
