﻿'************************************************************************************************************************************
'Class Name : clshearing_schedule_master.vb
'Purpose    :
'Date       : 13/06/2016
'Written By : Nilay
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
''' <summary>
''' Purpose: 
''' Developer: Nilay
''' </summary>
Public Class clshearing_schedule_master
    Private Shared ReadOnly mstrModuleName As String = "clshearing_schedule_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintHearingschedulemasterunkid As Integer
    Private mintDisciplinefileunkid As Integer
    Private mdtHearingDate As DateTime
    Private mdtHearningTime As DateTime
    Private mstrHearningVenue As String = String.Empty
    Private mstrRemark As String = String.Empty
    Private mintStatus As Integer = enHearingStatus.Open
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mintLoginEmployeeunkid As Integer = -1
    'S.SANDEEP [24 MAY 2016] -- Start
    'Email Notification
    Private mintLoginTypeId As Integer = 0
    'S.SANDEEP [24 MAY 2016] -- End

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Private objDocument As New clsScan_Attach_Documents
    'S.SANDEEP |11-NOV-2019| -- END

#End Region

#Region " Enum "
    Public Enum enHearingStatus
        Open = 1
        Close = 2
    End Enum
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Nilay
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hearingschedulemasterunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Hearingschedulemasterunkid() As Integer
        Get
            Return mintHearingschedulemasterunkid
        End Get
        Set(ByVal value As Integer)
            mintHearingschedulemasterunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set disciplinefileunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Disciplinefileunkid() As Integer
        Get
            Return mintDisciplinefileunkid
        End Get
        Set(ByVal value As Integer)
            mintDisciplinefileunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hearing_date
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _HearingDate() As DateTime
        Get
            Return mdtHearingDate
        End Get
        Set(ByVal value As DateTime)
            mdtHearingDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hearning_time
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _HearningTime() As DateTime
        Get
            Return mdtHearningTime
        End Get
        Set(ByVal value As DateTime)
            mdtHearningTime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hearning_venue
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _HearningVenue() As String
        Get
            Return mstrHearningVenue
        End Get
        Set(ByVal value As String)
            mstrHearningVenue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Status
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Status() As Integer
        Get
            Return mintStatus
        End Get
        Set(ByVal value As Integer)
            mintStatus = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webclientip
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webhostname
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    'S.SANDEEP [24 MAY 2016] -- Start
    'Email Notification
    Public Property _LoginTypeId() As Integer
        Get
            Return mintLoginTypeId
        End Get
        Set(ByVal value As Integer)
            mintLoginTypeId = value
        End Set
    End Property
    'S.SANDEEP [24 MAY 2016] -- End

#End Region

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        Try
            strQ = "SELECT " & _
                        "  hearingschedulemasterunkid " & _
                        ", disciplinefileunkid " & _
                        ", hearing_date " & _
                        ", hearning_time " & _
                        ", hearning_venue " & _
                        ", remark " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason " & _
                   "FROM hrhearing_schedule_master " & _
                   "WHERE hearingschedulemasterunkid = @hearingschedulemasterunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@hearingschedulemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHearingschedulemasterunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintHearingschedulemasterunkid = CInt(dtRow.Item("hearingschedulemasterunkid"))
                mintDisciplinefileunkid = CInt(dtRow.Item("disciplinefileunkid"))
                mdtHearingDate = IIf(IsDBNull(dtRow.Item("hearing_date")), Nothing, dtRow.Item("hearing_date"))
                mdtHearningTime = IIf(IsDBNull(dtRow.Item("hearning_time")), Nothing, dtRow.Item("hearning_time"))
                mstrHearningVenue = dtRow.Item("hearning_venue").ToString
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = IIf(IsDBNull(dtRow.Item("voiddatetime")), Nothing, dtRow.Item("voiddatetime"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal strFilter As String = "", _
                            Optional ByVal blnAddAccessFilter As Boolean = True) As DataSet 'S.SANDEEP |01-OCT-2019| -- START {blnAddAccessFilter} -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            If blnAddAccessFilter = True Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            'S.SANDEEP |01-OCT-2019| -- END


            strQ = "SELECT DISTINCT " & _
                        "  hrdiscipline_file_master.reference_no AS reference_no " & _
                        ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS person_involved " & _
                        ", hrhearing_schedule_master.hearing_date AS hearing_date " & _
                        ", hrhearing_schedule_master.hearning_time AS hearning_time " & _
                        ", hrhearing_schedule_master.hearning_venue AS hearning_venue " & _
                        ", cfcommon_master.name AS committee " & _
                        ", hrhearing_schedule_master.remark AS remark " & _
                        ", hrhearing_schedule_master.status AS status " & _
                        ", CASE WHEN hrhearing_schedule_master.status = " & enHearingStatus.Open & " THEN @Open " & _
                        "       WHEN hrhearing_schedule_master.status = " & enHearingStatus.Close & " THEN @Close " & _
                        "  END AS statusname " & _
                        ", hrhearing_schedule_master.hearingschedulemasterunkid AS hearingschedulemasterunkid " & _
                        ", hrdiscipline_file_master.disciplinefileunkid AS disciplinefileunkid " & _
                        ", CASE WHEN intf.mem = intf.isent THEN 1 ELSE 0 END AS iallsent " & _
                        ", CASE WHEN intf.mem = intf.isent THEN @Sent ELSE @Pending END AS [ISent] " & _
                   "FROM hrhearing_schedule_master " & _
                        " LEFT JOIN hrdiscipline_file_master ON hrdiscipline_file_master.disciplinefileunkid = hrhearing_schedule_master.disciplinefileunkid " & _
                        " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = hrdiscipline_file_master.involved_employeeunkid " & _
                        " LEFT JOIN hrhearing_schedule_tran ON hrhearing_schedule_tran.hearingschedulemasterunkid = hrhearing_schedule_master.hearingschedulemasterunkid " & _
                        " LEFT JOIN hrdiscipline_committee ON hrdiscipline_committee.committeetranunkid = hrhearing_schedule_tran.committeetranunkid " & _
                        " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrhearing_schedule_tran.committeemstunkid " & _
                        "LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        COUNT(hrhearing_schedule_master.hearingschedulemasterunkid) AS mem " & _
                        "       ,SUM(CASE WHEN isnotified = 1 THEN 1 ELSE 0 END) as isent " & _
                        "       ,hrhearing_schedule_master.hearingschedulemasterunkid " & _
                        "   FROM hrhearing_schedule_master " & _
                        "       JOIN hrhearing_schedule_tran ON hrhearing_schedule_tran.hearingschedulemasterunkid = hrhearing_schedule_master.hearingschedulemasterunkid " & _
                        "   WHERE hrhearing_schedule_master.isvoid = 0 AND hrhearing_schedule_tran.isvoid = 0 " & _
                        "   GROUP BY hrhearing_schedule_master.hearingschedulemasterunkid " & _
                        ") AS intf ON intf.hearingschedulemasterunkid = hrhearing_schedule_master.hearingschedulemasterunkid "
            '" LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrdiscipline_committee.committeemasterunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            strQ &= "WHERE ISNULL(hrhearing_schedule_master.isvoid,0) = 0 AND ISNULL(hrdiscipline_file_master.isvoid,0) = 0 "

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            strQ &= " ORDER BY reference_no, hearing_date DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Open", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Open"))
            objDataOperation.AddParameter("@Close", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Close"))

            objDataOperation.AddParameter("@Sent", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 100, "Sent"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 101, "Pending"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function getHearingStatus(ByVal strList As String, ByVal blnAddSelect As Boolean) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            If blnAddSelect = True Then
                strQ = "SELECT 0 AS id, @Select AS name UNION "
            End If

            strQ &= "SELECT '" & enHearingStatus.Open & "' AS id, @Open AS name UNION " & _
                    "SELECT '" & enHearingStatus.Close & "' AS id, @Close AS name "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@Open", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Open"))
            objDataOperation.AddParameter("@Close", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Close"))

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    Public Function getHearingScheduleMasterId(ByVal intDisciplineFileunkid As Integer) As Integer
        Dim strQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            Dim intHearingScheduleMasterId As Integer = 0

            strQ = "SELECT " & _
                        "   hrhearing_schedule_master.hearingschedulemasterunkid " & _
                   "FROM hrhearing_schedule_master " & _
                   "WHERE hrhearing_schedule_master.disciplinefileunkid = @disciplinefileunkid " & _
                        " AND hrhearing_schedule_master.isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intDisciplineFileunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "Id")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("Id").Rows.Count > 0 Then
                intHearingScheduleMasterId = CInt(dsList.Tables("Id").Rows(0).Item("hearingschedulemasterunkid"))
            End If

            Return intHearingScheduleMasterId
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getHearingScheduleMasterId", mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrhearing_schedule_master) </purpose>
    Public Function Insert(ByVal dtCurrentDateTime As DateTime, _
                           ByVal dtTran As DataTable, _
                           ByVal intCompanyUnkId As Integer, _
                           ByVal dtAttachment As DataTable, _
                           ByVal strTransScreenName As String, _
                           ByVal dtEmployeeAsOnDate As String) As Boolean 'Pinkal (19-Dec-2020) -- Start {dtEmployeeAsOnDate} -- End 
        'S.SANDEEP |11-NOV-2019| -- START {dtAttachment,strTransactionName} -- END
        'Sohail (30 Nov 2017) - [intCompanyUnkId]


        If isHearingStatusOpen(mintDisciplinefileunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 6, "Hearing Schedule is already in open status for selected reference no.")
            Return False
        End If

        If isExist(mintDisciplinefileunkid, mdtHearingDate, mdtHearningTime) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Hearing Schedule already exists for given Hearing Date or Hearing Time.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)
            objDataOperation.AddParameter("@hearing_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtHearingDate <> Nothing, mdtHearingDate, DBNull.Value))
            objDataOperation.AddParameter("@hearning_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtHearningTime <> Nothing, mdtHearningTime, DBNull.Value))
            objDataOperation.AddParameter("@hearning_venue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHearningVenue.ToString)
            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark.ToString)
            'S.SANDEEP |01-OCT-2019| -- END
            objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatus.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO hrhearing_schedule_master ( " & _
                      "  disciplinefileunkid " & _
                      ", hearing_date " & _
                      ", hearning_time " & _
                      ", hearning_venue " & _
                      ", remark " & _
                      ", status " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                   ") VALUES (" & _
                      "  @disciplinefileunkid " & _
                      ", @hearing_date " & _
                      ", @hearning_time " & _
                      ", @hearning_venue " & _
                      ", @remark " & _
                      ", @status " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintHearingschedulemasterunkid = dsList.Tables(0).Rows(0).Item(0)
            Call GetData(objDataOperation)

            If InsertATHearingScheduleMaster(objDataOperation, enAuditType.ADD, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            If dtAttachment IsNot Nothing Then
                Dim dtTab As DataTable = Nothing
                Dim row As DataRow() = dtAttachment.Select("form_name = '" & strTransScreenName & "'")
                If row.Length > 0 Then
                    row.ToList.ForEach(Function(x) UpdateRowValue(x, mintHearingschedulemasterunkid, mintUserunkid))
                    dtTab = row.CopyToDataTable()
                    objDocument._Datatable = dtTab
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    dtAttachment = dtTab
                End If
            End If
            'S.SANDEEP |11-NOV-2019| -- END


            Dim objHearingScheduleTran As New clshearing_schedule_Tran

            If dtTran IsNot Nothing Then
                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                objHearingScheduleTran._Hearingschedulemasterunkid(eZeeDate.convertDate(dtEmployeeAsOnDate.ToString())) = mintHearingschedulemasterunkid
                'Pinkal (19-Dec-2020) -- End
                objHearingScheduleTran._HeatingScheduleTranTable = dtTran
                objHearingScheduleTran._WebClientIP = mstrWebClientIP
                objHearingScheduleTran._WebFormName = mstrWebFormName
                objHearingScheduleTran._WebHostName = mstrWebHostName

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'If objHearingScheduleTran.InsertUpdateDeleteHearingTran(objDataOperation, mintUserunkid, dtCurrentDateTime, mintDisciplinefileunkid) = False Then

                'S.SANDEEP |11-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                If objHearingScheduleTran.InsertUpdateDeleteHearingTran(objDataOperation, mintUserunkid, dtCurrentDateTime, mintDisciplinefileunkid, intCompanyUnkId, dtAttachment, mstrModuleName) = False Then
                    'If objHearingScheduleTran.InsertUpdateDeleteHearingTran(objDataOperation, mintUserunkid, dtCurrentDateTime, mintDisciplinefileunkid, intCompanyUnkId) = False Then
                    'Sohail (30 Nov 2017) -- End
                    'S.SANDEEP |11-NOV-2019| -- END
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrhearing_schedule_master) </purpose>
    Public Function Update(ByVal dtCurrentDateTime As DateTime, _
                           ByVal dtTran As DataTable, _
                           ByVal intCompanyUnkId As Integer, _
                           ByVal dtAttachment As DataTable, _
                           ByVal strTransScreenName As String, _
                           ByVal dtEmployeeAsOnDate As String) As Boolean 'Pinkal (19-Dec-2020) -- Start {dtEmployeeAsOnDate} -- End 
        'S.SANDEEP |11-NOV-2019| -- START {dtAttachment,strTransactionName} -- END
        'Sohail (30 Nov 2017) - [intCompanyUnkId]

        If isExist(mintDisciplinefileunkid, mdtHearingDate, mdtHearningTime, mintHearingschedulemasterunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Hearing Schedule already exists for given Hearing Date or Hearing Time.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@hearingschedulemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHearingschedulemasterunkid.ToString)
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)
            objDataOperation.AddParameter("@hearing_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtHearingDate <> Nothing, mdtHearingDate, DBNull.Value))
            objDataOperation.AddParameter("@hearning_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtHearningTime <> Nothing, mdtHearningTime, DBNull.Value))
            objDataOperation.AddParameter("@hearning_venue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHearningVenue.ToString)
            'S.SANDEEP |01-OCT-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            'objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, mstrRemark.Length, mstrRemark.ToString)
            'S.SANDEEP |01-OCT-2019| -- END
            objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatus.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE hrhearing_schedule_master SET " & _
                      "  disciplinefileunkid = @disciplinefileunkid " & _
                      ", hearing_date = @hearing_date " & _
                      ", hearning_time = @hearning_time " & _
                      ", hearning_venue = @hearning_venue " & _
                      ", remark = @remark " & _
                      ", status = @status " & _
                      ", userunkid = @userunkid " & _
                      ", isvoid = @isvoid " & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = @voiddatetime " & _
                      ", voidreason = @voidreason " & _
                   "WHERE hearingschedulemasterunkid = @hearingschedulemasterunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Call GetData(objDataOperation)

            If InsertATHearingScheduleMaster(objDataOperation, enAuditType.EDIT, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            If dtAttachment IsNot Nothing Then
                Dim dtTab As DataTable = Nothing
                Dim row As DataRow() = dtAttachment.Select("form_name = '" & strTransScreenName & "'")
                If row.Length > 0 Then
                    row.ToList.ForEach(Function(x) UpdateRowValue(x, mintHearingschedulemasterunkid, mintUserunkid))
                    dtTab = row.CopyToDataTable()
                    objDocument._Datatable = dtTab
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    dtAttachment = dtTab
                End If
            End If
            'S.SANDEEP |11-NOV-2019| -- END

            Dim objHearingScheduleTran As New clshearing_schedule_Tran

            If dtTran IsNot Nothing Then
                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                objHearingScheduleTran._Hearingschedulemasterunkid(eZeeDate.convertDate(dtEmployeeAsOnDate.ToString())) = mintHearingschedulemasterunkid
                'Pinkal (19-Dec-2020) -- End
                objHearingScheduleTran._HeatingScheduleTranTable = dtTran
                objHearingScheduleTran._WebClientIP = mstrWebClientIP
                objHearingScheduleTran._WebFormName = mstrWebFormName
                objHearingScheduleTran._WebHostName = mstrWebHostName

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'If objHearingScheduleTran.InsertUpdateDeleteHearingTran(objDataOperation, mintUserunkid, dtCurrentDateTime, mintDisciplinefileunkid) = False Then

                'S.SANDEEP |11-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'If objHearingScheduleTran.InsertUpdateDeleteHearingTran(objDataOperation, mintUserunkid, dtCurrentDateTime, mintDisciplinefileunkid, intCompanyUnkId) = False Then
                If objHearingScheduleTran.InsertUpdateDeleteHearingTran(objDataOperation, mintUserunkid, dtCurrentDateTime, mintDisciplinefileunkid, intCompanyUnkId, dtAttachment, mstrModuleName) = False Then
                    'S.SANDEEP |11-NOV-2019| -- END
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrhearing_schedule_master) </purpose>
    Public Function Delete(ByVal inthearingschedulemasterunkid As Integer, _
                           ByVal dtCurrentDateTime As DateTime, _
                           ByVal dtTran As DataTable, _
                           ByVal intCompanyUnkId As Integer, _
                           ByVal dtAttachment As DataTable, _
                           ByVal dtEmployeeAsOnDate As String) As Boolean 'Pinkal (19-Dec-2020) -- Start {dtEmployeeAsOnDate} -- End 
        'S.SANDEEP |11-NOV-2019| -- START {dtAttachment} -- END
        'Sohail (30 Nov 2017) - [intCompanyUnkId]

        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            strQ = "UPDATE hrhearing_schedule_master SET " & _
                        "  isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                   "WHERE hearingschedulemasterunkid = @hearingschedulemasterunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@hearingschedulemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, inthearingschedulemasterunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintHearingschedulemasterunkid = inthearingschedulemasterunkid
            Call GetData(objDataOperation)

            If InsertATHearingScheduleMaster(objDataOperation, enAuditType.DELETE, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP |11-NOV-2019| -- START
            'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
            objDocument._Datatable = dtAttachment
            objDocument.InsertUpdateDelete_Documents(objDataOperation)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |11-NOV-2019| -- END

            Dim objHearingScheduleTran As New clshearing_schedule_Tran

            If dtTran IsNot Nothing Then
                'Pinkal (19-Dec-2020) -- Start
                'Enhancement  -  Working on Discipline module for NMB.
                objHearingScheduleTran._Hearingschedulemasterunkid(eZeeDate.convertDate(dtEmployeeAsOnDate.ToString())) = mintHearingschedulemasterunkid
                'Pinkal (19-Dec-2020) -- End
                objHearingScheduleTran._HeatingScheduleTranTable = dtTran
                objHearingScheduleTran._WebClientIP = mstrWebClientIP
                objHearingScheduleTran._WebFormName = mstrWebFormName
                objHearingScheduleTran._WebHostName = mstrWebHostName

                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'If objHearingScheduleTran.InsertUpdateDeleteHearingTran(objDataOperation, mintUserunkid, dtCurrentDateTime, mintDisciplinefileunkid) = False Then

                'S.SANDEEP |11-NOV-2019| -- START
                'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
                'If objHearingScheduleTran.InsertUpdateDeleteHearingTran(objDataOperation, mintUserunkid, dtCurrentDateTime, mintDisciplinefileunkid, intCompanyUnkId) = False Then
                '    'Sohail (30 Nov 2017) -- End
                If objHearingScheduleTran.InsertUpdateDeleteHearingTran(objDataOperation, mintUserunkid, dtCurrentDateTime, mintDisciplinefileunkid, intCompanyUnkId, Nothing, "") = False Then
                    'S.SANDEEP |11-NOV-2019| -- END
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@hearingschedulemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isHearingStatusOpen(ByVal intdisciplinefileunkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  hearingschedulemasterunkid " & _
                      ", disciplinefileunkid " & _
                      ", hearing_date " & _
                      ", hearning_time " & _
                      ", hearning_venue " & _
                      ", remark " & _
                      ", status " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                   "FROM hrhearing_schedule_master " & _
                   "WHERE disciplinefileunkid = @disciplinefileunkid " & _
                        " AND isvoid = 0 " & _
                        " AND status = " & enHearingStatus.Open

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intdisciplinefileunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isHearingStatusOpen; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal intdisciplinefileunkid As Integer, ByVal dtHearingDate As DateTime, ByVal dtHearingTime As DateTime, _
                            Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim strCulturePattern As String = Globalization.CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern.Replace("t", "")
            Dim newCulturePattern As String = strCulturePattern.Replace("h", "H")

            strQ = "SELECT " & _
                      "  hearingschedulemasterunkid " & _
                      ", disciplinefileunkid " & _
                      ", hearing_date " & _
                      ", hearning_time " & _
                      ", hearning_venue " & _
                      ", remark " & _
                      ", status " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                   "FROM hrhearing_schedule_master " & _
                   "WHERE disciplinefileunkid = @disciplinefileunkid AND isvoid = 0 " & _
                      " AND (CONVERT(CHAR(8),hearing_date,112) > '" & eZeeDate.convertDate(dtHearingDate) & "' OR " & _
                      "         (CONVERT(CHAR(8),hearing_date,112) >= '" & eZeeDate.convertDate(dtHearingDate) & "' " & _
                      "             AND CONVERT(CHAR(5),hearning_time, 108) >= '" & Format(dtHearingTime, newCulturePattern) & "'" & _
                      "         ) " & _
                      "     ) "

            If intUnkid > 0 Then
                strQ &= " AND hearingschedulemasterunkid <> @hearingschedulemasterunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@hearingschedulemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHearingschedulemasterunkid.ToString)
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intdisciplinefileunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP |11-NOV-2019| -- START
    'ISSUE/ENHANCEMENT : NMB DISCIPLINARY UAT COMMENTS
    Public Function GetComboListForCharge(ByVal intScheduleMstId As Integer, _
                                          Optional ByVal blnFlag As Boolean = False, _
                                          Optional ByVal blnShowAllCharges As Boolean = True) As DataSet
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Try
            Using objDo As New clsDataOperation
                If blnFlag Then
                    StrQ = "SELECT 0 AS disciplinefileunkid, @Select AS reference_no UNION "
                End If
                StrQ &= "SELECT DISTINCT " & _
                        "    hrhearing_schedule_master.hearingschedulemasterunkid  AS disciplinefileunkid " & _
                        "   ,hrdiscipline_file_master.reference_no AS reference_no " & _
                        "FROM hrhearing_schedule_master " & _
                        "   JOIN hrdiscipline_file_master ON hrdiscipline_file_master.disciplinefileunkid = hrhearing_schedule_master.disciplinefileunkid " & _
                        "WHERE hrhearing_schedule_master.isvoid = 0 AND hrhearing_schedule_master.hearingschedulemasterunkid  = @hearingschedulemasterunkid  "


                If blnShowAllCharges = False Then
                    StrQ &= " AND hrdiscipline_file_master.isvisibleoness = 1 "
                End If

                objDo.AddParameter("@hearingschedulemasterunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, intScheduleMstId)
                objDo.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 37, "Select"))

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetComboListForCharge; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Private Function UpdateRowValue(ByVal dr As DataRow, ByVal intValue As Integer, ByVal intUserId As Integer) As Boolean
        Try
            dr("transactionunkid") = intValue
            dr("userunkid") = intUserId
            dr.AcceptChanges()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRowValue; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP |11-NOV-2019| -- END

    Public Function InsertATHearingScheduleMaster(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType, _
                                                  ByVal dtCurrentDateTime As DateTime) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "INSERT INTO athrhearing_schedule_master ( " & _
                      "  hearingschedulemasterunkid " & _
                      ", disciplinefileunkid " & _
                      ", hearing_date " & _
                      ", hearning_time " & _
                      ", hearning_venue " & _
                      ", remark " & _
                      ", status " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", module_name1 " & _
                      ", module_name2 " & _
                      ", module_name3 " & _
                      ", module_name4 " & _
                      ", module_name5 " & _
                      ", isweb" & _
                      ", loginemployeeunkid " & _
                   ") VALUES (" & _
                      "  @hearingschedulemasterunkid " & _
                      ", @disciplinefileunkid " & _
                      ", @hearing_date " & _
                      ", @hearning_time " & _
                      ", @hearning_venue " & _
                      ", @remark " & _
                      ", @status " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @module_name1 " & _
                      ", @module_name2 " & _
                      ", @module_name3 " & _
                      ", @module_name4 " & _
                      ", @module_name5 " & _
                      ", @isweb" & _
                      ", @loginemployeeunkid " & _
                   ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@hearingschedulemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintHearingschedulemasterunkid.ToString)
            objDataOperation.AddParameter("@disciplinefileunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDisciplinefileunkid.ToString)
            objDataOperation.AddParameter("@hearing_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtHearingDate <> Nothing, mdtHearingDate, DBNull.Value))
            objDataOperation.AddParameter("@hearning_time", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtHearningTime <> Nothing, mdtHearningTime, DBNull.Value))
            objDataOperation.AddParameter("@hearning_venue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHearningVenue.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@status", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatus.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATHearingScheduleMaster; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "WEB")
            Language.setMessage(mstrModuleName, 2, "Hearing Schedule already exists for given Hearing Date or Hearing Time.")
            Language.setMessage(mstrModuleName, 3, "Select")
            Language.setMessage(mstrModuleName, 4, "Open")
            Language.setMessage(mstrModuleName, 5, "Close")
            Language.setMessage(mstrModuleName, 6, "Hearing Schedule is already in open status for selected reference no.")
            Language.setMessage(mstrModuleName, 37, "Select")
            Language.setMessage(mstrModuleName, 100, "Sent")
            Language.setMessage(mstrModuleName, 101, "Pending")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class