﻿'************************************************************************************************************************************
'Class Name : clsUserDefineReport.vb
'Purpose    :
'Date       :12-Jun-2012
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsUserDefineReport
    Private Const mstrModuleName = "clsUserDefineReport"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintReportunkid As Integer
    Private mstrTemplate_Name As String = String.Empty
    Private mstrTemplate_Query As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mintUserunkid As Integer
    Private mstrDatabase_Name As String = String.Empty
    Private mstrTemplate_Master As String = String.Empty
    Private mstrTemplate_Detail As String = String.Empty
    Private mstrTemplate_Formula As String = String.Empty

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reportunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Reportunkid() As Integer
        Get
            Return mintReportunkid
        End Get
        Set(ByVal value As Integer)
            mintReportunkid = value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set template_name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Template_Name() As String
        Get
            Return mstrTemplate_Name
        End Get
        Set(ByVal value As String)
            mstrTemplate_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set template_query
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Template_Query() As String
        Get
            Return mstrTemplate_Query
        End Get
        Set(ByVal value As String)
            mstrTemplate_Query = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set database_name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Database_Name() As String
        Get
            Return mstrDatabase_Name
        End Get
        Set(ByVal value As String)
            mstrDatabase_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set template_master
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Template_Master() As String
        Get
            Return mstrTemplate_Master
        End Get
        Set(ByVal value As String)
            mstrTemplate_Master = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set template_detail
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Template_Detail() As String
        Get
            Return mstrTemplate_Detail
        End Get
        Set(ByVal value As String)
            mstrTemplate_Detail = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set template_formula
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Template_Formula() As String
        Get
            Return mstrTemplate_Formula
        End Get
        Set(ByVal value As String)
            mstrTemplate_Formula = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  reportunkid " & _
                   ", template_name " & _
                   ", template_query " & _
                   ", userunkid " & _
                   ", database_name " & _
                   ", template_master " & _
                   ", template_detail " & _
                   ", template_formula " & _
                   ", isactive " & _
                   "FROM hrmsConfiguration..cfuserdefine_reports " & _
                   "WHERE reportunkid = @reportunkid "

            objDataOperation.AddParameter("@reportunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintReportUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintreportunkid = CInt(dtRow.Item("reportunkid"))
                mstrtemplate_name = dtRow.Item("template_name").ToString
                mstrTemplate_Query = dtRow.Item("template_query").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrDatabase_Name = dtRow.Item("database_name").ToString
                mstrTemplate_Master = dtRow.Item("template_master").ToString
                mstrTemplate_Detail = dtRow.Item("template_detail").ToString
                mstrTemplate_Formula = dtRow.Item("template_formula").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal intReportId As Integer = -1, _
                            Optional ByVal StrTemplateName As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  reportunkid " & _
                   ", template_name " & _
                   ", template_query " & _
                   ", userunkid " & _
                   ", database_name " & _
                   ", template_master " & _
                   ", template_detail " & _
                   ", template_formula " & _
                   ", isactive " & _
                   "FROM hrmsConfiguration..cfuserdefine_reports " & _
                   "WHERE 1 = 1 "

            If blnOnlyActive Then
                strQ &= " AND isactive = 1 "
            End If

            If intReportId > 0 Then
                strQ &= " AND reportunkid = '" & intReportId & "'"
            End If

            If StrTemplateName.Trim.Length > 0 Then
                strQ &= " AND template_name LIKE '" & StrTemplateName & "'"
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cfuserdefine_reports) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If isExist(mintUserunkid, mstrTemplate_Name) = True Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Template name is already defined. Please define new Template name.")
            Return False
        End If

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        mintReportunkid = GetNextNumber(objDataOperation)

        Try
            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReportunkid.ToString)
            objDataOperation.AddParameter("@template_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTemplate_Name.ToString)
            objDataOperation.AddParameter("@template_query", SqlDbType.VarChar, mstrTemplate_Query.Length, mstrTemplate_Query.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@database_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDatabase_Name.ToString)
            objDataOperation.AddParameter("@template_master", SqlDbType.VarChar, mstrTemplate_Master.Length, mstrTemplate_Master.ToString)
            objDataOperation.AddParameter("@template_detail", SqlDbType.VarChar, mstrTemplate_Detail.Length, mstrTemplate_Detail.ToString)
            objDataOperation.AddParameter("@template_formula", SqlDbType.VarChar, mstrTemplate_Formula.Length, mstrTemplate_Formula.ToString)

            strQ = "INSERT INTO hrmsConfiguration..cfuserdefine_reports ( " & _
                       "  reportunkid " & _
                       ", template_name " & _
                       ", template_query " & _
                       ", userunkid " & _
                       ", database_name " & _
                       ", template_master " & _
                       ", template_detail " & _
                       ", template_formula " & _
                       ", isactive" & _
                   ") VALUES (" & _
                       "  @reportunkid " & _
                       ", @template_name " & _
                       ", @template_query " & _
                       ", @userunkid " & _
                       ", @database_name " & _
                       ", @template_master " & _
                       ", @template_detail " & _
                       ", @template_formula " & _
                       ", @isactive " & _
                   ")"

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrmsConfiguration..cfuserdefine_reports", "reportunkid", mintReportunkid, True) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cfuserdefine_reports) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If isExist(mintUserunkid, mstrTemplate_Name, mintReportunkid) = True Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Template name is already defined. Please define new Template name.")
            Return False
        End If

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@reportunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintreportunkid.ToString)
            objDataOperation.AddParameter("@template_name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrtemplate_name.ToString)
            objDataOperation.AddParameter("@template_query", SqlDbType.VarChar, mstrTemplate_Query.Length, mstrTemplate_Query.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@database_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDatabase_Name.ToString)
            objDataOperation.AddParameter("@template_master", SqlDbType.VarChar, mstrTemplate_Master.Length, mstrTemplate_Master.ToString)
            objDataOperation.AddParameter("@template_detail", SqlDbType.VarChar, mstrTemplate_Detail.Length, mstrTemplate_Detail.ToString)
            objDataOperation.AddParameter("@template_formula", SqlDbType.VarChar, mstrTemplate_Formula.Length, mstrTemplate_Formula.ToString)

            strQ = "UPDATE hrmsConfiguration..cfuserdefine_reports SET " & _
                   "  template_name = @template_name " & _
                   ", template_query = @template_query " & _
                   ", userunkid = @userunkid " & _
                   ", database_name = @database_name" & _
                   ", template_master = @template_master" & _
                   ", template_detail = @template_detail" & _
                   ", template_formula = @template_formula" & _
                   ", isactive = @isactive " & _
                   "WHERE reportunkid = @reportunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsConfigTableDataUpdate("atconfigcommon_log", "hrmsConfiguration..cfuserdefine_reports", mintReportunkid, "reportunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrmsConfiguration..cfuserdefine_reports", "reportunkid", mintReportunkid, True) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfuserdefine_reports) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "UPDATE hrmsConfiguration..cfuserdefine_reports SET " & _
                    "isactive = 0 " & _
                   "WHERE reportunkid = @reportunkid "

            objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intUserId As Integer, ByVal StrTemplateName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  reportunkid " & _
                   ", template_name " & _
                   ", template_query " & _
                   ", userunkid " & _
                   ", isactive " & _
                   "FROM hrmsConfiguration..cfuserdefine_reports " & _
                   "WHERE template_name = @template_name " & _
                   "AND userunkid = @userunkid " & _
                   "AND isactive = 1 "

            If intUnkid > 0 Then
                strQ &= " AND reportunkid <> @reportunkid"
                objDataOperation.AddParameter("@reportunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)
            objDataOperation.AddParameter("@template_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrTemplateName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cfuserdefine_reports) </purpose>
    Private Function GetNextNumber(ByVal objDataOp As clsDataOperation) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intNxtNumber As Integer = 0
        Try
            strQ = " SELECT ISNULL(MAX(reportunkid),9000) AS NUM FROM hrmsConfiguration..cfuserdefine_reports "

            dsList = objDataOp.ExecQuery(strQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intNxtNumber = CInt(dsList.Tables("List").Rows(0)("NUM")) + 1
            End If

            Return intNxtNumber

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetNextNumber; Module Name: " & mstrModuleName)
            Return -1
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function Execute_Report_Query(ByVal StrQ As String) As DataSet
        Dim dsList As New DataSet
        Dim exForce As Exception
        Dim StrMessage As String = String.Empty
        Try
            objDataOperation = New clsDataOperation

            dsList = objDataOperation.ExecQuery(StrQ, "Table")

            If objDataOperation.ErrorMessage <> "" Then
                If objDataOperation.ErrorMessage.Contains("SqlException") Then
                    StrMessage = objDataOperation.ErrorMessage.ToString.Substring(14, objDataOperation.ErrorMessage.ToString.IndexOf("."c) - (14 - 1))
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            If StrMessage <> "" Then
                eZeeMsgBox.Show(StrMessage, enMsgBoxStyle.Information)
            Else
                DisplayError.Show("-1", ex.Message, "Execute_Report_Query", mstrModuleName)
            End If
            Return Nothing
        End Try
    End Function

End Class
