'************************************************************************************************************************************
'Class Name : clsGRMLetterType.vb
'Purpose    :
'Date       : 15 Nov 2008
'Written By : Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsLetterType
    Private Shared ReadOnly mstrModuleName As String = "clsLetterType"

#Region " Private variables "
    Private mintLettertypeUnkId As Integer
    Private mstrAlias As String
    Private mintLettergroupmasterUnkId As Integer
    Private mstrLetterName As String
    Private mstrLetterName1 As String
    Private mstrLetterName2 As String
    Private mstrLettercontent As String
    Private mblnIsActive As Boolean = True
    Private mintFieldtypeid As Integer

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get or Set LettertypeUnkId
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _LettertypeUnkId() As Integer
        Get
            Return mintLettertypeUnkId
        End Get
        Set(ByVal value As Integer)
            mintLettertypeUnkId = value
            getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Alias
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Alias() As String
        Get
            Return mstrAlias
        End Get
        Set(ByVal value As String)
            mstrAlias = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LettergroupmasterUnkId
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _LettergroupmasterUnkId() As Integer
        Get
            Return mintLettergroupmasterUnkId
        End Get
        Set(ByVal value As Integer)
            mintLettergroupmasterUnkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LetterName
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _LetterName() As String
        Get
            Return mstrLetterName
        End Get
        Set(ByVal value As String)
            mstrLetterName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LetterName1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _LetterName1() As String
        Get
            Return mstrLetterName1
        End Get
        Set(ByVal value As String)
            mstrLetterName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LetterName2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _LetterName2() As String
        Get
            Return mstrLetterName2
        End Get
        Set(ByVal value As String)
            mstrLetterName2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Lettercontent
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Lettercontent() As String
        Get
            Return mstrLettercontent
        End Get
        Set(ByVal value As String)
            mstrLettercontent = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsActive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _IsActive() As Boolean
        Get
            Return mblnIsActive
        End Get
        Set(ByVal value As Boolean)
            mblnIsActive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldtypeid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fieldtypeid() As Integer
        Get
            Return mintFieldtypeid
        End Get
        Set(ByVal value As Integer)
            mintFieldtypeid = Value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub getData()
        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                       " lettertypeunkid " & _
                       ", alias " & _
                       ", lettermasterunkid " & _
                       ", lettername " & _
                       ", lettername1 " & _
                       ", lettername2 " & _
                       ", lettercontent " & _
                       ", isactive " & _
                       ", ISNULL(fieldtypeid,1) AS fieldtypeid " & _
                   "FROM hrlettertype_master " & _
                   "WHERE lettertypeunkid = @lettertypeunkid "

            objDataOperation.AddParameter("@lettertypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLettertypeUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLettertypeUnkId = dtRow.Item("lettertypeunkid")
                mstrAlias = dtRow.Item("alias")
                mintLettergroupmasterUnkId = dtRow.Item("lettermasterunkid")
                mstrLetterName = dtRow.Item("lettername")
                mstrLetterName1 = dtRow.Item("lettername1")
                mstrLetterName2 = dtRow.Item("lettername2")
                'S.SANDEEP [ 04 MAY 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'mstrLettercontent = dtRow.Item("lettercontent")
                Dim xmldoc As New System.Xml.XmlDocument
                xmldoc.LoadXml(dtRow.Item("lettercontent"))
                mstrLettercontent = xmldoc.InnerText
                'S.SANDEEP [ 04 MAY 2012 ] -- END
                mblnIsActive = dtRow.Item("isactive")
                mintFieldtypeid = CInt(dtRow.Item("fieldtypeid"))
                Exit For
            Next
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "Get_GRMlettertype", mstrModuleName)
        Finally
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (grmlettertype) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrLetterName) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This letter type is already defined. Please define another letter type."), enMsgBoxStyle.Information)
            Return False
        End If

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim StrXMLVALUE As String = ""
        Dim objDataOperation As New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@alias", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAlias.ToString)
            objDataOperation.AddParameter("@lettermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLettergroupmasterUnkId.ToString)
            objDataOperation.AddParameter("@lettername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLetterName.ToString)
            objDataOperation.AddParameter("@lettername1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLetterName1.ToString)
            objDataOperation.AddParameter("@lettername2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLetterName2.ToString)
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@lettercontent", SqlDbType.NVarChar, 4000, mstrLettercontent.ToString)


            'S.SANDEEP [ 04 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@lettercontent", SqlDbType.VarChar, 8000, mstrLettercontent.ToString)
            mstrLettercontent = System.Security.SecurityElement.Escape(mstrLettercontent)
            StrXMLVALUE = "<hrlettertype_master xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbCrLf & _
                          "<lettercontent>" & mstrLettercontent & "</lettercontent>" & vbCrLf & _
                          "</hrlettertype_master>"

            objDataOperation.AddParameter("@lettercontent", SqlDbType.Xml, StrXMLVALUE.Length, StrXMLVALUE)
            'S.SANDEEP [ 04 MAY 2012 ] -- END



            'S.SANDEEP [ 07 NOV 2011 ] -- END
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsActive.ToString)
            objDataOperation.AddParameter("@fieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldtypeid.ToString)

            strQ = "INSERT INTO hrlettertype_master ( " & _
                            "  alias " & _
                            ", lettermasterunkid " & _
                            ", lettername " & _
                            ", lettername1 " & _
                            ", lettername2 " & _
                            ", lettercontent " & _
                            ", isactive " & _
                            ", fieldtypeid" & _
                        ") VALUES (" & _
                            "  @alias " & _
                            ", @lettermasterunkid " & _
                            ", @lettername " & _
                            ", @lettername1 " & _
                            ", @lettername2 " & _
                            ", @lettercontent " & _
                            ", @isactive " & _
                            ", @fieldtypeid" & _
                        "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLettertypeUnkId = dsList.Tables(0).Rows(0).Item(0)



            'S.SANDEEP [ 04 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            ''S.SANDEEP [ 12 OCT 2011 ] -- START
            ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrlettertype_master", "lettertypeunkid", mintLettertypeUnkId) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'objDataOperation.ReleaseTransaction(True)
            ''S.SANDEEP [ 12 OCT 2011 ] -- END 

            If InsertAuditTrail(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 04 MAY 2012 ] -- END

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            DisplayError.Show(-1, ex.Message, "Insert", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (grmlettertype) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrLetterName, mintLettertypeUnkId) Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "This letter type is already defined. Please define another letter type."), enMsgBoxStyle.Information)
            Return False
        End If

        Dim dsList As New DataSet
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation


        'S.SANDEEP [ 04 MAY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Dim StrXMLVALUE As String = ""
        'S.SANDEEP [ 04 MAY 2012 ] -- END


        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@lettertypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLettertypeUnkId.ToString)
            objDataOperation.AddParameter("@alias", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAlias.ToString)
            objDataOperation.AddParameter("@lettermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLettergroupmasterUnkId.ToString)
            objDataOperation.AddParameter("@lettername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLetterName.ToString)
            objDataOperation.AddParameter("@lettername1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLetterName1.ToString)
            objDataOperation.AddParameter("@lettername2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLetterName2.ToString)
            'S.SANDEEP [ 07 NOV 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@lettercontent", SqlDbType.NVarChar, 4000, mstrLettercontent.ToString)

            'S.SANDEEP [ 04 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@lettercontent", SqlDbType.VarChar, 8000, mstrLettercontent.ToString)

            mstrLettercontent = System.Security.SecurityElement.Escape(mstrLettercontent)

            StrXMLVALUE = "<hrlettertype_master xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbCrLf & _
                          "<lettercontent>" & mstrLettercontent & "</lettercontent>" & vbCrLf & _
                          "</hrlettertype_master>"

            objDataOperation.AddParameter("@lettercontent", SqlDbType.Xml, StrXMLVALUE.Length, StrXMLVALUE)
            'S.SANDEEP [ 04 MAY 2012 ] -- END

            'S.SANDEEP [ 07 NOV 2011 ] -- END
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsActive.ToString)
            objDataOperation.AddParameter("@fieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldtypeid.ToString)

            strQ = "UPDATE hrlettertype_master SET " & _
                         "  alias = @alias" & _
                         ", lettermasterunkid = @lettermasterunkid" & _
                         ", lettername = @lettername" & _
                         ", lettername1 = @lettername1" & _
                         ", lettername2 = @lettername2" & _
                         ", lettercontent = @lettercontent" & _
                         ", isactive = @isactive" & _
                         ", fieldtypeid = @fieldtypeid " & _
                         ", Syncdatetime = NULL " & _
                       "WHERE lettertypeunkid = @lettertypeunkid "
            'Hemant (30 Oct 2019) -- [Syncdatetime = NULL]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            'S.SANDEEP [ 04 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            ''S.SANDEEP [ 12 OCT 2011 ] -- START
            ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
            'If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrlettertype_master", mintLettertypeUnkId, "lettertypeunkid", 2) Then
            '    If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrlettertype_master", "lettertypeunkid", mintLettertypeUnkId) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If
            'objDataOperation.ReleaseTransaction(True)
            ''S.SANDEEP [ 12 OCT 2011 ] -- END 

            _LettertypeUnkId = mintLettertypeUnkId

            If InsertAuditTrail(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            'S.SANDEEP [ 04 MAY 2012 ] -- END

            objDataOperation.ReleaseTransaction(True)
            

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            DisplayError.Show(-1, ex.Message, "Update", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            dsList = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Purpose  : Delete Item
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <param name="intLetterTypeId"> </param>
    ''' <returns>Boolean</returns>
    ''' <purpose> </purpose>
    Public Function Delete(ByVal intLetterTypeId As Integer) As Boolean

        Dim strErrorMessage As String = ""
        Dim strQ As String
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try

            strQ = "UPDATE hrlettertype_master SET " & _
                       "  isactive=0 " & _
                      "WHERE lettertypeunkid = @lettertypeId "

            objDataOperation.AddParameter("@lettertypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intLetterTypeId)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            'S.SANDEEP [ 04 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            ''S.SANDEEP [ 12 OCT 2011 ] -- START
            ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE  
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrlettertype_master", "lettertypeunkid", intLetterTypeId) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'objDataOperation.ReleaseTransaction(True)
            ''S.SANDEEP [ 12 OCT 2011 ] -- END

            _LettertypeUnkId = intLetterTypeId

            If InsertAuditTrail(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP [ 04 MAY 2012 ] -- END

            objDataOperation.ReleaseTransaction(True)

           

            Return True

        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            DisplayError.Show(-1, ex.Message, "Delete", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Purpose  : Checking for Letter Type whether it Exist or not
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <param name="strName"> </param>
    ''' <param name="intLetterTypeUnkid"> </param>
    ''' <returns>Boolean</returns>
    ''' <purpose> </purpose>
    Public Function isExist(ByVal strName As String, _
                            Optional ByVal intLetterTypeUnkid As Integer = -1) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = "SELECT lettertypeunkid " & _
                       ", lettermasterunkid " & _
                       ", lettername " & _
                       ", lettername1 " & _
                       ", lettername2 " & _
                       ", lettercontent " & _
                       ", isactive " & _
                   "FROM hrlettertype_master " & _
                   "WHERE lettername = @LetterName " & _
                   "AND isactive=1  "

            If intLetterTypeUnkid <> -1 Then
                strQ = strQ & " AND lettertypeunkid <> @LetterTypeunkid "
                objDataOperation.AddParameter("@LetterTypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLetterTypeUnkid)
            End If

            objDataOperation.AddParameter("@LetterName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return (dsList.Tables(0).Rows.Count > 0)

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "isExist", mstrModuleName)
            Return True
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Purpose  : Checking for Letter Type whether is used or not
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <param name="intLetterTypeUnkId"> </param>
    ''' <returns>Boolean</returns>
    ''' <purpose> </purpose>
    Public Function isUsed(ByVal intLetterTypeUnkId As Integer) As Boolean

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Try

            strQ = "SELECT lettertypeunkid AS id  " & _
                   "FROM hremail_master " & _
                   "WHERE lettertypeunkid = @Letterunkid  "



            'Pinkal (18-Apr-2013) -- Start
            'Enhancement : TRA Changes

            strQ &= " UNION " & _
                         "SELECT ISNULL(key_value,'') id FROM hrmsConfiguration..cfconfiguration WHERE companyunkid = " & Company._Object._Companyunkid & _
                         " AND (UPPER(key_name) = 'ERC_EOC_TEMPLATEID' OR  UPPER(key_name) = 'ERC_RET_TEMPLATEID' " & _
                         " OR  UPPER(key_name) = 'ERC_CNF_TEMPLATEID' OR  UPPER(key_name) ='ERC_PED_TEMPLATEID' " & _
                         " OR  UPPER(key_name) = 'ERC_BRT_TEMPLATEID') AND ISNULL(key_value,'')= '" & intLetterTypeUnkId & "'"

            'Pinkal (18-Apr-2013) -- End


            objDataOperation.AddParameter("@Letterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLetterTypeUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If (dsList.Tables(0).Rows.Count > 0) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "isUsed", mstrModuleName)
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing
            exForce = Nothing
        End Try

    End Function

    'S.SANDEEP [ 04 MAY 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function InsertAuditTrail(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim StrXMLVALUE As String = String.Empty
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@lettertypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLettertypeUnkId.ToString)
            objDataOperation.AddParameter("@alias", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAlias.ToString)
            objDataOperation.AddParameter("@lettermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLettergroupmasterUnkId.ToString)
            objDataOperation.AddParameter("@lettername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLetterName.ToString)
            objDataOperation.AddParameter("@lettername1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLetterName1.ToString)
            objDataOperation.AddParameter("@lettername2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLetterName2.ToString)

            mstrLettercontent = System.Security.SecurityElement.Escape(mstrLettercontent)

            StrXMLVALUE = "<hrlettertype_master xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbCrLf & _
                          "<lettercontent>" & mstrLettercontent & "</lettercontent>" & vbCrLf & _
                          "</hrlettertype_master>"
            objDataOperation.AddParameter("@lettercontent", SqlDbType.Xml, StrXMLVALUE.Length, StrXMLVALUE)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsActive.ToString)
            objDataOperation.AddParameter("@fieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldtypeid.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            'S.SANDEEP [ 19 JULY 2012 ] -- END



            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'StrQ = "INSERT INTO athrlettertype_master ( " & _
            '                "  alias " & _
            '                ", lettermasterunkid " & _
            '                ", lettername " & _
            '                ", lettername1 " & _
            '                ", lettername2 " & _
            '                ", lettercontent " & _
            '                ", isactive " & _
            '                ", fieldtypeid " & _
            '                ", audittype " & _
            '                ", audituserunkid " & _
            '                ", auditdatetime " & _
            '                ", ip " & _
            '                ", machine_name " & _
            '            ") VALUES (" & _
            '                "  @alias " & _
            '                ", @lettermasterunkid " & _
            '                ", @lettername " & _
            '                ", @lettername1 " & _
            '                ", @lettername2 " & _
            '                ", @lettercontent " & _
            '                ", @isactive " & _
            '                ", @fieldtypeid " & _
            '                ", @audittype " & _
            '                ", @audituserunkid " & _
            '                ", @auditdatetime " & _
            '                ", @ip " & _
            '                ", @machine_name " & _
            '            "); SELECT @@identity"

            StrQ = "INSERT INTO athrlettertype_master ( " & _
                            "  alias " & _
                            ", lettermasterunkid " & _
                            ", lettername " & _
                            ", lettername1 " & _
                            ", lettername2 " & _
                            ", lettercontent " & _
                            ", isactive " & _
                            ", fieldtypeid " & _
                            ", audittype " & _
                            ", audituserunkid " & _
                            ", auditdatetime " & _
                            ", ip " & _
                            ", machine_name " & _
                          ", form_name " & _
                          ", module_name1 " & _
                          ", module_name2 " & _
                          ", module_name3 " & _
                          ", module_name4 " & _
                          ", module_name5 " & _
                          ", isweb " & _
                        ") VALUES (" & _
                            "  @alias " & _
                            ", @lettermasterunkid " & _
                            ", @lettername " & _
                            ", @lettername1 " & _
                            ", @lettername2 " & _
                            ", @lettercontent " & _
                            ", @isactive " & _
                            ", @fieldtypeid " & _
                            ", @audittype " & _
                            ", @audituserunkid " & _
                            ", @auditdatetime " & _
                            ", @ip " & _
                            ", @machine_name " & _
                          ", @form_name " & _
                          ", @module_name1 " & _
                          ", @module_name2 " & _
                          ", @module_name3 " & _
                          ", @module_name4 " & _
                          ", @module_name5 " & _
                          ", @isweb " & _
                        "); SELECT @@identity"

            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "InsertAuditTrail", mstrModuleName)
            Return False
        Finally
        End Try
    End Function
    'S.SANDEEP [ 04 MAY 2012 ] -- END

#Region " List "
    ''' <summary>
    ''' Purpose   : Get a List for Letter Type
    ''' Modify by : Sandeep J. Sharma
    ''' </summary>
    ''' <param name="strListName"></param>
    ''' <returns>Dataset</returns>
    ''' <remarks></remarks>
    Public Function getList(ByVal intMasterTypeID As Integer, Optional ByVal strListName As String = "List", _
                            Optional ByVal intLanguageID As Integer = -1 _
                            , Optional ByVal intUnkId As Integer = 0 _
                            , Optional ByVal intLetterMasterUnkId As Integer = 0 _
                            ) As DataSet
        'Sohail (25 Sep 2020) - [intUnkId, intLetterMasterUnkId]

        Dim strQ As String
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation

        Try

            If Not intLanguageID > 0 Then
                ' intLanguageID = User._Object._LanguageId
            End If

            strQ = "SELECT hrlettertype_master.lettertypeunkid " & _
                        ", hrlettertype_master.lettermasterunkid" & _
                        ", cfcommon_master.name" & _
                        ", CASE @Language WHEN 1 THEN hrlettertype_master.lettername1 " & _
                                          "WHEN 2 THEN hrlettertype_master.lettername2 " & _
                                          "ELSE hrlettertype_master.lettername END AS lettertypename " & _
                        ", hrlettertype_master.lettercontent " & _
                        ", hrlettertype_master.isactive " & _
                        ", hrlettertype_master.fieldtypeid " & _
                    " FROM hrlettertype_master, " & _
                        "cfcommon_master  " & _
                   "WHERE hrlettertype_master.lettermasterunkid = cfcommon_master.masterunkid " & _
                   "AND cfcommon_master.mastertype = @mastertypeId  " & _
                   "AND hrlettertype_master.isactive=1 "
            'Sohail (25 Sep 2020) - [fieldtypeid]

            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-75 #  : Search job page with apply button in ESS (same as MSS, only current open vacancies).
            If intUnkId > 0 Then
                strQ &= " AND hrlettertype_master.lettertypeunkid = @lettertypeunkid "
                objDataOperation.AddParameter("@lettertypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId)
            End If

            If intLetterMasterUnkId > 0 Then
                strQ &= " AND hrlettertype_master.lettermasterunkid = @lettermasterunkid "
                objDataOperation.AddParameter("@lettermasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLetterMasterUnkId)
            End If
            'Sohail (25 Sep 2020) -- End

            strQ &= "ORDER BY hrlettertype_master.lettermasterunkid "


            objDataOperation.AddParameter("@mastertypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, enCommonMaster.LETTER_TYPE)
            objDataOperation.AddParameter("@Language", SqlDbType.Int, eZeeDataType.INT_SIZE, intLanguageID)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing
            exForce = Nothing
        End Try
    End Function

    'S.SANDEEP [ 08 APR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function getComboList(Optional ByVal blnFlag As Boolean = False, Optional ByVal iLetterTypeId As Integer = 0, Optional ByVal iCompanyId As Integer = 0) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim sDataBase As String = String.Empty
        Try
            Dim objDataOperation As New clsDataOperation

            If iCompanyId > 0 Then
                'Pinkal (31-Mar-2023) -- Start
                '(A1X-717) NMB - As a user, based on the data displayed from the selection, I want to be able to shoot emails from the Feedback screen instead of going to the mail utility again. I should be able to select the template directly from this screen and input the subject.
                'StrQ = "SELECT database_name FROM cffinancial_year_tran WHERE companyunkid = '" & iCompanyId & "' AND isclosed = 0 "
                StrQ = "SELECT database_name FROM hrmsconfiguration..cffinancial_year_tran WHERE companyunkid = '" & iCompanyId & "' AND isclosed = 0 "
                'Pinkal (31-Mar-2023) -- End

                Dim dData As New DataSet
                dData = objDataOperation.ExecQuery(StrQ, "List")
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If dData.Tables(0).Rows.Count > 0 Then sDataBase = dData.Tables(0).Rows(0).Item("database_name")
            End If
            If sDataBase = "" Then sDataBase = FinancialYear._Object._DatabaseName
            StrQ = ""
            If blnFlag = True Then
                StrQ = "SELECT 0 AS Id, @Select AS name UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            End If
            StrQ &= "SELECT lettertypeunkid AS Id ,lettername AS name FROM " & sDataBase & "..hrlettertype_master WHERE isactive = 1 "
            If iLetterTypeId > 0 Then
                StrQ &= " AND lettermasterunkid = '" & iLetterTypeId & "' "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP [ 08 APR 2013 ] -- END

    'Hemant (07 Oct 2019) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 14 : On interview scheduling, system should be able to send automatic emails to applicants that have been placed in a batch. The content of the email should be picked from a template set on configuration.)
    Public Function getComboListFromFieldType(Optional ByVal blnFlag As Boolean = False, Optional ByVal iFieldTypeId As Integer = 0, Optional ByVal iCompanyId As Integer = 0) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim sDataBase As String = String.Empty
        Try
            Dim objDataOperation As New clsDataOperation

            If iCompanyId > 0 Then

                'Pinkal (31-Mar-2023) -- Start
                '(A1X-717) NMB - As a user, based on the data displayed from the selection, I want to be able to shoot emails from the Feedback screen instead of going to the mail utility again. I should be able to select the template directly from this screen and input the subject.
                'StrQ = "SELECT database_name FROM cffinancial_year_tran WHERE companyunkid = '" & iCompanyId & "' AND isclosed = 0 "
                StrQ = "SELECT database_name FROM hrmsconfiguration..cffinancial_year_tran WHERE companyunkid = '" & iCompanyId & "' AND isclosed = 0 "
                'Pinkal (31-Mar-2023) -- End

                Dim dData As New DataSet
                dData = objDataOperation.ExecQuery(StrQ, "List")
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                If dData.Tables(0).Rows.Count > 0 Then sDataBase = dData.Tables(0).Rows(0).Item("database_name")
            End If
            If sDataBase = "" Then sDataBase = FinancialYear._Object._DatabaseName
            StrQ = ""
            If blnFlag = True Then
                StrQ = "SELECT 0 AS Id, @Select AS name UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            End If
            StrQ &= "SELECT lettertypeunkid AS Id ,lettername AS name FROM " & sDataBase & "..hrlettertype_master WHERE isactive = 1 "
            If iFieldTypeId > 0 Then
                StrQ &= " AND fieldtypeid = '" & iFieldTypeId & "' "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'Hemant (07 Oct 2019) -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This letter type is already defined. Please define another letter type.")
			Language.setMessage(mstrModuleName, 2, "WEB")
			Language.setMessage(mstrModuleName, 3, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
