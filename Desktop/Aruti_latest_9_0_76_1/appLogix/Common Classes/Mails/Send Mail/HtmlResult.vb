﻿Imports System.Collections.Generic
Imports System.IO
Imports eZeeCommonLib

Public Class HtmlResult
    Private HTML As String
    Private Content As Dictionary(Of String, Byte())

    Public Sub New()
        Html = String.Empty
        Content = New Dictionary(Of String, Byte())()
    End Sub

    Public Property _HTML() As String
        Get
            Return HTML
        End Get
        Set(ByVal value As String)
            HTML = value
        End Set
    End Property

    Public ReadOnly Property _Content() As Dictionary(Of String, Byte())
        Get
            Return Content
        End Get
    End Property

    Public Sub WriteToFile(ByVal fileName As String)
        'S.SANDEEP [02-NOV-2018] -- START
        'File.WriteAllText(fileName, HTML)
        'Dim fileInfo = New FileInfo(fileName)
        Dim StrPath As String = System.IO.Path.GetTempPath()
        File.WriteAllText(Path.Combine(StrPath, fileName), HTML)
        Dim fileInfo = New FileInfo(Path.Combine(StrPath, fileName))
        'S.SANDEEP [02-NOV-2018] -- END
        For Each content_Renamed In Content
            Dim contentFileName = Path.GetFullPath(Path.Combine(fileInfo.DirectoryName, content_Renamed.Key))
            Dim contentPath = Path.GetDirectoryName(contentFileName)
            If Not Directory.Exists(contentPath) Then
                Directory.CreateDirectory(contentPath)
            End If            
            File.WriteAllBytes(contentFileName, content_Renamed.Value)
        Next
    End Sub

End Class
