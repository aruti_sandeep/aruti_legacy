﻿Imports System
Imports System.IO
Imports System.Reflection
Imports System.Windows

Public Class RtfToXamlConverter
    Private Sub New()
    End Sub

    Public Shared Function RtfContainsImage(ByVal rtfText As String) As Boolean
        ' cf.: http://www.biblioscape.com/rtf15_spec.htm#Heading49
        Return (Not String.IsNullOrEmpty(rtfText)) AndAlso rtfText.Contains("\pict")
    End Function

    ''' <summary>
    ''' Converts the specified RTF string into a Xaml package.
    ''' </summary>
    ''' <param name="rtfContent">The RTF content to convert to XAML</param>
    ''' <returns>A zipped stream containing a full xaml package; or null</returns>
    Public Shared Function RtfToXamlPackage(ByVal rtfContent As String) As MemoryStream
        If String.IsNullOrEmpty(rtfContent) Then
            Return Nothing
        End If
        Return DirectCast(TextEditorCopyPaste_ConvertRtfToXaml.Invoke(Nothing, New Object() {rtfContent}), MemoryStream)
    End Function

    ''' <summary>
    ''' Converts the specified Xaml content string into an RTF string.
    ''' </summary>
    Public Shared Function XamlToRtf(ByVal xamlContent As String, Optional ByVal wpfContainerMemory As Stream = Nothing) As String
        If String.IsNullOrEmpty(xamlContent) Then
            Return String.Empty
        End If
        Return DirectCast(TextEditorCopyPaste_ConvertXamlToRtf.Invoke(Nothing, New Object() {xamlContent, wpfContainerMemory}), String)
    End Function

    ''' <summary>
    ''' Converts the specified RTF string into a Xaml Flow document.
    ''' </summary>
    ''' <param name="rtfContent">The RTF content to convert to XAML</param>
    ''' <returns>A Xaml string</returns>
    ''' <remarks>Images and other content in the RTF are lost in the resulting Xaml content.</remarks>
    Public Shared Function RtfToXaml(ByVal rtfContent As String) As String
        If String.IsNullOrEmpty(rtfContent) Then
            Return String.Empty
        End If
        Return DirectCast(XamlRtfConverterType_ConvertRtfToXaml.Invoke(XamlRtfConverter, New Object() {rtfContent}), String)
    End Function

    Private Shared ReadOnly PresentationFrameworkAssembly As System.Reflection.Assembly = System.Reflection.Assembly.GetAssembly(GetType(FrameworkElement))
    Private Shared ReadOnly TextEditorCopyPasteType As Type = PresentationFrameworkAssembly.GetType("System.Windows.Documents.TextEditorCopyPaste")

    Private Shared ReadOnly XamlRtfConverterType As Type = PresentationFrameworkAssembly.GetType("System.Windows.Documents.XamlRtfConverter")

    ' ReSharper disable InconsistentNaming
    Private Shared ReadOnly TextEditorCopyPaste_ConvertRtfToXaml As MethodInfo = TextEditorCopyPasteType.GetMethod("ConvertRtfToXaml", BindingFlags.Static Or BindingFlags.NonPublic)

    Private Shared ReadOnly TextEditorCopyPaste_ConvertXamlToRtf As MethodInfo = TextEditorCopyPasteType.GetMethod("ConvertXamlToRtf", BindingFlags.Static Or BindingFlags.NonPublic)

    Private Shared ReadOnly XamlRtfConverterType_ConvertRtfToXaml As MethodInfo = XamlRtfConverterType.GetMethod("ConvertRtfToXaml", BindingFlags.Instance Or BindingFlags.NonPublic)

    Private Shared ReadOnly XamlRtfConverter As Object = Activator.CreateInstance(XamlRtfConverterType, BindingFlags.NonPublic Or BindingFlags.Instance, Nothing, Nothing, Nothing)
    ' ReSharper restore InconsistentNaming
End Class
