﻿'************************************************************************************************************************************
'Class Name :clsReportingToEmployee.vb
'Purpose    :
'Date       :07/09/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.DirectoryServices
Imports System.DirectoryServices.Protocols

#End Region

Public Class clsReportingToEmployee

#Region " Private Variable "

    Private Shared ReadOnly mstrModuleName As String = "clsReportingToEmployee"
    Private objDataOperation As clsDataOperation
    Private mstrMessage As String = ""
    Private mdtTran As DataTable
    Private mintEmployeeId As Integer

    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Private mintUserId As Integer = 0
    Private mdtVoiddatetime As Date
    'Gajanan [23-SEP-2019] -- End


    'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
    Private mdtAuditdatetime As Date
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHost As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    'Gajanan [24-OCT-2019] -- End

#End Region

#Region " Properties "
    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
    Public Property _EmployeeUnkid(ByVal mdtEmployeeAsonDate As Date, Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal blnIsAllData As Boolean = False) As Integer
        'Hemant (03 Feb 2023) -- [blnIsAllData]
        'Pinkal (20-Sep-2022) -- NMB Loan Module Enhancement.[Optional ByVal objDataOpr As clsDataOperation = Nothing]
        Get
            Return mintEmployeeId
        End Get
        Set(ByVal value As Integer)
            mintEmployeeId = value
            'Pinkal (20-Sep-2022) -- Start
            'NMB Loan Module Enhancement.
            'Call Get_ReportingList(mdtEmployeeAsonDate)
            Call Get_ReportingList(mdtEmployeeAsonDate, objDataOpr, blnIsAllData)
            'Hemant (03 Feb 2023) -- [blnIsAllData]
            'Pinkal (20-Sep-2022) -- End
        End Set
    End Property
'Pinkal (18-Aug-2018) -- End
    Public Property _RDataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property


    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala.
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    'Pinkal (18-Aug-2018) -- End


    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Public Property _UserId() As Integer
        Get
            Return mintUserId
        End Get
        Set(ByVal value As Integer)
            mintUserId = value
        End Set
    End Property

    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property
    'Gajanan [23-SEP-2019] -- End



    'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = value
        End Set
    End Property

    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = value
        End Set
    End Property

    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    'Gajanan [24-OCT-2019] -- End

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("DList")
            Dim dCol As DataColumn

            dCol = New DataColumn
            dCol.ColumnName = "reporttounkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "employeeunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "reporttoemployeeunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "ishierarchy"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "userunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "isvoid"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiduserunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiddatetime"
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voidreason"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            '/**************** DISPLAYING DATA ****************/
            dCol = New DataColumn
            dCol.ColumnName = "ecode"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "ename"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "job_group"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "job_name"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "AUD"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "GUID"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)
            '/**************** DISPLAYING DATA ****************/


            'Pinkal (18-Aug-2018) -- Start
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
            dCol = New DataColumn
            dCol.ColumnName = "EmpDisplayName"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "ReportingToEmpDisplayName"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)
            'Pinkal (18-Aug-2018) -- End


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.

            dCol = New DataColumn
            dCol.ColumnName = "ReportingToEmail"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "EmployeeEmail"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "EmployeeName"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            'Pinkal (01-Oct-2018) -- End


            'Pinkal (09-Mar-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            dCol = New DataColumn
            dCol.ColumnName = "IsEmpApproved"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "IsReportingToEmpApproved"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)
            'Pinkal (09-Mar-2020) -- End

            'Hemant (07 Mar 2022) -- Start            
            'ISSUE/ENHANCEMENT(NMB) : Point#10. After marking training as complete, the email sent to employee’s reporting to is inaccurate and mis leading
            dCol = New DataColumn
            dCol.ColumnName = "fename"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "lename"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)
            'Hemant (07 Mar 2022) -- End

            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            dCol = New DataColumn
            dCol.ColumnName = "effectivedate"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)
            'Hemant (03 Feb 2023) -- End


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "


    'Pinkal (18-Aug-2018) -- Start
    'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].

    'Private Sub Get_ReportingList()
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    Try
    '        objDataOperation = New clsDataOperation

    '        StrQ = "SELECT " & _
    '               "	 hremployee_reportto.reporttounkid " & _
    '               "	,hremployee_reportto.employeeunkid " & _
    '               "	,hremployee_reportto.reporttoemployeeunkid " & _
    '               "	,hremployee_reportto.ishierarchy " & _
    '               "	,hremployee_reportto.userunkid " & _
    '               "	,hremployee_reportto.isvoid " & _
    '               "	,hremployee_reportto.voiduserunkid " & _
    '               "	,hremployee_reportto.voiddatetime " & _
    '               "	,hremployee_reportto.voidreason " & _
    '               "	,hremployee_master.employeecode AS ecode " & _
    '               "	,ISNULL(hremployee_master.firstname,'')+' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') ename " & _
    '               "	,ISNULL(hrjobgroup_master.name,'') AS job_group " & _
    '               "	,ISNULL(hrjob_master.job_name,'') AS job_name " & _
    '               "	,'' AS AUD " & _
    '               "FROM hremployee_reportto " & _
    '               "	JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_reportto.reporttoemployeeunkid " & _
    '               "	LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
    '               "	JOIN hrjob_master on hrjob_master.jobunkid = hremployee_master.jobunkid " & _
    '               "WHERE hremployee_reportto.employeeunkid = '" & mintEmployeeId & "' AND hremployee_reportto.isvoid = 0 "

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dRow As DataRow In dsList.Tables("List").Rows
    '            mdtTran.ImportRow(dRow)
    '        Next

    '    Catch ex As Exception
    '        Call DisplayError.Show("-1", ex.Message, "Get_ReportingList", mstrModuleName)

    '    End Try
    'End Sub

    Private Sub Get_ReportingList(ByVal mdtEmployeeAsonDate As Date, Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal blnIsAllData As Boolean = False)
        'Hemant (03 Feb 2023) -- [blnIsAllData]
        'Pinkal (20-Sep-2022) -- NMB Loan Module Enhancement. [Optional ByVal objDataOpr As clsDataOperation = Nothing]
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try

            'Pinkal (20-Sep-2022) -- Start
            'NMB Loan Module Enhancement.
            If objDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDataOpr
            End If
            'Pinkal (20-Sep-2022) -- End

            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            StrQ = "SELECT " & _
                   "        * " & _
                   " FROM (  "
            'Hemant (03 Feb 2023) -- End

            StrQ &= "SELECT " & _
                   "	 hremployee_reportto.reporttounkid " & _
                   "	,hremployee_reportto.employeeunkid " & _
                   "	,hremployee_reportto.reporttoemployeeunkid " & _
                   "	,hremployee_reportto.ishierarchy " & _
                   "	,hremployee_reportto.userunkid " & _
                   "	,hremployee_reportto.isvoid " & _
                   "	,hremployee_reportto.voiduserunkid " & _
                   "	,hremployee_reportto.voiddatetime " & _
                   "	,hremployee_reportto.voidreason " & _
                   "	,hremployee_master.employeecode AS ecode " & _
                   "	,ISNULL(hremployee_master.firstname,'')+' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') ename " & _
                   "	,ISNULL(hrjobgroup_master.name,'') AS job_group " & _
                   "	,ISNULL(hrjob_master.job_name,'') AS job_name " & _
                   "	,ISNULL(Emp.displayname,'') AS EmpDisplayName " & _
                   "	,ISNULL(hremployee_master.displayname,'') AS ReportingToEmpDisplayName " & _
                   "	,ISNULL(hremployee_master.email,'') AS ReportingToEmail " & _
                   "	,ISNULL(Emp.email,'') AS EmployeeEmail " & _
                   "	,ISNULL(Emp.employeecode,'') + ' -  ' + ISNULL(Emp.firstname,'')+' ' +ISNULL(Emp.surname,'') As EmployeeName " & _
                   "	,'' AS AUD " & _
                   "	,ISNULL(Emp.isapproved,0) AS IsEmpApproved " & _
                   "	,ISNULL(hremployee_master.isapproved,0) AS IsReportingToEmpApproved " & _
                   "    ,ISNULL(hremployee_master.firstname,'') AS fename " & _
                   "    ,ISNULL(hremployee_master.surname,'') AS lename " & _
                   "	,ISNULL(CONVERT(CHAR(8), hremployee_reportto.effectivedate,112), '19000101')  AS effectivedate " & _
                   "	,DENSE_RANK() OVER (ORDER BY effectivedate DESC) AS Rno " & _
                   " FROM hremployee_reportto " & _
                   "	JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_reportto.reporttoemployeeunkid " & _
                   " JOIN hremployee_master Emp ON Emp.employeeunkid = hremployee_reportto.employeeunkid " & _
                   " LEFT JOIN " & _
                    "( " & _
                    "    SELECT " & _
                    "         jobgroupunkid " & _
                    "        ,jobunkid " & _
                    "        ,employeeunkid " & _
                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "    FROM hremployee_categorization_tran " & _
                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsonDate" & _
                    ") AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    " LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = J.jobgroupunkid " & _
                    " JOIN hrjob_master on hrjob_master.jobunkid = J.jobunkid " & _
                    " WHERE hremployee_reportto.employeeunkid = @EmpId AND hremployee_reportto.isvoid = 0 "

            'Hemant (03 Feb 2023) -- [effectivedate,Rno]
            'Hemant (07 Mar 2022) -- [fename,lename]
            'Pinkal (01-Oct-2018) --  'Enhancement - Leave Enhancement for NMB.[  "	,ISNULL(hremployee_master.email,'') AS ReportingToEmail 	,ISNULL(Emp.email,'') AS EmployeeEmail " & _]

            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            If blnIsAllData = False Then
                StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsonDate "
            End If
            StrQ &= " ) AS A " & _
                    " WHERE 1 = 1 "
            If blnIsAllData = False Then
                StrQ &= " AND A.Rno = 1 "
            End If
            StrQ &= " ORDER BY effectivedate DESC , A.ishierarchy DESC "
            'Hemant (03 Feb 2023) -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@EmpAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsonDate))
            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeId)
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows
                mdtTran.ImportRow(dRow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_ReportingList; Module Name: " & mstrModuleName)
        End Try
    End Sub


    'Pinkal (18-Aug-2018) -- End


    'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
    'Public Function InsertUpdateDelete(ByVal mblnCreateADUserFromEmp As Boolean) As Boolean
    Public Function InsertUpdateDelete(ByVal mblnCreateADUserFromEmp As Boolean, _
                                       ByVal xCompanyId As Integer, _
                                       Optional ByVal xmintOldDefaultReportingTo As Integer = -1, _
                                       Optional ByVal xmintNewDefaultReportingTo As Integer = -1, _
                                       Optional ByVal xmstrEmployeeAsOnDate As String = "", _
                                       Optional ByVal intEmployeeUnkId As Integer = -1 _
                                       ) As Boolean


        'Pinkal (20-Mar-2023) A1X(-625) NMB - Implementation of future dated reporting lines on AD from Aruti Flexcube. [ByVal xCompanyId As Integer]

        'Hemant (03 Feb 2023) -- [intEmployeeUnkId]
        'Gajanan [24-OCT-2019] -- End

        'Pinkal (18-Aug-2018) -- 'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmp As Boolean]

        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'Pinkal (20-Mar-2023) -- Start
            'A1X(-625) NMB - Implementation of future dated reporting lines on AD from Aruti Flexcube.
            Dim objConfig As New clsConfigOptions
            Dim mintGrievanceApprovalSetting As Integer = enGrievanceApproval.ApproverEmpMapping
            Dim mblnRoleBasedLoanApproval As Boolean = False

            Dim xGrievanceApprovalSetting As String = objConfig.GetKeyValue(xCompanyId, "GrievanceApprovalSetting", Nothing)
            Dim xRoleBasedLoanApproval As String = objConfig.GetKeyValue(xCompanyId, "RoleBasedLoanApproval", Nothing)

            If xGrievanceApprovalSetting.Trim.Length > 0 Then mintGrievanceApprovalSetting = CInt(xGrievanceApprovalSetting)
            If xRoleBasedLoanApproval.Trim.Length > 0 Then mblnRoleBasedLoanApproval = CBool(xRoleBasedLoanApproval)

            objConfig = Nothing
            'Pinkal (20-Mar-2023) -- End

            objDataOperation.BindTransaction()
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hremployee_reportto ( " & _
                                           "  employeeunkid " & _
                                           ", reporttoemployeeunkid " & _
                                           ", ishierarchy " & _
                                           ", userunkid " & _
                                           ", isvoid " & _
                                           ", voiduserunkid " & _
                                           ", voiddatetime " & _
                                           ", voidreason" & _
                                           ", effectivedate" & _
                                       ") VALUES (" & _
                                           "  @employeeunkid " & _
                                           ", @reporttoemployeeunkid " & _
                                           ", @ishierarchy " & _
                                           ", @userunkid " & _
                                           ", @isvoid " & _
                                           ", @voiduserunkid " & _
                                           ", @voiddatetime " & _
                                           ", @voidreason" & _
                                           ", @effectivedate" & _
                                       "); SELECT @@identity"
                                'Hemant (03 Feb 2023) -- [effectivedate]

                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@reporttoemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("reporttoemployeeunkid").ToString)
                                objDataOperation.AddParameter("@ishierarchy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("ishierarchy").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Hemant (03 Feb 2023) -- Start
                                'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
                                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, Date.ParseExact(.Item("effectivedate"), "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo))
                                'Hemant (03 Feb 2023) -- End


                                Dim dsList As New DataSet
                                Dim mintReporttoUnkId As Integer = -1
                                dsList = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintReporttoUnkId = dsList.Tables(0).Rows(0).Item(0)

                                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hremployee_reportto", "reporttounkid", mintReporttoUnkId) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                            Case "U"
                                StrQ = "UPDATE hremployee_reportto SET " & _
                                           "  employeeunkid = @employeeunkid" & _
                                           ", reporttoemployeeunkid = @reporttoemployeeunkid" & _
                                           ", ishierarchy = @ishierarchy" & _
                                           ", userunkid = @userunkid" & _
                                           ", isvoid = @isvoid" & _
                                           ", voiduserunkid = @voiduserunkid" & _
                                           ", voiddatetime = @voiddatetime" & _
                                           ", voidreason = @voidreason " & _
                                           ", effectivedate = @effectivedate " & _
                                       "WHERE reporttounkid = @reporttounkid "
                                'Hemant (03 Feb 2023) -- [effectivedate]

                                objDataOperation.AddParameter("@reporttounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("reporttounkid").ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
                                objDataOperation.AddParameter("@reporttoemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("reporttoemployeeunkid").ToString)
                                objDataOperation.AddParameter("@ishierarchy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("ishierarchy").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Hemant (03 Feb 2023) -- Start
                                'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
                                objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, Date.ParseExact(.Item("effectivedate"), "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo))
                                'Hemant (03 Feb 2023) -- End


                                objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hremployee_reportto", "reporttounkid", .Item("reporttounkid")) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If



                            Case "D"
                                StrQ = "UPDATE hremployee_reportto SET " & _
                                           "  isvoid = @isvoid" & _
                                           ", voiduserunkid = @voiduserunkid" & _
                                           ", voiddatetime = @voiddatetime" & _
                                           ", voidreason = @voidreason " & _
                                       "WHERE reporttounkid = @reporttounkid "

                                objDataOperation.AddParameter("@reporttounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("reporttounkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hremployee_reportto", "reporttounkid", .Item("reporttounkid")) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select

                    End If
                End With

            Next

            'Pinkal (07-Dec-2019) -- Start 
            'Enhancement - Active Directory Integration Requirement For NMB [Ref No : 273].
           
            'Pinkal (09-Mar-2020) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].

            If mblnCreateADUserFromEmp Then
                Dim drRow() As DataRow = mdtTran.Select("ishierarchy = 1 AND AUD <> 'D' ")
                If drRow.Length > 0 Then
                    If CBool(drRow(0)("IsEmpApproved")) Then
                    If SetADReportingTo(False, drRow(0)("EmpDisplayName").ToString(), drRow(0)("ReportingToEmpDisplayName").ToString(), objDataOperation) = False Then
                        If mstrMessage.Trim.Length > 0 Then
                            exForce = New Exception(mstrMessage)
                            Throw exForce
                        ElseIf objDataOperation.ErrorMessage.Trim.Length > 0 Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                    End If
                Else
                    drRow = mdtTran.Select("ishierarchy = 1 AND AUD = 'D' ")
                    If drRow.Length > 0 Then
                        If CBool(drRow(0)("IsEmpApproved")) Then
                            'Pinkal (01-Mar-2023) -- Start
                            '(A1X-658) As a user, i want to have a port configuration option while configuring AD connection parameters As a user, i want to have a port configuration option while configuring AD conection parameters.
                            'If SetADReportingTo(True, drRow(0)("EmpDisplayName").ToString(), "", objDataOperation) = False Then
                            If SetADReportingTo(True, drRow(0)("EmpDisplayName").ToString(), drRow(0)("ReportingToEmpDisplayName").ToString(), objDataOperation) = False Then
                                'Pinkal (01-Mar-2023) -- End
                            If mstrMessage.Trim.Length > 0 Then
                                exForce = New Exception(mstrMessage)
                                Throw exForce
                            ElseIf objDataOperation.ErrorMessage.Trim.Length > 0 Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        End If
                    End If
                End If
            End If
            End If
            'Pinkal (09-Mar-2020) -- End

            'Pinkal (07-Dec-2019) -- End


            'Pinkal (20-Mar-2023) -- Start
            'A1X(-625) NMB - Implementation of future dated reporting lines on AD from Aruti Flexcube.
            If mintGrievanceApprovalSetting = enGrievanceApproval.ReportingTo Then
            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            If ChangeGrievanceResolutionApprover(xmintOldDefaultReportingTo, xmintNewDefaultReportingTo, objDataOperation, intEmployeeUnkId) = False Then
                'Hemant (03 Feb 2023) -- [intEmployeeUnkId]
                If mstrMessage.Trim.Length > 0 Then
                    exForce = New Exception(mstrMessage)
                    Throw exForce
                ElseIf objDataOperation.ErrorMessage.Trim.Length > 0 Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Gajanan [24-OCT-2019] -- End
            End If
            'Pinkal (20-Mar-2023) -- End

            'Pinkal (20-Mar-2023) -- Start
            'A1X(-625) NMB - Implementation of future dated reporting lines on AD from Aruti Flexcube.
            If mblnRoleBasedLoanApproval Then
            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            If ChangeFlexcubeLoanApprover(xmintOldDefaultReportingTo, xmintNewDefaultReportingTo, intEmployeeUnkId, objDataOperation) = False Then
                If mstrMessage.Trim.Length > 0 Then
                    exForce = New Exception(mstrMessage)
                    Throw exForce
                ElseIf objDataOperation.ErrorMessage.Trim.Length > 0 Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Hemant (03 Feb 2023) -- End
            End If
            'Pinkal (20-Mar-2023) -- End

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete; Module Name: " & mstrModuleName)
        End Try
    End Function

    Public Function Get_ReportingToEmp(ByVal StrName As String, _
                                       ByVal dtPeriodStart As DateTime, _
                                       ByVal dtPeriodEnd As DateTime, _
                                       ByVal intEmpId As Integer, _
                                       ByVal xDatabaseName As String, _
                                       Optional ByVal blnFlag As Boolean = False, _
                                       Optional ByVal strFilterQuery As String = "") As DataTable

    'Gajanan [11-Dec-2019] -- Add [strFilterQuery]

        Dim StrQ As String = String.Empty
        Dim dTable As DataTable
        Dim dList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            If blnFlag = True Then
                StrQ = "SELECT " & _
                       "     -1 AS reporttounkid " & _
                       "    ,0 AS employeeunkid " & _
                       "    ,-1 AS reporttoemployeeunkid " & _
                       "    ,0 AS ishierarchy " & _
                       "    ,-1 AS userunkid " & _
                       "    ,0 AS isvoid " & _
                       "    ,-1 AS voiduserunkid " & _
                       "    ,NULL AS voiddatetime " & _
                       "    ,'' AS voidreason " & _
                       "    ,'' AS ecode " & _
                       "    ,@Select AS ename " & _
                       "    ,'' AS job_group " & _
                       "    ,'' AS job_name " & _
                       "    ,'' AS ReportingToEmpDisplayName " & _
                       "    ,CAST(0 AS BIT) AS IsReportingToEmpApproved " & _
                       "    ,NULL AS effectivedate " & _
                       " UNION "
                'Hemant (03 Feb 2023) -- [effectivedate]

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "  Select"))
            End If


            'Pinkal (04-Apr-2020) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].["    , CAST(0 AS BIT) AS IsReportingToEmpApproved " & _]


            'Gajanan [11-Dec-2019] -- Start   
            'Enhancement:Worked On December Cut Over Enhancement For NMB




            'StrQ &= "SELECT " & _
            '        "     -1 AS reporttounkid " & _
            '        "    ,hremployee_master.employeeunkid " & _
            '        "    ,-1 AS reporttoemployeeunkid " & _
            '        "    ,0 AS ishierarchy " & _
            '        "    ,-1 AS userunkid " & _
            '        "    ,0 AS isvoid " & _
            '        "    ,-1 AS voiduserunkid " & _
            '        "    ,NULL AS voiddatetime " & _
            '        "    ,'' AS voidreason " & _
            '        "    ,hremployee_master.employeecode AS ecode " & _
            '        "    ,ISNULL(hremployee_master.firstname,'')+' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') ename " & _
            '        "    ,ISNULL(hrjobgroup_master.name,'') AS job_group " & _
            '        "    ,ISNULL(hrjob_master.job_name,'') AS job_name " & _
            '        "    ,ISNULL(hremployee_master.displayname,'') AS ReportingToEmpDisplayName " & _
            '        "FROM hremployee_master " & _
            '        "    LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
            '        "    JOIN hrjob_master on hrjob_master.jobunkid = hremployee_master.jobunkid " & _
            '        "WHERE hremployee_master.employeeunkid <> '" & intEmpId & "' " & _
            '        "    AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '        "    AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '        "    AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '        "    AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "

            StrQ &= "SELECT " & _
                    "     -1 AS reporttounkid " & _
                    "    ,hremployee_master.employeeunkid " & _
                    "    ,-1 AS reporttoemployeeunkid " & _
                    "    ,0 AS ishierarchy " & _
                    "    ,-1 AS userunkid " & _
                    "    ,0 AS isvoid " & _
                    "    ,-1 AS voiduserunkid " & _
                    "    ,NULL AS voiddatetime " & _
                    "    ,'' AS voidreason " & _
                    "    ,hremployee_master.employeecode AS ecode " & _
                    "    ,ISNULL(hremployee_master.firstname,'')+' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') ename " & _
                    "    ,ISNULL(hrjobgroup_master.name,'') AS job_group " & _
                    "    ,ISNULL(hrjob_master.job_name,'') AS job_name " & _
                    "    ,ISNULL(hremployee_master.displayname,'') AS ReportingToEmpDisplayName " & _
                    "    ,ISNULL(hremployee_master.isapproved,0) AS IsReportingToEmpApproved " & _
                    "    ,NULL AS effectivedate " & _
                    "FROM hremployee_master "
            'Hemant (03 Feb 2023) -- [effectivedate]

            'Pinkal (04-Apr-2020) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[ "    ,ISNULL(hremployee_master.isapproved,0) AS IsReportingToEmpApproved " & _]


            Dim xAdvanceJoinQry As String = String.Empty
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, xDatabaseName)
            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If



            'S.SANDEEP |29-JUN-2022| -- START
            'ISSUE/ENHANCEMENT : EMPLOYEE NOT SHOWING ON LIST DUE TO WRONG JOIN.
            Dim xDateJoinQry, xDataFilterQry As String
            xDateJoinQry = "" : xDataFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDataFilterQry, dtPeriodStart, dtPeriodEnd, True, False)

            'StrQ &= "    LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
            '        "    JOIN hrjob_master on hrjob_master.jobunkid = hremployee_master.jobunkid " & _
            '        "WHERE hremployee_master.employeeunkid <> '" & intEmpId & "' " & _
            '        "    AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
            '        "    AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
            '        "    AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
            '        "    AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "


            StrQ &= "   LEFT JOIN " & _
                    "   ( " & _
                    "        SELECT " & _
                    "             jobgroupunkid " & _
                    "           ,jobunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) as Rno " & _
                    "       FROM hremployee_categorization_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @enddate " & _
                    "   ) AS J ON J.employeeunkid = hremployee_master.employeeunkid AND J.Rno = 1 " & _
                    "   LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = J.jobgroupunkid " & _
                    "   JOIN hrjob_master on hrjob_master.jobunkid = J.jobunkid "
            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If
            StrQ &= "WHERE hremployee_master.employeeunkid <> '" & intEmpId & "' "

            If xDataFilterQry.Trim.Length > 0 Then
                StrQ &= xDataFilterQry & " "
            End If
            'S.SANDEEP |29-JUN-2022| -- END

            If strFilterQuery.Trim <> "" Then
                StrQ &= " AND " & strFilterQuery & " "
            End If

            'Gajanan [11-Dec-2019] -- End


            StrQ &= UserAccessLevel._AccessLevelFilterString

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodStart))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))

            dList = objDataOperation.ExecQuery(StrQ, "Lst")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dTable = New DataTable(StrName)

            dTable = dList.Tables("Lst").Clone

            For Each dRow As DataRow In dList.Tables("Lst").Rows
                dTable.ImportRow(dRow)
            Next

            Return dTable
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Get_ReportingToEmp", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function Get_UnReportingEmp(ByVal StrName As String, _
                                         ByVal dtPeriodStart As String, _
                                         ByVal dtPeriodEnd As String) As DataSet
        Dim StrQ As String = String.Empty
        Dim dList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   "     hremployee_master.employeecode AS Code " & _
                   "    ,ISNULL(hremployee_master.firstname,'')+' ' +ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') Employee " & _
                   "    ,ISNULL(hrjobgroup_master.name,'') AS Job_Group " & _
                   "    ,ISNULL(hrjob_master.job_name,'') AS Job_Name " & _
                   "FROM hremployee_master " & _
                   "    LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hremployee_master.jobgroupunkid " & _
                   "    JOIN hrjob_master on hrjob_master.jobunkid = hremployee_master.jobunkid " & _
                   "WHERE employeeunkid NOT IN (SELECT DISTINCT employeeunkid FROM hremployee_reportto WHERE isvoid = 0) " & _
                   "    AND CONVERT(CHAR(8),appointeddate,112) <= @enddate " & _
                   "    AND ISNULL(CONVERT(CHAR(8),termination_from_date,112),@startdate) >= @startdate " & _
                   "    AND ISNULL(CONVERT(CHAR(8),termination_to_date,112),@startdate) >= @startdate " & _
                   "    AND ISNULL(CONVERT(CHAR(8), empl_enddate,112), @startdate) >= @startdate "

            StrQ &= UserAccessLevel._AccessLevelFilterString

            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtPeriodStart)
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtPeriodEnd)

            dList = objDataOperation.ExecQuery(StrQ, "Lst")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dList

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Get_UnReportingEmp", mstrModuleName)
            Return Nothing
        End Try
    End Function


    'S.SANDEEP [ 23 MAR 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Is_Present(ByVal intEid As Integer, ByVal intRid As Integer, ByVal mdtEmployeeAsonDate As Date) As Boolean
        'Hemant (03 Feb 2023) -- [mdtEmployeeAsonDate]
        Dim StrQ As String = String.Empty
        Dim dList As New DataSet
        Try
            objDataOperation = New clsDataOperation


            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            'StrQ = "SELECT 1 FROM hremployee_reportto WHERE employeeunkid = '" & intEid & "' AND reporttoemployeeunkid = '" & intRid & "' AND isvoid = 0 "
            StrQ = "SELECT  1 FROM ( " & _
                         "SELECT " & _
                              "employeeunkid " & _
                            ",reporttoemployeeunkid " & _
                            ",effectivedate " & _
                            ",DENSE_RANK() OVER (PARTITION BY hremployee_reportto.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                         "FROM hremployee_reportto " & _
                         "WHERE employeeunkid = '" & intEid & "' " & _
                         "AND isvoid = 0 " & _
                         "AND CONVERT(CHAR(8), effectivedate, 112) <= @EmpAsonDate " & _
                         ") AS A WHERE 1 = 1 " & _
                    "AND A.Rno = 1 " & _
                    "AND reporttoemployeeunkid = '" & intRid & "' "
            objDataOperation.AddParameter("@EmpAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsonDate))
            'Hemant (03 Feb 2023) -- End

            dList = objDataOperation.ExecQuery(StrQ, "List")

            If dList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Is_Present", mstrModuleName)
        End Try
    End Function

    Public Function Get_File(ByVal mdtEmployeeAsonDate As Date, Optional ByVal sList As String = "List") As DataSet
        'Hemant (03 Feb 2023) -- [mdtEmployeeAsonDate]
        Dim StrQ As String = String.Empty
        Dim dList As New DataSet
        Try
            objDataOperation = New clsDataOperation
            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            StrQ = "SELECT " & _
                   "        EmployeeCode AS [" & Language.getMessage(mstrModuleName, 4, "EmployeeCode") & "] " & _
                   "       ,EmployeeName AS [" & Language.getMessage(mstrModuleName, 5, "EmployeeName") & "]" & _
                   "       ,ReportingCode AS [" & Language.getMessage(mstrModuleName, 6, "ReportingCode") & "]" & _
                   "       ,ReportToEmp AS [" & Language.getMessage(mstrModuleName, 7, "ReportToEmp") & "]" & _
                   "       ,DefaultReportTo AS [" & Language.getMessage(mstrModuleName, 8, "DefaultReportTo") & "]" & _
                   " FROM (  "
            'Hemant (03 Feb 2023) -- End
            StrQ &= "SELECT " & _
                   "   ISNULL(E_Emp.employeecode,'') AS EmployeeCode " & _
                   "  ,ISNULL(E_Emp.firstname,'')+' '+ISNULL(E_Emp.surname,'') AS  EmployeeName " & _
                   "  ,ISNULL(R_Emp.employeecode,'') AS  ReportingCode " & _
                   "  ,ISNULL(R_Emp.firstname,'')+' '+ISNULL(R_Emp.surname,'') AS  ReportToEmp " & _
                   "  ,CASE WHEN hremployee_reportto.ishierarchy = 1 THEN 1 ELSE 0 END AS DefaultReportTo " & _
                   "  ,DENSE_RANK() OVER (PARTITION BY hremployee_reportto.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                   "FROM hremployee_reportto " & _
                   "  LEFT JOIN hremployee_master AS E_Emp ON hremployee_reportto.employeeunkid = E_Emp.employeeunkid " & _
                   "  LEFT JOIN hremployee_master AS R_Emp ON hremployee_reportto.reporttoemployeeunkid = R_Emp.employeeunkid " & _
                   "WHERE hremployee_reportto.isvoid = 0 " & _
                   " AND CONVERT(CHAR(8), effectivedate, 112) <= @EmpAsonDate " & _
                   " ) AS A" & _
                   " WHERE 1 = 1 " & _
                   " AND Rno = 1 " & _
                   "ORDER BY A.employeecode " 'S.SANDEEP [14-JUN-2018] -- START {ALIAS CHANGED} -- END
            'Hemant (03 Feb 2023) -- [Rno]

            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            objDataOperation.AddParameter("@EmpAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsonDate))
            'Hemant (03 Feb 2023) -- End
            dList = objDataOperation.ExecQuery(StrQ, sList)

            Return dList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_File", mstrModuleName)
            Return Nothing
        End Try
    End Function
    'S.SANDEEP [ 23 MAR 2013 ] -- END


    'Pinkal (01-Mar-2023) -- Start
    '(A1X-658) As a user, i want to have a port configuration option while configuring AD connection parameters As a user, i want to have a port configuration option while configuring AD conection parameters.
    'Private Function SetADReportingTo(ByVal blnDeleteMgr As Boolean, ByVal mstrEmpDisplayName As String, ByVal mstrReportingToDisplayName As String, ByVal objDataOperation As clsDataOperation) As Boolean
    Public Function SetADReportingTo(ByVal blnDeleteMgr As Boolean, ByVal mstrEmpDisplayName As String, ByVal mstrReportingToDisplayName As String, ByVal objDataOperation As clsDataOperation) As Boolean
        Try
            Dim exForce As Exception
            Dim objConfig As New clsConfigOptions
            Dim mstrADIPAddress As String = ""
            Dim mstrADDomain As String = ""
            Dim mstrADUserName As String = ""
            Dim mstrADUserPwd As String = ""
            'Pinkal (01-Mar-2023) -- Start
            '(A1X-658) As a user, i want to have a port configuration option while configuring AD connection parameters As a user, i want to have a port configuration option while configuring AD conection parameters.
            Dim mstrADPortNo As String = ""
            'Pinkal (01-Mar-2023) -- End


            'Pinkal (01-Mar-2023) -- Start
            '(A1X-658) As a user, i want to have a port configuration option while configuring AD connection parameters As a user, i want to have a port configuration option while configuring AD conection parameters.
            'GetADConnection(mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd, objDataOperation)
            'Dim entry As DirectoryEntry = New DirectoryEntry("LDAP://" & mstrADIPAddress.Trim & "/" & mstrADDomain.Trim, mstrADUserName.Trim, mstrADUserPwd.Trim)
            'Dim entry As DirectoryEntry = Nothing
            'If mstrADPortNo.Trim.Length > 0 Then
            '    entry = New DirectoryEntry("LDAP://" & mstrADIPAddress.Trim & ":" & mstrADPortNo.Trim & "/" & mstrADDomain.Trim, mstrADUserName.Trim, mstrADUserPwd.Trim, AuthenticationTypes.Secure)
            'Else
            '    entry = New DirectoryEntry("LDAP://" & mstrADIPAddress.Trim & "/" & mstrADDomain.Trim, mstrADUserName.Trim, mstrADUserPwd.Trim)
            'End If

            'Dim search As DirectorySearcher = New DirectorySearcher(entry)
            'search.Filter = "(SAMAccountName=" & mstrEmpDisplayName & ")"
            'search.PropertiesToLoad.Add("cn")
            'Dim result As SearchResult = search.FindOne()
            'If (result IsNot Nothing) Then
            '    If blnDeleteMgr = False Then
            '        '/* START SEARCH FOR MANAGER
            '        If mstrReportingToDisplayName.Trim.Length > 0 Then
            '            Dim managerSearcher As DirectorySearcher = New DirectorySearcher(entry)
            '            managerSearcher.Filter = "(SAMAccountName=" & mstrReportingToDisplayName & ")"
            '            Dim resultManager As SearchResult = managerSearcher.FindOne()
            '            If resultManager IsNot Nothing Then
            '                Dim ObjUserManager As DirectoryEntry = result.GetDirectoryEntry()
            '                ObjUserManager.Properties("Manager").Value = resultManager.Properties("DistinguishedName")(0).ToString()
            '                ObjUserManager.CommitChanges()
            '                ObjUserManager.Close()
            '                ObjUserManager = Nothing
            '            Else
            '                entry.Close()
            '                entry = Nothing
            '                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry,") & " " & mstrReportingToDisplayName & " " & Language.getMessage(mstrModuleName, 3, "does not Exist in active directory.Please first create this user in active directory then do further process.")
            '                Return False
            '            End If   '/* END SEARCH FOR MANAGER USER
            '    End If
            '    ElseIf blnDeleteMgr Then
            '        Dim ObjClearUserManager As DirectoryEntry = result.GetDirectoryEntry()
            '        ObjClearUserManager.Properties("Manager").Clear()
            '        ObjClearUserManager.CommitChanges()
            '        ObjClearUserManager.Close()
            '        ObjClearUserManager = Nothing
            'End If
            'Else
            '    entry.Close()
            '    entry = Nothing
            '    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry,") & " " & mstrEmpDisplayName & " " & Language.getMessage(mstrModuleName, 3, "does not Exist in active directory.Please first create this user in active directory then do further process.")
            '    Return False
            'End If '/* END SEARCH FOR EMPLOYEE USER
            'entry.Close()
            'entry = Nothing

            Dim mstrAttributes() As String = New String() {"SAMAccountName", "Manager"}
            clsADIntegration.GetADConnection(mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd, mstrADPortNo, objDataOperation)
            Dim objLDap As LdapConnection = clsADIntegration.SetADConnection(mstrADIPAddress, mstrADDomain, mstrADPortNo, mstrADUserName, mstrADUserPwd)
            Dim objSSLSearch As New SearchRequest(mstrADDomain, "(SAMAccountName=" & mstrEmpDisplayName & ")", Protocols.SearchScope.Subtree, mstrAttributes)
            Dim objResponse As SearchResponse = DirectCast(objLDap.SendRequest(objSSLSearch), SearchResponse)

            If objResponse.ResultCode = ResultCode.Success Then

                Dim srcResult As SearchResultEntry = objResponse.Entries(0)

                If srcResult.Attributes.Count > 0 Then

                If blnDeleteMgr = False Then
                    '/* START SEARCH FOR MANAGER
                    If mstrReportingToDisplayName.Trim.Length > 0 Then
                            objSSLSearch = New SearchRequest(mstrADDomain, "(SAMAccountName=" & mstrReportingToDisplayName & ")", Protocols.SearchScope.Subtree, mstrAttributes)
                            objResponse = DirectCast(objLDap.SendRequest(objSSLSearch), SearchResponse)

                            If objResponse.ResultCode = ResultCode.Success Then
                                Dim srcManagerResult As SearchResultEntry = objResponse.Entries(0)
                                If srcManagerResult.Attributes.Count > 0 Then
                                    If srcManagerResult.Attributes.Contains("Manager") Then
                                        Dim objModifyrequest As ModifyRequest = New ModifyRequest()
                                        objModifyrequest.DistinguishedName = srcResult.DistinguishedName
                                        Dim objModifyDir As DirectoryAttributeModification = New DirectoryAttributeModification()
                                        objModifyDir.Operation = DirectoryAttributeOperation.Replace
                                        objModifyDir.Name = "Manager"
                                        objModifyDir.Add(srcManagerResult.DistinguishedName)
                                        objModifyrequest.Modifications.Add(objModifyDir)

                                        Dim objModifyResponse As ModifyResponse = CType(objLDap.SendRequest(objModifyrequest), ModifyResponse)
                                        If objModifyResponse.ResultCode <> ResultCode.Success Then
                                            exForce = New Exception(objModifyResponse.ErrorMessage)
                                            Throw exForce
                                        End If   ' If objModifyResponse.ResultCode <> ResultCode.Success Then
                                        objModifyResponse = Nothing
                                        objModifyDir = Nothing
                                        objModifyrequest = Nothing
                                    End If   '   If srcManagerResult.Attributes.Contains("Manager") Then
                        Else
                            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry,") & " " & mstrReportingToDisplayName & " " & Language.getMessage(mstrModuleName, 3, "does not Exist in active directory.Please first create this user in active directory then do further process.")
                            Return False
                                End If  ' If srcManagerResult.Attributes.Count > 0 Then
                            Else
                                exForce = New Exception(objResponse.ErrorMessage)
                                Throw exForce
                            End If  'If objResponse.ResultCode = ResultCode.Success Then
                        Else
                            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry,") & " " & mstrReportingToDisplayName & " " & Language.getMessage(mstrModuleName, 3, "does not Exist in active directory.Please first create this user in active directory then do further process.")
                            Return False
                        End If  'If mstrReportingToDisplayName.Trim.Length > 0 Then

                ElseIf blnDeleteMgr Then
                        If mstrReportingToDisplayName.Trim.Length > 0 Then
                            objSSLSearch = New SearchRequest(mstrADDomain, "(SAMAccountName=" & mstrReportingToDisplayName & ")", Protocols.SearchScope.Subtree, mstrAttributes)
                            objResponse = DirectCast(objLDap.SendRequest(objSSLSearch), SearchResponse)
                            If objResponse.ResultCode = ResultCode.Success Then
                                Dim srcManagerResult As SearchResultEntry = objResponse.Entries(0)
                                If srcManagerResult.Attributes.Contains("Manager") Then
                                    Dim objModifyrequest As ModifyRequest = New ModifyRequest()
                                    objModifyrequest.DistinguishedName = srcResult.DistinguishedName
                                    Dim objModifyDir As DirectoryAttributeModification = New DirectoryAttributeModification()
                                    objModifyDir.Operation = DirectoryAttributeOperation.Delete
                                    objModifyDir.Name = "Manager"
                                    objModifyDir.Add(srcManagerResult.DistinguishedName)
                                    objModifyrequest.Controls.Add(New PermissiveModifyControl())
                                    objModifyrequest.Modifications.Add(objModifyDir)

                                    Dim objModifyResponse As ModifyResponse = CType(objLDap.SendRequest(objModifyrequest), ModifyResponse)
                                    If objModifyResponse.ResultCode <> ResultCode.Success Then
                                        exForce = New Exception(objModifyResponse.ErrorMessage)
                                        Throw exForce
                                    End If   ' If objModifyResponse.ResultCode <> ResultCode.Success Then
                                    objModifyResponse = Nothing
                                    objModifyDir = Nothing
                                    objModifyrequest = Nothing
                                Else
                                    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry,") & " " & mstrReportingToDisplayName & " " & Language.getMessage(mstrModuleName, 3, "does not Exist in active directory.Please first create this user in active directory then do further process.")
                                    Return False
                                End If  '   If srcManagerResult.Attributes.Contains("Manager") Then
                            Else
                                exForce = New Exception(objResponse.ErrorMessage)
                                Throw exForce
                            End If  '  If objResponse.ResultCode = ResultCode.Success Then
                        Else
                            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry,") & " " & mstrReportingToDisplayName & " " & Language.getMessage(mstrModuleName, 3, "does not Exist in active directory.Please first create this user in active directory then do further process.")
                            Return False
                        End If  '    If mstrReportingToDisplayName.Trim.Length > 0 Then
                    End If  'If blnDeleteMgr = False Then
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry,") & " " & mstrEmpDisplayName & " " & Language.getMessage(mstrModuleName, 3, "does not Exist in active directory.Please first create this user in active directory then do further process.")
                Return False
                End If  'If srcResult.Attributes.Count > 0 Then
            Else
                exForce = New Exception(objResponse.ErrorMessage)
                Throw exForce
            End If '   If objResponse.ResultCode = ResultCode.Success Then
            'Pinkal (01-Mar-2023) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetADReportingTo; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function
    'Pinkal (18-Aug-2018) -- End


    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Public Function Approver_Migration(ByVal xEmployeeunkid As Integer, ByVal xOldApproverId As Integer, _
                                       ByVal xNewApproverId As Integer, ByVal xEmployeeAsOnDate As String, _
                                       ByVal mblnCreateADUserFromEmpMst As Boolean, _
                                       Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        'Pinkal (07-Dec-2019) -- Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].[ByVal mblnCreateADUserFromEmpMst As Boolean]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Try
            Dim objDataOperation As clsDataOperation
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            End If


            Me._EmployeeUnkid(CDate(xEmployeeAsOnDate)) = xEmployeeunkid
            Dim drow As DataRow = mdtTran.AsEnumerable.Where(Function(x) x.Field(Of Boolean)("ishierarchy") = True).FirstOrDefault()
            Dim mintReporttoUnkId As Integer = -1
            If IsNothing(drow) = False Then
                mintReporttoUnkId = CInt(drow("reporttounkid"))
            End If


            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            'strQ = " Update hremployee_reportto set " & _
            '                  " isvoid = 1,voiddatetime=getdate(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
            '                  " WHERE reporttoemployeeunkid = @reporttoemployeeunkid AND employeeunkid = @employeeunkid "
            strQ = " Update hremployee_reportto set " & _
                   " isvoid = 1,voiddatetime=getdate(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                              " WHERE reporttounkid = @reporttounkid AND employeeunkid = @employeeunkid "
            'Hemant (03 Feb 2023) -- End

            objDataOperation.ClearParameters()

            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            objDataOperation.AddParameter("@reporttounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReporttoUnkId)
            'Hemant (03 Feb 2023) -- End
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 9, "Migration"))
            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            objDataOperation.AddParameter("@EmpAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xEmployeeAsOnDate)
            'Hemant (03 Feb 2023) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If





            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hremployee_reportto", "reporttounkid", mintReporttoUnkId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            strQ = "INSERT INTO hremployee_reportto ( " & _
                                           "  employeeunkid " & _
                                           ", reporttoemployeeunkid " & _
                                           ", ishierarchy " & _
                                           ", userunkid " & _
                                           ", isvoid " & _
                                           ", voiduserunkid " & _
                                           ", voiddatetime " & _
                                           ", voidreason" & _
                                           ", effectivedate" & _
                                       ") VALUES (" & _
                                           "  @employeeunkid " & _
                                           ", @reporttoemployeeunkid " & _
                                           ", @ishierarchy " & _
                                           ", @userunkid " & _
                                           ", @isvoid " & _
                                           ", @voiduserunkid " & _
                                           ", @voiddatetime " & _
                                           ", @voidreason" & _
                                           ", @effectivedate" & _
                                       "); SELECT @@identity"
            'Hemant (03 Feb 2023) -- [effectivedate]
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)
            objDataOperation.AddParameter("@reporttoemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverId)
            objDataOperation.AddParameter("@ishierarchy", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, 0)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(xEmployeeAsOnDate))
            'Hemant (03 Feb 2023) -- End

            dsList = New DataSet
            mintReporttoUnkId = -1
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintReporttoUnkId = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hremployee_reportto", "reporttounkid", mintReporttoUnkId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Gajanan [24-OCT-2019] -- Start   
            'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   
            If ChangeGrievanceResolutionApprover(xOldApproverId, xNewApproverId, objDataOperation, xEmployeeunkid) = False Then
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Gajanan [24-OCT-2019] -- End

            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            If ChangeFlexcubeLoanApprover(xOldApproverId, xNewApproverId, xEmployeeunkid, objDataOperation) = False Then
                If mstrMessage.Trim.Length > 0 Then
                    exForce = New Exception(mstrMessage)
                    Throw exForce
                ElseIf objDataOperation.ErrorMessage.Trim.Length > 0 Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Hemant (03 Feb 2023) -- End

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            'Pinkal (07-Dec-2019) -- Start
            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
            If mblnCreateADUserFromEmpMst Then
                Dim objEmployee As New clsEmployee_Master
                Dim mstrEmpDisplayName As String = ""
                Dim mstrReportingToEmpDisplayName As String = ""
                objEmployee._Employeeunkid(CDate(xEmployeeAsOnDate), objDataOperation) = xEmployeeunkid
                mstrEmpDisplayName = objEmployee._Displayname

                'Pinkal (15-Feb-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                'MsgBox("mstrEmpDisplayName : " & mstrEmpDisplayName)
                'Pinkal (15-Feb-2020) -- End



                objEmployee._Employeeunkid(CDate(xEmployeeAsOnDate), objDataOperation) = xNewApproverId
                mstrReportingToEmpDisplayName = objEmployee._Displayname

                'Pinkal (15-Feb-2020) -- Start
                'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                'MsgBox("mstrReportingToEmpDisplayName : " & mstrReportingToEmpDisplayName)
                'Pinkal (15-Feb-2020) -- End

                If SetADReportingTo(False, mstrEmpDisplayName, mstrReportingToEmpDisplayName, objDataOperation) = False Then
                    If mstrMessage.Trim.Length > 0 Then
                        exForce = New Exception(mstrMessage)
                        Throw exForce
                    ElseIf objDataOperation.ErrorMessage.Trim.Length > 0 Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                objEmployee = Nothing
            End If
            'Pinkal (07-Dec-2019) -- End

            Return True

        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Approver_Migration, Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function
    'Gajanan [23-SEP-2019] -- End


    'Gajanan [24-OCT-2019] -- Start   
    'Enhancement:Worked On NMB Grievance Reporting To Approval Flow Change   

    'Public Function IsWrongReportingTo(ByVal mintEmployeeid As Integer, Optional ByVal xObjDataOpr As clsDataOperation = Nothing) As Boolean
    Public Function IsGrievanceEmployeeReportingToWrong(ByVal mintEmployeeid As Integer, ByVal mdtEmployeeAsonDate As Date, Optional ByVal xObjDataOpr As clsDataOperation = Nothing) As Boolean
        'Hemant (03 Feb 2023) -- [mdtEmployeeAsonDate]
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try

            Dim objDataOperation As clsDataOperation
            If xObjDataOpr IsNot Nothing Then
                objDataOperation = xObjDataOpr
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()


            StrQ = "IF OBJECT_ID('tempdb..#Results') IS NOT NULL " & _
                   "DROP TABLE #Results " & _
                   "CREATE TABLE #Results ( " & _
                        "empid INT " & _
                        ",rempid INT " & _
                        ") " & _
                   "DECLARE @REmpId AS INT " & _
                        ",@NxEmpId AS INT " & _
                   "SET @REmpId = 0 " & _
                   "SET @NxEmpId = " & mintEmployeeid & "  " & _
                   "WHILE (@NxEmpId) > 0 " & _
                   "BEGIN " & _
                        "SET @REmpId = ISNULL(( " & _
                                       "SELECT TOP 1 reporttoemployeeunkid " & _
                                       "FROM hremployee_reportto " & _
                                       "WHERE employeeunkid = @NxEmpId " & _
                                            "AND ishierarchy = 1 " & _
                                            "AND isvoid = 0 " & _
                                            "AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsonDate " & _
                                       " ORDER BY effectivedate DESC " & _
                                       "), 0) " & _
                        "IF (@NxEmpId = @REmpId) " & _
                             "SET @REmpId = 0 " & _
                        "INSERT INTO #Results ( " & _
                             "empid " & _
                             ",rempid " & _
                             ") " & _
                        "VALUES ( " & _
                             "@NxEmpId " & _
                             ",@REmpId " & _
                             ") " & _
                        "SET @NxEmpId = @REmpId " & _
                        "DECLARE @Rcnt AS INT " & _
                        "SET @Rcnt = ( " & _
                                  "SELECT COUNT(1) " & _
                                  "FROM #Results " & _
                                  "WHERE rempid = @NxEmpId " & _
                                  ") " & _
                        "IF (@Rcnt > 1) " & _
                             "BREAK " & _
                        "ELSE " & _
                             "CONTINUE " & _
                   "END " & _
                   "SELECT * " & _
                   "FROM #Results " & _
                   "WHERE empid > 0 DROP TABLE #Results "

            objDataOperation.ClearParameters()
            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            objDataOperation.AddParameter("@EmpAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsonDate))
            'Hemant (03 Feb 2023) -- End
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsNothing(dsList) = False AndAlso dsList.Tables("List").Rows.Count > 0 Then
                If CInt(dsList.Tables("List").Rows(dsList.Tables("List").Rows.Count - 1)("rempid")) = 0 Then
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsEmployeeReportingToWrong; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

    Public Function IsReportToValid(ByVal xmintOldReportingTo As Integer, ByVal xmintNewReportingTo As Integer, ByVal mdtEmployeeAsonDate As Date, Optional ByRef xmdtDatatable As DataTable = Nothing, Optional ByVal xObjDataOpr As clsDataOperation = Nothing) As Boolean
        'Hemant (03 Feb 2023) -- [mdtEmployeeAsonDate]
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try

            Dim objDataOperation As clsDataOperation
            If xObjDataOpr IsNot Nothing Then
                objDataOperation = xObjDataOpr
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            StrQ = "IF OBJECT_ID('tempdb..#Results') IS NOT NULL DROP TABLE #Results " & _
                    "CREATE TABLE #Results (empid int, rempid int) " & _
                    "DECLARE  @REmpId AS INT, @NxEmpId AS INT " & _
                    "SET @REmpId = 0 " & _
                    "SET @NxEmpId  = " & xmintNewReportingTo & " " & _
                    "WHILE (@NxEmpId) > 0 " & _
                    "BEGIN " & _
                         "SET @REmpId = ISNULL((SELECT TOP 1 reporttoemployeeunkid FROM hremployee_reportto WHERE employeeunkid = @NxEmpId and ishierarchy =1 and isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsonDate ORDER BY effectivedate DESC),0) " & _
                              "if (@NxEmpId = @REmpId ) " & _
                              "set @REmpId =0 " & _
                              "INSERT INTO #Results(empid, rempid)VALUES(@NxEmpId, @REmpId) " & _
                              "SET @NxEmpId = @REmpId " & _
                              "DECLARE @Rcnt AS INT " & _
                              "SET @Rcnt = (SELECT COUNT(1) FROM #Results WHERE rempid = @NxEmpId) " & _
                              "IF (@Rcnt > 1) " & _
                                "BREAK " & _
                              "ELSE " & _
                                "CONTINUE " & _
                    "END " & _
                    "SELECT #Results.* " & _
                    ",emp1.employeecode + ' - ' + emp1.firstname + ' ' + emp1.surname + ' -> ' + emp2.employeecode + ' - ' + emp2.firstname + ' ' + emp2.surname as employee " & _
                    "FROM #Results " & _
                    "left join hremployee_master as emp1 ON emp1.employeeunkid = #Results.empid " & _
                    "left join hremployee_master as emp2 ON emp2.employeeunkid = #Results.rempid " & _
                    "WHERE empid > 0 and rempid > 0 "
            'Hemant (03 Feb 2023) -- [AND CONVERT(CHAR(8),effectivedate,112) <= @EmpAsonDate ORDER BY effectivedate DESC]

            'Hemant (03 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
            objDataOperation.AddParameter("@EmpAsonDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEmployeeAsonDate))
            'Hemant (03 Feb 2023) -- End
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                If dsList.Tables("List").AsEnumerable().Where(Function(x) x.Field(Of Integer)("rempid") = xmintOldReportingTo).Count > 0 Then
					'Gajanan [20-DEC-2019] -- Start   
                    'If IsNothing(xmdtDatatable) = False Then
                    '    xmdtDatatable = dsList.Tables("List")
                    'End If
                        xmdtDatatable = dsList.Tables("List")
					'Gajanan [20-DEC-2019] -- End                    Return False
                Else
                    Return True
                End If
            End If
            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsWrongReportingTo; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xObjDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function ChangeGrievanceResolutionApprover(ByVal xOldApproverId As Integer, _
                                                      ByVal xNewApproverId As Integer, _
                                                      Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                                                      Optional ByVal intAgainstEmployeeUnkId As Integer = -1) As Boolean
        'Hemant (03 Feb 2023) -- [intAgainstEmployeeUnkId]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Try
            Dim objDataOperation As clsDataOperation
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            End If

            Dim objResolution As New clsResolution_Step_Tran
            Dim mstrResolutionId As String = objResolution.GetPendingResolutionList(xOldApproverId, objDataOperation, intAgainstEmployeeUnkId)
            'Hemant (03 Feb 2023) -- [intAgainstEmployeeUnkId]
            If mstrResolutionId.Trim.Length > 0 Then
                strQ = "Update greresolution_step_tran set approverempid  = @newapproverunkid, approvermasterunkid = @newapproverunkid  WHERE resolutionsteptranunkid in (" & mstrResolutionId & ") and approverempid = @OldApproverId AND approvalsettingid = @approvalsettingid AND issubmitted = 0 and isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverId)
                objDataOperation.AddParameter("@OldApproverId", SqlDbType.Int, eZeeDataType.INT_SIZE, xOldApproverId)
                objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.ReportingTo))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = " Select resolutionsteptranunkid from greresolution_step_tran WHERE resolutionsteptranunkid in (" & mstrResolutionId & ") AND approvalsettingid = @approvalsettingid AND issubmitted = 0 and isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverId)
                objDataOperation.AddParameter("@approvalsettingid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(enGrievanceApproval.ReportingTo))
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                        objResolution._ResolutionStepTranunkid = CInt(dsList.Tables(0).Rows(k)("resolutionsteptranunkid").ToString())
                        objResolution._AuditUserId = mintAudituserunkid
                        objResolution._ClientIP = mstrIp
                        objResolution._HostName = mstrHost
                        objResolution._FormName = mstrForm_Name
                        objResolution._FromWeb = mblnIsweb

                        If objResolution.InsertAuditTrailForResolutionStep(objDataOperation, 2) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                End If
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True

        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ChangeGrievanceResolutionApprover, Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function
    'Gajanan [24-OCT-2019] -- End


    'Hemant (03 Feb 2023) -- Start
    'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
    Public Function ChangeFlexcubeLoanApprover(ByVal xOldApproverId As Integer, _
                                               ByVal xNewApproverId As Integer, _
                                               ByVal intEmployeeUnkId As Integer, _
                                               Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                               ) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Try
            Dim objDataOperation As clsDataOperation
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            End If

            Dim objRolebaseApproval As New clsroleloanapproval_process_Tran
            Dim mstrProcessPendingLoanunkId As String = objRolebaseApproval.GetProcessPendingLoanList(xOldApproverId, intEmployeeUnkId, objDataOperation)
            If mstrProcessPendingLoanunkId.Trim.Length > 0 Then
                strQ = "Update lnroleloanapproval_process_tran " & _
                       " set mapuserunkid  = @newapproverunkid " & _
                       " WHERE isvoid = 0 " & _
                       " AND pendingloanaprovalunkid in (" & mstrProcessPendingLoanunkId & ") " & _
                       " AND mapuserunkid = @OldApproverId " & _
                       " AND statusunkid = " & enLoanApplicationStatus.PENDING & _
                       " AND mappingunkid = -1  " & _
                       " AND roleunkid = -1  " & _
                       " AND levelunkid = -1  " & _
                       " AND priority = -1  "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xNewApproverId)
                objDataOperation.AddParameter("@OldApproverId", SqlDbType.Int, eZeeDataType.INT_SIZE, xOldApproverId)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim arrProcessPendingLoanunkId() As String = mstrProcessPendingLoanunkId.Split(",")

                For Each iProcessPendingLoanunkId As Integer In arrProcessPendingLoanunkId
                    objRolebaseApproval._Pendingloanaprovalunkid = iProcessPendingLoanunkId
                    objRolebaseApproval._AuditUserId = mintAudituserunkid
                    objRolebaseApproval._ClientIP = mstrIp
                    objRolebaseApproval._HostName = mstrHost
                    objRolebaseApproval._FormName = mstrForm_Name
                    objRolebaseApproval._IsFromWeb = mblnIsweb

                    If objRolebaseApproval.InsertAuditTrailForRoleLoanApproval(objDataOperation, enAuditType.EDIT) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return True

        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: ChangeFlexcubeLoanApprover, Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function
    'Hemant (03 Feb 2023) -- End


#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "  Select")
			Language.setMessage(mstrModuleName, 2, "Sorry,")
			Language.setMessage(mstrModuleName, 3, "does not Exist in active directory.Please first create this user in active directory then do further process.")
			Language.setMessage(mstrModuleName, 4, "EmployeeCode")
			Language.setMessage(mstrModuleName, 5, "EmployeeName")
			Language.setMessage(mstrModuleName, 6, "ReportingCode")
			Language.setMessage(mstrModuleName, 7, "ReportToEmp")
			Language.setMessage(mstrModuleName, 8, "DefaultReportTo")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
