﻿'************************************************************************************************************************************
'Class Name : clsDependants_Membership_tran.vb
'Purpose    :
'Date       :03/09/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsDependants_Benefit_tran

#Region " Private Variables "
    Private Const mstrModuleName As String = "clsDependants_Benefit_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintDependantTranUnkid As Integer = -1
    Private mdtTran As DataTable

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Private mstrApprovalTranguid As String = String.Empty

    Dim Arr() As String = ConfigParameter._Object._SkipApprovalOnEmpData.ToString().Split(CChar(","))
    Dim DependantApprovalFlowVal As String = Array.Find(Arr, Function(x) (x = CStr(enScreenName.frmDependantsAndBeneficiariesList)))
    'Gajanan [22-Feb-2019] -- End
#End Region

#Region " Properties "
    Public Property _DependantTranUnkid() As Integer
        Get
            Return mintDependantTranUnkid
        End Get
        Set(ByVal value As Integer)
            mintDependantTranUnkid = value
            Call Get_Benefit_Tran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    Public Property _ApprovalTranguid() As String
        Get
            Return mstrApprovalTranguid
        End Get
        Set(ByVal value As String)
            mstrApprovalTranguid = value
        End Set
    End Property

    'Gajanan [22-Feb-2019] -- End
#End Region

#Region " Constuctor "
    Public Sub New()
        mdtTran = New DataTable("BenefitTran")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("dpndtbenefittranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("dpndtbeneficetranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("benefitgroupunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("benefitplanunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("value_id")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("benefit_percent")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("benefit_amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = DBNull.Value
            dCol.AllowDBNull = True
            mdtTran.Columns.Add(dCol)

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'Shani(04-MAR-2017) -- Start
            'Enhancement - Add Membership and Benefit in employee dependant's screen on web
            mdtTran.Columns.Add("benefit_group", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("benefit_type", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("benefit_value", System.Type.GetType("System.String")).DefaultValue = ""
            'Shani(04-MAR-2017) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Functions "


    'Pinkal(17-Nov-2010)----- Start

    Public Function GetList(Optional ByVal strTableName As String = "List") As DataSet
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
                    "  dpndtbenefittranunkid " & _
                    ", dpndtbeneficetranunkid " & _
                    ", benefitgroupunkid " & _
                    ", benefitplanunkid " & _
                    ", value_id " & _
                    ", benefit_percent " & _
                    ", benefit_amount " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voiddatetime " & _
                    ", voidreason " & _
                   "FROM hrdependant_benefit_tran "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetList", mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal(17-Nov-2010)----- End


    Private Sub Get_Benefit_Tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowB_Tran As DataRow
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'Shani(04-MAR-2017) -- Start
            'Enhancement - Add Membership and Benefit in employee dependant's screen on web


            'strQ = "SELECT " & _
            '        "  dpndtbenefittranunkid " & _
            '        ", dpndtbeneficetranunkid " & _
            '        ", benefitgroupunkid " & _
            '        ", benefitplanunkid " & _
            '        ", value_id " & _
            '        ", benefit_percent " & _
            '        ", benefit_amount " & _
            '        ", '' As AUD " & _
            '       "FROM hrdependant_benefit_tran " & _
            '       "WHERE dpndtbeneficetranunkid = @dpndtbeneficetranunkid " & _
            '       "AND ISNULL(isvoid,0) = 0 "

            strQ = "SELECT " & _
                   "     hrdependant_benefit_tran.dpndtbenefittranunkid " & _
                   "    ,hrdependant_benefit_tran.dpndtbeneficetranunkid " & _
                   "    ,hrdependant_benefit_tran.benefitgroupunkid " & _
                   "    ,hrdependant_benefit_tran.benefitplanunkid " & _
                   "    ,hrdependant_benefit_tran.value_id " & _
                   "    ,hrdependant_benefit_tran.benefit_percent " & _
                   "    ,hrdependant_benefit_tran.benefit_amount " & _
                   "    ,'' As AUD " & _
                   "    ,cfcommon_master.name AS benefit_group " & _
                   "    ,hrbenefitplan_master.benefitplanname AS benefit_type " & _
                   "    ,CASE hrdependant_benefit_tran.value_id " & _
                   "        WHEN 1 THEN @Value " & _
                   "        WHEN 2 THEN @Percent " & _
                   "        ELSE '' " & _
                   "     END AS benefit_value " & _
                   "FROM hrdependant_benefit_tran " & _
                   "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid  = hrdependant_benefit_tran.benefitgroupunkid " & _
                   "    LEFT JOIN hrbenefitplan_master ON hrbenefitplan_master.benefitplanunkid = hrdependant_benefit_tran.benefitplanunkid " & _
                   "WHERE dpndtbeneficetranunkid = @dpndtbeneficetranunkid " & _
                   "AND ISNULL(isvoid,0) = 0 "
            'Shani(04-MAR-2017) -- End

            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid.ToString)
            'Shani(04-MAR-2017) -- Start
            'Enhancement - Add Membership and Benefit in employee dependant's screen on web
            objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 107, "Value"))
            objDataOperation.AddParameter("@Percent", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 108, "Percent"))
            'Shani(04-MAR-2017) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowB_Tran = mdtTran.NewRow()

                    dRowB_Tran.Item("dpndtbenefittranunkid") = .Item("dpndtbenefittranunkid")
                    dRowB_Tran.Item("dpndtbeneficetranunkid") = .Item("dpndtbeneficetranunkid")
                    dRowB_Tran.Item("benefitgroupunkid") = .Item("benefitgroupunkid")
                    dRowB_Tran.Item("benefitplanunkid") = .Item("benefitplanunkid")
                    dRowB_Tran.Item("value_id") = .Item("value_id")
                    dRowB_Tran.Item("benefit_percent") = .Item("benefit_percent")
                    dRowB_Tran.Item("benefit_amount") = .Item("benefit_amount")
                    dRowB_Tran.Item("AUD") = .Item("AUD")

                    'Shani(04-MAR-2017) -- Start
                    'Enhancement - Add Membership and Benefit in employee dependant's screen on web
                    dRowB_Tran.Item("benefit_group") = .Item("benefit_group")
                    dRowB_Tran.Item("benefit_type") = .Item("benefit_type")
                    dRowB_Tran.Item("benefit_value") = .Item("benefit_value")
                    'Shani(04-MAR-2017) -- End

                    mdtTran.Rows.Add(dRowB_Tran)
                End With
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Benefit_Tran", mstrModuleName)
        End Try
    End Sub



    'S.SANDEEP [ 27 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function InsertUpdateDelete_DependantBenefit_Tran() As Boolean

    'Shani(04-MAR-2017) -- Start
    'Enhancement - Add Membership and Benefit in employee dependant's screen on web
    'Public Function InsertUpdateDelete_DependantBenefit_Tran(Optional ByVal intUserUnkid As Integer = 0) As Boolean
    Public Function InsertUpdateDelete_DependantBenefit_Tran(Optional ByVal objDo As clsDataOperation = Nothing, Optional ByVal intUserUnkid As Integer = 0) As Boolean
        'Shani(04-MAR-2017) -- End
        'S.SANDEEP [ 27 APRIL 2012 ] -- END

        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            'Shani(04-MAR-2017) -- Start
            'Enhancement - Add Membership and Benefit in employee dependant's screen on web
            'objDataOperation = New clsDataOperation
            'objDataOperation.BindTransaction()
            If objDo Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            Else
                objDataOperation = objDo
            End If
            'Shani(04-MAR-2017) -- End
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                                strQ = "INSERT INTO hrdependant_benefit_tran ( " & _
                                            "  dpndtbeneficetranunkid " & _
                                            ", benefitgroupunkid " & _
                                            ", benefitplanunkid " & _
                                            ", value_id " & _
                                            ", benefit_percent " & _
                                            ", benefit_amount " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason" & _
                                        ") VALUES (" & _
                                            "  @dpndtbeneficetranunkid " & _
                                            ", @benefitgroupunkid " & _
                                            ", @benefitplanunkid " & _
                                            ", @value_id " & _
                                            ", @benefit_percent " & _
                                            ", @benefit_amount " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason" & _
                                        "); SELECT @@identity"
                                'Gajanan [22-Feb-2019] -- End


                                objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid.ToString)
                                objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("benefitgroupunkid").ToString)
                                objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("benefitplanunkid").ToString)
                                objDataOperation.AddParameter("@value_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("value_id").ToString)

                                'Pinkal (05-Jul-2011) -- Start
                                'objDataOperation.AddParameter("@benefit_percent", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("benefit_percent").ToString)
                                'objDataOperation.AddParameter("@benefit_amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, .Item("benefit_amount").ToString)
                                objDataOperation.AddParameter("@benefit_percent", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("benefit_percent").ToString)
                                objDataOperation.AddParameter("@benefit_amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("benefit_amount").ToString)
                                'Pinkal (05-Jul-2011) -- End

                                 'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                				'Gajanan [9-July-2019] -- Start      
                				'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column


                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                'If .Item("voiddatetime") = Nothing Then
                                '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'Else
                                'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                'End If
                				'Gajanan [9-July-2019] -- End                                'Gajanan [22-Feb-2019] -- End

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 


                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'objDataOperation.ExecNonQuery(strQ)
                                Dim dList As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                ''S.SANDEEP [ 12 OCT 2011 ] -- START
                                ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'If .Item("dpndtbeneficetranunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_benefit_tran", "dpndtbenefittranunkid", dList.Tables(0).Rows(0)(0), 2, 1) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'Else
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", mintDependantTranUnkid, "hrdependant_benefit_tran", "dpndtbenefittranunkid", dList.Tables(0).Rows(0)(0), 1, 1) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If
                                ''S.SANDEEP [ 12 OCT 2011 ] -- END 

                                If .Item("dpndtbeneficetranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_benefit_tran", "dpndtbenefittranunkid", dList.Tables(0).Rows(0)(0), 2, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", mintDependantTranUnkid, "hrdependant_benefit_tran", "dpndtbenefittranunkid", dList.Tables(0).Rows(0)(0), 1, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END
                          Case "U"

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.



                                strQ = "UPDATE hrdependant_benefit_tran SET " & _
                                        "  dpndtbeneficetranunkid = @dpndtbeneficetranunkid" & _
                                        ", benefitgroupunkid = @benefitgroupunkid" & _
                                        ", benefitplanunkid = @benefitplanunkid" & _
                                        ", value_id = @value_id" & _
                                        ", benefit_percent = @benefit_percent" & _
                                        ", benefit_amount = @benefit_amount" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                      "WHERE dpndtbenefittranunkid = @dpndtbenefittranunkid "
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@dpndtbenefittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dpndtbenefittranunkid").ToString)
                                objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dpndtbeneficetranunkid").ToString)
                                objDataOperation.AddParameter("@benefitgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("benefitgroupunkid").ToString)
                                objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("benefitplanunkid").ToString)
                                objDataOperation.AddParameter("@value_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("value_id").ToString)

                                'Pinkal (05-Jul-2011) -- Start
                                'objDataOperation.AddParameter("@benefit_percent", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("benefit_percent").ToString)
                                'objDataOperation.AddParameter("@benefit_amount", SqlDbType.Money, eZeeDataType.MONEY_SIZE, .Item("benefit_amount").ToString)
                                objDataOperation.AddParameter("@benefit_percent", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("benefit_percent").ToString)
                                objDataOperation.AddParameter("@benefit_amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("benefit_amount").ToString)
                                'Pinkal (05-Jul-2011) -- End


                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 


                                'Sandeep [ 17 DEC 2010 ] -- Start
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'Sandeep [ 17 DEC 2010 ] -- End` 


                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE


                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_benefit_tran", "dpndtbenefittranunkid", .Item("dpndtbenefittranunkid"), 2, 2) = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_benefit_tran", "dpndtbenefittranunkid", .Item("dpndtbenefittranunkid"), 2, 2, , intUserUnkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END


                                
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                            Case "D"
                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'strQ = "DELETE FROM hrdependant_benefit_tran " & _
                                '      "WHERE dpndtbenefittranunkid = @dpndtbenefittranunkid "

                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'If .Item("dpndtbenefittranunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_benefit_tran", "dpndtbenefittranunkid", .Item("dpndtbenefittranunkid"), 2, 3) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If

                                If .Item("dpndtbenefittranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrdependants_beneficiaries_tran", "dpndtbeneficetranunkid", .Item("dpndtbeneficetranunkid"), "hrdependant_benefit_tran", "dpndtbenefittranunkid", .Item("dpndtbenefittranunkid"), 2, 3, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                                'Gajanan [22-Feb-2019] -- Start
                                'Enhancement - Implementing Employee Approver Flow On Employee Data.

                                  strQ = "UPDATE hrdependant_benefit_tran SET " & _
                                       "  isvoid = @isvoid" & _
                                       " ,voiduserunkid = @voiduserunkid" & _
                                       " ,voiddatetime = @voiddatetime" & _
                                       " ,voidreason = @voidreason " & _
                                       "WHERE dpndtbenefittranunkid = @dpndtbenefittranunkid "
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 
                                'Gajanan [22-Feb-2019] -- End
                                objDataOperation.AddParameter("@dpndtbenefittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("dpndtbenefittranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next

            'Sandeep [ 17 DEC 2010 ] -- Start
            'Shani(04-MAR-2017) -- Start
            'Enhancement - Add Membership and Benefit in employee dependant's screen on web
            'objDataOperation.ReleaseTransaction(True)
            If objDo Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Shani(04-MAR-2017) -- End
            'Sandeep [ 17 DEC 2010 ] -- End 

            Return True
        Catch ex As Exception
            'Sandeep [ 17 DEC 2010 ] -- Start
            'DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_DependantBenefit_Tran", mstrModuleName)
            'Shani(04-MAR-2017) -- Start
            'Enhancement - Add Membership and Benefit in employee dependant's screen on web
            'objDataOperation.ReleaseTransaction(False)
            If objDo Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
            'Shani(04-MAR-2017) -- End
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_DependantBenefit_Tran", mstrModuleName)
            Return False
            'Sandeep [ 17 DEC 2010 ] -- End 
        End Try
    End Function

    'Gajanan [22-Feb-2019] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.
    'Public Function Void_All(ByVal isVoid As Boolean, _
    '                         ByVal dtVoidDate As DateTime, _
    '                         ByVal strVoidRemark As String, _
    '                         ByVal intVoidUserId As Integer
    '                         ) As Boolean

    Public Function Void_All(ByVal isVoid As Boolean, _
                             ByVal dtVoidDate As DateTime, _
                             ByVal strVoidRemark As String, _
                       ByVal intVoidUserId As Integer, _
                       Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        'Gajanan [22-Feb-2019] -- End

        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception


        'Gajanan [22-Feb-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.

        'Dim objDataOp As New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Gajanan [22-Feb-2019] -- End


        Try
            strQ = "UPDATE hrdependant_benefit_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voidreason = @voidreason " & _
                    "WHERE dpndtbeneficetranunkid = @dpndtbeneficetranunkid "


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            xDataOpr.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isVoid.ToString)
            xDataOpr.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            xDataOpr.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strVoidRemark.ToString)
            xDataOpr.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserId)
            xDataOpr.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDependantTranUnkid.ToString)

            Call xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If
            'Gajanan [22-Feb-2019] -- End


            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [22-Feb-2019] -- End

            Return True
        Catch ex As Exception

            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [22-Feb-2019] -- End
            DisplayError.Show("-1", ex.Message, "Void_All", mstrModuleName)
            Return False
            'Gajanan [22-Feb-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [22-Feb-2019] -- End
        End Try
    End Function
#End Region

End Class
