﻿'************************************************************************************************************************************
'Class Name : clsBenefit_allocation_tran.vb
'Purpose    :
'Date       :24/01/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBenefit_allocation_tran
    Private Const mstrModuleName = "clsBenefit_allocation_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""



#Region " Private variables "
    Private mintBenefitallocationtranunkid As Integer
    Private mintBenefitallocationunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintBenefitplanunkid As Integer
    Private mintTranheadunkid As Integer
    Private mdecAmount As Decimal

    Private mdtTable As DataTable
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitallocationtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Benefitallocationtranunkid() As Integer
        Get
            Return mintBenefitallocationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintBenefitallocationtranunkid = Value
            'Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitallocationunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Benefitallocationunkid() As Integer
        Get
            Return mintBenefitallocationunkid
        End Get
        Set(ByVal value As Integer)
            mintBenefitallocationunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set benefitplanunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Benefitplanunkid() As Integer
        Get
            Return mintBenefitplanunkid
        End Get
        Set(ByVal value As Integer)
            mintBenefitplanunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    Public Property _Datasource() As DataTable
        Get
            Return mdtTable
        End Get
        Set(ByVal value As DataTable)
            mdtTable = value
        End Set
    End Property
#End Region

#Region " Constructor "
    Public Sub New()
        Try
            mdtTable = New DataTable("BATran")

            mdtTable.Columns.Add("IsChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("benefitallocationtranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("benefitallocationunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("benefitgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("benefitplanunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("benefitplanname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("tranheadunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("trnheadname", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("amount", System.Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTable.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTable.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTable.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtTable.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTable.Columns.Add("trnheadtype_id", System.Type.GetType("System.Int32")).DefaultValue = 0 'Sohail (19 Mar 2016)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT  hrbenefit_allocation_tran.benefitallocationtranunkid " & _
                          ", hrbenefit_allocation_tran.benefitallocationunkid " & _
                          ", ISNULL(hrbenefitplan_master.benefitgroupunkid, 0 ) AS benefitgroupunkid " & _
                          ", hrbenefit_allocation_tran.benefitplanunkid " & _
                          ", ISNULL(hrbenefitplan_master.benefitplancode, '') AS benefitplancode " & _
                          ", ISNULL(hrbenefitplan_master.benefitplanname, '') AS benefitplanname " & _
                          ", hrbenefit_allocation_tran.tranheadunkid " & _
                          ", ISNULL(prtranhead_master.trnheadcode, '') AS trnheadcode " & _
                          ", ISNULL(prtranhead_master.trnheadname, '') AS trnheadname " & _
                          ", hrbenefit_allocation_tran.amount " & _
                          ", hrbenefit_allocation_tran.isvoid " & _
                          ", hrbenefit_allocation_tran.voiduserunkid " & _
                          ", hrbenefit_allocation_tran.voiddatetime " & _
                          ", hrbenefit_allocation_tran.voidreason " & _
                          ", ISNULL(prtranhead_master.trnheadtype_id, '') AS trnheadtype_id " & _
                          ", '' AS AUD " & _
                    "FROM    hrbenefit_allocation_tran " & _
                            "LEFT JOIN hrbenefitplan_master ON hrbenefit_allocation_tran.benefitplanunkid = hrbenefitplan_master.benefitplanunkid " & _
                            "LEFT JOIN prtranhead_master ON hrbenefit_allocation_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                    "WHERE   ISNULL(hrbenefit_allocation_tran.isvoid, 0) = 0 " & _
                            "AND hrbenefit_allocation_tran.benefitallocationunkid = @benefitallocationunkid "
            'Sohail (19 Mar 2016) - [trnheadtype_id]

            objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitallocationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dRow As DataRow

            For Each dtRow As DataRow In dsList.Tables(0).Rows

                dRow = mdtTable.NewRow

                dRow.Item("benefitallocationtranunkid") = CInt(dtRow.Item("benefitallocationtranunkid"))
                dRow.Item("benefitallocationunkid") = CInt(dtRow.Item("benefitallocationunkid"))
                dRow.Item("benefitgroupunkid") = CInt(dtRow.Item("benefitgroupunkid"))
                dRow.Item("benefitplanunkid") = CInt(dtRow.Item("benefitplanunkid"))
                dRow.Item("benefitplanname") = dtRow.Item("benefitplanname").ToString
                dRow.Item("tranheadunkid") = CInt(dtRow.Item("tranheadunkid"))
                dRow.Item("trnheadname") = dtRow.Item("trnheadname").ToString
                dRow.Item("amount") = CDec(dtRow.Item("amount"))
                dRow.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                dRow.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                dRow.Item("voiddatetime") = dtRow.Item("voiddatetime")
                dRow.Item("voidreason") = dtRow.Item("voidreason").ToString
                dRow.Item("AUD") = dtRow.Item("AUD").ToString
                dRow.Item("trnheadtype_id") = CInt(dtRow.Item("trnheadtype_id")) 'Sohail (19 Mar 2016)

                mdtTable.Rows.Add(dRow)

            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function InsertByDataTable() As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "INSERT INTO hrbenefit_allocation_tran ( " & _
              "  benefitallocationunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", benefitplanunkid " & _
              ", tranheadunkid " & _
              ", amount" & _
            ") VALUES (" & _
              "  @benefitallocationunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @benefitplanunkid " & _
              ", @tranheadunkid " & _
              ", @amount" & _
            "); SELECT @@identity"

            For Each dtRow As DataRow In mdtTable.Rows
                objDataOperation.ClearParameters()

                objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBenefitallocationunkid.ToString)
                objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("benefitplanunkid").ToString)
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("tranheadunkid").ToString)
                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, dtRow.Item("amount").ToString)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, dtRow.Item("isvoid").ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow.Item("voiduserunkid").ToString)
                If dtRow.Item("voiddatetime").ToString = Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtRow.Item("voiddatetime").ToString)
                End If
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtRow.Item("voidreason").ToString)

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintBenefitallocationtranunkid = dsList.Tables(0).Rows(0).Item(0)

                If dtRow.Item("benefitallocationunkid") > 0 Then
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrbenefit_allocation_master", "benefitallocationunkid", mintBenefitallocationunkid, "hrbenefit_allocation_tran", "benefitallocationtranunkid", mintBenefitallocationtranunkid, 2, 1) = False Then
                        Return False
                    End If
                Else
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrbenefit_allocation_master", "benefitallocationunkid", mintBenefitallocationunkid, "hrbenefit_allocation_tran", "benefitallocationtranunkid", mintBenefitallocationtranunkid, 1, 1) = False Then
                        Return False
                    End If
                End If
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidAll(ByVal intBenefitAllocationUnkid As Integer _
                           , ByVal intVoidUserID As Integer _
                           , ByVal dtVoidDateTime As DateTime _
                           , ByVal strVoidReason As String) As Boolean
        'If isUsed(intBenefitAllocationUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "hrbenefit_allocation_master", "benefitallocationunkid", intBenefitAllocationUnkid, "hrbenefit_allocation_tran", "benefitallocationtranunkid", 3, 3) = False Then
                Return False
            End If

            strQ = "UPDATE hrbenefit_allocation_tran SET " & _
                        "  isvoid = 1 " & _
                        ", voiduserunkid = @voiduserunkid" & _
                        ", voiddatetime = @voiddatetime" & _
                        ", voidreason = @voidreason " & _
                "WHERE benefitallocationunkid = @benefitallocationunkid "

            objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBenefitAllocationUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            If dtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    '' <summary>
    '' Modify By: Sohail
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    '' 

    'Pinkal (21-Dec-2015) -- Start
    'Enhancement - Working on Employee Benefit changes given By Rutta.

    'Public Function GetList(ByVal strTableName As String _
    '                        , Optional ByVal intBenefitAllocationID As Integer = 0 _
    '                        , Optional ByVal intBenefitPlanID As Integer = 0 _
    '                        , Optional ByVal intTransactionHeadID As Integer = 0) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        strQ = "SELECT  hrbenefit_allocation_tran.benefitallocationtranunkid " & _
    '                      ", hrbenefit_allocation_tran.benefitallocationunkid " & _
    '                      ", hrbenefit_allocation_tran.isvoid " & _
    '                      ", hrbenefit_allocation_tran.voiduserunkid " & _
    '                      ", hrbenefit_allocation_tran.voiddatetime " & _
    '                      ", hrbenefit_allocation_tran.voidreason " & _
    '                      ", hrbenefit_allocation_tran.benefitplanunkid " & _
    '                      ", hrbenefit_allocation_tran.tranheadunkid " & _
    '                      ", hrbenefit_allocation_tran.amount " & _
    '                "FROM    hrbenefit_allocation_tran " & _
    '                "WHERE   ISNULL(hrbenefit_allocation_tran.isvoid, 0) = 0 "

    '        If intBenefitAllocationID > 0 Then
    '            strQ &= " AND hrbenefit_allocation_tran.benefitallocationunkid = @benefitallocationunkid "
    '            objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBenefitAllocationID.ToString)
    '        End If
    '        If intBenefitPlanID > 0 Then
    '            strQ &= " AND hrbenefit_allocation_tran.benefitplanunkid = @benefitplanunkid "
    '            objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBenefitPlanID.ToString)
    '        End If
    '        If intTransactionHeadID > 0 Then
    '            strQ &= " AND hrbenefit_allocation_tran.tranheadunkid = @tranheadunkid "
    '            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTransactionHeadID.ToString)
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    Public Function GetList(ByVal strTableName As String _
                            , Optional ByVal intBenefitAllocationID As Integer = 0 _
                            , Optional ByVal strBenefitPlanID As String = "-1" _
                            , Optional ByVal intTransactionHeadID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  " & _
                      "  CAST (0 AS Bit) AS ischeck " & _
                      ", hrbenefit_allocation_tran.benefitallocationtranunkid " & _
                          ", hrbenefit_allocation_tran.benefitallocationunkid " & _
                          ", hrbenefit_allocation_tran.isvoid " & _
                          ", hrbenefit_allocation_tran.voiduserunkid " & _
                          ", hrbenefit_allocation_tran.voiddatetime " & _
                          ", hrbenefit_allocation_tran.voidreason " & _
                          ", hrbenefit_allocation_tran.benefitplanunkid " & _
                      ", ISNULL(hrbenefitplan_master.benefitplanname,'') AS benefitplanname " & _
                          ", hrbenefit_allocation_tran.tranheadunkid " & _
                      ", ISNULL(prtranhead_master.trnheadname,'') AS trnheadname " & _
                          ", hrbenefit_allocation_tran.amount " & _
                      ", hrbenefit_allocation_tran.amount As Defaultamount " & _
                      ", 0 AS actionid " & _
                      ", 0 AS departmentid " & _
                      ", 0 AS jobid " & _
                      ", 0 AS classid " & _
                      ", 0 AS gradeid " & _
                      ", 0 AS sectionid " & _
                      ", '' AS exemptperiodid " & _
                      ", 0 AS periodunkid " & _
                      ", '' AS periodname " & _
                      ", 0 AS benefitgroupunkid " & _
                      ", 0 AS benefitgroupid " & _
                      ", 'A' AS AUD " & _
                    "FROM    hrbenefit_allocation_tran " & _
                      " LEFT JOIN hrbenefitplan_master ON hrbenefitplan_master.benefitplanunkid = hrbenefit_allocation_tran.benefitplanunkid AND hrbenefitplan_master.isactive = 1 " & _
                      " LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = hrbenefit_allocation_tran.tranheadunkid AND prtranhead_master.isvoid = 0 " & _
                    "WHERE   ISNULL(hrbenefit_allocation_tran.isvoid, 0) = 0 "

            If intBenefitAllocationID > 0 Then
                strQ &= " AND hrbenefit_allocation_tran.benefitallocationunkid = @benefitallocationunkid "
                objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBenefitAllocationID.ToString)
            End If

            If strBenefitPlanID.Trim.Length > 0 Then
                strQ &= " AND hrbenefit_allocation_tran.benefitplanunkid in ( " & strBenefitPlanID & ")"
            End If

            If intTransactionHeadID > 0 Then
                strQ &= " AND hrbenefit_allocation_tran.tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTransactionHeadID.ToString)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Pinkal (21-Dec-2015) -- End





    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (hrbenefit_allocation_tran) </purpose>
    'Public Function Insert() As Boolean
    '    'If isExist(mstrName) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbenefitallocationunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)
    '        objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbenefitplanunkid.ToString)
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)
    '        objDataOperation.AddParameter("@amount", SqlDbType.decimal, eZeeDataType.FLOAT_SIZE, mdecAmount.ToString)

    '        StrQ = "INSERT INTO hrbenefit_allocation_tran ( " & _
    '          "  benefitallocationunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '          ", benefitplanunkid " & _
    '          ", tranheadunkid " & _
    '          ", amount" & _
    '        ") VALUES (" & _
    '          "  @benefitallocationunkid " & _
    '          ", @isvoid " & _
    '          ", @voiduserunkid " & _
    '          ", @voiddatetime " & _
    '          ", @voidreason " & _
    '          ", @benefitplanunkid " & _
    '          ", @tranheadunkid " & _
    '          ", @amount" & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintBenefitallocationTranUnkId = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (hrbenefit_allocation_tran) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, mintBenefitallocationtranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@benefitallocationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbenefitallocationtranunkid.ToString)
    '        objDataOperation.AddParameter("@benefitallocationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbenefitallocationunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)
    '        objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintbenefitplanunkid.ToString)
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)
    '        objDataOperation.AddParameter("@amount", SqlDbType.decimal, eZeeDataType.FLOAT_SIZE, mdecAmount.ToString)

    '        StrQ = "UPDATE hrbenefit_allocation_tran SET " & _
    '          "  benefitallocationunkid = @benefitallocationunkid" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime" & _
    '          ", voidreason = @voidreason" & _
    '          ", benefitplanunkid = @benefitplanunkid" & _
    '          ", tranheadunkid = @tranheadunkid" & _
    '          ", amount = @amount " & _
    '        "WHERE benefitallocationtranunkid = @benefitallocationtranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (hrbenefit_allocation_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "DELETE FROM hrbenefit_allocation_tran " & _
    '        "WHERE benefitallocationtranunkid = @benefitallocationtranunkid "

    '        objDataOperation.AddParameter("@benefitallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@benefitallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  benefitallocationtranunkid " & _
    '          ", benefitallocationunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '          ", benefitplanunkid " & _
    '          ", tranheadunkid " & _
    '          ", amount " & _
    '         "FROM hrbenefit_allocation_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND benefitallocationtranunkid <> @benefitallocationtranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@benefitallocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

End Class