﻿'************************************************************************************************************************************
'Class Name : clsclsempsubstantive_job_tran.vb
'Purpose    :
'Date       :16-Jun-2023
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsempsubstantive_job_tran
    Private Shared ReadOnly mstrModuleName As String = "clsempsubstantive_job_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintSubstantivetranunkid As Integer
    Private mdtEffectivedate As Date
    Private mdtStartdate As Date
    Private mdtEnddate As Date
    Private mintEmployeeunkid As Integer
    Private mintJobunkid As Integer
    Private mintRehiretranunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mblnIsWeb As Boolean = False

    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1458) Kairuki - Auto notification to selected users(s) and employee [x] number of days before expiry of an acting position.
    Private mdtexpiry_reminder As DateTime = Nothing
    Private mblnntfsendremindertoemp As Boolean = False
    Private mblnntfsendremindertousers As Boolean = False
    'Pinkal (16-Oct-2023) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set substantivetranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Substantivetranunkid() As Integer
        Get
            Return mintSubstantivetranunkid
        End Get
        Set(ByVal value As Integer)
            mintSubstantivetranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Enddate() As Date
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As Date)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webclientip
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webhostname
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property


    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1458) Kairuki - Auto notification to selected users(s) and employee [x] number of days before expiry of an acting position.

    ''' <summary>
    ''' Purpose: Get or Set Expiry_reminder
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Expiry_reminder() As DateTime
        Get
            Return mdtexpiry_reminder
        End Get
        Set(ByVal value As DateTime)
            mdtexpiry_reminder = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Ntfsendremindertoemp
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ntfsendremindertoemp() As Boolean
        Get
            Return mblnntfsendremindertoemp
        End Get
        Set(ByVal value As Boolean)
            mblnntfsendremindertoemp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _Ntfsendremindertousers
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ntfsendremindertousers() As Boolean
        Get
            Return mblnntfsendremindertousers
        End Get
        Set(ByVal value As Boolean)
            mblnntfsendremindertousers = value
        End Set
    End Property

    'Pinkal (16-Oct-2023) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  substantivetranunkid " & _
                      ", effectivedate " & _
                      ", startdate " & _
                      ", enddate " & _
                      ", employeeunkid " & _
                      ", jobunkid " & _
                      ", rehiretranunkid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", expiry_reminder " & _
                      ", ISNULL(ntfsendremindertoemp,0) AS ntfsendremindertoemp" & _
                      ", ISNULL(ntfsendremindertousers,0) AS ntfsendremindertousers" & _
                      " FROM hrempsubstantive_job_tran " & _
                      " WHERE substantivetranunkid = @substantivetranunkid "

            'Pinkal (16-Oct-2023) -- (A1X-1458) Kairuki - Auto notification to selected users(s) and employee [x] number of days before expiry of an acting position.[ISNULL(ntfsendremindertoemp,0) AS ntfsendremindertoemp, ISNULL(ntfsendremindertousers,0) AS ntfsendremindertousers ]

            objDataOperation.AddParameter("@substantivetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSubstantivetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintSubstantivetranunkid = CInt(dtRow.Item("substantivetranunkid"))
                mdtEffectivedate = dtRow.Item("effectivedate")
                mdtStartdate = dtRow.Item("startdate")
                If IsDBNull(dtRow.Item("enddate")) = False AndAlso dtRow.Item("enddate") <> Nothing Then
                    mdtEnddate = dtRow.Item("enddate")
                End If
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintJobunkid = CInt(dtRow.Item("jobunkid"))
                mintRehiretranunkid = CInt(dtRow.Item("rehiretranunkid"))
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False AndAlso dtRow.Item("voiddatetime") <> Nothing Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString

                'Pinkal (16-Oct-2023) -- Start
                '(A1X-1458) Kairuki - Auto notification to selected users(s) and employee [x] number of days before expiry of an acting position.
                If IsDBNull(dtRow.Item("expiry_reminder")) = False AndAlso dtRow.Item("expiry_reminder") <> Nothing Then
                    mdtexpiry_reminder = dtRow.Item("expiry_reminder")
                End If

                mblnntfsendremindertoemp = CBool(dtRow.Item("ntfsendremindertoemp"))
                mblnntfsendremindertousers = CBool(dtRow.Item("ntfsendremindertousers"))
                'Pinkal (16-Oct-2023) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intEmployeeId As Integer = 0, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim mdtAppDate As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
                      "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                      " FROM hremployee_master " & _
                      " WHERE employeeunkid = '" & intEmployeeId & "' "

            objDataOperation.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtAppDate = objDataOperation.GetParameterValue("@ADate")

            strQ = "SELECT " & _
                      "  hrempsubstantive_job_tran.substantivetranunkid " & _
                      ", hrempsubstantive_job_tran.effectivedate " & _
                      ", hrempsubstantive_job_tran.startdate " & _
                      ", hrempsubstantive_job_tran.enddate " & _
                      ", CONVERT(CHAR(8),hrempsubstantive_job_tran.effectivedate,112) AS EffectDate " & _
                      ", CONVERT(CHAR(8),hrempsubstantive_job_tran.startdate,112) AS StDate " & _
                      ", CONVERT(CHAR(8),hrempsubstantive_job_tran.enddate,112) AS eddate " & _
                      ", hrempsubstantive_job_tran.employeeunkid " & _
                      ", hrempsubstantive_job_tran.jobunkid " & _
                      ", ISNULL(hrjob_master.job_name,'') AS Job " & _
                      ", hrempsubstantive_job_tran.rehiretranunkid " & _
                      ", hrempsubstantive_job_tran.remark " & _
                      ", hrempsubstantive_job_tran.userunkid " & _
                      ", hrempsubstantive_job_tran.isvoid " & _
                      ", hrempsubstantive_job_tran.voiddatetime " & _
                      ", hrempsubstantive_job_tran.voiduserunkid " & _
                      ", hrempsubstantive_job_tran.voidreason " & _
                      ", hrempsubstantive_job_tran.expiry_reminder " & _
                      ", ISNULL(hrempsubstantive_job_tran.ntfsendremindertoemp,0) AS ntfsendremindertoemp" & _
                      ", ISNULL(hrempsubstantive_job_tran.ntfsendremindertousers,0) AS ntfsendremindertousers" & _
                      " FROM hrempsubstantive_job_tran " & _
                      " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hrempsubstantive_job_tran.jobunkid " & _
                      " WHERE 1 = 1 "

            If intEmployeeId > 0 Then
                strQ &= " AND hrempsubstantive_job_tran.employeeunkid = '" & intEmployeeId & "' "
            End If

            If mdtAppDate.Trim.Length > 0 Then
                strQ &= " AND CONVERT(CHAR(8),hrempsubstantive_job_tran.effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            If blnOnlyActive Then
                strQ &= " AND hrempsubstantive_job_tran.isvoid  = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrempsubstantive_job_tran) </purpose>
    Public Function Insert(ByVal intCompanyId As Integer, ByVal xDatabaseName As String, _
                                    Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean


        Dim objDataOperation As New clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If isExist(mdtEffectivedate, Nothing, Nothing, mintEmployeeunkid, mintSubstantivetranunkid, Nothing) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Substantive Post information is already present for the selected effective date.")
            Return False
        ElseIf isExist(Nothing, mdtStartdate, Nothing, mintEmployeeunkid, mintSubstantivetranunkid, Nothing) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Substantive Post information is already present for the selected start date.")
            Return False
        ElseIf mdtEnddate <> Nothing AndAlso isExist(Nothing, Nothing, mdtEnddate, mintEmployeeunkid, mintSubstantivetranunkid, Nothing) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Substantive Post information is already present for the selected end date.")
            Return False
        End If

        If mintSubstantivetranunkid <= 0 Then
            dsList = Get_Current_SubstantiveJob(mdtEffectivedate.Date, mintEmployeeunkid)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                If CInt(dsList.Tables(0).Rows(0)("jobunkid")) = mintJobunkid Then
                    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Substantive Post information is already present for the selected combination for the selected effective date.")
                    Return False
                End If
            End If
            dsList = Nothing
        End If


        If objDOperation Is Nothing Then
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate.ToString)

            If IsDBNull(mdtEnddate) = False AndAlso mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate.ToString)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1458) Kairuki - Auto notification to selected users(s) and employee [x] number of days before expiry of an acting position.
            If IsDBNull(mdtexpiry_reminder) = False AndAlso mdtexpiry_reminder <> Nothing Then
                objDataOperation.AddParameter("@expiry_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtexpiry_reminder.ToString)
            Else
                objDataOperation.AddParameter("@expiry_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@ntfsendremindertoemp", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnntfsendremindertoemp.ToString)
            objDataOperation.AddParameter("@ntfsendremindertousers", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnntfsendremindertousers.ToString)
            'Pinkal (16-Oct-2023) -- End


            strQ = "INSERT INTO hrempsubstantive_job_tran ( " & _
                      "  effectivedate " & _
                      ", startdate " & _
                      ", enddate " & _
                      ", employeeunkid " & _
                      ", jobunkid " & _
                      ", rehiretranunkid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                      ", expiry_reminder " & _
                      ", ntfsendremindertoemp " & _
                      ", ntfsendremindertousers " & _
                    ") VALUES (" & _
                      "  @effectivedate " & _
                      ", @startdate " & _
                      ", @enddate " & _
                      ", @employeeunkid " & _
                      ", @jobunkid " & _
                      ", @rehiretranunkid " & _
                      ", @remark " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                      ", @expiry_reminder " & _
                      ", @ntfsendremindertoemp " & _
                      ", @ntfsendremindertousers " & _
                    "); SELECT @@identity"

            'Pinkal (16-Oct-2023) -- (A1X-1458) Kairuki - Auto notification to selected users(s) and employee [x] number of days before expiry of an acting position.[@expiry_reminder , @ntfsendremindertoemp , @ntfsendremindertousers ]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintSubstantivetranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForSubstantiveJob(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrempsubstantive_job_tran) </purpose>
    Public Function Update(ByVal intCompanyId As Integer, ByVal xDatabaseName As String, _
                                     Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim objDataOperation As New clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If isExist(mdtEffectivedate, Nothing, Nothing, mintEmployeeunkid, mintSubstantivetranunkid, Nothing) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Substantive Post information is already present for the selected effective date.")
            Return False
        ElseIf isExist(Nothing, mdtStartdate, Nothing, mintEmployeeunkid, mintSubstantivetranunkid, Nothing) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Substantive Post information is already present for the selected start date.")
            Return False
        ElseIf mdtEnddate <> Nothing AndAlso isExist(Nothing, Nothing, mdtEnddate, mintEmployeeunkid, mintSubstantivetranunkid, Nothing) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Substantive Post information is already present for the selected end date.")
            Return False
        End If


        If xDataOpr Is Nothing Then
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@substantivetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSubstantivetranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate.ToString)

            If IsDBNull(mdtEnddate) = False AndAlso mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate.ToString)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If IsDBNull(mdtVoiddatetime) = False AndAlso mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1458) Kairuki - Auto notification to selected users(s) and employee [x] number of days before expiry of an acting position.
            If IsDBNull(mdtexpiry_reminder) = False AndAlso mdtexpiry_reminder <> Nothing Then
                objDataOperation.AddParameter("@expiry_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtexpiry_reminder.ToString)
            Else
                objDataOperation.AddParameter("@expiry_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@ntfsendremindertoemp", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnntfsendremindertoemp.ToString)
            objDataOperation.AddParameter("@ntfsendremindertousers", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnntfsendremindertousers.ToString)
            'Pinkal (16-Oct-2023) -- End

            strQ = " UPDATE hrempsubstantive_job_tran SET " & _
                      "  effectivedate = @effectivedate" & _
                      ", startdate = @startdate" & _
                      ", enddate = @enddate" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", jobunkid = @jobunkid" & _
                      ", rehiretranunkid = @rehiretranunkid" & _
                      ", remark = @remark" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      ", expiry_reminder = @expiry_reminder " & _
                      ", ntfsendremindertoemp = @ntfsendremindertoemp " & _
                      ", ntfsendremindertousers = @ntfsendremindertousers " & _
                      " WHERE isvoid = 0 AND substantivetranunkid = @substantivetranunkid "

            'Pinkal (16-Oct-2023) -- (A1X-1458) Kairuki - Auto notification to selected users(s) and employee [x] number of days before expiry of an acting position.[expiry_reminder = @expiry_reminder , ntfsendremindertoemp = @ntfsendremindertoemp , ntfsendremindertousers = @ntfsendremindertousers ]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForSubstantiveJob(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrempsubstantive_job_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If


        Try
            strQ = " UPDATE hrempsubstantive_job_tran SET " & _
                      "  isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      " WHERE substantivetranunkid = @substantivetranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@substantivetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ClearParameters()
            mintSubstantivetranunkid = intUnkid
            Call GetData(objDataOperation)

            If InsertAuditTrailForSubstantiveJob(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@substantivetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xEffDate As DateTime, _
                                    ByVal xDate1 As DateTime, _
                                    ByVal xDate2 As DateTime, _
                                    ByVal xEmployeeId As Integer, _
                                    Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing


        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If

        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  substantivetranunkid " & _
                      ", effectivedate " & _
                      ", startdate " & _
                      ", enddate " & _
                      ", employeeunkid " & _
                      ", jobunkid " & _
                      ", rehiretranunkid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", expiry_reminder " & _
                      ", ntfsendremindertoemp " & _
                      ", ntfsendremindertousers " & _
                      " FROM hrempsubstantive_job_tran " & _
                      " WHERE isvoid = 0 "

            If xEffDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate "
            End If

            If intUnkid > 0 Then
                strQ &= " AND substantivetranunkid <> @substantivetranunkid "
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If


            If xDate1 <> Nothing AndAlso xDate2 <> Nothing Then
                strQ &= "  AND " & _
                            "( " & _
                            "  (@xDate1 BETWEEN CONVERT(CHAR(8),startdate,112) AND CONVERT(CHAR(8),enddate,112) " & _
                            "       OR @xDate2 BETWEEN CONVERT(CHAR(8),startdate,112) AND CONVERT(CHAR(8),enddate,112)) " & _
                            "  OR " & _
                            "  (CONVERT(CHAR(8),startdate,112) BETWEEN @xDate1 and @xDate2 " & _
                            "   AND CONVERT(CHAR(8),enddate,112) BETWEEN @xDate1 and @xDate2) " & _
                            "   OR enddate IS NULL " & _
                            ") "

                objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))
                objDataOperation.AddParameter("xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))

            Else

                If xDate1 <> Nothing Then

                    strQ &= "  AND " & _
                                " ( " & _
                                "  @xDate1 BETWEEN CONVERT(CHAR(8),startdate,112) AND CONVERT(CHAR(8),enddate,112) " & _
                                "   OR enddate IS NULL " & _
                                " ) "
                    objDataOperation.AddParameter("xDate1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate1))

                End If

                If xDate2 <> Nothing Then

                    strQ &= "  AND " & _
                                " ( " & _
                                "  @xDate2 BETWEEN CONVERT(CHAR(8),startdate,112) AND CONVERT(CHAR(8),enddate,112) " & _
                                " ) "
                    objDataOperation.AddParameter("xDate2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate2))

                End If

            End If

            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEffDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@substantivetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Get_Current_SubstantiveJob(ByVal xDate As Date, Optional ByVal xEmployeeId As Integer = 0, Optional ByVal objDataOperation As clsDataOperation = Nothing) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim mdtAppDate As String = ""
        Dim objDo As clsDataOperation
        Try
            If objDataOperation Is Nothing Then
                objDo = New clsDataOperation
            Else
                objDo = objDataOperation
            End If
            objDo.ClearParameters()

            If xEmployeeId > 0 Then
                StrQ = "SELECT " & _
                       "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                       "FROM hremployee_master " & _
                       "WHERE employeeunkid = '" & xEmployeeId & "' "

                objDo.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

                objDo.ExecNonQuery(StrQ)

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

                mdtAppDate = objDo.GetParameterValue("@ADate")
            End If

            StrQ = "SELECT " & _
                   "     effectivedate " & _
                   "    ,startdate " & _
                   "    ,enddate " & _
                   "    ,employeeunkid " & _
                   "    ,jobunkid " & _
                   "    , Job " & _
                   "    , substantivetranunkid " & _
                   " FROM " & _
                   " ( " & _
                   "    SELECT " & _
                   "         hrempsubstantive_job_tran.effectivedate " & _
                   "        ,hrempsubstantive_job_tran.startdate " & _
                   "        ,hrempsubstantive_job_tran.enddate " & _
                   "        ,hrempsubstantive_job_tran.employeeunkid " & _
                   "        ,hrempsubstantive_job_tran.jobunkid " & _
                   "        ,hrjob_master.job_name As Job " & _
                   "        ,hrempsubstantive_job_tran.substantivetranunkid AS substantivetranunkid " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hrempsubstantive_job_tran.employeeunkid ORDER BY hrempsubstantive_job_tran.effectivedate DESC) AS xNo " & _
                   "    FROM hrempsubstantive_job_tran " & _
                   "        JOIN hrjob_master ON hrempsubstantive_job_tran.jobunkid = hrjob_master.jobunkid " & _
                   "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effdate "

            If xEmployeeId > 0 Then
                StrQ &= " AND hrempsubstantive_job_tran.employeeunkid = '" & xEmployeeId & "' "
            End If
            StrQ &= ") AS CA WHERE CA.xNo = 1 "

            If mdtAppDate.Trim.Length > 0 Then
                StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            objDo.AddParameter("@effdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate))

            dsList = objDo.ExecQuery(StrQ, "List")

            If objDo.ErrorMessage <> "" Then
                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
            End If
        Catch ex As Exception
            If objDataOperation Is Nothing Then objDo = Nothing
            Throw New Exception(ex.Message & "; Procedure Name: Get_Current_SubstantiveJob; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function InsertAuditTrailForSubstantiveJob(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            StrQ = "INSERT INTO athrempsubstantive_job_tran ( " & _
                      " substantivetranunkid " & _
                      ", effectivedate " & _
                      ", startdate " & _
                      ", enddate " & _
                      ", employeeunkid " & _
                      ", jobunkid " & _
                      ", rehiretranunkid " & _
                      ", remark " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name" & _
                      ", form_name " & _
                      ", isweb" & _
                      ", expiry_reminder " & _
                      ", ntfsendremindertoemp " & _
                      ", ntfsendremindertousers " & _
                    ") VALUES (" & _
                      "  @substantivetranunkid " & _
                      ", @effectivedate " & _
                      ", @startdate " & _
                      ", @enddate " & _
                      ", @employeeunkid " & _
                      ", @jobunkid " & _
                      ", @rehiretranunkid " & _
                      ", @remark " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @machine_name" & _
                      ", @form_name " & _
                      ", @isweb" & _
                      ", @expiry_reminder " & _
                      ", @ntfsendremindertoemp " & _
                      ", @ntfsendremindertousers " & _
                    "); SELECT @@identity"

            'Pinkal (16-Oct-2023) -- (A1X-1458) Kairuki - Auto notification to selected users(s) and employee [x] number of days before expiry of an acting position.[@expiry_reminder , @ntfsendremindertoemp , @ntfsendremindertousers ]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@substantivetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSubstantivetranunkid)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate.ToString)

            If IsDBNull(mdtEnddate) = False AndAlso mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate.ToString)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJobunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP.Trim))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName.Trim))
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1458) Kairuki - Auto notification to selected users(s) and employee [x] number of days before expiry of an acting position.
            If IsDBNull(mdtexpiry_reminder) = False AndAlso mdtexpiry_reminder <> Nothing Then
                objDataOperation.AddParameter("@expiry_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtexpiry_reminder.ToString)
            Else
                objDataOperation.AddParameter("@expiry_reminder", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@ntfsendremindertoemp", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnntfsendremindertoemp.ToString)
            objDataOperation.AddParameter("@ntfsendremindertousers", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnntfsendremindertousers.ToString)
            'Pinkal (16-Oct-2023) -- End

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForSubstantiveJob; Module Name: " & mstrModuleName)
        Finally
            'Pinkal (16-Oct-2023) -- Start
            '(A1X-1458) Kairuki - Auto notification to selected users(s) and employee [x] number of days before expiry of an acting position.
            Language.getMessage(mstrModuleName, 5, "Reminder of Expiry Of An Acting Position")
            Language.getMessage(mstrModuleName, 6, "Dear")
            Language.getMessage(mstrModuleName, 7, "Please be informed that the official end date for")
            Language.getMessage(mstrModuleName, 8, "acting role as")
            Language.getMessage(mstrModuleName, 9, "Please take the necessary steps to ensure smooth transition.")
            'Pinkal (16-Oct-2023) -- End
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, Substantive Post information is already present for the selected effective date.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Substantive Post information is already present for the selected start date.")
            Language.setMessage(mstrModuleName, 3, "Sorry, Substantive Post information is already present for the selected end date.")
            Language.setMessage(mstrModuleName, 4, "Sorry, Substantive Post information is already present for the selected combination for the selected effective date.")
            Language.setMessage(mstrModuleName, 5, "Reminder of Expiry Of An Acting Position")
            Language.setMessage(mstrModuleName, 6, "Dear")
            Language.setMessage(mstrModuleName, 7, "Please be informed that the official end date for")
            Language.setMessage(mstrModuleName, 8, "acting role as")
            Language.setMessage(mstrModuleName, 9, "Please take the necessary steps to ensure smooth transition.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class