﻿'************************************************************************************************************************************
'Class Name : clsemployee_cctranhead_tran.vb
'Purpose    :
'Date       :07-Apr-2015
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsemployee_cctranhead_tran
    Private Shared ReadOnly mstrModuleName As String = "clsemployee_cctranhead_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintCctranheadunkid As Integer = 0
    Private mdtEffectivedate As Date = Nothing
    Private mintEmployeeunkid As Integer
    Private mintRehiretranunkid As Integer = 0
    Private mintCctranheadvalueid As Integer = 0
    Private mblnIstransactionhead As Boolean = False
    Private mintChangereasonunkid As Integer = 0
    Private mblnIsfromemployee As Boolean = False
    Private mintUserunkid As Integer = 0
    Private mintStatusunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cctranheadunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Cctranheadunkid(Optional ByVal objDataOper As clsDataOperation = Nothing) As Integer 'S.SANDEEP [25 OCT 2016] -- START {objDataOperation} -- END
        Get
            Return mintCctranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintCctranheadunkid = value
            'S.SANDEEP [25 OCT 2016] -- START
            'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
            'Call GetData()
            Call GetData(objDataOper)
            'S.SANDEEP [25 OCT 2016] -- END
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cctranheadvalueid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Cctranheadvalueid() As Integer
        Get
            Return mintCctranheadvalueid
        End Get
        Set(ByVal value As Integer)
            mintCctranheadvalueid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set istransactionhead
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Istransactionhead() As Boolean
        Get
            Return mblnIstransactionhead
        End Get
        Set(ByVal value As Boolean)
            mblnIstransactionhead = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changereasonunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Changereasonunkid() As Integer
        Get
            Return mintChangereasonunkid
        End Get
        Set(ByVal value As Integer)
            mintChangereasonunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfromemployee
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isfromemployee() As Boolean
        Get
            Return mblnIsfromemployee
        End Get
        Set(ByVal value As Boolean)
            mblnIsfromemployee = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webformname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webclientip
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set webhostname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOpr As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
              "  cctranheadunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", rehiretranunkid " & _
              ", cctranheadvalueid " & _
              ", istransactionhead " & _
              ", changereasonunkid " & _
              ", isfromemployee " & _
              ", userunkid " & _
              ", statusunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hremployee_cctranhead_tran " & _
             "WHERE cctranheadunkid = @cctranheadunkid "

            objDataOperation.AddParameter("@cctranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCctranheadunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCctranheadunkid = CInt(dtRow.Item("cctranheadunkid"))
                mdtEffectivedate = dtRow.Item("effectivedate")
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintRehiretranunkid = CInt(dtRow.Item("rehiretranunkid"))
                mintCctranheadvalueid = CInt(dtRow.Item("cctranheadvalueid"))
                mblnIstransactionhead = CBool(dtRow.Item("istransactionhead"))
                mintChangereasonunkid = CInt(dtRow.Item("changereasonunkid"))
                mblnIsfromemployee = CBool(dtRow.Item("isfromemployee"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal blnIsCostCenter As Boolean, Optional ByVal xEmployeeUnkid As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdtAppDate As String = ""
        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                   "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                   "FROM hremployee_master " & _
                   "WHERE employeeunkid = '" & xEmployeeUnkid & "' "

            objDataOperation.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtAppDate = objDataOperation.GetParameterValue("@ADate")

            strQ = "SELECT " & _
                   "  hremployee_cctranhead_tran.cctranheadunkid " & _
                   ", hremployee_cctranhead_tran.effectivedate " & _
                   ", hremployee_cctranhead_tran.employeeunkid " & _
                   ", hremployee_cctranhead_tran.rehiretranunkid " & _
                   ", hremployee_cctranhead_tran.cctranheadvalueid " & _
                   ", hremployee_cctranhead_tran.istransactionhead " & _
                   ", hremployee_cctranhead_tran.changereasonunkid " & _
                   ", hremployee_cctranhead_tran.isfromemployee " & _
                   ", hremployee_cctranhead_tran.userunkid " & _
                   ", hremployee_cctranhead_tran.statusunkid " & _
                   ", hremployee_cctranhead_tran.isvoid " & _
                   ", hremployee_cctranhead_tran.voiduserunkid " & _
                   ", hremployee_cctranhead_tran.voiddatetime " & _
                   ", hremployee_cctranhead_tran.voidreason " & _
                   ", hremployee_master.employeecode as ecode " & _
                   ", hremployee_master.firstname+' '+ISNULL(hremployee_master.othername,'')+' '+hremployee_master.surname AS ename " & _
                   ", CONVERT(CHAR(8),hremployee_cctranhead_tran.effectivedate,112) AS edate " & _
                   ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS adate " & _
                   ", '' AS EffDate " & _
                   ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE ISNULL(cfcommon_master.name,'') END AS CReason " & _
                   ", CASE WHEN hremployee_cctranhead_tran.istransactionhead = 1 THEN ISNULL(prtranhead_master.trnheadname,'') " & _
                   "    ELSE ISNULL(prcostcenter_master.costcentername,'') END AS DispValue " & _
                   ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
                   ", ISNULL(prtranhead_master.trnheadname,'') AS trnheadname " & _
                   "FROM hremployee_cctranhead_tran " & _
                   "  JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_cctranhead_tran.employeeunkid " & _
                   "  LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_cctranhead_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.COST_CENTER & _
                   "  LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_cctranhead_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                   "  LEFT JOIN prcostcenter_master ON cctranheadvalueid = prcostcenter_master.costcenterunkid " & _
                   "  LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = hremployee_cctranhead_tran.cctranheadvalueid " & _
                   "WHERE hremployee_cctranhead_tran.isvoid = 0 "

            If blnIsCostCenter = True Then
                strQ &= " AND hremployee_cctranhead_tran.istransactionhead = 0 "
            Else
                strQ &= " AND hremployee_cctranhead_tran.istransactionhead = 1 "
            End If

            If xEmployeeUnkid Then
                strQ &= " AND hremployee_cctranhead_tran.employeeunkid = '" & xEmployeeUnkid & "' "
            End If

            If mdtAppDate.Trim.Length > 0 Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_cctranhead_tran.effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            strQ &= " ORDER BY effectivedate DESC "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each xRow As DataRow In dsList.Tables(0).Rows
                xRow("EffDate") = eZeeDate.convertDate(xRow.Item("edate").ToString).ToShortDateString
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_cctranhead_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        Dim blnIsCostCenter As Boolean = False
        If mblnIstransactionhead = True Then
            blnIsCostCenter = False
        Else
            blnIsCostCenter = True
        End If


        If isExist(mdtEffectivedate, -1, blnIsCostCenter, mintEmployeeunkid, , objDataOperation) Then
            If mblnIstransactionhead = True Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transaction head information is already present for the selected effective date.")
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, cost center information is already present for the selected effective date.")
            End If
            Return False
        End If


        'Pinkal (09-Apr-2015) -- Start
        'Enhancement - VALIDATION ON EMPLOYEE MASTER OF LEAVE MODULE WHEN CHANGING APPOINTMENT DATE FOR TANAPA

        'S.SANDEEP [29 APR 2015] -- START
        'ALLOW SAME COST CENTER & TRANSACTION HEADES IF EMPLOYEE IS REHIRED AND USER WANT PUT IN SAME
        'dsList = Get_Current_CostCenter_TranHeads(mdtEffectivedate.Date, blnIsCostCenter, mintEmployeeunkid)
        'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
        '    If CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid")) = mintCctranheadvalueid Then
        '        If mblnIstransactionhead = True Then
        '            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transaction head information is already present for the selected effective date.")
        '        Else
        '            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, cost center information is already present for the selected effective date.")
        '        End If
        '        Return False
        '    End If
        'End If
        If mintRehiretranunkid <= 0 Then
        dsList = Get_Current_CostCenter_TranHeads(mdtEffectivedate.Date, blnIsCostCenter, mintEmployeeunkid)
        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            If CInt(dsList.Tables(0).Rows(0)("cctranheadvalueid")) = mintCctranheadvalueid Then
                If mblnIstransactionhead = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transaction head information is already present for the selected effective date.")
                Else
                    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, cost center information is already present for the selected effective date.")
                End If
                Return False
            End If
        End If
        End If
        'S.SANDEEP [29 APR 2015] -- START

        'Pinkal (09-Apr-2015) -- End


        If isExist(mdtEffectivedate, mintCctranheadvalueid, blnIsCostCenter, _
                   mintEmployeeunkid, , objDataOperation) Then
            If mblnIstransactionhead = True Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transaction head information is already present for the selected effective date.")
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, cost center information is already present for the selected effective date.")
            End If
            Return False
        End If



        If xDataOpr Is Nothing Then
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@cctranheadvalueid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCctranheadvalueid.ToString)
            objDataOperation.AddParameter("@istransactionhead", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIstransactionhead.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO hremployee_cctranhead_tran ( " & _
                       "  effectivedate " & _
                       ", employeeunkid " & _
                       ", rehiretranunkid " & _
                       ", cctranheadvalueid " & _
                       ", istransactionhead " & _
                       ", changereasonunkid " & _
                       ", isfromemployee " & _
                       ", userunkid " & _
                       ", statusunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason" & _
                   ") VALUES (" & _
                       "  @effectivedate " & _
                       ", @employeeunkid " & _
                       ", @rehiretranunkid " & _
                       ", @cctranheadvalueid " & _
                       ", @istransactionhead " & _
                       ", @changereasonunkid " & _
                       ", @isfromemployee " & _
                       ", @userunkid " & _
                       ", @statusunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCctranheadunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForCCTranHeads(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_cctranhead_tran) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        Dim blnIsCostCenter As Boolean = False
        If mblnIstransactionhead = True Then
            blnIsCostCenter = False
        Else
            blnIsCostCenter = True
        End If

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        If isExist(mdtEffectivedate, -1, blnIsCostCenter, mintEmployeeunkid, mintCctranheadunkid, objDataOperation) Then
            If mblnIstransactionhead = True Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transaction head information is already present for the selected effective date.")
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, cost center information is already present for the selected effective date.")
            End If
            Return False
        End If

        If isExist(mdtEffectivedate, mintCctranheadvalueid, blnIsCostCenter, _
                   mintEmployeeunkid, mintCctranheadunkid, objDataOperation) Then
            If mblnIstransactionhead = True Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, transaction head information is already present for the selected effective date.")
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, cost center information is already present for the selected effective date.")
            End If
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@cctranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCctranheadunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@cctranheadvalueid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCctranheadvalueid.ToString)
            objDataOperation.AddParameter("@istransactionhead", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIstransactionhead.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE hremployee_cctranhead_tran SET " & _
                   "  effectivedate = @effectivedate" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", rehiretranunkid = @rehiretranunkid" & _
                   ", cctranheadvalueid = @cctranheadvalueid" & _
                   ", istransactionhead = @istransactionhead" & _
                   ", changereasonunkid = @changereasonunkid" & _
                   ", isfromemployee = @isfromemployee" & _
                   ", userunkid = @userunkid" & _
                   ", statusunkid = @statusunkid" & _
                   ", isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE cctranheadunkid = @cctranheadunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForCCTranHeads(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_cctranhead_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean 'S.SANDEEP [01 JUN 2016] -- START -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "UPDATE hremployee_cctranhead_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE cctranheadunkid = @cctranheadunkid "

            objDataOperation.AddParameter("@cctranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ClearParameters()
            mintCctranheadunkid = intUnkid

            Call GetData(objDataOperation)

            If InsertAuditTrailForCCTranHeads(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xEffDate As DateTime, _
                            ByVal xCCTrnHeadId As Integer, _
                            ByVal xIsCostCenter As Boolean, _
                            ByVal xEmployeeId As Integer, _
                            Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
            objDataOperation.ClearParameters()
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                   "  cctranheadunkid " & _
                   " ,effectivedate " & _
                   " ,employeeunkid " & _
                   " ,rehiretranunkid " & _
                   " ,cctranheadvalueid " & _
                   " ,istransactionhead " & _
                   " ,changereasonunkid " & _
                   " ,isfromemployee " & _
                   " ,userunkid " & _
                   " ,statusunkid " & _
                   " ,isvoid " & _
                   " ,voiduserunkid " & _
                   " ,voiddatetime " & _
                   " ,voidreason " & _
                   "FROM hremployee_cctranhead_tran " & _
                   "WHERE isvoid = 0 "

            If xEffDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate "
            End If

            If intUnkid > 0 Then
                strQ &= " AND cctranheadunkid <> @cctranheadunkid "
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            If xCCTrnHeadId > 0 Then
                strQ &= " AND cctranheadvalueid = @cctranheadvalueid "
                objDataOperation.AddParameter("@cctranheadvalueid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCCTrnHeadId)
            End If

            If xIsCostCenter = True Then
                strQ &= " AND istransactionhead = 0 "
            Else
                strQ &= " AND istransactionhead = 1 "
            End If

            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEffDate))
            objDataOperation.AddParameter("@cctranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Private Function InsertAuditTrailForCCTranHeads(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As enAuditType) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            StrQ = "INSERT INTO athremployee_cctranhead_tran ( " & _
                       "  cctranheadunkid " & _
                       ", effectivedate " & _
                       ", employeeunkid " & _
                       ", rehiretranunkid " & _
                       ", cctranheadvalueid " & _
                       ", istransactionhead " & _
                       ", changereasonunkid " & _
                       ", isfromemployee " & _
                       ", statusunkid " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", machine_name " & _
                       ", form_name " & _
                       ", module_name1 " & _
                       ", module_name2 " & _
                       ", module_name3 " & _
                       ", module_name4 " & _
                       ", module_name5 " & _
                       ", isweb" & _
                   ") VALUES (" & _
                       "  @cctranheadunkid " & _
                       ", @effectivedate " & _
                       ", @employeeunkid " & _
                       ", @rehiretranunkid " & _
                       ", @cctranheadvalueid " & _
                       ", @istransactionhead " & _
                       ", @changereasonunkid " & _
                       ", @isfromemployee " & _
                       ", @statusunkid " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @machine_name " & _
                       ", @form_name " & _
                       ", @module_name1 " & _
                       ", @module_name2 " & _
                       ", @module_name3 " & _
                       ", @module_name4 " & _
                       ", @module_name5 " & _
                       ", @isweb" & _
                   ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@cctranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCctranheadunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@cctranheadvalueid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCctranheadvalueid.ToString)
            objDataOperation.AddParameter("@istransactionhead", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIstransactionhead.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebClientIP.Trim = "", getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim = "", getHostName, mstrWebHostName))
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 3, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForCCTranHeads; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Get_Current_CostCenter_TranHeads(ByVal xDate As Date, ByVal blnIsCostCenter As Boolean, Optional ByVal xEmployeeId As Integer = 0) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        'S.SANDEEP [07 APR 2015] -- START
        Dim mdtAppDate As String = ""
        'S.SANDEEP [07 APR 2015] -- END
        Try
            Using objDo As New clsDataOperation

                'S.SANDEEP [07 APR 2015] -- START
                If xEmployeeId > 0 Then
                    StrQ = "SELECT " & _
                           "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                           "FROM hremployee_master " & _
                           "WHERE employeeunkid = '" & xEmployeeId & "' "

                    objDo.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

                    objDo.ExecNonQuery(StrQ)

                    If objDo.ErrorMessage <> "" Then
                        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    End If

                    mdtAppDate = objDo.GetParameterValue("@ADate")
                End If
                'S.SANDEEP [07 APR 2015] -- END

                StrQ = "SELECT " & _
                       "  effectivedate " & _
                       ", employeeunkid " & _
                       ", cctranheadvalueid " & _
                       "FROM " & _
                       "( " & _
                       "  SELECT " & _
                       "     effectivedate " & _
                       "    ,employeeunkid " & _
                       "    ,cctranheadvalueid " & _
                       "    ,istransactionhead " & _
                       "    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                       "  FROM hremployee_cctranhead_tran " & _
                       "  WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= @effdate "
                If blnIsCostCenter = True Then
                    StrQ &= " AND istransactionhead = 0 "
                Else
                    StrQ &= " AND istransactionhead = 1 "
                End If
                If xEmployeeId > 0 Then
                    StrQ &= " AND employeeunkid = '" & xEmployeeId & "' "
                End If
                StrQ &= ") AS CA WHERE CA.xNo = 1 "

                'S.SANDEEP [07 APR 2015] -- START
                If mdtAppDate.Trim.Length > 0 Then
                    StrQ &= " AND CONVERT(CHAR(8),effectivedate,112) >= '" & mdtAppDate & "' "
                End If
                'S.SANDEEP [07 APR 2015] -- END

                objDo.AddParameter("@effdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xDate))

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Current_Allocation; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function IsDetailPresent(ByVal xEmployeeId As Integer, ByVal blnIsCostCenter As Boolean, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = 0
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            StrQ = "SELECT 1 FROM hremployee_cctranhead_tran WHERE employeeunkid = '" & xEmployeeId & "'  AND isvoid = 0 "
            If blnIsCostCenter = True Then
                StrQ &= " AND istransactionhead = 0 "
            Else
                StrQ &= " AND istransactionhead = 1 "
            End If

            iCnt = objDataOperation.RecordCount(StrQ)

            If iCnt > 0 Then
                blnFlag = True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsPermitPresent; Module Name: " & mstrModuleName)
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
        Return blnFlag
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, transaction head information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 2, "Sorry, cost center information is already present for the selected effective date.")
			Language.setMessage(mstrModuleName, 3, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
