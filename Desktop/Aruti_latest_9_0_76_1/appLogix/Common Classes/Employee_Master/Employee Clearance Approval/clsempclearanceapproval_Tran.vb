﻿'************************************************************************************************************************************
'Class Name : clsempclearanceapproval_Tran.vb
'Purpose      :
'Date           : 06-Jan-2025
'Written By   : Pinkal
'Modified      :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Xml
Imports System.Net
Imports System.IO

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsempclearanceapproval_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsempclearanceapproval_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintEmployeeunkid As Integer
    Private mintMappingunkid As Integer
    Private mintStatusunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mblnIsnotified As Boolean
    Private mdtReminderDate As DateTime = Nothing
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHostname As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Pinkal 
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isnotified
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isnotified() As Boolean
        Get
            Return mblnIsnotified
        End Get
        Set(ByVal value As Boolean)
            mblnIsnotified = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtReminderDate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ReminderDate() As DateTime
        Get
            Return mdtReminderDate
        End Get
        Set(ByVal value As DateTime)
            mdtReminderDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Hostname() As String
        Get
            Return mstrHostname
        End Get
        Set(ByVal value As String)
            mstrHostname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property


#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If


        Try
            strQ = "SELECT " & _
                      "  tranguid " & _
                      ", transactiondate " & _
                      ", employeeunkid " & _
                      ", mappingunkid " & _
                      ", statusunkid " & _
                      ", remark " & _
                      ", isfinal " & _
                      ", reminder_date " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", voiduserunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", ip " & _
                      ", hostname " & _
                      ", form_name " & _
                      ", isweb " & _
                      " FROM hrempclearanceapproval_tran " & _
                      " WHERE tranguid = @tranguid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mstrTranguid = dtRow.Item("tranguid").ToString
                mdtTransactiondate = dtRow.Item("transactiondate")
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrRemark = dtRow.Item("remark").ToString
                mblnIsfinal = CBool(dtRow.Item("isfinal"))

                If Not IsDBNull(dtRow.Item("reminder_date")) Then
                    mdtReminderDate = dtRow.Item("reminder_date")
                End If

                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintAudittype = CInt(dtRow.Item("audittype"))
                mintAudituserunkid = CInt(dtRow.Item("audituserunkid"))
                mstrIp = dtRow.Item("ip").ToString
                mstrHostname = dtRow.Item("hostname").ToString
                mstrForm_Name = dtRow.Item("form_name").ToString
                mblnIsweb = CBool(dtRow.Item("isweb"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal iEmployeeID As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = " SELECT " & _
                      "  tranguid " & _
                      ", transactiondate " & _
                      ", employeeunkid " & _
                      ", mappingunkid " & _
                      ", statusunkid " & _
                      ", remark " & _
                      ", isfinal " & _
                      ", reminder_date " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", voiduserunkid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", ip " & _
                      ", hostname " & _
                      ", form_name " & _
                      ", isweb " & _
                      " FROM hrempclearanceapproval_tran "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid  = 0 "
            End If

            If iEmployeeID > 0 Then
                strQ &= " AND hrempclearanceapproval_tran.employeeunkid = @employeeunkid"
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeID)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployeeapproval_tran) </purpose>
    Public Function Insert(ByVal mdtTable As DataTable, _
                           ByVal xDatabaseName As String, _
                           ByVal xYearId As Integer, _
                           ByVal xCompanyunkid As Integer, _
                           ByVal dtTotal_Active_Employee_AsOnDate As Date, _
                           ByVal blnIsArutiDemo As String, _
                           ByVal intTotal_Active_Employee_ForAllCompany As Integer, _
                           ByVal intNoOfEmployees As Integer, _
                           ByVal intUserUnkId As Integer, _
                           ByVal blnDonotAttendanceinSeconds As Boolean, _
                           ByVal xUserModeSetting As String, _
                           ByVal xPeriodStart As DateTime, _
                           ByVal xPeriodEnd As DateTime, _
                           ByVal xOnlyApproved As Boolean, _
                           ByVal xIncludeIn_ActiveEmployee As Boolean, _
                           ByVal blnAllowToApproveEarningDeduction As Boolean, _
                           ByVal dtCurrentDateTime As DateTime, _
                           ByVal mblnCreateADUserFromEmp As Boolean, _
                           ByVal mblnUserMustChangePwdOnNextLogOn As Boolean, _
                           Optional ByVal xHostName As String = "", _
                           Optional ByVal xIPAddr As String = "", _
                           Optional ByVal xLoggedUserName As String = "", _
                           Optional ByVal xLoginMod As enLogin_Mode = enLogin_Mode.DESKTOP, _
                           Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objEmp As New clsEmployee_Master
        Dim objDataOperation As clsDataOperation = Nothing

        If mdtTable Is Nothing Then Return False

        Dim mintEmployeeClearanceApprovalReminderOccurrenceAfterDays As Integer = 0
        Dim objConfg As New clsConfigOptions
        Dim mstrEmployeeClearanceApprovalReminderOccurrenceAfterDays As String = objConfg.GetKeyValue(xCompanyunkid, "EmployeeClearanceApprovalReminderOccurrenceAfterDays", Nothing)
        If mstrEmployeeClearanceApprovalReminderOccurrenceAfterDays.Trim.Length > 0 Then
            mintEmployeeClearanceApprovalReminderOccurrenceAfterDays = CInt(mstrEmployeeClearanceApprovalReminderOccurrenceAfterDays)
        End If
        objConfg = Nothing


        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If



        Try

            strQ = "INSERT INTO hrempclearanceapproval_tran ( " & _
                   "  tranguid " & _
                   ", employeeunkid " & _
                   ", transactiondate " & _
                   ", mappingunkid " & _
                   ", statusunkid " & _
                   ", remark " & _
                   ", isfinal " & _
                   ", isnotified " & _
                   ", isvoid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   ", voiduserunkid " & _
                   ", audittype " & _
                   ", audituserunkid " & _
                   ", ip " & _
                   ", hostname " & _
                   ", form_name " & _
                   ", isweb" & _
                   ", reminder_date " & _
                 ") VALUES (" & _
                   "  @tranguid " & _
                   ", @employeeunkid " & _
                   ", @transactiondate " & _
                   ", @mappingunkid " & _
                   ", @statusunkid " & _
                   ", @remark " & _
                   ", @isfinal " & _
                   ", @isnotified " & _
                   ", @isvoid " & _
                   ", @voiddatetime " & _
                   ", @voidreason " & _
                   ", @voiduserunkid " & _
                   ", @audittype " & _
                   ", @audituserunkid " & _
                   ", @ip " & _
                   ", @hostname " & _
                   ", @form_name " & _
                   ", @isweb" & _
                   ", @reminder_date " & _
                 "); "


            For Each dr As DataRow In mdtTable.Rows

                If isExist(CInt(dr("employeeunkid")), mintStatusunkid, "", objDataOperation, mintMappingunkid) = False Then

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                    objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid)
                    objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
                    objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
                    objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
                    objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(dr("isfinal")).ToString)

                    If mintStatusunkid = clsEmployee_Master.EmpApprovalStatus.Approved OrElse mintStatusunkid = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                        objDataOperation.AddParameter("@isnotified", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    Else
                        objDataOperation.AddParameter("@isnotified", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsnotified.ToString)
                    End If
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

                    If mdtVoiddatetime <> Nothing Then
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    Else
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    End If

                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
                    objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
                    objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
                    objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
                    objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
                    objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

                    If mintEmployeeClearanceApprovalReminderOccurrenceAfterDays > 0 Then
                        mdtReminderDate = Now.AddDays(mintEmployeeClearanceApprovalReminderOccurrenceAfterDays).Date
                    End If

                    If mdtReminderDate <> Nothing AndAlso (mintStatusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval OrElse mintStatusunkid = clsEmployee_Master.EmpApprovalStatus.Approved) Then
                        objDataOperation.AddParameter("@reminder_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReminderDate)
                    Else
                        objDataOperation.AddParameter("@reminder_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                    End If

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'If mintEmployeeClearanceApprovalReminderOccurrenceAfterDays > 0 Then
                    '    If UpdateClearanceEmployeeApprovalReminderDate(objDataOperation, CInt(dr("employeeunkid")), mstrTranguid, mdtReminderDate) = False Then
                    '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '        Throw exForce
                    '    End If
                    'End If

                    If CBool(dr("isfinal")) Then

                        If mintStatusunkid = clsEmployee_Master.EmpApprovalStatus.Approved Then
                            objEmp._DataOperation = objDataOperation
                            objEmp._Employeeunkid(xPeriodEnd) = CInt(dr("employeeunkid"))
                            objEmp._IsClear = True

                            If objEmp.Update(xDatabaseName, _
                               xYearId, _
                               xCompanyunkid, _
                               dtTotal_Active_Employee_AsOnDate, _
                               blnIsArutiDemo, _
                               intTotal_Active_Employee_ForAllCompany, _
                               intNoOfEmployees, _
                               intUserUnkId, _
                               blnDonotAttendanceinSeconds, _
                               xUserModeSetting, xPeriodStart, _
                               xPeriodEnd, _
                               xOnlyApproved, _
                               xIncludeIn_ActiveEmployee, _
                               blnAllowToApproveEarningDeduction, _
                               dtCurrentDateTime, mblnCreateADUserFromEmp, mblnUserMustChangePwdOnNextLogOn, , , , , , , , , objDataOperation, , , , xHostName, xIPAddr, xLoggedUserName, xLoginMod, False, True, mstrForm_Name, False, -1, False, -1) = False Then

                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If

                        End If  '   If mintStatusunkid = clsEmployee_Master.EmpApprovalStatus.Approved Then

                    End If  ' If CBool(dr("isfinal")) Then

                End If  '   If isExist(CInt(dr("employeeunkid")), mintStatusunkid, "", objDataOperation, mintMappingunkid) = False Then

            Next

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception

            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)

            If ex.Message.ToString.Contains("Sorry, record cannot be saved. Some allocation does not exist in active directory.") Then
                mstrMessage = ex.Message
            ElseIf ex.Message.ToString().Contains("System cannot connect with Active Directory at the moment. Please check Active Directory connection.  Also username or password (Aruti to AD Server) may be incorrect.") Then
                mstrMessage = ex.Message
            Else
                Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            End If
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objEmp = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployeeapproval_tran) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

            strQ = "UPDATE hremployeeapproval_tran SET " & _
              "  employeeunkid = @employeeunkid" & _
              ", transactiondate = @transactiondate" & _
              ", mappingunkid = @mappingunkid" & _
              ", statusunkid = @statusunkid" & _
              ", remark = @remark" & _
              ", isfinal = @isfinal" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", audittype = @audittype" & _
              ", audituserunkid = @audituserunkid" & _
              ", ip = @ip" & _
              ", hostname = @hostname" & _
              ", form_name = @form_name" & _
              ", isweb = @isweb " & _
            "WHERE tranguid = @tranguid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployeeapproval_tran) </purpose>
    Public Function Delete(ByVal strtranguid As String, ByVal intuserunkid As Integer, ByVal intempid As Integer, ByVal strreasion As String) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " UPDATE hrempclearanceapproval_tran SET " & _
                      " isvoid = 1,voiddatetime = getdate(),voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                      " WHERE tranguid = @tranguid AND employeeunkid = @employeeunkid "

            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strtranguid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strreasion)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, intuserunkid)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intempid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@tranguid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function isExist(ByVal intEmpID As Integer, ByVal intStatusId As Integer, Optional ByVal intUnkid As String = "", Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal xMappingunkid As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If

        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
                   "     A.employeeunkid " & _
                   "    ,A.statusunkid " & _
                   "    ,A.tranguid " & _
                   "    ,A.mappingunkid " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY transactiondate DESC) AS rno " & _
                   "        ,employeeunkid " & _
                   "        ,statusunkid " & _
                   "        ,tranguid " & _
                   "        ,mappingunkid " & _
                   "    FROM hrempclearanceapproval_tran " & _
                   "    WHERE isvoid = 0 " & _
                   ") AS A WHERE A.rno = 1 AND employeeunkid = @employeeunkid AND statusunkid = @statusunkid "

            If intUnkid <> "" Then
                strQ &= " AND tranguid <> @tranguid"
            End If

            If xMappingunkid > 0 Then
                strQ &= " AND mappingunkid = @mappingunkid"
                objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMappingunkid)
            End If


            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpID)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusId)
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    'Public Sub GetUACFilters(ByVal strDatabaseName As String, ByVal strUserAccessMode As String, ByRef strFilter As String, ByRef strJoin As String, ByRef strSelect As String, ByRef strAccessJoin As String, ByRef strOuterJoin As String, ByRef strFinalString As String, ByVal intUserId As Integer, ByRef strOuterSelect As String, Optional ByVal xDataOpr As clsDataOperation = Nothing)
    '    'Hemant (22 Dec 2023) -- [strOuterSelect]
    '    Dim objDataOperation As New clsDataOperation
    '    If xDataOpr IsNot Nothing Then
    '        objDataOperation = xDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Dim StrQ As String = ""
    '    Try
    '        If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
    '        Dim strvalues() As String = strUserAccessMode.Split(",")
    '        For index As Integer = 0 To strvalues.Length - 1
    '            Select Case strvalues(index)
    '                Case enAllocation.BRANCH
    '                    strSelect &= ",ISNULL(BR.branchid,0) AS branchid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as branchid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
    '                                     "    ) AS BR ON BR.userunkid = cfuser_master.userunkid "

    '                    'Hemant (22 Dec 2023) -- Start
    '                    'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '                    'strFilter &= "AND ISNULL(BR.branchid,0) > 0 "
    '                    strFilter &= "AND ISNULL(A.branchid,0) > 0 "
    '                    strOuterSelect &= ",FN.branchid "
    '                    'Hemant (22 Dec 2023) -- End
    '                    strJoin &= "AND Fn.branchid = [@emp].branchid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrstation_master ON Fn.branchid = hrstation_master.stationunkid "

    '                Case enAllocation.DEPARTMENT_GROUP
    '                    strSelect &= ",ISNULL(DG.deptgrpid,0) AS deptgrpid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as deptgrpid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
    '                                     "    ) AS DG ON DG.userunkid = cfuser_master.userunkid "
    '                    'Hemant (22 Dec 2023) -- Start
    '                    'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '                    'strFilter &= "AND ISNULL(DG.deptgrpid,0) > 0 "
    '                    strFilter &= "AND ISNULL(A.deptgrpid,0) > 0 "
    '                    strOuterSelect &= ",FN.deptgrpid "
    '                    'Hemant (22 Dec 2023) -- End
    '                    strJoin &= "AND Fn.deptgrpid = [@emp].deptgrpid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master ON Fn.deptgrpid = hrdepartment_group_master.deptgroupunkid "

    '                Case enAllocation.DEPARTMENT
    '                    strSelect &= ",ISNULL(DP.deptid,0) AS deptid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as deptid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
    '                                     "    ) AS DP ON DP.userunkid = cfuser_master.userunkid "
    '                    'Hemant (22 Dec 2023) -- Start
    '                    'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '                    'strFilter &= "AND ISNULL(DP.deptid,0) > 0 "
    '                    strFilter &= "AND ISNULL(A.deptid,0) > 0 "
    '                    strOuterSelect &= ",FN.deptid "
    '                    'Hemant (22 Dec 2023) -- End
    '                    strJoin &= "AND Fn.deptid = [@emp].deptid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON Fn.deptid = hrdepartment_master.departmentunkid "

    '                Case enAllocation.SECTION_GROUP
    '                    strSelect &= ",ISNULL(SG.secgrpid,0) AS secgrpid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as secgrpid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
    '                                     "    ) AS SG ON SG.userunkid = cfuser_master.userunkid "
    '                    'Hemant (22 Dec 2023) -- Start
    '                    'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '                    'strFilter &= "AND ISNULL(SG.secgrpid,0) > 0 "
    '                    strFilter &= "AND ISNULL(A.secgrpid,0) > 0 "
    '                    strOuterSelect &= ",FN.secgrpid "
    '                    'Hemant (22 Dec 2023) -- End
    '                    strJoin &= "AND Fn.secgrpid = [@emp].secgrpid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON Fn.secgrpid = hrsectiongroup_master.sectiongroupunkid "

    '                Case enAllocation.SECTION
    '                    strSelect &= ",ISNULL(SC.secid,0) AS secid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as secid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
    '                                     "    ) AS SC ON SC.userunkid = cfuser_master.userunkid "
    '                    'Hemant (22 Dec 2023) -- Start
    '                    'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '                    'strFilter &= "AND ISNULL(SC.secid,0) > 0 "
    '                    strFilter &= "AND ISNULL(A.secid,0) > 0 "
    '                    strOuterSelect &= ",FN.secid "
    '                    'Hemant (22 Dec 2023) -- End
    '                    strJoin &= "AND Fn.secid = [@emp].secid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsection_master ON Fn.secid = hrsection_master.sectionunkid "

    '                Case enAllocation.UNIT_GROUP
    '                    strSelect &= ",ISNULL(UG.unitgrpid,0) AS unitgrpid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as unitgrpid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
    '                                     "    ) AS UG ON UG.userunkid = cfuser_master.userunkid "
    '                    'Hemant (22 Dec 2023) -- Start
    '                    'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '                    'strFilter &= "AND ISNULL(UG.unitgrpid,0) > 0 "
    '                    strFilter &= "AND ISNULL(A.unitgrpid,0) > 0 "
    '                    strOuterSelect &= ",FN.unitgrpid "
    '                    'Hemant (22 Dec 2023) -- End
    '                    strJoin &= "AND Fn.unitgrpid = [@emp].unitgrpid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master ON Fn.unitgrpid = hrunitgroup_master.unitgroupunkid "

    '                Case enAllocation.UNIT
    '                    strSelect &= ",ISNULL(UT.unitid,0) AS unitid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as unitid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
    '                                     "    ) AS UT ON UT.userunkid = cfuser_master.userunkid "
    '                    'Hemant (22 Dec 2023) -- Start
    '                    'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '                    'strFilter &= "AND ISNULL(UT.unitid,0) > 0 "
    '                    strFilter &= "AND ISNULL(A.unitid,0) > 0 "
    '                    strOuterSelect &= ",FN.unitid "
    '                    'Hemant (22 Dec 2023) -- End
    '                    strJoin &= "AND Fn.unitid = [@emp].unitid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunit_master ON Fn.unitid = hrunit_master.unitunkid "

    '                Case enAllocation.TEAM
    '                    strSelect &= ",ISNULL(TM.teamid,0) AS teamid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as teamid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
    '                                     "    ) AS TM ON TM.userunkid = cfuser_master.userunkid "
    '                    'Hemant (22 Dec 2023) -- Start
    '                    'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '                    'strFilter &= "AND ISNULL(TM.teamid,0) > 0 "
    '                    strFilter &= "AND ISNULL(A.teamid,0) > 0 "
    '                    strOuterSelect &= ",FN.teamid "
    '                    'Hemant (22 Dec 2023) -- End
    '                    strJoin &= "AND Fn.teamid = [@emp].teamid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrteam_master ON Fn.teamid = hrteam_master.teamunkid "

    '                Case enAllocation.JOB_GROUP
    '                    strSelect &= " ,ISNULL(JG.jgrpid,0) AS jgrpid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as jgrpid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
    '                                     "    ) AS JG ON JG.userunkid = cfuser_master.userunkid "
    '                    'Hemant (22 Dec 2023) -- Start
    '                    'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '                    'strFilter &= "AND ISNULL(JG.jgrpid,0) > 0 "
    '                    strFilter &= "AND ISNULL(A.jgrpid,0) > 0 "
    '                    strOuterSelect &= ",FN.jgrpid "
    '                    'Hemant (22 Dec 2023) -- End
    '                    strJoin &= "AND Fn.jgrpid = [@emp].jgrpid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON Fn.jgrpid = hrjobgroup_master.jobgroupunkid "

    '                Case enAllocation.JOBS
    '                    strSelect &= ",ISNULL(JB.jobid,0) AS jobid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as jobid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
    '                                     "    ) AS JB ON JB.userunkid = cfuser_master.userunkid "
    '                    'Hemant (22 Dec 2023) -- Start
    '                    'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '                    'strFilter &= "AND ISNULL(JB.jobid,0) > 0 "
    '                    strFilter &= "AND ISNULL(A.jobid,0) > 0 "
    '                    strOuterSelect &= ",FN.jobid "
    '                    'Hemant (22 Dec 2023) -- End
    '                    strJoin &= "AND Fn.jobid = [@emp].jobid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjob_master ON Fn.jobid = hrjob_master.jobunkid "

    '                Case enAllocation.CLASS_GROUP
    '                    strSelect &= ",ISNULL(CG.clsgrpid,0) AS clsgrpid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as clsgrpid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
    '                                     "    ) AS CG ON CG.userunkid = cfuser_master.userunkid "
    '                    'Hemant (22 Dec 2023) -- Start
    '                    'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '                    'strFilter &= "AND ISNULL(CG.clsgrpid,0) > 0 "
    '                    strFilter &= "AND ISNULL(A.clsgrpid,0) > 0 "
    '                    strOuterSelect &= ",FN.clsgrpid "
    '                    'Hemant (22 Dec 2023) -- End
    '                    strJoin &= "AND Fn.clsgrpid = [@emp].clsgrpid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON Fn.clsgrpid = hrclassgroup_master.classgroupunkid "

    '                Case enAllocation.CLASSES
    '                    strSelect &= ",ISNULL(CL.clsid,0) AS clsid "
    '                    strAccessJoin &= "    LEFT JOIN " & _
    '                                     "    ( " & _
    '                                     "        SELECT " & _
    '                                     "             UPM.userunkid " & _
    '                                     "            ,allocationunkid as clsid " & _
    '                                     "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '                                     "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '                                     "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
    '                                     "    ) AS CL ON CL.userunkid = cfuser_master.userunkid "
    '                    'Hemant (22 Dec 2023) -- Start
    '                    'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '                    'strFilter &= "AND ISNULL(CL.clsid,0) > 0 "
    '                    strFilter &= "AND ISNULL(A.clsid,0) > 0 "
    '                    strOuterSelect &= ",FN.clsid "
    '                    'Hemant (22 Dec 2023) -- End
    '                    strJoin &= "AND Fn.clsid = [@emp].clsid "
    '                    strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON Fn.clsid = hrclasses_master.classesunkid "

    '            End Select

    '            objDataOperation.ClearParameters()
    '            StrQ = "SELECT @Value = ISNULL(STUFF((SELECT DISTINCT ',' + CAST(allocationunkid AS NVARCHAR(MAX)) FROM hrmsConfiguration..cfuseraccess_privilege_tran WHERE useraccessprivilegeunkid IN (SELECT useraccessprivilegeunkid FROM hrmsConfiguration..cfuseraccess_privilege_master WHERE userunkid = " & intUserId & " AND referenceunkid = " & strvalues(index) & ") FOR XML PATH('')),1,1,''),'') "
    '            Dim strvalue = ""
    '            objDataOperation.AddParameter("@Value", SqlDbType.NVarChar, 4000, strvalue, ParameterDirection.InputOutput)

    '            objDataOperation.ExecNonQuery(StrQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            End If

    '            strvalue = objDataOperation.GetParameterValue("@Value")

    '            If strvalue.Trim.Length > 0 Then
    '                Select Case strvalues(index)
    '                    Case enAllocation.BRANCH
    '                        strFinalString &= " AND Fn.branchid IN (" & strvalue & ") "

    '                    Case enAllocation.DEPARTMENT_GROUP
    '                        strFinalString &= " AND Fn.deptgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.DEPARTMENT
    '                        strFinalString &= " AND Fn.deptid IN (" & strvalue & ") "

    '                    Case enAllocation.SECTION_GROUP
    '                        strFinalString &= " AND Fn.secgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.SECTION
    '                        strFinalString &= " AND Fn.secid IN (" & strvalue & ") "

    '                    Case enAllocation.UNIT_GROUP
    '                        strFinalString &= " AND Fn.unitgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.UNIT
    '                        strFinalString &= " AND Fn.unitid IN (" & strvalue & ") "

    '                    Case enAllocation.TEAM
    '                        strFinalString &= " AND Fn.teamid IN (" & strvalue & ") "

    '                    Case enAllocation.JOB_GROUP
    '                        strFinalString &= " AND Fn.jgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.JOBS
    '                        strFinalString &= " AND Fn.jobid IN (" & strvalue & ") "

    '                    Case enAllocation.CLASS_GROUP
    '                        strFinalString &= " AND Fn.clsgrpid IN (" & strvalue & ") "

    '                    Case enAllocation.CLASSES
    '                        strFinalString &= " AND Fn.clsid IN (" & strvalue & ") "

    '                End Select
    '            End If
    '        Next
    '        If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
    '        If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetUACFilters; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeApprovalData(ByVal strDatabaseName As String _
                                          , ByVal intCompanyId As Integer _
                                          , ByVal intYearId As Integer _
                                          , ByVal strUserAccessMode As String _
                                          , ByVal intPrivilegeId As Integer _
                                          , ByVal intUserId As Integer _
                                          , ByRef dtNotification As DataTable _
                                          , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                                          , Optional ByVal strEmpIds As String = "") As DataTable
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim oData As DataTable
        Dim objDataOperation As New clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try

            Dim strFilter As String = String.Empty
            Dim strJoin As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strAccessJoin As String = String.Empty
            Dim strOuterJoin As String = String.Empty
            Dim strFinalString As String = ""

            StrQ = "IF OBJECT_ID('tempdb..#USR') IS NOT NULL " & _
                   "DROP TABLE #USR "
            StrQ &= "SELECT " & _
                    "* " & _
                    "INTO #USR " & _
                    "FROM " & _
                    "( "

            If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
            Dim strvalues() As String = strUserAccessMode.Split(CChar(","))
            For index As Integer = 0 To strvalues.Length - 1
                Dim xStrJoinColName As String = ""
                Dim xIntAllocId As Integer = 0
                Select Case CInt(strvalues(index))
                    Case enAllocation.BRANCH
                        xStrJoinColName = "stationunkid"
                        xIntAllocId = CInt(enAllocation.BRANCH)
                    Case enAllocation.DEPARTMENT_GROUP
                        xStrJoinColName = "deptgroupunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT_GROUP)
                    Case enAllocation.DEPARTMENT
                        xStrJoinColName = "departmentunkid"
                        xIntAllocId = CInt(enAllocation.DEPARTMENT)
                    Case enAllocation.SECTION_GROUP
                        xStrJoinColName = "sectiongroupunkid"
                        xIntAllocId = CInt(enAllocation.SECTION_GROUP)
                    Case enAllocation.SECTION
                        xStrJoinColName = "sectionunkid"
                        xIntAllocId = CInt(enAllocation.SECTION)
                    Case enAllocation.UNIT_GROUP
                        xStrJoinColName = "unitgroupunkid"
                        xIntAllocId = CInt(enAllocation.UNIT_GROUP)
                    Case enAllocation.UNIT
                        xStrJoinColName = "unitunkid"
                        xIntAllocId = CInt(enAllocation.UNIT)
                    Case enAllocation.TEAM
                        xStrJoinColName = "teamunkid"
                        xIntAllocId = CInt(enAllocation.TEAM)
                    Case enAllocation.JOB_GROUP
                        xStrJoinColName = "jobgroupunkid"
                        xIntAllocId = CInt(enAllocation.JOB_GROUP)
                    Case enAllocation.JOBS
                        xStrJoinColName = "jobunkid"
                        xIntAllocId = CInt(enAllocation.JOBS)
                    Case enAllocation.CLASS_GROUP
                        xStrJoinColName = "classgroupunkid"
                        xIntAllocId = CInt(enAllocation.CLASS_GROUP)
                    Case enAllocation.CLASSES
                        xStrJoinColName = "classunkid"
                        xIntAllocId = CInt(enAllocation.CLASSES)
                End Select
                StrQ &= "SELECT " & _
                        "    B" & index.ToString() & ".userunkid " & _
                        "   ,A.employeeunkid " & _
                        "FROM " & _
                        "( " & _
                        "   SELECT " & _
                        "        AEM.employeeunkid " & _
                        "       ,ISNULL(AEM.departmentunkid, 0) AS departmentunkid " & _
                        "       ,ISNULL(AEM.jobunkid, 0) AS jobunkid " & _
                        "       ,ISNULL(AEM.classgroupunkid, 0) AS classgroupunkid " & _
                        "       ,ISNULL(AEM.classunkid, 0) AS classunkid " & _
                        "       ,ISNULL(AEM.stationunkid, 0) AS stationunkid " & _
                        "       ,ISNULL(AEM.deptgroupunkid, 0) AS deptgroupunkid " & _
                        "       ,ISNULL(AEM.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                        "       ,ISNULL(AEM.sectionunkid, 0) AS sectionunkid " & _
                        "       ,ISNULL(AEM.unitgroupunkid, 0) AS unitgroupunkid " & _
                        "       ,ISNULL(AEM.unitunkid, 0) AS unitunkid " & _
                        "       ,ISNULL(AEM.teamunkid, 0) AS teamunkid " & _
                        "       ,ISNULL(AEM.jobgroupunkid, 0) AS jobgroupunkid " & _
                        "   FROM " & strDatabaseName & "..hremployee_master AS AEM " & _
                        "   JOIN " & strDatabaseName & "..hrempclearanceapproval_tran AS TAT ON TAT.employeeunkid = AEM.employeeunkid " & _
                        "   WHERE TAT.isvoid = 0 "
                StrQ &= ") AS A " & _
                        "JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        UPM.userunkid " & _
                        "       ,UPT.allocationunkid " & _
                        "   FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
                        "   JOIN hrmsConfiguration..cfuseraccess_privilege_tran UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
                        "   WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid IN (" & xIntAllocId & ") " & _
                        ") AS B" & index.ToString() & " ON A." & xStrJoinColName & " = B" & index.ToString() & ".allocationunkid "

                If index < strvalues.Length - 1 Then
                    StrQ &= " UNION "
                End If

            Next

            StrQ &= ") AS Fl "

            StrQ &= "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
                   "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
                   "SELECT " & _
                    "     hremployee_master.employeeunkid " & _
                    "    ,hremployee_master.departmentunkid " & _
                    "    ,hremployee_master.jobunkid " & _
                    "    ,hremployee_master.classgroupunkid " & _
                    "    ,hremployee_master.classunkid	" & _
                    "    ,hremployee_master.stationunkid	" & _
                    "    ,hremployee_master.deptgroupunkid	" & _
                    "    ,hremployee_master.sectiongroupunkid	" & _
                    "    ,hremployee_master.sectionunkid	" & _
                    "    ,hremployee_master.unitgroupunkid	" & _
                    "    ,hremployee_master.unitunkid	" & _
                    "    ,hremployee_master.teamunkid	" & _
                    "    ,hremployee_master.jobgroupunkid	" & _
                   "FROM " & strDatabaseName & "..hremployee_master " & _
                   "JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "        EAT.employeeunkid AS iEmpId " & _
                   "       ,EAT.statusunkid " & _
                   "       ,ROW_NUMBER()OVER(PARTITION BY EAT.employeeunkid ORDER BY EAT.transactiondate DESC) as xrno " & _
                   "    FROM " & strDatabaseName & "..hrempclearanceapproval_tran AS EAT " & _
                   "    JOIN " & strDatabaseName & "..hremployee_master ON EAT.employeeunkid = hremployee_master.employeeunkid AND hremployee_master.isclear = 0 " & _
                   "    WHERE EAT.isvoid = 0 AND EAT.statusunkid = " & clsEmployee_Master.EmpApprovalStatus.SubmitForApproval & _
                   "   AND ISNULL(EAT.mappingunkid, 0) <= 0 AND ISNULL(EAT.isfinal, 0) = 0 " & _
                   " ) AS A ON A.iEmpId = hremployee_master.employeeunkid " & _
                   " SELECT DISTINCT " & _
                   "     ISNULL(Fn.userunkid,0) AS userunkid " & _
                   "    ,CAST(0 AS BIT) AS icheck " & _
                   "    ,CASE WHEN Fn.uname = '' THEN Fn.username ELSE  Fn.uname END AS username " & _
                   "    ,ISNULL(Fn.email,'') AS email " & _
                   "    ,hremployee_master.employeecode + ' - ' + hremployee_master.firstname +' '+hremployee_master.surname AS Employee " & _
                   "    ,hremployee_master.employeecode " & _
                   "    ,hremployee_master.firstname + ' ' + hremployee_master.surname AS EmpName " & _
                   "    ,ISNULL(EDP.name,'') AS Department " & _
                   "    ,ISNULL(EJB.job_name,'') AS Job " & _
                   "    ,ISNULL(ECG.name,'') AS ClassGroup " & _
                   "    ,ISNULL(ECM.name,'') AS Class " & _
                   "    ,ISNULL(B.iStatus,@Pending) AS [Status] " & _
                   "    ,SUM(1) OVER (PARTITION BY hremployee_master.employeeunkid) AS stotal " & _
                   "    ,hremployee_master.employeeunkid " & _
                   "    ,ISNULL(B.iStatusId,1) AS iStatusId " & _
                   "    ,ISNULL(B.isfinal,0) AS isfinal " & _
                   "    ,ISNULL(Fn.mappingid,0) AS mappingid " & _
                   "    ,CASE WHEN B.iStatusId IS NULL THEN 0 ELSE 1 END AS iRead " & _
                   "    ,B.reminder_date   " & _
                   " FROM @emp " & _
                   " JOIN #USR ON [@emp].empid = #USR.employeeunkid " & _
                   " JOIN " & strDatabaseName & "..hremployee_master ON empid = hremployee_master.employeeunkid " & _
                   " LEFT JOIN " & strDatabaseName & "..hrdepartment_master AS EDP ON EDP.departmentunkid = hremployee_master.departmentunkid " & _
                   " LEFT JOIN " & strDatabaseName & "..hrjob_master AS EJB ON EJB.jobunkid = hremployee_master.jobunkid " & _
                   " LEFT JOIN " & strDatabaseName & "..hrclassgroup_master AS ECG ON ECG.classgroupunkid = hremployee_master.classgroupunkid " & _
                   " LEFT JOIN " & strDatabaseName & "..hrclasses_master AS ECM ON ECM.classesunkid = hremployee_master.classunkid " & _
                   " JOIN " & _
                   " ( " & _
                   "    SELECT DISTINCT " & _
                   "         cfuser_master.userunkid " & _
                   "        ,cfuser_master.username " & _
                   "        ,cfuser_master.firstname + ' ' + cfuser_master.lastname AS uname " & _
                   "        ,cfuser_master.email " & _
                   strSelect & " " & _
                   "        ,cfuser_master.userunkid AS mappingid " & _
                   "    FROM hrmsConfiguration..cfuser_master " & _
                   "    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
                   "    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                   strAccessJoin & " " & _
                   "    WHERE cfuser_master.isactive = 1  AND yearunkid = @Y AND privilegeunkid = @P  " & _
                   " ) AS Fn ON #USR.userunkid = Fn.userunkid  " & _
                   strOuterJoin & " " & _
                   " LEFT JOIN " & _
                   " ( " & _
                   "    SELECT " & _
                   "         iEmpId,iStatusId,iStatus,iRno,mappingunkid,mapuserunkid,deptid,jobid,clsgrpid,clsid " & _
                   "        ,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid,isfinal,isnotified,reminder_date " & _
                   "    FROM " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             EAT.employeeunkid AS iEmpId " & _
                   "            ,EAT.statusunkid AS iStatusId " & _
                   "            ,CASE EAT.statusunkid WHEN 1 THEN @Pending " & _
                   "                                  WHEN 2 THEN @Approved +' : [ '+ USR.username + ' ]' " & _
                   "                                  WHEN 3 THEN @Reject +' : [ '+ USR.username + ' ]' " & _
                   "             END AS iStatus " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY EAT.employeeunkid ORDER BY EAT.transactiondate DESC) AS iRno " & _
                   "            ,ISNULL(EAT.mappingunkid,0) AS mappingunkid " & _
                   "            ,ISNULL(USR.userunkid,0) AS mapuserunkid " & _
                   "            ,EPM.departmentunkid AS deptid " & _
                   "            ,EPM.jobunkid AS jobid " & _
                   "            ,EPM.classgroupunkid AS clsgrpid " & _
                   "            ,EPM.classunkid	AS clsid " & _
                   "            ,EPM.stationunkid AS branchid " & _
                   "            ,EPM.deptgroupunkid	AS deptgrpid " & _
                   "            ,EPM.sectiongroupunkid AS secgrpid " & _
                   "            ,EPM.sectionunkid AS secid " & _
                   "            ,EPM.unitgroupunkid	AS unitgrpid " & _
                   "            ,EPM.unitunkid AS unitid " & _
                   "            ,EPM.teamunkid AS teamid " & _
                   "            ,EPM.jobgroupunkid AS jgrpid " & _
                   "            ,EAT.isfinal AS isfinal " & _
                   "            ,EAT.isnotified AS isnotified " & _
                   "            ,EAT.reminder_date " & _
                   "        FROM hrempclearanceapproval_tran AS EAT " & _
                   "            JOIN @emp ON [@emp].empid = EAT.employeeunkid " & _
                   "            JOIN hremployee_master AS EPM ON EAT.employeeunkid = EPM.employeeunkid AND EPM.isclear = 0 " & _
                   "            LEFT JOIN hrmsConfiguration..cfuser_master AS USR ON USR.userunkid = EAT.mappingunkid  " & _
                   "        WHERE EAT.isvoid = 0 AND ISNULL(EAT.isvoid, 0) = 0 " & _
                   "    ) AS A WHERE A.iRno = 1 " & _
                   " ) AS B ON B.iEmpId = [@emp].empid " & _
                   " WHERE 1 = 1 " & strFinalString & " "

            StrQ &= " ORDER BY employeeunkid  "

            objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Approved"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Rejected"))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            oData = dsList.Tables(0).Copy()
            dtNotification = dsList.Tables(0).Copy()

            If strEmpIds.Trim.Length <= 0 Then
                oData = New DataView(dsList.Tables(0), "userunkid = " & intUserId & " AND iStatusId = " & clsEmployee_Master.EmpApprovalStatus.SubmitForApproval, "Employee", DataViewRowState.CurrentRows).ToTable()
            Else
                oData = New DataView(dsList.Tables(0), "employeeunkid IN (" & strEmpIds.Trim & ") AND iStatusId = " & clsEmployee_Master.EmpApprovalStatus.SubmitForApproval, "Employee", DataViewRowState.CurrentRows).ToTable()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovalData ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return oData
    End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    '''' 
    'Public Function GetNextEmployeeApprovers(ByVal strDatabaseName As String _
    '                                        , ByVal intCompanyId As Integer _
    '                                        , ByVal intYearId As Integer _
    '                                        , ByVal strEmpids As String _
    '                                        , ByVal strUserAccessMode As String _
    '                                        , ByVal intPrivilegeId As Integer _
    '                                        , Optional ByVal objDataOpr As clsDataOperation = Nothing _
    '                                        , Optional ByVal mblnIsSubmitForApproaval As Boolean = False _
    '                                        , Optional ByVal intCurrentPriorityId As Integer = -1 _
    '                                        ) As DataTable
    '    'S.SANDEEP [12-SEP-2018] -- START
    '    'ISSUE/ENHANCEMENT : {#2517}
    '    'intCurrentPriorityId = 0, intCurrentPriorityId = -1
    '    'S.SANDEEP [12-SEP-2018] -- END

    '    Dim StrQ As String = String.Empty
    '    Dim dsList As New DataSet
    '    Dim objDataOperation As New clsDataOperation
    '    If objDataOpr IsNot Nothing Then
    '        objDataOperation = objDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '    End If
    '    objDataOperation.ClearParameters()
    '    Try
    '        Dim strFilter As String = String.Empty
    '        Dim strJoin As String = String.Empty
    '        Dim strSelect As String = String.Empty
    '        Dim strAccessJoin As String = String.Empty
    '        Dim strOuterJoin As String = String.Empty
    '        Dim strFilterString As String = String.Empty
    '        'Hemant (22 Dec 2023) -- Start
    '        'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '        Dim strOuterSelect As String = String.Empty
    '        'Hemant (22 Dec 2023) -- End


    '        Call GetUACFilters(strDatabaseName, strUserAccessMode, strFilter, strJoin, strSelect, strAccessJoin, strOuterJoin, strFilterString, 0, strOuterSelect, objDataOperation)
    '        'Hemant (22 Dec 2023) -- [strOuterSelect]

    '        'If strUserAccessMode.Trim.Length <= 0 Then strUserAccessMode = CInt(enAllocation.DEPARTMENT).ToString()
    '        'Dim strvalues() As String = strUserAccessMode.Split(",")
    '        'Dim strFilter As String = String.Empty
    '        'Dim strJoin As String = String.Empty
    '        'Dim strSelect As String = String.Empty
    '        'Dim strAccessJoin As String = String.Empty
    '        'Dim strOuterJoin As String = String.Empty

    '        'For index As Integer = 0 To strvalues.Length - 1
    '        '    Select Case strvalues(index)
    '        '        Case enAllocation.BRANCH
    '        '            strSelect &= ",ISNULL(BR.branchid,0) AS branchid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as branchid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.BRANCH) & "' " & _
    '        '                             "    ) AS BR ON BR.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(BR.branchid,0) > 0 "
    '        '            strJoin &= "AND Fn.branchid = [@emp].branchid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrstation_master ON Fn.branchid = hrstation_master.stationunkid "

    '        '        Case enAllocation.DEPARTMENT_GROUP
    '        '            strSelect &= ",ISNULL(DG.deptgrpid,0) AS deptgrpid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as deptgrpid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT_GROUP) & "' " & _
    '        '                             "    ) AS DG ON DG.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(DG.deptgrpid,0) > 0 "
    '        '            strJoin &= "AND Fn.deptgrpid = [@emp].deptgrpid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master ON Fn.deptgrpid = hrdepartment_group_master.deptgroupunkid "

    '        '        Case enAllocation.DEPARTMENT
    '        '            strSelect &= ",ISNULL(DP.deptid,0) AS deptid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as deptid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.DEPARTMENT) & "' " & _
    '        '                             "    ) AS DP ON DP.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(DP.deptid,0) > 0 "
    '        '            strJoin &= "AND Fn.deptid = [@emp].deptid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrdepartment_master ON Fn.deptid = hrdepartment_master.departmentunkid "

    '        '        Case enAllocation.SECTION_GROUP
    '        '            strSelect &= ",ISNULL(SG.secgrpid,0) AS secgrpid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as secgrpid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION_GROUP) & "' " & _
    '        '                             "    ) AS SG ON SG.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(SG.secgrpid,0) > 0 "
    '        '            strJoin &= "AND Fn.secgrpid = [@emp].secgrpid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master ON Fn.secgrpid = hrsectiongroup_master.sectiongroupunkid "

    '        '        Case enAllocation.SECTION
    '        '            strSelect &= ",ISNULL(SC.secid,0) AS secid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as secid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.SECTION) & "' " & _
    '        '                             "    ) AS SC ON SC.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(SC.secid,0) > 0 "
    '        '            strJoin &= "AND Fn.secid = [@emp].secid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrsection_master ON Fn.secid = hrsection_master.sectionunkid "

    '        '        Case enAllocation.UNIT_GROUP
    '        '            strSelect &= ",ISNULL(UG.unitgrpid,0) AS unitgrpid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as unitgrpid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT_GROUP) & "' " & _
    '        '                             "    ) AS UG ON UG.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(UG.unitgrpid,0) > 0 "
    '        '            strJoin &= "AND Fn.unitgrpid = [@emp].unitgrpid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master ON Fn.unitgrpid = hrunitgroup_master.unitgroupunkid "

    '        '        Case enAllocation.UNIT
    '        '            strSelect &= ",ISNULL(UT.unitid,0) AS unitid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as unitid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.UNIT) & "' " & _
    '        '                             "    ) AS UT ON UT.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(UT.unitid,0) > 0 "
    '        '            strJoin &= "AND Fn.unitid = [@emp].unitid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrunit_master ON Fn.unitid = hrunit_master.unitunkid "

    '        '        Case enAllocation.TEAM
    '        '            strSelect &= ",ISNULL(TM.teamid,0) AS teamid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as teamid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.TEAM) & "' " & _
    '        '                             "    ) AS TM ON TM.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(TM.teamid,0) > 0 "
    '        '            strJoin &= "AND Fn.teamid = [@emp].teamid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrteam_master ON Fn.teamid = hrteam_master.teamunkid "

    '        '        Case enAllocation.JOB_GROUP
    '        '            strSelect &= " ,ISNULL(JG.jgrpid,0) AS jgrpid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as jgrpid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOB_GROUP) & "' " & _
    '        '                             "    ) AS JG ON JG.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(JG.jgrpid,0) > 0 "
    '        '            strJoin &= "AND Fn.jgrpid = [@emp].jgrpid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjobgroup_master ON Fn.jgrpid = hrjobgroup_master.jobgroupunkid "

    '        '        Case enAllocation.JOBS
    '        '            strSelect &= ",ISNULL(JB.jobid,0) AS jobid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as jobid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.JOBS) & "' " & _
    '        '                             "    ) AS JB ON JB.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(JB.jobid,0) > 0 "
    '        '            strJoin &= "AND Fn.jobid = [@emp].jobid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrjob_master ON Fn.jobid = hrjob_master.jobunkid "

    '        '        Case enAllocation.CLASS_GROUP
    '        '            strSelect &= ",ISNULL(CG.clsgrpid,0) AS clsgrpid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as clsgrpid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASS_GROUP) & "' " & _
    '        '                             "    ) AS CG ON CG.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(CG.clsgrpid,0) > 0 "
    '        '            strJoin &= "AND Fn.clsgrpid = [@emp].clsgrpid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master ON Fn.clsgrpid = hrclassgroup_master.classgroupunkid "

    '        '        Case enAllocation.CLASSES
    '        '            strSelect &= ",ISNULL(CL.clsid,0) AS clsid "
    '        '            strAccessJoin &= "    LEFT JOIN " & _
    '        '                             "    ( " & _
    '        '                             "        SELECT " & _
    '        '                             "             UPM.userunkid " & _
    '        '                             "            ,allocationunkid as clsid " & _
    '        '                             "        FROM hrmsConfiguration..cfuseraccess_privilege_master AS UPM " & _
    '        '                             "            JOIN hrmsConfiguration..cfuseraccess_privilege_tran AS UPT ON UPM.useraccessprivilegeunkid = UPT.useraccessprivilegeunkid " & _
    '        '                             "        WHERE UPM.companyunkid = @C AND UPM.yearunkid = @Y AND UPM.referenceunkid = '" & CInt(enAllocation.CLASSES) & "' " & _
    '        '                             "    ) AS CL ON CL.userunkid = cfuser_master.userunkid "
    '        '            strFilter &= "AND ISNULL(CL.clsid,0) > 0 "
    '        '            strJoin &= "AND Fn.clsid = [@emp].clsid "
    '        '            strOuterJoin &= "LEFT JOIN " & strDatabaseName & "..hrclasses_master ON Fn.clsid = classesunkid "

    '        '    End Select
    '        'Next
    '        'If strFilter.Trim.Length > 0 Then strFilter = strFilter.Substring(3)
    '        'If strJoin.Trim.Length > 0 Then strJoin = strJoin.Substring(3)


    '        'S.SANDEEP |24-JUN-2023| -- START
    '        'ISSUE/ENHANCEMENT : Sprint 2023-13
    '        'StrQ &= "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
    '        '        "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
    '        '        "SELECT " & _
    '        '        "     employeeunkid " & _
    '        '        "    ,departmentunkid " & _
    '        '        "    ,jobunkid " & _
    '        '        "    ,classgroupunkid " & _
    '        '        "    ,classunkid	" & _
    '        '        "    ,stationunkid	" & _
    '        '        "    ,deptgroupunkid	" & _
    '        '        "    ,sectiongroupunkid	" & _
    '        '        "    ,sectionunkid	" & _
    '        '        "    ,unitgroupunkid	" & _
    '        '        "    ,unitunkid	" & _
    '        '        "    ,teamunkid	" & _
    '        '        "    ,jobgroupunkid	" & _
    '        '        "FROM " & strDatabaseName & "..hremployee_master " & _
    '        '        "WHERE employeeunkid IN (" & strEmpids & ") " & _
    '        '        "SELECT " & _
    '        '        "     Fn.userunkid " & _
    '        '        "    ,Fn.username " & _
    '        '        "    ,Fn.email " & _
    '        '        "    ,employeecode + ' - ' + firstname +' '+surname AS Employee " & _
    '        '        "    ,ISNULL(EDP.name,'') AS Department " & _
    '        '        "    ,ISNULL(EJB.job_name,'') AS Job " & _
    '        '        "    ,ISNULL(ECG.name,'') AS ClassGroup " & _
    '        '        "    ,ISNULL(ECM.name,'') AS Class " & _
    '        '        "    ,Fn.priority " & _
    '        '        "    ,employeeunkid " & _
    '        '        "    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY Fn.priority ASC) AS rno " & _
    '        '        "FROM @emp " & _
    '        '        "JOIN " & strDatabaseName & "..hremployee_master ON empid = hremployee_master.employeeunkid " & _
    '        '        "LEFT JOIN " & strDatabaseName & "..hrdepartment_master AS EDP ON EDP.departmentunkid = hremployee_master.departmentunkid " & _
    '        '        "LEFT JOIN " & strDatabaseName & "..hrjob_master AS EJB ON EJB.jobunkid = hremployee_master.jobunkid " & _
    '        '        "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master AS ECG ON ECG.classgroupunkid = hremployee_master.classgroupunkid " & _
    '        '        "LEFT JOIN " & strDatabaseName & "..hrclasses_master AS ECM ON ECM.classesunkid = hremployee_master.classunkid " & _
    '        '        "JOIN " & _
    '        '        "( " & _
    '        '        "    SELECT DISTINCT " & _
    '        '        "         cfuser_master.userunkid " & _
    '        '        "        ,cfuser_master.username " & _
    '        '        "        ,cfuser_master.email " & _
    '        '        strSelect & " " & _
    '        '        "        ,LM.priority " & _
    '        '        "    FROM hrmsConfiguration..cfuser_master " & _
    '        '        "    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
    '        '        "    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
    '        '        strAccessJoin & " " & _
    '        '        "    JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
    '        '        "    JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid " & _
    '        '        "    WHERE cfuser_master.isactive = 1 AND (" & strFilter & " ) AND EM.isactive = 1 AND EM.isvoid = 0 " & _
    '        '        "        AND EM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " AND LM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
    '        '        "        /* AND companyunkid = @C */ AND yearunkid = @Y AND privilegeunkid = @P  " & _
    '        '        ") AS Fn ON " & strJoin & " " & _
    '        '        strOuterJoin & " " & _
    '        '        "WHERE 1 = 1 "

    '        StrQ &= "DECLARE @emp AS table (empid int, deptid int, jobid int, clsgrpid int, clsid int, branchid int, deptgrpid int, secgrpid int, secid int, unitgrpid int, unitid int, teamid int, jgrpid int) " & _
    '                "INSERT INTO @emp(empid,deptid,jobid,clsgrpid,clsid,branchid,deptgrpid,secgrpid,secid,unitgrpid,unitid,teamid,jgrpid) " & _
    '                "SELECT " & _
    '                "     employeeunkid " & _
    '                "    ,departmentunkid " & _
    '                "    ,jobunkid " & _
    '                "    ,classgroupunkid " & _
    '                "    ,classunkid	" & _
    '                "    ,stationunkid	" & _
    '                "    ,deptgroupunkid	" & _
    '                "    ,sectiongroupunkid	" & _
    '                "    ,sectionunkid	" & _
    '                "    ,unitgroupunkid	" & _
    '                "    ,unitunkid	" & _
    '                "    ,teamunkid	" & _
    '                "    ,jobgroupunkid	" & _
    '                "FROM " & strDatabaseName & "..hremployee_master " & _
    '               "WHERE employeeunkid IN (" & strEmpids & ") "

    '        'Hemant (22 Dec 2023) -- Start
    '        'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '        StrQ &= " select * from ( "
    '        'Hemant (22 Dec 2023) -- End

    '        StrQ &= "SELECT " & _
    '                "     Fn.userunkid " & _
    '                "    ,Fn.username " & _
    '                "    ,Fn.email " & _
    '                "    ,employeecode + ' - ' + firstname +' '+surname AS Employee " & _
    '                "    ,employeecode AS [Employee Code] " & _
    '                "    ,firstname+' '+surname AS [Employee Name] " & _
    '                "    ,ISNULL(EBR.name,'') AS [" & Language.getMessage("clsMasterData", 430, "Branch") & "] " & _
    '                "    ,ISNULL(EDG.name,'') AS [" & Language.getMessage("clsMasterData", 429, "Department Group") & "] " & _
    '                "    ,ISNULL(EDP.name,'') AS [" & Language.getMessage("clsMasterData", 428, "Department") & "] " & _
    '                "    ,ISNULL(ESG.name,'') AS [" & Language.getMessage("clsMasterData", 427, "Section Group") & "] " & _
    '                "    ,ISNULL(ESC.name,'') AS [" & Language.getMessage("clsMasterData", 426, "Section") & "] " & _
    '                "    ,ISNULL(EUG.name,'') AS [" & Language.getMessage("clsMasterData", 425, "Unit Group") & "] " & _
    '                "    ,ISNULL(EUM.name,'') AS [" & Language.getMessage("clsMasterData", 424, "Unit") & "] " & _
    '                "    ,ISNULL(ETM.name,'') AS [" & Language.getMessage("clsMasterData", 423, "Team") & "] " & _
    '                "    ,ISNULL(ECG.name,'') AS [" & Language.getMessage("clsMasterData", 420, "Class Group") & "] " & _
    '                "    ,ISNULL(ECM.name,'') AS [" & Language.getMessage("clsMasterData", 419, "Classes") & "] " & _
    '                "    ,ISNULL(EJB.job_name,'') AS [" & Language.getMessage("clsMasterData", 421, "Jobs") & "] " & _
    '                "    ,ISNULL(REPLACE(CONVERT(NVARCHAR(11),appointeddate,106),' ','-'),'') AS [Appointment Date] " & _
    '                "    ,Fn.priority " & _
    '                "    ,employeeunkid " & _
    '               "    ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY Fn.priority ASC) AS rno "
    '        StrQ &= strOuterSelect
    '        StrQ &= "FROM @emp " & _
    '                "JOIN " & strDatabaseName & "..hremployee_master ON empid = hremployee_master.employeeunkid " & _
    '                "LEFT JOIN " & strDatabaseName & "..hrstation_master AS EBR ON EBR.stationunkid = hremployee_master.stationunkid " & _
    '                "LEFT JOIN " & strDatabaseName & "..hrdepartment_group_master AS EDG ON EDG.deptgroupunkid = hremployee_master.deptgroupunkid " & _
    '                "LEFT JOIN " & strDatabaseName & "..hrdepartment_master AS EDP ON EDP.departmentunkid = hremployee_master.departmentunkid " & _
    '                "LEFT JOIN " & strDatabaseName & "..hrsectiongroup_master AS ESG ON ESG.sectiongroupunkid = hremployee_master.sectiongroupunkid " & _
    '                "LEFT JOIN " & strDatabaseName & "..hrsection_master AS ESC ON ESC.sectionunkid = hremployee_master.sectionunkid " & _
    '                "LEFT JOIN " & strDatabaseName & "..hrunitgroup_master AS EUG ON EUG.unitgroupunkid = hremployee_master.unitgroupunkid " & _
    '                "LEFT JOIN " & strDatabaseName & "..hrunit_master AS EUM ON EUM.unitunkid = hremployee_master.unitunkid " & _
    '                "LEFT JOIN " & strDatabaseName & "..hrteam_master AS ETM ON ETM.teamunkid = hremployee_master.teamunkid " & _
    '                "LEFT JOIN " & strDatabaseName & "..hrclassgroup_master AS ECG ON ECG.classgroupunkid = hremployee_master.classgroupunkid " & _
    '                "LEFT JOIN " & strDatabaseName & "..hrclasses_master AS ECM ON ECM.classesunkid = hremployee_master.classunkid " & _
    '                "LEFT JOIN " & strDatabaseName & "..hrjob_master AS EJB ON EJB.jobunkid = hremployee_master.jobunkid " & _
    '                "JOIN " & _
    '                "( " & _
    '                "    SELECT DISTINCT " & _
    '                "         cfuser_master.userunkid " & _
    '                "        ,cfuser_master.username " & _
    '                "        ,cfuser_master.email " & _
    '                strSelect & " " & _
    '                "        ,LM.priority " & _
    '                "    FROM hrmsConfiguration..cfuser_master " & _
    '                "    JOIN hrmsConfiguration..cfcompanyaccess_privilege ON cfcompanyaccess_privilege.userunkid = cfuser_master.userunkid " & _
    '                "    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
    '                strAccessJoin & " " & _
    '                "    JOIN " & strDatabaseName & "..hremp_appusermapping EM ON EM.mapuserunkid = cfuser_master.userunkid " & _
    '              "    JOIN " & strDatabaseName & "..hrempapproverlevel_master AS LM ON EM.empapplevelunkid = LM.empapplevelunkid "
    '        'Hemant (22 Dec 2023) -- Start
    '        'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '        'StrQ &= "    WHERE cfuser_master.isactive = 1 AND (" & strFilter & " ) AND EM.isactive = 1 AND EM.isvoid = 0 "
    '        StrQ &= "    WHERE cfuser_master.isactive = 1  AND EM.isactive = 1 AND EM.isvoid = 0 "
    '        'Hemant (22 Dec 2023) -- End
    '        StrQ &= "        AND EM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " AND LM.approvertypeid = " & enEmpApproverType.APPR_EMPLOYEE & " " & _
    '                "        /* AND companyunkid = @C */ AND yearunkid = @Y AND privilegeunkid = @P  " & _
    '                ") AS Fn ON " & strJoin & " " & _
    '                strOuterJoin & " " & _
    '                "WHERE 1 = 1 "
    '        'S.SANDEEP |24-JUN-2023| -- END


    '        'S.SANDEEP [26-SEP-2018] -- START
    '        '/*AND companyunkid = @C -- REMOVED AS ADMIN USER DOES NOT HAVE COMPANY ID*/
    '        'S.SANDEEP [26-SEP-2018] -- END

    '        If mblnIsSubmitForApproaval = False Then
    '            StrQ &= " AND Fn.priority > " & intCurrentPriorityId
    '        End If

    '        'Hemant (22 Dec 2023) -- Start
    '        'ISSUE(NMB): Taking too longer while clicking on Submit for Approval on Employee Submit for Approval Screen
    '        StrQ &= " ) AS A WHERE 1 = 1 " & _
    '                " AND (" & strFilter & " ) "
    '        'Hemant (22 Dec 2023) -- End

    '        objDataOperation.AddParameter("@C", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyId)
    '        objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
    '        objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, intPrivilegeId)

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeApprovers; Module Name: " & mstrModuleName)
    '    Finally
    '        If objDataOpr Is Nothing Then objDataOperation = Nothing
    '    End Try
    '    Return dsList.Tables(0)
    'End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    '''' 
    'Public Function GetUserAccessLevel(ByVal intUserUnkId As Integer _
    '                                   , ByVal intComppanyUnkId As Integer _
    '                                   , ByVal intYearUnkId As Integer _
    '                                   , ByVal strReferenceUnkIds As String) As String
    '    Dim strAccessLevel As String = String.Empty
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    Dim strFilter As String = String.Empty
    '    Try
    '        objDataOperation = New clsDataOperation


    '        objDataOperation.AddParameter("@UserId", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkId)
    '        objDataOperation.AddParameter("@CompanyId", SqlDbType.Int, eZeeDataType.INT_SIZE, intComppanyUnkId)
    '        objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearUnkId)

    '        If strReferenceUnkIds = "" Then strReferenceUnkIds = ConfigParameter._Object._UserAccessModeSetting
    '        Dim arrID As String() = strReferenceUnkIds.Split(",")
    '        For i = 0 To arrID.Length - 1
    '            StrQ = "SELECT " & _
    '                              "STUFF((SELECT ',' + CAST(allocationunkid AS NVARCHAR(50)) " & _
    '                              "FROM hrmsConfiguration..cfuseraccess_privilege_tran " & _
    '                              "JOIN hrmsConfiguration..cfuseraccess_privilege_master ON hrmsConfiguration..cfuseraccess_privilege_tran.useraccessprivilegeunkid = hrmsConfiguration..cfuseraccess_privilege_master.useraccessprivilegeunkid " & _
    '                              "WHERE userunkid = @UserId AND companyunkid = @CompanyId AND yearunkid = @YearId AND referenceunkid = " & arrID(i).ToString & " " & _
    '                              "ORDER BY hrmsConfiguration..cfuseraccess_privilege_tran.allocationunkid FOR XML PATH('')),1,1,'') AS CSV "

    '            dsList = objDataOperation.ExecQuery(StrQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            If dsList.Tables("List").Rows.Count > 0 Then
    '                If dsList.Tables("List").Rows(0)("CSV").ToString.Trim.Length > 0 Then
    '                    strAccessLevel = dsList.Tables("List").Rows(0)("CSV").ToString
    '                Else
    '                    strAccessLevel = "0"
    '                End If
    '            Else
    '                strAccessLevel = "0"
    '            End If

    '            If CInt(arrID(i)) = enAllocation.BRANCH Then
    '                strFilter &= " AND  hremployee_master.stationunkid IN (" & strAccessLevel & ") "
    '            End If
    '            If CInt(arrID(i)) = enAllocation.DEPARTMENT_GROUP Then
    '                strFilter &= " AND  hremployee_master.deptgroupunkid IN (" & strAccessLevel & ") "
    '            End If
    '            If CInt(arrID(i)) = enAllocation.DEPARTMENT Then
    '                strFilter &= " AND  hremployee_master.departmentunkid IN (" & strAccessLevel & ") "
    '            End If
    '            If CInt(arrID(i)) = enAllocation.SECTION_GROUP Then
    '                strFilter &= " AND  hremployee_master.sectiongroupunkid IN (" & strAccessLevel & ") "
    '            End If
    '            If CInt(arrID(i)) = enAllocation.SECTION Then
    '                strFilter &= " AND  hremployee_master.sectionunkid IN (" & strAccessLevel & ") "
    '            End If
    '            If CInt(arrID(i)) = enAllocation.UNIT_GROUP Then
    '                strFilter &= " AND  hremployee_master.unitgroupunkid IN (" & strAccessLevel & ") "
    '            End If
    '            If CInt(arrID(i)) = enAllocation.UNIT Then
    '                strFilter &= " AND  hremployee_master.unitunkid IN (" & strAccessLevel & ") "
    '            End If
    '            If CInt(arrID(i)) = enAllocation.TEAM Then
    '                strFilter &= " AND  hremployee_master.teamunkid IN (" & strAccessLevel & ") "
    '            End If
    '            If CInt(arrID(i)) = enAllocation.JOB_GROUP Then
    '                strFilter &= " AND  hremployee_master.jobgroupunkid IN (" & strAccessLevel & ") "
    '            End If
    '            If CInt(arrID(i)) = enAllocation.JOBS Then
    '                strFilter &= " AND hremployee_master.jobunkid IN (" & strAccessLevel & ") "
    '            End If
    '            If CInt(arrID(i)) = enAllocation.CLASS_GROUP Then
    '                strFilter &= " AND hremployee_master.classgroupunkid IN (" & strAccessLevel & ") "
    '            End If
    '            If CInt(arrID(i)) = enAllocation.CLASSES Then
    '                strFilter &= " AND hremployee_master.classunkid IN (" & strAccessLevel & ") "
    '            End If
    '        Next

    '        If strFilter = "" Then
    '            strFilter = " hremployee_master.departmentunkid IN (0) ) "
    '        End If
    '        Return strFilter
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetUserAccessLevel", mstrModuleName)
    '        Return strFilter
    '    Finally
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    '''' 
    'Public Sub SendSubmitNotification(ByVal strDatabaseName As String, ByVal intCompanyId As Integer, ByVal intYearId As Integer, ByVal strEmpUnkids As String, ByVal strUserAccessMode As String, ByVal intPrivilgeId As Integer, ByVal strFormName As String, ByVal eMode As enLogin_Mode, ByVal intLoginUserId As Integer, ByVal strSenderAddress As String, ByVal ArutiSelfServiceURL As String, ByVal iNotifyUpdate As Dictionary(Of Integer, String))
    '    Dim dtTable As DataTable
    '    Try
    '        dtTable = GetNextEmployeeApprovers(strDatabaseName, intCompanyId, intYearId, strEmpUnkids, strUserAccessMode, intPrivilgeId, objDataOperation, True)
    '        Dim dr() As DataRow = dtTable.Select("rno = 1")
    '        If ArutiSelfServiceURL.Trim.Length <= 0 Then ArutiSelfServiceURL = ConfigParameter._Object._ArutiSelfServiceURL
    '        If dtTable IsNot Nothing Then
    '            Dim dtFilterTab As DataTable = dtTable.DefaultView.ToTable(True, "username", "email", "userunkid")
    '            For Each row In dtFilterTab.Rows
    '                If row("email").ToString().Trim() = "" Then Continue For

    '                Dim intUserId As Integer = row("userunkid")
    '                Dim iList As List(Of Integer) = dr.AsEnumerable().Where(Function(x) x.Field(Of Integer)("userunkid") = intUserId).Select(Function(y) y.Field(Of Integer)("employeeunkid")).Distinct().ToList()

    '                Dim intCnt As Integer = 0
    '                intCnt = dtTable.Select("username = '" & row("username").ToString & "' AND rno = 1").Length
    '                If intCnt > 0 Then
    '                    Dim StrMessage As New System.Text.StringBuilder
    '                    StrMessage.Append("<HTML><BODY>")
    '                    StrMessage.Append(vbCrLf)
    '                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

    '                    'Gajanan [27-Mar-2019] -- Start
    '                    'Enhancement - Change Email Language
    '                    'StrMessage.Append("Dear <b>" & row("username") & "</b></span></p>")
    '                    StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Dear") & " " & "<b>" & " " & getTitleCase(row("username").ToString()) & "</b></span></p>")
    '                    'Gajanan [27-Mar-2019] -- End

    '                    StrMessage.Append(vbCrLf)
    '                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

    '                    'Gajanan [27-Mar-2019] -- Start
    '                    'Enhancement - Change Email Language
    '                    'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; This is to inform you that total (" & intCnt.ToString & ") employee(s) have been submitted for approval, please go approve them. </span></p>")
    '                    StrMessage.Append(Language.getMessage(mstrModuleName, 11, "This is to inform you that total") & " (" & intCnt.ToString & ")" & Language.getMessage(mstrModuleName, 12, "employee(s) have been submitted for approval, please go approve them.") & "</span></p>")
    '                    'Gajanan [27-Mar-2019] -- End


    '                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                    'S.SANDEEP [21-NOV-2018] -- START -- UAT (NMB) NEED TO REDIRECT TO NEW PAGE WHEN READY FOR WEB
    '                    'StrMessage.Append(Language.getMessage(mstrModuleName, 1, "Please click the link below to approve/reject employee.") & "</span></p>")
    '                    'StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
    '                    'StrMessage.Append(ArutiSelfServiceURL & "/HR/wPgEmployeeProfile.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(intCompanyId & "|" & row("userunkid") & "|" & intYearId & "|" & "1")) & "</span></p>")
    '                    'S.SANDEEP [21-NOV-2018] -- END


    '                    'Gajanan [27-Mar-2019] -- Start
    '                    'Enhancement - Change Email Language
    '                    'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
    '                    StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
    '                    'Gajanan [27-Mar-2019] -- End

    '                    StrMessage.Append("</span></p>")
    '                    StrMessage.Append("</BODY></HTML>")

    '                    Dim objSendMail As New clsSendMail
    '                    objSendMail._ToEmail = row("email")
    '                    objSendMail._Subject = Language.getMessage(mstrModuleName, 3, "Notifications to Approve newly Hired Employee ")
    '                    objSendMail._Message = StrMessage.ToString()
    '                    objSendMail._Form_Name = strFormName
    '                    objSendMail._LogEmployeeUnkid = -1
    '                    objSendMail._OperationModeId = eMode
    '                    objSendMail._UserUnkid = intLoginUserId
    '                    objSendMail._SenderAddress = strSenderAddress
    '                    objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
    '                    Dim strSuccess As String = String.Empty
    '                    strSuccess = objSendMail.SendMail(intCompanyId)

    '                    If strSuccess.Trim.Length <= 0 Then
    '                        If iList.Count > 0 Then
    '                            For Each item In iList
    '                                If iNotifyUpdate.ContainsKey(item) = True Then
    '                                    Using objDo As New clsDataOperation
    '                                        objDo.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, item)
    '                                        objDo.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, iNotifyUpdate(item))
    '                                        objDo.ExecNonQuery("UPDATE hremployeeapproval_tran SET isnotified = 1 WHERE employeeunkid = @employeeunkid AND tranguid = @tranguid ")
    '                                    End Using
    '                                End If
    '                            Next
    '                        End If
    '                    End If
    '                End If
    '            Next
    '        End If
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: SendSubmitNotification; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Sub SendClearanceApproveRejectNotification(ByVal blnIsRejection As Boolean, _
                                                           ByVal strRejectionRemark As String, _
                                                           ByVal strDatabaseName As String, _
                                                           ByVal intCompanyId As Integer, _
                                                           ByVal intYearId As Integer, _
                                                           ByVal strEmpUnkids As String, _
                                                           ByVal strUserAccessMode As String, _
                                                           ByVal intPrivilgeId As Integer, _
                                                           ByVal strFormName As String, _
                                                           ByVal eMode As enLogin_Mode, _
                                                           ByVal intLoginUserId As Integer, _
                                                           ByVal strSenderAddress As String, _
                                                           ByVal mstrEmployeeCode As String, _
                                                           ByVal mstrEmployeeName As String)
        Try
            Dim mstrSubject As String = String.Empty
            Dim dtTable As DataTable = Nothing
            Dim dtNotification As DataTable = Nothing

            If blnIsRejection Then
                mstrSubject = Language.getMessage(mstrModuleName, 5, "Notification for Employee Clearance Rejection")
            Else
                mstrSubject = Language.getMessage(mstrModuleName, 4, "Notification for Employee Clearance Approval")
            End If

            dtTable = GetEmployeeApprovalData(strDatabaseName, intCompanyId, intYearId, strUserAccessMode, enUserPriviledge.AllowToApproveEmployeeClearance, intLoginUserId, dtNotification, Nothing)

            Dim dtFilterTab As DataTable = dtNotification.DefaultView.ToTable(True, "username", "email", "userunkid")

            For Each row In dtFilterTab.Rows
                Dim StrMessage As New System.Text.StringBuilder

                If row("email").ToString().Trim() = "" Then Continue For

                If blnIsRejection = False Then
                    StrMessage.Append("<HTML><BODY>")
                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 6, "Dear") & "<b>" & " " & getTitleCase(row("username")) & "</b></span></p>")
                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 9, "This is to notify you that clearance for") & " <b>" & "(" & mstrEmployeeCode & " - " & mstrEmployeeName & ")" & " </b>" & Language.getMessage(mstrModuleName, 7, "has been submitted for review and approval. Kindly login into the system for actioning.") & "</span></p>")
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Regards.") & "</span></p>")
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                    StrMessage.Append("</span></p>")
                    StrMessage.Append("</BODY></HTML>")
                Else
                    StrMessage.Append("<HTML><BODY>")
                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append("Dear <b>" & getTitleCase(row("username")) & "</b></span></p>")
                    StrMessage.Append(vbCrLf)
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 9, "This is to notify you that clearance for") & " <b>" & "(" & mstrEmployeeCode & " - " & mstrEmployeeName & ")" & " </b>" & " " & Language.getMessage(mstrModuleName, 8, "have been rejected by approver with below remark.") & "</span></p>")
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append("<b>" & strRejectionRemark & "</b></span></p>")
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append(Language.getMessage(mstrModuleName, 10, "Regards.") & "</span></p>")
                    StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
                    StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
                    StrMessage.Append("</span></p>")
                    StrMessage.Append("</BODY></HTML>")
                End If

                Dim objSendMail As New clsSendMail
                objSendMail._ToEmail = row("email").ToString().Trim()
                objSendMail._Subject = mstrSubject
                objSendMail._Message = StrMessage.ToString()
                objSendMail._Form_Name = strFormName
                objSendMail._LogEmployeeUnkid = -1
                objSendMail._OperationModeId = eMode
                objSendMail._UserUnkid = intLoginUserId
                objSendMail._SenderAddress = strSenderAddress
                objSendMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
                objSendMail.SendMail(intCompanyId)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendClearanceApproveRejectNotification; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Private Function GenerateTableString(ByVal oData As DataTable) As String
        Dim StrMessage As New System.Text.StringBuilder
        Try
            Dim odt As New DataTable
            odt = oData.DefaultView.ToTable(True, "Employee Code", _
                                            "Employee Name", _
                                            Language.getMessage("clsMasterData", 430, "Branch"), _
                                            Language.getMessage("clsMasterData", 429, "Department Group"), _
                                            Language.getMessage("clsMasterData", 428, "Department"), _
                                            Language.getMessage("clsMasterData", 427, "Section Group"), _
                                            Language.getMessage("clsMasterData", 426, "Section"), _
                                            Language.getMessage("clsMasterData", 425, "Unit Group"), _
                                            Language.getMessage("clsMasterData", 424, "Unit"), _
                                            Language.getMessage("clsMasterData", 423, "Team"), _
                                            Language.getMessage("clsMasterData", 420, "Class Group"), _
                                            Language.getMessage("clsMasterData", 419, "Classes"), _
                                            Language.getMessage("clsMasterData", 421, "Jobs"), _
                                            "Appointment Date")
            If odt IsNot Nothing AndAlso odt.Rows.Count > 0 Then
                StrMessage.AppendLine("<TABLE border = '1'>")
                StrMessage.AppendLine("<TR bgcolor= 'SteelBlue'>")
                For Each col As DataColumn In odt.Columns
                    StrMessage.AppendLine("<TD align = 'LEFT'><b><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; '>" & col.ColumnName & "</span></b></TD>")
                Next
                StrMessage.AppendLine("</TR>")
                For Each row As DataRow In odt.Rows
                    StrMessage.AppendLine("<TR>" & vbCrLf)
                    For Each col As DataColumn In odt.Columns
                        StrMessage.AppendLine("<TD align = 'LEFT'><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; '>" & row(col.ColumnName) & "</span></TD>")
                    Next
                    StrMessage.AppendLine("</TR>" & vbCrLf)
                Next
                StrMessage.AppendLine("</TABLE>")
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GenerateTableString; Module Name: " & mstrModuleName)
        End Try
        Return StrMessage.ToString()
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    'Public Function UpdateClearanceEmployeeApprovalReminderDate(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal mstrTranGuid As String, ByVal dtReminderDate As Date) As Boolean
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    If xDataOpr IsNot Nothing Then
    '        objDataOperation = xDataOpr
    '    Else
    '        objDataOperation = New clsDataOperation
    '        objDataOperation.BindTransaction()
    '    End If
    '    Try
    '        strQ = " UPDATE hrempclearanceapproval_tran SET reminder_date = @reminder_date WHERE employeeunkid = @employeeunkid AND tranguid = @tranguid "
    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
    '        objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranGuid)
    '        objDataOperation.AddParameter("@reminder_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtReminderDate)
    '        Call xDataOpr.ExecNonQuery(strQ)

    '        If xDataOpr.ErrorMessage <> "" Then
    '            exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If xDataOpr Is Nothing Then
    '            objDataOperation.ReleaseTransaction(True)
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: UpdateClearanceEmployeeApprovalReminderDate; Module Name: " & mstrModuleName)
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeClearanceList(ByVal strDatabaseName As String, ByVal intCompanyId As Integer, ByVal intYearId As Integer, ByVal xUserModeSetting As String, ByVal intUserId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            Dim StrAccess As String = ""

            strQ = "SELECT ' ' AS employeecode, ' ' + @Select AS employeename, 0 AS employeeunkid,0 AS isapproved, ' ' + @Select AS EmpCodeName UNION ALL " & _
                      "SELECT " & _
                      "   hremployee_master.employeecode AS employeecode " & _
                      ",  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS employeename " & _
                      ",  hremployee_master.employeeunkid As employeeunkid " & _
                      ",  ISNULL(hremployee_master.isapproved,0) AS isapproved " & _
                      ",  ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS EmpCodeName " & _
                      " FROM hrempclearanceapproval_tran " & _
                      " JOIN hremployee_master ON hremployee_master.employeeunkid = hrempclearanceapproval_tran.employeeunkid AND hremployee_master.isclear = 0 " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "    SELECT DISTINCT " & _
                      "         cfuser_master.userunkid " & _
                      "        ,cfuser_master.username " & _
                      "        ,cfuser_master.email " & _
                      "    FROM hrmsConfiguration..cfuser_master " & _
                      "    JOIN hrmsConfiguration..cfuser_privilege ON cfuser_master.userunkid = cfuser_privilege.userunkid " & _
                      "    WHERE cfuser_master.isactive = 1 AND privilegeunkid = @P  " & _
                      " ) AS Fn ON Fn.userunkid = hrempclearanceapproval_tran.mappingunkid " & _
                      " WHERE ISNULL(hrempclearanceapproval_tran.isvoid,0) = 0 AND ISNULL(hrempclearanceapproval_tran.mappingunkid,0) <= 0 AND ISNULL(hrempclearanceapproval_tran.isfinal,0) = 0 " & _
                      " AND Fn.userunkid = @userunkid And hrempclearanceapproval_tran.statusunkid = " & clsEmployee_Master.EmpApprovalStatus.SubmitForApproval


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Select"))
            objDataOperation.AddParameter("@P", SqlDbType.Int, eZeeDataType.INT_SIZE, enUserPriviledge.AllowToApproveEmployeeClearance)
            objDataOperation.AddParameter("@Y", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dfView As DataView = dsList.Tables(0).DefaultView
            dfView.Sort = "employeename"
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dfView.ToTable)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeClearanceList ; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 419, "Classes")
            Language.setMessage("clsMasterData", 420, "Class Group")
            Language.setMessage("clsMasterData", 421, "Jobs")
            Language.setMessage("clsMasterData", 423, "Team")
            Language.setMessage("clsMasterData", 424, "Unit")
            Language.setMessage("clsMasterData", 425, "Unit Group")
            Language.setMessage("clsMasterData", 426, "Section")
            Language.setMessage("clsMasterData", 427, "Section Group")
            Language.setMessage("clsMasterData", 428, "Department")
            Language.setMessage("clsMasterData", 429, "Department Group")
            Language.setMessage("clsMasterData", 430, "Branch")
            Language.setMessage(mstrModuleName, 1, "Pending")
            Language.setMessage(mstrModuleName, 2, "Approved")
            Language.setMessage(mstrModuleName, 3, "Rejected")
            Language.setMessage(mstrModuleName, 4, "Notification for Employee Clearance Approval")
            Language.setMessage(mstrModuleName, 5, "Notification for Employee Clearance Rejection")
            Language.setMessage(mstrModuleName, 6, "Dear")
            Language.setMessage(mstrModuleName, 7, "has been submitted for review and approval. Kindly login into the system for actioning.")
            Language.setMessage(mstrModuleName, 8, "have been rejected by approver with remark")
            Language.setMessage(mstrModuleName, 9, "This is to notify you that clearance for")
            Language.setMessage(mstrModuleName, 10, "Regards.")
            Language.setMessage(mstrModuleName, 11, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class