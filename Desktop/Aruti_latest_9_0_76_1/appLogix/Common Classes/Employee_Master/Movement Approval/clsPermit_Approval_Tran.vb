﻿'************************************************************************************************************************************
'Class Name : clsPermit_Approval_Tran.vb
'Purpose    :
'Date       :31-Jul-2018
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsPermit_Approval_Tran
    Private Const mstrModuleName = "clsPermit_Approval_Tran"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mstrTranguid As String = String.Empty
    Private mdtTransactiondate As Date
    Private mintMappingunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintStatusunkid As Integer
    Private mintWorkpermittranunkid As Integer
    Private mdtEffectivedate As Date
    Private mintEmployeeunkid As Integer
    Private mstrWork_Permit_No As String = String.Empty
    Private mintWorkcountryunkid As Integer
    Private mstrIssue_Place As String = String.Empty
    Private mdtIssue_Date As Date
    Private mdtExpiry_Date As Date
    Private mintChangereasonunkid As Integer
    Private mintRehiretranunkid As Integer
    Private mblnIsresidentpermit As Boolean
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintVoiduserunkid As Integer
    Private mintAudittype As Integer
    Private mintAudituserunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrHostname As String = String.Empty
    Private mstrForm_Name As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsProcessed As Boolean = False
    'S.SANDEEP |17-JAN-2019| -- START
    Private mintOperationTypeId As Integer = 0
    'S.SANDEEP |17-JAN-2019| -- END


    'Pinkal (20-Dec-2024) -- Start
    'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
    Private mdtReminderDate As DateTime = Nothing
    'Pinkal (20-Dec-2024) -- End

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranguid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Tranguid() As String
        Get
            Return mstrTranguid
        End Get
        Set(ByVal value As String)
            mstrTranguid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transactiondate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Transactiondate() As Date
        Get
            Return mdtTransactiondate
        End Get
        Set(ByVal value As Date)
            mdtTransactiondate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set workpermittranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Workpermittranunkid() As Integer
        Get
            Return mintWorkpermittranunkid
        End Get
        Set(ByVal value As Integer)
            mintWorkpermittranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effectivedate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Effectivedate() As Date
        Get
            Return mdtEffectivedate
        End Get
        Set(ByVal value As Date)
            mdtEffectivedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set work_permit_no
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Work_Permit_No() As String
        Get
            Return mstrWork_Permit_No
        End Get
        Set(ByVal value As String)
            mstrWork_Permit_No = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set workcountryunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Workcountryunkid() As Integer
        Get
            Return mintWorkcountryunkid
        End Get
        Set(ByVal value As Integer)
            mintWorkcountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issue_place
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Issue_Place() As String
        Get
            Return mstrIssue_Place
        End Get
        Set(ByVal value As String)
            mstrIssue_Place = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issue_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Issue_Date() As Date
        Get
            Return mdtIssue_Date
        End Get
        Set(ByVal value As Date)
            mdtIssue_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set expiry_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Expiry_Date() As Date
        Get
            Return mdtExpiry_Date
        End Get
        Set(ByVal value As Date)
            mdtExpiry_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changereasonunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Changereasonunkid() As Integer
        Get
            Return mintChangereasonunkid
        End Get
        Set(ByVal value As Integer)
            mintChangereasonunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isresidentpermit
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isresidentpermit() As Boolean
        Get
            Return mblnIsresidentpermit
        End Get
        Set(ByVal value As Boolean)
            mblnIsresidentpermit = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audituserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Audituserunkid() As Integer
        Get
            Return mintAudituserunkid
        End Get
        Set(ByVal value As Integer)
            mintAudituserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set hostname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Hostname() As String
        Get
            Return mstrHostname
        End Get
        Set(ByVal value As String)
            mstrHostname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mblnIsProcessed
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _IsProcessed() As Boolean
        Get
            Return mblnIsProcessed
        End Get
        Set(ByVal value As Boolean)
            mblnIsProcessed = value
        End Set
    End Property


    'S.SANDEEP |17-JAN-2019| -- START
    Public Property _OperationTypeId() As Integer
        Get
            Return mintOperationTypeId
        End Get
        Set(ByVal value As Integer)
            mintOperationTypeId = value
        End Set
    End Property
    'S.SANDEEP |17-JAN-2019| -- END


    'Pinkal (20-Dec-2024) -- Start
    'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 

    ''' <summary>
    ''' Purpose: Get or Set mdtReminderDate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _ReminderDate() As DateTime
        Get
            Return mdtReminderDate
        End Get
        Set(ByVal value As DateTime)
            mdtReminderDate = value
        End Set
    End Property

    'Pinkal (20-Dec-2024) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal blnIsResidentPermit As Boolean, Optional ByVal xEmployeeUnkid As Integer = 0, Optional ByVal blnOnlyPending As Boolean = True) As DataSet 'S.SANDEEP [14-AUG-2018] -- START {Ref#244} [blnOnlyPending] -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mdtAppDate As String = ""
        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                   "    @ADate = CONVERT(CHAR(8),hremployee_master.appointeddate,112) " & _
                   "FROM hremployee_master " & _
                   "WHERE employeeunkid = '" & xEmployeeUnkid & "' "

            objDataOperation.AddParameter("@ADate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mdtAppDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtAppDate = objDataOperation.GetParameterValue("@ADate")

            strQ = "SELECT " & _
              "  hremployee_permit_approval_tran.tranguid " & _
                    ", hremployee_permit_approval_tran.transactiondate " & _
                    ", hremployee_permit_approval_tran.mappingunkid " & _
                    ", hremployee_permit_approval_tran.remark " & _
                    ", hremployee_permit_approval_tran.isfinal " & _
                    ", hremployee_permit_approval_tran.statusunkid " & _
                   ", hremployee_permit_approval_tran.workpermittranunkid " & _
                   ", hremployee_permit_approval_tran.effectivedate " & _
                   ", hremployee_permit_approval_tran.employeeunkid " & _
                   ", hremployee_permit_approval_tran.work_permit_no " & _
                   ", hremployee_permit_approval_tran.workcountryunkid " & _
                   ", hremployee_permit_approval_tran.issue_place " & _
                   ", hremployee_permit_approval_tran.issue_date " & _
                   ", hremployee_permit_approval_tran.expiry_date " & _
                   ", hremployee_permit_approval_tran.changereasonunkid " & _
                   ", hremployee_permit_approval_tran.statusunkid " & _
                   ", hremployee_permit_approval_tran.isvoid " & _
                   ", hremployee_permit_approval_tran.voiduserunkid " & _
                   ", hremployee_permit_approval_tran.voiddatetime " & _
                   ", hremployee_permit_approval_tran.voidreason " & _
                   ", hremployee_master.employeecode as ecode " & _
                   ", hremployee_master.firstname+' '+ISNULL(hremployee_master.othername,'')+' '+hremployee_master.surname AS ename " & _
                   ", CONVERT(CHAR(8),hremployee_permit_approval_tran.effectivedate,112) AS edate " & _
                   ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS adate " & _
                   ", CONVERT(CHAR(8),hremployee_permit_approval_tran.issue_date,112) AS isdate " & _
                   ", CONVERT(CHAR(8),hremployee_permit_approval_tran.expiry_date,112) AS exdate " & _
                   ", '' AS EffDate " & _
                   ", '' AS IDate " & _
                   ", '' AS ExDate " & _
                   ", CASE WHEN rehiretranunkid > 0 THEN ISNULL(RH.name,'') ELSE " & _
                   "        CASE WHEN ISNULL(hremployee_permit_approval_tran.isresidentpermit,0) <=0 THEN ISNULL(cfcommon_master.name,'') " & _
                   "        ELSE ISNULL(RP.name,'') END " & _
                   "  END AS CReason " & _
                   ", ISNULL(CM.country_name,'') AS Country " & _
                   ", ISNULL(hremployee_permit_approval_tran.rehiretranunkid,0) AS rehiretranunkid " & _
                   ", ISNULL(hremployee_permit_approval_tran.isresidentpermit,0) AS isresidentpermit " & _
                   ", hremployee_permit_approval_tran.isvoid " & _
                    ", hremployee_permit_approval_tran.voiddatetime " & _
                    ", hremployee_permit_approval_tran.voidreason " & _
                    ", hremployee_permit_approval_tran.voiduserunkid " & _
                    ", hremployee_permit_approval_tran.audittype " & _
                    ", hremployee_permit_approval_tran.audituserunkid " & _
                    ", hremployee_permit_approval_tran.ip " & _
                    ", hremployee_permit_approval_tran.hostname " & _
                    ", hremployee_permit_approval_tran.form_name " & _
                    ", hremployee_permit_approval_tran.isweb " & _
                    ", ISNULL(hremployee_permit_approval_tran.operationtypeid,0) AS operationtypeid " & _
                    ", CASE WHEN ISNULL(hremployee_permit_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.ADDED) & "' THEN @ADDED " & _
                    "       WHEN ISNULL(hremployee_permit_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.EDITED) & "' THEN @EDITED " & _
                    "       WHEN ISNULL(hremployee_permit_approval_tran.operationtypeid,0) = '" & CInt(clsEmployeeMovmentApproval.enOperationType.DELETED) & "' THEN @DELETED " & _
                    "  END AS OperationType " & _
                    ", hremployee_permit_approval_tran.reminder_date " & _
             "FROM hremployee_permit_approval_tran " & _
                   "    LEFT JOIN hrmsConfiguration..cfcountry_master AS CM ON CM.countryunkid = hremployee_permit_approval_tran.workcountryunkid " & _
                   "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hremployee_permit_approval_tran.changereasonunkid AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.WORK_PERMIT & _
                   "    LEFT JOIN cfcommon_master AS RH ON RH.masterunkid = hremployee_permit_approval_tran.changereasonunkid AND RH.mastertype = " & clsCommon_Master.enCommonMaster.RE_HIRE & _
                   "    LEFT JOIN cfcommon_master AS RP ON RP.masterunkid = hremployee_permit_approval_tran.changereasonunkid AND RP.mastertype = " & clsCommon_Master.enCommonMaster.RESIDENT_PERMIT & _
                   "    JOIN hremployee_master ON hremployee_master.employeeunkid = hremployee_permit_approval_tran.employeeunkid " & _
                   "WHERE hremployee_permit_approval_tran.isvoid = 0 AND hremployee_permit_approval_tran.workpermittranunkid >= 0 AND ISNULL(hremployee_permit_approval_tran.isresidentpermit,0) = @isresidentpermit AND ISNULL(hremployee_permit_approval_tran.isprocessed,0) = 0  " 'S.SANDEEP [14 APR 2015] -- START {rehiretranunkid} -- END


            'Pinkal (20-Dec-2024) -- Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. [hremployee_permit_approval_tran.reminder_date]

            'S.SANDEEP [04-Jan-2018] -- START REF-ID # 120 {blnIsResidentPermit} -- END


            'S.SANDEEP [04-Jan-2018] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 120
            objDataOperation.AddParameter("@isresidentpermit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsResidentPermit)
            'S.SANDEEP [04-Jan-2018] -- END

            If xEmployeeUnkid > 0 Then
                strQ &= " AND hremployee_permit_approval_tran.employeeunkid = '" & xEmployeeUnkid & "' "
            End If

            If mdtAppDate.Trim.Length > 0 Then
                strQ &= " AND CONVERT(CHAR(8),hremployee_permit_approval_tran.effectivedate,112) >= '" & mdtAppDate & "' "
            End If

            strQ &= " ORDER BY effectivedate DESC "

            objDataOperation.AddParameter("@ADDED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 62, "Newly Added Information"))
            objDataOperation.AddParameter("@EDITED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 63, "Information Edited"))
            objDataOperation.AddParameter("@DELETED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployeeMovmentApproval", 64, "Information Deleted"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [14-AUG-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#244}
            Dim strValue As String = String.Join(",", dsList.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")).[Select](Function(x) New With {Key .empid = x.Key, Key .empstat = x.[Select](Function(z) z.Field(Of Integer)("statusunkid")).Max}).Where(Function(x) x.empstat = 2 Or x.empstat = 3).Select(Function(x) x.empstat.ToString()).ToArray())
            'S.SANDEEP [09-AUG-2018] -- START {Added : x.empstat | Removed : x.empid} -- END
            If strValue.Trim.Length > 0 Then
                'S.SANDEEP [09-AUG-2018] -- START
                'Dim dtTbl As DataTable = New DataView(dsList.Tables(0), "employeeunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                Dim dtTbl As DataTable = Nothing
                If strValue = clsEmployee_Master.EmpApprovalStatus.Rejected Then
                    strValue = String.Join(",", dsList.Tables(0).AsEnumerable().GroupBy(Function(x) x.Field(Of Integer)("employeeunkid")).[Select](Function(x) New With {Key .empid = x.Key, Key .empstat = x.[Select](Function(z) z.Field(Of Integer)("statusunkid")).Max}).Where(Function(x) x.empstat = 2 Or x.empstat = 3).Select(Function(x) x.empid.ToString()).ToArray())
                    If strValue.Trim.Length > 0 Then
                        dtTbl = New DataView(dsList.Tables(0), "employeeunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                    End If
                Else
                    dtTbl = New DataView(dsList.Tables(0), "statusunkid NOT IN (" & strValue & ")", "", DataViewRowState.CurrentRows).ToTable().Copy()
                End If
                'S.SANDEEP [09-AUG-2018] -- END
                dsList.Tables.Remove(strTableName)
                dsList.Tables.Add(dtTbl)
            End If

            'S.SANDEEP [14-AUG-2018] -- END
            For Each xRow As DataRow In dsList.Tables(0).Rows
                xRow("EffDate") = eZeeDate.convertDate(xRow.Item("edate").ToString).ToShortDateString
                If xRow.Item("isdate").ToString.Length > 0 Then
                    xRow("IDate") = eZeeDate.convertDate(xRow.Item("isdate").ToString).ToShortDateString
                End If
                If xRow.Item("exdate").ToString.Trim.Length > 0 Then
                    xRow("ExDate") = eZeeDate.convertDate(xRow.Item("exdate").ToString).ToShortDateString
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremployee_permit_approval_tran) </purpose>
    Public Function Insert(ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, ByVal xCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal blnFromApproval As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
            'S.SANDEEP |17-JAN-2019| -- START


        'Dim objDataOperation As New clsDataOperation
'        If xDataOpr IsNot Nothing Then
'            objDataOperation = xDataOpr
'            objDataOperation.ClearParameters()
'        Else
'            objDataOperation = New clsDataOperation
'        End If


        'Pinkal (20-Dec-2024) -- Start
        'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
        Dim mintEmployeeMovementApprovalReminderOccurrenceAfterDays As Integer = 0
        Dim objConfg As New clsConfigOptions
        Dim mstrEmployeeMovementApprovalReminderOccurrenceAfterDays As String = objConfg.GetKeyValue(xCompanyId, "EmployeeMovementApprovalReminderOccurrenceAfterDays", Nothing)
        If mstrEmployeeMovementApprovalReminderOccurrenceAfterDays.Trim.Length > 0 Then
            mintEmployeeMovementApprovalReminderOccurrenceAfterDays = CInt(mstrEmployeeMovementApprovalReminderOccurrenceAfterDays)
        End If
        objConfg = Nothing
        'Pinkal (20-Dec-2024) -- End



        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        If blnFromApproval = False Then
            'If isExist(mblnIsresidentpermit, Nothing, , mintEmployeeunkid, , objDataOperation, True) Then
            '    If mblnIsresidentpermit Then
            '        mstrMessage = Language.getMessage(mstrModuleName, 7, "Sorry, Resident Permit information is already present in pending state for the selected employee.")
            '    Else
            '        mstrMessage = Language.getMessage(mstrModuleName, 8, "Sorry, Work Permit information is already present in pending state for the selected employee.")
            '    End If
            '    Return False
            'End If

            'If isExist(mblnIsresidentpermit, mdtEffectivedate, , mintEmployeeunkid, , objDataOperation) Then
            '    If mblnIsresidentpermit Then
            '        mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, resident permit information is already present for the selected effective date.")
            '    Else
            '        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, work permit information is already present for the selected effective date.")
            '    End If
            '    Return False
            'End If

            'If isExist(mblnIsresidentpermit, Nothing, mstrWork_Permit_No, mintEmployeeunkid, , objDataOperation) Then
            '    If mblnIsresidentpermit Then
            '        mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, resident permit no is already present for the selected employee.")
            '    Else
            '        mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, work permit no is already present for the selected employee.")
            '    End If
            '    Return False
            'End If

            If isExist(mblnIsresidentpermit, eOprType, Nothing, "", mintEmployeeunkid, "", objDataOperation, True, mintWorkpermittranunkid) Then
                mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, Transfer information is already present in pending state for the selected employee.")
                Return False
            End If

                End If
            'S.SANDEEP |17-JAN-2019| -- END

        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@workpermittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkpermittranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@work_permit_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWork_Permit_No.ToString)
            objDataOperation.AddParameter("@workcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkcountryunkid.ToString)
            objDataOperation.AddParameter("@issue_place", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIssue_Place.ToString)
            If mdtIssue_Date <> Nothing Then
                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIssue_Date)
            Else
                objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            If mdtExpiry_Date <> Nothing Then
                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtExpiry_Date)
            Else
                objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@isresidentpermit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsresidentpermit.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@isprocessed", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsProcessed)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOperationTypeId)

            'Pinkal (20-Dec-2024) -- Start
            'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
            If mintEmployeeMovementApprovalReminderOccurrenceAfterDays > 0 Then
                mdtReminderDate = mdtTransactiondate.AddDays(mintEmployeeMovementApprovalReminderOccurrenceAfterDays).Date
            End If

            If mdtReminderDate <> Nothing Then
                objDataOperation.AddParameter("@reminder_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReminderDate)
            Else
                objDataOperation.AddParameter("@reminder_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (20-Dec-2024) -- End


            strQ = "INSERT INTO hremployee_permit_approval_tran ( " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", remark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", workpermittranunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", work_permit_no " & _
              ", workcountryunkid " & _
              ", issue_place " & _
              ", issue_date " & _
              ", expiry_date " & _
              ", changereasonunkid " & _
              ", rehiretranunkid " & _
              ", isresidentpermit " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voiduserunkid " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", hostname " & _
              ", form_name " & _
              ", isweb" & _
              ", isprocessed " & _
              ", operationtypeid " & _
              ", reminder_date " & _
            ") VALUES (" & _
              "  @tranguid " & _
              ", @transactiondate " & _
              ", @mappingunkid " & _
              ", @remark " & _
              ", @isfinal " & _
              ", @statusunkid " & _
              ", @workpermittranunkid " & _
              ", @effectivedate " & _
              ", @employeeunkid " & _
              ", @work_permit_no " & _
              ", @workcountryunkid " & _
              ", @issue_place " & _
              ", @issue_date " & _
              ", @expiry_date " & _
              ", @changereasonunkid " & _
              ", @rehiretranunkid " & _
              ", @isresidentpermit " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @voiduserunkid " & _
              ", @audittype " & _
              ", @audituserunkid " & _
              ", @ip " & _
              ", @hostname " & _
              ", @form_name " & _
              ", @isweb " & _
              ", @isprocessed " & _
              ", @operationtypeid " & _
              ", @reminder_date " & _
            "); SELECT @@identity"

            'Pinkal (20-Dec-2024) -- Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. [reminder_date]


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'mstrTranguid = dsList.Tables(0).Rows(0).Item(0)
            'S.SANDEEP |17-JAN-2019| -- START
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Update(ByVal xDataOpr As clsDataOperation, ByVal intEmployeeId As Integer, ByVal intTranUnkid As Integer, ByVal dtEffDate As Date, ByVal blnIsResident As Boolean, ByVal strColName As String) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

            'S.SANDEEP |17-JAN-2019| -- START
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
            'S.SANDEEP |17-JAN-2019| -- END
        Try
            'S.SANDEEP [15-NOV-2018] -- Start
            strQ = "UPDATE hremployee_permit_approval_tran SET " & _
                   " " & strColName & " = '" & intTranUnkid & "' " & _
                   "WHERE isprocessed= 0 and CONVERT(CHAR(8),effectivedate,112) = '" & eZeeDate.convertDate(dtEffDate).ToString & "' AND employeeunkid = '" & intEmployeeId & "' "
            '----REMOVED (effectivedate)
            '----ADDED (CONVERT(CHAR(8),effectivedate,112))
            'S.SANDEEP [15-NOV-2018] -- End
            If blnIsResident Then
                strQ &= " AND isresidentpermit = 1 "
            Else
                strQ &= " AND isresidentpermit = 0 "
            End If

            Call xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP |17-JAN-2019| -- START

            If xDataOpr Is Nothing Then
                If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
                objDataOperation.ReleaseTransaction(True)
            End If

            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
            'S.SANDEEP |17-JAN-2019| -- START


            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP |17-JAN-2019| -- END
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremployee_permit_approval_tran) </purpose>
    Public Function Update(ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
'S.SANDEEP |17-JAN-2019| -- START
        'Dim objDataOperation As New clsDataOperation

'        If xDataOpr IsNot Nothing Then
'            objDataOperation = xDataOpr
'            objDataOperation.ClearParameters()
'        Else
'            objDataOperation = New clsDataOperation
'        End If


        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
            'S.SANDEEP |17-JAN-2019| -- END

        If isExist(mblnIsresidentpermit, eOprType, mdtEffectivedate, , mintEmployeeunkid, mstrTranguid, objDataOperation) Then
            If mblnIsresidentpermit Then
                mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, resident permit information is already present for the selected effective date.")
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, work permit information is already present for the selected effective date.")
            End If
            Return False
        End If

        If isExist(mblnIsresidentpermit, eOprType, Nothing, mstrWork_Permit_No, mintEmployeeunkid, mstrTranguid, objDataOperation) Then
            If mblnIsresidentpermit Then
                mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, resident permit no is already present for the selected employee.")
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, work permit no is already present for the selected employee.")
            End If
            Return False
        End If

        Try
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTranguid.ToString)
            objDataOperation.AddParameter("@transactiondate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtTransactiondate.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@workpermittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkpermittranunkid.ToString)
            objDataOperation.AddParameter("@effectivedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffectivedate.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@work_permit_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWork_Permit_No.ToString)
            objDataOperation.AddParameter("@workcountryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWorkcountryunkid.ToString)
            objDataOperation.AddParameter("@issue_place", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIssue_Place.ToString)
            objDataOperation.AddParameter("@issue_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIssue_Date.ToString)
            objDataOperation.AddParameter("@expiry_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtExpiry_Date.ToString)
            objDataOperation.AddParameter("@changereasonunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangereasonunkid.ToString)
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@isresidentpermit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsresidentpermit.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudituserunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostname.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

            'Pinkal (20-Dec-2024) -- Start
            'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
            If mdtReminderDate <> Nothing Then
                objDataOperation.AddParameter("@reminder_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReminderDate)
            Else
                objDataOperation.AddParameter("@reminder_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'Pinkal (20-Dec-2024) -- End

            strQ = "UPDATE hremployee_permit_approval_tran SET " & _
              "  transactiondate = @transactiondate" & _
              ", mappingunkid = @mappingunkid" & _
              ", remark = @remark" & _
              ", isfinal = @isfinal" & _
              ", statusunkid = @statusunkid" & _
              ", workpermittranunkid = @workpermittranunkid" & _
              ", effectivedate = @effectivedate" & _
              ", employeeunkid = @employeeunkid" & _
              ", work_permit_no = @work_permit_no" & _
              ", workcountryunkid = @workcountryunkid" & _
              ", issue_place = @issue_place" & _
              ", issue_date = @issue_date" & _
              ", expiry_date = @expiry_date" & _
              ", changereasonunkid = @changereasonunkid" & _
              ", rehiretranunkid = @rehiretranunkid" & _
              ", isresidentpermit = @isresidentpermit" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", audittype = @audittype" & _
              ", audituserunkid = @audituserunkid" & _
              ", ip = @ip" & _
              ", hostname = @hostname" & _
              ", form_name = @form_name" & _
              ", isweb = @isweb " & _
                      ", reminder_date = @reminder_date " & _
            "WHERE tranguid = @tranguid "


            'Pinkal (20-Dec-2024) -- Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. [reminder_date = @reminder_date]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
                        'S.SANDEEP |17-JAN-2019| -- START

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            'S.SANDEEP |17-JAN-2019| -- END
            Return True
        Catch ex As Exception
            'S.SANDEEP |17-JAN-2019| -- START


            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
             'S.SANDEEP |17-JAN-2019| -- END
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremployee_permit_approval_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal eOprType As clsEmployeeMovmentApproval.enOperationType, ByVal intCompanyId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        mstrMessage = ""
        Try
            Dim objemployee_workpermit_tran As New clsemployee_workpermit_tran

            objemployee_workpermit_tran._Workpermittranunkid(objDataOperation) = intUnkid

            Me._Audittype = enAuditType.ADD
            Me._Audituserunkid = User._Object._Userunkid
            Me._Effectivedate = objemployee_workpermit_tran._Effectivedate
            Me._Employeeunkid = objemployee_workpermit_tran._Employeeunkid
            Me._Expiry_Date = objemployee_workpermit_tran._Expiry_Date
            Me._Issue_Date = objemployee_workpermit_tran._Issue_Date
            Me._Issue_Place = objemployee_workpermit_tran._Issue_Place
            Me._Work_Permit_No = objemployee_workpermit_tran._Work_Permit_No
            Me._Workcountryunkid = objemployee_workpermit_tran._Workcountryunkid
            Me._Changereasonunkid = objemployee_workpermit_tran._Changereasonunkid
            Me._Isresidentpermit = mblnIsresidentpermit

            Me._Isvoid = False
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
            'Gajanan [9-July-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            'Me._Voidreason = ""
            'Gajanan [9-July-2019] -- End
            Me._Voiduserunkid = -1
            Me._Tranguid = Guid.NewGuid.ToString()
            Me._Transactiondate = Now
            Me._Remark = ""
            Me._Rehiretranunkid = objemployee_workpermit_tran._Rehiretranunkid
            Me._Mappingunkid = 0

            Me._Isfinal = False


            Me._Workpermittranunkid = intUnkid
            Me._IsProcessed = False
            Me._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.DELETED
            Me._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval


            objemployee_workpermit_tran._Voidreason = Me._Voidreason
            objemployee_workpermit_tran._Voiduserunkid = Me._Audituserunkid
            objemployee_workpermit_tran._Voiddatetime = Now
            objemployee_workpermit_tran._Isvoid = True


            'Gajanan [14-AUG-2019] -- Start      
            'ISSUE/ENHANCEMENT : AT Changes For Display Missing Column
            If mblnIsweb Then
            objemployee_workpermit_tran._WebClientIP = Me._Ip
            objemployee_workpermit_tran._WebFormName = Me._Form_Name
            objemployee_workpermit_tran._WebHostName = Me._Hostname
            End If
            objemployee_workpermit_tran._Userunkid = Me._Audituserunkid
            'Gajanan [14-AUG-2019] -- End


            'Pinkal (20-Dec-2024) -- Start
            'Toyota Enhancement : (A1X-2901) Toyota - Approver reminders on pending approvals for employee movements. 
            'blnFlag = Insert(clsEmployeeMovmentApproval.enOperationType.DELETED, objDataOperation)
            blnFlag = Insert(clsEmployeeMovmentApproval.enOperationType.DELETED, intCompanyId, objDataOperation)
            'Pinkal (20-Dec-2024) -- End



            If blnFlag = True AndAlso mstrMessage.Trim.Length <= 0 Then
                If objemployee_workpermit_tran.Delete(intUnkid, objDataOperation) = False Then
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
                End If
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return blnFlag
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@tranguid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal blnIsResidentPermit As Boolean, _
                            ByVal xeOprType As clsEmployeeMovmentApproval.enOperationType, _
                            Optional ByVal xEffDate As DateTime = Nothing, _
                            Optional ByVal xWPermitNo As String = "", _
                            Optional ByVal xEmployeeId As Integer = 0, _
                            Optional ByVal strUnkid As String = "", _
                            Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                            Optional ByVal blnPreventNewEntry As Boolean = False, _
                            Optional ByVal xTranferUnkid As Integer = 0) As Boolean 'S.SANDEEP |17-JAN-2019| -- START {xTranferUnkid} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
            'S.SANDEEP |17-JAN-2019| -- START
        'Dim objDataOperation As New clsDataOperation

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
            'S.SANDEEP |17-JAN-2019| -- END
        Try
            strQ = "SELECT " & _
              "  tranguid " & _
              ", transactiondate " & _
              ", mappingunkid " & _
              ", remark " & _
              ", isfinal " & _
              ", statusunkid " & _
              ", workpermittranunkid " & _
              ", effectivedate " & _
              ", employeeunkid " & _
              ", work_permit_no " & _
              ", workcountryunkid " & _
              ", issue_place " & _
              ", issue_date " & _
              ", expiry_date " & _
              ", changereasonunkid " & _
              ", rehiretranunkid " & _
              ", isresidentpermit " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", voiduserunkid " & _
              ", audittype " & _
              ", audituserunkid " & _
              ", ip " & _
              ", hostname " & _
              ", form_name " & _
              ", isweb " & _
             "FROM hremployee_permit_approval_tran " & _
             "WHERE isvoid = 0 AND isresidentpermit = @isresidentpermit and operationtypeid = @operationtypeid " & _
             "AND statusunkid <> 3 "

            If blnPreventNewEntry Then
                'S.SANDEEP [09-AUG-2018] -- START
                'strQ &= " AND hremployee_transfer_approval_tran.isfinal = 0 "
                strQ &= " AND hremployee_permit_approval_tran.isfinal = 0 AND ISNULL(isprocessed,0) = 0 "
                'S.SANDEEP [09-AUG-2018] -- END
            End If

            If strUnkid.Trim.Length > 0 Then
                strQ &= " AND tranguid <> @tranguid"
            End If

            objDataOperation.AddParameter("@isresidentpermit", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsResidentPermit)
            If xEffDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),effectivedate,112) = @effectivedate "
            End If
            If xWPermitNo.Trim.Length > 0 Then
                strQ &= " AND work_permit_no = @work_permit_no "
                objDataOperation.AddParameter("@work_permit_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWork_Permit_No)
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
            End If

            'S.SANDEEP |17-JAN-2019| -- START
            If xeOprType = clsEmployeeMovmentApproval.enOperationType.EDITED Or mintOperationTypeId = clsEmployeeMovmentApproval.enOperationType.DELETED Then
                If xTranferUnkid > 0 Then
                    strQ &= " AND workpermittranunkid = @workpermittranunkid "
                    objDataOperation.AddParameter("@workpermittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTranferUnkid)
                End If
            End If
            'S.SANDEEP |17-JAN-2019| -- END

            objDataOperation.AddParameter("@effectivedate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEffDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@tranguid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strUnkid)
            objDataOperation.AddParameter("@operationtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xeOprType)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


End Class