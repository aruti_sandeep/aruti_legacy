﻿'************************************************************************************************************************************
'Class Name : clsclsemp_appUsermapping.vb
'Purpose    :
'Date       :14-Jun-2018
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsemp_appUsermapping
    Private Shared ReadOnly mstrModuleName As String = "clsemp_appUsermapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintMappingunkid As Integer
    Private mintEmpapplevelunkid As Integer
    Private mintMapuserunkid As Integer
    Private mblnIsactive As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing
    Private mintApprovertypeid As enEmpApproverType

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mappingunkid(Optional ByVal objOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
            Call GetData(objOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empapplevelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Empapplevelunkid() As Integer
        Get
            Return mintEmpapplevelunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpapplevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mapuserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mapuserunkid() As Integer
        Get
            Return mintMapuserunkid
        End Get
        Set(ByVal value As Integer)
            mintMapuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvertypeid() As enEmpApproverType
        Get
            Return mintApprovertypeid
        End Get
        Set(ByVal value As enEmpApproverType)
            mintApprovertypeid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  mappingunkid " & _
                      ", empapplevelunkid " & _
                      ", mapuserunkid " & _
                      ", isactive " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", approvertypeid " & _
                     "FROM hremp_appusermapping " & _
                     "WHERE mappingunkid = @mappingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintEmpapplevelunkid = CInt(dtRow.Item("empapplevelunkid"))
                mintMapuserunkid = CInt(dtRow.Item("mapuserunkid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintApprovertypeid = CType(CInt(dtRow.Item("approvertypeid")), enEmpApproverType)
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal eApproverType As enEmpApproverType, ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mstrFilter As String = "", Optional ByVal intApproverUserUnkId As Integer = 0 _
                            , Optional ByVal objDataOpr As clsDataOperation = Nothing _
                            ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        If objDataOpr IsNot Nothing Then
            objDataOperation = objDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                      "  hremp_appusermapping.mappingunkid " & _
                      ", hremp_appusermapping.empapplevelunkid " & _
                      ", ISNULL(hrempapproverlevel_master.empapplevelcode,'') as LevelCode " & _
                      ", ISNULL(hrempapproverlevel_master.empapplevelname,'') as Level " & _
                      ", hremp_appusermapping.mapuserunkid " & _
                      ", ISNULL(hrmsconfiguration..cfuser_master.username,'') AS [User] " & _
                      ", hremp_appusermapping.isactive " & _
                      ", CASE WHEN hremp_appusermapping.isactive = 0 THEN @InActive ELSE @Active END As Status " & _
                      ", hremp_appusermapping.isvoid " & _
                      ", hremp_appusermapping.voiduserunkid " & _
                      ", hremp_appusermapping.voiddatetime " & _
                      ", hremp_appusermapping.voidreason " & _
                      ", ISNULL(hrempapproverlevel_master.priority,0) AS priority " & _
                      " FROM hremp_appusermapping " & _
                      " LEFT JOIN hrempapproverlevel_master ON hremp_appusermapping.empapplevelunkid = hrempapproverlevel_master.empapplevelunkid " & _
                      " LEFT JOIN hrmsconfiguration..cfuser_master ON hrmsconfiguration..cfuser_master.userunkid = hremp_appusermapping.mapuserunkid " & _
                      " WHERE hremp_appusermapping.isvoid = 0 AND hremp_appusermapping.approvertypeid = @approvertypeid "


            If intApproverUserUnkId > 0 Then
                strQ &= " AND hremp_appusermapping.mapuserunkid = @mapuserunkid "
            End If


            If blnOnlyActive Then
                strQ &= " AND hremp_appusermapping.isactive = 1 "
            Else
                strQ &= " AND hremp_appusermapping.isactive = 0 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " " & mstrFilter.Trim
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(eApproverType))
            objDataOperation.AddParameter("@InActive", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "In Active"))
            objDataOperation.AddParameter("@Active", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Active"))
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkId)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremp_appusermapping) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintApprovertypeid, mintMapuserunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This user is already map with another level or same level.please select new user to map with this level.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@empapplevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpapplevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@approvertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertypeid)

            strQ = "INSERT INTO hremp_appusermapping ( " & _
                      "  empapplevelunkid " & _
                      ", mapuserunkid " & _
                      ", isactive " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", approvertypeid " & _
                    ") VALUES (" & _
                      "  @empapplevelunkid " & _
                      ", @mapuserunkid " & _
                      ", @isactive " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason " & _
                      ", @approvertypeid " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMappingunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForEmpApproverUserMapping(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremp_appusermapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = " UPDATE hremp_appusermapping SET " & _
                      " isvoid = @isvoid " & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = @voiddatetime " & _
                      ", voidreason = @voidreason " & _
                      " WHERE mappingunkid = @mappingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Mappingunkid(objDataOperation) = intUnkid

            If InsertAuditTrailForEmpApproverUserMapping(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intApprovertypeid As Integer, ByVal xMapUserId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  mappingunkid " & _
                      ", empapplevelunkid " & _
                      ", mapuserunkid " & _
                      ", isactive " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      " FROM hremp_appusermapping " & _
                      " WHERE mapuserunkid = @mapuserunkid " & _
                      " AND isvoid = 0 AND approvertypeid = @approvertypeid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApprovertypeid)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMapUserId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveapprover_master) </purpose>
    
    Public Function InActiveApprover(ByVal xMappingId As Integer, ByVal intCompanyID As Integer, ByVal blnIsActive As Boolean) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            If blnIsActive Then
                strQ = " Update hremp_appusermapping set isactive = 1 where mappingunkid = @mappingunkid AND isvoid = 0  "
            Else
                strQ = " Update hremp_appusermapping set isactive = 0 where mappingunkid = @mappingunkid AND isvoid = 0 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xMappingId)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Mappingunkid(objDataOperation) = xMappingId

            If InsertAuditTrailForEmpApproverUserMapping(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InActiveApprover; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrailForEmpApproverUserMapping(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = " INSERT INTO athremp_appusermapping ( " & _
                      "  mappingunkid " & _
                      ", empapplevelunkid " & _
                      ", mapuserunkid " & _
                      ", isactive " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip" & _
                      ", hostname" & _
                      ", form_name " & _
                      ", isweb " & _
                      ", approvertypeid " & _
                      " ) VALUES (" & _
                      "  @mappingunkid " & _
                      ", @empapplevelunkid " & _
                      ", @mapuserunkid " & _
                      ", @isactive " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip" & _
                      ", @hostname" & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ", @approvertypeid " & _
                      " ); "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid)
            objDataOperation.AddParameter("@empapplevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpapplevelunkid.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)
            objDataOperation.AddParameter("@approvertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertypeid)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForEmpApproverUserMapping; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This user is already map with another level or same level.please select new user to map with this level.")
            Language.setMessage(mstrModuleName, 2, "In Active")
            Language.setMessage(mstrModuleName, 3, "Active")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class