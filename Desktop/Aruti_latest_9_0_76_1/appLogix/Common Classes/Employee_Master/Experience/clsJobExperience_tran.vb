﻿'************************************************************************************************************************************
'Class Name : clsJobExperience_tran.vb
'Purpose    :
'Date       :20/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsJobExperience_tran
    Private Shared ReadOnly mstrModuleName As String = "clsJobExperience_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintExperiencetranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mstrCompany As String = String.Empty
    Private mstrAddress As String = String.Empty
    Private mintJobunkid As Integer
    Private mdtStart_Date As Date
    Private mdtEnd_Date As Date
    Private mstrSupervisor As String = String.Empty
    Private mstrRemark As String = String.Empty

    'Anjan (11 May 2011)-Start
    'Private mdblLast_Pay As Double
    Private mdecLast_Pay As Decimal
    'Anjan (11 May 2011)-End 


    Private mblnIscontact_Previous As Boolean
    Private mstrContact_Person As String = String.Empty
    Private mstrContact_No As String = String.Empty
    Private mstrMemo As String = String.Empty
    Private mstrLeave_Reason As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrPreviousBenefitPlan As String = String.Empty

    Private blnIsInserted As Boolean



    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    Private mintLoginEmployeeunkid As Integer = -1
    Private mintVoidloginEmployeeunkid As Integer = -1
    'Pinkal (12-Oct-2011) -- End

    'Anjan (02 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    Private mstrJob As String = String.Empty
    Private mstrOtherBenefit As String = String.Empty
    Private mstrCurrencySign As String = String.Empty
    'Anjan (02 Mar 2012)-End 
    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.



    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    'Gajanan [17-DEC-2018] -- End

    'Hemant (11 Apr 2022) -- Start            
    'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
    Private mdtCreated_Date As Date
    'Hemant (11 Apr 2022) -- End

    'S.SANDEEP |24-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : Sprint 2023-13
    Private mblnIsGovJob As Boolean = False
    'S.SANDEEP |24-JUN-2023| -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set experiencetranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Experiencetranunkid() As Integer
        Get
            Return mintExperiencetranunkid
        End Get
        Set(ByVal value As Integer)
            mintExperiencetranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set company
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Company() As String
        Get
            Return mstrCompany
        End Get
        Set(ByVal value As String)
            mstrCompany = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Address() As String
        Get
            Return mstrAddress
        End Get
        Set(ByVal value As String)
            mstrAddress = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jobunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Jobunkid() As Integer
        Get
            Return mintJobunkid
        End Get
        Set(ByVal value As Integer)
            mintJobunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set start_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Start_Date() As Date
        Get
            Return mdtStart_Date
        End Get
        Set(ByVal value As Date)
            mdtStart_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set end_date
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _End_Date() As Date
        Get
            Return mdtEnd_Date
        End Get
        Set(ByVal value As Date)
            mdtEnd_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set supervisor
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Supervisor() As String
        Get
            Return mstrSupervisor
        End Get
        Set(ByVal value As String)
            mstrSupervisor = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set last_pay
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>

    'Anjan (11 May 2011)-Start
    'Public Property _Last_Pay() As Double
    Public Property _Last_Pay() As Decimal
        'Anjan (11 May 2011)-End 


        Get
            Return mdecLast_Pay
        End Get

        'Anjan (11 May 2011)-Start
        'Set(ByVal value As Double)
        Set(ByVal value As Decimal)
            'Anjan (11 May 2011)-End

            mdecLast_Pay = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscontact_previous
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Iscontact_Previous() As Boolean
        Get
            Return mblnIscontact_Previous
        End Get
        Set(ByVal value As Boolean)
            mblnIscontact_Previous = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contact_person
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Contact_Person() As String
        Get
            Return mstrContact_Person
        End Get
        Set(ByVal value As String)
            mstrContact_Person = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contact_no
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Contact_No() As String
        Get
            Return mstrContact_No
        End Get
        Set(ByVal value As String)
            mstrContact_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set memo
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Memo() As String
        Get
            Return mstrMemo
        End Get
        Set(ByVal value As String)
            mstrMemo = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leave_reason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Leave_Reason() As String
        Get
            Return mstrLeave_Reason
        End Get
        Set(ByVal value As String)
            mstrLeave_Reason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Previous Benefit
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _PreviousBenefit() As String
        Get
            Return mstrPreviousBenefitPlan
        End Get
        Set(ByVal value As String)
            mstrPreviousBenefitPlan = value
        End Set
    End Property


    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    ''' <summary>
    ''' Purpose: Get or Set loginEmployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' 
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    ''' <summary>
    ''' Purpose: Get or Set voidloginEmployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' 
    Public Property _VoidLoginEmployeeunkid() As Integer
        Get
            Return mintVoidloginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginEmployeeunkid = value
        End Set
    End Property

    'Pinkal (12-Oct-2011) -- End


    'Anjan (02 Mar 2012)-Start
    'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    ''' <summary>
    ''' Purpose: Get or Set supervisor
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _CurrencySign() As String
        Get
            Return mstrCurrencySign
        End Get
        Set(ByVal value As String)
            mstrCurrencySign = value
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set supervisor
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Job() As String
        Get
            Return mstrJob
        End Get
        Set(ByVal value As String)
            mstrJob = value
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set supervisor
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _OtherBenefit() As String
        Get
            Return mstrOtherBenefit
        End Get
        Set(ByVal value As String)
            mstrOtherBenefit = value
        End Set
    End Property
    'Anjan (02 Mar 2012)-End
    'Gajanan [17-DEC-2018] -- Start
    'Enhancement - Implementing Employee Approver Flow On Employee Data.



    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    'Gajanan [17-DEC-2018] -- End

    'Hemant (11 Apr 2022) -- Start            
    'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
    ''' <summary>
    ''' Purpose: Get or Set created_date
    ''' Modify By: Hemant Morker
    ''' </summary>
    Public Property _Created_Date() As Date
        Get
            Return mdtCreated_Date
        End Get
        Set(ByVal value As Date)
            mdtCreated_Date = value
        End Set
    End Property
    'Hemant (11 Apr 2022) -- End

    'S.SANDEEP |24-JUN-2023| -- START
    'ISSUE/ENHANCEMENT : Sprint 2023-13
    Public Property _IsGovJob() As Boolean
        Get
            Return mblnIsGovJob
        End Get
        Set(ByVal value As Boolean)
            mblnIsGovJob = value
        End Set
    End Property
    'S.SANDEEP |24-JUN-2023| -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOperation As clsDataOperation = Nothing) 'Gajanan [17-DEC-2018] -- ADD Data Opration
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.


        'objDataOperation = New clsDataOperation

        If objDataOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        End If
        'Gajanan [17-DEC-2018] -- End
        Try


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            'strQ = "SELECT " & _
            '          "  experiencetranunkid " & _
            '          ", employeeunkid " & _
            '          ", company " & _
            '          ", address " & _
            '          ", jobunkid " & _
            '          ", start_date " & _
            '          ", end_date " & _
            '          ", supervisor " & _
            '          ", remark " & _
            '          ", last_pay " & _
            '          ", iscontact_previous " & _
            '          ", contact_person " & _
            '          ", contact_no " & _
            '          ", memo " & _
            '          ", leave_reason " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", voidreason " & _
            '         "FROM hremp_experience_tran " & _
            '         "WHERE experiencetranunkid = @experiencetranunkid "


            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'strQ = "SELECT " & _
            '          "  experiencetranunkid " & _
            '          ", employeeunkid " & _
            '          ", company " & _
            '          ", address " & _
            '          ", jobunkid " & _
            '          ", start_date " & _
            '          ", end_date " & _
            '          ", supervisor " & _
            '          ", remark " & _
            '          ", last_pay " & _
            '          ", iscontact_previous " & _
            '          ", contact_person " & _
            '          ", contact_no " & _
            '          ", memo " & _
            '          ", leave_reason " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", voidreason " & _
            '       ", loginemployeeunkid " & _
            '       ", voidloginemployeeunkid " & _
            '        "FROM hremp_experience_tran " & _
            '         "WHERE experiencetranunkid = @experiencetranunkid "

            strQ = "SELECT " & _
                      "  experiencetranunkid " & _
                      ", employeeunkid " & _
                      ", company " & _
                      ", address " & _
                      ", start_date " & _
                      ", end_date " & _
                      ", supervisor " & _
                      ", remark " & _
                      ", last_pay " & _
                      ", iscontact_previous " & _
                      ", contact_person " & _
                      ", contact_no " & _
                      ", memo " & _
                      ", leave_reason " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                   ", loginemployeeunkid " & _
                   ", voidloginemployeeunkid " & _
                      ", old_job " & _
                      ", otherbenefit " & _
                      ", currencysign " & _
                      ", created_date " & _
                      ", ISNULL(isgovjob,0) AS isgovjob " & _
                     "FROM hremp_experience_tran " & _
                     "WHERE experiencetranunkid = @experiencetranunkid "
            'S.SANDEEP |24-JUN-2023| -- START {isgovjob} -- END
            'Hemant (11 Apr 2022) -- [created_date]
            'Anjan (02 Mar 2012)-End 




            'Pinkal (12-Oct-2011) -- End



            objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExperiencetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintexperiencetranunkid = CInt(dtRow.Item("experiencetranunkid"))
                mintemployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mstrcompany = dtRow.Item("company").ToString
                mstraddress = dtRow.Item("address").ToString
                mdtStart_Date = dtRow.Item("start_date")

                'Anjan (17 Apr 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                'mdtEnd_Date = dtRow.Item("end_date")
                If IsDBNull(dtRow.Item("end_date")) = False Then
                    mdtEnd_Date = dtRow.Item("end_date")
                Else
                    mdtEnd_Date = Nothing
                End If
                'Anjan (17 Apr 2012)-End 


                mstrsupervisor = dtRow.Item("supervisor").ToString
                mstrRemark = dtRow.Item("remark").ToString

                'Anjan (11 May 2011)-Start
                'mdecLast_Pay = cdec(dtRow.Item("last_pay"))
                mdecLast_Pay = CDec(dtRow.Item("last_pay"))
                'Anjan (11 May 2011)-End 


                mblniscontact_previous = CBool(dtRow.Item("iscontact_previous"))
                mstrcontact_person = dtRow.Item("contact_person").ToString
                mstrcontact_no = dtRow.Item("contact_no").ToString
                mstrmemo = dtRow.Item("memo").ToString
                mstrleave_reason = dtRow.Item("leave_reason").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString


                'Pinkal (12-Oct-2011) -- Start
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

                If Not IsDBNull(dtRow.Item("loginemployeeunkid")) Then
                    mintLoginEmployeeunkid = dtRow.Item("loginemployeeunkid")
                End If

                If Not IsDBNull(dtRow.Item("voidloginemployeeunkid")) Then
                    mintVoidloginEmployeeunkid = dtRow.Item("voidloginemployeeunkid")
                End If

                'Pinkal (12-Oct-2011) -- End

                'Anjan (02 Mar 2012)-Start
                'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
                mstrJob = dtRow.Item("old_job").ToString
                mstrOtherBenefit = dtRow.Item("otherbenefit").ToString
                mstrCurrencySign = dtRow.Item("currencysign").ToString
                'Anjan (02 Mar 2012)-End 

                'Hemant (11 Apr 2022) -- Start            
                'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
                mdtCreated_Date = dtRow.Item("created_date")
                'Hemant (11 Apr 2022) -- End

                'S.SANDEEP |24-JUN-2023| -- START
                'ISSUE/ENHANCEMENT : Sprint 2023-13
                mblnIsGovJob = CBool(dtRow.Item("isgovjob"))
                'S.SANDEEP |24-JUN-2023| -- END

                Exit For
            Next
            mstrPreviousBenefitPlan = GetPreviousBenefit()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal strFilerString As String = "", _
                            Optional ByVal IsUsedAsMSS As Boolean = True, _
                            Optional ByVal mblnAddApprovalCondition As Boolean = True) As DataSet

        'S.SANDEEP [20-JUN-2018] -- 'Enhancement - Implementing Employee Approver Flow For NMB .[Optional ByVal mblnAddApprovalCondition As Boolean = True]

        'Pinkal (28-Dec-2015) -- Start    'Enhancement - Working on Changes in SS for Employee Master. [Optional ByVal IsUsedAsMSS As Boolean = True] 

        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            'Pinkal (28-Dec-2015) -- Start
            'Enhancement - Working on Changes in SS for Employee Master.
            ' Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)

            'S.SANDEEP [20-JUN-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow For NMB .
            'If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            If IsUsedAsMSS Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , mblnAddApprovalCondition)
            'S.SANDEEP [20-JUN-2018] -- End

            'Pinkal (28-Dec-2015) -- End


            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            Using objDo As New clsDataOperation
                StrQ = "SELECT " & _
                       "     ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                       "    ,ISNULL(hremp_experience_tran.company,'') AS Company " & _
                       "    ,CONVERT(CHAR(8),hremp_experience_tran.start_date,112) AS StartDate " & _
                       "    ,CONVERT(CHAR(8),hremp_experience_tran.end_date,112) AS EndDate " & _
                       "    ,ISNULL(hremp_experience_tran.supervisor,'') AS Supervisor " & _
                       "    ,ISNULL(hremp_experience_tran.remark,'') AS Remark " & _
                       "    ,hremployee_master.employeeunkid AS EmpId " & _
                       "    ,hremp_experience_tran.experiencetranunkid AS ExpId " & _
                       "    ,old_job AS Jobs " & _
                       "    ,otherbenefit AS OtherBenefit " & _
                       "    ,currencysign AS CurrencySign " & _
                       "    ,ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
                       "    ,ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
                       "    ,ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
                       "    ,ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
                       "    ,ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
                       "    ,ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
                       "    ,ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
                       "    ,ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
                       "    ,ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
                       "    ,ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
                       "    ,ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
                       "    ,ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
                       "    ,ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
                       "    ,ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
                       "    ,ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid " & _
                       "    ,hremp_experience_tran.loginemployeeunkid " & _
                       "    ,hremp_experience_tran.voidloginemployeeunkid " & _
                       "    ,0 AS operationtypeid " & _
                       "    ,'' AS OperationType " & _
                       "    , hremp_experience_tran.leave_reason " & _
                       "    , hremp_experience_tran.contact_person " & _
                       "    , hremp_experience_tran.contact_no " & _
                       "    , '' AS AUD " & _
                       "    , '' AS GUID " & _
                       "    ,CONVERT(CHAR(8), hremp_experience_tran.created_date, 112) AS Date " & _
                       "    ,ISNULL(hremp_experience_tran.isgovjob,0) AS isgovjob " & _
                       "FROM hremp_experience_tran " & _
                       "LEFT JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid "
                'S.SANDEEP |24-JUN-2023| -- START {isgovjob} -- END
                'Hemant (11 Apr 2022) -- [Date]
                'Sohail (25 Sep 2020) - [leave_reason, contact_person, contact_no, AUD, GUID]
                'Gajanan [17-DEC-2018] -- Start
                'Enhancement - Implementing Employee Approver Flow On Employee Data.
                '"    ,0 AS operationtypeid " --- ADDED
                '"    ,'' AS OperationType " --- ADDED
                'Gajanan [17-DEC-2018] -- End


                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= xAdvanceJoinQry
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry
                End If

                'S.SANDEEP [15 NOV 2016] -- START
                'If xUACQry.Trim.Length > 0 Then
                '    StrQ &= xUACQry
                'End If
                If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If
                End If
                'S.SANDEEP [15 NOV 2016] -- END

               

                StrQ &= " WHERE ISNULL(hremp_experience_tran.isvoid,0) = 0 "

                If blnApplyUserAccessFilter = True Then
                    If xUACFiltrQry.Trim.Length > 0 Then
                        StrQ &= " AND " & xUACFiltrQry
                    End If
                End If

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= xDateFilterQry
                    End If
                End If

                If strFilerString.Trim.Length > 0 Then
                    StrQ &= "AND " & strFilerString
                End If

                StrQ &= " ORDER BY CONVERT(CHAR(8),hremp_experience_tran.start_date,112)  DESC "

                dsList = objDo.ExecQuery(StrQ, strTableName)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & ": " & objDo.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal strIncludeInactiveEmployee As String = "", _
    '                        Optional ByVal strEmployeeAsOnDate As String = "", _
    '                        Optional ByVal strUserAccessLevelFilterString As String = "") As DataSet
    '    'S.SANDEEP [ 27 APRIL 2012 blnIncludeInactiveEmployee,strEmployeeAsOnDate,strUserAccessLevelFilterString] -- START -- END


    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try


    '        'Pinkal (12-Oct-2011) -- Start
    '        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

    '        'strQ = "SELECT " & _
    '        '            " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '            ",ISNULL(hremp_experience_tran.company,'') AS Company " & _
    '        '            ",ISNULL(hrjob_master.job_name,'') AS Jobs " & _
    '        '            ",CONVERT(CHAR(8),hremp_experience_tran.start_date,112) AS StartDate " & _
    '        '            ",CONVERT(CHAR(8),hremp_experience_tran.end_date,112) AS EndDate " & _
    '        '            ",ISNULL(hremp_experience_tran.supervisor,'') AS Supervisor " & _
    '        '            ",ISNULL(hremp_experience_tran.remark,'') AS Remark " & _
    '        '            ",hremployee_master.employeeunkid AS EmpId " & _
    '        '            ",hrjob_master.jobunkid AS JobId " & _
    '        '            ",hremp_experience_tran.experiencetranunkid AS ExpId " & _
    '        '        "FROM hremp_experience_tran " & _
    '        '            "LEFT JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "LEFT JOIN hrjob_master ON hremp_experience_tran.jobunkid = hrjob_master.jobunkid " & _
    '        '        "WHERE ISNULL(hremp_experience_tran.isvoid,0) = 0 "


    '        strQ = "SELECT " & _
    '                    " ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                    ",ISNULL(hremp_experience_tran.company,'') AS Company " & _
    '                    ",CONVERT(CHAR(8),hremp_experience_tran.start_date,112) AS StartDate " & _
    '                    ",CONVERT(CHAR(8),hremp_experience_tran.end_date,112) AS EndDate " & _
    '                    ",ISNULL(hremp_experience_tran.supervisor,'') AS Supervisor " & _
    '                    ",ISNULL(hremp_experience_tran.remark,'') AS Remark " & _
    '                    ",hremployee_master.employeeunkid AS EmpId " & _
    '                    ",hremp_experience_tran.experiencetranunkid AS ExpId "

    '        'Anjan (02 Mar 2012)-Start
    '        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
    '        strQ &= ", old_job AS Jobs " & _
    '                  ", otherbenefit AS OtherBenefit " & _
    '                  ", currencysign AS CurrencySign "
    '        'Anjan (02 Mar 2012)-End 


    '        'Anjan (21 Nov 2011)-Start
    '        'ENHANCEMENT : TRA COMMENTS
    '        strQ &= ",ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                 ",ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
    '                 ",ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
    '                 ",ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
    '                 ",ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
    '                 ",ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
    '                 ",ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
    '                 ",ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
    '                 ",ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
    '                 ",ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
    '                 ",ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
    '                 ",ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
    '                 ",ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid " & _
    '                 ",hremp_experience_tran.loginemployeeunkid " & _
    '                 ",hremp_experience_tran.voidloginemployeeunkid "
    '        'Anjan (21 Nov 2011)-End 


    '        strQ &= " FROM hremp_experience_tran " & _
    '                    "LEFT JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "WHERE ISNULL(hremp_experience_tran.isvoid,0) = 0 "

    '        'Pinkal (12-Oct-2011) -- End


    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        ''S.SANDEEP [ 29 JUNE 2011 ] -- START
    '        ''ISSUE : INACTIVE EMPLOYEE(S) COMING ON LIST
    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '        '    'Sohail (06 Jan 2012) -- Start
    '        '    'TRA - ENHANCEMENT
    '        '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    'Sohail (06 Jan 2012) -- End
    '        'End If
    '        ''S.SANDEEP [ 29 JUNE 2011 ] -- END 

    '        If strIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
    '        End If

    '        If CBool(strIncludeInactiveEmployee) = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END


    '        'Anjan (24 Jun 2011)-Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.


    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= " AND hremp_experience_tran.jobunkid IN (" & UserAccessLevel._AccessLevel & ")"
    '        'End If



    '        'S.SANDEEP [ 27 APRIL 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        'End Select

    '        If strUserAccessLevelFilterString = "" Then
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        Else
    '            strQ &= strUserAccessLevelFilterString
    '        End If
    '        'S.SANDEEP [ 27 APRIL 2012 ] -- END


    '        'Pinkal (25-APR-2012) -- Start
    '        'Enhancement : TRA Changes
    '        strQ &= " ORDER BY CONVERT(CHAR(8),hremp_experience_tran.start_date,112)  DESC "
    '        'Pinkal (25-APR-2012) -- End


    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'S.SANDEEP [04 JUN 2015] -- END

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hremp_experience_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                           Optional ByVal mdtTable As DataTable = Nothing) As Boolean 'Gajanan [17-DEC-2018] -- Start {xDataOpr} -- End

        'Gajanan [5-Dec-2019] -- Add [mdtTable]   


        'S.SANDEEP [ 27 FEB 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mintEmployeeunkid, mstrCompany) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Particular Job History is already entered for selected employee. Please enter new information.")
        '    Return False
        'End If

        'Anjan (17 Apr 2012)-Start
        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        'If isExist(mintEmployeeunkid, mstrCompany, mdtEnd_Date.Date) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Particular Job History is already entered for selected employee. Please enter new information.")
        '    Return False
        'End If


        'Gajanan [17-April-2019] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.




        'If isExist(mintEmployeeunkid, mstrCompany, mdtEnd_Date.Date, mdtStart_Date.Date, mstrJob) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Particular Job History is already entered for selected employee. Please enter new information.")
        '    Return False
        'End If

        If isExist(mintEmployeeunkid, mstrCompany, mdtStart_Date.Date, mdtEnd_Date.Date, mstrJob) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Particular Job History is already entered for selected employee. Please enter new information.")
            Return False
        End If
        'Gajanan [17-April-2019] -- End

        'Anjan (17 Apr 2012)-End 


        'S.SANDEEP [ 27 FEB 2012 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        'Gajanan [17-DEC-2018] -- End



        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany.ToString)
            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)

            If mdtStart_Date = Nothing Then
                objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date)
            End If

            If mdtEnd_Date = Nothing Then
                objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date)
            End If

            objDataOperation.AddParameter("@supervisor", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSupervisor.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)

            'Anjan (11 May 2011)-Start
            'objDataOperation.AddParameter("@last_pay", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecLast_Pay.ToString)
            objDataOperation.AddParameter("@last_pay", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLast_Pay)
            'Anjan (11 May 2011)-End 



            objDataOperation.AddParameter("@iscontact_previous", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscontact_Previous.ToString)
            objDataOperation.AddParameter("@contact_person", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_Person.ToString)
            objDataOperation.AddParameter("@contact_no", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrContact_No.ToString)
            objDataOperation.AddParameter("@memo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMemo.ToString)
            objDataOperation.AddParameter("@leave_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLeave_Reason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginEmployeeunkid.ToString)


            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            objDataOperation.AddParameter("@oldjob", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJob.ToString)
            objDataOperation.AddParameter("@otherbenefit", SqlDbType.NVarChar, 1000, mstrOtherBenefit.ToString)
            objDataOperation.AddParameter("@currencysign", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCurrencySign.ToString)
            'Anjan (02 Mar 2012)-End 

            'Hemant (11 Apr 2022) -- Start            
            'ISSUE/ENHANCEMENT(ZRA) : the updated information gets updated across all the vacancies including the closed ones. Only open vacancies should be updated with the new information
            If mdtCreated_Date = Nothing Then
                objDataOperation.AddParameter("@created_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@created_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCreated_Date)
            End If
            'Hemant (11 Apr 2022) -- End

            'S.SANDEEP |24-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : Sprint 2023-13
            objDataOperation.AddParameter("@isgovjob", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsGovJob)
            'S.SANDEEP |24-JUN-2023| -- END


            'strQ = "INSERT INTO hremp_experience_tran ( " & _
            '          "  employeeunkid " & _
            '          ", company " & _
            '          ", address " & _
            '          ", jobunkid " & _
            '          ", start_date " & _
            '          ", end_date " & _
            '          ", supervisor " & _
            '          ", remark " & _
            '          ", last_pay " & _
            '          ", iscontact_previous " & _
            '          ", contact_person " & _
            '          ", contact_no " & _
            '          ", memo " & _
            '          ", leave_reason " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", voidreason" & _
            '        ") VALUES (" & _
            '          "  @employeeunkid " & _
            '          ", @company " & _
            '          ", @address " & _
            '          ", @jobunkid " & _
            '          ", @start_date " & _
            '          ", @end_date " & _
            '          ", @supervisor " & _
            '          ", @remark " & _
            '          ", @last_pay " & _
            '          ", @iscontact_previous " & _
            '          ", @contact_person " & _
            '          ", @contact_no " & _
            '          ", @memo " & _
            '          ", @leave_reason " & _
            '          ", @userunkid " & _
            '          ", @isvoid " & _
            '          ", @voiduserunkid " & _
            '          ", @voiddatetime " & _
            '          ", @voidreason" & _
            '"); SELECT @@identity"


            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'strQ = "INSERT INTO hremp_experience_tran ( " & _
            '          "  employeeunkid " & _
            '          ", company " & _
            '          ", address " & _
            '          ", jobunkid " & _
            '          ", start_date " & _
            '          ", end_date " & _
            '          ", supervisor " & _
            '          ", remark " & _
            '          ", last_pay " & _
            '          ", iscontact_previous " & _
            '          ", contact_person " & _
            '          ", contact_no " & _
            '          ", memo " & _
            '          ", leave_reason " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", voidreason" & _
            '         ", loginemployeeunkid " & _
            '         ", voidloginemployeeunkid " & _
            '        ") VALUES (" & _
            '          "  @employeeunkid " & _
            '          ", @company " & _
            '          ", @address " & _
            '          ", @jobunkid " & _
            '          ", @start_date " & _
            '          ", @end_date " & _
            '          ", @supervisor " & _
            '          ", @remark " & _
            '          ", @last_pay " & _
            '          ", @iscontact_previous " & _
            '          ", @contact_person " & _
            '          ", @contact_no " & _
            '          ", @memo " & _
            '          ", @leave_reason " & _
            '          ", @userunkid " & _
            '          ", @isvoid " & _
            '          ", @voiduserunkid " & _
            '          ", @voiddatetime " & _
            '          ", @voidreason" & _
            '         ", @loginemployeeunkid " & _
            '         ", @voidloginemployeeunkid " & _
            '"); SELECT @@identity"

            strQ = "INSERT INTO hremp_experience_tran ( " & _
                      "  employeeunkid " & _
                      ", company " & _
                      ", address " & _
                      ", start_date " & _
                      ", end_date " & _
                      ", supervisor " & _
                      ", remark " & _
                      ", last_pay " & _
                      ", iscontact_previous " & _
                      ", contact_person " & _
                      ", contact_no " & _
                      ", memo " & _
                      ", leave_reason " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                     ", loginemployeeunkid " & _
                     ", voidloginemployeeunkid " & _
                      ", old_job " & _
                      ", otherbenefit " & _
                      ", currencysign " & _
                      ", created_date " & _
                      ", isgovjob " & _
                    ") VALUES (" & _
                      "  @employeeunkid " & _
                      ", @company " & _
                      ", @address " & _
                      ", @start_date " & _
                      ", @end_date " & _
                      ", @supervisor " & _
                      ", @remark " & _
                      ", @last_pay " & _
                      ", @iscontact_previous " & _
                      ", @contact_person " & _
                      ", @contact_no " & _
                      ", @memo " & _
                      ", @leave_reason " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                     ", @loginemployeeunkid " & _
                     ", @voidloginemployeeunkid " & _
                     ", @oldjob " & _
                     ", @otherbenefit " & _
                     ", @currencysign " & _
                     ", @created_date " & _
                     ", @isgovjob " & _
            "); SELECT @@identity"
            'S.SANDEEP |24-JUN-2023| -- START {isgovjob} -- END
            'Hemant (11 Apr 2022) --[created_date]
            'Anjan (02 Mar 2012)-End 

            'Pinkal (12-Oct-2011) -- End


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintExperiencetranunkid = dsList.Tables(0).Rows(0).Item(0)



            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            AddEditAttachment(objDataOperation, mdtTable)
            'Gajanan [5-Dec-2019] -- End

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If mstrPreviousBenefitPlan.Length > 0 Then
                Call InsertPreviousBenefit(objDataOperation, 1)
            Else
                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremp_experience_tran", "experiencetranunkid", mintExperiencetranunkid, "hrprevious_benefit_tran", "previousbenefittranunkid", 0, 1, 0) = False Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                clsCommonATLog._WebClientIP = mstrWebClientIP
                clsCommonATLog._WebFormName = mstrWebFormName
                clsCommonATLog._WebHostName = mstrWebHostName

                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremp_experience_tran", "experiencetranunkid", mintExperiencetranunkid, "hrprevious_benefit_tran", "previousbenefittranunkid", 0, 1, 0, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [ 27 APRIL 2012 ] -- END
            End If

            'Pinkal (12-Oct-2011) -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            ''Pinkal (12-Oct-2011) -- Start
            ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'objDataOperation.ReleaseTransaction(True)
            ''Pinkal (12-Oct-2011) -- End
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [17-DEC-2018] -- End
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            ''Pinkal (12-Oct-2011) -- Start
            ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'objDataOperation.ReleaseTransaction(False)
            ''Pinkal (12-Oct-2011) -- End
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If
            'Gajanan [17-DEC-2018] -- End

            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [17-DEC-2018] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hremp_experience_tran) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal mdtTable As DataTable = Nothing) As Boolean
        'S.SANDEEP [ 27 FEB 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'If isExist(mintEmployeeunkid, mstrCompany, mintExperiencetranunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Particular Job History is already entered for selected employee. Please enter new information.")
        '    Return False
        'End If

        'Anjan (17 Apr 2012)-Start
        'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
        'If isExist(mintEmployeeunkid, mstrCompany, mdtEnd_Date.Date,  mintExperiencetranunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Particular Job History is already entered for selected employee. Please enter new information.")
        '    Return False
        'End If

        If isExist(mintEmployeeunkid, mstrCompany, mdtEnd_Date.Date, mdtStart_Date.Date, mstrJob, mintExperiencetranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Particular Job History is already entered for selected employee. Please enter new information.")
            Return False
        End If
        'Anjan (17 Apr 2012)-End 


        'S.SANDEEP [ 27 FEB 2012 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Pinkal (12-Oct-2011) -- End


        Try
            objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExperiencetranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCompany.ToString)
            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            If mdtStart_Date = Nothing Then
                objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date)
            End If

            If mdtEnd_Date = Nothing Then
                objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date)
            End If
            objDataOperation.AddParameter("@supervisor", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSupervisor.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)

            'Anjan (11 May 2011)-Start
            'objDataOperation.AddParameter("@last_pay", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecLast_Pay.ToString)
            objDataOperation.AddParameter("@last_pay", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLast_Pay)
            'Anjan (11 May 2011)-End 



            objDataOperation.AddParameter("@iscontact_previous", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscontact_Previous.ToString)
            objDataOperation.AddParameter("@contact_person", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_Person.ToString)
            objDataOperation.AddParameter("@contact_no", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrContact_No.ToString)
            objDataOperation.AddParameter("@memo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMemo.ToString)
            objDataOperation.AddParameter("@leave_reason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLeave_Reason.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginEmployeeunkid.ToString)

            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            objDataOperation.AddParameter("@oldjob", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrJob.ToString)
            objDataOperation.AddParameter("@otherbenefit", SqlDbType.NVarChar, 1000, mstrOtherBenefit.ToString)
            objDataOperation.AddParameter("@currencysign", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCurrencySign.ToString)
            'Anjan (02 Mar 2012)-End 

            'S.SANDEEP |24-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : Sprint 2023-13
            objDataOperation.AddParameter("@isgovjob", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsGovJob)
            'S.SANDEEP |24-JUN-2023| -- END

            'strQ = "UPDATE hremp_experience_tran SET " & _
            '         "  employeeunkid = @employeeunkid" & _
            '         ", company = @company" & _
            '         ", address = @address" & _
            '         ", jobunkid = @jobunkid" & _
            '         ", start_date = @start_date" & _
            '         ", end_date = @end_date" & _
            '         ", supervisor = @supervisor" & _
            '         ", remark = @remark" & _
            '         ", last_pay = @last_pay" & _
            '         ", iscontact_previous = @iscontact_previous" & _
            '         ", contact_person = @contact_person" & _
            '         ", contact_no = @contact_no" & _
            '         ", memo = @memo" & _
            '         ", leave_reason = @leave_reason" & _
            '         ", userunkid = @userunkid" & _
            '         ", isvoid = @isvoid" & _
            '         ", voiduserunkid = @voiduserunkid" & _
            '         ", voiddatetime = @voiddatetime" & _
            '         ", voidreason = @voidreason " & _
            '       "WHERE experiencetranunkid = @experiencetranunkid "


            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'strQ = "UPDATE hremp_experience_tran SET " & _
            '          "  employeeunkid = @employeeunkid" & _
            '          ", company = @company" & _
            '          ", address = @address" & _
            '          ", jobunkid = @jobunkid" & _
            '          ", start_date = @start_date" & _
            '          ", end_date = @end_date" & _
            '          ", supervisor = @supervisor" & _
            '          ", remark = @remark" & _
            '          ", last_pay = @last_pay" & _
            '          ", iscontact_previous = @iscontact_previous" & _
            '          ", contact_person = @contact_person" & _
            '          ", contact_no = @contact_no" & _
            '          ", memo = @memo" & _
            '          ", leave_reason = @leave_reason" & _
            '          ", userunkid = @userunkid" & _
            '          ", isvoid = @isvoid" & _
            '          ", voiduserunkid = @voiduserunkid" & _
            '          ", voiddatetime = @voiddatetime" & _
            '          ", voidreason = @voidreason " & _
            '          ", loginemployeeunkid = @loginemployeeunkid " & _
            '          ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
            '        "WHERE experiencetranunkid = @experiencetranunkid "

            strQ = "UPDATE hremp_experience_tran SET " & _
                      "  employeeunkid = @employeeunkid" & _
                      ", company = @company" & _
                      ", address = @address" & _
                      ", start_date = @start_date" & _
                      ", end_date = @end_date" & _
                      ", supervisor = @supervisor" & _
                      ", remark = @remark" & _
                      ", last_pay = @last_pay" & _
                      ", iscontact_previous = @iscontact_previous" & _
                      ", contact_person = @contact_person" & _
                      ", contact_no = @contact_no" & _
                      ", memo = @memo" & _
                      ", leave_reason = @leave_reason" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                      ", loginemployeeunkid = @loginemployeeunkid " & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                      ", old_job = @oldjob  " & _
                      ", otherbenefit =  @otherbenefit" & _
                      ", currencysign =  @currencysign " & _
                      ", isgovjob = @isgovjob " & _
                    "WHERE experiencetranunkid = @experiencetranunkid "
            'S.SANDEEP |24-JUN-2023| -- START {isgovjob} -- END
            'Anjan (02 Mar 2012)-End 

            'Pinkal (12-Oct-2011) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sandeep [ 14 Aug 2010 ] -- Start
            'If mstrPreviousBenefitPlan.Length > 0 Then
            'Call InsertPreviousBenefit()
            'End If
            'Sandeep [ 14 Aug 2010 ] -- End 

            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            AddEditAttachment(objDataOperation, mdtTable)
            'Gajanan [5-Dec-2019] -- End

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If mstrPreviousBenefitPlan.Length > 0 Then
                If InsertPreviousBenefit(objDataOperation, 2) = True And blnIsInserted = False Then

                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hremp_experience_tran", mintExperiencetranunkid, "experiencetranunkid", 2, objDataOperation) Then

                        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremp_experience_tran", "experiencetranunkid", mintExperiencetranunkid, "hrprevious_benefit_tran", "previousbenefittranunkid", 0, 2, 0) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If

                End If

            Else

                Dim dList As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrprevious_benefit_tran", "experiencetranunkid", mintExperiencetranunkid)

                If dList.Tables(0).Rows.Count > 0 Then

                    DeleteBenefit(objDataOperation, mintExperiencetranunkid, dList)

                Else

                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "hremp_experience_tran", mintExperiencetranunkid, "experiencetranunkid", 2, objDataOperation) Then

                        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremp_experience_tran", "experiencetranunkid", mintExperiencetranunkid, "hrprevious_benefit_tran", "previousbenefittranunkid", 0, 2, 0) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If

                End If
                dList.Dispose()
            End If

            objDataOperation.ReleaseTransaction(True)

            'Pinkal (12-Oct-2011) -- End
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [17-DEC-2018] -- End
            Return True
        Catch ex As Exception


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If

            'Pinkal (12-Oct-2011) -- End
            'Gajanan [17-DEC-2018] -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [17-DEC-2018] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hremp_experience_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByVal xIsDeleteBenefit As Boolean = True) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.



        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Gajanan [17-DEC-2018] -- End

        'Pinkal (12-Oct-2011) -- End



        Try
            'strQ = "UPDATE hremp_experience_tran SET " & _
            '         "  isvoid = @isvoid" & _
            '         ", voiduserunkid = @voiduserunkid" & _
            '         ", voiddatetime = @voiddatetime" & _
            '         ", voidreason = @voidreason " & _
            '        "WHERE experiencetranunkid = @experiencetranunkid "


            strQ = "UPDATE hremp_experience_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voidreason = @voidreason "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)


            If mintUserunkid > 0 Then
                strQ &= ", voiduserunkid = @voiduserunkid "
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            ElseIf mintVoidloginEmployeeunkid > 0 Then
                strQ &= ", voidloginemployeeunkid = @voidloginemployeeunkid "
                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginEmployeeunkid.ToString)

            End If

            strQ &= " WHERE experiencetranunkid = @experiencetranunkid "


            Call objDataOperation.ExecNonQuery(strQ)


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'Call DeleteBenefit(intUnkid)


            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            'mintExperiencetranunkid = intUnkid
            _Experiencetranunkid = intUnkid
            'Gajanan [5-Dec-2019] -- End

            'Gajanan [5-Dec-2019] -- Start   
            'Enhancement:Worked On ADD Attachment In Bio Data Experience    
            Dim objDocument As New clsScan_Attach_Documents
            If objDocument.DeleteTransacation(_Employeeunkid, enScanAttactRefId.JOBHISTORYS, intUnkid, objDataOperation, _WebFormName) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDocument = Nothing
            'Gajanan [5-Dec-2019] -- End

            strQ = "Select isnull(previousbenefittranunkid,0) previousbenefittranunkid From hrprevious_benefit_tran WHERE experiencetranunkid = @experiencetranunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.
            If xIsDeleteBenefit Then
                'Gajanan [17-DEC-2018] -- End
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dsList.Tables(0).Rows

                    Call DeleteBenefit(objDataOperation, 3, CInt(dr("previousbenefittranunkid")))

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next
                End If
            End If

            mintExperiencetranunkid = 0

            'Pinkal (12-Oct-2011) -- End

            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [17-DEC-2018] -- End
            Return True
        Catch ex As Exception
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(False)
            End If

            'Pinkal (12-Oct-2011) -- End

            'Gajanan [17-DEC-2018] -- End
            'Hemant (01 Sep 2023) -- Start
            'ISSUE(ZRA) : A1X-1250 - Resolve bug when adding/editing/deleting experiences (Old Ui) 
            'Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            'Hemant (01 Sep 2023) -- End
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Gajanan [17-DEC-2018] -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    '''S.SANDEEP [ 27 FEB 2012 ] -- START
    '''ENHANCEMENT : TRA CHANGES
    '''Public Function isExist(ByVal intEmpId As Integer, ByVal strCompanyName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    Public Function isExist(ByVal intEmpId As Integer, ByVal strCompanyName As String, ByVal dtDate As DateTime, ByVal dtEDate As DateTime, ByVal StrJob As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'Public Function isExist(ByVal intEmpId As Integer, ByVal strCompanyName As String, ByVal dtDate As DateTime, Optional ByVal intUnkid As Integer = -1) As Boolean
        'S.SANDEEP [ 27 FEB 2012 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.
        objDataOperation = New clsDataOperation
        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Gajanan [17-DEC-2018] -- End

        Try
            'strQ = "SELECT " & _
            '          "  experiencetranunkid " & _
            '          ", employeeunkid " & _
            '          ", company " & _
            '          ", address " & _
            '          ", jobunkid " & _
            '          ", start_date " & _
            '          ", end_date " & _
            '          ", supervisor " & _
            '          ", remark " & _
            '          ", last_pay " & _
            '          ", iscontact_previous " & _
            '          ", contact_person " & _
            '          ", contact_no " & _
            '          ", memo " & _
            '          ", leave_reason " & _
            '          ", userunkid " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", voidreason " & _
            '        "FROM hremp_experience_tran " & _
            '        "WHERE employeeunkid = @EmpId " & _
            '        "AND company = @CompName " & _
            '        "AND isvoid = 0 "


            strQ = "SELECT " & _
                      "  experiencetranunkid " & _
                      ", employeeunkid " & _
                      ", company " & _
                      ", address " & _
                      ", start_date " & _
                      ", end_date " & _
                      ", supervisor " & _
                      ", remark " & _
                      ", last_pay " & _
                      ", iscontact_previous " & _
                      ", contact_person " & _
                      ", contact_no " & _
                      ", memo " & _
                      ", leave_reason " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                   ", loginemployeeunkid " & _
                      ", created_date " & _
                      ", isgovjob " & _
                    "FROM hremp_experience_tran " & _
                    "WHERE employeeunkid = @EmpId " & _
                    "AND company = @CompName " & _
                    "AND old_job = @Job " & _
                    "AND isvoid = 0 "
            'S.SANDEEP |24-JUN-2023| -- START {isgovjob} -- END
            'Hemant (11 Apr 2022) -- [created_date]
            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'If dtDate <> Nothing Then
            '    strQ &= " AND CONVERT(CHAR(8),end_date,112) > @Date "
            'End If



            'Gajanan [17-April-2019] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.

            'If dtEDate <> Nothing Then
            '    strQ &= " AND CONVERT(CHAR(8),start_date,112) = @EDate "
            'End If

            If dtDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),start_date,112) > @Date "
            End If
            'Gajanan [17-April-2019] -- End



            'Anjan (17 Apr 2012)-End 


            If intUnkid > 0 Then
                strQ &= " AND experiencetranunkid <> @experiencetranunkid"
            End If

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@CompName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCompanyName)
            objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            objDataOperation.AddParameter("@Job", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrJob)
            'Anjan (17 Apr 2012)-End 


            'S.SANDEEP [ 27 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Anjan (17 Apr 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            'objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate))
            If dtDate <> Nothing Then
                objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate))
            End If

            If dtEDate <> Nothing Then
                objDataOperation.AddParameter("@EDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEDate))
            End If
            'Anjan (17 Apr 2012)-End 

            'S.SANDEEP [ 27 FEB 2012 ] -- END



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP [ 07 NOV 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function GetExperienceData_Export() As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'Gajanan (24 Nov 2018) -- Start
            'Enhancement : Import template column headers should read from language set for 
            'users (custom 1) and columns' date formats for importation should be clearly known
            'Replace Column Name With Getmessage

            'Anjan (02 Mar 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request

            'Anjan (02 Mar 2012)-End 

            'StrQ = "SELECT " & _
            '             " ISNULL(hremployee_master.employeecode,'') AS ECODE " & _
            '             ",ISNULL(hremployee_master.firstname,'') AS FIRSTNAME " & _
            '             ",ISNULL(hremployee_master.surname,'') AS SURNAME " & _
            '             ",ISNULL(hremp_experience_tran.company,'') AS COMPANY " & _
            '             ",ISNULL(hrjob_master.job_name,'') AS JOB " & _
            '             ",hremp_experience_tran.start_date AS START_DATE " & _
            '             ",hremp_experience_tran.end_date AS END_DATE " & _
            '             ",hremp_experience_tran.leave_reason AS LEAVING " & _
            '            "FROM hremp_experience_tran " & _
            '             "JOIN hrjob_master ON hremp_experience_tran.jobunkid = hrjob_master.jobunkid " & _
            '             "JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '        "WHERE isvoid = 0 "
            StrQ = "SELECT " & _
                         " ISNULL(hremployee_master.employeecode,'') AS  [" & Language.getMessage(mstrModuleName, 2, "EMPLOYEE_CODE") & "]" & _
                         ",ISNULL(hremployee_master.firstname,'') AS  [" & Language.getMessage(mstrModuleName, 3, "FIRSTNAME") & "]" & _
                         ",ISNULL(hremployee_master.surname,'') AS  [" & Language.getMessage(mstrModuleName, 4, "SURNAME") & "]" & _
                         ",ISNULL(hremp_experience_tran.company,'') AS  [" & Language.getMessage(mstrModuleName, 5, "COMPANY") & "]" & _
                         ",hremp_experience_tran.start_date AS  [" & Language.getMessage(mstrModuleName, 6, "START_DATE") & "]" & _
                         ",hremp_experience_tran.end_date AS  [" & Language.getMessage(mstrModuleName, 7, "END_DATE") & "]" & _
                         ",hremp_experience_tran.leave_reason AS  [" & Language.getMessage(mstrModuleName, 8, "LEAVING_REASON") & "]" & _
                         ",hremp_experience_tran.old_job AS  [" & Language.getMessage(mstrModuleName, 9, "JOB") & "]" & _
                    "FROM hremp_experience_tran " & _
                         "JOIN hremployee_master ON hremp_experience_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    "WHERE isvoid = 0 "
            'S.SANDEEP [14-JUN-2018] -- START {NMB} [ECODE = EMPLOYEE_CODE & LEAVING = LEAVING_REASON] -- END
            'Gajanan (24 Nov 2018) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetExperienceData_Export ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetExperienceDataUnkid(ByVal intEmpId As Integer, ByVal strCompanyName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                   " experiencetranunkid " & _
                   "FROM hremp_experience_tran " & _
                   "WHERE employeeunkid = @EmpId AND LTRIM(RTRIM(company)) LIKE LTRIM(RTRIM(@Company)) AND isvoid = 0 "

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@Company", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCompanyName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("experiencetranunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetExperienceDataUnkid ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 07 NOV 2011 ] -- END

#Region " Private Functions "
    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    'Private Function InsertPreviousBenefit() As Boolean
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim strBenefit_value() As String = Nothing
    '    Try
    '        Call DeleteBenefit()

    '        If mstrPreviousBenefitPlan.Length > 0 Then
    '            strBenefit_value = mstrPreviousBenefitPlan.Split(",")
    '            For i As Integer = 0 To strBenefit_value.Length - 1
    '                objDataOperation.ClearParameters()

    '                objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExperiencetranunkid.ToString)
    '                objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strBenefit_value(i).ToString)

    '                StrQ = "INSERT INTO hrprevious_benefit_tran ( " & _
    '                            "  experiencetranunkid " & _
    '                            ", benefitplanunkid" & _
    '                          ") VALUES (" & _
    '                            "  @experiencetranunkid " & _
    '                            ", @benefitplanunkid" & _
    '                          "); SELECT @@identity"

    '                objDataOperation.ExecNonQuery(StrQ)

    '               If objDataOperation.ErrorMessage <> "" Then
    '                   exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                Next
    '            End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "InsertPreviousBenefit", mstrModuleName)
    '    End Try
    'End Function



    Private Function InsertPreviousBenefit(ByVal objDataOperation As clsDataOperation, ByVal intParentAuditTypeID As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim strBenefit_value() As String = Nothing
        Try


            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            blnIsInserted = False
            Dim dList As DataSet = clsCommonATLog.GetChildList(objDataOperation, "hrprevious_benefit_tran", "experiencetranunkid", mintExperiencetranunkid)
            Dim dtDel() As DataRow = Nothing
            If mstrPreviousBenefitPlan.Trim.Length > 0 AndAlso dList.Tables(0).Rows.Count > 0 Then
                dtDel = dList.Tables(0).Select("benefitplanunkid NOT IN (" & mstrPreviousBenefitPlan & ") AND experiencetranunkid = " & mintExperiencetranunkid)
                If dtDel.Length > 0 Then
                    For k As Integer = 0 To dtDel.Length - 1

                        'STRAT FOR DELETE ALL RECORDS OF THE PARTICUALR EMPLOYEE IN EMPLOYEE BENEFIT TABLE
                        DeleteBenefit(objDataOperation, intParentAuditTypeID, CInt(dtDel(k)("previousbenefittranunkid")))
                        'END FOR DELETE ALL RECORDS OF THE PARTICUALR EMPLOYEE IN EMPLOYEE BENEFIT TABLE

                        dList.Tables(0).Rows.Remove(dtDel(k))
                    Next
                    blnIsInserted = True
                    dList.AcceptChanges()
                End If
            End If


            If mstrPreviousBenefitPlan.Length > 0 Then

                strBenefit_value = mstrPreviousBenefitPlan.Split(",")
                For i As Integer = 0 To strBenefit_value.Length - 1


                    'Pinkal (12-Oct-2011) -- Start
                    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                    If dList IsNot Nothing AndAlso dList.Tables(0).Rows.Count > 0 Then
                        dtDel = dList.Tables(0).Select("benefitplanunkid = " & CInt(strBenefit_value.GetValue(i)))
                        If dtDel.Length > 0 Then Continue For
                    End If

                    'Pinkal (12-Oct-2011) -- End


                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExperiencetranunkid.ToString)
                    objDataOperation.AddParameter("@benefitplanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, strBenefit_value(i).ToString)

                    StrQ = "INSERT INTO hrprevious_benefit_tran ( " & _
                                "  experiencetranunkid " & _
                                ", benefitplanunkid" & _
                              ") VALUES (" & _
                                "  @experiencetranunkid " & _
                                ", @benefitplanunkid" & _
                              "); SELECT @@identity"

                    Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    Dim mintpreviousbenefittranunkid As Integer = 0
                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        mintpreviousbenefittranunkid = dsList.Tables(0).Rows(0)(0)
                    End If

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremp_experience_tran", "experiencetranunkid", mintExperiencetranunkid, "hrprevious_benefit_tran", "previousbenefittranunkid", mintpreviousbenefittranunkid, intParentAuditTypeID, 1, , mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    blnIsInserted = True

                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertPreviousBenefit", mstrModuleName)
            Return blnIsInserted
        End Try
        Return True
    End Function

    'Pinkal (12-Oct-2011) -- End
    'Pinkal (12-Oct-2011) -- Start
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    'Private Function DeleteBenefit(Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception
    '    Try
    '        StrQ = "DELETE FROM hrprevious_benefit_tran " & _
    '                    "WHERE experiencetranunkid = @experiencetranunkid "
    '
    '        objDataOperation.ClearParameters()
    '
    '        If intUnkid <> -1 Then
    '            objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
    '        Else
    '            objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExperiencetranunkid)
    '        End If
    '
    '        Call objDataOperation.ExecNonQuery(StrQ)

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            Return True

    '        Catch ex As Exception
    '            DisplayError.Show("-1", ex.Message, "DeleteAllergies", mstrModuleName)
    '        End Try
    '    End Function

    Private Function DeleteBenefit(ByVal xDataOpr As clsDataOperation, ByVal intParentAuditTypeId As Integer, ByVal intUnkid As Integer) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.



        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Gajanan [17-DEC-2018] -- End
        Try



            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremp_experience_tran", "experiencetranunkid", mintExperiencetranunkid, "hrprevious_benefit_tran", "previousbenefittranunkid", intUnkid, intParentAuditTypeId, 3) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hremp_experience_tran", "experiencetranunkid", mintExperiencetranunkid, "hrprevious_benefit_tran", "previousbenefittranunkid", intUnkid, intParentAuditTypeId, 3, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END


            StrQ = "DELETE FROM hrprevious_benefit_tran " & _
                        "WHERE previousbenefittranunkid = @previousbenefittranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@previousbenefittranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            Call objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [17-DEC-2018] -- End
            Return True

        Catch ex As Exception
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.



            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If

            DisplayError.Show("-1", ex.Message, "DeleteBenefit", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteBenefit; Module Name: " & mstrModuleName)
            Return False
            'Gajanan [17-DEC-2018] -- End

        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Private Function DeleteBenefit(ByVal xDataOpr As clsDataOperation, ByVal intUnkid As Integer, ByVal dsList As DataSet) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception

        'Gajanan [17-DEC-2018] -- Start
        'Enhancement - Implementing Employee Approver Flow On Employee Data.



        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()
        'Gajanan [17-DEC-2018] -- End
        Try

            'StrQ = "Select isnull(previousbenefittranunkid,0) previousbenefittranunkid From hrprevious_benefit_tran WHERE experiencetranunkid = @experiencetranunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@experiencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dsList.Tables(0).Rows

                    Call DeleteBenefit(objDataOperation, 2, CInt(dr("previousbenefittranunkid")))

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If
            'Gajanan [17-DEC-2018] -- Start
            'Enhancement - Implementing Employee Approver Flow On Employee Data.


            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            'Gajanan [17-DEC-2018] -- End
            Return True

        Catch ex As Exception

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: DeleteBenefit; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'Pinkal (12-Oct-2011) -- End

    Public Function GetPreviousBenefit() As String
        Dim strPrevious As String = String.Empty
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If

            StrQ = "SELECT " & _
                    "  previousbenefittranunkid " & _
                    ", experiencetranunkid " & _
                    ", benefitplanunkid " & _
                   "FROM hrprevious_benefit_tran " & _
                   "WHERE experiencetranunkid =" & mintExperiencetranunkid

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dtRow As DataRow In dsList.Tables(0).Rows
                If strPrevious.Length <= 0 Then
                    strPrevious = dtRow.Item("benefitplanunkid").ToString
                Else
                    strPrevious &= "," & dtRow.Item("benefitplanunkid").ToString
                End If
            Next

            Return strPrevious

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetPreviousBenefit", mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Gajanan [5-Dec-2019] -- Start   
    'Enhancement:Worked On ADD Attachment In Bio Data Experience    
    Public Function AddEditAttachment(ByVal xDataOpr As clsDataOperation, ByVal mdtTable As DataTable) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            If mdtTable IsNot Nothing Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

                Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.JOBHISTORYS).Tables(0).Rows(0)("Name").ToString
                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"

                Dim dr As DataRow
                For Each drow As DataRow In mdtTable.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    dr("transactionunkid") = mintExperiencetranunkid
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    dr("userunkid") = mintUserunkid
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")
                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(xDataOpr)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objDocument = Nothing
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AddEditAttachment; Module Name: " & mstrModuleName)
            Return False
        End Try


    End Function
    'Gajanan [5-Dec-2019] -- End

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Particular Job History is already entered for selected employee. Please enter new information.")
			Language.setMessage(mstrModuleName, 2, "EMPLOYEE_CODE")
			Language.setMessage(mstrModuleName, 3, "FIRSTNAME")
			Language.setMessage(mstrModuleName, 4, "SURNAME")
			Language.setMessage(mstrModuleName, 5, "COMPANY")
			Language.setMessage(mstrModuleName, 6, "START_DATE")
			Language.setMessage(mstrModuleName, 7, "END_DATE")
			Language.setMessage(mstrModuleName, 8, "LEAVING_REASON")
			Language.setMessage(mstrModuleName, 9, "JOB")

        Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class