﻿Option Strict On
'************************************************************************************************************************************
'Class Name : clsempapproverlevel_master.vb
'Purpose    : All Employee Approver Level Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :14/06/2018
'Written By :Pinkal
'Modified   :
'Last Message Index = 4
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsempapproverlevel_master


    Private Shared ReadOnly mstrModuleName As String = "clsempapproverlevel_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintEmpAppLevelunkid As Integer
    Private mstrEmpAppLevelcode As String = String.Empty
    Private mstrEmpAppLevelname As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mintPriority As Integer
    Private mstrEmpAppLevelname1 As String = String.Empty
    Private mstrEmpAppLevelname2 As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0
    Private mdtAuditDatetime As DateTime = Nothing
    Private mintApprovertypeid As enEmpApproverType

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set EmpAppLevelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EmpAppLevelunkid(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintEmpAppLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpAppLevelunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set EmpAppLevelcode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EmpAppLevelcode() As String
        Get
            Return mstrEmpAppLevelcode
        End Get
        Set(ByVal value As String)
            mstrEmpAppLevelcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set EmpAppLevelname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EmpAppLevelname() As String
        Get
            Return mstrEmpAppLevelname
        End Get
        Set(ByVal value As String)
            mstrEmpAppLevelname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set EmpAppLevelname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EmpAppLevelname1() As String
        Get
            Return mstrEmpAppLevelname1
        End Get
        Set(ByVal value As String)
            mstrEmpAppLevelname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set EmpAppLevelname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EmpAppLevelname2() As String
        Get
            Return mstrEmpAppLevelname2
        End Get
        Set(ByVal value As String)
            mstrEmpAppLevelname2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditDatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditDatetime() As DateTime
        Get
            Return mdtAuditDatetime
        End Get
        Set(ByVal value As DateTime)
            mdtAuditDatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvertypeid() As enEmpApproverType
        Get
            Return mintApprovertypeid
        End Get
        Set(ByVal value As enEmpApproverType)
            mintApprovertypeid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                   "  empapplevelunkid " & _
                   ", empapplevelcode " & _
                   ", empapplevelName " & _
                   ", priority " & _
                   ", isactive " & _
                   ", empapplevelName1 " & _
                   ", empapplevelName2 " & _
                   ", approvertypeid " & _
                   " FROM hrempapproverlevel_master " & _
                   " WHERE empapplevelunkid = @empapplevelunkid "

            objDataOperation.AddParameter("@empapplevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpAppLevelunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEmpAppLevelunkid = CInt(dtRow.Item("empapplevelunkid"))
                mstrEmpAppLevelcode = dtRow.Item("empapplevelcode").ToString
                mstrEmpAppLevelname = dtRow.Item("empapplevelName").ToString
                If dtRow.Item("priority") IsNot DBNull.Value Then mintPriority = CInt(dtRow.Item("priority"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrEmpAppLevelname1 = dtRow.Item("empapplevelName1").ToString
                mstrEmpAppLevelname2 = dtRow.Item("empapplevelName2").ToString
                mintApprovertypeid = CType(CInt(dtRow.Item("approvertypeid")), enEmpApproverType)
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal eApproverType As enEmpApproverType, ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  empapplevelunkid " & _
                      ", empapplevelcode " & _
                      ", empapplevelName " & _
                      ", priority " & _
                      ", isactive " & _
                      ", empapplevelName1 " & _
                      ", empapplevelName2 " & _
                      ", approvertypeid " & _
                     " FROM hrempapproverlevel_master " & _
                     " WHERE isactive = 1 AND approvertypeid = @approvertypeid "

            objDataOperation.AddParameter("@approvertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(eApproverType))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvapproverlevel_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintApprovertypeid, mstrEmpAppLevelcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist(mintApprovertypeid, "", mstrEmpAppLevelname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintApprovertypeid, mintPriority) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@empapplevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAppLevelcode.ToString)
            objDataOperation.AddParameter("@empapplevelName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAppLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@empapplevelName1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAppLevelname1.ToString)
            objDataOperation.AddParameter("@empapplevelName2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAppLevelname2.ToString)
            objDataOperation.AddParameter("@approvertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertypeid)

            strQ = "INSERT INTO hrempapproverlevel_master ( " & _
                      "  empapplevelcode  " & _
                      ", empapplevelName  " & _
                      ", priority " & _
                      ", isactive " & _
                      ", empapplevelName1  " & _
                      ", empapplevelName2 " & _
                      ", approvertypeid " & _
                    ") VALUES (" & _
                      "  @empapplevelcode  " & _
                      ", @empapplevelName " & _
                      ", @priority " & _
                      ", @isactive " & _
                      ", @empapplevelName1  " & _
                      ", @empapplevelName2 " & _
                      ", @approvertypeid " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmpAppLevelunkid = CInt(dsList.Tables(0).Rows(0).Item(0))


            If InsertAuditTrailForEmpApproverLevel(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvapproverlevel_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mintApprovertypeid, mstrEmpAppLevelcode, "", mintEmpAppLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist(mintApprovertypeid, "", mstrEmpAppLevelname, mintEmpAppLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintApprovertypeid, mintPriority, mintEmpAppLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@empapplevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpAppLevelunkid.ToString)
            objDataOperation.AddParameter("@empapplevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAppLevelcode.ToString)
            objDataOperation.AddParameter("@empapplevelName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAppLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@empapplevelName1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAppLevelname1.ToString)
            objDataOperation.AddParameter("@empapplevelName2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAppLevelname2.ToString)
            objDataOperation.AddParameter("@approvertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertypeid)

            strQ = "UPDATE hrempapproverlevel_master  SET " & _
                      "  empapplevelcode  = @empapplevelcode " & _
                      ", empapplevelName  = @empapplevelName " & _
                      ", priority  = @priority  " & _
                      ", isactive = @isactive" & _
                      ", empapplevelName1  = @empapplevelName1" & _
                      ", empapplevelName2 = @empapplevelName2 " & _
                      ", approvertypeid = @approvertypeid " & _
                      " WHERE empapplevelunkid  = @empapplevelunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If InsertAuditTrailForEmpApproverLevel(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvapproverlevel_master) </purpose>
    ''' 
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = " Update hrempapproverlevel_master set isactive = 0 " & _
                      " WHERE empapplevelunkid  = @empapplevelunkid  "

            objDataOperation.AddParameter("@empapplevelunkid ", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _EmpAppLevelunkid(objDataOperation) = intUnkid

            If InsertAuditTrailForEmpApproverLevel(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "select isnull(empapplevelunkid,0) from hremp_appusermapping WHERE empapplevelunkid = @empapplevelunkid"
            objDataOperation.AddParameter("@empapplevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intApprovertypeid As Integer, Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  empapplevelunkid " & _
                      ", empapplevelcode " & _
                      ", empapplevelName " & _
                      ", priority " & _
                      ", isactive " & _
                      ", empapplevelName1 " & _
                      ", empapplevelName2 " & _
                      ", approvertypeid " & _
                     " FROM hrempapproverlevel_master " & _
                     " WHERE 1=1 AND approvertypeid = @approvertypeid "

            If strCode.Length > 0 Then
                strQ &= " AND empapplevelcode = @empapplevelcode "
            End If
            If strName.Length > 0 Then
                strQ &= " AND empapplevelName = @empapplevelName "
            End If

            If intUnkid > 0 Then
                strQ &= " AND empapplevelunkid <> @empapplevelunkid"
            End If

            objDataOperation.AddParameter("@approvertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApprovertypeid)
            objDataOperation.AddParameter("@empapplevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@empapplevelName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@empapplevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isPriorityExist(ByVal intApprovertypeid As Integer, ByVal mintPriority As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT " & _
                      "  empapplevelunkid " & _
                      ", empapplevelcode " & _
                      ", empapplevelName " & _
                      ", priority " & _
                      ", isactive " & _
                      ", empapplevelName1 " & _
                      ", empapplevelName2 " & _
                     " FROM hrempapproverlevel_master " & _
                     " WHERE priority = @priority AND approvertypeid = @approvertypeid AND isactive = 1 "

            If intUnkid > 0 Then
                strQ &= " AND empapplevelunkid <> @empapplevelunkid"
            End If

            objDataOperation.AddParameter("@approvertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApprovertypeid)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
            objDataOperation.AddParameter("@empapplevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPriorityExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(ByVal intApprovertypeid As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as empapplevelunkid,' ' as code,  ' ' +  @name  as name, 0 AS approvertypeid UNION "
            End If
            strQ &= "SELECT empapplevelunkid, empapplevelcode as code, empapplevelName as name, approvertypeid AS approvertypeid FROM hrempapproverlevel_master where isactive = 1 AND approvertypeid = @approvertypeid ORDER BY name "

            objDataOperation.AddParameter("@approvertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApprovertypeid)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(CStr(-1), ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetApproverLevelUnkId(ByVal intApprovertypeid As Integer, ByVal strLevelName As String, Optional ByVal strLevelCode As String = "") As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "Select isnull(levelunkid,0) levelunkid from lvapproverlevel_master" & _
                   " where 1 = 1 AND isactive = 1 AND approvertypeid = @approvertypeid "

            objDataOperation.AddParameter("@approvertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApprovertypeid)

            If strLevelName <> "" And strLevelCode = "" Then

                strQ &= " AND levelname = @levelname"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLevelName)

            ElseIf strLevelCode <> "" And strLevelName = "" Then

                strQ &= " AND levelcode = @levelcode"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLevelCode)

            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return CInt(dt("levelunkid"))
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverLevelUnkId", mstrModuleName)
        End Try
        Return -1
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrailForEmpApproverLevel(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = " INSERT INTO athrempapproverlevel_master ( " & _
                      "  empapplevelunkid " & _
                      ", empapplevelcode " & _
                      ", empapplevelName " & _
                      ", priority " & _
                      ", empapplevelName1 " & _
                      ", empapplevelName2 " & _
                      ", approvertypeid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip" & _
                      ", hostname" & _
                      ", form_name " & _
                      ", isweb " & _
                      " ) VALUES (" & _
                      "  @empapplevelunkid " & _
                      ", @empapplevelcode " & _
                      ", @empapplevelName " & _
                      ", @priority " & _
                      ", @empapplevelName1 " & _
                      ", @empapplevelName2 " & _
                      ", @approvertypeid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", @auditdatetime " & _
                      ", @ip" & _
                      ", @hostname" & _
                      ", @form_name " & _
                      ", @isweb " & _
                      " ); "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@empapplevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpAppLevelunkid)
            objDataOperation.AddParameter("@empapplevelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAppLevelcode.ToString)
            objDataOperation.AddParameter("@empapplevelName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAppLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@empapplevelName1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAppLevelname1.ToString)
            objDataOperation.AddParameter("@empapplevelName2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmpAppLevelname2.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDatetime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)
            objDataOperation.AddParameter("@approvertypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovertypeid)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForEmpApproverLevel; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    Public Function GetLowPriority(ByVal intApprovertypeid As Integer) As Integer
        Try
            Dim dsList As New DataSet
            dsList = GetList(CType(intApprovertypeid, enEmpApproverType), "List") : Dim intPriority As Integer = 0
            intPriority = CInt(dsList.Tables(0).Compute("MIN(priority)", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLowPriority; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function GetMaxPriority(ByVal intApprovertypeid As Integer) As Integer
        Try
            Dim dsList As New DataSet
            dsList = GetList(CType(intApprovertypeid, enEmpApproverType), "List") : Dim intPriority As Integer = 0
            intPriority = CInt(dsList.Tables(0).Compute("MAX(priority)", ""))
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetMaxPriority; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Language.setMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Language.setMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Language.setMessage(mstrModuleName, 4, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class