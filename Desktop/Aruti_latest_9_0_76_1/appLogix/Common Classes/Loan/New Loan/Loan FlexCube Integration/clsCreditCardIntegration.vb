﻿'************************************************************************************************************************************
'Class Name : clsCreditCardIntegration.vb
'Purpose    :
'Date       :14-Mar-2024
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsCreditCardIntegration
    Private Const mstrModuleName = "clsCreditCardIntegration"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mstrId As String = String.Empty
    Private mstrProgramname As String = String.Empty
    Private mstrBranchname As String = String.Empty
    Private mstrBal_Category As String = String.Empty
    Private mstrClientidname As String = String.Empty
    Private mstrCbsclientidname As String = String.Empty
    Private mdtWalletopeningdate As Date
    Private mstrWalletno As String = String.Empty
    Private mstrWalletusage As String = String.Empty
    Private mstrWalletpromocodedesc As String = String.Empty
    Private mstrWalletcurrency As String = String.Empty
    Private mstrWalletpriority As String = String.Empty
    Private mstrWalletadminstatus As String = String.Empty
    Private mdtWalletadminstatusdate As Date
    Private mstrWalletauthflag As String = String.Empty
    Private msinWalletbalance As Single
    Private msinOpeningbalance As Single
    Private msinPostedcredit As Single
    Private msinPosteddebit As Single
    Private msinClosingbalance As Single
    Private msinUnpostedauthorization As Single
    Private msinUnpostedtransaction As Single
    Private msinUnpostedcredit As Single
    Private mdtDormancydate As Date
    Private mstrDormancyreason As String = String.Empty
    Private mstrCreditplan As String = String.Empty
    Private mstrBillingcycle As String = String.Empty
    Private mstrWalletunpaidstatus As String = String.Empty
    Private mdtWalletunpaidstatusdate As Date
    Private mstrWalletbalancestatus As String = String.Empty
    Private mdtWalletbalancestatusdate As Date
    Private msinWalletcreditlimit As Single
    Private msinWalletavailablecreditlimit As Single
    Private msinClientcreditlimit As Single
    Private msinClientavailablecreditlimit As Single
    Private mstrUnpaidstring As String = String.Empty
    Private mstrDayspastdue As String = String.Empty
    Private msinLastpaymentamount As Single
    Private mdtLastpaymentdate As Date
    Private mstrInternalclassification As String = String.Empty
    Private mstrExternalclassification As String = String.Empty
    Private msinInternalprovision As Single
    Private msinExternalprovision As Single
    Private mstrRating As String = String.Empty
    Private mstrDeviceno As String = String.Empty
    Private mstrDevicestatus As String = String.Empty
    Private mdtDevicecreationdate As Date
    Private mstrDevicenoalias As String = String.Empty
    Private mstrBillername As String = String.Empty
    Private msinMinimumamtdue As Single
    Private mstrDirectdebitaccno As String = String.Empty
    Private mstrLegalidtype As String = String.Empty
    Private mstrLegalid As String = String.Empty
    Private msinOustandingbalwithloan As Single
    Private msinOutstandingbalwithoutloan As Single
    Private mstrCustomdata1 As String = String.Empty
    Private mstrCustomdata2 As String = String.Empty
    Private mstrCustomdata3 As String = String.Empty
    Private mstrCustomdata4 As String = String.Empty
    Private mstrCustomdata5 As String = String.Empty
    Private mstrVipflag As String = String.Empty
#End Region

#Region "Enum"

    Public Enum enCreditCardColumn
        PROGRAM_NAME = 1
        BRANCH_NAME = 2
        BALANCE_CATEGORY = 3
        CLIENT_ID_NAME = 4
        CBS_CLIENTID_NAME = 5
        WALLET_OPENING_DATE = 6
        WALLET_NUMBER = 7
        WALLET_USAGE = 8
        WALLET_PROMO_CODE_DESCRIPTION = 9
        WALLET_CURRENCY = 10
        WALLET_PRIORITY = 11
        WALLET_ADMIN_STATUS = 12
        WALLET_ADMIN_STATUS_DATE = 13
        WALLET_AUTH_FLAG = 14
        WALLET_BALANCE = 15
        OPENING_BALANCE = 16
        POSTED_CREDIT = 17
        POSTED_DEBIT = 18
        CLOSING_BALANCE = 19
        UNPOSTED_AUTHORIZATION = 20
        UNPOSTED_TRANSACTION = 21
        UNPOSTED_CREDIT = 22
        DORMANCY_DATE = 23
        DORMANCY_REASON = 24
        CREDIT_PLAN = 25
        BILLING_CYCLE = 26
        WALLET_UNPAID_STATUS = 27
        WALLET_UNPAID_STATUS_DATE = 28
        WALLET_BALANCE_STATUS = 29
        WALLET_BALANCE_STATUS_DATE = 30
        WALLET_CREDIT_LIMIT = 31
        WALLET_AVAILABLE_CREDIT_LIMIT = 32
        CLIENT_CREDIT_LIMIT = 33
        CLIENT_AVAILABLE_CREDIT_LIMIT = 34
        UNPAID_STRING = 35
        DAYS_PAST_DUE = 36
        LAST_PAYMENT_AMOUNT = 37
        LAST_PAYMENT_DATE = 38
        INTERNAL_CLASSIFICATION = 39
        EXTERNAL_CLASSIFICATION = 40
        INTERNAL_PROVISION = 41
        EXTERNAL_PROVISION = 42
        RATING = 43
        DEVICE_NUMBER = 44
        DEVICE_STATUS = 45
        DATE_OF_DEVICE_CREATION = 46
        DEVICE_NUMBER_ALIAS = 47
        BILLER_NAME = 48
        MINIMUM_AMOUNT_DUE_MAD = 49
        DIRECT_DEBIT_ACCOUNT_NUMBER = 50
        LEGAL_ID_TYPE = 51
        LEGAL_ID = 52
        OUTSTANDING_BALANCE_WITH_LOAN = 53
        OUTSTANDING_BALANCE_WITHOUT_LOAN = 54
        CUSTOM_DATA_1 = 55
        CUSTOM_DATA_2 = 56
        CUSTOM_DATA_3 = 57
        CUSTOM_DATA_4 = 58
        CUSTOM_DATA_5 = 59
        VIP_FLAG = 60
    End Enum

    Public Enum enCreditCardColumnDataType
        NVARCHAR = 1
        NUMERIC = 2
        DATETIME = 3
    End Enum

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set id
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Id() As String
        Get
            Return mstrId
        End Get
        Set(ByVal value As String)
            mstrId = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set programname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Programname() As String
        Get
            Return mstrProgramname
        End Get
        Set(ByVal value As String)
            mstrProgramname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set branchname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Branchname() As String
        Get
            Return mstrBranchname
        End Get
        Set(ByVal value As String)
            mstrBranchname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set bal_category
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Bal_Category() As String
        Get
            Return mstrBal_Category
        End Get
        Set(ByVal value As String)
            mstrBal_Category = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set clientidname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Clientidname() As String
        Get
            Return mstrClientidname
        End Get
        Set(ByVal value As String)
            mstrClientidname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cbsclientidname
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Cbsclientidname() As String
        Get
            Return mstrCbsclientidname
        End Get
        Set(ByVal value As String)
            mstrCbsclientidname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletopeningdate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletopeningdate() As Date
        Get
            Return mdtWalletopeningdate
        End Get
        Set(ByVal value As Date)
            mdtWalletopeningdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletno
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletno() As String
        Get
            Return mstrWalletno
        End Get
        Set(ByVal value As String)
            mstrWalletno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletusage
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletusage() As String
        Get
            Return mstrWalletusage
        End Get
        Set(ByVal value As String)
            mstrWalletusage = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletpromocodedesc
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletpromocodedesc() As String
        Get
            Return mstrWalletpromocodedesc
        End Get
        Set(ByVal value As String)
            mstrWalletpromocodedesc = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletcurrency
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletcurrency() As String
        Get
            Return mstrWalletcurrency
        End Get
        Set(ByVal value As String)
            mstrWalletcurrency = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletpriority
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletpriority() As String
        Get
            Return mstrWalletpriority
        End Get
        Set(ByVal value As String)
            mstrWalletpriority = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletadminstatus
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletadminstatus() As String
        Get
            Return mstrWalletadminstatus
        End Get
        Set(ByVal value As String)
            mstrWalletadminstatus = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletadminstatusdate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletadminstatusdate() As Date
        Get
            Return mdtWalletadminstatusdate
        End Get
        Set(ByVal value As Date)
            mdtWalletadminstatusdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletauthflag
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletauthflag() As String
        Get
            Return mstrWalletauthflag
        End Get
        Set(ByVal value As String)
            mstrWalletauthflag = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletbalance
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletbalance() As Single
        Get
            Return msinWalletbalance
        End Get
        Set(ByVal value As Single)
            msinWalletbalance = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set openingbalance
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Openingbalance() As Single
        Get
            Return msinOpeningbalance
        End Get
        Set(ByVal value As Single)
            msinOpeningbalance = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set postedcredit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Postedcredit() As Single
        Get
            Return msinPostedcredit
        End Get
        Set(ByVal value As Single)
            msinPostedcredit = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set posteddebit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Posteddebit() As Single
        Get
            Return msinPosteddebit
        End Get
        Set(ByVal value As Single)
            msinPosteddebit = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set closingbalance
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Closingbalance() As Single
        Get
            Return msinClosingbalance
        End Get
        Set(ByVal value As Single)
            msinClosingbalance = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unpostedauthorization
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Unpostedauthorization() As Single
        Get
            Return msinUnpostedauthorization
        End Get
        Set(ByVal value As Single)
            msinUnpostedauthorization = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unpostedtransaction
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Unpostedtransaction() As Single
        Get
            Return msinUnpostedtransaction
        End Get
        Set(ByVal value As Single)
            msinUnpostedtransaction = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unpostedcredit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Unpostedcredit() As Single
        Get
            Return msinUnpostedcredit
        End Get
        Set(ByVal value As Single)
            msinUnpostedcredit = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dormancydate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Dormancydate() As Date
        Get
            Return mdtDormancydate
        End Get
        Set(ByVal value As Date)
            mdtDormancydate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dormancyreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Dormancyreason() As String
        Get
            Return mstrDormancyreason
        End Get
        Set(ByVal value As String)
            mstrDormancyreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set creditplan
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Creditplan() As String
        Get
            Return mstrCreditplan
        End Get
        Set(ByVal value As String)
            mstrCreditplan = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set billingcycle
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Billingcycle() As String
        Get
            Return mstrBillingcycle
        End Get
        Set(ByVal value As String)
            mstrBillingcycle = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletunpaidstatus
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletunpaidstatus() As String
        Get
            Return mstrWalletunpaidstatus
        End Get
        Set(ByVal value As String)
            mstrWalletunpaidstatus = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletunpaidstatusdate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletunpaidstatusdate() As Date
        Get
            Return mdtWalletunpaidstatusdate
        End Get
        Set(ByVal value As Date)
            mdtWalletunpaidstatusdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletbalancestatus
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletbalancestatus() As String
        Get
            Return mstrWalletbalancestatus
        End Get
        Set(ByVal value As String)
            mstrWalletbalancestatus = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletbalancestatusdate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletbalancestatusdate() As Date
        Get
            Return mdtWalletbalancestatusdate
        End Get
        Set(ByVal value As Date)
            mdtWalletbalancestatusdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletcreditlimit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletcreditlimit() As Single
        Get
            Return msinWalletcreditlimit
        End Get
        Set(ByVal value As Single)
            msinWalletcreditlimit = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set walletavailablecreditlimit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Walletavailablecreditlimit() As Single
        Get
            Return msinWalletavailablecreditlimit
        End Get
        Set(ByVal value As Single)
            msinWalletavailablecreditlimit = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set clientcreditlimit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Clientcreditlimit() As Single
        Get
            Return msinClientcreditlimit
        End Get
        Set(ByVal value As Single)
            msinClientcreditlimit = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set clientavailablecreditlimit
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Clientavailablecreditlimit() As Single
        Get
            Return msinClientavailablecreditlimit
        End Get
        Set(ByVal value As Single)
            msinClientavailablecreditlimit = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set unpaidstring
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Unpaidstring() As String
        Get
            Return mstrUnpaidstring
        End Get
        Set(ByVal value As String)
            mstrUnpaidstring = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dayspastdue
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Dayspastdue() As String
        Get
            Return mstrDayspastdue
        End Get
        Set(ByVal value As String)
            mstrDayspastdue = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lastpaymentamount
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Lastpaymentamount() As Single
        Get
            Return msinLastpaymentamount
        End Get
        Set(ByVal value As Single)
            msinLastpaymentamount = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lastpaymentdate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Lastpaymentdate() As Date
        Get
            Return mdtLastpaymentdate
        End Get
        Set(ByVal value As Date)
            mdtLastpaymentdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set internalclassification
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Internalclassification() As String
        Get
            Return mstrInternalclassification
        End Get
        Set(ByVal value As String)
            mstrInternalclassification = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set externalclassification
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Externalclassification() As String
        Get
            Return mstrExternalclassification
        End Get
        Set(ByVal value As String)
            mstrExternalclassification = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set internalprovision
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Internalprovision() As Single
        Get
            Return msinInternalprovision
        End Get
        Set(ByVal value As Single)
            msinInternalprovision = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set externalprovision
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Externalprovision() As Single
        Get
            Return msinExternalprovision
        End Get
        Set(ByVal value As Single)
            msinExternalprovision = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set rating
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Rating() As String
        Get
            Return mstrRating
        End Get
        Set(ByVal value As String)
            mstrRating = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deviceno
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Deviceno() As String
        Get
            Return mstrDeviceno
        End Get
        Set(ByVal value As String)
            mstrDeviceno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set devicestatus
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Devicestatus() As String
        Get
            Return mstrDevicestatus
        End Get
        Set(ByVal value As String)
            mstrDevicestatus = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set devicecreationdate
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Devicecreationdate() As Date
        Get
            Return mdtDevicecreationdate
        End Get
        Set(ByVal value As Date)
            mdtDevicecreationdate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set devicenoalias
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Devicenoalias() As String
        Get
            Return mstrDevicenoalias
        End Get
        Set(ByVal value As String)
            mstrDevicenoalias = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set billername
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Billername() As String
        Get
            Return mstrBillername
        End Get
        Set(ByVal value As String)
            mstrBillername = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set minimumamtdue
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Minimumamtdue() As Single
        Get
            Return msinMinimumamtdue
        End Get
        Set(ByVal value As Single)
            msinMinimumamtdue = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set directdebitaccno
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Directdebitaccno() As String
        Get
            Return mstrDirectdebitaccno
        End Get
        Set(ByVal value As String)
            mstrDirectdebitaccno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set legalidtype
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Legalidtype() As String
        Get
            Return mstrLegalidtype
        End Get
        Set(ByVal value As String)
            mstrLegalidtype = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set legalid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Legalid() As String
        Get
            Return mstrLegalid
        End Get
        Set(ByVal value As String)
            mstrLegalid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set oustandingbalwithloan
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Oustandingbalwithloan() As Single
        Get
            Return msinOustandingbalwithloan
        End Get
        Set(ByVal value As Single)
            msinOustandingbalwithloan = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set outstandingbalwithoutloan
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Outstandingbalwithoutloan() As Single
        Get
            Return msinOutstandingbalwithoutloan
        End Get
        Set(ByVal value As Single)
            msinOutstandingbalwithoutloan = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set customdata1
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Customdata1() As String
        Get
            Return mstrCustomdata1
        End Get
        Set(ByVal value As String)
            mstrCustomdata1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set customdata2
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Customdata2() As String
        Get
            Return mstrCustomdata2
        End Get
        Set(ByVal value As String)
            mstrCustomdata2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set customdata3
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Customdata3() As String
        Get
            Return mstrCustomdata3
        End Get
        Set(ByVal value As String)
            mstrCustomdata3 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set customdata4
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Customdata4() As String
        Get
            Return mstrCustomdata4
        End Get
        Set(ByVal value As String)
            mstrCustomdata4 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set customdata5
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Customdata5() As String
        Get
            Return mstrCustomdata5
        End Get
        Set(ByVal value As String)
            mstrCustomdata5 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vipflag
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Vipflag() As String
        Get
            Return mstrVipflag
        End Get
        Set(ByVal value As String)
            mstrVipflag = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  id " & _
              ", programname " & _
              ", branchname " & _
              ", bal_category " & _
              ", clientidname " & _
              ", cbsclientidname " & _
              ", walletopeningdate " & _
              ", walletno " & _
              ", walletusage " & _
              ", walletpromocodedesc " & _
              ", walletcurrency " & _
              ", walletpriority " & _
              ", walletadminstatus " & _
              ", walletadminstatusdate " & _
              ", walletauthflag " & _
              ", walletbalance " & _
              ", openingbalance " & _
              ", postedcredit " & _
              ", posteddebit " & _
              ", closingbalance " & _
              ", unpostedauthorization " & _
              ", unpostedtransaction " & _
              ", unpostedcredit " & _
              ", dormancydate " & _
              ", dormancyreason " & _
              ", creditplan " & _
              ", billingcycle " & _
              ", walletunpaidstatus " & _
              ", walletunpaidstatusdate " & _
              ", walletbalancestatus " & _
              ", walletbalancestatusdate " & _
              ", walletcreditlimit " & _
              ", walletavailablecreditlimit " & _
              ", clientcreditlimit " & _
              ", clientavailablecreditlimit " & _
              ", unpaidstring " & _
              ", dayspastdue " & _
              ", lastpaymentamount " & _
              ", lastpaymentdate " & _
              ", internalclassification " & _
              ", externalclassification " & _
              ", internalprovision " & _
              ", externalprovision " & _
              ", rating " & _
              ", deviceno " & _
              ", devicestatus " & _
              ", devicecreationdate " & _
              ", devicenoalias " & _
              ", billername " & _
              ", minimumamtdue " & _
              ", directdebitaccno " & _
              ", legalidtype " & _
              ", legalid " & _
              ", oustandingbalwithloan " & _
              ", outstandingbalwithoutloan " & _
              ", customdata1 " & _
              ", customdata2 " & _
              ", customdata3 " & _
              ", customdata4 " & _
              ", customdata5 " & _
              ", vipflag " & _
             "FROM lncreditcarddetails " & _
             "WHERE id = @id "

            objDataOperation.AddParameter("@id", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mstrid = dtRow.Item("id").ToString
                mstrprogramname = dtRow.Item("programname").ToString
                mstrbranchname = dtRow.Item("branchname").ToString
                mstrbal_category = dtRow.Item("bal_category").ToString
                mstrclientidname = dtRow.Item("clientidname").ToString
                mstrcbsclientidname = dtRow.Item("cbsclientidname").ToString
                mdtwalletopeningdate = dtRow.Item("walletopeningdate")
                mstrwalletno = dtRow.Item("walletno").ToString
                mstrwalletusage = dtRow.Item("walletusage").ToString
                mstrwalletpromocodedesc = dtRow.Item("walletpromocodedesc").ToString
                mstrwalletcurrency = dtRow.Item("walletcurrency").ToString
                mstrwalletpriority = dtRow.Item("walletpriority").ToString
                mstrwalletadminstatus = dtRow.Item("walletadminstatus").ToString
                mdtwalletadminstatusdate = dtRow.Item("walletadminstatusdate")
                mstrwalletauthflag = dtRow.Item("walletauthflag").ToString
                msinwalletbalance = dtRow.Item("walletbalance")
                msinopeningbalance = dtRow.Item("openingbalance")
                msinpostedcredit = dtRow.Item("postedcredit")
                msinposteddebit = dtRow.Item("posteddebit")
                msinclosingbalance = dtRow.Item("closingbalance")
                msinunpostedauthorization = dtRow.Item("unpostedauthorization")
                msinunpostedtransaction = dtRow.Item("unpostedtransaction")
                msinunpostedcredit = dtRow.Item("unpostedcredit")
                mdtdormancydate = dtRow.Item("dormancydate")
                mstrdormancyreason = dtRow.Item("dormancyreason").ToString
                mstrcreditplan = dtRow.Item("creditplan").ToString
                mstrbillingcycle = dtRow.Item("billingcycle").ToString
                mstrwalletunpaidstatus = dtRow.Item("walletunpaidstatus").ToString
                mdtwalletunpaidstatusdate = dtRow.Item("walletunpaidstatusdate")
                mstrwalletbalancestatus = dtRow.Item("walletbalancestatus").ToString
                mdtwalletbalancestatusdate = dtRow.Item("walletbalancestatusdate")
                msinwalletcreditlimit = dtRow.Item("walletcreditlimit")
                msinwalletavailablecreditlimit = dtRow.Item("walletavailablecreditlimit")
                msinclientcreditlimit = dtRow.Item("clientcreditlimit")
                msinclientavailablecreditlimit = dtRow.Item("clientavailablecreditlimit")
                mstrunpaidstring = dtRow.Item("unpaidstring").ToString
                mstrdayspastdue = dtRow.Item("dayspastdue").ToString
                msinlastpaymentamount = dtRow.Item("lastpaymentamount")
                mdtlastpaymentdate = dtRow.Item("lastpaymentdate")
                mstrinternalclassification = dtRow.Item("internalclassification").ToString
                mstrexternalclassification = dtRow.Item("externalclassification").ToString
                msininternalprovision = dtRow.Item("internalprovision")
                msinexternalprovision = dtRow.Item("externalprovision")
                mstrrating = dtRow.Item("rating").ToString
                mstrdeviceno = dtRow.Item("deviceno").ToString
                mstrdevicestatus = dtRow.Item("devicestatus").ToString
                mdtdevicecreationdate = dtRow.Item("devicecreationdate")
                mstrdevicenoalias = dtRow.Item("devicenoalias").ToString
                mstrbillername = dtRow.Item("billername").ToString
                msinminimumamtdue = dtRow.Item("minimumamtdue")
                mstrdirectdebitaccno = dtRow.Item("directdebitaccno").ToString
                mstrlegalidtype = dtRow.Item("legalidtype").ToString
                mstrlegalid = dtRow.Item("legalid").ToString
                msinoustandingbalwithloan = dtRow.Item("oustandingbalwithloan")
                msinoutstandingbalwithoutloan = dtRow.Item("outstandingbalwithoutloan")
                mstrcustomdata1 = dtRow.Item("customdata1").ToString
                mstrcustomdata2 = dtRow.Item("customdata2").ToString
                mstrcustomdata3 = dtRow.Item("customdata3").ToString
                mstrcustomdata4 = dtRow.Item("customdata4").ToString
                mstrcustomdata5 = dtRow.Item("customdata5").ToString
                mstrvipflag = dtRow.Item("vipflag").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        Try
            strQ = "SELECT " & _
                      "  id " & _
                      ", programname " & _
                      ", branchname " & _
                      ", bal_category " & _
                      ", clientidname " & _
                      ", cbsclientidname " & _
                      ", walletopeningdate " & _
                      ", walletno " & _
                      ", walletusage " & _
                      ", walletpromocodedesc " & _
                      ", walletcurrency " & _
                      ", walletpriority " & _
                      ", walletadminstatus " & _
                      ", walletadminstatusdate " & _
                      ", walletauthflag " & _
                      ", walletbalance " & _
                      ", openingbalance " & _
                      ", postedcredit " & _
                      ", posteddebit " & _
                      ", closingbalance " & _
                      ", unpostedauthorization " & _
                      ", unpostedtransaction " & _
                      ", unpostedcredit " & _
                      ", dormancydate " & _
                      ", dormancyreason " & _
                      ", creditplan " & _
                      ", billingcycle " & _
                      ", walletunpaidstatus " & _
                      ", walletunpaidstatusdate " & _
                      ", walletbalancestatus " & _
                      ", walletbalancestatusdate " & _
                      ", walletcreditlimit " & _
                      ", walletavailablecreditlimit " & _
                      ", clientcreditlimit " & _
                      ", clientavailablecreditlimit " & _
                      ", unpaidstring " & _
                      ", dayspastdue " & _
                      ", lastpaymentamount " & _
                      ", lastpaymentdate " & _
                      ", internalclassification " & _
                      ", externalclassification " & _
                      ", internalprovision " & _
                      ", externalprovision " & _
                      ", rating " & _
                      ", deviceno " & _
                      ", devicestatus " & _
                      ", devicecreationdate " & _
                      ", devicenoalias " & _
                      ", billername " & _
                      ", minimumamtdue " & _
                      ", directdebitaccno " & _
                      ", legalidtype " & _
                      ", legalid " & _
                      ", oustandingbalwithloan " & _
                      ", outstandingbalwithoutloan " & _
                      ", customdata1 " & _
                      ", customdata2 " & _
                      ", customdata3 " & _
                      ", customdata4 " & _
                      ", customdata5 " & _
                      ", vipflag " & _
                      " FROM lncreditcarddetails " & _
                      " WHERE 1 = 1 "

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter.Trim
            End If

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lncreditcarddetails) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@id", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Guid.NewGuid.ToString())
            objDataOperation.AddParameter("@programname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrProgramname.ToString)
            objDataOperation.AddParameter("@branchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBranchname.ToString)
            objDataOperation.AddParameter("@bal_category", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBal_Category.ToString)
            objDataOperation.AddParameter("@clientidname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientidname.ToString)
            objDataOperation.AddParameter("@cbsclientidname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCbsclientidname.ToString)
            objDataOperation.AddParameter("@walletopeningdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWalletopeningdate.ToString)
            objDataOperation.AddParameter("@walletno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWalletno.ToString)
            objDataOperation.AddParameter("@walletusage", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWalletusage.ToString)
            objDataOperation.AddParameter("@walletpromocodedesc", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWalletpromocodedesc.ToString)
            objDataOperation.AddParameter("@walletcurrency", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWalletcurrency.ToString)
            objDataOperation.AddParameter("@walletpriority", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWalletpriority.ToString)
            objDataOperation.AddParameter("@walletadminstatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWalletadminstatus.ToString)
            objDataOperation.AddParameter("@walletadminstatusdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWalletadminstatusdate.ToString)
            objDataOperation.AddParameter("@walletauthflag", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWalletauthflag.ToString)
            objDataOperation.AddParameter("@walletbalance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinWalletbalance.ToString)
            objDataOperation.AddParameter("@openingbalance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinOpeningbalance.ToString)
            objDataOperation.AddParameter("@postedcredit", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinPostedcredit.ToString)
            objDataOperation.AddParameter("@posteddebit", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinPosteddebit.ToString)
            objDataOperation.AddParameter("@closingbalance", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinClosingbalance.ToString)
            objDataOperation.AddParameter("@unpostedauthorization", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinUnpostedauthorization.ToString)
            objDataOperation.AddParameter("@unpostedtransaction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinUnpostedtransaction.ToString)
            objDataOperation.AddParameter("@unpostedcredit", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinUnpostedcredit.ToString)
            objDataOperation.AddParameter("@dormancydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDormancydate.ToString)
            objDataOperation.AddParameter("@dormancyreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDormancyreason.ToString)
            objDataOperation.AddParameter("@creditplan", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCreditplan.ToString)
            objDataOperation.AddParameter("@billingcycle", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBillingcycle.ToString)
            objDataOperation.AddParameter("@walletunpaidstatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWalletunpaidstatus.ToString)
            objDataOperation.AddParameter("@walletunpaidstatusdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWalletunpaidstatusdate.ToString)
            objDataOperation.AddParameter("@walletbalancestatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWalletbalancestatus.ToString)
            objDataOperation.AddParameter("@walletbalancestatusdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWalletbalancestatusdate.ToString)
            objDataOperation.AddParameter("@walletcreditlimit", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinWalletcreditlimit.ToString)
            objDataOperation.AddParameter("@walletavailablecreditlimit", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinWalletavailablecreditlimit.ToString)
            objDataOperation.AddParameter("@clientcreditlimit", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinClientcreditlimit.ToString)
            objDataOperation.AddParameter("@clientavailablecreditlimit", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinClientavailablecreditlimit.ToString)
            objDataOperation.AddParameter("@unpaidstring", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUnpaidstring.ToString)
            objDataOperation.AddParameter("@dayspastdue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDayspastdue.ToString)
            objDataOperation.AddParameter("@lastpaymentamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinLastpaymentamount.ToString)
            objDataOperation.AddParameter("@lastpaymentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLastpaymentdate.ToString)
            objDataOperation.AddParameter("@internalclassification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInternalclassification.ToString)
            objDataOperation.AddParameter("@externalclassification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExternalclassification.ToString)
            objDataOperation.AddParameter("@internalprovision", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinInternalprovision.ToString)
            objDataOperation.AddParameter("@externalprovision", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinExternalprovision.ToString)
            objDataOperation.AddParameter("@rating", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRating.ToString)
            objDataOperation.AddParameter("@deviceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDeviceno.ToString)
            objDataOperation.AddParameter("@devicestatus", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDevicestatus.ToString)
            objDataOperation.AddParameter("@devicecreationdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDevicecreationdate.ToString)
            objDataOperation.AddParameter("@devicenoalias", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDevicenoalias.ToString)
            objDataOperation.AddParameter("@billername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBillername.ToString)
            objDataOperation.AddParameter("@minimumamtdue", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinMinimumamtdue.ToString)
            objDataOperation.AddParameter("@directdebitaccno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDirectdebitaccno.ToString)
            objDataOperation.AddParameter("@legalidtype", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLegalidtype.ToString)
            objDataOperation.AddParameter("@legalid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLegalid.ToString)
            objDataOperation.AddParameter("@oustandingbalwithloan", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinOustandingbalwithloan.ToString)
            objDataOperation.AddParameter("@outstandingbalwithoutloan", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinOutstandingbalwithoutloan.ToString)
            objDataOperation.AddParameter("@customdata1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCustomdata1.ToString)
            objDataOperation.AddParameter("@customdata2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCustomdata2.ToString)
            objDataOperation.AddParameter("@customdata3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCustomdata3.ToString)
            objDataOperation.AddParameter("@customdata4", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCustomdata4.ToString)
            objDataOperation.AddParameter("@customdata5", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCustomdata5.ToString)
            objDataOperation.AddParameter("@vipflag", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVipflag.ToString)

            strQ = "INSERT INTO lncreditcarddetails ( " & _
                      "  id " & _
                      ", programname " & _
                      ", branchname " & _
                      ", bal_category " & _
                      ", clientidname " & _
                      ", cbsclientidname " & _
                      ", walletopeningdate " & _
                      ", walletno " & _
                      ", walletusage " & _
                      ", walletpromocodedesc " & _
                      ", walletcurrency " & _
                      ", walletpriority " & _
                      ", walletadminstatus " & _
                      ", walletadminstatusdate " & _
                      ", walletauthflag " & _
                      ", walletbalance " & _
                      ", openingbalance " & _
                      ", postedcredit " & _
                      ", posteddebit " & _
                      ", closingbalance " & _
                      ", unpostedauthorization " & _
                      ", unpostedtransaction " & _
                      ", unpostedcredit " & _
                      ", dormancydate " & _
                      ", dormancyreason " & _
                      ", creditplan " & _
                      ", billingcycle " & _
                      ", walletunpaidstatus " & _
                      ", walletunpaidstatusdate " & _
                      ", walletbalancestatus " & _
                      ", walletbalancestatusdate " & _
                      ", walletcreditlimit " & _
                      ", walletavailablecreditlimit " & _
                      ", clientcreditlimit " & _
                      ", clientavailablecreditlimit " & _
                      ", unpaidstring " & _
                      ", dayspastdue " & _
                      ", lastpaymentamount " & _
                      ", lastpaymentdate " & _
                      ", internalclassification " & _
                      ", externalclassification " & _
                      ", internalprovision " & _
                      ", externalprovision " & _
                      ", rating " & _
                      ", deviceno " & _
                      ", devicestatus " & _
                      ", devicecreationdate " & _
                      ", devicenoalias " & _
                      ", billername " & _
                      ", minimumamtdue " & _
                      ", directdebitaccno " & _
                      ", legalidtype " & _
                      ", legalid " & _
                      ", oustandingbalwithloan " & _
                      ", outstandingbalwithoutloan " & _
                      ", customdata1 " & _
                      ", customdata2 " & _
                      ", customdata3 " & _
                      ", customdata4 " & _
                      ", customdata5 " & _
                      ", vipflag" & _
                    ") VALUES (" & _
                      "  @id " & _
                      ", @programname " & _
                      ", @branchname " & _
                      ", @bal_category " & _
                      ", @clientidname " & _
                      ", @cbsclientidname " & _
                      ", @walletopeningdate " & _
                      ", @walletno " & _
                      ", @walletusage " & _
                      ", @walletpromocodedesc " & _
                      ", @walletcurrency " & _
                      ", @walletpriority " & _
                      ", @walletadminstatus " & _
                      ", @walletadminstatusdate " & _
                      ", @walletauthflag " & _
                      ", @walletbalance " & _
                      ", @openingbalance " & _
                      ", @postedcredit " & _
                      ", @posteddebit " & _
                      ", @closingbalance " & _
                      ", @unpostedauthorization " & _
                      ", @unpostedtransaction " & _
                      ", @unpostedcredit " & _
                      ", @dormancydate " & _
                      ", @dormancyreason " & _
                      ", @creditplan " & _
                      ", @billingcycle " & _
                      ", @walletunpaidstatus " & _
                      ", @walletunpaidstatusdate " & _
                      ", @walletbalancestatus " & _
                      ", @walletbalancestatusdate " & _
                      ", @walletcreditlimit " & _
                      ", @walletavailablecreditlimit " & _
                      ", @clientcreditlimit " & _
                      ", @clientavailablecreditlimit " & _
                      ", @unpaidstring " & _
                      ", @dayspastdue " & _
                      ", @lastpaymentamount " & _
                      ", @lastpaymentdate " & _
                      ", @internalclassification " & _
                      ", @externalclassification " & _
                      ", @internalprovision " & _
                      ", @externalprovision " & _
                      ", @rating " & _
                      ", @deviceno " & _
                      ", @devicestatus " & _
                      ", @devicecreationdate " & _
                      ", @devicenoalias " & _
                      ", @billername " & _
                      ", @minimumamtdue " & _
                      ", @directdebitaccno " & _
                      ", @legalidtype " & _
                      ", @legalid " & _
                      ", @oustandingbalwithloan " & _
                      ", @outstandingbalwithoutloan " & _
                      ", @customdata1 " & _
                      ", @customdata2 " & _
                      ", @customdata3 " & _
                      ", @customdata4 " & _
                      ", @customdata5 " & _
                      ", @vipflag" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrId = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lncreditcarddetails) </purpose>
    Public Function Delete(Optional ByVal mstrDebitAcNo As String = "") As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        Try
            strQ = "DELETE FROM lncreditcarddetails "

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()

            If mstrDebitAcNo.Trim.Length > 0 Then
                strQ &= " WHERE directdebitaccno = @directdebitaccno "
                objDataOperation.AddParameter("@directdebitaccno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDebitAcNo.Trim)
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@id", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal mstrDebitAcNo As String) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing


        Try
            strQ = "SELECT " & _
                      "  id " & _
                      ", programname " & _
                      ", branchname " & _
                      ", bal_category " & _
                      ", clientidname " & _
                      ", cbsclientidname " & _
                      ", walletopeningdate " & _
                      ", walletno " & _
                      ", walletusage " & _
                      ", walletpromocodedesc " & _
                      ", walletcurrency " & _
                      ", walletpriority " & _
                      ", walletadminstatus " & _
                      ", walletadminstatusdate " & _
                      ", walletauthflag " & _
                      ", walletbalance " & _
                      ", openingbalance " & _
                      ", postedcredit " & _
                      ", posteddebit " & _
                      ", closingbalance " & _
                      ", unpostedauthorization " & _
                      ", unpostedtransaction " & _
                      ", unpostedcredit " & _
                      ", dormancydate " & _
                      ", dormancyreason " & _
                      ", creditplan " & _
                      ", billingcycle " & _
                      ", walletunpaidstatus " & _
                      ", walletunpaidstatusdate " & _
                      ", walletbalancestatus " & _
                      ", walletbalancestatusdate " & _
                      ", walletcreditlimit " & _
                      ", walletavailablecreditlimit " & _
                      ", clientcreditlimit " & _
                      ", clientavailablecreditlimit " & _
                      ", unpaidstring " & _
                      ", dayspastdue " & _
                      ", lastpaymentamount " & _
                      ", lastpaymentdate " & _
                      ", internalclassification " & _
                      ", externalclassification " & _
                      ", internalprovision " & _
                      ", externalprovision " & _
                      ", rating " & _
                      ", deviceno " & _
                      ", devicestatus " & _
                      ", devicecreationdate " & _
                      ", devicenoalias " & _
                      ", billername " & _
                      ", minimumamtdue " & _
                      ", directdebitaccno " & _
                      ", legalidtype " & _
                      ", legalid " & _
                      ", oustandingbalwithloan " & _
                      ", outstandingbalwithoutloan " & _
                      ", customdata1 " & _
                      ", customdata2 " & _
                      ", customdata3 " & _
                      ", customdata4 " & _
                      ", customdata5 " & _
                      ", vipflag " & _
                      " FROM lncreditcarddetails " & _
                      " WHERE 1 = 1 "

            If mstrDebitAcNo.Trim.Length > 0 Then
                strQ &= " AND directdebitaccno = @directdebitaccno"
            End If

            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@directdebitaccno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDebitAcNo.Trim)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetCreditCardColumns(Optional ByVal mblnFlag As Boolean = False, Optional ByVal blnShowBlankInSelect As Boolean = False, Optional ByVal mstrFilter As String = "") As DataTable
        Dim dtTable As DataTable = Nothing
        Dim objDataOperation As clsDataOperation = Nothing
        Dim exForce As Exception
        Dim strQ As String = ""
        Try
            objDataOperation = New clsDataOperation

            If mblnFlag Then
                If blnShowBlankInSelect = False Then
                    strQ = " SELECT 0 AS Id,@Select AS Name, 0 AS DataType  UNION"
                Else
                    strQ = " SELECT 0 AS Id,'' AS Name, 0 AS DataType  UNION "
                End If
            End If


            strQ &= " SELECT " & enCreditCardColumn.PROGRAM_NAME & " AS Id, 'Program Name' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.BRANCH_NAME & " AS Id, 'Branch Name' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.BALANCE_CATEGORY & " AS Id, 'Balance Category' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.CLIENT_ID_NAME & " AS Id, 'Client ID-Name' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.CBS_CLIENTID_NAME & " AS Id, 'CBS Client ID-Name' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_OPENING_DATE & " AS Id, 'Wallet Opening Date' AS Name," & enCreditCardColumnDataType.DATETIME & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_NUMBER & " AS Id, 'Wallet Number' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_USAGE & " AS Id, 'Wallet Usage' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_PROMO_CODE_DESCRIPTION & " AS Id, 'Wallet Promo Code-Description' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_CURRENCY & " AS Id, 'Wallet Currency' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_PRIORITY & " AS Id, 'Wallet Priority' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_ADMIN_STATUS & " AS Id, 'Wallet Admin Status' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_ADMIN_STATUS_DATE & " AS Id, 'Wallet Admin Status Date' AS Name," & enCreditCardColumnDataType.DATETIME & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_AUTH_FLAG & " AS Id, 'Wallet Auth. Flag' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_BALANCE & " AS Id, 'Wallet Balance' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.OPENING_BALANCE & " AS Id, 'Opening Balance' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.POSTED_CREDIT & " AS Id, 'Posted Credit' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.POSTED_DEBIT & " AS Id, 'Posted Debit' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.CLOSING_BALANCE & " AS Id, 'Closing Balance' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.UNPOSTED_AUTHORIZATION & " AS Id, 'Unposted Authorization' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.UNPOSTED_TRANSACTION & " AS Id, 'Unposted Transaction' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.UNPOSTED_CREDIT & " AS Id, 'Unposted Credit' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.DORMANCY_DATE & " AS Id, 'Dormancy Date' AS Name," & enCreditCardColumnDataType.DATETIME & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.DORMANCY_REASON & " AS Id, 'Dormancy Reason' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.CREDIT_PLAN & " AS Id, 'Credit Plan' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.BILLING_CYCLE & " AS Id, 'Billing Cycle' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_UNPAID_STATUS & " AS Id, 'Wallet Unpaid Status' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_UNPAID_STATUS_DATE & " AS Id, 'Wallet Unpaid Status Date' AS Name," & enCreditCardColumnDataType.DATETIME & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_BALANCE_STATUS & " AS Id, 'Wallet Balance Status' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_BALANCE_STATUS_DATE & " AS Id, 'Wallet Balance Status Date' AS Name," & enCreditCardColumnDataType.DATETIME & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_CREDIT_LIMIT & " AS Id, 'Wallet Credit Limit' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.WALLET_AVAILABLE_CREDIT_LIMIT & " AS Id, 'Wallet Available Credit Limit' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.CLIENT_CREDIT_LIMIT & " AS Id, 'Client Credit Limit' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.CLIENT_AVAILABLE_CREDIT_LIMIT & " AS Id, 'Client Available Credit Limit' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.UNPAID_STRING & " AS Id, 'Unpaid String' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.DAYS_PAST_DUE & " AS Id, 'Days Past Due' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.LAST_PAYMENT_AMOUNT & " AS Id, 'Last Payment Amount' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.LAST_PAYMENT_DATE & " AS Id, 'Last Payment Date' AS Name," & enCreditCardColumnDataType.DATETIME & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.INTERNAL_CLASSIFICATION & " AS Id, 'Internal Classification' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.EXTERNAL_CLASSIFICATION & " AS Id, 'External Classification' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.INTERNAL_PROVISION & " AS Id, 'Internal Provision' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.EXTERNAL_PROVISION & " AS Id, 'External Provision' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.RATING & " AS Id, 'Rating' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.DEVICE_NUMBER & " AS Id, 'Device Number' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.DEVICE_STATUS & " AS Id, 'Device Status' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.DATE_OF_DEVICE_CREATION & " AS Id, 'Date of Device Creation' AS Name," & enCreditCardColumnDataType.DATETIME & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.DEVICE_NUMBER_ALIAS & " AS Id, 'Device Number Alias' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.BILLER_NAME & " AS Id, 'Biller Name' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.MINIMUM_AMOUNT_DUE_MAD & " AS Id, 'Minimum Amount Due (MAD)' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.DIRECT_DEBIT_ACCOUNT_NUMBER & " AS Id, 'Direct Debit Account Number' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.LEGAL_ID_TYPE & " AS Id, 'Legal ID Type' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.LEGAL_ID & " AS Id, 'Legal ID' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.OUTSTANDING_BALANCE_WITH_LOAN & " AS Id, 'Outstanding Balance with Loan' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.OUTSTANDING_BALANCE_WITHOUT_LOAN & " AS Id, 'Outstanding Balance without Loan' AS Name," & enCreditCardColumnDataType.NUMERIC & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.CUSTOM_DATA_1 & " AS Id, 'CUSTOM DATA 1' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.CUSTOM_DATA_2 & " AS Id, 'CUSTOM DATA 2' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.CUSTOM_DATA_3 & " AS Id, 'CUSTOM DATA 3' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.CUSTOM_DATA_4 & " AS Id, 'CUSTOM DATA 4' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.CUSTOM_DATA_5 & " AS Id, 'CUSTOM DATA 5' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType UNION " & _
                      " SELECT " & enCreditCardColumn.VIP_FLAG & " AS Id, 'VIP Flag' AS Name," & enCreditCardColumnDataType.NVARCHAR & " As DataType  "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Select"))
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mstrFilter.Trim.Length > 0 Then
                dtTable = New DataView(dsList.Tables(0), mstrFilter & IIf(mblnFlag, " OR DataType = 0", ""), "", DataViewRowState.CurrentRows).ToTable()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCreditCardColumns; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmpCreditCardAmount(ByVal mstrDebitAcNo As String, ByVal mstrLoanSchemeCode As String, Optional ByRef mdecCreditCardAmountOnExposure As Decimal = 0 _
                                                              , Optional ByVal mblnGetCreditCardAmountOnExposure As Boolean = False) As Decimal
        Dim strQ As String = ""
        Dim mdecCreditCardCalculatedAmount As Decimal = 0
        Dim mdecCreditCardFieldAmount As Decimal = 0
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Try

            If mstrDebitAcNo.Trim.Length > 0 AndAlso mstrLoanSchemeCode.Trim.Length > 0 Then
                Dim objLoanScheme As New clsLoan_Scheme
                Dim mdecCreditCardPercentageOnDSR As Decimal = 0
                Dim mintCreditCardAmountOnDSRFieldId As Integer = 0
                Dim mintCreditCardAmountOnExposureFieldId As Integer = 0
                Dim dsLoanSchemeList As DataSet = Nothing
                Dim mstrCreditCardAmountOnDSRFieldName As String = ""
                Dim mstrCreditCardAmountOnExposureFieldName As String = ""


                dsLoanSchemeList = objLoanScheme.GetList("List", True, False, "Code = '" & mstrLoanSchemeCode.Trim & "'")

                If dsLoanSchemeList IsNot Nothing AndAlso dsLoanSchemeList.Tables(0).Rows.Count > 0 Then
                    mdecCreditCardPercentageOnDSR = CDec(dsLoanSchemeList.Tables(0).Rows(0)("ccpercentageondsr"))
                    mintCreditCardAmountOnDSRFieldId = CInt(dsLoanSchemeList.Tables(0).Rows(0)("ccamtondsrfieldid"))
                    If mblnGetCreditCardAmountOnExposure Then mintCreditCardAmountOnExposureFieldId = CInt(dsLoanSchemeList.Tables(0).Rows(0)("ccamtonexposurefieldid"))
                End If ' If dsLoanSchemeList IsNot Nothing AndAlso dsLoanSchemeList.Tables(0).Rows.Count > 0 Then

                If mdecCreditCardPercentageOnDSR <> 0 AndAlso mintCreditCardAmountOnDSRFieldId > 0 Then

                    Select Case mintCreditCardAmountOnDSRFieldId

                        Case enCreditCardColumn.WALLET_BALANCE
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(walletbalance,0) AS walletbalance "

                        Case enCreditCardColumn.OPENING_BALANCE
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(openingbalance,0) AS openingbalance "

                        Case enCreditCardColumn.POSTED_CREDIT
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(postedcredit,0) AS postedcredit "

                        Case enCreditCardColumn.POSTED_DEBIT
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(posteddebit,0) AS posteddebit"

                        Case enCreditCardColumn.CLOSING_BALANCE
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(closingbalance,0) AS closingbalance"

                        Case enCreditCardColumn.UNPOSTED_AUTHORIZATION
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(unpostedauthorization,0) AS unpostedauthorization"

                        Case enCreditCardColumn.UNPOSTED_TRANSACTION
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(unpostedtransaction,0) AS unpostedtransaction "

                        Case enCreditCardColumn.UNPOSTED_CREDIT
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(unpostedcredit,0) AS unpostedcredit "

                        Case enCreditCardColumn.WALLET_CREDIT_LIMIT
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(walletcreditlimit,0) AS walletcreditlimit"

                        Case enCreditCardColumn.WALLET_AVAILABLE_CREDIT_LIMIT
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(walletavailablecreditlimit,0) AS walletavailablecreditlimit"

                        Case enCreditCardColumn.CLIENT_CREDIT_LIMIT
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(clientcreditlimit,0) AS clientcreditlimit"

                        Case enCreditCardColumn.CLIENT_AVAILABLE_CREDIT_LIMIT
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(clientavailablecreditlimit,0) AS clientavailablecreditlimit"

                        Case enCreditCardColumn.LAST_PAYMENT_AMOUNT
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(lastpaymentamount,0) AS lastpaymentamount"

                        Case enCreditCardColumn.INTERNAL_PROVISION
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(internalprovision,0) AS internalprovision"

                        Case enCreditCardColumn.EXTERNAL_PROVISION
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(externalprovision,0) AS externalprovision"

                        Case enCreditCardColumn.MINIMUM_AMOUNT_DUE_MAD
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(minimumamtdue,0) AS minimumamtdue"

                        Case enCreditCardColumn.OUTSTANDING_BALANCE_WITH_LOAN
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(oustandingbalwithloan,0) AS oustandingbalwithloan"

                        Case enCreditCardColumn.OUTSTANDING_BALANCE_WITHOUT_LOAN
                            mstrCreditCardAmountOnDSRFieldName = "ISNULL(outstandingbalwithoutloan,0) AS outstandingbalwithoutloan"

                    End Select

                End If 'If mdecCreditCardPercentageOnDSR > 0 AndAlso mintCreditCardAmountOnDSRFieldId > 0 Then

                If mstrCreditCardAmountOnDSRFieldName.Trim.Length > 0 Then

                    strQ = " SELECT " & mstrCreditCardAmountOnDSRFieldName.Trim & _
                              " FROM lncreditcarddetails " & _
                              " WHERE directdebitaccno = @directdebitaccno "

                    objDataOperation = New clsDataOperation
                    objDataOperation.AddParameter("@directdebitaccno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDebitAcNo)
                    Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        mdecCreditCardFieldAmount = CDec(dsList.Tables(0).Rows(0)(0))
                    End If 'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                    If mdecCreditCardFieldAmount <> 0 AndAlso mdecCreditCardPercentageOnDSR <> 0 Then
                        mdecCreditCardCalculatedAmount = (mdecCreditCardFieldAmount * mdecCreditCardPercentageOnDSR) / 100
                    Else
                        mdecCreditCardCalculatedAmount = mdecCreditCardFieldAmount
                    End If

                End If  ' If mstrCreditCardAmountOnDSRFieldName.Trim.Length > 0 Then

                If mblnGetCreditCardAmountOnExposure AndAlso mintCreditCardAmountOnExposureFieldId > 0 Then

                    Select Case mintCreditCardAmountOnExposureFieldId

                        Case enCreditCardColumn.WALLET_BALANCE
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(walletbalance,0) AS walletbalance "

                        Case enCreditCardColumn.OPENING_BALANCE
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(openingbalance,0) AS openingbalance "

                        Case enCreditCardColumn.POSTED_CREDIT
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(postedcredit,0) AS postedcredit "

                        Case enCreditCardColumn.POSTED_DEBIT
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(posteddebit,0) AS posteddebit"

                        Case enCreditCardColumn.CLOSING_BALANCE
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(closingbalance,0) AS closingbalance"

                        Case enCreditCardColumn.UNPOSTED_AUTHORIZATION
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(unpostedauthorization,0) AS unpostedauthorization"

                        Case enCreditCardColumn.UNPOSTED_TRANSACTION
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(unpostedtransaction,0) AS unpostedtransaction "

                        Case enCreditCardColumn.UNPOSTED_CREDIT
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(unpostedcredit,0) AS unpostedcredit "

                        Case enCreditCardColumn.WALLET_CREDIT_LIMIT
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(walletcreditlimit,0) AS walletcreditlimit"

                        Case enCreditCardColumn.WALLET_AVAILABLE_CREDIT_LIMIT
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(walletavailablecreditlimit,0) AS walletavailablecreditlimit"

                        Case enCreditCardColumn.CLIENT_CREDIT_LIMIT
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(clientcreditlimit,0) AS clientcreditlimit"

                        Case enCreditCardColumn.CLIENT_AVAILABLE_CREDIT_LIMIT
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(clientavailablecreditlimit,0) AS clientavailablecreditlimit"

                        Case enCreditCardColumn.LAST_PAYMENT_AMOUNT
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(lastpaymentamount,0) AS lastpaymentamount"

                        Case enCreditCardColumn.INTERNAL_PROVISION
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(internalprovision,0) AS internalprovision"

                        Case enCreditCardColumn.EXTERNAL_PROVISION
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(externalprovision,0) AS externalprovision"

                        Case enCreditCardColumn.MINIMUM_AMOUNT_DUE_MAD
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(minimumamtdue,0) AS minimumamtdue"

                        Case enCreditCardColumn.OUTSTANDING_BALANCE_WITH_LOAN
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(oustandingbalwithloan,0) AS oustandingbalwithloan"

                        Case enCreditCardColumn.OUTSTANDING_BALANCE_WITHOUT_LOAN
                            mstrCreditCardAmountOnExposureFieldName = "ISNULL(outstandingbalwithoutloan,0) AS outstandingbalwithoutloan"

                    End Select

                    If mstrCreditCardAmountOnExposureFieldName.Trim.Length > 0 Then

                        strQ = " SELECT " & mstrCreditCardAmountOnExposureFieldName.Trim & _
                                  " FROM lncreditcarddetails " & _
                                  " WHERE directdebitaccno = @directdebitaccno "

                        objDataOperation = New clsDataOperation
                        objDataOperation.AddParameter("@directdebitaccno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDebitAcNo)
                        Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                        If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                            mdecCreditCardAmountOnExposure = CDec(dsList.Tables(0).Rows(0)(0))
                        End If 'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                    End If  ' If mstrCreditCardAmountOnDSRFieldName.Trim.Length > 0 Then

                End If '  If mblnGetCreditCardAmountOnExposure Then

                objLoanScheme = Nothing

            End If 'If mstrDebitAcNo.Trim.Length > 0 AndAlso mstrLoanSchemeCode.Trim.Length > 0 Then

            Return mdecCreditCardCalculatedAmount

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpCreditCardAmount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

End Class