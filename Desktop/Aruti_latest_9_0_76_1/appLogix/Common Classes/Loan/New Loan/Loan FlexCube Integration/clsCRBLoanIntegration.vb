﻿Imports System.Runtime.Serialization
Imports System.Windows

Public Class clsCRBLoanIntegration

    Dim mstrdateOfBirth As String
    Dim mstrFirstName As String
    Dim mstrFullName As String
    Dim objIdNumbers As New idNumbers
    Dim objphoneNumbers As New phoneNumbers
    Dim mstrPresentSurname As String

    Public Property dateOfBirth() As String
        Get
            Return mstrdateOfBirth
        End Get
        Set(ByVal value As String)
            mstrdateOfBirth = value
        End Set
    End Property

    Public Property firstName() As String
        Get
            Return mstrFirstName
        End Get
        Set(ByVal value As String)
            mstrFirstName = value
        End Set
    End Property

    Public Property fullName() As String
        Get
            Return mstrFullName
        End Get
        Set(ByVal value As String)
            mstrFullName = value
        End Set
    End Property

    Public Property idNumbers() As idNumbers
        Get
            Return objIdNumbers
        End Get
        Set(ByVal value As idNumbers)
            objIdNumbers = value
        End Set
    End Property

    Public Property phoneNumbers() As phoneNumbers
        Get
            Return objphoneNumbers
        End Get
        Set(ByVal value As phoneNumbers)
            objphoneNumbers = value
        End Set
    End Property

    Public Property presentSurname() As String
        Get
            Return mstrPresentSurname
        End Get
        Set(ByVal value As String)
            mstrPresentSurname = value
        End Set
    End Property

End Class


Public Class idNumberPairIndividual
    Dim mstrIdNumber As String
    Dim mstrIdNumberType As String

    Public Property idNumber() As String
        Get
            Return mstrIdNumber
        End Get
        Set(ByVal value As String)
            mstrIdNumber = value
        End Set
    End Property
    Public Property idNumberType() As String
        Get
            Return mstrIdNumberType
        End Get
        Set(ByVal value As String)
            mstrIdNumberType = value
        End Set
    End Property
End Class

Public Class idNumbers
    Dim objidNumberPairIndividual As New idNumberPairIndividual

    Public Property idNumberPairIndividual() As idNumberPairIndividual
        Get
            Return objidNumberPairIndividual
        End Get
        Set(ByVal value As idNumberPairIndividual)
            objidNumberPairIndividual = value
        End Set
    End Property
End Class

Public Class phoneNumbers

    Dim mstrString As String

    Public Property [string]() As String
        Get
            Return mstrString
        End Get
        Set(ByVal value As String)
            mstrString = value
        End Set
    End Property

End Class