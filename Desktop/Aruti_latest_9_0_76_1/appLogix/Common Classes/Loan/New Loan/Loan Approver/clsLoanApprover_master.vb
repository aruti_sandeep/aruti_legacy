﻿'****************************************************************************************************************
'Class Name :clsLoanApprover_master.vb
'Purpose    :
'Date       :09-Mar-2015
'Written By :Nilay
'Modified   :
'****************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Nilay
''' </summary>
''' 
Public Class clsLoanApprover_master
    Private Shared ReadOnly mstrModuleName As String = "clsLoanApprover_master"
    Private objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region "Private Variables"

    Private mintlnApproverunkid As Integer
    Private mintlnLevelunkid As Integer
    Private mintApproverEmpunkid As Integer
    Private mintMappedUserId As Integer = 0
    Private mintUserunkid As Integer
    Private mblnIsswap As Boolean = False
    Private mblnIsactive As Boolean = True
    Private mblnIsvoid As Boolean
    Private mdtVoidDateTime As Date
    Private mintVoidUserunkid As Integer
    Private mstrVoidReason As String = ""
    Private mintSwapFromApproverID As Integer = 0
    Private mintSwapToApproverID As Integer = 0
    Private mdtLoanSchemeMapping As DataTable = Nothing
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private objLoanApproverTran As New clsLoanApprover_tran
    Private objLoanapproverMapping As New clsloan_approver_mapping
    Private objLoanSchemeMapping As New clsapprover_scheme_mapping
    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Private mblnIsExternalApprover As Boolean = False
    'Nilay (01-Mar-2016) -- End

#End Region

#Region "Properties"
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Nilay
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lnapproverunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _lnApproverunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        Get
            Return mintlnApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintlnApproverunkid = value
            'Nilay (07-Feb-2016) -- Start
            'Call GetData()
            Call GetData(objDataOp)
            'Nilay (07-Feb-2016) -- End
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lnLevelunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _lnLevelunkid() As Integer
        Get
            Return mintlnLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintlnLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ApproverEmpunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _ApproverEmpunkid() As Integer
        Get
            Return mintApproverEmpunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverEmpunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set MappedUserunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _MappedUserunkid() As Integer
        Get
            Return mintMappedUserId
        End Get
        Set(ByVal value As Integer)
            mintMappedUserId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set SwapFromApproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SwapFromApproverunkid() As Integer
        Get
            Return mintSwapFromApproverID
        End Get
        Set(ByVal value As Integer)
            mintSwapFromApproverID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set SwapToApproverunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SwapToApproverunkid() As Integer
        Get
            Return mintSwapToApproverID
        End Get
        Set(ByVal value As Integer)
            mintSwapToApproverID = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Userunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isvoid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set VoidDateTime
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _VoidDateTime() As Date
        Get
            Return mdtVoidDateTime
        End Get
        Set(ByVal value As Date)
            mdtVoidDateTime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set VoidUserunkid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _VoidUserunkid() As Integer
        Get
            Return mintVoidUserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set VoidReason
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _VoidReason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isvoid
    ''' Modify By: Pinkal 
    ''' </summary>
    Public Property _Isswap() As Boolean
        Get
            Return mblnIsswap
        End Get
        Set(ByVal value As Boolean)
            mblnIsswap = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtLoanSchemeMapping
    ''' Modify By: Pinkal 
    ''' </summary>
    Public Property _dtLoanSchemeMapping() As DataTable
        Get
            Return mdtLoanSchemeMapping
        End Get
        Set(ByVal value As DataTable)
            mdtLoanSchemeMapping = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isactive
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isvoid
    ''' Modify By: Nilay
    ''' </summary>
    Public Property _IsExternalApprover() As Boolean
        Get
            Return mblnIsExternalApprover
        End Get
        Set(ByVal value As Boolean)
            mblnIsExternalApprover = value
        End Set
    End Property

#End Region

    '' <summary>
    '' Modify By: Nilay
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>

    'Nilay (07-Feb-2016) -- Start
    'Public Sub GetData()
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        'Nilay (07-Feb-2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Nilay (07-Feb-2016) -- Start
        'Dim objDataOperation As New clsDataOperation
        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOp
        End If
        objDataOperation.ClearParameters()
        'Nilay (07-Feb-2016) -- End

        Try
            strQ = "SELECT " & _
                    "  lnloanapprover_master.lnapproverunkid " & _
                    " ,lnloanapprover_master.lnlevelunkid " & _
                    " ,lnloanapprover_master.approverempunkid " & _
                    " ,lnloanapprover_master.userunkid " & _
                    " ,lnloanapprover_master.isvoid " & _
                    " ,lnloanapprover_master.voiddatetime " & _
                    " ,lnloanapprover_master.voiduserunkid " & _
                    " ,lnloanapprover_master.voidreason " & _
                    " ,lnloanapprover_master.isswap " & _
                    " ,lnloanapprover_master.isactive " & _
                    " ,lnloanapprover_master.isexternalapprover " & _
                " FROM lnloanapprover_master " & _
                " WHERE lnapproverunkid=@lnapproverunkid "
            'Nilay (01-Mar-2016) -- [isexternalapprover]

            objDataOperation.AddParameter("@lnapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each drRow As DataRow In dsList.Tables(0).Rows
                mintlnApproverunkid = CInt(drRow.Item("lnapproverunkid"))
                mintlnLevelunkid = CInt(drRow.Item("lnlevelunkid"))
                mintApproverEmpunkid = CInt(drRow.Item("approverempunkid"))
                mintUserunkid = CInt(drRow.Item("userunkid"))
                mblnIsactive = CBool(drRow.Item("isactive"))
                mblnIsvoid = CBool(drRow.Item("isvoid"))
                If IsDBNull(drRow.Item("voiddatetime")) = False Then
                    mdtVoidDateTime = drRow.Item("voiddatetime")
                Else
                    mdtVoidDateTime = Nothing
                End If
                mintVoidUserunkid = CInt(drRow.Item("voiduserunkid"))
                mstrVoidReason = drRow.Item("voidreason").ToString
                mblnIsswap = CBool(drRow.Item("isswap"))
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                mblnIsExternalApprover = CBool(drRow.Item("isexternalapprover"))
                'Nilay (01-Mar-2016) -- End

                Dim objUserMapping As New clsloan_approver_mapping
                objUserMapping.GetData(mintlnApproverunkid, -1, -1)
                mintMappedUserId = objUserMapping._MappedUserunkid 'Nilay (05-May-2016) -- [_MappedUserunkid]
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Public Function GetExternalApproverList(Optional ByVal objDataOpr As clsDataOperation = Nothing, Optional ByVal strList As String = "List") As DataSet

        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsApprover As New DataSet

        Try
            If objDataOpr IsNot Nothing Then
                objDataOperation = objDataOpr
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            If strList.Trim.Length <= 0 Then strList = "List"

            StrQ = "SELECT DISTINCT " & _
                   "     ISNULL(cffinancial_year_tran.database_name,'') AS dbname " & _
                   "    ,ISNULL(cffinancial_year_tran.companyunkid,0) AS companyunkid " & _
                   "    ,ISNULL(cffinancial_year_tran.yearunkid,0) AS yearunkid " & _
                   "    ,ISNULL(EffDt.key_value,'') AS effectivedate " & _
                   "    ,ISNULL(UC.key_value,'') AS modeset " & _
                   "FROM lnloanapprover_master " & _
                   "    JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanapprover_master.approverempunkid " & _
                   "    LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.companyunkid = cfuser_master.companyunkid " & _
                   "            AND cffinancial_year_tran.isclosed = 0 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             cfconfiguration.companyunkid " & _
                   "            ,cfconfiguration.key_value " & _
                   "        FROM hrmsConfiguration..cfconfiguration " & _
                   "        WHERE UPPER(cfconfiguration.key_name) IN ('EMPLOYEEASONDATE') " & _
                   "    ) AS EffDt ON EffDt.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             cfconfiguration.companyunkid " & _
                   "            ,cfconfiguration.key_value " & _
                   "        FROM hrmsConfiguration..cfconfiguration " & _
                   "        WHERE UPPER(cfconfiguration.key_name) IN ('USERACCESSMODESETTING') " & _
                   "    ) AS UC ON UC.companyunkid = cffinancial_year_tran.companyunkid " & _
                   "WHERE lnloanapprover_master.isvoid = 0 AND lnloanapprover_master.isswap = 0 " & _
                   "  AND lnloanapprover_master.isexternalapprover = 1 "

            dsApprover = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetExternalApproverList; Module Name: " & mstrModuleName)
        Finally
            If objDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsApprover
    End Function
    'Nilay (01-Mar-2016) -- End

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    ' <summary>
    ' Modify By: Pinkal
    ' </summary>
    ' <purpose> Assign all Property variable </purpose>

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 

    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnOnlyActive As Boolean = True, _
                            Optional ByVal blnIsvoid As Boolean = False, _
                            Optional ByVal strFilter As String = "", _
                            Optional ByVal intApproverTranunkid As Integer = 0 _
                            ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            Dim StrFinalQurey As String = String.Empty
            Dim StrQCondition As String = String.Empty
            Dim StrQDtFilters As String = String.Empty
            Dim StrQDataJoin As String = String.Empty

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            strQ = "SELECT " & _
                      "  lnloanapprover_master.lnapproverunkid " & _
                      ", lnloanapprover_master.lnlevelunkid " & _
                      ", lnapproverlevel_master.lnlevelname " & _
                      ", lnapproverlevel_master.priority " & _
                      ", lnloanapprover_master.approverempunkid " & _
                      ", #APPROVER_CODE# AS code " & _
                      ", #APPROVER_NAME# as name " & _
                      ", #DEPT_NAME# as departmentname " & _
                      ", #SEC_NAME# as sectionname " & _
                      ", #JOB_NAME# As  jobname " & _
                      ", lnloanapprover_master.isactive " & _
                      ", lnloanapprover_master.isvoid " & _
                      ", lnloanapprover_master.voiddatetime " & _
                      ", lnloanapprover_master.userunkid " & _
                      ", lnloanapprover_master.voiduserunkid " & _
                      ", lnloanapprover_master.voidreason " & _
                      ", lnloanapprover_master.isswap " & _
                      ", ISNULL(UM.username,'') AS mappeduser " & _
                      ", lnloanapprover_master.isexternalapprover " & _
                      ", CASE WHEN lnloanapprover_master.isexternalapprover = 1 THEN @YES ELSE @NO END AS extapprover " & _
                      ", #APPROVER_NAME_WITHOUT_CODE# as ApproverName " & _
                    " FROM lnloanapprover_master " & _
                      " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
                      " #EMPLOYEE_JOIN# " & _
                      " #DATA_JOIN# " & _
                      " LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid " & _
                      " LEFT JOIN hrmsconfiguration..cfuser_master AS UM ON UM.userunkid = lnloan_approver_mapping.userunkid "

            'Gajanan [23-SEP-2019] -- Add [ApproverName]    

            StrFinalQurey = strQ

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            StrQCondition = " WHERE 1=1 AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQDtFilters &= xDateFilterQry & " "
                End If
            End If

            If blnOnlyActive = True Then
                StrQCondition &= " AND lnloanapprover_master.isactive = 1 "
            Else
                StrQCondition &= " AND lnloanapprover_master.isactive = 0 "
            End If

            If blnIsvoid = True Then
                StrQCondition &= " AND lnloanapprover_master.isvoid = 1 "
            Else
                StrQCondition &= " AND lnloanapprover_master.isvoid = 0 "
            End If

            If intApproverTranunkid > 0 Then
                StrQCondition &= " AND lnloanapprover_master.lnapproverunkid = " & intApproverTranunkid & " "
            End If

            StrQCondition &= " AND lnloanapprover_master.isswap = 0 "

            If strFilter.Trim.Length > 0 Then
                StrQCondition &= " AND " & strFilter
            End If

            strQ = strQ.Replace("#APPROVER_CODE#", "ISNULL(hremployee_master.employeecode,'') ")
            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(DPT.name,'') ")
            strQ = strQ.Replace("#SEC_NAME#", "ISNULL(SEC.name,'') ")
            strQ = strQ.Replace("#JOB_NAME#", "ISNULL(JOB.job_name,'') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid AND hremployee_master.isapproved = 1 ")

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            strQ = strQ.Replace("#APPROVER_NAME_WITHOUT_CODE#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.surname,'') ")
            'Gajanan [23-SEP-2019] -- End

            StrQDataJoin = " LEFT JOIN " & _
                      "( " & _
                      "     SELECT " & _
                      "          departmentunkid " & _
                      "         ,sectionunkid " & _
                      "         ,employeeunkid " & _
                      "         ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                           "     FROM #DB_NAME#hremployee_transfer_tran " & _
                      "     WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS ETT ON ETT.employeeunkid = hremployee_master.employeeunkid AND ETT.rno = 1 " & _
                           " LEFT JOIN #DB_NAME#hrdepartment_master AS DPT ON DPT.departmentunkid = ETT.departmentunkid " & _
                           " LEFT JOIN #DB_NAME#hrsection_master AS SEC ON SEC.sectionunkid = ETT.sectionunkid " & _
                      " LEFT JOIN " & _
                            "( " & _
                            "   SELECT " & _
                            "        jobunkid " & _
                            "       ,employeeunkid " & _
                            "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                 "   FROM #DB_NAME#hremployee_categorization_tran " & _
                            "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                            ") AS ECT ON ECT.employeeunkid = hremployee_master.employeeunkid AND ECT.rno = 1 " & _
                           " LEFT JOIN #DB_NAME#hrjob_master AS JOB ON JOB.jobunkid = ECT.jobunkid "

            strQ = strQ.Replace("#DATA_JOIN#", StrQDataJoin)
            strQ = strQ.Replace("#DB_NAME#", "")
            strQ &= StrQCondition
            strQ = strQ.Replace("#EXT_APPROVER#", "0")
            strQ &= StrQDtFilters

            objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Yes"))
            objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "No"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            dsCompany = GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows
                strQ = StrFinalQurey
                StrQDtFilters = ""

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPROVER_CODE#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#DEPT_NAME#", "'' ")
                    strQ = strQ.Replace("#SEC_NAME#", "'' ")
                    strQ = strQ.Replace("#JOB_NAME#", "'' ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = lnloanapprover_master.approverempunkid ")
                    strQ = strQ.Replace("#DATA_JOIN#", "")
                    strQ = strQ.Replace("#DB_NAME#", "")
                    'Gajanan [23-SEP-2019] -- Start    
                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    strQ = strQ.Replace("#APPROVER_NAME_WITHOUT_CODE#", "ISNULL(cfuser_master.username,'') ")
                    'Gajanan [23-SEP-2019] -- End
                Else
                    xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
                    Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, eZeeDate.convertDate(dRow("effectivedate")), eZeeDate.convertDate(dRow("effectivedate")), , , dRow("dbname").ToString)
                    Call GetAdvanceFilterQry(xAdvanceJoinQry, eZeeDate.convertDate(dRow("effectivedate")), dRow("dbname").ToString)

                    strQ = strQ.Replace("#APPROVER_CODE#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#DEPT_NAME#", "ISNULL(DPT.name,'') ")
                    strQ = strQ.Replace("#SEC_NAME#", "ISNULL(SEC.name,'') ")
                    strQ = strQ.Replace("#JOB_NAME#", "ISNULL(JOB.job_name,'') ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DATA_JOIN#", StrQDataJoin)
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")

                    'Gajanan [23-SEP-2019] -- Start    
                    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                    strQ = strQ.Replace("#APPROVER_NAME_WITHOUT_CODE#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                        "ELSE ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    'Gajanan [23-SEP-2019] -- End

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        strQ &= xAdvanceJoinQry
            End If

                End If

                strQ &= StrQCondition

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                        StrQDtFilters &= xDateFilterQry & " "
                End If
            End If

                strQ = strQ.Replace("#EXT_APPROVER#", "1")

                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@YES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Yes"))
                objDataOperation.AddParameter("@NO", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "No"))

                dsExtList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables(0).Copy)
                Else
                    dsList.Tables(0).Merge(dsExtList.Tables(0), True)
                End If

            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Public Function GetList(ByVal xDatabaseName As String, _
    '                        ByVal xUserUnkid As Integer, _
    '                        ByVal xYearUnkid As Integer, _
    '                        ByVal xCompanyUnkid As Integer, _
    '                        ByVal xPeriodStart As DateTime, _
    '                        ByVal xPeriodEnd As DateTime, _
    '                        ByVal xUserModeSetting As String, _
    '                        ByVal xOnlyApproved As Boolean, _
    '                        ByVal xIncludeIn_ActiveEmployee As Boolean, _
    '                        ByVal strTableName As String, _
    '                        Optional ByVal blnOnlyActive As Boolean = True, _
    '                        Optional ByVal blnIsvoid As Boolean = False, _
    '                        Optional ByVal strFilter As String = "") As DataSet

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        Dim xDateJoinQry, xDateFilterQry As String
    '        xDateJoinQry = "" : xDateFilterQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

    '        strQ = "SELECT " & _
    '                  "  lnloanapprover_master.lnapproverunkid " & _
    '                  ", lnloanapprover_master.lnlevelunkid " & _
    '                  ", lnapproverlevel_master.lnlevelname " & _
    '                  ", lnapproverlevel_master.priority " & _
    '                  ", lnloanapprover_master.approverempunkid " & _
    '                  ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') as name" & _
    '                  ", ISNULL(DPT.name,'') as departmentname " & _
    '                  ", ISNULL(SEC.name,'') as sectionname " & _
    '                  ", ISNULL(JOB.job_name,'') As  jobname " & _
    '                  ", lnloanapprover_master.isactive " & _
    '                  ", lnloanapprover_master.isvoid " & _
    '                  ", lnloanapprover_master.voiddatetime " & _
    '                  ", lnloanapprover_master.userunkid " & _
    '                  ", lnloanapprover_master.voiduserunkid " & _
    '                  ", lnloanapprover_master.voidreason " & _
    '                  ", lnloanapprover_master.isswap " & _
    '                  ", ISNULL(hrmsconfiguration..cfuser_master.username,'') AS mappeduser " & _
    '                " FROM lnloanapprover_master " & _
    '                  " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
    '                  " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid " & _
    '                  " LEFT JOIN " & _
    '                  "( " & _
    '                  "     SELECT " & _
    '                  "          departmentunkid " & _
    '                  "         ,sectionunkid " & _
    '                  "         ,employeeunkid " & _
    '                  "         ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                  "     FROM hremployee_transfer_tran " & _
    '                  "     WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                  ") AS ETT ON ETT.employeeunkid = hremployee_master.employeeunkid AND ETT.rno = 1 " & _
    '                  " LEFT JOIN hrdepartment_master AS DPT ON DPT.departmentunkid = ETT.departmentunkid " & _
    '                  " LEFT JOIN hrsection_master AS SEC ON SEC.sectionunkid = ETT.sectionunkid " & _
    '                  " LEFT JOIN " & _
    '                        "( " & _
    '                        "   SELECT " & _
    '                        "        jobunkid " & _
    '                        "       ,employeeunkid " & _
    '                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                        "   FROM hremployee_categorization_tran " & _
    '                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
    '                        ") AS ECT ON ECT.employeeunkid = hremployee_master.employeeunkid AND ECT.rno = 1 " & _
    '                  " LEFT JOIN hrjob_master AS JOB ON JOB.jobunkid = ECT.jobunkid " & _
    '                  " LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid " & _
    '                  " LEFT JOIN hrmsconfiguration..cfuser_master ON hrmsconfiguration..cfuser_master.userunkid = lnloan_approver_mapping.userunkid "

    '        If xDateJoinQry.Trim.Length > 0 Then
    '            strQ &= xDateJoinQry
    '        End If

    '        If blnOnlyActive Then
    '            strQ &= " WHERE lnloanapprover_master.isactive = 1 "
    '        Else
    '            strQ &= " WHERE lnloanapprover_master.isactive = 0 "
    '        End If

    '        strQ &= " AND lnloanapprover_master.isvoid =  " & IIf(blnIsvoid, 1, 0)

    '        strQ &= " AND lnloanapprover_master.isswap = 0"

    '        If xIncludeIn_ActiveEmployee = False Then
    '            If xDateFilterQry.Trim.Length > 0 Then
    '                strQ &= xDateFilterQry
    '            End If
    '        End If

    '        strQ &= " AND hremployee_master.isapproved = 1 "

    '        If strFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & strFilter
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'Nilay (01-Mar-2016) -- End

    

    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal blnOnlyActive As Boolean = True, _
    '                        Optional ByVal blnIsvoid As Boolean = False, _
    '                        Optional ByVal blnIncludeInactiveEmployee As String = "", _
    '                        Optional ByVal strEmployeeAsOnDate As String = "", _
    '                        Optional ByVal intUserID As Integer = 0, _
    '                        Optional ByVal strUserAccessLevelFilterString As String = "", _
    '                        Optional ByVal strFilter As String = "") As DataSet

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        strQ = "SELECT " & _
    '                  "  lnloanapprover_master.lnapproverunkid " & _
    '                  ", lnloanapprover_master.lnlevelunkid " & _
    '                  ", lnapproverlevel_master.lnlevelname " & _
    '                  ", lnapproverlevel_master.priority " & _
    '                  ", lnloanapprover_master.approverempunkid " & _
    '                  ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') as name" & _
    '                  ", ISNULL(hrdepartment_master.name,'') as departmentname " & _
    '                  ", ISNULL(hrsection_master.name,'') as sectionname " & _
    '                  ", ISNULL(hrjob_master.job_name,'') As  jobname " & _
    '                  ", lnloanapprover_master.isactive " & _
    '                  ", lnloanapprover_master.isvoid " & _
    '                  ", lnloanapprover_master.voiddatetime " & _
    '                  ", lnloanapprover_master.userunkid " & _
    '                  ", lnloanapprover_master.voiduserunkid " & _
    '                  ", lnloanapprover_master.voidreason " & _
    '                  ", lnloanapprover_master.isswap " & _
    '                  ", ISNULL(hrmsconfiguration..cfuser_master.username,'') AS mappeduser " & _
    '                  " FROM lnloanapprover_master " & _
    '                  " LEFT JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid " & _
    '                  " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid " & _
    '                  " LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hremployee_master.departmentunkid " & _
    '                  " LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hremployee_master.sectionunkid " & _
    '                  " LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hremployee_master.jobunkid " & _
    '                  " LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid " & _
    '                  " LEFT JOIN hrmsconfiguration..cfuser_master ON hrmsconfiguration..cfuser_master.userunkid = lnloan_approver_mapping.userunkid "

    '        If blnOnlyActive Then
    '            strQ &= " WHERE lnloanapprover_master.isactive = 1 "
    '        Else
    '            strQ &= " WHERE lnloanapprover_master.isactive = 0 "
    '        End If

    '        strQ &= " AND lnloanapprover_master.isvoid =  " & IIf(blnIsvoid, 1, 0)

    '        strQ &= " AND lnloanapprover_master.isswap = 0"


    '        If blnIncludeInactiveEmployee.Trim.Length <= 0 Then
    '            blnIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString()
    '        End If

    '        If CBool(blnIncludeInactiveEmployee) = False Then

    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
    '        End If
    '        strQ &= " AND hremployee_master.isapproved = 1 "


    '        If strFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & strFilter
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    'Nilay (10-Oct-2015) -- End

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Insert into Database Table lnloanapprover_master </purpose>
    Public Function Insert(ByVal dtAccess As DataTable) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Nilay (01-Mar-2016) -- Start
        'ENHANCEMENT - Implementing External Approval changes 
        'If isExistApprover(mintApproverEmpunkid, mintlnLevelunkid) = True Then
        If isExistApprover(mintApproverEmpunkid, mintlnLevelunkid, , , mblnIsExternalApprover) = True Then
            'Nilay (01-Mar-2016) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Approver already exists. Please define new Loan Approver.")
            Return False
        End If

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@lnlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnLevelunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsswap)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoidDateTime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidDateTime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalApprover.ToString)
            'Nilay (01-Mar-2016) -- End

            strQ = "INSERT INTO lnloanapprover_master (" & _
                        "  lnlevelunkid" & _
                        " ,approverempunkid " & _
                        " ,userunkid " & _
                        " ,isswap " & _
                        " ,isactive " & _
                        " ,isvoid " & _
                        " ,voiddatetime " & _
                        " ,voiduserunkid " & _
                        " ,voidreason " & _
                        " ,isexternalapprover " & _
                    ") VALUES (" & _
                        "  @lnlevelunkid" & _
                        " ,@approverempunkid " & _
                        " ,@userunkid " & _
                        " ,@isswap " & _
                        " ,@isactive " & _
                        " ,@isvoid " & _
                        " ,@voiddatetime " & _
                        " ,@voiduserunkid " & _
                        " ,@voidreason " & _
                        " ,@isexternalapprover " & _
                    "); SELECT @@identity"
            'Nilay (01-Mar-2016) -- [isexternalapprover]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintlnApproverunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertATApprover_Master(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objLoanApproverTran._LoanApproverunkid = mintlnApproverunkid
            objLoanApproverTran._TranDataTable = dtAccess
            objLoanApproverTran._UserId = mintUserunkid
            objLoanApproverTran._WebClientIP = mstrWebClientIP
            objLoanApproverTran._WebFormName = mstrWebFormName
            objLoanApproverTran._WebHostName = mstrWebHostName

            If objLoanApproverTran.Insert_Update_Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtLoanSchemeMapping IsNot Nothing AndAlso mdtLoanSchemeMapping.Rows.Count > 0 Then
                objLoanSchemeMapping._Approverunkid = mintlnApproverunkid
                objLoanSchemeMapping._dtLoanScheme = mdtLoanSchemeMapping
                objLoanSchemeMapping._Userunkid = mintUserunkid
                objLoanSchemeMapping._WebClientIP = mstrWebClientIP
                objLoanSchemeMapping._WebFormName = mstrWebFormName
                objLoanSchemeMapping._WebHostName = mstrWebHostName
                If objLoanSchemeMapping.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            'objLoanapproverMapping.GetData(mintApproverEmpunkid, -1, -1)
            'objLoanapproverMapping._Approvertranunkid = mintlnApproverunkid
            'objLoanapproverMapping._Userunkid = mintMappedUserId
            objLoanapproverMapping.GetData(mintlnApproverunkid, -1, -1)
            objLoanapproverMapping._Approvertranunkid = mintlnApproverunkid
            objLoanapproverMapping._MappedUserunkid = mintMappedUserId 'Nilay (05-May-2016) -- [_MappedUserunkid]
            'Nilay (01-Mar-2016) -- End

            'Nilay (05-May-2016) -- Start
            clsCommonATLog._WebClientIP = mstrWebClientIP
            clsCommonATLog._WebFormName = mstrWebFormName
            clsCommonATLog._WebHostName = mstrWebHostName
            'Nilay (05-May-2016) -- End

            If objLoanapproverMapping._Approvermappingunkid > 0 Then
                'Nilay (05-May-2016) -- Start
                'If objLoanapproverMapping.Update(objDataOperation) = False Then
                If objLoanapproverMapping.Update(objDataOperation, mintUserunkid) = False Then
                    'Nilay (05-May-2016) -- End
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                'Nilay (05-May-2016) -- Start
                'If objLoanapproverMapping.Insert(objDataOperation) = False Then
                If objLoanapproverMapping.Insert(objDataOperation, mintUserunkid) = False Then
                    'Nilay (05-May-2016) -- End
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Update Database Table lnloanapprover_master </purpose>
    Public Function Update(ByVal dtAccess As DataTable, Optional ByVal dtMapping As DataTable = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If isExistApprover(mintApproverEmpunkid, mintlnLevelunkid, mintlnApproverunkid) = True Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Approver already exists. Please define new Loan Approver.")
            Return False
        End If

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@lnapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid.ToString)
            objDataOperation.AddParameter("@lnlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnLevelunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsswap.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoidDateTime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidDateTime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalApprover.ToString)
            'Nilay (01-Mar-2016) -- End

            strQ = "UPDATE lnloanapprover_master  SET " & _
                    " lnlevelunkid = @lnlevelunkid " & _
                    " ,approverempunkid = @approverempunkid " & _
                    " ,userunkid = @userunkid " & _
                    " ,isswap = @isswap " & _
                    " ,isactive = @isactive " & _
                    " ,isvoid = @isvoid " & _
                    " ,voiddatetime = @voiddatetime " & _
                    " ,voiduserunkid = @voiduserunkid " & _
                    " ,voidreason = @voidreason " & _
                    " ,isexternalapprover = @isexternalapprover " & _
                " WHERE lnapproverunkid = @lnapproverunkid"
            'Nilay (01-Mar-2016) -- [isexternalapprover]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertATApprover_Master(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objLoanApproverTran._LoanApproverunkid = mintlnApproverunkid
            objLoanApproverTran._TranDataTable = dtAccess
            objLoanApproverTran._UserId = mintUserunkid
            objLoanApproverTran._WebClientIP = mstrWebClientIP
            objLoanApproverTran._WebFormName = mstrWebFormName
            objLoanApproverTran._WebHostName = mstrWebHostName

            If objLoanApproverTran.Insert_Update_Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtLoanSchemeMapping IsNot Nothing AndAlso mdtLoanSchemeMapping.Rows.Count > 0 Then
                objLoanSchemeMapping._Approverunkid = mintlnApproverunkid
                objLoanSchemeMapping._dtLoanScheme = mdtLoanSchemeMapping
                objLoanSchemeMapping._Userunkid = mintUserunkid
                objLoanSchemeMapping._WebClientIP = mstrWebClientIP
                objLoanSchemeMapping._WebFormName = mstrWebFormName
                objLoanSchemeMapping._WebHostName = mstrWebHostName

                If objLoanSchemeMapping.Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objLoanapproverMapping.GetData(mintlnApproverunkid, -1, -1)
            objLoanapproverMapping._Approvertranunkid = mintlnApproverunkid
            objLoanapproverMapping._MappedUserunkid = mintMappedUserId 'Nilay (05-May-2016) -- [_MappedUserunkid]

            If objLoanapproverMapping._Approvermappingunkid > 0 Then
                'Nilay (05-May-2016) -- Start
                'If objLoanapproverMapping.Update(objDataOperation) = False Then
                If objLoanapproverMapping.Update(objDataOperation, mintUserunkid) = False Then
                    'Nilay (05-May-2016) -- End
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                'Nilay (05-May-2016) -- Start
                'If objLoanapproverMapping.Insert(objDataOperation) = False Then
                If objLoanapproverMapping.Insert(objDataOperation, mintUserunkid) = False Then
                    'Nilay (05-May-2016) -- End
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Delete Database Table entries </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        End If

        Try
            dsList = clsCommonATLog.GetChildList(objDataOperation, "lnloanapprover_tran", "lnloanapproverunkid", intUnkid)

            strQ = "UPDATE lnloanapprover_tran SET " & _
                    "  isvoid = @isvoid " & _
                    " ,voiddatetime = @voiddatetime " & _
                    " ,voiduserunkid = @voiduserunkid " & _
                    " ,voidreason = @voidreason " & _
                " WHERE lnapprovertranunkid = @lnapprovertranunkid AND isvoid = 0 "

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each drRow As DataRow In dsList.Tables(0).Rows
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidDateTime)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserunkid.ToString)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason)
                    objDataOperation.AddParameter("@lnapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, drRow.Item("lnapprovertranunkid"))

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Nilay (07-Feb-2016) -- Start
                    'objLoanApproverTran._LoanApproverTranunkid = CInt(drRow.Item("lnapprovertranunkid"))
                    objLoanApproverTran._LoanApproverTranunkid(objDataOperation) = CInt(drRow.Item("lnapprovertranunkid"))
                    'Nilay (07-Feb-2016) -- End
                    objLoanApproverTran._LoanApproverunkid = CInt(drRow.Item("lnloanapproverunkid"))
                    objLoanApproverTran._UserId = mintVoidUserunkid
                    objLoanApproverTran._WebClientIP = mstrWebClientIP
                    objLoanApproverTran._WebFormName = mstrWebFormName
                    objLoanApproverTran._WebHostName = mstrWebHostName

                    If objLoanApproverTran.InsertATApprover_Tran(objDataOperation, 3, CInt(drRow.Item("employeeunkid"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next
            End If

            Dim objSchemeMapping As New clsapprover_scheme_mapping
            dsList = objLoanSchemeMapping.GetList("List", True, intUnkid)
            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dsList.Tables(0).Rows
                    'Nilay (05-May-2016) -- Start
                    'objSchemeMapping._Loanschememappingunkid = CInt(dr("loanschememappingunkid"))
                    objSchemeMapping._Loanschememappingunkid(objDataOperation) = CInt(dr("loanschememappingunkid"))
                    'Nilay (05-May-2016) -- End
                    objSchemeMapping._Isvoid = mblnIsvoid
                    objSchemeMapping._Voiddatetime = mdtVoidDateTime
                    objSchemeMapping._Voiduserunkid = mintVoidUserunkid
                    objSchemeMapping._Voidreason = mstrVoidReason
                    objSchemeMapping._WebClientIP = mstrWebClientIP
                    objSchemeMapping._WebFormName = mstrWebFormName
                    objSchemeMapping._WebHostName = mstrWebHostName

                    If objSchemeMapping.Delete(objDataOperation, dr) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

            End If

            dsList = Nothing
            strQ = " SELECT approvermappingunkid FROM lnloan_approver_mapping WHERE approvertranunkid = '" & intUnkid & "' "
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then

                'Nilay (05-May-2016) -- Start
                clsCommonATLog._WebClientIP = mstrWebClientIP
                clsCommonATLog._WebFormName = mstrWebFormName
                clsCommonATLog._WebHostName = mstrWebHostName
                'Nilay (05-May-2016) -- End

                For Each drRow As DataRow In dsList.Tables("List").Rows
                    'Nilay (05-May-2016) -- Start
                    'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_approver_mapping", "approvermappingunkid", CInt(drRow.Item("approvermappingunkid")), False) = False Then
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_approver_mapping", "approvermappingunkid", CInt(drRow.Item("approvermappingunkid")), False, mintVoidUserunkid) = False Then
                        'Nilay (05-May-2016) -- End
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = " DELETE FROM lnloan_approver_mapping WHERE approvermappingunkid = '" & drRow.Item("approvermappingunkid") & "' "
                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            strQ = "UPDATE lnloanapprover_master SET " & _
                    "  isvoid = @isvoid " & _
                    " ,voiddatetime = @voiddatetime " & _
                    " ,voiduserunkid = @voiduserunkid " & _
                    " ,voidreason = @voidreason " & _
                " WHERE lnapproverunkid = @lnapproverunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidDateTime)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason)
            objDataOperation.AddParameter("@lnapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Nilay (05-May-2016) -- Start
            '_lnApproverunkid = intUnkid
            _lnApproverunkid(objDataOperation) = intUnkid
            'Nilay (05-May-2016) -- End

            If InsertATApprover_Master(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then
                dsList.Dispose()
            End If
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExistApprover(ByVal intLoanApproverEmpunkid As Integer, ByVal intApproveLevelunkid As Integer, _
                                    Optional ByVal intunkid As Integer = -1, Optional ByRef intApproverMstId As Integer = 0, _
                                    Optional ByVal blnIsExternalApprover As Boolean = False) As Boolean
        'Nilay (01-Mar-2016) -- [blnIsExternalApprover]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                    "  lnloanapprover_master.lnapproverunkid " & _
                    " ,lnloanapprover_master.lnlevelunkid " & _
                    " ,lnloanapprover_master.approverempunkid " & _
                    " ,lnloanapprover_master.userunkid " & _
                    " ,lnloanapprover_master.isvoid " & _
                    " ,lnloanapprover_master.voiddatetime " & _
                    " ,lnloanapprover_master.voiduserunkid " & _
                    " ,lnloanapprover_master.voidreason " & _
                    " ,lnloanapprover_master.isactive " & _
                    " FROM lnloanapprover_master " & _
                   " WHERE    lnloanapprover_master.approverempunkid = @approverempunkid " & _
                        " AND lnloanapprover_master.lnlevelunkid = @levelunkid " & _
                        " AND lnloanapprover_master.isvoid = 0 " & _
                        " AND lnloanapprover_master.isswap = 0 " & _
                        " AND isexternalapprover = @isexternalapprover "
            'Nilay (01-Mar-2016) -- [isexternalapprover]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproveLevelunkid)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanApproverEmpunkid)
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExternalApprover)
            'Nilay (01-Mar-2016) -- End

            If intunkid > 0 Then
                strQ &= " AND lnloanapprover_master.lnapproverunkid <> @lnapproverunkid"
                objDataOperation.AddParameter("@lnapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intApproverMstId = dsList.Tables(0).Rows(0)("lnapproverunkid")
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistApprover; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ' <summary>
    ' Modify By: Nilay
    ' </summary>
    ' <purpose> Assign all Property variable </purpose>

    'Nilay (21-Oct-2015) -- Start
    'ENHANCEMENT : NEW LOAN Given By Rutta
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim blnIsUsed As Boolean = False
        Dim exForce As Exception
        Dim dsTable As DataSet = Nothing
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "       'SELECT 1 FROM ' + INFORMATION_SCHEMA.TABLES.TABLE_NAME + ' WHERE ' + COLUMN_NAME + ' = @id AND isvoid = 0 ' AS SQL_QRY " & _
                    " FROM INFORMATION_SCHEMA.TABLES " & _
                    "       JOIN INFORMATION_SCHEMA.COLUMNS ON INFORMATION_SCHEMA.TABLES.TABLE_NAME = INFORMATION_SCHEMA.COLUMNS.TABLE_NAME " & _
                    " WHERE TABLES.TABLE_NAME like 'ln%' AND TABLES.TABLE_NAME NOT IN('lnloan_approver_mapping','lnloanapprover_master','lnloan_approver_tran','lnloanapprover_tran','lnapprover_scheme_mapping') " & _
                    " AND COLUMN_NAME IN ('lnapproverunkid','approverunkid','approverid','approvertranunkid','lnloanapproverunkid') "

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""

            For Each dRow As DataRow In dsList.Tables("List").Rows

                strQ = dRow.Item("SQL_QRY").ToString.Replace("@id", intUnkid)

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        End Try
    End Function
    'Nilay (21-Oct-2015) -- End

    

    ' <summary>
    ' Modify By: Nilay
    ' </summary>
    ' <purpose> Assign all Property variable </purpose>

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Public Function getListForCombo(Optional ByVal strListName As String = "List", _
                                    Optional ByVal mblFlag As Boolean = False, _
                                    Optional ByVal blnIsActive As Boolean = True, _
                                    Optional ByVal mblnShowLevels As Boolean = False) As DataSet

        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception

        Try
            Dim strFinal As String = String.Empty
            Dim strQCondition As String = String.Empty
            Dim strSelect As String = String.Empty
            Dim strCombine As String = String.Empty

            If mblFlag = True Then
                strSelect = "SELECT  0 AS lnapproverunkid, 0 AS lnempapproverunkid, ' ' +  @lnapprovername  as name, 0 AS lnlevelunkid, '' AS isexternalapprover "
                strSelect &= " UNION "
            End If

            strQ &= "SELECT " & _
                    "   lnloanapprover_master.lnapproverunkid " & _
                    "  ,lnloanapprover_master.approverempunkid AS lnempapproverunkid "

            If mblnShowLevels = True Then
                strQ &= "  ,#APPROVER_NAME# + ' - ' + ISNULL(lnapproverlevel_master.lnlevelname,'') AS name "
            Else
                strQ &= "  ,#APPROVER_NAME# AS name "
            End If

            strQ &= "  ,lnloanapprover_master.lnlevelunkid AS lnlevelunkid " & _
                    "  ,lnloanapprover_master.isexternalapprover AS isexternalapprover " & _
                    "FROM lnloanapprover_master " & _
                        " #EMPLOYEE_JOIN# "

            If mblnShowLevels = True Then
                strQ &= " JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid "
            End If

            strFinal = strQ

            strCombine = strSelect
            strCombine &= strQ
            strQ = strCombine
            strCombine = ""

            strQCondition &= " WHERE   lnloanapprover_master.isvoid = 0 AND lnloanapprover_master.isswap = 0 " & _
                            " AND lnloanapprover_master.isactive = " & IIf(blnIsActive, 1, 0) & ""

            strQCondition &= " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            strQ &= strQCondition

            'strQ &= " ORDER BY lnapproverunkid, name ASC "

            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid AND hremployee_master.isapproved = 1 ")
            strQ = strQ.Replace("#EXT_APPROVER#", "0")

            objDataOperation.ClearParameters()

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.AddParameter("@lnapprovername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            objDataOperation.AddParameter("@lnapprovername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            'Gajanan [23-SEP-2019] -- End

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow As DataRow In dsCompany.Tables("Company").Rows
                strQ = strFinal
                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = lnloanapprover_master.approverempunkid ")
                Else
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master ON hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                End If

                strQ &= strQCondition
                strQ = strQ.Replace("#EXT_APPROVER#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()

                'Gajanan [23-SEP-2019] -- Start    
                'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                'objDataOperation.AddParameter("@lnapprovername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
                objDataOperation.AddParameter("@lnapprovername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
                'Gajanan [23-SEP-2019] -- End

                dsExtList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables(strListName).Copy)
                Else
                    dsList.Tables(strListName).Merge(dsExtList.Tables(strListName), True)
                End If
            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables(strListName), "", "lnlevelunkid, name", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    'Public Function getListForCombo(Optional ByVal strListName As String = "List", _
    '                                Optional ByVal mblFlag As Boolean = False, _
    '                                Optional ByVal blnIsActive As Boolean = True, _
    '                                Optional ByVal mblnShowLevels As Boolean = False) As DataSet
    '    Dim dsList As New DataSet
    '    Dim objDataOperation As New clsDataOperation
    '    Dim strQ As String = String.Empty
    '    Dim exForce As Exception
    '    Try
    '        If mblFlag = True Then
    '            strQ = "SELECT  0 as lnapproverunkid, 0 as lnempapproverunkid, ' ' +  @lnapprovername  as name UNION "
    '        End If
    '        strQ &= "SELECT " & _
    '                "   lnloanapprover_master.lnapproverunkid " & _
    '                "  ,lnloanapprover_master.approverempunkid AS lnempapproverunkid "
    '        If mblnShowLevels = True Then
    '            strQ &= ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') +' - '+ lnlevelname AS name "
    '        Else
    '            strQ &= ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS name "
    '        End If
    '        strQ &= " FROM lnloanapprover_master " & _
    '                " JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid "
    '        If mblnShowLevels = True Then
    '            strQ &= " JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid "
    '        End If

    '        'Nilay (10-Oct-2015) -- Start
    '        'strQ &= " WHERE lnloanapprover_master.isvoid = 0 AND lnloanapprover_master.isswap = 0 AND lnloanapprover_master.isactive = " & IIf(blnIsActive, 1, 0) & "  ORDER BY name "
    '        strQ &= " WHERE lnloanapprover_master.isvoid = 0 AND lnloanapprover_master.isswap = 0 AND lnloanapprover_master.isactive = " & IIf(blnIsActive, 1, 0) & "  ORDER BY lnapproverunkid, name ASC "
    '        'Nilay (10-Oct-2015) -- End

    '        objDataOperation.AddParameter("@lnapprovername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
    '        dsList = objDataOperation.ExecQuery(strQ, strListName)
    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        Return dsList
    '    Catch ex As Exception
    '        DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
    '        Return Nothing
    '    Finally
    '        exForce = Nothing
    '        objDataOperation = Nothing
    '        dsList.Dispose()
    '        dsList = Nothing
    '    End Try

    'End Function
    'Nilay (01-Mar-2016) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function MakeActiveInActive(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@lnapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)

            strQ = "UPDATE lnloanapprover_master  SET " & _
                      " isactive = @isactive " & _
                      " WHERE lnapproverunkid = @lnapproverunkid AND isvoid = 0 AND lnloanapprover_master.isswap = 0 "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If InsertATApprover_Master(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: MakeActiveInActive , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    '' 

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    'Public Function GetLevelFromLoanApprover(Optional ByVal blnFlag As Boolean = True, Optional ByVal intLoanEmpApproveunkid As Integer = -1) As DataSet
    Public Function GetLevelFromLoanApprover(ByVal intLoanApproverEmpunkid As Integer, ByVal blnIsExternalApprover As Boolean, Optional ByVal blnFlag As Boolean = True) As DataSet
        'Nilay (01-Mar-2016) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            Dim strApproverunkid As String = ""
            strApproverunkid = GetApproverUnkid(intLoanApproverEmpunkid, blnIsExternalApprover, objDataOperation)

            If blnFlag Then
                strQ = " Select 0 as lnlevelunkid , @name as name UNION "
            End If

            strQ &= " SELECT  lnloanapprover_master.lnlevelunkid,lnlevelname as name " & _
                      " FROM lnloanapprover_master " & _
                      " JOIN lnapproverlevel_master ON lnloanapprover_master.lnlevelunkid = lnapproverlevel_master.lnlevelunkid AND lnapproverlevel_master.isactive = 1 " & _
                      " WHERE  lnloanapprover_master.isvoid = 0 AND lnloanapprover_master.isactive = 1 AND lnloanapprover_master.isswap = 0 "

            If intLoanApproverEmpunkid > 0 Then
                strQ &= " AND approverempunkid = " & intLoanApproverEmpunkid
            End If

            If strApproverunkid.Trim.Length > 0 Then
                strQ &= " AND lnloanapprover_master.lnapproverunkid IN (" & strApproverunkid & ") "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLevelFromLoanApprover", mstrMessage)
        End Try
        Return dsList
    End Function

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>

    Public Function GetEmployeeFromLoanApprover(ByVal xDatabaseName As String, _
                                                ByVal xUserUnkid As Integer, _
                                                ByVal xYearUnkid As Integer, _
                                                ByVal xCompanyUnkid As Integer, _
                                                ByVal xPeriodStart As DateTime, _
                                                ByVal xPeriodEnd As DateTime, _
                                                ByVal xUserModeSetting As String, _
                                                ByVal xOnlyApproved As Boolean, _
                                                ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                                ByVal strTableName As String, _
                                                ByVal intLoanEmpApproveunkid As Integer, _
                                                ByVal intLevelId As Integer, _
                                                ByVal blnIsExternalApprover As Boolean) As DataSet
        'Nilay (01-Mar-2016) -- [blnIsExternalApprover]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            Dim strApproverUnkids As String = ""
            strApproverUnkids = GetApproverUnkid(intLoanEmpApproveunkid, blnIsExternalApprover, objDataOperation)
            'Nilay (01-Mar-2016) -- End

            strQ = "SELECT  " & _
                      " 0 as 'select' " & _
                      ", lnloanapprover_master.lnapproverunkid " & _
                      ", lnloanapprover_tran.lnapprovertranunkid " & _
                      ", lnloanapprover_master.approverempunkid " & _
                      ", lnloanapprover_tran.employeeunkid " & _
                      ", ISNULL(hremployee_master.employeecode,'') as employeecode " & _
                      ", ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.othername,'')  + ' '  + ISNULL(hremployee_master.surname,'') as employeename " & _
                      ", hremployee_master.gender " & _
                      ", hremployee_master.maritalstatusunkid " & _
                      ", hremployee_master.employmenttypeunkid " & _
                      ", hremployee_master.paytypeunkid " & _
                      ", hremployee_master.paypointunkid " & _
                      ", hremployee_master.nationalityunkid " & _
                      ", hremployee_master.religionunkid " & _
                      ", ETT.stationunkid " & _
                      ", ETT.deptgroupunkid " & _
                      ", ETT.departmentunkid " & _
                      ", ISNULL(ETT.sectiongroupunkid,0) AS sectiongroupunkid " & _
                      ", ETT.sectionunkid " & _
                      ", ISNULL(ETT.unitgroupunkid,0) AS unitgroupunkid " & _
                      ", ETT.unitunkid " & _
                      ", ISNULL(ETT.teamunkid,0) AS teamunkid " & _
                      ", ETT.classgroupunkid " & _
                      ", ETT.classunkid " & _
                      ", ECT.jobgroupunkid " & _
                      ", ECT.jobunkid " & _
                      ", GRD.gradegroupunkid " & _
                      ", GRD.gradeunkid " & _
                      ", GRD.gradelevelunkid " & _
                      ", CC.cctranheadvalueid " & _
                      ", lnapproverlevel_master.lnlevelunkid " & _
                      ", lnapproverlevel_master.lnlevelname " & _
                      ", lnapproverlevel_master.priority " & _
                      ", lnloanapprover_master.isexternalapprover " & _
                " FROM  lnloanapprover_master " & _
                      " JOIN lnloanapprover_tran ON lnloanapprover_master.lnapproverunkid = lnloanapprover_tran.lnloanapproverunkid AND lnloanapprover_tran.isvoid = 0  " & _
                      " JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_tran.employeeunkid " & _
                      " JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid  " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "        stationunkid " & _
                      "       ,deptgroupunkid " & _
                      "       ,departmentunkid " & _
                      "       ,sectiongroupunkid " & _
                      "       ,sectionunkid " & _
                      "       ,unitgroupunkid " & _
                      "       ,unitunkid " & _
                      "       ,teamunkid " & _
                      "       ,classgroupunkid " & _
                      "       ,classunkid " & _
                      "       ,employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "   FROM hremployee_transfer_tran " & _
                      "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS ETT ON ETT.employeeunkid = hremployee_master.employeeunkid AND ETT.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "        jobgroupunkid " & _
                      "       ,jobunkid " & _
                      "       ,employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "   FROM hremployee_categorization_tran " & _
                      "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS ECT ON ECT.employeeunkid = hremployee_master.employeeunkid AND ECT.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "   SELECT " & _
                      "        gradegroupunkid " & _
                      "       ,gradeunkid " & _
                      "       ,gradelevelunkid " & _
                      "       ,employeeunkid " & _
                      "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                      "   FROM prsalaryincrement_tran " & _
                      "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS GRD ON GRD.employeeunkid = hremployee_master.employeeunkid AND GRD.rno = 1 " & _
                      " LEFT JOIN " & _
                      "( " & _
                      "     SELECT " & _
                      "         cctranheadvalueid " & _
                      "        ,employeeunkid " & _
                      "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                      "     FROM hremployee_cctranhead_tran " & _
                      "     WHERE istransactionhead = 0 AND isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                      ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "
            'Nilay (01-Mar-2016) -- [isexternalapprover]

            strQ &= " WHERE lnloanapprover_master.isvoid = 0 AND lnloanapprover_master.isactive = 1 AND lnloanapprover_master.approverempunkid = " & intLoanEmpApproveunkid

            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            If strApproverUnkids.Trim.Length > 0 Then
                strQ &= " AND lnloanapprover_master.lnapproverunkid IN (" & strApproverUnkids & ") "
            End If
            'Nilay (01-Mar-2016) -- End

            If intLevelId >= 0 Then
                strQ &= " AND lnloanapprover_master.lnlevelunkid = " & intLevelId
            End If

            strQ &= " AND hremployee_master.isapproved = 1 "

            strQ &= " AND lnloanapprover_master.isswap = 0 "

            strQ &= " ORDER By lnlevelname,isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeFromLoanApprover", mstrMessage)
        End Try
        Return dsList
    End Function

    'Public Function GetEmployeeFromLoanApprover(ByVal intLoanEmpApproveunkid As Integer, ByVal intLevelId As Integer) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try
    '        Dim objDataOperation As New clsDataOperation

    '        strQ = "SELECT  " & _
    '                  " 0 as 'select' " & _
    '                  ", lnloanapprover_master.lnapproverunkid " & _
    '                  ", lnloanapprover_tran.lnapprovertranunkid " & _
    '                  ", lnloanapprover_master.approverempunkid " & _
    '                  ", lnloanapprover_tran.employeeunkid " & _
    '                  ", ISNULL(hremployee_master.employeecode,'') as employeecode " & _
    '                  ", ISNULL(hremployee_master.firstname,'') + ' '  + ISNULL(hremployee_master.othername,'')  + ' '  + ISNULL(hremployee_master.surname,'') as employeename " & _
    '                  ", hremployee_master.gender " & _
    '                  ", hremployee_master.maritalstatusunkid " & _
    '                  ", hremployee_master.employmenttypeunkid " & _
    '                  ", hremployee_master.paytypeunkid " & _
    '                  ", hremployee_master.paypointunkid " & _
    '                  ", hremployee_master.nationalityunkid " & _
    '                  ", hremployee_master.religionunkid " & _
    '                  ", hremployee_master.stationunkid " & _
    '                  ", hremployee_master.deptgroupunkid " & _
    '                  ", hremployee_master.departmentunkid " & _
    '                  ", ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                  ", hremployee_master.sectionunkid " & _
    '                  ", ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
    '                  ", hremployee_master.unitunkid " & _
    '                  ", hremployee_master.jobgroupunkid " & _
    '                  ", hremployee_master.jobunkid " & _
    '                  ", hremployee_master.gradegroupunkid " & _
    '                  ", hremployee_master.gradeunkid " & _
    '                  ", hremployee_master.gradelevelunkid " & _
    '                  ", hremployee_master.classgroupunkid " & _
    '                  ", hremployee_master.classunkid " & _
    '                  ", hremployee_master.costcenterunkid " & _
    '                  ", ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
    '                  ", lnapproverlevel_master.lnlevelunkid " & _
    '                  ", lnapproverlevel_master.lnlevelname " & _
    '                  ", lnapproverlevel_master.priority " & _
    '            " FROM  lnloanapprover_master " & _
    '                  " JOIN lnloanapprover_tran ON lnloanapprover_master.lnapproverunkid = lnloanapprover_tran.lnloanapproverunkid AND lnloanapprover_tran.isvoid = 0  " & _
    '                  " JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_tran.employeeunkid " & _
    '                  " JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid  " & _
    '                  " WHERE lnloanapprover_master.isvoid = 0 AND lnloanapprover_master.isactive = 1 AND lnloanapprover_master.approverempunkid = " & intLoanEmpApproveunkid

    '        If intLevelId >= 0 Then
    '            strQ &= " AND lnloanapprover_master.lnlevelunkid = " & intLevelId
    '        End If

    '        strQ &= " AND hremployee_master.isapproved = 1 "

    '        strQ &= " AND lnloanapprover_master.isswap = 0 "

    '        strQ &= " ORDER By lnlevelname,isnull(hremployee_master.firstname,'') + ' ' + isnull(hremployee_master.surname,'') "

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetEmployeeFromLoanApprover", mstrMessage)
    '    End Try
    '    Return dsList
    'End Function

    'Nilay (10-Oct-2015) -- End

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ' <summary>
    ' Modify By: Pinkal
    ' </summary>
    ' <purpose> Assign all Property variable </purpose>

    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Public Function GetEmployeeApprover(ByVal xDatabaseName As String, _
                                        ByVal xUserUnkid As Integer, _
                                        ByVal xYearUnkid As Integer, _
                                        ByVal xCompanyUnkid As Integer, _
                                        ByVal intEmployeeID As Integer, _
                                        ByVal blnLoanApproverForLoanScheme As Boolean, _
                                        Optional ByVal intSchemeId As Integer = -1, _
                                        Optional ByVal mstrFilter As String = "" _
                                        ) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            Dim strQFinal As String = String.Empty
            Dim strQCondition As String = String.Empty

            objDataOperation.ClearParameters()

            strQ &= " SELECT " & _
                      "  lnloanapprover_master.lnapproverunkid " & _
                      ", lnloanapprover_tran.lnapprovertranunkid " & _
                      ", lnloanapprover_master.approverempunkid " & _
                      ", lnloanapprover_tran.employeeunkid " & _
                      ", #CODE# AS employeecode " & _
                      ", #APPROVER_NAME# AS employeename " & _
                      ", lnapproverlevel_master.lnlevelunkid " & _
                      ", lnapproverlevel_master.lnlevelname " & _
                      ", lnapproverlevel_master.priority " & _
                      ", lnloanapprover_master.isexternalapprover " & _
                          ", lnloan_approver_mapping.userunkid AS MappedUserID " & _
                      " FROM lnloanapprover_tran " & _
                      " JOIN lnloanapprover_master ON lnloanapprover_tran.lnloanapproverunkid = lnloanapprover_master.lnapproverunkid " & _
                      "     AND lnloanapprover_master.isvoid = 0 AND lnloanapprover_master.isactive = 1 AND lnloanapprover_master.isswap = 0 " & _
                      " #EMPLOYEE_JOIN# " & _
                      " JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid "
            'Nilay (08-Dec-2016) -- [MappedUserID]
            'Nilay (30-May-2016) -- [lnloanapprover_master.isexternalapprover]

            If blnLoanApproverForLoanScheme = True And intSchemeId > 0 Then

                strQ &= "  JOIN lnapprover_scheme_mapping ON lnapprover_scheme_mapping.approverunkid = lnloanapprover_master.lnapproverunkid AND lnapprover_scheme_mapping.isvoid = 0 " & _
                            "  JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnapprover_scheme_mapping.loanschemeunkid AND lnapprover_scheme_mapping.loanschemeunkid = @loanschemeunkid "

                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSchemeId.ToString)

            End If

            'Nilay (08-Dec-2016) -- Start
            'Issue #7: If same user is the approver and posting loan, loan should by pass Approval process
            strQ &= " JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid "
            'Nilay (08-Dec-2016) -- End

            strQFinal = strQ

            strQCondition = " WHERE lnloanapprover_tran.isvoid = 0  AND lnloanapprover_tran.employeeunkid = " & intEmployeeID

            strQCondition &= " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            If mstrFilter.Trim.Length > 0 Then
                strQCondition &= " AND " & mstrFilter.Trim
            End If

            strQ &= strQCondition

            strQ = strQ.Replace("#CODE#", "ISNULL(hremployee_master.employeecode,'') ")
            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid AND hremployee_master.isapproved = 1 ")
            strQ = strQ.Replace("#EXT_APPROVER#", "0")

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow In dsCompany.Tables("Company").Rows
                strQ = strQFinal

                If dRow("dbname").ToString.Trim.Length <= 0 Then
                    strQ = strQ.Replace("#CODE#", "'' ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = lnloanapprover_master.approverempunkid ")

                Else
                    strQ = strQ.Replace("#CODE#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN '' " & _
                                                       "ELSE ISNULL(hremployee_master.employeecode,'') END ")
                    strQ = strQ.Replace("#APPROVER_NAME#", "CASE WHEN ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') = ' ' THEN ISNULL(cfuser_master.username,'') " & _
                                                                "ELSE ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') END ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloanapprover_master.approverempunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                End If

                strQ &= strQCondition
                strQ = strQ.Replace("#EXT_APPROVER#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSchemeId.ToString)

                dsExtList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables("List").Copy)
                Else
                    dsList.Tables("List").Merge(dsExtList.Tables("List"), True)
                End If
            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "priority,employeename", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeApprover", mstrMessage)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
        If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
            Return dsList.Tables("List")
        Else
            Return Nothing
        End If
    End Function

    'Public Function GetEmployeeApprover(ByVal xDatabaseName As String, _
    '                                    ByVal xUserUnkid As Integer, _
    '                                    ByVal xYearUnkid As Integer, _
    '                                    ByVal xCompanyUnkid As Integer, _
    '                                    ByVal intEmployeeID As Integer, _
    '                                    ByVal blnLoanApproverForLoanScheme As Boolean, _
    '                                    Optional ByVal intSchemeId As Integer = -1, _
    '                                    Optional ByVal mstrFilter As String = "") As DataTable
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try
    '        Dim objDataOperation As New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        strQ = " SELECT " & _
    '                  "  lnloanapprover_master.lnapproverunkid " & _
    '                  ", lnloanapprover_tran.lnapprovertranunkid " & _
    '                  ", lnloanapprover_master.approverempunkid " & _
    '                  ", lnloanapprover_tran.employeeunkid " & _
    '                  ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
    '                  ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
    '                  ", lnapproverlevel_master.lnlevelunkid " & _
    '                  ", lnapproverlevel_master.lnlevelname " & _
    '                  ", lnapproverlevel_master.priority " & _
    '               " FROM lnloanapprover_tran " & _
    '                  " JOIN lnloanapprover_master ON lnloanapprover_tran.lnloanapproverunkid = lnloanapprover_master.lnapproverunkid " & _
    '                  "     AND lnloanapprover_master.isvoid = 0 AND lnloanapprover_master.isactive = 1 " & _
    '                  " JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid " & _
    '                  " JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid "

    '        If blnLoanApproverForLoanScheme = True And intSchemeId > 0 Then

    '            strQ &= "  JOIN lnapprover_scheme_mapping ON lnapprover_scheme_mapping.approverunkid = lnloanapprover_master.lnapproverunkid AND lnapprover_scheme_mapping.isvoid = 0 " & _
    '                        "  JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnapprover_scheme_mapping.loanschemeunkid AND lnapprover_scheme_mapping.loanschemeunkid = @loanschemeunkid "

    '            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSchemeId.ToString)

    '        End If


    '        strQ &= " WHERE lnloanapprover_tran.isvoid = 0  AND lnloanapprover_tran.employeeunkid = " & intEmployeeID

    '        If mstrFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & mstrFilter.Trim
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetEmployeeApprover", mstrMessage)
    '    End Try
    '    If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
    '        Return dsList.Tables("List")
    '    Else
    '        Return Nothing
    '    End If
    'End Function
    'Nilay (01-Mar-2016) -- End



    'Public Function GetEmployeeApprover(ByVal intEmployeeID As Integer, _
    '                                    Optional ByVal blnLoanApproverForLoanScheme As String = "", _
    '                                    Optional ByVal intSchemeId As Integer = -1, _
    '                                    Optional ByVal mstrFilter As String = "") As DataTable
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try
    '        Dim objDataOperation As New clsDataOperation
    '        objDataOperation.ClearParameters()

    '        strQ = " SELECT " & _
    '                  "  lnloanapprover_master.lnapproverunkid " & _
    '                  ", lnloanapprover_tran.lnapprovertranunkid " & _
    '                  ", lnloanapprover_master.approverempunkid " & _
    '                  ", lnloanapprover_tran.employeeunkid " & _
    '                  ", ISNULL(hremployee_master.employeecode, '') AS employeecode " & _
    '                  ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename " & _
    '                  ", lnapproverlevel_master.lnlevelunkid " & _
    '                  ", lnapproverlevel_master.lnlevelname " & _
    '                  ", lnapproverlevel_master.priority " & _
    '                  " FROM lnloanapprover_tran " & _
    '                  " JOIN lnloanapprover_master ON lnloanapprover_tran.lnloanapproverunkid = lnloanapprover_master.lnapproverunkid " & _
    '                  " AND lnloanapprover_master.isvoid = 0 AND lnloanapprover_master.isactive = 1 " & _
    '                  " JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid " & _
    '                  " JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid "

    '        If blnLoanApproverForLoanScheme.Trim.Length <= 0 Then
    '            blnLoanApproverForLoanScheme = ConfigParameter._Object._IsLoanApprover_ForLoanScheme.ToString()
    '        End If

    '        If CBool(blnLoanApproverForLoanScheme) = True And intSchemeId > 0 Then

    '            strQ &= "  JOIN lnapprover_scheme_mapping ON lnapprover_scheme_mapping.approverunkid = lnloanapprover_master.lnapproverunkid AND lnapprover_scheme_mapping.isvoid = 0 " & _
    '                        "  JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnapprover_scheme_mapping.loanschemeunkid AND lnapprover_scheme_mapping.loanschemeunkid = @loanschemeunkid "

    '            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSchemeId.ToString)

    '        End If


    '        strQ &= " WHERE lnloanapprover_tran.isvoid = 0  AND lnloanapprover_tran.employeeunkid = " & intEmployeeID

    '        If mstrFilter.Trim.Length > 0 Then
    '            strQ &= " AND " & mstrFilter.Trim
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetEmployeeApprover", mstrMessage)
    '    End Try
    '    If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
    '        Return dsList.Tables("List")
    '    Else
    '        Return Nothing
    '    End If
    'End Function

    'Nilay (10-Oct-2015) -- End

    'Nilay (10-Oct-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>

    Public Function GetEmployeeFromUser(ByVal xDatabaseName As String, _
                                        ByVal xUserUnkid As Integer, _
                                        ByVal xYearUnkid As Integer, _
                                        ByVal xCompanyUnkid As Integer, _
                                        ByVal xPeriodStart As DateTime, _
                                        ByVal xPeriodEnd As DateTime, _
                                        ByVal xUserModeSetting As String, _
                                        ByVal xOnlyApproved As Boolean, _
                                        ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                        ByVal strTableName As String) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim xDateJoinQry, xDateFilterQry As String
            xDateJoinQry = "" : xDateFilterQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)

            Dim objDataOperation As New clsDataOperation

            'Nilay (09-Aug-2016) -- Start
            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            'strQ = "SELECT  0 AS employeeunkid,'' AS employeecode, '  ' + @Select AS employeename "
            strQ = "SELECT  0 AS employeeunkid,'' AS employeecode, '  ' + @Select AS employeename, '  ' + @Select AS EmpCodeName "
            'Nilay (09-Aug-2016) -- End

            strQ &= " UNION " & _
                            " SELECT " & _
                            "	 ISNULL(hremployee_master.employeeunkid,0) AS employeeunkid " & _
                            "	,ISNULL(employeecode,'') AS employeecode " & _
                            "	,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename " & _
                            "   ,ISNULL(employeecode,'') + ' - ' + ISNULL(firstname,'') + ' ' + ISNULL(othername,'') + ' ' + ISNULL(surname,'') AS EmpCodeName " & _
                            " FROM lnloanapprover_tran " & _
                            "	JOIN hremployee_master ON lnloanapprover_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            "   LEFT JOIN lnloan_approver_mapping ON lnloanapprover_tran.lnloanapproverunkid = lnloan_approver_mapping.approvertranunkid " & _
                            "  AND lnloan_approver_mapping.userunkid = " & xUserUnkid & ""

            'Nilay (09-Aug-2016) -- Start
            'ENHANCEMENT : Employee Name with Code Requested by {Andrew, Rutta}
            'ADDED - [ISNULL(employeecode,'') + ' - ' + ISNULL(firstname,'') + ' ' + ISNULL(othername,'') + ' ' + ISNULL(surname,'') AS EmpCodeName]
            'Nilay (09-Aug-2016) -- End

            'Nilay (07-Feb-2016) -- Start
            'OLD : JOIN lnloan_approver_mapping ON lnloanapprover_tran.lnloanapproverunkid = lnloan_approver_mapping.approvertranunkid
            'NEW : LEFT JOIN lnloan_approver_mapping ON lnloanapprover_tran.lnloanapproverunkid = lnloan_approver_mapping.approvertranunkid
            'Nilay (07-Feb-2016) -- End

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            strQ &= " WHERE 1 = 1  AND lnloanapprover_tran.isvoid = 0 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            strQ &= " AND hremployee_master.isapproved = 1 ORDER BY employeename "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeFromUser", mstrMessage)
            Return Nothing
        End Try
    End Function

    'Public Function GetEmployeeFromUser(ByVal intUserID As Integer, ByVal strEmployeeAsonDate As String, ByVal mblnIsIncludeInActiveEmp As Boolean) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Try
    '        Dim objDataOperation As New clsDataOperation

    '        strQ = "SELECT  0 AS employeeunkid,'' AS employeecode, '  ' + @Select AS employeename "

    '        strQ &= " UNION " & _
    '                        " SELECT " & _
    '                        "	 ISNULL(hremployee_master.employeeunkid,0) AS employeeunkid " & _
    '                        "	,ISNULL(employeecode,'') AS employeecode " & _
    '                        "	,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS employeename " & _
    '                        " FROM lnloanapprover_tran " & _
    '                        "	JOIN hremployee_master ON lnloanapprover_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "  JOIN lnloan_approver_mapping ON lnloanapprover_tran.lnloanapproverunkid = lnloan_approver_mapping.approvertranunkid " & _
    '                        "  AND lnloan_approver_mapping.userunkid = " & intUserID & " WHERE 1 = 1  AND lnloanapprover_tran.isvoid = 0 "

    '        If mblnIsIncludeInActiveEmp = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "
    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsonDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmployeeAsonDate)
    '        End If

    '        strQ &= " AND hremployee_master.isapproved = 1 ORDER BY employeename "

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        Return dsList

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetEmployeeFromUser", mstrMessage)
    '        Return Nothing
    '    End Try
    'End Function
    'Nilay (10-Oct-2015) -- End

    ''' <summary>
    ''' Created By: Nilay
    ''' </summary>
    ''' <purpose> Get Approver Employee(s) Id(s) </purpose>
    Public Function GetApproverEmployeeId(ByVal intApproverunkid As Integer, ByVal blnIsExternalApprover As Boolean) As String
        Dim mstrEmployeeIds As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = "SELECT  ISNULL " & _
                                "(STUFF " & _
                                        "(" & _
                                            "(SELECT ',' + CAST(lnloanapprover_tran.employeeunkid AS VARCHAR(MAX)) " & _
                                            " FROM     lnloanapprover_master " & _
                                            " JOIN lnloanapprover_tran ON lnloanapprover_master.lnapproverunkid = lnloanapprover_tran.lnloanapproverunkid " & _
                                            "   AND lnloanapprover_tran.isvoid = 0 " & _
                                            " WHERE lnloanapprover_master.approverempunkid = " & intApproverunkid & " " & _
                                            "   AND lnloanapprover_master.isvoid = 0 " & _
                                            "   AND lnloanapprover_master.isswap = 0 " & _
                                            "   AND lnloanapprover_master.isactive = 1 " & _
                                            "   AND lnloanapprover_master.isexternalapprover = @isexternalapprover " & _
                                            " ORDER BY lnloanapprover_tran.employeeunkid " & _
                                            " FOR XML PATH('') " & _
                                            ") , 1, 1, '' " & _
                                        "), '' " & _
                                ") EmployeeIds "

            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExternalApprover)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                mstrEmployeeIds = dsList.Tables(0).Rows(0)("EmployeeIds").ToString()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverEmployeeId", mstrMessage)
        End Try
        Return mstrEmployeeIds
    End Function


    'Nilay (01-Mar-2016) -- Start
    'ENHANCEMENT - Implementing External Approval changes 
    Public Function GetApproverUnkid(ByVal intApproverEmpunkid As Integer, ByVal blnIsExternalApprover As Boolean, _
                                     Optional ByVal objDataOp As clsDataOperation = Nothing) As String
        Dim strApproverunkid As String = ""
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            If objDataOp IsNot Nothing Then
                objDataOperation = objDataOp
            Else
                objDataOperation = New clsDataOperation
            End If
            objDataOperation.ClearParameters()

            strQ = "SELECT  ISNULL " & _
                                "(STUFF " & _
                                        "(" & _
                                            "(SELECT ',' + CAST(lnapproverunkid AS VARCHAR(MAX)) " & _
                                            " FROM     lnloanapprover_master " & _
                                            " WHERE lnloanapprover_master.approverempunkid = " & intApproverEmpunkid & " " & _
                                            "   AND lnloanapprover_master.isvoid = 0 " & _
                                            "   AND lnloanapprover_master.isswap = 0 " & _
                                            "   AND lnloanapprover_master.isactive = 1 " & _
                                            "   AND lnloanapprover_master.isexternalapprover = @isexternalapprover " & _
                                            " FOR XML PATH('') " & _
                                            ") , 1, 1, '' " & _
                                        "), '' " & _
                                ") ApproverIds "

            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsExternalApprover)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                strApproverunkid = dsList.Tables(0).Rows(0)("ApproverIds").ToString()
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverEmployeeId", mstrMessage)
        Finally
            If objDataOp Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try

        Return strApproverunkid
    End Function
    'Nilay (01-Mar-2016) -- End


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function IsEmployeeExistForUser(ByVal intUserID As Integer, ByVal intApproverEmployeeID As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()

            strQ = " SELECT " & _
                      " lnloanapprover_master.lnapproverunkid " & _
                      ",lnloanapprover_master.approverempunkid " & _
                      ",lnloan_approver_mapping.userunkid " & _
                      " FROM lnloan_approver_mapping " & _
                      " LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloan_approver_mapping.approvertranunkid " & _
                      " AND lnloanapprover_master.isvoid = 0 AND isswap = 0 " & _
                      " WHERE lnloan_approver_mapping.userunkid =  @userid AND lnloanapprover_master.approverempunkid <> @ApproverEmpID "

            objDataOperation.AddParameter("@userid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID)
            objDataOperation.AddParameter("@ApproverEmpID", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverEmployeeID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsEmployeeExistForUser , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function InsertATApprover_Master(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@lnapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid.ToString)
            objDataOperation.AddParameter("@lnlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnLevelunkid.ToString)
            objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid.ToString)
            objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsswap)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)

                'Gajanan [23-SEP-2019] -- Start    
                'Enhancement:Enforcement of approval migration From Transfer and recategorization.
                'objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 5, "WEB"))
                'Gajanan [23-SEP-2019] -- End
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            'Nilay (01-Mar-2016) -- Start
            'ENHANCEMENT - Implementing External Approval changes 
            objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalApprover)
            'Nilay (01-Mar-2016) -- End

            strQ = "INSERT INTO atlnloanapprover_master (" & _
                        " lnapproverunkid " & _
                        ",lnlevelunkid" & _
                        ",approverempunkid " & _
                        ",isswap " & _
                        ",isactive " & _
                        ",audittype " & _
                        ",audituserunkid " & _
                        ",auditdatetime " & _
                        ",ip " & _
                        ",machine_name " & _
                        ",form_name " & _
                        ",module_name1 " & _
                        ",module_name2 " & _
                        ",module_name3 " & _
                        ",module_name4 " & _
                        ",module_name5 " & _
                        ",isweb " & _
                        ",isexternalapprover " & _
                    ") VALUES (" & _
                        "  @lnapproverunkid " & _
                        " ,@lnlevelunkid" & _
                        " ,@approverempunkid " & _
                        ", @isswap " & _
                        " ,@isactive " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name " & _
                        ", @form_name " & _
                        ", @module_name1 " & _
                        ", @module_name2 " & _
                        ", @module_name3 " & _
                        ", @module_name4 " & _
                        ", @module_name5 " & _
                        ", @isweb " & _
                        ", @isexternalapprover " & _
                    "); SELECT @@identity"
            'Nilay (01-Mar-2016) -- [isexternalapprover]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATApprover_Master , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function SwapApprover(ByVal dtFromEmployee As DataTable, ByVal dtToEmployee As DataTable, _
                                 ByVal mblnLoanApproverForLoanScheme As Boolean) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""
        'Nilay (07-Feb-2016) -- Start
        Dim strPendingProcessIds As String = ""
        'Nilay (07-Feb-2016) -- End

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            '==================================START FOR FROM APPROVER=====================================

            Dim mstrFormId As String = ""

            'Pinkal (26-Sep-2017) -- Start
            'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.
            Dim objOtherLoanOperation As New clsloanotherop_approval_tran
            'Pinkal (26-Sep-2017) -- End


            If dtFromEmployee IsNot Nothing Then

                strQ = "UPDATE lnloanapprover_master SET isswap = 1 WHERE lnapproverunkid = @lnapproverunkid and isswap = 0  AND isvoid = 0"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@lnapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                _lnApproverunkid(objDataOperation) = mintSwapFromApproverID

                If InsertATApprover_Master(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "INSERT INTO lnloanapprover_master (" & _
                         "  lnlevelunkid" & _
                         " ,approverempunkid " & _
                         " ,userunkid " & _
                         " ,isswap " & _
                         " ,isactive " & _
                         " ,isvoid " & _
                         " ,voiddatetime " & _
                         " ,voiduserunkid " & _
                         " ,voidreason " & _
                         " ,isexternalapprover " & _
                     ") VALUES (" & _
                         "  @lnlevelunkid" & _
                         " ,@approverempunkid " & _
                         " ,@userunkid " & _
                         " ,@isswap " & _
                         " ,@isactive " & _
                         " ,@isvoid " & _
                         " ,@voiddatetime " & _
                         " ,@voiduserunkid " & _
                         " ,@voidreason " & _
                         " ,@isexternalapprover " & _
                     "); SELECT @@identity"
                'Nilay (01-Mar-2016) -- [isexternalapprover]

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@lnlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnLevelunkid.ToString)
                objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                If mdtVoidDateTime <> Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidDateTime)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalApprover)
                'Nilay (01-Mar-2016) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintlnApproverunkid = dsList.Tables(0).Rows(0).Item(0)
                _lnApproverunkid(objDataOperation) = mintlnApproverunkid

                If InsertATApprover_Master(objDataOperation, 1) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                dsList = Nothing
                Dim objLnApproverMapping As New clsloan_approver_mapping
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objLnApproverMapping.GetList("List", -1, mintSwapFromApproverID)
                dsList = objLnApproverMapping.GetList(FinancialYear._Object._DatabaseName, _
                                                      -1, _
                                                      FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      ConfigParameter._Object._UserAccessModeSetting, _
                                                      True, _
                                                      ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                      "List", mintSwapFromApproverID, objDataOperation)
                'Nilay (07-Feb-2016) -- [objDataOperation]

                'Nilay (10-Oct-2015) -- End

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In dsList.Tables(0).Rows
                        'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_approver_mapping", "approvermappingunkid", CInt(dr("approvermappingunkid"))) = False Then
                        '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        '    Throw exForce
                        'End If
                        'strQ = " DELETE FROM lnloan_approver_mapping WHERE approvertranunkid = @approvertranunkid "
                        'objDataOperation.ClearParameters()
                        'objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                        'objDataOperation.ExecNonQuery(strQ)

                        'If objDataOperation.ErrorMessage <> "" Then
                        '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        '    Throw exForce
                        'End If

                        'Nilay (07-Feb-2016) -- Start
                        Dim objApproverMapping As New clsloan_approver_mapping
                        objApproverMapping._Approvertranunkid = mintlnApproverunkid
                        objApproverMapping._MappedUserunkid = CInt(dsList.Tables(0).Rows(0)("userunkid")) 'Nilay (05-May-2016) -- [_MappedUserunkid]
                        'Nilay (05-May-2016) -- Start
                        'If objApproverMapping.Insert(objDataOperation) = False Then
                        If objApproverMapping.Insert(objDataOperation, mintUserunkid) = False Then
                            'Nilay (05-May-2016) -- End
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                        objApproverMapping = Nothing
                        'Nilay (07-Feb-2016) -- End
                    Next
                End If
                objLnApproverMapping = Nothing
                dsList = Nothing

                If mblnLoanApproverForLoanScheme Then

                    Dim objApproverSchemeMapping As New clsapprover_scheme_mapping
                    'Nilay (07-Feb-2016) -- Start
                    'dsList = objApproverSchemeMapping.GetList("List", True, mintSwapFromApproverID)
                    dsList = objApproverSchemeMapping.GetList("List", True, mintSwapFromApproverID, objDataOperation)
                    'Nilay (07-Feb-2016) -- End

                    strQ = " UPDATE lnapprover_scheme_mapping SET approverunkid = @newapproverunkid WHERE approverunkid = @oldapproverunkid AND loanschemeunkid = @loanschemeunkid AND isvoid = 0 "

                    For Each dr As DataRow In dsList.Tables(0).Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                        objDataOperation.AddParameter("@oldapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                        objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("loanschemeunkid")))
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'Nilay (07-Feb-2016) -- Start
                        'objApproverSchemeMapping._Loanschememappingunkid = CInt(dr("loanschememappingunkid"))
                        objApproverSchemeMapping._Loanschememappingunkid(objDataOperation) = CInt(dr("loanschememappingunkid"))
                        'Nilay (07-Feb-2016) -- End

                        objApproverSchemeMapping._WebClientIP = mstrWebClientIP
                        objApproverSchemeMapping._WebFormName = mstrWebFormName
                        objApproverSchemeMapping._WebHostName = mstrWebHostName

                        If objApproverSchemeMapping.InsertATLoanSchemeMapping(objDataOperation, 2, CInt(dr("loanschemeunkid"))) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next
                    objApproverSchemeMapping = Nothing

                End If

                For Each drRow As DataRow In dtFromEmployee.Rows

                    strQ = " UPDATE lnloanapprover_tran SET lnloanapproverunkid = @newloanapproverunkid WHERE isvoid = 0 AND lnloanapproverunkid = @oldloanapproverunkid AND employeeunkid = @employeeunkid "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                    objDataOperation.AddParameter("@oldloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("employeeunkid")))
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    Dim objLoanApproverTran As New clsLoanApprover_tran
                    strQ = " SELECT lnapprovertranunkid FROM lnloanapprover_tran WHERE isvoid = 0 AND lnloanapproverunkid = @newloanapproverunkid AND employeeunkid = @employeeunkid "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("employeeunkid")))
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        'Nilay (07-Feb-2016) -- Start
                        'objLoanApproverTran._LoanApproverTranunkid = CInt(dsList.Tables(0).Rows(0)("lnapprovertranunkid"))
                        objLoanApproverTran._LoanApproverTranunkid(objDataOperation) = CInt(dsList.Tables(0).Rows(0)("lnapprovertranunkid"))
                        'Nilay (01-Mar-2016) -- Start
                        'ENHANCEMENT - Implementing External Approval changes 
                        objLoanApproverTran._LoanApproverunkid = mintlnApproverunkid
                        objLoanApproverTran._UserId = mintUserunkid
                        'Nilay (01-Mar-2016) -- End

                        'Nilay (07-Feb-2016) -- End

                        If objLoanApproverTran.InsertATApprover_Tran(objDataOperation, 2, CInt(drRow("employeeunkid"))) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                    dsList = Nothing
                    objLoanApproverTran = Nothing

                    'Nilay (07-Feb-2016) -- Start
                    Dim objApprovalProcessTran As New clsloanapproval_process_Tran


                    'Pinkal (26-Sep-2017) -- Start
                    'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.

                    'strPendingProcessIds = objApprovalProcessTran.GetProcessPendingLoanIds(mintSwapFromApproverID, CInt(drRow("employeeunkid")), objDataOperation)

                    strPendingProcessIds = objApprovalProcessTran.GetProcessPendingLoanIds(mintSwapToApproverID, CInt(drRow("employeeunkid")), objDataOperation)

                    If strPendingProcessIds.Trim.Length > 0 Then


                        'strQ = "UPDATE lnloanapproval_process_tran set " & _
                        '            " approvertranunkid = @newapprovertranunkid " & _
                        '       " WHERE   processpendingloanunkid IN (" & strPendingProcessIds & ") " & _
                        '       "    AND lnloanapproval_process_tran.isvoid = 0 " & _
                        '       "    AND lnloanapproval_process_tran.approvertranunkid = " & mintSwapFromApproverID

                        strQ = " UPDATE lnloanapproval_process_tran set " & _
                                    " approvertranunkid = @newapprovertranunkid " & _
                                  ", approverempunkid = @newapproverempunkid " & _
                               " WHERE   processpendingloanunkid IN (" & strPendingProcessIds & ") " & _
                               "    AND lnloanapproval_process_tran.isvoid = 0 " & _
                                  "    AND lnloanapproval_process_tran.approvertranunkid =  @oldapprovertranunkid "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                        objDataOperation.AddParameter("@newapproverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid)
                        objDataOperation.AddParameter("@oldapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)


                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'strQ = "SELECT lnloanapproval_process_tran.pendingloantranunkid " & _
                        '           " FROM lnloanapproval_process_tran " & _
                        '           " WHERE   lnloanapproval_process_tran.statusunkid = 1 " & _
                        '           "    AND lnloanapproval_process_tran.processpendingloanunkid IN (" & strPendingProcessIds & ")" & _
                        '           "    AND lnloanapproval_process_tran.approvertranunkid = " & mintSwapFromApproverID & _
                        '           "    AND lnloanapproval_process_tran.employeeunkid = " & CInt(drRow("employeeunkid")) & _
                        '           "    AND lnloanapproval_process_tran.isvoid = 0 "


                        strQ = " SELECT lnloanapproval_process_tran.pendingloantranunkid " & _
                               " FROM lnloanapproval_process_tran " & _
                               " WHERE   lnloanapproval_process_tran.statusunkid = 1 " & _
                                  "    AND lnloanapproval_process_tran.approvertranunkid = @newapprovertranunkid " & _
                                  "    AND lnloanapproval_process_tran.approverempunkid = @newapproverempunkid " & _
                                  "    AND lnloanapproval_process_tran.employeeunkid = @employeeunkid " & _
                               "    AND lnloanapproval_process_tran.processpendingloanunkid IN (" & strPendingProcessIds & ")" & _
                               "    AND lnloanapproval_process_tran.isvoid = 0 "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("employeeunkid")))
                        objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                        objDataOperation.AddParameter("@newapproverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid)
                        'Pinkal (26-Sep-2017) -- End

                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If dsList.Tables("List").Rows.Count > 0 Then
                            For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                                objApprovalProcessTran._Pendingloantranunkid(objDataOperation) = dsList.Tables("List").Rows(0)("pendingloantranunkid")
                                If objApprovalProcessTran.InsertAuditTrailForLoanApproval(objDataOperation, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            Next
                        End If

                    End If
                    'Nilay (07-Feb-2016) -- End


                    'Pinkal (26-Sep-2017) -- Start
                    'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.

                    '/*  START FOR CHECK OTHER LOAN OPERATION   */
                    mstrFormId = ""
                    mstrFormId = objOtherLoanOperation.GetApproverPendingOtherLoanOperation(mintSwapToApproverID, CInt(drRow("employeeunkid")), objDataOperation)

                    If mstrFormId.Length > 0 Then

                        strQ = " Update lnloanotherop_approval_tran set " & _
                                  " approverempunkid  = @newapproverempunkid,approvertranunkid = @newapprovertranunkid   " & _
                                  " WHERE loanadvancetranunkid in (" & mstrFormId & ") AND approvertranunkid = @approvertranunkid AND lnloanotherop_approval_tran.isvoid = 0 "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newapproverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid)
                        objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        strQ = "Select ISNULL(lnotheroptranunkid,0) lnotheroptranunkid " & _
                                  " FROM lnloanotherop_approval_tran  " & _
                                  " WHERE approverempunkid = @approverempunkid AND approvertranunkid = @approvertranunkid AND loanadvancetranunkid  in (" & mstrFormId & ") AND isvoid = 0"

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If dsList.Tables(0).Rows.Count > 0 Then
                            For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lnloanotherop_approval_tran", "lnotheroptranunkid", dsList.Tables(0).Rows(k)("lnotheroptranunkid").ToString(), False) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            Next
                        End If

                    End If

                    '/*  END FOR CHECK OTHER LOAN OPERATION   */

                    'Pinkal (26-Sep-2017) -- End


                    mstrEmployeeID &= drRow("employeeunkid").ToString() & ","

                Next

                If mstrEmployeeID.Trim.Length > 0 Then
                    mstrEmployeeID = mstrEmployeeID.Substring(0, mstrEmployeeID.Trim.Length - 1)
                End If

                Dim objMigration As New clsMigration
                objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
                objMigration._Migrationfromunkid = mintSwapFromApproverID
                objMigration._Migrationtounkid = mintSwapToApproverID
                objMigration._Usertypeid = enUserType.LoanApprover
                objMigration._Userunkid = mintUserunkid
                objMigration._Migratedemployees = mstrEmployeeID

                'Nilay (05-May-2016) -- Start
                'If objMigration.Insert(objDataOperation) = False Then
                If objMigration.Insert(objDataOperation, mintUserunkid) = False Then
                    'Nilay (05-May-2016) -- End
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


            End If

            '==================================END FOR FROM APPROVER=====================================

            '==================================START FOR TO APPROVER=====================================


            'objLeavForm._Formunkid = 0
            'mstrFormId = ""
            mstrEmployeeID = ""

            If dtToEmployee IsNot Nothing Then

                strQ = "UPDATE lnloanapprover_master SET isswap = 1 WHERE lnapproverunkid = @lnapproverunkid and isswap = 0  AND isvoid = 0"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@lnapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                _lnApproverunkid(objDataOperation) = mintSwapToApproverID

                If InsertATApprover_Master(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "INSERT INTO lnloanapprover_master (" & _
                            "  lnlevelunkid" & _
                            " ,approverempunkid " & _
                            " ,userunkid " & _
                            " ,isswap " & _
                            " ,isactive " & _
                            " ,isvoid " & _
                            " ,voiddatetime " & _
                            " ,voiduserunkid " & _
                            " ,voidreason " & _
                            " ,isexternalapprover " & _
                        ") VALUES (" & _
                            "  @lnlevelunkid" & _
                            " ,@approverempunkid " & _
                            " ,@userunkid " & _
                            " ,@isswap " & _
                            " ,@isactive " & _
                            " ,@isvoid " & _
                            " ,@voiddatetime " & _
                            " ,@voiduserunkid " & _
                            " ,@voidreason " & _
                            " ,@isexternalapprover " & _
                        "); SELECT @@identity"
                'Nilay (01-Mar-2016) -- [isexternalapprover]

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@lnlevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnLevelunkid.ToString)
                objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid.ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@isswap", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                If mdtVoidDateTime <> Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoidDateTime)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidUserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidReason.ToString)
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                objDataOperation.AddParameter("@isexternalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExternalApprover)
                'Nilay (01-Mar-2016) -- End

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintlnApproverunkid = dsList.Tables(0).Rows(0).Item(0)
                _lnApproverunkid(objDataOperation) = mintlnApproverunkid
                If InsertATApprover_Master(objDataOperation, 1) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                dsList = Nothing
                Dim objLnApproverMapping As New clsloan_approver_mapping
                'Nilay (10-Oct-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'dsList = objLnApproverMapping.GetList("List", -1, mintSwapToApproverID)
                dsList = objLnApproverMapping.GetList(FinancialYear._Object._DatabaseName, _
                                                      -1, _
                                                      FinancialYear._Object._YearUnkid, _
                                                      Company._Object._Companyunkid, _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                      ConfigParameter._Object._UserAccessModeSetting, _
                                                      True, _
                                                      ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                      "List", mintSwapToApproverID, objDataOperation)
                'Nilay (07-Feb-2016) -- [objDataOperation]

                'Nilay (10-Oct-2015) -- End

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In dsList.Tables(0).Rows
                        'If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_approver_mapping", "approvermappingunkid", CInt(dr("approvermappingunkid"))) = False Then
                        '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        '    Throw exForce
                        'End If

                        'strQ = " DELETE FROM lnloan_approver_mapping WHERE approvertranunkid = @approvertranunkid "
                        'objDataOperation.ClearParameters()
                        'objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                        'objDataOperation.ExecNonQuery(strQ)

                        'Nilay (07-Feb-2016) -- Start
                        Dim objApproverMapping As New clsloan_approver_mapping
                        objApproverMapping._Approvertranunkid = mintlnApproverunkid
                        objApproverMapping._MappedUserunkid = CInt(dsList.Tables(0).Rows(0)("userunkid")) 'Nilay (05-May-2016) -- [_MappedUserunkid]
                        'Nilay (05-May-2016) -- Start
                        'If objApproverMapping.Insert(objDataOperation) = False Then
                        If objApproverMapping.Insert(objDataOperation, mintUserunkid) = False Then
                            'Nilay (05-May-2016) -- End
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                        objApproverMapping = Nothing
                        'Nilay (07-Feb-2016) -- End
                    Next
                End If
                objLnApproverMapping = Nothing
                dsList = Nothing

                If mblnLoanApproverForLoanScheme Then

                    Dim objApproverSchemeMapping As New clsapprover_scheme_mapping
                    'Nilay (07-Feb-2016) -- Start
                    'dsList = objApproverSchemeMapping.GetList("List", True, mintSwapToApproverID)
                    dsList = objApproverSchemeMapping.GetList("List", True, mintSwapToApproverID, objDataOperation)
                    'Nilay (07-Feb-2016) -- End

                    strQ = " UPDATE lnapprover_scheme_mapping SET approverunkid = @newapproverunkid WHERE approverunkid = @oldapproverunkid AND loanschemeunkid = @loanschemeunkid AND isvoid = 0 "

                    For Each dr As DataRow In dsList.Tables(0).Rows
                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                        objDataOperation.AddParameter("@oldapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapToApproverID)
                        objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("loanschemeunkid")))
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        objApproverSchemeMapping._Loanschememappingunkid = CInt(dr("loanschememappingunkid"))
                        objApproverSchemeMapping._WebClientIP = mstrWebClientIP
                        objApproverSchemeMapping._WebFormName = mstrWebFormName
                        objApproverSchemeMapping._WebHostName = mstrWebHostName

                        If objApproverSchemeMapping.InsertATLoanSchemeMapping(objDataOperation, 2, CInt(dr("loanschemeunkid"))) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    Next
                    objApproverSchemeMapping = Nothing

                End If

                dsList = Nothing
                For Each drRow As DataRow In dtToEmployee.Rows

                    strQ = " UPDATE lnloanapprover_tran SET lnloanapproverunkid = @newloanapproverunkid WHERE isvoid = 0 AND lnloanapproverunkid = @oldloanapproverunkid AND employeeunkid = @employeeunkid "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                    objDataOperation.AddParameter("@oldloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("employeeunkid")))
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    Dim objLoanApproverTran As New clsLoanApprover_tran
                    strQ = " SELECT lnapprovertranunkid FROM lnloanapprover_tran WHERE isvoid = 0 AND lnloanapproverunkid = @newloanapproverunkid AND employeeunkid = @employeeunkid "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("employeeunkid")))
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                        objLoanApproverTran._LoanApproverTranunkid(objDataOperation) = CInt(dsList.Tables(0).Rows(0)("lnapprovertranunkid"))
                        'Nilay (01-Mar-2016) -- Start
                        'ENHANCEMENT - Implementing External Approval changes 
                        objLoanApproverTran._LoanApproverunkid = mintlnApproverunkid
                        objLoanApproverTran._UserId = mintUserunkid
                        'Nilay (01-Mar-2016) -- End
                        If objLoanApproverTran.InsertATApprover_Tran(objDataOperation, 2, CInt(drRow("employeeunkid"))) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                    dsList = Nothing
                    objLoanApproverTran = Nothing

                    'Nilay (07-Feb-2016) -- Start
                    Dim objApprovalProcessTran As New clsloanapproval_process_Tran


                    'Pinkal (26-Sep-2017) -- Start
                    'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.

                    'strPendingProcessIds = objApprovalProcessTran.GetProcessPendingLoanIds(mintSwapToApproverID, CInt(drRow("employeeunkid")), objDataOperation)

                    strPendingProcessIds = objApprovalProcessTran.GetProcessPendingLoanIds(mintSwapFromApproverID, CInt(drRow("employeeunkid")), objDataOperation)

                    If strPendingProcessIds.Trim.Length > 0 Then

                        'strQ = "UPDATE lnloanapproval_process_tran set " & _
                        '            " approvertranunkid = @newapprovertranunkid " & _
                        '       " WHERE   processpendingloanunkid IN (" & strPendingProcessIds & ") " & _
                        '       "    AND lnloanapproval_process_tran.isvoid = 0 " & _
                        '       "    AND lnloanapproval_process_tran.approvertranunkid = " & mintSwapToApproverID

                        strQ = "UPDATE lnloanapproval_process_tran set " & _
                                    " approvertranunkid = @newapprovertranunkid " & _
                                 ", approverempunkid = @newapproverempunkid " & _
                               " WHERE   processpendingloanunkid IN (" & strPendingProcessIds & ") " & _
                               "    AND lnloanapproval_process_tran.isvoid = 0 " & _
                                 "    AND lnloanapproval_process_tran.approvertranunkid = @oldapprovertranunkid"

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                        objDataOperation.AddParameter("@newapproverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid)
                        objDataOperation.AddParameter("@oldapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'strQ = "SELECT lnloanapproval_process_tran.pendingloantranunkid " & _
                        '       " FROM lnloanapproval_process_tran " & _
                        '       " WHERE   lnloanapproval_process_tran.statusunkid = 1 " & _
                        '       "    AND lnloanapproval_process_tran.processpendingloanunkid IN (" & strPendingProcessIds & ")" & _
                        '       "    AND lnloanapproval_process_tran.approvertranunkid = " & mintSwapToApproverID & _
                        '       "    AND lnloanapproval_process_tran.employeeunkid = " & CInt(drRow("employeeunkid")) & _
                        '       "    AND lnloanapproval_process_tran.isvoid = 0 "

                        strQ = "SELECT lnloanapproval_process_tran.pendingloantranunkid " & _
                               " FROM lnloanapproval_process_tran " & _
                               " WHERE   lnloanapproval_process_tran.statusunkid = 1 " & _
                                 "    AND lnloanapproval_process_tran.approvertranunkid = @newapprovertranunkid " & _
                                 "    AND lnloanapproval_process_tran.approverempunkid = @newapproverempunkid " & _
                                 "    AND lnloanapproval_process_tran.employeeunkid = @employeeunkid " & _
                               "    AND lnloanapproval_process_tran.processpendingloanunkid IN (" & strPendingProcessIds & ")" & _
                               "    AND lnloanapproval_process_tran.isvoid = 0 "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("employeeunkid")))
                        objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                        objDataOperation.AddParameter("@newapproverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid)
                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If dsList.Tables("List").Rows.Count > 0 Then
                            For i As Integer = 0 To dsList.Tables("List").Rows.Count - 1
                                objApprovalProcessTran._Pendingloantranunkid(objDataOperation) = dsList.Tables("List").Rows(0)("pendingloantranunkid")
                                If objApprovalProcessTran.InsertAuditTrailForLoanApproval(objDataOperation, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            Next
                        End If
                    End If
                    objApprovalProcessTran = Nothing
                    'Nilay (07-Feb-2016) -- End



                    'Pinkal (26-Sep-2017) -- Start
                    'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.

                    '/*  START FOR CHECK OTHER LOAN OPERATION   */
                    mstrFormId = ""
                    mstrFormId = objOtherLoanOperation.GetApproverPendingOtherLoanOperation(mintSwapFromApproverID, CInt(drRow("employeeunkid")), objDataOperation)

                    If mstrFormId.Length > 0 Then

                        strQ = " Update lnloanotherop_approval_tran set " & _
                                  " approverempunkid  = @newapproverempunkid,approvertranunkid = @newapprovertranunkid   " & _
                                  " WHERE loanadvancetranunkid in (" & mstrFormId & ") AND approvertranunkid = @approvertranunkid AND lnloanotherop_approval_tran.isvoid = 0 "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@newapproverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid)
                        objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSwapFromApproverID)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        strQ = "Select ISNULL(lnotheroptranunkid,0) lnotheroptranunkid " & _
                                  " FROM lnloanotherop_approval_tran  " & _
                                  " WHERE approverempunkid = @approverempunkid AND approvertranunkid = @approvertranunkid AND loanadvancetranunkid  in (" & mstrFormId & ") AND isvoid = 0"

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverEmpunkid)
                        objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                        dsList = objDataOperation.ExecQuery(strQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If dsList.Tables(0).Rows.Count > 0 Then
                            For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lnloanotherop_approval_tran", "lnotheroptranunkid", dsList.Tables(0).Rows(k)("lnotheroptranunkid").ToString(), False) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            Next
                        End If

                    End If

                    '/*  END FOR CHECK OTHER LOAN OPERATION   */

                    'Pinkal (26-Sep-2017) -- End



                    mstrEmployeeID &= drRow("employeeunkid").ToString() & ","

                Next

                If mstrEmployeeID.Trim.Length > 0 Then
                    mstrEmployeeID = mstrEmployeeID.Substring(0, mstrEmployeeID.Trim.Length - 1)
                End If

                Dim objMigration As New clsMigration
                objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
                objMigration._Migrationfromunkid = mintSwapToApproverID
                objMigration._Migrationtounkid = mintSwapFromApproverID
                objMigration._Usertypeid = enUserType.LoanApprover
                objMigration._Userunkid = mintUserunkid
                objMigration._Migratedemployees = mstrEmployeeID

                'Nilay (05-May-2016) -- Start
                'If objMigration.Insert(objDataOperation) = False Then
                If objMigration.Insert(objDataOperation, mintUserunkid) = False Then
                    'Nilay (05-May-2016) -- End
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            '==================================END FOR TO APPROVER=====================================


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SwapApprover; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Shani (21-Jul-2016) -- Start
    'Enhancement - Create New Loan Notification 
    Public Function GetEmailNotification(ByVal strUnkid As String, _
                                         ByVal intEmployeeID As Integer, _
                                         ByVal blnLoanApproverForLoanScheme As Boolean, _
                                         ByVal blnIsLoanApplication As Boolean, _
                                         Optional ByVal intSchemeId As Integer = -1, _
                                         Optional ByVal mstrFilter As String = "", _
                                         Optional ByVal mintApproverLevel As Integer = -1) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation
            Dim strQFinal As String = String.Empty
            Dim strQCondition As String = String.Empty

            objDataOperation.ClearParameters()

            strQ = " SELECT " & _
                   "     lnloanapprover_master.lnapproverunkid " & _
                   "    ,lnloan_approver_mapping.userunkid AS userunkid " & _
                   "    ,#APPROVER_NAME# AS app_name " & _
                   "    ,#APPROVER_EMAIL# AS app_email " & _
                   "    ,EMP.employeeunkid AS emp_empunkid " & _
                   "    ,ISNULL(EMP.firstname,'') + ' ' + ISNULL(EMP.surname,'') AS emp_name " & _
                   "    ,ISNULL(EMP.employeecode,'') AS emp_code " & _
                   "    ,EMP.email as emp_email" & _
                   "    ,lnapproverlevel_master.priority AS priority " & _
                   "    ,#APPROVER_TABLE#.statusunkid AS statusunkid " & _
                   "    ,ISNULL(LSM.name,'') AS scheme_name "

            If blnIsLoanApplication Then
                strQ &= "    ,lnloan_process_pending_loan.application_no AS application_no " & _
                        "    ,CONVERT(CHAR(8),lnloan_process_pending_loan.application_date,112) AS application_date " & _
                        "    ,#APPROVER_TABLE#.pendingloantranunkid AS pendingloantranunkid " & _
                        "    ,#APPROVER_TABLE#.approvertranunkid AS approverunkid " & _
                        "    ,#APPROVER_TABLE#.processpendingloanunkid AS processpendingloanunkid "
            Else
                strQ &= "   ,lnloan_advance_tran.loanvoucher_no AS loanvoucher_no " & _
                        "   ,lnloan_advance_tran.loanadvancetranunkid AS loanadvancetranunkid " & _
                        "   ,#APPROVER_TABLE#.lnotheroptranunkid AS lnotheroptranunkid "
            End If

            strQ &= "FROM lnloanapprover_tran " & _
                    "    JOIN lnloanapprover_master ON lnloanapprover_tran.lnloanapproverunkid = lnloanapprover_master.lnapproverunkid " & _
                    "    JOIN hremployee_master AS EMP ON EMP.employeeunkid = lnloanapprover_tran.employeeunkid " & _
                    "    JOIN #APPROVER_TABLE# ON #APPROVER_TABLE#.approvertranunkid =  lnloanapprover_master.lnapproverunkid " & _
                    "    JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid " & _
                    "    #EMPLOYEE_JOIN# " & _
                    "    JOIN lnapproverlevel_master ON lnapproverlevel_master.lnlevelunkid = lnloanapprover_master.lnlevelunkid "

            If blnIsLoanApplication Then
                strQ &= "    JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid " & _
                        "    LEFT JOIN lnloan_scheme_master AS LSM ON LSM.loanschemeunkid = lnloan_process_pending_loan.loanschemeunkid "
            Else
                strQ &= "   JOIN lnloan_advance_tran ON lnloan_advance_tran.loanadvancetranunkid = lnloanotherop_approval_tran.loanadvancetranunkid " & _
                        "   JOIN lnloan_scheme_master AS LSM ON LSM.loanschemeunkid = lnloan_advance_tran.loanschemeunkid"
            End If

            If blnLoanApproverForLoanScheme = True And intSchemeId > 0 Then
                strQ &= "   JOIN lnapprover_scheme_mapping ON lnapprover_scheme_mapping.approverunkid = lnloanapprover_master.lnapproverunkid AND lnapprover_scheme_mapping.isvoid = 0 " & _
                        "   JOIN lnloan_scheme_master ON lnloan_scheme_master.loanschemeunkid = lnapprover_scheme_mapping.loanschemeunkid AND lnapprover_scheme_mapping.loanschemeunkid = '" & intSchemeId & "' "
            End If

            If blnIsLoanApplication Then
                strQ = strQ.Replace("#APPROVER_TABLE#", "lnloanapproval_process_tran")
            Else
                strQ = strQ.Replace("#APPROVER_TABLE#", "lnloanotherop_approval_tran")
            End If

            strQFinal = strQ

            strQCondition = " WHERE lnloanapprover_tran.isvoid = 0 AND lnloanapprover_master.isvoid = 0 AND lnloanapprover_master.isactive = 1 AND lnloanapprover_master.isswap = 0   AND lnloanapprover_tran.employeeunkid = " & intEmployeeID
            strQCondition &= " AND lnloanapprover_master.isexternalapprover = #EXT_APPROVER# "

            If blnIsLoanApplication Then
                strQCondition &= " AND lnloanapproval_process_tran.processpendingloanunkid = '" & strUnkid & "' "
            Else
                strQCondition &= " AND lnloanotherop_approval_tran.identify_guid = '" & strUnkid & "' "
            End If


            If mstrFilter.Trim.Length > 0 Then
                strQCondition &= " AND " & mstrFilter.Trim
            End If

            If mintApproverLevel > 0 Then
                strQCondition &= "AND lnapproverlevel_master.priority = " & mintApproverLevel & " "
            End If

            strQ &= strQCondition
            strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname,'') ")
            strQ = strQ.Replace("#APPROVER_EMAIL#", "ISNULL(hremployee_master.email,'') ")
            strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = lnloanapprover_master.approverempunkid ")
            strQ = strQ.Replace("#EXT_APPROVER#", "0")

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsCompany As DataSet
            Dim objlnApprover As New clsLoanApprover_master

            dsCompany = objlnApprover.GetExternalApproverList(objDataOperation, "Company")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsExtList As New DataSet

            For Each dRow In dsCompany.Tables("Company").Rows
                strQ = strQFinal
                If dRow("dbname").ToString.Trim.Length <= 0 AndAlso CInt(dRow("companyunkid")) <= 0 Then
                    strQ = strQ.Replace("#APPROVER_NAME#", "ISNULL(cfuser_master.username,'') ")
                    strQ = strQ.Replace("#APPROVER_EMAIL#", "ISNULL(cfuser_master.email,'') ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid  = lnloan_approver_mapping.userunkid ")
                Else
                    strQ = strQ.Replace("#APPROVER_NAME#", " ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') ")
                    strQ = strQ.Replace("#APPROVER_EMAIL#", "ISNULL(hremployee_master.email,'') ")
                    strQ = strQ.Replace("#EMPLOYEE_JOIN#", "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = lnloan_approver_mapping.userunkid " & _
                                                           "LEFT JOIN #DB_NAME#hremployee_master on hremployee_master.employeeunkid = cfuser_master.employeeunkid ")
                    strQ = strQ.Replace("#DB_NAME#", dRow("dbname").ToString & "..")
                End If

                strQ &= strQCondition
                strQ = strQ.Replace("#EXT_APPROVER#", "1")
                strQ &= " AND cfuser_master.companyunkid = " & dRow("companyunkid")

                objDataOperation.ClearParameters()

                dsExtList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables.Count <= 0 Then
                    dsList.Tables.Add(dsExtList.Tables("List").Copy)
                Else
                    dsList.Tables("List").Merge(dsExtList.Tables("List"), True)
                End If
            Next

            Dim dtTable As DataTable
            dtTable = New DataView(dsList.Tables("List"), "", "priority", DataViewRowState.CurrentRows).ToTable.Copy
            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dtTable.Copy)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmailNotification", mstrMessage)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
        If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
            Return dsList.Tables("List")
        Else
            Return Nothing
        End If
    End Function
    'Shani (21-Jul-2016) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Loan Approver already exists. Please define new Loan Approver.")
            Language.setMessage(mstrModuleName, 2, "Select")
            Language.setMessage(mstrModuleName, 3, "Yes")
            Language.setMessage(mstrModuleName, 4, "No")
            Language.setMessage(mstrModuleName, 5, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
