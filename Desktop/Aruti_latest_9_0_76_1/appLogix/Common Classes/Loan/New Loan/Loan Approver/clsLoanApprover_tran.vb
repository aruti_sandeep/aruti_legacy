﻿'************************************************************************************************************************************
'Class Name :clsLoanApprover_tran.vb
'Purpose    :
'Date       :09-Mar-2015
'Written By :Nilay
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clsLoanApprover_tran

    Private Shared ReadOnly mstrModuleName As String = "clsLoanApprover_tran"
    Dim mstrMessage As String = ""

#Region "Private Variables"

    Private mintlnApproverTranunkid As Integer = 0
    Private mintlnApproverunkid As Integer = 0
    Private mdtTran As DataTable
    Private mintUserId As Integer = 0
    Private objDataOperation As clsDataOperation
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty

#End Region

#Region "Properties"

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    '' <summary>
    '' Purpose: Get or Set WebFormName
    '' Modify By: Pinkal
    '' </summary>
    '' 

    'Nilay (07-Feb-2016) -- Start
    'Public Property _LoanApproverTranunkid() As Integer
    Public Property _LoanApproverTranunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        'Nilay (07-Feb-2016) -- End

        Get
            Return mintlnApproverTranunkid
        End Get
        Set(ByVal value As Integer)
            mintlnApproverTranunkid = value
            'Nilay (07-Feb-2016) -- Start
            'Call GetData()
            Call GetData(objDataOp)
            'Nilay (07-Feb-2016) -- End
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LoanApproverunkid() As Integer
        Get
            Return mintlnApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintlnApproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _TranDataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _UserId() As Integer
        Get
            Return mintUserId
        End Get
        Set(ByVal value As Integer)
            mintUserId = value
        End Set
    End Property

#End Region

#Region "Constructor"

    Public Sub New()
        mdtTran = New DataTable("List")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("ischeck")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("lnapprovertranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("lnloanapproverunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ecode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("edept")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ejob")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Public Methods"

    '' <summary>
    '' Modify By: Pinkal
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>
    '' 

    'Nilay (07-Feb-2016) -- Start
    'Public Sub GetData()
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        'Nilay (07-Feb-2016) -- End


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Nilay (07-Feb-2016) -- Start
        'objDataOperation = New clsDataOperation
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
        objDataOperation = New clsDataOperation
        End If
        'Nilay (07-Feb-2016) -- End

        objDataOperation.ClearParameters()
        Try
            strQ = " SELECT " & _
                    "  CAST (0 AS BIT) AS ischeck " & _
                    " ,lnloanapprover_tran.lnapprovertranunkid " & _
                    " ,lnloanapprover_tran.lnloanapproverunkid " & _
                    " ,lnloanapprover_tran.employeeunkid " & _
                    " ,lnloanapprover_tran.userunkid " & _
                    " ,lnloanapprover_tran.isvoid " & _
                    " ,lnloanapprover_tran.voiddatetime " & _
                    " ,lnloanapprover_tran.voiduserunkid " & _
                    " ,lnloanapprover_tran.voidreason " & _
                    " ,'' AS AUD " & _
                    " ,hremployee_master.employeecode AS ecode " & _
                    " ,hremployee_master.firstname +' '+ hremployee_master.surname AS ename  " & _
                    " ,hrdepartment_master.name AS edept " & _
                    " ,hrjob_master.job_name AS ejob " & _
                " FROM lnloanapprover_tran " & _
                    " JOIN hremployee_master ON lnloanapprover_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                    " JOIN lnloanapprover_master ON lnloanapprover_tran.lnloanapproverunkid = lnloanapprover_master.lnapproverunkid " & _
                " WHERE lnloanapprover_master.lnapproverunkid = '" & mintlnApproverunkid & "' AND lnloanapprover_tran.isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()
            For Each drRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(drRow)
                ''Nilay (01-Mar-2016) -- Start
                ''ENHANCEMENT - Implementing External Approval changes 
                'mintlnApproverunkid = CInt(drRow.Item("lnloanapproverunkid"))
                'mintUserId = CInt(drRow.Item("userunkid"))
                ''Nilay (01-Mar-2016) -- End
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetData", mstrModuleName)
            'Nilay (07-Feb-2016) -- Start
        Finally
            If objDataOp Is Nothing Then objDataOperation = Nothing
            'Nilay (07-Feb-2016) -- End
        End Try

    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function Insert_Update_Delete(ByVal objDataOperation As clsDataOperation) As Boolean

        Dim i As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exFoece As Exception

        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = " INSERT INTO lnloanapprover_tran (" & _
                                            "  lnloanapproverunkid " & _
                                            " ,employeeunkid " & _
                                            " ,userunkid " & _
                                            " ,isvoid " & _
                                            " ,voiddatetime " & _
                                            " ,voiduserunkid " & _
                                            " ,voidreason " & _
                                        ") VALUES (" & _
                                            "  @lnloanapproverunkid " & _
                                            " ,@employeeunkid " & _
                                            " ,@userunkid " & _
                                            " ,@isvoid " & _
                                            " ,@voiddatetime " & _
                                            " ,@voiduserunkid " & _
                                            " ,@voidreason " & _
                                        "); SELECT @@identity "

                                objDataOperation.AddParameter("@lnloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                                mintlnApproverTranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If InsertATApprover_Tran(objDataOperation, 1, CInt(.Item("employeeunkid"))) = False Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If


                            Case "U"
                                strQ = "UPDATE lnloanapprover_tran SET " & _
                                            "  lnloanapproverunkid = @lnloanapproverunkid " & _
                                            " ,employeeunkid = @ employeeunkid " & _
                                            " ,userunkid = @ userunkid " & _
                                            " ,isvoid = @ isvoid " & _
                                            " ,voiddatetime = @ voiddatetime " & _
                                            " ,voiduserunkid = @ voiduserunkid " & _
                                            " ,voidreason = @ voidreason " & _
                                        "WHERE lnapprovertranunkid = @ lnapprovertranunkid "

                                objDataOperation.AddParameter("@lnloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@lnapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("lnapprovertranunkid"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                                mintlnApproverTranunkid = CInt(.Item("lnapprovertranunkid"))

                                If InsertATApprover_Tran(objDataOperation, 2, CInt(.Item("employeeunkid"))) = False Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                            Case "D"
                                strQ = "UPDATE lnloanapprover_tran SET " & _
                                            " isvoid = @isvoid " & _
                                            " ,voiddatetime = @voiddatetime " & _
                                            " ,voiduserunkid = @voiduserunkid " & _
                                            " ,voidreason = @voidreason " & _
                                        "WHERE lnapprovertranunkid = @lnapprovertranunkid "

                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@lnapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("lnapprovertranunkid"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                                If InsertATApprover_Tran(objDataOperation, 3, CInt(.Item("employeeunkid"))) = False Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                        End Select
                    End If

                End With
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_Update_Delete , Module Name: " & mstrModuleName)
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function IsUserMapped(ByVal iUserId As Integer, ByRef sMsg As String) As Boolean
        Dim strQ As String = String.Empty
        Dim dsList As New DataSet
        Try
            Dim objData As New clsDataOperation

            strQ = "SELECT " & _
                   "	 hremployee_master.employeecode AS ECode " & _
                   "	,hremployee_master.firstname +' '+ hremployee_master.surname AS EName " & _
                   "FROM lnloan_approver_mapping " & _
                   "	JOIN lnloanapprover_master ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid " & _
                   "	JOIN hremployee_master ON lnloanapprover_master.approverempunkid = hremployee_master.employeeunkid " & _
                   "WHERE  lnloan_approver_mapping.userunkid = '" & iUserId & "' "

            dsList = objData.ExecQuery(strQ, "List")

            If objData.ErrorMessage <> "" Then
                Throw New Exception(objData.ErrorNumber & " : " & objData.ErrorMessage)
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                sMsg = Language.getMessage(mstrModuleName, 1, "Sorry, you cannot map selected user with this employee." & vbCrLf & _
                                           "Reason user is already mapped with [ Code : ") & dsList.Tables(0).Rows(0).Item("ECode") & _
                Language.getMessage(mstrModuleName, 2, " Employee : ") & dsList.Tables(0).Rows(0).Item("EName") & Language.getMessage(mstrModuleName, 3, " ]. Please select new user to map.")
                Return True
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsUserMapped", mstrModuleName)
            Return True
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function InsertATApprover_Tran(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal intEmployeeId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@lnapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverTranunkid.ToString)
            objDataOperation.AddParameter("@lnloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 5, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            strQ = "INSERT INTO atlnloanapprover_tran (" & _
                        " lnapprovertranunkid " & _
                        ",lnloanapproverunkid " & _
                        ",employeeunkid " & _
                        ",audittype " & _
                        ",audituserunkid " & _
                        ",auditdatetime " & _
                        ",ip " & _
                        ",machine_name " & _
                        ",form_name " & _
                        ",module_name1 " & _
                        ",module_name2 " & _
                        ",module_name3 " & _
                        ",module_name4 " & _
                        ",module_name5 " & _
                        ",isweb " & _
                    ") VALUES (" & _
                        "  @lnapprovertranunkid " & _
                        ", @lnloanapproverunkid " & _
                        ", @employeeunkid " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name " & _
                        ", @form_name " & _
                        ", @module_name1 " & _
                        ", @module_name2 " & _
                        ", @module_name3 " & _
                        ", @module_name4 " & _
                        ", @module_name5 " & _
                        ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATApprover_Tran , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function Approver_Migration(ByVal dtTable As DataTable, ByVal intNewApproverId As Integer, ByVal intNewEmpapproverId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean

        'Gajanan [23-SEP-2019] -- Add {xDataOpr}    

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""
        Try


            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.

            'Dim objDataOperation As New clsDataOperation
            Dim objDataOperation As clsDataOperation

            'objDataOperation.BindTransaction()
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            End If
            'Gajanan [23-SEP-2019] -- End


            If dtTable Is Nothing Then Return True

            'Pinkal (26-Sep-2017) -- Start
            'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.
            Dim objApprovalProcessTran As New clsloanapproval_process_Tran
            Dim objOtherLoanOperation As New clsloanotherop_approval_tran
            'Pinkal (26-Sep-2017) -- End

            'Pinkal (11-Dec-2017) -- Start
            'Bug - Solved Loan Approver Migration Issue .
            Dim mintOldApproverId As Integer = 0
            'Pinkal (11-Dec-2017) -- End


            For i = 0 To dtTable.Rows.Count - 1


                strQ = " Update lnloanapprover_tran set " & _
                     " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                     " WHERE lnloanapproverunkid = @lnloanapproverunkid AND employeeunkid = @employeeunkid"

                mintlnApproverunkid = CInt(dtTable.Rows(i)("lnapproverunkid"))

                'Pinkal (11-Dec-2017) -- Start
                'Bug - Solved Loan Approver Migration Issue .
                mintOldApproverId = CInt(dtTable.Rows(i)("lnapproverunkid"))
                'Pinkal (11-Dec-2017) -- End

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@lnloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtTable.Rows(i)("lnapproverunkid")))
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 4, "Migration"))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Nilay (07-Feb-2016) -- Start
                '_LoanApproverTranunkid = CInt(dtTable.Rows(i)("lnapprovertranunkid"))
                _LoanApproverTranunkid(objDataOperation) = CInt(dtTable.Rows(i)("lnapprovertranunkid"))
                'Nilay (07-Feb-2016) -- End

                If InsertATApprover_Tran(objDataOperation, 3, CInt(dtTable.Rows(i)("employeeunkid"))) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                '===START-- GETTING ALREADY EXIST EMPLOYEE FROM NEW LOAN APPROVER AND ADDED TO MIGRATED EMPLOYEE VARIABLE.IT IS USED WHEN ALL OLD LOAN APPROVER'S EMPLOYEE TO NEW LOAN APPROVER.
                strQ = " SELECT ISNULL(employeeunkid,0) employeeunkid FROM lnloanapprover_tran WHERE lnloanapproverunkid = @lnloanapproverunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@lnloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewApproverId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                Dim dtEmpCount As DataSet = objDataOperation.ExecQuery(strQ, "List")
                If dtEmpCount.Tables(0).Rows.Count > 0 Then
                    If CInt(dtEmpCount.Tables(0).Rows(0)("employeeunkid")) > 0 Then
                        mstrEmployeeID &= dtTable.Rows(i)("employeeunkid").ToString() & ","
                        Continue For
                    End If
                End If
                '===END-- GETTING ALREADY EXIST EMPLOYEE FROM NEW LOAN APPROVER AND ADDED TO MIGRATED EMPLOYEE VARIABLE.IT IS USED WHEN ALL OLD LOAN APPROVER'S EMPLOYEE TO NEW LOAN APPROVER.


                strQ = "INSERT INTO lnloanapprover_tran ( " & _
                             " lnloanapproverunkid " & _
                             ", employeeunkid " & _
                             ", userunkid " & _
                             ", isvoid " & _
                             ", voiduserunkid " & _
                         ") VALUES (" & _
                             "  @lnloanapproverunkid " & _
                             ", @employeeunkid " & _
                             ", @userunkid " & _
                             ", @isvoid " & _
                             ", @voiduserunkid " & _
                             "); SELECT @@identity"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@lnloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewApproverId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim mintApprovertranunkid As Integer = dsList.Tables(0).Rows(0).Item(0)

                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes
                mintlnApproverunkid = intNewApproverId
                _LoanApproverTranunkid(objDataOperation) = mintApprovertranunkid
                'Nilay (01-Mar-2016) -- End

                If InsertATApprover_Tran(objDataOperation, 1, CInt(dtTable.Rows(i)("employeeunkid"))) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Pinkal (26-Sep-2017) -- Start
                'Bug - Sytem allow to delete employee from loan approver even if employee's loan application is in pending status.

                '/*  START FOR CHECK LOAN APPLICATIONS   */

                Dim mstrFormId As String = objApprovalProcessTran.GetProcessPendingLoanIds(CInt(dtTable.Rows(i)("lnapproverunkid")), dtTable.Rows(i)("employeeunkid").ToString(), objDataOperation)

                If mstrFormId.Trim.Length > 0 Then

                    strQ = " Update lnloanapproval_process_tran set approverempunkid  = @newapproverempunkid,approvertranunkid = @newapprovertranunkid  WHERE processpendingloanunkid in (" & mstrFormId & ") AND approvertranunkid = @approvertranunkid AND lnloanapproval_process_tran.isvoid = 0 "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newapproverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewEmpapproverId)
                    objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewApproverId)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtTable.Rows(i)("lnapproverunkid")))
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "Select ISNULL(pendingloantranunkid,0) pendingloantranunkid FROM lnloanapproval_process_tran WHERE approverempunkid = @approverempunkid AND approvertranunkid = @approvertranunkid AND processpendingloanunkid  in (" & mstrFormId & ") AND isvoid = 0"
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewEmpapproverId)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewApproverId)
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables(0).Rows.Count > 0 Then
                        For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lnloanapproval_process_tran", "pendingloantranunkid", dsList.Tables(0).Rows(k)("pendingloantranunkid").ToString(), False) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Next
                    End If

                End If

                '/*  END FOR CHECK LOAN APPLICATIONS   */

                '/*  START FOR CHECK OTHER LOAN OPERATION   */
                mstrFormId = ""
                mstrFormId = objOtherLoanOperation.GetApproverPendingOtherLoanOperation(CInt(dtTable.Rows(i)("lnapproverunkid")), dtTable.Rows(i)("employeeunkid").ToString, objDataOperation)

                If mstrFormId.Length > 0 Then

                    strQ = " Update lnloanotherop_approval_tran set approverempunkid  = @newapproverempunkid,approvertranunkid = @newapprovertranunkid  WHERE loanadvancetranunkid in (" & mstrFormId & ") AND approvertranunkid = @approvertranunkid AND lnloanotherop_approval_tran.isvoid = 0 "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newapproverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewEmpapproverId)
                    objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewApproverId)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtTable.Rows(i)("lnapproverunkid")))
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "Select ISNULL(lnotheroptranunkid,0) lnotheroptranunkid FROM lnloanotherop_approval_tran WHERE approverempunkid = @approverempunkid AND approvertranunkid = @approvertranunkid AND loanadvancetranunkid  in (" & mstrFormId & ") AND isvoid = 0"
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@approverempunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewEmpapproverId)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewApproverId)
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables(0).Rows.Count > 0 Then
                        For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lnloanotherop_approval_tran", "lnotheroptranunkid", dsList.Tables(0).Rows(k)("lnotheroptranunkid").ToString(), False) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Next
                    End If

                End If

                '/*  END FOR CHECK OTHER LOAN OPERATION   */

                'Pinkal (26-Sep-2017) -- End


                mstrEmployeeID &= dtTable.Rows(i)("employeeunkid").ToString() & ","

            Next


            If mstrEmployeeID.Trim.Length > 0 Then
                mstrEmployeeID = mstrEmployeeID.Substring(0, mstrEmployeeID.Trim.Length - 1)
            End If

            Dim objMigration As New clsMigration
            objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
            'Pinkal (11-Dec-2017) -- Start
            'Bug - Solved Loan Approver Migration Issue .
            'objMigration._Migrationfromunkid = mintlnApproverunkid
            objMigration._Migrationfromunkid = mintOldApproverId
            'Pinkal (11-Dec-2017) -- End
            objMigration._Migrationtounkid = intNewApproverId
            objMigration._Usertypeid = enUserType.LoanApprover
            objMigration._Userunkid = mintUserId
            objMigration._Migratedemployees = mstrEmployeeID

            If objMigration.Insert(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " SELECT ISNULL(employeeunkid,0) employeeunkid FROM lnloanapprover_tran WHERE lnloanapproverunkid = @lnloanapproverunkid AND isvoid = 0  "
            objDataOperation.ClearParameters()

            'Pinkal (11-Dec-2017) -- Start
            'Bug - Solved Loan Approver Migration Issue .
            'objDataOperation.AddParameter("@lnloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
            objDataOperation.AddParameter("@lnloanapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverId)
            'Pinkal (11-Dec-2017) -- Start


            Dim dtCount As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dtCount IsNot Nothing AndAlso dtCount.Tables(0).Rows.Count <= 0 Then

                Dim objlnApprover As New clsLoanApprover_master

                strQ = " Update lnloanapprover_master set " & _
                          " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                          " WHERE lnapproverunkid = @lnapproverunkid "

                objDataOperation.ClearParameters()
                'Pinkal (11-Dec-2017) -- Start
                'Bug - Solved Loan Approver Migration Issue .
                'objDataOperation.AddParameter("@lnapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                objDataOperation.AddParameter("@lnapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverId)
                'Pinkal (11-Dec-2017) -- End
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 4, "Migration"))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'Pinkal (11-Dec-2017) -- Start
                'Bug - Solved Loan Approver Migration Issue .
                'objlnApprover._lnApproverunkid = mintlnApproverunkid
                objlnApprover._lnApproverunkid = mintOldApproverId
                'Pinkal (11-Dec-2017) -- End
                If objlnApprover.InsertATApprover_Master(objDataOperation, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "SELECT ISNULL(approvermappingunkid,0) approvermappingunkid FROM lnloan_approver_mapping WHERE approvertranunkid = @approvertranunkid"
                objDataOperation.ClearParameters()
                'Pinkal (11-Dec-2017) -- Start
                'Bug - Solved Loan Approver Migration Issue .
                'objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverId)
                'Pinkal (11-Dec-2017) -- End
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_approver_mapping", "approvermappingunkid", CInt(dsList.Tables(0).Rows(i)("approvermappingunkid")), False, mintUserId) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

                strQ = "Delete from lnloan_approver_mapping WHERE approvertranunkid = @approvertranunkid "
                objDataOperation.ClearParameters()
                'Pinkal (11-Dec-2017) -- Start
                'Bug - Solved Loan Approver Migration Issue .
                'objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintlnApproverunkid)
                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverId)
                'Pinkal (11-Dec-2017) -- End
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.ReleaseTransaction(True)
            If xDataOpr Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Gajanan [23-SEP-2019] -- End


            Return True
        Catch ex As Exception
            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [23-SEP-2019] -- End
            Throw New Exception(ex.Message & "; Procedure Name: Approver_Migration, Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            'Gajanan [23-SEP-2019] -- Start    
            'Enhancement:Enforcement of approval migration From Transfer and recategorization.
            'objDataOperation.ReleaseTransaction(False)
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Gajanan [23-SEP-2019] -- End
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function


    'Gajanan [23-SEP-2019] -- Start    
    'Enhancement:Enforcement of approval migration From Transfer and recategorization.
    Public Function GetApproverTranIdFromEmployeeAndApprover(ByVal ApproverId As Integer, ByVal EmployeeId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Integer

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = " SELECT ISNULL(lnapprovertranunkid,0) as lnapprovertranunkid FROM lnloanapprover_tran WHERE lnloanapproverunkid = @approverunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, ApproverId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, EmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables("List").Rows(0)("lnapprovertranunkid").ToString())
            End If

            Return 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverTranIdFromEmployeeAndApprover; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
    'Gajanan [23-SEP-2019] -- End
#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, you cannot map selected user with this employee." & vbCrLf & _
                                           "Reason user is already mapped with [ Code :")
			Language.setMessage(mstrModuleName, 2, " Employee :")
			Language.setMessage(mstrModuleName, 3, " ]. Please select new user to map.")
			Language.setMessage(mstrModuleName, 4, "Migration")
			Language.setMessage(mstrModuleName, 5, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
