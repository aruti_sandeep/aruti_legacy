﻿'************************************************************************************************************************************
'Class Name : clsLoanCategoryMapping.vb
'Purpose    :
'Date       : 20-Sep-2022
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsLoanCategoryMapping
    Private Const mstrModuleName = "clsLoanCategoryMapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintMappingunkid As Integer
    Private mintLoanschemeCategoryId As Integer
    Private mintIdentityTypeUnkid As Integer
    Private mintChargesStatusunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsWeb As Boolean = False
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    '' <summary>
    '' Purpose: Get or Set mappingunkid
    '' Modify By: Hemant
    '' </summary>

    Public Property _Mappingunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
            Call GetData(objDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set schemecategoryid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _LoanschemeCategoryId() As Integer
        Get
            Return mintLoanschemeCategoryId
        End Get
        Set(ByVal value As Integer)
            mintLoanschemeCategoryId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set identitytypeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IdentityTypeUnkid() As Integer
        Get
            Return mintIdentityTypeUnkid
        End Get
        Set(ByVal value As Integer)
            mintIdentityTypeUnkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set chargestatusunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ChargeStatusunkid() As Integer
        Get
            Return mintChargesStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintChargesStatusunkid = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
    Private mintOtherDocumentunkid As Integer
    Public Property _OtherDocumentunkid() As Integer
        Get
            Return mintOtherDocumentunkid
        End Get
        Set(ByVal value As Integer)
            mintOtherDocumentunkid = value
        End Set
    End Property
    'Hemant (27 Oct 2022) -- End

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mintCRBDocumentunkid As Integer
    Public Property _CRBDocumentunkid() As Integer
        Get
            Return mintCRBDocumentunkid
        End Get
        Set(ByVal value As Integer)
            mintCRBDocumentunkid = value
        End Set
    End Property

    'Pinkal (23-Nov-2022) -- End


#End Region

    '' <summary>
    '' Modify By: Hemant
    '' </summary>
    '' <purpose> Assign all Property variable </purpose>

    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

       
        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
                      "  mappingunkid " & _
                      ", schemecategoryid " & _
                      ", identitytypeunkid " & _
                      ", chargestatusunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", ISNULL(otherdocumentunkid,0) AS otherdocumentunkid " & _
                      ", ISNULL(crbdocumentunkid,0) AS crbdocumentunkid " & _
                      "FROM lnloanscheme_category_mapping " & _
                     "WHERE mappingunkid = @mappingunkid "

            'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.[ ", ISNULL(crbdocumentunkid,0) AS crbdocumentunkid " & _]

            'Hemant (27 Oct 2022) -- [otherdocumentunkid]

            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintIdentityTypeUnkid = CInt(dtRow.Item("identitytypeunkid"))
                mintChargesStatusunkid = CInt(dtRow.Item("chargestatusunkid"))
                mintLoanschemeCategoryId = CInt(dtRow.Item("schemecategoryid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
                mintOtherDocumentunkid = CInt(dtRow.Item("otherdocumentunkid"))
                'Hemant (27 Oct 2022) -- End

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                mintCRBDocumentunkid = CInt(dtRow.Item("crbdocumentunkid"))
                'Pinkal (23-Nov-2022) -- End

                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intSchemeCategoryId As Integer = -1, Optional ByVal objDataOp As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim mdicSchemeCategory As New Dictionary(Of Integer, String)
        Dim dsSchemeCategory As DataSet = (New clsLoan_Advance).GetLoan_Scheme_Categories("List", True)
        mdicSchemeCategory = (From p In dsSchemeCategory.Tables("List") Select New With {Key .Id = CInt(p.Item("Id")), Key .Name = p.Item("name").ToString}).ToDictionary(Function(x) CInt(x.Id), Function(y) y.Name)


        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  mappingunkid " & _
                      ", schemecategoryid " & _
                      ", identitytypeunkid " & _
                      ", chargestatusunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason "

            If mdicSchemeCategory.Count > 0 Then
                strQ &= ", CASE "
                For Each key In mdicSchemeCategory
                    strQ &= " WHEN schemecategoryid = " & key.Key & " THEN '" & key.Value & "' "
                Next
                strQ &= " END AS SchemeCategory "
            End If

            strQ &= ", ISNULL(cfcommon_master.name, '') AS identitytype " & _
                    ", ISNULL(hrdisciplinestatus_master.name, '') as chargestatus " & _
                    ", ISNULL(otherdocumentunkid,0) AS otherdocumentunkid " & _
                        ", ISNULL(otherdocument.name, '') as otherdocument " & _
                        ", ISNULL(crbdocumentunkid,0) AS crbdocumentunkid " & _
                        ", ISNULL(cbdoc.name, '') as crbdocument "

            'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.[ISNULL(crbdocumentunkid,0) AS crbdocumentunkid , ISNULL(cbdoc.name, '') as crbdocument ]


            strQ &= " FROM lnloanscheme_category_mapping " & _
                      " LEFT JOIN cfcommon_master on cfcommon_master.masterunkid = lnloanscheme_category_mapping.identitytypeunkid AND cfcommon_master.isactive = 1 and mastertype = " & clsCommon_Master.enCommonMaster.IDENTITY_TYPES & " " & _
                      " LEFT JOIN hrdisciplinestatus_master ON hrdisciplinestatus_master.disciplinestatusunkid = lnloanscheme_category_mapping.chargestatusunkid AND hrdisciplinestatus_master.isactive = 1 " & _
                      " LEFT JOIN cfcommon_master otherdocument on otherdocument.masterunkid = lnloanscheme_category_mapping.otherdocumentunkid AND otherdocument.isactive = 1 and otherdocument.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " " & _
                      " LEFT JOIN cfcommon_master cbdoc on cbdoc.masterunkid = lnloanscheme_category_mapping.crbdocumentunkid AND cbdoc.isactive = 1 and cbdoc.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " " & _
                      " WHERE isvoid = 0 "

            'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.[" LEFT JOIN cfcommon_master cbdoc on cbdoc.masterunkid = lnloanscheme_category_mapping.crbdocumentunkid AND cbdoc.isactive = 1 and cbdoc.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " " & _ ]

            'Hemant (27 Oct 2022) -- [otherdocumentunkid,otherdocument]
            If intSchemeCategoryId > 0 Then
                strQ &= " AND lnloanscheme_category_mapping.schemecategoryid = @schemecategoryid "
                objDataOperation.AddParameter("@schemecategoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSchemeCategoryId.ToString)
            End If

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            strQ &= " ORDER BY schemecategoryid "
            'Pinkal (23-Nov-2022) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDataOp Is Nothing Then objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloanscheme_category_mapping) </purpose>
    Public Function Insert() As Boolean

        'Pinkal (23-Nov-2022) -- Start
        'NMB Loan Module Enhancement.
        'If isExist(mintLoanschemeCategoryId, -1) Then
        If isExist(mintLoanschemeCategoryId, mintIdentityTypeUnkid, -1) Then
            'Pinkal (23-Nov-2022) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Loan Scheme category is already defined. Please define new Loan Scheme Category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@schemecategoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeCategoryId.ToString)
            objDataOperation.AddParameter("@identitytypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIdentityTypeUnkid.ToString)
            objDataOperation.AddParameter("@chargestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChargesStatusunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            objDataOperation.AddParameter("@otherdocumentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherDocumentunkid.ToString)
            'Hemant (27 Oct 2022) -- End

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@crbdocumentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCRBDocumentunkid.ToString)
            'Pinkal (23-Nov-2022) -- End



            strQ = "INSERT INTO lnloanscheme_category_mapping ( " & _
                       " schemecategoryid " & _
                       ",  identitytypeunkid " & _
                       ", chargestatusunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason" & _
                       ", otherdocumentunkid " & _
                       ", crbdocumentunkid " & _
                      ") VALUES (" & _
                        "  @schemecategoryid " & _
                        ", @identitytypeunkid " & _
                        ", @chargestatusunkid " & _
                        ", @isvoid " & _
                        ", @voiduserunkid " & _
                        ", @voiddatetime " & _
                        ", @voidreason" & _
                        ", @otherdocumentunkid " & _
                        ", @crbdocumentunkid " & _
                      "); SELECT @@identity"

            'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.[@crbdocumentunkid]

            'Hemant (27 Oct 2022) -- [otherdocumentunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintMappingunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function Update() As Boolean

        'Pinkal (23-Nov-2022) -- Start
        'NMB Loan Module Enhancement.
        'If isExist(mintLoanschemeCategoryId, mintMappingunkid) Then
        If isExist(mintLoanschemeCategoryId, mintIdentityTypeUnkid, mintMappingunkid) Then
            'Pinkal (23-Nov-2022) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Scheme Category is already defined. Please define new Loan Scheme Category.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        objDataOperation.BindTransaction()


        Try
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@schemecategoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeCategoryId.ToString)
            objDataOperation.AddParameter("@identitytypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIdentityTypeUnkid.ToString)
            objDataOperation.AddParameter("@chargestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChargesStatusunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            objDataOperation.AddParameter("@otherdocumentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherDocumentunkid.ToString)
            'Hemant (27 Oct 2022) -- End

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@crbdocumentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCRBDocumentunkid.ToString)
            'Pinkal (23-Nov-2022) -- End


            strQ = "UPDATE lnloanscheme_category_mapping SET " & _
                     "  schemecategoryid = @schemecategoryid " & _
                     ", identitytypeunkid = @identitytypeunkid " & _
                     ", chargestatusunkid = @chargestatusunkid " & _
                     ", isvoid = @isvoid " & _
                     ", voiduserunkid = @voiduserunkid " & _
                     ", voiddatetime = @voiddatetime " & _
                     ", voidreason = @voidreason " & _
                     ", otherdocumentunkid = @otherdocumentunkid " & _
                     ", crbdocumentunkid = @crbdocumentunkid " & _
                    "WHERE mappingunkid = @mappingunkid "

            'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.[crbdocumentunkid = @crbdocumentunkid]

            'Hemant (27 Oct 2022) -- [otherdocumentunkid]
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)


            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloanscheme_category_mapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try


            strQ = " UPDATE lnloanscheme_category_mapping set " & _
                      "  isvoid = @isvoid " & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime=@voiddatetime  " & _
                      ", voidreason=@voidreason  " & _
                      " WHERE mappingunkid = @mappingunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            _Mappingunkid = intUnkid

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)

            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intschemecategoryid As Integer, ByVal xIdentityTypeunkid As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal mobjDataOperation As clsDataOperation = Nothing) As Boolean
        'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.[ByVal xIdentityTypeunkid As Integer]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mobjDataOperation IsNot Nothing Then
            objDataOperation = mobjDataOperation
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  mappingunkid " & _
                      ", schemecategoryid " & _
                      ", identitytypeunkid " & _
                      ", chargestatusunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", ISNULL(otherdocumentunkid,0) AS otherdocumentunkid " & _
                      ", ISNULL(crbdocumentunkid,0) AS crbdocumentunkid " & _
                      " FROM lnloanscheme_category_mapping " & _
                      " WHERE  isvoid = 0 " & _
                      " AND schemecategoryid = @schemecategoryid " & _
                      " AND identitytypeunkid  =  @identitytypeunkid "

            'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.["identitytypeunkid  =  @identitytypeunkid , ISNULL(crbdocumentunkid,0) AS crbdocumentunkid " & _]


            'Hemant (27 Oct 2022) -- [otherdocumentunkid]
            If intUnkid > 0 Then
                strQ &= " AND mappingunkid <> @mappingunkid"
            End If

            objDataOperation.AddParameter("@schemecategoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, intschemecategoryid)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@identitytypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xIdentityTypeunkid)
            'Pinkal (23-Nov-2022) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@schemecategoryid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeCategoryId.ToString)
            objDataOperation.AddParameter("@identitytypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIdentityTypeUnkid.ToString)
            objDataOperation.AddParameter("@chargestatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChargesStatusunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-1001 - As a user, where an application is an exceptional application, I want to have the ability to attach supporting documents. This option will only show on the application and approval pages only when the exceptional option is ticked. Otherwise not
            objDataOperation.AddParameter("@otherdocumentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOtherDocumentunkid.ToString)
            'Hemant (27 Oct 2022) -- End

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@crbdocumentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCRBDocumentunkid.ToString)
            'Pinkal (23-Nov-2022) -- End


            strQ = "INSERT INTO atlnloanscheme_category_mapping ( " & _
                                "  tranguid " & _
                                ", mappingunkid " & _
                                ", schemecategoryid " & _
                                ", identitytypeunkid " & _
                                ", chargestatusunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", host " & _
                                ", form_name " & _
                                ", isweb " & _
                                ", otherdocumentunkid " & _
                                ", crbdocumentunkid " & _
                              ") VALUES (" & _
                                "  LOWER(NEWID()) " & _
                                ", @mappingunkid " & _
                                ", @schemecategoryid " & _
                                ", @identitytypeunkid " & _
                                ", @chargestatusunkid " & _
                                ", @audittype " & _
                                ", @audituserunkid " & _
                                ", @auditdatetime " & _
                                ", @ip " & _
                                ", @host " & _
                                ", @form_name " & _
                                ", @isweb " & _
                                ", @otherdocumentunkid " & _
                                ", @crbdocumentunkid " & _
                              "); SELECT @@identity"



            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function


  

End Class
