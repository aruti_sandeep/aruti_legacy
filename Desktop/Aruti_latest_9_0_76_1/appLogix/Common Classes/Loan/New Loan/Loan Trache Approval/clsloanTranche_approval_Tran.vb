﻿'************************************************************************************************************************************
'Class Name : clsloanTranche_approval_Tran.vb
'Purpose    :
'Date       :24-Aug-2023
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Data.OracleClient

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsloanTranche_approval_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsloanTranche_approval_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLoantrancheapprovalunkid As Integer
    Private mintLoantrancherequestunkid As Integer
    Private mintProcesspendingloanunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintMappingunkid As Integer
    Private mintRoleunkid As Integer
    Private mintLevelunkid As Integer
    Private mintPriority As Integer
    Private mintMapuserunkid As Integer
    Private mintDeductionperiodunkid As Integer
    Private mdtApprovaldate As Date
    Private mdecLoantranche_Amount As Decimal
    Private mintCountryunkid As Integer
    Private mintStatusunkid As Integer
    Private mintVisibleunkid As Integer
    Private mstrRemark As String = String.Empty
    Private mdtDocument As DataTable = Nothing
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0

    'Pinkal (16-Nov-2023) -- Start
    '(A1X-1489) NMB - Mortgage tranche disbursement API.
    Private mstrLoanSchemeCode As String = String.Empty
    Private mstrCurrency As String = String.Empty
    'Pinkal (16-Nov-2023) -- End


#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loantrancheapprovalunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loantrancheapprovalunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintLoantrancheapprovalunkid
        End Get
        Set(ByVal value As Integer)
            mintLoantrancheapprovalunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loantrancherequestunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loantrancherequestunkid() As Integer
        Get
            Return mintLoantrancherequestunkid
        End Get
        Set(ByVal value As Integer)
            mintLoantrancherequestunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processpendingloanunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Processpendingloanunkid() As Integer
        Get
            Return mintProcesspendingloanunkid
        End Get
        Set(ByVal value As Integer)
            mintProcesspendingloanunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mappingunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mappingunkid() As Integer
        Get
            Return mintMappingunkid
        End Get
        Set(ByVal value As Integer)
            mintMappingunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set roleunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Roleunkid() As Integer
        Get
            Return mintRoleunkid
        End Get
        Set(ByVal value As Integer)
            mintRoleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mapuserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Mapuserunkid() As Integer
        Get
            Return mintMapuserunkid
        End Get
        Set(ByVal value As Integer)
            mintMapuserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deductionperiodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Deductionperiodunkid() As Integer
        Get
            Return mintDeductionperiodunkid
        End Get
        Set(ByVal value As Integer)
            mintDeductionperiodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvaldate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Approvaldate() As Date
        Get
            Return mdtApprovaldate
        End Get
        Set(ByVal value As Date)
            mdtApprovaldate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loantranche_amount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loantranche_Amount() As Decimal
        Get
            Return mdecLoantranche_Amount
        End Get
        Set(ByVal value As Decimal)
            mdecLoantranche_Amount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set visibleunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Visibleunkid() As Integer
        Get
            Return mintVisibleunkid
        End Get
        Set(ByVal value As Integer)
            mintVisibleunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Document
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Document() As DataTable
        Get
            Return mdtDocument
        End Get
        Set(ByVal value As DataTable)
            mdtDocument = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    'Pinkal (16-Nov-2023) -- Start
    '(A1X-1489) NMB - Mortgage tranche disbursement API.
    ''' <summary>
    ''' Purpose: Get or Set LoanSchemeCode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LoanSchemeCode() As String
        Get
            Return mstrLoanSchemeCode
        End Get
        Set(ByVal value As String)
            mstrLoanSchemeCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Currency
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Currency() As String
        Get
            Return mstrCurrency
        End Get
        Set(ByVal value As String)
            mstrCurrency = value
        End Set
    End Property

    'Pinkal (16-Nov-2023) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If


        Try
            strQ = "SELECT " & _
                      "  loantrancheapprovalunkid " & _
                      ", loantrancherequestunkid " & _
                      ", processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", mappingunkid " & _
                      ", roleunkid " & _
                      ", levelunkid " & _
                      ", priority " & _
                      ", mapuserunkid " & _
                      ", deductionperiodunkid " & _
                      ", approvaldate " & _
                      ", loantranche_amount " & _
                      ", countryunkid " & _
                      ", statusunkid " & _
                      ", visibleunkid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                     " FROM lnloantranche_approval_tran " & _
                     " WHERE loantrancheapprovalunkid = @loantrancheapprovalunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancheapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancheapprovalunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLoantrancheapprovalunkid = CInt(dtRow.Item("loantrancheapprovalunkid"))
                mintLoantrancherequestunkid = CInt(dtRow.Item("loantrancherequestunkid"))
                mintProcesspendingloanunkid = CInt(dtRow.Item("processpendingloanunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintMappingunkid = CInt(dtRow.Item("mappingunkid"))
                mintRoleunkid = CInt(dtRow.Item("roleunkid"))
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mintPriority = CInt(dtRow.Item("priority"))
                mintMapuserunkid = CInt(dtRow.Item("mapuserunkid"))
                mintDeductionperiodunkid = CInt(dtRow.Item("deductionperiodunkid"))

                If IsDBNull(dtRow.Item("approvaldate")) = False Then
                    mdtApprovaldate = dtRow.Item("approvaldate")
                End If

                mdecLoantranche_Amount = CDec(dtRow.Item("loantranche_amount"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintVisibleunkid = CInt(dtRow.Item("visibleunkid"))
                mstrRemark = dtRow.Item("remark").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))

                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If

                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLoanTrancheApprovalList(ByVal xDatabaseName As String, _
                                    ByVal xUserUnkid As Integer, _
                                    ByVal xYearUnkid As Integer, _
                                    ByVal xCompanyUnkid As Integer, _
                                    ByVal xPeriodStart As DateTime, _
                                    ByVal xPeriodEnd As DateTime, _
                                    ByVal xUserModeSetting As String, _
                                    ByVal xOnlyApproved As Boolean, _
                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                    ByVal strTableName As String, _
                                    Optional ByVal mstrFilter As String = "", _
                                    Optional ByVal strAdvanceFilter As String = "", _
                                    Optional ByVal mblnIncludeGroup As Boolean = True, _
                                    Optional ByVal objDataOp As clsDataOperation = Nothing) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOp
        End If

        Try

            Dim xDateJoinQry, xDateFilterQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName, "Emp")
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName, "Emp")


            If mblnIncludeGroup Then

                strQ = " SELECT " & _
                          " -1 AS loantrancheapprovalunkid " & _
                          ", lta.loantrancherequestunkid " & _
                          ", lta.loantrancherequestunkid " & _
                          ", ltr.applicationno AS ApplicationNo " & _
                          ", NULL AS ApplicationDate " & _
                          ", lta.employeeunkid " & _
                          ",'' AS Employee " & _
                          ",'' AS EmployeeName " & _
                          ",-1 AS mappingunkid " & _
                          ",'' AS LoanScheme " & _
                          ",-1 AS roleunkid " & _
                          ",'' AS Role " & _
                          ",-1 AS levelunkid " & _
                          ",'' AS Level " & _
                          ",-1 AS priority " & _
                          ",-1 AS mapuserunkid " & _
                          ",''  AS CUser " & _
                          ",'' AS loginuser " & _
                          ",''  AS UserName " & _
                          ",''  AS UserEmail " & _
                          ",-1 AS deductionperiodunkid " & _
                          ",'' AS Period " & _
                          ",NULL AS approvaldate " & _
                          ", 0.00 AS LoanTrancheAmt " & _
                          ", 0.00 AS ApprovedLoanTrancheamt " & _
                          ", 0.00 AS ltapproved_trancheamt  " & _
                          ",-1 AS countryunkid " & _
                          ",'' AS Currency " & _
                          ",'' Currencysign " & _
                          ", ltr.statusunkid AS Appstatusunkid " & _
                          ",-1 AS statusunkid " & _
                          ",'' AS Status " & _
                          ",0 AS visibleunkid " & _
                          ",'' AS remark " & _
                          ",-1 AS userunkid " & _
                          ",0 AS isvoid " & _
                          ",NULL AS voiddatetime " & _
                          ",-1 AS voiduserunkid " & _
                          ",'' AS voidreason " & _
                          ",-1 AS stationunkid " & _
                          ",-1 AS deptgroupunkid " & _
                          " ,-1 AS departmentunkid " & _
                          " ,-1 AS sectiongroupunkid " & _
                          " ,-1 AS sectionunkid " & _
                          " ,-1 AS unitgroupunkid " & _
                          " ,-1 AS unitunkid " & _
                          " ,-1 AS teamunkid " & _
                          " ,-1 AS classgroupunkid " & _
                          " ,-1 AS classunkid " & _
                          " ,-1 AS jobgroupunkid " & _
                          " ,-1 AS jobunkid " & _
                          "  ,-1 AS gradegroupunkid " & _
                          "  ,-1 AS gradeunkid " & _
                          " ,-1 AS gradelevelunkid " & _
                          ", '' AS station " & _
                          ", '' AS deptgrp " & _
                          ", '' AS department " & _
                          ", '' AS sectiongrp " & _
                          ", '' AS section " & _
                          ", '' AS unitgrp " & _
                          ", '' AS unit " & _
                          ", '' AS team " & _
                          ", '' AS classgrp " & _
                          ", '' AS classes " & _
                          ", '' AS jobgrp " & _
                          ", '' AS job " & _
                          ", 1 AS isgrp " & _
                          " FROM lnloantranche_approval_tran lta  " & _
                          " LEFT JOIN lnloantranche_request_tran ltr ON lta.loantrancherequestunkid = ltr.loantrancherequestunkid AND ltr.isvoid = 0 " & _
                          " LEFT JOIN hremployee_master AS Emp ON Emp.employeeunkid = ltr.employeeunkid " & _
                          " LEFT JOIN hremployee_master rp ON rp.employeeunkid = lta.mapuserunkid " & _
                          " LEFT JOIN lnapproverlevel_master lvl ON lvl.lnlevelunkid = lta.levelunkid " & _
                          " LEFT JOIN cfcommon_period_tran p ON p.periodunkid = lta.deductionperiodunkid " & _
                          " LEFT JOIN cfexchange_rate ex ON ex.countryunkid = lta.countryunkid " & _
                          " LEFT JOIN hrmsConfiguration..cfrole_master r ON r.roleunkid = lta.roleunkid "

                If xDateJoinQry.Trim.Length > 0 Then
                    strQ &= xDateJoinQry '
                End If

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    strQ &= xAdvanceJoinQry
                End If

                strQ &= " WHERE lta.isvoid = 0  "

                If xIncludeIn_ActiveEmployee = False Then
                    If xDateFilterQry.Trim.Length > 0 Then
                        strQ &= xDateFilterQry
                    End If
                End If

                If mstrFilter.Trim.Length > 0 Then
                    strQ &= " AND " & mstrFilter
                End If

                If strAdvanceFilter.Trim.Length > 0 Then
                    strQ &= " AND " & strAdvanceFilter
                End If

                strQ &= " UNION "

            End If


            strQ &= "SELECT " & _
                      "  lta.loantrancheapprovalunkid " & _
                      ", lta.loantrancherequestunkid " & _
                      ", lta.processpendingloanunkid " & _
                      ", ltr.applicationno AS ApplicationNo " & _
                      ", ltr.tranchedate AS ApplicationDate " & _
                      ", lta.employeeunkid " & _
                      ", ISNULL(Emp.employeecode, '') + ' - ' + ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.surname, '') AS Employee " & _
                      ", ISNULL(Emp.firstname, '') + ' ' + ISNULL(Emp.surname, '') AS EmployeeName " & _
                      ", lta.mappingunkid " & _
                      ", ISNULL(s.name,'') AS LoanScheme " & _
                      ", lta.roleunkid " & _
                      ", ISNULL(r.name, '') AS Role " & _
                      ", lta.levelunkid " & _
                      ", ISNULL(lvl.lnlevelname, '') AS Level " & _
                      ", lta.priority " & _
                      ", lta.mapuserunkid " & _
                      ", ISNULL(u.username,'') + ' - ' + ISNULL(lvl.lnlevelname, '')  AS CUser  " & _
                      ", ISNULL(u.username,'') AS loginuser  " & _
                      ", ISNULL(u.firstname,'') + ' ' + ISNULL(u.lastname,'')  AS UserName " & _
                      ", ISNULL(u.email,'')  AS UserEmail " & _
                      ", lta.deductionperiodunkid " & _
                      ", ISNULL(p.period_name, '') AS Period " & _
                      ", lta.approvaldate " & _
                      ", ltr.trancheamt AS LoanTrancheAmt " & _
                      ",CASE WHEN lta.statusunkid = 1 THEN 0.00 " & _
                      "          WHEN lta.statusunkid = 2 THEN lta.loantranche_amount " & _
                      "          WHEN lta.statusunkid = 3 THEN 0.00 END as ApprovedLoanTrancheamt " & _
                      ", ltr.approved_trancheamt AS ltapproved_trancheamt  " & _
                      ", lta.countryunkid " & _
                      ", ISNULL(ex.currency_name, '') AS Currency " & _
                      ", ISNULL(ex.currency_sign, '') AS Currencysign " & _
                      ", ltr.statusunkid AS Appstatusunkid " & _
                      ", lta.statusunkid " & _
                      ",CASE WHEN lta.statusunkid =1 THEN @Pending " & _
                      "          WHEN lta.statusunkid =2 THEN @Approved " & _
                      "          WHEN lta.statusunkid =3 THEN @Rejected " & _
                      "  END AS Status " & _
                      ", lta.visibleunkid " & _
                      ", lta.remark " & _
                      ", lta.userunkid " & _
                      ", lta.isvoid " & _
                      ", lta.voiddatetime " & _
                      ", lta.voiduserunkid " & _
                      ", lta.voidreason " & _
                      ",ISNULL(ETT.stationunkid, 0) AS stationunkid  " & _
                      ",ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid  " & _
                      ",ISNULL(ETT.departmentunkid, 0) AS departmentunkid  " & _
                      ",ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid  " & _
                      ",ISNULL(ETT.sectionunkid, 0) AS sectionunkid  " & _
                      ",ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid  " & _
                      ",ISNULL(ETT.unitunkid, 0) AS unitunkid  " & _
                      ",ISNULL(ETT.teamunkid, 0) AS teamunkid  " & _
                      ",ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid  " & _
                      ",ISNULL(ETT.classunkid, 0) AS classunkid  " & _
                      ",ISNULL(ECT.jobgroupunkid, 0) AS jobgroupunkid  " & _
                      ",ISNULL(ECT.jobunkid, 0) AS jobunkid  " & _
                      ",ISNULL(GRD.gradegroupunkid, 0) AS gradegroupunkid  " & _
                      ",ISNULL(GRD.gradeunkid, 0) AS gradeunkid  " & _
                      ",ISNULL(GRD.gradelevelunkid, 0) AS gradelevelunkid  " & _
                      ", ISNULL(hrstation_master.name,'') AS station " & _
                      ", ISNULL(hrdepartment_group_master.name,'') AS deptgrp " & _
                      ", ISNULL(hrdepartment_master.name,'') AS department " & _
                      ", ISNULL(hrsectiongroup_master.name,'') AS sectiongrp " & _
                      ", ISNULL(hrsection_master.name,'') AS section " & _
                      ", ISNULL(hrunitgroup_master.name,'') AS unitgrp " & _
                      ", ISNULL(hrunit_master.name,'') AS unit " & _
                      ", ISNULL(hrteam_master.name,'') AS team " & _
                      ", ISNULL(hrclassgroup_master.name,'') AS classgrp " & _
                      ", ISNULL(hrclasses_master.name,'') AS classes " & _
                      ", ISNULL(hrjobgroup_master.name,'') AS jobgrp " & _
                      ", ISNULL(hrjob_master.job_name,'') AS job " & _
                      ", 0 AS isgrp " & _
                      " FROM lnloantranche_approval_tran lta " & _
                      " LEFT JOIN lnloantranche_request_tran ltr ON ltr.loantrancherequestunkid = lta.loantrancherequestunkid AND ltr.isvoid = 0 " & _
                      " LEFT JOIN lnloan_scheme_master s ON s.loanschemeunkid = ltr.loanschemeunkid  " & _
                      " LEFT JOIN hremployee_master AS Emp  ON Emp.employeeunkid = ltr.employeeunkid " & _
                      " LEFT JOIN lnapproverlevel_master lvl ON lvl.lnlevelunkid = lta.levelunkid " & _
                      " LEFT JOIN cfcommon_period_tran p ON p.periodunkid = lta.deductionperiodunkid " & _
                      " LEFT JOIN cfexchange_rate ex ON ex.countryunkid = lta.countryunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfrole_master r ON r.roleunkid = lta.roleunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfuser_master u ON u.userunkid = lta.mapuserunkid " & _
                       " LEFT JOIN " & _
                       "( " & _
                       "   SELECT " & _
                       "        stationunkid " & _
                       "       ,deptgroupunkid " & _
                       "       ,departmentunkid " & _
                       "       ,sectiongroupunkid " & _
                       "       ,sectionunkid " & _
                       "       ,unitgroupunkid " & _
                       "       ,unitunkid " & _
                       "       ,teamunkid " & _
                       "       ,classgroupunkid " & _
                       "       ,classunkid " & _
                       "       ,employeeunkid " & _
                       "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                       "   FROM hremployee_transfer_tran " & _
                       "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                       ") AS ETT ON ETT.employeeunkid = Emp.employeeunkid AND ETT.rno = 1 " & _
                       " LEFT JOIN " & _
                       "( " & _
                        "   SELECT " & _
                        "        jobgroupunkid " & _
                        "       ,jobunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                        "   FROM hremployee_categorization_tran " & _
                        "   WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        ") AS ECT ON ECT.employeeunkid = Emp.employeeunkid AND ECT.rno = 1 " & _
                        " LEFT JOIN " & _
                        "( " & _
                        "   SELECT " & _
                        "        gradegroupunkid " & _
                        "       ,gradeunkid " & _
                        "       ,gradelevelunkid " & _
                        "       ,employeeunkid " & _
                        "       ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY incrementdate DESC,salaryincrementtranunkid DESC) AS rno " & _
                        "   FROM prsalaryincrement_tran " & _
                        "   WHERE isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "' " & _
                        " ) AS GRD ON GRD.employeeunkid = Emp.employeeunkid AND GRD.rno = 1 " & _
                        " LEFT JOIN hrstation_master ON ETT.stationunkid = hrstation_master.stationunkid AND hrstation_master.isactive = 1 " & _
                        " LEFT JOIN hrdepartment_group_master ON ETT.deptgroupunkid = hrdepartment_group_master.deptgroupunkid AND hrdepartment_group_master.isactive = 1 " & _
                        " LEFT JOIN hrdepartment_master ON ETT.departmentunkid = hrdepartment_master.departmentunkid AND hrdepartment_master.isactive = 1 " & _
                        " LEFT JOIN hrsectiongroup_master ON ETT.sectiongroupunkid = hrsectiongroup_master.sectiongroupunkid AND hrsectiongroup_master.isactive = 1 " & _
                        " LEFT JOIN hrsection_master ON ETT.sectionunkid = hrsection_master.sectionunkid AND hrsection_master.isactive = 1 " & _
                        " LEFT JOIN hrunitgroup_master ON ETT.unitgroupunkid = hrunitgroup_master.unitgroupunkid AND hrunitgroup_master.isactive = 1 " & _
                        " LEFT JOIN hrunit_master ON ETT.unitunkid = hrunit_master.unitunkid AND hrunit_master.isactive = 1 " & _
                        " LEFT JOIN hrteam_master ON ETT.teamunkid = hrteam_master.teamunkid AND hrteam_master.isactive = 1 " & _
                        " LEFT JOIN hrclassgroup_master ON ETT.classgroupunkid = hrclassgroup_master.classgroupunkid AND hrclassgroup_master.isactive = 1 " & _
                        " LEFT JOIN hrclasses_master ON ETT.classunkid = hrclasses_master.classesunkid AND hrclasses_master.isactive = 1 " & _
                        " LEFT JOIN hrjob_master ON ECT.jobunkid = hrjob_master.jobunkid AND hrjob_master.isactive = 1 " & _
                        " LEFT JOIN hrjobgroup_master ON ECT.jobgroupunkid = hrjobgroup_master.jobgroupunkid AND hrjobgroup_master.isactive = 1 "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry '
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= " WHERE lta.isvoid = 0 AND lta.mappingunkid > 0 AND lta.roleunkid > 0 AND lta.levelunkid > 0 "

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            If strAdvanceFilter.Trim.Length > 0 Then
                strQ &= " AND " & strAdvanceFilter
            End If

            strQ &= " ORDER BY ApplicationNo DESC,priority,isgrp DESC "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Pending"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Approved"))
            objDataOperation.AddParameter("@Rejected", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Rejected"))
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanTrancheApprovalList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloantranche_approval_tran) </purpose>
    Public Function Insert(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancherequestunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)

            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@loantranche_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoantranche_Amount.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)

            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO lnloantranche_approval_tran ( " & _
                      "  loantrancherequestunkid " & _
                      ", processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", mappingunkid " & _
                      ", roleunkid " & _
                      ", levelunkid " & _
                      ", priority " & _
                      ", mapuserunkid " & _
                      ", deductionperiodunkid " & _
                      ", approvaldate " & _
                      ", loantranche_amount " & _
                      ", countryunkid " & _
                      ", statusunkid " & _
                      ", visibleunkid " & _
                      ", remark " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                    ") VALUES (" & _
                      "  @loantrancherequestunkid " & _
                      ", @processpendingloanunkid " & _
                      ", @employeeunkid " & _
                      ", @mappingunkid " & _
                      ", @roleunkid " & _
                      ", @levelunkid " & _
                      ", @priority " & _
                      ", @mapuserunkid " & _
                      ", @deductionperiodunkid " & _
                      ", @approvaldate " & _
                      ", @loantranche_amount " & _
                      ", @countryunkid " & _
                      ", @statusunkid " & _
                      ", @visibleunkid " & _
                      ", @remark " & _
                      ", @userunkid " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLoantrancheapprovalunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrailForLoanTrancheApproval(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloantranche_approval_tran) </purpose>
    Public Function Update(ByVal xDatabaseName As String, _
                                    ByVal xUserUnkid As Integer, _
                                    ByVal xYearUnkid As Integer, _
                                    ByVal xCompanyUnkid As Integer, _
                                    ByVal xPeriodStart As DateTime, _
                                    ByVal xPeriodEnd As DateTime, _
                                    ByVal xUserModeSetting As String, _
                                    ByVal xOnlyApproved As Boolean, _
                                    ByVal xIncludeIn_ActiveEmployee As Boolean) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Dim xLoantrancheapprovalunkid As Integer = mintLoantrancheapprovalunkid

        Dim mstrDocumentPath As String = ""
        Dim mintEmpNotificationTrancheDateBeforeDays As Integer = 0

        Dim objConfig As New clsConfigOptions
        mstrDocumentPath = objConfig.GetKeyValue(xCompanyUnkid, "DocumentPath", Nothing)
        mintEmpNotificationTrancheDateBeforeDays = CInt(objConfig.GetKeyValue(xCompanyUnkid, "EmpNotificationTrancheDateBeforeDays", Nothing))
        objConfig = Nothing

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancheapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancheapprovalunkid.ToString)
            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancherequestunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)

            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@loantranche_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoantranche_Amount.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE lnloantranche_approval_tran SET " & _
                      "  loantrancherequestunkid = @loantrancherequestunkid" & _
                      ", processpendingloanunkid = @processpendingloanunkid" & _
                      ", employeeunkid = @employeeunkid" & _
                      ", mappingunkid = @mappingunkid" & _
                      ", roleunkid = @roleunkid" & _
                      ", levelunkid = @levelunkid" & _
                      ", priority = @priority" & _
                      ", mapuserunkid = @mapuserunkid" & _
                      ", deductionperiodunkid = @deductionperiodunkid" & _
                      ", approvaldate = @approvaldate" & _
                      ", loantranche_amount = @loantranche_amount" & _
                      ", countryunkid = @countryunkid" & _
                      ", statusunkid = @statusunkid" & _
                      ", visibleunkid = @visibleunkid" & _
                      ", remark = @remark" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      " WHERE isvoid = 0 AND loantrancheapprovalunkid = @loantrancheapprovalunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mintStatusunkid = enLoanApplicationStatus.REJECTED Then
                If InsertAuditTrailForLoanTrancheApproval(objDataOperation, enAuditType.EDIT) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            strQ = " SELECT loantrancheapprovalunkid,loantrancherequestunkid,mappingunkid, roleunkid,levelunkid,priority,visibleunkid FROM lnloantranche_approval_tran WHERE isvoid = 0 AND loantrancherequestunkid = @loantrancherequestunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancherequestunkid.ToString)
            Dim dsApprover As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim dtVisibility As DataTable = Nothing
            If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

                strQ = " UPDATE lnloantranche_approval_tran set " & _
                          " visibleunkid = " & mintStatusunkid & _
                          " WHERE  loantrancheapprovalunkid = @loantrancheapprovalunkid AND loantrancherequestunkid = @loantrancherequestunkid and employeeunkid = @employeeunkid " & _
                          " AND mappingunkid = @mappingunkid AND roleunkid = @roleunkid AND levelunkid = @levelunkid  AND isvoid = 0   "

                dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & mintPriority, "", DataViewRowState.CurrentRows).ToTable

                For i As Integer = 0 To dtVisibility.Rows.Count - 1
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@loantrancheapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("loantrancheapprovalunkid").ToString)
                    objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("loantrancherequestunkid").ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                    objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("mappingunkid").ToString)
                    objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("roleunkid").ToString)
                    objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("levelunkid").ToString)
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If  'If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count > 0 Then

            Dim intMinPriority As Integer = dsApprover.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("priority") > mintPriority).Select(Function(x) x.Field(Of Integer)("priority")).DefaultIfEmpty().Min()

            dtVisibility = New DataView(dsApprover.Tables(0), "priority = " & intMinPriority & " AND priority <> -1", "", DataViewRowState.CurrentRows).ToTable

            If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then

                strQ = " UPDATE lnloantranche_approval_tran set " & _
                             " visibleunkid = @visibleunkid " & _
                             ", deductionperiodunkid=@deductionperiodunkid" & _
                             ", loantranche_amount=@loantranche_amount" & _
                             ", countryunkid=@countryunkid" & _
                             " WHERE  loantrancheapprovalunkid = @loantrancheapprovalunkid AND loantrancherequestunkid = @loantrancherequestunkid  " & _
                             " AND employeeunkid = @employeeunkid AND mappingunkid = @mappingunkid AND roleunkid = @roleunkid AND levelunkid = @levelunkid AND isvoid = 0   "

                For i As Integer = 0 To dtVisibility.Rows.Count - 1
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@loantrancheapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("loantrancheapprovalunkid").ToString)
                    objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("loantrancherequestunkid").ToString)
                    objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("mappingunkid").ToString)
                    objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("roleunkid").ToString)
                    objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtVisibility.Rows(i)("levelunkid").ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid)
                    objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid)
                    objDataOperation.AddParameter("@loantranche_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoantranche_Amount)
                    objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid)

                    If mintStatusunkid = enLoanApplicationStatus.APPROVED Then
                        objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, enLoanApplicationStatus.PENDING)
                    Else
                        objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                    End If

                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If   'If dtVisibility IsNot Nothing AndAlso dtVisibility.Rows.Count > 0 Then


            _Loantrancheapprovalunkid(objDataOperation) = xLoantrancheapprovalunkid


            If mdtDocument IsNot Nothing Then
                Dim objDocument As New clsScan_Attach_Documents
                Dim dtTran As DataTable = objDocument._Datatable
                Dim docLocalPath As String = mstrDocumentPath & "\"

                Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.LOAN_TRANCHE).Tables(0).Rows(0)("Name").ToString
                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                Dim dr As DataRow
                For Each drow As DataRow In mdtDocument.Rows
                    dr = dtTran.NewRow
                    dr("scanattachtranunkid") = drow("scanattachtranunkid")
                    dr("documentunkid") = drow("documentunkid")
                    dr("employeeunkid") = drow("employeeunkid")
                    dr("filename") = drow("filename")
                    dr("scanattachrefid") = drow("scanattachrefid")
                    dr("modulerefid") = drow("modulerefid")
                    dr("form_name") = drow("form_name")
                    dr("userunkid") = drow("userunkid")
                    dr("transactionunkid") = drow("transactionunkid")
                    dr("attached_date") = drow("attached_date")
                    dr("orgfilepath") = drow("localpath")
                    dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                    dr("AUD") = drow("AUD")
                    dr("userunkid") = mintUserunkid
                    dr("fileuniquename") = drow("fileuniquename")
                    dr("filepath") = drow("filepath")
                    dr("filesize") = drow("filesize_kb")
                    dr("file_data") = drow("file_data")
                    dtTran.Rows.Add(dr)
                Next
                objDocument._Datatable = dtTran
                objDocument.InsertUpdateDelete_Documents(objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If UpdateLoanTrancheApplicationStatus(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting _
                                                                    , xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmpNotificationTrancheDateBeforeDays, objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)


            'Pinkal (16-Nov-2023) -- Start
            '(A1X-1489) NMB - Mortgage tranche disbursement API.
            Dim objLoanTrancheApplication As New clsloanTranche_request_Tran
            objLoanTrancheApplication._Loantrancherequestunkid = mintLoantrancherequestunkid

            If objLoanTrancheApplication._Statusunkid = enLoanApplicationStatus.APPROVED Then

                Dim mstrEmpBankAcNo As String = ""
                objConfig = New clsConfigOptions

                Dim mstrOracleHostName As String = objConfig.GetKeyValue(xCompanyUnkid, "OracleHostName", Nothing)
                Dim mstrOraclePortNo As String = objConfig.GetKeyValue(xCompanyUnkid, "OraclePortNo", Nothing)
                Dim mstrOracleServiceName As String = objConfig.GetKeyValue(xCompanyUnkid, "OracleServiceName", Nothing)
                Dim mstrOracleUserName As String = objConfig.GetKeyValue(xCompanyUnkid, "OracleUserName", Nothing)
                Dim mstrOracleUserPassword As String = objConfig.GetKeyValue(xCompanyUnkid, "OracleUserPassword", Nothing)

                Dim mstrManualMortgageDisbursementRequestFlexcubeURL As String = objConfig.GetKeyValue(xCompanyUnkid, "ManualMortgageDisbursementRequestFlexcubeURL", Nothing)

                If objConfig.IsKeyExist(xCompanyUnkid, "Advance_CostCenterunkid", Nothing) = True Then
                    mstrOracleUserPassword = clsSecurity.Decrypt(mstrOracleUserPassword, "ezee")
                End If
                objConfig = Nothing

                If objLoanTrancheApplication._DeductionPeriodunkid > 0 Then
                    Dim dsEmpBank As DataSet = Nothing
                    Dim objEmpBankTran As New clsEmployeeBanks
                    dsEmpBank = objEmpBankTran.GetList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, True, False, "EmployeeBank", False, "", objLoanTrancheApplication._Employeeunkid.ToString(), xPeriodEnd, "BankGrp, BranchName, EmpName, end_date DESC")
                    If dsEmpBank IsNot Nothing AndAlso dsEmpBank.Tables(0).Rows.Count > 0 Then
                        mstrEmpBankAcNo = dsEmpBank.Tables(0).Rows(0)("accountno").ToString()
                    End If
                    If dsEmpBank IsNot Nothing Then dsEmpBank.Clear()
                    dsEmpBank = Nothing
                End If


                If mstrLoanSchemeCode.Trim().ToUpper = "CL28" Then   'Manual Mortgage Disbursement Request

                    If SendLoanTrancheFlexcubeRequest(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrManualMortgageDisbursementRequestFlexcubeURL _
                                             , xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee _
                                             , mstrEmpBankAcNo, mstrLoanSchemeCode, mstrCurrency, objLoanTrancheApplication) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                End If  '     If mstrLoanSchemeCode.Trim().ToUpper = "CL28" Then   'Manual Mortgage Disbursement Request

            End If   '   If objLoanTrancheApplication._Statusunkid = enLoanApplicationStatus.APPROVED Then

            objLoanTrancheApplication = Nothing
            'Pinkal (16-Nov-2023) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloantranche_approval_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try

            strQ = " UPDATE lnloantranche_approval_tran SET " & _
                        "  isvoid = @isvoid" & _
                        ", voiduserunkid = @voiduserunkid" & _
                        ", voiddatetime = GETDATE() " & _
                        ", voidreason = @voidreason " & _
                        " WHERE isvoid = 0 AND  loantrancheapprovalunkid = @loantrancheapprovalunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancheapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForLoanTrancheApproval(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mdtDocument IsNot Nothing AndAlso mdtDocument.Rows.Count > 0 Then
                Dim objDocument As New clsScan_Attach_Documents
                For Each iRow As DataRow In mdtDocument.Rows
                    iRow("AUD") = "D"
                Next
                objDocument._Datatable = mdtDocument
                objDocument.InsertUpdateDelete_Documents(objDataOperation)
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                objDocument = Nothing
            End If

            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@loantrancheapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  loantrancheapprovalunkid " & _
              ", loantrancherequestunkid " & _
              ", processpendingloanunkid " & _
              ", employeeunkid " & _
              ", mappingunkid " & _
              ", roleunkid " & _
              ", levelunkid " & _
              ", priority " & _
              ", mapuserunkid " & _
              ", deductionperiodunkid " & _
              ", approvaldate " & _
              ", loantranche_amount " & _
              ", countryunkid " & _
              ", statusunkid " & _
              ", visibleunkid " & _
              ", remark " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             "FROM lnloantranche_approval_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND loantrancheapprovalunkid <> @loantrancheapprovalunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@loantrancheapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloantranche_approval_tran) </purpose>
    Public Function InsertAuditTrailForLoanTrancheApproval(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancheapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancheapprovalunkid.ToString)
            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancherequestunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@mappingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingunkid.ToString)
            objDataOperation.AddParameter("@roleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRoleunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapuserunkid.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)

            If mdtApprovaldate <> Nothing Then
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApprovaldate.ToString)
            Else
                objDataOperation.AddParameter("@approvaldate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@loantranche_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoantranche_Amount.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)


            strQ = "INSERT INTO atlnloantranche_approval_tran ( " & _
                      "  loantrancheapprovalunkid " & _
                      ", loantrancherequestunkid " & _
                      ", processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", mappingunkid " & _
                      ", roleunkid " & _
                      ", levelunkid " & _
                      ", priority " & _
                      ", mapuserunkid " & _
                      ", deductionperiodunkid " & _
                      ", approvaldate " & _
                      ", loantranche_amount " & _
                      ", countryunkid " & _
                      ", statusunkid " & _
                      ", visibleunkid " & _
                      ", remark " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name" & _
                      ", form_name " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      "  @loantrancheapprovalunkid " & _
                      ", @loantrancherequestunkid " & _
                      ", @processpendingloanunkid " & _
                      ", @employeeunkid " & _
                      ", @mappingunkid " & _
                      ", @roleunkid " & _
                      ", @levelunkid " & _
                      ", @priority " & _
                      ", @mapuserunkid " & _
                      ", @deductionperiodunkid " & _
                      ", @approvaldate " & _
                      ", @loantranche_amount " & _
                      ", @countryunkid " & _
                      ", @statusunkid " & _
                      ", @visibleunkid " & _
                      ", @remark " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @machine_name" & _
                      ", @form_name " & _
                      ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForLoanTrancheApproval; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateLoanTrancheApplication(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try
            strQ = " UPDATE lnloantranche_approval_tran SET " & _
                      "    deductionperiodunkid = @deductionperiodunkid" & _
                      "    ,loantranche_amount = @loantranche_amount" & _
                      " WHERE loantrancherequestunkid = @loantrancherequestunkid " & _
                      " AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancherequestunkid.ToString)
            objDataOperation.AddParameter("@deductionperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductionperiodunkid.ToString)
            objDataOperation.AddParameter("@loantranche_amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecLoantranche_Amount.ToString)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " SELECT " & _
                      "  lna.loantrancheapprovalunkid " & _
                      ", lna.loantrancherequestunkid " & _
                      ", lna.statusunkid " & _
                      ", lna.visibleunkid " & _
                      ", lna.priority " & _
                      ", ln.tranchedate " & _
                      " FROM lnloantranche_approval_tran lna " & _
                      " JOIN lnloantranche_request_tran ln on ln.loantrancherequestunkid = lna.loantrancherequestunkid AND ln.isvoid = 0 " & _
                      " WHERE lna.isvoid=0 AND lna.loantrancherequestunkid = @loantrancherequestunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancherequestunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables("List").Rows.Count > 0 Then
                Dim xCount As Integer = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("visibleunkid") = -1).DefaultIfEmpty.Count
                Dim xPriority As Integer = dsList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Integer)("statusunkid") = enLoanApplicationStatus.PENDING AndAlso x.Field(Of Integer)("visibleunkid") = -1).Select(Function(x) x.Field(Of Integer)("priority")).DefaultIfEmpty.Min

                For Each dRow As DataRow In dsList.Tables("List").Rows

                    _Loantrancheapprovalunkid(objDataOperation) = dRow.Item("loantrancheapprovalunkid")

                    If dsList.Tables(0).Rows.Count = xCount AndAlso xPriority = mintPriority AndAlso mintStatusunkid = enLoanApplicationStatus.PENDING AndAlso mintVisibleunkid = -1 Then

                        mintVisibleunkid = enLoanApplicationStatus.PENDING

                        strQ = " UPDATE lnloantranche_approval_tran SET " & _
                                  "  visibleunkid = @visibleunkid " & _
                                  "  WHERE loantrancheapprovalunkid = @loantrancheapprovalunkid " & _
                                  "  AND loantrancherequestunkid = @loantrancherequestunkid " & _
                                  "  AND isvoid = 0 "

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@loantrancheapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoantrancheapprovalunkid.ToString)
                        objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
                        objDataOperation.AddParameter("@visibleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVisibleunkid)
                        objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                    End If

                    If InsertAuditTrailForLoanTrancheApproval(objDataOperation, enAuditType.EDIT) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If


            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLoanTrancheApplication; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function IsPendingLoanTrancheApplication(ByVal intLoanTrancheRequestId As Integer, Optional ByVal blnGetApplicationStatus As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation = Nothing
        Try
            objDataOperation = New clsDataOperation

            strQ = " SELECT " & _
                   "     loantrancherequestunkid " & _
                   "    ,statusunkid " & _
                   " FROM lnloantranche_approval_tran " & _
                   " WHERE isvoid = 0 " & _
                   " AND loantrancherequestunkid = @loantrancherequestunkid "

            If blnGetApplicationStatus = True Then
                strQ &= "    AND statusunkid = 1 AND visibleid < 2 "
            Else
                strQ &= "    AND statusunkid <> 1 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loantrancherequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanTrancheRequestId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsPendingLoanTrancheApplication; Module Name: " & mstrModuleName)
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateLoanTrancheApplicationStatus(ByVal xDatabaseName As String, _
                                                                    ByVal xUserUnkid As Integer, _
                                                                    ByVal xYearUnkid As Integer, _
                                                                    ByVal xCompanyUnkid As Integer, _
                                                                    ByVal xPeriodStart As DateTime, _
                                                                    ByVal xPeriodEnd As DateTime, _
                                                                    ByVal xUserModeSetting As String, _
                                                                    ByVal xOnlyApproved As Boolean, _
                                                                    ByVal xIncludeIn_ActiveEmployee As Boolean, _
                                                                    ByVal mintEmpNotificationTrancheDateBeforeDays As Integer, _
                                                                    ByVal objDoOperation As clsDataOperation) As Boolean
        Try
            Dim exForce As Exception


            If mintStatusunkid = enLoanApplicationStatus.APPROVED Then

                Dim dsApprover As DataSet = GetLoanTrancheApprovalList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                       , xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved _
                                                                       , xIncludeIn_ActiveEmployee, "List", " lta.loantrancherequestunkid = " & mintLoantrancherequestunkid & " AND lta.priority > " & mintPriority _
                                                                       , "", False, objDoOperation)

                If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count <= 0 Then
                    Dim objLoanTrancheApplication As New clsloanTranche_request_Tran
                    objLoanTrancheApplication._Loantrancherequestunkid(objDoOperation) = mintLoantrancherequestunkid
                    objLoanTrancheApplication._FinalDeductionPeriodunkid = mintDeductionperiodunkid
                    objLoanTrancheApplication._Finalapproverunkid = mintUserunkid
                    objLoanTrancheApplication._Approved_Trancheamt = mdecLoantranche_Amount
                    objLoanTrancheApplication._Statusunkid = enLoanApplicationStatus.APPROVED
                    objLoanTrancheApplication._ClientIP = mstrClientIP
                    objLoanTrancheApplication._FormName = mstrFormName
                    objLoanTrancheApplication._HostName = mstrHostName
                    objLoanTrancheApplication._IsFromWeb = True

                    If objLoanTrancheApplication.Update(xCompanyUnkid, False, objDoOperation) = False Then
                        exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                        Throw exForce
                    End If

                    dsApprover.Tables(0).Rows.Clear()
                    dsApprover = Nothing

                End If '  If dsApprover IsNot Nothing AndAlso dsApprover.Tables(0).Rows.Count <= 0 Then

            ElseIf mintStatusunkid = enLoanApplicationStatus.REJECTED Then
                Dim objLoanTrancheApplication As New clsloanTranche_request_Tran
                objLoanTrancheApplication._Loantrancherequestunkid(objDoOperation) = mintLoantrancherequestunkid
                objLoanTrancheApplication._FinalDeductionPeriodunkid = 0
                objLoanTrancheApplication._Finalapproverunkid = 0
                objLoanTrancheApplication._Approved_Trancheamt = 0
                objLoanTrancheApplication._Statusunkid = enLoanApplicationStatus.REJECTED
                objLoanTrancheApplication._ClientIP = mstrClientIP
                objLoanTrancheApplication._FormName = mstrFormName
                objLoanTrancheApplication._HostName = mstrHostName
                objLoanTrancheApplication._IsFromWeb = True

                If objLoanTrancheApplication.Update(xCompanyUnkid, False, objDoOperation) = False Then
                    exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                    Throw exForce
                End If
                objLoanTrancheApplication = Nothing
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLoanTrancheApplicationStatus; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function


    'Pinkal (16-Nov-2023) -- Start
    '(A1X-1489) NMB - Mortgage tranche disbursement API.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function SendLoanTrancheFlexcubeRequest(ByVal mstrOracleHostName As String, ByVal mstrOraclePortNo As String, ByVal mstrOracleServiceName As String, ByVal mstrOracleUserName As String, ByVal mstrOracleUserPassword As String _
                                                  , ByVal mstrLoanTrancheRequestFlexcubeURL As String, ByVal xDatabaseName As String, ByVal xUserUnkid As Integer, ByVal xYearUnkid As Integer _
                                                  , ByVal xCompanyUnkid As Integer, ByVal xPeriodStart As Date, ByVal xPeriodEnd As Date, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean, ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                  , ByVal mstrEmpBankAcNo As String, ByVal mstrLoanSchemeCode As String, ByVal mstrCurrency As String, ByVal objLoanTrancheApplication As clsloanTranche_request_Tran) As Boolean

        Dim objLoanTrancheFlexCube As New clsLoanTrancheFlexCubeIntegration
        Dim objMasterData As New clsMasterData
        Dim mblnFlag As Boolean = True
        Dim mstrLoanFlexcubeFailureNotificationUserIds As String = ""
        Dim dtTable As DataTable = Nothing


        Try
            Dim exForce As Exception = Nothing
            Dim mstrError As String = ""
            Dim mstrPostedData As String = ""
            Dim mstrResponseData As String = ""

            Dim mdtServerDate As DateTime = Nothing
            Dim objConfig As New clsConfigOptions
            mdtServerDate = objConfig._CurrentDateAndTime
            objConfig = Nothing


            Dim mblnLoanTrancheApplicationExistInCBS As Boolean = False
            If objLoanTrancheApplication._IsPostedError Then
                mblnLoanTrancheApplicationExistInCBS = LoanTrancheApplicantExistInCBS(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, objLoanTrancheApplication._Applicationno, mstrEmpBankAcNo)
            End If

            Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(mstrOracleUserName.Trim & ":" & mstrOracleUserPassword)
            Dim mstrBase64Credential As String = Convert.ToBase64String(byt)

            Dim mstrEmpLoanAccountNo As String = ""
            If mstrEmpBankAcNo.Trim.Length > 0 Then
                mstrEmpLoanAccountNo = GetFlexCubeEmpLoanAccountNo(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrLoanSchemeCode, mstrEmpBankAcNo)
            End If


            If mstrLoanSchemeCode.Trim.ToUpper = "CL28" Then

                If mstrLoanTrancheRequestFlexcubeURL.Trim.Length > 0 AndAlso mstrBase64Credential.Trim.Length > 0 Then

                    objLoanTrancheFlexCube.service = "aruti-loan-automation"
                    objLoanTrancheFlexCube.type = "mortgage-disbursement"

                    If objLoanTrancheApplication._IsPostedError Then
                        If mblnLoanTrancheApplicationExistInCBS Then
                            objLoanTrancheApplication._ReponseData = "{""statusCode"": 600,""message"": ""Success"",""body"": {""reason"": ""ST-SAVE-052:Successfully Saved and Authorized""}}"
                            objLoanTrancheApplication._IsPostedError = False
                            objLoanTrancheApplication._Userunkid = xUserUnkid
                            objLoanTrancheApplication._ClientIP = mstrClientIP
                            objLoanTrancheApplication._FormName = mstrFormName
                            objLoanTrancheApplication._HostName = mstrHostName

                            If objLoanTrancheApplication.Update(xCompanyUnkid, False) = False Then
                                mblnFlag = False
                                exForce = New Exception(objLoanTrancheApplication._Message)
                                Throw exForce
                            End If
                            Return mblnFlag
                        Else
                            objLoanTrancheFlexCube.payload.transactionReference = objLoanTrancheApplication._Applicationno + mdtServerDate.ToString("yyyyMMddhhmmssff")
                        End If  '   If mblnLoanApplicationExistInCBS Then
                    Else
                        objLoanTrancheFlexCube.payload.transactionReference = objLoanTrancheApplication._Applicationno
                    End If  '  If objLoanTrancheApplication._IsPostedError Then

                    Dim mintLoanTrancheSrNo As Integer = 0
                    mintLoanTrancheSrNo = GetLoanTrancheSrNo(mstrOracleHostName, mstrOraclePortNo, mstrOracleServiceName, mstrOracleUserName, mstrOracleUserPassword, mstrEmpLoanAccountNo)


                    objLoanTrancheFlexCube.payload.loanAccountNumber = mstrEmpLoanAccountNo
                    objLoanTrancheFlexCube.payload.valueDate = mdtApprovaldate.ToString("yyyy-MM-dd")

                    'objLoanTrancheFlexCube.payload.disbursementSerialNumber = "5"
                    objLoanTrancheFlexCube.payload.disbursementSerialNumber = mintLoanTrancheSrNo

                    objLoanTrancheFlexCube.payload.settlementAmount = mdecLoantranche_Amount
                    objLoanTrancheFlexCube.payload.currency = mstrCurrency
                    objLoanTrancheFlexCube.payload.settlementAccountNumber = mstrEmpBankAcNo
                    objLoanTrancheFlexCube.payload.productCode = mstrLoanSchemeCode
                    objLoanTrancheFlexCube.payload.disbursementChecklistDescription = "test"
                    objLoanTrancheFlexCube.payload.disbursementChecklistValue = "Y"
                    objLoanTrancheFlexCube.payload.remark = "Mortgage disbursement request"


                    If objMasterData.GetSetFlexCubeLoanRequest(mstrLoanTrancheRequestFlexcubeURL, mstrBase64Credential, objLoanTrancheFlexCube, mstrError, mstrPostedData, mstrResponseData) = False Then
                        mblnFlag = False
                        exForce = New Exception("Loan Tranche Request Flexcube Error : " & mstrError)
                        Throw exForce
                    End If

                End If 'If mstrLoanTrancheRequestFlexcubeURL.Trim.Length > 0 AndAlso mstrBase64Credential.Trim.Length > 0 Then

            End If  'If mstrLoanSchemeCode.Trim.ToUpper = "CL28" Then

            objMasterData = Nothing


            objLoanTrancheApplication._PostedData = mstrPostedData
            objLoanTrancheApplication._ReponseData = mstrResponseData

            dtTable = JsonStringToDataTable(mstrResponseData)
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                If CInt(dtTable.Rows(0)("statusCode")) <> 600 Then
                    objLoanTrancheApplication._IsPostedError = True
                Else
                    objLoanTrancheApplication._IsPostedError = False
                End If
            End If

            objLoanTrancheApplication._Userunkid = xUserUnkid
            objLoanTrancheApplication._ClientIP = mstrClientIP
            objLoanTrancheApplication._FormName = mstrFormName
            objLoanTrancheApplication._HostName = mstrHostName

            If objLoanTrancheApplication.Update(xCompanyUnkid, False) = False Then
                mblnFlag = False
                exForce = New Exception(objLoanTrancheApplication._Message)
                Throw exForce
            End If

        Catch ex As Exception
            mblnFlag = False
            Throw New Exception(ex.Message & "; Procedure Name: SendLoanTrancheFlexcubeRequest; Module Name: " & mstrModuleName)
        Finally
            'If mblnFlag = False Then
            '    SendLoanFlexcubeFailureNotificationToUsers(xCompanyUnkid, xPeriodStart, mstrLoanFlexcubeFailureNotificationUserIds, objLoanApplication._Application_No.Trim, objLoanApplication._Employeeunkid)
            'Else
            '    If objLoanApplication._IsPostedError Then
            '        SendLoanFlexcubeFailureNotificationToUsers(xCompanyUnkid, xPeriodStart, mstrLoanFlexcubeFailureNotificationUserIds, objLoanApplication._Application_No.Trim, objLoanApplication._Employeeunkid)
            '    End If
            'End If
            objMasterData = Nothing
            objLoanTrancheFlexCube = Nothing
        End Try
        Return mblnFlag
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetFlexCubeEmpLoanAccountNo(ByVal strOracleHostName As String, _
                                                                      ByVal strOraclePortNo As String, _
                                                                      ByVal strOracleServiceName As String, _
                                                                      ByVal strOracleUserName As String, _
                                                                      ByVal strOracleUserPassword As String, _
                                                                      ByVal mstrLoanSchemeCode As String, _
                                                                      ByVal mstrEmpBankAcNo As String) As String
        Dim mstrEmpLoanAccountNo As String = ""
        Dim dsList As New DataSet
        Try
            Dim strConn As String = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" & strOracleHostName & ")(PORT=" & strOraclePortNo & ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" & strOracleServiceName & "))); User Id=" & strOracleUserName & ";Password=" & strOracleUserPassword & "; "
            Using cnnOracle As OracleConnection = New OracleConnection
                cnnOracle.ConnectionString = strConn

                Try
                    cnnOracle.Open()
                Catch ex As Exception
                    Throw ex
                End Try

                Dim StrQ As String = " SELECT  " & _
                                               " ACCOUNT_NUMBER  " & _
                                               " FROM fcubs.NMB_STAFF_LOANS_ALL  " & _
                                               " WHERE PRODUCT_CODE  = '" & mstrLoanSchemeCode & "' AND CUST_AC_NO= '" & mstrEmpBankAcNo & "' AND STATUS <> 'L' "

                Using cmdOracle As New OracleCommand()

                    If cnnOracle.State = ConnectionState.Closed Or cnnOracle.State = ConnectionState.Broken Then
                        cnnOracle.Open()
                    End If
                    cmdOracle.Connection = cnnOracle
                    cmdOracle.CommandType = CommandType.Text
                    cmdOracle.CommandText = StrQ
                    cmdOracle.Parameters.Clear()
                    Dim oda As New OracleDataAdapter(cmdOracle)
                    oda.Fill(dsList)
                End Using
            End Using

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mstrEmpLoanAccountNo = dsList.Tables(0).Rows(0)("ACCOUNT_NUMBER").ToString()
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFlexCubeEmpLoanAccountNo; Module Name: " & mstrModuleName)
        End Try
        Return mstrEmpLoanAccountNo
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function LoanTrancheApplicantExistInCBS(ByVal strOracleHostName As String, _
                                                              ByVal strOraclePortNo As String, _
                                                              ByVal strOracleServiceName As String, _
                                                              ByVal strOracleUserName As String, _
                                                              ByVal strOracleUserPassword As String, _
                                                              ByVal mstrLoanTransactionReference As String, _
                                                              ByVal mstrEmpBankAcNo As String) As Boolean
        Dim mblnFlag As Boolean = False
        Dim dsList As New DataSet
        Try

            Dim strConn As String = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" & strOracleHostName & ")(PORT=" & strOraclePortNo & ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" & strOracleServiceName & "))); User Id=" & strOracleUserName & ";Password=" & strOracleUserPassword & "; "
            Using cnnOracle As OracleConnection = New OracleConnection
                cnnOracle.ConnectionString = strConn

                Try
                    cnnOracle.Open()
                Catch ex As Exception
                    Throw ex
                End Try

                Dim StrQ As String = " SELECT * " & _
                                                " FROM fcubs.NMB_STAFF_LOANS_ALL " & _
                                                " WHERE CUST_AC_NO IN ('" & mstrEmpBankAcNo & "') AND APPLICATION_NUM IN ('" & mstrLoanTransactionReference & "')"

                Using cmdOracle As New OracleCommand()

                    If cnnOracle.State = ConnectionState.Closed Or cnnOracle.State = ConnectionState.Broken Then
                        cnnOracle.Open()
                    End If
                    cmdOracle.Connection = cnnOracle
                    cmdOracle.CommandType = CommandType.Text
                    cmdOracle.CommandText = StrQ
                    cmdOracle.Parameters.Clear()
                    Dim oda As New OracleDataAdapter(cmdOracle)
                    oda.Fill(dsList)
                End Using
            End Using

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                Dim xLoanTransactionReference As String = dsList.Tables(0).Rows(0)("APPLICATION_NUM").ToString()
                If xLoanTransactionReference.Trim.Length > 0 Then mblnFlag = True Else mblnFlag = False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: LoanTrancheApplicantExistInCBS; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLoanTrancheSrNo(ByVal strOracleHostName As String, _
                                                                      ByVal strOraclePortNo As String, _
                                                                      ByVal strOracleServiceName As String, _
                                                                      ByVal strOracleUserName As String, _
                                                                      ByVal strOracleUserPassword As String, _
                                                                      ByVal mstrLoanAcNo As String) As Integer
        Dim mintLoanTrancheSrNo As Integer = 0
        Dim dsList As New DataSet
        Try
            Dim strConn As String = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=" & strOracleHostName & ")(PORT=" & strOraclePortNo & ")))(CONNECT_DATA=(SERVER=DEDICATED)(SERVICE_NAME=" & strOracleServiceName & "))); User Id=" & strOracleUserName & ";Password=" & strOracleUserPassword & "; "
            Using cnnOracle As OracleConnection = New OracleConnection
                cnnOracle.ConnectionString = strConn

                Try
                    cnnOracle.Open()
                Catch ex As Exception
                    Throw ex
                End Try

                Dim StrQ As String = "SELECT ESN  from fcubs.ARUTI_MDSBR_DETAILS WHERE ACCOUNT_NUMBER = '" & mstrLoanAcNo & "'"

                Using cmdOracle As New OracleCommand()

                    If cnnOracle.State = ConnectionState.Closed Or cnnOracle.State = ConnectionState.Broken Then
                        cnnOracle.Open()
                    End If
                    cmdOracle.Connection = cnnOracle
                    cmdOracle.CommandType = CommandType.Text
                    cmdOracle.CommandText = StrQ
                    cmdOracle.Parameters.Clear()
                    Dim oda As New OracleDataAdapter(cmdOracle)
                    oda.Fill(dsList)
                End Using
            End Using

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintLoanTrancheSrNo = CInt(dsList.Tables(0).Rows(0)("ESN"))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanTrancheSrNo; Module Name: " & mstrModuleName)
        End Try
        Return mintLoanTrancheSrNo
    End Function


    'Pinkal (16-Nov-2023) -- End

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Pending")
            Language.setMessage(mstrModuleName, 2, "Approved")
            Language.setMessage(mstrModuleName, 3, "Rejected")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class