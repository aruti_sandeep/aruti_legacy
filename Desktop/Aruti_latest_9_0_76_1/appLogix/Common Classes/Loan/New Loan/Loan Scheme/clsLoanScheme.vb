﻿'************************************************************************************************************************************
'Class Name : clsLnloan_scheme.vb
'Purpose    :
'Date       :07/07/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsLoan_Scheme
    Private Shared ReadOnly mstrModuleName As String = "clsLoan_Scheme"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLoanschemeunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty

    'Anjan (11 May 2011)-Start
    'Private mdblMinnetsalary As Double
    Private mdecMinnetsalary As Decimal
    'Anjan (11 May 2011)-End 


    Private mdblMinhoursworked As Double
    Private mintLastnoofmonths As Integer
    'Private mblnIsinclude_Parttime As Boolean = False
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty


    'Pinkal (14-Apr-2015) -- Start
    'Enhancement - WORKING ON REDESIGNING LOAN MODULE.
    Private mblnIsShowOnESS As Boolean = False
    'Pinkal (14-Apr-2015) -- End

    'Nilay (19-Oct-2016) -- Start
    'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
    Private mblnShowLoanBalOnPayslip As Boolean = False
    'Nilay (19-Oct-2016) -- End

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 52
    Private mdecMaxLoanAmountLimit As Decimal = 0
    'S.SANDEEP [20-SEP-2017] -- END

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
    Private mintLoanCalcTypeId As Integer = 0
    Private mintInterestCalctypeId As Integer = 0
    Private mdecInterestRate As Decimal = 0
    'S.SANDEEP [20-SEP-2017] -- END

    'Sohail (22 Sep 2017) -- Start
    'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
    Private mdecEmi_netpaypercentage As Decimal = 0
    'Sohail (22 Sep 2017) -- End


    'Varsha (25 Nov 2017) -- Start
    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
    Private mintMaxNoOfInstallment As Integer = 0
    'Varsha (25 Nov 2017) -- End

    'Sohail (02 Apr 2018) -- Start
    'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
    Private mintRefLoanSchemeUnkid As Integer = 0
    'Sohail (02 Apr 2018) -- End
    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
    Private mintTranheadUnkid As Integer = 0
    'Sohail (11 Apr 2018) -- End
    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
    Private mintCostcenterunkid As Integer = 0
    'Sohail (14 Mar 2019) -- End
    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Private mintMapped_TranheadUnkid As Integer = 0
    'Sohail (29 Apr 2019) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loanschemeunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Loanschemeunkid() As Integer
        Get
            Return mintLoanschemeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanschemeunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set minnetsalary
    '''' Modify By: Anjan
    '''' </summary>

    'Anjan (11 May 2011)-Start
    'Public Property _Minnetsalary() As Double
    Public Property _Minnetsalary() As Decimal
        'Anjan (11 May 2011)-End 

        Get
            Return mdecMinnetsalary
        End Get

        'Anjan (11 May 2011)-Start
        'Set(ByVal value As Double)
        Set(ByVal value As Decimal)
            'Anjan (11 May 2011)-End
            mdecMinnetsalary = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set minhoursworked
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Minhoursworked() As Double
        Get
            Return mdblMinhoursworked
        End Get
        Set(ByVal value As Double)
            mdblMinhoursworked = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lastnoofmonths
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Lastnoofmonths() As Integer
        Get
            Return mintLastnoofmonths
        End Get
        Set(ByVal value As Integer)
            mintLastnoofmonths = Value
        End Set
    End Property

    'Public Property _Isinclude_Parttime() As Boolean
    '    Get
    '        Return mblnIsinclude_Parttime
    '    End Get
    '    Set(ByVal value As Boolean)
    '        mblnIsinclude_Parttime = Value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property


    'Pinkal (14-Apr-2015) -- Start
    'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

    ''' <summary>
    ''' Purpose: Get or Set isshowoness
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsShowonESS() As Boolean
        Get
            Return mblnIsShowOnESS
        End Get
        Set(ByVal value As Boolean)
            mblnIsShowOnESS = value
        End Set
    End Property

    'Pinkal (14-Apr-2015) -- End

     'Nilay (19-Oct-2016) -- Start
    'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
    Public Property _IsShowLoanBalOnPayslip() As Boolean
        Get
            Return mblnShowLoanBalOnPayslip
        End Get
        Set(ByVal value As Boolean)
            mblnShowLoanBalOnPayslip = value
        End Set
    End Property
    'Nilay (19-Oct-2016) -- End

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 52
    Public Property _MaxLoanAmountLimit() As Decimal
        Get
            Return mdecMaxLoanAmountLimit
        End Get
        Set(ByVal value As Decimal)
            mdecMaxLoanAmountLimit = value
        End Set
    End Property
    'S.SANDEEP [20-SEP-2017] -- END

    'S.SANDEEP [20-SEP-2017] -- START
    'ISSUE/ENHANCEMENT : REF-ID # 50
    Public Property _LoanCalcTypeId() As Integer
        Get
            Return mintLoanCalcTypeId
        End Get
        Set(ByVal value As Integer)
            mintLoanCalcTypeId = value
        End Set
    End Property
    Public Property _InterestCalctypeId() As Integer
        Get
            Return mintInterestCalctypeId
        End Get
        Set(ByVal value As Integer)
            mintInterestCalctypeId = value
        End Set
    End Property
    Public Property _InterestRate() As Decimal
        Get
            Return mdecInterestRate
        End Get
        Set(ByVal value As Decimal)
            mdecInterestRate = value
        End Set
    End Property
    'S.SANDEEP [20-SEP-2017] -- END

    'Sohail (22 Sep 2017) -- Start
    'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
    Public Property _EMI_NetPayPercentage() As Decimal
        Get
            Return mdecEmi_netpaypercentage
        End Get
        Set(ByVal value As Decimal)
            mdecEmi_netpaypercentage = value
        End Set
    End Property
    'Sohail (22 Sep 2017) -- End


    'Varsha (25 Nov 2017) -- Start
    'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
    Public Property _MaxNoOfInstallment() As Integer
        Get
            Return mintMaxNoOfInstallment
        End Get
        Set(ByVal value As Integer)
            mintMaxNoOfInstallment = value
        End Set
    End Property

    'Varsha (25 Nov 2017) -- End

    'Sohail (02 Apr 2018) -- Start
    'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
    Public Property _RefLoanSchemeUnkid() As Integer
        Get
            Return mintRefLoanSchemeUnkid
        End Get
        Set(ByVal value As Integer)
            mintRefLoanSchemeUnkid = value
        End Set
    End Property
    'Sohail (02 Apr 2018) -- End

    'Sohail (11 Apr 2018) -- Start
    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
    Public Property _TranheadUnkid() As Integer
        Get
            Return mintTranheadUnkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadUnkid = value
        End Set
    End Property
    'Sohail (11 Apr 2018) -- End

    'Sohail (14 Mar 2019) -- Start
    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
    Public Property _Costcenterunkid() As Integer
        Get
            Return mintCostcenterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostcenterunkid = value
        End Set
    End Property
    'Sohail (14 Mar 2019) -- End

    'Sohail (29 Apr 2019) -- Start
    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
    Public Property _Mapped_TranheadUnkid() As Integer
        Get
            Return mintMapped_TranheadUnkid
        End Get
        Set(ByVal value As Integer)
            mintMapped_TranheadUnkid = value
        End Set
    End Property
    'Sohail (29 Apr 2019) -- End

    'Hemant (24 Aug 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-805 - Develop the logic for maximum loan amount

    Private mstrMaxInstallmentHeadFormula As String
    Public Property _MaxInstallmentHeadFormula() As String
        Get
            Return mstrMaxInstallmentHeadFormula
        End Get
        Set(ByVal value As String)
            mstrMaxInstallmentHeadFormula = value
        End Set
    End Property
    'Hemant (24 Aug 2022) -- End

    'Hemant (24 Aug 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-818 - As a user, I want to be able to categorize a loan scheme as either secured,unsecured or Advance as radio buttons. A loan scheme can only be one at a time
    Private mintLoanSchemeCategoryId As Integer
    Public Property _LoanSchemeCategoryId() As Integer
        Get
            Return mintLoanSchemeCategoryId
        End Get
        Set(ByVal value As Integer)
            mintLoanSchemeCategoryId = value
        End Set
    End Property
    'Hemant (24 Aug 2022) -- End

    'Hemant (24 Aug 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-819 - As a user, I want be able to define the installment repayment schedule e.g monthly, quarterly etc
    Private mintRepaymentDays As Integer
    Public Property _RepaymentDays() As Integer
        Get
            Return mintRepaymentDays
        End Get
        Set(ByVal value As Integer)
            mintRepaymentDays = value
        End Set
    End Property
    'Hemant (24 Aug 2022) -- End

    'Hemant (24 Aug 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-819 - As a user, I want to have a field to configure the insurance amount on a loan scheme. Same idea we have with the interest rate
    Private mdecInsuranceRate As Decimal
    Public Property _InsuranceRate() As Decimal
        Get
            Return mdecInsuranceRate
        End Get
        Set(ByVal value As Decimal)
            mdecInsuranceRate = value
        End Set
    End Property
    'Hemant (24 Aug 2022) -- End

    'Hemant (24 Aug 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-821 - As a user, I want to be able to define maximum loan amount on a loan scheme as either a flat rate or a formula based on a payroll transaction head
    Private mintMaxLoanAmountCalcTypeId As Integer
    Public Property _MaxLoanAmountCalcTypeId() As Integer
        Get
            Return mintMaxLoanAmountCalcTypeId
        End Get
        Set(ByVal value As Integer)
            mintMaxLoanAmountCalcTypeId = value
        End Set
    End Property


    Private mstrMaxLoanAmountHeadFormula As String
    Public Property _MaxLoanAmountHeadFormula() As String
        Get
            Return mstrMaxLoanAmountHeadFormula
        End Get
        Set(ByVal value As String)
            mstrMaxLoanAmountHeadFormula = value
        End Set
    End Property
    'Hemant (24 Aug 2022) -- End

    'Hemant (24 Aug 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-822 - As a user I want to be able to define whether attachment on a loan scheme is mandatory or not
    Private mblnIsAttachmentRequired As Boolean
    Public Property _IsAttachmentRequired() As Boolean
        Get
            Return mblnIsAttachmentRequired
        End Get
        Set(ByVal value As Boolean)
            mblnIsAttachmentRequired = value
        End Set
    End Property

    Private mstrDocumentTypeIDs As String
    Public Property _DocumentTypeIDs() As String
        Get
            Return mstrDocumentTypeIDs
        End Get
        Set(ByVal value As String)
            mstrDocumentTypeIDs = value
        End Set
    End Property
    'Hemant (24 Aug 2022) -- End

    'Pinkal (20-Sep-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mblnRequiredReportingToApproval As Boolean = False
    Public Property _RequiredReportingToApproval() As Boolean
        Get
            Return mblnRequiredReportingToApproval
        End Get
        Set(ByVal value As Boolean)
            mblnRequiredReportingToApproval = value
        End Set
    End Property
    'Pinkal (20-Sep-2022) -- End

 'Hemant (03 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-914 - NMB - As a user, I want to have a field to specify the minimum amount on loan scheme. Same idea as the maximum amount on loan scheme. System should not allow application of figure below this amount
    Private mdecMinLoanAmountLimit As Decimal = 0
    Public Property _MinLoanAmountLimit() As Decimal
        Get
            Return mdecMinLoanAmountLimit
        End Get
        Set(ByVal value As Decimal)
            mdecMinLoanAmountLimit = value
        End Set
    End Property
    'Hemant (03 Oct 2022) -- End

    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : - As a user, I want to have option on loan scheme master to skip approval
    Private mblnIsSkipApproval As Boolean = False
    Public Property _IsSkipApproval() As Boolean
        Get
            Return mblnIsSkipApproval
        End Get
        Set(ByVal value As Boolean)
            mblnIsSkipApproval = value
        End Set
    End Property
    'Hemant (12 Oct 2022) -- End

    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : - As a user, I want to have a checkbox option on the loan scheme master to define whether a loan scheme requires posting to flexcube or not. Not all loan schemes will be posting to flexcube after final approval  
    Private mblnIsPostingToFlexcube As Boolean = False
    Public Property _IsPostingToFlexcube() As Boolean
        Get
            Return mblnIsPostingToFlexcube
        End Get
        Set(ByVal value As Boolean)
            mblnIsPostingToFlexcube = value
        End Set
    End Property
    'Hemant (12 Oct 2022) -- End

    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : - As a user, I want to have an option (check box) on the loan scheme to indicate which loan schemes are eligible for top-up. If a loan scheme doesn’t have this option checked, user cannot do top-up on it   
    Private mblnIsEligibleForTopUp As Boolean
    Public Property _IsEligibleForTopUp() As Boolean
        Get
            Return mblnIsEligibleForTopUp
        End Get
        Set(ByVal value As Boolean)
            mblnIsEligibleForTopUp = value
        End Set
    End Property
    'Hemant (12 Oct 2022) -- End

    'Hemant (12 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : - On loan scheme master, if a loan scheme has posting to flexcube enabled, as a user, I want to have the option of specifying the number of installments that need to be paid before a top-up can be done. 
    Private mintMinOfInstallmentPaid As Integer
    Public Property _MinOfInstallmentPaid() As Integer
        Get
            Return mintMinOfInstallmentPaid
        End Get
        Set(ByVal value As Integer)
            mintMinOfInstallmentPaid = value
        End Set
    End Property

    'Hemant (12 Oct 2022) -- End

    'Hemant (27 Oct 2022) -- Start
    'ENHANCEMENT(NMB) : AC2-990 - As a user, I want to pass the principal balance contained in flexcube loan view to Aruti payroll to display on employee salary slip as the loan balance for loans set to post to flexcube
    Private mstrDatabaseName As String
    Public Property _DatabaseName() As String
        Get
            Return mstrDatabaseName
        End Get
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Private mintUserunkid As Integer
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property
    'Hemant (27 Oct 2022) -- End


    'Pinkal (10-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mblnLoanApproval_DailyReminder As Boolean
    Public Property _LoanApproval_DailyReminder() As Boolean
        Get
            Return mblnLoanApproval_DailyReminder
        End Get
        Set(ByVal value As Boolean)
            mblnLoanApproval_DailyReminder = value
        End Set
    End Property

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mintEscalationDays As Integer = 0
    Public Property _EscalationDays() As Integer
        Get
            Return mintEscalationDays
        End Get
        Set(ByVal value As Integer)
            mintEscalationDays = value
        End Set
    End Property

    Private mstrEscalatedToEmployeeIds As String = ""
    Public Property _EscalatedToEmployeeIds() As String
        Get
            Return mstrEscalatedToEmployeeIds
        End Get
        Set(ByVal value As String)
            mstrEscalatedToEmployeeIds = value
        End Set
    End Property
    'Pinkal (23-Nov-2022) -- End

    'Pinkal (10-Nov-2022) -- End

    'Pinkal (23-Nov-2022) -- Start
    'NMB Loan Module Enhancement.
    Private mintMinNoOfInstallment As Integer = 0
    Public Property _MinNoOfInstallment() As Integer
        Get
            Return mintMinNoOfInstallment
        End Get
        Set(ByVal value As Integer)
            mintMinNoOfInstallment = value
        End Set
    End Property
    'Pinkal (23-Nov-2022) -- End

    'Hemant (07 Jul 2023) -- Start
    'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
    Private mintNoOfDaysBefereTitleExpiry As Integer
    Public Property _NoOfDaysBefereTitleExpiry() As Integer
        Get
            Return mintNoOfDaysBefereTitleExpiry
        End Get
        Set(ByVal value As Integer)
            mintNoOfDaysBefereTitleExpiry = value
        End Set
    End Property
    'Hemant (07 July 2023) -- End

    'Hemant (07 Jul 2023) -- Start
    'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
    Private mstrMaxInstallmentHeadFormulaForMortgage As String = ""
    Public Property _MaxInstallmentHeadFormulaForMortgage() As String
        Get
            Return mstrMaxInstallmentHeadFormulaForMortgage
        End Get
        Set(ByVal value As String)
            mstrMaxInstallmentHeadFormulaForMortgage = value
        End Set
    End Property
    'Hemant (07 Jul 2023) -- End

    'Pinkal (21-Jul-2023) -- Start
    '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
    Private mstrTitleExpiryToEmployeeIds As String = ""
    Public Property _TitleExpiryToEmployeeIds() As String
        Get
            Return mstrTitleExpiryToEmployeeIds
        End Get
        Set(ByVal value As String)
            mstrTitleExpiryToEmployeeIds = value
        End Set
    End Property

    'Pinkal (21-Jul-2023) -- End


    'Pinkal (04-Aug-2023) -- Start
    '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
    Private mstrLoanTrancheDocumentTypeIDs As String = ""
    Public Property _LoanTrancheDocumentTypeIDs() As String
        Get
            Return mstrLoanTrancheDocumentTypeIDs
        End Get
        Set(ByVal value As String)
            mstrLoanTrancheDocumentTypeIDs = value
        End Set
    End Property
    'Pinkal (04-Aug-2023) -- End

    'Pinkal (11-Mar-2024) -- Start
    '(A1X-2505) NMB - Credit card integration.
    Private mdecCreditCardPercentageOnDSR As Decimal = 0
    Public Property _CreditCardPercentageOnDSR() As Decimal
        Get
            Return mdecCreditCardPercentageOnDSR
        End Get
        Set(ByVal value As Decimal)
            mdecCreditCardPercentageOnDSR = value
        End Set
    End Property

    Private mintCreditCardAmountOnDSRFieldId As Integer = 0
    Public Property _CreditCardAmountOnDSRFieldId() As Integer
        Get
            Return mintCreditCardAmountOnDSRFieldId
        End Get
        Set(ByVal value As Integer)
            mintCreditCardAmountOnDSRFieldId = value
        End Set
    End Property

    Private mintCreditCardAmountOnExposureFieldId As Integer = 0
    Public Property _CreditCardAmountOnExposureFieldId() As Integer
        Get
            Return mintCreditCardAmountOnExposureFieldId
        End Get
        Set(ByVal value As Integer)
            mintCreditCardAmountOnExposureFieldId = value
        End Set
    End Property

    'Pinkal (11-Mar-2024) -- End


    'Pinkal (17-May-2024) -- Start
    'NMB Enhancement For Mortgage Loan.
    Private mblnMarketValueMandatory As Boolean = True
    Public Property _MarketValueMandatory() As Boolean
        Get
            Return mblnMarketValueMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnMarketValueMandatory = value
        End Set
    End Property

    Private mblnFSVValueMandatory As Boolean = True
    Public Property _FSVValueMandatory() As Boolean
        Get
            Return mblnFSVValueMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnFSVValueMandatory = value
        End Set
    End Property

    Private mblnBOQMandatory As Boolean = True
    Public Property _BOQMandatory() As Boolean
        Get
            Return mblnBOQMandatory
        End Get
        Set(ByVal value As Boolean)
            mblnBOQMandatory = value
        End Set
    End Property

    Private mintSeniorApproverLevelId As Integer = 0
    Public Property _SeniorApproverLevelId() As Integer
        Get
            Return mintSeniorApproverLevelId
        End Get
        Set(ByVal value As Integer)
            mintSeniorApproverLevelId = value
        End Set
    End Property

    Private mstrApproverLevelAlertIds As String = ""
    Public Property _ApproverLevelAlertIds() As String
        Get
            Return mstrApproverLevelAlertIds
        End Get
        Set(ByVal value As String)
            mstrApproverLevelAlertIds = value
        End Set
    End Property

    'Pinkal (17-May-2024) -- End

    'Hemant (22 Nov 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
    Private mblnIsDisableEmployeeConfirm As Boolean = False
    Public Property _IsDisableEmployeeConfirm() As Boolean
        Get
            Return mblnIsDisableEmployeeConfirm
        End Get
        Set(ByVal value As Boolean)
            mblnIsDisableEmployeeConfirm = value
        End Set
    End Property
    'Hemant (22 Nov 2024) -- End

    'Hemant (06 Dec 2024) -- Start
    'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
    Private mblnIsFinalApprovalNotify As Boolean = False
    Public Property _IsFinalApprovalNotify() As Boolean
        Get
            Return mblnIsFinalApprovalNotify
        End Get
        Set(ByVal value As Boolean)
            mblnIsFinalApprovalNotify = value
        End Set
    End Property

    Private mstrFinalApprovalNotifyEmployeeIds As String = ""
    Public Property _FinalApprovalNotifyEmployeeIds() As String
        Get
            Return mstrFinalApprovalNotifyEmployeeIds
        End Get
        Set(ByVal value As String)
            mstrFinalApprovalNotifyEmployeeIds = value
        End Set
    End Property
    'Hemant (06 Dec 2024) -- End


#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (14-Apr-2015) -- Start
            'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

            'strQ = "SELECT " & _
            '   "  loanschemeunkid " & _
            '   ", code " & _
            '   ", name " & _
            '   ", description " & _
            '   ", minnetsalary " & _
            '   ", isactive " & _
            '  "FROM lnloan_scheme_master " & _
            '  "WHERE loanschemeunkid = @loanschemeunkid "

            strQ = "SELECT " & _
             "  loanschemeunkid " & _
             ", code " & _
             ", name " & _
             ", description " & _
             ", minnetsalary " & _
             ", isactive " & _
             ", isshowoness " & _
             ", isshowloanbalonpayslip " & _
             ", ISNULL(maxloanamount,0) AS maxloanamount " & _
             ", ISNULL(interest_rate,0) AS interest_rate " & _
             ", ISNULL(loancalctype_id,0) AS loancalctype_id " & _
             ", ISNULL(interest_calctype_id,0) AS interest_calctype_id " & _
             ", ISNULL(emi_netpaypercentage, 0) AS emi_netpaypercentage " & _
             ", ISNULL(maxnoofinstallment, 0) AS maxnoofinstallment " & _
             ", ISNULL(refloanschemeunkid, 0) AS refloanschemeunkid " & _
             ", ISNULL(tranheadunkid, 0) AS tranheadunkid " & _
             ", ISNULL(costcenterunkid, 0) AS costcenterunkid " & _
             ", ISNULL(mapped_tranheadunkid, 0) AS mapped_tranheadunkid " & _
             ", ISNULL(maxinstallmentheadformula, '') AS maxinstallmentheadformula " & _
             ", ISNULL(loanschemecategory_id,0) AS loanschemecategory_id " & _
             ", ISNULL(repaymentdays, 0) AS repaymentdays " & _
             ", ISNULL(insurance_rate,0) AS insurance_rate " & _
             ", ISNULL(maxloanamountcalctype_id,0) AS maxloanamountcalctype_id " & _
             ", ISNULL(maxloanamountheadformula, '') AS maxloanamountheadformula " & _
             ", ISNULL(isattachmentrequired,0) AS isattachmentrequired " & _
             ", ISNULL(documenttypeids,'') AS documenttypeids " & _
             ", ISNULL(reportingtoapproval,0) AS reportingtoapproval " & _
             ", ISNULL(minloanamount,0) AS minloanamount " & _
             ", ISNULL(isskipapproval,0) AS isskipapproval " & _
             ", ISNULL(ispostingtoflexcube,0) AS ispostingtoflexcube " & _
             ", ISNULL(iseligiblefortopup,0) AS iseligiblefortopup " & _
             ", ISNULL(minnoofinstallmentpaid, 0) AS minnoofinstallmentpaid " & _
             ", ISNULL(lnapproval_dailyreminder, 0) AS lnapproval_dailyreminder " & _
             ", ISNULL(escalation_days, 0) AS escalation_days " & _
             ", ISNULL(escalatedtoempids, '') AS escalatedtoempids " & _
             ", ISNULL(minnoofinstallment,0) AS minnoofinstallment " & _
             ", ISNULL(noofdaysbeferetitleexpiry, 0) AS noofdaysbeferetitleexpiry " & _
             ", ISNULL(maxinstallmentheadformulaformortgage, '') AS maxinstallmentheadformulaformortgage " & _
                     ", ISNULL(titleexpiryempids, '') AS titleexpiryempids " & _
                     ", ISNULL(loantranchedocumentids, '') AS loantranchedocumentids " & _
             ", ISNULL(ccpercentageondsr, 0) AS ccpercentageondsr " & _
             ", ISNULL(ccamtondsrfieldid, 0) AS ccamtondsrfieldid " & _
             ", ISNULL(ccamtonexposurefieldid, 0) AS ccamtonexposurefieldid " & _
                     ", ISNULL(ismandatory_mktvalue, 0) AS ismandatory_mktvalue " & _
                     ", ISNULL(ismandatory_fsv, 0) AS ismandatory_fsv " & _
                     ", ISNULL(ismandatory_boq, 0) AS ismandatory_boq " & _
                     ", ISNULL(seniorapplevelunkid, 0) AS seniorapplevelunkid " & _
                     ", ISNULL(approverlevelalertunkids, '') AS approverlevelalertunkids " & _
             ", ISNULL(isdisableemployeeconfirm,0) AS isdisableemployeeconfirm " & _
             ", ISNULL(isfinalapprovalnotify,0) AS isfinalapprovalnotify " & _
             ", ISNULL(finalapprovalnotifyemployeeids, '') AS finalapprovalnotifyemployeeids " & _
            "FROM lnloan_scheme_master " & _
            "WHERE loanschemeunkid = @loanschemeunkid "

            'Hemant (06 Dec 2024) -- [isfinalapprovalnotify,finalapprovalnotifyemployeeids]
            'Hemant (22 Nov 2024) -- [isdisableemployeeconfirm]
            'Pinkal (17-May-2024) -- NMB Enhancement For Mortgage Loan. [ISNULL(ismandatory_mktvalue, 0) AS ismandatory_mktvalue ,  ISNULL(ismandatory_fsv, 0) AS ismandatory_fsv, ISNULL(ismandatory_boq, 0) AS ismandatory_boq,ISNULL(ismortgage_attachementoptional, 0) AS ismortgage_attachementoptional, ISNULL(seniorapplevelunkid, 0) AS seniorapplevelunkid , ISNULL(approverlevelalertunkids, '') AS approverlevelalertunkids ]

            'Pinkal (11-Mar-2024) -- (A1X-2505) NMB - Credit card integration. [ccpercentageondsr,ccamtondsrfieldid,ccamtonexposurefieldid]

            'Pinkal (04-Aug-2023) -- (A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.[ ", ISNULL(loantranchedocumentids, '') AS loantranchedocumentids " & _]

            'Pinkal (21-Jul-2023) -- (A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.[", ISNULL(titleexpiryempids, '') AS titleexpiryempids " & _]

            'Hemant (07 Jul 2023) -- [noofdaysbeferetitleexpiry,maxinstallmentheadformulaformortgage]
            'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.[ISNULL(minnoofinstallment,0) AS minnoofinstallment ]
            'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[", ISNULL(lnapproval_dailyreminder, 0) AS lnapproval_dailyreminder, ISNULL(escalation_days, 0) AS escalation_days , ISNULL(escalatedtoempids, '') AS escalatedtoempids ]
            'Hemant (12 Oct 2022) -- [isskipapproval,ispostingtoflexcube,iseligiblefortopup,minnoofinstallmentpaid]
            'Hemant (03 Oct 2022) -- [minloanamount]
            'Pinkal (20-Sep-2022) -- NMB Loan Module Enhancement.[", ISNULL(reportingtoapproval,0) AS reportingtoapproval " & _]

            'Hemant (24 Aug 2022) -- [maxinstallmentheadformula,loanschemecategory_id,repaymentdays,insurance_rate,maxloanamountcalctype_id,maxloanamountheadformula,isattachmentrequired,documenttypeids]
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (14 Mar 2019) - [costcenterunkid]
            'Sohail (11 Apr 2018) - [tranheadunkid]
            'Sohail (02 Apr 2018) - [refloanschemeunkid]
            'Varsha (25 Nov 2017) -- [maxnoofinstallment]
            'Sohail (22 Sep 2017) - [emi_netpaypercentage]
            'Nilay (19-Oct-2016) -- [isshowloanbalonpayslip]
            'Pinkal (14-Apr-2015) -- End
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 52, maxloanamount} -- END
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 50, loancalctype_id,interest_rate,interest_calctype_id} -- END

            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLoanschemeunkid = CInt(dtRow.Item("loanschemeunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mstrDescription = dtRow.Item("description").ToString

                'Anjan (11 May 2011)-Start
                'mdecMinnetsalary = CDbl(dtRow.Item("minnetsalary"))
                mdecMinnetsalary = CDec(dtRow.Item("minnetsalary"))
                'Anjan (11 May 2011)-End 

                'mblnIsinclude_Parttime = CBool(dtRow.Item("isinclude_parttime"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                'Pinkal (14-Apr-2015) -- Start
                'Enhancement - WORKING ON REDESIGNING LOAN MODULE.
                mblnIsShowOnESS = CBool(dtRow.Item("isshowoness"))
                'Pinkal (14-Apr-2015) -- End

                'Nilay (19-Oct-2016) -- Start
                'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
                mblnShowLoanBalOnPayslip = CBool(dtRow.Item("isshowloanbalonpayslip"))
                'Nilay (19-Oct-2016) -- End

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 52
                mdecMaxLoanAmountLimit = Convert.ToDecimal(dtRow.Item("maxloanamount"))
                'S.SANDEEP [20-SEP-2017] -- END

                'S.SANDEEP [20-SEP-2017] -- START
                'ISSUE/ENHANCEMENT : REF-ID # 50
                mintLoanCalcTypeId = Convert.ToInt32(dtRow.Item("loancalctype_id"))
                mintInterestCalctypeId = Convert.ToInt32(dtRow.Item("interest_calctype_id"))
                mdecInterestRate = Convert.ToDecimal(dtRow.Item("interest_rate"))
                'S.SANDEEP [20-SEP-2017] -- END
                'Sohail (22 Sep 2017) -- Start
                'PACRA Enhancement - 69.1 - Ref. Id # 51 - Provide loan installment limitation during application, to check on previous net pay not to go beyond the set % of the Net pay against loan installment amount..
                mdecEmi_netpaypercentage = Convert.ToDecimal(dtRow.Item("emi_netpaypercentage"))
                'Sohail (22 Sep 2017) -- End

                'Varsha (25 Nov 2017) -- Start
                'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
                mintMaxNoOfInstallment = Convert.ToInt32(dtRow.Item("maxnoofinstallment"))
                'Varsha (25 Nov 2017) -- End

                'Sohail (02 Apr 2018) -- Start
                'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
                mintRefLoanSchemeUnkid = Convert.ToInt32(dtRow.Item("refloanschemeunkid"))
                'Sohail (02 Apr 2018) -- End
                'Sohail (11 Apr 2018) -- Start
                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                mintTranheadUnkid = Convert.ToInt32(dtRow.Item("tranheadunkid"))
                'Sohail (11 Apr 2018) -- End
                'Sohail (14 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
                mintCostcenterunkid = Convert.ToInt32(dtRow.Item("costcenterunkid"))
                'Sohail (14 Mar 2019) -- End
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                mintMapped_TranheadUnkid = Convert.ToInt32(dtRow.Item("mapped_tranheadunkid"))
                'Sohail (29 Apr 2019) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-805 - Develop the logic for maximum loan amount
                mstrMaxInstallmentHeadFormula = Convert.ToString(dtRow.Item("maxinstallmentheadformula"))
                'Hemant (24 Aug 2022) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-818 - As a user, I want to be able to categorize a loan scheme as either secured,unsecured or Advance as radio buttons. A loan scheme can only be one at a time
                mintLoanSchemeCategoryId = Convert.ToInt32(dtRow.Item("loanschemecategory_id"))
                'Hemant (24 Aug 2022) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-819 - As a user, I want be able to define the installment repayment schedule e.g monthly, quarterly etc
                mintRepaymentDays = Convert.ToInt32(dtRow.Item("repaymentdays"))
                'Hemant (24 Aug 2022) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-819 - As a user, I want to have a field to configure the insurance amount on a loan scheme. Same idea we have with the interest rate
                mdecInsuranceRate = Convert.ToDecimal(dtRow.Item("insurance_rate"))
                'Hemant (24 Aug 2022) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-821 - As a user, I want to be able to define maximum loan amount on a loan scheme as either a flat rate or a formula based on a payroll transaction head
                mintMaxLoanAmountCalcTypeId = Convert.ToInt32(dtRow.Item("maxloanamountcalctype_id"))
                'Hemant (24 Aug 2022) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-821 - As a user, I want to be able to define maximum loan amount on a loan scheme as either a flat rate or a formula based on a payroll transaction head
                mstrMaxLoanAmountHeadFormula = Convert.ToString(dtRow.Item("maxloanamountheadformula"))
                'Hemant (24 Aug 2022) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-822 - As a user I want to be able to define whether attachment on a loan scheme is mandatory or not
                mblnIsAttachmentRequired = CBool(dtRow.Item("isattachmentrequired"))
                mstrDocumentTypeIDs = CStr(dtRow.Item("documenttypeids"))
                'Hemant (24 Aug 2022) -- End

                'Pinkal (20-Sep-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnRequiredReportingToApproval = CBool(dtRow.Item("reportingtoapproval"))
                'Pinkal (20-Sep-2022) -- End
  'Hemant (03 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-914 - NMB - As a user, I want to have a field to specify the minimum amount on loan scheme. Same idea as the maximum amount on loan scheme. System should not allow application of figure below this amount
                mdecMinLoanAmountLimit = Convert.ToDecimal(dtRow.Item("minloanamount"))
                'Hemant (03 Oct 2022) -- End
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : - As a user, I want to have option on loan scheme master to skip approval
                mblnIsSkipApproval = CBool(dtRow.Item("isskipapproval"))
                'Hemant (12 Oct 2022) -- End
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : - As a user, I want to have a checkbox option on the loan scheme master to define whether a loan scheme requires posting to flexcube or not. Not all loan schemes will be posting to flexcube after final approval  
                mblnIsPostingToFlexcube = CBool(dtRow.Item("ispostingtoflexcube"))
                'Hemant (12 Oct 2022) -- End
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : - As a user, I want to have an option (check box) on the loan scheme to indicate which loan schemes are eligible for top-up. If a loan scheme doesn’t have this option checked, user cannot do top-up on it   
                mblnIsEligibleForTopUp = CBool(dtRow.Item("iseligiblefortopup"))
                'Hemant (12 Oct 2022) -- End
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : - On loan scheme master, if a loan scheme has posting to flexcube enabled, as a user, I want to have the option of specifying the number of installments that need to be paid before a top-up can be done. 
                mintMinOfInstallmentPaid = Convert.ToInt32(dtRow.Item("minnoofinstallmentpaid"))
                'Hemant (12 Oct 2022) -- End

                'Pinkal (10-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                mblnLoanApproval_DailyReminder = CBool(dtRow.Item("lnapproval_dailyreminder"))
                mintEscalationDays = Convert.ToInt32(dtRow.Item("escalation_days"))
                mstrEscalatedToEmployeeIds = dtRow.Item("escalatedtoempids").ToString()
                'Pinkal (10-Nov-2022) -- End

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                mintMinNoOfInstallment = dtRow.Item("minnoofinstallment").ToString()
                'Pinkal (23-Nov-2022) -- End
                'Hemant (07 Jul 2023) -- Start
                'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
                mintNoOfDaysBefereTitleExpiry = CInt(dtRow.Item("noofdaysbeferetitleexpiry"))
                'Hemant (07 July 2023) -- End
                'Hemant (07 Jul 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
                mstrMaxInstallmentHeadFormulaForMortgage = Convert.ToString(dtRow.Item("maxinstallmentheadformulaformortgage"))
                'Hemant (07 Jul 2023) -- End

                'Pinkal (21-Jul-2023) -- Start
                '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
                mstrTitleExpiryToEmployeeIds = dtRow.Item("titleexpiryempids").ToString()
                'Pinkal (21-Jul-2023) -- End

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
                mstrLoanTrancheDocumentTypeIDs = dtRow.Item("loantranchedocumentids").ToString()
                'Pinkal (04-Aug-2023) -- End

                'Pinkal (11-Mar-2024) -- Start
                '(A1X-2505) NMB - Credit card integration.
                mdecCreditCardPercentageOnDSR = CDec(dtRow.Item("ccpercentageondsr"))
                mintCreditCardAmountOnDSRFieldId = CDec(dtRow.Item("ccamtondsrfieldid"))
                mintCreditCardAmountOnExposureFieldId = CDec(dtRow.Item("ccamtonexposurefieldid"))
                'Pinkal (11-Mar-2024) -- End

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                mblnMarketValueMandatory = CBool(dtRow.Item("ismandatory_mktvalue"))
                mblnFSVValueMandatory = CBool(dtRow.Item("ismandatory_fsv"))
                mblnBOQMandatory = CBool(dtRow.Item("ismandatory_boq"))
                mintSeniorApproverLevelId = CInt(dtRow.Item("seniorapplevelunkid"))
                mstrApproverLevelAlertIds = dtRow.Item("approverlevelalertunkids").ToString()
                'Pinkal (17-May-2024) -- End

                'Hemant (22 Nov 2024) -- Start
                'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                mblnIsDisableEmployeeConfirm = CBool(dtRow.Item("isdisableemployeeconfirm"))
                'Hemant (22 Nov 2024) -- End

                'Hemant (06 Dec 2024) -- Start
                'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
                mblnIsFinalApprovalNotify = CBool(dtRow.Item("isfinalapprovalnotify"))
                mstrFinalApprovalNotifyEmployeeIds = dtRow.Item("finalapprovalnotifyemployeeids").ToString()
                'Hemant (06 Dec 2024) -- End



                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnAddLoanInterestScheme As Boolean = False, Optional ByVal mstrFiler As String = "") As DataSet
        'Pinkal (11-Mar-2024) -- (A1X-2505) NMB - Credit card integration.[Optional ByVal mstrFiler As String = ""]
        'Sohail (02 Apr 2018) - [blnAddLoanInterestScheme]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objLoan As New clsLoan_Advance 'Sohail (29 Apr 2019)

        objDataOperation = New clsDataOperation

        Try

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            Dim dsLoanCalc As DataSet = objLoan.GetLoanCalculationTypeList("List", True, True, objDataOperation)
            Dim dicLoanCalc As Dictionary(Of Integer, String) = (From p In dsLoanCalc.Tables(0) Select New With {Key .Id = CInt(p.Item("Id")), Key .Name = p.Item("Name").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)

            Dim dsInterestCalc As DataSet = objLoan.GetLoan_Interest_Calculation_Type("List", True, True, True, True, objDataOperation)
            Dim dicInterestCalc As Dictionary(Of Integer, String) = (From p In dsInterestCalc.Tables("List") Select New With {.Id = CInt(p.Item("Id")), .Name = p.Item("NAME").ToString}).ToDictionary(Function(x) x.Id, Function(x) x.Name)
            'Sohail (29 Apr 2019) -- End


            'Pinkal (14-Apr-2015) -- Start
            'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

            'strQ = "SELECT " & _
            '  "  loanschemeunkid " & _
            '  ", code " & _
            '  ", name " & _
            '  ", description " & _
            '  ", minnetsalary " & _
            '  ", isactive " & _
            '  "FROM lnloan_scheme_master "
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ' strQ = "SELECT " & _
            '"  loanschemeunkid " & _
            '", code " & _
            '", name " & _
            '", description " & _
            '", minnetsalary " & _
            '", isactive " & _
            '", isshowoness " & _
            '            ", isshowloanbalonpayslip " & _
            '         ", ISNULL(maxloanamount,0) AS maxloanamount " & _
            '         ", ISNULL(loancalctype_id,0) AS loancalctype_id " & _
            '         ", ISNULL(interest_rate,0) AS interest_rate " & _
            '         ", ISNULL(interest_calctype_id,0) AS interest_calctype_id " & _
            '         ", CASE WHEN loancalctype_id = " & enLoanCalcId.Simple_Interest & " THEN @Simple_Interest " & _
            '         "       WHEN loancalctype_id = " & enLoanCalcId.Reducing_Amount & " THEN @Reducing_Amount " & _
            '         "       WHEN loancalctype_id = " & enLoanCalcId.No_Interest & " THEN @No_Interest " & _
            '         "       WHEN loancalctype_id = " & enLoanCalcId.Reducing_Balance_With_Fixed_Principal_EMI & " THEN @Reducing_Balance_With_Fixed_Pricipal_EMI " & _
            '         "   END AS loancalctype " & _
            '         ", CASE WHEN interest_calctype_id = " & enLoanInterestCalcType.DAILY & " THEN @DAILY " & _
            '         "       WHEN interest_calctype_id = " & enLoanInterestCalcType.MONTHLY & " THEN @MONTHLY " & _
            '         "       WHEN interest_calctype_id = " & enLoanInterestCalcType.BY_TENURE & " THEN @BY_TENURE " & _
            '         "       WHEN interest_calctype_id = " & enLoanInterestCalcType.SIMPLE_INTEREST & " THEN @SINTEREST " & _
            '         "   END AS intercalctype " & _
            '         ", ISNULL(emi_netpaypercentage, 0) AS emi_netpaypercentage " & _
            '         ", ISNULL(maxnoofinstallment, 0) AS maxnoofinstallment " & _
            '         ", ISNULL(refloanschemeunkid, 0) AS refloanschemeunkid " & _
            '         ", ISNULL(tranheadunkid, 0) AS tranheadunkid " & _
            '         ", ISNULL(costcenterunkid, 0) AS tranheadunkid " & _
            '"FROM lnloan_scheme_master " & _
            '"WHERE 1 = 1 "
            strQ = "SELECT " & _
           "  loanschemeunkid " & _
           ", code " & _
           ", name " & _
           ", description " & _
           ", minnetsalary " & _
           ", isactive " & _
           ", isshowoness " & _
                       ", isshowloanbalonpayslip " & _
                    ", ISNULL(maxloanamount,0) AS maxloanamount " & _
                    ", ISNULL(loancalctype_id,0) AS loancalctype_id " & _
                    ", ISNULL(interest_rate,0) AS interest_rate " & _
                    ", ISNULL(interest_calctype_id,0) AS interest_calctype_id " & _
                    ", ISNULL(emi_netpaypercentage, 0) AS emi_netpaypercentage " & _
                    ", ISNULL(maxnoofinstallment, 0) AS maxnoofinstallment " & _
                    ", ISNULL(refloanschemeunkid, 0) AS refloanschemeunkid " & _
                    ", ISNULL(tranheadunkid, 0) AS tranheadunkid " & _
                    ", ISNULL(costcenterunkid, 0) AS tranheadunkid " & _
                        ", ISNULL(mapped_tranheadunkid, 0) AS mapped_tranheadunkid "

            strQ &= "       ,CASE lnloan_scheme_master.loancalctype_id "
            For Each pair In dicLoanCalc
                strQ &= "       WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            Next
            strQ &= "         END AS loancalctype "

            strQ &= "       ,CASE lnloan_scheme_master.interest_calctype_id "
            For Each pair In dicInterestCalc
                strQ &= "       WHEN ISNULL(" & pair.Key & ",'') THEN ISNULL('" & pair.Value & "','') "
            Next
            strQ &= "         END AS intercalctype "

            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-805 - Develop the logic for maximum loan amount
            strQ &= ", ISNULL(maxinstallmentheadformula, '') AS maxinstallmentheadformula "
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-818 - As a user, I want to be able to categorize a loan scheme as either secured,unsecured or Advance as radio buttons. A loan scheme can only be one at a time
            strQ &= ", ISNULL(loanschemecategory_id,0) AS loanschemecategory_id "
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-819 - As a user, I want be able to define the installment repayment schedule e.g monthly, quarterly etc
            strQ &= ", ISNULL(repaymentdays, 0) AS repaymentdays "
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-819 - As a user, I want to have a field to configure the insurance amount on a loan scheme. Same idea we have with the interest rate
            strQ &= ", ISNULL(insurance_rate,0) AS insurance_rate "
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-821 - As a user, I want to be able to define maximum loan amount on a loan scheme as either a flat rate or a formula based on a payroll transaction head
            strQ &= ", ISNULL(maxloanamountcalctype_id,0) AS maxloanamountcalctype_id " & _
                    ", ISNULL(maxloanamountheadformula, '') AS maxloanamountheadformula "
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-822 - As a user I want to be able to define whether attachment on a loan scheme is mandatory or not
            strQ &= ", ISNULL(isattachmentrequired,0) AS isattachmentrequired " & _
                    ", ISNULL(documenttypeids,'') AS documenttypeids "
            'Hemant (24 Aug 2022) -- End


            strQ &= ", ISNULL(reportingtoapproval,0) AS reportingtoapproval " & _
                    ", ISNULL(minloanamount,0) AS minloanamount " & _
                    ", ISNULL(isskipapproval,0) AS isskipapproval " & _
                    ", ISNULL(ispostingtoflexcube,0) AS ispostingtoflexcube " & _
                    ", ISNULL(iseligiblefortopup,0) AS iseligiblefortopup " & _
                    ", ISNULL(minnoofinstallmentpaid, 0) AS minnoofinstallmentpaid " & _
                    ", ISNULL(lnapproval_dailyreminder, 0) AS lnapproval_dailyreminder " & _
                    ", ISNULL(escalation_days, 0) AS escalation_days " & _
                    ", ISNULL(escalatedtoempids, '') AS escalatedtoempids " & _
                    ", ISNULL(minnoofinstallment, 0) AS minnoofinstallment " & _
                    ", ISNULL(noofdaysbeferetitleexpiry, 0) AS noofdaysbeferetitleexpiry " & _
                    ", ISNULL(maxinstallmentheadformulaformortgage, '') AS maxinstallmentheadformulaformortgage " & _
                        ", ISNULL(titleexpiryempids, '') AS titleexpiryempids " & _
                    ", ISNULL(loantranchedocumentids, '') AS loantranchedocumentids " & _
                        ", ISNULL(ccpercentageondsr, 0) AS ccpercentageondsr " & _
                        ", ISNULL(ccamtondsrfieldid, 0) AS ccamtondsrfieldid " & _
                        ", ISNULL(ccamtonexposurefieldid, 0) AS ccamtonexposurefieldid " & _
                        ", ISNULL(ismandatory_mktvalue, 0) AS ismandatory_mktvalue " & _
                        ", ISNULL(ismandatory_fsv, 0) AS ismandatory_fsv " & _
                        ", ISNULL(seniorapplevelunkid, 0) AS seniorapplevelunkid " & _
                        ", ISNULL(approverlevelalertunkids, '') AS approverlevelalertunkids " & _
                    ", ISNULL(isdisableemployeeconfirm,0) AS isdisableemployeeconfirm " & _
                    ", ISNULL(isfinalapprovalnotify,0) AS isfinalapprovalnotify " & _
                    ", ISNULL(finalapprovalnotifyemployeeids, '') AS finalapprovalnotifyemployeeids " & _
                        " FROM lnloan_scheme_master " & _
           "WHERE 1 = 1 "


            'Hemant (06 Dec 2024) -- [isfinalapprovalnotify,finalapprovalnotifyemployeeids]
            'Hemant (22 Nov 2024) -- [isdisableemployeeconfirm]
            'Pinkal (17-May-2024) -- NMB Enhancement For Mortgage Loan.[ ISNULL(ismandatory_mktvalue, 0) AS ismandatory_mktvalue , ISNULL(ismandatory_fsv, 0) AS ismandatory_fsv, ISNULL(ismandatory_boq, 0) AS ismandatory_boq,, ISNULL(ismortgage_attachementoptional, 0) AS ismortgage_attachementoptional , ISNULL(seniorapplevelunkid, 0) AS seniorapplevelunkid , ISNULL(approverlevelalertunkids, '') AS approverlevelalertunkids " & _ ]

            'Pinkal (11-Mar-2024) -- (A1X-2505) NMB - Credit card integration.[ISNULL(ccpercentageondsr, 0) AS ccpercentageondsr, ISNULL(ccamtondsrfieldid, 0) AS ccamtondsrfieldid,ISNULL(ccamtonexposurefieldid, 0) AS ccamtonexposurefieldid]

            'Pinkal (04-Aug-2023) -- (A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.[", ISNULL(loantranchedocumentids, '') AS loantranchedocumentids " & _]

            'Pinkal (21-Jul-2023) -- (A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.[", ISNULL(titleexpiryempids, '') AS titleexpiryempids " & _]
            'Hemant (07 Jul 2023) -- [noofdaysbeferetitleexpiry,maxinstallmentheadformulaformortgage]
            'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.[ ", ISNULL(minnoofinstallment, 0) AS minnoofinstallment " & _ ]
            'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[", ISNULL(lnapproval_dailyreminder, 0) AS lnapproval_dailyreminder, ISNULL(escalation_days, 0) AS escalation_days , ISNULL(escalatedtoempids, '') AS escalatedtoempids " ]

            'Hemant (12 Oct 2022) -- [isskipapproval,ispostingtoflexcube,iseligiblefortopup,minnoofinstallmentpaid]
            'Hemant (03 Oct 2022) -- [minloanamount]
            'Pinkal (20-Sep-2022) -- NMB Loan Module Enhancement.[ISNULL(reportingtoapproval,0) AS reportingtoapproval]

            'Sohail (29 Apr 2019) -- End
            'Sohail (14 Mar 2019) - [costcenterunkid]
            'Sohail (11 Apr 2018) - [tranheadunkid]
            'Sohail (02 Apr 2018) - [refloanschemeunkid, WHERE 1 = 1 ]
            'Varsha (25 Nov 2017) -- [maxnoofinstallment]
            'Sohail (22 Sep 2017) - [emi_netpaypercentage]
            'Nilay (19-Oct-2016) -- [isshowloanbalonpayslip]
            'Pinkal (14-Apr-2015) -- End
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 52,maxloanamount} -- END

            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            ''S.SANDEEP [20-SEP-2017] -- START
            ''ISSUE/ENHANCEMENT : {REF-ID # 50, loancalctype_id,interest_rate,interest_calctype_id}
            'objDataOperation.AddParameter("@Simple_Interest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 14, "Simple Interest"))
            'objDataOperation.AddParameter("@Reducing_Amount", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 15, "Reducing Balance"))
            'objDataOperation.AddParameter("@No_Interest", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 17, "No Interest"))
            'objDataOperation.AddParameter("@Reducing_Balance_With_Fixed_Pricipal_EMI", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 18, "Reducing Balance With Fixed Pricipal EMI"))

            'objDataOperation.AddParameter("@DAILY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 20, "Daily"))
            'objDataOperation.AddParameter("@MONTHLY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 21, "Monthly"))
            'objDataOperation.AddParameter("@BY_TENURE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 24, "By Tenure"))
            'objDataOperation.AddParameter("@SINTEREST", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsLoan_Advance", 24, "Simple Interest"))
            ''S.SANDEEP [20-SEP-2017] -- END
            'Sohail (29 Apr 2019) -- End

            If blnOnlyActive Then
                'Sohail (02 Apr 2018) -- Start
                'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
                'strQ &= " WHERE isactive = 1 "
                strQ &= " AND isactive = 1 "
                'Sohail (02 Apr 2018) -- End
            End If

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            If blnAddLoanInterestScheme = False Then
                strQ &= " AND ISNULL(lnloan_scheme_master.refloanschemeunkid, 0) <= 0 "
            End If
            'Sohail (02 Apr 2018) -- End

            'Pinkal (11-Mar-2024) -- Start
            '(A1X-2505) NMB - Credit card integration.
            If mstrFiler.Trim.Length > 0 Then
                strQ &= " AND " & mstrFiler.Trim
            End If
            'Pinkal (11-Mar-2024) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloan_scheme_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Code is already defined. Please define new Code.")
            Return False
        End If

        If isExist(, mstrName, ) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Scheme is already defined. Please define new Loan Scheme.")
            Return False
        End If


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation


        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)

            'Anjan (11 May 2011)-Start
            'objDataOperation.AddParameter("@minnetsalary", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecMinnetsalary.ToString)
            objDataOperation.AddParameter("@minnetsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinnetsalary)
            'Anjan (11 May 2011)-End 


            'objDataOperation.AddParameter("@isinclude_parttime", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsinclude_Parttime.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            



            'Pinkal (14-Apr-2015) -- Start
            'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

            objDataOperation.AddParameter("@isshowoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShowOnESS.ToString)

            'Nilay (19-Oct-2016) -- Start
            'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
            objDataOperation.AddParameter("@isshowloanbalonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShowLoanBalOnPayslip.ToString)
            'Nilay (19-Oct-2016) -- End

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 52
            objDataOperation.AddParameter("@maxloanamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxLoanAmountLimit)
            'S.SANDEEP [20-SEP-2017] -- END

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            objDataOperation.AddParameter("@loancalctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanCalcTypeId)
            objDataOperation.AddParameter("@interest_calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterestCalctypeId)
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestRate)
            'S.SANDEEP [20-SEP-2017] -- END
            objDataOperation.AddParameter("@emi_netpaypercentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_netpaypercentage) 'Sohail (22 Sep 2017)

            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            objDataOperation.AddParameter("@maxnoofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNoOfInstallment)
            'Varsha (25 Nov 2017) -- End

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            objDataOperation.AddParameter("@refloanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRefLoanSchemeUnkid)
            'Sohail (02 Apr 2018) -- End
            'Sohail (11 Apr 2018) -- Start
            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadUnkid)
            'Sohail (11 Apr 2018) -- End
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid)
            'Sohail (14 Mar 2019) -- End
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            objDataOperation.AddParameter("@mapped_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapped_TranheadUnkid)
            'Sohail (29 Apr 2019) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-805 - Develop the logic for maximum loan amount
            objDataOperation.AddParameter("@maxinstallmentheadformula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMaxInstallmentHeadFormula)
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-818 - As a user, I want to be able to categorize a loan scheme as either secured,unsecured or Advance as radio buttons. A loan scheme can only be one at a time
            objDataOperation.AddParameter("@loanschemecategory_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeCategoryId)
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-819 - As a user, I want be able to define the installment repayment schedule e.g monthly, quarterly etc
            objDataOperation.AddParameter("@repaymentdays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRepaymentDays)
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-819 - As a user, I want to have a field to configure the insurance amount on a loan scheme. Same idea we have with the interest rate
            objDataOperation.AddParameter("@insurance_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInsuranceRate)
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-821 - As a user, I want to be able to define maximum loan amount on a loan scheme as either a flat rate or a formula based on a payroll transaction head
            objDataOperation.AddParameter("@maxloanamountcalctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxLoanAmountCalcTypeId)
            objDataOperation.AddParameter("@maxloanamountheadformula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMaxLoanAmountHeadFormula)
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-822 - As a user I want to be able to define whether attachment on a loan scheme is mandatory or not
            objDataOperation.AddParameter("@isattachmentrequired", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAttachmentRequired.ToString)
            objDataOperation.AddParameter("@documenttypeids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDocumentTypeIDs.ToString)
            'Hemant (24 Aug 2022) -- End

            'Pinkal (20-Sep-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@reportingtoapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnRequiredReportingToApproval)
            'Pinkal (20-Sep-2022) -- End

            'Hemant (03 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-914 - NMB - As a user, I want to have a field to specify the minimum amount on loan scheme. Same idea as the maximum amount on loan scheme. System should not allow application of figure below this amount
            objDataOperation.AddParameter("@minloanamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinLoanAmountLimit)
            'Hemant (03 Oct 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : - As a user, I want to have option on loan scheme master to skip approval
            objDataOperation.AddParameter("@isskipapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSkipApproval)
            'Hemant (12 Oct 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : - As a user, I want to have a checkbox option on the loan scheme master to define whether a loan scheme requires posting to flexcube or not. Not all loan schemes will be posting to flexcube after final approval  
            objDataOperation.AddParameter("@ispostingtoflexcube", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPostingToFlexcube)
            'Hemant (12 Oct 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : - As a user, I want to have an option (check box) on the loan scheme to indicate which loan schemes are eligible for top-up. If a loan scheme doesn’t have this option checked, user cannot do top-up on it   
            objDataOperation.AddParameter("@iseligiblefortopup", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEligibleForTopUp)
            'Hemant (12 Oct 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : - On loan scheme master, if a loan scheme has posting to flexcube enabled, as a user, I want to have the option of specifying the number of installments that need to be paid before a top-up can be done. 
            objDataOperation.AddParameter("@minnoofinstallmentpaid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinOfInstallmentPaid)
            'Hemant (12 Oct 2022) -- End

            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@lnapproval_dailyreminder", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnLoanApproval_DailyReminder)
            objDataOperation.AddParameter("@escalation_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEscalationDays)
            objDataOperation.AddParameter("@escalatedtoempids", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrEscalatedToEmployeeIds)
            'Pinkal (10-Nov-2022) -- End

            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@minnoofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinNoOfInstallment)
            'Pinkal (23-Nov-2022) -- End
            'Hemant (07 Jul 2023) -- Start
            'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
            objDataOperation.AddParameter("@noofdaysbeferetitleexpiry ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoOfDaysBefereTitleExpiry)
            'Hemant (07 July 2023) -- End
            'Hemant (07 Jul 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
            objDataOperation.AddParameter("@maxinstallmentheadformulaformortgage", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMaxInstallmentHeadFormulaForMortgage)
            'Hemant (07 Jul 2023) -- End

            'Pinkal (21-Jul-2023) -- Start
            '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
            objDataOperation.AddParameter("@titleexpiryempids", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrTitleExpiryToEmployeeIds)
            'Pinkal (21-Jul-2023) -- End

            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
            objDataOperation.AddParameter("@loantranchedocumentids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanTrancheDocumentTypeIDs.ToString)
            'Pinkal (04-Aug-2023) -- End

            'Pinkal (11-Mar-2024) -- Start
            '(A1X-2505) NMB - Credit card integration.
            objDataOperation.AddParameter("@ccpercentageondsr", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCreditCardPercentageOnDSR)
            objDataOperation.AddParameter("@ccamtondsrfieldid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreditCardAmountOnDSRFieldId)
            objDataOperation.AddParameter("@ccamtonexposurefieldid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreditCardAmountOnExposureFieldId.ToString)
            'Pinkal (11-Mar-2024) -- End


            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            objDataOperation.AddParameter("@ismandatory_mktvalue", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnMarketValueMandatory)
            objDataOperation.AddParameter("@ismandatory_fsv", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnFSVValueMandatory)
            objDataOperation.AddParameter("@ismandatory_boq", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnBOQMandatory)
            objDataOperation.AddParameter("@seniorapplevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSeniorApproverLevelId)
            objDataOperation.AddParameter("@approverlevelalertunkids", SqlDbType.NVarChar, mstrApproverLevelAlertIds.Trim.Length, mstrApproverLevelAlertIds.Trim)
            'Pinkal (17-May-2024) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            objDataOperation.AddParameter("@isdisableemployeeconfirm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDisableEmployeeConfirm)
            'Hemant (22 Nov 2024) -- End

            'Hemant (06 Dec 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
            objDataOperation.AddParameter("@isfinalapprovalnotify", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinalApprovalNotify)
            objDataOperation.AddParameter("@finalapprovalnotifyemployeeids", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFinalApprovalNotifyEmployeeIds)
            'Hemant (06 Dec 2024) -- End



            'strQ = "INSERT INTO lnloan_scheme_master ( " & _
            '   "  code " & _
            '   ", name " & _
            '   ", description " & _
            '   ", minnetsalary " & _
            '   ", isactive " & _
            ' ") VALUES (" & _
            '   "  @code " & _
            '   ", @name " & _
            '   ", @description " & _
            '   ", @minnetsalary " & _
            '   ", @isactive " & _
            ' "); SELECT @@identity"


            strQ = "INSERT INTO lnloan_scheme_master ( " & _
            "  code " & _
            ", name " & _
            ", description " & _
            ", minnetsalary " & _
            ", isactive " & _
            ", isshowoness " & _
                        ", isshowloanbalonpayslip " & _
            ", maxloanamount " & _
            ", loancalctype_id " & _
            ", interest_rate " & _
            ", interest_calctype_id " & _
            ", emi_netpaypercentage " & _
            ", maxnoofinstallment " & _
            ", refloanschemeunkid " & _
            ", tranheadunkid " & _
            ", costcenterunkid " & _
            ", mapped_tranheadunkid " & _
            ", maxinstallmentheadformula " & _
            ", loanschemecategory_id " & _
            ", repaymentdays " & _
            ", insurance_rate " & _
            ", maxloanamountcalctype_id " & _
            ", maxloanamountheadformula " & _
            ", isattachmentrequired " & _
            ", documenttypeids " & _
                        ", reportingtoapproval " & _
                        ", minloanamount " & _
            ", isskipapproval " & _
            ", ispostingtoflexcube " & _
            ", iseligiblefortopup " & _
            ", minnoofinstallmentpaid " & _
            ", lnapproval_dailyreminder " & _
            ", escalation_days " & _
            ", escalatedtoempids " & _
            ", minnoofinstallment " & _
            ", noofdaysbeferetitleexpiry " & _
            ", maxinstallmentheadformulaformortgage " & _
                      ", titleexpiryempids " & _
                        ", loantranchedocumentids " & _
                    ", ccpercentageondsr " & _
                    ", ccamtondsrfieldid " & _
                    ", ccamtonexposurefieldid " & _
                        ", ismandatory_mktvalue " & _
                        ", ismandatory_fsv " & _
                        ", ismandatory_boq " & _
                        ", seniorapplevelunkid " & _
                        ", approverlevelalertunkids " & _
            ", isdisableemployeeconfirm " & _
            ", isfinalapprovalnotify " & _
            ", finalapprovalnotifyemployeeids " & _
          ") VALUES (" & _
            "  @code " & _
            ", @name " & _
            ", @description " & _
            ", @minnetsalary " & _
            ", @isactive " & _
            ", @isshowoness " & _
                        ", @isshowloanbalonpayslip " & _
            ", @maxloanamount " & _
            ", @loancalctype_id " & _
            ", @interest_rate " & _
            ", @interest_calctype_id " & _
            ", @emi_netpaypercentage " & _
            ", @maxnoofinstallment " & _
            ", @refloanschemeunkid " & _
            ", @tranheadunkid " & _
            ", @costcenterunkid " & _
            ", @mapped_tranheadunkid " & _
            ", @maxinstallmentheadformula " & _
            ", @loanschemecategory_id " & _
            ", @repaymentdays " & _
            ", @insurance_rate " & _
            ", @maxloanamountcalctype_id " & _
            ", @maxloanamountheadformula " & _
            ", @isattachmentrequired " & _
            ", @documenttypeids " & _
                        ", @reportingtoapproval " & _
                        ", @minloanamount " & _
            ", @isskipapproval " & _
            ", @ispostingtoflexcube " & _
            ", @iseligiblefortopup " & _
            ", @minnoofinstallmentpaid " & _
            ", @lnapproval_dailyreminder " & _
            ", @escalation_days " & _
            ", @escalatedtoempids " & _
            ", @minnoofinstallment " & _
            ", @noofdaysbeferetitleexpiry " & _
            ", @maxinstallmentheadformulaformortgage " & _
                      ", @titleexpiryempids " & _
                        ", @loantranchedocumentids " & _
                    ", @ccpercentageondsr " & _
                    ", @ccamtondsrfieldid " & _
                    ", @ccamtonexposurefieldid " & _
                        ", @ismandatory_mktvalue " & _
                        ", @ismandatory_fsv " & _
                        ", @ismandatory_boq " & _
                        ", @seniorapplevelunkid " & _
                        ", @approverlevelalertunkids " & _
            ", @isdisableemployeeconfirm " & _
            ", @isfinalapprovalnotify " & _
            ", @finalapprovalnotifyemployeeids " & _
          "); SELECT @@identity"


            'Hemant (06 Dec 2024) -- [isfinalapprovalnotify,finalapprovalnotifyemployeeids]
            'Hemant (22 Nov 2024) -- [isdisableemployeeconfirm]
            'Pinkal (17-May-2024) -- NMB Enhancement For Mortgage Loan.[@ismandatory_mktvalue , @ismandatory_fsv,@ismandatory_boq,@ismortgage_attachementoptional,@seniorapplevelunkid,@approverlevelalertunkids ]

            'Pinkal (11-Mar-2024) -- (A1X-2505) NMB - Credit card integration. [@ccpercentageondsr,@ccamtondsrfieldid,@ccamtonexposurefieldid]

            'Pinkal (04-Aug-2023) -- (A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.[@loantranchedocumentids]
            'Pinkal (21-Jul-2023) -- (A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.[ ", @titleexpiryempids " & _]
            'Hemant (07 Jul 2023) -- [noofdaysbeferetitleexpiry,maxinstallmentheadformulaformortgage]
            'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.[", @minnoofinstallment " & _]
            'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[", @lnapproval_dailyreminder , @escalation_days , @escalatedtoempids "]
            'Hemant (12 Oct 2022) -- [isskipapproval,ispostingtoflexcube,iseligiblefortopup,minnoofinstallmentpaid]
            'Hemant (03 Oct 2022) -- [minloanamount]
            'Pinkal (20-Sep-2022) --NMB Loan Module Enhancement.[@reportingtoapproval ]

            'Hemant (24 Aug 2022) -- [maxinstallmentheadformula,loanschemecategory_id,repaymentdays,insurance_rate,maxloanamountcalctype_id,maxloanamountheadformula,isattachmentrequired,documenttypeids]
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (14 Mar 2019) - [costcenterunkid]
            'Sohail (11 Apr 2018) - [tranheadunkid]
            'Sohail (02 Apr 2018) - [refloanschemeunkid]
            'Varsha (25 Nov 2017) -- [maxnoofinstallment]
            'Sohail (22 Sep 2017) - [emi_netpaypercentage]
            'Nilay (19-Oct-2016) -- [isshowloanbalonpayslip]
            'Pinkal (14-Apr-2015) -- End
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 52,maxloanamount} -- END
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 50, loancalctype_id,interest_rate,interest_calctype_id} -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLoanschemeunkid = dsList.Tables(0).Rows(0).Item(0)

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lnloan_scheme_master", "loanschemeunkid", mintLoanschemeunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            If mintRefLoanSchemeUnkid <= 0 Then
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString + " " + Language.getMessage(mstrModuleName, 4, "Interest"))
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString + " " + Language.getMessage(mstrModuleName, 4, "Interest"))
                objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
                objDataOperation.AddParameter("@minnetsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinnetsalary)
                objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
                objDataOperation.AddParameter("@isshowoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShowOnESS.ToString)
                objDataOperation.AddParameter("@isshowloanbalonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShowLoanBalOnPayslip.ToString)
                objDataOperation.AddParameter("@maxloanamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxLoanAmountLimit)
                objDataOperation.AddParameter("@loancalctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanCalcTypeId)
                objDataOperation.AddParameter("@interest_calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterestCalctypeId)
                objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestRate)
                objDataOperation.AddParameter("@emi_netpaypercentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_netpaypercentage)
                objDataOperation.AddParameter("@maxnoofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNoOfInstallment)
                objDataOperation.AddParameter("@refloanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid)
                'Sohail (11 Apr 2018) -- Start
                'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadUnkid)
                'Sohail (11 Apr 2018) -- End
                'Sohail (14 Mar 2019) -- Start
                'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
                objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid)
                'Sohail (14 Mar 2019) -- End
                'Sohail (29 Apr 2019) -- Start
                'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                objDataOperation.AddParameter("@mapped_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapped_TranheadUnkid)
                'Sohail (29 Apr 2019) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-805 - Develop the logic for maximum loan amount
                objDataOperation.AddParameter("@maxinstallmentheadformula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMaxInstallmentHeadFormula)
                'Hemant (24 Aug 2022) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-818 - As a user, I want to be able to categorize a loan scheme as either secured,unsecured or Advance as radio buttons. A loan scheme can only be one at a time
                objDataOperation.AddParameter("@loanschemecategory_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeCategoryId)
                'Hemant (24 Aug 2022) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-819 - As a user, I want be able to define the installment repayment schedule e.g monthly, quarterly etc
                objDataOperation.AddParameter("@repaymentdays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRepaymentDays)
                'Hemant (24 Aug 2022) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-819 - As a user, I want to have a field to configure the insurance amount on a loan scheme. Same idea we have with the interest rate
                objDataOperation.AddParameter("@insurance_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInsuranceRate)
                'Hemant (24 Aug 2022) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-821 - As a user, I want to be able to define maximum loan amount on a loan scheme as either a flat rate or a formula based on a payroll transaction head
                objDataOperation.AddParameter("@maxloanamountcalctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxLoanAmountCalcTypeId)
                objDataOperation.AddParameter("@maxloanamountheadformula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMaxLoanAmountHeadFormula)
                'Hemant (24 Aug 2022) -- End
                'Hemant (24 Aug 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-822 - As a user I want to be able to define whether attachment on a loan scheme is mandatory or not
                objDataOperation.AddParameter("@isattachmentrequired", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAttachmentRequired.ToString)
                objDataOperation.AddParameter("@documenttypeids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDocumentTypeIDs.ToString)
                'Hemant (24 Aug 2022) -- End

                'Pinkal (20-Sep-2022) -- Start
                'NMB Loan Module Enhancement.
                objDataOperation.AddParameter("@reportingtoapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnRequiredReportingToApproval)
                'Pinkal (20-Sep-2022) -- End

                'Hemant (03 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-914 - NMB - As a user, I want to have a field to specify the minimum amount on loan scheme. Same idea as the maximum amount on loan scheme. System should not allow application of figure below this amount
                objDataOperation.AddParameter("@minloanamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinLoanAmountLimit)
                'Hemant (03 Oct 2022) -- End
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : - As a user, I want to have option on loan scheme master to skip approval
                objDataOperation.AddParameter("@isskipapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSkipApproval)
                'Hemant (12 Oct 2022) -- End
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : - As a user, I want to have a checkbox option on the loan scheme master to define whether a loan scheme requires posting to flexcube or not. Not all loan schemes will be posting to flexcube after final approval  
                objDataOperation.AddParameter("@ispostingtoflexcube", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPostingToFlexcube)
                'Hemant (12 Oct 2022) -- End
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : - As a user, I want to have an option (check box) on the loan scheme to indicate which loan schemes are eligible for top-up. If a loan scheme doesn’t have this option checked, user cannot do top-up on it   
                objDataOperation.AddParameter("@iseligiblefortopup", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEligibleForTopUp)
                'Hemant (12 Oct 2022) -- End
                'Hemant (12 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : - On loan scheme master, if a loan scheme has posting to flexcube enabled, as a user, I want to have the option of specifying the number of installments that need to be paid before a top-up can be done. 
                objDataOperation.AddParameter("@minnoofinstallmentpaid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinOfInstallmentPaid)
                'Hemant (12 Oct 2022) -- End

                'Pinkal (10-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                objDataOperation.AddParameter("@lnapproval_dailyreminder", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnLoanApproval_DailyReminder)
                objDataOperation.AddParameter("@escalation_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEscalationDays)
                objDataOperation.AddParameter("@escalatedtoempids", SqlDbType.NVarChar, mstrEscalatedToEmployeeIds.Trim.Length, mstrEscalatedToEmployeeIds)
                'Pinkal (10-Nov-2022) -- End

                'Pinkal (23-Nov-2022) -- Start
                'NMB Loan Module Enhancement.
                objDataOperation.AddParameter("@minnoofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinNoOfInstallment)
                'Pinkal (23-Nov-2022) -- End
                'Hemant (07 Jul 2023) -- Start
                'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
                objDataOperation.AddParameter("@noofdaysbeferetitleexpiry ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoOfDaysBefereTitleExpiry)
                'Hemant (07 July 2023) -- End
                'Hemant (07 Jul 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
                objDataOperation.AddParameter("@maxinstallmentheadformulaformortgage", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMaxInstallmentHeadFormulaForMortgage)
                'Hemant (07 Jul 2023) -- End

                'Pinkal (21-Jul-2023) -- Start
                '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
                objDataOperation.AddParameter("@titleexpiryempids", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrTitleExpiryToEmployeeIds)
                'Pinkal (21-Jul-2023) -- End

                'Pinkal (04-Aug-2023) -- Start
                '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
                objDataOperation.AddParameter("@loantranchedocumentids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanTrancheDocumentTypeIDs.ToString)
                'Pinkal (04-Aug-2023) -- End

                'Pinkal (11-Mar-2024) -- Start
                '(A1X-2505) NMB - Credit card integration.
                objDataOperation.AddParameter("@ccpercentageondsr", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCreditCardPercentageOnDSR)
                objDataOperation.AddParameter("@ccamtondsrfieldid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreditCardAmountOnDSRFieldId)
                objDataOperation.AddParameter("@ccamtonexposurefieldid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreditCardAmountOnExposureFieldId.ToString)
                'Pinkal (11-Mar-2024) -- End

                'Pinkal (17-May-2024) -- Start
                'NMB Enhancement For Mortgage Loan.
                objDataOperation.AddParameter("@ismandatory_mktvalue", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnMarketValueMandatory)
                objDataOperation.AddParameter("@ismandatory_fsv", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnFSVValueMandatory)
                objDataOperation.AddParameter("@ismandatory_boq", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnBOQMandatory)
                objDataOperation.AddParameter("@seniorapplevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSeniorApproverLevelId)
                objDataOperation.AddParameter("@approverlevelalertunkids", SqlDbType.NVarChar, mstrApproverLevelAlertIds.Trim.Length, mstrApproverLevelAlertIds.Trim)
                'Pinkal (17-May-2024) -- End

                'Hemant (22 Nov 2024) -- Start
                'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                objDataOperation.AddParameter("@isdisableemployeeconfirm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDisableEmployeeConfirm)
                'Hemant (22 Nov 2024) -- End

                'Hemant (06 Dec 2024) -- Start
                'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
                objDataOperation.AddParameter("@isfinalapprovalnotify", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinalApprovalNotify)
                objDataOperation.AddParameter("@finalapprovalnotifyemployeeids", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFinalApprovalNotifyEmployeeIds)
                'Hemant (06 Dec 2024) -- End


                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim intUnkId As Integer = dsList.Tables(0).Rows(0).Item(0)

                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lnloan_scheme_master", "loanschemeunkid", intUnkId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Sohail (02 Apr 2018) -- End

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-990 - As a user, I want to pass the principal balance contained in flexcube loan view to Aruti payroll to display on employee salary slip as the loan balance for loans set to post to flexcube
            If mblnIsPostingToFlexcube = True Then
                'LOAN INSTALLMENT AMOUNT HEAD
                Dim objTransactionHead As New clsTransactionHead
                Dim intHeadID As Integer = 0

                If objTransactionHead.isExistLoanSchemeHead(enCalcType.LOAN_INSTALLMENT, mintLoanschemeunkid, intHeadID, objDataOperation) = False Then
                    objTransactionHead._Trnheadcode = mstrCode & " INSTALLMENT"
                    objTransactionHead._Trnheadname = mstrName & " INSTALLMENT"
                    objTransactionHead._Trnheadtype_Id = enTranHeadType.DeductionForEmployee
                    objTransactionHead._Typeof_id = enTypeOf.LOAN
                    objTransactionHead._Calctype_Id = enCalcType.LOAN_INSTALLMENT
                    objTransactionHead._Isrecurrent = True
                    objTransactionHead._Isappearonpayslip = mblnShowLoanBalOnPayslip
                    objTransactionHead._Computeon_Id = 0
                    objTransactionHead._Formula = ""
                    objTransactionHead._Formulaid = ""

                    objTransactionHead._Userunkid = mintUserunkid

                    objTransactionHead._Istaxable = False
                    objTransactionHead._Istaxrelief = False
                    objTransactionHead._Iscumulative = False
                    objTransactionHead._Isnoncashbenefit = False
                    objTransactionHead._Ismonetary = True
                    objTransactionHead._Isactive = True
                    objTransactionHead._Activityunkid = 0
                    objTransactionHead._Isbasicsalaryasotherearning = False
                    objTransactionHead._CostCenterUnkId = 0
                    objTransactionHead._LoanSchemeunkid = mintLoanschemeunkid
                    If objTransactionHead.Insert(enCalcType.LOAN_INSTALLMENT, ConfigParameter._Object._CurrentDateAndTime) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        If objTransactionHead._Message <> "" Then
                            eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                        End If
                        Return False
                    End If
                Else
                    objTransactionHead._Tranheadunkid(mstrDatabaseName) = intHeadID
                    objTransactionHead._Trnheadcode = mstrCode & " INSTALLMENT"
                    objTransactionHead._Trnheadname = mstrName & " INSTALLMENT"
                    objTransactionHead._Trnheadtype_Id = enTranHeadType.DeductionForEmployee
                    objTransactionHead._Isappearonpayslip = mblnShowLoanBalOnPayslip
                    objTransactionHead._Istaxable = False

                    objTransactionHead._Userunkid = mintUserunkid


                    If objTransactionHead.Update(mstrDatabaseName, enCalcType.LOAN_INSTALLMENT, ConfigParameter._Object._CurrentDateAndTime) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        If objTransactionHead._Message <> "" Then
                            eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                        End If
                        Return False
                    End If
                End If

                'LOAN BALANCE AMOUNT HEAD
                intHeadID = 0
                objTransactionHead = New clsTransactionHead
                If objTransactionHead.isExistLoanSchemeHead(enCalcType.LOAN_BALANCE, mintLoanschemeunkid, intHeadID, objDataOperation) = False Then
                    objTransactionHead._Trnheadcode = mstrCode & " BALANCE"
                    objTransactionHead._Trnheadname = mstrName & " BALANCE"
                    objTransactionHead._Trnheadtype_Id = enTranHeadType.Informational
                    objTransactionHead._Typeof_id = enTypeOf.LOAN
                    objTransactionHead._Calctype_Id = enCalcType.LOAN_BALANCE
                    objTransactionHead._Isrecurrent = True
                    objTransactionHead._Isappearonpayslip = mblnShowLoanBalOnPayslip
                    objTransactionHead._Computeon_Id = 0
                    objTransactionHead._Formula = ""
                    objTransactionHead._Formulaid = ""

                    objTransactionHead._Userunkid = mintUserunkid

                    objTransactionHead._Istaxable = False
                    objTransactionHead._Istaxrelief = False
                    objTransactionHead._Iscumulative = False
                    objTransactionHead._Isnoncashbenefit = False
                    objTransactionHead._Ismonetary = True
                    objTransactionHead._Isactive = True
                    objTransactionHead._Activityunkid = 0
                    objTransactionHead._Isbasicsalaryasotherearning = False
                    objTransactionHead._CostCenterUnkId = 0
                    objTransactionHead._LoanSchemeunkid = mintLoanschemeunkid
                    If objTransactionHead.Insert(enCalcType.LOAN_BALANCE, ConfigParameter._Object._CurrentDateAndTime) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        If objTransactionHead._Message <> "" Then
                            eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                        End If
                        Return False
                    End If
                Else
                    objTransactionHead._Tranheadunkid(mstrDatabaseName) = intHeadID
                    objTransactionHead._Trnheadcode = mstrCode & " BALANCE"
                    objTransactionHead._Trnheadname = mstrName & " BALANCE"
                    objTransactionHead._Trnheadtype_Id = enTranHeadType.Informational
                    objTransactionHead._Isappearonpayslip = mblnShowLoanBalOnPayslip
                    objTransactionHead._Istaxable = False

                    objTransactionHead._Userunkid = mintUserunkid


                    If objTransactionHead.Update(mstrDatabaseName, enCalcType.LOAN_BALANCE, ConfigParameter._Object._CurrentDateAndTime) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        If objTransactionHead._Message <> "" Then
                            eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                        End If
                        Return False
                    End If
                End If
            End If
            'Hemant (27 Oct 2022) -- End

            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 

            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_scheme_master) </purpose>
    Public Function Update() As Boolean
        If isExist(, mstrName, mintLoanschemeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Loan Scheme is already defined. Please define new Loan Scheme.")
            Return False
        End If


        If isExist(mstrCode, , mintLoanschemeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Code is already defined. Please define new Code.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 


        Try
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloanschemeunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)


            'Anjan (11 May 2011)-Start
            'objDataOperation.AddParameter("@minnetsalary", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdecMinnetsalary.ToString)
            objDataOperation.AddParameter("@minnetsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinnetsalary)
            'Anjan (11 May 2011)-End 



            'objDataOperation.AddParameter("@isinclude_parttime", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsinclude_Parttime.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)


            'Pinkal (14-Apr-2015) -- Start
            'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

            objDataOperation.AddParameter("@isshowoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShowOnESS.ToString)

            'Nilay (19-Oct-2016) -- Start
            'Enhancement - Appear Loan Balance on Payslip option on loan scheme master for B.C.Patel & Co.
            objDataOperation.AddParameter("@isshowloanbalonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShowLoanBalOnPayslip.ToString)
            'Nilay (19-Oct-2016) -- End

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 52
            objDataOperation.AddParameter("@maxloanamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxLoanAmountLimit)
            'S.SANDEEP [20-SEP-2017] -- END

            'S.SANDEEP [20-SEP-2017] -- START
            'ISSUE/ENHANCEMENT : REF-ID # 50
            objDataOperation.AddParameter("@loancalctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanCalcTypeId)
            objDataOperation.AddParameter("@interest_calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterestCalctypeId)
            objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestRate)
            'S.SANDEEP [20-SEP-2017] -- END
            objDataOperation.AddParameter("@emi_netpaypercentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_netpaypercentage) 'Sohail (22 Sep 2017)

            'Varsha (25 Nov 2017) -- Start
            'Enhancement: (RefNo: 124) PACRA - 70.1 - On the Add/Edit loan scheme screen, add option to indicate max number of installments that can ever be allowed for a particular loan scheme ID. E.g 36 max installments for loan scheme A
            objDataOperation.AddParameter("@maxnoofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNoOfInstallment)
            'Varsha (25 Nov 2017) -- End

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            objDataOperation.AddParameter("@refloanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRefLoanSchemeUnkid)
            'Sohail (02 Apr 2018) -- End
            'Sohail (11 Apr 2018) -- Start
            'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadUnkid)
            'Sohail (11 Apr 2018) -- End
            'Sohail (14 Mar 2019) -- Start
            'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid)
            'Sohail (14 Mar 2019) -- End
            'Sohail (29 Apr 2019) -- Start
            'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
            objDataOperation.AddParameter("@mapped_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapped_TranheadUnkid)
            'Sohail (29 Apr 2019) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-805 - Develop the logic for maximum loan amount
            objDataOperation.AddParameter("@maxinstallmentheadformula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMaxInstallmentHeadFormula)
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-818 - As a user, I want to be able to categorize a loan scheme as either secured,unsecured or Advance as radio buttons. A loan scheme can only be one at a time
            objDataOperation.AddParameter("@loanschemecategory_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeCategoryId)
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-819 - As a user, I want be able to define the installment repayment schedule e.g monthly, quarterly etc
            objDataOperation.AddParameter("@repaymentdays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRepaymentDays)
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-819 - As a user, I want to have a field to configure the insurance amount on a loan scheme. Same idea we have with the interest rate
            objDataOperation.AddParameter("@insurance_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInsuranceRate)
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-821 - As a user, I want to be able to define maximum loan amount on a loan scheme as either a flat rate or a formula based on a payroll transaction head
            objDataOperation.AddParameter("@maxloanamountcalctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxLoanAmountCalcTypeId)
            objDataOperation.AddParameter("@maxloanamountheadformula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMaxLoanAmountHeadFormula)
            'Hemant (24 Aug 2022) -- End
            'Hemant (24 Aug 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-822 - As a user I want to be able to define whether attachment on a loan scheme is mandatory or not
            objDataOperation.AddParameter("@isattachmentrequired", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAttachmentRequired.ToString)
            objDataOperation.AddParameter("@documenttypeids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDocumentTypeIDs.ToString)
            'Hemant (24 Aug 2022) -- End

            'Pinkal (20-Sep-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@reportingtoapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnRequiredReportingToApproval)
            'Pinkal (20-Sep-2022) -- End

            'Hemant (03 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-914 - NMB - As a user, I want to have a field to specify the minimum amount on loan scheme. Same idea as the maximum amount on loan scheme. System should not allow application of figure below this amount
            objDataOperation.AddParameter("@minloanamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinLoanAmountLimit)
            'Hemant (03 Oct 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : - As a user, I want to have option on loan scheme master to skip approval
            objDataOperation.AddParameter("@isskipapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSkipApproval)
            'Hemant (12 Oct 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : - As a user, I want to have a checkbox option on the loan scheme master to define whether a loan scheme requires posting to flexcube or not. Not all loan schemes will be posting to flexcube after final approval  
            objDataOperation.AddParameter("@ispostingtoflexcube", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPostingToFlexcube)
            'Hemant (12 Oct 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : - As a user, I want to have an option (check box) on the loan scheme to indicate which loan schemes are eligible for top-up. If a loan scheme doesn’t have this option checked, user cannot do top-up on it   
            objDataOperation.AddParameter("@iseligiblefortopup", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEligibleForTopUp)
            'Hemant (12 Oct 2022) -- End
            'Hemant (12 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : - On loan scheme master, if a loan scheme has posting to flexcube enabled, as a user, I want to have the option of specifying the number of installments that need to be paid before a top-up can be done. 
            objDataOperation.AddParameter("@minnoofinstallmentpaid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinOfInstallmentPaid)
            'Hemant (12 Oct 2022) -- End

            'Pinkal (10-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@lnapproval_dailyreminder", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnLoanApproval_DailyReminder)
            objDataOperation.AddParameter("@escalation_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEscalationDays)
            objDataOperation.AddParameter("@escalatedtoempids", SqlDbType.NVarChar, mstrEscalatedToEmployeeIds.Trim.Length, mstrEscalatedToEmployeeIds)
            'Pinkal (10-Nov-2022) -- End


            'Pinkal (23-Nov-2022) -- Start
            'NMB Loan Module Enhancement.
            objDataOperation.AddParameter("@minnoofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinNoOfInstallment)
            'Pinkal (23-Nov-2022) -- End
            'Hemant (07 Jul 2023) -- Start
            'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
            objDataOperation.AddParameter("@noofdaysbeferetitleexpiry ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoOfDaysBefereTitleExpiry)
            'Hemant (07 July 2023) -- End
            'Hemant (07 Jul 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
            objDataOperation.AddParameter("@maxinstallmentheadformulaformortgage", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMaxInstallmentHeadFormulaForMortgage)
            'Hemant (07 Jul 2023) -- End
            'strQ = "UPDATE lnloan_scheme_master SET " & _
            '  "  code = @code " &
            '  ", name = @name " & _
            '  ", description = @description " & _
            '  ", minnetsalary = @minnetsalary " & _
            '  ", isactive = @isactive " & _
            ' "WHERE loanschemeunkid = @loanschemeunkid "

            'Pinkal (21-Jul-2023) -- Start
            '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
            objDataOperation.AddParameter("@titleexpiryempids", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrTitleExpiryToEmployeeIds)
            'Pinkal (21-Jul-2023) -- End


            'Pinkal (04-Aug-2023) -- Start
            '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
            objDataOperation.AddParameter("@loantranchedocumentids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanTrancheDocumentTypeIDs.ToString)
            'Pinkal (04-Aug-2023) -- End

            'Pinkal (11-Mar-2024) -- Start
            '(A1X-2505) NMB - Credit card integration.
            objDataOperation.AddParameter("@ccpercentageondsr", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCreditCardPercentageOnDSR)
            objDataOperation.AddParameter("@ccamtondsrfieldid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreditCardAmountOnDSRFieldId)
            objDataOperation.AddParameter("@ccamtonexposurefieldid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreditCardAmountOnExposureFieldId.ToString)
            'Pinkal (11-Mar-2024) -- End

            'Pinkal (17-May-2024) -- Start
            'NMB Enhancement For Mortgage Loan.
            objDataOperation.AddParameter("@ismandatory_mktvalue", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnMarketValueMandatory)
            objDataOperation.AddParameter("@ismandatory_fsv", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnFSVValueMandatory)
            objDataOperation.AddParameter("@ismandatory_boq", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnBOQMandatory)
            objDataOperation.AddParameter("@seniorapplevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSeniorApproverLevelId)
            objDataOperation.AddParameter("@approverlevelalertunkids", SqlDbType.NVarChar, mstrApproverLevelAlertIds.Trim.Length, mstrApproverLevelAlertIds.Trim)
            'Pinkal (17-May-2024) -- End

            'Hemant (22 Nov 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
            objDataOperation.AddParameter("@isdisableemployeeconfirm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDisableEmployeeConfirm)
            'Hemant (22 Nov 2024) -- End

            'Hemant (06 Dec 2024) -- Start
            'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
            objDataOperation.AddParameter("@isfinalapprovalnotify", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinalApprovalNotify)
            objDataOperation.AddParameter("@finalapprovalnotifyemployeeids", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFinalApprovalNotifyEmployeeIds)
            'Hemant (06 Dec 2024) -- End


            strQ = "UPDATE lnloan_scheme_master SET " & _
                     "  code = @code " & _
                     ", name = @name " & _
                     ", description = @description " & _
                     ", minnetsalary = @minnetsalary " & _
                     ", isactive = @isactive " & _
                     ", isshowoness = @isshowoness " & _
                     ", isshowloanbalonpayslip = @isshowloanbalonpayslip " & _
                     ", maxloanamount = @maxloanamount " & _
                     ", loancalctype_id = @loancalctype_id " & _
                     ", interest_rate = @interest_rate " & _
                     ", interest_calctype_id = @interest_calctype_id " & _
                     ", emi_netpaypercentage = @emi_netpaypercentage " & _
                     ", maxnoofinstallment = @maxnoofinstallment " & _
                     ", refloanschemeunkid = @refloanschemeunkid " & _
                     ", tranheadunkid = @tranheadunkid " & _
                     ", costcenterunkid = @costcenterunkid " & _
                     ", mapped_tranheadunkid = @mapped_tranheadunkid " & _
                     ", maxinstallmentheadformula = @maxinstallmentheadformula " & _
                     ", loanschemecategory_id = @loanschemecategory_id " & _
                     ", repaymentdays = @repaymentdays " & _
                     ", insurance_rate = @insurance_rate " & _
                     ", maxloanamountcalctype_id = @maxloanamountcalctype_id " & _
                     ", maxloanamountheadformula = @maxloanamountheadformula " & _
                     ", isattachmentrequired = @isattachmentrequired " & _
                     ", documenttypeids = @documenttypeids " & _
                     ", reportingtoapproval = @reportingtoapproval " & _
                     ", minloanamount = @minloanamount " & _
                     ", isskipapproval = @isskipapproval " & _
                     ", ispostingtoflexcube = @ispostingtoflexcube " & _
                     ", iseligiblefortopup = @iseligiblefortopup " & _
                     ", minnoofinstallmentpaid = @minnoofinstallmentpaid " & _
                     ", lnapproval_dailyreminder = @lnapproval_dailyreminder " & _
                     ", escalation_days = @escalation_days " & _
                     ", escalatedtoempids = @escalatedtoempids " & _
                     ", minnoofinstallment = @minnoofinstallment " & _
                     ", noofdaysbeferetitleexpiry = @noofdaysbeferetitleexpiry " & _
                     ", maxinstallmentheadformulaformortgage = @maxinstallmentheadformulaformortgage " & _
                     ", titleexpiryempids = @titleexpiryempids " & _
                     ", loantranchedocumentids = @loantranchedocumentids " & _
                     ", ccpercentageondsr = @ccpercentageondsr " & _
                     ", ccamtondsrfieldid = @ccamtondsrfieldid " & _
                     ", ccamtonexposurefieldid = @ccamtonexposurefieldid " & _
                     ", ismandatory_mktvalue = @ismandatory_mktvalue " & _
                     ", ismandatory_fsv = @ismandatory_fsv " & _
                     ", ismandatory_boq = @ismandatory_boq " & _
                     ", seniorapplevelunkid = @seniorapplevelunkid " & _
                     ", approverlevelalertunkids = @approverlevelalertunkids " & _
                     ", isdisableemployeeconfirm = @isdisableemployeeconfirm " & _
                     ", isfinalapprovalnotify = @isfinalapprovalnotify " & _
                     ", finalapprovalnotifyemployeeids = @finalapprovalnotifyemployeeids " & _
                    "WHERE loanschemeunkid = @loanschemeunkid "

            'Hemant (06 Dec 2024) -- [isfinalapprovalnotify,finalapprovalnotifyemployeeids]
            'Hemant (22 Nov 2024) -- [isdisableemployeeconfirm]
            'Pinkal (17-May-2024) -- NMB Enhancement For Mortgage Loan.[ ismandatory_mktvalue = @ismandatory_mktvalue, ismandatory_fsv = @ismandatory_fsv, ismandatory_boq = @ismandatory_boq,ismortgage_attachementoptional = @ismortgage_attachementoptional, seniorapplevelunkid = @seniorapplevelunkid , approverlevelalertunkids = @approverlevelalertunkids " & _]

            'Pinkal (11-Mar-2024) -- (A1X-2505) NMB - Credit card integration. [ ccpercentageondsr = @ccpercentageondsr,ccamtondsrfieldid = @ccamtondsrfieldid,ccamtonexposurefieldid = @ccamtonexposurefieldid]

            'Pinkal (04-Aug-2023) -- (A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.[", loantranchedocumentids = @loantranchedocumentids " & _]

            'Pinkal (21-Jul-2023) -- (A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.[  ", titleexpiryempids = @titleexpiryempids " & _]
            'Hemant (07 Jul 2023) -- [noofdaysbeferetitleexpiry,maxinstallmentheadformulaformortgage]
            'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.[", minnoofinstallment = @minnoofinstallment " & _]
            'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[", lnapproval_dailyreminder = @lnapproval_dailyreminder , escalation_days = @escalation_days , escalatedtoempids = @escalatedtoempids " & _]

            'Hemant (12 Oct 2022) -- [isskipapproval,ispostingtoflexcube,iseligiblefortopup,minnoofinstallmentpaid]
            'Hemant (03 Oct 2022) -- [minloanamount]
            'Pinkal (20-Sep-2022) -- 'NMB Loan Module Enhancement.[ ", reportingtoapproval = @reportingtoapproval " & _]

            'Hemant (24 Aug 2022) -- [maxinstallmentheadformula,loanschemecategory_id,repaymentdays,insurance_rate,maxloanamountheadformula,isattachmentrequired,documenttypeids]
            'Sohail (29 Apr 2019) - [mapped_tranheadunkid]
            'Sohail (14 Mar 2019) - [costcenterunkid]
            'Sohail (11 Apr 2018) - [tranheadunkid]
            'Sohail (02 Apr 2018) - [refloanschemeunkid]
            'Varsha (25 Nov 2017) -- [maxnoofinstallment]
            'Sohail (22 Sep 2017) - [emi_netpaypercentage]
            'Nilay (19-Oct-2016) -- [isshowloanbalonpayslip]
            'Pinkal (14-Apr-2015) -- End
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 52,maxloanamount} -- END
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 50, loancalctype_id,interest_rate,interest_calctype_id} -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "lnloan_scheme_master", mintLoanschemeunkid, "loanschemeunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lnloan_scheme_master", "loanschemeunkid", mintLoanschemeunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            If mintRefLoanSchemeUnkid <= 0 Then
                Dim intInterestLoanUnkId As Integer = GetLoanSchemeInterestID(mintLoanschemeunkid, objDataOperation)
                If intInterestLoanUnkId > 0 Then
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intInterestLoanUnkId.ToString)
                    objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString + " " + Language.getMessage(mstrModuleName, 4, "Interest"))
                    objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString + " " + Language.getMessage(mstrModuleName, 4, "Interest"))
                    objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
                    objDataOperation.AddParameter("@minnetsalary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinnetsalary)
                    objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
                    objDataOperation.AddParameter("@isshowoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShowOnESS.ToString)
                    objDataOperation.AddParameter("@isshowloanbalonpayslip", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShowLoanBalOnPayslip.ToString)
                    objDataOperation.AddParameter("@maxloanamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMaxLoanAmountLimit)
                    objDataOperation.AddParameter("@loancalctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanCalcTypeId)
                    objDataOperation.AddParameter("@interest_calctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInterestCalctypeId)
                    objDataOperation.AddParameter("@interest_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInterestRate)
                    objDataOperation.AddParameter("@emi_netpaypercentage", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecEmi_netpaypercentage)
                    objDataOperation.AddParameter("@maxnoofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxNoOfInstallment)
                    objDataOperation.AddParameter("@refloanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanschemeunkid)
                    'Sohail (11 Apr 2018) -- Start
                    'PACRA Enhancement - Ref # 220 : Provide control that will stop loan/advance application if NET pay for staff will be less than 25% if application is approved in 72.1.
                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadUnkid)
                    'Sohail (11 Apr 2018) -- End
                    'Sohail (14 Mar 2019) -- Start
                    'NMB Enhancement - 76.1 - Give cost centre mapping on loan scheme, saving scheme screen and configuration and show cost center on JV from mapped cost centre on loan and saving screen for loan and saving tranactions.
                    objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostcenterunkid)
                    'Sohail (14 Mar 2019) -- End
                    'Sohail (29 Apr 2019) -- Start
                    'ENGENDER HEALTH Enhancement - REF # 0003775 - 76.1 - Automatic loan deduction change when employee salary changes (provide mapping of head as loan deduction).
                    objDataOperation.AddParameter("@mapped_tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapped_TranheadUnkid)
                    'Sohail (29 Apr 2019) -- End
                    'Hemant (24 Aug 2022) -- Start
                    'ENHANCEMENT(NMB) : AC2-805 - Develop the logic for maximum loan amount
                    objDataOperation.AddParameter("@maxinstallmentheadformula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMaxInstallmentHeadFormula)
                    'Hemant (24 Aug 2022) -- End
                    'Hemant (24 Aug 2022) -- Start
                    'ENHANCEMENT(NMB) : AC2-818 - As a user, I want to be able to categorize a loan scheme as either secured,unsecured or Advance as radio buttons. A loan scheme can only be one at a time
                    objDataOperation.AddParameter("@loanschemecategory_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanSchemeCategoryId)
                    'Hemant (24 Aug 2022) -- End
                    'Hemant (24 Aug 2022) -- Start
                    'ENHANCEMENT(NMB) : AC2-819 - As a user, I want be able to define the installment repayment schedule e.g monthly, quarterly etc
                    objDataOperation.AddParameter("@repaymentdays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRepaymentDays)
                    'Hemant (24 Aug 2022) -- End
                    'Hemant (24 Aug 2022) -- Start
                    'ENHANCEMENT(NMB) : AC2-819 - As a user, I want to have a field to configure the insurance amount on a loan scheme. Same idea we have with the interest rate
                    objDataOperation.AddParameter("@insurance_rate", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecInsuranceRate)
                    'Hemant (24 Aug 2022) -- End
                    'Hemant (24 Aug 2022) -- Start
                    'ENHANCEMENT(NMB) : AC2-821 - As a user, I want to be able to define maximum loan amount on a loan scheme as either a flat rate or a formula based on a payroll transaction head
                    objDataOperation.AddParameter("@maxloanamountcalctype_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxLoanAmountCalcTypeId)
                    objDataOperation.AddParameter("@maxloanamountheadformula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMaxLoanAmountHeadFormula)
                    'Hemant (24 Aug 2022) -- End
                    'Hemant (24 Aug 2022) -- Start
                    'ENHANCEMENT(NMB) : AC2-822 - As a user I want to be able to define whether attachment on a loan scheme is mandatory or not
                    objDataOperation.AddParameter("@isattachmentrequired", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAttachmentRequired.ToString)
                    objDataOperation.AddParameter("@documenttypeids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDocumentTypeIDs.ToString)
                    'Hemant (24 Aug 2022) -- End

                    'Pinkal (20-Sep-2022) -- Start
                    'NMB Loan Module Enhancement.
                    objDataOperation.AddParameter("@reportingtoapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnRequiredReportingToApproval)
                    'Pinkal (20-Sep-2022) -- End
                    'Hemant (03 Oct 2022) -- Start
                    'ENHANCEMENT(NMB) : AC2-914 - NMB - As a user, I want to have a field to specify the minimum amount on loan scheme. Same idea as the maximum amount on loan scheme. System should not allow application of figure below this amount
                    objDataOperation.AddParameter("@minloanamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMinLoanAmountLimit)
                    'Hemant (03 Oct 2022) -- End
                    'Hemant (12 Oct 2022) -- Start
                    'ENHANCEMENT(NMB) : - As a user, I want to have option on loan scheme master to skip approval
                    objDataOperation.AddParameter("@isskipapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSkipApproval)
                    'Hemant (12 Oct 2022) -- End
                    'Hemant (12 Oct 2022) -- Start
                    'ENHANCEMENT(NMB) : - As a user, I want to have a checkbox option on the loan scheme master to define whether a loan scheme requires posting to flexcube or not. Not all loan schemes will be posting to flexcube after final approval  
                    objDataOperation.AddParameter("@ispostingtoflexcube", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPostingToFlexcube)
                    'Hemant (12 Oct 2022) -- End
                    'Hemant (12 Oct 2022) -- Start
                    'ENHANCEMENT(NMB) : - As a user, I want to have an option (check box) on the loan scheme to indicate which loan schemes are eligible for top-up. If a loan scheme doesn’t have this option checked, user cannot do top-up on it   
                    objDataOperation.AddParameter("@iseligiblefortopup", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsEligibleForTopUp)
                    'Hemant (12 Oct 2022) -- End
                    'Hemant (12 Oct 2022) -- Start
                    'ENHANCEMENT(NMB) : - On loan scheme master, if a loan scheme has posting to flexcube enabled, as a user, I want to have the option of specifying the number of installments that need to be paid before a top-up can be done. 
                    objDataOperation.AddParameter("@minnoofinstallmentpaid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinOfInstallmentPaid)
                    'Hemant (12 Oct 2022) -- End

                    'Pinkal (10-Nov-2022) -- Start
                    'NMB Loan Module Enhancement.
                    objDataOperation.AddParameter("@lnapproval_dailyreminder", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnLoanApproval_DailyReminder)
                    objDataOperation.AddParameter("@escalation_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEscalationDays)
                    objDataOperation.AddParameter("@escalatedtoempids", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrEscalatedToEmployeeIds)
                    'Pinkal (10-Nov-2022) -- End

                    'Pinkal (23-Nov-2022) -- Start
                    'NMB Loan Module Enhancement.
                    objDataOperation.AddParameter("@minnoofinstallment", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMinNoOfInstallment)
                    'Pinkal (23-Nov-2022) -- End
                    'Hemant (07 Jul 2023) -- Start
                    'Enhancement(NMB) : A1X-1105 : Deny Mortgage loan application if the title expiry date is less than X number of configured days
                    objDataOperation.AddParameter("@noofdaysbeferetitleexpiry ", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoOfDaysBefereTitleExpiry)
                    'Hemant (07 July 2023) -- End
                    'Hemant (07 Jul 2023) -- Start
                    'ENHANCEMENT(NMB) : A1X-1098 - Changes on maximum loan installment amount computation formula for loan applicants already servicing mortgage loans
                    objDataOperation.AddParameter("@maxinstallmentheadformulaformortgage", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMaxInstallmentHeadFormulaForMortgage)
                    'Hemant (07 Jul 2023) -- End

                    'Pinkal (21-Jul-2023) -- Start
                    '(A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.
                    objDataOperation.AddParameter("@titleexpiryempids", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrTitleExpiryToEmployeeIds)
                    'Pinkal (21-Jul-2023) -- End


                    'Pinkal (04-Aug-2023) -- Start
                    '(A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.
                    objDataOperation.AddParameter("@loantranchedocumentids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLoanTrancheDocumentTypeIDs.ToString)
                    'Pinkal (04-Aug-2023) -- End

                    'Pinkal (11-Mar-2024) -- Start
                    '(A1X-2505) NMB - Credit card integration.
                    objDataOperation.AddParameter("@ccpercentageondsr", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCreditCardPercentageOnDSR)
                    objDataOperation.AddParameter("@ccamtondsrfieldid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreditCardAmountOnDSRFieldId)
                    objDataOperation.AddParameter("@ccamtonexposurefieldid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCreditCardAmountOnExposureFieldId.ToString)
                    'Pinkal (11-Mar-2024) -- End

                    'Pinkal (17-May-2024) -- Start
                    'NMB Enhancement For Mortgage Loan.
                    objDataOperation.AddParameter("@ismandatory_mktvalue", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnMarketValueMandatory)
                    objDataOperation.AddParameter("@ismandatory_fsv", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnFSVValueMandatory)
                    objDataOperation.AddParameter("@ismandatory_boq", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnBOQMandatory)
                    objDataOperation.AddParameter("@seniorapplevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSeniorApproverLevelId)
                    objDataOperation.AddParameter("@approverlevelalertunkids", SqlDbType.NVarChar, mstrApproverLevelAlertIds.Trim.Length, mstrApproverLevelAlertIds.Trim)
                    'Pinkal (17-May-2024) -- End

                    'Hemant (22 Nov 2024) -- Start
                    'ISSUE/ENHANCEMENT(TADB): A1X - 2850 :  FlexCube Loan changes
                    objDataOperation.AddParameter("@isdisableemployeeconfirm", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsDisableEmployeeConfirm)
                    'Hemant (22 Nov 2024) -- End

                    'Hemant (06 Dec 2024) -- Start
                    'ISSUE/ENHANCEMENT(TADB): A1X - 2880 :  FlexCube Loan changes
                    objDataOperation.AddParameter("@isfinalapprovalnotify", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinalApprovalNotify)
                    objDataOperation.AddParameter("@finalapprovalnotifyemployeeids", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFinalApprovalNotifyEmployeeIds)
                    'Hemant (06 Dec 2024) -- End


                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If clsCommonATLog.IsTableDataUpdate("atcommon_log", "lnloan_scheme_master", intInterestLoanUnkId, "loanschemeunkid", 2) Then
                        If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lnloan_scheme_master", "loanschemeunkid", intInterestLoanUnkId) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                Else
                    objDataOperation.ClearParameters()
                    mstrCode = mstrCode.ToString + " " + Language.getMessage(mstrModuleName, 4, "Interest")
                    mstrName = mstrName.ToString + " " + Language.getMessage(mstrModuleName, 4, "Interest")
                    mintRefLoanSchemeUnkid = mintLoanschemeunkid

                    If Insert() = False Then
                        Return False
                    End If
                End If
            End If
            'Sohail (02 Apr 2018) -- End

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-990 - As a user, I want to pass the principal balance contained in flexcube loan view to Aruti payroll to display on employee salary slip as the loan balance for loans set to post to flexcube
            If mblnIsPostingToFlexcube = True Then
                'LOAN INSTALLMENT AMOUNT HEAD
                Dim objTransactionHead As New clsTransactionHead
                Dim intHeadID As Integer = 0

                If objTransactionHead.isExistLoanSchemeHead(enCalcType.LOAN_INSTALLMENT, mintLoanschemeunkid, intHeadID, objDataOperation) = False Then
                    objTransactionHead._Trnheadcode = mstrCode & " INSTALLMENT"
                    objTransactionHead._Trnheadname = mstrName & " INSTALLMENT"
                    objTransactionHead._Trnheadtype_Id = enTranHeadType.DeductionForEmployee
                    objTransactionHead._Typeof_id = enTypeOf.LOAN
                    objTransactionHead._Calctype_Id = enCalcType.LOAN_INSTALLMENT
                    objTransactionHead._Isrecurrent = True
                    objTransactionHead._Isappearonpayslip = mblnShowLoanBalOnPayslip
                    objTransactionHead._Computeon_Id = 0
                    objTransactionHead._Formula = ""
                    objTransactionHead._Formulaid = ""

                    objTransactionHead._Userunkid = mintUserunkid

                    objTransactionHead._Istaxable = False
                    objTransactionHead._Istaxrelief = False
                    objTransactionHead._Iscumulative = False
                    objTransactionHead._Isnoncashbenefit = False
                    objTransactionHead._Ismonetary = True
                    objTransactionHead._Isactive = True
                    objTransactionHead._Activityunkid = 0
                    objTransactionHead._Isbasicsalaryasotherearning = False
                    objTransactionHead._CostCenterUnkId = 0
                    objTransactionHead._LoanSchemeunkid = mintLoanschemeunkid
                    If objTransactionHead.Insert(enCalcType.LOAN_INSTALLMENT, ConfigParameter._Object._CurrentDateAndTime) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        If objTransactionHead._Message <> "" Then
                            eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                        End If
                        Return False
                    End If
                Else
                    objTransactionHead._Tranheadunkid(mstrDatabaseName) = intHeadID
                    objTransactionHead._Trnheadcode = mstrCode & " INSTALLMENT"
                    objTransactionHead._Trnheadname = mstrName & " INSTALLMENT"
                    objTransactionHead._Trnheadtype_Id = enTranHeadType.DeductionForEmployee
                    objTransactionHead._Isappearonpayslip = mblnShowLoanBalOnPayslip
                    objTransactionHead._Istaxable = False

                    objTransactionHead._Userunkid = mintUserunkid


                    If objTransactionHead.Update(mstrDatabaseName, enCalcType.LOAN_INSTALLMENT, ConfigParameter._Object._CurrentDateAndTime) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        If objTransactionHead._Message <> "" Then
                            eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                        End If
                        Return False
                    End If
                End If

                'LOAN BALANCE AMOUNT HEAD
                intHeadID = 0
                objTransactionHead = New clsTransactionHead
                If objTransactionHead.isExistLoanSchemeHead(enCalcType.LOAN_BALANCE, mintLoanschemeunkid, intHeadID, objDataOperation) = False Then
                    objTransactionHead._Trnheadcode = mstrCode & " BALANCE"
                    objTransactionHead._Trnheadname = mstrName & " BALANCE"
                    objTransactionHead._Trnheadtype_Id = enTranHeadType.Informational
                    objTransactionHead._Typeof_id = enTypeOf.LOAN
                    objTransactionHead._Calctype_Id = enCalcType.LOAN_BALANCE
                    objTransactionHead._Isrecurrent = True
                    objTransactionHead._Isappearonpayslip = mblnShowLoanBalOnPayslip
                    objTransactionHead._Computeon_Id = 0
                    objTransactionHead._Formula = ""
                    objTransactionHead._Formulaid = ""

                    objTransactionHead._Userunkid = mintUserunkid

                    objTransactionHead._Istaxable = False
                    objTransactionHead._Istaxrelief = False
                    objTransactionHead._Iscumulative = False
                    objTransactionHead._Isnoncashbenefit = False
                    objTransactionHead._Ismonetary = True
                    objTransactionHead._Isactive = True
                    objTransactionHead._Activityunkid = 0
                    objTransactionHead._Isbasicsalaryasotherearning = False
                    objTransactionHead._CostCenterUnkId = 0
                    objTransactionHead._LoanSchemeunkid = mintLoanschemeunkid
                    If objTransactionHead.Insert(enCalcType.LOAN_BALANCE, ConfigParameter._Object._CurrentDateAndTime) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        If objTransactionHead._Message <> "" Then
                            eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                        End If
                        Return False
                    End If
                Else
                    objTransactionHead._Tranheadunkid(mstrDatabaseName) = intHeadID
                    objTransactionHead._Trnheadcode = mstrCode & " BALANCE"
                    objTransactionHead._Trnheadname = mstrName & " BALANCE"
                    objTransactionHead._Trnheadtype_Id = enTranHeadType.Informational
                    objTransactionHead._Isappearonpayslip = mblnShowLoanBalOnPayslip
                    objTransactionHead._Istaxable = False

                    objTransactionHead._Userunkid = mintUserunkid


                    If objTransactionHead.Update(mstrDatabaseName, enCalcType.LOAN_BALANCE, ConfigParameter._Object._CurrentDateAndTime) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        If objTransactionHead._Message <> "" Then
                            eZeeMsgBox.Show(objTransactionHead._Message, enMsgBoxStyle.Critical)
                        End If
                        Return False
                    End If
                End If
            End If
            'Hemant (27 Oct 2022) -- End

            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_scheme_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 

        Try
            'strQ = "DELETE FROM lnloan_scheme_master " & _
            '"WHERE loanschemeunkid = @loanschemeunkid "

            strQ = "UPDATE lnloan_scheme_master " & _
                   " SET isactive = 0 " & _
                   "WHERE loanschemeunkid = @loanschemeunkid "

            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_scheme_master", "loanschemeunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            If mintRefLoanSchemeUnkid <= 0 Then
                Dim intInterestLoanUnkId As Integer = GetLoanSchemeInterestID(mintLoanschemeunkid, objDataOperation)
                If intInterestLoanUnkId > 0 Then
                    objDataOperation.ClearParameters()

                    strQ = "UPDATE lnloan_scheme_master " & _
                          " SET isactive = 0 " & _
                          "WHERE loanschemeunkid = @loanschemeunkid "

                    objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intInterestLoanUnkId)

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lnloan_scheme_master", "loanschemeunkid", intInterestLoanUnkId) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If
            'Sohail (02 Apr 2018) -- End

            'Hemant (27 Oct 2022) -- Start
            'ENHANCEMENT(NMB) : AC2-990 - As a user, I want to pass the principal balance contained in flexcube loan view to Aruti payroll to display on employee salary slip as the loan balance for loans set to post to flexcube
            'LOAN INSTALLMENT AMOUNT HEAD
            Dim objTransactionHead As New clsTransactionHead
            Dim intHeadID As Integer = 0

            If objTransactionHead.isExistLoanSchemeHead(enCalcType.LOAN_INSTALLMENT, intUnkid, intHeadID, objDataOperation) = True Then
                If objTransactionHead.Void(mstrDatabaseName, intHeadID, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, Language.getMessage(mstrModuleName, 5, "Loan Scheme Deleted")) = False Then
                    objDataOperation.ReleaseTransaction(False)
                End If
            End If

            intHeadID = 0
            objTransactionHead = New clsTransactionHead
            If objTransactionHead.isExistLoanSchemeHead(enCalcType.LOAN_BALANCE, intUnkid, intHeadID, objDataOperation) = True Then
                If objTransactionHead.Void(mstrDatabaseName, intHeadID, mintUserunkid, ConfigParameter._Object._CurrentDateAndTime, Language.getMessage(mstrModuleName, 5, "Loan Scheme Deleted")) = False Then
                    objDataOperation.ReleaseTransaction(False)
                End If
            End If
            'LOAN OUTSTANDING BALANCE AMOUNT HEAD
            'Hemant (27 Oct 2022) -- End


            objDataOperation.ReleaseTransaction(True)
            'Anjan (12 Oct 2011)-End 

            Return True
        Catch ex As Exception
            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            objDataOperation.ReleaseTransaction(False)
            'Anjan (12 Oct 2011)-End 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsUsed As Boolean = False

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                    "TABLE_NAME AS TableName " & _
                    ",COLUMN_NAME " & _
                    "FROM INFORMATION_SCHEMA.COLUMNS " & _
                    "WHERE COLUMN_NAME IN ('loanschemeunkid') "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If dtRow.Item("TableName") = "lnloan_scheme_master" Then Continue For
                'Hemant (27 Oct 2022) -- Start
                'ENHANCEMENT(NMB) : AC2-990 - As a user, I want to pass the principal balance contained in flexcube loan view to Aruti payroll to display on employee salary slip as the loan balance for loans set to post to flexcube
                If dtRow.Item("TableName").ToString.ToUpper = "PRTRANHEAD_MASTER" Then Continue For 'Do Nothing as it is System Generated Head for Loan
                'Hemant (27 Oct 2022) -- End

                strQ = "SELECT " & dtRow.Item("COLUMN_NAME") & " FROM " & dtRow.Item("TableName") & " WHERE " & dtRow.Item("COLUMN_NAME") & " = @loanschemeunkid "

                dsList = objDataOperation.ExecQuery(strQ, "Used")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            'Sohail (14 Nov 2011) -- Start
            If blnIsUsed = False Then
                strQ = "SELECT  tranheadunkid " & _
                    "FROM    praccount_configuration " & _
                    "WHERE   isactive = 1 " & _
                            "AND transactiontype_Id = " & enJVTransactionType.LOAN & " " & _
                            "AND tranheadunkid = @loanschemeunkid " & _
                    "UNION " & _
                    "SELECT  tranheadunkid " & _
                    "FROM    praccount_configuration_employee " & _
                    "WHERE   isactive = 1 " & _
                            "AND transactiontype_Id = " & enJVTransactionType.LOAN & " " & _
                            "AND tranheadunkid = @loanschemeunkid " & _
                    "UNION " & _
                    "SELECT  tranheadunkid " & _
                    "FROM    praccount_configuration_costcenter " & _
                    "WHERE   isactive = 1 " & _
                            "AND transactiontype_Id = " & enJVTransactionType.LOAN & " " & _
                            "AND tranheadunkid = @loanschemeunkid "

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                blnIsUsed = dsList.Tables("List").Rows.Count > 0
            End If
            'Sohail (14 Nov 2011) -- End

            mstrMessage = ""
            Return blnIsUsed

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByRef intId As Integer = 0) As Boolean
        'Sohail (02 Aug 2017) - [intId]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            intId = 0 'Sohail (02 Aug 2017)

            'Pinkal (14-Apr-2015) -- Start
            'Enhancement - WORKING ON REDESIGNING LOAN MODULE.

            'strQ = "SELECT " & _
            '   "  loanschemeunkid " & _
            '   ", code " & _
            '   ", name " & _
            '   ", description " & _
            '   ", minnetsalary " & _
            '   ", isactive " & _
            ' "FROM lnloan_scheme_master " & _
            '  "WHERE 1 = 1"

            strQ = "SELECT " & _
                       "  loanschemeunkid " & _
                       ", code " & _
                       ", name " & _
                       ", description " & _
                       ", minnetsalary " & _
                       ", isactive " & _
                       ", isshowoness " & _
                       ", isshowloanbalonpayslip " & _
                       ", ISNULL(maxloanamount,0) AS maxloanamount " & _
                       ", ISNULL(loancalctype_id,0) AS loancalctype_id " & _
                       ", ISNULL(interest_rate,0) AS interest_rate " & _
                       ", ISNULL(interest_calctype_id,0) AS interest_calctype_id " & _
                       ", ISNULL(emi_netpaypercentage,0) AS emi_netpaypercentage " & _
                       ", ISNULL(maxnoofinstallment,0) AS maxnoofinstallment " & _
                       ", ISNULL(loanschemecategory_id,0) AS loanschemecategory_id " & _
                       ", ISNULL(repaymentdays,0) AS repaymentdays " & _
                       ", ISNULL(insurance_rate,0) AS insurance_rate " & _
                       ", ISNULL(maxloanamountcalctype_id,0) AS maxloanamountcalctype_id " & _
                       ", ISNULL(isattachmentrequired,0) AS isattachmentrequired " & _
                       ", ISNULL(documenttypeids,'') AS documenttypeids " & _
                       ", ISNULL(reportingtoapproval,0) AS reportingtoapproval " & _
                       ", ISNULL(minloanamount,0) AS minloanamount " & _
                       ", ISNULL(isskipapproval,0) AS isskipapproval " & _
                       ", ISNULL(ispostingtoflexcube,0) AS ispostingtoflexcube " & _
                       ", ISNULL(iseligiblefortopup,0) AS iseligiblefortopup " & _
                       ", ISNULL(minnoofinstallmentpaid, 0) AS minnoofinstallmentpaid " & _
                       ", ISNULL(lnapproval_dailyreminder, 0) AS lnapproval_dailyreminder " & _
                       ", ISNULL(escalation_days, 0) AS escalation_days " & _
                       ", ISNULL(escalatedtoempids, '') AS escalatedtoempids " & _
                       ", ISNULL(minnoofinstallment, 0) AS minnoofinstallment " & _
                       ", ISNULL(noofdaysbeferetitleexpiry, 0) AS noofdaysbeferetitleexpiry " & _
                       ", ISNULL(titleexpiryempids, '') AS titleexpiryempids " & _
                       ", ISNULL(loantranchedocumentids, '') AS loantranchedocumentids " & _
                       ", ISNULL(ccpercentageondsr, 0) AS ccpercentageondsr " & _
                       ", ISNULL(ccamtondsrfieldid, 0) AS ccamtondsrfieldid " & _
                       ", ISNULL(ccamtonexposurefieldid, 0) AS ccamtonexposurefieldid " & _
                       ", ISNULL(ismandatory_mktvalue, 0) AS ismandatory_mktvalue " & _
                       ", ISNULL(ismandatory_fsv, 0) AS ismandatory_fsv " & _
                       ", ISNULL(ismandatory_boq, 0) AS ismandatory_boq " & _
                       ", ISNULL(seniorapplevelunkid, 0) AS seniorapplevelunkid " & _
                       ", ISNULL(approverlevelalertunkids, '') AS approverlevelalertunkids " & _
                       ", ISNULL(isdisableemployeeconfirm,0) AS isdisableemployeeconfirm " & _
                       ", ISNULL(isfinalapprovalnotify,0) AS isfinalapprovalnotify " & _
                       ", ISNULL(finalapprovalnotifyemployeeids, '') AS finalapprovalnotifyemployeeids " & _
                     "FROM lnloan_scheme_master " & _
                      "WHERE 1 = 1"

            'Hemant (06 Dec 2024) -- [isfinalapprovalnotify,finalapprovalnotifyemployeeids]
            'Hemant (22 Nov 2024) -- [isdisableemployeeconfirm]
            'Pinkal (17-May-2024) -- NMB Enhancement For Mortgage Loan.[ISNULL(ismandatory_mktvalue, 0) AS ismandatory_mktvalue,ISNULL(ismandatory_fsv, 0) AS ismandatory_fsv, ISNULL(ismandatory_boq, 0) AS ismandatory_boq, ISNULL(ismortgage_attachementoptional, 0) AS ismortgage_attachementoptional,ISNULL(seniorapplevelunkid, 0) AS seniorapplevelunkid , ISNULL(approverlevelalertunkids, '') AS approverlevelalertunkids ]

            'Pinkal (11-Mar-2024) -- (A1X-2505) NMB - Credit card integratio.[ ", ISNULL(ccpercentageondsr, 0) AS ccpercentageondsr, ISNULL(ccamtondsrfieldid, 0) AS ccamtondsrfieldid,ISNULL(ccamtonexposurefieldid, 0) AS ccamtonexposurefieldid]

            'Pinkal (04-Aug-2023) -- (A1X-1164) NMB - Option to configure applicable document types for mortgage tranche requests.[", ISNULL(loantranchedocumentids, '') AS loantranchedocumentids " & _]

            'Pinkal (21-Jul-2023) -- (A1X-1104) NMB - Send notification to mortgage loanees and selected users x number of days before expiry of the title.[ ", ISNULL(titleexpiryempids, '') AS titleexpiryempids " & _]
            'Hemant (07 Jul 2023) -- [noofdaysbeferetitleexpiry]
            'Pinkal (23-Nov-2022) -- NMB Loan Module Enhancement.[", ISNULL(minnoofinstallment, 0) AS minnoofinstallment " & _]
            'Pinkal (10-Nov-2022) -- NMB Loan Module Enhancement.[  ", ISNULL(lnapproval_dailyreminder, 0) AS lnapproval_dailyreminder , ISNULL(escalation_days, 0) AS escalation_days, ISNULL(escalatedtoempids, '') AS escalatedtoempids ]

            'Hemant (12 Oct 2022) -- [isskipapproval,ispostingtoflexcube,iseligiblefortopup,minnoofinstallmentpaid]
 'Hemant (03 Oct 2022) -- [minloanamount]
            'Pinkal (20-Sep-2022) -- NMB Loan Module Enhancement.[", ISNULL(reportingtoapproval,0) AS reportingtoapproval " & _]

            'Hemant (24 Aug 2022) -- [,loanschemecategory_id,repaymentdays,insurance_rate,isattachmentrequired,documenttypeids]
            'Varsha (25 Nov 2017) -- [maxnoofinstallment]
            'Sohail (22 Sep 2017) - [emi_netpaypercentage]
            'Nilay (19-Oct-2016) -- [isshowloanbalonpayslip]
            'Pinkal (14-Apr-2015) -- End
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 52,maxloanamount} -- END
            'S.SANDEEP [20-SEP-2017] -- START {REF-ID # 50, loancalctype_id,interest_rate,interest_calctype_id} -- END

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If strName.Length > 0 Then
                strQ &= " AND name = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND loanschemeunkid <> @loanschemeunkid"
                objDataOperation.AddParameter("@loanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (02 Aug 2017) -- Start
            'TANAPA Enhancement - 69.1 - Import option on Employee Account Configuration.
            If dsList.Tables(0).Rows.Count > 0 Then
                intId = CInt(dsList.Tables(0).Rows(0).Item("loanschemeunkid"))
            End If
            'Sohail (02 Aug 2017) -- End

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    ''' <summary>
    '''  Modify By: Anjan
    ''' </summary>
    ''' <param name="blnNA"></param>
    ''' <param name="strListName"></param>
    ''' <param name="intLanguageID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getComboList(Optional ByVal blnNA As Boolean = False, _
                                    Optional ByVal strListName As String = "List", _
                                      Optional ByVal intLanguageID As Integer = -1, _
                                                Optional ByVal strFilter As String = "", _
                                                Optional ByVal blnSchemeshowOnEss As Boolean = False, _
                                                Optional ByVal blnAddLoanInterestScheme As Boolean = False, _
                                 Optional ByVal strOrderByQuery As String = "") As DataSet
        'Hemant (23 May 2019) -- [strOrderByQuery] 
        'Sohail (02 Apr 2018) - [blnAddLoanInterestScheme]
        'Sohail (03 Feb 2016) - [strFilter]

        'Pinkal (07-Dec-2017) --  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application[Optional ByVal blnSchemeshowOnEss As Boolean = False]

        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation

        Try

            If blnNA Then
                strQ = "SELECT 0 AS loanschemeunkid " & _
                          ",  ' ' + @Select AS name " & _
                          ",  ' ' as Code " & _
                       "UNION "

                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            End If

            strQ &= "SELECT loanschemeunkid " & _
                        ", name AS name " & _
                        ", code as Code " & _
                     "FROM lnloan_scheme_master " & _
                     "WHERE isactive = 1 "




            'Pinkal (07-Dec-2017) -- Start
            'Bug -  issue # 0001719: Loan Schemes That have not been set to appear on ESS still showing under loan application.
            If blnSchemeshowOnEss Then
                strQ &= " AND isshowoness = 1"
            End If

            'Pinkal (07-Dec-2017) -- End

            'Sohail (02 Apr 2018) -- Start
            'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
            If blnAddLoanInterestScheme = False Then
                strQ &= " AND ISNULL(lnloan_scheme_master.refloanschemeunkid, 0) <= 0 "
            End If
            'Sohail (02 Apr 2018) -- End

            'Sohail (03 Feb 2016) -- Start
            'Enhancement - Show Loans and Savings in Separate Columns option in Payroll Report.
            'strQ &= "ORDER BY  name "
            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            'Hemant (23 May 2019) -- Start
            'Support Issue Id # 0003829(KCMU) : Allow the payroll transaction heads to be arranged due to Transaction head code on payroll reports.
            'strQ &= " ORDER BY  name "
            If strOrderByQuery.Trim <> "" Then
                strQ &= strOrderByQuery
            Else
            strQ &= " ORDER BY  name "
            End If
            'Hemant (23 May 2019) -- End
            'Sohail (03 Feb 2016) -- End


            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    'Sohail (14 Nov 2011) -- Start
    Public Function GetLoanSchemeIDUsedInJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_employee" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                strTable1 = "praccount_configuration_costcenter" 'Sohail (25 Jul 2020)

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.LOAN)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanSchemeIDUsedInJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function

    Public Function GetLoanSchemeIDUsedInOtherJV(ByVal enAccConfigType As enAccountConfigType, ByVal dtAsOnDate As Date) As String
        'Sohail (25 Jul 2020) - [dtAsOnDate]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strIDs As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees..
            If dtAsOnDate = Nothing Then dtAsOnDate = DateTime.Now
            Dim strTable1 As String = ""
            Dim strTable2 As String = ""
            'Sohail (25 Jul 2020) -- End

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            'Dim strFilter As String = "WHERE isactive=1 " & _
            '                          "AND transactiontype_Id = @transactiontype_Id "
            Dim strFilter As String = "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "

            strQ = "SELECT DISTINCT A.tranheadunkid FROM ( "
            'Sohail (25 Jul 2020) -- End

            If enAccConfigType = enAccountConfigType.COMPANY_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration_costcenter"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.EMPLOYEE_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_costcenter"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_costcenter.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_costcenter.transactiontype_Id, praccount_configuration_costcenter.tranheadunkid, allocationbyid, praccount_configuration_costcenter.costcenterunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_costcenter " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_costcenter.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            ElseIf enAccConfigType = enAccountConfigType.COST_CENTER_ACCOUNT_CONFIGURATION Then

                'Sohail (25 Jul 2020) -- Start
                'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
                strTable1 = "praccount_configuration"
                strTable2 = "praccount_configuration_employee"
                'Sohail (25 Jul 2020) -- End

                strQ &= "SELECT DISTINCT praccount_configuration.tranheadunkid " & _
                        ", ISNULL(" & strTable1 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration.transactiontype_Id, praccount_configuration.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable1 & ".isactive = 1 " & _
                        "AND " & strTable1 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter & _
                        "UNION " & _
                        "SELECT DISTINCT praccount_configuration_employee.tranheadunkid " & _
                        ", ISNULL(" & strTable2 & ".isinactive, 0) AS isinactive " & _
                        ", DENSE_RANK() OVER (PARTITION BY praccount_configuration_employee.transactiontype_Id, praccount_configuration_employee.tranheadunkid, praccount_configuration_employee.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC) AS ROWNO " & _
                        "FROM praccount_configuration_employee " & _
                        "LEFT JOIN cfcommon_period_tran ON praccount_configuration_employee.periodunkid = cfcommon_period_tran.periodunkid " & _
                        "WHERE " & strTable2 & ".isactive = 1 " & _
                        "AND " & strTable2 & ".transactiontype_Id = @transactiontype_Id " & _
                        strFilter
                'Sohail (25 Jul 2020) - [LEFT JOIN cfcommon_period_tran, ROWNO]

            End If

            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            strQ &= " ) AS A " & _
                    " WHERE A.ROWNO = 1 " & _
                    "AND ISNULL(A.isinactive, 0) = 0 "
            'Sohail (25 Jul 2020) -- End

            objDataOperation.AddParameter("@transactiontype_Id", SqlDbType.Int, eZeeDataType.INT_SIZE, enJVTransactionType.LOAN)
            'Sohail (25 Jul 2020) -- Start
            'NMB Issue # : JV is not picking employee bank account when net pay head is mapped as default for all non mapped employees.
            objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            'Sohail (25 Jul 2020) -- End

            dsList = objDataOperation.ExecQuery(strQ, "IDList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("IDList").Rows
                strIDs &= ", " & dtRow(0).ToString
            Next

            If strIDs.Length > 0 Then
                strIDs = Mid(strIDs, 3)
            Else
                strIDs = "-999"
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanSchemeIDUsedInOtherJV; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return strIDs
    End Function
    'Sohail (14 Nov 2011) -- End


    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLoanSchemeUnkid(ByVal mstrName As String, ByVal blnIsFlexcube As Boolean) As Integer
        'Hemant (10 Nov 2022) -- [blnIsFlexcube]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = " SELECT " & _
                      "  loanschemeunkid " & _
                      "  FROM lnloan_scheme_master " & _
                      " WHERE name = @name  AND isactive = 1 "

            'Hemant (10 Nov 2022) -- Start
            If blnIsFlexcube = True Then
                strQ &= " AND ispostingtoflexcube = 1 "
            Else
                strQ &= " AND ispostingtoflexcube = 0 "
            End If
            'Hemant (10 Nov 2022) -- End           

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("loanschemeunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanSchemeUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 29 DEC 2011 ] -- END

    'Sohail (02 Apr 2018) -- Start
    'CCK Enhancement : Ref. No. 184 - Loan interest to be seperated from loan EMI on JV since client is posting loan EMI and interest to different accounts in 71.1.
    Public Function GetLoanSchemeInterestID(ByVal intLoanSchemeUnkid As Integer, Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim intUnkId As Integer = -1
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                "  loanschemeunkid " & _
                " FROM lnloan_scheme_master " & _
                " WHERE isactive = 1 " & _
                " AND refloanschemeunkid = @refloanschemeunkid"

            objDataOperation.AddParameter("@refloanschemeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLoanSchemeUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0).Item("loanschemeunkid").ToString)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLoanSchemeInterestID; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return intUnkId
    End Function
    'Sohail (02 Apr 2018) -- End



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Loan Scheme is already defined. Please define new Loan Scheme.")
			Language.setMessage(mstrModuleName, 2, "This Code is already defined. Please define new Code.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Interest")
			Language.setMessage(mstrModuleName, 5, "Loan Scheme Deleted")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
