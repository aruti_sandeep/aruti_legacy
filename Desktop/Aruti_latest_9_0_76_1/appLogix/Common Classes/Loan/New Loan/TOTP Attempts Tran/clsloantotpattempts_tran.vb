﻿'************************************************************************************************************************************
'Class Name : clsloantotpattempts_tran.vb
'Purpose    :
'Date       :14-Jun-2023
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsloantotpattempts_tran
    Private Const mstrModuleName = "clsloantotpattempts_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAttemptsunkid As Integer
    Private mdtAttemptdate As Date
    Private mintAttempts As Integer
    Private mdtOtpretrivaltime As Date
    Private mintUserunkid As Integer
    Private mintloginemployeeunkid As Integer
    Private mstrIp As String = String.Empty
    Private mstrMachine_Name As String = String.Empty
    Private mstrFormName As String = String.Empty
    Private mblnIsweb As Boolean
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set attemptsunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Attemptsunkid() As Integer
        Get
            Return mintAttemptsunkid
        End Get
        Set(ByVal value As Integer)
            mintAttemptsunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set attemptdate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Attemptdate() As Date
        Get
            Return mdtAttemptdate
        End Get
        Set(ByVal value As Date)
            mdtAttemptdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set attempts
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Attempts() As Integer
        Get
            Return mintAttempts
        End Get
        Set(ByVal value As Integer)
            mintAttempts = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set otpretrivaltime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Otpretrivaltime() As Date
        Get
            Return mdtOtpretrivaltime
        End Get
        Set(ByVal value As Date)
            mdtOtpretrivaltime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _loginemployeeunkid() As Integer
        Get
            Return mintloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintloginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set machine_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Machine_Name() As String
        Get
            Return mstrMachine_Name
        End Get
        Set(ByVal value As String)
            mstrMachine_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  attemptsunkid " & _
                      ", attemptdate " & _
                      ", attempts " & _
                      ", otpretrivaltime " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb " & _
                     " FROM lnloantotpattempts_tran " & _
                     " WHERE attemptsunkid = @attemptsunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@attemptsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAttemptsunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAttemptsunkid = CInt(dtRow.Item("attemptsunkid"))
                mdtAttemptdate = dtRow.Item("attemptdate")
                mintAttempts = CInt(dtRow.Item("attempts"))

                If IsDBNull(mdtOtpretrivaltime) = False AndAlso mdtOtpretrivaltime <> Nothing Then
                    mdtOtpretrivaltime = dtRow.Item("otpretrivaltime")
                End If

                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintloginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mstrIp = dtRow.Item("ip").ToString
                mstrMachine_Name = dtRow.Item("machine_name").ToString
                mstrFormName = dtRow.Item("form_name").ToString
                mblnIsweb = CBool(dtRow.Item("isweb"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xAttemptDate As Date, Optional ByVal xUserId As Integer = 0, Optional ByVal xEmployeeId As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  attemptsunkid " & _
                      ", attemptdate " & _
                      ", attempts " & _
                      ", otpretrivaltime " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb " & _
                      " FROM lnloantotpattempts_tran " & _
                      " WHERE CONVERT(CHAR(8),attemptdate,112) = @attemptdate "

            If xEmployeeId > 0 Then
                strQ &= " AND loginemployeeunkid = @loginemployeeunkid "
            End If

            If xUserId > 0 Then
                strQ &= " AND userunkid = @userunkid "
            End If

            strQ &= " ORDER BY attemptdate DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xUserId)
            objDataOperation.AddParameter("@attemptdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xAttemptDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloantotpattempts_tran) </purpose>
    Public Function Insert() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@attemptdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAttemptdate.ToString)
            objDataOperation.AddParameter("@attempts", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAttempts.ToString)

            If IsDBNull(mdtOtpretrivaltime) = False AndAlso mdtOtpretrivaltime <> Nothing Then
                objDataOperation.AddParameter("@otpretrivaltime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtOtpretrivaltime.ToString)
            Else
                objDataOperation.AddParameter("@otpretrivaltime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachine_Name.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

            strQ = "INSERT INTO lnloantotpattempts_tran ( " & _
                      "  attemptdate " & _
                      ", attempts " & _
                      ", otpretrivaltime " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb" & _
                    ") VALUES (" & _
                      "  @attemptdate " & _
                      ", @attempts " & _
                      ", @otpretrivaltime " & _
                      ", @userunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @isweb" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAttemptsunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloantotpattempts_tran) </purpose>
    Public Function Update() As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@attemptsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAttemptsunkid.ToString)
            objDataOperation.AddParameter("@attemptdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAttemptdate.ToString)
            objDataOperation.AddParameter("@attempts", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAttempts.ToString)

            If IsDBNull(mdtOtpretrivaltime) = False AndAlso mdtOtpretrivaltime <> Nothing Then
                objDataOperation.AddParameter("@otpretrivaltime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtOtpretrivaltime.ToString)
            Else
                objDataOperation.AddParameter("@otpretrivaltime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachine_Name.ToString)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

            strQ = "UPDATE lnloantotpattempts_tran SET " & _
                      "  attemptdate = @attemptdate" & _
                      ", attempts = @attempts" & _
                      ", otpretrivaltime = @otpretrivaltime" & _
                      ", userunkid = @userunkid" & _
                      ", loginemployeeunkid = @loginemployeeunkid " & _
                      ", ip = @ip" & _
                      ", machine_name = @machine_name" & _
                      ", form_name = @form_name " & _
                      ", isweb = @isweb " & _
                      " WHERE attemptsunkid = @attemptsunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloantotpattempts_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = "<Message>"
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM lnloantotpattempts_tran " & _
            "WHERE attemptsunkid = @attemptsunkid "

            objDataOperation.AddParameter("@attemptsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@attemptsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xEmployeeId As Integer, ByVal xAttemptDate As Date, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  attemptsunkid " & _
                      ", attemptdate " & _
                      ", attempts " & _
                      ", otpretrivaltime " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb " & _
                      " FROM lnloantotpattempts_tran " & _
                      " WHERE employeeunkid= @employeeunkid " & _
                      " AND CONVERT(char(8),attemptdate,112) = @attemptdate "

            If intUnkid > 0 Then
                strQ &= " AND attemptsunkid <> @attemptsunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@attemptdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, xAttemptDate)
            objDataOperation.AddParameter("@attemptsunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
End Class
