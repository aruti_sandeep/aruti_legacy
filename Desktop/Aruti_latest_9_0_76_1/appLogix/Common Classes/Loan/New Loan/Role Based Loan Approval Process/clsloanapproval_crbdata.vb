﻿'************************************************************************************************************************************
'Class Name : clsloanapproval_crbdata.vb
'Purpose    :
'Date       :08-Dec-2022
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsloanapproval_crbdata
    Private Const mstrModuleName = "clsloanapproval_crbdata"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLnloancrbunkid As Integer
    Private mintPendingloanapprovalunkid As Integer
    Private mintProcesspendingloanunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintMapuserunkid As Integer
    Private mblnIvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mintAudittype As Integer
    Private mdtAuditdatetime As Date
    Private mstrForm_Name As String = String.Empty
    Private mstrIp As String = String.Empty
    Private mstrHost As String = String.Empty
    Private mblnIsweb As Boolean
    Private mdtCRBData As DataTable = Nothing
#End Region


#Region "Constructor"

    Public Sub New()
        mdtCRBData = New DataTable("CRBData")
        mdtCRBData.Columns.Add("lnloancrbunkid", Type.GetType("System.Int32"))
        mdtCRBData.Columns.Add("pendingloanapprovalunkid", Type.GetType("System.Int32"))
        mdtCRBData.Columns.Add("processpendingloanunkid", Type.GetType("System.Int32"))
        mdtCRBData.Columns.Add("employeeunkid", Type.GetType("System.Int32"))
        mdtCRBData.Columns.Add("mapuserunkid", Type.GetType("System.Int32"))
        mdtCRBData.Columns.Add("idtypeunkid", Type.GetType("System.Int32"))
        mdtCRBData.Columns.Add("crb_posteddata", Type.GetType("System.String"))
        mdtCRBData.Columns.Add("crb_responsedata", Type.GetType("System.String"))
        mdtCRBData.Columns.Add("iscrb_error", Type.GetType("System.Boolean"))
        mdtCRBData.Columns.Add("crb_status", Type.GetType("System.String"))
        mdtCRBData.Columns.Add("isvoid", Type.GetType("System.Boolean"))
        mdtCRBData.Columns.Add("voiddatetime", Type.GetType("System.DateTime"))
        mdtCRBData.Columns.Add("voidreason", Type.GetType("System.String"))
        mdtCRBData.Columns.Add("voiduserunkid", Type.GetType("System.Int32"))
    End Sub

#End Region


#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lnloancrbunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Lnloancrbunkid() As Integer
        Get
            Return mintLnloancrbunkid
        End Get
        Set(ByVal value As Integer)
            mintLnloancrbunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pendingloanapprovalunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Pendingloanapprovalunkid() As Integer
        Get
            Return mintPendingloanapprovalunkid
        End Get
        Set(ByVal value As Integer)
            mintPendingloanapprovalunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processpendingloanunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Processpendingloanunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintProcesspendingloanunkid
        End Get
        Set(ByVal value As Integer)
            mintProcesspendingloanunkid = value
            GetCRBDataList(mintProcesspendingloanunkid, 0, 0, objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _isvoid() As Boolean
        Get
            Return mblnIvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As DateTime
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As DateTime)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set audittype
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Audittype() As Integer
        Get
            Return mintAudittype
        End Get
        Set(ByVal value As Integer)
            mintAudittype = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set auditdatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Auditdatetime() As Date
        Get
            Return mdtAuditdatetime
        End Get
        Set(ByVal value As Date)
            mdtAuditdatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set form_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set host
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DtCRBData() As DataTable
        Get
            Return mdtCRBData
        End Get
        Set(ByVal value As DataTable)
            mdtCRBData = value
        End Set
    End Property

#End Region


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetCRBDataList(ByVal xLoanApplicationId As Integer, Optional ByVal xLoanApprovalTranId As Integer = 0, Optional ByVal xEmployeeId As Integer = 0, Optional ByVal objDoOperation As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If


        Try
            strQ = "SELECT " & _
                      "  lnloancrbunkid " & _
                      ", pendingloanapprovalunkid " & _
                      ", processpendingloanunkid " & _
                      ", employeeunkid " & _
                      ", mapuserunkid " & _
                      ", idtypeunkid " & _
                      ", crb_posteddata " & _
                      ", crb_responsedata " & _
                      ", iscrb_error " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      ", audittype " & _
                      ", auditdatetime " & _
                      ", form_name " & _
                      ", ip " & _
                      ", host " & _
                      ", isweb " & _
                      " FROM lnloanapproval_crbdata " & _
                      " WHERE isvoid = 0 " & _
                      " AND processpendingloanunkid = @processpendingloanunkid "

            If xLoanApprovalTranId > 0 Then
                strQ &= " AND pendingloanapprovalunkid = @pendingloanapprovalunkid"
            End If

            If xEmployeeId > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLoanApplicationId)
            objDataOperation.AddParameter("@pendingloanapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLoanApprovalTranId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtCRBData = dsList.Tables(0).Copy()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloanapproval_crbdata) </purpose>
    Public Function Insert(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If mdtCRBData Is Nothing OrElse mdtCRBData.Rows.Count <= 0 Then Return True

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try

            strQ = "INSERT INTO lnloanapproval_crbdata ( " & _
                   "  pendingloanapprovalunkid " & _
                   ", processpendingloanunkid " & _
                   ", employeeunkid " & _
                   ", mapuserunkid " & _
                   ", idtypeunkid " & _
                   ", crb_posteddata " & _
                   ", crb_responsedata " & _
                   ", iscrb_error " & _
                   ", isvoid " & _
                   ", voiddatetime " & _
                   ", voiduserunkid " & _
                   ", voidreason " & _
                   ", audittype " & _
                   ", auditdatetime " & _
                   ", form_name " & _
                   ", ip " & _
                   ", host " & _
                   ", isweb" & _
                 ") VALUES (" & _
                   "  @pendingloanapprovalunkid " & _
                   ", @processpendingloanunkid " & _
                   ", @employeeunkid " & _
                   ", @mapuserunkid " & _
                   ", @idtypeunkid " & _
                   ", @crb_posteddata " & _
                   ", @crb_responsedata " & _
                   ", @iscrb_error " & _
                   ", @isvoid " & _
                   ", @voiddatetime " & _
                   ", @voiduserunkid " & _
                   ", @voidreason " & _
                   ", @audittype " & _
                   ", GetDate() " & _
                   ", @form_name " & _
                   ", @ip " & _
                   ", @host " & _
                   ", @isweb" & _
                 "); SELECT @@identity"


            For Each dr As DataRow In mdtCRBData.Rows

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@pendingloanapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("pendingloanapprovalunkid")))
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("processpendingloanunkid")))
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("mapuserunkid")))
                objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("idtypeunkid")))
                objDataOperation.AddParameter("@crb_posteddata", SqlDbType.NVarChar, dr("crb_posteddata").ToString().Trim.Length, dr("crb_posteddata").ToString().Trim)
                objDataOperation.AddParameter("@crb_responsedata", SqlDbType.NVarChar, dr("crb_responsedata").ToString().Trim.Length, dr("crb_responsedata").ToString().Trim)
                objDataOperation.AddParameter("@iscrb_error", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(dr("iscrb_error")))
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIvoid.ToString)

                If mdtVoiddatetime <> Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If

                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
                objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next
        
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloanapproval_crbdata) </purpose>
    Public Function Update(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mdtCRBData Is Nothing OrElse mdtCRBData.Rows.Count <= 0 Then Return True

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDoOperation
        End If

        Try

            strQ = "UPDATE lnloanapproval_crbdata SET " & _
                   "  pendingloanapprovalunkid = @pendingloanapprovalunkid" & _
                   ", processpendingloanunkid = @processpendingloanunkid" & _
                   ", employeeunkid = @employeeunkid" & _
                   ", mapuserunkid = @mapuserunkid" & _
                   ", idtypeunkid = @idtypeunkid" & _
                   ", crb_posteddata = @crb_posteddata" & _
                   ", crb_responsedata = @crb_responsedata" & _
                   ", iscrb_error = @iscrb_error" & _
                   ", isvoid = @isvoid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voidreason = @voidreason" & _
                   ", audittype = @audittype" & _
                   ", auditdatetime = GetDate()" & _
                   ", form_name = @form_name" & _
                   ", ip = @ip" & _
                   ", host = @host" & _
                   ", isweb = @isweb " & _
                   " WHERE lnloancrbunkid = @lnloancrbunkid "

            For Each dr As DataRow In mdtCRBData.Rows

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@lnloancrbunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("lnloancrbunkid")))
                objDataOperation.AddParameter("@pendingloanapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("pendingloanapprovalunkid")))
                objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("processpendingloanunkid")))
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("employeeunkid")))
                objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("mapuserunkid")))
                objDataOperation.AddParameter("@idtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("idtypeunkid")))
                objDataOperation.AddParameter("@crb_posteddata", SqlDbType.NVarChar, dr("crb_posteddata").ToString().Trim.Length, dr("crb_posteddata").ToString().Trim)
                objDataOperation.AddParameter("@crb_responsedata", SqlDbType.NVarChar, dr("crb_responsedata").ToString().Trim.Length, dr("crb_responsedata").ToString().Trim)
                objDataOperation.AddParameter("@iscrb_error", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(dr("iscrb_error")))
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIvoid.ToString)

                If mdtVoiddatetime <> Nothing Then
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
                Else
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                End If

                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAudittype.ToString)
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrForm_Name.ToString)
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrIp.ToString)
                objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHost.ToString)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next


            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
        End Try
    End Function


End Class