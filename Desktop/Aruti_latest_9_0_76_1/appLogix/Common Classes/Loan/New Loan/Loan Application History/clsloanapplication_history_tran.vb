﻿'************************************************************************************************************************************
'Class Name : clsloanapplication_history_tran.vb
'Purpose    :
'Date       : 25-Nov-2022
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib

Public Class clsloanapplication_history_tran
    Private Shared ReadOnly mstrModuleName As String = "clsloanapplication_history_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintLoanHistoryTranunkid As Integer
    Private mintProcesspendingloanunkid As Integer
    Private mintPendingloanaprovalunkid As Integer
    Private mintAtEmployeeunkid As Integer
    Private mintTransferunkid As Integer
    Private mintCategorizationTranunkid As Integer
    Private mblnIsApplicant As Boolean = False
    Private mblnIsCentralized As Boolean = False
    Private mblnIsFinalApprover As Boolean = False
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidreason As String = String.Empty
    Private mstrFormName As String = ""
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mblnIsFromWeb As Boolean = False
    Private mintAuditUserId As Integer = 0

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processpendingloanunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _LoanHistoryTranunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        Get
            Return mintLoanHistoryTranunkid
        End Get
        Set(ByVal value As Integer)
            mintLoanHistoryTranunkid = value
            Call GetData(objDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set processpendingloanunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Processpendingloanunkid() As Integer
        Get
            Return mintProcesspendingloanunkid
        End Get
        Set(ByVal value As Integer)
            mintProcesspendingloanunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pendingloanaprovalunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Pendingloanaprovalunkid() As Integer
        Get
            Return mintPendingloanaprovalunkid
        End Get
        Set(ByVal value As Integer)
            mintPendingloanaprovalunkid = value

        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _AtEmployeeunkid() As Integer
        Get
            Return mintAtEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintAtEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set transferunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Transferunkid() As Integer
        Get
            Return mintTransferunkid
        End Get
        Set(ByVal value As Integer)
            mintTransferunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set categorizationTranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CategorizationTranunkid() As Integer
        Get
            Return mintCategorizationTranunkid
        End Get
        Set(ByVal value As Integer)
            mintCategorizationTranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsApplicant
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsApplicant() As Boolean
        Get
            Return mblnIsApplicant
        End Get
        Set(ByVal value As Boolean)
            mblnIsApplicant = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsCentralized
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsCentralized() As Boolean
        Get
            Return mblnIsCentralized
        End Get
        Set(ByVal value As Boolean)
            mblnIsCentralized = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFinalApprover
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsFinalApprover() As Boolean
        Get
            Return mblnIsFinalApprover
        End Get
        Set(ByVal value As Boolean)
            mblnIsFinalApprover = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsFromWeb
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsFromWeb() As Boolean
        Get
            Return mblnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set AuditUserId
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _AuditUserId() As Integer
        Get
            Return mintAuditUserId
        End Get
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDataOp
        End If

        Try
            strQ = "SELECT " & _
                      "  loanhistorytranunkid " & _
                      ", processpendingloanunkid " & _
                      "  pendingloanaprovalunkid " & _
                      ", atemployeeunkid " & _
                      ", transferunkid " & _
                      ", categorizationtranunkid " & _
                      ", isapplicant " & _
                      ", iscentralized " & _
                      ", isfinalapprover " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      " FROM lnloan_history_tran " & _
                      " WHERE loanhistorytranunkid = @loanhistorytranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loanhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanHistoryTranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLoanHistoryTranunkid = CInt(dtRow.Item("loanhistorytranunkid"))
                mintProcesspendingloanunkid = CInt(dtRow.Item("processpendingloanunkid"))
                mintPendingloanaprovalunkid = CInt(dtRow.Item("pendingloanaprovalunkid"))
                mintAtEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintTransferunkid = CInt(dtRow.Item("transferunkid"))
                mintCategorizationTranunkid = CInt(dtRow.Item("categorizationtranunkid"))
                mblnIsApplicant = CBool(dtRow.Item("isapplicant"))
                mblnIsCentralized = CBool(dtRow.Item("iscentralized"))
                mblnIsFinalApprover = CBool(dtRow.Item("isfinalapprover"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidreason = dtRow.Item("voidreason").ToString

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Nilay
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                     "  loanhistorytranunkid " & _
                      ", processpendingloanunkid " & _
                      ", pendingloanaprovalunkid " & _
                      ", atemployeeunkid " & _
                      ", transferunkid " & _
                      ", categorizationtranunkid " & _
                      ", isapplicant " & _
                      ", iscentralized " & _
                      ", isfinalapprover " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      " FROM lnloan_history_tran " & _
                    " WHERE isvoid = 0 "

            
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lnloan_history_tran) </purpose>
    Public Function Insert(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@pendingloanaprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingloanaprovalunkid.ToString)
            objDataOperation.AddParameter("@atemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAtEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transferunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferunkid.ToString)
            objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategorizationTranunkid.ToString)
            objDataOperation.AddParameter("@isapplicant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsApplicant.ToString)
            objDataOperation.AddParameter("@iscentralized", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCentralized.ToString)
            objDataOperation.AddParameter("@isfinalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinalApprover.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, enAuditType.ADD)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromWeb)


            strQ = "INSERT INTO lnloan_history_tran ( " & _
                      "  processpendingloanunkid " & _
                      ", pendingloanaprovalunkid " & _
                      ", atemployeeunkid " & _
                      ", transferunkid " & _
                      ", categorizationtranunkid " & _
                      ", isapplicant " & _
                      ", iscentralized " & _
                      ", isfinalapprover " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason" & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", host" & _
                      ", form_name " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      "  @processpendingloanunkid " & _
                      ", @pendingloanaprovalunkid " & _
                      ", @atemployeeunkid " & _
                      ", @transferunkid " & _
                      ", @categorizationtranunkid " & _
                      ", @isapplicant " & _
                      ", @iscentralized " & _
                      ", @isfinalapprover " & _
                      ", @isvoid " & _
                      ", @voiddatetime " & _
                      ", @voiduserunkid " & _
                      ", @voidreason " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @host" & _
                      ", @form_name " & _
                      ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLoanHistoryTranunkid = dsList.Tables(0).Rows(0).Item(0)


            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lnloan_history_tran) </purpose>
    Public Function Update(Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If


        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loanhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoanHistoryTranunkid.ToString)
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProcesspendingloanunkid.ToString)
            objDataOperation.AddParameter("@pendingloanaprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPendingloanaprovalunkid.ToString)
            objDataOperation.AddParameter("@atemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAtEmployeeunkid.ToString)
            objDataOperation.AddParameter("@transferunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTransferunkid.ToString)
            objDataOperation.AddParameter("@categorizationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCategorizationTranunkid.ToString)
            objDataOperation.AddParameter("@isapplicant", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsApplicant.ToString)
            objDataOperation.AddParameter("@iscentralized", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsCentralized.ToString)
            objDataOperation.AddParameter("@isfinalapprover", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFinalApprover.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            strQ = "UPDATE lnloan_history_tran SET " & _
                      "  processpendingloanunkid = @processpendingloanunkid" & _
                      ", pendingloanaprovalunkid = @pendingloanaprovalunkid" & _
                      ", atemployeeunkid = @atemployeeunkid" & _
                      ", transferunkid = @transferunkid" & _
                      ", categorizationtranunkid = @categorizationtranunkid" & _
                      ", isapplicant = @isapplicant" & _
                      ", iscentralized = @iscentralized" & _
                      ", isfinalapprover = @isfinalapprover" & _
                      ", isvoid = @isvoid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidreason = @voidreason " & _
                      " WHERE loanhistorytranunkid = @loanhistorytranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lnloan_history_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If


        Try

            strQ = " UPDATE lnloan_history_tran SET " & _
                      " isvoid = @isvoid,voiduserunkid = @voiduserunkid " & _
                      ",voiddatetime = GetDate(),voidreason = @voidreason " & _
            "WHERE loanhistorytranunkid = @loanhistorytranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@loanhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Int, eZeeDataType.INT_SIZE, True)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function DeleteByProcessPendingLoanUnkId(ByVal intProcesspendingloanunkid As Integer, Optional ByVal objDOperation As clsDataOperation = Nothing, Optional ByVal blnIsApprovalData As Boolean = False, Optional ByVal intPendingLoanApprovalID As Integer = 0) As Boolean
        'Pinkal (21-Mar-2024) -- NMB - Mortgage UAT Enhancements.[Optional ByVal intPendingLoanApprovalID As Integer = 0]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try

            strQ = " UPDATE lnloan_history_tran SET " & _
                   "    isvoid = @isvoid " & _
                   ",   voiduserunkid = @voiduserunkid " & _
                   ",   voiddatetime = GetDate() " & _
                   ",   voidreason = @voidreason " & _
            "WHERE processpendingloanunkid = @processpendingloanunkid "

            If blnIsApprovalData = True Then
                strQ &= " AND pendingloanaprovalunkid > 0 "
            End If

            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            If intPendingLoanApprovalID > 0 Then
                strQ &= " AND pendingloanaprovalunkid  =  @pendingloanaprovalunkid "
            End If
            'Pinkal (21-Mar-2024) -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@processpendingloanunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intProcesspendingloanunkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Int, eZeeDataType.INT_SIZE, True)
            'Pinkal (21-Mar-2024) -- Start
            'NMB - Mortgage UAT Enhancements.
            objDataOperation.AddParameter("@pendingloanaprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPendingLoanApprovalID)
            'Pinkal (21-Mar-2024) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@loanhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  loanhistorytranunkid " & _
                      ", processpendingloanunkid " & _
                      "  pendingloanaprovalunkid " & _
                      ", atemployeeunkid " & _
                      ", transferunkid " & _
                      ", categorizationtranunkid " & _
                      ", isapplicant " & _
                      ", iscentralized " & _
                      ", isfinalapprover " & _
                      ", isvoid " & _
                      ", voiddatetime " & _
                      ", voiduserunkid " & _
                      ", voidreason " & _
                      " FROM lnloan_history_tran " & _
                      " WHERE isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND loanhistorytranunkid <> @loanhistorytranunkid"
            End If

            objDataOperation.AddParameter("@loanhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
End Class
