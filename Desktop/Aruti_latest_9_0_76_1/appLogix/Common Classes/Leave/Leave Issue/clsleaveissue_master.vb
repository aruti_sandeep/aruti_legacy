﻿'************************************************************************************************************************************
'Class Name : clsleaveissue_master.vb
'Purpose    :
'Date       :13/07/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsleaveissue_master
    Private Shared ReadOnly mstrModuleName As String = "clsleaveissue_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLeaveissueunkid As Integer
    Private mintLeaveyearunkid As Integer
    Private mintFormunkid As Integer
    Private mintLeavetypeunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As Date
    Private mintVoiduserunkid As Integer
    Private mstrVoidReason As String = ""
    Private objIssueLeave_tran As New clsleaveissue_Tran
    Private objAccrue As New clsleavebalance_tran
    Private mdclTotalIssue As Decimal = 0
    Private mdtStartdate As Date
    Private mdtEnddate As Date
    Private mdclTotalCancel As Decimal = 0
    Private mobjDataOperation As clsDataOperation = Nothing
    Private mstrWebFormName As String = ""
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mblnIsLeaveEncashment As Boolean = False
    Private mintClaimRequestMasterId As Integer = 0
    Private mdtExpense As DataTable = Nothing
    Private mblnPaymentApprovalwithLeaveApproval As Boolean = False

    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnSkipApproverFlow As Boolean = False
    'Pinkal (01-Oct-2018) -- End

    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
    Private mblnConsiderLeaveOnTnAPeriod As Boolean = False
    'Pinkal (01-Jan-2019) -- End

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leaveissueunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leaveissueunkid() As Integer
        Get
            Return mintLeaveissueunkid
        End Get
        Set(ByVal value As Integer)
            mintLeaveissueunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leaveyearunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leaveyearunkid() As Integer
        Get
            Return mintLeaveyearunkid
        End Get
        Set(ByVal value As Integer)
            mintLeaveyearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Formunkid() As Integer
        Get
            Return mintFormunkid
        End Get
        Set(ByVal value As Integer)
            mintFormunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavetypeunkid() As Integer
        Get
            Return mintLeavetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintLeavetypeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _VoidReason() As String
        Get
            Return mstrVoidReason
        End Get
        Set(ByVal value As String)
            mstrVoidReason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TotalIssue
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _TotalIssue() As Decimal
        Get
            Return mdclTotalIssue
        End Get
        Set(ByVal value As Decimal)
            mdclTotalIssue = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TotalCancelCount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _TotalCancel() As Decimal
        Get
            Return mdclTotalCancel
        End Get
        Set(ByVal value As Decimal)
            mdclTotalCancel = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set StartDate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _StartDate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set EndDate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _EndDate() As Date
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As Date)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mobjDataOperation
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ObjDataOperation() As clsDataOperation
        Get
            Return mobjDataOperation
        End Get
        Set(ByVal value As clsDataOperation)
            mobjDataOperation = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClaimRequestMasterId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClaimRequestMasterId() As Integer
        Get
            Return mintClaimRequestMasterId
        End Get
        Set(ByVal value As Integer)
            mintClaimRequestMasterId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtExpense
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _DtExpense() As DataTable
        Get
            Return mdtExpense
        End Get
        Set(ByVal value As DataTable)
            mdtExpense = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set _PaymentApprovalwithLeaveApproval
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _PaymentApprovalwithLeaveApproval() As Boolean
        Get
            Return mblnPaymentApprovalwithLeaveApproval
        End Get
        Set(ByVal value As Boolean)
            mblnPaymentApprovalwithLeaveApproval = value
        End Set
    End Property

    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Get or Set SkipApproverFlow
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SkipApproverFlow() As Boolean
        Get
            Return mblnSkipApproverFlow
        End Get
        Set(ByVal value As Boolean)
            mblnSkipApproverFlow = value
        End Set
    End Property

    'Pinkal (01-Oct-2018) -- End

    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.

    ''' <summary>
    ''' Purpose: Get or Set ConsiderLeaveOnTnAPeriod
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _ConsiderLeaveOnTnAPeriod() As Boolean
        Get
            Return mblnConsiderLeaveOnTnAPeriod
        End Get
        Set(ByVal value As Boolean)
            mblnConsiderLeaveOnTnAPeriod = value
        End Set
    End Property

    'Pinkal (01-Jan-2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If mobjDataOperation IsNot Nothing Then
            objDataOperation = mobjDataOperation
            objDataOperation.ClearParameters()
        End If

        Try

            strQ = "SELECT " & _
              "  leaveissueunkid " & _
              ", leaveyearunkid " & _
              ", formunkid " & _
              ", leavetypeunkid " & _
              ", employeeunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
             ", ISNULL(issuecount,0) AS Issuecount " & _
             ", startdate " & _
             ", enddate " & _
             ", ISNULL(cancelcount,0.00) as Cancelcount " & _
                          ", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _
             "FROM lvleaveIssue_master " & _
             "WHERE leaveissueunkid = @leaveissueunkid "

            'Pinkal (01-Jan-2019) -- 'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _]

            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLeaveissueunkid = CInt(dtRow.Item("leaveissueunkid"))
                mintLeaveyearunkid = CInt(dtRow.Item("leaveyearunkid"))
                mintFormunkid = CInt(dtRow.Item("formunkid"))
                mintLeavetypeunkid = CInt(dtRow.Item("leavetypeunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                If mdtVoiddatetime <> Nothing Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mstrVoidReason = dtRow.Item("voidreason").ToString
                mdclTotalIssue = CDbl(dtRow.Item("Issuecount"))
                mdclTotalCancel = CDbl(dtRow.Item("cancelcount"))
                If dtRow.Item("startdate") <> Nothing Then
                    mdtStartdate = CDate(dtRow.Item("startdate"))
                End If
                If dtRow.Item("enddate") <> Nothing Then
                    mdtEnddate = CDate(dtRow.Item("enddate"))
                End If

                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                mblnConsiderLeaveOnTnAPeriod = CBool(dtRow.Item("istnaperiodlinked"))
                'Pinkal (01-Jan-2019) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            ' objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
              "  leaveissueunkid " & _
              ", leaveyearunkid " & _
              ", formunkid " & _
              ", leavetypeunkid " & _
              ", employeeunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", voidreason " & _
              ", ISNULL(issuecount,0.00) As 'Issuecount' " & _
              ", ISNULL(cancelcount,0.00) As 'Cancelcount' " & _
              ", startdate " & _
              ", enddate " & _
                       ", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _
             "FROM lvleaveIssue_master "

            'Pinkal (01-Jan-2019) -- 'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _]

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    'Pinkal (24-Aug-2015) -- Start
    'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (lvleaveIssue_master) </purpose>
    'Public Function Insert(ByVal strLeavedate() As String, ByVal isPaid As Boolean, ByVal mdblLeaveAmout As Double, Optional ByVal intLeaveBalanceSetting As Integer = -1 _
    '                                , Optional ByVal blnLeaveApproverForLeaveType As String = "", Optional ByVal intApproverID As Integer = -1, Optional ByVal objdoOpeation As clsDataOperation = Nothing) As Boolean

    '    'Pinkal (01-Feb-2014) -- Start [ Optional ByVal objdoOpeation As clsDataOperation = Nothing ]


    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception


    '    'Pinkal (01-Feb-2014) -- Start
    '    'Enhancement : TRA Changes

    '    If objdoOpeation Is Nothing Then
    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()
    '    Else
    '        objDataOperation = objdoOpeation
    '    End If

    '    'Pinkal (01-Feb-2014) -- End

    '    Try
    '        objDataOperation.ClearParameters()
    '        objDataOperation.AddParameter("@leaveyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveyearunkid.ToString)
    '        objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
    '        objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
    '        objDataOperation.AddParameter("@issuecount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclTotalIssue.ToString)
    '        objDataOperation.AddParameter("@cancelcount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclTotalCancel.ToString)
    '        objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartdate <> Nothing, mdtStartdate, DBNull.Value))
    '        objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))

    '        strQ = "INSERT INTO lvleaveIssue_master ( " & _
    '                  "  leaveyearunkid " & _
    '                  ", formunkid " & _
    '                  ", leavetypeunkid " & _
    '                  ", employeeunkid " & _
    '                  ", userunkid " & _
    '                  ", isvoid " & _
    '                  ", voiddatetime " & _
    '                  ", voiduserunkid" & _
    '                  ", voidreason " & _
    '                  ", issuecount " & _
    '                  ", cancelcount " & _
    '                  ", startdate " & _
    '                  ", enddate " & _
    '                ") VALUES (" & _
    '                  "  @leaveyearunkid " & _
    '                  ", @formunkid " & _
    '                  ", @leavetypeunkid " & _
    '                  ", @employeeunkid " & _
    '                  ", @userunkid " & _
    '                  ", @isvoid " & _
    '                  ", @voiddatetime " & _
    '                  ", @voiduserunkid" & _
    '                  ", @voidreason " & _
    '                  ", @issuecount " & _
    '                  ", @cancelcount " & _
    '                  ", @startdate " & _
    '                  ", @enddate " & _
    '                "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintLeaveissueunkid = dsList.Tables(0).Rows(0).Item(0)
    '        objIssueLeave_tran._Leaveissueunkid = mintLeaveissueunkid
    '        objIssueLeave_tran._IsPaid = isPaid
    '        objIssueLeave_tran._TotalAccrue = mdblLeaveAmout
    '        objIssueLeave_tran._Userunkid = mintUserunkid
    '        If intLeaveBalanceSetting <= 0 Then
    '            intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
    '        End If
    '        If blnLeaveApproverForLeaveType.Trim = "" Then
    '            blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType
    '        End If
    '        objIssueLeave_tran._WebFormName = mstrWebFormName
    '        objIssueLeave_tran._WebClientIP = mstrWebClientIP
    '        objIssueLeave_tran._WebHostName = mstrWebHostName
    '        objIssueLeave_tran._Userunkid = mintUserunkid
    '        objIssueLeave_tran.InsertDelete(objDataOperation, strLeavedate, mintLeaveyearunkid, mintEmployeeunkid, mintLeavetypeunkid, intLeaveBalanceSetting, blnLeaveApproverForLeaveType, intApproverID)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Pinkal (06-Mar-2014) -- Start
    '        'Enhancement : TRA Changes

    '        If mintClaimRequestMasterId > 0 AndAlso mblnPaymentApprovalwithLeaveApproval Then
    '            Dim objclaimMst As New clsclaim_request_master
    '            Dim objExpenseMst As New clsExpense_Master
    '            objclaimMst._Crmasterunkid = mintClaimRequestMasterId
    '            If objclaimMst._Statusunkid = 2 Then
    '                Dim objExpenseApproverTran As New clsclaim_request_approval_tran
    '                objExpenseApproverTran._YearId = mintLeaveyearunkid
    '                objExpenseApproverTran._LeaveBalanceSetting = intLeaveBalanceSetting
    '                If objExpenseApproverTran.UpdateExpenseBalance(objDataOperation, mdtExpense, 1, mintUserunkid, mintClaimRequestMasterId, "") = False Then
    '                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '                    Throw exForce
    '                End If
    '                objExpenseApproverTran = Nothing
    '            End If
    '            objclaimMst = Nothing
    '        End If

    '        'Pinkal (06-Mar-2014) -- End

    '        If objdoOpeation Is Nothing Then
    '        objDataOperation.ReleaseTransaction(True)
    '        End If
    '        Return True

    '    Catch ex As Exception
    '        If objdoOpeation Is Nothing Then objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()

    '        'Pinkal (01-Feb-2014) -- Start
    '        'Enhancement : TRA Changes
    '        If objdoOpeation Is Nothing Then objDataOperation = Nothing
    '        'Pinkal (01-Feb-2014) -- End

    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (lvleaveIssue_master) </purpose>
    'Public Function Update(ByVal strLeavedate() As String, ByVal isPaid As Boolean, ByVal mdblLeaveAmout As Double, Optional ByVal intApproverID As Integer = -1) As Boolean    'Pinkal (15-Jul-2013) [Optional ByVal intApproverID As Integer = -1]) 

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try
    '        objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
    '        objDataOperation.AddParameter("@leaveyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveyearunkid.ToString)
    '        objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
    '        objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason)
    '        objDataOperation.AddParameter("@issuecount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclTotalIssue.ToString)
    '        objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartdate <> Nothing, mdtStartdate, DBNull.Value))
    '        objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))

    '        strQ = "UPDATE lvleaveIssue_master SET " & _
    '                  "  leaveyearunkid = @leaveyearunkid" & _
    '                  ", formunkid = @formunkid" & _
    '                  ", leavetypeunkid = @leavetypeunkid" & _
    '                  ", employeeunkid = @employeeunkid" & _
    '                  ", userunkid = @userunkid" & _
    '                  ", isvoid = @isvoid" & _
    '                  ", voiddatetime = @voiddatetime" & _
    '                  ", voiduserunkid = @voiduserunkid " & _
    '                  ", voidreason = @voidreason " & _
    '                 ", issuecount  = @issuecount " & _
    '                 ", startdate = @startdate " & _
    '                 ", enddate = @enddate " & _
    '                "WHERE leaveissueunkid = @leaveissueunkid "

    '        objDataOperation.ExecNonQuery(strQ)

    '        objIssueLeave_tran._Leaveissueunkid = mintLeaveissueunkid
    '        objIssueLeave_tran._IsPaid = isPaid
    '        objIssueLeave_tran._TotalAccrue = mdblLeaveAmout
    '        objIssueLeave_tran.InsertDelete(objDataOperation, strLeavedate, mintLeaveyearunkid, mintEmployeeunkid, mintLeavetypeunkid, -1, intApproverID)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If
    '        objDataOperation.ReleaseTransaction(True)
    '        Return True
    '    Catch ex As Exception
    '        objDataOperation.ReleaseTransaction(False)
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveIssue_master) </purpose>
    Public Function Insert(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                     , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                     , ByVal xIncludeIn_ActiveEmployee As Boolean, ByVal strLeavedate() As String, ByVal isPaid As Boolean, ByVal mdblLeaveAmout As Double _
                                     , Optional ByVal intLeaveBalanceSetting As Integer = -1, Optional ByVal blnLeaveApproverForLeaveType As String = "" _
                                     , Optional ByVal intApproverID As Integer = -1, Optional ByVal objdoOpeation As clsDataOperation = Nothing _
                                     , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                     , Optional ByVal mblnIsExternalApprover As Boolean = False _
                                     , Optional ByVal blnExemptFromPayroll As Boolean = False _
                                     , Optional ByVal blnSkipEmployeeMovementApprovalFlow As Boolean = True _
                                     , Optional ByVal blnCreateADUserFromEmpMst As Boolean = False _
                                     ) As Boolean
        'Sohail (21 Oct 2019) - [blnExemptFromPayroll, blnSkipEmployeeMovementApprovalFlow, blnCreateADUserFromEmpMst]


        'Pinkal (01-Mar-2016) -- Implementing External Approver in Claim Request & Leave Module.[Optional ByVal mblnIsExternalApprover As Boolean = False]

        'Pinkal (01-Mar-2016) -- End


        'Pinkal (01-Feb-2014) -- Start [ Optional ByVal objdoOpeation As clsDataOperation = Nothing ]


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        'Pinkal (21-Oct-2015) -- Start
        'Enhancement - WORKING ON DUPLICATE LEAVE FOR ISSUE. 

        mstrMessage = ""
        If isExist(mintEmployeeunkid, mintFormunkid, mintLeavetypeunkid, objdoOpeation) Then
            mstrMessage = Language.getMessage(mstrModuleName, 8, "You cannot perform this operation.Reason : This Leave form is already issued.")
            Return False
        End If

        'Pinkal (21-Oct-2015) -- End


        'Pinkal (01-Feb-2014) -- Start
        'Enhancement : TRA Changes

        If objdoOpeation Is Nothing Then
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objdoOpeation
        End If

        'Pinkal (01-Feb-2014) -- End

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leaveyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveyearunkid.ToString)
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")
            objDataOperation.AddParameter("@issuecount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclTotalIssue.ToString)
            objDataOperation.AddParameter("@cancelcount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclTotalCancel.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartdate <> Nothing, mdtStartdate, DBNull.Value))
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))

            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            objDataOperation.AddParameter("@istnaperiodlinked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnConsiderLeaveOnTnAPeriod)
            'Pinkal (01-Jan-2019) -- End


            strQ = "INSERT INTO lvleaveIssue_master ( " & _
              "  leaveyearunkid " & _
              ", formunkid " & _
              ", leavetypeunkid " & _
              ", employeeunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid" & _
              ", voidreason " & _
              ", issuecount " & _
              ", cancelcount " & _
              ", startdate " & _
              ", enddate " & _
                      ",istnaperiodlinked " & _
            ") VALUES (" & _
              "  @leaveyearunkid " & _
              ", @formunkid " & _
              ", @leavetypeunkid " & _
              ", @employeeunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiddatetime " & _
              ", @voiduserunkid" & _
              ", @voidreason " & _
              ", @issuecount " & _
              ", @cancelcount " & _
              ", @startdate " & _
              ", @enddate " & _
                      ", @istnaperiodlinked " & _
            "); SELECT @@identity"

            'Pinkal (01-Jan-2019) -- 'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", @istnaperiodlinked " & _]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLeaveissueunkid = dsList.Tables(0).Rows(0).Item(0)
            objIssueLeave_tran._Leaveissueunkid = mintLeaveissueunkid
            objIssueLeave_tran._IsPaid = isPaid
            objIssueLeave_tran._TotalAccrue = mdblLeaveAmout
            objIssueLeave_tran._Userunkid = mintUserunkid
            If intLeaveBalanceSetting <= 0 Then
                intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            End If
            If blnLeaveApproverForLeaveType.Trim = "" Then
                blnLeaveApproverForLeaveType = ConfigParameter._Object._IsLeaveApprover_ForLeaveType
            End If
            objIssueLeave_tran._WebFormName = mstrWebFormName
            objIssueLeave_tran._WebClientIP = mstrWebClientIP
            objIssueLeave_tran._WebHostName = mstrWebHostName
            objIssueLeave_tran._Userunkid = mintUserunkid


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objIssueLeave_tran._SkipApproverFlow = mblnSkipApproverFlow
            'Pinkal (01-Oct-2018) -- End
          
            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            objIssueLeave_tran._ConsiderLeaveOnTnAPeriod = mblnConsiderLeaveOnTnAPeriod

            'Pinkal (01-Jan-2019) -- End

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objIssueLeave_tran.InsertDelete(objDataOperation, strLeavedate, mintLeaveyearunkid, mintEmployeeunkid, mintLeavetypeunkid, intLeaveBalanceSetting, blnLeaveApproverForLeaveType, intApproverID)
            objIssueLeave_tran.InsertDelete(xDatabaseName, xUserUnkid, xCompanyUnkid, strEmployeeAsOnDate _
                                                         , xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, objDataOperation, strLeavedate _
                                                         , mintLeaveyearunkid, mintEmployeeunkid, mintLeavetypeunkid, intLeaveBalanceSetting _
                                                         , blnLeaveApproverForLeaveType, intApproverID, blnApplyUserAccessFilter, mblnIsExternalApprover)
            'Pinkal (24-Aug-2015) -- End



            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (06-Mar-2014) -- Start
            'Enhancement : TRA Changes

            If mintClaimRequestMasterId > 0 AndAlso mblnPaymentApprovalwithLeaveApproval Then
                Dim objclaimMst As New clsclaim_request_master
                Dim objExpenseMst As New clsExpense_Master
                'Pinkal (02-Dec-2015) -- Start
                'Enhancement - Solving Leave bug in Self Service.
                objclaimMst._DoOperation = objDataOperation
                'Pinkal (02-Dec-2015) -- End
                objclaimMst._Crmasterunkid = mintClaimRequestMasterId
                If objclaimMst._Statusunkid = 2 Then
                    Dim objExpenseApproverTran As New clsclaim_request_approval_tran
                    objExpenseApproverTran._YearId = mintLeaveyearunkid
                    objExpenseApproverTran._LeaveBalanceSetting = intLeaveBalanceSetting
                    If objExpenseApproverTran.UpdateExpenseBalance(objDataOperation, mdtExpense, 1, mintUserunkid, mintClaimRequestMasterId, "") = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objExpenseApproverTran = Nothing
                End If
                objclaimMst = Nothing
            End If

            'Pinkal (06-Mar-2014) -- End

            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : if Exempt from Payroll leave is issue then system will put one transaction in employee exemption without end date It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            If blnExemptFromPayroll = True Then
                Dim objEDates As New clsemployee_dates_tran
                If blnSkipEmployeeMovementApprovalFlow = True Then

                    objEDates._Effectivedate = mdtStartdate
                    objEDates._Employeeunkid = mintEmployeeunkid
                    objEDates._Date1 = mdtStartdate
                    objEDates._Date2 = Nothing
                    'If mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION Then
                    '    objEDates._Isconfirmed = True
                    'Else
                    objEDates._Isconfirmed = False
                    'End If
                    objEDates._Datetypeunkid = enEmp_Dates_Transaction.DT_EXEMPTION
                    objEDates._Changereasonunkid = mintLeavetypeunkid
                    objEDates._Isvoid = False
                    objEDates._Statusunkid = 0
                    objEDates._Userunkid = mintUserunkid
                    objEDates._Voiddatetime = Nothing
                    objEDates._Voidreason = ""
                    objEDates._Voiduserunkid = -1
                    objEDates._Leaveissueunkid = mintLeaveissueunkid
                    'If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                    '    objEDates._Actionreasonunkid = CInt(cboChangeReason.SelectedValue)
                    '    objEDates._Isexclude_payroll = chkExclude.Checked
                    'End If
                    If objEDates.Insert(blnCreateADUserFromEmpMst, xCompanyUnkid, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                Else
                    Dim objADate As New clsDates_Approval_Tran
                    If objADate.isExist(mdtStartdate, mdtStartdate, Nothing, enEmp_Dates_Transaction.DT_EXEMPTION, mintEmployeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", objDataOperation, True) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 16, "Sorry, exemption Information is already present in approval process with selected effective date and allocation combination.")
                        Return False
                    End If

                    If objADate.isExist(mdtStartdate, Nothing, Nothing, enEmp_Dates_Transaction.DT_EXEMPTION, mintEmployeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", objDataOperation, True) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 15, "Sorry, exemption Information is already present in approval process with selected effective date.")
                        Return False
                    End If

                    If objADate.isExist(Nothing, mdtStartdate, Nothing, enEmp_Dates_Transaction.DT_EXEMPTION, mintEmployeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", objDataOperation, True) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 17, "Sorry, exemption Information is already present in approval process with selected allocation combination.")
                        Return False
                    End If

                    If objADate.isExist(mdtStartdate, mdtStartdate, Nothing, enEmp_Dates_Transaction.DT_EXEMPTION, mintEmployeeunkid, clsEmployeeMovmentApproval.enOperationType.EDITED, "", objDataOperation, True) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 16, "Sorry, exemption Information is already present in approval process with selected effective date and allocation combination.")
                        Return False
                    End If

                    If objADate.isExist(mdtStartdate, Nothing, Nothing, enEmp_Dates_Transaction.DT_EXEMPTION, mintEmployeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", objDataOperation, True) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 15, "Sorry, exemption Information is already present in approval process with selected effective date.")
                        Return False
                    End If

                    If objADate.isExist(Nothing, mdtStartdate, Nothing, enEmp_Dates_Transaction.DT_EXEMPTION, mintEmployeeunkid, clsEmployeeMovmentApproval.enOperationType.DELETED, "", Nothing, True) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 17, "Sorry, exemption Information is already present in approval process with selected allocation combination.")
                        Return False
                    End If

                    objADate._Audittype = enAuditType.ADD
                    objADate._Audituserunkid = mintUserunkid
                    objADate._Effectivedate = mdtStartdate
                    objADate._Employeeunkid = mintEmployeeunkid
                    objADate._Date1 = mdtStartdate
                    objADate._Date2 = Nothing

                    'If mintDatesTypeId = enEmp_Dates_Transaction.DT_CONFIRMATION Then
                    '    objADate._Isconfirmed = True
                    'Else
                    objADate._Isconfirmed = False
                    'End If
                    objADate._Datetypeunkid = enEmp_Dates_Transaction.DT_EXEMPTION
                    objADate._Changereasonunkid = mintLeavetypeunkid
                    objADate._Isvoid = False
                    objADate._Statusunkid = 0
                    objADate._Voiddatetime = Nothing
                    objADate._Voidreason = ""
                    objADate._Voiduserunkid = -1
                    'If mintDatesTypeId = enEmp_Dates_Transaction.DT_TERMINATION Then
                    '    objADate._Actionreasonunkid = CInt(cboChangeReason.SelectedValue)
                    '    objADate._Isexclude_Payroll = chkExclude.Checked
                    'End If
                    objADate._Isvoid = False
                    objADate._Statusunkid = clsEmployee_Master.EmpApprovalStatus.SubmitForApproval
                    objADate._Voiddatetime = Nothing
                    objADate._Voidreason = ""
                    objADate._Voiduserunkid = -1
                    objADate._Tranguid = Guid.NewGuid.ToString()
                    objADate._Transactiondate = Now
                    objADate._Remark = ""
                    objADate._Rehiretranunkid = 0
                    objADate._Mappingunkid = 0
                    objADate._Isweb = False
                    objADate._Isfinal = False
                    objADate._Ip = mstrWebClientIP
                    objADate._Hostname = mstrWebHostName
                    objADate._Form_Name = mstrWebFormName
                    objADate._Leaveissueunkid = mintLeaveissueunkid

                    objADate._OperationTypeId = clsEmployeeMovmentApproval.enOperationType.ADDED
                    If objADate.Insert(xCompanyUnkid, clsEmployeeMovmentApproval.enOperationType.ADDED, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If
            'Sohail (21 Oct 2019) -- End

            'Pinkal (25-AUG-2017) -- Start
            'Enhancement - Working on B5 plus TnA Enhancement.
            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Time_and_Attendance_Management) OrElse ArtLic._Object.IsDemo Then
                Dim objLogin As New clslogin_Tran
                If objLogin.UpdateLoginSummaryFromLeave(objDataOperation, strLeavedate, mintEmployeeunkid) = False Then
                    eZeeMsgBox.Show(objLogin._Message, enMsgBoxStyle.Information)
                End If
                objLogin = Nothing
            End If
            'Pinkal (25-AUG-2017) -- End


            If objdoOpeation Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            Return True

        Catch ex As Exception
            If objdoOpeation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'Pinkal (01-Feb-2014) -- Start
            'Enhancement : TRA Changes
            If objdoOpeation Is Nothing Then objDataOperation = Nothing
            'Pinkal (01-Feb-2014) -- End

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvleaveIssue_master) </purpose>
    Public Function Update(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                     , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                     , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                     , ByVal xIncludeIn_ActiveEmployee As Boolean, ByVal blnOnlyActive As Boolean, ByVal strLeavedate() As String, ByVal isPaid As Boolean _
                                     , ByVal mdblLeaveAmout As Double, Optional ByVal intLeaveBalanceSetting As Integer = -1, Optional ByVal blnLeaveApproverForLeaveType As String = "" _
                                     , Optional ByVal intApproverID As Integer = -1, Optional ByVal blnApplyUserAccessFilter As Boolean = True) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveissueunkid.ToString)
            objDataOperation.AddParameter("@leaveyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeaveyearunkid.ToString)
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidReason)
            objDataOperation.AddParameter("@issuecount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclTotalIssue.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtStartdate <> Nothing, mdtStartdate, DBNull.Value))
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtEnddate <> Nothing, mdtEnddate, DBNull.Value))

            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            objDataOperation.AddParameter("@istnaperiodlinked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnConsiderLeaveOnTnAPeriod)
            'Pinkal (01-Jan-2019) -- End


            strQ = "UPDATE lvleaveIssue_master SET " & _
              "  leaveyearunkid = @leaveyearunkid" & _
              ", formunkid = @formunkid" & _
              ", leavetypeunkid = @leavetypeunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voiduserunkid = @voiduserunkid " & _
              ", voidreason = @voidreason " & _
             ", issuecount  = @issuecount " & _
             ", startdate = @startdate " & _
             ", enddate = @enddate " & _
                         ", istnaperiodlinked = @istnaperiodlinked " & _
            "WHERE leaveissueunkid = @leaveissueunkid "

            'Pinkal (01-Jan-2019) --  'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", istnaperiodlinked = @istnaperiodlinked " & _]

            objDataOperation.ExecNonQuery(strQ)

            objIssueLeave_tran._Leaveissueunkid = mintLeaveissueunkid
            objIssueLeave_tran._IsPaid = isPaid
            objIssueLeave_tran._TotalAccrue = mdblLeaveAmout

            'Pinkal (24-Aug-2015) -- Start
            'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
            'objIssueLeave_tran.InsertDelete(objDataOperation, strLeavedate, mintLeaveyearunkid, mintEmployeeunkid, mintLeavetypeunkid, -1, intApproverID)
            objIssueLeave_tran.InsertDelete(xDatabaseName, xUserUnkid, xCompanyUnkid, strEmployeeAsOnDate _
                                                         , xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, objDataOperation _
                                                         , strLeavedate, mintLeaveyearunkid, mintEmployeeunkid, mintLeavetypeunkid, intLeaveBalanceSetting _
                                                         , blnLeaveApproverForLeaveType, intApproverID, blnApplyUserAccessFilter)
            'Pinkal (24-Aug-2015) -- End



            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Pinkal (24-Aug-2015) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvleaveIssue_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "DELETE FROM lvleaveIssue_master " & _
            "WHERE leaveissueunkid = @leaveissueunkid "

            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@leaveissueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmployeeId As Integer, ByVal intFormId As Integer, ByVal intLeaveTypeId As Integer, Optional ByVal objdoOpeation As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objdoOpeation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objdoOpeation
        End If

        Try

            strQ = "SELECT " & _
              "  leaveissueunkid " & _
              ", leaveyearunkid " & _
              ", formunkid " & _
              ", leavetypeunkid " & _
              ", employeeunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiddatetime " & _
              ", voiduserunkid " & _
              ", issuecount " & _
              ", startdate " & _
              ", enddate " & _
                          ", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _
                          " FROM lvleaveIssue_master " & _
             " WHERE employeeunkid = @employeeunkid " & _
                     " AND formunkid = @formunkid AND leavetypeunkid = @leavetypeunkid AND isvoid = 0 "

            'Pinkal (01-Jan-2019) --  'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intFormId)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmployeeId)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intLeaveTypeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objdoOpeation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetFormNo(ByVal intEmployeeunkid As Integer, ByVal intLeavetypeunkid As Integer, ByVal mintLeaveBalanceSetting As Integer, Optional ByVal dtIssueEndDate As DateTime = Nothing, Optional ByVal blnUnProcessed As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "Select lvleaveform.formunkid,lvleaveform.formno, lvleaveform.leavetypeunkid from lvleaveform " & _
                   " LEFT JOIN lvleaveissue_master on lvleaveform.formunkid = lvleaveissue_master.formunkid "



            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                strQ &= " LEFT JOIN lvleavebalance_tran on lvleavebalance_tran.employeeunkid = lvleaveform.employeeunkid AND lvleavebalance_tran.leavetypeunkid = lvleaveform.leavetypeunkid AND lvleavebalance_tran.isvoid = 0 "
            End If

            'Pinkal (21-Jul-2014) -- End

            strQ &= " where lvleaveform.statusunkid = 7 and lvleaveform.employeeunkid = @employeeunkid AND lvleaveform.leavetypeunkid = @leavetypeunkid AND lvleaveform.isvoid = 0"

            If dtIssueEndDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), lvleaveissue_master.startdate, 112) <= @startdate "
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtIssueEndDate))
            End If

            If blnUnProcessed = True Then
                strQ &= " AND ISNULL(isprocess, 1) = 0 "
            End If

            'Pinkal (21-Jul-2014) -- Start
            'Enhancement - TRA Leave Enhancement From TRA and TANAPA Urgent Fixes.Docx File Point No 1 & 16

            If mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then

                'Pinkal (03-Nov-2014) -- Start
                'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

                'strQ &= " AND CONVERT(char(8),lvleaveform.startdate,112) >= convert(char(8),lvleavebalance_tran.startdate,112) " & _
                '         " AND convert(char(8),lvleaveform.returndate,112) >= CONVERT(char(8),lvleavebalance_tran.enddate,112) "

                strQ &= " AND CONVERT(char(8),lvleaveform.startdate,112) BETWEEN convert(char(8),lvleavebalance_tran.startdate,112) " & _
                         " AND CONVERT(char(8),lvleavebalance_tran.enddate,112) "

                'Pinkal (03-Nov-2014) -- End

            End If

            'Pinkal (21-Jul-2014) -- End


            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeavetypeunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFormNo; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeIssueData(ByVal intEmployeeunkid As Integer, Optional ByVal intLeaveTypeUnkId As Integer = 0, _
                                         Optional ByVal xDataOpr As clsDataOperation = Nothing, _
                                         Optional ByVal mstrFilter As String = "") As DataSet
        'Gajanan [20-Dec-2019] -- ADD [xDataOpr,mstrFilter]   


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            'Gajanan [20-Dec-2019] -- Start   
            'Enhancement:Voltamp Transformers-Oman [0003275] : Unable to apply leave on cancelled days
            'objDataOperation = New clsDataOperation
            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
            objDataOperation = New clsDataOperation
            End If
            'Gajanan [20-Dec-2019] -- End

            'Gajanan [20-Dec-2019] -- Start   
            'Enhancement:Voltamp Transformers-Oman [0003275] : Unable to apply leave on cancelled days
            objDataOperation.ClearParameters()
            'Gajanan [20-Dec-2019] -- End


            strQ = " SELECT leaveissuetranunkid,lvleaveIssue_tran.leaveissueunkid,Convert(char(8),leavedate,112) as leavedate,lvleavetype_master.color,lvleavetype_Master.leavetypeunkid,lvleaveissue_master.formunkid " & _
                       ", ISNULL(lvleaveIssue_tran.dayfraction,0.00) AS dayfraction " & _
                       " FROM lvleaveIssue_tran " & _
                   " JOIN lvleaveissue_master on lvleaveissue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid AND employeeunkid=@employeeunkid AND lvleaveissue_master.isvoid = 0" & _
                   " JOIN lvleavetype_master on lvleavetype_master.leavetypeunkid = lvleaveissue_master.leavetypeunkid " & _
                   " WHERE lvleaveIssue_tran.isvoid = 0 "

            'Pinkal (16-May-2019) -- 'Enhancement - Working on Leave UAT Changes for NMB.[", ISNULL(lvleaveIssue_tran.dayfraction,0.00) AS dayfraction " & _]

            If intLeaveTypeUnkId > 0 Then
                strQ &= " AND lvleavetype_Master.leavetypeunkid = @leavetypeunkid "
                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeUnkId)
            End If

            'Gajanan [20-Dec-2019] -- Start   
            'Enhancement:Voltamp Transformers-Oman [0003275] : Unable to apply leave on cancelled days
            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If
            'Gajanan [20-Dec-2019] -- End


            strQ &= " ORDER BY lvleaveIssue_tran.leavedate"

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeIssueData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'Gajanan [20-Dec-2019] -- Start   
            'Enhancement:Voltamp Transformers-Oman [0003275] : Unable to apply leave on cancelled days
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then
            objDataOperation = Nothing
            End If
            'Gajanan [20-Dec-2019] -- End

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' <param name="intIsProcessed"> -1=All, 0=Only Unprocessed, 1=Only Processed</param>
    Public Function GetEmployeeTotalIssue(ByVal intEmployeeunkid As Integer, ByVal intLeaveTypeId As Integer, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mdtStartdate As Date = Nothing, Optional ByVal mdtEndDate As Date = Nothing, Optional ByVal blnFromProcessPayroll As Boolean = False, Optional ByVal intLeaveFormUnkId As Integer = 0, Optional ByVal intIsProcessed As Integer = -1, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal mdtPayrollStartdate As Date = Nothing, Optional ByVal mdtPayrollEndDate As Date = Nothing, Optional ByVal intLeaveYearUnkId As Integer = 0) As Decimal
        'Sohail (25 Jan 2021) - [intLeaveYearUnkId]
        'Sohail (28 Dec 2018) - [mdtPayrollStartdate, mdtPayrollEndDate]
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnIsSickLeave As Boolean = False
        Try

            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim objDataOperation As New clsDataOperation
            Dim objDataOperation As clsDataOperation
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            'Sohail (03 May 2018) -- End
            objDataOperation.ClearParameters()
            If blnFromProcessPayroll = True Then
                strQ = "SELECT  ISNULL(issickleave, 0) AS issickleave " & _
                        "FROM    lvleavetype_master " & _
                        "WHERE   leavetypeunkid = " & intLeaveTypeId & " "

                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    blnIsSickLeave = CBool(dsList.Tables(0).Rows(0)("issickleave"))
                End If
            End If

            'Sohail (28 Dec 2018) -- Start
            'VOLTAMP Enhancement - 76.1 - Calculate Short Leave on Payroll Period instead of TnA Period.
            Dim blnPayrollPeriod As Boolean = False
            If mdtPayrollStartdate <> Nothing AndAlso mdtPayrollEndDate <> Nothing Then
                blnPayrollPeriod = True
            End If
            'Sohail (28 Dec 2018) -- End

            If blnIsSickLeave = False Then
                'Sohail (28 Dec 2018) -- Start
                'VOLTAMP Enhancement - 76.1 - Calculate Short Leave on Payroll Period instead of TnA Period.
                'If mdtStartdate <> Nothing AndAlso mdtEndDate <> Nothing Then
                If mdtStartdate <> Nothing AndAlso mdtEndDate <> Nothing AndAlso blnPayrollPeriod = False Then
                    'Sohail (28 Dec 2018) -- End

                strQ = " SELECT ISNULL(SUM(dayfraction),0.00) AS TotalIssue FROM lvleaveIssue_master  " & _
                          " JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                          " AND CONVERT(CHAR(8),leavedate,112) >= @startdate AND CONVERT(CHAR(8),leavedate,112) <= @enddate AND lvleaveIssue_tran.isvoid = 0 " & _
                        " WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid "

                If blnOnlyActive = True Then
                    strQ &= "AND ISNULL(lvleaveIssue_master.isvoid, 0) = 0 "
                End If

                    'Sohail (25 Jan 2021) -- Start
                    'Voltamp Issue : : Sick leave was deducted as per TnA period when TnA linked period is not set.
                    If intLeaveYearUnkId > 0 Then
                        strQ &= " AND lvleaveIssue_master.leaveyearunkid = @leaveyearunkid "
                    End If
                    'Sohail (25 Jan 2021) -- End

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartdate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))

                    'Sohail (28 Dec 2018) -- Start
                    'VOLTAMP Enhancement - 76.1 - Calculate Short Leave on Payroll Period instead of TnA Period.
                ElseIf mdtStartdate <> Nothing AndAlso mdtEndDate <> Nothing AndAlso blnPayrollPeriod = True Then

                    strQ = "SELECT ISNULL(SUM(dayfraction), 0.00) AS TotalIssue " & _
                            "FROM lvleaveIssue_master " & _
                                "JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                                "LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                            "WHERE employeeunkid = @employeeunkid " & _
                                  "AND lvleavetype_master.leavetypeunkid = @leavetypeunkid " & _
                                  "AND lvleaveIssue_tran.isvoid = 0 " & _
                                  "AND ( " & _
                                        "( lvleavetype_master.istnaperiodlinked = 0 AND CONVERT(CHAR(8), lvleaveIssue_tran.leavedate, 112) >= @payrollstartdate AND CONVERT(CHAR(8), lvleaveIssue_tran.leavedate, 112) <= @payrollenddate ) " & _
                                        "OR " & _
                                        "( lvleavetype_master.istnaperiodlinked = 1 AND CONVERT(CHAR(8), lvleaveIssue_tran.leavedate, 112) >= @startdate AND CONVERT(CHAR(8), lvleaveIssue_tran.leavedate, 112) <= @enddate ) " & _
                                    ") "

                    If blnOnlyActive = True Then
                        strQ &= "AND ISNULL(lvleaveIssue_master.isvoid, 0) = 0 "
                    End If

                    'Sohail (25 Jan 2021) -- Start
                    'Voltamp Issue : : Sick leave was deducted as per TnA period when TnA linked period is not set.
                    If intLeaveYearUnkId > 0 Then
                        strQ &= " AND lvleaveIssue_master.leaveyearunkid = @leaveyearunkid "
                    End If
                    'Sohail (25 Jan 2021) -- End

                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartdate))
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
                    objDataOperation.AddParameter("@payrollstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartdate))
                    objDataOperation.AddParameter("@payrollenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPayrollEndDate))
                    'Sohail (28 Dec 2018) -- End

            Else
                strQ = "SELECT ISNULL(SUM(issuecount),0.00) AS TotalIssue FROM lvleaveIssue_master  " & _
                           "WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid "

                If blnOnlyActive = True Then
                    strQ &= "AND ISNULL(lvleaveIssue_master.isvoid, 0) = 0 "
                End If

                    'Sohail (25 Jan 2021) -- Start
                    'Voltamp Issue : : Sick leave was deducted as per TnA period when TnA linked period is not set.
                    If intLeaveYearUnkId > 0 Then
                        strQ &= " AND lvleaveIssue_master.leaveyearunkid = @leaveyearunkid "
                    End If
                    'Sohail (25 Jan 2021) -- End

            End If

                If intIsProcessed = 0 Then 'Only Unprocessed
                    strQ &= " AND ISNULL(lvleaveIssue_tran.isprocess, 1) = 0 "
                ElseIf intIsProcessed = 1 Then 'Only Processed
                    strQ &= " AND ISNULL(lvleaveIssue_tran.isprocess, 1) = 1 "
                End If

                If intLeaveFormUnkId > 0 Then
                    strQ &= "AND formunkid = @formunkid "
                    objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveFormUnkId)
                End If

            Else

                'Sohail (28 Dec 2018) -- Start
                'VOLTAMP Enhancement - 76.1 - Calculate Short Leave on Payroll Period instead of TnA Period.
                'strQ = "SELECT ISNULL(SUM(issuecount),0.00) AS TotalIssue FROM lvleaveIssue_master  " & _
                '              "WHERE employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid " 'AND ISNULL(isprocess, 1) = 0
                strQ = "SELECT ISNULL(SUM(issuecount), 0.00) AS TotalIssue " & _
                        "FROM lvleaveIssue_master "

                If blnPayrollPeriod = True Then
                    strQ &= "LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid "
                End If

                strQ &= "WHERE lvleaveIssue_master.employeeunkid = @employeeunkid " & _
                              "AND lvleaveIssue_master.leavetypeunkid = @leavetypeunkid "
                'Sohail (28 Dec 2018) -- End

                If blnOnlyActive = True Then
                    strQ &= "AND ISNULL(lvleaveIssue_master.isvoid, 0) = 0 "
                End If

                'Sohail (25 Jan 2021) -- Start
                'Voltamp Issue : : Sick leave was deducted as per TnA period when TnA linked period is not set.
                If intLeaveYearUnkId > 0 Then
                    strQ &= " AND lvleaveIssue_master.leaveyearunkid = @leaveyearunkid "
                End If
                'Sohail (25 Jan 2021) -- End

                If intLeaveFormUnkId > 0 Then
                    strQ &= "AND formunkid = @formunkid "
                    objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveFormUnkId)
                End If

                'Sohail (28 Dec 2018) -- Start
                'VOLTAMP Enhancement - 76.1 - Calculate Short Leave on Payroll Period instead of TnA Period.
                'If mdtStartdate <> Nothing AndAlso mdtEndDate <> Nothing Then
                If mdtStartdate <> Nothing AndAlso mdtEndDate <> Nothing AndAlso blnPayrollPeriod = False Then
                    'Sohail (28 Dec 2018) -- End
                    strQ &= " AND CONVERT(CHAR(8), startdate, 112) <= @startdate "
                    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
                    'Sohail (28 Dec 2018) -- Start
                    'VOLTAMP Enhancement - 76.1 - Calculate Short Leave on Payroll Period instead of TnA Period.
                ElseIf mdtStartdate <> Nothing AndAlso mdtEndDate <> Nothing AndAlso blnPayrollPeriod = True Then
                    strQ &= "AND ( " & _
                                    "( lvleavetype_master.istnaperiodlinked = 0 AND CONVERT(CHAR(8),startdate,112) <= @payrollenddate ) " & _
                                    "OR " & _
                                    "( lvleavetype_master.istnaperiodlinked = 1 AND CONVERT(CHAR(8),startdate,112) <= @enddate ) " & _
                                ") "

                    objDataOperation.AddParameter("@payrollenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPayrollEndDate))
                    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
                    'Sohail (28 Dec 2018) -- End
                End If
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
            'Sohail (25 Jan 2021) -- Start
            'Voltamp Issue : : Sick leave was deducted as per TnA period when TnA linked period is not set.
            If intLeaveYearUnkId > 0 Then
                objDataOperation.AddParameter("@leaveyearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveYearUnkId)
            End If
            'Sohail (25 Jan 2021) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CDec(dsList.Tables(0).Rows(0)("TotalIssue"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeTotalIssue; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function GetEmployeeTotalCancel(ByVal intEmployeeunkid As Integer, ByVal intLeaveTypeId As Integer, Optional ByVal mdtStartdate As Date = Nothing, Optional ByVal mdtEndDate As Date = Nothing, Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByVal mdtPayrollStartdate As Date = Nothing, Optional ByVal mdtPayrollEndDate As Date = Nothing) As Decimal
        'Sohail (28 Dec 2018) - [mdtPayrollStartdate, mdtPayrollEndDate]
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'Dim objDataOperation As New clsDataOperation
            Dim objDataOperation As clsDataOperation
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOp
            End If
            'Sohail (03 May 2018) -- End
            objDataOperation.ClearParameters()

            'Sohail (28 Dec 2018) -- Start
            'VOLTAMP Enhancement - 76.1 - Calculate Short Leave on Payroll Period instead of TnA Period.
            Dim blnPayrollPeriod As Boolean = False
            If mdtPayrollStartdate <> Nothing AndAlso mdtPayrollEndDate <> Nothing Then
                blnPayrollPeriod = True
            End If
            'Sohail (28 Dec 2018) -- End

            'Sohail (28 Dec 2018) -- Start
            'VOLTAMP Enhancement - 76.1 - Calculate Short Leave on Payroll Period instead of TnA Period.
            'If mdtStartdate <> Nothing AndAlso mdtEndDate <> Nothing Then
            If mdtStartdate <> Nothing AndAlso mdtEndDate <> Nothing AndAlso blnPayrollPeriod = False Then
                'Sohail (28 Dec 2018) -- End

                strQ = " SELECT ISNULL(SUM(dayfraction),0.00) TotalCancel FROM lvcancelform " & _
                          " JOIN lvleaveform ON lvcancelform.formunkid = lvleaveform.formunkid AND lvleaveform.employeeunkid = @employeeunkid " & _
                          " AND lvleaveform.leavetypeunkid = @leavetypeunkid  " & _
                          " WHERE lvcancelform.isvoid = 0 AND CONVERT(CHAR(8),cancel_leavedate,112) >= @startdate AND CONVERT(CHAR(8),cancel_leavedate,112) <= @enddate "
                'Sohail (28 Dec 2018) - [lvcancelform.isvoid = 0]

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartdate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))

                'Sohail (28 Dec 2018) -- Start
                'VOLTAMP Enhancement - 76.1 - Calculate Short Leave on Payroll Period instead of TnA Period.
            ElseIf mdtStartdate <> Nothing AndAlso mdtEndDate <> Nothing AndAlso blnPayrollPeriod = True Then

                strQ = "SELECT ISNULL(SUM(dayfraction), 0.00) TotalCancel " & _
                        "FROM lvcancelform " & _
                            "JOIN lvleaveform ON lvcancelform.formunkid = lvleaveform.formunkid " & _
                            "LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid " & _
                        "WHERE lvcancelform.isvoid = 0 " & _
                              "AND lvleaveform.employeeunkid = @employeeunkid " & _
                              "AND lvleaveform.leavetypeunkid = @leavetypeunkid " & _
                              "AND ( " & _
                                        "( lvleavetype_master.isshortleave = 1 AND CONVERT(CHAR(8),lvcancelform.cancel_leavedate,112) >= @payrollstartdate AND CONVERT(CHAR(8), lvcancelform.cancel_leavedate, 112) <= @payrollenddate ) " & _
                                        "OR " & _
                                        "( lvleavetype_master.isshortleave = 0 AND CONVERT(CHAR(8),lvcancelform.cancel_leavedate,112) >= @startdate AND CONVERT(CHAR(8), lvcancelform.cancel_leavedate, 112) <= @enddate ) " & _
                                    ") "

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartdate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
                objDataOperation.AddParameter("@payrollstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPayrollStartdate))
                objDataOperation.AddParameter("@payrollenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtPayrollEndDate))
                'Sohail (28 Dec 2018) -- End

            Else
                strQ = "SELECT ISNULL(SUM(ISNULL(cancelcount,0.00)), 0) AS TotalCancel FROM lvleaveIssue_master  " & _
                           "WHERE lvleaveIssue_master.isvoid = 0 AND employeeunkid = @employeeunkid AND leavetypeunkid = @leavetypeunkid "
                'Sohail (28 Dec 2018) - [lvleaveIssue_master.isvoid = 0]
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CDec(dsList.Tables(0).Rows(0)("TotalCancel"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeTotalCancel; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
    End Function


    'Pinkal (03-Nov-2014) -- Start
    'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.

    Public Function SetNotificationForIssuedUser(ByVal mstrUserName As String, _
                             ByVal strEmployeeName As String, _
                             ByVal strFormNo As String, _
                             ByVal mstrLeaveType As String, _
                             ByVal dtStartdate As Date, _
                             ByVal dtEndDate As Date) As String

        Dim strMessage As String = ""
        Try
            strMessage = "<HTML> <BODY>"
            strMessage &= Language.getMessage(mstrModuleName, 1, "Dear") & " " & mstrUserName & ", <BR><BR>"

            strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 2, " This is to notify that leave application no") & " " & strFormNo.Trim & _
                                       Language.getMessage(mstrModuleName, 3, " with ") & mstrLeaveType & Language.getMessage(mstrModuleName, 4, " of ") & strEmployeeName & Language.getMessage(mstrModuleName, 5, " having Start date : ")

            strMessage &= dtStartdate.Date & Language.getMessage(mstrModuleName, 6, " and End Date : ") & dtEndDate.Date & Language.getMessage(mstrModuleName, 7, " has been issued.")

            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SetNotificationForIssuedUser; Module Name: " & mstrModuleName)
        End Try
        Return strMessage
    End Function


    'Pinkal (03-Nov-2014) -- End


    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Public Function GetEmployeeIssueLeaveDays(ByVal xEmployeeunkid As Integer, ByVal xLeaveTypeId As Integer, ByVal mdtStartdate As Date, ByVal mdtEndDate As Date) As DataTable
        Dim mdtTable As DataTable = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Try
            Dim objDataOperation As New clsDataOperation

            strQ = " SELECT lvleaveIssue_master.leavetypeunkid " & _
                      ", ISNULL(lvleavetype_master.leavename,'') AS LeaveName " & _
                      ", CONVERT(char(8),leavedate,112) AS Date " & _
                      ", lvleaveIssue_tran.dayfraction " & _
                      " FROM lvleaveIssue_master  " & _
                      " JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveIssue_master.leavetypeunkid " & _
                      " JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                      " WHERE lvleaveIssue_master.employeeunkid = @employeeunkid AND lvleaveIssue_master.leavetypeunkid = @leavetypeunkid AND ISNULL(lvleaveIssue_master.isvoid, 0) = 0 " & _
                      " AND CONVERT(CHAR(8),leavedate,112) >= @startdate AND CONVERT(CHAR(8),leavedate,112) <= @enddate AND lvleaveIssue_tran.isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeunkid)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLeaveTypeId)
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartdate))
            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
            Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                mdtTable = dsList.Tables(0)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeIssueLeaveDays; Module Name: " & mstrModuleName)
        End Try
        Return mdtTable
    End Function
    'Pinkal (28-Jul-2018) -- End




	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Dear")
			Language.setMessage(mstrModuleName, 2, " This is to notify that leave application no")
			Language.setMessage(mstrModuleName, 3, " with")
			Language.setMessage(mstrModuleName, 4, " of")
			Language.setMessage(mstrModuleName, 5, " having Start date :")
			Language.setMessage(mstrModuleName, 6, " and End Date :")
			Language.setMessage(mstrModuleName, 7, " has been issued.")
			Language.setMessage(mstrModuleName, 8, "You cannot perform this operation.Reason : This Leave form is already issued.")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class