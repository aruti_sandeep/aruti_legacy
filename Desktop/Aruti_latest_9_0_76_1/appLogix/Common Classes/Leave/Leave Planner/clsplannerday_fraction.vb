﻿'************************************************************************************************************************************
'Class Name : clsplannerday_fraction.vb
'Purpose    :
'Date       :7/14/2016
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsplannerday_fraction
    Private Const mstrModuleName = "clsplannerday_fraction"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintPlannerfractionunkid As Integer
    Private mintleaveplannerunkid As Integer
    Private mdtLeavedate As Date
    Private mdecDayfraction As Decimal
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer = -1
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtFraction As DataTable = Nothing
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = String.Empty
    Private mstrWebHostName As String = String.Empty

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set plannerfractionunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Plannerfractionunkid(ByVal objDataOperation As clsDataOperation) As Integer
        Get
            Return mintPlannerfractionunkid
        End Get
        Set(ByVal value As Integer)
            mintPlannerfractionunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leaveplannerunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _leaveplannerunkid() As Integer
        Get
            Return mintleaveplannerunkid
        End Get
        Set(ByVal value As Integer)
            mintleaveplannerunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavedate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavedate() As Date
        Get
            Return mdtLeavedate
        End Get
        Set(ByVal value As Date)
            mdtLeavedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dayfraction
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Dayfraction() As Decimal
        Get
            Return mdecDayfraction
        End Get
        Set(ByVal value As Decimal)
            mdecDayfraction = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtFraction
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtFraction() As DataTable
        Get
            Return mdtFraction
        End Get
        Set(ByVal value As DataTable)
            mdtFraction = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emp_remark
    ''' Modify By: Pinkal
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(ByVal objDoOperation As clsDataOperation)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        Try
            strQ = "SELECT " & _
                      "  plannerfractionunkid " & _
                      ", leaveplannerunkid " & _
                      ", leavedate " & _
                      ", dayfraction " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM lvplannerday_fraction " & _
                     "WHERE plannerfractionunkid = @plannerfractionunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@plannerfractionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPlannerfractionunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPlannerfractionunkid = CInt(dtRow.Item("plannerfractionunkid"))
                mintleaveplannerunkid = CInt(dtRow.Item("leaveplannerunkid"))
                mdtLeavedate = dtRow.Item("leavedate")
                mdecDayfraction = Convert.ToDecimal(dtRow.Item("dayfraction"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal intPlannerID As Integer, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  lvplannerday_fraction.plannerfractionunkid " & _
                      ", lvplannerday_fraction.leaveplannerunkid " & _
                      ", lvleaveplanner.employeeunkid " & _
                      ", Convert(char(8),lvplannerday_fraction.leavedate,112) AS leavedate " & _
                      ", lvplannerday_fraction.dayfraction " & _
                      ", '' AS AUD " & _
                     " FROM lvplannerday_fraction " & _
                     " JOIN lvleaveplanner ON lvleaveplanner.leaveplannerunkid  = lvplannerday_fraction.leaveplannerunkid AND lvleaveplanner.isvoid = 0 " & _
                     " WHERE lvplannerday_fraction.leaveplannerunkid = @leaveplannerunkid "

            If blnOnlyActive Then
                strQ &= " AND lvplannerday_fraction.isvoid = 0 "
            End If

            objDataOperation.AddParameter("@leaveplannerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPlannerID)
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtFraction = dsList.Tables(0)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvplannerday_fraction) </purpose>
    Public Function InsertUpdatePlannerDayFraction(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            If mdtFraction Is Nothing Then Return True

            For i = 0 To mdtFraction.Rows.Count - 1

                With mdtFraction.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then

                        Select Case .Item("AUD")

                            Case "A"

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@leaveplannerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveplannerunkid.ToString)
                                objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(.Item("leavedate").ToString()).Date)
                                objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(.Item("dayfraction")))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                                strQ = "INSERT INTO lvplannerday_fraction ( " & _
                                          "  leaveplannerunkid " & _
                                          ", leavedate " & _
                                          ", dayfraction " & _
                                          ", userunkid " & _
                                          ", loginemployeeunkid " & _
                                          ", isvoid " & _
                                          ", voiduserunkid " & _
                                          ", voiddatetime " & _
                                          ", voidreason" & _
                                        ") VALUES (" & _
                                          "  @leaveplannerunkid " & _
                                          ", @leavedate " & _
                                          ", @dayfraction " & _
                                          ", @userunkid " & _
                                          ", @loginemployeeunkid " & _
                                          ", @isvoid " & _
                                          ", @voiduserunkid " & _
                                          ", @voiddatetime " & _
                                          ", @voidreason" & _
                                        "); SELECT @@identity"

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                _Plannerfractionunkid(objDataOperation) = dsList.Tables(0).Rows(0).Item(0)


                                If InsertAuditTrailForPlannerDays(objDataOperation, 1) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                strQ = " Update lvplannerday_fraction SET " & _
                                          " isvoid = 1  " & _
                                          ", voiddatetime = GetDate() " & _
                                          ", voidreason = @voidreason "

                                If mintVoidloginemployeeunkid > 0 Then
                                    strQ &= ", voidloginemployeeunkid = @voidloginemployeeunkid "
                                Else
                                    strQ &= ", voiduserunkid = @voiduserunkid "
                                End If

                                strQ &= " WHERE plannerfractionunkid =  @plannerfractionunkid AND isvoid = 0 "

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason.ToString)
                                objDataOperation.AddParameter("@plannerfractionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("plannerfractionunkid")).ToString)
                                If mintVoidloginemployeeunkid > 0 Then
                                    objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)
                                Else
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                End If
                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                _Plannerfractionunkid(objDataOperation) = CInt(.Item("plannerfractionunkid"))

                                If InsertAuditTrailForPlannerDays(objDataOperation, 3) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select

                    End If

                End With

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdatePlannerDayFraction; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetPlannerDayFraction(ByVal strXML As String, ByVal intLeaveTypeID As Integer, ByVal mdtStartDate As Date, ByVal mdtEndDate As Date) As DataTable
        Dim dtTable As DataTable = Nothing
        Dim objLeaveType As New clsleavetype_master
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            If mdtEndDate = Nothing Then
                mdtEndDate = mdtStartDate
            End If

            objLeaveType._Leavetypeunkid = intLeaveTypeID


            Dim objDataOperation As New clsDataOperation
            objDataOperation.ClearParameters()


            strQ = "DECLARE @string NVARCHAR(MAX);" & _
                      " DECLARE @xml XML; " & _
                      " SET @string = '" & strXML & "'" & _
                      " SET @xml = @string;" & _
                      " CREATE TABLE #TableEmp " & _
                      "( " & _
                        "  employeeunkid INT " & _
                        ",  employeecode NVARCHAR(MAX) " & _
                        ",  employee NVARCHAR(MAX) " & _
                       ") ; " & _
                       " INSERT INTO #TableEmp (employeeunkid,employeecode,employee) " & _
                       "  SELECT " & _
                       "     (Emp).value('employeeunkid[1]', 'Int') as 'employeeunkid' " & _
                       "    ,(Emp).value('employeecode[1]', 'Nvarchar(max)') as 'employeecode' " & _
                       "    ,(Emp).value('name[1]', 'Nvarchar(max)') as 'name' " & _
                       " FROM @xml.nodes('//Employee') as [A](Emp); " & _
                       " DECLARE @daystable Table (dday int,ddate nvarchar(8),dname nvarchar(50),seed int,employeeunkid int,shiftunkid int,isweekend bit,fraction decimal) " & _
                       " DECLARE @dw AS INT " & _
                       " SET @dw = DATEPART(dw,@start) " & _
                       "; WITH mdate AS " & _
                       " ( " & _
                       "    SELECT CAST(@start AS DATETIME) AS Dvalue " & _
                       "    UNION ALL " & _
                       "    SELECT  Dvalue + 1 FROM mdate WHERE   Dvalue + 1 <= @end " & _
                       " ) " & _
                       " INSERT INTO @daystable " & _
                       " SELECT " & _
                       "    DATEPART(DAY,Dvalue) AS dday " & _
                       "    ,CONVERT(NVARCHAR(8),Dvalue,112) AS ddate " & _
                       "    ,DATENAME(dw,Dvalue) AS dname " & _
                       "    ,CASE WHEN @start = CONVERT(NVARCHAR(8),Dvalue,112) THEN DATEPART(dw,@start) ELSE 0 END AS dwkno " & _
                       "    ,B.employeeunkid " & _
                       "    ,B.shiftunkid " & _
                       "    ,isweekend " & _
                       "    ,1 As fraction " & _
                       " FROM mdate " & _
                       " RIGHT Join " & _
                       " ( " & _
                                   "SELECT " & _
                                        "hremployee_shift_tran.employeeunkid , " & _
                                        "effectivedate, " & _
                                        "shiftunkid, " & _
                                        "1 AS Rowno " & _
                                   "FROM hremployee_shift_tran " & _
                                   "JOIN #TableEmp ON #TableEmp.employeeunkid = hremployee_shift_tran.employeeunkid " & _
                                   "WHERE CONVERT(CHAR(8), effectivedate, 112) > @start " & _
                                   "AND isvoid = 0 " & _
                                             "UNION " & _
                                             "SELECT " & _
                                                  "* " & _
                                                  "FROM (SELECT " & _
                                                  "hremployee_shift_tran.employeeunkid, " & _
                                                  "effectivedate, " & _
                                                  "shiftunkid, " & _
                                                  "ROW_NUMBER() OVER (PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY effectivedate DESC) Rowno " & _
                                             "FROM hremployee_shift_tran " & _
                                             "JOIN #TableEmp ON #TableEmp.employeeunkid = hremployee_shift_tran.employeeunkid " & _
                                             "WHERE isvoid = 0 " & _
                                             "AND CONVERT(CHAR(8), effectivedate, 112) <= @start) AS A " & _
                                             "WHERE a.Rowno = 1 " & _
                       ")  AS B ON B.effectivedate <= @end " & _
                       "Join tnashift_tran ON tnashift_tran.shiftunkid = B.shiftunkid " & _
                       "WHERE DATEPART(dw,Dvalue) = tnashift_tran.dayid + 1 " & _
                       "OPTION (MAXRECURSION 0); " & _
                       "UPDATE @daystable SET @dw =  @dw + seed FROM @daystable WHERE dday > 1 " & _
                       "SELECT " & _
                            "-1 AS plannerfractionunkid " & _
                           ",-1 AS leaveplannerunkid " & _
                           ",[@daystable].employeeunkid " & _
                           ", ddate AS leavedate " & _
                           ", Cast (fraction AS Decimal(4,2)) AS dayfraction " & _
                           ", 'A' AS AUD " & _
                           ", isweekend " & _
                       "FROM @daystable " & _
                       "order by [@daystable].employeeunkid,dday " & _
                       " DROP TABLE #TableEmp;"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@start", SqlDbType.NChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
            objDataOperation.AddParameter("@end", SqlDbType.NChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
            Dim dsEmployeeList As DataSet = objDataOperation.ExecQuery(strQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = "DECLARE @string NVARCHAR(MAX);" & _
                      " DECLARE @xml XML; " & _
                      " SET @string = '" & strXML & "'" & _
                      " SET @xml = @string;" & _
                      " CREATE TABLE #TableHolidayEmp " & _
                      "( " & _
                        "  employeeunkid INT " & _
                        ",  employeecode NVARCHAR(MAX) " & _
                        ",  employee NVARCHAR(MAX) " & _
                       ") ; " & _
                       " INSERT INTO #TableHolidayEmp (employeeunkid,employeecode,employee) " & _
                       "  SELECT " & _
                       "     (Emp).value('employeeunkid[1]', 'Int') as 'employeeunkid' " & _
                       "    ,(Emp).value('employeecode[1]', 'Nvarchar(max)') as 'employeecode' " & _
                       "    ,(Emp).value('name[1]', 'Nvarchar(max)') as 'name' " & _
                       " FROM @xml.nodes('//Employee') as [A](Emp);  " & _
                       "  SELECT " & _
                       "  #TableHolidayEmp.employeeunkid " & _
                       " ,#TableHolidayEmp.employeecode " & _
                       " ,#TableHolidayEmp.employee " & _
                       " ,CONVERT(CHAR(8),lvholiday_master.holidaydate,112) AS Holidaydate " & _
                       " FROM #TableHolidayEmp " & _
                       " Join lvemployee_holiday  ON lvemployee_holiday.employeeunkid = #TableHolidayEmp.employeeunkid " & _
                       " Join lvholiday_master ON lvholiday_master.holidayunkid = lvemployee_holiday.holidayunkid " & _
                       " WHERE  CONVERT(CHAR(8),lvholiday_master.holidaydate, 112) >= @Start  AND CONVERT(CHAR(8), lvholiday_master.holidaydate, 112) <= @End " & _
                       " DROP TABLE #TableHolidayEmp;"


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@start", SqlDbType.NChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtStartDate))
            objDataOperation.AddParameter("@end", SqlDbType.NChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtEndDate))
            Dim dsHolidayList As DataSet = objDataOperation.ExecQuery(strQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If objLeaveType._Isissueonweekend = False Then
                Dim emplist = dsEmployeeList.Tables(0).AsEnumerable().Where(Function(x) x.Field(Of Boolean)("isweekend") = True)
                emplist.ToList.ForEach(Function(x) DeleteRow(x))
                dsEmployeeList.AcceptChanges()
            End If

            If objLeaveType._Isissueonholiday = False Then
                Dim emplist = dsEmployeeList.Tables(0).AsEnumerable()
                Dim empHolidayList = dsHolidayList.Tables(0).AsEnumerable()

                Dim rows = From r1 In emplist Join r2 In empHolidayList On r1.Field(Of Integer)("employeeunkid") Equals r2.Field(Of Integer)("employeeunkid") And r1.Field(Of String)("leavedate") Equals r2.Field(Of String)("Holidaydate") _
                                Select r1

                rows.ToList.ForEach(Function(x) DeleteRow(x))
                dsEmployeeList.AcceptChanges()
            End If


            If dsEmployeeList IsNot Nothing AndAlso dsEmployeeList.Tables(0).Columns.Contains("isweekend") Then
                dsEmployeeList.Tables(0).Columns.Remove("isweekend")
            End If

            dtTable = dsEmployeeList.Tables(0)


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPlannerDayFraction; Module Name: " & mstrModuleName)
        End Try
        Return dtTable
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function DeleteRow(ByVal dr As DataRow) As Boolean
        Try
            dr.Delete()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: DeleteRow; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function UpdateRow(ByVal dr As DataRow, ByVal mstrAUD As String) As Boolean
        Try
            dr("AUD") = mstrAUD
            dr.AcceptChanges()
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateRow; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrailForPlannerDays(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO atlvplannerday_fraction ( " & _
                        "  plannerfractionunkid " & _
                        ", leaveplannerunkid " & _
                        ", leavedate " & _
                        ", dayfraction " & _
                        ", audittype " & _
                        ", audituserunkid " & _
                        ", loginemployeeunkid " & _
                        ", voidloginemployeeunkid " & _
                        ", auditdatetime " & _
                        ", ip " & _
                        ", machine_name" & _
                        ", form_name " & _
                        ", module_name1 " & _
                        ", module_name2 " & _
                        ", module_name3 " & _
                        ", module_name4 " & _
                        ", module_name5 " & _
                        ", isweb " & _
                   ") VALUES (" & _
                        "  @plannerfractionunkid " & _
                        ", @leaveplannerunkid " & _
                        ", @leavedate " & _
                        ", @dayfraction " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @loginemployeeunkid " & _
                        ", @voidloginemployeeunkid " & _
                        ", GetDate() " & _
                        ", @ip " & _
                        ", @machine_name" & _
                        ", @form_name " & _
                        ", @module_name1 " & _
                        ", @module_name2 " & _
                        ", @module_name3 " & _
                        ", @module_name4 " & _
                        ", @module_name5 " & _
                        ", @isweb " & _
                   "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@plannerfractionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPlannerfractionunkid.ToString)
            objDataOperation.AddParameter("@leaveplannerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintleaveplannerunkid.ToString)
            objDataOperation.AddParameter("@leavedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtLeavedate.Date)
            objDataOperation.AddParameter("@dayfraction", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecDayfraction)
            objDataOperation.AddParameter("@audittype", SqlDbType.SmallInt, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrWebClientIP.Trim.Length <= 0, getIP, mstrWebClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrWebHostName.Trim.Length <= 0, getHostName, mstrWebHostName))

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)



            objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForPlannerDays; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

End Class
