﻿'************************************************************************************************************************************
'Class Name : clsleaveexpense.vb
'Purpose    :
'Date       :06/02/2012
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>
Public Class clsleaveexpense
    Private Const mstrModuleName = "clsleaveexpense"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region "Constructor"

    Public Sub New()
        mdtExpense = New DataTable("Expense")
        mdtExpense.Columns.Add("lvexpenseunkid", Type.GetType("System.Int32"))
        mdtExpense.Columns.Add("formunkid", Type.GetType("System.Int32"))
        mdtExpense.Columns.Add("description", Type.GetType("System.String"))
        mdtExpense.Columns.Add("qty", Type.GetType("System.Decimal"))
        mdtExpense.Columns.Add("unitcost", Type.GetType("System.Decimal"))
        mdtExpense.Columns.Add("amount", Type.GetType("System.Decimal"))
        mdtExpense.Columns.Add("AUD", Type.GetType("System.String"))
        mdtExpense.Columns.Add("GUID", Type.GetType("System.String"))
    End Sub

#End Region

#Region " Private variables "

    Private mintLvexpenseunkid As Integer
    Private mintFormunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintLoginemployeeunkid As Integer = -1
    Private mdtExpense As DataTable

    'Pinkal (07-APR-2012) -- Start
    'Enhancement : TRA Changes
    Private mintVoidlogingemployeeunkid As Integer = -1
    'Pinkal (07-APR-2012) -- End

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set lvexpenseunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Lvexpenseunkid() As Integer
        Get
            Return mintLvexpenseunkid
        End Get
        Set(ByVal value As Integer)
            mintLvexpenseunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Formunkid() As Integer
        Get
            Return mintFormunkid
        End Get
        Set(ByVal value As Integer)
            mintFormunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set mdtExpense
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _dtExpense() As DataTable
        Get
            Return mdtExpense
        End Get
        Set(ByVal value As DataTable)
            mdtExpense = value
        End Set
    End Property

    'Pinkal (07-APR-2012) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidlogingemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidlogingemployeeunkid = value
        End Set
    End Property

    'Pinkal (07-APR-2012) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intFormunkid As Integer = -1, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (07-APR-2012) -- Start
            'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '          "  lvexpenseunkid " & _
            '          ", formunkid " & _
            '          ", description " & _
            '          ", qty " & _
            '          ", unitcost " & _
            '          ", amount " & _
            '          ", userunkid " & _
            '          ", loginemployeeunkid  " & _
            '          ", isvoid " & _
            '          ", voiduserunkid " & _
            '          ", voiddatetime " & _
            '          ", voidreason " & _
            '          ", '' as  AUD " & _
            '          ", '' as  GUID " & _
            '          " FROM lvleaveexpense "

            strQ = "SELECT " & _
                      "  lvexpenseunkid " & _
                      ", formunkid " & _
                      ", description " & _
                      ", qty " & _
                      ", unitcost " & _
                      ", amount " & _
                      ", userunkid " & _
                      ", loginemployeeunkid  " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", '' as  AUD " & _
                      ", '' as  GUID " & _
                      ", ISNULL(voidloginemployeeunkid,0) voidloginemployeeunkid " & _
                      " FROM lvleaveexpense "

            'Pinkal (07-APR-2012) -- End

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            If intFormunkid > 0 Then
                strQ &= " AND formunkid = @formunkid"
                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleaveexpense) </purpose>
    Public Function InsertUpdateDelete_Expense(ByVal objDataOperation As clsDataOperation) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            If mdtExpense Is Nothing Then Return True

            For i = 0 To mdtExpense.Rows.Count - 1

                With mdtExpense.Rows(i)

                    objDataOperation.ClearParameters()

                    If Not IsDBNull(.Item("AUD")) Then

                        Select Case .Item("AUD")

                            Case "A"

Insert:
                                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
                                objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("description").ToString)
                                objDataOperation.AddParameter("@qty", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("qty").ToString)
                                objDataOperation.AddParameter("@unitcost", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("unitcost").ToString)
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("amount").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


                                strQ = "INSERT INTO lvleaveexpense ( " & _
                                  "  formunkid " & _
                                  ", description " & _
                                  ", qty " & _
                                  ", unitcost " & _
                                  ", amount " & _
                                  ", userunkid " & _
                                  ", loginemployeeunkid " & _
                                  ", isvoid " & _
                                  ", voiduserunkid " & _
                                  ", voiddatetime " & _
                                  ", voidreason" & _
                                ") VALUES (" & _
                                  "  @formunkid " & _
                                  ", @description " & _
                                  ", @qty " & _
                                  ", @unitcost " & _
                                  ", @amount " & _
                                  ", @userunkid " & _
                                  ", @loginemployeeunkid " & _
                                  ", @isvoid " & _
                                  ", @voiduserunkid " & _
                                  ", @voiddatetime " & _
                                  ", @voidreason" & _
                                "); SELECT @@identity"


                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintLvexpenseunkid = dsList.Tables(0).Rows(0).Item(0)


                                'Pinkal (25-APR-2012) -- Start
                                'Enhancement : TRA Changes

                                'If CInt(.Item("formunkid")) > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", CInt(.Item("formunkid")), "lvleaveexpense", "lvexpenseunkid", mintLvexpenseunkid, 2, 1) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'Else
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", mintFormunkid, "lvleaveexpense", "lvexpenseunkid", mintLvexpenseunkid, 1, 1) = False Then
                                '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If

                                If CInt(.Item("formunkid")) > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", CInt(.Item("formunkid")), "lvleaveexpense", "lvexpenseunkid", mintLvexpenseunkid, 2, 1, False, mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", mintFormunkid, "lvleaveexpense", "lvexpenseunkid", mintLvexpenseunkid, 1, 1, False, mintUserunkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                'Pinkal (25-APR-2012) -- End

                            Case "U"

                                If .Item("GUID").ToString() <> "" Then
                                    GoTo Insert
                                End If

                                strQ = " UPDATE lvleaveexpense SET " & _
                                           "  formunkid = @formunkid" & _
                                           ", description = @description" & _
                                           ", qty = @qty" & _
                                           ", unitcost = @unitcost " & _
                                           ", amount = @amount " & _
                                           ", userunkid = @userunkid" & _
                                           ", loginemployeeunkid = @loginemployeeunkid" & _
                                           ", isvoid = @isvoid" & _
                                           ", voiduserunkid = @voiduserunkid" & _
                                           ", voiddatetime = @voiddatetime" & _
                                           ", voidreason = @voidreason " & _
                                         " WHERE lvexpenseunkid = @lvexpenseunkid "

                                objDataOperation.AddParameter("@lvexpenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("lvexpenseunkid").ToString)
                                objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormunkid.ToString)
                                objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("description").ToString)
                                objDataOperation.AddParameter("@qty", SqlDbType.Float, eZeeDataType.MONEY_SIZE, .Item("qty").ToString)
                                objDataOperation.AddParameter("@unitcost", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("unitcost").ToString)
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, .Item("amount").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Pinkal (25-APR-2012) -- Start
                                'Enhancement : TRA Changes

                                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", CInt(.Item("formunkid")), "lvleaveexpense", "lvexpenseunkid", CInt(.Item("lvexpenseunkid")), 2, 2) = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", CInt(.Item("formunkid")), "lvleaveexpense", "lvexpenseunkid", CInt(.Item("lvexpenseunkid")), 2, 2, False, mintUserunkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Pinkal (25-APR-2012) -- End


                            Case "D"


                                'Pinkal (07-APR-2012) -- Start
                                'Enhancement : TRA Changes

                                'strQ = "UPDATE lvleaveexpense SET " & _
                                '        " isvoid = @isvoid" & _
                                '        ", voiduserunkid = @voiduserunkid" & _
                                '        ", voiddatetime = @voiddatetime" & _
                                '        ", voidreason = @voidreason " & _
                                '      "WHERE lvexpenseunkid = @lvexpenseunkid "

                                strQ = "UPDATE lvleaveexpense SET " & _
                                        " isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                   ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                                      "WHERE lvexpenseunkid = @lvexpenseunkid "

                                'Pinkal (07-APR-2012) -- End

                                objDataOperation.AddParameter("@lvexpenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("lvexpenseunkid")).ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True.ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                                'Pinkal (07-APR-2012) -- Start
                                'Enhancement : TRA Changes

                                If mintVoidlogingemployeeunkid > 0 Then
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                Else

                                    'Pinkal (25-APR-2012) -- Start
                                    'Enhancement : TRA Changes
                                    'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid.ToString)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                    'Pinkal (25-APR-2012) -- End


                                End If

                                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidlogingemployeeunkid.ToString)

                                'Pinkal (07-APR-2012) -- End

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Pinkal (25-APR-2012) -- Start
                                'Enhancement : TRA Changes

                                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", CInt(.Item("formunkid")), "lvleaveexpense", "lvexpenseunkid", CInt(.Item("lvexpenseunkid")), 2, 3) = False Then
                                '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                '    Throw exForce
                                'End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", CInt(.Item("formunkid")), "lvleaveexpense", "lvexpenseunkid", CInt(.Item("lvexpenseunkid")), 2, 3, False, mintUserunkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Pinkal (25-APR-2012) -- End


                        End Select

                    End If

                End With

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_Expense; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvleaveday_fraction) </purpose>
    Public Function Delete(ByVal intFormunkid As Integer, ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try


            strQ = "Select * from lvleaveexpense where formunkid = @formunkid and isvoid = 0"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@formunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then


                'Pinkal (07-APR-2012) -- Start
                'Enhancement : TRA Changes

                'strQ = "UPDATE lvleaveexpense SET " & _
                '         " isvoid = @isvoid" & _
                '         ", voiduserunkid = @voiduserunkid" & _
                '         ", voiddatetime = @voiddatetime" & _
                '         ", voidreason = @voidreason " & _
                '        " WHERE lvexpenseunkid = @lvexpenseunkid "

                strQ = "UPDATE lvleaveexpense SET " & _
                         " isvoid = @isvoid" & _
                         ", voiduserunkid = @voiduserunkid" & _
                         ", voiddatetime = @voiddatetime" & _
                         ", voidreason = @voidreason " & _
                       ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                        " WHERE lvexpenseunkid = @lvexpenseunkid "

                'Pinkal (07-APR-2012) -- End


                For Each dr As DataRow In dsList.Tables(0).Rows
                    objDataOperation.ClearParameters()

                    objDataOperation.AddParameter("@lvexpenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dr("lvexpenseunkid")).ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                    'Pinkal (07-APR-2012) -- Start
                    'Enhancement : TRA Changes

                    If mintVoidlogingemployeeunkid > 0 Then
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                    Else
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid.ToString)
                    End If

                    objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidlogingemployeeunkid.ToString)

                    'Pinkal (07-APR-2012) -- End

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    'Pinkal (25-APR-2012) -- Start
                    'Enhancement : TRA Changes

                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", CInt(dr("formunkid")), "lvleaveexpense", "lvexpenseunkid", CInt(dr("lvexpenseunkid")), 3, 3) = False Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If


                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "lvleaveform", "formunkid", CInt(dr("formunkid")), "lvleaveexpense", "lvexpenseunkid", CInt(dr("lvexpenseunkid")), 3, 3, False, mintUserunkid) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    'Pinkal (25-APR-2012) -- End

                Next

            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'Pinkal (07-APR-2012) -- Start
            'Enhancement : TRA Changes


            'StrQ = "SELECT " & _
            '  "  lvexpenseunkid " & _
            '  ", formunkid " & _
            '  ", description " & _
            '  ", qty " & _
            '  ", unitcost " & _
            '  ", amount " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            ' "FROM lvleaveexpense " & _
            ' "WHERE name = @name " & _
            ' "AND code = @code "

            strQ = "SELECT " & _
              "  lvexpenseunkid " & _
              ", formunkid " & _
              ", description " & _
              ", qty " & _
              ", unitcost " & _
              ", amount " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             ", voidloginemployeeunkid " & _
             "FROM lvleaveexpense " & _
             "WHERE name = @name " & _
             "AND code = @code "

            'Pinkal (07-APR-2012) -- End

            If intUnkid > 0 Then
                strQ &= " AND lvexpenseunkid <> @lvexpenseunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@lvexpenseunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

End Class