﻿'************************************************************************************************************************************
'Class Name : clsholiday_master.vb
'Purpose    : All Holiday Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :28/06/2010
'Written By :Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib

Public Enum HolidayType
    Recurrent = 1
    OnceEffect = 2
End Enum

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsholiday_master
    Private Shared ReadOnly mstrModuleName As String = "clsholiday_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintHolidayunkid As Integer
    Private mstrHolidayname As String = String.Empty
    Private mdtHolidaydate As Date
    Private mintYearunkid As Integer
    Private mintPeriodunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mintHolidaytypeid As Integer
    Private mintColor As Integer
    Private mblnIsactive As Boolean = True
    Private mstrHolidayname1 As String = String.Empty
    Private mstrHolidayname2 As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set holidayunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Holidayunkid(Optional ByVal xDataOperation As clsDataOperation = Nothing) As Integer
        'S.SANDEEP [19 OCT 2016] -- START {xDataOperation} -- END
        Get
            Return mintHolidayunkid
        End Get
        Set(ByVal value As Integer)
            mintHolidayunkid = value
            Call GetData(xDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set holidayname
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Holidayname() As String
        Get
            Return mstrHolidayname
        End Get
        Set(ByVal value As String)
            mstrHolidayname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set holidaydate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Holidaydate() As Date
        Get
            Return mdtHolidaydate
        End Get
        Set(ByVal value As Date)
            mdtHolidaydate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set holidaytypeid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Holidaytypeid() As Integer
        Get
            Return mintHolidaytypeid
        End Get
        Set(ByVal value As Integer)
            mintHolidaytypeid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set color
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Color() As Integer
        Get
            Return mintColor
        End Get
        Set(ByVal value As Integer)
            mintColor = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set holidayname1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Holidayname1() As String
        Get
            Return mstrHolidayname1
        End Get
        Set(ByVal value As String)
            mstrHolidayname1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set holidayname2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Holidayname2() As String
        Get
            Return mstrHolidayname2
        End Get
        Set(ByVal value As String)
            mstrHolidayname2 = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOperation As clsDataOperation = Nothing) 'S.SANDEEP [19 OCT 2016] -- START {xDataOperation} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [19 OCT 2016] -- START
        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
        'objDataOperation = New clsDataOperation
        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [19 OCT 2016] -- END

        Try
            strQ = "SELECT " & _
                   "  holidayunkid " & _
                   ", holidayname " & _
                   ", holidaydate " & _
                   ", yearunkid " & _
                   ", periodunkid " & _
                   ", description " & _
                   ", holidaytypeid " & _
                   ", color " & _
                   ", isactive " & _
                   ", holidayname1 " & _
                   ", holidayname2 " & _
                 "FROM lvholiday_master " & _
                 "WHERE holidayunkid = @holidayunkid "

            objDataOperation.AddParameter("@holidayunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintHolidayUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintholidayunkid = CInt(dtRow.Item("holidayunkid"))
                mstrholidayname = dtRow.Item("holidayname").ToString
                mdtholidaydate = dtRow.Item("holidaydate")
                mintyearunkid = CInt(dtRow.Item("yearunkid"))
                mintperiodunkid = CInt(dtRow.Item("periodunkid"))
                mstrdescription = dtRow.Item("description").ToString
                mintholidaytypeid = CInt(dtRow.Item("holidaytypeid"))
                mintcolor = CInt(dtRow.Item("color"))
                mblnisactive = CBool(dtRow.Item("isactive"))
                mstrholidayname1 = dtRow.Item("holidayname1").ToString
                mstrholidayname2 = dtRow.Item("holidayname2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [19 OCT 2016] -- END
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal xDataOperation As clsDataOperation = Nothing) As DataSet 'S.SANDEEP [19 OCT 2016] -- START {xDataOperation} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [19 OCT 2016] -- START
        'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
        'objDataOperation = New clsDataOperation
        If xDataOperation IsNot Nothing Then
            objDataOperation = xDataOperation
        Else
        objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'S.SANDEEP [19 OCT 2016] -- END

        Try

            'Sandeep [ 04 NOV 2010 ] -- Start
            'strQ = "SELECT " & _
            '       "  lvholiday_master.holidayunkid " & _
            '       ", lvholiday_master.holidayname " & _
            '       ", convert(Char(8),lvholiday_master.holidaydate,112) As holidaydate " & _
            '              ", lvholiday_master.yearunkid " & _
            '              ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name as yearname" & _
            '              ", lvholiday_master.periodunkid " & _
            '              ", cfcommon_period_tran.period_name as periodname" & _
            '       ", lvholiday_master.description " & _
            '       ", lvholiday_master.holidaytypeid " & _
            '       ", lvholiday_master.color " & _
            '              ", lvholiday_master.isactive " & _
            '       ", lvholiday_master.holidayname1 " & _
            '       ", lvholiday_master.holidayname2 " & _
            '             "FROM lvholiday_master" & _
            '             " LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran on cffinancial_year_tran.yearunkid = lvholiday_master.yearunkid " & _
            '             " LEFT JOIN cfcommon_period_tran  on cfcommon_period_tran.periodunkid = lvholiday_master.periodunkid "
            'Sandeep [ 04 NOV 2010 ] -- End 

            'Pinkal (10-Mar-2011) -- Start

            strQ = "SELECT " & _
                   "  lvholiday_master.holidayunkid " & _
                   ", lvholiday_master.holidayname " & _
                   ", convert(Char(8),lvholiday_master.holidaydate,112) As holidaydate " & _
                   ", lvholiday_master.yearunkid " & _
                   ", " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran.financialyear_name as yearname" & _
                   ", lvholiday_master.periodunkid " & _
                   ", cfcommon_period_tran.period_name as periodname" & _
                   ", lvholiday_master.description " & _
                   ", lvholiday_master.holidaytypeid " & _
                ", case when lvholiday_master.holidaytypeid  = 1 then 'Recurrent' " & _
                        " when  lvholiday_master.holidaytypeid  = 2 then 'Once Effect' end as holidaytype " & _
                   ", lvholiday_master.color " & _
                   ", lvholiday_master.isactive " & _
                   ", lvholiday_master.holidayname1 " & _
                   ", lvholiday_master.holidayname2 " & _
                   "FROM lvholiday_master " & _
                   "  LEFT JOIN " & FinancialYear._Object._ConfigDatabaseName & "..cffinancial_year_tran on cffinancial_year_tran.yearunkid = lvholiday_master.yearunkid " & _
                   "  LEFT JOIN cfcommon_period_tran  on cfcommon_period_tran.periodunkid = lvholiday_master.periodunkid "


            'Pinkal (10-Mar-2011) -- End

            If blnOnlyActive Then
                strQ &= " WHERE lvholiday_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'S.SANDEEP [19 OCT 2016] -- START
            'ISSUE : EMPLOYEE MASTER BIND TRANSACTION ISSUE
            'objDataOperation = Nothing
            If xDataOperation Is Nothing Then objDataOperation = Nothing
            'S.SANDEEP [19 OCT 2016] -- END
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvholiday_master) </purpose>
    Public Function Insert() As Boolean

        'Pinkal (10-Jan-2020) -- Start
        'Bug [0004407] Crown Paints -  Working on Leave Holiday Master Issue.
        'If isExist(mstrHolidayname, mdtHolidaydate.Date) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Holiday Name is already defined. Please define new Holiday Name.")
        '    Return False
        'End If

        If isExist(mstrHolidayname, Nothing) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Holiday Name is already defined. Please define new Holiday Name.")
            Return False
        End If

        If isExist("", mdtHolidaydate.Date) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Holiday Date is already defined. Please define new Holiday Date.")
            Return False
        End If

        'Pinkal (10-Jan-2020) -- End
        

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            objDataOperation.AddParameter("@holidayname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrholidayname.ToString)
            objDataOperation.AddParameter("@holidaydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtHolidaydate)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintyearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@holidaytypeid", SqlDbType.int, eZeeDataType.INT_SIZE, mintholidaytypeid.ToString)
            objDataOperation.AddParameter("@color", SqlDbType.int, eZeeDataType.INT_SIZE, mintcolor.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@holidayname1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrholidayname1.ToString)
            objDataOperation.AddParameter("@holidayname2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrholidayname2.ToString)

            StrQ = "INSERT INTO lvholiday_master ( " & _
              "  holidayname " & _
              ", holidaydate " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", description " & _
              ", holidaytypeid " & _
              ", color " & _
              ", isactive " & _
              ", holidayname1 " & _
              ", holidayname2" & _
            ") VALUES (" & _
              "  @holidayname " & _
              ", @holidaydate " & _
              ", @yearunkid " & _
              ", @periodunkid " & _
              ", @description " & _
              ", @holidaytypeid " & _
              ", @color " & _
              ", @isactive " & _
              ", @holidayname1 " & _
              ", @holidayname2" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintHolidayunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lvholiday_master", "holidayunkid", mintHolidayunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvholiday_master) </purpose>
    Public Function Update() As Boolean


        'Pinkal (10-Jan-2020) -- Start
        'Bug [0004407] Crown Paints -  Working on Leave Holiday Master Issue.
        'If isExist(mstrHolidayname, mdtHolidaydate.Date, mintHolidayunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Holiday Name is already defined. Please define new Holiday Name.")
        '    Return False
        'End If

        If isExist(mstrHolidayname, Nothing, mintHolidayunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Holiday Name is already defined. Please define new Holiday Name.")
            Return False
        End If

        If isExist("", mdtHolidaydate.Date, mintHolidayunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Holiday Date is already defined. Please define new Holiday Date.")
            Return False
        End If

        'Pinkal (10-Jan-2020) -- End
        

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@holidayunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintholidayunkid.ToString)
            objDataOperation.AddParameter("@holidayname", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrholidayname.ToString)
            objDataOperation.AddParameter("@holidaydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtHolidaydate)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintyearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@holidaytypeid", SqlDbType.int, eZeeDataType.INT_SIZE, mintholidaytypeid.ToString)
            objDataOperation.AddParameter("@color", SqlDbType.int, eZeeDataType.INT_SIZE, mintcolor.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@holidayname1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrholidayname1.ToString)
            objDataOperation.AddParameter("@holidayname2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrholidayname2.ToString)

            StrQ = "UPDATE lvholiday_master SET " & _
              "  holidayname = @holidayname" & _
              ", holidaydate = @holidaydate" & _
              ", yearunkid = @yearunkid" & _
              ", periodunkid = @periodunkid" & _
              ", description = @description" & _
              ", holidaytypeid = @holidaytypeid" & _
              ", color = @color" & _
              ", isactive = @isactive" & _
              ", holidayname1 = @holidayname1" & _
              ", holidayname2 = @holidayname2 " & _
            "WHERE holidayunkid = @holidayunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "lvholiday_master", mintHolidayunkid, "holidayunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvholiday_master", "holidayunkid", mintHolidayunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvholiday_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Holiday. Reason: This Holiday is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            strQ = "Update lvholiday_master set isactive = 0 " & _
            "WHERE holidayunkid = @holidayunkid "

            objDataOperation.AddParameter("@holidayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lvholiday_master", "holidayunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'Pinkal (12-Oct-2011) -- End

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "select isnull(holidayunkid,0) from lvemployee_holiday where holidayunkid = @holidayunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@holidayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, ByVal mdtDate As Date, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  holidayunkid " & _
              ", holidayname " & _
              ", holidaydate " & _
              ", yearunkid " & _
              ", periodunkid " & _
              ", description " & _
              ", holidaytypeid " & _
              ", color " & _
              ", isactive " & _
              ", holidayname1 " & _
              ", holidayname2 " & _
             "FROM lvholiday_master " & _
                     " WHERE isactive = 1 "

            If intUnkid > 0 Then
                strQ &= " AND holidayunkid <> @holidayunkid"
            End If


            'Pinkal (10-Jan-2020) -- Start
            'Bug [0004407] Crown Paints -  Working on Leave Holiday Master Issue.
            If strName.Trim.Length > 0 Then
                strQ &= " AND holidayname = @holidayname "
            End If
            'Pinkal (10-Jan-2020) -- End


            If mdtDate <> Nothing Then
                strQ &= " AND Convert(char(8),holidaydate,112) = @holidaydate"
                objDataOperation.AddParameter("@holidaydate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtDate.Date))
            End If


            objDataOperation.AddParameter("@holidayname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@holidayunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    'Sohail (29 Aug 2019) -- Start
    'NMB Payroll UAT # TC008 - 76.1 - System should be able to restrict batch posting when it is holiday unless it is activated by user to allow batch posting on holidays.
    Public Function isHoliday(ByVal dtDate As Date, Optional ByVal strFilter As String = "") As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  holidayunkid " & _
             "FROM lvholiday_master " & _
             "WHERE isactive = 1 "

            If dtDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), holidaydate, 112) = @holidaydate "
                objDataOperation.AddParameter("@holidaydate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtDate.Date))
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isHoliday; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (29 Aug 2019) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetHolidayColor(ByVal mdtHolidaydate As DateTime) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'strQ = "SELECT " & _
            '  " 1 as Holiday,isnull(color,0) as color " & _
            ' "FROM lvholiday_master " & _
            ' "WHERE holidaydate = @holidaydate"

            'Pinkal (24-Jan-2011) -- Start

            strQ = "SELECT " & _
              " 1 as Holiday,isnull(color,0) as color " & _
             "FROM lvholiday_master " & _
             "WHERE CONVERT(VARCHAR(8),holidaydate,112) = @holidaydate"

            'Pinkal (24-Jan-2011) -- End

            objDataOperation.AddParameter("@holidaydate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtHolidaydate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                Return CInt(dsList.Tables("List").Rows(0)("color"))
            End If
            'Return dsList

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetHolidayColor; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetHolidayUnkId(ByVal mstrHolidayName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              " Holidayunkid " & _
             "FROM lvholiday_master " & _
             "WHERE holidayname = @holidayname"

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@holidayname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHolidayName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                Return CInt(dsList.Tables("List").Rows(0)("Holidayunkid"))
            End If
            'Return dsList

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetHolidayUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Holiday Name is already defined. Please define new Holiday Name.")
			Language.setMessage(mstrModuleName, 2, "This Holiday Date is already defined. Please define new Holiday Date.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class