﻿'************************************************************************************************************************************
'Class Name : clsleavetype_master.vb
'Purpose    : All Leave Type Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       : 27/06/2010
'Written By : Pinkal
'Modified   :
'Last Message Index = 3
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Windows.Forms

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
''' 

Public Enum AccrueType
    Daily = 1
    Monthly = 2
    Annualy = 3
End Enum


Public Class clsleavetype_master
    Private Shared ReadOnly mstrModuleName As String = "clsleavetype_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLeavetypeunkid As Integer
    Private mstrLeavetypecode As String = String.Empty
    Private mstrLeavename As String = String.Empty
    Private mblnIsaccrued As Boolean
    Private mblnIsPaid As Boolean
    Private mblnIsissueonholiday As Boolean
    Private mblnIsAccrueAmount As Boolean
    Private mintMaxAmount As Integer
    Private mintColor As Integer
    Private mstrDescription As String
    Private mblnIsactive As Boolean = True
    Private mstrLeavename1 As String = String.Empty
    Private mstrLeavename2 As String = String.Empty
    Private mblnIsShortLeave As Boolean
    Private mintGender As Integer
    Private mintDeductFromLeaveTypeunkid As Integer

    'Pinkal (06-Feb-2013) -- Start
    'Enhancement : TRA Changes
    Private mdclCFAmount As Decimal = 0
    'Pinkal (06-Feb-2013) -- End


    'Pinkal (24-Feb-2013) -- Start
    'Enhancement : TRA Changes

    'Pinkal (29-Sep-2017) -- Start
    'Enhancement - Working Leave Expense Eligility Option for PACRA.
    'Private mintEligibilityAfter As Integer = 0
    Private mintEligibilityOnAfter_Expense As Integer = 0
    'Pinkal (29-Sep-2017) -- End

    'Pinkal (24-Feb-2013) -- End


    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes
    Private mblnNoAction As Boolean = False
    'Pinkal (24-May-2013) -- End

    'S.SANDEEP [ 26 SEPT 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mblnIsSickLeave As Boolean = False
    'S.SANDEEP [ 26 SEPT 2013 ] -- END


    'Pinkal (03-Jan-2014) -- Start
    'Enhancement : Oman Changes
    Private mblncheckDocOnLeaveForm As Boolean = False
    Private mblncheckDocOnLeaveIssue As Boolean = False
    'Pinkal (03-Jan-2014) -- End


    'Pinkal (31-Jan-2014) -- Start
    'Enhancement : Oman Changes
    Private mblnIsLeaveEncashment As Boolean = False
    'Pinkal (31-Jan-2014) -- End


    'Pinkal (01-Feb-2014) -- Start
    'Enhancement : TRA Changes
    Private mblnIsissueonweekend As Boolean = False
    Private mblnIsLeaveHL_onWK As Boolean = False
    'Pinkal (01-Feb-2014) -- End

    'Pinkal (02-Dec-2015) -- Start
    'Enhancement - Solving Leave bug in Self Service.
    Private objDoOperation As clsDataOperation = Nothing
    'Pinkal (02-Dec-2015) -- End


    'Pinkal (28-Nov-2017) -- Start
    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
    Private mblnCancelLvFormDocmandatory As Boolean = False
    'Pinkal (28-Nov-2017) -- End


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mblnSkipApproverFlow As Boolean = False
    'Pinkal (01-Oct-2018) -- End


    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.
    Private mintLvDaysMinLimit As Integer = 0
    Private mintLvDaysMaxLimit As Integer = 0
    'Pinkal (08-Oct-2018) -- End

    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
    Private mblnConsiderLeaveOnTnAPeriod As Boolean = False
    'Pinkal (01-Jan-2019) -- End


    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
    Private mblnIsBlockLeave As Boolean = False
    Private mblnIsStopApplyAcrossFYELC As Boolean = False
    'Pinkal (26-Feb-2019) -- End


    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.
    Private mblnConsiderExpense As Boolean = True
    'Pinkal (03-May-2019) -- End


    'Pinkal (25-May-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.
    Private mblnShowOnESS As Boolean = True
    'Pinkal (25-May-2019) -- End

    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Private mblnIsexempt_from_payroll As Boolean = False
    'Sohail (21 Oct 2019) -- End

    'Pinkal (05-Jun-2020) -- Start
    'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
    Private mblnDoNotApplyForFutureDate As Boolean = False
    'Pinkal (05-Jun-2020) -- End


    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA.
    Private mblnIsExcuseLeave As Boolean = False
    'Pinkal (14-Feb-2022) -- End


    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA.
    Private mintMaxFutureDaysLimit As Integer = 0
    'Pinkal (14-Feb-2022) -- End

    'Pinkal (20-Feb-2023) -- Start
    '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.
    Private mdtOldLeavePay As DataTable = Nothing
    Private mdtLeavePay As DataTable = Nothing
    Private mstrLeaveNotificationUserIds As String = ""
    Private objLeavePay As New clsleavepaysettings
    Private mintUserunkId As Integer = 0
    Private mstrClientIP As String = ""
    Private mstrHostName As String = ""
    Private mstrWebFormName As String = ""
    Private mblnIsWeb As Boolean = False
    'Pinkal (20-Feb-2023) -- End

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavetypeunkid() As Integer
        Get
            Return mintLeavetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintLeavetypeunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypecode
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavetypecode() As String
        Get
            Return mstrLeavetypecode
        End Get
        Set(ByVal value As String)
            mstrLeavetypecode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavename
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavename() As String
        Get
            Return mstrLeavename
        End Get
        Set(ByVal value As String)
            mstrLeavename = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isaccrued
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isaccrued() As Boolean
        Get
            Return mblnIsaccrued
        End Get
        Set(ByVal value As Boolean)
            mblnIsaccrued = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isissueonholiday
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isissueonholiday() As Boolean
        Get
            Return mblnIsissueonholiday
        End Get
        Set(ByVal value As Boolean)
            mblnIsissueonholiday = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isAccrueAmount
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsAccrueAmount() As Boolean
        Get
            Return mblnIsAccrueAmount
        End Get
        Set(ByVal value As Boolean)
            mblnIsAccrueAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _MaxAmount() As Integer
        Get
            Return mintMaxAmount
        End Get
        Set(ByVal value As Integer)
            mintMaxAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ispaid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsPaid() As Boolean
        Get
            Return mblnIsPaid
        End Get
        Set(ByVal value As Boolean)
            mblnIsPaid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set color
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Color() As Integer
        Get
            Return mintColor
        End Get
        Set(ByVal value As Integer)
            mintColor = value
        End Set
    End Property

    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavename1
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavename1() As String
        Get
            Return mstrLeavename1
        End Get
        Set(ByVal value As String)
            mstrLeavename1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavename2
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Leavename2() As String
        Get
            Return mstrLeavename2
        End Get
        Set(ByVal value As String)
            mstrLeavename2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set shortLeave
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsShortLeave() As Boolean
        Get
            Return mblnIsShortLeave
        End Get
        Set(ByVal value As Boolean)
            mblnIsShortLeave = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gender
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Gender() As Integer
        Get
            Return mintGender
        End Get
        Set(ByVal value As Integer)
            mintGender = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set deductleavetypeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DeductFromLeaveTypeunkid() As Integer
        Get
            Return mintDeductFromLeaveTypeunkid
        End Get
        Set(ByVal value As Integer)
            mintDeductFromLeaveTypeunkid = value
        End Set
    End Property


    'Pinkal (06-Feb-2013) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set cfamount
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _CFAmount() As Decimal
        Get
            Return mdclCFAmount
        End Get
        Set(ByVal value As Decimal)
            mdclCFAmount = value
        End Set
    End Property

    'Pinkal (06-Feb-2013) -- End


    'Pinkal (24-Feb-2013) -- Start
    'Enhancement : TRA Changes


    'Pinkal (29-Sep-2017) -- Start
    'Enhancement - Working Leave Expense Eligility Option for PACRA.

    '' <summary>
    '' Purpose: Get or Set eligibilityafter
    '' Modify By: Pinkal
    '' </summary>
    '' 
    'Public Property _EligibilityAfter() As Integer
    '    Get
    '        Return mintEligibilityAfter
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintEligibilityAfter = value
    '    End Set
    'End Property

    Public Property _EligibilityOnAfter_Expense() As Integer
        Get
            Return mintEligibilityOnAfter_Expense
        End Get
        Set(ByVal value As Integer)
            mintEligibilityOnAfter_Expense = value
        End Set
    End Property

    'Pinkal (29-Sep-2017) -- End

    'Pinkal (24-Feb-2013) -- End


    'Pinkal (24-May-2013) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set IsNoAction
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _IsNoAction() As Boolean
        Get
            Return mblnNoAction
        End Get
        Set(ByVal value As Boolean)
            mblnNoAction = value
        End Set
    End Property

    'Pinkal (24-May-2013) -- End

    'S.SANDEEP [ 26 SEPT 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Property _IsSickLeave() As Boolean
        Get
            Return mblnIsSickLeave
        End Get
        Set(ByVal value As Boolean)
            mblnIsSickLeave = value
        End Set
    End Property
    'S.SANDEEP [ 26 SEPT 2013 ] -- END


    'Pinkal (03-Jan-2014) -- Start
    'Enhancement : Oman Changes

    ''' <summary>
    ''' Purpose: Get or Set IsCheckDocOnLeaveForm
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _IsCheckDocOnLeaveForm() As Boolean
        Get
            Return mblncheckDocOnLeaveForm
        End Get
        Set(ByVal value As Boolean)
            mblncheckDocOnLeaveForm = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsCheckDocOnLeaveIssue
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _IsCheckDocOnLeaveIssue() As Boolean
        Get
            Return mblncheckDocOnLeaveIssue
        End Get
        Set(ByVal value As Boolean)
            mblncheckDocOnLeaveIssue = value
        End Set
    End Property

    'Pinkal (03-Jan-2014) -- End


    'Pinkal (31-Jan-2014) -- Start
    'Enhancement : Oman Changes

    ''' <summary>
    ''' Purpose: Get or Set IsLeaveEncashment
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _IsLeaveEncashment() As Boolean
        Get
            Return mblnIsLeaveEncashment
        End Get
        Set(ByVal value As Boolean)
            mblnIsLeaveEncashment = value
        End Set
    End Property

    'Pinkal (31-Jan-2014) -- End


    'Pinkal (01-Feb-2014) -- Start
    'Enhancement : TRA Changes

    ''' <summary>
    ''' Purpose: Get or Set isissueonweekend
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isissueonweekend() As Boolean
        Get
            Return mblnIsissueonweekend
        End Get
        Set(ByVal value As Boolean)
            mblnIsissueonweekend = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsLeaveHL_onWK
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _IsLeaveHL_onWK() As Boolean
        Get
            Return mblnIsLeaveHL_onWK
        End Get
        Set(ByVal value As Boolean)
            mblnIsLeaveHL_onWK = value
        End Set
    End Property

    'Pinkal (01-Feb-2014) -- End

    'Pinkal (02-Dec-2015) -- Start
    'Enhancement - Solving Leave bug in Self Service.

    ''' <summary>
    ''' Purpose: Get or Set ObjDoOperation
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _DoOperation() As clsDataOperation
        Get
            Return objDoOperation
        End Get
        Set(ByVal value As clsDataOperation)
            objDoOperation = value
        End Set
    End Property

    'Pinkal (02-Dec-2015) -- End


    'Pinkal (28-Nov-2017) -- Start
    'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .

    ''' <summary>
    ''' Purpose: Get or Set _CancelLvFormDocMandatory
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _CancelLvFormDocMandatory() As Boolean
        Get
            Return mblnCancelLvFormDocmandatory
        End Get
        Set(ByVal value As Boolean)
            mblnCancelLvFormDocmandatory = value
        End Set
    End Property

    'Pinkal (28-Nov-2017) -- End


    'Pinkal (01-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Get or Set SkipApproverFlow
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _SkipApproverFlow() As Boolean
        Get
            Return mblnSkipApproverFlow
        End Get
        Set(ByVal value As Boolean)
            mblnSkipApproverFlow = value
        End Set
    End Property

    'Pinkal (01-Oct-2018) -- End


    'Pinkal (08-Oct-2018) -- Start
    'Enhancement - Leave Enhancement for NMB.

    ''' <summary>
    ''' Purpose: Get or Set LVDaysMinLimit
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _LVDaysMinLimit() As Integer
        Get
            Return mintLvDaysMinLimit
        End Get
        Set(ByVal value As Integer)
            mintLvDaysMinLimit = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LVDaysMaxLimit
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _LVDaysMaxLimit() As Integer
        Get
            Return mintLvDaysMaxLimit
        End Get
        Set(ByVal value As Integer)
            mintLvDaysMaxLimit = value
        End Set
    End Property

    'Pinkal (08-Oct-2018) -- End

    'Pinkal (01-Jan-2019) -- Start
    'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.

    ''' <summary>
    ''' Purpose: Get or Set ConsiderLeaveOnTnAPeriod
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _ConsiderLeaveOnTnAPeriod() As Boolean
        Get
            Return mblnConsiderLeaveOnTnAPeriod
        End Get
        Set(ByVal value As Boolean)
            mblnConsiderLeaveOnTnAPeriod = value
        End Set
    End Property

    'Pinkal (01-Jan-2019) -- End

    'Pinkal (26-Feb-2019) -- Start
    'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.

    ''' <summary>
    ''' Purpose: Get or Set IsBlockLeave
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _IsBlockLeave() As Boolean
        Get
            Return mblnIsBlockLeave
        End Get
        Set(ByVal value As Boolean)
            mblnIsBlockLeave = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsApplyAcrossFYELC
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _IsStopApplyAcrossFYELC() As Boolean
        Get
            Return mblnIsStopApplyAcrossFYELC
        End Get
        Set(ByVal value As Boolean)
            mblnIsStopApplyAcrossFYELC = value
        End Set
    End Property

    'Pinkal (26-Feb-2019) -- End


    'Pinkal (03-May-2019) -- Start
    'Enhancement - Working on Leave UAT Changes for NMB.

    ''' <summary>
    ''' Purpose: Get or Set ConsiderExpense
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _ConsiderExpense() As Boolean
        Get
            Return mblnConsiderExpense
        End Get
        Set(ByVal value As Boolean)
            mblnConsiderExpense = value
        End Set
    End Property

    'Pinkal (03-May-2019) -- End


    'Pinkal (25-May-2019) -- Start
    'Enhancement - NMB FINAL LEAVE UAT CHANGES.

    ''' <summary>
    ''' Purpose: Get or Set ShowOnESS
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _ShowOnESS() As Boolean
        Get
            Return mblnShowOnESS
        End Get
        Set(ByVal value As Boolean)
            mblnShowOnESS = value
        End Set
    End Property

    'Pinkal (25-May-2019) -- End

    'Sohail (21 Oct 2019) -- Start
    'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
    Public Property _Isexempt_from_payroll() As Boolean
        Get
            Return mblnIsexempt_from_payroll
        End Get
        Set(ByVal value As Boolean)
            mblnIsexempt_from_payroll = value
        End Set
    End Property
    'Sohail (21 Oct 2019) -- End

    'Pinkal (05-Jun-2020) -- Start
    'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
    Public Property _DoNotApplyForFutureDates() As Boolean
        Get
            Return mblnDoNotApplyForFutureDate
        End Get
        Set(ByVal value As Boolean)
            mblnDoNotApplyForFutureDate = value
        End Set
    End Property
    'Pinkal (05-Jun-2020) -- End


    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA.

    ''' <summary>
    ''' Purpose: Get or Set IsBlockLeave
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _IsExcuseLeave() As Boolean
        Get
            Return mblnIsExcuseLeave
        End Get
        Set(ByVal value As Boolean)
            mblnIsExcuseLeave = value
        End Set
    End Property

    'Pinkal (14-Feb-2022) -- End

    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA.
    Public Property _MaxFutureDaysLimit() As Integer
        Get
            Return mintMaxFutureDaysLimit
        End Get
        Set(ByVal value As Integer)
            mintMaxFutureDaysLimit = value
        End Set
    End Property
    'Pinkal (14-Feb-2022) -- End


    'Pinkal (20-Feb-2023) -- Start
    '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.

    ''' <summary>
    ''' Purpose: Get or Set OlddtLeavePay
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _OlddtLeavePay() As DataTable
        Get
            Return mdtOldLeavePay
        End Get
        Set(ByVal value As DataTable)
            mdtOldLeavePay = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtLeavePay
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _dtLeavePay() As DataTable
        Get
            Return mdtLeavePay
        End Get
        Set(ByVal value As DataTable)
            mdtLeavePay = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set LeaveNotificationUserIds
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _LeaveNotificationUserIds() As String
        Get
            Return mstrLeaveNotificationUserIds
        End Get
        Set(ByVal value As String)
            mstrLeaveNotificationUserIds = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set UserId
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _UserId() As Integer
        Get
            Return mintUserunkId
        End Get
        Set(ByVal value As Integer)
            mintUserunkId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ClientIP() As String
        Get
            Return mstrClientIP
        End Get
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Isweb
    ''' Modify By: Pinkal
    ''' </summary>
    ''' 
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    'Pinkal (20-Feb-2023) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        objDataOperation.ClearParameters()

        Try


            strQ = "SELECT " & _
                     "  leavetypeunkid " & _
                     ", leavetypecode " & _
                     ", leavename " & _
                     ", description " & _
                     ", isaccrued " & _
                     ", accruetypeid " & _
                     ", isissueonholiday " & _
                     ", isnull(isaccrueamount,0) as isaccrueamount " & _
                     ", color " & _
                     ", isactive " & _
                     ", leavename1 " & _
                     ", leavename2 " & _
                     ", ISNULL(maxamount,0)maxamount " & _
                     ", ISNULL(ispaid,0) ispaid " & _
                    ", ISNULL(isshortleave,0) isshortleave " & _
                     ", ISNULL(gender,0) gender " & _
                       ", ISNULL(deductfromlvtypeunkid,0) deductfromlvtypeunkid " & _
                     ", ISNULL(cfamount,0.00) cfamount " & _
                     ", ISNULL(eligibility_expense,0) eligibility_expense " & _
                     ", ISNULL(isnoaction,0) isnoaction " & _
                     ", ISNULL(issickleave,0) AS issickleave " & _
                     ", ISNULL(ischeckdoconlvform,0) AS ischeckdoconlvform " & _
                     ", ISNULL(ischeckdoconlvissue,0) AS ischeckdoconlvissue " & _
                     ", ISNULL(isleaveencashment,0) AS isleaveencashment " & _
                     ", ISNULL(isissueonweekend,0) AS isissueonweekend " & _
                     ", ISNULL(isleave_hl_onwk,0)  AS isleave_hl_onwk " & _
                     ", ISNULL(islvcanceldocmandatory,0)  AS islvcanceldocmandatory " & _
                     ", ISNULL(skipapproverflow,0)  AS skipapproverflow " & _
                     ", ISNULL(lvform_mindays,0)  AS lvform_mindays " & _
                     ", ISNULL(lvform_maxdays,0)  AS lvform_maxdays " & _
                     ", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _
                     ", ISNULL(isblockleave,0) AS isblockleave " & _
                     ", ISNULL(isstopapplyacross_fyelc,0) AS isstopapplyacross_fyelc " & _
                     ", ISNULL(consider_expense,1) AS consider_expense " & _
                     ", ISNULL(isshowoness,1) AS isshowoness " & _
                     ", ISNULL(isexempt_from_payroll, 0) AS isexempt_from_payroll " & _
                     ", ISNULL(donotapplyfuturedate, 0) AS donotapplyfuturedate " & _
                     ", ISNULL(IsExcuseLeave, 0) AS IsExcuseLeave " & _
                     ", ISNULL(maxfuturedaylimit, 0) AS maxfuturedaylimit " & _
                     ", ISNULL(leavenotificationuserids, '') AS leavenotificationuserids " & _
                    " FROM lvleavetype_master " & _
                     " WHERE leavetypeunkid = @leavetypeunkid "

            'Pinkal (20-Feb-2023) -- '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.[ ", ISNULL(leavenotificationuserids, '') AS leavenotificationuserids " & _]	

            'Pinkal (20-Apr-2022) -- 'Enhancement Prevision Air  - Doing Leave module Enhancement[", ISNULL(maxfuturedaylimit, 0) AS maxfuturedaylimit " & _].	

            'Pinkal (14-Feb-2022) -- Enhancement TRA : TnA Module Enhancement for TRA. [ ", ISNULL(IsExcuseLeave, 0) AS IsExcuseLeave " & _]

            'Pinkal (05-Jun-2020) --Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.[", ISNULL(donotapplyfuturedate, 0) AS donotapplyfuturedate " & _]

            'Sohail (21 Oct 2019) - [isexempt_from_payroll]
            'Pinkal (25-May-2019) -- 'Enhancement - NMB FINAL LEAVE UAT CHANGES.[ ", ISNULL(isshowoness,1) AS isshowoness " & _]


            'Pinkal (03-May-2019) -- Enhancement - Working on Leave UAT Changes for NMB.[", ISNULL(consider_expense,1) AS consider_expense " & _]


            'Pinkal (01-Jan-2019) --'Enhancement [Ref #: 0003287] -  Working on Leave Frequency Change for Voltamp.[ ", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _]
            'Pinkal (26-Feb-2019) --  'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[ ", ISNULL(isblockleave,0) AS isblockleave , ISNULL(isstopapplyacross_fyelc,0) AS isstopapplyacross_fyelc " & _]

            'Pinkal (08-Oct-2018) --  'Enhancement - Leave Enhancement for NMB.[", ISNULL(lvform_mindays,0)  AS lvform_mindays, ISNULL(lvform_maxdays,0)  AS lvform_maxdays " & _]

            'Pinkal (01-Oct-2018) --  'Enhancement - Leave Enhancement for NMB.[ ", ISNULL(skipapproverflow,0)  AS skipapproverflow " & _]


            'Pinkal (29-Sep-2017) -- 'Enhancement - Working Leave Expense Eligility Option for PACRA.[  ", ISNULL(eligibility_expense,0) eligibility_expense "]


            'Pinkal (28-Nov-2017) --  'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            ' [", ISNULL(islvcanceldocmandatory,0)  AS islvcanceldocmandatory " & _]



            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLeavetypeunkid = CInt(dtRow.Item("leavetypeunkid"))
                mstrLeavetypecode = dtRow.Item("leavetypecode").ToString
                mstrLeavename = dtRow.Item("leavename").ToString
                mstrDescription = dtRow.Item("description").ToString
                mblnIsaccrued = CBool(dtRow.Item("isaccrued"))
                mblnIsissueonholiday = CBool(dtRow.Item("isissueonholiday"))
                mblnIsAccrueAmount = CBool(dtRow.Item("isaccrueamount"))
                mintColor = CInt(dtRow.Item("color"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrLeavename1 = dtRow.Item("leavename1").ToString
                mstrLeavename2 = dtRow.Item("leavename2").ToString
                mintMaxAmount = CInt(dtRow.Item("maxamount"))
                mblnIsPaid = CBool(dtRow.Item("ispaid"))
                mblnIsShortLeave = CBool(dtRow.Item("isshortleave"))
                mintGender = CInt(dtRow.Item("gender"))
                mintDeductFromLeaveTypeunkid = CInt(dtRow.Item("deductfromlvtypeunkid"))
                mdclCFAmount = CDec(dtRow.Item("cfamount"))

                'Pinkal (29-Sep-2017) -- Start
                'Enhancement - Working Leave Expense Eligility Option for PACRA.
                'mintEligibilityAfter = CInt(dtRow.Item("eligibilityafter"))
                mintEligibilityOnAfter_Expense = CInt(dtRow.Item("eligibility_expense"))
                'Pinkal (29-Sep-2017) -- End

                mblnNoAction = CBool(dtRow.Item("isnoaction"))
                mblnIsSickLeave = CBool(dtRow.Item("issickleave"))
                mblncheckDocOnLeaveForm = CBool(dtRow.Item("ischeckdoconlvform"))
                mblncheckDocOnLeaveIssue = CBool(dtRow.Item("ischeckdoconlvissue"))
                mblnIsLeaveEncashment = CBool(dtRow.Item("isleaveencashment"))
                mblnIsissueonweekend = CBool(dtRow.Item("isissueonweekend"))
                mblnIsLeaveHL_onWK = CBool(dtRow.Item("isleave_hl_onwk"))

                'Pinkal (28-Nov-2017) -- Start
                'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
                mblnCancelLvFormDocmandatory = CBool(dtRow.Item("islvcanceldocmandatory"))
                'Pinkal (28-Nov-2017) -- End

                'Pinkal (01-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                mblnSkipApproverFlow = CBool(dtRow.Item("skipapproverflow"))
                'Pinkal (01-Oct-2018) -- End


                'Pinkal (08-Oct-2018) -- Start
                'Enhancement - Leave Enhancement for NMB.
                mintLvDaysMinLimit = CInt(dtRow.Item("lvform_mindays"))
                mintLvDaysMaxLimit = CInt(dtRow.Item("lvform_maxdays"))
                'Pinkal (08-Oct-2018) -- End


                'Pinkal (01-Jan-2019) -- Start
                'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
                mblnConsiderLeaveOnTnAPeriod = CBool(dtRow.Item("istnaperiodlinked"))
                'Pinkal (01-Jan-2019) -- End

                'Pinkal (26-Feb-2019) -- Start
                'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
                mblnIsBlockLeave = CBool(dtRow.Item("isblockleave"))
                mblnIsStopApplyAcrossFYELC = CBool(dtRow.Item("isstopapplyacross_fyelc"))
                'Pinkal (26-Feb-2019) -- End


                'Pinkal (03-May-2019) -- Start
                'Enhancement - Working on Leave UAT Changes for NMB.
                mblnConsiderExpense = CBool(dtRow.Item("consider_expense"))
                'Pinkal (03-May-2019) -- End


                'Pinkal (25-May-2019) -- Start
                'Enhancement - NMB FINAL LEAVE UAT CHANGES.
                mblnShowOnESS = CBool(dtRow.Item("isshowoness"))
                'Pinkal (25-May-2019) -- End

                'Sohail (21 Oct 2019) -- Start
                'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
                mblnIsexempt_from_payroll = CBool(dtRow.Item("isexempt_from_payroll"))
                'Sohail (21 Oct 2019) -- End


                'Pinkal (05-Jun-2020) -- Start
                'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
                mblnDoNotApplyForFutureDate = CBool(dtRow.Item("donotapplyfuturedate"))
                'Pinkal (05-Jun-2020) -- End


                'Pinkal (14-Feb-2022) -- Start
                'Enhancement TRA : TnA Module Enhancement for TRA.
                mblnIsExcuseLeave = CBool(dtRow.Item("IsExcuseLeave"))
                'Pinkal (14-Feb-2022) -- End


                'Pinkal (20-Apr-2022) -- Start
                'Enhancement Prevision Air  - Doing Leave module Enhancement.	
                mintMaxFutureDaysLimit = CInt(dtRow.Item("maxfuturedaylimit"))
                'Pinkal (20-Apr-2022) -- End

                'Pinkal (20-Feb-2023) -- Start
                '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.
                mstrLeaveNotificationUserIds = dtRow.Item("leavenotificationuserids").ToString()
                'Pinkal (20-Feb-2023) -- End


                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (02-Dec-2015) -- Start
            'Enhancement - Solving Leave bug in Self Service.
            If objDoOperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (02-Dec-2015) -- End
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal blnIncludeShortLeave As Boolean = True, Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT " & _
                     "  leavetypeunkid " & _
                     ", leavetypecode " & _
                     ", leavename " & _
                     ", description " & _
                     ", isaccrued " & _
                     ", case when ispaid = 0 THEN @unpaid ELSE @paid END accuretype " & _
                     ", isissueonholiday " & _
                     ", isaccrueamount " & _
                     ", color " & _
                     ", isactive " & _
                     ", leavename1 " & _
                     ", leavename2 " & _
                     ", maxamount " & _
                     ", ispaid " & _
                     ", ISNULL(isshortleave,0) as isshortleave " & _
                     ", ISNULL(gender,0) as gender " & _
                    ", ISNULL(deductfromlvtypeunkid,0) deductfromlvtypeunkid " & _
                    ", ISNULL(cfamount,0.00) cfamount " & _
                    ", ISNULL(eligibility_expense,0) eligibility_expense " & _
                    ", ISNULL(isnoaction,0) isnoaction " & _
                     ", ISNULL(issickleave,0) AS issickleave " & _
                     ", ISNULL(ischeckdoconlvform,0) AS ischeckdoconlvform " & _
                     ", ISNULL(ischeckdoconlvissue,0) AS ischeckdoconlvissue " & _
                     ", ISNULL(isleaveencashment,0) AS isleaveencashment " & _
                     ", ISNULL(isissueonweekend,0) AS isissueonweekend " & _
                     ", ISNULL(isleave_hl_onwk,0) AS isleave_hl_onwk " & _
                     ", ISNULL(islvcanceldocmandatory,0)  AS islvcanceldocmandatory " & _
                    ", ISNULL(skipapproverflow,0)  AS skipapproverflow " & _
                    ", ISNULL(lvform_mindays,0)  AS lvform_mindays " & _
                    ", ISNULL(lvform_maxdays,0)  AS lvform_maxdays " & _
                    ", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _
                     ", ISNULL(isblockleave,0) AS isblockleave " & _
                     ", ISNULL(isstopapplyacross_fyelc,0) AS isstopapplyacross_fyelc " & _
                    ", ISNULL(consider_expense,1) AS consider_expense " & _
                    ", ISNULL(isshowoness,1) AS isshowoness " & _
                    ", ISNULL(isexempt_from_payroll, 0) AS isexempt_from_payroll " & _
                     ", ISNULL(donotapplyfuturedate, 0) AS donotapplyfuturedate " & _
                    ", ISNULL(IsExcuseLeave, 0) AS IsExcuseLeave " & _
                    ", ISNULL(maxfuturedaylimit, 0) AS maxfuturedaylimit " & _
                    ", ISNULL(leavenotificationuserids, '') AS leavenotificationuserids " & _
                     " FROM lvleavetype_master "

            'Pinkal (20-Feb-2023) -- '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.[ ", ISNULL(leavenotificationuserids, '') AS leavenotificationuserids " & _]	

            'Pinkal (20-Apr-2022) -- 'Enhancement Prevision Air  - Doing Leave module Enhancement[", ISNULL(maxfuturedaylimit, 0) AS maxfuturedaylimit " & _].	

            'Pinkal (14-Feb-2022) -- Enhancement TRA : TnA Module Enhancement for TRA.[  ", ISNULL(IsExcuseLeave, 0) AS IsExcuseLeave " & _]

            'Pinkal (05-Jun-2020) -- Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.[ ", ISNULL(donotapplyfuturedate, 0) AS donotapplyfuturedate " & _]

            'Sohail (21 Oct 2019) - [isexempt_from_payroll]
            'Pinkal (25-May-2019) -- 'Enhancement - NMB FINAL LEAVE UAT CHANGES.[", ISNULL(isshowoness,1) AS isshowoness " & _]

            'Pinkal (03-May-2019) -- 'Enhancement - Working on Leave UAT Changes for NMB. [", ISNULL(consider_expense,1) AS consider_expense " & _]

            'Pinkal (01-Jan-2019) --  'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", ISNULL(istnaperiodlinked,0)  AS istnaperiodlinked " & _]
            'Pinkal (26-Feb-2019) -- 'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[, ISNULL(isblockleave,0) AS isblockleave , ISNULL(isstopapplyacross_fyelc,0) AS isstopapplyacross_fyelc " & _]

            'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[", ISNULL(lvform_mindays,0)  AS lvform_mindays, ISNULL(lvform_maxdays,0)  AS lvform_maxdays " & __]

            'Pinkal (01-Oct-2018) --   'Enhancement - Leave Enhancement for NMB.[ ", ISNULL(skipapproverflow,0)  AS skipapproverflow " & _]


            'Pinkal (29-Sep-2017) -- 'Enhancement - Working Leave Expense Eligility Option for PACRA.[  ", ISNULL(eligibility_expense,0) eligibility_expense "]

            'Pinkal (28-Nov-2017) --  'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            ' [", ISNULL(islvcanceldocmandatory,0)  AS islvcanceldocmandatory " & _]


            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            If blnIncludeShortLeave = False Then
                strQ &= " AND ISNULL(isshortleave,0) = 0 "
            End If

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@paid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Paid Leave"))
            objDataOperation.AddParameter("@unpaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Unpaid Leave"))

            'Pinkal (03-Jan-2014) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (lvleavetype_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrLeavetypecode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Leave Type Code is already defined. Please define new Leave Type Code.")
            Return False
        ElseIf isExist("", mstrLeavename) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Leave Type Name is already defined. Please define new Leave Type Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            objDataOperation.AddParameter("@leavetypecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLeavetypecode.ToString)
            objDataOperation.AddParameter("@leavename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLeavename.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isaccrued", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsaccrued.ToString)
            objDataOperation.AddParameter("@isissueonholiday", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsissueonholiday.ToString)
            objDataOperation.AddParameter("@isaccrueamount", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAccrueAmount.ToString)
            objDataOperation.AddParameter("@color", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColor.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@leavename1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLeavename1.ToString)
            objDataOperation.AddParameter("@leavename2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLeavename2.ToString)
            objDataOperation.AddParameter("@maxamount", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxAmount.ToString)
            objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPaid.ToString)
            objDataOperation.AddParameter("@isshortleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShortLeave.ToString)
            objDataOperation.AddParameter("@gender", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGender.ToString)
            objDataOperation.AddParameter("@deductfromlvtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductFromLeaveTypeunkid.ToString)
            objDataOperation.AddParameter("@cfamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclCFAmount.ToString)

            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA.
            'objDataOperation.AddParameter("@eligibilityafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibilityAfter.ToString)
            objDataOperation.AddParameter("@eligibility_expense", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibilityOnAfter_Expense.ToString)
            'Pinkal (29-Sep-2017) -- End

            objDataOperation.AddParameter("@isnoaction", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnNoAction.ToString)
            objDataOperation.AddParameter("@issickleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSickLeave.ToString)
            objDataOperation.AddParameter("@ischeckdoconlvform", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblncheckDocOnLeaveForm.ToString)
            objDataOperation.AddParameter("@ischeckdoconlvissue", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblncheckDocOnLeaveIssue.ToString)
            objDataOperation.AddParameter("@isleaveencashment", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLeaveEncashment.ToString)
            objDataOperation.AddParameter("@isissueonweekend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsissueonweekend.ToString)
            objDataOperation.AddParameter("@isleave_hl_onwk", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLeaveHL_onWK.ToString)


            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            objDataOperation.AddParameter("@islvcanceldocmandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnCancelLvFormDocmandatory.ToString)
            'Pinkal (28-Nov-2017) -- End


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@SkipApproverFlow", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnSkipApproverFlow.ToString)
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@lvform_mindays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLvDaysMinLimit.ToString)
            objDataOperation.AddParameter("@lvform_maxdays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLvDaysMaxLimit.ToString)
            'Pinkal (08-Oct-2018) -- End


            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            objDataOperation.AddParameter("@istnaperiodlinked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnConsiderLeaveOnTnAPeriod.ToString)
            'Pinkal (01-Jan-2019) -- End

            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            objDataOperation.AddParameter("@isblockleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsBlockLeave)
            objDataOperation.AddParameter("@isstopapplyacross_fyelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsStopApplyAcrossFYELC)
            'Pinkal (26-Feb-2019) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            objDataOperation.AddParameter("@consider_expense", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnConsiderExpense)
            'Pinkal (03-May-2019) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            objDataOperation.AddParameter("@isshowoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShowOnESS)
            'Pinkal (25-May-2019) -- End
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            objDataOperation.AddParameter("@isexempt_from_payroll", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexempt_from_payroll)
            'Sohail (21 Oct 2019) -- End


            'Pinkal (05-Jun-2020) -- Start
            'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
            objDataOperation.AddParameter("@donotapplyfuturedate", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnDoNotApplyForFutureDate)
            'Pinkal (05-Jun-2020) -- End


            'Pinkal (14-Feb-2022) -- Start
            'Enhancement TRA : TnA Module Enhancement for TRA.
            objDataOperation.AddParameter("@isexcuseleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExcuseLeave)
            'Pinkal (14-Feb-2022) -- End


            'Pinkal (20-Apr-2022) -- Start
            'Enhancement Prevision Air  - Doing Leave module Enhancement.	
            objDataOperation.AddParameter("@maxfuturedaylimit", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxFutureDaysLimit)
            'Pinkal (20-Apr-2022) -- End

            'Pinkal (20-Feb-2023) -- Start
            '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.
            objDataOperation.AddParameter("@leavenotificationuserids", SqlDbType.NVarChar, mstrLeaveNotificationUserIds.Trim.Length, mstrLeaveNotificationUserIds.Trim)
            'Pinkal (20-Feb-2023) -- End


            strQ = "INSERT INTO lvleavetype_master ( " & _
                      "  leavetypecode " & _
                      ", leavename " & _
                      ", description " & _
                      ", isaccrued " & _
                      ", isissueonholiday " & _
                      ", isaccrueamount " & _
                      ", color " & _
                      ", isactive " & _
                      ", leavename1 " & _
                      ", leavename2 " & _
                      ", maxamount " & _
                      ", ispaid" & _
                      ", isshortleave " & _
                      ", gender " & _
                      ", deductfromlvtypeunkid " & _
                      ", cfamount " & _
                      ", eligibility_expense " & _
                      ", isnoaction " & _
                      ", issickleave " & _
                      ", ischeckdoconlvform " & _
                      ", ischeckdoconlvissue " & _
                      ", isleaveencashment " & _
                      ", isissueonweekend " & _
                      ", isleave_hl_onwk " & _
                      ", islvcanceldocmandatory " & _
                      ", SkipApproverFlow " & _
                      ", lvform_mindays " & _
                      ", lvform_maxdays " & _
                      ", istnaperiodlinked " & _
                      ", isblockleave " & _
                      ", isstopapplyacross_fyelc " & _
                      ", consider_expense " & _
                      ", isshowoness " & _
                      ", isexempt_from_payroll " & _
                      ", donotapplyfuturedate " & _
                      ", isexcuseleave " & _
                      ", maxfuturedaylimit " & _
                      ", leavenotificationuserids " & _
                ") VALUES (" & _
                  "  @leavetypecode " & _
                  ", @leavename " & _
                  ", @description " & _
                  ", @isaccrued " & _
                  ", @isissueonholiday " & _
                  ", @isaccrueamount " & _
                  ", @color " & _
                  ", @isactive " & _
                  ", @leavename1 " & _
                  ", @leavename2 " & _
                  ", @maxamount " & _
                  ", @ispaid" & _
                  ", @isshortleave " & _
                  ", @gender " & _
                  ", @deductfromlvtypeunkid " & _
                  ", @cfamount " & _
                  ", @eligibility_expense " & _
                  ", @isnoaction " & _
                  ", @issickleave " & _
                  ", @ischeckdoconlvform " & _
                  ", @ischeckdoconlvissue " & _
                  ", @isleaveencashment " & _
                      ", @isissueonweekend " & _
                      ", @isleave_hl_onwk " & _
                  ", @islvcanceldocmandatory " & _
                      ", @SkipApproverFlow " & _
                      ", @lvform_mindays " & _
                      ", @lvform_maxdays " & _
                  ",@istnaperiodlinked " & _
                  ", @isblockleave " & _
                  ", @isstopapplyacross_fyelc " & _
                  ", @consider_expense " & _
                  ", @isshowoness " & _
                  ", @isexempt_from_payroll " & _
                  ", @donotapplyfuturedate " & _
                  ", @isexcuseleave " & _
                  ", @maxfuturedaylimit " & _
                      ", @leavenotificationuserids " & _
            "); SELECT @@identity"

            'Pinkal (20-Feb-2023) -- (A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.[leavenotificationuserids]

            'Pinkal (20-Apr-2022) -- Enhancement Prevision Air  - Doing Leave module Enhancement.	[maxfuturedaylimit]

            'Pinkal (14-Feb-2022) -- Enhancement TRA : TnA Module Enhancement for TRA.[isexcuseleave]

            'Pinkal (05-Jun-2020) -- Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.[@donotapplyfuturedate]

            'Sohail (21 Oct 2019) - [isexempt_from_payroll]
            'Pinkal (25-May-2019) -- 'Enhancement - NMB FINAL LEAVE UAT CHANGES.[", @isshowoness " & _]

            'Pinkal (03-May-2019) -- 'Enhancement - Working on Leave UAT Changes for NMB.[", @consider_expense " & _]

            'Pinkal (01-Jan-2019) -- 'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[ ",@istnaperiodlinked " & _]
            'Pinkal (26-Feb-2019) --  'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[", @isblockleave,@isstopapplyacross_fyelc]

            'Pinkal (08-Oct-2018)  'Enhancement - Leave Enhancement for NMB.[   ", @lvform_mindays , @lvform_maxdays " & _]



            'Pinkal (01-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB. [SkipApproverFlow] 


            'Pinkal (29-Sep-2017) -- 'Enhancement - Working Leave Expense Eligility Option for PACRA. [eligibility_expense = @eligibility_expense] 

            'Pinkal (28-Nov-2017) --  'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            ' [", @islvcanceldocmandatory " & _]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLeavetypeunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "lvleavetype_master", "leavetypeunkid", mintLeavetypeunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (20-Feb-2023) -- Start
            '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.
            If mdtLeavePay IsNot Nothing AndAlso mdtLeavePay.Rows.Count > 0 Then
                objLeavePay._Leavetypeunkid = mintLeavetypeunkid
                objLeavePay._dtPaySetting = mdtLeavePay.Copy
                objLeavePay._Isvoid = 0
                objLeavePay._Voiddatatime = Nothing
                objLeavePay._Voidreason = ""
                objLeavePay._Voiduserunkid = 0
                objLeavePay._AuditUserId = mintUserunkId
                objLeavePay._FormName = mstrWebFormName
                objLeavePay._ClientIP = getIP()
                objLeavePay._HostName = getHostName()
                objLeavePay._IsFromWeb = mblnIsWeb
                If objLeavePay.InsertUpdateDelete_LeavePaySetting(mdtOldLeavePay, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Pinkal (20-Feb-2023) -- End

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (lvleavetype_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrLeavetypecode, "", mintLeavetypeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Leave Type Code is already defined. Please define new Leave Type Code.")
            Return False
        ElseIf isExist("", mstrLeavename, mintLeavetypeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Leave Type Name is already defined. Please define new Leave Type Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLeavetypeunkid.ToString)
            objDataOperation.AddParameter("@leavetypecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLeavetypecode.ToString)
            objDataOperation.AddParameter("@leavename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLeavename.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isaccrued", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsaccrued.ToString)
            objDataOperation.AddParameter("@isissueonholiday", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsissueonholiday.ToString)
            objDataOperation.AddParameter("@isaccrueamount", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAccrueAmount.ToString)
            objDataOperation.AddParameter("@color", SqlDbType.Int, eZeeDataType.INT_SIZE, mintColor.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@leavename1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLeavename1.ToString)
            objDataOperation.AddParameter("@leavename2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLeavename2.ToString)
            objDataOperation.AddParameter("@maxamount", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxAmount.ToString)
            objDataOperation.AddParameter("@ispaid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsPaid.ToString)
            objDataOperation.AddParameter("@isshortleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsShortLeave.ToString)
            objDataOperation.AddParameter("@gender", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGender.ToString)
            objDataOperation.AddParameter("@deductfromlvtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDeductFromLeaveTypeunkid.ToString)
            objDataOperation.AddParameter("@cfamount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdclCFAmount.ToString)


            'Pinkal (29-Sep-2017) -- Start
            'Enhancement - Working Leave Expense Eligility Option for PACRA.
            'objDataOperation.AddParameter("@eligibilityafter", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibilityAfter.ToString)
            objDataOperation.AddParameter("@eligibility_expense", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEligibilityOnAfter_Expense.ToString)
            'Pinkal (29-Sep-2017) -- End


            objDataOperation.AddParameter("@isnoaction", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnNoAction.ToString)

            'S.SANDEEP [ 26 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@issickleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSickLeave.ToString)
            'S.SANDEEP [ 26 SEPT 2013 ] -- END

            objDataOperation.AddParameter("@ischeckdoconlvform", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblncheckDocOnLeaveForm.ToString)
            objDataOperation.AddParameter("@ischeckdoconlvissue", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblncheckDocOnLeaveIssue.ToString)
            objDataOperation.AddParameter("@isleaveencashment", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLeaveEncashment.ToString)
            objDataOperation.AddParameter("@isissueonweekend", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsissueonweekend.ToString)
            objDataOperation.AddParameter("@isleave_hl_onwk", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLeaveHL_onWK.ToString)

            'Pinkal (28-Nov-2017) -- Start
            'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            objDataOperation.AddParameter("@islvcanceldocmandatory", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnCancelLvFormDocmandatory.ToString)
            'Pinkal (28-Nov-2017) -- End


            'Pinkal (01-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@SkipApproverFlow", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnSkipApproverFlow.ToString)
            'Pinkal (01-Oct-2018) -- End


            'Pinkal (08-Oct-2018) -- Start
            'Enhancement - Leave Enhancement for NMB.
            objDataOperation.AddParameter("@lvform_mindays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLvDaysMinLimit.ToString)
            objDataOperation.AddParameter("@lvform_maxdays", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLvDaysMaxLimit.ToString)
            'Pinkal (08-Oct-2018) -- End


            'Pinkal (01-Jan-2019) -- Start
            'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.
            objDataOperation.AddParameter("@istnaperiodlinked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnConsiderLeaveOnTnAPeriod.ToString)
            'Pinkal (01-Jan-2019) -- End


            'Pinkal (26-Feb-2019) -- Start
            'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.
            objDataOperation.AddParameter("@isblockleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsBlockLeave)
            objDataOperation.AddParameter("@isstopapplyacross_fyelc", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsStopApplyAcrossFYELC)
            'Pinkal (26-Feb-2019) -- End


            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            objDataOperation.AddParameter("@consider_expense", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnConsiderExpense)
            'Pinkal (03-May-2019) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            objDataOperation.AddParameter("@isshowoness", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnShowOnESS)
            'Pinkal (25-May-2019) -- End
            'Sohail (21 Oct 2019) -- Start
            'NMB Enhancement # : Option to set Exempt from Payroll on leave type master for It should be possible to update active employee status as Unpaid and automatically prorate payment of that employee effectively from the date selected. Also when employee is back, user should be able to update end date of the Unpaid status and automatically include that employee in payroll and prorate based on the date set
            objDataOperation.AddParameter("@isexempt_from_payroll", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsexempt_from_payroll)
            'Sohail (21 Oct 2019) -- End


            'Pinkal (05-Jun-2020) -- Start
            'Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.
            objDataOperation.AddParameter("@donotapplyfuturedate", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnDoNotApplyForFutureDate)
            'Pinkal (05-Jun-2020) -- End

            'Pinkal (14-Feb-2022) -- Start
            'Enhancement TRA : TnA Module Enhancement for TRA.
            objDataOperation.AddParameter("@isexcuseleave", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsExcuseLeave)
            'Pinkal (14-Feb-2022) -- End

            'Pinkal (20-Apr-2022) -- Start
            'Enhancement Prevision Air  - Doing Leave module Enhancement.	
            objDataOperation.AddParameter("@maxfuturedaylimit", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMaxFutureDaysLimit)
            'Pinkal (20-Apr-2022) -- End

            'Pinkal (20-Feb-2023) -- Start
            '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.
            objDataOperation.AddParameter("@leavenotificationuserids", SqlDbType.NVarChar, mstrLeaveNotificationUserIds.Trim.Length, mstrLeaveNotificationUserIds.Trim)
            'Pinkal (20-Feb-2023) -- End



            strQ = "UPDATE lvleavetype_master SET " & _
                  "  leavetypecode = @leavetypecode " & _
                  ", leavename = @leavename " & _
                  ", description = @description " & _
                  ", isaccrued = @isaccrued " & _
                  ", isissueonholiday = @isissueonholiday " & _
                  ", isaccrueamount = @isaccrueamount " & _
                  ", color = @color " & _
                  ", isactive = @isactive " & _
                  ", leavename1 = @leavename1 " & _
                  ", leavename2 = @leavename2 " & _
                  ", maxamount = @maxamount " & _
                  ", ispaid = @ispaid " & _
                  ", isshortleave = @isshortleave " & _
                  ", gender = @gender " & _
                  ", deductfromlvtypeunkid = @deductfromlvtypeunkid " & _
                  ", cfamount = @cfamount " & _
                  ", eligibility_expense = @eligibility_expense " & _
                  ", isnoaction = @isnoaction " & _
                  ", issickleave = @issickleave " & _
                  ", ischeckdoconlvform = @ischeckdoconlvform " & _
                  ", ischeckdoconlvissue = @ischeckdoconlvissue " & _
                  ", isleaveencashment = @isleaveencashment " & _
                  ", isissueonweekend = @isissueonweekend " & _
                  ", isleave_hl_onwk  = @isleave_hl_onwk " & _
                      ", islvcanceldocmandatory = @islvcanceldocmandatory " & _
                  ", skipapproverflow = @skipapproverflow " & _
                      ", lvform_mindays = @lvform_mindays " & _
                      ", lvform_maxdays = @lvform_maxdays " & _
                  ", istnaperiodlinked = @istnaperiodlinked " & _
                  ", isblockleave = @isblockleave " & _
                  ", isstopapplyacross_fyelc = @isstopapplyacross_fyelc " & _
                      ", consider_expense = @consider_expense " & _
                  ", isshowoness = @isshowoness " & _
                  ", isexempt_from_payroll = @isexempt_from_payroll " & _
                  ", donotapplyfuturedate = @donotapplyfuturedate " & _
                  ", isexcuseleave = @isexcuseleave " & _
                  ", maxfuturedaylimit = @maxfuturedaylimit " & _
                  ", leavenotificationuserids = @leavenotificationuserids " & _
                  " WHERE leavetypeunkid = @leavetypeunkid "

            'Pinkal (20-Feb-2023) -- (A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.[leavenotificationuserids = @leavenotificationuserids]

            'Pinkal (20-Apr-2022) -- Enhancement Prevision Air  - Doing Leave module Enhancement.	[", maxfuturedaylimit = @maxfuturedaylimit " & _]

            'Pinkal (14-Feb-2022) -- Enhancement TRA : TnA Module Enhancement for TRA.[", isexcuseleave = @isexcuseleave " & _]

            'Pinkal (05-Jun-2020) -- Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.[donotapplyfuturedate = @donotapplyfuturedate]

            'Sohail (21 Oct 2019) - [isexempt_from_payroll]
            'Pinkal (25-May-2019) -- 'Enhancement - NMB FINAL LEAVE UAT CHANGES.[", isshowoness = @isshowoness " & _]

            'Pinkal (03-May-2019) -- 'Enhancement - Working on Leave UAT Changes for NMB.[", consider_expense = @consider_expense " & _]

            'Pinkal (01-Jan-2019) -- 'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[", istnaperiodlinked = @istnaperiodlinked " & _]
            'Pinkal (26-Feb-2019) --  'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[", isblockleave = @isblockleave , isstopapplyacross_fyelc = @isstopapplyacross_fyelc " & _]

            'Pinkal (08-Oct-2018) --  'Enhancement - Leave Enhancement for NMB.[", lvform_mindays = @lvform_mindays , lvform_maxdays = @lvform_maxdays " & _]

            'Pinkal (01-Oct-2018) --  'Enhancement - Leave Enhancement for NMB. [", skipapproverflow = @skipapproverflow " & _]

            'Pinkal (29-Sep-2017) -- Enhancement - Working Leave Expense Eligility Option for PACRA. [ ", eligibility_expense = @eligibility_expense " & _]


            'Pinkal (28-Nov-2017) --  'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            ' [", islvcanceldocmandatory = @islvcanceldocmandatory " & _]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "lvleavetype_master", mintLeavetypeunkid, "leavetypeunkid", 2) Then

                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "lvleavetype_master", "leavetypeunkid", mintLeavetypeunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'Pinkal (20-Feb-2023) -- Start
            '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.
            If mdtLeavePay IsNot Nothing AndAlso mdtLeavePay.Rows.Count > 0 Then
                objLeavePay._Leavetypeunkid = mintLeavetypeunkid
                objLeavePay._dtPaySetting = mdtLeavePay.Copy
                objLeavePay._Userunkid = mintUserunkId
                objLeavePay._Isvoid = 0
                objLeavePay._Voiddatatime = Nothing
                objLeavePay._Voidreason = ""
                objLeavePay._Voiduserunkid = 0
                objLeavePay._AuditUserId = mintUserunkId
                objLeavePay._FormName = mstrWebFormName
                objLeavePay._ClientIP = getIP()
                objLeavePay._HostName = getHostName()
                objLeavePay._IsFromWeb = mblnIsWeb
                If objLeavePay.InsertUpdateDelete_LeavePaySetting(mdtOldLeavePay, objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            'Pinkal (20-Feb-2023) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(True)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (lvleavetype_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Leave Type. Reason: This Leave Type is in use.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            strQ = " Update lvleavetype_master set isactive = 0 " & _
                      " WHERE leavetypeunkid = @leavetypeunkid "

            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "lvleavetype_master", "leavetypeunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (20-Feb-2023) -- Start
            '(A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.
            objLeavePay.GetList("List", intUnkid, True, objDataOperation)
            Dim mdtLeavePaySetting As DataTable = objLeavePay._dtPaySetting.Copy
            If mdtLeavePaySetting IsNot Nothing AndAlso mdtLeavePaySetting.Rows.Count > 0 Then
                For Each dr As DataRow In mdtLeavePaySetting.Rows
                    objLeavePay._Leavepayunkid(objDataOperation) = CInt(dr("leavepayunkid"))
                    objLeavePay._Leavetypeunkid = CInt(dr("leavetypeunkid"))
                    objLeavePay._Userunkid = mintUserunkId
                    objLeavePay._Isvoid = True
                    objLeavePay._Voidreason = "Posting Error"
                    objLeavePay._AuditUserId = mintUserunkId
                    objLeavePay._FormName = mstrWebFormName
                    objLeavePay._ClientIP = mstrClientIP
                    objLeavePay._HostName = mstrHostName
                    objLeavePay._Voiduserunkid = mintUserunkId

                    If objLeavePay.Delete(objDataOperation) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If
            'Pinkal (20-Feb-2023) -- End

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation

        Try
            strQ = " SELECT ISNULL(leavetypeunkid,0) FROM lvbatch_master  WHERE leavetypeunkid=@leavetypeunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            strQ = " SELECT ISNULL(leavetypeunkid,0) from  lvleaveIssue_master WHERE leavetypeunkid=@leavetypeunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True


            strQ = " SELECT ISNULL(leavetypeunkid,0) from  lvleaveform WHERE leavetypeunkid = @leavetypeunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True


            'Pinkal (06-Feb-2013) -- Start
            'Enhancement : TRA Changes

            strQ = " SELECT ISNULL(leavetypeunkid,0) from  lvleaveplanner WHERE leavetypeunkid = @leavetypeunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            strQ = " SELECT ISNULL(leavetypeunkid,0) from  lvleavebalance_tran WHERE leavetypeunkid = @leavetypeunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            strQ = " SELECT ISNULL(leavetypeunkid,0) from  lvapprover_leavetype_mapping WHERE leavetypeunkid = @leavetypeunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            strQ = " SELECT ISNULL(leavetypeunkid,0) from  atlvleavebalance_tran WHERE leavetypeunkid = @leavetypeunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            'Pinkal (06-Feb-2013) -- End


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            
            strQ = "SELECT " & _
                    "  leavetypeunkid " & _
                    ", leavetypecode " & _
                    ", leavename " & _
                    ", description " & _
                    ", isaccrued " & _
                    ", isissueonholiday " & _
                    ", isaccrueamount " & _
                    ", color " & _
                    ", isactive " & _
                    ", leavename1 " & _
                    ", leavename2 " & _
                    ", maxamount " & _
                    ", ispaid " & _
                   ", ISNULL(isshortleave,0) isshortleave " & _
                 ", ISNULL(gender,0) gender " & _
                   ", ISNULL(deductfromlvtypeunkid,0) deductfromlvtypeunkid " & _
                    ", ISNULL(cfamount,0.00) cfamount " & _
                    ", ISNULL(eligibility_expense,0) eligibility_expense " & _
                    ", ISNULL(isnoaction,0) isnoaction " & _
                    ", ISNULL(ischeckdoconlvform,0) ischeckdoconlvform " & _
                    ", ISNULL(ischeckdoconlvissue,0) ischeckdoconlvissue " & _
                   ",  ISNULL(isleaveencashment,0) isleaveencashment " & _
                   ",  ISNULL(isissueonweekend,0)  isissueonweekend " & _
                   ",  ISNULL(isleave_hl_onwk,0) isleave_hl_onwk " & _
                   ", ISNULL(islvcanceldocmandatory,0)  AS islvcanceldocmandatory " & _
                   ", ISNULL(skipapproverflow,0)  AS skipapproverflow " & _
                   ", ISNULL(lvform_mindays,0) AS lvform_mindays " & _
                   ",  ISNULL(lvform_maxdays,0) AS lvform_maxdays " & _
                   ",  ISNULL(istnaperiodlinked,0) AS istnaperiodlinked " & _
                   ", ISNULL(isblockleave,0) AS isblockleave " & _
                   ", ISNULL(isstopapplyacross_fyelc,0) AS isstopapplyacross_fyelc " & _
                   ", ISNULL(consider_expense,1) AS consider_expense " & _
                   ", ISNULL(isshowoness,1) AS isshowoness " & _
                   ", ISNULL(isexempt_from_payroll, 0) AS isexempt_from_payroll " & _
                    ", ISNULL(donotapplyfuturedate, 0) AS donotapplyfuturedate " & _
                    ", ISNULL(isexcuseleave, 0) AS isexcuseleave " & _
                   ", ISNULL(maxfuturedaylimit, 0) AS maxfuturedaylimit " & _
                    ", ISNULL(leavenotificationuserids,'') AS leavenotificationuserids " & _
                   " FROM lvleavetype_master " & _
                   " WHERE 1=1 "

            'Pinkal (20-Feb-2023) -- (A1X-622) NMB - As a user, I want a configuration setting on leave type master that will allow me to define days range and their respective salary percentage calculation where leave type has been marked as sick leave.[", ISNULL(leavenotificationuserids,'') AS leavenotificationuserids " & _]

            'Pinkal (20-Apr-2022) -- 'Enhancement Prevision Air  - Doing Leave module Enhancement.	[ ", ISNULL(maxfuturedaylimit, 0) AS maxfuturedaylimit " & _]

            'Pinkal (14-Feb-2022) -- Enhancement TRA : TnA Module Enhancement for TRA.[", ISNULL(isexcuseleave, 0) AS isexcuseleave " & _]

            'Pinkal (05-Jun-2020) -- Enhancement NMB - Implementing Setting on Leave Type Master for Employee/User can't able to apply for future date if that setting is ON.[ISNULL(donotapplyfuturedate, 0) AS donotapplyfuturedate]

            'Sohail (21 Oct 2019) - [isexempt_from_payroll]
            'Pinkal (25-May-2019) -- 'Enhancement - NMB FINAL LEAVE UAT CHANGES.[", ISNULL(isshowoness,1) AS isshowoness " & _]

            'Pinkal (03-May-2019) --  'Enhancement - Working on Leave UAT Changes for NMB.[", ISNULL(consider_expense,0) AS consider_expense " & _]

            'Pinkal (01-Jan-2019) --  'Enhancement [Ref #: 0003287] - Working on Leave Frequency Change for Voltamp.[   ",  ISNULL(istnaperiodlinked,0) AS istnaperiodlinked " & _]

            'Pinkal (26-Feb-2019) --  'Enhancement - Working on Leave & Claim Enhancement Phase 2 for NMB.[ ", ISNULL(isblockleave,0) AS isblockleave , ISNULL(isstopapplyacross_fyelc,0) AS isstopapplyacross_fyelc " & _]

            'Pinkal (08-Oct-2018) -- 'Enhancement - Leave Enhancement for NMB.[ ", ISNULL(lvform_mindays,0) AS lvform_mindays ",  ISNULL(lvform_maxdays,0) AS lvform_maxdays " & _]

            'Pinkal (01-Oct-2018) --  'Enhancement - Leave Enhancement for NMB.[", ISNULL(skipapproverflow,0)  AS skipapproverflow " & _]

            'Pinkal (29-Sep-2017) --  'Enhancement - Working Leave Expense Eligility Option for PACRA.[ ", ISNULL(eligibility_expense,0) eligibility_expense " & _]

            'Pinkal (28-Nov-2017) --  'Enhancement - Cancel form document attachment  - (RefNo: 132) On leave cancel process there should be a place to attach document for a reference and this feature (cancel leave form) should be included in Aruti self service .
            ' [", ISNULL(islvcanceldocmandatory,0)  AS islvcanceldocmandatory " & _]

            If strCode.Length > 0 Then
                strQ &= " AND leavetypecode = @leavetypecode"
            End If

            If strName.Length > 0 Then
                strQ &= " AND leavename = @leavename"
            End If

            If intUnkid > 0 Then
                strQ &= " AND leavetypeunkid <> @leavetypeunkid"
            End If

            objDataOperation.AddParameter("@leavetypecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@leavename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal intPaid As Integer = -1 _
                                    , Optional ByVal strDatabaseName As String = "", Optional ByVal mstrFilter As String = "", Optional ByVal blnShowOnESS As Boolean = False _
                                    , Optional ByVal blnIsSickLeave As Boolean = False _
                                    ) As DataSet
        'Hemant (17 Feb 2023) -- [blnIsSickLeave]
        'Pinkal (06-Dec-2019) -- Enhancement SPORT PESA [0004321] -  They needs to allow short leave to appear on screen even when "Show on ESS" is not selected.[Optional ByVal blnShowOnESS As Boolean = True]

        'Pinkal (03-May-2019) -- 'Enhancement - Working on Leave UAT Changes for NMB.[Optional ByVal mstrFilter As String = ""]

        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If strDatabaseName = "" Then strDatabaseName = FinancialYear._Object._DatabaseName

            If mblFlag = True Then
                strQ = "SELECT 0 as leavetypeunkid, ' ' +  @name  as name UNION "
            End If

            strQ &= "SELECT leavetypeunkid, leavename as name FROM " & strDatabaseName & "..lvleavetype_master where isnull(isactive,0) = 1  "

            'Pinkal (03-May-2019) -- Start
            'Enhancement - Working on Leave UAT Changes for NMB.
            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If
            'Pinkal (03-May-2019) -- End


            'Pinkal (25-May-2019) -- Start
            'Enhancement - NMB FINAL LEAVE UAT CHANGES.
            If blnShowOnESS Then
                strQ &= " AND ISNULL(isshowoness,1) = 1 "
            End If
            'Pinkal (25-May-2019) -- End


            'Hemant (17 Feb 2023) -- Start
            'ENHANCEMENT(NMB) : A1X-627 - Implementation of leave tenure in half-pay computation function
            'If intPaid = -1 Then
            '    strQ &= " ORDER BY name "
            'Else
            '    strQ &= " AND ispaid = 1 ORDER BY name "
            'End If
            If intPaid = 1 Then
                strQ &= " AND ispaid = 1  "
            End If

            If blnIsSickLeave = True Then
                strQ &= " AND issickleave = 1  "
            End If
                strQ &= " ORDER BY name "
            'Hemant (17 Feb 2023) -- End


            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetListFromLeaveForm(ByVal strTableName As String, ByVal intEmployeeunkid As Integer, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ &= "SELECT " & _
              " distinct lvleavetype_master.leavetypeunkid " & _
              ", leavename as name " & _
              ", ispaid " & _
         ", ISNULL(isshortleave,0)  isshortleave " & _
              ", isaccrueamount " & _
          ", ISNULL(deductfromlvtypeunkid,0) as deductfromlvtypeunkid " & _
          ", ISNULL(maxamount,0) as maxamount " & _
              " FROM lvleaveform " & _
              " LEFT JOIN lvleavetype_master on lvleaveform.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
              " AND lvleaveform.employeeunkid = @employeeunkid  " & _
              " AND lvleaveform.isvoid = 0 "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 ORDER BY name"
            End If

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetListFromLeaveForm; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLeaveTypeFromBalance(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal intLeaveBalanceSetting As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            If intLeaveBalanceSetting <= 0 Then
                intLeaveBalanceSetting = ConfigParameter._Object._LeaveBalanceSetting
            End If

            If mblFlag = True Then
                strQ = "SELECT 0 as leavetypeunkid, ' ' +  @name  as leavename UNION "
            End If

            strQ &= " SELECT lvleavetype_master.leavetypeunkid ,lvleavetype_master.leavename " & _
                         " FROM lvleavebalance_tran  " & _
                         " JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleavebalance_tran.leavetypeunkid  " & _
                         " AND lvleavetype_master.ispaid = 1 AND lvleavetype_master.isactive = 1  " & _
                         " WHERE lvleavebalance_tran.remaining_bal > 0 "

            If intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                strQ &= " AND isopenelc = 1 AND iselc = 1 "
            End If

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, "Leave")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLeaveTypeFromBalance", mstrModuleName)
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetLeaveTypeUnkId(ByVal strLeaveName As String, Optional ByVal strLeaveCode As String = "") As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "Select isnull(leavetypeunkid,0) leavetypeunkid from lvleavetype_master" & _
                       " where 1 = 1 "

            If strLeaveName <> "" And strLeaveCode = "" Then

                strQ &= " AND leaveName = @leavename"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLeaveName)

            ElseIf strLeaveCode <> "" And strLeaveName = "" Then

                strQ &= " AND leavetypecode = @leavecode"
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@leavecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLeaveCode)

            End If


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("leavetypeunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLeaveTypeUnkId", mstrModuleName)
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetListForDeductFromLeaveType(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as leavetypeunkid, ' ' +  @name  as name UNION "
            End If
            strQ &= "SELECT leavetypeunkid, leavename as name FROM lvleavetype_master where isnull(isactive,0) = 1 AND ISNULL(isshortleave,0) = 0 AND isaccrueamount = 1 AND ispaid = 1 ORDER BY name"

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function

    'Pinkal (15-Jan-2019) -- Start
    'Bug - SOLVED ISSUE FOR PACRA ABOUT DEDUCTION LEAVE ISSUE.
    Public Function GetDeductdToLeaveType(ByVal strTableName As String, ByVal intEmployeeunkid As Integer, Optional ByVal blnOnlyActive As Boolean = True) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            Dim objDataOperation As New clsDataOperation

            'strQ &= "SELECT " & _
            '              " distinct lvleavetype_master.leavetypeunkid " & _
            '              ", leavename as name " & _
            '              ", ispaid " & _
            '              ", ISNULL(isshortleave,0)  isshortleave " & _
            '              ", isaccrueamount " & _
            '              ", ISNULL(deductfromlvtypeunkid,0) as deductfromlvtypeunkid " & _
            '              ", ISNULL(maxamount,0) as maxamount " & _
            '              " FROM lvleaveform " & _
            '              " JOIN lvleavetype_master on lvleaveform.leavetypeunkid = lvleavetype_master.leavetypeunkid AND deductfromlvtypeunkid > 0 " & _
            '              " AND lvleaveform.employeeunkid = @employeeunkid  " & _
            '              " AND lvleaveform.isvoid = 0 "


            strQ &= "SELECT " & _
                     "  lvleavetype_master.leavetypeunkid " & _
                          ", leavename as name " & _
                          ", ispaid " & _
                          ", ISNULL(isshortleave,0)  isshortleave " & _
                          ", isaccrueamount " & _
                          ", ISNULL(deductfromlvtypeunkid,0) as deductfromlvtypeunkid " & _
                          ", ISNULL(maxamount,0) as maxamount " & _
                     " FROM lvleavetype_master " & _
                     " WHERE deductfromlvtypeunkid > 0 "

            If blnOnlyActive Then
                strQ &= " AND isactive = 1 ORDER BY name"
            End If

            'Pinkal (15-Jan-2019) -- End 


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDeductdToLeaveType; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try

        If dsList IsNot Nothing Then
            Return dsList.Tables(0)
        Else
            Return Nothing
        End If
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function IsDeductedLeaveType(ByVal intLeaveTypeId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            Dim objDataOperation As New clsDataOperation

            strQ = " SELECT ISNULL(leavetypeunkid,0) AS leavetypeID FROM lvleavetype_master WHERE deductfromlvtypeunkid = @leavetypeId AND isactive = 1"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@leavetypeId", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeaveTypeId)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsDeductedFromLeaveType", mstrModuleName)
            Return False
        End Try
    End Function

    'Pinkal (09-Aug-2018) -- Start
    'Bug - Solving Bug for PACRA.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetDeductdToLeaveTypeIDs(ByVal xLeaveTypeID As Integer) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrLeaveTypeIds As String = ""
        Try

            Dim objDataOperation As New clsDataOperation

            strQ &= " SELECT STUFF(( SELECT  ',' + CAST(ISNULL(lvleavetype_master.leavetypeunkid,0) AS VARCHAR(max))  " & _
                        " FROM lvleavetype_master " & _
                        " WHERE lvleavetype_master.deductfromlvtypeunkid In (" & xLeaveTypeID & ") AND isactive = 1 " & _
                        " ORDER BY lvleavetype_master.leavetypeunkid   FOR  XML PATH('')), 1, 1, '')  AS leavetypeIds "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count Then
                mstrLeaveTypeIds = dsList.Tables(0).Rows(0)("leavetypeIds").ToString()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDeductdToLeaveTypeIDs; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return mstrLeaveTypeIds
    End Function
    'Pinkal (09-Aug-2018) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Leave Type Code is already defined. Please define new Leave Type Code.")
			Language.setMessage(mstrModuleName, 2, "This Leave Type Name is already defined. Please define new Leave Type Name.")
			Language.setMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Leave Type. Reason: This Leave Type is in use.")
			Language.setMessage(mstrModuleName, 4, "Select")
			Language.setMessage(mstrModuleName, 5, "Paid Leave")
			Language.setMessage(mstrModuleName, 6, "Unpaid Leave")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class