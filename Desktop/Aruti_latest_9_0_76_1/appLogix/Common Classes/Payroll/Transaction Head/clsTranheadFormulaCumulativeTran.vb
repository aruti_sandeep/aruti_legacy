﻿'************************************************************************************************************************************
'Class Name : clsTranheadFormulaCumulativeTran.vb
'Purpose    :
'Date       :14/11/2013
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsTranheadFormulaCumulativeTran

#Region " Private variables "
    Private Const mstrModuleName = "clsTranheadFormulaCumulativeTran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

    Private mintTranheadformulacumulativetranunkid As Integer
    Private mintFormulatranheadunkid As Integer
    Private mintTranheadunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintDefaultvalue_id As Integer = 0
    Private mintTranheadslabtranunkid As Integer = 0

    Private mdtFormula As DataTable
#End Region

#Region " Constructor "
    Public Sub New()
        mdtFormula = New DataTable("Formula")
        Dim dCol As New DataColumn

        Try
            dCol = New DataColumn("formulatranheadunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFormula.Columns.Add(dCol)

            dCol = New DataColumn("tranheadunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtFormula.Columns.Add(dCol)

            mdtFormula.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtFormula.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtFormula.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormula.Columns.Add("defaultvalue_id", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFormula.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFormula.Columns.Add("tranheadslabtranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFormula.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = String.Empty

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Properties "

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property _Datasource() As DataTable
        Get
            Return mdtFormula
        End Get
        Set(ByVal value As DataTable)
            mdtFormula = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formulatranheadunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Formulatranheadunkid() As Integer
        Get
            Return mintFormulatranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintFormulatranheadunkid = value
            Call GetData()
        End Set
    End Property



    ''' <summary>
    ''' Purpose: Get or Set tranheadformulacumulativetranunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Tranheadformulacumulativetranunkid() As Integer
        Get
            Return mintTranheadformulacumulativetranunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadformulacumulativetranunkid = value
            'Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = value
        End Set
    End Property

    'Sohail (28 Jan 2019) -- Start
    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
    Private xDataOpr As clsDataOperation = Nothing
    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'Sohail (28 Jan 2019) -- End

#End Region

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadformulacumulativetranunkid " & _
              ", formulatranheadunkid " & _
              ", tranheadunkid " & _
              ", ISNULL(isvoid, 0) AS isvoid " & _
              ", ISNULL(voiduserunkid, -1) AS voiduserunkid " & _
              ", voiddatetime " & _
              ", ISNULL(voidreason, '') AS voidreason " & _
              ", ISNULL(defaultvalue_id, 0) AS defaultvalue_id " & _
              ", ISNULL(periodunkid, 0) AS periodunkid " & _
              ", ISNULL(tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
              ", '' AS AUD " & _
             "FROM prtranhead_formula_cumulative_tran " & _
             "WHERE formulatranheadunkid = @formulatranheadunkid " & _
             "AND ISNULL(isvoid,0) = 0 "

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormulatranheadunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtFormula.Rows.Clear()
            Dim dRow As DataRow

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                dRow = mdtFormula.NewRow
                dRow.Item("formulatranheadunkid") = CInt(dtRow.Item("formulatranheadunkid"))
                dRow.Item("tranheadunkid") = CInt(dtRow.Item("tranheadunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime").ToString
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                dRow.Item("defaultvalue_id") = CInt(dtRow.Item("defaultvalue_id"))
                dRow.Item("periodunkid") = CInt(dtRow.Item("periodunkid"))
                dRow.Item("tranheadslabtranunkid") = CInt(dtRow.Item("tranheadslabtranunkid"))

                dRow.Item("AUD") = dtRow.Item("AUD").ToString
                mdtFormula.Rows.Add(dRow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal intTranHeadUnkID As Integer, Optional ByVal strTableName As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadformulacumulativetranunkid " & _
              ", formulatranheadunkid " & _
              ", tranheadunkid " & _
              ", ISNULL(isvoid, 0) AS isvoid " & _
              ", ISNULL(voiduserunkid, -1) AS voiduserunkid " & _
              ", voiddatetime " & _
              ", ISNULL(voidreason, '') AS voidreason " & _
              ", ISNULL(defaultvalue_id, 0) AS defaultvalue_id " & _
              ", ISNULL(prtranhead_formula_cumulative_tran.periodunkid, 0) AS periodunkid " & _
              ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
              ", ISNULL(prtranhead_formula_cumulative_tran.tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
             "FROM prtranhead_formula_cumulative_tran " & _
             "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_cumulative_tran.periodunkid " & _
             "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
             "WHERE formulatranheadunkid = @formulatranheadunkid " & _
             " AND ISNULL(isvoid, 0) = 0 "

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
        Return dsList
    End Function

    Public Function InsertDelete(ByVal mintOldTranheadslabtranunkid As Integer, ByVal mintNewTranheadslabtranunkid As Integer, ByVal intUserunkid As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [intUserunkid, dtCurrentDateAndTime]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            For i = 0 To mdtFormula.Rows.Count - 1
                With mdtFormula.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then

                        If CInt(.Item("tranheadslabtranunkid")) <> mintOldTranheadslabtranunkid Then Continue For

                        Select Case .Item("AUD")
                            Case "A"

                                strQ = "INSERT INTO prtranhead_formula_cumulative_tran ( " & _
                                    "  formulatranheadunkid " & _
                                    ", tranheadunkid" & _
                                    ", isvoid " & _
                                    ", voiduserunkid " & _
                                    ", voiddatetime " & _
                                    ", voidreason " & _
                                    ", defaultvalue_id " & _
                                    ", periodunkid " & _
                                    ", tranheadslabtranunkid " & _
                                ") VALUES (" & _
                                    "  @formulatranheadunkid " & _
                                    ", @tranheadunkid" & _
                                    ", @isvoid " & _
                                    ", @voiduserunkid " & _
                                    ", @voiddatetime " & _
                                    ", @voidreason " & _
                                    ", @defaultvalue_id " & _
                                    ", @periodunkid " & _
                                    ", @tranheadslabtranunkid " & _
                                "); SELECT @@identity"

                                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormulatranheadunkid.ToString)
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isvoid")))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@defaultvalue_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("defaultvalue_id").ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid").ToString)
                                objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNewTranheadslabtranunkid)

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintTranheadformulacumulativetranunkid = dsList.Tables(0).Rows(0)(0)
                                If .Item("formulatranheadunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_cumulative_tran", "tranheadformulacumulativetranunkid", mintTranheadformulacumulativetranunkid, 2, 1) = False Then
                                        Return False
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_cumulative_tran", "tranheadformulacumulativetranunkid", mintTranheadformulacumulativetranunkid, 1, 1) = False Then
                                        Return False
                                    End If
                                End If

                            Case "D"

                                If .Item("formulatranheadunkid") > 0 Then
                                    If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_cumulative_tran", "tranheadformulacumulativetranunkid", 2, 3, "formulatranheadunkid", "tranheadunkid = " & .Item("tranheadunkid") & " AND tranheadslabtranunkid = " & .Item("tranheadslabtranunkid") & "") = False Then
                                        Return False
                                    End If


                                    strQ = "UPDATE prtranhead_formula_cumulative_tran SET " & _
                                            " isvoid = 1 " & _
                                            ", voiduserunkid = @voiduserunkid " & _
                                            ", voiddatetime = @voiddatetime " & _
                                            ", voidreason = @voidreason " & _
                                        "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                                        "AND tranheadunkid = @tranheadunkid " & _
                                        "AND tranheadslabtranunkid = @tranheadslabtranunkid "

                                    objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormulatranheadunkid.ToString)
                                    objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadunkid").ToString)
                                    objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadslabtranunkid").ToString)
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                                    'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                                    'Sohail (21 Aug 2015) -- End
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                    dsList = objDataOperation.ExecQuery(strQ, "List")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                        End Select
                    End If
                End With
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    Public Function VoidByTranHeadUnkID(ByVal intTranHeadUnkid As Integer, _
                                        ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intTranHeadUnkid, "prtranhead_formula_cumulative_tran", "tranheadformulacumulativetranunkid", 3, 3, "formulatranheadunkid") = False Then
                Return False
            End If

            strQ = "UPDATE prtranhead_formula_cumulative_tran SET " & _
                        " isvoid = 1 " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                    "WHERE formulatranheadunkid = @formulatranheadunkid "

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidByTranHeadUnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    Public Function GetCurrentSlabForProcessPayroll(ByVal strList As String, ByVal intTranHeadUnkID As Integer, ByVal dtPeriodEndDate As DateTime, Optional ByVal intSlabTranUnkId As Integer = -1, Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'strQ = "SELECT " & _
            '  " tranheadunkid " & _
            '  ", ISNULL(defaultvalue_id, 0) AS defaultvalue_id " & _
            '  ", ISNULL(tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
            ' "FROM prtranhead_formula_cumulative_tran " & _
            ' "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_cumulative_tran.periodunkid " & _
            ' "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
            ' "WHERE formulatranheadunkid = @formulatranheadunkid " & _
            ' " AND ISNULL(isvoid, 0) = 0 " & _
            ' "AND cfcommon_period_tran.end_date = ( SELECT    MAX(end_date) " & _
            '                                      "FROM      prtranhead_formula_cumulative_tran " & _
            '                                                "LEFT JOIN cfcommon_period_tran ON prtranhead_formula_cumulative_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                                      "WHERE     ISNULL(prtranhead_formula_cumulative_tran.isvoid, 0) = 0 " & _
            '                                                "AND prtranhead_formula_cumulative_tran.formulatranheadunkid = @formulatranheadunkid " & _
            '                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date " & _
            '                                    ") "
            strQ = "SELECT  tranheadunkid  " & _
                          ", defaultvalue_id " & _
                          ", tranheadslabtranunkid " & _
                          ", formulatranheadunkid " & _
                          ", calctype_id_head_in_formula " & _
                    "FROM    ( SELECT    prtranhead_formula_cumulative_tran.tranheadunkid  " & _
              ", ISNULL(defaultvalue_id, 0) AS defaultvalue_id " & _
              ", ISNULL(tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
                                      ", formulatranheadunkid " & _
                                      ", ISNULL(prtranhead_master.calctype_id, 0) AS calctype_id_head_in_formula " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY prtranhead_formula_cumulative_tran.formulatranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
             "FROM prtranhead_formula_cumulative_tran " & _
             "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_cumulative_tran.periodunkid " & _
             "LEFT JOIN prtranhead_master ON prtranhead_formula_cumulative_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
             "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                              "WHERE     prtranhead_formula_cumulative_tran.isvoid = 0 " & _
                                        "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
            'Sohail (25 Sep 2021) - [calctype_id, LEFT JOIN prtranhead_master]

            If intTranHeadUnkID > 0 Then
                strQ &= "AND formulatranheadunkid = @formulatranheadunkid "
            End If

            strQ &= "   ) AS A " & _
                    "WHERE   A.ROWNO = 1 "
            'Sohail (26 Aug 2016) -- End

            If intSlabTranUnkId >= 0 Then
                strQ &= " AND ISNULL(tranheadslabtranunkid, 0) = @tranheadslabtranunkid "
                objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSlabTranUnkId)
            End If

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)
            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCurrentSlabForProcessPayroll; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Return the Dataset of Flate Rate Transaction Heads UnkID which is used in the formula of specified transaction head.
    ''' Modify By : Sohail
    ''' </summary>
    ''' <param name="intTranHeadUnkID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function getFlateRateHeadFromFormula(ByVal intTranHeadUnkID As Integer, Optional ByVal strListName As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT DISTINCT prtranhead_master.tranheadunkid " & _
                                ", ISNULL(prtranhead_formula_cumulative_tran.defaultvalue_id, 0 ) AS defaultvalue_id " & _
                    "FROM prtranhead_formula_cumulative_tran " & _
                    "INNER JOIN prtranhead_master " & _
                    "ON prtranhead_formula_cumulative_tran.tranheadunkid = prtranhead_master.tranheadunkid " & _
                    "WHERE prtranhead_formula_cumulative_tran.formulatranheadunkid = @tranheadunkid " & _
                    "AND ISNULL(prtranhead_formula_cumulative_tran.isvoid, 0) = 0 " & _
                    "AND prtranhead_master.calctype_id IN (" & enCalcType.FlatRate_Others & ") " 'Flate Rate Salary, Flate Rate All

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getFlateRateHeadFromFormula; Module Name: " & mstrModuleName)
            Return dsList
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try

    End Function

    Public Function IsFormulaHeadRecursive(ByVal strDatabaseName As String, _
                                           ByVal intTranHeadUnkId As Integer, _
                                           ByVal intHeadTypeID As Integer, _
                                           ByVal intTypeOfID As Integer, _
                                           ByVal intFormulaTranHeadUnkId As Integer, _
                                           ByRef strMessageHeadName As String) As Boolean
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try

            If intTranHeadUnkId <= 0 AndAlso intTypeOfID <> enTypeOf.Salary AndAlso intHeadTypeID <> enTranHeadType.EarningForEmployees AndAlso intHeadTypeID <> enTranHeadType.DeductionForEmployee AndAlso intHeadTypeID <> enTranHeadType.EmployeesStatutoryDeductions Then Return False

            If intTypeOfID = enTypeOf.Salary Then
                Dim objTranHead As New clsTransactionHead
                'Sohail (28 Jan 2019) -- Start
                'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                objTranHead._xDataOperation = objDataOperation
                'Sohail (28 Jan 2019) -- End
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objTranHead._Tranheadunkid = intFormulaTranHeadUnkId
                objTranHead._Tranheadunkid(strDatabaseName) = intFormulaTranHeadUnkId
                'Sohail (21 Aug 2015) -- End

                If objTranHead._Formulaid.Contains("#BSL#") = True Then
                    strMessageHeadName = " Function 'BASIC SALARY' "
                    objTranHead = Nothing
                    Return True
                ElseIf objTranHead._Formulaid.Contains("#DBS#") = True Then
                    strMessageHeadName = " Function 'DAILY BASIC SALARY WEEKENDS EXCLUDED' "
                    objTranHead = Nothing
                    Return True
                ElseIf objTranHead._Formulaid.Contains("#HBS#") = True Then
                    strMessageHeadName = " Function 'HOURLY BASIC SALARY WEEKENDS EXCLUDED' "
                    objTranHead = Nothing
                    Return True
                ElseIf objTranHead._Formulaid.Contains("#DWI#") = True Then
                    strMessageHeadName = " Function 'DAILY BASIC SALARY WEEKENDS INCLUDED' "
                    objTranHead = Nothing
                    Return True
                ElseIf objTranHead._Formulaid.Contains("#HWI#") = True Then
                    strMessageHeadName = " Function 'HOURLY BASIC SALARY WEEKENDS INCLUDED' "
                    objTranHead = Nothing
                    Return True
                Else
                    objTranHead = Nothing
                End If
            End If

            strQ = "SELECT  prtranhead_formula_cumulative_tran.tranheadunkid " & _
                          ", prtranhead_master.trnheadname " & _
                          ", prtranhead_master.typeof_id " & _
                          ", prtranhead_master.calctype_id " & _
                    "FROM    prtranhead_formula_cumulative_tran " & _
                            "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = prtranhead_formula_cumulative_tran.tranheadunkid " & _
                    "WHERE   ISNULL(prtranhead_formula_cumulative_tran.isvoid, 0) = 0 " & _
                            "AND formulatranheadunkid = @formulatranheadunkid "

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormulaTranHeadUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count <= 0 Then
                Return False
            Else

                For Each dsRow As DataRow In dsList.Tables("List").Rows

                    If intTranHeadUnkId = CInt(dsRow.Item("tranheadunkid")) Then
                        strMessageHeadName = " Transaction Head '" & dsRow.Item("trnheadname").ToString & "' Itself "
                        Return True
                    End If

                    If intTypeOfID = enTypeOf.Salary AndAlso CInt(dsRow.Item("typeof_id")) = enTypeOf.Salary Then
                        strMessageHeadName = " Salary Head '" & dsRow.Item("trnheadname").ToString & "' "
                        Return True
                    End If

                    If intHeadTypeID = enTranHeadType.EarningForEmployees AndAlso (CInt(dsRow.Item("calctype_id")) = enCalcType.NET_PAY OrElse CInt(dsRow.Item("calctype_id")) = enCalcType.TOTAL_EARNING OrElse CInt(dsRow.Item("calctype_id")) = enCalcType.TAXABLE_EARNING_TOTAL OrElse CInt(dsRow.Item("calctype_id")) = enCalcType.NON_TAXABLE_EARNING_TOTAL) Then
                        strMessageHeadName = " Transaction Head '" & dsRow.Item("trnheadname").ToString & "' calculation Type of NET PAY or TOTAL EARNING "
                        Return True
                    End If

                    If intHeadTypeID = enTranHeadType.DeductionForEmployee AndAlso (CInt(dsRow.Item("calctype_id")) = enCalcType.NET_PAY OrElse CInt(dsRow.Item("calctype_id")) = enCalcType.TOTAL_DEDUCTION) Then
                        strMessageHeadName = " Transaction Head '" & dsRow.Item("trnheadname").ToString & "' calculation Type of NET PAY or TOTAL DEDUCTION "
                        Return True
                    End If

                    If intHeadTypeID = enTranHeadType.EmployeesStatutoryDeductions AndAlso (CInt(dsRow.Item("calctype_id")) = enCalcType.NET_PAY OrElse CInt(dsRow.Item("calctype_id")) = enCalcType.TOTAL_DEDUCTION) Then
                        strMessageHeadName = " Transaction Head '" & dsRow.Item("trnheadname").ToString & "' calculation Type of NET PAY or TOTAL DEDUCTION "
                        Return True
                    End If

                    If intHeadTypeID = enTranHeadType.Informational AndAlso (CInt(dsRow.Item("calctype_id")) = enCalcType.NON_CASH_BENEFIT_TOTAL) Then
                        strMessageHeadName = " Transaction Head '" & dsRow.Item("trnheadname").ToString & "' calculation Type of NON CASH BENEFIT TOTAL "
                        Return True
                    End If

                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'If IsFormulaHeadRecursive(intTranHeadUnkId, intHeadTypeID, intTypeOfID, CInt(dsRow.Item("tranheadunkid")), strMessageHeadName) = True Then
                    If IsFormulaHeadRecursive(strDatabaseName, intTranHeadUnkId, intHeadTypeID, intTypeOfID, CInt(dsRow.Item("tranheadunkid")), strMessageHeadName) = True Then
                        'Sohail (21 Aug 2015) -- End
                        Return True
                    End If

                Next

            End If


            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsFormulaHeadRecursive; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function



    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  tranheadformulacumulativetranunkid " & _
    '          ", formulatranheadunkid " & _
    '          ", tranheadunkid " & _
    '         "FROM prtranhead_formula_cumulative_tran "

    '        If blnOnlyActive Then
    '            strQ &= " WHERE isactive = 1 "
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (prtranhead_formula_cumulative_tran) </purpose>
    'Public Function Insert() As Boolean
    '    'If isExist(mstrName) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintformulatranheadunkid.ToString)
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)

    '        StrQ = "INSERT INTO prtranhead_formula_cumulative_tran ( " & _
    '          "  formulatranheadunkid " & _
    '          ", tranheadunkid" & _
    '        ") VALUES (" & _
    '          "  @formulatranheadunkid " & _
    '          ", @tranheadunkid" & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintTranheadformulaTranUnkId = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (prtranhead_formula_cumulative_tran) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, mintTranheadformulacumulativetranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@tranheadformulacumulativetranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadformulacumulativetranunkid.ToString)
    '        objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintformulatranheadunkid.ToString)
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadunkid.ToString)

    '        StrQ = "UPDATE prtranhead_formula_cumulative_tran SET " & _
    '          "  formulatranheadunkid = @formulatranheadunkid" & _
    '          ", tranheadunkid = @tranheadunkid " & _
    '        "WHERE tranheadformulacumulativetranunkid = @tranheadformulacumulativetranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (prtranhead_formula_cumulative_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "DELETE FROM prtranhead_formula_cumulative_tran " & _
    '        "WHERE tranheadformulacumulativetranunkid = @tranheadformulacumulativetranunkid "

    '        objDataOperation.AddParameter("@tranheadformulacumulativetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@tranheadformulacumulativetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  tranheadformulacumulativetranunkid " & _
    '          ", formulatranheadunkid " & _
    '          ", tranheadunkid " & _
    '         "FROM prtranhead_formula_cumulative_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND tranheadformulacumulativetranunkid <> @tranheadformulacumulativetranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@tranheadformulacumulativetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

End Class
