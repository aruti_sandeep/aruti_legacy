﻿'************************************************************************************************************************************
'Class Name : clsTranheadFormulaTran.vb
'Purpose    :
'Date       :10/10/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsTranheadFormulaLeaveTran

#Region " Private variables "
    Private Const mstrModuleName = "clsTranheadFormulaLeaveTran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

    Private mintTranheadformulaleavetypeunkid As Integer
    Private mintFormulatranheadunkid As Integer
    Private mintLeavetypeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintDefaultvalue_id As Integer = 0
    Private mintTranheadslabtranunkid As Integer = 0 'Sohail (24 Sep 2013)

    Private mdtFormula As DataTable
#End Region

#Region " Constructor "
    Public Sub New()
        mdtFormula = New DataTable("FormulaLeave")
        Dim dCol As New DataColumn

        Try
            mdtFormula.Columns.Add("formulatranheadunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("leavetypeunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtFormula.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtFormula.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = Nothing
            mdtFormula.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = String.Empty
            mdtFormula.Columns.Add("defaultvalue_id", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtFormula.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = 0 'Sohail (12 Apr 2013)
            mdtFormula.Columns.Add("tranheadslabtranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0 'Sohail (24 Sep 2013)
            mdtFormula.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = String.Empty

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Properties "

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property _Datasource() As DataTable
        Get
            Return mdtFormula
        End Get
        Set(ByVal value As DataTable)
            mdtFormula = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formulatranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Formulatranheadunkid() As Integer
        Get
            Return mintFormulatranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintFormulatranheadunkid = value
            Call GetData()
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set tranheadformulaleavetypeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadformulaleavetypeunkid() As Integer
        Get
            Return mintTranheadformulaleavetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadformulaleavetypeunkid = value
            'Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set leavetypeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Leavetypeunkid() As Integer
        Get
            Return mintLeavetypeunkid
        End Get
        Set(ByVal value As Integer)
            mintLeavetypeunkid = value
        End Set
    End Property

    'Sohail (28 Jan 2019) -- Start
    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
    Private xDataOpr As clsDataOperation = Nothing
    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'Sohail (28 Jan 2019) -- End

#End Region

#Region " Public Methods "

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadformulaleavetypeunkid " & _
              ", formulatranheadunkid " & _
              ", leavetypeunkid " & _
              ", ISNULL(isvoid, 0) AS isvoid " & _
              ", ISNULL(voiduserunkid, -1) AS voiduserunkid " & _
              ", voiddatetime " & _
              ", ISNULL(voidreason, '') AS voidreason " & _
              ", ISNULL(defaultvalue_id, 0) AS defaultvalue_id " & _
              ", ISNULL(periodunkid, 0) AS periodunkid " & _
              ", ISNULL(tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
              ", '' AS AUD " & _
             "FROM prtranhead_formula_leavetype_tran " & _
             "WHERE formulatranheadunkid = @formulatranheadunkid " & _
             "AND ISNULL(isvoid,0) = 0 "
            'Sohail (24 Sep 2013) - [tranheadslabtranunkid]
            'Sohail (12 Apr 2013) - [periodunkid]

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormulatranheadunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtFormula.Rows.Clear()
            Dim dRow As DataRow

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                dRow = mdtFormula.NewRow
                dRow.Item("formulatranheadunkid") = CInt(dtRow.Item("formulatranheadunkid"))
                dRow.Item("leavetypeunkid") = CInt(dtRow.Item("leavetypeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime").ToString
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                dRow.Item("defaultvalue_id") = CInt(dtRow.Item("defaultvalue_id"))
                dRow.Item("periodunkid") = CInt(dtRow.Item("periodunkid")) 'Sohail (12 Apr 2013)
                dRow.Item("tranheadslabtranunkid") = CInt(dtRow.Item("tranheadslabtranunkid")) 'Sohail (24 Sep 2013)
                dRow.Item("AUD") = dtRow.Item("AUD").ToString
                mdtFormula.Rows.Add(dRow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal intLeavetypeunkid As Integer, Optional ByVal strTableName As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadformulaleavetypeunkid " & _
              ", formulatranheadunkid " & _
              ", leavetypeunkid " & _
              ", ISNULL(isvoid, 0) AS isvoid " & _
              ", ISNULL(voiduserunkid, -1) AS voiduserunkid " & _
              ", voiddatetime " & _
              ", ISNULL(voidreason, '') AS voidreason " & _
              ", ISNULL(defaultvalue_id, 0) AS defaultvalue_id " & _
              ", ISNULL(prtranhead_formula_leavetype_tran.periodunkid, 0) AS periodunkid " & _
              ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
              ", ISNULL(prtranhead_formula_leavetype_tran.tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
             "FROM prtranhead_formula_leavetype_tran " & _
             "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_leavetype_tran.periodunkid " & _
             "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
             "WHERE formulatranheadunkid = @formulatranheadunkid " & _
             " AND ISNULL(isvoid, 0) = 0 "
            'Sohail (24 Sep 2013) - [tranheadslabtranunkid]
            'Sohail (12 Apr 2013) - [periodunkid, period_name]

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLeavetypeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
        Return dsList
    End Function

    Public Function InsertDelete(ByVal mintOldTranheadslabtranunkid As Integer, ByVal mintNewTranheadslabtranunkid As Integer, ByVal intUserunkid As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [intUserunkid, dtCurrentDateAndTime]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            For i = 0 To mdtFormula.Rows.Count - 1
                With mdtFormula.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then

                        'Sohail (24 Sep 2013) -- Start
                        'TRA - ENHANCEMENT
                        If CInt(.Item("tranheadslabtranunkid")) <> mintOldTranheadslabtranunkid Then Continue For
                        'Sohail (24 Sep 2013) -- End

                        Select Case .Item("AUD")
                            Case "A"

                                strQ = "INSERT INTO prtranhead_formula_leavetype_tran ( " & _
                                    "  formulatranheadunkid " & _
                                    ", leavetypeunkid" & _
                                    ", isvoid " & _
                                    ", voiduserunkid " & _
                                    ", voiddatetime " & _
                                    ", voidreason " & _
                                    ", defaultvalue_id " & _
                                    ", periodunkid " & _
                                    ", tranheadslabtranunkid " & _
                                ") VALUES (" & _
                                    "  @formulatranheadunkid " & _
                                    ", @leavetypeunkid" & _
                                    ", @isvoid " & _
                                    ", @voiduserunkid " & _
                                    ", @voiddatetime " & _
                                    ", @voidreason " & _
                                    ", @defaultvalue_id " & _
                                    ", @periodunkid " & _
                                    ", @tranheadslabtranunkid " & _
                                "); SELECT @@identity"
                                'Sohail (24 Sep 2013) - [tranheadslabtranunkid]
                                'Sohail (12 Apr 2013) - [periodunkid]

                                objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormulatranheadunkid.ToString)
                                objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("leavetypeunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, CBool(.Item("isvoid")))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@defaultvalue_id", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("defaultvalue_id").ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid").ToString) 'Sohail (12 Apr 2013)
                                objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNewTranheadslabtranunkid) 'Sohail (24 Sep 2013)

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintTranheadformulaleavetypeunkid = dsList.Tables(0).Rows(0)(0)
                                If .Item("formulatranheadunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_leavetype_tran", "tranheadformulaleavetypeunkid", mintTranheadformulaleavetypeunkid, 2, 1) = False Then
                                        Return False
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_leavetype_tran", "tranheadformulaleavetypeunkid", mintTranheadformulaleavetypeunkid, 1, 1) = False Then
                                        Return False
                                    End If
                                End If

                            Case "D"

                                If .Item("formulatranheadunkid") > 0 Then
                                    'Sohail (04 Oct 2013) -- Start
                                    'TRA - ENHANCEMENT
                                    'If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_leavetype_tran", "tranheadformulaleavetypeunkid", 2, 3, "formulatranheadunkid") = False Then
                                    '    Return False
                                    'End If
                                    If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintFormulatranheadunkid, "prtranhead_formula_leavetype_tran", "tranheadformulaleavetypeunkid", 2, 3, "formulatranheadunkid", "leavetypeunkid = " & .Item("leavetypeunkid") & " AND tranheadslabtranunkid = " & .Item("tranheadslabtranunkid") & "") = False Then
                                        Return False
                                    End If
                                    'Sohail (04 Oct 2013) -- End

                                    strQ = "UPDATE prtranhead_formula_leavetype_tran SET " & _
                                            " isvoid = 1 " & _
                                            ", voiduserunkid = @voiduserunkid " & _
                                            ", voiddatetime = @voiddatetime " & _
                                            ", voidreason = @voidreason " & _
                                        "WHERE formulatranheadunkid = @formulatranheadunkid " & _
                                        "AND leavetypeunkid = @leavetypeunkid " & _
                                        "AND tranheadslabtranunkid = @tranheadslabtranunkid "
                                    'Sohail (24 Sep 2013) - [tranheadslabtranunkid]

                                    objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFormulatranheadunkid.ToString)
                                    objDataOperation.AddParameter("@leavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("leavetypeunkid").ToString) 'Sohail (22 Feb 2012)
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                                    'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                                    'Sohail (21 Aug 2015) -- End
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                    objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadslabtranunkid").ToString) 'Sohail (24 Sep 2013)

                                    dsList = objDataOperation.ExecQuery(strQ, "List")

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                        End Select
                    End If
                End With
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    Public Function VoidByTranHeadUnkID(ByVal intTranHeadUnkid As Integer, _
                                        ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intTranHeadUnkid, "prtranhead_formula_leavetype_tran", "tranheadformulaleavetypeunkid", 3, 3, "formulatranheadunkid") = False Then
                Return False
            End If

            strQ = "UPDATE prtranhead_formula_leavetype_tran SET " & _
                        " isvoid = 1 " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                    "WHERE formulatranheadunkid = @formulatranheadunkid "

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: VoidByTranHeadUnkID; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    'Sohail (24 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function GetCurrentSlabForProcessPayroll(ByVal strList As String, ByVal intTranHeadUnkID As Integer, ByVal dtPeriodEndDate As DateTime, Optional ByVal intSlabTranUnkId As Integer = -1, Optional ByVal blnOnlySickLeave As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              " formulatranheadunkid " & _
              ", prtranhead_formula_leavetype_tran.leavetypeunkid " & _
              ", ISNULL(defaultvalue_id, 0) AS defaultvalue_id " & _
              ", ISNULL(tranheadslabtranunkid, 0) AS tranheadslabtranunkid " & _
              ", ISNULL(lvleavetype_master.issickleave, 0) AS issickleave " & _
             "FROM prtranhead_formula_leavetype_tran " & _
             "LEFT JOIN lvleavetype_master ON prtranhead_formula_leavetype_tran.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
             "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prtranhead_formula_leavetype_tran.periodunkid " & _
             "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
             "WHERE formulatranheadunkid = @formulatranheadunkid " & _
             " AND ISNULL(isvoid, 0) = 0 " & _
             "AND cfcommon_period_tran.end_date = ( SELECT    MAX(end_date) " & _
                                                  "FROM      prtranhead_formula_leavetype_tran " & _
                                                            "LEFT JOIN cfcommon_period_tran ON prtranhead_formula_leavetype_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                                                  "WHERE     ISNULL(prtranhead_formula_leavetype_tran.isvoid, 0) = 0 " & _
                                                            "AND prtranhead_formula_leavetype_tran.formulatranheadunkid = @formulatranheadunkid " & _
                                                            "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date " & _
                                                ") "

            If intSlabTranUnkId >= 0 Then
                strQ &= " AND ISNULL(tranheadslabtranunkid, 0) = @tranheadslabtranunkid "
                objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intSlabTranUnkId)
            End If

            If blnOnlySickLeave = True Then
                strQ &= " AND ISNULL(lvleavetype_master.issickleave, 0) = 1 "
            End If

            objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkID)
            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCurrentSlabForProcessPayroll; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
        Return dsList
    End Function
    'Sohail (24 Sep 2013) -- End

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  tranheadformulaleavetypeunkid " & _
    '          ", formulatranheadunkid " & _
    '          ", exchangerateunkid " & _
    '         "FROM prtranhead_formula_leavetype_tran "

    '        If blnOnlyActive Then
    '            strQ &= " WHERE isactive = 1 "
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (prtranhead_formula_leavetype_tran) </purpose>
    'Public Function Insert() As Boolean
    '    'If isExist(mstrName) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintformulatranheadunkid.ToString)
    '        objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintexchangerateunkid.ToString)

    '        StrQ = "INSERT INTO prtranhead_formula_leavetype_tran ( " & _
    '          "  formulatranheadunkid " & _
    '          ", exchangerateunkid" & _
    '        ") VALUES (" & _
    '          "  @formulatranheadunkid " & _
    '          ", @exchangerateunkid" & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        minttranheadformulaleavetypeunkid = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (prtranhead_formula_leavetype_tran) </purpose>
    'Public Function Update() As Boolean
    '    'If isExist(mstrName, minttranheadformulaleavetypeunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@tranheadformulaleavetypeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, minttranheadformulaleavetypeunkid.ToString)
    '        objDataOperation.AddParameter("@formulatranheadunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintformulatranheadunkid.ToString)
    '        objDataOperation.AddParameter("@exchangerateunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintexchangerateunkid.ToString)

    '        StrQ = "UPDATE prtranhead_formula_leavetype_tran SET " & _
    '          "  formulatranheadunkid = @formulatranheadunkid" & _
    '          ", exchangerateunkid = @exchangerateunkid " & _
    '        "WHERE tranheadformulacurrenytranunkid = @tranheadformulaleavetypeunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Delete Database Table (prtranhead_formula_leavetype_tran) </purpose>
    'Public Function Delete(ByVal intUnkid As Integer) As Boolean
    '    If isUsed(intUnkid) Then
    '        mstrMessage = "<Message>"
    '        Return False
    '    End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "DELETE FROM prtranhead_formula_leavetype_tran " & _
    '        "WHERE tranheadformulaleavetypeunkid = @tranheadformulaleavetypeunkid "

    '        objDataOperation.AddParameter("@tranheadformulaleavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@tranheadformulaleavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  tranheadformulaleavetypeunkid " & _
    '          ", formulatranheadunkid " & _
    '          ", exchangerateunkid " & _
    '         "FROM prtranhead_formula_leavetype_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND tranheadformulaleavetypeunkid <> @tranheadformulaleavetypeunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@tranheadformulaleavetypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

#End Region

End Class
