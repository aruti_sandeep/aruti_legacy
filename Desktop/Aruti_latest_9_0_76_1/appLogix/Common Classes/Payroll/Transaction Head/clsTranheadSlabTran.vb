﻿'************************************************************************************************************************************
'Class Name : clsTranheadSlabTran.vb
'Purpose    :
'Date       :09/07/2010
'Written By :Suhail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Suhail
''' </summary>
Public Class clsTranheadSlabTran
    

#Region " Private variables "
    Private Shared ReadOnly mstrModuleName As String = "clsTranheadSlabTran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintTranheadunkid As Integer = -1

    Private mintTranheadslabtranunkid As Integer
    Private mdecAmountupto As Decimal 'Sohail (11 May 2011)
    Private mintSlabtype As Integer
    Private mdecValuebasis As Decimal 'Sohail (11 May 2011)
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Sohail (24 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Private mstrFormula As String = String.Empty
    Private mstrFormulaid As String = String.Empty

    Private mdtFormula As DataTable
    Private mdtFormulaCurrency As DataTable
    Private mdtFormulaLeave As DataTable
    Private mdtFormulaPeriodWiseSlab As DataTable
    Private mdtFormulaMeasurementUnit As DataTable
    Private mdtFormulaActivityUnit As DataTable
    Private mdtFormulaMeasurementAmount As DataTable
    Private mdtFormulaActivityAmount As DataTable
    'Sohail (24 Sep 2013) -- End
    Private mdtFormulaShift As DataTable 'Sohail (29 Oct 2013)
    Private mdtFormulaCumulative As DataTable 'Sohail (09 Nov 2013)
    Private mdtFormulaCMaster As DataTable 'Sohail (02 Oct 2014)
    Private mdtFormulaCRExpense As DataTable 'Sohail (12 Nov 2014)
    Private mdtFormulaLoanScheme As DataTable 'Sohail (24 Oct 2017)

    Private mdtSimpleSlab As DataTable

#End Region

#Region " Constructor "
    Public Sub New()
        mdtSimpleSlab = New DataTable("SimpleSlab")
        Dim dCol As New DataColumn

        Try
            dCol = New DataColumn("tranheadslabtranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("tranheadunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("amountupto")
            dCol.DataType = System.Type.GetType("System.Decimal") 'Sohail (11 May 2011)
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("slabtype")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("valuebasis")
            dCol.DataType = System.Type.GetType("System.Decimal") 'Sohail (11 May 2011)
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtSimpleSlab.Columns.Add(dCol)

            'Sohail (21 Nov 2011) -- Start
            dCol = New DataColumn("periodunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("period_name")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("end_date")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtSimpleSlab.Columns.Add(dCol)
            'Sohail (21 Nov 2011) -- End

            'Sohail (24 Sep 2013) -- Start
            'TRA - ENHANCEMENT
            dCol = New DataColumn("formula")
            dCol.DataType = System.Type.GetType("System.String")
            mdtSimpleSlab.Columns.Add(dCol)

            dCol = New DataColumn("formulaid")
            dCol.DataType = System.Type.GetType("System.String")
            mdtSimpleSlab.Columns.Add(dCol)
            'Sohail (24 Sep 2013) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Properties "

    Public Property _Datasource() As DataTable
        Get
            Return mdtSimpleSlab
        End Get
        Set(ByVal value As DataTable)
            mdtSimpleSlab = value
        End Set
    End Property

    'Sohail (24 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _FormulaDatasource() As DataTable
        Get
            Return mdtFormula
        End Get
        Set(ByVal value As DataTable)
            mdtFormula = value
        End Set
    End Property

    Public Property _FormulaCurrencyDatasource() As DataTable
        Get
            Return mdtFormulaCurrency
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaCurrency = value
        End Set
    End Property

    Public Property _FormulaLeaveDatasource() As DataTable
        Get
            Return mdtFormulaLeave
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaLeave = value
        End Set
    End Property

    Public Property _FormulaMeasurementUnitDatasource() As DataTable
        Get
            Return mdtFormulaMeasurementUnit
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaMeasurementUnit = value
        End Set
    End Property

    Public Property _FormulaActivityUnitDatasource() As DataTable
        Get
            Return mdtFormulaActivityUnit
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaActivityUnit = value
        End Set
    End Property

    Public Property _FormulaMeasurementAmountDatasource() As DataTable
        Get
            Return mdtFormulaMeasurementAmount
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaMeasurementAmount = value
        End Set
    End Property

    Public Property _FormulaActivityAmountDatasource() As DataTable
        Get
            Return mdtFormulaActivityAmount
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaActivityAmount = value
        End Set
    End Property
    'Sohail (24 Sep 2013) -- End

    'Sohail (29 Oct 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _FormulaShiftDatasource() As DataTable
        Get
            Return mdtFormulaShift
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaShift = value
        End Set
    End Property
    'Sohail (29 Oct 2013) -- End

    'Sohail (09 Nov 2013) -- Start
    'TRA - ENHANCEMENT
    Public Property _FormulaCumulativeDatasource() As DataTable
        Get
            Return mdtFormulaCumulative
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaCumulative = value
        End Set
    End Property
    'Sohail (09 Nov 2013) -- End

    'Sohail (02 Oct 2014) -- Start
    'Enhancement - No Of Active Inactive Dependants RELATION WISE.
    Public Property _FormulaCMasterDatasource() As DataTable
        Get
            Return mdtFormulaCMaster
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaCMaster = value
        End Set
    End Property
    'Sohail (02 Oct 2014) -- End

    'Sohail (12 Nov 2014) -- Start
    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
    Public Property _FormulaCRExpenseDatasource() As DataTable
        Get
            Return mdtFormulaCRExpense
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaCRExpense = value
        End Set
    End Property
    'Sohail (12 Nov 2014) -- End

    'Sohail (24 Oct 2017) -- Start
    'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
    Public Property _FormulaLoanSchemeDatasource() As DataTable
        Get
            Return mdtFormulaLoanScheme
        End Get
        Set(ByVal value As DataTable)
            mdtFormulaLoanScheme = value
        End Set
    End Property
    'Sohail (24 Oct 2017) -- End

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = value
            Call GetData()
        End Set
    End Property



    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Suhail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadslabtranunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Tranheadslabtranunkid() As Integer
        Get
            Return mintTranheadslabtranunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadslabtranunkid = value
            'call GetData()
        End Set
    End Property

   

    ''' <summary>
    ''' Purpose: Get or Set amountupto
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Amountupto() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecAmountupto
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecAmountupto = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set slabtype
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Slabtype() As Integer
        Get
            Return mintSlabtype
        End Get
        Set(ByVal value As Integer)
            mintSlabtype = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set valuebasis
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Valuebasis() As Decimal 'Sohail (11 May 2011)
        Get
            Return mdecValuebasis
        End Get
        Set(ByVal value As Decimal) 'Sohail (11 May 2011)
            mdecValuebasis = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Sohail (24 Sep 2013) -- Start
    'TRA - ENHANCEMENT
    ''' <summary>
    ''' Purpose: Get or Set formula
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Formula() As String
        Get
            Return mstrFormula
        End Get
        Set(ByVal value As String)
            mstrFormula = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formulaid
    ''' Modify By: Suhail
    ''' </summary>
    Public Property _Formulaid() As String
        Get
            Return mstrFormulaid
        End Get
        Set(ByVal value As String)
            mstrFormulaid = value
        End Set
    End Property
    'Sohail (24 Sep 2013) -- End

    'Sohail (28 Jan 2019) -- Start
    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
    Private xDataOpr As clsDataOperation = Nothing
    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property
    'Sohail (28 Jan 2019) -- End

#End Region


    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim drSlab As DataRow

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            'Sohail (21 Nov 2011) -- Start
            'strQ = "SELECT " & _
            '  "  tranheadslabtranunkid " & _
            '  ", tranheadunkid " & _
            '  ", amountupto " & _
            '  ", slabtype " & _
            '  ", valuebasis " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            '  ", '' As AUD " & _
            ' "FROM prtranhead_slab_tran " & _
            ' "WHERE tranheadunkid = @tranheadunkid " 'Sohail (16 Oct 2010)
            strQ = "SELECT  prtranhead_slab_tran.tranheadslabtranunkid " & _
                  ", prtranhead_slab_tran.tranheadunkid " & _
                  ", prtranhead_slab_tran.amountupto " & _
                  ", prtranhead_slab_tran.slabtype " & _
                  ", prtranhead_slab_tran.valuebasis " & _
                  ", prtranhead_slab_tran.userunkid " & _
                  ", prtranhead_slab_tran.isvoid " & _
                  ", prtranhead_slab_tran.voiduserunkid " & _
                  ", prtranhead_slab_tran.voiddatetime " & _
                  ", prtranhead_slab_tran.voidreason " & _
                  ", prtranhead_slab_tran.periodunkid " & _
                  ", cfcommon_period_tran.period_name " & _
                  ", CONVERT(CHAR(8), cfcommon_period_tran.start_date,112) AS start_date " & _
                  ", CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) AS end_date " & _
                  ", prtranhead_slab_tran.formula " & _
                  ", prtranhead_slab_tran.formulaid " & _
                  ", '' AS AUD " & _
             "FROM prtranhead_slab_tran " & _
            "LEFT JOIN cfcommon_period_tran ON prtranhead_slab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            "WHERE   ISNULL(prtranhead_slab_tran.isvoid, 0) = 0 " & _
            "AND     prtranhead_slab_tran.tranheadunkid = @tranheadunkid "
            'Sohail (24 Sep 2013) - [formula, formulaid]
            'Sohail (21 Nov 2011) -- End

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtSimpleSlab.Clear()

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                drSlab = mdtSimpleSlab.NewRow()
                drSlab.Item("tranheadslabtranunkid") = CInt(dtRow.Item("tranheadslabtranunkid"))
                drSlab.Item("tranheadunkid") = CInt(dtRow.Item("tranheadunkid"))
                drSlab.Item("amountupto") = CDec(dtRow.Item("amountupto")) 'Sohail (11 May 2011)
                drSlab.Item("slabtype") = CInt(dtRow.Item("slabtype"))
                drSlab.Item("valuebasis") = CDec(dtRow.Item("valuebasis")) 'Sohail (11 May 2011)
                drSlab.Item("userunkid") = CInt(dtRow.Item("userunkid"))
                drSlab.Item("isvoid") = CBool(dtRow.Item("isvoid"))
                drSlab.Item("voiduserunkid") = CInt(dtRow.Item("voiduserunkid"))
                drSlab.Item("voiddatetime") = dtRow.Item("voiddatetime")
                drSlab.Item("voidreason") = dtRow.Item("voidreason")
                drSlab.Item("AUD") = dtRow.Item("AUD").ToString
                'Sohail (21 Nov 2011) -- Start
                drSlab.Item("periodunkid") = dtRow.Item("periodunkid").ToString
                drSlab.Item("period_name") = dtRow.Item("period_name").ToString
                drSlab.Item("end_date") = dtRow.Item("end_date").ToString
                'Sohail (21 Nov 2011) -- End
                'Sohail (24 Sep 2013) -- Start
                'TRA - ENHANCEMENT
                drSlab.Item("formula") = dtRow.Item("formula").ToString
                drSlab.Item("formulaid") = dtRow.Item("formulaid").ToString
                'Sohail (24 Sep 2013) -- End

                mdtSimpleSlab.Rows.Add(drSlab)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Sub

    Public Function InserUpdateDeleteSimpleSlab(ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]

        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet

        Try
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = New clsDataOperation
            If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            Else
                objDataOperation = xDataOpr
            End If
            objDataOperation.ClearParameters()
            'Sohail (28 Jan 2019) -- End

            For i = 0 To mdtSimpleSlab.Rows.Count - 1
                With mdtSimpleSlab.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO prtranhead_slab_tran ( " & _
                                    "  tranheadunkid " & _
                                    ", amountupto " & _
                                    ", slabtype " & _
                                    ", valuebasis " & _
                                    ", userunkid " & _
                                    ", isvoid " & _
                                    ", voiduserunkid " & _
                                    ", voiddatetime" & _
                                    ", voidreason " & _
                                    ", periodunkid " & _
                                    ", formula " & _
                                    ", formulaid " & _
                                ") VALUES (" & _
                                    "  @tranheadunkid " & _
                                    ", @amountupto " & _
                                    ", @slabtype " & _
                                    ", @valuebasis " & _
                                    ", @userunkid " & _
                                    ", @isvoid " & _
                                    ", @voiduserunkid " & _
                                    ", @voiddatetime" & _
                                    ", @voidreason " & _
                                    ", @periodunkid " & _
                                    ", @formula " & _
                                    ", @formulaid " & _
                                "); SELECT @@identity" 'Sohail (21 Nov 2011) - [periodunkid]
                                'Sohail (24 Sep 2013) - [formula, formulaid]

                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
                                objDataOperation.AddParameter("@amountupto", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amountupto").ToString) 'Sohail (11 May 2011)
                                objDataOperation.AddParameter("@slabtype", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("slabtype").ToString)
                                objDataOperation.AddParameter("@valuebasis", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("valuebasis").ToString) 'Sohail (11 May 2011)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid").ToString) 'Sohail (21 Nov 2011)
                                'Sohail (24 Sep 2013) -- Start
                                'TRA - ENHANCEMENT
                                objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("formula").ToString)
                                objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("formulaid").ToString)
                                'Sohail (24 Sep 2013) -- End

                                'Sohail (12 Oct 2011) -- Start
                                'objDataOperation.ExecNonQuery(strQ)
                                dsList = objDataOperation.ExecQuery(strQ, "List")
                                'Sohail (12 Oct 2011) -- End

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (12 Oct 2011) -- Start
                                mintTranheadslabtranunkid = dsList.Tables(0).Rows(0)(0)
                                If .Item("tranheadunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "prtranhead_slab_tran", "tranheadslabtranunkid", mintTranheadslabtranunkid, 2, 1) = False Then
                                        Return False
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", mintTranheadunkid, "prtranhead_slab_tran", "tranheadslabtranunkid", mintTranheadslabtranunkid, 1, 1) = False Then
                                        Return False
                                    End If
                                End If
                                'Sohail (12 Oct 2011) -- End

                                'Sohail (24 Sep 2013) -- Start
                                'TRA - ENHANCEMENT
                                If mdtFormula IsNot Nothing AndAlso mdtFormula.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormula As New clsTranheadFormulaTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormula._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormula._Formulatranheadunkid = mintTranheadunkid
                                    objFormula._Datasource = mdtFormula
                                    If objFormula.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid, CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                If mdtFormulaLeave IsNot Nothing AndAlso mdtFormulaLeave.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaLeave As New clsTranheadFormulaLeaveTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaLeave._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaLeave._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaLeave._Datasource = mdtFormulaLeave
                                    If objFormulaLeave.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid, CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                If mdtFormulaCurrency IsNot Nothing AndAlso mdtFormulaCurrency.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaCurrency As New clsTranheadFormulaCurrencyTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaCurrency._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaCurrency._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaCurrency._Datasource = mdtFormulaCurrency
                                    If objFormulaCurrency.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid, CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                If mdtFormulaMeasurementUnit IsNot Nothing AndAlso mdtFormulaMeasurementUnit.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaMeasurementUnit As New clsTranhead_formula_measurement_unit_Tran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaMeasurementUnit._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaMeasurementUnit._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaMeasurementUnit._Datasource = mdtFormulaMeasurementUnit
                                    If objFormulaMeasurementUnit.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid, CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                If mdtFormulaActivityUnit IsNot Nothing AndAlso mdtFormulaActivityUnit.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaActivityUnit As New clsTranhead_formula_activity_unit_Tran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaActivityUnit._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaActivityUnit._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaActivityUnit._Datasource = mdtFormulaActivityUnit
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'If objFormulaActivityUnit.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid) = False Then
                                    If objFormulaActivityUnit.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid, CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        'Sohail (21 Aug 2015) -- End
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                If mdtFormulaMeasurementAmount IsNot Nothing AndAlso mdtFormulaMeasurementAmount.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaMeasurementAmount As New clsTranhead_formula_measurement_amount_Tran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaMeasurementAmount._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaMeasurementAmount._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaMeasurementAmount._Datasource = mdtFormulaMeasurementAmount
                                    If objFormulaMeasurementAmount.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid, CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                If mdtFormulaActivityAmount IsNot Nothing AndAlso mdtFormulaActivityAmount.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaActivityAmount As New clsTranhead_formula_activity_amount_Tran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaActivityAmount._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaActivityAmount._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaActivityAmount._Datasource = mdtFormulaActivityAmount
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'If objFormulaActivityAmount.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid) = False Then
                                    If objFormulaActivityAmount.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid, CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        'Sohail (21 Aug 2015) -- End
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (24 Sep 2013) -- End

                                'Sohail (29 Oct 2013) -- Start
                                'TRA - ENHANCEMENT
                                If mdtFormulaShift IsNot Nothing AndAlso mdtFormulaShift.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaShift As New clsTranheadFormulaShiftTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaShift._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaShift._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaShift._Datasource = mdtFormulaShift
                                    If objFormulaShift.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid, CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (29 Oct 2013) -- End

                                'Sohail (09 Nov 2013) -- Start
                                'TRA - ENHANCEMENT
                                If mdtFormulaCumulative IsNot Nothing AndAlso mdtFormulaCumulative.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaCumulative As New clsTranheadFormulaCumulativeTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaCumulative._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaCumulative._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaCumulative._Datasource = mdtFormulaCumulative
                                    If objFormulaCumulative.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid, CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (09 Nov 2013) -- End

                                'Sohail (02 Oct 2014) -- Start
                                'Enhancement - No Of Active Inactive Dependants RELATION WISE.
                                If mdtFormulaCMaster IsNot Nothing AndAlso mdtFormulaCMaster.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaCMaster As New clsTranheadFormulaCMasterTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaCMaster._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaCMaster._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaCMaster._Datasource = mdtFormulaCMaster
                                    If objFormulaCMaster.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid, CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (02 Oct 2014) -- End

                                'Sohail (12 Nov 2014) -- Start
                                'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                                If mdtFormulaCRExpense IsNot Nothing AndAlso mdtFormulaCRExpense.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaCRExpense As New clsTranheadFormulaCRExpenseTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaCRExpense._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaCRExpense._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaCRExpense._Datasource = mdtFormulaCRExpense
                                    If objFormulaCRExpense.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid, CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (12 Nov 2014) -- End

                                'Sohail (24 Oct 2017) -- Start
                                'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
                                If mdtFormulaLoanScheme IsNot Nothing AndAlso mdtFormulaLoanScheme.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaLoanScheme As New clsTranheadFormulaLoanSchemeTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaLoanScheme._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaLoanScheme._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaLoanScheme._Datasource = mdtFormulaLoanScheme
                                    If objFormulaLoanScheme.InsertDelete(CInt(.Item("tranheadslabtranunkid")), mintTranheadslabtranunkid, CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (24 Oct 2017) -- End

                            Case "U"

                                strQ = "UPDATE prtranhead_slab_tran SET " & _
                                    "  tranheadunkid = @tranheadunkid" & _
                                    ", amountupto = @amountupto" & _
                                    ", slabtype = @slabtype" & _
                                    ", valuebasis = @valuebasis" & _
                                    ", userunkid = @userunkid" & _
                                    ", isvoid = @isvoid" & _
                                    ", voiduserunkid = @voiduserunkid" & _
                                    ", voiddatetime = @voiddatetime " & _
                                    ", voidreason = @voidreason " & _
                                    ", periodunkid = @periodunkid " & _
                                    ", formula = @formula " & _
                                    ", formulaid = @formulaid " & _
                                "WHERE tranheadslabtranunkid = @tranheadslabtranunkid " 'Sohail (21 Nov 2011) - [periodunkid]
                                'Sohail (24 Sep 2013) - [formula, formulaid]

                                objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadslabtranunkid").ToString)
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadunkid").ToString)
                                objDataOperation.AddParameter("@amountupto", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amountupto").ToString) 'Sohail (11 May 2011)
                                objDataOperation.AddParameter("@slabtype", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("slabtype").ToString)
                                objDataOperation.AddParameter("@valuebasis", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("valuebasis").ToString) 'Sohail (11 May 2011)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("periodunkid").ToString) 'Sohail (21 Nov 2011)
                                'Sohail (24 Sep 2013) -- Start
                                'TRA - ENHANCEMENT
                                objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("formula").ToString)
                                objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("formulaid").ToString)
                                'Sohail (24 Sep 2013) -- End

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (12 Oct 2011) -- Start
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", .Item("tranheadunkid").ToString, "prtranhead_slab_tran", "tranheadslabtranunkid", .Item("tranheadslabtranunkid").ToString, 2, 2) = False Then
                                    Return False
                                End If
                                'Sohail (12 Oct 2011) -- End

                                'Sohail (24 Sep 2013) -- Start
                                'TRA - ENHANCEMENT
                                If mdtFormula IsNot Nothing AndAlso mdtFormula.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormula As New clsTranheadFormulaTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormula._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormula._Formulatranheadunkid = mintTranheadunkid
                                    objFormula._Datasource = mdtFormula
                                    If objFormula.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                If mdtFormulaLeave IsNot Nothing AndAlso mdtFormulaLeave.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaLeave As New clsTranheadFormulaLeaveTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaLeave._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaLeave._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaLeave._Datasource = mdtFormulaLeave
                                    If objFormulaLeave.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                If mdtFormulaCurrency IsNot Nothing AndAlso mdtFormulaCurrency.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaCurrency As New clsTranheadFormulaCurrencyTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaCurrency._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaCurrency._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaCurrency._Datasource = mdtFormulaCurrency
                                    If objFormulaCurrency.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                If mdtFormulaMeasurementUnit IsNot Nothing AndAlso mdtFormulaMeasurementUnit.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaMeasurementUnit As New clsTranhead_formula_measurement_unit_Tran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaMeasurementUnit._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaMeasurementUnit._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaMeasurementUnit._Datasource = mdtFormulaMeasurementUnit
                                    If objFormulaMeasurementUnit.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                If mdtFormulaActivityUnit IsNot Nothing AndAlso mdtFormulaActivityUnit.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaActivityUnit As New clsTranhead_formula_activity_unit_Tran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaActivityUnit._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaActivityUnit._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaActivityUnit._Datasource = mdtFormulaActivityUnit
                                    If objFormulaActivityUnit.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                If mdtFormulaMeasurementAmount IsNot Nothing AndAlso mdtFormulaMeasurementAmount.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaMeasurementAmount As New clsTranhead_formula_measurement_amount_Tran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaMeasurementAmount._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaMeasurementAmount._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaMeasurementAmount._Datasource = mdtFormulaMeasurementAmount
                                    If objFormulaMeasurementAmount.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If

                                If mdtFormulaActivityAmount IsNot Nothing AndAlso mdtFormulaActivityAmount.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaActivityAmount As New clsTranhead_formula_activity_amount_Tran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaActivityAmount._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaActivityAmount._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaActivityAmount._Datasource = mdtFormulaActivityAmount
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'If objFormulaActivityAmount.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid"))) = False Then
                                    If objFormulaActivityAmount.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        'Sohail (21 Aug 2015) -- End
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (24 Sep 2013) -- End

                                'Sohail (29 Oct 2013) -- Start
                                'TRA - ENHANCEMENT
                                If mdtFormulaShift IsNot Nothing AndAlso mdtFormulaShift.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaShift As New clsTranheadFormulaShiftTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaShift._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaShift._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaShift._Datasource = mdtFormulaShift
                                    If objFormulaShift.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (29 Oct 2013) -- End

                                'Sohail (09 Nov 2013) -- Start
                                'TRA - ENHANCEMENT
                                If mdtFormulaCumulative IsNot Nothing AndAlso mdtFormulaCumulative.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaCumulative As New clsTranheadFormulaCumulativeTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaCumulative._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaCumulative._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaCumulative._Datasource = mdtFormulaCumulative
                                    If objFormulaCumulative.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (09 Nov 2013) -- End

                                'Sohail (02 Oct 2014) -- Start
                                'Enhancement - No Of Active Inactive Dependants RELATION WISE.
                                If mdtFormulaCMaster IsNot Nothing AndAlso mdtFormulaCMaster.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaCMaster As New clsTranheadFormulaCMasterTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaCMaster._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaCMaster._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaCMaster._Datasource = mdtFormulaCMaster
                                    If objFormulaCMaster.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (02 Oct 2014) -- End

                                'Sohail (12 Nov 2014) -- Start
                                'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                                If mdtFormulaCRExpense IsNot Nothing AndAlso mdtFormulaCRExpense.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaCRExpense As New clsTranheadFormulaCRExpenseTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaCRExpense._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaCRExpense._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaCRExpense._Datasource = mdtFormulaCRExpense
                                    If objFormulaCRExpense.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (12 Nov 2014) -- End

                                'Sohail (24 Oct 2017) -- Start
                                'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
                                If mdtFormulaLoanScheme IsNot Nothing AndAlso mdtFormulaLoanScheme.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                    Dim objFormulaLoanScheme As New clsTranheadFormulaLoanSchemeTran
                                    'Sohail (28 Jan 2019) -- Start
                                    'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                    objFormulaLoanScheme._DataOperation = objDataOperation
                                    'Sohail (28 Jan 2019) -- End
                                    objFormulaLoanScheme._Formulatranheadunkid = mintTranheadunkid
                                    objFormulaLoanScheme._Datasource = mdtFormulaLoanScheme
                                    If objFormulaLoanScheme.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                        objDataOperation.ReleaseTransaction(False)
                                        Return False
                                    End If
                                End If
                                'Sohail (24 Oct 2017) -- End

                            Case "D"

                                'Sohail (12 Oct 2011) -- Start
                                If .Item("tranheadslabtranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", .Item("tranheadunkid").ToString, "prtranhead_slab_tran", "tranheadslabtranunkid", .Item("tranheadslabtranunkid").ToString, 2, 3) = False Then
                                        Return False
                                    End If

                                    'Sohail (24 Sep 2013) -- Start
                                    'TRA - ENHANCEMENT
                                    'strQ = "DELETE FROM prtranhead_slab_tran " & _
                                    '      "WHERE tranheadslabtranunkid = @tranheadslabtranunkid "
                                    strQ = "UPDATE prtranhead_slab_tran SET " & _
                                               "  isvoid = 1 " & _
                                               ", voiduserunkid = @voiduserunkid " & _
                                               ", voiddatetime = @voiddatetime " & _
                                               ", voidreason = @voidreason " & _
                                        "WHERE tranheadslabtranunkid = @tranheadslabtranunkid "

                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                                    'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("userunkid").ToString))
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                                    'Sohail (21 Aug 2015) -- End
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")
                                    'Sohail (24 Sep 2013) -- End
                                objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("tranheadslabtranunkid").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                    'Sohail (24 Sep 2013) -- Start
                                    'TRA - ENHANCEMENT
                                    If mdtFormula IsNot Nothing AndAlso mdtFormula.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                        Dim objFormula As New clsTranheadFormulaTran
                                        'Sohail (28 Jan 2019) -- Start
                                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                        objFormula._DataOperation = objDataOperation
                                        'Sohail (28 Jan 2019) -- End
                                        objFormula._Formulatranheadunkid = mintTranheadunkid
                                        objFormula._Datasource = mdtFormula
                                        If objFormula.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End If

                                    If mdtFormulaLeave IsNot Nothing AndAlso mdtFormulaLeave.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                        Dim objFormulaLeave As New clsTranheadFormulaLeaveTran
                                        'Sohail (28 Jan 2019) -- Start
                                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                        objFormulaLeave._DataOperation = objDataOperation
                                        'Sohail (28 Jan 2019) -- End
                                        objFormulaLeave._Formulatranheadunkid = mintTranheadunkid
                                        objFormulaLeave._Datasource = mdtFormulaLeave
                                        If objFormulaLeave.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End If

                                    If mdtFormulaCurrency IsNot Nothing AndAlso mdtFormulaCurrency.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                        Dim objFormulaCurrency As New clsTranheadFormulaCurrencyTran
                                        'Sohail (28 Jan 2019) -- Start
                                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                        objFormulaCurrency._DataOperation = objDataOperation
                                        'Sohail (28 Jan 2019) -- End
                                        objFormulaCurrency._Formulatranheadunkid = mintTranheadunkid
                                        objFormulaCurrency._Datasource = mdtFormulaCurrency
                                        If objFormulaCurrency.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End If

                                    If mdtFormulaMeasurementUnit IsNot Nothing AndAlso mdtFormulaMeasurementUnit.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                        Dim objFormulaMeasurementUnit As New clsTranhead_formula_measurement_unit_Tran
                                        'Sohail (28 Jan 2019) -- Start
                                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                        objFormulaMeasurementUnit._DataOperation = objDataOperation
                                        'Sohail (28 Jan 2019) -- End
                                        objFormulaMeasurementUnit._Formulatranheadunkid = mintTranheadunkid
                                        objFormulaMeasurementUnit._Datasource = mdtFormulaMeasurementUnit
                                        If objFormulaMeasurementUnit.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End If

                                    If mdtFormulaActivityUnit IsNot Nothing AndAlso mdtFormulaActivityUnit.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                        Dim objFormulaActivityUnit As New clsTranhead_formula_activity_unit_Tran
                                        'Sohail (28 Jan 2019) -- Start
                                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                        objFormulaActivityUnit._DataOperation = objDataOperation
                                        'Sohail (28 Jan 2019) -- End
                                        objFormulaActivityUnit._Formulatranheadunkid = mintTranheadunkid
                                        objFormulaActivityUnit._Datasource = mdtFormulaActivityUnit
                                        If objFormulaActivityUnit.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End If

                                    If mdtFormulaMeasurementAmount IsNot Nothing AndAlso mdtFormulaMeasurementAmount.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                        Dim objFormulaMeasurementAmount As New clsTranhead_formula_measurement_amount_Tran
                                        'Sohail (28 Jan 2019) -- Start
                                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                        objFormulaMeasurementAmount._DataOperation = objDataOperation
                                        'Sohail (28 Jan 2019) -- End
                                        objFormulaMeasurementAmount._Formulatranheadunkid = mintTranheadunkid
                                        objFormulaMeasurementAmount._Datasource = mdtFormulaMeasurementAmount
                                        If objFormulaMeasurementAmount.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End If

                                    If mdtFormulaActivityAmount IsNot Nothing AndAlso mdtFormulaActivityAmount.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                        Dim objFormulaActivityAmount As New clsTranhead_formula_activity_amount_Tran
                                        'Sohail (28 Jan 2019) -- Start
                                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                        objFormulaActivityAmount._DataOperation = objDataOperation
                                        'Sohail (28 Jan 2019) -- End
                                        objFormulaActivityAmount._Formulatranheadunkid = mintTranheadunkid
                                        objFormulaActivityAmount._Datasource = mdtFormulaActivityAmount
                                        'Sohail (21 Aug 2015) -- Start
                                        'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                        'If objFormulaActivityAmount.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid"))) = False Then
                                        If objFormulaActivityAmount.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                            'Sohail (21 Aug 2015) -- End
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End If
                                    'Sohail (24 Sep 2013) -- End

                                    'Sohail (29 Oct 2013) -- Start
                                    'TRA - ENHANCEMENT
                                    If mdtFormulaShift IsNot Nothing AndAlso mdtFormulaShift.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                        Dim objFormulaShift As New clsTranheadFormulaShiftTran
                                        'Sohail (28 Jan 2019) -- Start
                                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                        objFormulaShift._DataOperation = objDataOperation
                                        'Sohail (28 Jan 2019) -- End
                                        objFormulaShift._Formulatranheadunkid = mintTranheadunkid
                                        objFormulaShift._Datasource = mdtFormulaShift
                                        If objFormulaShift.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End If
                                    'Sohail (29 Oct 2013) -- End

                                    'Sohail (09 Nov 2013) -- Start
                                    'TRA - ENHANCEMENT
                                    If mdtFormulaCumulative IsNot Nothing AndAlso mdtFormulaCumulative.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                        Dim objFormulaCumulative As New clsTranheadFormulaCumulativeTran
                                        'Sohail (28 Jan 2019) -- Start
                                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                        objFormulaCumulative._DataOperation = objDataOperation
                                        'Sohail (28 Jan 2019) -- End
                                        objFormulaCumulative._Formulatranheadunkid = mintTranheadunkid
                                        objFormulaCumulative._Datasource = mdtFormulaCumulative
                                        If objFormulaCumulative.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End If
                                    'Sohail (09 Nov 2013) -- End

                                    'Sohail (02 Oct 2014) -- Start
                                    'Enhancement - No Of Active Inactive Dependants RELATION WISE.
                                    If mdtFormulaCMaster IsNot Nothing AndAlso mdtFormulaCMaster.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                        Dim objFormulaCMaster As New clsTranheadFormulaCMasterTran
                                        'Sohail (28 Jan 2019) -- Start
                                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                        objFormulaCMaster._DataOperation = objDataOperation
                                        'Sohail (28 Jan 2019) -- End
                                        objFormulaCMaster._Formulatranheadunkid = mintTranheadunkid
                                        objFormulaCMaster._Datasource = mdtFormulaCMaster
                                        If objFormulaCMaster.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End If
                                    'Sohail (02 Oct 2014) -- End

                                    'Sohail (12 Nov 2014) -- Start
                                    'Voltamp Enhancement - Quentity Total and Amount Total for Leave Claim Request in transaction head formula.
                                    If mdtFormulaCRExpense IsNot Nothing AndAlso mdtFormulaCRExpense.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                        Dim objFormulaCRExpense As New clsTranheadFormulaCRExpenseTran
                                        'Sohail (28 Jan 2019) -- Start
                                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                        objFormulaCRExpense._DataOperation = objDataOperation
                                        'Sohail (28 Jan 2019) -- End
                                        objFormulaCRExpense._Formulatranheadunkid = mintTranheadunkid
                                        objFormulaCRExpense._Datasource = mdtFormulaCRExpense
                                        If objFormulaCRExpense.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End If
                                    'Sohail (12 Nov 2014) -- End

                                    'Sohail (24 Oct 2017) -- Start
                                    'CCBRT Enhancement - 70.1 - (Ref. Id - 73) - Add a loan function (like the leave function) in payroll where you can select loan scheme, interest or EMI, total loan amount, etc.
                                    If mdtFormulaLoanScheme IsNot Nothing AndAlso mdtFormulaLoanScheme.Select("tranheadslabtranunkid = " & CInt(.Item("tranheadslabtranunkid")) & " ").Length > 0 Then
                                        Dim objFormulaLoanScheme As New clsTranheadFormulaLoanSchemeTran
                                        'Sohail (28 Jan 2019) -- Start
                                        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
                                        objFormulaLoanScheme._DataOperation = objDataOperation
                                        'Sohail (28 Jan 2019) -- End
                                        objFormulaLoanScheme._Formulatranheadunkid = mintTranheadunkid
                                        objFormulaLoanScheme._Datasource = mdtFormulaLoanScheme
                                        If objFormulaLoanScheme.InsertDelete(CInt(.Item("tranheadslabtranunkid")), CInt(.Item("tranheadslabtranunkid")), CInt(.Item("userunkid").ToString), dtCurrentDateAndTime) = False Then
                                            objDataOperation.ReleaseTransaction(False)
                                            Return False
                                        End If
                                    End If
                                    'Sohail (24 Oct 2017) -- End

                                End If
                                'Sohail (12 Oct 2011) -- End

                        End Select
                    End If
                End With
            Next

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InserUpdateDeleteSimpleSlab", mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Suhail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Void Database Table (prtranhead_slab_tran) </purpose>
    Public Function Void(ByVal intTranHeadUnkid As Integer, _
                         ByVal intVoidUserID As Integer, _
                         ByVal dtVoidDateTime As DateTime, _
                         ByVal strVoidReason As String) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            'Sohail (12 Oct 2011) -- Start
            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intTranHeadUnkid, "prtranhead_slab_tran", "tranheadslabtranunkid", 3, 3) = False Then
                Return False
            End If
            'Sohail (12 Oct 2011) -- End

            'strQ = "DELETE FROM prtranhead_slab_tran " & _
            '"WHERE tranheadslabtranunkid = @tranheadslabtranunkid "
            strQ = "UPDATE prtranhead_slab_tran SET " & _
                         " isvoid = 1" & _
                         ", voiduserunkid = @voiduserunkid" & _
                         ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason " & _
            "WHERE tranheadunkid = @tranheadunkid "

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            If dtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(dtVoidDateTime) & " " & Format(mdtVoiddatetime, "HH:mm:ss"))
            End If

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function

    Public Function GetCurrentTaxSlab(ByVal intTranHeadUnkId As Integer, ByVal dtPeriodEndDate As DateTime, Optional ByVal xDataOp As clsDataOperation = Nothing) As DataTable
        'Sohail (03 May 2018) - [xDataOp]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dtTable As DataTable = Nothing

        'Sohail (03 May 2018) -- Start
        'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Sohail (03 May 2018) -- End

        Try
            'Sohail (26 Aug 2016) -- Start
            'Enhancement - 57.4 & 63.1 - Process Payroll Performance enhancement for TRA.
            'strQ = "SELECT  prtranhead_slab_tran.tranheadslabtranunkid " & _
            '      ", prtranhead_slab_tran.tranheadunkid " & _
            '      ", prtranhead_slab_tran.amountupto " & _
            '      ", prtranhead_slab_tran.slabtype " & _
            '      ", prtranhead_slab_tran.valuebasis " & _
            '      ", prtranhead_slab_tran.userunkid " & _
            '      ", prtranhead_slab_tran.isvoid " & _
            '      ", prtranhead_slab_tran.voiduserunkid " & _
            '      ", prtranhead_slab_tran.voiddatetime " & _
            '      ", prtranhead_slab_tran.voidreason " & _
            '      ", prtranhead_slab_tran.periodunkid " & _
            '      ", cfcommon_period_tran.period_name " & _
            '      ", CONVERT(CHAR(8), cfcommon_period_tran.start_date,112) AS start_date " & _
            '      ", CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) AS end_date " & _
            '      ", prtranhead_slab_tran.formula " & _
            '      ", prtranhead_slab_tran.formulaid " & _
            '      ", '' AS AUD " & _
            '"FROM    prtranhead_slab_tran " & _
            '"LEFT JOIN cfcommon_period_tran ON prtranhead_slab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '"WHERE   ISNULL(prtranhead_slab_tran.isvoid, 0) = 0 " & _
            '"AND     prtranhead_slab_tran.tranheadunkid = @tranheadunkid " & _
            '"AND cfcommon_period_tran.end_date = ( SELECT    MAX(end_date) " & _
            '                                      "FROM      prtranhead_slab_tran " & _
            '                                                "LEFT JOIN cfcommon_period_tran ON prtranhead_slab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
            '                                      "WHERE     ISNULL(prtranhead_slab_tran.isvoid, 0) = 0 " & _
            '                                                "AND prtranhead_slab_tran.tranheadunkid = @tranheadunkid " & _
            '                                                "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date " & _
            '                                    ") "
            'Sohail (24 Sep 2013) - [formula, formulaid]
            strQ = "SELECT  tranheadslabtranunkid  " & _
                          ", tranheadunkid " & _
                          ", amountupto " & _
                          ", slabtype " & _
                          ", valuebasis " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiduserunkid " & _
                          ", voiddatetime " & _
                          ", voidreason " & _
                          ", periodunkid " & _
                          ", period_name " & _
                          ", start_date " & _
                          ", end_date " & _
                          ", formula " & _
                          ", formulaid " & _
                          ", AUD " & _
                    "FROM    ( SELECT    prtranhead_slab_tran.tranheadslabtranunkid  " & _
                  ", prtranhead_slab_tran.tranheadunkid " & _
                  ", prtranhead_slab_tran.amountupto " & _
                  ", prtranhead_slab_tran.slabtype " & _
                  ", prtranhead_slab_tran.valuebasis " & _
                  ", prtranhead_slab_tran.userunkid " & _
                  ", prtranhead_slab_tran.isvoid " & _
                  ", prtranhead_slab_tran.voiduserunkid " & _
                  ", prtranhead_slab_tran.voiddatetime " & _
                  ", prtranhead_slab_tran.voidreason " & _
                  ", prtranhead_slab_tran.periodunkid " & _
                  ", cfcommon_period_tran.period_name " & _
                  ", CONVERT(CHAR(8), cfcommon_period_tran.start_date,112) AS start_date " & _
                  ", CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) AS end_date " & _
                  ", prtranhead_slab_tran.formula " & _
                  ", prtranhead_slab_tran.formulaid " & _
                  ", '' AS AUD " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY prtranhead_slab_tran.tranheadunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
            "FROM    prtranhead_slab_tran " & _
            "LEFT JOIN cfcommon_period_tran ON prtranhead_slab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                              "WHERE     prtranhead_slab_tran.isvoid = 0 " & _
                                        "AND cfcommon_period_tran.isactive = 1 " & _
                                        "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                                        "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) <= @end_date "

            If intTranHeadUnkId > 0 Then
                strQ &= "AND prtranhead_slab_tran.tranheadunkid = @tranheadunkid "
            End If

            strQ &= "       ) AS A " & _
                    "WHERE   A.ROWNO = 1 "
            'Sohail (26 Aug 2016) -- End

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkId.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = New DataView(dsList.Tables("List")).ToTable

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetCurrentTaxSlab; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (03 May 2018) -- Start
            'Enhancement : Removing global objects and passing objDataOperation in process payroll in 72.1.
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Sohail (03 May 2018) -- End
        End Try
        Return dtTable
    End Function

    'Sohail (15 Feb 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function MakeActive(ByVal intTranHeadUnkid As Integer, _
                         ByVal intUserID As Integer _
                         ) As Boolean
       

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "prtranhead_master", "tranheadunkid", intTranHeadUnkid, "prtranhead_slab_tran", "tranheadslabtranunkid", 2, 2) = False Then
                Return False
            End If

            strQ = "UPDATE prtranhead_slab_tran SET " & _
                         " isvoid = 0 " & _
                         ", voiduserunkid = -1 " & _
                         ", userunkid = @userunkid" & _
                         ", voiddatetime = @voiddatetime " & _
                         ", voidreason = @voidreason " & _
            "WHERE tranheadunkid = @tranheadunkid "

            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, String.Empty)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: MakeActive; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
    End Function
    'Sohail (15 Feb 2012) -- End

    ' Pinkal (22-Dec-2010) -- Start

    Public Function GetList(Optional ByVal strTableName As String = "List") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'objDataOperation = New clsDataOperation
        If xDataOpr Is Nothing Then
        objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End

        Try
            strQ = "SELECT " & _
              "  tranheadslabtranunkid " & _
              ", tranheadunkid " & _
              ", amountupto " & _
              ", slabtype " & _
              ", valuebasis " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", '' As AUD " & _
              ", periodunkid " & _
              ", formula " & _
              ", formulaid " & _
             "FROM prtranhead_slab_tran " 'Sohail (21 Nov 2011) - [periodunkid]
            'Sohail (24 Sep 2013) - [formula, formulaid]

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        Finally
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
        End Try
        Return dsList
    End Function

    '  Pinkal (22-Dec-2010) -- End

    'Sohail (06 Aug 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function GetSlabPeriodList(ByVal intModuleRefid As Integer, ByVal intYearunkid As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal intStatusID As Integer = 0 _
                                          , Optional ByVal intTranHeadUnkId As Integer = 0) As DataSet

        Dim dsList As New DataSet
        'Sohail (28 Jan 2019) -- Start
        'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
        'Dim objDataOperation As New clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        'Sohail (28 Jan 2019) -- End
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as periodunkid, ' ' +  @name  as name, '19000101' AS start_date, '19000101' AS end_date UNION "
            End If

            strQ &= "SELECT  DISTINCT prtranhead_slab_tran.periodunkid " & _
                          ", cfcommon_period_tran.period_name as name " & _
                          ", CONVERT(CHAR(8), cfcommon_period_tran.start_date, 112) AS start_date " & _
                          ", CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) AS end_date " & _
                    "FROM    prtranhead_slab_tran " & _
                            "LEFT JOIN cfcommon_period_tran ON prtranhead_slab_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND cfcommon_period_tran.modulerefid = @modulerefid "

            If intYearunkid > 0 Then
                strQ &= "AND cfcommon_period_tran.yearunkid = @YearId "
                objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearunkid)
            End If

            If intStatusID > 0 Then
                strQ &= "AND cfcommon_period_tran.statusid = @statusid "
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If

            If intTranHeadUnkId > 0 Then
                strQ &= "AND prtranhead_slab_tran.tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadUnkId)
            End If

            strQ &= "ORDER BY end_date "

            objDataOperation.AddParameter("@modulerefid", SqlDbType.Int, eZeeDataType.INT_SIZE, intModuleRefid)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))


            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "GetSlabPeriodList", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            'Sohail (28 Jan 2019) -- Start
            'NMB Enhancement - 76.1 - Option "Prorate Flat Rate head" on Transaction Head for Recurrent Head.
            'objDataOperation = Nothing
            If xDataOpr Is Nothing Then objDataOperation = Nothing
            'Sohail (28 Jan 2019) -- End
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function
    'Sohail (06 Aug 2013) -- End

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  tranheadslabtranunkid " & _
    '          ", tranheadunkid " & _
    '          ", amountupto " & _
    '          ", slabtype " & _
    '          ", valuebasis " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '         "FROM prtranhead_slab_tran "

    '        If blnOnlyActive Then
    '            strQ &= " WHERE isactive = 1 "
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (prtranhead_slab_tran) </purpose>
    'Public Function Insert() As Boolean
    '    '<TODO >
    '    'If isExist(mstrName) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
    '        objDataOperation.AddParameter("@amountupto", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblAmountupto.ToString)
    '        objDataOperation.AddParameter("@slabtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSlabtype.ToString)
    '        objDataOperation.AddParameter("@valuebasis", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblValuebasis.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

    '        strQ = "INSERT INTO prtranhead_slab_tran ( " & _
    '          "  tranheadunkid " & _
    '          ", amountupto " & _
    '          ", slabtype " & _
    '          ", valuebasis " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime" & _
    '        ") VALUES (" & _
    '          "  @tranheadunkid " & _
    '          ", @amountupto " & _
    '          ", @slabtype " & _
    '          ", @valuebasis " & _
    '          ", @userunkid " & _
    '          ", @isvoid " & _
    '          ", @voiduserunkid " & _
    '          ", @voiddatetime" & _
    '        "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintTranheadslabtranunkid = dsList.Tables(0).Rows(0).Item(0)

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (prtranhead_slab_tran) </purpose>
    'Public Function Update() As Boolean
    '    '<TODO >
    '    'If isExist(mstrName, mintTranheadslabtranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadslabtranunkid.ToString)
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
    '        objDataOperation.AddParameter("@amountupto", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblAmountupto.ToString)
    '        objDataOperation.AddParameter("@slabtype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSlabtype.ToString)
    '        objDataOperation.AddParameter("@valuebasis", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblValuebasis.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)

    '        strQ = "UPDATE prtranhead_slab_tran SET " & _
    '          "  tranheadunkid = @tranheadunkid" & _
    '          ", amountupto = @amountupto" & _
    '          ", slabtype = @slabtype" & _
    '          ", valuebasis = @valuebasis" & _
    '          ", userunkid = @userunkid" & _
    '          ", isvoid = @isvoid" & _
    '          ", voiduserunkid = @voiduserunkid" & _
    '          ", voiddatetime = @voiddatetime " & _
    '        "WHERE tranheadslabtranunkid = @tranheadslabtranunkid "

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isUsed(ByVal intUnkid As Integer) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "<Query>"

    '        objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Suhail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  tranheadslabtranunkid " & _
    '          ", tranheadunkid " & _
    '          ", amountupto " & _
    '          ", slabtype " & _
    '          ", valuebasis " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '         "FROM prtranhead_slab_tran " & _
    '         "WHERE name = @name " & _
    '         "AND code = @code "

    '        If intUnkid > 0 Then
    '            strQ &= " AND tranheadslabtranunkid <> @tranheadslabtranunkid"
    '        End If

    '        objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
    '        objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
    '        objDataOperation.AddParameter("@tranheadslabtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList.tables(0).rows.count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class