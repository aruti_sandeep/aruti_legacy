﻿'************************************************************************************************************************************
'Class Name : clsPaymentPosting_Tran.vb
'Purpose    :
'Date       : 11-04-2023
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsPaymentPosting_Tran
    Private Const mstrModuleName = "clsPaymentPosting_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private xDataOp As clsDataOperation
    Private mintPaymentPostingTranunkid As Integer
    Private mintPeriodunkid As Integer
    Private mstrBatchno As String = String.Empty
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentpostingtranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _PaymentPostingTranunkid() As Integer
        Get
            Return mintPaymentPostingTranunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymentPostingTranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchno
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Batchno() As String
        Get
            Return mstrBatchno
        End Get
        Set(ByVal value As String)
            mstrBatchno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  paymentpostingtranunkid " & _
              ", periodunkid " & _
              ", batchno " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM prpaymentposting_tran " & _
             "WHERE paymentpostingtranunkid = @paymentpostingtranunkid "

            objDataOperation.AddParameter("@paymentpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentPostingTranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPaymentPostingTranunkid = CInt(dtRow.Item("paymentpostingtranunkid"))
                mintperiodunkid = CInt(dtRow.Item("periodunkid"))
                mstrBatchno = dtRow.Item("batchno").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mintloginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnisweb = CBool(dtRow.Item("isweb"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintvoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intPeriodId As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
                      "  prpaymentposting_tran.paymentpostingtranunkid " & _
                      ", prpaymentposting_tran.periodunkid " & _
                      ", cfcommon_period_tran.period_code " & _
                      ", cfcommon_period_tran.period_name " & _
                      ", prpaymentposting_tran.batchno " & _
                      ", prpaymentposting_tran.userunkid " & _
                      ", prpaymentposting_tran.loginemployeeunkid " & _
                      ", prpaymentposting_tran.isweb " & _
                      ", prpaymentposting_tran.isvoid " & _
                      ", prpaymentposting_tran.voiduserunkid " & _
                      ", prpaymentposting_tran.voidloginemployeeunkid " & _
                      ", prpaymentposting_tran.voiddatetime " & _
                      ", prpaymentposting_tran.voidreason "

            strQ &= "FROM prpaymentposting_tran " & _
                    "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prpaymentposting_tran.periodunkid " & _
                 "WHERE prpaymentposting_tran.isvoid = 0 "

            If intPeriodId > 0 Then
                strQ &= " AND prpaymentposting_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prpaymentposting_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)
            objDataOperation.AddParameter("@batchno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchno.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            strQ = "INSERT INTO prpaymentposting_tran ( " & _
              "  periodunkid " & _
              ", batchno " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @periodunkid " & _
              ", @batchno " & _
              ", @userunkid " & _
              ", @loginemployeeunkid " & _
              ", @isweb " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidloginemployeeunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPaymentPostingTranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrail(objDataOperation, enAuditType.ADD) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prpaymentposting_tran) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintPaymentPostingTranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@paymentpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentPostingTranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)
            objDataOperation.AddParameter("@batchno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchno.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            strQ = "UPDATE prpaymentposting_tran SET " & _
              "  periodunkid = @periodunkid" & _
              ", batchno = @batchno" & _
              ", userunkid = @userunkid" & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", isweb = @isweb" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE paymentpostingtranunkid = @paymentpostingtranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrail(objDataOperation, enAuditType.EDIT) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prpaymentposting_tran) </purpose>
    Public Function Delete() As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE prpaymentposting_tran SET " & _
             "  isvoid = 1 " & _
             ", voiduserunkid = @voiduserunkid" & _
             ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
             ", voiddatetime = @voiddatetime" & _
             ", voidreason = @voidreason " & _
           "WHERE paymentpostingtranunkid = @paymentpostingtranunkid "


            objDataOperation.AddParameter("@paymentpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentPostingTranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._xDataOp = objDataOperation
            Me._PaymentPostingTranunkid = mintPaymentPostingTranunkid

            If InsertAuditTrail(objDataOperation, enAuditType.DELETE) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@paymentpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intPeriodUnkId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal strFilter As String = "", Optional ByRef str_BatchNo As String = "", Optional ByRef intPaymentPostingTranunkid As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            str_BatchNo = ""
            intPaymentPostingTranunkid = 0

            strQ = "SELECT " & _
              "  paymentpostingtranunkid " & _
              ", periodunkid " & _
              ", batchno " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM prpaymentposting_tran " & _
             "WHERE isvoid = 0 " & _
             "AND periodunkid = @periodunkid "

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            If intUnkid > 0 Then
                strQ &= " AND paymentpostingtranunkid <> @paymentpostingtranunkid"
                objDataOperation.AddParameter("@paymentpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                str_BatchNo = dsList.Tables(0).Rows(0).Item("batchno").ToString
                intPaymentPostingTranunkid = CInt(dsList.Tables(0).Rows(0).Item("paymentpostingtranunkid"))
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrail(ByVal objDataOp As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Try
            objDataOp.ClearParameters()
            objDataOp.AddParameter("@paymentpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentPostingTranunkid.ToString)
            objDataOp.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOp.AddParameter("@batchno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchno.ToString)
            objDataOp.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOp.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOp.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOp.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOp.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOp.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOp.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOp.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)

            strQ = "INSERT INTO atprpaymentposting_tran ( " & _
              "  paymentpostingtranunkid " & _
              ", periodunkid " & _
              ", batchno " & _
              ", audituserunkid " & _
              ", loginemployeeunkid " & _
              ", audittype " & _
              ", auditdatetime " & _
              ", isweb " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
            ") VALUES (" & _
              "  @paymentpostingtranunkid " & _
              ", @periodunkid " & _
              ", @batchno " & _
              ", @audituserunkid " & _
              ", @loginemployeeunkid " & _
              ", @audittype " & _
              ", @auditdatetime " & _
              ", @isweb " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
            "); SELECT @@identity"


            dsList = objDataOp.ExecQuery(strQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

End Class
