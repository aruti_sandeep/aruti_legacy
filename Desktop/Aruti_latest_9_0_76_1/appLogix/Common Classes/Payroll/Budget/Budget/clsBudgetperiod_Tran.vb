﻿'************************************************************************************************************************************
'Class Name : clsBudgetperiod_Tran.vb
'Purpose    :
'Date       :07/06/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudgetperiod_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsBudgetperiod_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgetperiodtranunkid As Integer
    Private mintBudgetunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

    Private mstrWebFormName As String = String.Empty
    Private mstrWebIP As String = ""
    Private mstrWebHost As String = ""
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetperiodtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetperiodtranunkid() As Integer
        Get
            Return mintBudgetperiodtranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetperiodtranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Budgetunkid() As Integer
        Get
            Return mintBudgetunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetunkid = value

        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    Public WriteOnly Property _WebFormName() As String
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHost() As String
        Set(ByVal value As String)
            mstrWebHost = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  budgetperiodtranunkid " & _
              ", budgetunkid " & _
              ", periodunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetperiod_tran " & _
             "WHERE budgetperiodtranunkid = @budgetperiodtranunkid "

            objDataOperation.AddParameter("@budgetperiodtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintBudgetperiodTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintbudgetperiodtranunkid = CInt(dtRow.Item("budgetperiodtranunkid"))
                mintbudgetunkid = CInt(dtRow.Item("budgetunkid"))
                mintperiodunkid = CInt(dtRow.Item("periodunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  budgetperiodtranunkid " & _
              ", budgetunkid " & _
              ", periodunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM bgbudgetperiod_tran " & _
             "WHERE isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    Public Function GetPeriodIDs(ByVal intBudgetUnkId As Integer) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim strIDs As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' " & _
                                                    "+ CAST(bgbudgetperiod_tran.periodunkid AS VARCHAR(MAX)) " & _
                                           "FROM     bgbudgetperiod_tran " & _
                                           "WHERE    bgbudgetperiod_tran.isvoid = 0 " & _
                                                    "AND bgbudgetperiod_tran.budgetunkid = @budgetunkid " & _
                                         "FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS IDs "

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strIDs = dsList.Tables(0).Rows(0).Item(0).ToString

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPeriodIDs; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return strIDs
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudgetperiod_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO bgbudgetperiod_tran ( " & _
                        "  budgetunkid " & _
                        ", periodunkid " & _
                        ", userunkid " & _
                        ", isvoid " & _
                        ", voiduserunkid " & _
                        ", voiddatetime " & _
                        ", voidreason" & _
                    ") VALUES (" & _
                        "  @budgetunkid " & _
                        ", @periodunkid " & _
                        ", @userunkid " & _
                        ", @isvoid " & _
                        ", @voiduserunkid " & _
                        ", @voiddatetime " & _
                        ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintBudgetperiodtranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditBudget_Period(objDataOperation, enAuditType.ADD) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function InsertAll(ByVal objDataOp As clsDataOperation, ByVal strPeriodIDs As String) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOp.ClearParameters()

        Try

            For Each strPeriod In strPeriodIDs.Split(",")

                mintPeriodunkid = CInt(strPeriod)

                'If isExist(mintBudgetunkid, mintPeriodunkid, , objDataOp, mintBudgetperiodtranunkid) = False Then

                If Insert(objDataOp) = False Then
                    Return False
                End If
                'Else
                '    If Update(objDataOp) = False Then
                '        Return False
                '    End If
                'End If

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (bgbudgetperiod_tran) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintBudgetperiodtranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@budgetperiodtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetperiodtranunkid.ToString)
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudgetperiod_tran SET " & _
                          "  budgetunkid = @budgetunkid" & _
                          ", periodunkid = @periodunkid" & _
                          ", userunkid = @userunkid" & _
                          ", isvoid = @isvoid" & _
                          ", voiduserunkid = @voiduserunkid" & _
                          ", voiddatetime = @voiddatetime" & _
                          ", voidreason = @voidreason " & _
                    "WHERE budgetperiodtranunkid = @budgetperiodtranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditBudget_Period(objDataOperation, enAuditType.EDIT) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudgetperiod_tran) </purpose>
    Public Function Void(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            objDataOperation.AddParameter("@budgetperiodtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE bgbudgetperiod_tran SET " & _
                         " isvoid = @isvoid" & _
                         ", voiduserunkid = @voiduserunkid" & _
                         ", voiddatetime = @voiddatetime" & _
                         ", voidreason = @voidreason " & _
                   "WHERE budgetperiodtranunkid = @budgetperiodtranunkid " & _
                   "AND isvoid = 0 "


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._Budgetperiodtranunkid = intUnkid

            If InsertAuditBudget_Period(objDataOperation, enAuditType.DELETE) = False Then
                If xDataOpr Is Nothing Then
                    objDataOperation.ReleaseTransaction(False)
                End If
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function VoidAllByBudgetUnkId(ByVal intBudgetUnkid As Integer _
                                         , ByVal intVoiduserunkid As Integer _
                                         , ByVal dtVoiddatetime As Date _
                                         , ByVal strVoidreason As String _
                                         , Optional ByVal xDataOpr As clsDataOperation = Nothing _
                                         ) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intRowsAffected As Integer = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try

            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebHost.Trim = "" Then mstrWebHost = getHostName()

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            strQ = "INSERT INTO atbgbudgetperiod_tran ( " & _
                                "  budgetperiodtranunkid " & _
                                ", budgetunkid " & _
                                ", periodunkid " & _
                                ", audittype " & _
                                ", audituserunkid " & _
                                ", auditdatetime " & _
                                ", ip " & _
                                ", machine_name " & _
                                ", form_name " & _
                                ", module_name1 " & _
                                ", module_name2 " & _
                                ", module_name3 " & _
                                ", module_name4 " & _
                                ", module_name5 " & _
                                ", isweb " & _
                         ") " & _
                         "SELECT " & _
                               "  budgetperiodtranunkid " & _
                                ", budgetunkid " & _
                                ", periodunkid " & _
                                ", 3 " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", '" & mstrWebIP & "' " & _
                                ", '" & mstrWebHost & "' " & _
                                ", @form_name " & _
                                ", @module_name1 " & _
                                ", @module_name2 " & _
                                ", @module_name3 " & _
                                ", @module_name4 " & _
                                ", @module_name5 " & _
                                ", @isweb " & _
                         "FROM bgbudgetperiod_tran " & _
                         "WHERE budgetunkid = @budgetunkid " & _
                         "AND isvoid = 0 "

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoiduserunkid.ToString)
            If dtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidreason.ToString)

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "UPDATE bgbudgetperiod_tran SET " & _
               " isvoid = @isvoid " & _
               ", voiduserunkid = @voiduserunkid" & _
               ", voiddatetime = @voiddatetime" & _
               ", voidreason = @voidreason " & _
             "WHERE budgetunkid = @budgetunkid " & _
             "AND isvoid = 0 "


            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: VoidAllByBudgetUnkId; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@budgetperiodtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intBudgetUnkid As Integer, ByVal intPeriodUnkid As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByRef intBudgetperiodtranunkid As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        intBudgetperiodtranunkid = 0

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                        "  budgetperiodtranunkid " & _
                 "FROM bgbudgetperiod_tran " & _
                 "WHERE isvoid = 0 " & _
                 "AND budgetunkid = @budgetunkid " & _
                 "AND periodunkid = @periodunkid "

            If intUnkid > 0 Then
                strQ &= " AND budgetperiodtranunkid <> @budgetperiodtranunkid"
            End If

            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBudgetUnkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkid)
            objDataOperation.AddParameter("@budgetperiodtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intBudgetperiodtranunkid = CInt(dsList.Tables(0).Rows(0).Item("budgetperiodtranunkid"))
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Private Function InsertAuditBudget_Period(ByVal xDataOpr As clsDataOperation, ByVal xAuditType As enAuditType) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try
            strQ = "INSERT INTO atbgbudgetperiod_tran ( " & _
                            "  budgetperiodtranunkid " & _
                            ", budgetunkid " & _
                            ", periodunkid " & _
                            ", audittype " & _
                            ", audituserunkid " & _
                            ", auditdatetime " & _
                            ", ip " & _
                            ", machine_name " & _
                            ", form_name " & _
                            ", module_name1 " & _
                            ", module_name2 " & _
                            ", module_name3 " & _
                            ", module_name4 " & _
                            ", module_name5 " & _
                            ", isweb " & _
                    ") VALUES (" & _
                            "  @budgetperiodtranunkid " & _
                            ", @budgetunkid " & _
                            ", @periodunkid " & _
                            ", @audittype " & _
                            ", @audituserunkid " & _
                            ", @auditdatetime " & _
                            ", @ip " & _
                            ", @machine_name " & _
                            ", @form_name " & _
                            ", @module_name1 " & _
                            ", @module_name2 " & _
                            ", @module_name3 " & _
                            ", @module_name4 " & _
                            ", @module_name5 " & _
                            ", @isweb " & _
                    ") "


            xDataOpr.ClearParameters()
            objDataOperation.AddParameter("@budgetperiodtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetperiodtranunkid.ToString)
            objDataOperation.AddParameter("@budgetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            xDataOpr.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType)
            xDataOpr.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            xDataOpr.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)

            If mstrWebIP.ToString().Trim.Length <= 0 Then
                mstrWebIP = getIP()
            End If
            xDataOpr.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)

            If mstrWebHost.ToString().Length <= 0 Then
                mstrWebHost = getHostName()
            End If
            xDataOpr.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHost)

            If mstrWebFormName.Trim.Length <= 0 Then
                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                xDataOpr.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                xDataOpr.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                xDataOpr.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            xDataOpr.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            xDataOpr.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            xDataOpr.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            xDataOpr.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            xDataOpr.ExecNonQuery(strQ)

            If xDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(xDataOpr.ErrorNumber & ": " & xDataOpr.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditBudget_Period; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
        Return True
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class