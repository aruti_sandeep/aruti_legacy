﻿'************************************************************************************************************************************
'Class Name : clsBudgetformula_Tran.vb
'Purpose    :
'Date       :21/06/2016
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System


''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBudgetformula_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsBudgetformula_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintBudgetformulatranunkid As Integer
    Private mintBudgetformulaunkid As Integer
    Private mdtEffective_Date As Date
    Private mstrFormulaid As String = String.Empty
    Private mstrFormula As String = String.Empty
    Private mdtFormula_Tran As DataTable = Nothing
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrWebFormName As String = ""
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetformulatranunkid
    ''' Modify By: sohail
    ''' </summary>
    Public Property _Budgetformulatranunkid(ByVal objDataOperation As clsDataOperation) As Integer
        Get
            Return mintBudgetformulatranunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetformulatranunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set budgetformulaunkid
    ''' Modify By: sohail
    ''' </summary>
    Public Property _Budgetformulaunkid() As Integer
        Get
            Return mintBudgetformulaunkid
        End Get
        Set(ByVal value As Integer)
            mintBudgetformulaunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set effective_date
    ''' Modify By: sohail
    ''' </summary>
    Public Property _Effective_Date() As Date
        Get
            Return mdtEffective_Date
        End Get
        Set(ByVal value As Date)
            mdtEffective_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formulaid
    ''' Modify By: sohail
    ''' </summary>
    Public Property _Formulaid() As String
        Get
            Return mstrFormulaid
        End Get
        Set(ByVal value As String)
            mstrFormulaid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set formula
    ''' Modify By: sohail
    ''' </summary>
    Public Property _Formula() As String
        Get
            Return mstrFormula
        End Get
        Set(ByVal value As String)
            mstrFormula = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set  Formula_Tran
    ''' Modify By: sohail
    ''' </summary>
    Public Property _Formula_Tran() As DataTable
        Get
            Return mdtFormula_Tran
        End Get
        Set(ByVal value As DataTable)
            mdtFormula_Tran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: sohail
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: sohail
    ''' </summary>
    Public WriteOnly Property _WebClientIP() As String
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: sohail
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property


#End Region

    ''' <summary>
    ''' Modify By: sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If

        Try
            strQ = " SELECT " & _
                      "  budgetformulatranunkid " & _
                      ", budgetformulaunkid " & _
                      ", effective_date " & _
                      ", formulaid " & _
                      ", formula " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM bgbudgetformula_tran " & _
                     "WHERE budgetformulatranunkid = @budgetformulatranunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetformulatranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulatranunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintBudgetformulatranunkid = CInt(dtRow.Item("budgetformulatranunkid"))
                mintBudgetformulaunkid = CInt(dtRow.Item("budgetformulaunkid"))
                mdtEffective_Date = dtRow.Item("effective_date")
                mstrFormulaid = dtRow.Item("formulaid").ToString
                mstrFormula = dtRow.Item("formula").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intFormulaUnkId As Integer = 0, Optional ByVal dtEffectiveDate As Date = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  * " & _
                    "FROM    ( SELECT    bgbudgetformula_master.budgetformulaunkid  " & _
                                      ", bgbudgetformula_master.formula_code " & _
                                      ", bgbudgetformula_master.formula_name " & _
                                      ", bgbudgetformula_tran.budgetformulatranunkid " & _
                                      ", CONVERT(CHAR(8), bgbudgetformula_tran.effective_date, 112) AS effectivedate " & _
                                      ", bgbudgetformula_tran.formulaid AS functionid " & _
                                      ", bgbudgetformula_tran.formula As [function] " & _
                                      ", '' As headid " & _
                                      ", '' As [head] " & _
                                      ", 0 AS defaultid " & _
                                      ", '' AS [default] " & _
                                      ", '' AS AUD " & _
                                      "/*, bgbudgetformula_tran.defaultvalue_id*/ " & _
                                      ", DENSE_RANK() OVER ( PARTITION BY bgbudgetformula_master.budgetformulaunkid ORDER BY bgbudgetformula_tran.effective_date DESC, bgbudgetformula_tran.budgetformulatranunkid DESC ) AS ROWNO " & _
                              "FROM      bgbudgetformula_master " & _
                                        "LEFT JOIN bgbudgetformula_tran ON bgbudgetformula_master.budgetformulaunkid = bgbudgetformula_tran.budgetformulaunkid " & _
                              "WHERE     bgbudgetformula_master.isvoid = 0 " & _
                                        "AND bgbudgetformula_tran.isvoid = 0 "

            If intFormulaUnkId > 0 Then
                strQ &= "AND bgbudgetformula_master.budgetformulaunkid = @budgetformulaunkid "
                objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFormulaUnkId)
            End If

            If dtEffectiveDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), bgbudgetformula_tran.effective_date, 112) <= @effective_date "
                objDataOperation.AddParameter("@effective_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtEffectiveDate))
            End If

            strQ &= "        ) AS A " & _
                    "WHERE 1 = 1 "

            If dtEffectiveDate <> Nothing Then
                strQ &= " AND A.ROWNO = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (bgbudgetformula_tran) </purpose>
    Public Function InsertDelete_Formula(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            If mdtFormula_Tran Is Nothing OrElse mdtFormula_Tran.Rows.Count <= 0 Then Return True

            For i = 0 To mdtFormula_Tran.Rows.Count - 1
                With mdtFormula_Tran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then

                        mdtEffective_Date = eZeeDate.convertDate(.Item("effectivedate").ToString()).Date
                        mstrFormulaid = .Item("functionid")
                        mstrFormula = .Item("function")

                        Select Case .Item("AUD")
                            Case "A"

                               
                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid)
                                objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(.Item("effectivedate").ToString()).Date)
                                objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("functionid").ToString())
                                objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("function").ToString())
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, "")

                                strQ = "INSERT INTO bgbudgetformula_tran ( " & _
                                          "  budgetformulaunkid " & _
                                          ", effective_date " & _
                                          ", formulaid " & _
                                          ", formula " & _
                                          ", userunkid " & _
                                          ", isvoid " & _
                                          ", voiduserunkid " & _
                                          ", voiddatetime " & _
                                          ", voidreason" & _
                                        ") VALUES (" & _
                                          "  @budgetformulaunkid " & _
                                          ", @effective_date " & _
                                          ", @formulaid " & _
                                          ", @formula " & _
                                          ", @userunkid " & _
                                          ", @isvoid " & _
                                          ", @voiduserunkid " & _
                                          ", @voiddatetime " & _
                                          ", @voidreason" & _
                                        "); SELECT @@identity"

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                _Budgetformulatranunkid(objDataOperation) = dsList.Tables(0).Rows(0).Item(0)

                                If ATInsertBudgetFormula_tran(objDataOperation, 1) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                            Case "U"

                                strQ = "UPDATE bgbudgetformula_tran SET " & _
                                          " effective_date = @effective_date " & _
                                          ", formulaid = @formulaid " & _
                                          ", formula  =  @formula " & _
                                          " WHERE budgetformulatranunkid = @budgetformulatranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@budgetformulatranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(.Item("budgetformulatranunkid")))
                                objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(.Item("effectivedate").ToString()).Date)
                                objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("functionid").ToString())
                                objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("function").ToString())
                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                _Budgetformulatranunkid(objDataOperation) = CInt(.Item("budgetformulatranunkid"))

                                If ATInsertBudgetFormula_tran(objDataOperation, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                            Case "D"

                                strQ = "UPDATE bgbudgetformula_tran SET " & _
                                         "  isvoid = 1" & _
                                         ", voiduserunkid = @voiduserunkid" & _
                                         ", voiddatetime = GetDate()" & _
                                         ", voidreason = @voidreason " & _
                                         " WHERE budgetformulaunkid = @budgetformulaunkid "

                                objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If ATInsertBudgetFormula_tran(objDataOperation, 3) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select

                    End If

                End With

            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDelete_Formula; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@budgetformulatranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (bgbudgetformula_master) </purpose>
    Public Function Delete(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "SELECT ISNULL(budgetformulatranunkid,0) AS budgetformulatranunkid  FROM bgbudgetformula_tran WHERE isvoid = 0 AND budgetformulaunkid = @budgetformulaunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            strQ = "UPDATE bgbudgetformula_tran SET " & _
                       "  isvoid = 1" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = GetDate()" & _
                       ", voidreason = @voidreason " & _
                       "WHERE budgetformulaunkid = @budgetformulaunkid AND isvoid = 0 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    _Budgetformulatranunkid(objDataOperation) = CInt(dsList.Tables(0).Rows(i)("budgetformulatranunkid"))
                    If ATInsertBudgetFormula_tran(objDataOperation, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  budgetformulatranunkid " & _
                      ", budgetformulaunkid " & _
                      ", effective_date " & _
                      ", formulaid " & _
                      ", formula " & _
                      ", defaultvalue_id " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                     "FROM bgbudgetformula_tran " & _
                     "WHERE name = @name " & _
                     "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND budgetformulatranunkid <> @budgetformulatranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@budgetformulatranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Private Function ATInsertBudgetFormula_tran(ByVal objDataOperation As clsDataOperation, ByVal intAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            strQ = "INSERT INTO atbgbudgetformula_tran ( " & _
                     " budgetformulaunkid " & _
                     ", effective_date " & _
                     ", formulaid " & _
                     ", formula " & _
                     ", AuditType " & _
                     ", audituserunkid " & _
                     ", auditdatetime " & _
                     ", ip " & _
                     ", machine_name " & _
                     ", form_name " & _
                     ", module_name1 " & _
                     ", module_name2 " & _
                     ", module_name3 " & _
                     ", module_name4 " & _
                     ", module_name5 " & _
                     ", isweb " & _
               ") VALUES (" & _
                    "  @budgetformulaunkid " & _
                    ", @effective_date " & _
                    ", @formulaid " & _
                    ", @formula " & _
                    ", @AuditType " & _
                    ", @audituserunkid " & _
                    ", Getdate() " & _
                    ", @ip " & _
                    ", @machine_name " & _
                    ", @form_name " & _
                    ", @module_name1 " & _
                    ", @module_name2 " & _
                    ", @module_name3 " & _
                    ", @module_name4 " & _
                    ", @module_name5 " & _
                    ", @isweb " & _
               "); "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@budgetformulaunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBudgetformulaunkid.ToString)
            objDataOperation.AddParameter("@effective_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEffective_Date.ToString)
            objDataOperation.AddParameter("@formulaid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormulaid.ToString)
            objDataOperation.AddParameter("@formula", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormula.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, intAuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            If mstrWebClientIP.ToString().Length <= 0 Then
                mstrWebClientIP = getIP()
            End If

            If mstrWebHostName.ToString().Length <= 0 Then
                mstrWebHostName = getHostName()
            End If

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 1, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ATInsertBudgetFormula_tran; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "WEB")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class