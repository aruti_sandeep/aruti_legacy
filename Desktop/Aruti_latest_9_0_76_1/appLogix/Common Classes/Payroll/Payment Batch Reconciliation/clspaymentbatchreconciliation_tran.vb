﻿'************************************************************************************************************************************
'Class Name : clspaymentbatchreconciliation_tran.vb
'Purpose    :
'Date       : 14-06-2023
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
'''
Public Class clspaymentbatchreconciliation_tran
    Private Const mstrModuleName = "clspaymentbatchreconciliation_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintPaymentBatchReconciliationunkid As Integer
    Private mstrBatchName As String = String.Empty
    Private mintPeriodunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintBranchunkid As Integer
    Private mstrAccountNo As String = String.Empty
    Private mintPaidCurrencyunkid As Integer
    Private mdblAmount As Double
    Private mdtBatchPostedDateTime As Date
    Private mintStatusunkid As Integer
    Private mstrResponseMessage As String = String.Empty
    Private mdtReconciliationDateTime As Date
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtTran As DataTable = Nothing
    Private mintDpndtbeneficetranunkid As Integer
    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    Private mstrClientReference As String = String.Empty
    Private mstrStatus As String = String.Empty
    'Hemant (21 Jul 2023) -- End
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentbatchpostingunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _PaymentBatchReconciliationunkid(Optional ByVal xDataOp As clsDataOperation = Nothing) As Integer
        Get
            Return mintPaymentBatchReconciliationunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymentBatchReconciliationunkid = value
            Call GetData(xDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchname
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _BatchName() As String
        Get
            Return mstrBatchName
        End Get
        Set(ByVal value As String)
            mstrBatchName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set branchunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Branchunkid() As Integer
        Get
            Return mintBranchunkid
        End Get
        Set(ByVal value As Integer)
            mintBranchunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accountno
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _AccountNo() As String
        Get
            Return mstrAccountNo
        End Get
        Set(ByVal value As String)
            mstrAccountNo = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set paidcurrencyunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _PaidCurrencyunkid() As Integer
        Get
            Return mintPaidCurrencyunkid
        End Get
        Set(ByVal value As Integer)
            mintPaidCurrencyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Amount() As Double
        Get
            Return mdblAmount
        End Get
        Set(ByVal value As Double)
            mdblAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchposteddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _BatchPostedDateTime() As Date
        Get
            Return mdtBatchPostedDateTime
        End Get
        Set(ByVal value As Date)
            mdtBatchPostedDateTime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set message
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ResponseMessage() As String
        Get
            Return mstrResponseMessage
        End Get
        Set(ByVal value As String)
            mstrResponseMessage = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _ReconciliationDateTime() As Date
        Get
            Return mdtReconciliationDateTime
        End Get
        Set(ByVal value As Date)
            mdtReconciliationDateTime = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dpndtbeneficetranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Dpndtbeneficetranunkid() As Integer
        Get
            Return mintDpndtbeneficetranunkid
        End Get
        Set(ByVal value As Integer)
            mintDpndtbeneficetranunkid = value
        End Set
    End Property

    'Hemant (21 Jul 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
    Public Property _ClientReference() As String
        Get
            Return mstrClientReference
        End Get
        Set(ByVal value As String)
            mstrClientReference = value
        End Set
    End Property

    Public Property _Status() As String
        Get
            Return mstrStatus
        End Get
        Set(ByVal value As String)
            mstrStatus = value
        End Set
    End Property
    'Hemant (21 Jul 2023) -- End


    Public Property _TranDataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal xDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  paymentreconciliationtranunkid " & _
                      ", batchname " & _
                      ", periodunkid " & _
                      ", employeeunkid " & _
                      ", branchunkid " & _
                      ", accountno " & _
                      ", paidcurrencyid " & _
                      ", amount" & _
                      ", batchposted_date " & _
                      ", statusunkid " & _
                      ", message " & _
                      ", reconciliation_date " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", dpndtbeneficetranunkid " & _
                      ", clientreference " & _
                      ", status " & _
             "FROM prpayment_reconciliation_tran " & _
             "WHERE paymentreconciliationtranunkid = @paymentreconciliationtranunkid "
            'Hemant (21 Jul 2023) -- [clientreference,status]

            objDataOperation.AddParameter("@paymentreconciliationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchReconciliationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPaymentBatchReconciliationunkid = CInt(dtRow.Item("paymentreconciliationtranunkid"))
                mstrBatchName = dtRow.Item("batchname").ToString
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintBranchunkid = CInt(dtRow.Item("branchunkid"))
                mstrAccountNo = dtRow.Item("accountno").ToString
                mintPaidCurrencyunkid = CInt(dtRow.Item("paidcurrencyid"))
                mdblAmount = CDbl(dtRow.Item("amount"))
                If IsDBNull(dtRow.Item("batchposted_date")) = False Then
                    mdtBatchPostedDateTime = dtRow.Item("batchposted_date")
                Else
                    mdtBatchPostedDateTime = Nothing
                End If
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrResponseMessage = CStr(dtRow.Item("message"))
                If IsDBNull(dtRow.Item("reconciliation_date")) = False Then
                    mdtReconciliationDateTime = dtRow.Item("reconciliation_date")
                Else
                    mdtReconciliationDateTime = Nothing
                End If
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintDpndtbeneficetranunkid = CInt(dtRow.Item("dpndtbeneficetranunkid"))
                'Hemant (21 Jul 2023) -- Start
                'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
                mstrClientReference = dtRow.Item("clientreference").ToString
                mstrStatus = dtRow.Item("status").ToString
                'Hemant (21 Jul 2023) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal strBatchName As String = "", Optional ByVal xDataOp As clsDataOperation = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
                      "  paymentreconciliationtranunkid " & _
                      ", batchname " & _
                      ", periodunkid " & _
                      ", prpayment_reconciliation_tran.employeeunkid " & _
                      ", CASE WHEN prpayment_reconciliation_tran.employeeunkid = - 1 THEN '000000' ELSE ISNULL(hremployee_master.employeecode, '') END AS employeecode " & _
                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee " & _
                      ", prpayment_reconciliation_tran.branchunkid " & _
                      ", hrmsConfiguration..cfpayrollgroup_master.groupname as  bankname" & _
                      ", hrmsConfiguration..cfbankbranch_master.branchcode " & _
                      ", hrmsConfiguration..cfbankbranch_master.branchname " & _
                      ", accountno " & _
                      ", paidcurrencyid " & _
                      ", amount" & _
                      ", batchposted_date " & _
                      ", statusunkid " & _
                      ", message " & _
                      ", reconciliation_date " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", dpndtbeneficetranunkid " & _
                      ", clientreference " & _
                      ", status "
            'Hemant (04 Aug 2023) -- [employeecode,employee,bankname,branchcode,branchname]
            'Hemant (21 Jul 2023) -- [clientreference,status]

            strQ &= "FROM prpayment_reconciliation_tran " & _
                    " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = prpayment_reconciliation_tran.employeeunkid " & _
                    " LEFT JOIN hrmsConfiguration..cfbankbranch_master ON hrmsConfiguration..cfbankbranch_master.branchunkid = prpayment_reconciliation_tran.branchunkid " & _
                    " JOIN hrmsconfiguration..cfpayrollgroup_master ON hrmsconfiguration..cfbankbranch_master.bankgroupunkid = hrmsconfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                 "WHERE prpayment_reconciliation_tran.isvoid = 0 "

            If strBatchName.Trim.Length > 0 Then
                strQ &= " AND prpayment_reconciliation_tran.batchname = @batchname "
                objDataOperation.AddParameter("@batchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strBatchName)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prpayment_reconciliation_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@paymentreconciliationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchReconciliationunkid.ToString)
            objDataOperation.AddParameter("@batchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchName.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountNo.ToString)
            objDataOperation.AddParameter("@paidcurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaidCurrencyunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblAmount.ToString)
            If mdtBatchPostedDateTime <> Nothing Then
                objDataOperation.AddParameter("@batchposted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBatchPostedDateTime.ToString)
            Else
                objDataOperation.AddParameter("@batchposted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@message", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrResponseMessage.ToString)
            If mdtReconciliationDateTime <> Nothing Then
                objDataOperation.AddParameter("@reconciliation_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReconciliationDateTime.ToString)
            Else
                objDataOperation.AddParameter("@reconciliation_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficetranunkid.ToString)
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            objDataOperation.AddParameter("@clientreference", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientReference.ToString)
            objDataOperation.AddParameter("@status", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStatus.ToString)
            'Hemant (21 Jul 2023) -- End


            strQ = "INSERT INTO prpayment_reconciliation_tran ( " & _
              "  batchname " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", branchunkid " & _
              ", accountno " & _
              ", paidcurrencyid " & _
              ", amount " & _
              ", batchposted_date " & _
              ", statusunkid " & _
              ", message " & _
              ", reconciliation_date " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", dpndtbeneficetranunkid " & _
              ", clientreference " & _
              ", status " & _
            ") VALUES (" & _
              "  @batchname " & _
              ", @periodunkid " & _
              ", @employeeunkid " & _
              ", @branchunkid " & _
              ", @accountno " & _
              ", @paidcurrencyid " & _
              ", @amount " & _
              ", @batchposted_date " & _
              ", @statusunkid " & _
              ", @message " & _
              ", @reconciliation_date " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @dpndtbeneficetranunkid " & _
              ", @clientreference " & _
              ", @status " & _
            "); SELECT @@identity"
            'Hemant (21 Jul 2023) -- [clientreference,status]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPaymentBatchReconciliationunkid = dsList.Tables(0).Rows(0).Item(0)

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAll(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Try
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = xDataOp
            End If

            For Each drRow As DataRow In mdtTran.Rows
                mintBranchunkid = CInt(drRow.Item("EMP_BRID"))
                mstrAccountNo = CStr(drRow.Item("EMP_AccountNo"))
                mintEmployeeunkid = CInt(drRow.Item("employeeunkid"))
                mintPeriodunkid = CInt(drRow.Item("periodunkid"))
                mdblAmount = CDbl(drRow.Item("amount"))
                mintPaidCurrencyunkid = CInt(drRow.Item("paidcurrencyid"))
                mintDpndtbeneficetranunkid = CInt(drRow.Item("dpndtbeneficetranunkid"))
                'Hemant (21 Jul 2023) -- Start
                'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
                mstrClientReference = CStr(drRow.Item("clientreference"))
                mstrStatus = CStr(drRow.Item("status"))
                'Hemant (21 Jul 2023) -- End
                If Insert(objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prpayment_reconciliation_tran) </purpose>
    Public Function Update(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'If isExist(mstrName, mintPaymentBatchPostingTranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@paymentreconciliationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchReconciliationunkid.ToString)
            objDataOperation.AddParameter("@batchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchName.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountNo.ToString)
            objDataOperation.AddParameter("@paidcurrencyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaidCurrencyunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdblAmount.ToString)
            If mdtBatchPostedDateTime <> Nothing Then
                objDataOperation.AddParameter("@batchposted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBatchPostedDateTime.ToString)
            Else
                objDataOperation.AddParameter("@batchposted_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@message", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrResponseMessage.ToString)
            If mdtReconciliationDateTime <> Nothing Then
                objDataOperation.AddParameter("@reconciliation_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReconciliationDateTime.ToString)
            Else
                objDataOperation.AddParameter("@reconciliation_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficetranunkid.ToString)
            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            objDataOperation.AddParameter("@clientreference", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientReference.ToString)
            objDataOperation.AddParameter("@status", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrStatus.ToString)
            'Hemant (21 Jul 2023) -- End

            strQ = "UPDATE prpayment_reconciliation_tran SET " & _
              "  batchname = @batchname " & _
              ", periodunkid = @periodunkid" & _
              ", employeeunkid = @employeeunkid" & _
              ", branchunkid = @branchunkid" & _
              ", accountno = @accountno" & _
              ", paidcurrencyid = @paidcurrencyid" & _
              ", amount = @amount" & _
              ", batchposted_date = @batchposted_date" & _
              ", statusunkid = @statusunkid" & _
              ", message = @message" & _
              ", reconciliation_date = @reconciliation_date" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", dpndtbeneficetranunkid = @dpndtbeneficetranunkid " & _
              ", clientreference = @clientreference " & _
              ", status = @status " & _
            "WHERE paymentreconciliationtranunkid = @paymentreconciliationtranunkid "
            'Hemant (21 Jul 2023) -- [clientreference,status]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdateAll(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Try
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = xDataOp
            End If

            For Each drRow As DataRow In mdtTran.Rows
                _PaymentBatchReconciliationunkid(objDataOperation) = CInt(drRow.Item("paymentreconciliationtranunkid"))
                mintStatusunkid = CInt(drRow.Item("statusunkid"))
                'Hemant (21 Jul 2023) -- Start
                'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
                'mdtReconciliationDateTime = CDate(drRow.Item("reconciliation_date"))
                If IsDBNull(drRow.Item("reconciliation_date")) = False Then
                mdtReconciliationDateTime = CDate(drRow.Item("reconciliation_date"))
                Else
                    mdtReconciliationDateTime = Nothing
                End If
                mstrStatus = drRow.Item("status")
                'Hemant (21 Jul 2023) -- End
                If Update(objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class
