﻿'************************************************************************************************************************************
'Class Name : clsemployee_timesheet.vb
'Purpose    :
'Date       :21-Oct-2016
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm
Imports System.Web 'Nilay (10 Jan 2017) 
Imports System.Threading 'Nilay (10 Jan 2017) 
Imports ArutiReport
Imports System

''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsBudgetEmp_timesheet
    Private Shared ReadOnly mstrModuleName As String = "clsBudgetEmp_timesheet"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintEmptimesheetunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mdtActivitydate As Date
    Private mintFundsourceunkid As Integer
    Private mintProjectcodeunkid As Integer
    Private mintActivityunkid As Integer

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
    'Private mintActivity_Hrs As Integer
    'Private mintApprovedActivity_Hrs As Integer
    Private mdecActivity_Hrs As Decimal
    Private mdecApprovedActivity_Hrs As Decimal
    'Pinkal (28-Mar-2018) -- End



    Private mstrDescription As String = String.Empty
    Private mintStatusunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintLoginEmployeeunkid As Integer = 0
    Private mintVoidLoginEmployeeunkid As Integer = 0
    Private mstrWebFormName As String = String.Empty
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mdtApproverList As DataTable = Nothing
    Private objApproval As New clstsemptimesheet_approval
    Private mdtEmployeeActivity As DataTable = Nothing
    Private mblnIsSubmitForApproval As Boolean = False 'Nilay (10 Jan 2017)
    Private objThread As Thread 'Nilay (10 Jan 2017)

    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
    Private mstrSubmissionRemark As String = ""
    'Pinkal (28-Jul-2018) -- End

    'Pinkal (06-Jan-2023) -- Start
    '(A1X-556) FHS/PSI - Hide Leave Day Fraction from add/edit leave form screen.
    Private mintCostCenterunkid As Integer = 0
    'Pinkal (06-Jan-2023) -- End


#End Region

#Region " Public eNums "

    Public Enum enBudgetTimesheetStatus
        APPROVED = 1
        PENDING = 2
        REJECTED = 3
        DELETED = 4
        CANCELLED = 6
        'Nilay (01 Apr 2017) -- Start
        SUBMIT_FOR_APPROVAL = 7
        VIEW_COMPLETED = 8
        'Nilay (01 Apr 2017) -- End

    End Enum

    Public Enum enBudgetTimesheetEmailType
        APPROVER = 1
        EMPLOYEE = 2
    End Enum

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set emptimesheetunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Emptimesheetunkid(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintEmptimesheetunkid
        End Get
        Set(ByVal value As Integer)
            mintEmptimesheetunkid = value
            Call GetData(objDataOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set activitydate
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Activitydate() As Date
        Get
            Return mdtActivitydate
        End Get
        Set(ByVal value As Date)
            mdtActivitydate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fundsourceunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Fundsourceunkid() As Integer
        Get
            Return mintFundsourceunkid
        End Get
        Set(ByVal value As Integer)
            mintFundsourceunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set projectcodeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Projectcodeunkid() As Integer
        Get
            Return mintProjectcodeunkid
        End Get
        Set(ByVal value As Integer)
            mintProjectcodeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set activityunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Activityunkid() As Integer
        Get
            Return mintActivityunkid
        End Get
        Set(ByVal value As Integer)
            mintActivityunkid = value
        End Set
    End Property

    'Pinkal (28-Mar-2018) -- Start
    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.

    '''' <summary>
    '''' Purpose: Get or Set activity_hrs
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Activity_Hrs() As Integer
    '    Get
    '        Return mintActivity_Hrs
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintActivity_Hrs = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set approvedactivity_hrs
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _ApprovedActivity_Hrs() As Integer
    '    Get
    '        Return mintApprovedActivity_Hrs
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintApprovedActivity_Hrs = value
    '    End Set
    'End Property

    ''' <summary>
    ''' Purpose: Get or Set activity_hrs
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Activity_Hrs() As Decimal
        Get
            Return mdecActivity_Hrs
        End Get
        Set(ByVal value As Decimal)
            mdecActivity_Hrs = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvedactivity_hrs
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ApprovedActivity_Hrs() As Decimal
        Get
            Return mdecApprovedActivity_Hrs
        End Get
        Set(ByVal value As Decimal)
            mdecApprovedActivity_Hrs = value
        End Set
    End Property


    'Pinkal (28-Mar-2018) -- End



    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _VoidLoginEmployeeunkid() As Integer
        Get
            Return mintVoidLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtApproverList
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtApproverList() As DataTable
        Get
            Return mdtApproverList
        End Get
        Set(ByVal value As DataTable)
            mdtApproverList = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dtEmployeeActivity
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _dtEmployeeActivity() As DataTable
        Get
            Return mdtEmployeeActivity
        End Get
        Set(ByVal value As DataTable)
            mdtEmployeeActivity = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsSubmitForApproval
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsSubmitForApproval() As Boolean
        Get
            Return mblnIsSubmitForApproval
        End Get
        Set(ByVal value As Boolean)
            mblnIsSubmitForApproval = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set SubmissionRemark
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _SubmissionRemark() As String
        Get
            Return mstrSubmissionRemark
        End Get
        Set(ByVal value As String)
            mstrSubmissionRemark = value
        End Set
    End Property

    'Pinkal (06-Jan-2023) -- Start
    '(A1X-556) FHS/PSI - Hide Leave Day Fraction from add/edit leave form screen.
    ''' <summary>
    ''' Purpose: Get or Set CostCenterunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _CostCenterunkid() As Integer
        Get
            Return mintCostCenterunkid
        End Get
        Set(ByVal value As Integer)
            mintCostCenterunkid = value
        End Set
    End Property
    'Pinkal (06-Jan-2023) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoperation
        End If

        Try
            strQ = "SELECT " & _
                      "  emptimesheetunkid " & _
                      ", periodunkid " & _
                      ", employeeunkid " & _
                      ", activitydate " & _
                      ", fundsourceunkid " & _
                      ", projectcodeunkid " & _
                      ", activityunkid " & _
                      ", activity_hrs " & _
                      ", approvedactivity_hrs " & _
                      ", description " & _
                      ", statusunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", loginemployeeunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", issubmit_approval " & _
                      ", submission_remark " & _
                      ", ISNULL(costcenterunkid,0) AS costcenterunkid " & _
                     "FROM ltbemployee_timesheet " & _
                     "WHERE emptimesheetunkid = @emptimesheetunkid "

            'Pinkal (06-Jan-2023) -- (A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets..[", ISNULL(costcenterunkid,0) AS costcenterunkid " & _]

            'Pinkal (28-Jul-2018) -- 'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251] [ ", submission_remark " & _] 


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmptimesheetunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEmptimesheetunkid = CInt(dtRow.Item("emptimesheetunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mdtActivitydate = dtRow.Item("activitydate")
                mintFundsourceunkid = CInt(dtRow.Item("fundsourceunkid"))
                mintProjectcodeunkid = CInt(dtRow.Item("projectcodeunkid"))
                mintActivityunkid = CInt(dtRow.Item("activityunkid"))

                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                'mintActivity_Hrs = CInt(dtRow.Item("activity_hrs"))
                'mintApprovedActivity_Hrs = CInt(dtRow.Item("approvedactivity_hrs"))
                mdecActivity_Hrs = CDec(dtRow.Item("activity_hrs"))
                mdecApprovedActivity_Hrs = CDec(dtRow.Item("approvedactivity_hrs"))
                'Pinkal (28-Mar-2018) -- End

                mstrDescription = dtRow.Item("description").ToString
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mdtVoiddatetime = IIf(IsDBNull(dtRow.Item("voiddatetime")), Nothing, dtRow.Item("voiddatetime"))
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintLoginEmployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mintVoidLoginEmployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                mblnIsSubmitForApproval = CBool(dtRow.Item("issubmit_approval"))

                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                mstrSubmissionRemark = CStr(dtRow.Item("submission_remark"))
                'Pinkal (28-Jul-2018) -- End

                'Pinkal (06-Jan-2023) -- Start
                '(A1X-556) FHS/PSI - Hide Leave Day Fraction from add/edit leave form screen.
                mintCostCenterunkid = CInt(dtRow.Item("costcenterunkid"))
                'Pinkal (06-Jan-2023) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            , ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal mblnAllowOverTimeInTimesheet As Boolean _
                            , Optional ByVal xEmployeeID As Integer = 0 _
                            , Optional ByVal blnGroup As Boolean = False _
                            , Optional ByVal blnOnlyActive As Boolean = True _
                            , Optional ByVal xPeriodID As Integer = 0 _
                            , Optional ByVal objDoOperation As clsDataOperation = Nothing _
                            , Optional ByVal mstrFilter As String = "" _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal blnIsForSubmitForApprovalStatus As Boolean = False) As DataSet

        'Pinkal (28-Mar-2018) --  'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.[ByVal mblnAllowOverTimeInTimesheet As Boolean]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        objDataOperation.ClearParameters()

        Try


            'Pinkal (28-Mar-2019) -- Start
            'Enhancement - Working on Budget Timesheet enhancement for Pact.
            Dim xUACQry, xUACFiltrQry As String
            xUACQry = "" : xUACFiltrQry = ""

            If blnApplyUserAccessFilter Then Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting, , True)
            'Pinkal (28-Mar-2019) -- End


            If blnGroup Then

                strQ = "SELECT " & _
                          "  CAST (0 AS BIT) AS IsChecked " & _
                          ", CAST (1 AS bit) AS IsGrp  " & _
                          ", -1 AS emptimesheetunkid " & _
                          ", ltbemployee_timesheet.periodunkid AS periodunkid" & _
                          ", '' Period " & _
                          ", ltbemployee_timesheet.employeeunkid " & _
                          ", ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Particular " & _
                          ", ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                          ", NULL AS activitydate " & _
                          ", '' ADate " & _
                          ", -1 AS fundsourceunkid " & _
                          ",'' AS fundname  " & _
                          ", -1 AS fundprojectcodeunkid " & _
                          ",'' AS fundprojectname " & _
                          ", -1 AS fundactivityunkid " & _
                          ", ''  AS activity_name " & _
                          ", NULL AS activity_hrs " & _
                          ", NULL AS approvedactivity_hrs " & _
                          ", 0 AS ActivityHoursInMin  " & _
                          ", 0 AS ApprovedActivityHoursInMin " & _
                          ", '' AS description " & _
                          ", -1 AS statusunkid " & _
                          ", '' AS Status " & _
                          ", -1 AS userunkid " & _
                          ", 0 AS isvoid " & _
                          ", -1 AS voiduserunkid " & _
                          ", NULL AS voiddatetime " & _
                          ", '' AS voidreason " & _
                          ", -1 AS loginemployeeunkid " & _
                          ", -1 AS voidloginemployeeunkid " & _
                          ", 0 AS isholiday " & _
                          ", 0 AS isLeave " & _
                          ", 0 AS isDayOff " & _
                          ", 0 AS issubmit_approval " & _
                          ",'' submission_remark " & _
                          ", 0.00 AS percentage " & _
                          ", 0 AS costcenterunkid " & _
                          ", '' AS costcentername " & _
                          " FROM ltbemployee_timesheet " & _
                          " JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = ltbemployee_timesheet.periodunkid AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & _
                          " JOIN hremployee_master ON hremployee_master.employeeunkid = ltbemployee_timesheet.employeeunkid "

                'Pinkal (06-Jan-2023) -- '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.[", 0 AS costcenterunkid, '' AS costcentername " & _]

                'Pinkal (28-Jul-2018) --  'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251] [",'' submission_remark " & _]

                'Pinkal (28-Mar-2019) -- Start
                'Enhancement - Working on Budget Timesheet enhancement for Pact.
                If blnApplyUserAccessFilter Then
                    If xUACQry.Trim.Length > 0 Then
                        strQ &= xUACQry
                    End If
                End If
                'Pinkal (28-Mar-2019) -- End


                strQ &= " WHERE ltbemployee_timesheet.isvoid = 0 "
             
                If xEmployeeID > 0 Then
                    strQ &= " AND ltbemployee_timesheet.employeeunkid = @employeeunkid"
                End If

                If xPeriodID <> 0 Then
                    strQ &= " AND ltbemployee_timesheet.periodunkid = @periodunkid"
                End If

                If mstrFilter.Trim.Length > 0 Then
                    strQ &= " AND " & mstrFilter
                End If

                strQ &= " UNION "

            End If

            strQ &= " SELECT " & _
                        "  CAST (0 AS BIT) AS IsChecked " & _
                        ", CAST (0 AS bit) AS IsGrp  " & _
                        ",  ltbemployee_timesheet.emptimesheetunkid " & _
                        ", ltbemployee_timesheet.periodunkid " & _
                        ", ISNULL(cfcommon_period_tran.period_name,'') AS Period " & _
                        ", ltbemployee_timesheet.employeeunkid " & _
                        ", '' AS Particular " & _
                        ", ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                        ", ltbemployee_timesheet.activitydate " & _
                        ", CONVERT(CHAR(8),ltbemployee_timesheet.activitydate,112) AS ADate " & _
                        ", ltbemployee_timesheet.fundsourceunkid " & _
                        ", ISNULL(bgfundsource_master.fundname,'') AS fundname  " & _
                        ", ltbemployee_timesheet.projectcodeunkid AS fundprojectcodeunkid " & _
                        ", ISNULL(bgfundprojectcode_master.fundprojectname,'') AS fundprojectname " & _
                        ", ltbemployee_timesheet.activityunkid AS fundactivityunkid " & _
                        ", ISNULL(bgfundactivity_tran.activity_name,'') AS activity_name " & _
                        ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),CAST(ltbemployee_timesheet.activity_hrs AS INT) / 60), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( CAST(ltbemployee_timesheet.activity_hrs AS DECIMAL) % 60)), 2), '00:00') AS activity_hrs " & _
                        ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),CAST(ltbemployee_timesheet.approvedactivity_hrs AS INT) / 60), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( CAST(ltbemployee_timesheet.approvedactivity_hrs AS DECIMAL) % 60)), 2), '') AS approvedactivity_hrs " & _
                        ", ltbemployee_timesheet.activity_hrs AS ActivityHoursInMin  " & _
                        ", ltbemployee_timesheet.approvedactivity_hrs AS ApprovedActivityHoursInMin " & _
                        ", ltbemployee_timesheet.description " & _
                        ", ltbemployee_timesheet.statusunkid " & _
                        ", CASE WHEN ltbemployee_timesheet.statusunkid = 1 then @Approve " & _
                        "            WHEN ltbemployee_timesheet.statusunkid = 2 then @Pending  " & _
                        "            WHEN ltbemployee_timesheet.statusunkid = 3 then @Reject " & _
                        "            WHEN ltbemployee_timesheet.statusunkid = 6 then @Cancel end as status" & _
                        ", ltbemployee_timesheet.userunkid " & _
                        ", ltbemployee_timesheet.isvoid " & _
                        ", ltbemployee_timesheet.voiduserunkid " & _
                        ", ltbemployee_timesheet.voiddatetime " & _
                        ", ltbemployee_timesheet.voidreason " & _
                        ", ltbemployee_timesheet.loginemployeeunkid " & _
                        ", ltbemployee_timesheet.voidloginemployeeunkid " & _
                        ", 0 AS isholiday " & _
                        ", 0 AS isLeave " & _
                        ", 0 AS isDayOff " & _
                        ", ltbemployee_timesheet.issubmit_approval " & _
                        ", ISNULL(ltbemployee_timesheet.submission_remark,'') AS submission_remark " & _
                        ", ISNULL(bgbudgetcodesfundsource_tran.percentage,0.00) AS percentage " & _
                        ", ISNULL(ltbemployee_timesheet.costcenterunkid,0) AS costcenterunkid " & _
                        ", ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _
                        " FROM ltbemployee_timesheet " & _
                        " LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = ltbemployee_timesheet.periodunkid AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & _
                        " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = ltbemployee_timesheet.employeeunkid " & _
                        " LEFT JOIN bgfundsource_master ON bgfundsource_master.fundsourceunkid = ltbemployee_timesheet.fundsourceunkid  " & _
                        " LEFT JOIN bgfundprojectcode_master ON bgfundprojectcode_master.fundprojectcodeunkid =  ltbemployee_timesheet.projectcodeunkid " & _
                        " LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = ltbemployee_timesheet.activityunkid " & _
                        " LEFT JOIN bgbudgetcodesfundsource_tran ON ltbemployee_timesheet.periodunkid = bgbudgetcodesfundsource_tran.periodunkid " & _
                        " AND bgfundprojectcode_master.fundprojectcodeunkid = bgbudgetcodesfundsource_tran.fundprojectcodeunkid  " & _
                        " AND bgfundactivity_tran.fundactivityunkid = bgbudgetcodesfundsource_tran.fundactivityunkid" & _
                        " LEFT JOIN bgbudgetcodes_tran ON bgbudgetcodesfundsource_tran.budgetcodestranunkid =  bgbudgetcodes_tran.budgetcodestranunkid  " & _
                        " AND ltbemployee_timesheet.employeeunkid = bgbudgetcodes_tran.allocationtranunkid " & _
                        " LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = ltbemployee_timesheet.costcenterunkid "

            'Pinkal (06-Jan-2023) -- (A1X-556) FHS/PSI - Hide Leave Day Fraction from add/edit leave form screen.[", ISNULL(ltbemployee_timesheet.costcenterunkid,0) AS costcenterunkid,ISNULL(prcostcenter_master.costcentername,'') AS costcentername " & _]

            'Pinkal (21-Oct-2019) -- 'Defect NMB - Worked On Employee Timesheet Id not set properly due to that it is creating problem.[LEFT JOIN bgbudgetcodesfundsource_tran ON ltbemployee_timesheet.periodunkid = bgbudgetcodesfundsource_tran.periodunkid]

            'Pinkal (28-Jul-2018) --  'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251] [",'' ISNULL(ltbemployee_timesheet.submission_remark,'') AS submission_remark " & _]

            'Pinkal (28-Mar-2019) -- Start
            'Enhancement - Working on Budget Timesheet enhancement for Pact.
            If blnApplyUserAccessFilter Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If
            'Pinkal (28-Mar-2019) -- End


            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            '", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),CAST(ltbemployee_timesheet.activity_hrs AS INT) / 60), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( CAST(ltbemployee_timesheet.activity_hrs AS DECIMAL) % 60)), 2), '00:00') AS activity_hrs " & _
            '", ISNULL(RIGHT('00' + CONVERT(VARCHAR(2),CAST(ltbemployee_timesheet.approvedactivity_hrs AS INT) / 60), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), ( CAST(ltbemployee_timesheet.approvedactivity_hrs AS DECIMAL) % 60)), 2), '') AS approvedactivity_hrs " & _
            'Pinkal (28-Mar-2018) -- End

            'Pinkal (21-Oct-2019) -- Start 'Defect NMB - Worked On Employee Timesheet Id not set properly due to that it is creating problem.
            strQ &= " WHERE bgbudgetcodesfundsource_tran.percentage > 0 AND bgbudgetcodesfundsource_tran.isvoid = 0 AND bgbudgetcodes_tran.isvoid = 0 "
            'Pinkal (21-Oct-2019) -- End

            If xEmployeeID > 0 Then
                strQ &= " AND ltbemployee_timesheet.employeeunkid = @employeeunkid "
            End If

            If blnOnlyActive Then
                strQ &= " AND ltbemployee_timesheet.isvoid = 0 "
            End If

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            If xPeriodID <> 0 Then
                strQ &= " AND ltbemployee_timesheet.periodunkid = @periodunkid"
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriodID)
            End If

            strQ &= " ORDER BY  ltbemployee_timesheet.employeeunkid, activitydate "

           
            If xEmployeeID > 0 Then
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeID)
            End If

            objDataOperation.AddParameter("@Approve", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 110, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 111, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 112, "Rejected"))
            objDataOperation.AddParameter("@Cancel", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 115, "Cancelled"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (13-Oct-2017) -- Start
            'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
            If blnIsForSubmitForApprovalStatus = False AndAlso xEmployeeID > 0 Then
                'Pinkal (13-Oct-2017) -- End

            Dim objEmpHoliday As New clsemployee_holiday
            Dim dsHolidayList As DataSet = objEmpHoliday.GetList("Holiday", xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                                                             , xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved _
                                                                                             , xIncludeIn_ActiveEmployee, blnApplyUserAccessFilter, xEmployeeID, , _
                                                                                             , " AND  CONVERT(char(8),lvholiday_master.holidaydate,112) >= '" & eZeeDate.convertDate(xPeriodStart) & "'  AND CONVERT(char(8),lvholiday_master.holidaydate,112) <= '" & eZeeDate.convertDate(xPeriodEnd) & "'")

            objEmpHoliday = Nothing

                'Pinkal (22-Jan-2021)-- Start
                'Bug -  solved bug for IHI.
                'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso dsHolidayList IsNot Nothing AndAlso dsHolidayList.Tables(0).Rows.Count > 0 Then
                If dsHolidayList IsNot Nothing AndAlso dsHolidayList.Tables(0).Rows.Count > 0 Then
                    'Pinkal (22-Jan-2021) -- End

                For Each dr As DataRow In dsHolidayList.Tables(0).Rows
                        'Pinkal (28-Mar-2018) -- Start
                        'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
                        Dim drHolidayEntry() As DataRow = dsList.Tables(0).Select("ADate = '" & dr("holidaydate").ToString() & "' AND employeeunkid = " & CInt(dr("employeeunkid")))
                        If drHolidayEntry.Length > 0 Then
                            If mblnAllowOverTimeInTimesheet Then
                                'Pinkal (28-Jul-2018) -- Start
                                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                                For i As Integer = 0 To drHolidayEntry.Length - 1
                                    drHolidayEntry(i)("isholiday") = True
                                Next
                                'Pinkal (28-Jul-2018) -- End
                                Continue For
                            Else
                                For i As Integer = 0 To drHolidayEntry.Length - 1
                                    dsList.Tables(0).Rows.Remove(drHolidayEntry(i))
                                Next
                                dsList.Tables(0).AcceptChanges()
                            End If
                        End If
                        'Pinkal (28-Mar-2018) -- End

                    Dim drRow As DataRow = dsList.Tables(0).NewRow
                    drRow("IsChecked") = False
                        drRow("IsGrp") = False 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
                    drRow("periodunkid") = -1
                    drRow("employeeunkid") = CInt(dr("employeeunkid"))
                    drRow("fundsourceunkid") = -1
                    drRow("fundprojectcodeunkid") = -1
                    drRow("fundactivityunkid") = -1
                    drRow("activitydate") = eZeeDate.convertDate(dr("holidaydate").ToString())
                    drRow("description") = dr("holidayname").ToString()
                    drRow("activity_hrs") = "00:00"
                    drRow("approvedactivity_hrs") = "00:00"
                    drRow("ActivityHoursInMin") = 0
                    drRow("Status") = ""
                    drRow("isholiday") = True
                    drRow("isLeave") = False
                    drRow("isDayOff") = False
                    dsList.Tables(0).Rows.Add(drRow)
                Next

            dsHolidayList.Tables(0).Rows.Clear()
            dsHolidayList = Nothing

                End If

                'Pinkal (13-Oct-2017) -- End


            Dim objEmpDayOff As New clsemployee_dayoff_Tran
            'Pinkal (17-Jan-2017) -- Start
            'Enhancement - Working on DAYOFF Error given by AMOS. 
            'Dim dsEmpDayOff As DataSet = objEmpDayOff.GetList("List", "", xEmployeeID, xPeriodStart.Date, xPeriodEnd.Date)
            Dim dsEmpDayOff As DataSet = objEmpDayOff.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                                                                                                , xCompanyUnkid, xUserModeSetting, xPeriodEnd.Date _
                                                                                                , "", xEmployeeID, xPeriodStart.Date, xPeriodEnd.Date)
            'Pinkal (17-Jan-2017) -- End
            objEmpDayOff = Nothing

                'Pinkal (22-Jan-2021)-- Start
                'Bug -  solved bug for IHI.
                'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso dsEmpDayOff IsNot Nothing AndAlso dsEmpDayOff.Tables(0).Rows.Count > 0 Then
                If dsEmpDayOff IsNot Nothing AndAlso dsEmpDayOff.Tables(0).Rows.Count > 0 Then
                    'Pinkal (22-Jan-2021) -- End

                For Each dr As DataRow In dsEmpDayOff.Tables(0).Rows
                    Dim drRow As DataRow = dsList.Tables(0).NewRow
                    drRow("IsChecked") = False
                        drRow("IsGrp") = False 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
                    drRow("periodunkid") = -1
                    drRow("employeeunkid") = CInt(dr("employeeunkid"))
                    drRow("fundsourceunkid") = -1
                    drRow("fundprojectcodeunkid") = -1
                    drRow("fundactivityunkid") = -1
                    drRow("activitydate") = eZeeDate.convertDate(dr("dayoffdate").ToString())
                    drRow("description") = Language.getMessage(mstrModuleName, 3, "Day Off")
                    drRow("activity_hrs") = "00:00"
                    drRow("approvedactivity_hrs") = "00:00"
                    drRow("ActivityHoursInMin") = 0
                    drRow("Status") = ""
                    drRow("isholiday") = False
                    drRow("isLeave") = False
                    drRow("isDayOff") = True
                    dsList.Tables(0).Rows.Add(drRow)
                Next

            End If
            dsEmpDayOff.Tables(0).Rows.Clear()
            dsEmpDayOff = Nothing


            Dim objLeaveIssue As New clsleaveissue_Tran
            Dim dsLeaveList As DataSet = objLeaveIssue.GetList("Leave", True, -1, xEmployeeID, xPeriodStart, xPeriodEnd)
            objLeaveIssue = Nothing


                'Pinkal (22-Jan-2021)-- Start
                'Bug -  solved bug for IHI.
                'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso dsLeaveList IsNot Nothing AndAlso dsLeaveList.Tables(0).Rows.Count > 0 Then
                If dsLeaveList IsNot Nothing AndAlso dsLeaveList.Tables(0).Rows.Count > 0 Then
                    'Pinkal (22-Jan-2021) -- End

                For Each dr As DataRow In dsLeaveList.Tables(0).Rows


                        'Pinkal (24-Jul-2017) -- Start
                        'Enhancement - Budget Timesheet Issue For THPS .
                        Dim dRow As DataRow() = dsList.Tables(0).Select("employeeunkid = " & CInt(dr("employeeunkid")) & " AND activitydate = '" & eZeeDate.convertDate(dr("leavedate").ToString()) & "'")
                        If dRow.Length > 0 Then
                            Dim objLeaveType As New clsleavetype_master
                            objLeaveType._Leavetypeunkid = CInt(dr("leavetypeunkid"))
                            If objLeaveType._Isissueonholiday Then
                                dsList.Tables(0).Rows.Remove(dRow(0))
                                dsList.AcceptChanges()
                            End If

                            'Pinkal (22-Jan-2021)-- Start
                            'Bug -  solved bug for IHI.

                            'Pinkal (28-Apr-2021)-- Start
                            'IHI Bug  -  Resolved IHIH Bug.
                            If (dRow(0).RowState <> DataRowState.Detached AndAlso dRow(0).RowState <> DataRowState.Deleted) AndAlso (CBool(dRow(0)("isholiday")) OrElse CBool(dRow(0)("isDayOff"))) Then
                                dsList.Tables(0).Rows.Remove(dRow(0))
                                dsList.AcceptChanges()
                            End If
                            'Pinkal (28-Apr-2021) -- End

                            'Pinkal (22-Jan-2021) -- End

                        End If
                        'Pinkal (24-Jul-2017) -- End


                    Dim drRow As DataRow = dsList.Tables(0).NewRow
                    drRow("IsChecked") = False
                        drRow("IsGrp") = False 'Nilay (21 Mar 2017) -- [IsGroup REPLACED BY IsGrp]
                    drRow("periodunkid") = -1
                    drRow("employeeunkid") = CInt(dr("employeeunkid"))
                    drRow("fundsourceunkid") = -1
                    drRow("fundprojectcodeunkid") = -1
                    drRow("fundactivityunkid") = -1
                    drRow("activitydate") = eZeeDate.convertDate(dr("leavedate").ToString())
                    drRow("description") = dr("leavename").ToString()
                    drRow("activity_hrs") = "00:00"
                    drRow("approvedactivity_hrs") = "00:00"
                    drRow("ActivityHoursInMin") = 0
                    drRow("Status") = ""
                    drRow("isholiday") = False
                    drRow("isLeave") = True
                    drRow("isDayOff") = False
                    dsList.Tables(0).Rows.Add(drRow)
                Next

            End If
            dsLeaveList.Tables(0).Rows.Clear()
            dsLeaveList = Nothing



                'Pinkal (13-Oct-2017) -- Start
                'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.

                '/* START FOR PENDING LEAVE APPLICATION */
                Dim objLvdayFraction As New clsleaveday_fraction
                dsLeaveList = objLvdayFraction.GetList("List", -1, True, xEmployeeID, -1, "AND lvleaveform.statusunkid = 2 AND lvleaveday_fraction.leavedate BETWEEN '" & eZeeDate.convertDate(xPeriodStart) & "' AND '" & eZeeDate.convertDate(xPeriodEnd) & "'")
                objLvdayFraction = Nothing

                If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 AndAlso dsLeaveList IsNot Nothing AndAlso dsLeaveList.Tables(0).Rows.Count > 0 Then

                    For Each dr As DataRow In dsLeaveList.Tables(0).Rows
                        Dim drRow As DataRow = dsList.Tables(0).NewRow
                        drRow("IsChecked") = False
                        drRow("IsGrp") = False
                        drRow("periodunkid") = -1
                        drRow("employeeunkid") = xEmployeeID
                        drRow("fundsourceunkid") = -1
                        drRow("fundprojectcodeunkid") = -1
                        drRow("fundactivityunkid") = -1
                        drRow("activitydate") = CDate(dr("leavedate")).ToShortDateString()
                        drRow("description") = dr("leavename").ToString()
                        drRow("activity_hrs") = "00:00"
                        drRow("approvedactivity_hrs") = "00:00"
                        drRow("ActivityHoursInMin") = 0
                        drRow("Status") = ""
                        drRow("isholiday") = False
                        drRow("isLeave") = True
                        drRow("isDayOff") = False
                        dsList.Tables(0).Rows.Add(drRow)
                    Next

                End If
                dsLeaveList.Tables(0).Rows.Clear()
                dsLeaveList = Nothing

                '/* END FOR PENDING LEAVE APPLICATION */



                Dim dtFinalTable As DataTable = New DataView(dsList.Tables(0), "", "activitydate,employeeunkid", DataViewRowState.CurrentRows).ToTable()
            dsList.Tables.Remove(dsList.Tables(0))
            dsList.Tables.Add(dtFinalTable)

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (ltbemployee_timesheet) </purpose>
    Public Function Insert(ByVal xDatabaseName As String, ByVal xUserUnkid As Integer _
                                 , ByVal xYearUnkid As Integer, ByVal xCompanyUnkid As Integer _
                                 , ByVal strEmployeeAsOnDate As String, ByVal xUserModeSetting As String, ByVal xOnlyApproved As Boolean _
                                 , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                 , ByVal dtFromDate As Date, ByVal dtToDate As Date, ByVal mblnAllowOverTimeToEmpTimesheet As Boolean _
                                 , ByVal mblnAllowActivityHoursByPercentage As Boolean _
                                 , ByVal mblnAllowToExceedAssignedTime As Boolean _
                                 , ByVal mblnAllowEmpAssignedProjectExceedTime As Boolean _
                                 , ByRef mstrShiftExceedDates As String) As Boolean


        'Pinkal (13-Aug-2019) --  'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.[ ByVal mblnAllowEmpAssignedProjectExceedTime As Boolean,ByRef mstrShiftExceedDates As String]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (04-Aug-2023) -- Start
        '(A1X-1150) Afya Plus - Develop configuration setting for st. Judes to include or exclude weekends when filling timesheets.
        Dim mblnIncludeWeekendInOvertimeToEmpTimesheet As Boolean = True
        Dim mblnIncludeHolidayInOvertimeToEmpTimesheet As Boolean = True
        Dim objconfig As New clsConfigOptions
        Dim mstrIncludeWeekendInOvertimeToEmpTimesheet As String = objconfig.GetKeyValue(xCompanyUnkid, "IncludeWeekendInOvertimeToEmpTimesheet", Nothing)
        Dim mstrIncludeHolidayInOvertimeToEmpTimesheet As String = objconfig.GetKeyValue(xCompanyUnkid, "IncludeHolidayInOvertimeToEmpTimesheet", Nothing)

        If mstrIncludeWeekendInOvertimeToEmpTimesheet.Trim.Length > 0 Then mblnIncludeWeekendInOvertimeToEmpTimesheet = CBool(mstrIncludeWeekendInOvertimeToEmpTimesheet)
        If mstrIncludeHolidayInOvertimeToEmpTimesheet.Trim.Length > 0 Then mblnIncludeHolidayInOvertimeToEmpTimesheet = CBool(mstrIncludeHolidayInOvertimeToEmpTimesheet)
        'Pinkal (04-Aug-2023) -- End

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try




            If mdtEmployeeActivity IsNot Nothing AndAlso mdtEmployeeActivity.Rows.Count > 0 Then

                Dim objEmpDayOFF As New clsemployee_dayoff_Tran
                Dim objLeaveIssue As New clsleaveissue_Tran
                Dim objLeaveForm As New clsleaveform
                Dim objEmpHoliday As New clsemployee_holiday
                Dim objEmpshift As New clsEmployee_Shift_Tran


                'Pinkal (30-Aug-2019) -- Start
                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                Dim mintWeekdaysWorkingHrs As Integer = 0
                'Pinkal (30-Aug-2019) -- End


                Dim mintDays As Integer = DateDiff(DateInterval.Day, dtFromDate.Date, dtToDate.Date.AddDays(1))

                For i As Integer = 0 To mintDays - 1

                    Dim mintShiftID As Integer = objEmpshift.GetEmployee_Current_ShiftId(mdtActivitydate.AddDays(i).Date, mintEmployeeunkid)

                For Each drRow As DataRow In mdtEmployeeActivity.Rows

                        '/* START FOR CHECK DAYOFF */
                        Dim dsDayOff As DataSet = objEmpDayOFF.GetList("List", xDatabaseName, xUserUnkid, xYearUnkid _
                                                                                              , xCompanyUnkid, xUserModeSetting, eZeeDate.convertDate(strEmployeeAsOnDate).Date _
                                                                                              , "", mintEmployeeunkid, mdtActivitydate.AddDays(i).Date, mdtActivitydate.AddDays(i).Date)

                        If dsDayOff IsNot Nothing AndAlso dsDayOff.Tables(0).Rows.Count > 0 Then
                            dsDayOff.Clear()
                            dsDayOff = Nothing
                            Continue For
                        End If

                        '/* END FOR CHECK DAYOFF */

                        Dim objShiftTran As New clsshift_tran
                        objShiftTran.GetShiftTran(mintShiftID)
                        Dim mdecActivityHoursInMin As Decimal = CDec(drRow("ActivityHoursInMin"))

                        '/* START FOR CHECK WEEKEND */
                        If mblnAllowOverTimeToEmpTimesheet = False Then
                            Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(mdtActivitydate.AddDays(i).Date, FirstDayOfWeek.Sunday) - 1 & " AND isweekend = 1")
                            If drWeekend IsNot Nothing AndAlso drWeekend.Length > 0 Then
                                Continue For
                            Else
                                Dim drWeekDayRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(mdtActivitydate.AddDays(i).Date, FirstDayOfWeek.Sunday) - 1 & " AND isweekend = 0")
                                If drWeekDayRow.Length > 0 Then

                                    mintWeekdaysWorkingHrs = CInt(drWeekDayRow(0)("workinghrsinsec"))

                                    'Pinkal (21-Oct-2019) -- Start
                                    'Defect NMB - Worked On Employee Timesheet Id not set properly due to that it is creating problem.
                                    'If DateDiff(DateInterval.Day, dtFromDate.Date, dtToDate.Date) > 0 Then
                                    If DateDiff(DateInterval.Day, dtFromDate.Date, dtToDate.AddDays(1).Date) > 0 Then
                                        'Pinkal (21-Oct-2019) -- End

                                        'Pinkal (13-Aug-2019) -- Start
                                        'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                                        'Dim minTotalActivitydaysPerDay As Integer = mdtEmployeeActivity.AsEnumerable().Sum(Function(x) x.Field(Of Integer)("ActivityHoursInMin"))
                                        'If (minTotalActivitydaysPerDay * 60) > CInt(drWeekDayRow(0)("workinghrsinsec")) Then
                                        '    If mstrShiftExceedDates.Contains(mdtActivitydate.AddDays(i).ToShortDateString()) = False Then
                                        '        mstrShiftExceedDates &= mdtActivitydate.AddDays(i).Date.ToShortDateString() & ","
                                        '    End If
                                        '    Continue For
                                        'End If
                                        'Pinkal (13-Aug-2019) -- End

                                        If mblnAllowActivityHoursByPercentage Then

                                            If mblnAllowToExceedAssignedTime = False Then

                                                'Pinkal (30-Aug-2019) -- Start
                                                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                                                If mblnAllowEmpAssignedProjectExceedTime Then
                                                    Dim objExemptEmp As New clstsexemptemployee_tran
                                                    If objExemptEmp.isExist(mintEmployeeunkid, -1, objDataOperation) Then
                                                        If (CDec(drRow("ActivityHoursInMin")) * 60) > CInt(drWeekDayRow(0)("workinghrsinsec")) Then
                                                            mdecActivityHoursInMin = ((CInt(drWeekDayRow(0)("workinghrsinsec")) * CInt(drRow("percentage"))) / 100) / 60
                                                        Else
                                                    mdecActivityHoursInMin = CDec(drRow("ActivityHoursInMin").ToString)
                                                        End If
                                                    Else
                                                        mdecActivityHoursInMin = ((CInt(drWeekDayRow(0)("workinghrsinsec")) * CInt(drRow("percentage"))) / 100) / 60
                                                    End If
                                                    objExemptEmp = Nothing
                                                Else
                                                mdecActivityHoursInMin = ((CInt(drWeekDayRow(0)("workinghrsinsec")) * CInt(drRow("percentage"))) / 100) / 60
                                                End If 'If mblnAllowEmpAssignedProjectExceedTime Then
                                                'Pinkal (30-Aug-2019) -- End

                                            ElseIf mblnAllowToExceedAssignedTime Then

                                                'Pinkal (30-Aug-2019) -- Start
                                                'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                                                If mblnAllowEmpAssignedProjectExceedTime Then
                                                    Dim objExemptEmp As New clstsexemptemployee_tran
                                                    If objExemptEmp.isExist(mintEmployeeunkid, -1, objDataOperation) Then
                                                        If (CDec(drRow("ActivityHoursInMin")) * 60) > CInt(drWeekDayRow(0)("workinghrsinsec")) Then
                                                            mdecActivityHoursInMin = ((CInt(drWeekDayRow(0)("workinghrsinsec")) * CInt(drRow("percentage"))) / 100) / 60
                                                        Else
                                                    mdecActivityHoursInMin = CDec(drRow("ActivityHoursInMin").ToString)
                                                        End If
                                                    Else
                                                        mdecActivityHoursInMin = ((CInt(drWeekDayRow(0)("workinghrsinsec")) * CInt(drRow("percentage"))) / 100) / 60
                                                    End If
                                                    objExemptEmp = Nothing
                                                Else
                                                    mdecActivityHoursInMin = ((CInt(drWeekDayRow(0)("workinghrsinsec")) * CInt(drRow("percentage"))) / 100) / 60
                                                End If  ' If mblnAllowEmpAssignedProjectExceedTime Then
                                                'Pinkal (30-Aug-2019) -- End
                                            End If  '   If mblnAllowToExceedAssignedTime = False Then
                                        Else

                                            'Pinkal (30-Aug-2019) -- Start
                                            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                                            If mblnAllowEmpAssignedProjectExceedTime Then
                                                Dim objExemptEmp As New clstsexemptemployee_tran
                                                If objExemptEmp.isExist(mintEmployeeunkid, -1, objDataOperation) Then
                                                    If (CDec(drRow("ActivityHoursInMin")) * 60) > CInt(drWeekDayRow(0)("workinghrsinsec")) Then
                                                        mdecActivityHoursInMin = ((CInt(drWeekDayRow(0)("workinghrsinsec")) * CInt(drRow("percentage"))) / 100) / 60
                                                    Else
                                                mdecActivityHoursInMin = CDec(drRow("ActivityHoursInMin").ToString)
                                                    End If
                                                Else
                                                    mdecActivityHoursInMin = ((CInt(drWeekDayRow(0)("workinghrsinsec")) * CInt(drRow("percentage"))) / 100) / 60
                                                End If
                                                objExemptEmp = Nothing
                                        Else
                                            mdecActivityHoursInMin = ((CInt(drWeekDayRow(0)("workinghrsinsec")) * CInt(drRow("percentage"))) / 100) / 60
                                            End If  ' If mblnAllowEmpAssignedProjectExceedTime Then
                                            'Pinkal (30-Aug-2019) -- End

                                        End If    ' If mblnAllowActivityHoursByPercentage Then
                                    Else
                                        'Pinkal (13-Aug-2019) -- Start
                                        'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                                        'Dim minTotalActivitydaysPerDay As Integer = mdtEmployeeActivity.AsEnumerable().Sum(Function(x) x.Field(Of Integer)("ActivityHoursInMin"))
                                        'If (minTotalActivitydaysPerDay * 60) > CInt(drWeekDayRow(0)("workinghrsinsec")) Then
                                        '    If mstrShiftExceedDates.Contains(mdtActivitydate.AddDays(i).ToShortDateString()) = False Then
                                        '        mstrShiftExceedDates &= mdtActivitydate.AddDays(i).Date.ToShortDateString() & ","
                                        '    End If
                                        '    Continue For
                                        'End If
                                        'Pinkal (13-Aug-2019) -- End


                                        'Pinkal (30-Aug-2019) -- Start
                                        'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                                        If mblnAllowEmpAssignedProjectExceedTime Then
                                            Dim objExemptEmp As New clstsexemptemployee_tran
                                            If objExemptEmp.isExist(mintEmployeeunkid, -1, objDataOperation) Then
                                                If (CDec(drRow("ActivityHoursInMin")) * 60) > CInt(drWeekDayRow(0)("workinghrsinsec")) Then
                                                    mdecActivityHoursInMin = ((CInt(drWeekDayRow(0)("workinghrsinsec")) * CInt(drRow("percentage"))) / 100) / 60
                                                Else
                                            mdecActivityHoursInMin = CDec(drRow("ActivityHoursInMin").ToString)
                                                End If
                                            Else
                                                mdecActivityHoursInMin = ((CInt(drWeekDayRow(0)("workinghrsinsec")) * CInt(drRow("percentage"))) / 100) / 60
                                            End If
                                            objExemptEmp = Nothing
                                    Else
                                        mdecActivityHoursInMin = Convert.ToDouble(CalculateTime(False, CInt(drWeekDayRow(0)("workinghrsinsec"))))
                                        End If  'If mblnAllowEmpAssignedProjectExceedTime Then
                                        'Pinkal (30-Aug-2019) -- End


                                        'Pinkal (19-Feb-2018) -- End

                                    End If  ' If DateDiff(DateInterval.Day, dtFromDate.Date, dtToDate.Date) > 0 Then

                                End If ' If drWeekDayRow.Length > 0 Then

                                End If ' If drWeekend IsNot Nothing AndAlso drWeekend.Length > 0 Then

                        ElseIf mblnAllowOverTimeToEmpTimesheet Then

                            If mblnIncludeWeekendInOvertimeToEmpTimesheet = False Then
                                Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(mdtActivitydate.AddDays(i).Date, FirstDayOfWeek.Sunday) - 1 & " AND isweekend = 1")
                                If drWeekend IsNot Nothing AndAlso drWeekend.Length > 0 Then
                                    Continue For
                                End If
                            End If  ' If mblnIncludeWeekendInOvertimeToEmpTimesheet = False Then

                        End If 'If mblnAllowOverTimeToEmpTimesheet = False Then

                        objShiftTran = Nothing

                        'Dim drWeekDayRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(mdtActivitydate.AddDays(i).Date, FirstDayOfWeek.Sunday) - 1 & " AND isweekend = 0")
                        'If drWeekDayRow.Length > 0 Then
                        '    If DateDiff(DateInterval.Day, dtFromDate.Date, dtToDate.Date) > 0 Then

                        '        'Pinkal (19-Feb-2018) -- Start
                        '        'BUG - BUG SOLVED GIVEN BY RUTTA [ALLOW ACTIVITY BY PERCENTAGE DID NOT APPLY].
                        '        If mblnAllowActivityHoursByPercentage Then
                        '            If mblnAllowToExceedAssignedTime = False Then
                        '                drRow("ActivityHoursInMin") = ((CInt(drWeekDayRow(0)("workinghrsinsec")) * CDec(drRow("percentage"))) / 100) / 60
                        '            End If
                        '        Else
                        '            drRow("ActivityHoursInMin") = Convert.ToDouble(CalculateTime(False, CInt(drWeekDayRow(0)("workinghrsinsec"))))
                        '        End If
                        '        'Pinkal (19-Feb-2018) -- End

                        '    End If
                        'End If

                        'If mblnAllowOverTimeToEmpTimesheet = False Then
                        '    Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(mdtActivitydate.AddDays(i).Date, FirstDayOfWeek.Sunday) - 1 & " AND isweekend = 1")
                        '    If drWeekend IsNot Nothing AndAlso drWeekend.Length > 0 Then
                        '        Continue For
                        '    End If
                        'End If

                        'objShiftTran = Nothing

                        '/* END FOR CHECK WEEKEND */

                        'Pinkal (28-Mar-2018) -- End

                        'Pinkal (28-Dec-2017) -- End
                      
                        'Pinkal (23-Apr-2018) -- End
  


                        '/* START FOR CHECK LEAVE */

                        'Pinkal (11-Sep-2019) -- Start
                        'Defect PACT - Cancel Days are not inserting in bugdet timesheet .

                        'Pinkal (28-Aug-2018) -- Start
                        'Bug - Solved Problem For PACT WHEN BUDGET TIMESHEET IS APPLIED ON LEAVE DAYS.
                        'If objLeaveForm.isDayExist(False, mintEmployeeunkid, mdtActivitydate.AddDays(i).Date, mdtActivitydate.AddDays(i).Date, -1) Then 'For Applied Leave

                        If objLeaveIssue.GetIssuedDayFractionForViewer(mintEmployeeunkid, eZeeDate.convertDate(mdtActivitydate.AddDays(i).Date)) > 0 Then  'For Issued Leave 
                            Continue For
                        End If

                        Dim objCancelform As New clsCancel_Leaveform
                        If objCancelform.GetCancelDayExistForEmployee(mintEmployeeunkid, mdtActivitydate.AddDays(i).Date, objDataOperation) = False Then
                            If objLeaveForm.isDayExist(True, mintEmployeeunkid, mdtActivitydate.AddDays(i).Date, mdtActivitydate.AddDays(i).Date, -1) Then 'For Applied Leave
                            Continue For
                        End If
                        End If
                        objCancelform = Nothing

                        'Pinkal (11-Sep-2019) -- End

                        '/* END FOR CHECK LEAVE */

                        '/* START FOR CHECK HOLIDAY */

                        'Pinkal (13-Aug-2018) -- Start
                        'Enhancement - Changes For PACT [Ref #249,252]
                        Dim mstrDescription As String = drRow("Description").ToString()
                        'Pinkal (13-Aug-2018) -- End


                        'Pinkal (28-Jul-2018) -- Start
                        'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                        If mblnAllowOverTimeToEmpTimesheet = False Then
                        Dim dtHoliday As DataTable = objEmpHoliday.GetEmployeeHoliday(mintEmployeeunkid, mdtActivitydate.AddDays(i).Date)
                        If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                            dtHoliday = Nothing
                            Continue For
                        End If
                            'Pinkal (13-Aug-2018) -- Start
                            'Enhancement - Changes For PACT [Ref #249,252]
                        ElseIf mblnAllowOverTimeToEmpTimesheet Then

                            Dim dtHoliday As DataTable = objEmpHoliday.GetEmployeeHoliday(mintEmployeeunkid, mdtActivitydate.AddDays(i).Date)
                            If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then

                                'Pinkal (04-Aug-2023) -- Start
                                '(A1X-1150) Afya Plus - Develop configuration setting for st. Judes to include or exclude weekends when filling timesheets.
                                If mblnIncludeHolidayInOvertimeToEmpTimesheet = False Then Continue For
                                'Pinkal (04-Aug-2023) -- End

                                If drRow("Description").ToString().Trim().Length > 0 Then
                                mstrDescription = dtHoliday.Rows(0)("holidayname").ToString() & " / " & drRow("Description").ToString()
                                Else
                                    mstrDescription = dtHoliday.Rows(0)("holidayname").ToString()
                                End If
                            End If
                            dtHoliday = Nothing
                        End If
                        'Pinkal (28-Jul-2018) -- End

                        '/* END FOR CHECK HOLIDAY */



                        'Pinkal (30-Aug-2019) -- Start
                        'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                        If mblnAllowOverTimeToEmpTimesheet = False Then

                            'Pinkal (20-Sep-2019) -- Start
                            'Enhancement NMB - Working On Budget timesheet testing cases for PACT.
                            Dim minTotalActivitydaysPerDay As Decimal = 0
                            Dim mintExistingTotalHrsPerDay As Integer = 0

                            'Pinkal (09-Mar-2020) -- Start
                            'Enhancement NMB AD -   Active Directory Integration Requirement For NMB [Ref No : 273].
                            'minTotalActivitydaysPerDay = mdtEmployeeActivity.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("ActivityHoursInMin"))

                            'Pinkal (05-May-2020) -- Start
                            'Enhancement NMB Employee Claim Form Report -   Working on Employee Claim Form Report .
                            'minTotalActivitydaysPerDay = mdtEmployeeActivity.AsEnumerable().Sum(Function(x) x.Field(Of Integer)("ActivityHoursInMin"))

                            If IsDBNull(mdtEmployeeActivity.Compute("SUM(ActivityHoursInMin)", "1=1")) Then
                                minTotalActivitydaysPerDay = 0
                            Else
                                minTotalActivitydaysPerDay = CDec(mdtEmployeeActivity.Compute("SUM(ActivityHoursInMin)", "1=1"))
                            End If
                            'Pinkal (05-May-2020) -- End


                            'Pinkal (09-Mar-2020) -- End


                            If mdtEmployeeActivity.AsEnumerable().Count <= 1 Then
                                mintExistingTotalHrsPerDay = GetEmpTotalWorkingHrsForTheDay(mdtActivitydate.AddDays(i).Date, mdtActivitydate.AddDays(i).Date, mintEmployeeunkid, objDataOperation)
                            End If

                            If ((minTotalActivitydaysPerDay + mintExistingTotalHrsPerDay) * 60) > mintWeekdaysWorkingHrs Then
                                If mstrShiftExceedDates.Contains(mdtActivitydate.AddDays(i).ToShortDateString()) = False Then
                                    mstrShiftExceedDates &= mdtActivitydate.AddDays(i).Date.ToShortDateString() & ","
                                End If
                                Continue For
                            End If
                            'Pinkal (20-Sep-2019) -- End

                        End If
                        'Pinkal (30-Aug-2019) -- End




                        If isExist(mintEmployeeunkid, CInt(drRow("fundactivityunkid")), mdtActivitydate.AddDays(i).Date, objDataOperation, -1) Then
                        mstrMessage = Language.getMessage(mstrModuleName, 1, "This Activity is already defined of this employee for same date. Please define different activity for this date.")
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If


                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                        objDataOperation.AddParameter("@activitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActivitydate.AddDays(i).Date.ToString)
                    objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("fundsourceunkid").ToString))
                    objDataOperation.AddParameter("@projectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("fundprojectcodeunkid").ToString))
                    objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("fundactivityunkid").ToString))

                        'Pinkal (28-Mar-2018) -- Start
                        'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                        'objDataOperation.AddParameter("@activity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("ActivityHoursInMin").ToString))
                        'objDataOperation.AddParameter("@approvedactivity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovedActivity_Hrs.ToString)

                        'Pinkal (23-Apr-2018) -- Start
                        'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                        'objDataOperation.AddParameter("@activity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(drRow("ActivityHoursInMin").ToString))
                        objDataOperation.AddParameter("@activity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecActivityHoursInMin)
                        'Pinkal (23-Apr-2018) -- End

                        objDataOperation.AddParameter("@approvedactivity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedActivity_Hrs.ToString)
                        'Pinkal (28-Mar-2018) -- End


                        'Pinkal (13-Aug-2018) -- Start
                        'Enhancement - Changes For PACT [Ref #249,252]
                        'objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, drRow("Description").ToString)
                        objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrDescription)
                        'Pinkal (13-Aug-2018) -- End

                    objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                    objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
                    objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
                    objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitForApproval.ToString)


                        'Pinkal (28-Jul-2018) -- Start
                        'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                        objDataOperation.AddParameter("@submission_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrSubmissionRemark.ToString)
                        'Pinkal (28-Jul-2018) -- End

                        'Pinkal (06-Jan-2023) -- Start
                        ''(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                        objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("costcenterunkid").ToString))
                        'Pinkal (06-Jan-2023) -- End



                    strQ = "INSERT INTO ltbemployee_timesheet ( " & _
                              "  periodunkid " & _
                              ", employeeunkid " & _
                              ", activitydate " & _
                              ", fundsourceunkid " & _
                              ", projectcodeunkid " & _
                              ", activityunkid " & _
                              ", activity_hrs " & _
                              ", approvedactivity_hrs " & _
                              ", description " & _
                              ", statusunkid " & _
                              ", userunkid " & _
                              ", isvoid " & _
                              ", voiduserunkid " & _
                              ", voiddatetime " & _
                              ", voidreason" & _
                              ", loginemployeeunkid " & _
                              ", voidloginemployeeunkid " & _
                              ", issubmit_approval " & _
                                  ", submission_remark " & _
                                  ", costcenterunkid " & _
                            ") VALUES (" & _
                              "  @periodunkid " & _
                              ", @employeeunkid " & _
                              ", @activitydate " & _
                              ", @fundsourceunkid " & _
                              ", @projectcodeunkid " & _
                              ", @activityunkid " & _
                              ", @activity_hrs " & _
                              ", @approvedactivity_hrs " & _
                              ", @description " & _
                              ", @statusunkid " & _
                              ", @userunkid " & _
                              ", @isvoid " & _
                              ", @voiduserunkid " & _
                              ", @voiddatetime " & _
                              ", @voidreason" & _
                              ", @loginemployeeunkid " & _
                              ", @voidloginemployeeunkid " & _
                              ", @issubmit_approval " & _
                                  ", @submission_remark " & _
                                  ", @costcenterunkid " & _
                            "); SELECT @@identity"

                        'Pinkal (06-Jan-2023) -- '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.[@costcenterunkid]

                        'Pinkal (28-Jul-2018) --  'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251] [ ", @submission_remark " & _] 

                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintEmptimesheetunkid = dsList.Tables(0).Rows(0).Item(0)

                    mintFundsourceunkid = CInt(drRow("fundsourceunkid"))
                    mintProjectcodeunkid = CInt(drRow("fundprojectcodeunkid"))
                    mintActivityunkid = CInt(drRow("fundactivityunkid"))

                        'Pinkal (28-Mar-2018) -- Start
                        'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                        'mintActivity_Hrs = CInt(drRow("ActivityHoursInMin"))
                        mdecActivity_Hrs = CDec(drRow("ActivityHoursInMin"))
                        'Pinkal (28-Mar-2018) -- End

                    mstrDescription = drRow("Description").ToString().Trim
                        mdtActivitydate = dtFromDate.AddDays(i).Date


                        'Pinkal (28-Jul-2018) -- Start
                        'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                        mstrSubmissionRemark = drRow("submission_remark").ToString()
                        'Pinkal (28-Jul-2018) -- End

                        'Pinkal (06-Jan-2023) -- Start
                        '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                        mintCostCenterunkid = CInt(drRow("costcenterunkid"))
                        'Pinkal (06-Jan-2023) -- End

                    If InsertATBudget_Timesheet(objDataOperation, 1) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                        mdtActivitydate = dtFromDate.Date

                Next

                Next

            End If


            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (ltbemployee_timesheet) </purpose>
    ''' <param name="blnIsSubmitForApproval">Pass True when cancel status is set</param>
    Public Function Update(ByVal xDatabaseName As String, ByVal xYearID As Integer, ByVal xUserID As Integer _
                                     , ByVal xCompanyID As Integer, ByVal mdtEmpAsonDate As Date _
                                     , ByVal xIncludeInActiveEmployee As Boolean _
                         , ByVal blnIsFromSubmitForApproval As Boolean _
                                     , ByVal mblnAllowOverTimeToEmpTimesheet As Boolean _
                         , Optional ByVal objDOperation As clsDataOperation = Nothing _
                                     , Optional ByVal blnIsCancel As Boolean = False _
                                     , Optional ByRef mstrShiftExceedDates As String = "") As Boolean


        'Pinkal (29-Aug-2019) -- 'Enhancement NMB - Working on P2P Get Token Service URL.[ ByRef mstrShiftExceedDates As String]

        'Pinkal (13-Aug-2018) -- 'Enhancement - Changes For PACT [Ref #249,252] [ , ByVal mblnAllowOverTimeToEmpTimesheet As Boolean _]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If

        Try


            'Pinkal (30-Aug-2019) -- Start
            'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
            Dim objConfig As New clsConfigOptions
            Dim mblnAllowEmpAssignedProjectExceedTime As Boolean = objConfig.GetKeyValue(xCompanyID, "AllowEmpAssignedProjectExceedTime", objDataOperation)
            Dim mblnAllowActivityHoursByPercentage As Boolean = objConfig.GetKeyValue(xCompanyID, "AllowActivityHoursByPercentage", objDataOperation)
            Dim mblnAllowToExceedTimeAssignedToActivity As Boolean = objConfig.GetKeyValue(xCompanyID, "AllowToExceedTimeAssignedToActivity", objDataOperation)
            objConfig = Nothing
            'Pinkal (30-Aug-2019) -- End


            If mdtEmployeeActivity IsNot Nothing AndAlso mdtEmployeeActivity.Rows.Count > 0 Then

                For Each drRow As DataRow In mdtEmployeeActivity.Rows
                    If blnIsFromSubmitForApproval = True AndAlso blnIsCancel = False Then 'Nilay (27 Feb 2017) -- [AndAlso blnIsCancel = False]
                        mdtActivitydate = CDate(drRow.Item("activitydate")).Date
                        mintStatusunkid = CInt(drRow("statusunkid"))
                        mintEmployeeunkid = CInt(drRow.Item("employeeunkid"))
                    End If

                    'Pinkal (13-Oct-2017) -- Start
                    'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                    'If isExist(mintEmployeeunkid, CInt(drRow("fundactivityunkid")), mdtActivitydate.Date, objDataOperation, CInt(drRow("emptimesheetunkid"))) Then

                    If mintEmptimesheetunkid <= 0 Then
                        mintEmptimesheetunkid = CInt(drRow("emptimesheetunkid"))
                    End If


                    If isExist(mintEmployeeunkid, CInt(drRow("fundactivityunkid")), mdtActivitydate.Date, objDataOperation, mintEmptimesheetunkid) Then
                        mstrMessage = Language.getMessage(mstrModuleName, 1, "This Activity is already defined of this employee for same date. Please define different activity for this date.")
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If

                    'Pinkal (20-Sep-2019) -- Start
                    'Enhancement NMB - Working On Budget timesheet testing cases for PACT.
                    Dim mintWeekdaysWorkingHrs As Integer = 0
                    'Pinkal (20-Sep-2019) -- End


                    'Pinkal (30-Aug-2019) -- Start
                    'Enhancement [0003693 - PACT] - Working on Allow people to exceed time assigned to the project initially.
                    If blnIsCancel = False AndAlso mblnAllowOverTimeToEmpTimesheet = False Then

                        Dim objEmpshift As New clsEmployee_Shift_Tran
                        Dim mintShiftID As Integer = objEmpshift.GetEmployee_Current_ShiftId(mdtActivitydate, mintEmployeeunkid)
                        objEmpshift = Nothing

                        Dim objShiftTran As New clsshift_tran
                        objShiftTran.GetShiftTran(mintShiftID)

                        Dim drWeekend() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(mdtActivitydate.Date, FirstDayOfWeek.Sunday) - 1 & " AND isweekend = 1")
                        If drWeekend IsNot Nothing AndAlso drWeekend.Length > 0 Then
                            Continue For
                        Else
                            Dim drWeekDayRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & Weekday(mdtActivitydate.Date, FirstDayOfWeek.Sunday) - 1 & " AND isweekend = 0")
                            If drWeekDayRow.Length > 0 Then
                                mintWeekdaysWorkingHrs = CInt(drWeekDayRow(0)("workinghrsinsec"))

                                'Pinkal (21-Oct-2019) -- Start
                                'Defect NMB - Worked On Employee Timesheet Id not set properly due to that it is creating problem.
                                Dim minTotalActivitydaysPerDay As Integer = 0
                                If blnIsFromSubmitForApproval Then
                                    'Pinkal (27-Nov-2019) -- Start
                                    'Bug THPS [0004286] - Issues with employee timesheet.
                                    If mblnAllowActivityHoursByPercentage Then
                                        If mblnAllowEmpAssignedProjectExceedTime Then
                                    minTotalActivitydaysPerDay = mdtEmployeeActivity.AsEnumerable().Where(Function(x) x.Field(Of Date)("activitydate") = mdtActivitydate.Date).Sum(Function(x) x.Field(Of Decimal)("ActivityHoursInMin"))
                                Else
                                            minTotalActivitydaysPerDay = mdtEmployeeActivity.AsEnumerable().Where(Function(x) x.Field(Of Date)("activitydate") = mdtActivitydate.Date And x.Field(Of Integer)("employeeunkid") = mintEmployeeunkid And x.Field(Of Integer)("fundactivityunkid") = CInt(drRow("fundactivityunkid"))).Sum(Function(x) x.Field(Of Decimal)("ActivityHoursInMin"))
                                        End If
                                    Else
                                        'Pinkal (18-Aug-2023) -- Start
                                        'Problem Resolved When Activities are multiple in single day. 
                                        If mblnAllowEmpAssignedProjectExceedTime Then
                                        minTotalActivitydaysPerDay = mdtEmployeeActivity.AsEnumerable().Where(Function(x) x.Field(Of Date)("activitydate") = mdtActivitydate.Date).Sum(Function(x) x.Field(Of Decimal)("ActivityHoursInMin"))
                                        Else
                                            minTotalActivitydaysPerDay = mdtEmployeeActivity.AsEnumerable().Where(Function(x) x.Field(Of Date)("activitydate") = mdtActivitydate.Date And x.Field(Of Integer)("employeeunkid") = mintEmployeeunkid And x.Field(Of Integer)("fundactivityunkid") = CInt(drRow("fundactivityunkid"))).Sum(Function(x) x.Field(Of Decimal)("ActivityHoursInMin"))
                                        End If
                                        'Pinkal (18-Aug-2023) -- End
                                    End If
                                    'Pinkal (27-Nov-2019) -- End
                                Else
                                    minTotalActivitydaysPerDay = mdtEmployeeActivity.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("ActivityHoursInMin"))
                                End If
                                'Pinkal (21-Oct-2019) -- End

                                If mblnAllowEmpAssignedProjectExceedTime Then
                                    Dim objExemptEmp As New clstsexemptemployee_tran
                                    If objExemptEmp.isExist(mintEmployeeunkid, -1, objDataOperation) Then

                                        If (minTotalActivitydaysPerDay * 60) > mintWeekdaysWorkingHrs Then
                                            mstrShiftExceedDates &= mdtActivitydate.ToShortDateString() & ","
                                            objShiftTran = Nothing
                                            mintEmptimesheetunkid = 0    'Pinkal (27-Nov-2019) -- 'Bug THPS [0004286] - Issues with employee timesheet.
                                            Continue For
                                        End If 'If (minTotalActivitydaysPerDay * 60) > mintWeekdaysWorkingHrs Then

                                    Else  '  If objExemptEmp.isExist(mintEmployeeunkid, -1, objDataOperation) Then

                                        If mblnAllowToExceedTimeAssignedToActivity = False Then
                                            If minTotalActivitydaysPerDay > CInt((mintWeekdaysWorkingHrs * CInt(drRow("percentage")) / 100) / 60) Then
                                                mstrShiftExceedDates &= mdtActivitydate.ToShortDateString() & ","
                                                objShiftTran = Nothing
                                                mintEmptimesheetunkid = 0    'Pinkal (27-Nov-2019) -- 'Bug THPS [0004286] - Issues with employee timesheet.
                                                Continue For
                                            Else
                                                If (minTotalActivitydaysPerDay * 60) > mintWeekdaysWorkingHrs Then
                                                    mstrShiftExceedDates &= mdtActivitydate.ToShortDateString() & ","
                                                    objShiftTran = Nothing
                                                    mintEmptimesheetunkid = 0    'Pinkal (27-Nov-2019) -- 'Bug THPS [0004286] - Issues with employee timesheet.
                                                    Continue For
                                                End If '  If (CDec(drRow("ActivityHoursInMin")) * 60) > CInt(drWeekDayRow(0)("workinghrsinsec")) Then
                                            End If

                                        ElseIf mblnAllowToExceedTimeAssignedToActivity Then

                                            If (minTotalActivitydaysPerDay * 60) > mintWeekdaysWorkingHrs Then
                                                mstrShiftExceedDates &= mdtActivitydate.ToShortDateString() & ","
                                                objShiftTran = Nothing
                                                mintEmptimesheetunkid = 0    'Pinkal (27-Nov-2019) -- 'Bug THPS [0004286] - Issues with employee timesheet.
                                                Continue For
                                            End If 'If (minTotalActivitydaysPerDay * 60) > mintWeekdaysWorkingHrs Then

                                        End If  ' If mblnAllowToExceedTimeAssignedToActivity = False Then

                                    End If '  If mblnAllowEmpAssignedProjectExceedTime Then

                                ElseIf mblnAllowEmpAssignedProjectExceedTime = False Then

                                    If mblnAllowToExceedTimeAssignedToActivity = False Then
                                        If minTotalActivitydaysPerDay > CInt((mintWeekdaysWorkingHrs * CInt(drRow("percentage")) / 100) / 60) Then
                                            mstrShiftExceedDates &= mdtActivitydate.ToShortDateString() & ","
                                            objShiftTran = Nothing
                                            mintEmptimesheetunkid = 0    'Pinkal (27-Nov-2019) -- 'Bug THPS [0004286] - Issues with employee timesheet.
                                            Continue For
                                        Else
                                            If (minTotalActivitydaysPerDay * 60) > mintWeekdaysWorkingHrs Then
                                                mstrShiftExceedDates &= mdtActivitydate.ToShortDateString() & ","
                                                objShiftTran = Nothing
                                                mintEmptimesheetunkid = 0    'Pinkal (27-Nov-2019) -- 'Bug THPS [0004286] - Issues with employee timesheet.
                                                Continue For
                                            End If '  If (CDec(drRow("ActivityHoursInMin")) * 60) > CInt(drWeekDayRow(0)("workinghrsinsec")) Then
                                        End If

                                    ElseIf mblnAllowToExceedTimeAssignedToActivity Then

                                        If (minTotalActivitydaysPerDay * 60) > mintWeekdaysWorkingHrs Then
                                            mstrShiftExceedDates &= mdtActivitydate.ToShortDateString() & ","
                                            objShiftTran = Nothing
                                            mintEmptimesheetunkid = 0    'Pinkal (27-Nov-2019) -- 'Bug THPS [0004286] - Issues with employee timesheet.
                                            Continue For
                                        End If 'If (minTotalActivitydaysPerDay * 60) > mintWeekdaysWorkingHrs Then

                                    End If  ' If mblnAllowToExceedTimeAssignedToActivity = False Then

                                End If  ' If mblnAllowEmpAssignedProjectExceedTime Then
                                objShiftTran = Nothing

                            End If ' If drWeekDayRow.Length > 0 Then

                        End If '   If drWeekend IsNot Nothing AndAlso drWeekend.Length > 0 Then

                    End If ' If blnIsFromSubmitForApproval = False AndAlso blnIsCancel = False AndAlso mblnAllowOverTimeToEmpTimesheet = False Then
                    'Pinkal (30-Aug-2019) -- End



                    'Pinkal (13-Aug-2018) -- Start
                    'Enhancement - Changes For PACT [Ref #249,252]
                    Dim mstrDescription As String = drRow("Description").ToString()
                    If mblnAllowOverTimeToEmpTimesheet Then
                        Dim objEmpHoliday As New clsemployee_holiday
                        Dim dtHoliday As DataTable = objEmpHoliday.GetEmployeeHoliday(mintEmployeeunkid, mdtActivitydate.Date)
                        If dtHoliday IsNot Nothing AndAlso dtHoliday.Rows.Count > 0 Then
                            If drRow("Description").ToString().Trim.Length > 0 Then
                                If mstrDescription.Trim.Contains(dtHoliday.Rows(0)("holidayname").ToString().Trim()) = False Then
                                    mstrDescription = dtHoliday.Rows(0)("holidayname").ToString() & " / " & drRow("Description").ToString()
                                Else
                                    mstrDescription = dtHoliday.Rows(0)("holidayname").ToString() & " " & mstrDescription.Trim().Remove(0, dtHoliday.Rows(0)("holidayname").ToString().Length).Trim()
                                End If
                            Else
                                mstrDescription = dtHoliday.Rows(0)("holidayname").ToString()
                            End If
                        End If
                        dtHoliday = Nothing
                        objEmpHoliday = Nothing

                        'Pinkal (20-Sep-2019) -- Start
                        'Enhancement NMB - Working On Budget timesheet testing cases for PACT.
                    ElseIf mblnAllowOverTimeToEmpTimesheet = False Then
                        Dim minTotalActivitydaysPerDay As Integer = 0
                        Dim mintExistingTotalHrsPerDay As Integer = 0
                        Dim mintSpecificActivityHrsPerDay As Integer = 0

                        'Pinkal (21-Oct-2019) -- Start
                        'Defect NMB - Worked On Employee Timesheet Id not set properly due to that it is creating problem.
                        If blnIsFromSubmitForApproval = False Then
                        minTotalActivitydaysPerDay = mdtEmployeeActivity.AsEnumerable().Sum(Function(x) x.Field(Of Decimal)("ActivityHoursInMin"))
                        Else
                            'Pinkal (27-Nov-2019) -- Start
                            'Bug THPS [0004286] - Issues with employee timesheet.
                            If mblnAllowActivityHoursByPercentage Then
                                If mblnAllowEmpAssignedProjectExceedTime Then
                                    minTotalActivitydaysPerDay = mdtEmployeeActivity.AsEnumerable().Where(Function(x) x.Field(Of Date)("activitydate") = mdtActivitydate.Date).Sum(Function(x) x.Field(Of Decimal)("ActivityHoursInMin"))
                                Else
                                    minTotalActivitydaysPerDay = mdtEmployeeActivity.AsEnumerable().Where(Function(x) x.Field(Of Date)("activitydate") = mdtActivitydate.Date And x.Field(Of Integer)("employeeunkid") = mintEmployeeunkid And x.Field(Of Integer)("fundactivityunkid") = CInt(drRow("fundactivityunkid"))).Sum(Function(x) x.Field(Of Decimal)("ActivityHoursInMin"))
                                End If
                            Else
                            minTotalActivitydaysPerDay = mdtEmployeeActivity.AsEnumerable().Where(Function(x) x.Field(Of Date)("activitydate") = mdtActivitydate).Sum(Function(x) x.Field(Of Decimal)("ActivityHoursInMin"))
                        End If
                            'Pinkal (27-Nov-2019) -- End
                        End If
                        'Pinkal (21-Oct-2019) -- End

                        mintSpecificActivityHrsPerDay = GetEmpTotalWorkingHrsForTheDay(mdtActivitydate.Date, mdtActivitydate.Date, mintEmployeeunkid, objDataOperation, CInt(drRow("fundactivityunkid")))
                        mintExistingTotalHrsPerDay = GetEmpTotalWorkingHrsForTheDay(mdtActivitydate.Date, mdtActivitydate.Date, mintEmployeeunkid, objDataOperation)

                        'Pinkal (21-Oct-2019) -- Start
                        'Defect NMB - Worked On Employee Timesheet Id not set properly due to that it is creating problem.
                        If blnIsFromSubmitForApproval = False Then
                        If (((mintExistingTotalHrsPerDay - mintSpecificActivityHrsPerDay) + minTotalActivitydaysPerDay) * 60) > mintWeekdaysWorkingHrs Then
                            If mstrShiftExceedDates.Contains(mdtActivitydate.ToShortDateString()) = False Then
                                mstrShiftExceedDates &= mdtActivitydate.ToShortDateString() & ","
                            End If
                                mintEmptimesheetunkid = 0    'Pinkal (27-Nov-2019) -- 'Bug THPS [0004286] - Issues with employee timesheet.
                            Continue For
                        End If
                        End If
                        'Pinkal (21-Oct-2019) -- Start

                        'Pinkal (20-Sep-2019) -- End
                    End If
                    'Pinkal (13-Aug-2018) -- End



                    objDataOperation.ClearParameters()


                    'Pinkal (13-Oct-2017) -- Start
                    'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                    'objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("emptimesheetunkid")).ToString)
                    objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmptimesheetunkid)
                    'Pinkal (13-Oct-2017) -- End
                    objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
                    objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                    objDataOperation.AddParameter("@activitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActivitydate.ToString)
                    objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("fundsourceunkid").ToString))
                    objDataOperation.AddParameter("@projectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("fundprojectcodeunkid").ToString))
                    objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("fundactivityunkid").ToString))

                    'Pinkal (28-Mar-2018) -- Start
                    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                    'objDataOperation.AddParameter("@activity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("ActivityHoursInMin").ToString))
                    'objDataOperation.AddParameter("@approvedactivity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovedActivity_Hrs.ToString)
                    objDataOperation.AddParameter("@activity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, CDec(drRow("ActivityHoursInMin").ToString))
                    objDataOperation.AddParameter("@approvedactivity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedActivity_Hrs.ToString)
                    'Pinkal (28-Mar-2018) -- End

                    'Pinkal (13-Aug-2018) -- Start
                    'Enhancement - Changes For PACT [Ref #249,252]
                    'objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, drRow("Description").ToString)
                    objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrDescription.Trim())
                    'Pinkal (13-Aug-2018) -- End

                    objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
                    objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
                    objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
                    objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitForApproval)


                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    objDataOperation.AddParameter("@submission_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, drRow("submission_remark").ToString)
                    'Pinkal (28-Jul-2018) -- End

                    'Pinkal (06-Jan-2023) -- Start
                    '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                    objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(drRow("costcenterunkid")))
                    'Pinkal (06-Jan-2023) -- End


                    strQ = "UPDATE ltbemployee_timesheet SET " & _
                              "  periodunkid = @periodunkid" & _
                              ", employeeunkid = @employeeunkid" & _
                              ", activitydate = @activitydate" & _
                              ", fundsourceunkid = @fundsourceunkid" & _
                              ", projectcodeunkid = @projectcodeunkid" & _
                              ", activityunkid = @activityunkid" & _
                              ", activity_hrs = @activity_hrs" & _
                              ", approvedactivity_hrs = @approvedactivity_hrs " & _
                              ", description = @description" & _
                              ", statusunkid = @statusunkid " & _
                              ", userunkid = @userunkid" & _
                              ", isvoid = @isvoid" & _
                              ", voiduserunkid = @voiduserunkid" & _
                              ", voiddatetime = @voiddatetime" & _
                              ", voidreason = @voidreason " & _
                              ", loginemployeeunkid = @loginemployeeunkid " & _
                              ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                              ", issubmit_approval = @issubmit_approval " & _
                              ", submission_remark = @submission_remark " & _
                              ", costcenterunkid = @costcenterunkid " & _
                            "WHERE emptimesheetunkid = @emptimesheetunkid "

                    'Pinkal (06-Jan-2023) -- (A1X-556) FHS/PSI - Hide Leave Day Fraction from add/edit leave form screen.[    ", costcenterunkid = @costcenterunkid " & _]

                    'Pinkal (28-Jul-2018) -- 'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251][", submission_remark = @submission_remark " & _]

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintEmptimesheetunkid = CInt(drRow("emptimesheetunkid"))
                    mintFundsourceunkid = CInt(drRow("fundsourceunkid"))
                    mintProjectcodeunkid = CInt(drRow("fundprojectcodeunkid"))
                    mintActivityunkid = CInt(drRow("fundactivityunkid"))
                    'Pinkal (28-Mar-2018) -- Start
                    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                    'mintActivity_Hrs = CInt(drRow("ActivityHoursInMin"))
                    mdecActivity_Hrs = CDec(drRow("ActivityHoursInMin"))
                    'Pinkal (28-Mar-2018) -- End

                    mstrDescription = drRow("Description").ToString().Trim

                    'Pinkal (28-Jul-2018) -- Start
                    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                    mstrSubmissionRemark = drRow("submission_remark").ToString().Trim()
                    'Pinkal (28-Jul-2018) -- End

                    'Pinkal (06-Jan-2023) -- Start
                    '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                    mintCostCenterunkid = CInt(drRow("costcenterunkid"))
                    'Pinkal (06-Jan-2023) -- End


                    If InsertATBudget_Timesheet(objDataOperation, 2) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

            
                    If blnIsFromSubmitForApproval = True AndAlso blnIsCancel = False Then 'Nilay (27 Feb 2017) -- [AndAlso blnIsCancel = False]
                        Dim objApproverMst As New clstsapprover_master
                        mdtApproverList = objApproverMst.GetEmployeeApprover(xDatabaseName, xYearID, xCompanyID, mdtEmpAsonDate, xIncludeInActiveEmployee, _
                                                                             -1, mintEmployeeunkid.ToString, -1, objDataOperation)
                    End If


                    If mdtApproverList IsNot Nothing AndAlso mdtApproverList.Rows.Count > 0 Then
                        objApproval._Emptimesheetunkid = mintEmptimesheetunkid
                        objApproval._Employeeunkid = mintEmployeeunkid
                        objApproval._Periodunkid = mintPeriodunkid
                        objApproval._Fundsourceunkid = CInt(drRow("fundsourceunkid"))
                        objApproval._Projectcodeunkid = CInt(drRow("fundprojectcodeunkid"))
                        objApproval._Activityunkid = CInt(drRow("fundactivityunkid"))

                        'Pinkal (28-Mar-2018) -- Start
                        'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                        'objApproval._Activity_Hrs = CInt(drRow("ActivityHoursInMin"))
                        objApproval._Activity_Hrs = CDec(drRow("ActivityHoursInMin"))
                        'Pinkal (28-Mar-2018) -- End

                        'Pinkal (06-Jan-2023) -- Start
                        '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
                        objApproval._CostCenterunkid = CInt(drRow("costcenterunkid"))
                        'Pinkal (06-Jan-2023) -- End

                        objApproval._Approvaldate = mdtActivitydate
                        objApproval._Statusunkid = 2
                        objApproval._Description = ""
                        objApproval._IsCancel = 0
                        objApproval._Userunkid = mintUserunkid
                        objApproval._WebFormName = mstrWebFormName
                        objApproval._WebClientIP = mstrWebClientIP
                        objApproval._WebHostName = mstrWebHostName

                        Dim mintMinPriority As Integer = 0
                        Dim drPriorityRow() As DataRow = mdtApproverList.Select("Priority = MIN(Priority)")
                        If drPriorityRow.Length > 0 Then
                            mintMinPriority = CInt(drPriorityRow(0)("Priority"))
                        End If

                        For Each dr As DataRow In mdtApproverList.Rows
                            objApproval._Tsapproverunkid = CInt(dr("tsapproverunkid"))
                            objApproval._Approveremployeeunkid = CInt(dr("employeeapproverunkid"))

                            If mintMinPriority = CInt(dr("Priority")) Then
                                objApproval._Visibleunkid = 2
                            Else
                                objApproval._Visibleunkid = -1
                            End If

                            If blnIsFromSubmitForApproval = True AndAlso blnIsCancel = False Then 'Nilay (27 Feb 2017) -- [AndAlso blnIsCancel = False]
                                    If objApproval.Insert(objDataOperation) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                            If objApproval.isExist(mintEmployeeunkid, CInt(drRow("fundactivityunkid")), mdtActivitydate, mintEmptimesheetunkid, CInt(dr("tsapproverunkid")), objDataOperation) Then
                                If objApproval.Update(xDatabaseName, xYearID, xUserID, xCompanyID, mdtEmpAsonDate, xIncludeInActiveEmployee, False, CInt(dr("Priority")), objDataOperation, False) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            Else
                                If objApproval.Insert(objDataOperation) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            End If
                                End If
                        Next
                    End If

                    'Pinkal (13-Oct-2017) -- Start
                    'Enhancement - Ref Id 62 Working on Global Budget Timesheet Change.
                    mintEmptimesheetunkid = 0
                    'Pinkal (13-Oct-2017) -- End
                Next
            End If

            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If objDOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (ltbemployee_timesheet) </purpose>
    Public Function Delete(ByVal xDatabaseName As String, ByVal mdtEmployeeAsonDate As Date, ByVal intUnkid As Integer _
                                   , Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean

        'Pinkal (28-Mar-2018) -- 'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.[Optional ByVal objDOperation As clsDataOperation = Nothing]

        'Pinkal (28-Mar-2018) --  End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (28-Mar-2018) -- Start
        'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
        'Dim objDataOperation As New clsDataOperation
        'objDataOperation.BindTransaction()
        Dim objDataOperation As clsDataOperation = Nothing
        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Else
            objDataOperation = objDOperation
        End If
        'Pinkal (28-Mar-2018) --  End


        Try
            strQ = " UPDATE ltbemployee_timesheet SET " & _
                      " isvoid = 1,voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = GetDate(),voidreason = @voidreason " & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                      " WHERE emptimesheetunkid = @emptimesheetunkid "

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, IIf(mintVoidLoginEmployeeunkid > 0, mintVoidLoginEmployeeunkid, -1))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Emptimesheetunkid(objDataOperation) = intUnkid

            If InsertATBudget_Timesheet(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (13-Aug-2018) -- Start
            'Enhancement - Changes For PACT [Ref #249,252]
            'dsList = objApproval.GetList("List", xDatabaseName, mdtEmployeeAsonDate, "", mintVoiduserunkid, False, objDataOperation, True, intUnkid, False, "")
            dsList = objApproval.GetList("List", xDatabaseName, mdtEmployeeAsonDate, "", mintVoiduserunkid, False, ConfigParameter._Object._AllowOverTimeToEmpTimesheet, objDataOperation, True, intUnkid, False, "")
            'Pinkal (13-Aug-2018) -- End


            strQ = " UPDATE tsemptimesheet_approval SET " & _
                      " isvoid = 1,voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = GetDate(),voidreason = @voidreason " & _
                      " WHERE emptimesheetunkid = @emptimesheetunkid AND isvoid = 0 AND iscancel=0 "
            'Nilay (10 Jan 2017) -- [AND iscancel=0]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsList.Tables(0).Rows
                    objApproval._Timesheetapprovalunkid(objDataOperation) = CInt(dr("timesheetapprovalunkid"))
                    If objApproval.InsertATEmpTimesheet_Approval(objDataOperation, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If


            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
            If objDOperation Is Nothing Then
            objDataOperation.ReleaseTransaction(True)
            End If
            'Pinkal (28-Mar-2018) --  End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.
            If objDOperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (28-Mar-2018) --  End

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (tsemptimesheet_approval) </purpose>
    Public Function CancelTimesheet(ByVal xDatabaseName As String, ByVal xYearID As Integer, ByVal xUserID As Integer, ByVal xCompanyID As Integer, _
                                    ByVal xIncludeInActiveEmployee As Boolean, ByVal xUserModeSetting As String, ByVal xCancelReason As String, _
                                    ByVal strEmpTimehseetIDs As String, ByVal mblnAllowOverTimeInTimesheet As Boolean, Optional ByVal objDOperation As clsDataOperation = Nothing) As Boolean

        'Pinkal (28-Mar-2018) --'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.[ByVal mblnAllowOverTimeInTimesheet As Boolean]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            Dim arrayEmpTimesheetID() As String = strEmpTimehseetIDs.Split(",")

            If arrayEmpTimesheetID.Length > 0 Then

                For i As Integer = 0 To arrayEmpTimesheetID.Length - 1

                    mintEmptimesheetunkid = arrayEmpTimesheetID(i)
                    Call GetData(objDataOperation)

            strQ = "SELECT ISNULL(timesheetapprovalunkid,0) AS timesheetapprovalunkid    FROM tsemptimesheet_approval WHERE isvoid = 0 AND iscancel = 0 AND emptimesheetunkid = @emptimesheetunkid "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmptimesheetunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "List")


            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count <= 0 Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If


                    'Pinkal (13-Apr-2017) -- Start
                    'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
                    objApproval._WebFormName = mstrWebFormName
                    objApproval._WebClientIP = mstrWebClientIP
                    objApproval._WebHostName = mstrWebHostName
                    mintUserunkid = xUserID
                    'Pinkal (13-Apr-2017) -- End


            strQ = " UPDATE tsemptimesheet_approval SET " & _
                      "  iscancel = @iscancel " & _
                      ", canceluserunkid = @canceluserunkid" & _
                      ", canceldatetime = GetDate() " & _
                      ", cancelreason = @cancelreason " & _
                      " WHERE isvoid =  0 AND iscancel = 0 AND timesheetapprovalunkid = @timesheetapprovalunkid "

            For Each dr As DataRow In dsList.Tables(0).Rows

                objApproval._Timesheetapprovalunkid(objDataOperation) = CInt(dr("timesheetapprovalunkid"))
                objApproval._IsCancel = True
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@timesheetapprovalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objApproval._Timesheetapprovalunkid.ToString)
                objDataOperation.AddParameter("@iscancel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, objApproval._IsCancel.ToString)
                objDataOperation.AddParameter("@canceluserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                objDataOperation.AddParameter("@cancelreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, xCancelReason)
                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If objApproval.InsertATEmpTimesheet_Approval(objDataOperation, 2) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next


                    'Pinkal (28-Mar-2018) -- Start
                    'Enhancement - (RefNo: 191)  Working on Duplication of timesheet & Holiday on same day.

                    'mdtEmployeeActivity = GetList("List", xDatabaseName, xUserID, xYearID, xCompanyID, mdtActivitydate, mdtActivitydate, xUserModeSetting, True, _
                    '                              xIncludeInActiveEmployee, mintEmployeeunkid, False, True, mintPeriodunkid, objDataOperation, _
                    '                              "ltbemployee_timesheet.activityunkid = " & mintActivityunkid & "  AND CONVERT(Char(8),ltbemployee_timesheet.activitydate,112) = '" & eZeeDate.convertDate(mdtActivitydate.Date).ToString() & "'").Tables(0)

                    mdtEmployeeActivity = GetList("List", xDatabaseName, xUserID, xYearID, xCompanyID, mdtActivitydate, mdtActivitydate, xUserModeSetting, True, _
                                                  xIncludeInActiveEmployee, mblnAllowOverTimeInTimesheet, mintEmployeeunkid, False, True, mintPeriodunkid, objDataOperation, _
                                                  "ltbemployee_timesheet.activityunkid = " & mintActivityunkid & "  AND CONVERT(Char(8),ltbemployee_timesheet.activitydate,112) = '" & eZeeDate.convertDate(mdtActivitydate.Date).ToString() & "'").Tables(0)

                    'Pinkal (28-Mar-2018) -- End



            mintStatusunkid = 6  'Cancelled

                    'Pinkal (28-Mar-2018) -- Start
                    'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                    'mintApprovedActivity_Hrs = 0
                    mdecApprovedActivity_Hrs = 0
                    'Pinkal (28-Mar-2018) -- End

                    'Pinkal (13-Aug-2018) -- Start
                    'Enhancement - Changes For PACT [Ref #249,252]
                    'If Update(xDatabaseName, xYearID, xUserID, xCompanyID, mdtActivitydate, xIncludeInActiveEmployee, False, objDataOperation, True) = False Then
                    If Update(xDatabaseName, xYearID, xUserID, xCompanyID, mdtActivitydate, xIncludeInActiveEmployee, False, mblnAllowOverTimeInTimesheet, objDataOperation, True) = False Then
                        'Pinkal (13-Aug-2018) -- End
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStatusunkid = 2  'Pending

                    mblnIsSubmitForApproval = False

                    'Pinkal (13-Aug-2018) -- Start
                    'Enhancement - Changes For PACT [Ref #249,252]
                    'If Update(xDatabaseName, xYearID, xUserID, xCompanyID, mdtActivitydate, xIncludeInActiveEmployee, False, objDataOperation, False) = False Then
                    If Update(xDatabaseName, xYearID, xUserID, xCompanyID, mdtActivitydate, xIncludeInActiveEmployee, False, mblnAllowOverTimeInTimesheet, objDataOperation, False) = False Then
                        'Pinkal (13-Aug-2018) -- End
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
                Next
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: CancelTimesheet; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xEmployeeId As Integer, ByVal xActivityunkid As Integer, ByVal xActivityDate As Date _
                                    , Optional ByVal objDOperation As clsDataOperation = Nothing, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDOperation
        End If


        Try
            strQ = "SELECT " & _
                      "  emptimesheetunkid " & _
                      ", periodunkid " & _
                      ", employeeunkid " & _
                      ", activitydate " & _
                      ", fundsourceunkid " & _
                      ", projectcodeunkid " & _
                      ", activityunkid " & _
                      ", activity_hrs " & _
                      ", approvedactivity_hrs " & _
                      ", description " & _
                      ", statusunkid " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", issubmit_approval " & _
                      ", submission_remark " & _
                      ", ISNULL(costcenterunkid,0) AS costcenterunkid " & _
                      " FROM ltbemployee_timesheet " & _
                      " WHERE isvoid = 0  AND CONVERT(char(8),activitydate,112) = @activitydate " & _
                      " AND employeeunkid = @employeeunkid AND activityunkid = @activityunkid "

            'Pinkal (06-Jan-2023) -- (A1X-556) FHS/PSI - Hide Leave Day Fraction from add/edit leave form screen.[ ", ISNULL(costcenterunkid,0) AS costcenterunkid " & _]

            'Pinkal (28-Jul-2018) --  'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251][  ", submission_remark " & _]

            'Pinkal (23-Nov-2017) -- Start
            'Enhancement - Issue related to Budget timesheet Insertion issue.
            '"AND statusunkid NOT IN(1,3) "
            'Pinkal (23-Nov-2017) -- End

            If intUnkid > 0 Then
                strQ &= " AND emptimesheetunkid <> @emptimesheetunkid"
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@activitydate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xActivityDate))
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xActivityunkid)
            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (ltbemployee_timesheet) </purpose>
    Public Function InsertATBudget_Timesheet(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            strQ = "INSERT INTO atltbemployee_timesheet ( " & _
                      " emptimesheetunkid " & _
                      ",  periodunkid " & _
                      ", employeeunkid " & _
                      ", activitydate " & _
                      ", fundsourceunkid " & _
                      ", projectcodeunkid " & _
                      ", activityunkid " & _
                      ", activity_hrs " & _
                      ", approvedactivity_hrs " & _
                      ", description " & _
                      ", statusunkid " & _
                      ", AuditType " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", module_name1 " & _
                      ", module_name2 " & _
                      ", module_name3 " & _
                      ", module_name4 " & _
                      ", module_name5 " & _
                      ", isweb " & _
                      ", issubmit_approval " & _
                      ", loginemployeeunkid " & _
                      ", submission_remark " & _
                      ", costcenterunkid " & _
                    ") VALUES (" & _
                      "  @emptimesheetunkid " & _
                      ", @periodunkid " & _
                      ", @employeeunkid " & _
                      ", @activitydate " & _
                      ", @fundsourceunkid " & _
                      ", @projectcodeunkid " & _
                      ", @activityunkid " & _
                      ", @activity_hrs " & _
                      ", @approvedactivity_hrs " & _
                      ", @description " & _
                      ", @statusunkid " & _
                      ", @auditType " & _
                      ", @audituserunkid " & _
                      ", GetDate() " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @module_name1 " & _
                      ", @module_name2 " & _
                      ", @module_name3 " & _
                      ", @module_name4 " & _
                      ", @module_name5 " & _
                      ", @isweb " & _
                      ", @issubmit_approval " & _
                      ", @loginemployeeunkid " & _
                      ", @submission_remark " & _
                      ", @costcenterunkid " & _
                    "); SELECT @@identity"

            'Pinkal (06-Jan-2023) -- (A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.[@costcenterunkid]

            'Pinkal (28-Jul-2018) -- 'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251][", @submission_remark " & _]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmptimesheetunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@activitydate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtActivitydate.ToString)
            objDataOperation.AddParameter("@fundsourceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFundsourceunkid.ToString)
            objDataOperation.AddParameter("@projectcodeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintProjectcodeunkid.ToString)
            objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivityunkid.ToString)

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            'objDataOperation.AddParameter("@activity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintActivity_Hrs.ToString)
            'objDataOperation.AddParameter("@approvedactivity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovedActivity_Hrs.ToString)
            objDataOperation.AddParameter("@activity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecActivity_Hrs.ToString)
            objDataOperation.AddParameter("@approvedactivity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedActivity_Hrs.ToString)
            'Pinkal (28-Mar-2018) -- End


            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitForApproval.ToString)

            'Pinkal (13-Apr-2017) -- Start
            'Enhancement - Working on Transfer Employee Budget Timesheet Report Move From Payroll Report To TnA Reports.
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            'Pinkal (13-Apr-2017) -- End


            'Pinkal (28-Jul-2018) -- Start
            'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
            objDataOperation.AddParameter("@submission_remark", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrSubmissionRemark.ToString)
            'Pinkal (28-Jul-2018) -- End


            'Pinkal (06-Jan-2023) -- Start
            '(A1X-540) FHS/PSI - As a user, I want to select the cost center that applies to my project when filling my project budget timesheets.
            objDataOperation.AddParameter("@costcenterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCostCenterunkid)
            'Pinkal (06-Jan-2023) -- End


            If mstrWebClientIP.ToString().Length <= 0 Then
                mstrWebClientIP = getIP()
            End If

            If mstrWebHostName.ToString().Length <= 0 Then
                mstrWebHostName = getHostName()
            End If

            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)

            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertATBudget_Timesheet; Module Name: " & mstrModuleName)
            Return False
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (ltbemployee_timesheet) </purpose>
    ''' 
    Public Function GetEmpTotalWorkingHrsForTheDay(ByVal xFromDate As Date, ByVal xToDate As Date, ByVal xEmployeeId As Integer _
                                                                            , Optional ByVal objDoOperation As clsDataOperation = Nothing, Optional ByVal xActivityId As Integer = 0) As Integer

        'Pinkal (20-Sep-2019) --  'Enhancement NMB - Working On Budget timesheet testing cases for PACT.[Optional ByVal objDoOperation As clsDataOperation = Nothing, Optional ByVal xActivityId As Integer = 0]

        Dim mintWorkingHrs As Integer = 0
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        'Pinkal (20-Sep-2019) -- Start
        'Enhancement NMB - Working On Budget timesheet testing cases for PACT.
        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If
        'Pinkal (20-Sep-2019) -- End

        Try

            strQ = "SELECT  " & _
                      " ISNULL(SUM(activity_hrs),0) AS activity_hrs  " & _
                      " FROM ltbemployee_timesheet " & _
                      " WHERE isvoid = 0 AND employeeunkid = @employeeunkid AND CONVERT(CHAR(8),activitydate,112)  BETWEEN @FromDate AND @ToDate "

            'Pinkal (20-Sep-2019) -- Start
            'Enhancement NMB - Working On Budget timesheet testing cases for PACT.
            If xActivityId > 0 Then
                strQ &= " AND activityunkid = @activityunkid"
            End If
            'Pinkal (20-Sep-2019) -- End


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xEmployeeId)
            objDataOperation.AddParameter("@FromDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xFromDate))
            objDataOperation.AddParameter("@ToDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xToDate))

            'Pinkal (20-Sep-2019) -- Start
            'Enhancement NMB - Working On Budget timesheet testing cases for PACT.
            If xActivityId > 0 Then
                objDataOperation.AddParameter("@activityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xActivityId)
            End If
            'Pinkal (20-Sep-2019) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then mintWorkingHrs = CInt(dsList.Tables(0).Rows(0)("activity_hrs"))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpTotalWorkingHrsForTheDay; Module Name: " & mstrModuleName)
        Finally
            dsList = Nothing
            'Pinkal (20-Sep-2019) -- Start
            'Enhancement NMB - Working On Budget timesheet testing cases for PACT.
            If objDoOperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (20-Sep-2019) -- End

        End Try
        Return mintWorkingHrs
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    Public Function GetApproverPendingTimesheet(ByVal xApproverID As Integer, ByVal mstrEmpID As String, Optional ByVal mobjDataOperation As clsDataOperation = Nothing) As String
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrTimesheetID As String = String.Empty
        Try
            Dim objDataOperation As clsDataOperation = Nothing

            If mobjDataOperation IsNot Nothing Then
                objDataOperation = mobjDataOperation
            Else
                objDataOperation = New clsDataOperation
            End If

            objDataOperation.ClearParameters()

            strQ = " SELECT STUFF( " & _
                      " (SELECT  ',' +  CAST(ltbemployee_timesheet.emptimesheetunkid AS NVARCHAR(max)) " & _
                      "   FROM ltbemployee_timesheet " & _
                      "   JOIN tsemptimesheet_approval ON ltbemployee_timesheet.emptimesheetunkid = tsemptimesheet_approval.emptimesheetunkid AND tsapproverunkid  = " & xApproverID & _
                      " WHERE ltbemployee_timesheet.statusunkid in (2) AND ltbemployee_timesheet.employeeunkid IN (" & mstrEmpID & ") AND ltbemployee_timesheet.isvoid = 0  AND tsemptimesheet_approval.isvoid = 0 " & _
                      " ORDER BY ltbemployee_timesheet.emptimesheetunkid  FOR XML PATH('')),1,1,'') AS CSV"

            dsList = objDataOperation.ExecQuery(strQ, "FormList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrTimesheetID = dsList.Tables(0).Rows(0)("CSV").ToString()

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApproverPendingTimesheet; Module Name: " & mstrModuleName)
        End Try
        Return mstrTimesheetID
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function UpdateEmployeeTimesheetStatus(ByVal objDataOperation As clsDataOperation) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mblnFlag As Boolean = False
        Try


            strQ = " UPDATE ltbemployee_timesheet SET " & _
                      "  approvedactivity_hrs = @approvedactivity_hrs " & _
                      ", statusunkid = @statusunkid " & _
                      ", issubmit_approval = @issubmit_approval " & _
                      " WHERE emptimesheetunkid = @emptimesheetunkid "
            'Nilay (01 Apr 2017) -- [issubmit_approval]

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmptimesheetunkid)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid)

            'Pinkal (28-Mar-2018) -- Start
            'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
            'objDataOperation.AddParameter("@approvedactivity_hrs", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovedActivity_Hrs)
            objDataOperation.AddParameter("@approvedactivity_hrs", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecApprovedActivity_Hrs)
            'Pinkal (28-Mar-2018) -- End



            'Nilay (01 Apr 2017) -- Start
            'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitForApproval)
            'Nilay (01 Apr 2017) -- End

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertATBudget_Timesheet(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mblnFlag = True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateEmployeeTimesheetStatus; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function GetActivityNamesByEmployeeID(ByVal intEmployeeID As Integer, _
                                                 ByVal intPeriodID As Integer, _
                                                 ByVal strEmpTimesheetIDs As String, _
                                                 Optional ByVal intStatusID As Integer = 0, _
                                                 Optional ByVal blnIsForPivot As Boolean = False, _
                                                 Optional ByVal blnIsVoid As Boolean = False) As String
        objDataOperation = New clsDataOperation
        Dim exForce As Exception
        Dim strQ As String = String.Empty
        Dim strActivityNames As String = String.Empty
        Dim dsList As DataSet = Nothing
        Try
            objDataOperation.ClearParameters()
            objDataOperation.ClearParameters()

            strQ &= " SELECT (" & _
                              " stuff((SELECT DISTINCT "

            If blnIsForPivot = True Then
                strQ &= " ',[' + bgfundactivity_tran.activity_name + ']' "
            Else
                strQ &= " ',' + bgfundactivity_tran.activity_name "
            End If

            strQ &= " FROM ltbemployee_timesheet " & _
                            " LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = ltbemployee_timesheet.activityunkid " & _
                    " WHERE bgfundactivity_tran.isvoid = 0 " & _
                            " AND ltbemployee_timesheet.employeeunkid=@employeeunkid " & _
                            " AND ltbemployee_timesheet.periodunkid=@periodunkid "
            If blnIsVoid = True Then
                strQ &= " AND ltbemployee_timesheet.isvoid = 1 "
            Else
                strQ &= " AND ltbemployee_timesheet.isvoid = 0 "
            End If

            If intStatusID > 0 Then

                strQ &= " AND ltbemployee_timesheet.statusunkid=@statusunkid "

                Select Case intStatusID
                    Case enBudgetTimesheetStatus.REJECTED, enBudgetTimesheetStatus.DELETED, enBudgetTimesheetStatus.CANCELLED
                        objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, enBudgetTimesheetStatus.PENDING)
                    Case Else
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID.ToString)
                End Select
            End If

            If strEmpTimesheetIDs.Trim.Length > 0 Then
                strQ &= " AND ltbemployee_timesheet.emptimesheetunkid IN(" & strEmpTimesheetIDs & ")  "
            End If

            strQ &= " FOR XML PATH('')),1,1,'') ) "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")
            strActivityNames = dsList.Tables("List").Rows(0).Item(0).ToString

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetActivityNamesByApproverID; Module Name: " & mstrModuleName)
        End Try
        Return strActivityNames
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function GetActivityListByEmployee(ByVal intEmployeeID As Integer, ByVal intPeriodID As Integer, ByVal strEmpTimesheetIDs As String, _
                                              Optional ByVal intStatusID As Integer = 0, Optional ByVal blnIsVoid As Boolean = False) As DataSet

        Dim exForce As Exception
        Dim strQ As String = String.Empty
        Dim strQCols As String = String.Empty
        Dim dsList As DataSet = Nothing

        objDataOperation = New clsDataOperation

        Try
            Dim strActivityCols As String = String.Empty

            strActivityCols = GetActivityNamesByEmployeeID(intEmployeeID, intPeriodID, strEmpTimesheetIDs, intStatusID, True, blnIsVoid)

            If strActivityCols.Trim.Length <= 0 Then Return Nothing

            objDataOperation.ClearParameters()

            strQ = "SELECT * " & _
                   "FROM " & _
                   "( " & _
                   " SELECT " & _
                   "     ltbemployee_timesheet.activitydate AS ActivityDate " & _
                   "    ,bgfundactivity_tran.activity_name AS ActivityName "

            If intStatusID > 0 Then
                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                'Select Case intStatusID
                '    Case enBudgetTimesheetStatus.REJECTED\
                '        strQ &= "    ,ISNULL(RIGHT('00' + CAST(TSA.activity_hrs  / 60 AS NVARCHAR(MAX)), 2) + ':' + RIGHT('00' + CAST(TSA.activity_hrs % 60 AS NVARCHAR(MAX)), 2), '00:00') AS ActivityHours "

                '    Case enBudgetTimesheetStatus.PENDING, enBudgetTimesheetStatus.DELETED
                '        strQ &= "    ,ISNULL(RIGHT('00' + CAST(ltbemployee_timesheet.activity_hrs  /60 AS NvarCHAR(max)),2) + ':' + RIGHT('00' + CAST(ltbemployee_timesheet.activity_hrs % 60 AS NVARCHAR(max)),2),'00:00') AS ActivityHours "

                '    Case enBudgetTimesheetStatus.APPROVED
                '        strQ &= "    ,ISNULL(RIGHT('00' + CAST(ltbemployee_timesheet.approvedactivity_hrs /60 AS NvarCHAR(max)),2) + ':' + RIGHT('00' + CAST(ltbemployee_timesheet.approvedactivity_hrs % 60 AS NVARCHAR(max)),2),'00:00') AS ActivityHours "

                '    Case enBudgetTimesheetStatus.CANCELLED
                '        strQ &= "    ,ISNULL(RIGHT('00' + CAST(AT.approvedactivity_hrs /60 AS NvarCHAR(max)),2) + ':' + RIGHT('00' + CAST(AT.approvedactivity_hrs % 60 AS NVARCHAR(max)),2),'00:00') AS ActivityHours "
                'End Select
                Select Case intStatusID
                    Case enBudgetTimesheetStatus.REJECTED
                        strQ &= "    ,ISNULL(RIGHT('00' + CAST(CAST(TSA.activity_hrs AS INT) / 60 AS NVARCHAR(MAX)), 2) + ':' + RIGHT('00' + CAST(CAST(TSA.activity_hrs AS DECIMAL) % 60 AS NVARCHAR(MAX)), 2), '00:00') AS ActivityHours "

                    Case enBudgetTimesheetStatus.PENDING, enBudgetTimesheetStatus.DELETED
                        strQ &= "    ,ISNULL(RIGHT('00' + CAST(CAST(ltbemployee_timesheet.activity_hrs AS INT) /60 AS NvarCHAR(max)),2) + ':' + RIGHT('00' + CAST(CAST(ltbemployee_timesheet.activity_hrs AS DECIMAL) % 60 AS NVARCHAR(max)),2),'00:00') AS ActivityHours "

                    Case enBudgetTimesheetStatus.APPROVED
                        strQ &= "    ,ISNULL(RIGHT('00' + CAST(CAST(ltbemployee_timesheet.approvedactivity_hrs AS INT)/60 AS NvarCHAR(max)),2) + ':' + RIGHT('00' + CAST(CAST(ltbemployee_timesheet.approvedactivity_hrs AS DECIMAL) % 60 AS NVARCHAR(max)),2),'00:00') AS ActivityHours "

                    Case enBudgetTimesheetStatus.CANCELLED
                        strQ &= "    ,ISNULL(RIGHT('00' + CAST(CAST(AT.approvedactivity_hrs AS INT)/60 AS NvarCHAR(max)),2) + ':' + RIGHT('00' + CAST(CAST(AT.approvedactivity_hrs AS DECIMAL) % 60 AS NVARCHAR(max)),2),'00:00') AS ActivityHours "
                End Select
                'Pinkal (28-Mar-2018) -- End
            Else
                'Pinkal (28-Mar-2018) -- Start
                'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                'strQ &= "    ,ISNULL(RIGHT('00' + CAST(ltbemployee_timesheet.approvedactivity_hrs/60 AS NvarCHAR(max)),2) + ':' + RIGHT('00' + CAST(ltbemployee_timesheet.approvedactivity_hrs% 60 AS NVARCHAR(max)),2),'00:00') AS ActivityHours "
                strQ &= "    ,ISNULL(RIGHT('00' + CAST(CAST(ltbemployee_timesheet.approvedactivity_hrs AS INT)/60 AS NvarCHAR(max)),2) + ':' + RIGHT('00' + CAST(CAST(ltbemployee_timesheet.approvedactivity_hrs AS DECIMAL) % 60 AS NVARCHAR(max)),2),'00:00') AS ActivityHours "
                'Pinkal (28-Mar-2018) -- End
            End If

            strQ &= " FROM ltbemployee_timesheet " & _
                    "    LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = ltbemployee_timesheet.activityunkid "

            If intStatusID = enBudgetTimesheetStatus.CANCELLED Then
                strQ &= "JOIN " & _
                        "( " & _
                            " SELECT " & _
                                "  atltbemployee_timesheet.emptimesheetunkid " & _
                                ", atltbemployee_timesheet.approvedactivity_hrs " & _
                                ", ROW_NUMBER() OVER(PARTITION BY atltbemployee_timesheet.emptimesheetunkid ORDER BY atltbemployee_timesheet.auditdatetime DESC) AS RNO " & _
                            " FROM atltbemployee_timesheet " & _
                            " WHERE atltbemployee_timesheet.approvedactivity_hrs > 0 " & _
                                "  AND atltbemployee_timesheet.statusunkid=1 " & _
                                "  AND atltbemployee_timesheet.audittype=2 " & _
                                "  AND atltbemployee_timesheet.emptimesheetunkid IN(" & strEmpTimesheetIDs & ") " & _
                        ") AS AT ON AT.emptimesheetunkid = ltbemployee_timesheet.emptimesheetunkid and AT.RNO=1 "

            ElseIf intStatusID = enBudgetTimesheetStatus.REJECTED Then
                strQ &= "JOIN " & _
                            "( " & _
                                " SELECT " & _
                                    "  tsemptimesheet_approval.emptimesheetunkid " & _
                                    ", tsemptimesheet_approval.activity_hrs " & _
                                    ", ROW_NUMBER() OVER(PARTITION BY tsemptimesheet_approval.emptimesheetunkid ORDER BY tsemptimesheet_approval.statusunkid) AS R_NO " & _
                                " FROM tsemptimesheet_approval " & _
                                    " LEFT JOIN tsapprover_master ON tsapprover_master.tsapproverunkid = tsemptimesheet_approval.tsapproverunkid " & _
                                    " LEFT JOIN tsapproverlevel_master ON tsapproverlevel_master.tslevelunkid = tsapprover_master.tslevelunkid " & _
                                " WHERE tsemptimesheet_approval.isvoid=1 " & _
                                    " AND tsapprover_master.isactive=1 " & _
                                    " AND tsapproverlevel_master.isactive=1 " & _
                                    " AND tsemptimesheet_approval.emptimesheetunkid IN(" & strEmpTimesheetIDs & ") " & _
                            ") AS TSA ON TSA.emptimesheetunkid = ltbemployee_timesheet.emptimesheetunkid and TSA.R_NO=1 "
            End If

            strQ &= " WHERE bgfundactivity_tran.isvoid=0 " & _
                    "    AND ltbemployee_timesheet.employeeunkid=@employeeunkid " & _
                    "    AND ltbemployee_timesheet.periodunkid=@periodunkid "

            If blnIsVoid = True Then
                strQ &= " AND ltbemployee_timesheet.isvoid=1 "
            Else
                strQ &= " AND ltbemployee_timesheet.isvoid=0 "
            End If

            If intStatusID > 0 Then

                strQ &= " AND ltbemployee_timesheet.statusunkid=@statusunkid "

                Select Case intStatusID
                    Case enBudgetTimesheetStatus.REJECTED, enBudgetTimesheetStatus.DELETED, enBudgetTimesheetStatus.CANCELLED
                        objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, enBudgetTimesheetStatus.PENDING)
                    Case Else
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID.ToString)
                End Select
            End If

            If strEmpTimesheetIDs.Trim.Length > 0 Then
                strQ &= " AND ltbemployee_timesheet.emptimesheetunkid IN(" & strEmpTimesheetIDs & ")  "
            End If

            strQ &= ") AS A " & _
                    "PIVOT " & _
                    "( " & _
                    "    MAX(ActivityHours) " & _
                    "    FOR ActivityName IN(" & strActivityCols & ") " & _
                    ") AS P "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpCountListByActivity; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function GetActivityNamesByApproverID(ByVal intApproverID As Integer, _
                                                 ByVal intPeriodID As Integer, _
                                                 Optional ByVal blnIsForPivot As Boolean = False) As String
        objDataOperation = New clsDataOperation
        Dim exForce As Exception
        Dim strQ As String = String.Empty
        Dim strActivityNames As String = String.Empty
        Dim dsList As DataSet = Nothing
        Try

            strQ &= " SELECT (" & _
                    " stuff((SELECT DISTINCT "

            If blnIsForPivot = True Then
                strQ &= " ',[' + bgfundactivity_tran.activity_name + ']' "
            Else
                strQ &= " ',' + bgfundactivity_tran.activity_name "
            End If

            strQ &= " FROM tsemptimesheet_approval " & _
                            " JOIN ltbemployee_timesheet ON tsemptimesheet_approval.emptimesheetunkid = ltbemployee_timesheet.emptimesheetunkid " & _
                            "   AND ltbemployee_timesheet.isvoid = 0 " & _
                            " LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = tsemptimesheet_approval.activityunkid " & _
                    " WHERE tsemptimesheet_approval.isvoid = 0 And bgfundactivity_tran.isvoid = 0 " & _
                            " AND tsapproverunkid=@tsapproverunkid AND tsemptimesheet_approval.visibleunkid=2 " & _
                            " AND tsemptimesheet_approval.periodunkid=@periodunkid " & _
                    " FOR XML PATH('')),1,1,'') " & _
                    ") "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverID.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")
            strActivityNames = dsList.Tables("List").Rows(0).Item(0).ToString

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetActivityNamesByApproverID; Module Name: " & mstrModuleName)
        End Try
        Return strActivityNames
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function GetEmpCountListByActivity(ByVal intApproverID As Integer, ByVal intPeriodID As Integer) As DataSet

        Dim exForce As Exception
        Dim strQ As String = String.Empty
        Dim strQCols As String = String.Empty
        Dim dsList As DataSet = Nothing

        objDataOperation = New clsDataOperation

        Try
            Dim strActivityCols As String = String.Empty

            strActivityCols = GetActivityNamesByApproverID(intApproverID, intPeriodID, True)

            If strActivityCols.Trim.Length <= 0 Then Return Nothing

            strQ = "SELECT * " & _
                   "FROM " & _
                   "( " & _
                   " SELECT " & _
                   "     ltbemployee_timesheet.activitydate AS ActivityDate " & _
                   "    ,bgfundactivity_tran.activity_name AS ActivityName " & _
                   "    ,tsemptimesheet_approval.employeeunkid AS employeeunkid " & _
                   " FROM tsemptimesheet_approval " & _
                   "    JOIN ltbemployee_timesheet ON tsemptimesheet_approval.emptimesheetunkid = ltbemployee_timesheet.emptimesheetunkid " & _
                   "        AND ltbemployee_timesheet.isvoid = 0 " & _
                   "    LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = tsemptimesheet_approval.activityunkid " & _
                   " WHERE tsemptimesheet_approval.isvoid = 0 And bgfundactivity_tran.isvoid = 0 " & _
                   "    AND tsapproverunkid=@tsapproverunkid AND tsemptimesheet_approval.visibleunkid=2 " & _
                   "    AND tsemptimesheet_approval.periodunkid=@periodunkid " & _
                   ") AS A " & _
                   "PIVOT " & _
                   "( " & _
                   "    count(employeeunkid) " & _
                   "    FOR ActivityName IN(" & strActivityCols & ") " & _
                   ") AS P "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@tsapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverID.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim strExpression As String = String.Empty

            If dsList.Tables("List").Rows.Count > 0 Then
                For Each dCol As DataColumn In dsList.Tables("List").Columns
                    If dCol.ColumnName = "ActivityDate" Then Continue For
                    strExpression &= " + [" & dCol.ToString & "] "
                Next
                If strExpression.Trim <> "" Then
                    dsList.Tables("List").Columns.Add("colhTotal", System.Type.GetType("System.Decimal"), strExpression.Substring(2)).DefaultValue = 0
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpCountListByActivity; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function GetEmployeeTimesheetList(ByVal intPeriodID As Integer, _
                                             Optional ByVal intEmpTimesheetID As Integer = -1, _
                                             Optional ByVal intEmployeeID As Integer = -1, _
                                             Optional ByVal intStatusID As Integer = -1, _
                                             Optional ByVal dtActivityDate As Date = Nothing, _
                                             Optional ByVal strFilter As String = "", _
                                             Optional ByVal blnIsVoid As Boolean = False) As DataSet

        objDataOperation = New clsDataOperation
        Dim exForce As Exception
        Dim strQ As String = String.Empty
        Dim dsList As DataSet = Nothing
        Try
            objDataOperation.ClearParameters()

            strQ = "SELECT " & _
                        "  ltbemployee_timesheet.emptimesheetunkid " & _
                        " ,hremployee_master.employeeunkid " & _
                        " ,ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
                        " ,ISNULL(hremployee_master.firstname,'') + ' - ' + ISNULL(hremployee_master.othername,'') + ' - ' + ISNULL(hremployee_master.surname,'') AS EmployeeName " & _
                        " ,ISNULL(hremployee_master.employeecode,'') + ' - ' + ISNULL(hremployee_master.firstname,'') + ' - ' + ISNULL(hremployee_master.othername,'') + ' - ' + ISNULL(hremployee_master.surname,'') AS Employee " & _
                        " ,ltbemployee_timesheet.periodunkid " & _
                        " ,ISNULL(cfcommon_period_tran.period_name,'') AS period_name " & _
                        " ,ltbemployee_timesheet.activitydate " & _
                        " ,ISNULL(bgfundactivity_tran.activity_name,'') AS activity_name " & _
                        " ,ltbemployee_timesheet.activity_hrs " & _
                        " ,CAST(ltbemployee_timesheet.activity_hrs/60 AS NvarCHAR(max)) + ':' + CAST(ltbemployee_timesheet.activity_hrs % 60 AS NVARCHAR(max)) AS ActivityHours " & _
                        " ,ltbemployee_timesheet.approvedactivity_hrs " & _
                        " ,CAST(ltbemployee_timesheet.approvedactivity_hrs/60 AS NvarCHAR(max)) + ':' + CAST(ltbemployee_timesheet.approvedactivity_hrs % 60 AS NVARCHAR(max)) AS ApprovedActivityHours " & _
                        " ,ISNULL(ltbemployee_timesheet.description,'') AS description " & _
                  " FROM ltbemployee_timesheet " & _
                        "  LEFT JOIN hremployee_master ON ltbemployee_timesheet.employeeunkid = hremployee_master.employeeunkid " & _
                        "  LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = ltbemployee_timesheet.periodunkid " & _
                        "  LEFT JOIN bgfundactivity_tran ON bgfundactivity_tran.fundactivityunkid = ltbemployee_timesheet.activityunkid " & _
                  " WHERE ltbemployee_timesheet.periodunkid=@periodunkid AND bgfundactivity_tran.isvoid=0 "

            If blnIsVoid = False Then
                strQ &= " AND ltbemployee_timesheet.isvoid=0 "
            ElseIf blnIsVoid = True Then
                strQ &= " AND ltbemployee_timesheet.isvoid=1 "
            End If

            If intEmpTimesheetID > 0 Then
                strQ &= " AND ltbemployee_timesheet.emptimesheetunkid=@emptimesheetunkid "
                objDataOperation.AddParameter("@emptimesheetunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpTimesheetID)
            End If

            If intEmployeeID > 0 Then
                strQ &= " AND ltbemployee_timesheet.employeeunkid=@employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
            End If

            If intStatusID > 0 Then
                strQ &= " AND ltbemployee_timesheet.statusunkid=@statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If

            If dtActivityDate <> Nothing Then
                strQ &= " AND ltbemployee_timesheet.activitydate=@activitydate "
                objDataOperation.AddParameter("@activitydate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtActivityDate))
            End If

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Pinkal (30-Sep-2023) -- Start
            ' (A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal.

            'If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
            '    Return dsList
            'Else
            '    Return Nothing
            'End If

                Return dsList

            'Pinkal (30-Sep-2023) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeTimesheetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOperation IsNot Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Sub Send_Notification_Employee(ByVal strDatabaseName As String, _
                                          ByVal intYearID As Integer, _
                                          ByVal intCompanyID As Integer, _
                                          ByVal dtEmployeeAsOnDate As Date, _
                                          ByVal blnIncludeInactiveEmp As Boolean, _
                                          ByVal strEmployeeIDs As String, _
                                          ByVal strEmpTimesheetIDs As String, _
                                          ByVal intPeriodID As Integer, _
                                          ByVal enTSStatus As enBudgetTimesheetStatus, _
                                          ByVal intLoginTypeId As Integer, _
                                          ByVal intLoginEmployeeId As Integer, _
                                          ByVal intUserId As Integer, _
                                          ByVal strArutiSelfServiceURL As String, _
                                          Optional ByVal strRemarks As String = "", _
                                          Optional ByVal strExportReportPath As String = "", _
                                          Optional ByVal strFileName As String = "", _
                                          Optional ByVal blnAddEmailInQueue As Boolean = False)

        Dim dsList As DataSet = Nothing
        Dim dtTable As DataTable = Nothing
        Try
            objDataOperation = New clsDataOperation
            Dim objMail As New clsSendMail
            Dim objEmp As New clsEmployee_Master
            Dim objPeriod As New clscommom_period_Tran
            Dim strSubject As String = String.Empty
            Dim strMessage As String = String.Empty
            Dim strLink As String = String.Empty
            Dim intStatusID As Integer = -1

            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

            strSubject &= Language.getMessage(mstrModuleName, 16, "Employee Timesheet Status Notification")

            objPeriod._Periodunkid(strDatabaseName) = intPeriodID

            Select Case enTSStatus
                Case enBudgetTimesheetStatus.APPROVED
                    intStatusID = enBudgetTimesheetStatus.APPROVED
                Case enBudgetTimesheetStatus.REJECTED
                    intStatusID = enBudgetTimesheetStatus.REJECTED
                Case enBudgetTimesheetStatus.CANCELLED
                    intStatusID = enBudgetTimesheetStatus.CANCELLED
                Case enBudgetTimesheetStatus.DELETED
                    intStatusID = enBudgetTimesheetStatus.DELETED
            End Select

            Dim arrayEmpID() As String = strEmployeeIDs.Split(",")

            If arrayEmpID.Length > 0 Then

                For i As Integer = 0 To arrayEmpID.Length - 1

                    objEmp._Employeeunkid(dtEmployeeAsOnDate) = CInt(arrayEmpID(i))
            If objEmp._Email = "" Then Exit Sub

            strMessage = "<HTML><BODY>"
            strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & objEmp._Firstname & " " & objEmp._Surname & ", <BR></BR><BR></BR>"

                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language
                    'strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                    'Gajanan [27-Mar-2019] -- End
                    strMessage &= Language.getMessage(mstrModuleName, 17, "This is to inform you that the Timesheet that was submitted for period") & " " & _
                                  objPeriod._Period_Name & " " & Language.getMessage(mstrModuleName, 18, "has been") & " " 'Nilay (21 Mar 2017)

            Select Case enTSStatus
                Case enBudgetTimesheetStatus.APPROVED
                    strMessage &= Language.getMessage(mstrModuleName, 19, "Approved")
                Case enBudgetTimesheetStatus.REJECTED
                    strMessage &= Language.getMessage(mstrModuleName, 20, "Rejected")
                Case enBudgetTimesheetStatus.DELETED
                    strMessage &= Language.getMessage(mstrModuleName, 26, "Deleted")
                Case enBudgetTimesheetStatus.CANCELLED
                    strMessage &= Language.getMessage(mstrModuleName, 24, "Cancelled")
            End Select

            strMessage &= "." & "<BR></BR><BR></BR>"

                    If enTSStatus <> enBudgetTimesheetStatus.APPROVED Then
            strMessage &= Language.getMessage(mstrModuleName, 7, "Please refer the following Activity Details.") & "<BR></BR><BR></BR>"
                    End If

            Select Case enTSStatus

                        Case enBudgetTimesheetStatus.APPROVED

                            If strEmpTimesheetIDs.Trim.Length <= 0 Then Exit Sub

                            strMessage &= "<BR></BR>Please refer to the attached Timesheet Report<BR></BR>"

                        Case enBudgetTimesheetStatus.REJECTED, enBudgetTimesheetStatus.DELETED, enBudgetTimesheetStatus.CANCELLED

                    If strEmpTimesheetIDs.Trim.Length <= 0 Then Exit Sub

                            If enTSStatus = enBudgetTimesheetStatus.REJECTED Then
                                dsList = GetActivityListByEmployee(CInt(arrayEmpID(i)), intPeriodID, strEmpTimesheetIDs, enBudgetTimesheetStatus.REJECTED, False)

                            ElseIf enTSStatus = enBudgetTimesheetStatus.DELETED Then
                                dsList = GetActivityListByEmployee(CInt(arrayEmpID(i)), intPeriodID, strEmpTimesheetIDs, enBudgetTimesheetStatus.DELETED, True)

                            ElseIf enTSStatus = enBudgetTimesheetStatus.CANCELLED Then
                                dsList = GetActivityListByEmployee(CInt(arrayEmpID(i)), intPeriodID, strEmpTimesheetIDs, enBudgetTimesheetStatus.CANCELLED, False)

                            End If

                    If dsList Is Nothing OrElse dsList.Tables(0).Rows.Count <= 0 Then Exit Sub

                    If dsList.Tables(0).Rows.Count > 0 Then

                        strMessage &= "<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='100%'>"

                                Dim strActivityNames As String = String.Empty

                                If enTSStatus = enBudgetTimesheetStatus.REJECTED Then
                                    strActivityNames = GetActivityNamesByEmployeeID(CInt(arrayEmpID(i)), intPeriodID, strEmpTimesheetIDs, enBudgetTimesheetStatus.REJECTED, False)

                                ElseIf enTSStatus = enBudgetTimesheetStatus.DELETED Then
                                    strActivityNames = GetActivityNamesByEmployeeID(CInt(arrayEmpID(i)), intPeriodID, strEmpTimesheetIDs, enBudgetTimesheetStatus.DELETED, False, True)

                                ElseIf enTSStatus = enBudgetTimesheetStatus.CANCELLED Then
                                    strActivityNames = GetActivityNamesByEmployeeID(CInt(arrayEmpID(i)), intPeriodID, strEmpTimesheetIDs, enBudgetTimesheetStatus.CANCELLED, False)

                                End If


                        If strActivityNames = "" Then Exit Sub

                        Dim arr() As String = strActivityNames.Split(",")
                        Dim intWidth As Integer = 0
                        If arr.Length > 0 Then
                            intWidth = 80 / arr.Length
                        End If

                        strMessage &= "<TR WIDTH='100%'>"
                        strMessage &= "<TD WIDTH='20%' ALIGN='LEFT' ROWSPAN='2' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 10, "Activity Date") & "</B></FONT></TD>"
                        strMessage &= "<TD WIDTH='80%' ALIGN='CENTER' COLSPAN='" & arr.Length & "' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 12, "Activity Hours") & "</B></FONT></TD>"
                        strMessage &= "</TR>"

                        strMessage &= "<TR WIDTH='100%'>"
                        For j As Integer = 0 To arr.Length - 1
                            strMessage &= "<TD WIDTH='" & intWidth & "%' ALIGN='RIGHT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & CStr(arr(j)) & "</B></FONT></TD>"
                        Next
                        strMessage &= "</TR>"

                        For Each row As DataRow In dsList.Tables(0).Rows
                            strMessage &= "<TR WIDTH='100%'>"
                            strMessage &= "<TD WIDTH='20%' ALIGN='LEFT'><FONT SIZE=2>" & CDate(row.Item("ActivityDate")).Date & "</FONT></TD>"
                            For Each col As DataColumn In dsList.Tables(0).Columns
                                If col.ColumnName.ToUpper = "ACTIVITYDATE" Then Continue For
                                        strMessage &= "<TD WIDTH='" & intWidth & "%' ALIGN='RIGHT' VALIGN = 'TOP'><FONT SIZE=2>" & IIf(IsDBNull(row.Item(col.ColumnName)) = True, "&nbsp;", row.Item(col.ColumnName)) & "</FONT></TD>"
                            Next
                            strMessage &= "</TR>"
                        Next
                        strMessage &= "</TABLE>"

                            strMessage &= "<BR></BR><BR></BR>" & Language.getMessage(mstrModuleName, 21, "Please refer the below Remarks/Comments:") & "<BR></BR><BR></BR>" & _
                                          ChrW(34) & strRemarks & ChrW(34)
                        End If

            End Select


                    'Gajanan [27-Mar-2019] -- Start
                    'Enhancement - Change Email Language

                    'strMessage &= "<BR></BR><BR></BR><BR></BR><CENTER>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</CENTER>"
                    strMessage &= "<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>"

                    'Gajanan [27-Mar-2019] -- End
            strMessage &= "</BODY></HTML>"

            Dim objUser As New clsUserAddEdit
            objUser._Userunkid = IIf(intUserId <= 0, User._Object._Userunkid, intUserId)

            Dim objEmailColl As New clsEmailCollection(CStr(objEmp._Email), strSubject, strMessage, mstrWebFormName, _
                                                       intLoginEmployeeId, mstrWebClientIP, mstrWebHostName, _
                                                       IIf(intUserId <= 0, User._Object._Userunkid, intUserId), intLoginTypeId, _
                                                       clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, _
                                                               IIf(objUser._Email = "", objUser._Firstname & " " & objUser._Lastname, objUser._Email), _
                                                               strFileName, strExportReportPath)

            gobjEmailList.Add(objEmailColl)
            objUser = Nothing

                Next
            End If

            If blnAddEmailInQueue = False Then
            If gobjEmailList.Count > 0 Then
                If HttpContext.Current Is Nothing Then
                    objThread = New Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objThread.Start()
                        Dim arr(1) As Object
                        arr(0) = intCompanyID
                        objThread.Start(arr)
                        'Sohail (30 Nov 2017) -- End
                Else
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'Call Send_Notification()
                        Call Send_Notification(intCompanyID)
                        'Sohail (30 Nov 2017) -- End
                End If
            End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_Notification_Employee; Module Name: " & mstrModuleName)
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Sub Send_Notification_Approver(ByVal strDatabaseName As String, _
                                          ByVal intYearID As Integer, _
                                          ByVal intCompanyID As Integer, _
                                          ByVal dtEmployeeAsOnDate As Date, _
                                          ByVal blnIncludeInactiveEmp As Boolean, _
                                          ByVal strEmployeeID As String, _
                                          ByVal intPeriodID As Integer, _
                                          ByVal intCurrentPriority As Integer, _
                                          ByVal dtSelectedData As DataTable, _
                                          ByVal intLoginTypeId As Integer, _
                                          ByVal intLoginEmployeeId As Integer, _
                                          ByVal intUserId As Integer, _
                                          ByVal strArutiSelfServiceURL As String, _
                                          Optional ByVal blnAddEmailInQueue As Boolean = False)
        'Nilay (01 Apr 2017) -- [blnAddEmailInQueue]

        Try
            Dim objMail As New clsSendMail
            Dim intMinPriority As Integer = -1
            Dim strSubject As String = String.Empty
            Dim strMessage As String = String.Empty
            Dim strLink As String = String.Empty
            Dim objApprover As New clstsapprover_master
            Dim dtApproverList As DataTable = Nothing
            Dim dicEmployeeGroup As New Dictionary(Of String, Integer)
            Dim dicTsApprIDs As New Dictionary(Of Integer, Integer)

            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

                    Dim objPeriod As New clscommom_period_Tran
                    objPeriod._Periodunkid(strDatabaseName) = intPeriodID

            dtSelectedData = New DataView(dtSelectedData, "IsGrp=0", "employeeunkid,ADate", DataViewRowState.CurrentRows).ToTable

                    dtApproverList = objApprover.GetEmployeeApprover(strDatabaseName, intYearID, intCompanyID, dtEmployeeAsOnDate, blnIncludeInactiveEmp, _
                                                                     -1, strEmployeeID, , Nothing, , "priority > " & intCurrentPriority & " ")

            strSubject &= Language.getMessage(mstrModuleName, 4, "Notification for Approving Employee Timesheet") 'Nilay (27 Feb 2017)

                    Dim intTSApproverID As Integer = -1
            Dim arrayEmpID() As String = strEmployeeID.Split(",")

            If arrayEmpID.Length > 0 Then
                For i As Integer = 0 To arrayEmpID.Length - 1
                    Dim dtTable As DataTable = New DataView(dtApproverList, "employeeunkid=" & CInt(arrayEmpID(i)) & "", "", DataViewRowState.CurrentRows).ToTable
                            If dtTable.Rows.Count > 0 Then
                                Dim dRow() As DataRow = dtTable.Select("priority=MIN(priority)")

                                If dRow.Length > 0 Then

                            'Pinkal (02-Sep-2022) -- Start
                            'Enhancement Baylor Malawi : Baylor - Put Setting on confirguration for timesheet approval notification to include holiday and weekend entries.
                            Dim objConfig As New clsConfigOptions
                            Dim mblnIncludeWeekendHolidaysBudgetTimesheetApprovalNotification As Boolean = CBool(objConfig.GetKeyValue(intCompanyID, "IncludeWeekendHolidaysBudgetTimesheetApprovalNotification", Nothing))
                            Dim mblnNotAllowIncompleteTimesheet As Boolean = CBool(objConfig.GetKeyValue(intCompanyID, "NotAllowIncompleteTimesheet", Nothing))
                            objConfig = Nothing

                            If mblnNotAllowIncompleteTimesheet AndAlso mblnIncludeWeekendHolidaysBudgetTimesheetApprovalNotification Then
                                Dim objMaster As New clsMasterData


                                'Pinkal (22-Dec-2022) -- Start
                                '(A1X-348) Baylor Malawi - As a timesheet approver, I want the budget timesheet approval notification to include leave days taken.
                                'Dim dsEmpDays As DataSet = objMaster.getEmpHolidayLeaveDayoffWeekendsList(CInt(arrayEmpID(i)), objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, True, False, False, True, False)
                                Dim dsEmpDays As DataSet = objMaster.getEmpHolidayLeaveDayoffWeekendsList(CInt(arrayEmpID(i)), objPeriod._TnA_StartDate.Date, objPeriod._TnA_EndDate.Date, True, True, False, True, False)
                                'Pinkal (22-Dec-2022) -- End

                                If dsEmpDays IsNot Nothing AndAlso dsEmpDays.Tables(0).Rows.Count > 0 Then
                                    For Each dr As DataRow In dsEmpDays.Tables(0).Rows
                                        Dim LstDays = dtSelectedData.AsEnumerable().Where(Function(x) x.Field(Of Integer)("employeeunkid") = CInt(arrayEmpID(i)) AndAlso x.Field(Of String)("ADate") = dr("ddate").ToString())
                                        If LstDays IsNot Nothing AndAlso LstDays.Count <= 0 Then
                                            Dim drRow As DataRow = dtSelectedData.NewRow
                                            drRow("employeeunkid") = CInt(arrayEmpID(i))
                                            drRow("ADate") = dr("ddate").ToString()
                                            drRow("IsGrp") = False
                                            drRow("periodunkid") = intPeriodID
                                            drRow("Period") = objPeriod._Period_Name
                                            drRow("activitydate") = eZeeDate.convertDate(dr("ddate").ToString()).Date
                                            drRow("fundname") = ""
                                            drRow("fundprojectname") = ""
                                            drRow("activity_name") = ""
                                            drRow("activity_hrs") = "00:00"
                                            If dtSelectedData.Columns.Contains("statusunkid") Then
                                                drRow("statusunkid") = enBudgetTimesheetStatus.PENDING
                                            Else
                                                drRow("EmptimesheetStatusId") = enBudgetTimesheetStatus.PENDING
                                            End If
                                            drRow("emptimesheetunkid") = -1
                                            drRow("IsChecked") = True


                                            If dtSelectedData.Columns.Contains("isholiday") Then
                                                drRow("isholiday") = False
                                            End If

                                            If dtSelectedData.Columns.Contains("isLeave") Then
                                                drRow("isLeave") = False
                                            End If

                                            If dtSelectedData.Columns.Contains("isDayOff") Then
                                                drRow("isDayOff") = False
                                            End If

                                            If CBool(dr("Isholiday")) Then
                                                Dim objEmpHoliday As New clsemployee_holiday
                                                Dim dtEmpHoliday As DataTable = objEmpHoliday.GetEmployeeHoliday(CInt(arrayEmpID(i)), eZeeDate.convertDate(dr("ddate").ToString()).Date, Nothing)
                                                If dtEmpHoliday IsNot Nothing AndAlso dtEmpHoliday.Rows.Count > 0 Then
                                                    drRow("description") = dtEmpHoliday.Rows(0)("holidayname").ToString()
                                                    drRow("isholiday") = True
                                                End If
                                                dtEmpHoliday.Clear()
                                                dtEmpHoliday = Nothing
                                                objEmpHoliday = Nothing
                                            Else
                                                drRow("description") = Language.getMessage(mstrModuleName, 33, "Weekend")
                                                drRow("isholiday") = True
                                            End If

                                            'Pinkal (22-Dec-2022) -- Start
                                            '(A1X-348) Baylor Malawi - As a timesheet approver, I want the budget timesheet approval notification to include leave days taken.
                                            If CBool(dr("isLeave")) Then
                                                Dim objLeaveIssue As New clsleaveissue_Tran
                                                Dim dsLeaveList As DataSet = objLeaveIssue.GetList("Leave", True, -1, CInt(arrayEmpID(i)), eZeeDate.convertDate(dr("ddate").ToString()).Date, eZeeDate.convertDate(dr("ddate").ToString()).Date)
                                                If dsLeaveList IsNot Nothing AndAlso dsLeaveList.Tables(0).Rows.Count > 0 Then
                                                    drRow("description") = dsLeaveList.Tables(0).Rows(0)("leavename").ToString()
                                                    drRow("isLeave") = True
                                                End If
                                                If dsLeaveList IsNot Nothing Then dsLeaveList.Clear()
                                                dsLeaveList = Nothing
                                                objLeaveIssue = Nothing
                                            End If
                                            'Pinkal (22-Dec-2022) -- End

                                            dtSelectedData.Rows.Add(drRow)
                                        End If
                                    Next
                                    dtSelectedData = New DataView(dtSelectedData, "IsGrp=0", "employeeunkid,ADate", DataViewRowState.CurrentRows).ToTable
                                End If

                                objMaster = Nothing
                            End If
                            'Pinkal (02-Sep-2022) -- End

                            For Each dtRow In dRow

                                If dicTsApprIDs.Values.Contains(CInt(dtRow.Item("tsapproverunkid"))) = False Then
                                    dicTsApprIDs.Add(CInt(dtRow.Item("employeeapproverunkid")), CInt(dtRow.Item("tsapproverunkid")))

                                    If CStr(dtRow.Item("approveremail")) = "" Then Continue For

                                        strMessage = "<HTML><BODY>"
                                    strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " " & dtRow.Item("employeename").ToString() & ", <BR><BR>"
                                        strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                                    strMessage &= Language.getMessage(mstrModuleName, 6, "This is the notification for approving the Employee Timesheet application for period") & _
                                                  " " & objPeriod._Period_Name.ToString & "." & "<BR></BR><BR></BR>" 'Nilay (27 Feb 2017)


                                    strLink = strArutiSelfServiceURL & "/Budget_Timesheet/wPg_EmployeeTimeSheetApprovalList.aspx?" & _
                                              HttpUtility.UrlEncode(clsCrypto.Encrypt(intPeriodID.ToString & "|" & dtRow.Item("tsapproverunkid").ToString & _
                                                                                      "|" & intCompanyID.ToString & "|" & dtRow.Item("MappedUserID").ToString))

                                    strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & _
                                                  Language.getMessage(mstrModuleName, 32, "Please click on the following link to approve.") & _
                                                  "<BR></BR><BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>" & "<BR></BR><BR></BR>" 'Nilay (27 Feb 2017)

                                    strMessage &= Language.getMessage(mstrModuleName, 7, "Please refer the following Activity Details.") & "<BR></BR><BR></BR>"

                                        strMessage &= "<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=3 WIDTH='100%'>"

                                        strMessage &= "<TR WIDTH='100%'>"
                                    strMessage &= "<TD WIDTH='10%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 10, "Activity Date") & "</B></FONT></TD>"
                                    strMessage &= "<TD WIDTH='18%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 31, "Donor/Grant") & "</B></FONT></TD>"
                                    strMessage &= "<TD WIDTH='18%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 28, "Project Code") & "</B></FONT></TD>"
                                    strMessage &= "<TD WIDTH='18%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 29, "Activity Code") & "</B></FONT></TD>"
                                    strMessage &= "<TD WIDTH='10%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 12, "Activity Hours") & "</B></FONT></TD>"
                                    strMessage &= "<TD WIDTH='26%' ALIGN='LEFT' BGCOLOR='#4682b4'><FONT COLOR='WHITE' SIZE=2><B>" & Language.getMessage(mstrModuleName, 30, "Description") & "</B></FONT></TD>"
                                        strMessage &= "</TR>"

                                    For Each row As DataRow In dtSelectedData.Rows

                                        'Nilay (01 Apr 2017) -- Start
                                        'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                                        _Emptimesheetunkid = CInt(row.Item("emptimesheetunkid"))
                                        If mintStatusunkid = enBudgetTimesheetStatus.APPROVED Then Continue For
                                        'Nilay (01 Apr 2017) -- End

                                        If dicEmployeeGroup.Keys.Contains(CStr(CInt(dtRow.Item("tsapproverunkid")) & "|" & CInt(row.Item("employeeunkid")))) = False Then
                                            dicEmployeeGroup.Add(CStr(CInt(dtRow.Item("tsapproverunkid")) & "|" & CInt(row.Item("employeeunkid"))), CInt(row.Item("employeeunkid")))
                                        strMessage &= "<TR WIDTH='100%'>"
                                            strMessage &= "<TD WIDTH='100%' COLSPAN='6' ALIGN='LEFT' BGCOLOR='#DDD'><FONT SIZE=2><FONT COLOR='BLACK' SIZE=2><B>" & row.Item("Employee").ToString & "</B></FONT></TD>"
                                        strMessage &= "</TR>"
                                        End If

                                            strMessage &= "<TR WIDTH='100%'>"
                                        strMessage &= "<TD WIDTH='12%' ALIGN='LEFT'><FONT SIZE=2>" & CDate(row.Item("activitydate")).Date & "</FONT></TD>"
                                        strMessage &= "<TD WIDTH='18%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("fundname").ToString & "</FONT></TD>"
                                        strMessage &= "<TD WIDTH='18%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("fundprojectname").ToString & "</FONT></TD>"
                                        strMessage &= "<TD WIDTH='18%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("activity_name").ToString & "</FONT></TD>"
                                        strMessage &= "<TD WIDTH='10%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("activity_hrs").ToString & "</FONT></TD>"
                                        strMessage &= "<TD WIDTH='24%' ALIGN='LEFT'><FONT SIZE=2>" & row.Item("description").ToString & "</FONT></TD>"
                                            strMessage &= "</TR>"
                                        Next

                                        strMessage &= "</TABLE>"
                                        strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                                        strMessage &= "</BODY></HTML>"


                                    'Pinkal (01-Sep-2021)-- Start
                                    'THPS Bug : Solving Bug to sending approver notification.

                                    'Dim objEmailColl As New clsEmailCollection(CStr(dtRow.Item("approveremail")), strSubject, strMessage, mstrWebFormName, _
                                    '                                               intLoginEmployeeId, mstrWebClientIP, mstrWebHostName, _
                                    '                                           intUserId, intLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, _
                                    '                                           IIf(dtRow.Item("approveremail").ToString = "", dtRow.Item("employeename").ToString, dtRow.Item("approveremail").ToString))

                                    'gobjEmailList.Add(objEmailColl)

                                    objMail._Subject = strSubject
                                    objMail._Message = strMessage
                                    objMail._ToEmail = CStr(dtRow.Item("approveremail"))
                                    objMail._SenderAddress = IIf(dtRow.Item("approveremail").ToString = "", dtRow.Item("employeename").ToString, dtRow.Item("approveremail").ToString)

                                    If intLoginTypeId <= 0 Then intLoginTypeId = enLogin_Mode.DESKTOP
                                    If mstrWebFormName.Trim.Length > 0 Then
                                        objMail._Form_Name = mstrWebFormName
                                        objMail._WebClientIP = mstrWebClientIP
                                        objMail._WebHostName = mstrWebHostName
                                    End If

                                    objMail._LogEmployeeUnkid = intLoginEmployeeId
                                    objMail._OperationModeId = intLoginTypeId
                                    objMail._UserUnkid = intUserId
                                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT
                                    objMail.SendMail(intCompanyID)

                                    'Pinkal (01-Sep-2021) -- End

                                    End If
                            Next
                                End If
                            End If
                        Next
                    End If

            'Pinkal (01-Sep-2021)-- Start
            'THPS Bug : Solving Bug to sending approver notification.

            ''Nilay (01 Apr 2017) -- Start
            ''ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
            'If blnAddEmailInQueue = False Then
            '    If gobjEmailList.Count > 0 Then
            '        If HttpContext.Current Is Nothing Then
            '            objThread = New Thread(AddressOf Send_Notification)
            '            objThread.IsBackground = True
            '            'Sohail (30 Nov 2017) -- Start
            '            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            '            'objThread.Start()
            '            Dim arr(1) As Object
            '            arr(0) = intCompanyID
            '            objThread.Start(arr)
            '            'Sohail (30 Nov 2017) -- End
            '        Else
            '            'Sohail (30 Nov 2017) -- Start
            '            'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
            '            'Call Send_Notification()
            '            Call Send_Notification(intCompanyID)
            '            'Sohail (30 Nov 2017) -- End
            '            6End If
            '        End If
            '    End If
            '    'Nilay (01 Apr 2017) -- End
            'Pinkal (01-Sep-2021) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_Notification_Approver; Module Name: " & mstrModuleName)
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Private Sub Send_Notification(ByVal intCompanyID As Object)
        'Sohail (30 Nov 2017) - [intCompanyID]
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail

                'Pinkal (23-Apr-2018) -- Start
                'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
SendEmail:
                'Pinkal (23-Apr-2018) -- End

                For Each objEmail In gobjEmailList
                    objSendMail._ToEmail = objEmail._EmailTo
                    objSendMail._Subject = objEmail._Subject
                    objSendMail._Message = objEmail._Message
                    objSendMail._Form_Name = objEmail._Form_Name
                    objSendMail._LogEmployeeUnkid = objEmail._LogEmployeeUnkid
                    objSendMail._OperationModeId = objEmail._OperationModeId
                    objSendMail._UserUnkid = objEmail._UserUnkid
                    objSendMail._SenderAddress = objEmail._SenderAddress
                    objSendMail._ModuleRefId = objEmail._ModuleRefId

                    'Nilay (01 Apr 2017) -- Start
                    'ISSUE #23: Enhancements: Budget Employee TimeSheet Changes
                    If objEmail._FileName.ToString.Trim.Length > 0 Then
                        objSendMail._AttachedFiles = objEmail._FileName
                    End If
                    'If objSendMail.SendMail().ToString.Length > 0 Then
                    '    gobjEmailList.Remove(objEmail)
                    '    Continue For
                    'End If
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'If objSendMail.SendMail(IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False), _
                    '                            objEmail._ExportReportPath).ToString.Length > 0 Then
                    'Sohail (13 Dec 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'If objSendMail.SendMail(CInt(intCompanyID), IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False), _
                    '                            objEmail._ExportReportPath).ToString.Length > 0 Then
                    Dim intCUnkId As Integer = 0
                    If TypeOf intCompanyID Is Integer Then
                        intCUnkId = intCompanyID
                    Else
                        intCUnkId = CInt(intCompanyID(0))
                    End If
                    If objSendMail.SendMail(intCUnkId, IIf(objEmail._FileName.ToString.Trim.Length > 0, True, False), _
                                                objEmail._ExportReportPath).ToString.Length > 0 Then
                        'Sohail (13 Dec 2017) -- End                    
                        'Sohail (30 Nov 2017) -- End                    
                        gobjEmailList.Remove(objEmail)
                        'Pinkal (23-Apr-2018) -- Start
                        'Enhancement - (RefNo: 198)  Working on Project Targeted/Acutal Hours Details.
                        'Continue For
                        GoTo SendEmail
                        'Pinkal (23-Apr-2018) -- End
                    End If
                    'Nilay (01 Apr 2017) -- End
                Next
                gobjEmailList.Clear()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Send_Notification; Module Name: " & mstrModuleName)
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Sub StartSendingEmail(ByVal intCompanyID As Integer)
        'Sohail (30 Nov 2017) - [intCompanyID]
        Try
            If gobjEmailList.Count > 0 Then
                If HttpContext.Current Is Nothing Then
                    objThread = New Thread(AddressOf Send_Notification)
                    objThread.IsBackground = True
                    objThread.Start()
                Else
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'Call Send_Notification()
                    Call Send_Notification(intCompanyID)
                    'Sohail (30 Nov 2017) -- End
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: StartSendingEmail; Module Name: " & mstrModuleName)
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function IsTimesheetCompleteForPeriod(ByVal dtTimesheetList As DataTable _
                                                                         , ByVal intTotalDaysInMonth As Integer _
                                                                         , ByVal dtStartDate As Date, ByVal dtEndDate As Date _
                                                                         , ByVal enTimesheetStatus As enBudgetTimesheetStatus _
                                                                         , ByVal mdtEmpAsonDate As Date _
                                                                         , ByVal mblnAllowOverTimeToEmpTimesheet As Boolean) As Boolean

        'Pinkal (28-Jul-2018) -- 'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251] [ByVal mblnAllowOverTimeToEmpTimesheet As Boolean]


        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim objMaster As New clsMasterData
        objDataOperation = New clsDataOperation
        Try
            Dim intDaysCount As Integer = 0
            Dim intWeekends As Integer = 0
            Dim intHLDWCount As Integer = 0
            Dim intEmpTSCount As Integer = 0
            Dim dtTimesheet As New DataTable

            Select Case enTimesheetStatus
                Case enBudgetTimesheetStatus.SUBMIT_FOR_APPROVAL, enBudgetTimesheetStatus.CANCELLED
                    dtTimesheet = New DataView(dtTimesheetList, "IsGrp=1", "", DataViewRowState.CurrentRows).ToTable

                Case enBudgetTimesheetStatus.APPROVED, enBudgetTimesheetStatus.REJECTED
                    dtTimesheet = New DataView(dtTimesheetList, "IsGrp=1", "", DataViewRowState.CurrentRows).ToTable(True, "employeeunkid")
            End Select

            Dim objemployee As New clsEmployee_Master
            For Each row As DataRow In dtTimesheet.Rows
                objemployee._Employeeunkid(mdtEmpAsonDate) = CInt(row.Item("employeeunkid"))
                Dim mdtDate As DateTime = Nothing

                If objemployee._Reinstatementdate <> Nothing Then
                    If objemployee._Appointeddate.Date < objemployee._Reinstatementdate.Date AndAlso (objemployee._Reinstatementdate.Date >= dtStartDate.Date AndAlso objemployee._Reinstatementdate.Date <= dtEndDate.Date) Then
                        mdtDate = objemployee._Reinstatementdate.Date
                    Else
                        mdtDate = dtStartDate.Date
                    End If
                ElseIf (objemployee._Appointeddate.Date >= dtStartDate.Date AndAlso objemployee._Appointeddate.Date <= dtEndDate.Date) Then
                    mdtDate = objemployee._Appointeddate.Date
                Else
                    mdtDate = dtStartDate.Date
                End If


                If objemployee._Termination_From_Date.Date <> Nothing OrElse objemployee._Empl_Enddate.Date <> Nothing Then
                    If objemployee._Termination_From_Date.Date <> Nothing AndAlso (objemployee._Termination_From_Date.Date >= dtStartDate.Date AndAlso objemployee._Termination_From_Date.Date <= dtEndDate.Date) Then
                        dtEndDate = objemployee._Termination_From_Date.Date
                    ElseIf objemployee._Empl_Enddate.Date <> Nothing AndAlso (objemployee._Empl_Enddate.Date >= dtStartDate.Date AndAlso objemployee._Empl_Enddate.Date <= dtEndDate.Date) Then
                        dtEndDate = objemployee._Empl_Enddate.Date
                    End If
                ElseIf (objemployee._Termination_To_Date.Date >= dtStartDate.Date AndAlso objemployee._Termination_To_Date.Date <= dtEndDate.Date) Then
                    dtEndDate = objemployee._Termination_To_Date.Date
                End If

                dtStartDate = mdtDate

                dsList = objMaster.getEmpHolidayLeaveDayoffWeekendsList(CInt(row.Item("employeeunkid")), dtStartDate, dtEndDate, , , , True, True)
                    intHLDWCount = dsList.Tables(0).Rows.Count

                If (DateDiff(DateInterval.Day, dtStartDate, dtEndDate) + 1) <> intTotalDaysInMonth Then
                    intTotalDaysInMonth = DateDiff(DateInterval.Day, dtStartDate, dtEndDate) + 1
                End If

                    intDaysCount = intTotalDaysInMonth - intHLDWCount

                Dim dtTable As DataTable = New DataView(dtTimesheetList, "ischecked = 1 AND IsGrp=0 AND employeeunkid=" & CInt(row.Item("employeeunkid")), "", _
                                                        DataViewRowState.CurrentRows).ToTable(True, "ADate", "employeeunkid")
                

                    intEmpTSCount = dtTable.Rows.Count

                    For Each dR As DataRow In dsList.Tables(0).Select("isweekend=1")
                    Dim dEmpRow As DataRow() = dtTable.Select("ADate='" & dR.Item("ddate").ToString & "'")
                        If dEmpRow.Length > 0 Then
                            intEmpTSCount = intEmpTSCount - 1
                        End If
                    Next


                'Pinkal (28-Jul-2018) -- Start
                'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]
                If mblnAllowOverTimeToEmpTimesheet Then
                    For Each dR As DataRow In dsList.Tables(0).Select("isholiday=1")
                        Dim dEmpRow As DataRow() = dtTable.Select("ADate='" & dR.Item("ddate").ToString & "'")
                        If dEmpRow.Length > 0 Then
                            intEmpTSCount = intEmpTSCount - 1
                        End If
                    Next
                End If
                'Pinkal (28-Jul-2018) -- End


                    If intDaysCount <> intEmpTSCount Then
                        Return False
                    End If
                Next

            objemployee = Nothing

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTimesheetCompleteForPeriod; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function IsAllTimesheetSubmittedForApproval(ByVal intPeriodID As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ &= "SELECT * FROM ltbemployee_timesheet WHERE isvoid=0 AND issubmit_approval=0 AND periodunkid=@periodunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsAllTimesheetSubmittedForApproval; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function IsPendingApprovalBudgetTimesheet(ByVal intPeriodID As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try

            'Pinkal (13 Jun 2017) -- 'ISSUE : AFK HAVING ISSUE THAT THEY DON'T ABLE TO CLOSE PAYROLL PERIOD. [AND issubmit_approval=1]

            strQ &= "SELECT * FROM ltbemployee_timesheet WHERE isvoid=0 AND statusunkid=2 AND periodunkid=@periodunkid AND issubmit_approval=1"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                Return False
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsAllTimesheetSubmittedForApproval; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function GetEmpApprovedTimesheetHrs(ByVal xStartDate As Date, ByVal xEndDate As Date) As DataTable
        Dim strQ As String = ""
        Dim dtApprovedHrs As DataTable = Nothing
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = " SELECT ltbemployee_timesheet.employeeunkid, (ISNULL(SUM(approvedactivity_hrs),0) * 60) ApprovedHrsinSec  " & _
                       " FROM ltbemployee_timesheet " & _
                       " WHERE isvoid = 0 AND statusunkid = 1 " & _
                       " AND CONVERT(char(8),activitydate,112) BETWEEN @StartDate AND @EndDate " & _
                       " Group By ltbemployee_timesheet.employeeunkid"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xStartDate))
            objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xEndDate))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                dtApprovedHrs = dsList.Tables(0)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmpApprovedTimesheetHrs; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return dtApprovedHrs
    End Function

    'Pinkal (16-May-2018) -- Start
    'Enhancement - Display Pending Budget timesheet Approval Employeee List Suggestion given by Suzan.

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function GetEmployeeFromPendingBudgetTimesheet(ByVal xPeriod As Integer, ByVal isSubmitForApproval As Boolean) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try

            strQ &= "SELECT DISTINCT ISNULL(ltbemployee_timesheet.employeeunkid,0) AS employeeunkid, ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
                        ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' ' + ISNULL(hremployee_master.surname,'') AS employeename " & _
                        " FROM ltbemployee_timesheet  " & _
                        " JOIN hremployee_master ON hremployee_master.employeeunkid = ltbemployee_timesheet.employeeunkid " & _
                        " WHERE ltbemployee_timesheet.isvoid=0 AND ltbemployee_timesheet.statusunkid=2  " & _
                        " AND ltbemployee_timesheet.periodunkid=@periodunkid AND ltbemployee_timesheet.issubmit_approval=@issubmit_approval"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xPeriod.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isSubmitForApproval)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeFromPendingBudgetTimesheet; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return dsList.Tables(0)
    End Function

    'Pinkal (16-May-2018) -- End


    'Pinkal (28-Jul-2018) -- Start
    'Enhancement - Doing for PACT Budget Timesheet Requirment [Ref #249,250,251]

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' 
    Public Function GetEmployeeExtraHours(ByVal iEmpId As Integer, ByVal istartDate As DateTime, ByVal iEndDate As DateTime) As DataTable
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try

            strQ &= " SELECT " & _
                        " ltbemployee_timesheet.employeeunkid " & _
                        ", ISNULL(hremployee_master.employeecode,'') AS Employeecode " & _
                        ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'') AS Employee " & _
                        ", CONVERT(Char(8),activitydate,112) AS activitydate " & _
                        ", SUM(activity_hrs) AS ActivityHrs " & _
                        ",  SUM(activity_hrs) * 60 AS ActivityHrsInSec " & _
                        ",  SUM(approvedactivity_hrs) AS ApprovedActivityHrs " & _
                        ", SUM(approvedactivity_hrs) * 60 AS ApprovedActivityHrsInSec " & _
                        ", 0 as Extrahrs " & _
                        ", 0 AS ShiftId " & _
                        ", 0 As ShiftHrsinSec " & _
                        " FROM ltbemployee_timesheet " & _
                        " LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = ltbemployee_timesheet.employeeunkid " & _
                        " WHERE ltbemployee_timesheet.employeeunkid = @employeeunkid AND CONVERT(Char(8),activitydate,112) >= @startdate AND CONVERT(Char(8),activitydate,112) <= @EndDate " & _
                        " AND ltbemployee_timesheet.isvoid = 0  AND statusunkid = 1 " & _
                        " GROUP by ltbemployee_timesheet.employeeunkid " & _
                        ", ISNULL(hremployee_master.employeecode,'') " & _
                        ", activitydate " & _
                        ", ISNULL(hremployee_master.firstname,'') + ' ' + ISNULL(hremployee_master.othername,'') + ' '+ ISNULL(hremployee_master.surname,'') "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpId)
            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(istartDate))
            objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(iEndDate))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                dsList.Tables(0).AsEnumerable().ToList().ForEach(Function(x) UpdateExtraHours(x))
                dsList.AcceptChanges()
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetEmployeeExtraHours; Module Name: " & mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return dsList.Tables(0)
    End Function

    Private Function UpdateExtraHours(ByVal iRow As DataRow) As Boolean
        Try
            If iRow IsNot Nothing Then

                Dim mblnHoliday As Boolean = False
                Dim mblnIsWeekend As Boolean = False

                Dim objEmpShiftTran As New clsEmployee_Shift_Tran
                iRow("ShiftId") = objEmpShiftTran.GetEmployee_Current_ShiftId(eZeeDate.convertDate(iRow("activitydate").ToString()).Date, CInt(iRow("employeeunkid")))
                objEmpShiftTran = Nothing

                Dim objShiftTran As New clsshift_tran
                objShiftTran.GetShiftTran(CInt(iRow("ShiftId")))
                Dim drRow() As DataRow = objShiftTran._dtShiftday.Select("dayid = " & GetWeekDayNumber(WeekdayName(Weekday(eZeeDate.convertDate(iRow("activitydate").ToString()).Date), False, FirstDayOfWeek.Sunday).ToString()))
                If drRow.Length > 0 Then
                    iRow("ShiftHrsinSec") = CInt(drRow(0)("workinghrsinsec"))
                    mblnIsWeekend = CBool(drRow(0)("isweekend"))
                End If
                objShiftTran = Nothing


                Dim objEmpHoliday As New clsemployee_holiday
                If objEmpHoliday.GetEmployeeHoliday(CInt(iRow("employeeunkid")), eZeeDate.convertDate(iRow("activitydate").ToString()).Date).Rows.Count > 0 Then
                    mblnHoliday = True
                End If
                objEmpHoliday = Nothing

                If CInt(iRow("ApprovedActivityHrsInSec")) > CInt(iRow("ShiftHrsinSec")) AndAlso mblnHoliday = False AndAlso mblnIsWeekend = False Then
                    iRow("Extrahrs") = CInt(iRow("ApprovedActivityHrsInSec")) - CInt(iRow("ShiftHrsinSec"))
                ElseIf mblnHoliday OrElse mblnIsWeekend Then
                    iRow("Extrahrs") = CInt(iRow("ApprovedActivityHrsInSec"))
                End If

            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateExtraHours; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (28-Jul-2018) -- End



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 110, "Approved")
            Language.setMessage("clsMasterData", 111, "Pending")
            Language.setMessage("clsMasterData", 112, "Rejected")
            Language.setMessage("clsMasterData", 115, "Cancelled")
            Language.setMessage(mstrModuleName, 1, "This Activity is already defined of this employee for same date. Please define different activity for this date.")
            Language.setMessage(mstrModuleName, 2, "WEB")
            Language.setMessage(mstrModuleName, 3, "Day Off")
            Language.setMessage(mstrModuleName, 4, "Notification for Approving Employee Timesheet")
            Language.setMessage(mstrModuleName, 5, "Dear")
            Language.setMessage(mstrModuleName, 6, "This is the notification for approving the Employee Timesheet application for period")
            Language.setMessage(mstrModuleName, 7, "Please refer the following Activity Details.")
            Language.setMessage(mstrModuleName, 10, "Activity Date")
            Language.setMessage(mstrModuleName, 12, "Activity Hours")
            Language.setMessage(mstrModuleName, 16, "Employee Timesheet Status Notification")
            Language.setMessage(mstrModuleName, 17, "This is to inform you that the Timesheet that was submitted for period")
            Language.setMessage(mstrModuleName, 18, "has been")
            Language.setMessage(mstrModuleName, 19, "Approved")
            Language.setMessage(mstrModuleName, 20, "Rejected")
            Language.setMessage(mstrModuleName, 21, "Please refer the below Remarks/Comments:")
            Language.setMessage(mstrModuleName, 24, "Cancelled")
            Language.setMessage(mstrModuleName, 26, "Deleted")
            Language.setMessage(mstrModuleName, 28, "Project Code")
            Language.setMessage(mstrModuleName, 29, "Activity Code")
            Language.setMessage(mstrModuleName, 30, "Description")
            Language.setMessage(mstrModuleName, 31, "Donor/Grant")
            Language.setMessage(mstrModuleName, 32, "Please click on the following link to approve.")
            Language.setMessage(mstrModuleName, 33, "Weekend")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
