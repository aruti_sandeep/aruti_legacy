﻿'************************************************************************************************************************************
'Class Name : clsJVPosting_Tran.vb
'Purpose    :
'Date       :24-06-2019
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsJVPosting_Tran
    Private Const mstrModuleName = "clsJVPosting_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private xDataOp As clsDataOperation
    Private mintJvpostingtranunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintStationunkid As Integer
    Private mstrBatchno As String = String.Empty
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Sohail (10 Apr 2020) -- Start
    'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
    Private mintAllocationbyid As Integer
    Private mintAllocationtranunkid As Integer
    'Sohail (10 Apr 2020) -- End
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set jvpostingtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Jvpostingtranunkid() As Integer
        Get
            Return mintJvpostingtranunkid
        End Get
        Set(ByVal value As Integer)
            mintJvpostingtranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stationunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Stationunkid() As Integer
        Get
            Return mintStationunkid
        End Get
        Set(ByVal value As Integer)
            mintStationunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set batchno
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Batchno() As String
        Get
            Return mstrBatchno
        End Get
        Set(ByVal value As String)
            mstrBatchno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Sohail (10 Apr 2020) -- Start
    'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
    Public Property _Allocationbyid() As Integer
        Get
            Return mintAllocationbyid
        End Get
        Set(ByVal value As Integer)
            mintAllocationbyid = value
        End Set
    End Property

    Public Property _Allocationtranunkid() As Integer
        Get
            Return mintAllocationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintAllocationtranunkid = value
        End Set
    End Property
    'Sohail (10 Apr 2020) -- End



    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            StrQ = "SELECT " & _
              "  jvpostingtranunkid " & _
              ", periodunkid " & _
              ", stationunkid " & _
              ", batchno " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(allocationbyid, 1) AS allocationbyid " & _
              ", ISNULL(allocationtranunkid, prjvposting_tran.stationunkid) AS allocationtranunkid " & _
             "FROM prjvposting_tran " & _
             "WHERE jvpostingtranunkid = @jvpostingtranunkid "
            'Sohail (10 Apr 2020) - [allocationbyid, allocationtranunkid]

            objDataOperation.AddParameter("@jvpostingtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintJvpostingTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintjvpostingtranunkid = CInt(dtRow.Item("jvpostingtranunkid"))
                mintperiodunkid = CInt(dtRow.Item("periodunkid"))
                mintstationunkid = CInt(dtRow.Item("stationunkid"))
                mstrbatchno = dtRow.Item("batchno").ToString
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mintloginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnisweb = CBool(dtRow.Item("isweb"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintvoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString
                'Sohail (10 Apr 2020) -- Start
                'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
                mintAllocationbyid = CInt(dtRow.Item("allocationbyid"))
                mintAllocationtranunkid = CInt(dtRow.Item("allocationtranunkid"))
                'Sohail (10 Apr 2020) -- End

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intPeriodId As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            Dim dsAllocation As DataSet = (New clsMasterData).GetReportAllocation("List", , , True)
            'Sohail (10 Apr 2020) -- End

            strQ = "SELECT " & _
                      "  prjvposting_tran.jvpostingtranunkid " & _
                      ", prjvposting_tran.periodunkid " & _
                      ", cfcommon_period_tran.period_code " & _
                      ", cfcommon_period_tran.period_name " & _
                      ", prjvposting_tran.stationunkid " & _
                      ", ISNULL(hrstation_master.code, '') AS BracnhCode " & _
                      ", ISNULL(hrstation_master.name, '') AS BracnhName " & _
                      ", prjvposting_tran.batchno " & _
                      ", prjvposting_tran.userunkid " & _
                      ", prjvposting_tran.loginemployeeunkid " & _
                      ", prjvposting_tran.isweb " & _
                      ", prjvposting_tran.isvoid " & _
                      ", prjvposting_tran.voiduserunkid " & _
                      ", prjvposting_tran.voidloginemployeeunkid " & _
                      ", prjvposting_tran.voiddatetime " & _
                      ", prjvposting_tran.voidreason " & _
                      ", ISNULL(prjvposting_tran.allocationbyid, 1) AS allocationbyid " & _
                      ", ISNULL(prjvposting_tran.allocationtranunkid, prjvposting_tran.stationunkid) AS allocationtranunkid "

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            strQ &= ", CASE ISNULL(prjvposting_tran.allocationbyid, " & enAnalysisReport.Branch & ") "
            For Each dsRow As DataRow In dsAllocation.Tables(0).Rows
                If CInt(dsRow.Item("Id")) <= 0 Then
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '' "
                Else
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '" & dsRow.Item("Name").ToString & "' "
                End If
            Next
            strQ &= " END AS AllocationByName "

            strQ &= ", CASE ISNULL(prjvposting_tran.allocationbyid, " & enAnalysisReport.Branch & ") "
            For Each dsRow As DataRow In dsAllocation.Tables(0).Rows
                If CInt(dsRow.Item("Id")) <= 0 Then
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN '' "
                Else
                    strQ &= " WHEN " & CInt(dsRow.Item("Id")) & "  THEN " & dsRow.Item("AllocationTranName") & " "
                End If
            Next
            strQ &= " END AS AllocationTranName "
            'Sohail (10 Apr 2020) -- End

            strQ &= "FROM prjvposting_tran " & _
                    "LEFT JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = prjvposting_tran.periodunkid " & _
                    "LEFT JOIN hrstation_master ON hrstation_master.stationunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid = " & enAnalysisReport.Branch & " " & _
                    "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.Department & " " & _
                    "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.Section & " " & _
                    "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.Unit & " " & _
                    "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.Job & " " & _
                    "LEFT JOIN prcostcenter_master ON prcostcenter_master.costcenterunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.CostCenter & " " & _
                    "LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.SectionGroup & " " & _
                    "LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.UnitGroup & " " & _
                    "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.Team & " " & _
                    "LEFT JOIN hrdepartment_group_master ON hrdepartment_group_master.deptgroupunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.DepartmentGroup & " " & _
                    "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.JobGroup & " " & _
                    "LEFT JOIN hrclassgroup_master ON hrclassgroup_master.classgroupunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.ClassGroup & " " & _
                    "LEFT JOIN hrclasses_master ON hrclasses_master.classesunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.Classs & " " & _
                    "LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.GradeGroup & " " & _
                    "LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.Grade & " " & _
                    "LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = prjvposting_tran.allocationtranunkid AND prjvposting_tran.allocationbyid =  " & enAnalysisReport.GradeLevel & " " & _
                 "WHERE prjvposting_tran.isvoid = 0 "
            'Sohail (10 Apr 2020) - [allocationbyid, allocationtranunkid]

            If intPeriodId > 0 Then
                strQ &= " AND prjvposting_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prjvposting_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstationunkid.ToString)
            objDataOperation.AddParameter("@batchno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrbatchno.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)
            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyid)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid)
            'Sohail (10 Apr 2020) -- End

            StrQ = "INSERT INTO prjvposting_tran ( " & _
              "  periodunkid " & _
              ", stationunkid " & _
              ", batchno " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", allocationbyid" & _
              ", allocationtranunkid" & _
            ") VALUES (" & _
              "  @periodunkid " & _
              ", @stationunkid " & _
              ", @batchno " & _
              ", @userunkid " & _
              ", @loginemployeeunkid " & _
              ", @isweb " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voidloginemployeeunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @allocationbyid" & _
              ", @allocationtranunkid" & _
            "); SELECT @@identity"
            'Sohail (10 Apr 2020) - [allocationbyid, allocationtranunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintJvpostingTranUnkId = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrail(objDataOperation, enAuditType.ADD) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prjvposting_tran) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintJvpostingtranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@jvpostingtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintjvpostingtranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintperiodunkid.ToString)
            objDataOperation.AddParameter("@stationunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintstationunkid.ToString)
            objDataOperation.AddParameter("@batchno", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrbatchno.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)
            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyid)
            objDataOperation.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid)
            'Sohail (10 Apr 2020) -- End

            StrQ = "UPDATE prjvposting_tran SET " & _
              "  periodunkid = @periodunkid" & _
              ", stationunkid = @stationunkid" & _
              ", batchno = @batchno" & _
              ", userunkid = @userunkid" & _
              ", loginemployeeunkid = @loginemployeeunkid" & _
              ", isweb = @isweb" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", allocationbyid = @allocationbyid " & _
              ", allocationtranunkid = @allocationtranunkid " & _
            "WHERE jvpostingtranunkid = @jvpostingtranunkid "
            'Sohail (10 Apr 2020) - [allocationbyid, allocationtranunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrail(objDataOperation, enAuditType.EDIT) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prjvposting_tran) </purpose>
    Public Function Delete() As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE prjvposting_tran SET " & _
             "  isvoid = 1 " & _
             ", voiduserunkid = @voiduserunkid" & _
             ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
             ", voiddatetime = @voiddatetime" & _
             ", voidreason = @voidreason " & _
           "WHERE jvpostingtranunkid = @jvpostingtranunkid "


            objDataOperation.AddParameter("@jvpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJvpostingtranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._xDataOp = objDataOperation
            Me._Jvpostingtranunkid = mintJvpostingtranunkid

            If InsertAuditTrail(objDataOperation, enAuditType.DELETE) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@jvpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intPeriodUnkId As Integer, ByVal intAllocationbyid As Integer, ByVal strAllocationtranunkIDs As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal strFilter As String = "", Optional ByRef str_BatchNo As String = "", Optional ByRef intJVPostingtranunkid As Integer = 0) As Boolean
        'Sohail (10 Apr 2020) - [intStationunkid=intAllocationbyid, intAllocationtranunkid]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            str_BatchNo = ""
            intJVPostingtranunkid = 0

            strQ = "SELECT " & _
              "  jvpostingtranunkid " & _
              ", periodunkid " & _
              ", stationunkid " & _
              ", batchno " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM prjvposting_tran " & _
             "WHERE isvoid = 0 " & _
             "AND periodunkid = @periodunkid "

            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            'If intStationunkid > 0 Then
            '    strQ &= " AND stationunkid = @stationunkid "
            '    objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStationunkid)
            'End If
            If intAllocationbyid > 0 Then
                strQ &= " AND allocationbyid = @allocationbyid "
                objDataOperation.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAllocationbyid)
            End If

            If strAllocationtranunkIDs.Trim.Length > 0 Then
                strQ &= " AND allocationtranunkid IN (" & strAllocationtranunkIDs & ") "
            End If
            'Sohail (10 Apr 2020) -- End

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            If intUnkid > 0 Then
                strQ &= " AND jvpostingtranunkid <> @jvpostingtranunkid"
                objDataOperation.AddParameter("@jvpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                str_BatchNo = dsList.Tables(0).Rows(0).Item("batchno").ToString
                intJVPostingtranunkid = CInt(dsList.Tables(0).Rows(0).Item("jvpostingtranunkid"))
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrail(ByVal objDataOp As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Try
            objDataOp.ClearParameters()
            objDataOp.AddParameter("@jvpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintJvpostingtranunkid.ToString)
            objDataOp.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOp.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOp.AddParameter("@batchno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchno.ToString)
            'Sohail (10 Apr 2020) -- Start
            'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
            objDataOp.AddParameter("@allocationbyid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationbyid)
            objDataOp.AddParameter("@allocationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAllocationtranunkid)
            'Sohail (10 Apr 2020) -- End
            objDataOp.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOp.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOp.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOp.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOp.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOp.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOp.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOp.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)

            strQ = "INSERT INTO atprjvposting_tran ( " & _
              "  jvpostingtranunkid " & _
              ", periodunkid " & _
              ", stationunkid " & _
              ", batchno " & _
              ", allocationbyid" & _
              ", allocationtranunkid" & _
              ", audituserunkid " & _
              ", loginemployeeunkid " & _
              ", audittype " & _
              ", auditdatetime " & _
              ", isweb " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
            ") VALUES (" & _
              "  @jvpostingtranunkid " & _
              ", @periodunkid " & _
              ", @stationunkid " & _
              ", @batchno " & _
              ", @allocationbyid" & _
              ", @allocationtranunkid" & _
              ", @audituserunkid " & _
              ", @loginemployeeunkid " & _
              ", @audittype " & _
              ", @auditdatetime " & _
              ", @isweb " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
            "); SELECT @@identity"
            'Sohail (10 Apr 2020) - [allocationbyid, allocationtranunkid]

            dsList = objDataOp.ExecQuery(strQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

End Class
