﻿'************************************************************************************************************************************
'Class Name : clsEmployee_batchposting.vb
'Purpose    :
'Date       :24/12/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsBatchPosting
    Private Shared ReadOnly mstrModuleName As String = "clsBatchPosting"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Dim objBatchPostingTran As New clsBatchPostingTran
#Region " Private variables "
    Private mintEmpbatchpostingunkid As Integer
    Private mstrBatchno As String = String.Empty
    Private mblnIsposted As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    Private mstrBatchCode As String = String.Empty
    Private mstrBatchName As String = String.Empty
    Private mstrDescription As String = String.Empty
    'Sohail (24 Feb 2016) -- End

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empbatchpostingunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Empbatchpostingunkid() As Integer
        Get
            Return mintEmpbatchpostingunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpbatchpostingunkid = value
            Call GetData()
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set batchno
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Batchno() As String
        Get
            Return mstrBatchno
        End Get
        Set(ByVal value As String)
            mstrBatchno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isposted
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isposted() As Boolean
        Get
            Return mblnIsposted
        End Get
        Set(ByVal value As Boolean)
            mblnIsposted = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    Public Property _BatchCode() As String
        Get
            Return mstrBatchCode
        End Get
        Set(ByVal value As String)
            mstrBatchCode = value
        End Set
    End Property

    Public Property _BatchName() As String
        Get
            Return mstrBatchName
        End Get
        Set(ByVal value As String)
            mstrBatchName = value
        End Set
    End Property

    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property
    'Sohail (24 Feb 2016) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  empbatchpostingunkid " & _
              ", batchno " & _
              ", isposted " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ISNULL(premployee_batchposting_master.batchcode, '') AS batchcode " & _
              ", ISNULL(premployee_batchposting_master.batchname, '') AS batchname " & _
              ", ISNULL(premployee_batchposting_master.description, '') AS description " & _
             "FROM premployee_batchposting_master " & _
             "WHERE empbatchpostingunkid = @empbatchpostingunkid "
            'Sohail (24 Feb 2016) - [batchcode, batchname, description]

            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbatchpostingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEmpbatchpostingunkid = CInt(dtRow.Item("empbatchpostingunkid"))
                mstrBatchno = dtRow.Item("batchno").ToString
                mblnIsposted = CBool(dtRow.Item("isposted"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                'Sohail (24 Feb 2016) -- Start
                'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                mstrBatchCode = dtRow.Item("batchcode").ToString
                mstrBatchName = dtRow.Item("batchname").ToString
                mstrDescription = dtRow.Item("description").ToString
                'Sohail (24 Feb 2016) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Public Sub GetTranData()
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  empbatchpostingunkid " & _
    '          ", batchno " & _
    '          ", userunkid " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '         "FROM premployee_batchposting_tran " & _
    '         "WHERE empbatchpostingtranunkid = @empbatchpostingtranunkid "

    '        objDataOperation.AddParameter("@empbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbatchpostingunkid.ToString)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            mintEmpbatchpostingunkid = CInt(dtRow.Item("empbatchpostingunkid"))
    '            mstrBatchno = dtRow.Item("batchno").ToString
    '            mintUserunkid = CInt(dtRow.Item("userunkid"))
    '            mblnIsvoid = CBool(dtRow.Item("isvoid"))
    '            mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
    '            mdtVoiddatetime = dtRow.Item("voiddatetime")
    '            mstrVoidreason = dtRow.Item("voidreason").ToString
    '            Exit For
    '        Next
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetTranData; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal strBatchNo As String = "", _
    '                        Optional ByVal intPeriodId As Integer = 0, _
    '                        Optional ByVal intEmployeeId As Integer = 0, _
    '                        Optional ByVal intTranHeadId As Integer = 0, _
    '                        Optional ByVal enPostingStatus As Integer = 0, _
    '                        Optional ByVal strFilter As String = "", _
    '                        Optional ByVal strOrderBy As String = "") As DataSet

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        strQ = "SELECT  premployee_batchposting_master.empbatchpostingunkid " & _
    '                      ", premployee_batchposting_master.batchno " & _
    '                      ", premployee_batchposting_tran.periodunkid " & _
    '                      ", premployee_batchposting_tran.empbatchpostingtranunkid " & _
    '                      ", cfcommon_period_tran.period_code AS PeriodCode " & _
    '                      ", cfcommon_period_tran.period_name AS PeriodName " & _
    '                      ", premployee_batchposting_tran.employeeunkid " & _
    '                      ", hremployee_master.employeecode AS EmpCode " & _
    '                      ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
    '                      ", premployee_batchposting_tran.tranheadunkid " & _
    '                      ", prtranhead_master.trnheadcode AS TranHeadCode " & _
    '                      ", prtranhead_master.trnheadname AS TranHeadName " & _
    '                      ", premployee_batchposting_tran.amount " & _
    '                      ", premployee_batchposting_master.isposted " & _
    '                      ", premployee_batchposting_tran.isposted AS IsPostedTran " & _
    '                      ", premployee_batchposting_master.userunkid " & _
    '                      ", premployee_batchposting_master.isvoid " & _
    '                      ", premployee_batchposting_master.voiduserunkid " & _
    '                      ", premployee_batchposting_master.voiddatetime " & _
    '                      ", premployee_batchposting_master.voidreason " & _
    '                "FROM    premployee_batchposting_master " & _
    '                        "JOIN premployee_batchposting_tran ON premployee_batchposting_tran.empbatchpostingunkid = premployee_batchposting_master.empbatchpostingunkid " & _
    '                        "JOIN hremployee_master ON hremployee_master.employeeunkid = premployee_batchposting_tran.employeeunkid " & _
    '                        "JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = premployee_batchposting_tran.periodunkid " & _
    '                        "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = premployee_batchposting_tran.tranheadunkid " & _
    '                "WHERE   ISNULL(premployee_batchposting_master.isvoid, 0) = 0 " & _
    '                        "AND ISNULL(premployee_batchposting_tran.isvoid, 0) = 0 "

    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        End If

    '        strQ &= UserAccessLevel._AccessLevelFilterString

    '        If strBatchNo.Trim <> "" Then
    '            strQ &= " AND premployee_batchposting_master.batchno LIKE '%" & strBatchNo.Trim & "%' "
    '        End If

    '        If intPeriodId > 0 Then
    '            strQ &= " AND premployee_batchposting_tran.periodunkid = @periodunkid "
    '            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
    '        End If

    '        If intEmployeeId > 0 Then
    '            strQ &= " AND premployee_batchposting_tran.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
    '        End If

    '        If intTranHeadId > 0 Then
    '            strQ &= " AND premployee_batchposting_tran.tranheadunkid = @tranheadunkid "
    '            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadId)
    '        End If

    '        If enPostingStatus = enEDBatchPostingStatus.Posted_to_ED Then
    '            strQ &= " AND premployee_batchposting_master.isposted = 1 "
    '        ElseIf enPostingStatus = enEDBatchPostingStatus.Not_Posted Then
    '            strQ &= " AND premployee_batchposting_master.isposted = 0 "
    '        End If

    '        If strFilter.Trim <> "" Then
    '            strQ &= " AND " & strFilter
    '        End If

    '        If strOrderBy.Trim <> "" Then
    '            strQ &= " ORDER BY " & strOrderBy
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    Public Function GetList(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal strTableName As String _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal intBatchID As Integer = 0 _
                            , Optional ByVal intPeriodId As Integer = 0 _
                            , Optional ByVal intEmployeeId As Integer = 0 _
                            , Optional ByVal intTranHeadId As Integer = 0 _
                            , Optional ByVal enPostingStatus As Integer = 0 _
                            , Optional ByVal strFilter As String = "" _
                            , Optional ByVal strOrderBy As String = "") As DataSet
        'Sohail (24 Feb 2016) - [strBatchNo = intBatchID]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  premployee_batchposting_master.empbatchpostingunkid " & _
                          ", premployee_batchposting_master.batchno " & _
                          ", premployee_batchposting_tran.periodunkid " & _
                          ", premployee_batchposting_tran.empbatchpostingtranunkid " & _
                          ", cfcommon_period_tran.period_code AS PeriodCode " & _
                          ", cfcommon_period_tran.period_name AS PeriodName " & _
                          ", premployee_batchposting_tran.employeeunkid " & _
                          ", hremployee_master.employeecode AS EmpCode " & _
                          ", ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EmpName " & _
                          ", premployee_batchposting_tran.tranheadunkid " & _
                          ", prtranhead_master.trnheadcode AS TranHeadCode " & _
                          ", prtranhead_master.trnheadname AS TranHeadName " & _
                          ", premployee_batchposting_tran.amount " & _
                          ", premployee_batchposting_master.isposted " & _
                          ", premployee_batchposting_tran.isposted AS IsPostedTran " & _
                          ", premployee_batchposting_master.userunkid " & _
                          ", premployee_batchposting_master.isvoid " & _
                          ", premployee_batchposting_master.voiduserunkid " & _
                          ", premployee_batchposting_master.voiddatetime " & _
                          ", premployee_batchposting_master.voidreason " & _
                          ", ISNULL(premployee_batchposting_master.batchcode, '') AS batchcode " & _
                          ", ISNULL(premployee_batchposting_master.batchname, '') AS batchname " & _
                          ", ISNULL(premployee_batchposting_master.description, '') AS description " & _
                    "FROM    premployee_batchposting_master " & _
                            "JOIN premployee_batchposting_tran ON premployee_batchposting_tran.empbatchpostingunkid = premployee_batchposting_master.empbatchpostingunkid " & _
                            "JOIN hremployee_master ON hremployee_master.employeeunkid = premployee_batchposting_tran.employeeunkid " & _
                            "JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = premployee_batchposting_tran.periodunkid " & _
                            "JOIN prtranhead_master ON prtranhead_master.tranheadunkid = premployee_batchposting_tran.tranheadunkid "
            'Sohail (24 Feb 2016) - [batchcode, batchname, description]

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END



            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE   ISNULL(premployee_batchposting_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(premployee_batchposting_tran.isvoid, 0) = 0 "

            'strQ &= UserAccessLevel._AccessLevelFilterString
            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'If strBatchNo.Trim <> "" Then
            '    strQ &= " AND premployee_batchposting_master.batchno LIKE '%" & strBatchNo.Trim & "%' "
            'End If
            If intBatchID > 0 Then
                strQ &= " AND premployee_batchposting_master.empbatchpostingunkid = @empbatchpostingunkid "
                objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBatchID)
            End If
            'Sohail (24 Feb 2016) -- End

            If intPeriodId > 0 Then
                strQ &= " AND premployee_batchposting_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If

            If intEmployeeId > 0 Then
                strQ &= " AND premployee_batchposting_tran.employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
            End If

            If intTranHeadId > 0 Then
                strQ &= " AND premployee_batchposting_tran.tranheadunkid = @tranheadunkid "
                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadId)
            End If

            If enPostingStatus = enEDBatchPostingStatus.Posted_to_ED Then
                strQ &= " AND premployee_batchposting_master.isposted = 1 "
            ElseIf enPostingStatus = enEDBatchPostingStatus.Not_Posted Then
                strQ &= " AND premployee_batchposting_master.isposted = 0 "
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            If strOrderBy.Trim <> "" Then
                strQ &= " ORDER BY " & strOrderBy
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (21 Aug 2015) -- End

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Public Function GetBatchList(ByVal strTableName As String, _
    '                        Optional ByVal strBatchNo As String = "", _
    '                        Optional ByVal intPeriodId As Integer = 0, _
    '                        Optional ByVal enPostingStatus As Integer = 0, _
    '                        Optional ByVal strFilter As String = "", _
    '                        Optional ByVal strOrderBy As String = "") As DataSet
    Public Function GetBatchList(ByVal xDatabaseName As String _
                           , ByVal xUserUnkid As Integer _
                           , ByVal xYearUnkid As Integer _
                           , ByVal xCompanyUnkid As Integer _
                           , ByVal xPeriodStart As DateTime _
                           , ByVal xPeriodEnd As DateTime _
                           , ByVal xUserModeSetting As String _
                           , ByVal xOnlyApproved As Boolean _
                           , ByVal xIncludeIn_ActiveEmployee As Boolean _
                           , ByVal strTableName As String _
                           , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                           , Optional ByVal intBatchID As Integer = 0 _
                           , Optional ByVal intPeriodId As Integer = 0 _
                           , Optional ByVal enPostingStatus As Integer = 0 _
                           , Optional ByVal strFilter As String = "" _
                           , Optional ByVal strOrderBy As String = "" _
                           ) As DataSet
        'Sohail (24 Feb 2016) - [strBatchNo = intBatchID]
        'Sohail (21 Aug 2015) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
        Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  premployee_batchposting_master.empbatchpostingunkid " & _
                          ", premployee_batchposting_master.batchno " & _
                          ", premployee_batchposting_tran.periodunkid " & _
                          ", cfcommon_period_tran.period_code AS PeriodCode " & _
                          ", cfcommon_period_tran.period_name AS PeriodName " & _
                          ", cfcommon_period_tran.end_date " & _
                          ", premployee_batchposting_master.IsPosted " & _
                          ", ISNULL(premployee_batchposting_master.batchcode, '') AS batchcode " & _
                          ", ISNULL(premployee_batchposting_master.batchname, '') AS batchname " & _
                          ", ISNULL(premployee_batchposting_master.description, '') AS description " & _
                          ", premployee_batchposting_master.userunkid " & _
                          ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') AS user_name " & _
                          ", SUM(premployee_batchposting_tran.amount) AS TotalAmount " & _
                    "FROM    premployee_batchposting_master " & _
                            "JOIN premployee_batchposting_tran ON premployee_batchposting_master.empbatchpostingunkid = premployee_batchposting_tran.empbatchpostingunkid " & _
                            "JOIN hremployee_master ON hremployee_master.employeeunkid = premployee_batchposting_tran.employeeunkid " & _
                            "JOIN cfcommon_period_tran ON cfcommon_period_tran.periodunkid = premployee_batchposting_tran.periodunkid " & _
                                                         "AND cfcommon_period_tran.modulerefid = 1 " & _
                            "LEFT JOIN hrmsConfiguration..cfuser_master ON cfuser_master.userunkid = premployee_batchposting_master.userunkid "
            'Sohail (24 Feb 2016) - [batchcode, batchname, description, userunkid, user_name], [LEFT JOIN hrmsConfiguration..cfuser_master]

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END


            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE   ISNULL(premployee_batchposting_master.isvoid, 0) = 0 " & _
                            "AND ISNULL(premployee_batchposting_tran.isvoid, 0) = 0 " & _
                            "AND cfcommon_period_tran.isactive = 1 "


            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            'If strBatchNo.Trim <> "" Then
            '    strQ &= " AND premployee_batchposting_master.batchno LIKE '%" & strBatchNo.Trim & "%' "
            'End If
            If intBatchID > 0 Then
                strQ &= " AND premployee_batchposting_master.empbatchpostingunkid = @empbatchpostingunkid "
                objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBatchID)
            End If
            'Sohail (24 Feb 2016) -- End

            If intPeriodId > 0 Then
                strQ &= " AND premployee_batchposting_tran.periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            End If

            If enPostingStatus = enEDBatchPostingStatus.Posted_to_ED Then
                strQ &= " AND premployee_batchposting_master.isposted = 1 "
            ElseIf enPostingStatus = enEDBatchPostingStatus.Not_Posted Then
                strQ &= " AND premployee_batchposting_master.isposted = 0 "
            End If

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            strQ &= "GROUP BY premployee_batchposting_master.empbatchpostingunkid " & _
                          ", premployee_batchposting_master.batchno " & _
                          ", premployee_batchposting_tran.periodunkid " & _
                          ", cfcommon_period_tran.period_code " & _
                          ", cfcommon_period_tran.period_name " & _
                          ", cfcommon_period_tran.end_date " & _
                          ", premployee_batchposting_master.IsPosted " & _
                          ", ISNULL(premployee_batchposting_master.batchcode, '') " & _
                          ", ISNULL(premployee_batchposting_master.batchname, '') " & _
                          ", ISNULL(premployee_batchposting_master.description, '') " & _
                          ", premployee_batchposting_master.userunkid " & _
                          ", ISNULL(cfuser_master.firstname, '') + ' ' + ISNULL(cfuser_master.lastname, '') "
            'Sohail (24 Feb 2016) - [batchcode, batchname, description, userunkid, firstname+lastname]

            If strOrderBy.Trim <> "" Then
                strQ &= " ORDER BY " & strOrderBy
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBatchList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (premployee_batchposting_master) </purpose>
    Public Function Insert(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal dtTran As DataTable _
                            , ByVal intBatchPostingNotype As Integer _
                            , ByVal strBatchPostingPrifix As String _
                            , ByVal strTableName As String _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal strFilerString As String = "" _
                            ) As Boolean
        'Public Function Insert(ByVal dtTran As DataTable) As Boolean 'Sohail (21 Aug 2015)
        'If isExist(mstrBatchno) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Batch Posting No. is already defined.")
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            Dim intBachVocNoType As Integer = 0
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'intBachVocNoType = ConfigParameter._Object._BatchPostingNotype
            intBachVocNoType = intBatchPostingNotype
            'Sohail (21 Aug 2015) -- End
            If intBachVocNoType = 0 Then
                If isExist(mstrBatchno) Then
                    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Batch Posting No. is already defined. Please define new Batch Posting No.")
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            End If

            objDataOperation.AddParameter("@batchno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchno.ToString)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            objDataOperation.AddParameter("@batchcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchCode.ToString)
            objDataOperation.AddParameter("@batchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrDescription.ToString)
            'Sohail (24 Feb 2016) -- End

            strQ = "INSERT INTO premployee_batchposting_master ( " & _
              "  batchno " & _
              ", isposted " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", batchcode " & _
              ", batchname " & _
              ", description " & _
            ") VALUES (" & _
              "  @batchno " & _
              ", @isposted " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @batchcode " & _
              ", @batchname " & _
              ", @description " & _
            "); SELECT @@identity"
            'Sohail (24 Feb 2016) - [batchcode, batchname, description]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmpbatchpostingunkid = dsList.Tables(0).Rows(0).Item(0)

            If intBachVocNoType = 1 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'If Set_AutoNumber(objDataOperation, mintEmpbatchpostingunkid, "premployee_batchposting_master", "batchno", "empbatchpostingunkid", "NextBatchPostingNo", ConfigParameter._Object._BatchPostingPrifix) = False Then
                If Set_AutoNumber(objDataOperation, mintEmpbatchpostingunkid, "premployee_batchposting_master", "batchno", "empbatchpostingunkid", "NextBatchPostingNo", strBatchPostingPrifix) = False Then
                    'Sohail (21 Aug 2015) -- End
                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If

                If Get_Saved_Number(objDataOperation, mintEmpbatchpostingunkid, "premployee_batchposting_master", "batchno", "empbatchpostingunkid", mstrBatchno) = False Then
                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                'Sohail (14 Nov 2017) -- Start
                'Enhancement - 70.1 - Automate Batch Code generation,Add this to Number setting for both Auto increment or Manual generated. (Ref. No. : 119) | Hide Batch code option on Batch Posting Screen.
                mstrBatchCode = mstrBatchno
                strQ = "UPDATE premployee_batchposting_master SET batchcode = '" & mstrBatchCode & "' WHERE empbatchpostingunkid = " & mintEmpbatchpostingunkid & " "

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'Sohail (14 Nov 2017) -- End

            End If

            If dtTran IsNot Nothing AndAlso dtTran.Rows.Count > 0 Then
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objBatchPostingTran._Empbatchpostingunkid = mintEmpbatchpostingunkid
                objBatchPostingTran.GetBatchPosting_Tran(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmpbatchpostingunkid, blnApplyUserAccessFilter, strFilerString)
                'Sohail (21 Aug 2015) -- End
                objBatchPostingTran._DataTable = dtTran

                'Pinkal (24-May-2013) -- Start
                'Enhancement : TRA Changes

                'If objBatchPostingTran.InsertUpdateDelete_EmpBatchPosting() = False Then
                '    objDataOperation.ReleaseTransaction(False)
                '    Return False
                'End If

                If objBatchPostingTran.InsertUpdateDelete_EmpBatchPosting(objDataOperation) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                'Pinkal (24-May-2013) -- End

            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (premployee_batchposting_master) </purpose>
    Public Function Update(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal dtTran As DataTable _
                            , ByVal strTableName As String _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal strFilerString As String = "" _
                            ) As Boolean
        'Public Function Update(ByVal dtTran As DataTable) As Boolean 'Sohail (21 Aug 2015)

        If isExist(mstrBatchno, mintEmpbatchpostingunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Batch Posting No. is already defined. Please define new Batch Posting No.")
            Return False
        End If

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbatchpostingunkid.ToString)
            objDataOperation.AddParameter("@batchno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchno.ToString)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            objDataOperation.AddParameter("@batchcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchCode.ToString)
            objDataOperation.AddParameter("@batchname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrBatchName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrDescription.ToString)
            'Sohail (24 Feb 2016) -- End

            strQ = "UPDATE premployee_batchposting_master SET " & _
              "  batchno = @batchno" & _
              ", isposted = @isposted" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", batchcode = @batchcode " & _
              ", batchname = @batchname " & _
              ", description = @description " & _
              "FROM    premployee_batchposting_tran " & _
            "WHERE premployee_batchposting_master.empbatchpostingunkid = premployee_batchposting_tran.empbatchpostingunkid " & _
            "AND premployee_batchposting_master.empbatchpostingunkid = @empbatchpostingunkid "
            'Sohail (24 Feb 2016) - [batchcode, batchname, description]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtTran.Rows.Count > 0 Then
                Dim dt() As DataRow = dtTran.Select("AUD=''")
                If dt.Length = dtTran.Rows.Count Then


                    'Pinkal (24-May-2013) -- Start
                    'Enhancement : TRA Changes

                    'If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prbatch_transaction_master", mintEmpbatchpostingunkid, "batchtransactionunkid", 2) Then
                    '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prbatch_transaction_master", "batchtransactionunkid", mintEmpbatchpostingunkid, "prbatch_transaction_tran", "batchtransactiontranunkid", -1, 2, 0) = False Then
                    '        objDataOperation.ReleaseTransaction(False)
                    '        Return False
                    '    End If
                    'End If


                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "prbatch_transaction_master", mintEmpbatchpostingunkid, "batchtransactionunkid", 2) Then
                        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "prbatch_transaction_master", "batchtransactionunkid", mintEmpbatchpostingunkid, "prbatch_transaction_tran", "batchtransactiontranunkid", -1, 2, 0, False, mintUserunkid) = False Then
                            objDataOperation.ReleaseTransaction(False)
                            Return False
                        End If
                    End If

                    'Pinkal (24-May-2013) -- End

                Else
                    'Sohail (21 Aug 2015) -- Start
                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                    'objBatchPostingTran._Empbatchpostingunkid = mintEmpbatchpostingunkid
                    objBatchPostingTran.GetBatchPosting_Tran(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee, mintEmpbatchpostingunkid, blnApplyUserAccessFilter, strFilerString)
                    'Sohail (21 Aug 2015) -- End
                    objBatchPostingTran._DataTable = dtTran


                    'Pinkal (24-May-2013) -- Start
                    'Enhancement : TRA Changes

                    'If objBatchPostingTran.InsertUpdateDelete_EmpBatchPosting() = False Then
                    '    objDataOperation.ReleaseTransaction(False)
                    '    Return False
                    'End If

                    If objBatchPostingTran.InsertUpdateDelete_EmpBatchPosting(objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If


                    'Pinkal (24-May-2013) -- End

                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (premployee_batchposting_master) </purpose>
    Public Function Void(ByVal intUnkid As Integer, ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, _
                           Optional ByVal blnOnlyIfNotPosted As Boolean = False, _
                           Optional ByVal blnOnlyIfPeriodOpen As Boolean = False) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE premployee_batchposting_master SET " & _
                        "  premployee_batchposting_master.isvoid = 1" & _
                        ", premployee_batchposting_master.voiduserunkid = @voiduserunkid" & _
                        ", premployee_batchposting_master.voiddatetime = @voiddatetime " & _
                        ", premployee_batchposting_master.voidreason = @voidreason " & _
                "FROM    premployee_batchposting_tran, cfcommon_period_tran " & _
                "WHERE   premployee_batchposting_master.empbatchpostingunkid = premployee_batchposting_tran.empbatchpostingunkid " & _
                "AND cfcommon_period_tran.periodunkid = premployee_batchposting_tran.periodunkid " & _
                "AND premployee_batchposting_master.empbatchpostingunkid = @empbatchpostingunkid "

            If blnOnlyIfNotPosted = True Then
                strQ &= " AND premployee_batchposting_tran.isposted = 0 "
            End If

            If blnOnlyIfPeriodOpen = True Then
                strQ &= " AND cfcommon_period_tran.statusid = 1 "
            End If

            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserID.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            If dtVoidDateTime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            End If

            Dim intRowsAffected As Integer = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If intRowsAffected > 0 Then

                'Pinkal (24-May-2013) -- Start
                'Enhancement : TRA Changes

                'If objBatchPostingTran.VoidAll(intUnkid, intVoidUserID, dtVoidDateTime, strVoidReason, blnOnlyIfNotPosted, blnOnlyIfPeriodOpen) = False Then
                '    objDataOperation.ReleaseTransaction(False)
                '    Return False
                'End If

                If objBatchPostingTran.VoidAll(objDataOperation, intUnkid, intVoidUserID, dtVoidDateTime, strVoidReason, blnOnlyIfNotPosted, blnOnlyIfPeriodOpen) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                'Pinkal (24-May-2013) -- End

                
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Void; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strBatchNo As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              " batchno " & _
             "FROM premployee_batchposting_master " & _
             "WHERE ISNULL(isvoid, 0) = 0  AND batchno = @batchno "

            If intUnkid > 0 Then
                strQ &= " AND empbatchpostingunkid <> @empbatchpostingunkid"
            End If

            objDataOperation.AddParameter("@batchno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strBatchNo)
            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdateStatus(ByVal intUnkId As Integer, _
                                 ByVal blnSetIsPosted As Boolean, _
                                 Optional ByVal blnOnlyIfNotPosted As Boolean = False, _
                                 Optional ByVal blnOnlyIfPeriodOpen As Boolean = False, _
                                 Optional ByVal blnOnlyIfPosted As Boolean = False _
                                 ) As Boolean
        'Sohail (24 Feb 2016) - [blnOnlyIfPosted]
        'If isExist(mstrName, mintEmpbatchpostingtranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId.ToString)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnSetIsPosted.ToString)

            strQ = "UPDATE premployee_batchposting_master SET " & _
                        "  premployee_batchposting_master.isposted = @isposted " & _
                   "FROM    premployee_batchposting_tran, cfcommon_period_tran " & _
                   "WHERE   premployee_batchposting_master.empbatchpostingunkid = premployee_batchposting_tran.empbatchpostingunkid " & _
                   "AND cfcommon_period_tran.periodunkid = premployee_batchposting_tran.periodunkid " & _
                   "AND premployee_batchposting_master.empbatchpostingunkid = @empbatchpostingunkid "

            If blnOnlyIfNotPosted = True Then
                strQ &= " AND premployee_batchposting_master.isposted = 0 "
            End If

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            If blnOnlyIfPosted = True Then
                strQ &= " AND premployee_batchposting_master.isposted = 1 "
            End If
            'Sohail (24 Feb 2016) -- End

            If blnOnlyIfPeriodOpen = True Then
                strQ &= " AND cfcommon_period_tran.statusid = 1 "
            End If

            Dim intRowsAffected As Integer = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId.ToString)
            objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnSetIsPosted.ToString)

            strQ = "UPDATE premployee_batchposting_tran SET " & _
                        "  premployee_batchposting_tran.isposted = @isposted " & _
                "FROM    premployee_batchposting_master, cfcommon_period_tran " & _
                "WHERE   premployee_batchposting_master.empbatchpostingunkid = premployee_batchposting_tran.empbatchpostingunkid " & _
                "AND   cfcommon_period_tran.periodunkid = premployee_batchposting_tran.periodunkid " & _
                "AND ISNULL(premployee_batchposting_tran.isvoid, 0) = 0 " & _
                "AND premployee_batchposting_tran.empbatchpostingunkid = @empbatchpostingunkid "


            If blnOnlyIfNotPosted = True Then
                strQ &= " AND premployee_batchposting_tran.isposted = 0 "
            End If

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            If blnOnlyIfPosted = True Then
                strQ &= " AND premployee_batchposting_tran.isposted = 1 "
            End If
            'Sohail (24 Feb 2016) -- End

            If blnOnlyIfPeriodOpen = True Then
                strQ &= " AND cfcommon_period_tran.statusid = 1 "
            End If

            intRowsAffected = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If intRowsAffected > 0 Then

                'Pinkal (24-May-2013) -- Start
                'Enhancement : TRA Changes

                'If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", intUnkId, "premployee_batchposting_tran", "empbatchpostingtranunkid", 2, 2) = False Then
                '    objDataOperation.ReleaseTransaction(False)
                '    Return False
                'End If

                If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", intUnkId, "premployee_batchposting_tran", "empbatchpostingtranunkid", 2, 2, "", "", False, mintUserunkid) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If

                'Pinkal (24-May-2013) -- End

            End If

            objDataOperation.ReleaseTransaction(True)
            Return True


        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateStatus; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (24 Feb 2016) -- Start
    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
    Public Function getComboList(Optional ByVal strListName As String = "List" _
                                 , Optional ByVal mblnAddSelect As Boolean = False _
                                 , Optional ByVal strFilter As String = "" _
                                 ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            If mblnAddSelect = True Then
                strQ = "SELECT 0 As empbatchpostingunkid , @Select AS name, '' AS code  UNION "
            End If

            strQ &= "SELECT empbatchpostingunkid, batchname AS name, batchcode AS code FROM premployee_batchposting_master WHERE isvoid = 0 "

            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (24 Feb 2016) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Batch Posting No. is already defined. Please define new Batch Posting No.")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class