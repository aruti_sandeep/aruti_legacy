﻿'************************************************************************************************************************************
'Class Name : clsEmployeeBanks.vb
'Purpose    :
'Date       :30/10/2012
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'Imports eZee.Common.eZeeForm

Public Class clsBatchPostingTran
    Private Shared ReadOnly mstrModuleName As String = "clsBatchPostingTran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintEmpBatchPostingtranUnkId As Integer = 0
    Private mintEmpbatchpostingunkid As Integer
    Private mintPeriodUnkId As Integer = 0
    Private mintEmployeeunkid As Integer
    Private mintTranheadunkid As Integer
    Private mdecAmount As Decimal
    Private mblnIsposted As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mdtTran As DataTable
#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("BatchPosting")
        Try
            mdtTran.Columns.Add("empbatchpostingtranunkid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("empbatchpostingunkid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("batchno", Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("periodunkid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("periodname", Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("employeeunkid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("employeecode", Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("tranheadunkid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("trnheadcode", Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("amount", Type.GetType("System.Decimal")).DefaultValue = 0
            mdtTran.Columns.Add("isposted", Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("enddate", Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("userunkid", Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("AUD", Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", Type.GetType("System.String")).DefaultValue = ""
            'Sohail (31 Oct 2019) -- Start
            'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
            mdtTran.Columns.Add("calctype_id", Type.GetType("System.Int32")).DefaultValue = 0
            'Sohail (31 Oct 2019) -- End

            'Anjan [09 October 2014] -- Start
            'ENHANCEMENT : included Employee name column,requested by Rutta in Email - Item #405.
            mdtTran.Columns.Add("EmpName", Type.GetType("System.String")).DefaultValue = ""
            'Anjan [09 October 2014] -- End

            'Sohail (24 Feb 2016) -- Start
            'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
            mdtTran.Columns.Add("TranHeadName", Type.GetType("System.String")).DefaultValue = ""
            'Sohail (24 Feb 2016) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Employee Batch Posting UnkId from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _EmpBatchPostingtranUnkId() As Integer
        Get
            Return mintEmpBatchPostingtranUnkId
        End Get
        Set(ByVal value As Integer)
            mintEmpBatchPostingtranUnkId = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empbatchpostingunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Empbatchpostingunkid() As Integer
        Get
            Return mintEmpbatchpostingunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpbatchpostingunkid = value
            'Call GetBatchPosting_Tran()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Period UnkId from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _PeriodUnkId() As Integer
        Get
            Return mintPeriodUnkId
        End Get
        Set(ByVal value As Integer)
            mintPeriodUnkId = value
            'Call GetBatchPosting_Tran()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get Data table for given PeriodUnkId
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set tranheadunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Tranheadunkid() As Integer
        Get
            Return mintTranheadunkid
        End Get
        Set(ByVal value As Integer)
            mintTranheadunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set amount
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isposted
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isposted() As Boolean
        Get
            Return mblnIsposted
        End Get
        Set(ByVal value As Boolean)
            mblnIsposted = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

#End Region

#Region " Private Functions "
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  empbatchpostingtranunkid " & _
                          ", ISNULL(empbatchpostingunkid, 0) AS empbatchpostingunkid " & _
                          ", periodunkid " & _
                          ", employeeunkid " & _
                          ", tranheadunkid " & _
                          ", amount " & _
                          ", isposted " & _
                          ", userunkid " & _
                          ", isvoid " & _
                          ", voiduserunkid " & _
                          ", voiddatetime " & _
                          ", voidreason " & _
                    "FROM    premployee_batchposting_tran " & _
                    "WHERE   empbatchpostingtranunkid = @empbatchpostingtranunkid "

            objDataOperation.AddParameter("@empbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpBatchPostingtranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEmpBatchPostingtranUnkId = CInt(dtRow.Item("empbatchpostingtranunkid"))
                mintEmpbatchpostingunkid = CInt(dtRow.Item("empbatchpostingunkid"))
                mintPeriodUnkId = CInt(dtRow.Item("periodunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintTranheadunkid = CInt(dtRow.Item("tranheadunkid"))
                mdecAmount = dtRow.Item("amount")
                mblnIsposted = CBool(dtRow.Item("isposted"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If dtRow.Item("voiddatetime").ToString = Nothing Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Private Sub GetBatchPosting_Tran()
    Public Sub GetBatchPosting_Tran(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , ByVal intEmpbatchpostingunkid As Integer _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal strFilerString As String = "" _
                            )
        'Sohail (21 Aug 2015) -- End

        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowID_Tran As DataRow
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            mintEmpbatchpostingunkid = intEmpbatchpostingunkid
            'Sohail (21 Aug 2015) -- End

            strQ = "SELECT  premployee_batchposting_tran.empbatchpostingtranunkid " & _
                           ", ISNULL(premployee_batchposting_tran.empbatchpostingunkid, 0) AS empbatchpostingunkid " & _
                           ", premployee_batchposting_tran.periodunkid " & _
                           ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
                           ", premployee_batchposting_tran.employeeunkid " & _
                           ", ISNULL(hremployee_master.employeecode, '') AS EmpCode " & _
                           ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
                           ", premployee_batchposting_tran.tranheadunkid " & _
                           ", ISNULL(prtranhead_master.trnheadcode, '') AS TranHeadCode " & _
                           ", ISNULL(prtranhead_master.trnheadname, '') AS TranHeadName " & _
                           ", ISNULL(prtranhead_master.calctype_id, 0) AS calctype_id " & _
                           ", premployee_batchposting_tran.amount " & _
                           ", premployee_batchposting_tran.isposted " & _
                           ", premployee_batchposting_tran.userunkid " & _
                           ", premployee_batchposting_tran.isvoid " & _
                           ", premployee_batchposting_tran.voiduserunkid " & _
                           ", premployee_batchposting_tran.voiddatetime " & _
                           ", premployee_batchposting_tran.voidreason " & _
                           ", CONVERT(CHAR(8),cfcommon_period_tran.end_date, 112) AS enddate " & _
                           ", '' As AUD " & _
                     "FROM    premployee_batchposting_tran " & _
                             "LEFT JOIN cfcommon_period_tran ON premployee_batchposting_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                             "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = premployee_batchposting_tran.employeeunkid " & _
                             "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = premployee_batchposting_tran.tranheadunkid "
            'Sohail (31 Oct 2019) - [calctype_id]

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    strQ &= xUACQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END
            

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE   ISNULL(premployee_batchposting_tran.isvoid, 0) = 0 " & _
                     "AND     premployee_batchposting_tran.empbatchpostingunkid = @empbatchpostingunkid "


            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If


            If strFilerString.Trim.Length > 0 Then
                strQ &= " AND " & strFilerString
            End If

            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbatchpostingunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()


            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowID_Tran = mdtTran.NewRow()

                    dRowID_Tran.Item("empbatchpostingtranunkid") = .Item("empbatchpostingtranunkid")
                    dRowID_Tran.Item("empbatchpostingunkid") = .Item("empbatchpostingunkid")
                    dRowID_Tran.Item("periodunkid") = .Item("periodunkid")
                    dRowID_Tran.Item("periodname") = .Item("PeriodName")
                    dRowID_Tran.Item("employeeunkid") = .Item("employeeunkid")
                    dRowID_Tran.Item("employeecode") = .Item("EmpCode")
                    dRowID_Tran.Item("tranheadunkid") = .Item("tranheadunkid")
                    dRowID_Tran.Item("trnheadcode") = .Item("TranHeadCode")
                    dRowID_Tran.Item("amount") = .Item("amount")
                    dRowID_Tran.Item("isposted") = .Item("isposted")
                    dRowID_Tran.Item("enddate") = .Item("enddate")
                    dRowID_Tran.Item("userunkid") = .Item("userunkid")
                    'Sohail (31 Oct 2019) -- Start
                    'Mainspring Resourcing Ghana : Support Issue Id # 0004199 - Email notification not sent to approvers when there is uploaded, edited e & d.
                    dRowID_Tran.Item("calctype_id") = .Item("calctype_id")
                    'Sohail (31 Oct 2019) -- End

                    'Anjan [09 October 2014] -- Start
                    'ENHANCEMENT : included Employee name column,requested by Rutta in Email - Item #405.
                    dRowID_Tran.Item("EmpName") = .Item("EmpName")
                    'Anjan [09 October 2014] -- End

                    'Sohail (24 Feb 2016) -- Start
                    'Enhancement - Batch Posting changes as per online doc in 58.1 SP. (#15 (KBC & Kenya Project Comments List.xls))
                    dRowID_Tran.Item("TranHeadName") = .Item("TranHeadName")
                    'Sohail (24 Feb 2016) -- End

                    dRowID_Tran.Item("AUD") = .Item("AUD")

                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeBank_Tran", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Public Functions "
    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal intPeriodId As Integer = 0, _
    '                        Optional ByVal intEmployeeId As Integer = 0, _
    '                        Optional ByVal intTranHeadId As Integer = 0, _
    '                        Optional ByVal enPostingStatus As Integer = 0, _
    '                        Optional ByVal strFilter As String = "", _
    '                        Optional ByVal strOrderBy As String = "") As DataSet

    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT  premployee_batchposting_tran.empbatchpostingtranunkid " & _
    '                      ", ISNULL(premployee_batchposting_tran.empbatchpostingunkid, 0) AS empbatchpostingunkid " & _
    '                      ", premployee_batchposting_tran.periodunkid " & _
    '                      ", ISNULL(cfcommon_period_tran.period_name, '') AS PeriodName " & _
    '                      ", premployee_batchposting_tran.employeeunkid " & _
    '                      ", ISNULL(hremployee_master.employeecode, '') AS EmpCode " & _
    '                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS EmpName " & _
    '                      ", premployee_batchposting_tran.tranheadunkid " & _
    '                      ", ISNULL(prtranhead_master.trnheadcode, '') AS TranHeadCode " & _
    '                      ", ISNULL(prtranhead_master.trnheadname, '') AS TranHeadName " & _
    '                      ", premployee_batchposting_tran.amount " & _
    '                      ", premployee_batchposting_tran.isposted " & _
    '                      ", premployee_batchposting_tran.userunkid " & _
    '                      ", premployee_batchposting_tran.isvoid " & _
    '                      ", premployee_batchposting_tran.voiduserunkid " & _
    '                      ", premployee_batchposting_tran.voiddatetime " & _
    '                      ", premployee_batchposting_tran.voidreason " & _
    '                "FROM    premployee_batchposting_tran " & _
    '                        "LEFT JOIN cfcommon_period_tran ON premployee_batchposting_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '                        "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = premployee_batchposting_tran.employeeunkid " & _
    '                        "LEFT JOIN prtranhead_master ON prtranhead_master.tranheadunkid = premployee_batchposting_tran.tranheadunkid " & _
    '                "WHERE   ISNULL(premployee_batchposting_tran.isvoid, 0) = 0 "


    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate "

    '            objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        End If

    '        strQ &= UserAccessLevel._AccessLevelFilterString

    '        If intPeriodId > 0 Then
    '            strQ &= " AND premployee_batchposting_tran.periodunkid = @periodunkid "
    '            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
    '        End If

    '        If intEmployeeId > 0 Then
    '            strQ &= " AND premployee_batchposting_tran.employeeunkid = @employeeunkid "
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId)
    '        End If

    '        If intTranHeadId > 0 Then
    '            strQ &= " AND premployee_batchposting_tran.tranheadunkid = @tranheadunkid "
    '            objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranHeadId)
    '        End If

    '        If enPostingStatus = enEDBatchPostingStatus.Posted_to_ED Then
    '            strQ &= " AND premployee_batchposting_tran.isposted = 1 "
    '        ElseIf enPostingStatus = enEDBatchPostingStatus.Not_Posted Then
    '            strQ &= " AND premployee_batchposting_tran.isposted = 0 "
    '        End If

    '        If strFilter.Trim <> "" Then
    '            strQ &= " AND " & strFilter
    '        End If

    '        If strOrderBy.Trim <> "" Then
    '            strQ &= " ORDER BY " & strOrderBy
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' <returns>Boolean</returns>
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function InsertUpdateDelete_EmpBatchPosting(ByVal objDataOperation As clsDataOperation) As Boolean   'Pinkal (24-May-2013) [objDataOperation]
        'Dim objDataOperation As clsDataOperation

        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Dim dsList As DataSet

        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()

        Try
            

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        mintEmpBatchPostingtranUnkId = CInt(.Item("empbatchpostingtranunkid").ToString)
                        'mintEmpbatchpostingunkid = CInt(.Item("empbatchpostingunkid").ToString)
                        mintPeriodUnkId = CInt(.Item("periodunkid").ToString)
                        mintEmployeeunkid = CInt(.Item("employeeunkid").ToString)
                        mintTranheadunkid = CInt(.Item("tranheadunkid").ToString)
                        mdecAmount = CDec(.Item("amount").ToString)
                        mblnIsposted = CBool(.Item("isposted"))
                        mintUserunkid = CInt(.Item("userunkid").ToString)

                        Select Case .Item("AUD")
                            Case "A"

                                strQ = "INSERT INTO premployee_batchposting_tran ( " & _
                                          "  empbatchpostingunkid" & _
                                          ", periodunkid " & _
                                          ", employeeunkid " & _
                                          ", tranheadunkid " & _
                                          ", amount " & _
                                          ", isposted " & _
                                          ", userunkid " & _
                                          ", isvoid " & _
                                          ", voiduserunkid " & _
                                          ", voiddatetime " & _
                                          ", voidreason" & _
                                    ") VALUES (" & _
                                          "  @empbatchpostingunkid" & _
                                          ", @periodunkid " & _
                                          ", @employeeunkid " & _
                                          ", @tranheadunkid " & _
                                          ", @amount " & _
                                          ", @isposted " & _
                                          ", @userunkid " & _
                                          ", @isvoid " & _
                                          ", @voiduserunkid " & _
                                          ", @voiddatetime " & _
                                          ", @voidreason" & _
                                    "); SELECT @@identity"

                                objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbatchpostingunkid.ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecAmount.ToString)
                                objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                If mdtVoiddatetime = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    objDataOperation.ReleaseTransaction(False)
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintEmpBatchPostingtranUnkId = dsList.Tables(0).Rows(0)(0)


                                'Pinkal (24-May-2013) -- Start
                                'Enhancement : TRA Changes

                                'If .Item("empbatchpostingunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", mintEmpbatchpostingunkid, "premployee_batchposting_tran", "empbatchpostingtranunkid", mintEmpBatchPostingtranUnkId, 2, 1, ) = False Then
                                '        Return False
                                '    End If
                                'Else
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", mintEmpbatchpostingunkid, "premployee_batchposting_tran", "empbatchpostingtranunkid", mintEmpBatchPostingtranUnkId, 1, 1) = False Then
                                '        Return False
                                '    End If
                                'End If

                                If .Item("empbatchpostingunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", mintEmpbatchpostingunkid, "premployee_batchposting_tran", "empbatchpostingtranunkid", mintEmpBatchPostingtranUnkId, 2, 1, False, mintUserunkid) = False Then
                                        Return False
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", mintEmpbatchpostingunkid, "premployee_batchposting_tran", "empbatchpostingtranunkid", mintEmpBatchPostingtranUnkId, 1, 1, False, mintUserunkid) = False Then
                                        Return False
                                    End If
                                End If

                                'Pinkal (24-May-2013) -- End

                            Case "U"

                                strQ = "UPDATE premployee_batchposting_tran SET " & _
                                           "  periodunkid = @periodunkid" & _
                                           ", employeeunkid = @employeeunkid" & _
                                           ", tranheadunkid = @tranheadunkid" & _
                                           ", amount = @amount" & _
                                           ", isposted = @isposted" & _
                                           ", userunkid = @userunkid" & _
                                           ", isvoid = @isvoid" & _
                                           ", voiduserunkid = @voiduserunkid" & _
                                           ", voiddatetime = @voiddatetime" & _
                                           ", voidreason = @voidreason " & _
                                   "FROM    cfcommon_period_tran " & _
                                   "WHERE   cfcommon_period_tran.periodunkid = premployee_batchposting_tran.periodunkid " & _
                                   "AND empbatchpostingtranunkid = @empbatchpostingtranunkid " & _
                                   "AND premployee_batchposting_tran.isposted = 0 " & _
                                   "AND cfcommon_period_tran.statusid = 1 "


                                objDataOperation.AddParameter("@empbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpBatchPostingtranUnkId.ToString)
                                objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbatchpostingunkid.ToString)
                                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
                                objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecAmount.ToString)
                                objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                                If mdtVoiddatetime = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    objDataOperation.ReleaseTransaction(False)
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'Pinkal (24-May-2013) -- Start
                                'Enhancement : TRA Changes

                                'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", mintEmpbatchpostingunkid, "premployee_batchposting_tran", "empbatchpostingtranunkid", mintEmpBatchPostingtranUnkId, 2, 2) = False Then
                                '    Return False
                                'End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", mintEmpbatchpostingunkid, "premployee_batchposting_tran", "empbatchpostingtranunkid", mintEmpBatchPostingtranUnkId, 2, 2, False, mintUserunkid) = False Then
                                    Return False
                                End If

                                'Pinkal (24-May-2013) -- End

                                

                            Case "D"
                                If mintEmpBatchPostingtranUnkId > 0 Then


                                    'Pinkal (24-May-2013) -- Start
                                    'Enhancement : TRA Changes

                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", mintEmpbatchpostingunkid, "premployee_batchposting_tran", "empbatchpostingtranunkid", mintEmpBatchPostingtranUnkId, 2, 3) = False Then
                                    '    Return False
                                    'End If

                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", mintEmpbatchpostingunkid, "premployee_batchposting_tran", "empbatchpostingtranunkid", mintEmpBatchPostingtranUnkId, 2, 3, False, mintUserunkid) = False Then
                                        Return False
                                    End If

                                    'Pinkal (24-May-2013) -- End

                                    

                                    strQ = "UPDATE premployee_batchposting_tran SET " & _
                                               "  isvoid = 1 " & _
                                               ", voiduserunkid = @voiduserunkid" & _
                                               ", voiddatetime = @voiddatetime" & _
                                               ", voidreason = @voidreason " & _
                                          "FROM    cfcommon_period_tran " & _
                                          "WHERE cfcommon_period_tran.periodunkid = premployee_batchposting_tran.periodunkid " & _
                                          "AND empbatchpostingtranunkid = @empbatchpostingtranunkid " & _
                                          "AND premployee_batchposting_tran.isposted = 0 " & _
                                          "AND cfcommon_period_tran.statusid = 1 "

                                    objDataOperation.AddParameter("@empbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpBatchPostingtranUnkId)
                                    'Sohail (21 Aug 2015) -- Start
                                    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                    'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                    'Sohail (21 Aug 2015) -- End
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                                    Call objDataOperation.ExecNonQuery(strQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        objDataOperation.ReleaseTransaction(False)
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If

                                End If

                        End Select
                    End If
                End With
            Next

            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'objDataOperation.ReleaseTransaction(True)

            'Pinkal (24-May-2013) -- End

            Return True

        Catch ex As Exception

            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes
            'objDataOperation.ReleaseTransaction(False)
            'Pinkal (24-May-2013) -- End
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_EmpBatchPosting; Module Name: " & mstrModuleName)
            exForce = Nothing

            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes
            'objDataOperation = Nothing
            'Pinkal (24-May-2013) -- End
        Finally

        End Try
    End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> INSERT INTO Database Table (premployee_batchposting_tran) </purpose>
    'Public Function Insert() As Boolean
    '    'If isExist(mstrName) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim objDataOperation As clsDataOperation
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try
    '        objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbatchpostingunkid.ToString)
    '        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
    '        objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecAmount.ToString)
    '        objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

    '        strQ = "INSERT INTO premployee_batchposting_tran ( " & _
    '                  "  empbatchpostingunkid" & _
    '                  ", periodunkid " & _
    '                  ", employeeunkid " & _
    '                  ", tranheadunkid " & _
    '                  ", amount " & _
    '                  ", isposted " & _
    '                  ", userunkid " & _
    '                  ", isvoid " & _
    '                  ", voiduserunkid " & _
    '                  ", voiddatetime " & _
    '                  ", voidreason" & _
    '            ") VALUES (" & _
    '                  "  @empbatchpostingunkid" & _
    '                  ", @periodunkid " & _
    '                  ", @employeeunkid " & _
    '                  ", @tranheadunkid " & _
    '                  ", @amount " & _
    '                  ", @isposted " & _
    '                  ", @userunkid " & _
    '                  ", @isvoid " & _
    '                  ", @voiduserunkid " & _
    '                  ", @voiddatetime " & _
    '                  ", @voidreason" & _
    '            "); SELECT @@identity"

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mintEmpBatchPostingtranUnkId = dsList.Tables(0).Rows(0).Item(0)

    '        If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "premployee_batchposting_tran", "empbatchpostingtranunkid", mintEmpBatchPostingtranUnkId) = False Then
    '            objDataOperation.ReleaseTransaction(False)
    '            Return False
    '        Else
    '            objDataOperation.ReleaseTransaction(True)
    '            Return True
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <returns>Boolean</returns>
    '''' <purpose> Update Database Table (premployee_batchposting_tran) </purpose>
    'Public Function Update(Optional ByVal blnOnlyIfNotPosted As Boolean = False, _
    '                       Optional ByVal blnOnlyIfPeriodOpen As Boolean = False) As Boolean
    '    'If isExist(mstrName, mintEmpbatchpostingtranunkid) Then
    '    '    mstrMessage = "<Message>"
    '    '    Return False
    '    'End If

    '    Dim objDataOperation As clsDataOperation
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation
    '    objDataOperation.BindTransaction()

    '    Try
    '        objDataOperation.AddParameter("@empbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpBatchPostingtranUnkId.ToString)
    '        objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbatchpostingunkid.ToString)
    '        objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodUnkId.ToString)
    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
    '        objDataOperation.AddParameter("@tranheadunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTranheadunkid.ToString)
    '        objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.FLOAT_SIZE, mdecAmount.ToString)
    '        objDataOperation.AddParameter("@isposted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsposted.ToString)
    '        objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
    '        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
    '        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
    '        If mdtVoiddatetime = Nothing Then
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
    '        Else
    '            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
    '        End If
    '        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

    '        strQ = "UPDATE premployee_batchposting_tran SET " & _
    '                    "  periodunkid = @periodunkid" & _
    '                    ", employeeunkid = @employeeunkid" & _
    '                    ", tranheadunkid = @tranheadunkid" & _
    '                    ", amount = @amount" & _
    '                    ", isposted = @isposted" & _
    '                    ", userunkid = @userunkid" & _
    '                    ", isvoid = @isvoid" & _
    '                    ", voiduserunkid = @voiduserunkid" & _
    '                    ", voiddatetime = @voiddatetime" & _
    '                    ", voidreason = @voidreason " & _
    '            "FROM    cfcommon_period_tran " & _
    '            "WHERE   cfcommon_period_tran.periodunkid = premployee_batchposting_tran.periodunkid " & _
    '            "AND empbatchpostingtranunkid = @empbatchpostingtranunkid "


    '        If blnOnlyIfNotPosted = True Then
    '            strQ &= " AND premployee_batchposting_tran.isposted = 0 "
    '        End If

    '        If blnOnlyIfPeriodOpen = True Then
    '            strQ &= " AND cfcommon_period_tran.statusid = 1 "
    '        End If

    '        Call objDataOperation.ExecNonQuery(strQ)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", mintEmpbatchpostingunkid, "premployee_batchposting_tran", "empbatchpostingtranunkid", mintEmpBatchPostingtranUnkId, 2, 2) = False Then
    '            objDataOperation.ReleaseTransaction(False)
    '            Return False
    '        End If


    '        objDataOperation.ReleaseTransaction(True)
    '        Return True

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
    '        Return False
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (premployee_batchposting_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, _
                           ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, _
                           Optional ByVal blnOnlyIfNotPosted As Boolean = False, _
                           Optional ByVal blnOnlyIfPeriodOpen As Boolean = False) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, You cannot delete this Employee Batch Posting. This Employee Batch Posting is in use.")
        '    Return False
        'End If

        Dim objDataOperation As clsDataOperation
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            'Sohail (20 Jun 2017) -- Start
            mintUserunkid = intVoidUserID
            mstrVoidreason = strVoidReason
            mdtVoiddatetime = dtVoidDateTime
            'Sohail (20 Jun 2017) -- End

            strQ = "UPDATE premployee_batchposting_tran SET " & _
                        "  isvoid = 1 " & _
                        ", voiduserunkid = @voiduserunkid" & _
                        ", voiddatetime = @voiddatetime" & _
                        ", voidreason = @voidreason " & _
                   "WHERE isvoid = 0 " & _
                   "AND empbatchpostingtranunkid = @empbatchpostingtranunkid "

            If blnOnlyIfNotPosted = True Then
                strQ &= " AND premployee_batchposting_tran.isposted = 0 "
            End If

            If blnOnlyIfPeriodOpen = True Then
                strQ &= " AND cfcommon_period_tran.statusid = 1 "
            End If

            objDataOperation.AddParameter("@empbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, ConfigParameter._Object._CurrentDateAndTime))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnToInsert As Boolean
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "premployee_batchposting_tran", intUnkid, "empbatchpostingtranunkid", 3) Then
                blnToInsert = True
            Else
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If blnToInsert = True AndAlso clsCommonATLog.Insert_AtLog(objDataOperation, 3, "premployee_batchposting_tran", "empbatchpostingtranunkid", intUnkid) = False Then
            '    objDataOperation.ReleaseTransaction(False)
            '    Return False
            'Else
            '    objDataOperation.ReleaseTransaction(True)
            '    Return True
            'End If

            If blnToInsert = True AndAlso clsCommonATLog.Insert_AtLog(objDataOperation, 3, "premployee_batchposting_tran", "empbatchpostingtranunkid", intUnkid, False, mintUserunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            Else
                objDataOperation.ReleaseTransaction(True)
                Return True
            End If

            'Pinkal (24-May-2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function VoidAll(ByVal objDataOperation As clsDataOperation, ByVal intUnkid As Integer, _
                           ByVal intVoidUserID As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, _
                           Optional ByVal blnOnlyIfNotPosted As Boolean = False, _
                           Optional ByVal blnOnlyIfPeriodOpen As Boolean = False) As Boolean    ' Pinkal (24-May-2013) [ objDataOperation]
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, You cannot delete this Employee Batch Posting. This Employee Batch Posting is in use.")
        '    Return False
        'End If

        'Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strFilter As String = ""
        Dim strATFilter As String = ""


        'Pinkal (24-May-2013) -- Start
        'Enhancement : TRA Changes
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()

        'Pinkal (24-May-2013) -- End


        Try


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes
            mintUserunkid = intVoidUserID
            mstrVoidreason = strVoidReason
            'Pinkal (24-May-2013) -- End
            mdtVoiddatetime = dtVoidDateTime 'Sohail (20 Jun 2017)

            If blnOnlyIfNotPosted = True Then
                strFilter &= " AND premployee_batchposting_tran.isposted = 0 AND premployee_batchposting_tran.isvoid = 0 "
                strATFilter = " premployee_batchposting_tran.isposted = 0 AND premployee_batchposting_tran.isvoid = 0 "
            End If

            If blnOnlyIfPeriodOpen = True Then
                strFilter &= " AND cfcommon_period_tran.statusid = 1 "
            End If


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes

            'If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", intUnkid, "premployee_batchposting_tran", "empbatchpostingtranunkid", 3, 3, , strATFilter) = False Then
            '    objDataOperation.ReleaseTransaction(False)
            '    Return False
            'End If

            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "premployee_batchposting_master", "empbatchpostingunkid", intUnkid, "premployee_batchposting_tran", "empbatchpostingtranunkid", 3, 3, , strATFilter, False, mintUserunkid) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            'Pinkal (24-May-2013) -- End

            strQ = "UPDATE premployee_batchposting_tran SET " & _
                        "  premployee_batchposting_tran.isvoid = 1 " & _
                        ", premployee_batchposting_tran.voiduserunkid = @voiduserunkid" & _
                        ", premployee_batchposting_tran.voiddatetime = @voiddatetime" & _
                        ", premployee_batchposting_tran.voidreason = @voidreason " & _
                   "FROM    premployee_batchposting_master, cfcommon_period_tran " & _
                   "WHERE   premployee_batchposting_master.empbatchpostingunkid = premployee_batchposting_tran.empbatchpostingunkid " & _
                   "AND cfcommon_period_tran.periodunkid = premployee_batchposting_tran.periodunkid " & _
                   "AND  premployee_batchposting_tran.isvoid = 0 " & _
                   "AND premployee_batchposting_tran.empbatchpostingunkid = @empbatchpostingunkid "

            strQ &= strFilter

            objDataOperation.AddParameter("@empbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, ConfigParameter._Object._CurrentDateAndTime))
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Dim intRowsAffected As Integer = objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                objDataOperation.ReleaseTransaction(False)
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes
            ' objDataOperation.ReleaseTransaction(True)
            'Pinkal (24-May-2013) -- End

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: VoidAll; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()

            'Pinkal (24-May-2013) -- Start
            'Enhancement : TRA Changes
            'objDataOperation = Nothing
            'Pinkal (24-May-2013) -- End

        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@empbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              ", ISNULL(premployee_batchposting_tran.empbatchpostingunkid, 0) AS empbatchpostingunkid " & _
              "  empbatchpostingtranunkid " & _
              ", periodunkid " & _
              ", employeeunkid " & _
              ", tranheadunkid " & _
              ", amount " & _
              ", isposted " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM premployee_batchposting_tran " & _
             "WHERE name = @name " & _
             "AND code = @code "

            If intUnkid > 0 Then
                strQ &= " AND empbatchpostingtranunkid <> @empbatchpostingtranunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@empbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

#End Region

End Class
