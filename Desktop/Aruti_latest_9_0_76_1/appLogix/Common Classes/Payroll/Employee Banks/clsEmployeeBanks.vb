﻿'************************************************************************************************************************************
'Class Name : clsEmployeeBanks.vb
'Purpose    :
'Date       :16/07/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsEmployeeBanks
    Private Shared ReadOnly mstrModuleName As String = "clsEmployeeBanks"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintEmpbanktranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintBranchunkid As Integer
    Private mintAccounttypeunkid As Integer
    Private mstrAccountno As String = String.Empty
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    'Sohail (16 Oct 2010) -- Start
    Private mdblPercentage As Double
    Private mdtTran As DataTable
    'Sohail (16 Oct 2010) -- End

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'Sohail (21 Apr 2014) -- Start
    'Enhancement - Salary Distribution by Amount and bank priority.
    Private mintModeid As Integer = 2 'Percentage
    Private mdecAmount As Decimal = 0
    Private mintPriority As Integer = 1
    'Sohail (21 Apr 2014) -- End
    Private mintPeriodunkid As Integer = 0 'Sohail (25 Apr 2014)

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empbanktranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Empbanktranunkid() As Integer
        Get
            Return mintEmpbanktranunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpbanktranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        'Set(ByVal value As Integer)
        Set(ByVal value As Integer)
            'Sohail (21 Aug 2015) -- End
            mintEmployeeunkid = value
            'Call GetEmployeeBank_Tran() 'Sohail (16 Oct 2010)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set branchunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Branchunkid() As Integer
        Get
            Return mintBranchunkid
        End Get
        Set(ByVal value As Integer)
            mintBranchunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accounttypeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Accounttypeunkid() As Integer
        Get
            Return mintAccounttypeunkid
        End Get
        Set(ByVal value As Integer)
            mintAccounttypeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accountno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Accountno() As String
        Get
            Return mstrAccountno
        End Get
        Set(ByVal value As String)
            mstrAccountno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    'Sohail (16 Oct 2010) -- Start
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
    'Sohail (16 Oct 2010) -- End

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'Hemant (26 May 2023) -- Start
    'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
    Public Property _Percetage() As Double
        Get
            Return mdblPercentage
        End Get
        Set(ByVal value As Double)
            mdblPercentage = value
        End Set
    End Property

    'Hemant (26 May 2023) -- End

    'Sohail (21 Apr 2014) -- Start
    'Enhancement - Salary Distribution by Amount and bank priority.
    Public Property _Modeid() As Integer
        Get
            Return mintModeid
        End Get
        Set(ByVal value As Integer)
            mintModeid = value
        End Set
    End Property

    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property
    'Sohail (21 Apr 2014) -- End

    'Sohail (25 Apr 2014) -- Start
    'Enhancement - Employee Bank Details Period Wise.
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property
    'Sohail (25 Apr 2014) -- End

    'Sohail (12 Jun 2023) -- Start
    'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
    Private mintPayment_typeid As Integer
    Public Property _Payment_typeid() As Integer
        Get
            Return mintPayment_typeid
        End Get
        Set(ByVal value As Integer)
            mintPayment_typeid = value
        End Set
    End Property

    Private mstrMobileno As String = ""
    Public Property _Mobileno() As String
        Get
            Return mstrMobileno
        End Get
        Set(ByVal value As String)
            mstrMobileno = value
        End Set
    End Property
    'Sohail (12 Jun 2023) -- End

#End Region

    'Sohail (16 Oct 2010) -- Start
#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("EmpBanks")
        Dim dCol As DataColumn
        Try

            'Sohail (21 Apr 2014) -- Start
            'Enhancement - Salary Distribution by Amount and bank priority.
            dCol = New DataColumn("UnkId")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.AutoIncrementSeed = 1
            dCol.AutoIncrement = True
            mdtTran.Columns.Add(dCol)
            'Sohail (21 Apr 2014) -- End

            dCol = New DataColumn("empbanktranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("bankgroupunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("bankgroup")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("branchunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("branchname")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("accounttypeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("acctypename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("accountno")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("percentage")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            'Sohail (21 Apr 2014) -- Start
            'Enhancement - Salary Distribution by Amount and bank priority.
            dCol = New DataColumn("modeid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("priority")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)
            'Sohail (21 Apr 2014) -- End

            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            dCol = New DataColumn("periodunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("period_name")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("end_date")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)
            'Sohail (25 Apr 2014) -- End

            'Sohail (12 Jun 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            dCol = New DataColumn("payment_typeid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("mobileno")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)
            'Sohail (12 Jun 2023) -- End

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Private Sub GetEmployeeBank_Tran()
    'Dim strQ As String = ""
    'Dim strErrorMessage As String = ""
    'Dim dsList As New DataSet
    'Dim dRowID_Tran As DataRow
    'Dim exForce As Exception
    '    Try
    '        objDataOperation = New clsDataOperation

    '        strQ = "SELECT " & _
    '                    " premployee_bank_tran.empbanktranunkid " & _
    '                    ",premployee_bank_tran.employeeunkid " & _
    '                    ",premployee_bank_tran.branchunkid " & _
    '                    ",premployee_bank_tran.accounttypeunkid " & _
    '                    ",hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankGrpUnkid " & _
    '                    ",premployee_bank_tran.accountno " & _
    '                    ",hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
    '                    ",hrmsConfiguration..cfpayrollgroup_master.groupname AS BankGrp " & _
    '                    ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                    ",hrmsConfiguration..cfbankacctype_master.accounttype_name AS AccName " & _
    '                    ",premployee_bank_tran.percentage " & _
    '                    ",ISNULL(premployee_bank_tran.modeid, 2) AS modeid " & _
    '                    ",ISNULL(premployee_bank_tran.amount, 0) AS amount " & _
    '                    ",ISNULL(premployee_bank_tran.priority, 1) AS priority " & _
    '                    ", ISNULL(premployee_bank_tran.periodunkid, 0) AS periodunkid " & _
    '                    ", cfcommon_period_tran.period_name " & _
    '                    ", CONVERT(CHAR(8), cfcommon_period_tran.start_date,112) AS start_date " & _
    '                    ", CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) AS end_date " & _
    '                    ",'' As AUD " & _
    '                "FROM premployee_bank_tran " & _
    '                    "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
    '                    "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id =   " & enPayrollGroupType.Bank & _
    '                    "LEFT JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "LEFT JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
    '                    "LEFT JOIN dbo.cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '                "WHERE ISNULL(isvoid,0) = 0 " & _
    '                "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
    '                "AND premployee_bank_tran.employeeunkid = @employeeunkid " 'Sohail (28 Dec 2010)
    '        'Sohail (25 Apr 2014) - [periodunkid]
    '        'Sohail (21 Apr 2014) - [modeid, amount, priority]

    '        'Anjan (09 Aug 2011)-Start
    '        'Issue : For including setting of acitve and inactive employee.
    '        'comment reason : when employee is terminated in middle of payroll period then payment was not falling into emp_salary_Tran and was falling into payment_tran table.
    '        'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '        '    'Sohail (06 Jan 2012) -- Start
    '        '    'TRA - ENHANCEMENT
    '        '    'strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '        '    strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '        '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '        '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '        '    'Sohail (06 Jan 2012) -- End
    '        'End If
    '        'Anjan (09 Aug 2011)-End 

    '        'Sohail (24 Jun 2011) -- Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.


    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If

    '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
    '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        'End Select
    '        strQ &= UserAccessLevel._AccessLevelFilterString
    '        'S.SANDEEP [ 01 JUNE 2012 ] -- END


    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Sohail (24 Jun 2011) -- End

    '        'Sohail (25 Apr 2014) -- Start
    '        'Enhancement - Employee Bank Details Period Wise.
    '        'strQ &= " ORDER BY ISNULL(premployee_bank_tran.priority, 1) " 'Sohail (21 Apr 2014)
    '        strQ &= " ORDER BY cfcommon_period_tran.end_date, ISNULL(premployee_bank_tran.priority, 1) "
    '        'Sohail (25 Apr 2014) -- End

    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mdtTran.Clear()


    '        For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
    '            With dsList.Tables("List").Rows(i)
    '                dRowID_Tran = mdtTran.NewRow()

    '                dRowID_Tran.Item("empbanktranunkid") = .Item("empbanktranunkid")
    '                dRowID_Tran.Item("employeeunkid") = .Item("employeeunkid")
    '                dRowID_Tran.Item("employeename") = .Item("EmpName")
    '                dRowID_Tran.Item("bankgroupunkid") = .Item("bankgrpunkid")
    '                dRowID_Tran.Item("bankgroup") = .Item("bankgrp")
    '                dRowID_Tran.Item("branchunkid") = .Item("branchunkid")
    '                dRowID_Tran.Item("branchname") = .Item("branchname")
    '                dRowID_Tran.Item("accounttypeunkid") = .Item("accounttypeunkid")
    '                dRowID_Tran.Item("acctypename") = .Item("accname")
    '                dRowID_Tran.Item("accountno") = .Item("accountno")

    '                dRowID_Tran.Item("percentage") = .Item("percentage")
    '                'Sohail (21 Apr 2014) -- Start
    '                'Enhancement - Salary Distribution by Amount and bank priority.
    '                dRowID_Tran.Item("modeid") = .Item("modeid")
    '                dRowID_Tran.Item("amount") = .Item("amount")
    '                dRowID_Tran.Item("priority") = .Item("priority")
    '                'Sohail (21 Apr 2014) -- End
    '                'Sohail (25 Apr 2014) -- Start
    '                'Enhancement - Employee Bank Details Period Wise.
    '                dRowID_Tran.Item("periodunkid") = .Item("periodunkid")
    '                dRowID_Tran.Item("period_name") = .Item("period_name").ToString
    '                dRowID_Tran.Item("end_date") = .Item("end_date").ToString
    '                'Sohail (25 Apr 2014) -- End
    '                dRowID_Tran.Item("AUD") = .Item("AUD")

    '                mdtTran.Rows.Add(dRowID_Tran)
    '            End With
    '        Next
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetEmployeeBank_Tran", mstrModuleName)
    '    End Try
    'End Sub
    Public Sub GetEmployeeBank_Tran(ByVal intEmpUnkId As Integer _
                                    , ByVal xDatabaseName As String _
                                    , ByVal xUserUnkid As Integer _
                                    , ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer _
                                    , ByVal xPeriodStart As DateTime _
                                    , ByVal xPeriodEnd As DateTime _
                                    , ByVal xUserModeSetting As String _
                                    , ByVal xOnlyApproved As Boolean _
                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                    , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                    , Optional ByVal strFilerString As String = "" _
                                    )

        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRowID_Tran As DataRow
        Dim exForce As Exception
        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation

            strQ = "SELECT " & _
                        " premployee_bank_tran.empbanktranunkid " & _
                        ",premployee_bank_tran.employeeunkid " & _
                        ",premployee_bank_tran.branchunkid " & _
                        ",premployee_bank_tran.accounttypeunkid " & _
                        ",hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankGrpUnkid " & _
                        ",premployee_bank_tran.accountno " & _
                        ",hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
                        ",hrmsConfiguration..cfpayrollgroup_master.groupname AS BankGrp " & _
                        ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                        ",hrmsConfiguration..cfbankacctype_master.accounttype_name AS AccName " & _
                        ",premployee_bank_tran.percentage " & _
                        ",ISNULL(premployee_bank_tran.modeid, 2) AS modeid " & _
                        ",ISNULL(premployee_bank_tran.amount, 0) AS amount " & _
                        ",ISNULL(premployee_bank_tran.priority, 1) AS priority " & _
                        ", ISNULL(premployee_bank_tran.periodunkid, 0) AS periodunkid " & _
                        ", cfcommon_period_tran.period_name " & _
                        ", CONVERT(CHAR(8), cfcommon_period_tran.start_date,112) AS start_date " & _
                        ", CONVERT(CHAR(8), cfcommon_period_tran.end_date,112) AS end_date " & _
                        ", ISNULL(premployee_bank_tran.payment_typeid, 0) AS payment_typeid " & _
                        ", ISNULL(premployee_bank_tran.mobileno, '') AS mobileno " & _
                        ",'' As AUD " & _
                    "FROM premployee_bank_tran " & _
                        "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                        "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id =   " & enPayrollGroupType.Bank & _
                        "LEFT JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "LEFT JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
                        "LEFT JOIN dbo.cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid "
            'Sohail (12 Jun 2023) - [payment_typeid, mobileno]

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE ISNULL(isvoid,0) = 0 " & _
                    "AND cfcommon_period_tran.modulerefid = " & enModuleReference.Payroll & " " & _
                    "AND premployee_bank_tran.employeeunkid = @employeeunkid "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strFilerString.Trim.Length > 0 Then
                strQ &= " AND " & strFilerString
            End If
            
            strQ &= " ORDER BY cfcommon_period_tran.end_date, ISNULL(premployee_bank_tran.priority, 1) "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()


            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowID_Tran = mdtTran.NewRow()

                    dRowID_Tran.Item("empbanktranunkid") = .Item("empbanktranunkid")
                    dRowID_Tran.Item("employeeunkid") = .Item("employeeunkid")
                    dRowID_Tran.Item("employeename") = .Item("EmpName")
                    dRowID_Tran.Item("bankgroupunkid") = .Item("bankgrpunkid")
                    dRowID_Tran.Item("bankgroup") = .Item("bankgrp")
                    dRowID_Tran.Item("branchunkid") = .Item("branchunkid")
                    dRowID_Tran.Item("branchname") = .Item("branchname")
                    dRowID_Tran.Item("accounttypeunkid") = .Item("accounttypeunkid")
                    dRowID_Tran.Item("acctypename") = .Item("accname")
                    dRowID_Tran.Item("accountno") = .Item("accountno")

                    dRowID_Tran.Item("percentage") = .Item("percentage")
                    dRowID_Tran.Item("modeid") = .Item("modeid")
                    dRowID_Tran.Item("amount") = .Item("amount")
                    dRowID_Tran.Item("priority") = .Item("priority")
                    dRowID_Tran.Item("periodunkid") = .Item("periodunkid")
                    dRowID_Tran.Item("period_name") = .Item("period_name").ToString
                    dRowID_Tran.Item("end_date") = .Item("end_date").ToString
                    'Sohail (12 Jun 2023) -- Start
                    'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
                    dRowID_Tran.Item("payment_typeid") = .Item("payment_typeid")
                    dRowID_Tran.Item("mobileno") = .Item("mobileno")
                    'Sohail (12 Jun 2023) -- End
                    dRowID_Tran.Item("AUD") = .Item("AUD")

                    mdtTran.Rows.Add(dRowID_Tran)
                End With
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeBank_Tran", mstrModuleName)
        End Try
    End Sub
    'Sohail (21 Aug 2015) -- End

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (16 Oct 2010) -- Start
            'Changes : New column 'percentage' added.
            'strQ = "SELECT " & _
            '  "  empbanktranunkid " & _
            '  ", employeeunkid " & _
            '  ", branchunkid " & _
            '  ", accounttypeunkid " & _
            '  ", accountno " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            ' "FROM premployee_bank_tran " & _
            ' "WHERE empbanktranunkid = @empbanktranunkid "
            strQ = "SELECT " & _
              "  empbanktranunkid " & _
              ", employeeunkid " & _
              ", branchunkid " & _
              ", accounttypeunkid " & _
              ", accountno " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
                        ", percentage " & _
              ", ISNULL(modeid, 2) AS modeid " & _
              ", ISNULL(amount, 0) AS amount " & _
              ", ISNULL(priority, 1) AS priority " & _
              ", ISNULL(periodunkid, 1) AS periodunkid " & _
              ", ISNULL(payment_typeid, 0) AS payment_typeid " & _
              ", ISNULL(mobileno, '') AS mobileno " & _
             "FROM premployee_bank_tran " & _
             "WHERE empbanktranunkid = @empbanktranunkid "
            'Sohail (12 Jun 2023) - [payment_typeid, mobileno]
            'Sohail (21 Apr 2014) - [modeid, amount, priority]
            'Sohail (16 Oct 2010) -- End

            objDataOperation.AddParameter("@empbanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbanktranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEmpbanktranunkid = CInt(dtRow.Item("empbanktranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintBranchunkid = CInt(dtRow.Item("branchunkid"))
                mintAccounttypeunkid = CInt(dtRow.Item("accounttypeunkid"))
                mstrAccountno = dtRow.Item("accountno").ToString
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                'Sohail (30 Aug 2010) -- Start
                'Issue:Conversion from type 'DBNull' to type 'Date' is not valid.
                'mdtVoiddatetime = dtRow.Item("voiddatetime")
                If dtRow.Item("voiddatetime").ToString = Nothing Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                'Sohail (30 Aug 2010) -- End
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mdblPercentage = CDec(dtRow.Item("percentage")) 'Sohail (11 Nov 2010)
                'Sohail (21 Apr 2014) -- Start
                'Enhancement - Salary Distribution by Amount and bank priority.
                mdecAmount = CDec(dtRow.Item("amount"))
                mintModeid = CInt(dtRow.Item("modeid"))
                mintPriority = CInt(dtRow.Item("priority"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintPayment_typeid = CInt(dtRow.Item("payment_typeid"))
                mstrMobileno = dtRow.Item("mobileno").ToString
                'Sohail (21 Apr 2014) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    '''' <summary>
    '''' Modify By: Sandeep J. Sharma
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, _
    '                        Optional ByVal strEmpUnkIDs As String = "", _
    '                        Optional ByVal dtPeriodEndDate As DateTime = Nothing, _
    '                        Optional ByVal strFilter As String = "", _
    '                        Optional ByVal strSortField As String = "", _
    '                        Optional ByVal intPeriodID As Integer = 0, _
    '                        Optional ByVal strAccessLevelFilterString As String = "" _
    '                        ) As DataSet
    '    'Sohail (25 Mar 2015) - [strAccessLevelFilterString]
    '    'Sohail (22 May 2014) - [intPeriodID]
    '    'Sohail (25 Apr 2014) - [strEmpUnkIDs, dtPeriodEndDate, strSortField]
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        'Sohail (16 Oct 2010) -- Start
    '        'Changes : New column 'percentage' added.
    '        'strQ = "SELECT " & _
    '        '            " premployee_bank_tran.empbanktranunkid " & _
    '        '            ",premployee_bank_tran.employeeunkid " & _
    '        '            ",premployee_bank_tran.branchunkid " & _
    '        '            ",premployee_bank_tran.accounttypeunkid " & _
    '        '            ",prpayrollgroup_master.groupmasterunkid AS BankGrpUnkid " & _
    '        '            ",premployee_bank_tran.accountno " & _
    '        '            ",prbankbranch_master.branchname AS BranchName " & _
    '        '            ",prpayrollgroup_master.groupname AS BankGrp " & _
    '        '            ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '         ",prbankacctype_master.accounttype_name AS AccName " & _
    '        '        "FROM premployee_bank_tran " & _
    '        '            "LEFT JOIN prbankbranch_master ON premployee_bank_tran.branchunkid = prbankbranch_master.branchunkid " & _
    '        '            "LEFT JOIN prpayrollgroup_master ON prbankbranch_master.bankgroupunkid = prpayrollgroup_master.groupmasterunkid AND grouptype_id =   " & enPayrollGroupType.Bank & _
    '        '            "LEFT JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '        '            "LEFT JOIN prbankacctype_master ON premployee_bank_tran.accounttypeunkid = prbankacctype_master.accounttypeunkid " & _
    '        '            " WHERE ISNULL(isvoid,0) = 0 "

    '        'Sohail (25 Apr 2014) -- Start
    '        'Enhancement - Employee Bank Details Period Wise.
    '        'strQ = "SELECT " & _
    '        '            " premployee_bank_tran.empbanktranunkid " & _
    '        '            ",premployee_bank_tran.employeeunkid " & _
    '        '            ",premployee_bank_tran.branchunkid " & _
    '        '            ",premployee_bank_tran.accounttypeunkid " & _
    '        '            ",hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankGrpUnkid " & _
    '        '            ",premployee_bank_tran.accountno " & _
    '        '            ",hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
    '        '            ",hrmsConfiguration..cfpayrollgroup_master.groupname AS BankGrp " & _
    '        '            ",ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
    '        '            ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '        '            ",hrmsConfiguration..cfbankacctype_master.accounttype_name AS AccName " & _
    '        '            ",premployee_bank_tran.percentage " & _
    '        '            ",ISNULL(premployee_bank_tran.modeid, 2) AS modeid " & _
    '        '            ",ISNULL(premployee_bank_tran.amount, 0) AS amount " & _
    '        '            ",ISNULL(premployee_bank_tran.priority, 1) AS priority " & _
    '        '            ",ISNULL(premployee_bank_tran.periodunkid, 1) AS periodunkid " & _
    '        '            ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
    '        '            ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date,112), '19000101') AS start_date " & _
    '        '            ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date,112), '19000101') AS end_date " & _
    '        '            ", DENSE_RANK() OVER ( PARTITION  BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO "
    '        strQ = "SELECT  empbanktranunkid " & _
    '                     ", employeeunkid " & _
    '                     ", employeecode " & _
    '                     ", EmpName " & _
    '                     ", BankGrpUnkid " & _
    '                     ", BankGrp " & _
    '                     ", branchunkid " & _
    '                     ", BranchName " & _
    '                     ", accounttypeunkid " & _
    '                     ", AccName " & _
    '                     ", accountno " & _
    '                     ", percentage " & _
    '                     ", modeid " & _
    '                     ", amount " & _
    '                     ", priority " & _
    '                     ", periodunkid " & _
    '                     ", period_name " & _
    '                     ", start_date " & _
    '                     ", end_date " & _
    '                     ", '' As AUD " & _
    '                     ", '' As GUID " & _
    '               "FROM    ( " & _
    '                            "SELECT  premployee_bank_tran.empbanktranunkid " & _
    '                    ",premployee_bank_tran.employeeunkid " & _
    '                                 ", ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
    '                                 ", ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                                 ", hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankGrpUnkid " & _
    '                                 ", hrmsConfiguration..cfpayrollgroup_master.groupname AS BankGrp " & _
    '                    ",premployee_bank_tran.branchunkid " & _
    '                                 ", hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
    '                    ",premployee_bank_tran.accounttypeunkid " & _
    '                                 ", hrmsConfiguration..cfbankacctype_master.accounttype_name AS AccName " & _
    '                    ",premployee_bank_tran.accountno " & _
    '                    ",premployee_bank_tran.percentage " & _
    '                    ",ISNULL(premployee_bank_tran.modeid, 2) AS modeid " & _
    '                    ",ISNULL(premployee_bank_tran.amount, 0) AS amount " & _
    '                                 ", ISNULL(premployee_bank_tran.priority, 1) AS priority " & _
    '                                 ", ISNULL(premployee_bank_tran.periodunkid, 1) AS periodunkid " & _
    '                                 ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
    '                                 ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date,112), '19000101') AS start_date " & _
    '                                 ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date,112), '19000101') AS end_date " & _
    '                                 ", DENSE_RANK() OVER ( PARTITION  BY premployee_bank_tran.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO "
    '        'Sohail (25 Apr 2014) -- End
    '        'Sohail (21 Apr 2014) - [modeid, amount, priority]
    '        'Sohail (28 Jan 2012) - [employeecode]
    '        'Anjan (21 Nov 2011)-Start
    '        'ENHANCEMENT : TRA COMMENTS
    '        strQ &= " ,ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
    '                 ",ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
    '                 ",ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
    '                 ",ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
    '                 ",ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
    '                 ",ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
    '                 ",ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
    '                 ",ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
    '                 ",ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
    '                 ",ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
    '                 ",ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
    '                 ",ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
    '                 ",ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
    '                 ",ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid "
    '        'Anjan (21 Nov 2011)-End 

    '        strQ &= "FROM premployee_bank_tran " & _
    '                "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
    '                "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id =   " & enPayrollGroupType.Bank & _
    '                    "LEFT JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "LEFT JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
    '                    "LEFT JOIN cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
    '                    " WHERE ISNULL(isvoid,0) = 0 "
    '        'Sohail (28 Dec 2010)
    '        'Sohail (16 Oct 2010) -- End


    '        'Sohail (25 Apr 2014) -- Start
    '        'Enhancement - Employee Bank Details Period Wise.
    '        If dtPeriodEndDate <> Nothing Then
    '            strQ &= "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
    '            objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEndDate))
    '        End If

    '        If strEmpUnkIDs.Trim <> "" AndAlso strEmpUnkIDs.Trim <> "0" AndAlso strEmpUnkIDs.Trim <> "-1" Then
    '            strQ &= "AND premployee_bank_tran.employeeunkid IN (" & strEmpUnkIDs & ") "
    '        End If
    '        'Sohail (25 Apr 2014) -- End

    '        'Anjan (09 Aug 2011)-Start
    '        'Issue : For including setting of acitve and inactive employee.
    '        If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
    '            '    strQ &= " AND ISNULL(hremployee_master.isactive,0) = 1 "
    '            'Sohail (06 Jan 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
    '                       " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

    '            If intPeriodID > 0 Then
    '                Dim objPeriod As New clscommom_period_Tran
    '                objPeriod._Periodunkid = intPeriodID
    '                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._Start_Date))
    '                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(objPeriod._End_Date))
    '                objPeriod = Nothing
    '            Else
    '                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
    '            End If
    '            'Sohail (06 Jan 2012) -- End
    '        End If
    '        'Anjan (09 Aug 2011)-End 

    '        'Sohail (24 Jun 2011) -- Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.

    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If

    '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
    '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        'End Select
    '        'Sohail (30 Mar 2015) -- Start
    '        'Enhancement - Allow to switch User from MSS to ESS and vice versa if he is imported Employee as User.
    '        'strQ &= UserAccessLevel._AccessLevelFilterString
    '        If strAccessLevelFilterString.Trim <> "" Then
    '            strQ &= strAccessLevelFilterString
    '        Else
    '            strQ &= UserAccessLevel._AccessLevelFilterString
    '        End If
    '        'Sohail (30 Mar 2015) -- End
    '        'S.SANDEEP [ 01 JUNE 2012 ] -- END


    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Sohail (24 Jun 2011) -- End

    '        'Sohail (25 Apr 2014) -- Start
    '        'Enhancement - Employee Bank Details Period Wise.
    '        If strFilter.Length > 0 Then
    '            strQ &= "AND " & strFilter
    '        End If

    '        strQ &= " ) AS A "

    '        strQ &= "WHERE   1 = 1 "

    '        If dtPeriodEndDate <> Nothing Then
    '            strQ &= " AND   ROWNO = 1 "
    '        End If

    '        If strSortField.Trim.Length > 0 Then
    '            strQ += " ORDER BY " & strSortField
    '        End If
    '        'Sohail (25 Apr 2014) -- End

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDatabaseName As String, _
                            ByVal xUserUnkid As Integer, _
                            ByVal xYearUnkid As Integer, _
                            ByVal xCompanyUnkid As Integer, _
                            ByVal xPeriodStart As DateTime, _
                            ByVal xPeriodEnd As DateTime, _
                            ByVal xUserModeSetting As String, _
                            ByVal xOnlyApproved As Boolean, _
                            ByVal xIncludeIn_ActiveEmployee As Boolean, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal strAdvanceFilerString As String = "", _
                            Optional ByVal strEmpUnkIDs As String = "", _
                            Optional ByVal dtPeriodEndDate As DateTime = Nothing, _
                            Optional ByVal strSortField As String = "", _
                            Optional ByVal strFilter As String = "" _
                            ) As DataSet
        'Sohail (12 Oct 2021) - [strFilerString=strAdvanceFilerString, strFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'strQ = "SELECT " & _
            '            " premployee_bank_tran.empbanktranunkid " & _
            '            ",premployee_bank_tran.employeeunkid " & _
            '            ",premployee_bank_tran.branchunkid " & _
            '            ",premployee_bank_tran.accounttypeunkid " & _
            '            ",prpayrollgroup_master.groupmasterunkid AS BankGrpUnkid " & _
            '            ",premployee_bank_tran.accountno " & _
            '            ",prbankbranch_master.branchname AS BranchName " & _
            '            ",prpayrollgroup_master.groupname AS BankGrp " & _
            '            ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '         ",prbankacctype_master.accounttype_name AS AccName " & _
            '        "FROM premployee_bank_tran " & _
            '            "LEFT JOIN prbankbranch_master ON premployee_bank_tran.branchunkid = prbankbranch_master.branchunkid " & _
            '            "LEFT JOIN prpayrollgroup_master ON prbankbranch_master.bankgroupunkid = prpayrollgroup_master.groupmasterunkid AND grouptype_id =   " & enPayrollGroupType.Bank & _
            '            "LEFT JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid " & _
            '            "LEFT JOIN prbankacctype_master ON premployee_bank_tran.accounttypeunkid = prbankacctype_master.accounttypeunkid " & _
            '            " WHERE ISNULL(isvoid,0) = 0 "

            'Sohail (25 Apr 2014) -- Start
            'Enhancement - Employee Bank Details Period Wise.
            'strQ = "SELECT " & _
            '            " premployee_bank_tran.empbanktranunkid " & _
            '            ",premployee_bank_tran.employeeunkid " & _
            '            ",premployee_bank_tran.branchunkid " & _
            '            ",premployee_bank_tran.accounttypeunkid " & _
            '            ",hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankGrpUnkid " & _
            '            ",premployee_bank_tran.accountno " & _
            '            ",hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
            '            ",hrmsConfiguration..cfpayrollgroup_master.groupname AS BankGrp " & _
            '            ",ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
            '            ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
            '            ",hrmsConfiguration..cfbankacctype_master.accounttype_name AS AccName " & _
            '            ",premployee_bank_tran.percentage " & _
            '            ",ISNULL(premployee_bank_tran.modeid, 2) AS modeid " & _
            '            ",ISNULL(premployee_bank_tran.amount, 0) AS amount " & _
            '            ",ISNULL(premployee_bank_tran.priority, 1) AS priority " & _
            '            ",ISNULL(premployee_bank_tran.periodunkid, 1) AS periodunkid " & _
            '            ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
            '            ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date,112), '19000101') AS start_date " & _
            '            ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date,112), '19000101') AS end_date " & _
            '            ", DENSE_RANK() OVER ( PARTITION  BY prearningdeduction_master.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO "

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            If blnApplyUserAccessFilter = True Then 'Sohail (12 Oct 2021)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            End If 'Sohail (12 Oct 2021)
            If strAdvanceFilerString.Trim.Length > 0 Then 'Sohail (12 Oct 2021)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)
            End If 'Sohail (12 Oct 2021)

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            strQ = "SELECT   hremployee_master.employeeunkid " & _
                             ", hremployee_master.employeecode " & _
                             ", hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname AS EmpName " & _
                             ", ISNULL(hremployee_master.sectiongroupunkid,0) AS sectiongroupunkid " & _
                             ", ISNULL(hremployee_master.unitgroupunkid,0) AS unitgroupunkid " & _
                             ", ISNULL(hremployee_master.teamunkid,0) AS teamunkid " & _
                             ", ISNULL(hremployee_master.stationunkid,0) AS stationunkid " & _
                             ", ISNULL(hremployee_master.deptgroupunkid,0) AS deptgroupunkid " & _
                             ", ISNULL(hremployee_master.departmentunkid,0) AS departmentunkid " & _
                             ", ISNULL(hremployee_master.sectionunkid,0) AS sectionunkid " & _
                             ", ISNULL(hremployee_master.unitunkid,0) AS unitunkid " & _
                             ", ISNULL(hremployee_master.jobunkid,0) AS jobunkid " & _
                             ", ISNULL(hremployee_master.classgroupunkid,0) AS classgroupunkid " & _
                             ", ISNULL(hremployee_master.classunkid,0) AS classunkid " & _
                             ", ISNULL(hremployee_master.jobgroupunkid,0) AS jobgroupunkid " & _
                             ", ISNULL(hremployee_master.gradegroupunkid,0) AS gradegroupunkid " & _
                             ", ISNULL(hremployee_master.gradeunkid,0) AS gradeunkid " & _
                             ", ISNULL(hremployee_master.gradelevelunkid,0) AS gradelevelunkid " & _
                    "INTO #Tableemp " & _
                    "FROM hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If blnApplyUserAccessFilter = True Then
                If xUACQry.Trim.Length > 0 Then
                    strQ &= xUACQry
                End If
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If strEmpUnkIDs.Trim <> "" AndAlso strEmpUnkIDs.Trim <> "0" AndAlso strEmpUnkIDs.Trim <> "-1" Then
                strQ &= " AND hremployee_master.employeeunkid IN (" & strEmpUnkIDs & ") "
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If
            
            If strAdvanceFilerString.Length > 0 Then
                strQ &= " AND " & strAdvanceFilerString
            End If
            'Sohail (12 Oct 2021) -- End

            strQ &= "SELECT  empbanktranunkid " & _
                         ", employeeunkid " & _
                         ", employeecode " & _
                         ", EmpName " & _
                         ", BankGrpUnkid " & _
                         ", BankGrp " & _
                         ", branchunkid " & _
                         ", BranchName " & _
                         ", accounttypeunkid " & _
                         ", AccName " & _
                         ", accountno " & _
                         ", percentage " & _
                         ", modeid " & _
                         ", amount " & _
                         ", priority " & _
                         ", periodunkid " & _
                         ", period_name " & _
                         ", start_date " & _
                         ", end_date " & _
                         ", payment_typeid " & _
                         ", mobileno " & _
                         ", '' As AUD " & _
                         ", '' As GUID " & _
                   "FROM    ( SELECT  premployee_bank_tran.empbanktranunkid " & _
                                     ", premployee_bank_tran.employeeunkid " & _
                                     ", #Tableemp.employeecode " & _
                                     ", #Tableemp.EmpName " & _
                                     ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid, 0) AS BankGrpUnkid " & _
                                     ", ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupname, '') AS BankGrp " & _
                                     ", premployee_bank_tran.branchunkid " & _
                                     ", ISNULL(hrmsConfiguration..cfbankbranch_master.branchname, '') AS BranchName " & _
                                     ", premployee_bank_tran.accounttypeunkid " & _
                                     ", ISNULL(hrmsConfiguration..cfbankacctype_master.accounttype_name, '') AS AccName " & _
                                     ", premployee_bank_tran.accountno " & _
                                     ", premployee_bank_tran.percentage " & _
                                     ", ISNULL(premployee_bank_tran.modeid, 2) AS modeid " & _
                                     ", ISNULL(premployee_bank_tran.amount, 0) AS amount " & _
                                     ", ISNULL(premployee_bank_tran.priority, 1) AS priority " & _
                                     ", ISNULL(premployee_bank_tran.periodunkid, 1) AS periodunkid " & _
                                     ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
                                     ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.start_date,112), '19000101') AS start_date " & _
                                     ", ISNULL(CONVERT(CHAR(8), cfcommon_period_tran.end_date,112), '19000101') AS end_date " & _
                                     ", ISNULL(premployee_bank_tran.payment_typeid, 0) AS payment_typeid " & _
                                     ", ISNULL(premployee_bank_tran.mobileno, '') AS mobileno " & _
                                     ", ISNULL(#Tableemp.sectiongroupunkid,0) AS sectiongroupunkid " & _
                                     ", ISNULL(#Tableemp.unitgroupunkid,0) AS unitgroupunkid " & _
                                     ", ISNULL(#Tableemp.teamunkid,0) AS teamunkid " & _
                                     ", ISNULL(#Tableemp.stationunkid,0) AS stationunkid " & _
                                     ", ISNULL(#Tableemp.deptgroupunkid,0) AS deptgroupunkid " & _
                                     ", ISNULL(#Tableemp.departmentunkid,0) AS departmentunkid " & _
                                     ", ISNULL(#Tableemp.sectionunkid,0) AS sectionunkid " & _
                                     ", ISNULL(#Tableemp.unitunkid,0) AS unitunkid " & _
                                     ", ISNULL(#Tableemp.jobunkid,0) AS jobunkid " & _
                                     ", ISNULL(#Tableemp.classgroupunkid,0) AS classgroupunkid " & _
                                     ", ISNULL(#Tableemp.classunkid,0) AS classunkid " & _
                                     ", ISNULL(#Tableemp.jobgroupunkid,0) AS jobgroupunkid " & _
                                     ", ISNULL(#Tableemp.gradegroupunkid,0) AS gradegroupunkid " & _
                                     ", ISNULL(#Tableemp.gradeunkid,0) AS gradeunkid " & _
                                     ", ISNULL(#Tableemp.gradelevelunkid,0) AS gradelevelunkid " & _
                                    ", DENSE_RANK() OVER ( PARTITION  BY premployee_bank_tran.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                             "FROM   premployee_bank_tran " & _
                                    "JOIN #Tableemp ON premployee_bank_tran.employeeunkid = #Tableemp.employeeunkid " & _
                    "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                    "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id =   " & enPayrollGroupType.Bank & _
                        "LEFT JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
                                    "LEFT JOIN cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid "
            'Sohail (12 Jun 2023) - [payment_typeid, mobileno]
            'Sohail (12 Oct 2021) - [hremployee_master=#Tableemp]

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'If xDateJoinQry.Trim.Length > 0 Then
            '    strQ &= xDateJoinQry
            'End If

            ''S.SANDEEP [15 NOV 2016] -- START
            ''If xUACQry.Trim.Length > 0 Then
            ''    StrQ &= xUACQry
            ''End If
            'If blnApplyUserAccessFilter = True Then
            '    If xUACQry.Trim.Length > 0 Then
            '        strQ &= xUACQry
            '    End If
            'End If
            ''S.SANDEEP [15 NOV 2016] -- END

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    strQ &= xAdvanceJoinQry
            'End If
            'Sohail (12 Oct 2021) -- End

            strQ &= "         WHERE  ISNULL(isvoid,0) = 0 "

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            'If xIncludeIn_ActiveEmployee = False Then
            '    If xDateFilterQry.Trim.Length > 0 Then
            '        strQ &= xDateFilterQry
            '    End If
            'End If

            'If strFilerString.Trim.Length > 0 Then
            '    strQ &= " AND " & strFilerString
            'End If
            'Sohail (12 Oct 2021) -- End

            If strFilter.Trim.Length > 0 Then
                strQ &= " AND " & strFilter
            End If

            If dtPeriodEndDate <> Nothing Then
                strQ &= "AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
                objDataOperation.AddParameter("@end_date", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(xPeriodEnd))
            End If

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            'If strEmpUnkIDs.Trim <> "" AndAlso strEmpUnkIDs.Trim <> "0" AndAlso strEmpUnkIDs.Trim <> "-1" Then
            '    strQ &= "AND premployee_bank_tran.employeeunkid IN (" & strEmpUnkIDs & ") "
            'End If
            'Sohail (12 Oct 2021) -- End


            strQ &= " ) AS A "

            strQ &= "WHERE   1 = 1 "

            If dtPeriodEndDate <> Nothing Then
                strQ &= " AND   ROWNO = 1 "
            End If

            If strSortField.Trim.Length > 0 Then
                strQ += " ORDER BY " & strSortField
            End If

            'Sohail (12 Oct 2021) -- Start
            'NMB Issue :  : Performance issue on process payroll.
            strQ &= " DROP TABLE #Tableemp "
            'Sohail (12 Oct 2021) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Sohail (21 Aug 2015) -- End

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (premployee_bank_tran) </purpose>
    Public Function Insert(ByVal dtCurrentDateAndTime As Date, Optional ByRef intNewUnkId As Integer = 0, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (26 May 2023) -- [intNewUnkId,xDataOp]
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]
        'Sohail (26 Nov 2011) -- Start
        'If isExist(-1, mstrAccountno) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Account is already defined for particular employee. Please define new Account.")
        '    Return False
        'End If
        'Sohail (26 Nov 2011) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (26 May 2023) -- Start
        'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Hemant (26 May 2023) -- End

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccounttypeunkid.ToString)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (16 Oct 2010) -- Start
            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            'Sohail (16 Oct 2010) -- End
            'Sohail (21 Apr 2014) -- Start
            'Enhancement - Salary Distribution by Amount and bank priority.
            objDataOperation.AddParameter("@modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            'Sohail (21 Apr 2014) -- End
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString) 'Sohail (25 Apr 2014)
            'Sohail (12 Jun 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            objDataOperation.AddParameter("@payment_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayment_typeid.ToString)
            objDataOperation.AddParameter("@mobileno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobileno.ToString)
            'Sohail (12 Jun 2023) -- End

            'Sohail (16 Oct 2010) -- Start
            'Changes : New column 'percentage' added.
            'StrQ = "INSERT INTO premployee_bank_tran ( " & _
            '  "  employeeunkid " & _
            '  ", branchunkid " & _
            '  ", accounttypeunkid " & _
            '  ", accountno " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason" & _
            '") VALUES (" & _
            '  "  @employeeunkid " & _
            '  ", @branchunkid " & _
            '  ", @accounttypeunkid " & _
            '  ", @accountno " & _
            '  ", @userunkid " & _
            '  ", @isvoid " & _
            '  ", @voiduserunkid " & _
            '  ", @voiddatetime " & _
            '  ", @voidreason" & _
            '"); SELECT @@identity"
            strQ = "INSERT INTO premployee_bank_tran ( " & _
              "  employeeunkid " & _
              ", branchunkid " & _
              ", accounttypeunkid " & _
              ", accountno " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
                          ", percentage " & _
              ", modeid " & _
              ", amount " & _
              ", priority " & _
              ", periodunkid " & _
              ", payment_typeid " & _
              ", mobileno " & _
            ") VALUES (" & _
              "  @employeeunkid " & _
              ", @branchunkid " & _
              ", @accounttypeunkid " & _
              ", @accountno " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
                          ", @percentage " & _
              ", @modeid " & _
              ", @amount " & _
              ", @priority " & _
              ", @periodunkid " & _
              ", @payment_typeid " & _
              ", @mobileno " & _
            "); SELECT @@identity"
            'Sohail (12 Jun 2023) - [payment_typeid, mobileno]
            'Sohail (25 Apr 2014) - [periodunkid]
            'Sohail (21 Apr 2014) - [modeid, amount, priority]
            'Sohail (16 Oct 2010) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmpbanktranunkid = dsList.Tables(0).Rows(0).Item(0)

            'Hemant (26 May 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
            intNewUnkId = mintEmpbanktranunkid
            'Hemant (26 May 2023) -- End

            'Sohail (11 Nov 2010) -- Start

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForEmployeeBank(objDataOperation, 1)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForEmployeeBank(objDataOperation, 1) = False Then
            If InsertAuditTrailForEmployeeBank(objDataOperation, 1, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End

            'Sohail (11 Nov 2010) -- End

            'Hemant (26 May 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (26 May 2023) -- End

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (26 May 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (26 May 2023) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (premployee_bank_tran) </purpose>
    Public Function Update(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        'Hemant (26 May 2023) -- [xDataOp]
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]
        'Sohail (26 Nov 2011) -- Start
        'If isExist(-1, mstrAccountno, mintEmpbanktranunkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 1, "This Account is already defined for particular employee. Please define new Account.")
        '    Return False
        'End If
        'Sohail (26 Nov 2011) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (26 May 2023) -- Start
        'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
        'objDataOperation = New clsDataOperation
        If xDataOp Is Nothing Then
        objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        'Hemant (26 May 2023) -- End

        Try
            objDataOperation.AddParameter("@empbanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbanktranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccounttypeunkid.ToString)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (16 Oct 2010) -- Start
            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            'Sohail (16 Oct 2010) -- End
            'Sohail (21 Apr 2014) -- Start
            'Enhancement - Salary Distribution by Amount and bank priority.
            objDataOperation.AddParameter("@modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            'Sohail (21 Apr 2014) -- End
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString) 'Sohail (25 Apr 2014)
            'Sohail (12 Jun 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            objDataOperation.AddParameter("@payment_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayment_typeid.ToString)
            objDataOperation.AddParameter("@mobileno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobileno.ToString)
            'Sohail (12 Jun 2023) -- End

            'Sohail (16 Oct 2010) -- Start
            'Changes : New column 'percentage' added.
            'strQ = "UPDATE premployee_bank_tran SET " & _
            '          "  employeeunkid = @employeeunkid" & _
            '          ", branchunkid = @branchunkid" & _
            '          ", accounttypeunkid = @accounttypeunkid" & _
            '          ", accountno = @accountno" & _
            '          ", userunkid = @userunkid" & _
            '          ", isvoid = @isvoid" & _
            '          ", voiduserunkid = @voiduserunkid" & _
            '          ", voiddatetime = @voiddatetime" & _
            '          ", voidreason = @voidreason " & _
            '        "WHERE empbanktranunkid = @empbanktranunkid "
            strQ = "UPDATE premployee_bank_tran SET " & _
                      "  employeeunkid = @employeeunkid" & _
                      ", branchunkid = @branchunkid" & _
                      ", accounttypeunkid = @accounttypeunkid" & _
                      ", accountno = @accountno" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                        ", percentage = @percentage " & _
                      ", modeid = @modeid " & _
                      ", amount = @amount " & _
                      ", priority = @priority " & _
                      ", periodunkid = @periodunkid " & _
                      ", payment_typeid = @payment_typeid " & _
                      ", mobileno = @mobileno " & _
                    "WHERE empbanktranunkid = @empbanktranunkid "
            'Sohail (12 Jun 2023) - [payment_typeid, mobileno]
            'Sohail (25 Apr 2014) - [periodunkid]
            'Sohail (21 Apr 2014) - [modeid, amount, priority]
            'Sohail (16 Oct 2010) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (11 Nov 2010) -- Start

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForEmployeeBank(objDataOperation, 2)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForEmployeeBank(objDataOperation, 2) = False Then
            If InsertAuditTrailForEmployeeBank(objDataOperation, 2, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End

            'Sohail (11 Nov 2010) -- End

            'Hemant (26 May 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (26 May 2023) -- End

            Return True
        Catch ex As Exception
            'Hemant (26 May 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
            objDataOperation.ReleaseTransaction(False)
            'Hemant (26 May 2023) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (26 May 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-943 - Integrate NBC payment API in Aruti
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (26 May 2023) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (premployee_bank_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]
        'Sohail (03 Nov 2010) -- Start
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Employee Bank. This Employee Bank is in use.")
            Return False
        End If
        'Sohail (03 Nov 2010) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (16 Oct 2010) -- Start
            'strQ = "DELETE FROM premployee_bank_tran " & _
            '"WHERE empbanktranunkid = @empbanktranunkid "
            strQ = "UPDATE premployee_bank_tran SET " & _
                        "  isvoid = 1 " & _
                        ", voiduserunkid = @voiduserunkid" & _
                        ", voiddatetime = @voiddatetime" & _
                        ", voidreason = @voidreason " & _
            "WHERE empbanktranunkid = @empbanktranunkid "

            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            'Sohail (16 Oct 2010) -- End

            objDataOperation.AddParameter("@empbanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (11 Nov 2010) -- Start
            Me._Empbanktranunkid = intUnkid
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            'Call InsertAuditTrailForEmployeeBank(objDataOperation, 3)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'If InsertAuditTrailForEmployeeBank(objDataOperation, 3) = False Then
            If InsertAuditTrailForEmployeeBank(objDataOperation, 3, dtCurrentDateAndTime) = False Then
                'Sohail (21 Aug 2015) -- End
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If
            'Anjan (11 Jun 2011)-End

            'Sohail (11 Nov 2010) -- End

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (03 Nov 2010) -- Start
            'strQ = "<Query>"
            strQ = "SELECT DISTINCT " & _
                            "empsalarytranunkid " & _
                    "FROM    prempsalary_tran " & _
                    "WHERE   empbanktranid = @empbanktranunkid " & _
                            "AND ISNULL(isvoid, 0) = 0 "
            'Sohail (03 Nov 2010) -- End

            objDataOperation.AddParameter("@empbanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intBankGrp As Integer, ByVal strAccNo As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal intPeriodID As Integer = 0) As Boolean
        'Sohail (29 Apr 2014) - [intPeriodID]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (16 Oct 2010) -- Start
            'Changes : bankbranch from hrmsconfiguration
            'strQ = "SELECT " & _
            '        "	 prpayrollgroup_master.groupmasterunkid " & _
            '        "	,premployee_bank_tran.accountno " & _
            '        "	,employeeunkid " & _
            '       "FROM premployee_bank_tran " & _
            '        "	LEFT JOIN prbankbranch_master ON premployee_bank_tran.branchunkid = prbankbranch_master.branchunkid " & _
            '        "	LEFT JOIN prpayrollgroup_master ON prbankbranch_master.bankgroupunkid = prpayrollgroup_master.groupmasterunkid AND prpayrollgroup_master.grouptype_id = " & enPayrollGroupType.Bank & _
            '       "WHERE ISNULL(premployee_bank_tran.isvoid,0) = 0 " & _
            '        "	AND prpayrollgroup_master.groupmasterunkid = @BankGrpId " & _
            '        "	AND premployee_bank_tran.accountno = @AccNo "

            'Sohail (16 Dec 2011) -- Start
            'TRA - Allow adding same Bank Account to more than one employee but Give notification to Accept or not when same account number already assigned (NO Notification if account if account exist for different Bank)
            'strQ = "SELECT " & _
            '            "hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
            '        "	,premployee_bank_tran.accountno " & _
            '        "	,employeeunkid " & _
            '       "FROM premployee_bank_tran " & _
            '            "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
            '            "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND hrmsConfiguration..cfpayrollgroup_master.grouptype_id = " & enPayrollGroupType.Bank & _
            '       "WHERE ISNULL(premployee_bank_tran.isvoid,0) = 0 " & _
            '            "AND hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid = @BankGrpId " & _
            '        "	AND premployee_bank_tran.accountno = @AccNo "
            strQ = "SELECT  hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                    "	,premployee_bank_tran.accountno " & _
                    "	,employeeunkid " & _
                   "FROM premployee_bank_tran " & _
                        "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                            "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                                                                                  "AND hrmsConfiguration..cfpayrollgroup_master.grouptype_id = 3 " & _
                   "WHERE ISNULL(premployee_bank_tran.isvoid,0) = 0 " & _
                    "	AND premployee_bank_tran.accountno = @AccNo "
            'Sohail (16 Dec 2011) -- End
            'Sohail (16 Oct 2010) -- End

            'Sohail (29 Apr 2014) -- Start
            If intPeriodID > 0 Then
                strQ &= " AND periodunkid = @periodunkid"
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID)
            End If
            'Sohail (29 Apr 2014) -- End

            If intUnkid > 0 Then
                strQ &= " AND empbanktranunkid <> @empbanktranunkid"
            End If

            'Sohail (16 Dec 2011) -- Start
            'TRA - Allow adding same Bank Account to more than one employee but Give notification to Accept or not when same account number already assigned (NO Notification if account if account exist for different Bank)
            'Dim objBranch As New clsbankbranch_master
            'objBranch._Branchunkid = mintBranchunkid
            'intBankGrp = objBranch._Bankgroupunkid
            'objDataOperation.AddParameter("@BankGrpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intBankGrp)
            'Sohail (16 Dec 2011) -- End    

            'Sohail (30 Aug 2010) -- Start
            'Issue:Showing error of converting string to integer
            'objDataOperation.AddParameter("@AccNo", SqlDbType.Int, eZeeDataType.INT_SIZE, strAccNo)
            objDataOperation.AddParameter("@AccNo", SqlDbType.NVarChar, eZeeDataType.CARDNO_SIZE, strAccNo)
            'Sohail (30 Aug 2010) -- End
            objDataOperation.AddParameter("@empbanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (12 Jun 2023) -- Start
    'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
    Public Function isExistMobileNo(ByVal intPeriodID As Integer, ByVal strMobileNo As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
           
            strQ = "SELECT  premployee_bank_tran.mobileno " & _
                    "	,employeeunkid " & _
                   "FROM premployee_bank_tran " & _
                   "WHERE premployee_bank_tran.isvoid = 0 " & _
                    "   AND periodunkid = @periodunkid " & _
                    "	AND premployee_bank_tran.mobileno = @mobileno "



            If intUnkid > 0 Then
                strQ &= " AND empbanktranunkid <> @empbanktranunkid "
                objDataOperation.AddParameter("@empbanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodID)
            objDataOperation.AddParameter("@mobileno", SqlDbType.NVarChar, eZeeDataType.CARDNO_SIZE, strMobileNo)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistMobileNo; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (12 Jun 2023) -- End

    'Sohail (16 Oct 2010) -- Start
    Public Function Get_DIST_EmployeeList_WithBanks(Optional ByVal intBankUnkID As Integer = 0, Optional ByVal intBrachUnkID As Integer = 0, Optional ByVal dtAsOnDate As DateTime = Nothing) As String
        'Sohail (07 Dec 2013) - [intBankUnkID, intBrachUnkID]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strEmpList As String = ""

        objDataOperation = New clsDataOperation

        Try
            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            'strQ = "SELECT DISTINCT " & _
            '                "employeeunkid " & _
            '        "FROM    premployee_bank_tran " & _
            '        "JOIN hrmsConfiguration..cfbankbranch_master ON cfbankbranch_master.branchunkid = premployee_bank_tran.branchunkid " & _
            '        "JOIN hrmsConfiguration..cfpayrollgroup_master ON cfpayrollgroup_master.groupmasterunkid = cfbankbranch_master.bankgroupunkid AND cfpayrollgroup_master.grouptype_id = " & enPayrollGroupType.Bank & " " & _
            '        "JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
            '        "WHERE   ISNULL(premployee_bank_tran.isvoid,0) = 0 " & _
            '                "AND cfbankbranch_master.isactive = 1 " & _
            '                "AND cfpayrollgroup_master.isactive = 1 " & _
            '                "AND cfbankacctype_master.isactive = 1 "

            'If intBankUnkID > 0 Then
            '    strQ &= " AND cfpayrollgroup_master.groupmasterunkid = @groupmasterunkid "
            '    objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBankUnkID)
            'End If

            'If intBrachUnkID > 0 Then
            '    strQ &= " AND cfbankbranch_master.branchunkid = @branchunkid "
            '    objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBrachUnkID)
            'End If
            'Sohail (12 Jan 2015) -- Start
            'Enhancement - Allow to employee bank period wise.
            strQ = "DECLARE @strList VARCHAR(MAX) " & _
                    "SELECT  @strList = COALESCE(@strList + ',', '') + CAST(employeeunkid AS VARCHAR(MAX)) " & _
                    "FROM    ( SELECT DISTINCT " & _
                            "employeeunkid " & _
                            ", premployee_bank_tran.branchunkid " & _
                            ", cfpayrollgroup_master.groupmasterunkid " & _
                            ", DENSE_RANK() OVER ( PARTITION  BY premployee_bank_tran.employeeunkid ORDER BY cfcommon_period_tran.end_date DESC ) AS ROWNO " & _
                    "FROM    premployee_bank_tran " & _
                    "JOIN hrmsConfiguration..cfbankbranch_master ON cfbankbranch_master.branchunkid = premployee_bank_tran.branchunkid " & _
                                        "JOIN hrmsConfiguration..cfpayrollgroup_master ON cfpayrollgroup_master.groupmasterunkid = cfbankbranch_master.bankgroupunkid " & _
                                                                                  "AND cfpayrollgroup_master.grouptype_id = " & enPayrollGroupType.Bank & " " & _
                    "JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
                    "LEFT JOIN cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                    "WHERE   ISNULL(premployee_bank_tran.isvoid,0) = 0 " & _
                            "AND cfbankbranch_master.isactive = 1 " & _
                            "AND cfpayrollgroup_master.isactive = 1 " & _
                            "AND cfbankacctype_master.isactive = 1 "

            If dtAsOnDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8), cfcommon_period_tran.end_date, 112) <= @end_date "
                objDataOperation.AddParameter("@end_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If
            'Sohail (12 Jan 2015) -- End

            strQ &= "        ) AS A " & _
                    "WHERE A.ROWNO = 1 "

            If intBankUnkID > 0 Then
                strQ &= " AND A.groupmasterunkid = @groupmasterunkid "
                objDataOperation.AddParameter("@groupmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBankUnkID)
            End If

            If intBrachUnkID > 0 Then
                strQ &= " AND A.branchunkid = @branchunkid "
                objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBrachUnkID)
            End If

            strQ &= "SELECT  ISNULL(@strList, '') AS IDs "
            'Sohail (07 Dec 2013) -- End
            '               'Sohail (11 Sep 2012) - [JOIN ON cfbankbranch_master, cfpayrollgroup_master, cfbankacctype_master]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (07 Dec 2013) -- Start
            'Enhancement - OMAN
            'For Each dtRow As DataRow In dsList.Tables("List").Rows
            '    If strEmpList.Trim.Length = 0 Then
            '        strEmpList = dtRow.Item("employeeunkid").ToString
            '    Else
            '        strEmpList &= "," & dtRow.Item("employeeunkid").ToString
            '    End If
            'Next
            If dsList.Tables("List").Rows.Count > 0 Then
                'strEmpList = dsList.Tables("List").Rows(0).Item("IDs").ToString
                strEmpList = String.Join(",", dsList.Tables("List").Rows(0).Item("IDs").ToString.Split(",").ToArray.Distinct().ToArray())
                End If
            'Sohail (07 Dec 2013) -- End

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_DIST_EmployeeList_WithBanks; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return strEmpList
    End Function

    Public Function InsertUpdateDelete_EmpBanks(ByVal dtCurrentDateAndTime As Date, Optional ByVal objDoOperation As clsDataOperation = Nothing) As Boolean
        'Pinkal (03-Jun-2023) --Introduction of a tab on employee master to capture employee bank name, branch, A/C no, distribution mode and effective period.[Optional ByVal objDoOperation As clsDataOperation = Nothing]
        'Sohail (21 Aug 2015) - [dtCurrentDateAndTime]
        Dim objDataOperation As clsDataOperation

        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception = Nothing
        Try

            'Pinkal (03-Jun-2023) -- Start
            'Introduction of a tab on employee master to capture employee bank name, branch, A/C no, distribution mode and effective period.
            If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            Else
                objDataOperation = objDoOperation
            End If
            'Pinkal (03-Jun-2023) -- End

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        mintEmpbanktranunkid = CInt(.Item("empbanktranunkid").ToString)
                        mintEmployeeunkid = CInt(.Item("employeeunkid").ToString)
                        mintBranchunkid = CInt(.Item("branchunkid").ToString)
                        mintAccounttypeunkid = CInt(.Item("accounttypeunkid").ToString)
                        mstrAccountno = .Item("accountno").ToString
                        mdblPercentage = CDec(.Item("percentage").ToString)
                        'Sohail (21 Apr 2014) -- Start
                        'Enhancement - Salary Distribution by Amount and bank priority.
                        mintModeid = CInt(.Item("modeid").ToString)
                        mdecAmount = CDec(.Item("amount").ToString)
                        mintPriority = CInt(.Item("priority").ToString)
                        mblnIsvoid = False
                        mintVoiduserunkid = 0
                        mdtVoiddatetime = Nothing
                        mstrVoidreason = ""
                        'Sohail (21 Apr 2014) -- End
                        mintPeriodunkid = CInt(.Item("periodunkid").ToString) 'Sohail (25 Apr 2014)
                        'Sohail (12 Jun 2023) -- Start
                        'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
                        mintPayment_typeid = CInt(.Item("payment_typeid").ToString)
                        mstrMobileno = .Item("mobileno").ToString
                        'Sohail (12 Jun 2023) -- End

                        Select Case .Item("AUD")
                            Case "A"

                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'If Insert() = False Then
                                If Insert(dtCurrentDateAndTime) = False Then
                                    'Sohail (21 Aug 2015) -- End
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If

                            Case "U"

                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'If Update() = False Then
                                If Update(dtCurrentDateAndTime) = False Then
                                    'Sohail (21 Aug 2015) -- End
                                    objDataOperation.ReleaseTransaction(False)
                                    Return False
                                End If

                            Case "D"

                                'Sohail (21 Aug 2015) -- Start
                                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                                'If Delete(mintEmpbanktranunkid) = False Then
                                If Delete(mintEmpbanktranunkid, dtCurrentDateAndTime) = False Then
                                    'Sohail (21 Aug 2015) -- End
                                    'Pinkal (03-Jun-2023) -- Start
                                    'Introduction of a tab on employee master to capture employee bank name, branch, A/C no, distribution mode and effective period.
                                    If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
                                    'Pinkal (03-Jun-2023) -- End
                                    Return False
                                End If
                        End Select
                    End If
                End With
            Next


            'Pinkal (03-Jun-2023) -- Start
            'Introduction of a tab on employee master to capture employee bank name, branch, A/C no, distribution mode and effective period.
            If objDoOperation Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Pinkal (03-Jun-2023) -- End

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_EmpBanks; Module Name: " & mstrModuleName)
            exForce = Nothing
            'Pinkal (03-Jun-2023) -- Start
            'Introduction of a tab on employee master to capture employee bank name, branch, A/C no, distribution mode and effective period.
            If objDoOperation Is Nothing Then objDataOperation = Nothing
            'Pinkal (03-Jun-2023) -- End
        Finally
        End Try
    End Function
    'Sohail (16 Oct 2010) -- End

    'Sohail (11 Nov 2010) -- Start

    'Anjan (11 Jun 2011)-Start
    'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
    'Public Sub InsertAuditTrailForEmployeeBank(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer)
    Public Function InsertAuditTrailForEmployeeBank(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Anjan (11 Jun 2011)-End

        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'strQ = "INSERT INTO atpremployee_bank_tran ( " & _
            '              "  empbanktranunkid " & _
            '              ", employeeunkid " & _
            '              ", branchunkid " & _
            '              ", accounttypeunkid " & _
            '              ", accountno " & _
            '              ", percentage " & _
            '              ", audittype " & _
            '              ", audituserunkid " & _
            '              ", auditdatetime " & _
            '              ", ip " & _
            '              ", machine_name" & _
            '        ") VALUES (" & _
            '              "  @empbanktranunkid " & _
            '              ", @employeeunkid " & _
            '              ", @branchunkid " & _
            '              ", @accounttypeunkid " & _
            '              ", @accountno " & _
            '              ", @percentage " & _
            '              ", @audittype " & _
            '              ", @audituserunkid " & _
            '              ", @auditdatetime " & _
            '              ", @ip " & _
            '              ", @machine_name" & _
            '        "); SELECT @@identity"

            strQ = "INSERT INTO atpremployee_bank_tran ( " & _
                          "  empbanktranunkid " & _
                          ", employeeunkid " & _
                          ", branchunkid " & _
                          ", accounttypeunkid " & _
                          ", accountno " & _
                          ", percentage " & _
                          ", modeid " & _
                          ", amount " & _
                          ", priority " & _
                          ", periodunkid " & _
                          ", payment_typeid " & _
                          ", mobileno " & _
                          ", audittype " & _
                          ", audituserunkid " & _
                          ", auditdatetime " & _
                          ", ip " & _
                          ", machine_name" & _
                    ", form_name " & _
                    ", module_name1 " & _
                    ", module_name2 " & _
                    ", module_name3 " & _
                    ", module_name4 " & _
                    ", module_name5 " & _
                    ", isweb " & _
                    ") VALUES (" & _
                          "  @empbanktranunkid " & _
                          ", @employeeunkid " & _
                          ", @branchunkid " & _
                          ", @accounttypeunkid " & _
                          ", @accountno " & _
                          ", @percentage " & _
                          ", @modeid " & _
                          ", @amount " & _
                          ", @priority " & _
                          ", @periodunkid " & _
                          ", @payment_typeid " & _
                          ", @mobileno " & _
                          ", @audittype " & _
                          ", @audituserunkid " & _
                          ", @auditdatetime " & _
                          ", @ip " & _
                          ", @machine_name" & _
                    ", @form_name " & _
                    ", @module_name1 " & _
                    ", @module_name2 " & _
                    ", @module_name3 " & _
                    ", @module_name4 " & _
                    ", @module_name5 " & _
                    ", @isweb " & _
                    "); SELECT @@identity"
            'Sohail (12 Jun 2023) - [payment_typeid, mobileno]
            'Sohail (25 Apr 2014) - [periodunkid]
            'Sohail (21 Apr 2014) - [modeid, amount, priority]
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@empbanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpbanktranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAccounttypeunkid.ToString)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountno.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            'objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Sohail (21 Aug 2015) -- End
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName)
            'Sohail (21 Apr 2014) -- Start
            'Enhancement - Salary Distribution by Amount and bank priority.
            objDataOperation.AddParameter("@modeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintModeid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            'Sohail (21 Apr 2014) -- End
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString) 'Sohail (25 Apr 2014)
 'Sohail (12 Jun 2023) -- Start
            'Enhancement : A1X-961 : Mobile money account numbers - Create a new screen to capture mobile money account numbers.
            objDataOperation.AddParameter("@payment_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPayment_typeid.ToString)
            objDataOperation.AddParameter("@mobileno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMobileno.ToString)
            'Sohail (12 Jun 2023) -- End

            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 3, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return True
            'Anjan (11 Jun 2011)-End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertAuditTrailForEmployeeBank", mstrModuleName)
            'Anjan (11 Jun 2011)-Start
            'Issue : In main table entry was get inserted if there any error occurs at time of insertion in AT table.
            Return False
            'Anjan (11 Jun 2011)-End
        Finally
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (11 Nov 2010) -- End

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Public Function GetEmployeeBanks(ByVal intEmployeeunkid As Integer) As DataSet
    '    Dim strQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Try
    '        objDataOperation = New clsDataOperation
    '        strQ = "SELECT " & _
    '                    " premployee_bank_tran.empbanktranunkid " & _
    '                    ",premployee_bank_tran.employeeunkid " & _
    '                    ",premployee_bank_tran.branchunkid " & _
    '                    ",premployee_bank_tran.accounttypeunkid " & _
    '                    ",hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankGrpUnkid " & _
    '                    ",premployee_bank_tran.accountno " & _
    '                    ",hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
    '                    ",hrmsConfiguration..cfpayrollgroup_master.groupname AS BankGrp " & _
    '                    ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
    '                    ",hrmsConfiguration..cfbankacctype_master.accounttype_name AS AccName " & _
    '                    ",premployee_bank_tran.percentage " & _
    '                    ",ISNULL(premployee_bank_tran.modeid, 2) AS modeid " & _
    '                    ",ISNULL(premployee_bank_tran.amount, 0) AS amount " & _
    '                    ",ISNULL(premployee_bank_tran.priority, 1) AS priority " & _
    '                    ",ISNULL(premployee_bank_tran.periodunkid, 1) AS periodunkid " & _
    '                    ",'' As AUD " & _
    '                "FROM premployee_bank_tran " & _
    '                    "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
    '                    "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id =   " & enPayrollGroupType.Bank & _
    '                    "LEFT JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "LEFT JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
    '                "WHERE ISNULL(isvoid,0) = 0 " & _
    '                "AND premployee_bank_tran.employeeunkid = @employeeunkid "
    '        'Sohail (25 Apr 2014) - [periodunkid]
    '        'Sohail (21 Apr 2014) - [modeid, amount, priority]

    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)

    '        'Sohail (24 Jun 2011) -- Start
    '        'Issue : According to privilege that lower level user should not see superior level employees.

    '        'S.SANDEEP [ 04 FEB 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '    strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        'End If

    '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
    '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        'End Select
    '        strQ &= UserAccessLevel._AccessLevelFilterString
    '        'S.SANDEEP [ 01 JUNE 2012 ] -- END


    '        'S.SANDEEP [ 04 FEB 2012 ] -- END

    '        'Sohail (24 Jun 2011) -- End

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        mdtTran.Clear()

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetEmployeeBanks", mstrModuleName)
    '    End Try
    '    Return dsList
    'End Function
    Public Function GetEmployeeBanks(ByVal xDatabaseName As String _
                                    , ByVal xUserUnkid As Integer _
                                    , ByVal xYearUnkid As Integer _
                                    , ByVal xCompanyUnkid As Integer _
                                    , ByVal xPeriodStart As DateTime _
                                    , ByVal xPeriodEnd As DateTime _
                                    , ByVal xUserModeSetting As String _
                                    , ByVal xOnlyApproved As Boolean _
                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                    , ByVal intEmployeeunkid As Integer _
                                    , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                                    , Optional ByVal strFilerString As String = "" _
                            ) As DataSet

        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation

            strQ = "SELECT   premployee_bank_tran.empbanktranunkid " & _
                        ",premployee_bank_tran.employeeunkid " & _
                        ",premployee_bank_tran.branchunkid " & _
                        ",premployee_bank_tran.accounttypeunkid " & _
                        ",hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AS BankGrpUnkid " & _
                        ",premployee_bank_tran.accountno " & _
                        ",hrmsConfiguration..cfbankbranch_master.branchname AS BranchName " & _
                        ",hrmsConfiguration..cfpayrollgroup_master.groupname AS BankGrp " & _
                        ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
                        ",hrmsConfiguration..cfbankacctype_master.accounttype_name AS AccName " & _
                        ",premployee_bank_tran.percentage " & _
                        ",ISNULL(premployee_bank_tran.modeid, 2) AS modeid " & _
                        ",ISNULL(premployee_bank_tran.amount, 0) AS amount " & _
                        ",ISNULL(premployee_bank_tran.priority, 1) AS priority " & _
                        ",ISNULL(premployee_bank_tran.periodunkid, 1) AS periodunkid " & _
                        ", ISNULL(premployee_bank_tran.payment_typeid, 0) AS payment_typeid " & _
                        ", ISNULL(premployee_bank_tran.mobileno, '') AS mobileno " & _
                        ",'' As AUD " & _
                    "FROM premployee_bank_tran " & _
                        "LEFT JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                        "LEFT JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid AND grouptype_id =   " & enPayrollGroupType.Bank & _
                        "LEFT JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid " & _
                        "LEFT JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid "
            'Sohail (12 Jun 2023) - [payment_typeid, mobileno]

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE ISNULL(isvoid,0) = 0 " & _
                    "AND premployee_bank_tran.employeeunkid = @employeeunkid "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    strQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strFilerString.Trim.Length > 0 Then
                strQ &= " AND " & strFilerString
            End If
            
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid.ToString)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeBanks", mstrModuleName)
        End Try
        Return dsList
    End Function
    'Sohail (21 Aug 2015) -- End

    'S.SANDEEP [ 21 JUNE 2011 ] -- START
    'ISSUE : MR. RUTTA'S COMMENTS.
    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Public Function GetEmployeeBanksTemplate(Optional ByVal StrList As String = "List") As DataSet
    '    Dim dsList As New DataSet
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Try
    '        objDataOperation = New clsDataOperation

    '        StrQ = "SELECT " & _
    '                        "  ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
    '                        " ,ISNULL(hremployee_master.firstname,'') AS firstname " & _
    '                        " ,ISNULL(hremployee_master.surname,'') AS surname " & _
    '                        " ,ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupname,'') AS bank " & _
    '                        " ,ISNULL(hrmsConfiguration..cfbankbranch_master.branchname,'') AS branchname " & _
    '                        " ,ISNULL(hrmsConfiguration..cfbankacctype_master.accounttype_name,'') AS accounttype_name " & _
    '                        " ,ISNULL(premployee_bank_tran.accountno,'') AS accountno " & _
    '                        ", CASE ISNULL(premployee_bank_tran.modeid, 2) WHEN 1 THEN 'Amount' ELSE 'Percentage' END AS distribution_mode " & _
    '                        " ,ISNULL(percentage,'0.00') AS percentage " & _
    '                        ", ISNULL(premployee_bank_tran.amount, 0) AS amount " & _
    '                        ", ISNULL(premployee_bank_tran.priority, 1) AS priority " & _
    '                        ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
    '                    "FROM premployee_bank_tran " & _
    '                        " JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                        " JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
    '                        " JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
    '                        " 	AND hrmsConfiguration..cfpayrollgroup_master.grouptype_id = " & enPayrollGroupType.Bank & "   " & _
    '                        " JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
    '                        " LEFT JOIN cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid  "
    '        'Sohail (25 Apr 2014) - [period_name]
    '        'Sohail (21 Apr 2014) - [modeid, amount, priority]

    '        'S.SANDEEP [ 12 OCT 2011 ] -- START
    '        StrQ &= " WHERE premployee_bank_tran.isvoid = 0 "
    '        'S.SANDEEP [ 12 OCT 2011 ] -- END 

    '        dsList = objDataOperation.ExecQuery(StrQ, StrList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure : GetEmployeeBanks ; Module Name : " & mstrModuleName)
    '    Finally
    '        dsList.Dispose()
    '    End Try
    'End Function
    Public Function GetEmployeeBanksTemplate(ByVal xDatabaseName As String _
                            , ByVal xUserUnkid As Integer _
                            , ByVal xYearUnkid As Integer _
                            , ByVal xCompanyUnkid As Integer _
                            , ByVal xPeriodStart As DateTime _
                            , ByVal xPeriodEnd As DateTime _
                            , ByVal xUserModeSetting As String _
                            , ByVal xOnlyApproved As Boolean _
                            , ByVal xIncludeIn_ActiveEmployee As Boolean _
                            , Optional ByVal blnApplyUserAccessFilter As Boolean = True _
                            , Optional ByVal strFilerString As String = "" _
                            , Optional ByVal StrList As String = "List" _
                            ) As DataSet

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation

            StrQ = "SELECT    ISNULL(hremployee_master.employeecode,'') AS employeecode " & _
                            " ,ISNULL(hremployee_master.firstname,'') AS firstname " & _
                            " ,ISNULL(hremployee_master.surname,'') AS surname " & _
                            " ,ISNULL(hrmsConfiguration..cfpayrollgroup_master.groupname,'') AS bank " & _
                            " ,ISNULL(hrmsConfiguration..cfbankbranch_master.branchname,'') AS branchname " & _
                            " ,ISNULL(hrmsConfiguration..cfbankacctype_master.accounttype_name,'') AS accounttype_name " & _
                            " ,ISNULL(premployee_bank_tran.accountno,'') AS accountno " & _
                            ", CASE ISNULL(premployee_bank_tran.modeid, 2) WHEN 1 THEN 'Amount' ELSE 'Percentage' END AS distribution_mode " & _
                            " ,ISNULL(percentage,'0.00') AS percentage " & _
                            ", ISNULL(premployee_bank_tran.amount, 0) AS amount " & _
                            ", ISNULL(premployee_bank_tran.priority, 1) AS priority " & _
                            ", ISNULL(cfcommon_period_tran.period_name, '') AS period_name " & _
                        "FROM premployee_bank_tran " & _
                            " JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            " JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                            " JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                            " 	AND hrmsConfiguration..cfpayrollgroup_master.grouptype_id = " & enPayrollGroupType.Bank & "   " & _
                            " JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
                            " LEFT JOIN cfcommon_period_tran ON premployee_bank_tran.periodunkid = cfcommon_period_tran.periodunkid  "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            If blnApplyUserAccessFilter = True Then
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= xAdvanceJoinQry
            End If

            StrQ &= " WHERE premployee_bank_tran.isvoid = 0 "

            If blnApplyUserAccessFilter = True Then
                If xUACFiltrQry.Trim.Length > 0 Then
                    StrQ &= " AND " & xUACFiltrQry
                End If
            End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry
                End If
            End If

            If strFilerString.Trim.Length > 0 Then
                StrQ &= " AND " & strFilerString
            End If

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : GetEmployeeBanks ; Module Name : " & mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Function
    'Sohail (21 Aug 2015) -- End

    Public Function IsBankAssigned(ByVal intEmpId As Integer, _
                                   ByVal strBankGrp As String, _
                                   ByVal strBranch As String, _
                                   ByVal strAccType As String, _
                                   ByVal strAccountNo As String) As Integer
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   "	premployee_bank_tran.employeeunkid AS EmpId " & _
                   "FROM premployee_bank_tran " & _
                   "	JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                   "	JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                   "		AND hrmsConfiguration..cfpayrollgroup_master.grouptype_id = 3 " & _
                   "	JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
                   "WHERE employeeunkid = @intEmpId " & _
                   "	AND hrmsConfiguration..cfpayrollgroup_master.groupname = @strBankGrp " & _
                   "	AND hrmsConfiguration..cfbankbranch_master.branchname = @strBranch " & _
                   "	AND hrmsConfiguration..cfbankacctype_master.accounttype_name = @strAccType " & _
                   "	AND premployee_bank_tran.isvoid = 0 "

            objDataOperation.AddParameter("@intEmpId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmpId)
            objDataOperation.AddParameter("@strBankGrp", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strBankGrp)
            objDataOperation.AddParameter("@strBranch", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strBranch)
            objDataOperation.AddParameter("@strAccType", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strAccType)
            objDataOperation.AddParameter("@strAccountNo", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strAccountNo)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList.Tables(0).Rows(0)("EmpId")
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsBankAssigned", mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Function
    'S.SANDEEP [ 21 JUNE 2011 ] -- END 

    'Sohail (25 Apr 2014) -- Start
    'Enhancement - Employee Bank Details Period Wise.
    'Sohail (15 Mar 2018) -- Start
    'TANAPA Issue - Support Issue Id # 0002096 : System does not allow to import two bank group in the same period under VALUE distribution mode on Import Employee Bank in 70.2.
    'Public Function IsBankAssigned(ByVal intPeriodID As Integer, _
    '                               ByVal intEmpId As Integer) As Integer
    Public Function IsBankAssigned(ByVal intPeriodID As Integer, _
                                   ByVal intEmpId As Integer) As DataSet
        'Sohail (15 Mar 2018) -- End

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT  premployee_bank_tran.employeeunkid AS EmpId " & _
                       ", premployee_bank_tran.periodunkid " & _
                       ", modeid " & _
                       ", percentage " & _
                       ", amount " & _
                       ", priority " & _
                   "FROM premployee_bank_tran " & _
                   "JOIN hrmsConfiguration..cfbankbranch_master ON premployee_bank_tran.branchunkid = hrmsConfiguration..cfbankbranch_master.branchunkid " & _
                   "JOIN hrmsConfiguration..cfpayrollgroup_master ON hrmsConfiguration..cfbankbranch_master.bankgroupunkid = hrmsConfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                        "AND hrmsConfiguration..cfpayrollgroup_master.grouptype_id = 3 " & _
                   "JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
                   "WHERE premployee_bank_tran.isvoid = 0 " & _
                        "AND premployee_bank_tran.periodunkid = @periodunkid " & _
                        "AND employeeunkid = @intEmpId "


            objDataOperation.AddParameter("@periodunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intPeriodID)
            objDataOperation.AddParameter("@intEmpId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmpId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sohail (15 Mar 2018) -- Start
            'TANAPA Issue - Support Issue Id # 0002096 : System does not allow to import two bank group in the same period under VALUE distribution mode on Import Employee Bank in 70.2.
            'If dsList.Tables(0).Rows.Count > 0 Then
            '    Return dsList.Tables(0).Rows(0)("EmpId")
            'End If
            'Sohail (15 Mar 2018) -- End

        Catch ex As Exception
            'Sohail (15 Mar 2018) -- Start
            'TANAPA Issue - Support Issue Id # 0002096 : System does not allow to import two bank group in the same period under VALUE distribution mode on Import Employee Bank in 70.2.
            'DisplayError.Show("-1", ex.Message, "IsBankAssigned", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: IsBankAssigned; Module Name: " & mstrModuleName)
            'Sohail (15 Mar 2018) -- End
        Finally
            dsList.Dispose()
        End Try
        Return dsList 'Sohail (15 Mar 2018)
    End Function
    'Sohail (25 Apr 2014) -- End

    'Sohail (16 Jul 2012) -- Start
    'TRA - ENHANCEMENT
    Public Function isEmployeeBankExist(ByVal intEmployeeInkId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  empbanktranunkid " & _
                    "FROM    premployee_bank_tran " & _
                    "JOIN hrmsConfiguration..cfbankbranch_master ON cfbankbranch_master.branchunkid = premployee_bank_tran.branchunkid " & _
                    "JOIN hrmsConfiguration..cfpayrollgroup_master ON cfpayrollgroup_master.groupmasterunkid = cfbankbranch_master.bankgroupunkid AND cfpayrollgroup_master.grouptype_id = " & enPayrollGroupType.Bank & " " & _
                    "JOIN hrmsConfiguration..cfbankacctype_master ON premployee_bank_tran.accounttypeunkid = hrmsConfiguration..cfbankacctype_master.accounttypeunkid " & _
                    "WHERE   ISNULL(premployee_bank_tran.isvoid, 0) = 0 " & _
                            "AND cfbankbranch_master.isactive = 1 " & _
                            "AND cfpayrollgroup_master.isactive = 1 " & _
                            "AND cfbankacctype_master.isactive = 1 " & _
                            "AND employeeunkid = @employeeunkid "
            '               'Sohail (11 Sep 2012) - [JOIN ON cfbankbranch_master, cfpayrollgroup_master, cfbankacctype_master]

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeInkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isEmployeeBankExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (21 Aug 2015) -- Start
    'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
    'Public Function GetEmployeeWithOverDistribution(Optional ByVal strList As String = "List", Optional ByVal strEmpIDs As String = "") As DataSet
    '    Dim strQ As String = ""
    '    Dim dsList As New DataSet
    '    Dim exForce As Exception
    '    Try
    '        objDataOperation = New clsDataOperation

    '        strQ = "SELECT  premployee_bank_tran.employeeunkid " & _
    '                      ", hremployee_master.employeecode " & _
    '                      ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename " & _
    '                "FROM    premployee_bank_tran " & _
    '                        "JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid " & _
    '                "WHERE   ISNULL(isvoid, 0) = 0 " & _
    '                        "AND premployee_bank_tran.employeeunkid > 0 " & _
    '                        "AND premployee_bank_tran.modeid = " & enPaymentBy.Percentage & " "
    '        'Sohail (21 Apr 2014) - [premployee_bank_tran.modeid]

    '        If strEmpIDs.Trim <> "" Then
    '            strQ &= " AND premployee_bank_tran.employeeunkid IN (" & strEmpIDs & ")  "
    '        End If

    '        strQ &= "GROUP BY premployee_bank_tran.employeeunkid " & _
    '                      ", hremployee_master.employeecode " & _
    '                      ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname " & _
    '                      ", periodunkid " & _
    '                "HAVING  SUM(percentage) <> 100 "
    '        'Sohail (04 Mar 2013) - "HAVING  SUM(percentage) > 100 "



    '        dsList = objDataOperation.ExecQuery(strQ, strList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "GetEmployeeWithOverDistribution", mstrModuleName)
    '    End Try
    '    Return dsList
    'End Function
    Public Function GetEmployeeWithOverDistribution(ByVal xDatabaseName As String _
                                                    , ByVal xUserUnkid As Integer _
                                                    , ByVal xYearUnkid As Integer _
                                                    , ByVal xCompanyUnkid As Integer _
                                                    , ByVal xPeriodStart As DateTime _
                                                    , ByVal xPeriodEnd As DateTime _
                                                    , ByVal xUserModeSetting As String _
                                                    , ByVal xOnlyApproved As Boolean _
                                                    , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                                    , Optional ByVal blnApplyUserAccessFilter As Boolean = False _
                                                    , Optional ByVal strFilerString As String = "" _
                                                    , Optional ByVal strList As String = "List" _
                                                    , Optional ByVal strEmpIDs As String = "" _
                                                    ) As DataSet
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStart, xPeriodEnd, , , xDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEnd, xOnlyApproved, xDatabaseName, xUserUnkid, xCompanyUnkid, xYearUnkid, xUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEnd, xDatabaseName)

            objDataOperation = New clsDataOperation

            strQ = "SELECT  premployee_bank_tran.employeeunkid " & _
                          ", hremployee_master.employeecode " & _
                          ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename " & _
                    "FROM    premployee_bank_tran " & _
                            "JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACQry.Trim.Length > 0 Then
            '    StrQ &= xUACQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE   ISNULL(isvoid, 0) = 0 " & _
                            "AND premployee_bank_tran.employeeunkid > 0 " & _
                            "AND premployee_bank_tran.modeid = " & enPaymentBy.Percentage & " "

            '*** do not apply user access filter 
            'If blnApplyUserAccessFilter = True Then
            '    If xUACFiltrQry.Trim.Length > 0 Then
            '        strQ &= " AND " & xUACFiltrQry
            '    End If
            'End If

            If xIncludeIn_ActiveEmployee = False Then
                If xDateFilterQry.Trim.Length > 0 Then
                    strQ &= xDateFilterQry
                End If
            End If

            If strFilerString.Trim.Length > 0 Then
                strQ &= " AND " & strFilerString
            End If

            If strEmpIDs.Trim <> "" Then
                strQ &= " AND premployee_bank_tran.employeeunkid IN (" & strEmpIDs & ")  "
            End If

            strQ &= "GROUP BY premployee_bank_tran.employeeunkid " & _
                          ", hremployee_master.employeecode " & _
                          ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname " & _
                          ", periodunkid " & _
                    "HAVING  SUM(percentage) <> 100 "



            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetEmployeeWithOverDistribution", mstrModuleName)
        End Try
        Return dsList
    End Function
    'Sohail (16 Jul 2012) -- End
    'Sohail (21 Aug 2015) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Account is already defined for particular employee. Please define new Account.")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Employee Bank. This Employee Bank is in use.")
			Language.setMessage(mstrModuleName, 3, "WEB")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
''************************************************************************************************************************************
''Class Name : clsEmployeeBanks.vb
''Purpose    :
''Date       :06/07/2010
''Written By :Sandeep J. Sharma
''Modified   :
''************************************************************************************************************************************

'Imports eZeeCommonLib
'''' <summary>
'''' Purpose: 
'''' Developer: Sandeep J. Sharma
'''' </summary>
'Public Class clsEmployeeBanks

'#Region " Private Variables "
'    Private Shared Readonly mstrModuleName As String = "clsEmployeeBanks"
'    Dim objDataOperation As clsDataOperation
'    Dim mstrMessage As String = ""
'    Private mintEmployeeUnkid As Integer = -1
'    Private mdtTran As DataTable
'#End Region

'#Region " Properties "

'    ''' <summary>
'    ''' Purpose: Get Message from Class 
'    ''' Modify By: Sandeep J. Sharma
'    ''' </summary>
'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    Public Property _EmployeeUnkid() As Integer
'        Get
'            Return mintEmployeeUnkid
'        End Get
'        Set(ByVal value As Integer)
'            mintEmployeeUnkid = value
'            Call GetEmployeeBank_Tran()
'        End Set
'    End Property

'    Public Property _DataTable() As DataTable
'        Get
'            Return mdtTran
'        End Get
'        Set(ByVal value As DataTable)
'            mdtTran = value
'        End Set
'    End Property

'#End Region

'#Region " Constructor "
'    Public Sub New()
'        mdtTran = New DataTable("EmpBanks")
'        Dim dCol As DataColumn
'        Try
'            dCol = New DataColumn("empbanktranunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("employeeunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("branchunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("bankgroupunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            dCol.DefaultValue = 0
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("accounttypeunkid")
'            dCol.DataType = System.Type.GetType("System.Int32")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("accountno")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("AUD")
'            dCol.DataType = System.Type.GetType("System.String")
'            dCol.AllowDBNull = True
'            dCol.DefaultValue = DBNull.Value
'            mdtTran.Columns.Add(dCol)

'            dCol = New DataColumn("GUID")
'            dCol.DataType = System.Type.GetType("System.String")
'            mdtTran.Columns.Add(dCol)

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
'        End Try
'    End Sub
'#End Region

'#Region " Private Functions "
'    Private Sub GetEmployeeBank_Tran()
'        Dim strQ As String = ""
'        Dim strErrorMessage As String = ""
'        Dim dsList As New DataSet
'        Dim dRowID_Tran As DataRow
'        Dim exForce As Exception
'        Try
'            objDataOperation = New clsDataOperation
'            strQ = "SELECT premployee_bank_tran.empbanktranunkid " & _
'                    "	  ,premployee_bank_tran.employeeunkid " & _
'                    "	  ,premployee_bank_tran.branchunkid " & _
'                    "	  ,premployee_bank_tran.accounttypeunkid " & _
'                    "	  ,premployee_bank_tran.accountno " & _
'                    "	  ,prpayrollgroup_master.groupmasterunkid As bankgroupunkid " & _
'                    "     ,'' As AUD " & _
'                   "FROM premployee_bank_tran " & _
'                    "	JOIN prbankbranch_master ON premployee_bank_tran.branchunkid = prbankbranch_master.branchunkid " & _
'                    "	JOIN prpayrollgroup_master ON prbankbranch_master.bankgroupunkid = prpayrollgroup_master.groupmasterunkid AND prpayrollgroup_master.grouptype_id                         = " & enPayrollGroupType.Bank & _
'                    "WHERE employeeunkid = @employeeunkid "

'            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeUnkid.ToString)

'            dsList = objDataOperation.ExecQuery(strQ, "List")

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            mdtTran.Clear()


'            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
'                With dsList.Tables("List").Rows(i)
'                    dRowID_Tran = mdtTran.NewRow()

'                    dRowID_Tran.Item("empbanktranunkid") = .Item("empbanktranunkid")
'                    dRowID_Tran.Item("employeeunkid") = .Item("employeeunkid")
'                    dRowID_Tran.Item("branchunkid") = .Item("branchunkid")
'                    dRowID_Tran.Item("accounttypeunkid") = .Item("accounttypeunkid")
'                    dRowID_Tran.Item("accountno") = .Item("accountno")
'                    dRowID_Tran.Item("bankgroupunkid") = .Item("bankgroupunkid")
'                    dRowID_Tran.Item("AUD") = .Item("AUD")

'                    mdtTran.Rows.Add(dRowID_Tran)
'                End With
'            Next
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "GetEmployeeBank_Tran", mstrModuleName)
'        End Try
'    End Sub

'    Public Function InsertUpdateDelete_EmpBanks() As Boolean
'        Dim i As Integer
'        Dim strQ As String = ""
'        Dim strErrorMessage As String = ""
'        Dim exForce As Exception
'        Try
'            objDataOperation = New clsDataOperation
'            objDataOperation.BindTransaction()
'            For i = 0 To mdtTran.Rows.Count - 1
'                With mdtTran.Rows(i)
'                    objDataOperation.ClearParameters()
'                    If Not IsDBNull(.Item("AUD")) Then
'                        Select Case .Item("AUD")
'                            Case "A"
'                                strQ = "INSERT INTO premployee_bank_tran ( " & _
'                                            "  employeeunkid " & _
'                                            ", branchunkid " & _
'                                            ", accounttypeunkid " & _
'                                            ", accountno" & _
'                                        ") VALUES (" & _
'                                            "  @employeeunkid " & _
'                                            ", @branchunkid " & _
'                                            ", @accounttypeunkid " & _
'                                            ", @accountno" & _
'                                        "); SELECT @@identity"

'                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
'                                objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("branchunkid").ToString)
'                                objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("accounttypeunkid").ToString)
'                                objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("accountno").ToString)


'                                objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                            Case "U"

'                                strQ = "UPDATE premployee_bank_tran SET " & _
'                                          "  employeeunkid = @employeeunkid" & _
'                                          ", branchunkid = @branchunkid" & _
'                                          ", accounttypeunkid = @accounttypeunkid" & _
'                                          ", accountno = @accountno " & _
'                                       "WHERE empbanktranunkid = @empbanktranunkid "

'                                objDataOperation.AddParameter("@empbanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("empbanktranunkid").ToString)
'                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid").ToString)
'                                objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("branchunkid").ToString)
'                                objDataOperation.AddParameter("@accounttypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("accounttypeunkid").ToString)
'                                objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("accountno").ToString)


'                                objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If

'                            Case "D"

'                                strQ = "DELETE FROM premployee_bank_tran " & _
'                                       "WHERE empbanktranunkid = @empbanktranunkid "

'                                objDataOperation.AddParameter("@empbanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("empbanktranunkid").ToString)

'                                Call objDataOperation.ExecNonQuery(strQ)

'                                If objDataOperation.ErrorMessage <> "" Then
'                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                                    Throw exForce
'                                End If
'                        End Select
'                    End If
'                End With
'            Next

'            objDataOperation.ReleaseTransaction(True)

'            Return True

'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_EmpBanks", mstrModuleName)
'        End Try
'    End Function

'    Public Function GetList(Optional ByVal strListName As String = "List") As DataSet
'        Dim dsList As DataSet = Nothing
'        Dim strQ As String = ""
'        Dim exForce As Exception

'        objDataOperation = New clsDataOperation

'        Try
'            strQ = "SELECT " & _
'                        " premployee_bank_tran.empbanktranunkid " & _
'                        ",premployee_bank_tran.employeeunkid " & _
'                        ",premployee_bank_tran.branchunkid " & _
'                        ",premployee_bank_tran.accounttypeunkid " & _
'                        ",prpayrollgroup_master.groupmasterunkid AS BankGrpUnkid " & _
'                        ",premployee_bank_tran.accountno " & _
'                        ",prbankbranch_master.branchname AS BranchName " & _
'                        ",prpayrollgroup_master.groupname AS BankGrp " & _
'                        ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS EmpName " & _
'                     ",prbankacctype_master.accounttype_name AS AccName " & _
'                    "FROM premployee_bank_tran " & _
'                        "LEFT JOIN prbankbranch_master ON premployee_bank_tran.branchunkid = prbankbranch_master.branchunkid " & _
'                        "LEFT JOIN prpayrollgroup_master ON prbankbranch_master.bankgroupunkid = prpayrollgroup_master.groupmasterunkid AND grouptype_id =   " & enPayrollGroupType.Bank & _
'                        "LEFT JOIN hremployee_master ON premployee_bank_tran.employeeunkid = hremployee_master.employeeunkid " & _
'                        "LEFT JOIN prbankacctype_master ON premployee_bank_tran.accounttypeunkid = prbankacctype_master.accounttypeunkid "
'            dsList = objDataOperation.ExecQuery(strQ, strListName)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return dsList

'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
'        Finally
'            exForce = Nothing
'            If dsList IsNot Nothing Then dsList.Dispose()
'            objDataOperation = Nothing
'        End Try

'    End Function

'    Public Function Delete(ByVal intUnkid As Integer) As Boolean
'        Dim strQ As String = ""
'        Dim strErrorMessage As String = ""
'        Dim exForce As Exception
'        Try
'            objDataOperation = New clsDataOperation

'            strQ = "DELETE FROM premployee_bank_tran " & _
'                   "WHERE empbanktranunkid = @empbanktranunkid "

'            objDataOperation.AddParameter("@empbanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)

'            Call objDataOperation.ExecNonQuery(strQ)

'            If objDataOperation.ErrorMessage <> "" Then
'                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'                Throw exForce
'            End If

'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Delete", mstrModuleName)
'            Return False
'        End Try
'    End Function
'#End Region


'    '''' <summary>
'    '''' Modify By: Sandeep J. Sharma
'    '''' </summary>
'    '''' <purpose> Assign all Property variable </purpose>
'    'Public Function isExist(ByVal intEmpUnkid As Integer, ByVal intBankUnkid As Integer, ByVal intBranchUnkid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
'    '    Dim dsList As DataSet = Nothing
'    '    Dim strQ As String = ""
'    '    Dim exForce As Exception

'    '    'objDataOperation = New clsDataOperation

'    '    Try
'    '        strQ = "SELECT " & _
'    '                 " empbanktranunkid " & _
'    '                 ",employeeunkid " & _
'    '                 ",bankgroupunkid " & _
'    '                 ",branchunkid " & _
'    '                 ",accounttypeunkid " & _
'    '                 ",accountno " & _
'    '            "FROM premployee_bank_tran " & _
'    '            "WHERE employeeunkid = @employeeunkid " & _
'    '                 "AND bankgroupunkid = @bankgroupunkid " & _
'    '                 "AND branchunkid = @branchunkid "

'    '        If intUnkid > 0 Then
'    '            strQ &= " AND empbanktranunkid <> @empbanktranunkid"
'    '        End If

'    '        objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpUnkid)
'    '        objDataOperation.AddParameter("@bankgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBankUnkid)
'    '        objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBranchUnkid)
'    '        objDataOperation.AddParameter("@empbanktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

'    '        dsList = objDataOperation.ExecQuery(strQ, "List")

'    '        If objDataOperation.ErrorMessage <> "" Then
'    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'    '            Throw exForce
'    '        End If

'    '        Return dsList.Tables(0).Rows.Count > 0

'    '    Catch ex As Exception
'    '        Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
'    '    Finally
'    '        exForce = Nothing
'    '        If dsList IsNot Nothing Then dsList.Dispose()
'    '        'objDataOperation = Nothing
'    '    End Try
'    'End Function

'End Class
