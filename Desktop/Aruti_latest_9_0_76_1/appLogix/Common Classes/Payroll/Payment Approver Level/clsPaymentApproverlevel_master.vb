﻿'************************************************************************************************************************************
'Class Name : clsPaymentApproverlevel_master.vb
'Purpose    : All Approver Level Opration like getList, Insert, Update, Delete, checkDuplicate
'Date       :13/02/2013
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsPaymentApproverlevel_master


    Private Shared ReadOnly mstrModuleName As String = "clsPaymentApproverlevel_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintLevelunkid As Integer
    Private mstrLevelcode As String = String.Empty
    Private mstrLevelname As String = String.Empty
    Private mintPriority As Integer
    Private mintUserunkid As Integer 'Sohail (23 Jun 2014)
    Private mblnIsactive As Boolean = True
    Private mstrLevelname1 As String = String.Empty
    Private mstrLevelname2 As String = String.Empty
    Private mblnIsinactive As Boolean = False 'Sohail (29 Oct 2014)

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelcode
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelcode() As String
        Get
            Return mstrLevelcode
        End Get
        Set(ByVal value As String)
            mstrLevelcode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelname() As String
        Get
            Return mstrLevelname
        End Get
        Set(ByVal value As String)
            mstrLevelname = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    'Sohail (23 Jun 2014) -- Start
    'Enhancement - UserUnkId in prpaymentapproverlevel_master. 
    ''' <summary>
    ''' Purpose: Get or Set Userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property
    'Sohail (23 Jun 2014) -- End

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname1
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelname1() As String
        Get
            Return mstrLevelname1
        End Get
        Set(ByVal value As String)
            mstrLevelname1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelname2
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Levelname2() As String
        Get
            Return mstrLevelname2
        End Get
        Set(ByVal value As String)
            mstrLevelname2 = value
        End Set
    End Property

    'Sohail (29 Oct 2014) -- Start
    'AKF Enhancement - Inactive option on Payment Approver Level.
    Public Property _Isinactive() As Boolean
        Get
            Return mblnIsinactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsinactive = value
        End Set
    End Property
    'Sohail (29 Oct 2014) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", userunkid " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2 " & _
              ", ISNULL(isinactive, 0) AS isinactive " & _
             "FROM prpaymentapproverlevel_master " & _
             "WHERE levelunkid = @levelunkid "
            'Sohail (29 Oct 2014) - [isinactive]
            'Sohail (23 Jun 2014) - [userunkid]

            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintLevelunkid = CInt(dtRow.Item("levelunkid"))
                mstrLevelcode = dtRow.Item("levelcode").ToString
                mstrLevelname = dtRow.Item("levelname").ToString
                If dtRow.Item("priority") IsNot DBNull.Value Then mintPriority = dtRow.Item("priority")
                mintUserunkid = CInt(dtRow.Item("userunkid")) 'Sohail (23 Jun 2014)
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrLevelname1 = dtRow.Item("levelname1").ToString
                mstrLevelname2 = dtRow.Item("levelname2").ToString
                mblnIsinactive = CBool(dtRow.Item("isinactive")) 'Sohail (29 Oct 2014)
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True _
                            , Optional ByVal blnOrderByPriority As Boolean = False, Optional ByVal strFilter As String = "") As DataSet
        '                   'Sohail (29 Oct 2014) - [strFilter]
        '                   'Sohail (11 Mar 2013) - [blnOrderByPriority]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", userunkid " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2 " & _
              ", isinactive " & _
             "FROM prpaymentapproverlevel_master " & _
             "WHERE isactive = 1 "
            'Sohail (29 Oct 2014) - [isinactive, WHERE 1 = 1]
            'Sohail (23 Jun 2014) = [prpaymentapproverlevel_master]

            If blnOnlyActive Then
                'Sohail (29 Oct 2014) -- Start
                'AKF Enhancement - Inactive option on Payment Approver Level.
                'strQ &= " AND isactive = 1 "
                strQ &= " AND isinactive = 0 "
                'Sohail (29 Oct 2014) -- End
            End If

            'Sohail (29 Oct 2014) -- Start
            'AKF Enhancement - Inactive option on Payment Approver Level.
            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            'Sohail (29 Oct 2014) -- End

            'Sohail (11 Mar 2013) -- Start
            'TRA - ENHANCEMENT
            If blnOrderByPriority = True Then
                strQ &= " ORDER BY priority  "
            End If
            'Sohail (11 Mar 2013) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Sohail (11 Mar 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function GetLevelListCount() As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT "
            strQ = "SELECT  COUNT(levelunkid) AS TotalCount " & _
                    "FROM    dbo.prpaymentapproverlevel_master " & _
                    "WHERE   isactive = 1 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables("List").Rows(0).Item("TotalCount")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLevelListCount; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (11 Mar 2013) -- End


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prpaymentapproverlevel_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrLevelcode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist("", mstrLevelname) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintPriority) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString) 'Sohail (23 Jun 2014)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@levelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname1.ToString)
            objDataOperation.AddParameter("@levelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname2.ToString)
            objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsinactive.ToString) 'Sohail (29 Oct 2014)

            strQ = "INSERT INTO prpaymentapproverlevel_master ( " & _
              "  levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", userunkid " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2" & _
              ", isinactive " & _
            ") VALUES (" & _
              "  @levelcode " & _
              ", @levelname " & _
              ", @priority " & _
              ", @userunkid " & _
              ", @isactive " & _
              ", @levelname1 " & _
              ", @levelname2" & _
              ", @isinactive " & _
            "); SELECT @@identity"
            'Sohail (29 Oct 2014) - [isinactive]
            'Sohail (23 Jun 2014) - [userunkid]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintLevelunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "prpaymentapproverlevel_master", "levelunkid", mintLevelunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prpaymentapproverlevel_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrLevelcode, "", mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Return False
        ElseIf isExist("", mstrLevelname, mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Return False
        ElseIf isPriorityExist(mintPriority, mintLevelunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLevelunkid.ToString)
            objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelcode.ToString)
            objDataOperation.AddParameter("@levelname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname.ToString)
            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString) 'Sohail (23 Jun 2014)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@levelname1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname1.ToString)
            objDataOperation.AddParameter("@levelname2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrLevelname2.ToString)
            objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsinactive.ToString) 'Sohail (29 Oct 2014)

            strQ = "UPDATE prpaymentapproverlevel_master SET " & _
              "  levelcode = @levelcode " & _
              ", levelname = @levelname " & _
              ", priority  = @priority  " & _
              ", userunkid  = @userunkid  " & _
              ", isactive = @isactive" & _
              ", levelname1 = @levelname1" & _
              ", levelname2 = @levelname2 " & _
              ", isinactive = @isinactive " & _
            "WHERE levelunkid = @levelunkid "
            'Sohail (29 Oct 2014) - [isinactive]
            'Sohail (23 Jun 2014) - [userunkid]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "prpaymentapproverlevel_master", mintLevelunkid, "levelunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "prpaymentapproverlevel_master", "levelunkid", mintLevelunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)


            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prpaymentapproverlevel_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, You cannot delete selected Approver Level. Reason: This Approver Level is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "Update prpaymentapproverlevel_master set isactive = 0 " & _
            "WHERE levelunkid = @levelunkid "

            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "prpaymentapproverlevel_master", "levelunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "select isnull(levelunkid,0) from prpayment_approver_mapping WHERE ISNULL(isvoid, 0) = 0 AND levelunkid = @levelunkid"
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", userunkid " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2 " & _
              ", isinactive " & _
             "FROM prpaymentapproverlevel_master " & _
             "WHERE 1=1 "
            'Sohail (29 Oct 2014) - [isinactive]
            'Sohail (23 Jun 2014) - [userunkid]

            If strCode.Length > 0 Then
                strQ &= " AND levelcode = @code "
            End If
            If strName.Length > 0 Then
                strQ &= " AND levelname = @name "
            End If

            If intUnkid > 0 Then
                strQ &= " AND levelunkid <> @levelunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isPriorityExist(ByVal mintPriority As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  levelunkid " & _
              ", levelcode " & _
              ", levelname " & _
              ", priority " & _
              ", isactive " & _
              ", levelname1 " & _
              ", levelname2 " & _
              ", isinactive " & _
             "FROM prpaymentapproverlevel_master " & _
             "WHERE priority = @priority " & _
             " AND isactive = 1 "
            'Sohail (29 Oct 2014) - [isactive = 1]

            If intUnkid > 0 Then
                strQ &= " AND levelunkid <> @levelunkid"
            End If

            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isPriorityExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal blnOnlyActive As Boolean = False) As DataSet
        'Sohail (29 Oct 2014) - [blnOnlyActive]
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as levelunkid, ' ' +  @name  as name UNION "
            End If
            strQ &= "SELECT levelunkid, levelname as name FROM prpaymentapproverlevel_master where isactive = 1 "

            'Sohail (29 Oct 2014) -- Start
            'AKF Enhancement - Inactive option on Payment Approver Level.
            'ORDER BY name 
            If blnOnlyActive = True Then
                strQ &= " AND isinactive = 0 "
            End If
            strQ &= " ORDER BY name "
            'Sohail (29 Oct 2014) -- End

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    Public Function GetLowerLevelPriority(ByVal intCurrentPriority As Integer, Optional ByVal blnOnlyActive As Boolean = True) As Integer
        'Sohail (29 Oct 2014) - [blnOnlyActive]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT  ISNULL(MAX(priority), -1) AS LowerLevelPriority " & _
                    "FROM    prpaymentapproverlevel_master " & _
                    "WHERE   isactive = 1 " & _
                            "AND priority < @priority "

            'Sohail (29 Oct 2014) -- Start
            'AKF Enhancement - Inactive option on Payment Approver Level.
            If blnOnlyActive = True Then
                strQ &= " AND isinactive = 0 "
            End If
            'Sohail (29 Oct 2014) -- End

            objDataOperation.AddParameter("@priority", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrentPriority)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("LowerLevelPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLowerLevelPriority", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetNextLevelPriority(ByVal intCurrentPriority As Integer, Optional ByVal blnOnlyActive As Boolean = True) As Integer
        'Sohail (29 Oct 2014) - [blnOnlyActive]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT  ISNULL(MIN(priority), -1) AS NextLevelPriority " & _
                    "FROM    prpaymentapproverlevel_master " & _
                    "WHERE   isactive = 1 " & _
                            "AND priority > @priority "

            'Sohail (29 Oct 2014) -- Start
            'AKF Enhancement - Inactive option on Payment Approver Level.
            If blnOnlyActive = True Then
                strQ &= " AND isinactive = 0 "
            End If
            'Sohail (29 Oct 2014) -- End

            objDataOperation.AddParameter("@priority", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intCurrentPriority)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("NextLevelPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNextLevelPriority", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetMaxPriority(Optional ByVal blnOnlyActive As Boolean = True) As Integer
        'Sohail (29 Oct 2014) - [blnOnlyActive]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT  ISNULL(MAX(priority), -1) AS MaxPriority " & _
                    "FROM    prpaymentapproverlevel_master " & _
                    "WHERE   isactive = 1 "

            'Sohail (29 Oct 2014) -- Start
            'AKF Enhancement - Inactive option on Payment Approver Level.
            If blnOnlyActive = True Then
                strQ &= " AND isinactive = 0 "
            End If
            'Sohail (29 Oct 2014) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("MaxPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMaxPriority", mstrModuleName)
        End Try
        Return -1
    End Function

    Public Function GetMinPriority(Optional ByVal blnOnlyActive As Boolean = True) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT  ISNULL(MIN(priority), -1) AS MinPriority " & _
                    "FROM    prpaymentapproverlevel_master " & _
                    "WHERE   isactive = 1 "

            'Sohail (29 Oct 2014) -- Start
            'AKF Enhancement - Inactive option on Payment Approver Level.
            If blnOnlyActive = True Then
                strQ &= " AND isinactive = 0 "
            End If
            'Sohail (29 Oct 2014) -- End

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return CInt(dsList.Tables(0).Rows(0).Item("MinPriority"))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetMinPriority", mstrModuleName)
        End Try
        Return -1
    End Function

    'Sohail (29 Oct 2014) -- Start
    'AKF Enhancement - Inactive option on Payment Approver Level.
    Public Function isUsedInOpenPeriod(ByVal intUnkid As Integer, ByVal intFirstOpenPeriodID As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT  prpayment_approval_tran.paymenttranunkid " & _
                    "FROM    prpayment_tran " & _
                            "LEFT JOIN prpayment_approval_tran ON prpayment_approval_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                    "WHERE   prpayment_tran.isvoid = 0 " & _
                            "AND prpayment_approval_tran.isvoid = 0 " & _
                            "AND periodunkid = @periodunkid " & _
                            "AND referenceid = @referenceid " & _
                            "AND paytypeid = @paytypeid " & _
                            "AND levelunkid = @levelunkid "

            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFirstOpenPeriodID)
            objDataOperation.AddParameter("@referenceid", SqlDbType.Int, eZeeDataType.INT_SIZE, clsPayment_tran.enPaymentRefId.PAYSLIP)
            objDataOperation.AddParameter("@paytypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, clsPayment_tran.enPayTypeId.PAYMENT)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsedInOpenPeriod; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function MakeActiveInactive(ByVal blnMakeActive As Boolean _
                                       , ByVal intUnkid As Integer _
                                       , ByVal intUserID As Integer _
                                       ) As Boolean

        Dim objDataOperation As clsDataOperation

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try


            strQ = "UPDATE   prpaymentapproverlevel_master SET " & _
                            " isinactive = @isinactive " & _
                            ", userunkid = @userunkid " & _
                    "WHERE levelunkid = @levelunkid "

            If blnMakeActive = True Then
                objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
            Else
                objDataOperation.AddParameter("@isinactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            End If

            objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserID.ToString)


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "prpaymentapproverlevel_master", "levelunkid", intUnkid, False, intUserID) = False Then
                Return False
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: MakeActiveInactive; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (29 Oct 2014) -- End


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Approver Level Code is already defined. Please define new Approver Level Code.")
            Language.setMessage(mstrModuleName, 2, "This Approver Level Name is already defined. Please define new Approver Level Name.")
            Language.setMessage(mstrModuleName, 3, "This Approver Level Priority is already assigned. Please assign new Approver Level Priority.")
            Language.setMessage(mstrModuleName, 4, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class