﻿'************************************************************************************************************************************
'Class Name : clspayment_approver_mapping.vb
'Purpose    :
'Date       :13-Feb-2013
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>

Public Class clspayment_approver_mapping
    Private Shared ReadOnly mstrModuleName As String = "clspayment_approver_mapping"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintPaymentapproverunkid As Integer
    Private mintLevelunkid As Integer
    Private mintUserapproverunkid As Integer
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentapproverunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Paymentapproverunkid() As Integer
        Get
            Return mintPaymentapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymentapproverunkid = value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set levelunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Levelunkid() As Integer
        Get
            Return mintLevelunkid
        End Get
        Set(ByVal value As Integer)
            mintLevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userapproverunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userapproverunkid() As Integer
        Get
            Return mintUserapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintUserapproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                  "  paymentapproverunkid " & _
                  ", levelunkid " & _
                  ", userapproverunkid " & _
                  ", userunkid " & _
                  ", isvoid " & _
                  ", voiduserunkid " & _
                  ", voiddatetime " & _
                  ", voidreason " & _
             "FROM prpayment_approver_mapping " & _
             "WHERE paymentapproverunkid = @paymentapproverunkid "

            objDataOperation.AddParameter("@paymentapproverunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintPaymentapproverUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintpaymentapproverunkid = CInt(dtRow.Item("paymentapproverunkid"))
                mintlevelunkid = CInt(dtRow.Item("levelunkid"))
                mintuserapproverunkid = CInt(dtRow.Item("userapproverunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            , Optional ByVal intApproverUserUnkId As Integer = 0 _
                            , Optional ByVal strFilter As String = "" _
                            ) As DataSet
        'Sohail (29 Oct 2014) - [strFilter]

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            strQ = "SELECT  prpayment_approver_mapping.paymentapproverunkid " & _
                          ", prpayment_approver_mapping.levelunkid " & _
                          ", prpaymentapproverlevel_master.levelcode " & _
                          ", prpaymentapproverlevel_master.levelname AS approver_level " & _
                          ", prpaymentapproverlevel_master.levelname1 " & _
                          ", prpaymentapproverlevel_master.levelname2 " & _
                          ", prpaymentapproverlevel_master.priority " & _
                          ", prpayment_approver_mapping.userapproverunkid " & _
                          ", prpayment_approver_mapping.userunkid " & _
                          ", hrmsConfiguration..cfuser_master.username AS approver " & _
                          ", prpayment_approver_mapping.isvoid " & _
                          ", prpayment_approver_mapping.voiduserunkid " & _
                          ", prpayment_approver_mapping.voiddatetime " & _
                          ", prpayment_approver_mapping.voidreason " & _
                    "FROM    prpayment_approver_mapping " & _
                              "JOIN hrmsConfiguration..cfuser_master ON prpayment_approver_mapping.userapproverunkid = hrmsConfiguration..cfuser_master.userunkid " & _
                            "LEFT JOIN prpaymentapproverlevel_master ON prpaymentapproverlevel_master.levelunkid = prpayment_approver_mapping.levelunkid " & _
                    "WHERE   ISNULL(prpayment_approver_mapping.isvoid, 0) = 0 " & _
                            "AND prpaymentapproverlevel_master.isactive = 1 "

            If intApproverUserUnkId > 0 Then
                strQ &= " AND userapproverunkid = @userapproverunkid "
                objDataOperation.AddParameter("@userapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkId)
            End If

            'Sohail (29 Oct 2014) -- Start
            'AKF Enhancement - Inactive option on Payment Approver Level.
            If strFilter.Trim <> "" Then
                strQ &= " AND " & strFilter
            End If
            'Sohail (29 Oct 2014) -- End

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prpayment_approver_mapping) </purpose>
    Public Function Insert() As Boolean
        If isExist(mintUserapproverunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This approver already mapped with some level.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@levelunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintlevelunkid.ToString)
            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserapproverunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            StrQ = "INSERT INTO prpayment_approver_mapping ( " & _
              "  levelunkid " & _
              ", userapproverunkid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @levelunkid " & _
              ", @userapproverunkid " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPaymentapproverUnkId = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "prpayment_approver_mapping", "paymentapproverunkid", mintPaymentapproverunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)


            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prpayment_approver_mapping) </purpose>
    Public Function Update() As Boolean
        If isExist(mintUserapproverunkid, mintPaymentapproverunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This approver already mapped with some level.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@paymentapproverunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintpaymentapproverunkid.ToString)
            objDataOperation.AddParameter("@levelunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintlevelunkid.ToString)
            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserapproverunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            StrQ = "UPDATE prpayment_approver_mapping SET " & _
              "  levelunkid = @levelunkid" & _
              ", userapproverunkid = @userapproverunkid" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE paymentapproverunkid = @paymentapproverunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "prpayment_approver_mapping", mintPaymentapproverunkid, "paymentapproverunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "prpayment_approver_mapping", "levelunkid", mintLevelunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prpayment_approver_mapping) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE prpayment_approver_mapping SET " & _
                   "  isvoid = @isvoid" & _
                   " ,voiduserunkid = @voiduserunkid" & _
                   " ,voiddatetime = @voiddatetime" & _
                   " ,voidreason = @voidreason " & _
                   "WHERE paymentapproverunkid = @paymentapproverunkid "

            objDataOperation.AddParameter("@paymentapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "prpayment_approver_mapping", "paymentapproverunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intApproverUserUnkID As Integer, Optional ByVal intFirstOpenPeriodUnkID As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Sohail (23 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            'strQ = "SELECT 1 FROM prpayment_approval_tran WHERE paymentapproverunkid = @paymentapproverunkid AND isvoid = 0 "
            'Sohail (25 Feb 2013) -- Start
            'TRA - ENHANCEMENT
            'strQ = "SELECT atprpayment_approval_tran.audituserunkid " & _
            '        "FROM   atprpayment_approval_tran " & _
            '            "JOIN prpayment_tran ON atprpayment_approval_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
            '        "WHERE  levelunkid <> -1 " & _
            '            "AND priority <> -1 " & _
            '            "AND atprpayment_approval_tran.audituserunkid = @audituserunkid " & _
            '            "AND atprpayment_approval_tran.paymenttranunkid NOT IN ( " & _
            '                                            "SELECT  paymenttranunkid " & _
            '                                            "FROM    atprpayment_approval_tran " & _
            '                                            "WHERE   audittype = 3 ) "
            strQ = "SELECT atprpayment_approval_tran.audituserunkid " & _
                    "FROM   atprpayment_approval_tran " & _
                        "JOIN prpayment_tran ON atprpayment_approval_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                    "WHERE  levelunkid <> -1 " & _
                        "AND priority <> -1 " & _
                        "AND atprpayment_approval_tran.audituserunkid = @audituserunkid " & _
                            "AND atprpayment_approval_tran.paymentapprovaltranunkid NOT IN ( " & _
                            "SELECT  paymentapprovaltranunkid " & _
                                                        "FROM    atprpayment_approval_tran " & _
                            "GROUP BY paymentapprovaltranunkid " & _
                            "HAVING  MAX(statusunkid) = 2 ) "
            'Sohail (25 Feb 2013) -- End

            If intFirstOpenPeriodUnkID > 0 Then
                strQ &= " AND periodunkid = @periodunkid "
                objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intFirstOpenPeriodUnkID)
            End If
            'Sohail (23 Feb 2013) -- End


            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverUserUnkID)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intApproverUserId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "  paymentapproverunkid " & _
                   " ,levelunkid " & _
                   " ,userapproverunkid " & _
                   " ,userunkid " & _
                   " ,isvoid " & _
                   " ,voiduserunkid " & _
                   " ,voiddatetime " & _
                   " ,voidreason " & _
                   "FROM prpayment_approver_mapping " & _
                   "WHERE userapproverunkid = @userapproverunkid " & _
                   " AND isvoid = 0 "

            If intUnkid > 0 Then
                strQ &= " AND paymentapproverunkid <> @paymentapproverunkid"
            End If

            objDataOperation.AddParameter("@userapproverunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intApproverUserId)
            objDataOperation.AddParameter("@paymentapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'Sohail (20 Feb 2013) -- Start
    'TRA - ENHANCEMENT
    Public Function GetApproverUnkIDs(Optional ByVal intPririty As Integer = -1 _
                                      , Optional ByVal intLevelUnkId As Integer = 0 _
                                      , Optional ByVal strLevelCode As String = "" _
                                      ) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' + CAST(userapproverunkid AS NVARCHAR(50)) " & _
                                           "FROM     prpayment_approver_mapping " & _
                                                    "LEFT JOIN prpaymentapproverlevel_master ON prpaymentapproverlevel_master.levelunkid = prpayment_approver_mapping.levelunkid " & _
                                           "WHERE    ISNULL(prpayment_approver_mapping.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prpaymentapproverlevel_master.isactive, 1) = 1 "

            If intPririty > -1 Then
                strQ &= " AND prpaymentapproverlevel_master.priority = @priority "
                objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPririty)
            End If

            If intLevelUnkId > 0 Then
                strQ &= " AND prpayment_approver_mapping.levelunkid  = @levelunkid "
                objDataOperation.AddParameter("@levelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intLevelUnkId)
            End If

            If strLevelCode.Trim <> "" Then
                strQ &= " AND prpaymentapproverlevel_master.levelcode = @levelcode "
                objDataOperation.AddParameter("@levelcode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strLevelCode)
            End If

            strQ &= "                   FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS ApproverIDs "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [23-OCT-2017] -- START
            'Return dsList.Tables(0).Rows(0)("ApproverIDs")
            Dim strRefUserIds As String = ""
            If dsList.Tables("List").Rows.Count > 0 Then
                Dim objConfig As New clsConfigOptions
                objConfig.GetActiveUserList_CSV(dsList.Tables(0).Rows(0)("ApproverIDs"), strRefUserIds)
                objConfig = Nothing
            End If

            Return strRefUserIds
            'S.SANDEEP [23-OCT-2017] -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverUnkIDs", mstrModuleName)
        End Try
        Return ""
    End Function

    Public Function GetAuthorizePaymentUserUnkIDs() As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' + CAST(userunkid AS NVARCHAR(50)) " & _
                                           "FROM     hrmsConfiguration..cfuser_privilege " & _
                                           "WHERE    privilegeunkid = 609 " & _
                                         "FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS ApproverIDs "


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [23-OCT-2017] -- START
            'Return dsList.Tables(0).Rows(0)("ApproverIDs")
            Dim strRefUserIds As String = ""
            If dsList.Tables("List").Rows.Count > 0 Then
                Dim objConfig As New clsConfigOptions
                objConfig.GetActiveUserList_CSV(dsList.Tables(0).Rows(0)("ApproverIDs"), strRefUserIds)
                objConfig = Nothing
            End If

            Return strRefUserIds
            'S.SANDEEP [23-OCT-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetAuthorizePaymentUserUnkIDs", mstrModuleName)
        End Try
        Return ""
    End Function
    'Sohail (20 Feb 2013) -- End

    'Sohail (10 Mar 2014) -- Start
    'Enhancement - Send Email Notification to lower levels when payment disapproved.
    Public Function GetLowerApproverUnkIDs(ByVal intCurrLevelPririty As Integer) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT  ISNULL(STUFF(( SELECT   ',' + CAST(userapproverunkid AS NVARCHAR(50)) " & _
                                           "FROM     prpayment_approver_mapping " & _
                                                    "LEFT JOIN prpaymentapproverlevel_master ON prpaymentapproverlevel_master.levelunkid = prpayment_approver_mapping.levelunkid " & _
                                           "WHERE    ISNULL(prpayment_approver_mapping.isvoid, 0) = 0 " & _
                                                    "AND ISNULL(prpaymentapproverlevel_master.isactive, 1) = 1 " & _
                                                    "AND prpaymentapproverlevel_master.priority < @priority "


            strQ &= "                   FOR " & _
                                           "XML PATH('') " & _
                                         "), 1, 1, ''), '') AS ApproverIDs "


            objDataOperation.AddParameter("@priority", SqlDbType.Int, eZeeDataType.INT_SIZE, intCurrLevelPririty)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [23-OCT-2017] -- START
            'Return dsList.Tables(0).Rows(0)("ApproverIDs")
            Dim strRefUserIds As String = ""
            If dsList.Tables("List").Rows.Count > 0 Then
                Dim objConfig As New clsConfigOptions
                objConfig.GetActiveUserList_CSV(dsList.Tables(0).Rows(0)("ApproverIDs"), strRefUserIds)
                objConfig = Nothing
            End If

            Return strRefUserIds
            'S.SANDEEP [23-OCT-2017] -- END

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetLowerApproverUnkIDs", mstrModuleName)
        End Try
        Return ""
    End Function
    'Sohail (10 Mar 2014) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, This approver already mapped with some level.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
