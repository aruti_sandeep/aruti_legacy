﻿'************************************************************************************************************************************
'Class Name : clspaymentbatchposting_tran.vb
'Purpose    :
'Date       : 01-06-2023
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
'''
Public Class clspaymentbatchposting_tran
    Private Const mstrModuleName = "clspaymentbatchposting_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private xDataOp As clsDataOperation
    Private mintPaymentBatchPostingTranunkid As Integer
    Private mintPaymentBatchPostingunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintBranchunkid As Integer
    Private mstrAccountNo As String = String.Empty
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mintDpndtbeneficetranunkid As Integer
    Private mdtTran As DataTable

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentbatchpostingtranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _PaymentBatchPostingTranunkid() As Integer
        Get
            Return mintPaymentBatchPostingTranunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymentBatchPostingTranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set paymentbatchpostingunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _PaymentBatchPostingunkid() As Integer
        Get
            Return mintPaymentBatchPostingunkid
        End Get
        Set(ByVal value As Integer)
            mintPaymentBatchPostingunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set branchunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Branchunkid() As Integer
        Get
            Return mintBranchunkid
        End Get
        Set(ByVal value As Integer)
            mintBranchunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set accountno
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _AccountNo() As String
        Get
            Return mstrAccountNo
        End Get
        Set(ByVal value As String)
            mstrAccountNo = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set dpndtbeneficetranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Dpndtbeneficetranunkid() As Integer
        Get
            Return mintDpndtbeneficetranunkid
        End Get
        Set(ByVal value As Integer)
            mintDpndtbeneficetranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TranDataTable
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TranDataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region "Constructor"
    Public Sub New()
        mdtTran = New DataTable("List")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("ischeck")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("batchname")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("periodunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeecode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("branchunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            'Hemant (04 Aug 2023) -- Start
            dCol = New DataColumn("branchname")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)
            'Hemant (04 Aug 2023) -- End

            dCol = New DataColumn("bankgroupunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("bankname")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("accountno")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("statusunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("status")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("dpndtbeneficetranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            'Hemant (04 Aug 2023) -- Start
            dCol = New DataColumn("amount")
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)
            'Hemant (04 Aug 2023) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region
    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  paymentbatchpostingtranunkid " & _
              ", paymentbatchpostingunkid " & _
              ", employeeunkid " & _
              ", branchunkid " & _
              ", accountno " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", dpndtbeneficetranunkid " & _
             "FROM prpayment_batchposting_tran " & _
             "WHERE paymentbatchpostingtranunkid = @paymentbatchpostingtranunkid "

            objDataOperation.AddParameter("@paymentbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchPostingTranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPaymentBatchPostingTranunkid = CInt(dtRow.Item("paymentbatchpostingtranunkid"))
                mintPaymentBatchPostingunkid = CInt(dtRow.Item("paymentbatchpostingunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintBranchunkid = CInt(dtRow.Item("branchunkid"))
                mstrAccountNo = dtRow.Item("accountno").ToString
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mintDpndtbeneficetranunkid = CInt(dtRow.Item("dpndtbeneficetranunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal intPaymentBatchPostingTranunkid As Integer = 0) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try

            strQ = "SELECT " & _
                      "  prpayment_batchposting_tran.paymentbatchpostingtranunkid " & _
                      ", prpayment_batchposting_tran.paymentbatchpostingunkid " & _
                      ", ISNULL(prpayment_batchposting_master.batchname, '') AS batchname" & _
                      ", prpayment_batchposting_tran.employeeunkid " & _
                      ", ISNULL(hremployee_master.employeecode, '')  AS employeecode " & _
                      ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employee " & _
                      ", prpayment_batchposting_tran.branchunkid " & _
                      ", hrmsConfiguration..cfpayrollgroup_master.groupname as  bankname" & _
                      ", hrmsConfiguration..cfbankbranch_master.branchcode " & _
                      ", hrmsConfiguration..cfbankbranch_master.branchname " & _
                      ", prpayment_batchposting_tran.accountno " & _
                      ", prpayment_batchposting_tran.isvoid " & _
                      ", prpayment_batchposting_tran.voiduserunkid " & _
                      ", prpayment_batchposting_tran.voiddatetime " & _
                      ", prpayment_batchposting_tran.voidreason " & _
                      ", prpayment_batchposting_tran.dpndtbeneficetranunkid "

            strQ &= "FROM prpayment_batchposting_tran " & _
                    " LEFT JOIN prpayment_batchposting_master ON prpayment_batchposting_master.paymentbatchpostingunkid = prpayment_batchposting_tran.paymentbatchpostingunkid" & _
                    " JOIN hremployee_master ON hremployee_master.employeeunkid = prpayment_batchposting_tran.employeeunkid " & _
                    " LEFT JOIN hrmsConfiguration..cfbankbranch_master ON hrmsConfiguration..cfbankbranch_master.branchunkid = prpayment_batchposting_tran.branchunkid " & _
                    " JOIN hrmsconfiguration..cfpayrollgroup_master ON hrmsconfiguration..cfbankbranch_master.bankgroupunkid = hrmsconfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                 "WHERE prpayment_batchposting_tran.isvoid = 0 "


            'Hemant (21 Jul 2023) -- Start
            'ENHANCEMENT(TRA) : A1X-1133 - NBC reconciliation API changes
            If intPaymentBatchPostingTranunkid > 0 Then
                strQ &= " AND prpayment_batchposting_tran.paymentbatchpostingunkid = @paymentbatchpostingunkid "
            End If

            strQ &= "UNION ALL "

            strQ &= "SELECT " & _
                     "  prpayment_batchposting_tran.paymentbatchpostingtranunkid " & _
                     ", prpayment_batchposting_tran.paymentbatchpostingunkid " & _
                     ", ISNULL(prpayment_batchposting_master.batchname, '') AS batchname" & _
                     ", prpayment_batchposting_tran.employeeunkid " & _
                     ", '000000'  AS employeecode " & _
                     ", '' AS employee " & _
                     ", prpayment_batchposting_tran.branchunkid " & _
                     ", hrmsConfiguration..cfpayrollgroup_master.groupname as  bankname" & _
                     ", hrmsConfiguration..cfbankbranch_master.branchcode " & _
                     ", hrmsConfiguration..cfbankbranch_master.branchname " & _
                     ", prpayment_batchposting_tran.accountno " & _
                     ", prpayment_batchposting_tran.isvoid " & _
                     ", prpayment_batchposting_tran.voiduserunkid " & _
                     ", prpayment_batchposting_tran.voiddatetime " & _
                     ", prpayment_batchposting_tran.voidreason " & _
                     ", prpayment_batchposting_tran.dpndtbeneficetranunkid "

            strQ &= "FROM prpayment_batchposting_tran " & _
                    " LEFT JOIN prpayment_batchposting_master ON prpayment_batchposting_master.paymentbatchpostingunkid = prpayment_batchposting_tran.paymentbatchpostingunkid" & _
                    " LEFT JOIN hrmsConfiguration..cfbankbranch_master ON hrmsConfiguration..cfbankbranch_master.branchunkid = prpayment_batchposting_tran.branchunkid " & _
                    " JOIN hrmsconfiguration..cfpayrollgroup_master ON hrmsconfiguration..cfbankbranch_master.bankgroupunkid = hrmsconfiguration..cfpayrollgroup_master.groupmasterunkid " & _
                 "WHERE prpayment_batchposting_tran.isvoid = 0 " & _
                    " AND prpayment_batchposting_tran.employeeunkid = -1 "
            'Hemant (21 Jul 2023) -- End

            If intPaymentBatchPostingTranunkid > 0 Then
                strQ &= " AND prpayment_batchposting_tran.paymentbatchpostingunkid = @paymentbatchpostingunkid "
                objDataOperation.AddParameter("@paymentbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPaymentBatchPostingTranunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prpayment_batchposting_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@paymentbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchPostingunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountNo.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficetranunkid.ToString)


            strQ = "INSERT INTO prpayment_batchposting_tran ( " & _
              "  paymentbatchpostingunkid " & _
              ", employeeunkid " & _
              ", branchunkid " & _
              ", accountno " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
              ", dpndtbeneficetranunkid " & _
            ") VALUES (" & _
              "  @paymentbatchpostingunkid " & _
              ", @employeeunkid " & _
              ", @branchunkid " & _
              ", @accountno " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
              ", @dpndtbeneficetranunkid " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPaymentBatchPostingTranunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrail(objDataOperation, enAuditType.ADD) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAll() As Boolean
        Try
            If xDataOp Is Nothing Then
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            Else
                objDataOperation = xDataOp
            End If

            For Each drRow As DataRow In mdtTran.Rows
                mintBranchunkid = CInt(drRow.Item("branchunkid"))
                mstrAccountNo = CStr(drRow.Item("AccountNo"))
                mintEmployeeunkid = CInt(drRow.Item("employeeunkid"))
                mintDpndtbeneficetranunkid = CInt(drRow.Item("dpndtbeneficetranunkid"))
                If Insert() = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAll; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prpayment_batchposting_tran) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintPaymentBatchPostingTranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@paymentbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchPostingTranunkid.ToString)
            objDataOperation.AddParameter("@paymentbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchPostingunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOperation.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountNo.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)
            objDataOperation.AddParameter("@dpndtbeneficetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDpndtbeneficetranunkid.ToString)

            strQ = "UPDATE prpayment_batchposting_tran SET " & _
              "  paymentbatchpostingunkid = @paymentbatchpostingunkid " & _
              ", employeeunkid = @employeeunkid" & _
              ", branchunkid = @branchunkid" & _
              ", accountno = @accountno" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
              ", dpndtbeneficetranunkid = @dpndtbeneficetranunkid " & _
            "WHERE paymentbatchpostingtranunkid = @paymentbatchpostingtranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrail(objDataOperation, enAuditType.EDIT) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prpayment_batchposting_tran) </purpose>
    Public Function Delete() As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE prpayment_batchposting_tran SET " & _
             "  isvoid = 1 " & _
             ", voiduserunkid = @voiduserunkid" & _
             ", voiddatetime = @voiddatetime" & _
             ", voidreason = @voidreason " & _
           "WHERE paymentbatchpostingtranunkid = @paymentbatchpostingtranunkid "


            objDataOperation.AddParameter("@paymentbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchPostingTranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._xDataOp = objDataOperation
            Me._PaymentBatchPostingTranunkid = mintPaymentBatchPostingTranunkid

            If InsertAuditTrail(objDataOperation, enAuditType.DELETE) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@paymentbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intBranchUnkId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal strFilter As String = "", Optional ByRef strAccountNo As String = "", Optional ByRef intPaymentBatchPostingTranunkid As Integer = 0) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strAccountNo = ""
            intPaymentBatchPostingTranunkid = 0

            strQ = "SELECT " & _
              "  paymentbatchpostingtranunkid " & _
              ", paymentbatchpostingunkid " & _
              ", employeeunkid " & _
              ", branchunkid " & _
              ", accountno " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", dpndtbeneficetranunkid " & _
             "FROM prpayment_batchposting_tran " & _
             "WHERE isvoid = 0 " & _
             "AND branchunkid = @branchunkid "

            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter & " "
            End If

            If intUnkid > 0 Then
                strQ &= " AND paymentbatchpostingtranunkid <> @paymentbatchpostingtranunkid"
                objDataOperation.AddParameter("@paymentbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBranchUnkId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                strAccountNo = dsList.Tables(0).Rows(0).Item("accountno").ToString
                intPaymentBatchPostingTranunkid = CInt(dsList.Tables(0).Rows(0).Item("paymentbatchpostingtranunkid"))
                Return True
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrail(ByVal objDataOp As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        Try
            objDataOp.ClearParameters()
            objDataOp.AddParameter("@paymentbatchpostingtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchPostingTranunkid.ToString)
            objDataOp.AddParameter("@paymentbatchpostingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPaymentBatchPostingunkid.ToString)
            objDataOp.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOp.AddParameter("@branchunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBranchunkid.ToString)
            objDataOp.AddParameter("@accountno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAccountNo.ToString)
            objDataOp.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOp.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOp.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOp.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOp.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOp.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOp.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)

            strQ = "INSERT INTO atprpayment_batchposting_tran ( " & _
              "  tranguid " & _
              ", paymentbatchpostingtranunkid " & _
              ", paymentbatchpostingunkid " & _
              ", employeeunkid " & _
              ", branchunkid " & _
              ", accountno " & _
              ", audituserunkid " & _
              ", audittype " & _
              ", auditdatetime " & _
              ", isweb " & _
              ", ip " & _
              ", host " & _
              ", form_name " & _
            ") VALUES (" & _
              "  LOWER(NEWID()) " & _
              ", @paymentbatchpostingtranunkid " & _
              ", @paymentbatchpostingunkid " & _
              ", @employeeunkid " & _
              ", @branchunkid " & _
              ", @accountno " & _
              ", @audituserunkid " & _
              ", @audittype " & _
              ", @auditdatetime " & _
              ", @isweb " & _
              ", @ip " & _
              ", @host " & _
              ", @form_name " & _
            "); SELECT @@identity"


            dsList = objDataOp.ExecQuery(strQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function
End Class
