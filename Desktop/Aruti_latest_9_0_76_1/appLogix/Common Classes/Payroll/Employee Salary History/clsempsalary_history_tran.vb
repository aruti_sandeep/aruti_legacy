﻿Option Strict On

'************************************************************************************************************************************
'Class Name :clsempsalary_history_tran.vb
'Purpose    :
'Date       :12/05/2016
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

#End Region

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsempsalary_history_tran

    Private Shared ReadOnly mstrModuleName As String = "clsempsalary_history_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintEmpsalaryhistorytranunkid As Integer = 0
    Private mintSalaryincrementtranunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mintEmployeeunkid As Integer = 0
    Private mdtIncrementdate As Date = Nothing
    Private msinCurrentscale As Decimal = 0
    Private msinIncrement As Decimal = 0
    Private msinNewscale As Decimal = 0
    Private mintGradegroupunkid As Integer = 0
    Private mintGradeunkid As Integer = 0
    Private mintGradelevelunkid As Integer = 0
    Private mblnIsgradechange As Boolean = False
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private mblnIsfromemployee As Boolean = False
    Private mintReason_Id As Integer = 0
    Private mintIncrement_Mode As Integer = 0
    Private mdblPercentage As Double = 0
    Private mblnIsapproved As Boolean = False
    Private mintApproveruserunkid As Integer = 0
    Private mdtPsoft_Syncdatetime As Date = Nothing
    Private mintRehiretranunkid As Integer = 0
    Private mintChangetypeid As Integer = 0
    Private mdtPromotion_Date As Date = Nothing
    Private mobjDataOperation As clsDataOperation = Nothing
    Private mstrWebFormName As String = String.Empty
    Private mstrWebIP As String = ""
    Private mstrWebhostName As String = ""
    Private mintLogEmployeeUnkid As Integer = -1

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set empsalaryhistorytranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Empsalaryhistorytranunkid() As Integer
        Get
            Return mintEmpsalaryhistorytranunkid
        End Get
        Set(ByVal value As Integer)
            mintEmpsalaryhistorytranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set salaryincrementtranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Salaryincrementtranunkid() As Integer
        Get
            Return mintSalaryincrementtranunkid
        End Get
        Set(ByVal value As Integer)
            mintSalaryincrementtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set incrementdate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Incrementdate() As Date
        Get
            Return mdtIncrementdate
        End Get
        Set(ByVal value As Date)
            mdtIncrementdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set currentscale
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Currentscale() As Decimal
        Get
            Return msinCurrentscale
        End Get
        Set(ByVal value As Decimal)
            msinCurrentscale = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set increment
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Increment() As Decimal
        Get
            Return msinIncrement
        End Get
        Set(ByVal value As Decimal)
            msinIncrement = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set newscale
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Newscale() As Decimal
        Get
            Return msinNewscale
        End Get
        Set(ByVal value As Decimal)
            msinNewscale = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradegroupunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Gradegroupunkid() As Integer
        Get
            Return mintGradegroupunkid
        End Get
        Set(ByVal value As Integer)
            mintGradegroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Gradeunkid() As Integer
        Get
            Return mintGradeunkid
        End Get
        Set(ByVal value As Integer)
            mintGradeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gradelevelunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Gradelevelunkid() As Integer
        Get
            Return mintGradelevelunkid
        End Get
        Set(ByVal value As Integer)
            mintGradelevelunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isgradechange
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isgradechange() As Boolean
        Get
            Return mblnIsgradechange
        End Get
        Set(ByVal value As Boolean)
            mblnIsgradechange = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfromemployee
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isfromemployee() As Boolean
        Get
            Return mblnIsfromemployee
        End Get
        Set(ByVal value As Boolean)
            mblnIsfromemployee = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reason_id
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Reason_Id() As Integer
        Get
            Return mintReason_Id
        End Get
        Set(ByVal value As Integer)
            mintReason_Id = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set increment_mode
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Increment_Mode() As Integer
        Get
            Return mintIncrement_Mode
        End Get
        Set(ByVal value As Integer)
            mintIncrement_Mode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set percentage
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Percentage() As Double
        Get
            Return mdblPercentage
        End Get
        Set(ByVal value As Double)
            mdblPercentage = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isapproved
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isapproved() As Boolean
        Get
            Return mblnIsapproved
        End Get
        Set(ByVal value As Boolean)
            mblnIsapproved = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approveruserunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Approveruserunkid() As Integer
        Get
            Return mintApproveruserunkid
        End Get
        Set(ByVal value As Integer)
            mintApproveruserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set psoft_syncdatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Psoft_Syncdatetime() As Date
        Get
            Return mdtPsoft_Syncdatetime
        End Get
        Set(ByVal value As Date)
            mdtPsoft_Syncdatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set rehiretranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Rehiretranunkid() As Integer
        Get
            Return mintRehiretranunkid
        End Get
        Set(ByVal value As Integer)
            mintRehiretranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set changetypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Changetypeid() As Integer
        Get
            Return mintChangetypeid
        End Get
        Set(ByVal value As Integer)
            mintChangetypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set promotion_date
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Promotion_Date() As Date
        Get
            Return mdtPromotion_Date
        End Get
        Set(ByVal value As Date)
            mdtPromotion_Date = value
        End Set
    End Property

    Public WriteOnly Property _objDataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            mobjDataOperation = value
        End Set
    End Property

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property

    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If

        'S.SANDEEP [25 OCT 2016] -- START
        'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
        objDataOperation.ClearParameters()
        'S.SANDEEP [25 OCT 2016] -- END

        Try
            strQ = "SELECT " & _
                   "  empsalaryhistorytranunkid " & _
                   ", ISNULL(salaryincrementtranunkid,0) AS salaryincrementtranunkid " & _
                   ", ISNULL(periodunkid,0) AS periodunkid " & _
                   ", ISNULL(employeeunkid,0) AS employeeunkid " & _
                   ", ISNULL(incrementdate,NULL) AS incrementdate " & _
                   ", ISNULL(currentscale,0) AS currentscale " & _
                   ", ISNULL(increment,0) AS increment " & _
                   ", ISNULL(newscale,0) AS newscale " & _
                   ", ISNULL(gradegroupunkid,0) AS gradegroupunkid " & _
                   ", ISNULL(gradeunkid,0) AS gradeunkid " & _
                   ", ISNULL(gradelevelunkid,0) AS gradelevelunkid " & _
                   ", ISNULL(isgradechange,0) AS isgradechange " & _
                   ", ISNULL(userunkid,0) AS userunkid " & _
                   ", ISNULL(isvoid,0) AS isvoid " & _
                   ", ISNULL(voiduserunkid,0) AS voiduserunkid " & _
                   ", ISNULL(voiddatetime,0) AS voiddatetime " & _
                   ", ISNULL(voidreason,'') AS voidreason " & _
                   ", ISNULL(isfromemployee,0) AS isfromemployee " & _
                   ", ISNULL(reason_id,0) AS reason_id " & _
                   ", ISNULL(increment_mode,0) AS increment_mode " & _
                   ", ISNULL(percentage,0) AS percentage " & _
                   ", ISNULL(isapproved,0) AS isapproved " & _
                   ", ISNULL(approveruserunkid,0) AS approveruserunkid " & _
                   ", ISNULL(psoft_syncdatetime,NULL) AS psoft_syncdatetime " & _
                   ", ISNULL(rehiretranunkid,0) AS rehiretranunkid " & _
                   ", ISNULL(changetypeid,0) AS changetypeid " & _
                   ", ISNULL(promotion_date,NULL) AS promotion_date " & _
                   "FROM prempsalary_history_tran " & _
                   "WHERE empsalaryhistorytranunkid = @empsalaryhistorytranunkid "

            objDataOperation.AddParameter("@empsalaryhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpsalaryhistorytranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintEmpsalaryhistorytranunkid = CInt(dtRow.Item("empsalaryhistorytranunkid"))
                mintSalaryincrementtranunkid = CInt(dtRow.Item("salaryincrementtranunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                If IsDBNull(dtRow.Item("incrementdate")) = False Then
                    mdtIncrementdate = CDate(dtRow.Item("incrementdate"))
                Else
                    mdtIncrementdate = Nothing
                End If
                msinCurrentscale = CDec(dtRow.Item("currentscale"))
                msinIncrement = CDec(dtRow.Item("increment"))
                msinNewscale = CDec(dtRow.Item("newscale"))
                mintGradegroupunkid = CInt(dtRow.Item("gradegroupunkid"))
                mintGradeunkid = CInt(dtRow.Item("gradeunkid"))
                mintGradelevelunkid = CInt(dtRow.Item("gradelevelunkid"))
                mblnIsgradechange = CBool(dtRow.Item("isgradechange"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = CDate(dtRow.Item("voiddatetime"))
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mblnIsfromemployee = CBool(dtRow.Item("isfromemployee"))
                mintReason_Id = CInt(dtRow.Item("reason_id"))
                mintIncrement_Mode = CInt(dtRow.Item("increment_mode"))
                mdblPercentage = CDbl(dtRow.Item("percentage"))
                mblnIsapproved = CBool(dtRow.Item("isapproved"))
                mintApproveruserunkid = CInt(dtRow.Item("approveruserunkid"))
                If IsDBNull(dtRow.Item("psoft_syncdatetime")) = False Then
                    mdtPsoft_Syncdatetime = CDate(dtRow.Item("psoft_syncdatetime"))
                Else
                    mdtPsoft_Syncdatetime = Nothing
                End If
                mintRehiretranunkid = CInt(dtRow.Item("rehiretranunkid"))
                mintChangetypeid = CInt(dtRow.Item("changetypeid"))
                If IsDBNull(dtRow.Item("promotion_date")) = False Then
                    mdtPromotion_Date = CDate(dtRow.Item("promotion_date"))
                Else
                    mdtPromotion_Date = Nothing
                End If

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal intEmployeeUnkid As Integer, Optional ByVal dtChangeDate As Date = Nothing) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "     prempsalary_history_tran.salaryincrementtranunkid " & _
                   "    ,prempsalary_history_tran.periodunkid " & _
                   "    ,ISNULL(hremployee_master.firstname + ' ' + hremployee_master.othername + ' ' + hremployee_master.surname,'') AS employeename " & _
                   "    ,ISNULL(cfcommon_period_tran.period_name,'') AS period_name " & _
                   "    ,prempsalary_history_tran.employeeunkid " & _
                   "    ,prempsalary_history_tran.incrementdate AS incrementdate " & _
                   "    ,prempsalary_history_tran.currentscale " & _
                   "    ,prempsalary_history_tran.increment " & _
                   "    ,prempsalary_history_tran.newscale " & _
                   "    ,prempsalary_history_tran.gradegroupunkid " & _
                   "    ,ISNULL(hrgradegroup_master.name, '') AS GradeGroup " & _
                   "    ,prempsalary_history_tran.gradeunkid " & _
                   "    ,ISNULL(hrgrade_master.name, '') AS Grade " & _
                   "    ,prempsalary_history_tran.gradelevelunkid " & _
                   "    ,ISNULL(hrgradelevel_master.name, '') AS GradeLevel " & _
                   "    ,prempsalary_history_tran.isgradechange " & _
                   "    ,prempsalary_history_tran.isfromemployee " & _
                   "    ,prempsalary_history_tran.userunkid " & _
                   "    ,prempsalary_history_tran.isvoid " & _
                   "    ,prempsalary_history_tran.voiduserunkid " & _
                   "    ,prempsalary_history_tran.voiddatetime " & _
                   "    ,prempsalary_history_tran.voidreason " & _
                   "    ,prempsalary_history_tran.reason_id " & _
                   "    ,ISNULL(prempsalary_history_tran.increment_mode, 0) AS increment_mode " & _
                   "    ,CASE WHEN (prempsalary_history_tran.increment_mode = 1 OR prempsalary_history_tran.isgradechange = 1) THEN @GRD " & _
                   "          WHEN prempsalary_history_tran.increment_mode = 2 THEN @PCT " & _
                   "          WHEN prempsalary_history_tran.increment_mode = 3 THEN @AMT " & _
                   "     ELSE '' END AS ChangeBy " & _
                   "    ,ISNULL(prempsalary_history_tran.percentage, 0) AS percentage " & _
                   "    ,ISNULL(prempsalary_history_tran.isapproved, 0) AS isapproved " & _
                   "    ,ISNULL(prempsalary_history_tran.approveruserunkid, 0) AS approveruserunkid " & _
                   "    ,prempsalary_history_tran.psoft_syncdatetime " & _
                   "    ,ISNULL(rehiretranunkid,0) AS rehiretranunkid " & _
                   "    ,CASE WHEN changetypeid = '" & enSalaryChangeType.SIMPLE_SALARY_CHANGE & "' THEN @SSC " & _
                   "          WHEN changetypeid = '" & enSalaryChangeType.SALARY_CHANGE_WITH_PROMOTION & "' THEN @SCP " & _
                   "     END AS changetype " & _
                   "    ,prempsalary_history_tran.changetypeid " & _
                   "    ,prempsalary_history_tran.promotion_date " & _
                   "    ,ISNULL(cfcommon_master.name,'') AS changereason " & _
                   "    ,prempsalary_history_tran.empsalaryhistorytranunkid " & _
                   "FROM prempsalary_history_tran " & _
                   "    LEFT JOIN cfcommon_period_tran ON prempsalary_history_tran.periodunkid = cfcommon_period_tran.periodunkid " & _
                   "    LEFT JOIN hremployee_master ON prempsalary_history_tran.employeeunkid = hremployee_master.employeeunkid " & _
                   "    LEFT JOIN hrgradegroup_master ON hrgradegroup_master.gradegroupunkid = prempsalary_history_tran.gradegroupunkid " & _
                   "    LEFT JOIN hrgrade_master ON hrgrade_master.gradeunkid = prempsalary_history_tran.gradeunkid " & _
                   "    LEFT JOIN hrgradelevel_master ON hrgradelevel_master.gradelevelunkid = prempsalary_history_tran.gradelevelunkid " & _
                   "    LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = prempsalary_history_tran.reason_id AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.SALINC_REASON & " " & _
                   "WHERE prempsalary_history_tran.isvoid = 0 AND hremployee_master.employeeunkid = @employeeunkid "

            If dtChangeDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),prempsalary_history_tran.incrementdate,112) = @incrementdate "
                objDataOperation.AddParameter("@incrementdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtChangeDate))
            End If

            strQ &= "ORDER BY incrementdate DESC "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkid)
            objDataOperation.AddParameter("@SSC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 717, "Simple Salary Change"))
            objDataOperation.AddParameter("@SCP", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 718, "Salary Change With Promotion"))
            objDataOperation.AddParameter("@GRD", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 300, "Grade"))
            objDataOperation.AddParameter("@PCT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 301, "Percentage"))
            objDataOperation.AddParameter("@AMT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 302, "Amount"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (prempsalary_history_tran) </purpose>
    Public Function Insert(ByVal dtCurrentDateTime As DateTime, Optional ByVal xAttachments As DataTable = Nothing) As Boolean
        'S.SANDEEP |05-JUN-2023| -- START {xAttachments} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If isExist(mdtIncrementdate, mintEmployeeunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry! Salary History for the employee on the selected change date is already done.")
            Return False
        End If

        If mdtPromotion_Date <> Nothing Then
            If isExist(mdtPromotion_Date, mintEmployeeunkid, , True) Then
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry! Salary History for the employee on the selected promotion date is already done.")
                Return False
            End If
        End If

        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalaryincrementtranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

            If mdtIncrementdate <> Nothing Then
                objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIncrementdate)
            Else
                objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@currentscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinCurrentscale.ToString)
            objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinIncrement.ToString)
            objDataOperation.AddParameter("@newscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinNewscale.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@isgradechange", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgradechange.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)

            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@reason_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReason_Id.ToString)
            objDataOperation.AddParameter("@increment_mode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIncrement_Mode.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)

            If mdtPsoft_Syncdatetime <> Nothing Then
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPsoft_Syncdatetime)
            Else
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)

            End If
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@changetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangetypeid.ToString)

            If mdtPromotion_Date <> Nothing Then
                objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPromotion_Date)
            Else
                objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            strQ = "INSERT INTO prempsalary_history_tran ( " & _
                       "  salaryincrementtranunkid " & _
                       ", periodunkid " & _
                       ", employeeunkid " & _
                       ", incrementdate " & _
                       ", currentscale " & _
                       ", increment " & _
                       ", newscale " & _
                       ", gradegroupunkid " & _
                       ", gradeunkid " & _
                       ", gradelevelunkid " & _
                       ", isgradechange " & _
                       ", userunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                       ", isfromemployee " & _
                       ", reason_id " & _
                       ", increment_mode " & _
                       ", percentage " & _
                       ", isapproved " & _
                       ", approveruserunkid " & _
                       ", psoft_syncdatetime " & _
                       ", rehiretranunkid " & _
                       ", changetypeid " & _
                       ", promotion_date" & _
                   ") VALUES (" & _
                       "  @salaryincrementtranunkid " & _
                       ", @periodunkid " & _
                       ", @employeeunkid " & _
                       ", @incrementdate " & _
                       ", @currentscale " & _
                       ", @increment " & _
                       ", @newscale " & _
                       ", @gradegroupunkid " & _
                       ", @gradeunkid " & _
                       ", @gradelevelunkid " & _
                       ", @isgradechange " & _
                       ", @userunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                       ", @isfromemployee " & _
                       ", @reason_id " & _
                       ", @increment_mode " & _
                       ", @percentage " & _
                       ", @isapproved " & _
                       ", @approveruserunkid " & _
                       ", @psoft_syncdatetime " & _
                       ", @rehiretranunkid " & _
                       ", @changetypeid " & _
                       ", @promotion_date" & _
                   "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintEmpsalaryhistorytranunkid = CInt(dsList.Tables(0).Rows(0).Item(0))

            If InsertAuditTrailForSalaryHistory(objDataOperation, enAuditType.ADD, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-933,934
            '==========================SALARY DOCUMENT ATTACHMENT ================================================== > START
            If xAttachments IsNot Nothing Then
                Dim drRow = xAttachments.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "")
                If drRow.Count > 0 Then
                    Dim dtTable As DataTable = drRow.CopyToDataTable()
                    For Each drow As DataRow In dtTable.Rows
                        drow("transactionunkid") = mintEmpsalaryhistorytranunkid
                        drow("userunkid") = mintUserunkid
                    Next

                    Dim objDocument As New clsScan_Attach_Documents
                    objDocument._Datatable = dtTable
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objDocument = Nothing
                End If
            End If
            '========================== SALARY DOCUMENT ATTACHMENT ================================================== > END
            'S.SANDEEP |05-JUN-2023| -- END


            If mobjDataOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

        Catch ex As Exception
            If mobjDataOperation Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (prempsalary_history_tran) </purpose>
    Public Function Update(ByVal dtCurrentDateTime As DateTime, Optional ByVal xAttachments As DataTable = Nothing) As Boolean
        'S.SANDEEP |05-JUN-2023| -- START {xAttachments} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If isExist(mdtIncrementdate, mintEmployeeunkid, mintEmpsalaryhistorytranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry! Salary History for the employee on the selected change date is already done.")
            Return False
        End If

        If mdtPromotion_Date <> Nothing Then
            If isExist(mdtPromotion_Date, mintEmployeeunkid, mintEmpsalaryhistorytranunkid, True) Then
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry! Salary History for the employee on the selected promotion date is already done.")
                Return False
            End If
        End If

        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@empsalaryhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpsalaryhistorytranunkid.ToString)
            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalaryincrementtranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            If mdtIncrementdate <> Nothing Then
                objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIncrementdate)
            Else
                objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@currentscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinCurrentscale.ToString)
            objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinIncrement.ToString)
            objDataOperation.AddParameter("@newscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinNewscale.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@isgradechange", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgradechange.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@reason_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReason_Id.ToString)
            objDataOperation.AddParameter("@increment_mode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIncrement_Mode.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            If mdtPsoft_Syncdatetime <> Nothing Then
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPsoft_Syncdatetime)
            Else
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@changetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangetypeid.ToString)
            If mdtPromotion_Date <> Nothing Then
                objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPromotion_Date)
            Else
                objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            strQ = "UPDATE prempsalary_history_tran SET " & _
                       "  salaryincrementtranunkid = @salaryincrementtranunkid" & _
                       ", periodunkid = @periodunkid" & _
                       ", employeeunkid = @employeeunkid" & _
                       ", incrementdate = @incrementdate" & _
                       ", currentscale = @currentscale" & _
                       ", increment = @increment" & _
                       ", newscale = @newscale" & _
                       ", gradegroupunkid = @gradegroupunkid" & _
                       ", gradeunkid = @gradeunkid" & _
                       ", gradelevelunkid = @gradelevelunkid" & _
                       ", isgradechange = @isgradechange" & _
                       ", userunkid = @userunkid" & _
                       ", isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidreason = @voidreason" & _
                       ", isfromemployee = @isfromemployee" & _
                       ", reason_id = @reason_id" & _
                       ", increment_mode = @increment_mode" & _
                       ", percentage = @percentage" & _
                       ", isapproved = @isapproved" & _
                       ", approveruserunkid = @approveruserunkid" & _
                       ", psoft_syncdatetime = @psoft_syncdatetime" & _
                       ", rehiretranunkid = @rehiretranunkid" & _
                       ", changetypeid = @changetypeid" & _
                       ", promotion_date = @promotion_date " & _
                   "WHERE empsalaryhistorytranunkid = @empsalaryhistorytranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrailForSalaryHistory(objDataOperation, enAuditType.EDIT, dtCurrentDateTime) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-933,934
            '==========================SALARY DOCUMENT ATTACHMENT ================================================== > START
            If xAttachments IsNot Nothing Then
                Dim drRow = xAttachments.AsEnumerable().Where(Function(x) x.Field(Of String)("AUD") <> "")
                If drRow.Count > 0 Then
                    Dim dtTable As DataTable = drRow.CopyToDataTable()
                    For Each drow As DataRow In dtTable.Rows
                        drow("transactionunkid") = mintEmpsalaryhistorytranunkid
                        drow("userunkid") = mintUserunkid
                    Next

                    Dim objDocument As New clsScan_Attach_Documents
                    objDocument._Datatable = dtTable
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                    objDocument = Nothing
                End If
            End If
            '========================== SALARY DOCUMENT ATTACHMENT ================================================== > END
            'S.SANDEEP |05-JUN-2023| -- END

            If mobjDataOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
        Return True
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (prempsalary_history_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal dtCurrentDateTime As DateTime, Optional ByVal xAttachments As DataTable = Nothing) As Boolean
        'S.SANDEEP |05-JUN-2023| -- START {xAttachments} -- END
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        Dim objDocument As New clsScan_Attach_Documents        'S.SANDEEP |05-JUN-2023| -- START -- END

        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@empsalaryhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            'S.SANDEEP [25 OCT 2016] -- START
            'strQ = "UPDATE prempsalary_history_tran SET " & _
            '                       "  isvoid = @isvoid" & _
            '                       ", voiduserunkid = @voiduserunkid" & _
            '                       ", voiddatetime = @voiddatetime" & _
            '                       ", voidreason = @voidreason" & _
            '"WHERE empsalaryhistorytranunkid = @empsalaryhistorytranunkid "

            strQ = "UPDATE prempsalary_history_tran SET " & _
                       "  isvoid = @isvoid " & _
                       ", voiduserunkid = @voiduserunkid " & _
                       ", voiddatetime = @voiddatetime " & _
                       ", voidreason = @voidreason " & _
                   "WHERE empsalaryhistorytranunkid = @empsalaryhistorytranunkid "
            'S.SANDEEP [25 OCT 2016] -- END
            

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [17 SEP 2016] -- START
            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-933,934
            objDataOperation.ClearParameters()
            '_objDataOperation = objDataOperation
            '_Empsalaryhistorytranunkid = intUnkid
            'S.SANDEEP |05-JUN-2023| -- END
            'S.SANDEEP [17 SEP 2016] -- END

            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-933,934
            'If InsertAuditTrailForSalaryHistory(objDataOperation, enAuditType.ADD, dtCurrentDateTime) = False Then
            If InsertAuditTrailForSalaryHistory(objDataOperation, enAuditType.DELETE, dtCurrentDateTime) = False Then
                'S.SANDEEP |05-JUN-2023| -- END
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-933,934
            '========================== REHIRE DOCUMENT ATTACHMENT   ================================================== > START
            If xAttachments IsNot Nothing AndAlso xAttachments.Rows.Count > 0 Then
                For Each iRow As DataRow In xAttachments.Rows
                    iRow("AUD") = "D"
                Next
                objDocument._Datatable = xAttachments
                objDocument.InsertUpdateDelete_Documents(objDataOperation)
                If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
                objDocument = Nothing
            End If
            '========================== REHIRE DOCUMENT ATTACHMENT ================================================== > END
            'S.SANDEEP |0-JUN-2023| -- END

            If mobjDataOperation Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If
            'S.SANDEEP [17 SEP 2016] -- START
            Return True
            'S.SANDEEP [17 SEP 2016] -- END
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal dtChangeDate As DateTime, ByVal intEmployeeId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal blnCheckPromotionDate As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation

        If mobjDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = mobjDataOperation
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "SELECT " & _
                   "  empsalaryhistorytranunkid " & _
                   ", salaryincrementtranunkid " & _
                   ", periodunkid " & _
                   ", employeeunkid " & _
                   ", incrementdate " & _
                   ", currentscale " & _
                   ", increment " & _
                   ", newscale " & _
                   ", gradegroupunkid " & _
                   ", gradeunkid " & _
                   ", gradelevelunkid " & _
                   ", isgradechange " & _
                   ", userunkid " & _
                   ", isvoid " & _
                   ", voiduserunkid " & _
                   ", voiddatetime " & _
                   ", voidreason " & _
                   ", isfromemployee " & _
                   ", reason_id " & _
                   ", increment_mode " & _
                   ", percentage " & _
                   ", isapproved " & _
                   ", approveruserunkid " & _
                   ", psoft_syncdatetime " & _
                   ", rehiretranunkid " & _
                   ", changetypeid " & _
                   ", promotion_date " & _
                   "FROM prempsalary_history_tran " & _
                   "WHERE isvoid = 0 " & _
                   "   AND employeeunkid = @employeeunkid "
            If blnCheckPromotionDate Then
                strQ &= "   AND CONVERT(CHAR(8), promotion_date, 112) = @promotion_date "
                objDataOperation.AddParameter("@promotion_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtChangeDate))
            Else
                strQ &= "   AND CONVERT(CHAR(8), incrementdate, 112) = @incrementdate "
                objDataOperation.AddParameter("@incrementdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtChangeDate))
            End If

            If intUnkid > 0 Then
                strQ &= " AND empsalaryhistorytranunkid <> @empsalaryhistorytranunkid"
                objDataOperation.AddParameter("@empsalaryhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, intEmployeeId)


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If mobjDataOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP [25 OCT 2016] -- START
    'ENHANCEMENT : APPOINTMENT DATE CHANGES TO TRANSACTION TABLE
    Public Function InsertAuditTrailForSalaryHistory(ByVal objDataOperation As clsDataOperation, ByVal AuditType As enAuditType, ByVal dtCurrentDateAndTime As Date) As Boolean
        'Private Function InsertAuditTrailForSalaryHistory(ByVal objDataOperation As clsDataOperation, ByVal AuditType As enAuditType, ByVal dtCurrentDateAndTime As Date) As Boolean
        'S.SANDEEP [25 OCT 2016] -- END
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
            If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()

            strQ = "INSERT INTO atprempsalary_history_tran ( " & _
                       "  empsalaryhistorytranunkid " & _
                       ", salaryincrementtranunkid " & _
                       ", periodunkid " & _
                       ", employeeunkid " & _
                       ", incrementdate " & _
                       ", currentscale " & _
                       ", increment " & _
                       ", newscale " & _
                       ", gradegroupunkid " & _
                       ", gradeunkid " & _
                       ", gradelevelunkid " & _
                       ", isgradechange " & _
                       ", isfromemployee " & _
                       ", audittype " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", ip " & _
                       ", machine_name " & _
                       ", reason_id " & _
                       ", increment_mode " & _
                       ", percentage " & _
                       ", form_name " & _
                       ", module_name1 " & _
                       ", module_name2 " & _
                       ", module_name3 " & _
                       ", module_name4 " & _
                       ", module_name5 " & _
                       ", isweb " & _
                       ", loginemployeeunkid " & _
                       ", isapproved " & _
                       ", approveruserunkid " & _
                       ", psoft_syncdatetime " & _
                       ", rehiretranunkid " & _
                       ", changetypeid " & _
                       ", promotion_date" & _
                   ") VALUES (" & _
                       "  @empsalaryhistorytranunkid " & _
                       ", @salaryincrementtranunkid " & _
                       ", @periodunkid " & _
                       ", @employeeunkid " & _
                       ", @incrementdate " & _
                       ", @currentscale " & _
                       ", @increment " & _
                       ", @newscale " & _
                       ", @gradegroupunkid " & _
                       ", @gradeunkid " & _
                       ", @gradelevelunkid " & _
                       ", @isgradechange " & _
                       ", @isfromemployee " & _
                       ", @audittype " & _
                       ", @audituserunkid " & _
                       ", @auditdatetime " & _
                       ", @ip " & _
                       ", @machine_name " & _
                       ", @reason_id " & _
                       ", @increment_mode " & _
                       ", @percentage " & _
                       ", @form_name " & _
                       ", @module_name1 " & _
                       ", @module_name2 " & _
                       ", @module_name3 " & _
                       ", @module_name4 " & _
                       ", @module_name5 " & _
                       ", @isweb " & _
                       ", @loginemployeeunkid " & _
                       ", @isapproved " & _
                       ", @approveruserunkid " & _
                       ", @psoft_syncdatetime " & _
                       ", @rehiretranunkid " & _
                       ", @changetypeid " & _
                       ", @promotion_date" & _
                   "); SELECT @@identity"

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@empsalaryhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmpsalaryhistorytranunkid.ToString)
            objDataOperation.AddParameter("@salaryincrementtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSalaryincrementtranunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)

            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-933,934
            If mdtIncrementdate <> Nothing Then
            objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtIncrementdate)
            Else
                objDataOperation.AddParameter("@incrementdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'S.SANDEEP |05-JUN-2023| -- END

            objDataOperation.AddParameter("@currentscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinCurrentscale.ToString)
            objDataOperation.AddParameter("@increment", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinIncrement.ToString)
            objDataOperation.AddParameter("@newscale", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, msinNewscale.ToString)
            objDataOperation.AddParameter("@gradegroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradegroupunkid.ToString)
            objDataOperation.AddParameter("@gradeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradeunkid.ToString)
            objDataOperation.AddParameter("@gradelevelunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGradelevelunkid.ToString)
            objDataOperation.AddParameter("@isgradechange", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsgradechange.ToString)
            objDataOperation.AddParameter("@isfromemployee", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfromemployee.ToString)
            objDataOperation.AddParameter("@reason_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReason_Id.ToString)
            objDataOperation.AddParameter("@increment_mode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintIncrement_Mode.ToString)
            objDataOperation.AddParameter("@percentage", SqlDbType.Money, eZeeDataType.MONEY_SIZE, mdblPercentage.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid.ToString)
            objDataOperation.AddParameter("@isapproved", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsapproved.ToString)
            objDataOperation.AddParameter("@approveruserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproveruserunkid.ToString)
            If mdtPsoft_Syncdatetime <> Nothing Then
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPsoft_Syncdatetime)
            Else
                objDataOperation.AddParameter("@psoft_syncdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@rehiretranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintRehiretranunkid.ToString)
            objDataOperation.AddParameter("@changetypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintChangetypeid.ToString)
            If mdtPromotion_Date <> Nothing Then
                objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPromotion_Date)
            Else
                objDataOperation.AddParameter("@promotion_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebIP)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebhostName)
            If mstrWebFormName.Trim.Length <= 0 Then
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 2, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrailForSalaryHistory; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry! Salary History for the employee on the selected date is already done.")
            Language.setMessage(mstrModuleName, 2, "WEB")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
