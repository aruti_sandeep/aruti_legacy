﻿'************************************************************************************************************************************
'Class Name : clsTraining_Priority_Master
'Purpose    :
'Date       : 01-Feb-2021
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsTraining_Priority_Master
    Private Shared ReadOnly mstrModuleName As String = "clsTraining_Priority_Master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region "Enum"

    Public Enum enPriorityDefaultTypeId
        Custom = 0
        Low = 1
        Medium = 2
        High = 3
        Very_High = 4
        Highest = 5
    End Enum

#End Region

#Region " Private variables "
    Private mintPriorityunkid As Integer
    Private mstrPriorityName As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mintPriority As Integer
    Private mintDefautlTypeId As Integer
    Private minAuditUserid As Integer
    Private minAuditDate As DateTime
    Private mstrClientIp As String
    Private mstrHostName As String
    Private mstrFormName As String
    Private blnIsFromWeb As Boolean

    Dim objDoOpseration As clsDataOperation
#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priorityunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Priorityunkid(Optional ByVal objDoOpseration As clsDataOperation = Nothing) As Integer
        Get
            Return mintPriorityunkid
        End Get
        Set(ByVal value As Integer)
            mintPriorityunkid = value
            Call GetData(objDoOpseration)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority_name
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _PriorityName() As String
        Get
            Return mstrPriorityName
        End Get
        Set(ByVal value As String)
            mstrPriorityName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set priority
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Priority() As Integer
        Get
            Return mintPriority
        End Get
        Set(ByVal value As Integer)
            mintPriority = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DefaultTypeId
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _DefaultTypeId() As Integer
        Get
            Return mintDefautlTypeId
        End Get
        Set(ByVal value As Integer)
            mintDefautlTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    Public Property _AuditUserid() As Integer
        Get
            Return minAuditUserid
        End Get
        Set(ByVal value As Integer)
            minAuditUserid = value
        End Set
    End Property

    Public Property _AuditDate() As DateTime
        Get
            Return minAuditDate
        End Get
        Set(ByVal value As DateTime)
            minAuditDate = value
        End Set
    End Property

    Public Property _ClientIp() As String
        Get
            Return mstrClientIp
        End Get
        Set(ByVal value As String)
            mstrClientIp = value
        End Set
    End Property

    Public Property _HostName() As String
        Get
            Return mstrHostName
        End Get
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _IsFromWeb() As Boolean
        Get
            Return blnIsFromWeb
        End Get
        Set(ByVal value As Boolean)
            blnIsFromWeb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOpseration As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOpseration Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOpseration
        End If
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT " & _
              "  trpriorityunkid " & _
              ", trpriority_name " & _
              ", trpriority " & _
                      ", trdefaultypeid " & _
              ", isactive " & _
                     " FROM trtrainingpriority_master " & _
                     " WHERE trpriorityunkid = @trpriorityunkid "

            objDataOperation.AddParameter("@trpriorityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriorityunkid.ToString())

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintPriorityunkid = CInt(dtRow.Item("trpriorityunkid"))
                mstrPriorityName = dtRow.Item("trpriority_name").ToString
                mintPriority = CInt(dtRow.Item("trpriority"))
                mintDefautlTypeId = CInt(dtRow.Item("trdefaultypeid"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOpseration Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True, Optional ByVal mblnblank As Boolean = False, Optional ByVal strfilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
                strQ = "SELECT " & _
             "  trpriorityunkid " & _
             ", trpriority_name " & _
             ", trpriority " & _
                     ", trdefaultypeid" & _
             ", isactive " & _
             " FROM trtrainingpriority_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            If strfilter <> "" Then
                strQ &= "And " & strfilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trtrainingpriority_master) </purpose>
    Public Function Insert() As Boolean

        If isExist(mstrPriorityName, -1, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Priority Name is already defined.Please define new Priority Name.")
            Return False
        ElseIf isExist("", mintPriority, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Priority is already assigned.Please assign new Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try

            strQ = "INSERT INTO trtrainingpriority_master ( " & _
              " trpriority_name " & _
              ", trpriority " & _
                      ", trdefaultypeid" & _
              ", isactive " & _
            ") VALUES ( " & _
              " @trpriority_name " & _
              ", @trpriority " & _
                      ", @trdefaultypeid" & _
              ", @isactive " & _
            "); SELECT @@identity"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trpriority_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPriorityName.ToString)
            objDataOperation.AddParameter("@trpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@trdefaultypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDefautlTypeId.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPriorityunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintPriorityunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trtrainingpriority_master) </purpose>
    Public Function Update() As Boolean

        If isExist(mstrPriorityName, -1, mintPriorityunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Priority Name is already defined.Please define new Priority Name.")
            Return False
        ElseIf isExist("", mintPriority, mintPriorityunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "This Priority is already assigned.Please assign new Priority.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trpriority_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPriorityName.ToString)
            objDataOperation.AddParameter("@trpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority.ToString)
            objDataOperation.AddParameter("@trdefaultypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDefautlTypeId.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive)
            objDataOperation.AddParameter("@trpriorityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriorityunkid)

            strQ = "UPDATE trtrainingpriority_master SET " & _
              " trpriority_name = @trpriority_name " & _
              ", trpriority  = @trpriority  " & _
                      ", trdefaultypeid = @trdefaultypeid " & _
              ", isactive = @isactive " & _
            " WHERE trpriorityunkid = @trpriorityunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintPriorityunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trtrainingpriority_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Priority. Reason : This Priority is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "Update trtrainingpriority_master set isactive = 0 WHERE trpriorityunkid = @trpriorityunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@trpriorityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            _Priorityunkid(objDataOperation) = intUnkid

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, mintPriorityunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strq As String = ""
        Dim exforce As Exception
        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()
        Try
            strq = "select isnull(trainingpriority,0) FROM trdepartmentaltrainingneed_master WHERE trainingpriority = @trpriorityunkid AND isvoid = 0 "
            objDataOperation.AddParameter("@trpriorityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            If CInt(objDataOperation.RecordCount(strq)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exforce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exforce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exforce = Nothing
            objDataOperation = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strName As String = "", Optional ByVal intPriority As Integer = -1, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  trpriorityunkid " & _
              ", trpriority_name " & _
              ", trpriority " & _
              ", trdefaultypeid " & _
              ", isactive " & _
             "FROM trtrainingpriority_master " & _
             "WHERE isactive = 1 "

            If strName.Length > 0 Then
                strQ &= " AND trpriority_name = @name "
            End If
            If intPriority > -1 Then
                strQ &= " AND trpriority = @trpriority "
            End If

            If intUnkid > 0 Then
                strQ &= " AND trpriorityunkid <> @trpriorityunkid"
            End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@trpriorityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@trpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, intPriority)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False, Optional ByVal xDataOper As clsDataOperation = Nothing) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation
        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If

        Dim strQ As String = String.Empty
        Dim exForce As Exception
        objDataOperation.ClearParameters()
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as trpriorityunkid, ' ' +  @name as name, 0 AS trpriority, @name AS priority   UNION "
            End If
            strQ &= " SELECT trpriorityunkid, trpriority_name as name, trtrainingpriority_master.trpriority, trpriority_name + ' - ' + CAST(trpriority as NVarchar(10)) AS priority  " & _
                        " FROM trtrainingpriority_master " & _
                        " WHERE isactive = 1  ORDER BY trpriority"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If xDataOper Is Nothing Then objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrails(ByVal objDoOps As clsDataOperation, ByVal AuditType As Integer, ByVal intUnkid As Integer)
        Dim StrQ As String = String.Empty
        Dim extForce As Exception
        Dim dsList As New DataSet

        Try
            StrQ = "INSERT INTO attrtrainingpriority_master ( " & _
             "  tranguid " & _
             ", trpriorityunkid " & _
             ", trpriority_name " & _
             ", trpriority " & _
                         ", trdefaultypeid " & _
             ", auditdatetime " & _
             ", audittype " & _
             ", audituserunkid " & _
             ", ip " & _
             ", host " & _
             ", form_name " & _
             ", isweb " & _
           ") VALUES (" & _
             "  LOWER(NEWID()) " & _
             ", @trpriorityunkid " & _
             ", @trpriority_name " & _
             ", @trpriority " & _
                         ", @trdefaultypeid " & _
             ", GETDATE() " & _
             ", @audittype " & _
             ", @audituserunkid " & _
             ", @ip " & _
                         ", @machine_name " & _
             ", @form_name " & _
             ", @isweb )"

            objDoOps.ClearParameters()
            objDoOps.AddParameter("@trpriorityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriorityunkid)
            objDoOps.AddParameter("@trpriority_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrPriorityName)
            objDoOps.AddParameter("@trpriority", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPriority)
            objDoOps.AddParameter("@trdefaultypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDefautlTypeId)
            objDoOps.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType)
            objDoOps.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, minAuditUserid)

            If mstrClientIP.Trim.Length > 0 Then
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            Else
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            End If

            If mstrHostName.Length > 0 Then
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            Else
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName())
            End If

            objDoOps.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDoOps.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsFromWeb)

            dsList = objDoOps.ExecQuery(StrQ, "List")

            If objDoOps.ErrorMessage <> "" Then
                extForce = New Exception(objDoOps.ErrorNumber & ": " & objDoOps.ErrorMessage)
                Throw extForce
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

    



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "This Priority Name is already defined.Please define new Priority Name.")
			Language.setMessage(mstrModuleName, 3, "This Priority is already assigned.Please assign new Priority.")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this Priority. Reason : This Priority is already linked with some transaction.")
			
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
