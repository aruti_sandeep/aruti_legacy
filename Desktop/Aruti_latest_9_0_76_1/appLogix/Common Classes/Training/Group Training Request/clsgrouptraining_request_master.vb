﻿'************************************************************************************************************************************
'Class Name : clsgrouptraining_request_master.vb
'Purpose    :
'Date       :06-Aug-2024
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System

''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsgrouptraining_request_master
    Private Const mstrModuleName = "clsgrouptraining_request_master"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "

    Private mintGroupTrainingRequestunkid As Integer
    Private mdtApplication_Date As Date
    Private mintCourseMasterunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintDepartmentaltrainingneedunkid As Integer
    Private mdtStart_Date As Date
    Private mdtEnd_Date As Date
    Private mdecTotalTrainingCost As Decimal
    Private mintTrainingproviderunkid As Integer
    Private mintTrainingvenueunkid As Integer
    Private mstrVenue As String
    Private mblnIsTravelling As Boolean
    Private mintTrainingTypeId As Integer
    Private mstrExpectedReturn As String = String.Empty
    Private mstrRemarks As String = String.Empty
    Private mstrFinancingSourcesTranUnkids As String = String.Empty
    Private mintTargetedgroupunkid As Integer
    Private mstrAllocationtranunkids As String = String.Empty
    Private mblnIsSubmitApproval As Boolean

    Private mintUserunkid As Integer
    Private mintLoginEmployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidLoginEmployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""

    Private mdtTrainingCostItem As DataTable = Nothing

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set grouptrainingrequestunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _GroupTrainingRequestunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        Get
            Return mintGroupTrainingRequestunkid
        End Get
        Set(ByVal value As Integer)
            mintGroupTrainingRequestunkid = value
            Call GetData(objDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set application_date
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Application_Date() As Date
        Get
            Return mdtApplication_Date
        End Get
        Set(ByVal value As Date)
            mdtApplication_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set coursemasterunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Coursemasterunkid() As Integer
        Get
            Return mintCourseMasterunkid
        End Get
        Set(ByVal value As Integer)
            mintCourseMasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set departmentaltrainingneedunkid 
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _DepartmentalTrainingNeedunkid() As Integer
        Get
            Return mintDepartmentaltrainingneedunkid
        End Get
        Set(ByVal value As Integer)
            mintDepartmentaltrainingneedunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set start_date
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Start_Date() As Date
        Get
            Return mdtStart_Date
        End Get
        Set(ByVal value As Date)
            mdtStart_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set end_date
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _End_Date() As Date
        Get
            Return mdtEnd_Date
        End Get
        Set(ByVal value As Date)
            mdtEnd_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set totaltrainingcost
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TotalTrainingCost() As Decimal
        Get
            Return mdecTotalTrainingCost
        End Get
        Set(ByVal value As Decimal)
            mdecTotalTrainingCost = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingproviderunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Trainingproviderunkid() As Integer
        Get
            Return mintTrainingproviderunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingproviderunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingvenueunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Trainingvenueunkid() As Integer
        Get
            Return mintTrainingvenueunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingvenueunkid = value
        End Set
    End Property

    Public Property _Venue() As String
        Get
            Return mstrVenue
        End Get
        Set(ByVal value As String)
            mstrVenue = value
        End Set
    End Property

    Public Property _IsTravelling() As Boolean
        Get
            Return mblnIsTravelling
        End Get
        Set(ByVal value As Boolean)
            mblnIsTravelling = value
        End Set
    End Property

    Public Property _TrainingTypeId() As Integer
        Get
            Return mintTrainingTypeId
        End Get
        Set(ByVal value As Integer)
            mintTrainingTypeId = value
        End Set
    End Property

    Public Property _ExpectedReturn() As String
        Get
            Return mstrExpectedReturn
        End Get
        Set(ByVal value As String)
            mstrExpectedReturn = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remarks
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Remarks() As String
        Get
            Return mstrRemarks
        End Get
        Set(ByVal value As String)
            mstrRemarks = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set financingsourcestranunkids
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FinancingSourcesTranUnkids() As String
        Get
            Return mstrFinancingSourcesTranUnkids
        End Get
        Set(ByVal value As String)
            mstrFinancingSourcesTranUnkids = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set targetedgroupunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Targetedgroupunkid() As Integer
        Get
            Return mintTargetedgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintTargetedgroupunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set allocationtranunkids
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Allocationtranunkids() As String
        Get
            Return mstrAllocationtranunkids
        End Get
        Set(ByVal value As String)
            mstrAllocationtranunkids = value
        End Set
    End Property


    Public Property _IsSubmitApproval() As Boolean
        Get
            Return mblnIsSubmitApproval
        End Get
        Set(ByVal value As Boolean)
            mblnIsSubmitApproval = value
        End Set
    End Property


    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VoidLoginEmployeeunkid() As Integer
        Get
            Return mintVoidLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public Property _Datasource_TrainingCostItem() As DataTable
        Get
            Return mdtTrainingCostItem
        End Get
        Set(ByVal value As DataTable)
            mdtTrainingCostItem = value
        End Set
    End Property

    Private mlstGroupTEmpNew As List(Of clsgrouptraining_request_employee_tran)
    Public WriteOnly Property _lstGroupTEmpNew() As List(Of clsgrouptraining_request_employee_tran)
        Set(ByVal value As List(Of clsgrouptraining_request_employee_tran))
            mlstGroupTEmpNew = value
        End Set
    End Property

    Private mlstGroupTEmpVoid As List(Of clsgrouptraining_request_employee_tran)
    Public WriteOnly Property _lstGroupTEmpVoid() As List(Of clsgrouptraining_request_employee_tran)
        Set(ByVal value As List(Of clsgrouptraining_request_employee_tran))
            mlstGroupTEmpVoid = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()


        Try
            strQ = "SELECT " & _
                       " grouptrainingrequestunkid " & _
                       ",application_date " & _
                       ",coursemasterunkid " & _
                       ",periodunkid " & _
                       ",departmentaltrainingneedunkid " & _
                       ",start_date " & _
                       ",end_date " & _
                       ",totaltrainingcost " & _
                       ",trainingproviderunkid " & _
                       ",trainingvenueunkid " & _
                       ",ISNULL(venue, '') AS venue " & _
                       ",istravelling " & _
                       ",trainingtypeid " & _
                       ",expectedreturn " & _
                       ",remarks " & _
                       ",ISNULL(financingsourcestranunkids, '') AS financingsourcestranunkids " & _
                       ",targetedgroupunkid " & _
                       ",allocationtranunkids " & _
                       ",issubmit_approval " & _
                       ",userunkid " & _
                       ",loginemployeeunkid " & _
                       ",isvoid " & _
                       ",voiduserunkid " & _
                       ",voidloginemployeeunkid " & _
                       ",voiddatetime " & _
                       ",voidreason " & _
             "FROM trgrouptraining_request_master " & _
             "WHERE grouptrainingrequestunkid = @grouptrainingrequestunkid "
          
            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintGroupTrainingRequestunkid = CInt(dtRow.Item("grouptrainingrequestunkid"))
                mdtApplication_Date = CDate(dtRow.Item("application_date"))
                mintCourseMasterunkid = CInt(dtRow.Item("coursemasterunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintDepartmentaltrainingneedunkid = CInt(dtRow.Item("departmentaltrainingneedunkid"))
                mdtStart_Date = CDate(dtRow.Item("start_date"))
                mdtEnd_Date = CDate(dtRow.Item("end_date"))
                mdecTotalTrainingCost = CDec(dtRow.Item("totaltrainingcost"))
                mintTrainingproviderunkid = CInt(dtRow.Item("trainingproviderunkid"))
                mintTrainingvenueunkid = CInt(dtRow.Item("trainingvenueunkid"))
                mstrVenue = dtRow.Item("venue")
                mblnIsTravelling = CBool(dtRow.Item("istravelling"))
                mintTrainingTypeId = CInt(dtRow.Item("trainingtypeid"))
                mstrExpectedReturn = dtRow.Item("expectedreturn")
                mstrRemarks = dtRow.Item("remarks")
                mstrFinancingSourcesTranUnkids = dtRow.Item("financingsourcestranunkids")
                mintTargetedgroupunkid = CInt(dtRow.Item("targetedgroupunkid"))
                mstrAllocationtranunkids = CStr(dtRow.Item("allocationtranunkids"))
                mblnIsSubmitApproval = CBool(dtRow.Item("issubmit_approval"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginEmployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintVoidLoginEmployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>    ''' 
    Public Function GetList(ByVal strTableName As String _
                            , Optional ByVal strFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT " & _
                      " grouptrainingrequestunkid " & _
                      ",application_date " & _
                      ",coursemasterunkid " & _
                      ", ISNULL(cfcommon_master.name, '') AS Training " & _
                      ",periodunkid " & _
                      ",departmentaltrainingneedunkid " & _
                      ",start_date " & _
                      ",end_date " & _
                      ", CONVERT(NVARCHAR(8),start_date,112) AS SDate " & _
                      ", CONVERT(NVARCHAR(8),end_date,112) AS EDate " & _
                      ", totaltrainingcost AS TotalTrainingCost " & _
                      ",trainingproviderunkid " & _
                      ",trainingvenueunkid " & _
                      ",ISNULL(venue, '') AS venue " & _
                      ",istravelling " & _
                      ",trainingtypeid " & _
                      ",expectedreturn " & _
                      ",remarks " & _
                      ",ISNULL(financingsourcestranunkids, '') AS financingsourcestranunkids " & _
                      ",targetedgroupunkid " & _
                      ",ISNULL(allocationtranunkids, '') AS allocationtranunkids " & _
                      ",issubmit_approval " & _
                      ",userunkid " & _
                      ",loginemployeeunkid " & _
                      ",isvoid " & _
                      ",voiduserunkid " & _
                      ",voidloginemployeeunkid " & _
                      ",voiddatetime " & _
                      ",voidreason " & _
            "FROM trgrouptraining_request_master " & _
            " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = trgrouptraining_request_master.coursemasterunkid " & _
            " WHERE ISNULL(trgrouptraining_request_master.isvoid,0) = 0 "

            objDataOperation.ClearParameters()

            If strFilter.Trim.Length > 0 Then
                strQ &= "AND " & strFilter
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trgrouptraining_request_master) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing, Optional ByRef intNewUnkId As Integer = 0) As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseMasterunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@totaltrainingcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalTrainingCost.ToString)

            objDataOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDataOperation.AddParameter("@trainingvenueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingvenueunkid.ToString)
            objDataOperation.AddParameter("@venue", SqlDbType.NVarChar, mstrVenue.Trim.Length, mstrVenue.ToString)
            objDataOperation.AddParameter("@istravelling", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsTravelling.ToString)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeId.ToString)
            objDataOperation.AddParameter("@expectedreturn", SqlDbType.NVarChar, mstrExpectedReturn.Trim.Length, mstrExpectedReturn.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrRemarks.ToString)
            objDataOperation.AddParameter("@financingsourcestranunkids", SqlDbType.NVarChar, mstrFinancingSourcesTranUnkids.Trim.Length, mstrFinancingSourcesTranUnkids.ToString)
            objDataOperation.AddParameter("@targetedgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTargetedgroupunkid.ToString)
            objDataOperation.AddParameter("@allocationtranunkids", SqlDbType.NVarChar, mstrAllocationtranunkids.Trim.Length, mstrAllocationtranunkids.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitApproval.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)
            
           
            strQ = "INSERT INTO trgrouptraining_request_master ( " & _
                       "  application_date " & _
                       ", coursemasterunkid " & _
                       ", periodunkid " & _
                       ", departmentaltrainingneedunkid " & _
                       ", start_date " & _
                       ", end_date " & _
                       ", totaltrainingcost " & _
                       ", trainingproviderunkid " & _
                       ", trainingvenueunkid " & _
                       ", venue " & _
                       ", istravelling " & _
                       ", trainingtypeid " & _
                       ", expectedreturn " & _
                       ", remarks " & _
                       ", financingsourcestranunkids " & _
                       ", targetedgroupunkid " & _
                       ", allocationtranunkids " & _
                       ", issubmit_approval " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                    ") VALUES (" & _
                       "  @application_date " & _
                       ", @coursemasterunkid " & _
                       ", @periodunkid " & _
                       ", @departmentaltrainingneedunkid " & _
                       ", @start_date " & _
                       ", @end_date " & _
                       ", @totaltrainingcost " & _
                       ", @trainingproviderunkid " & _
                       ", @trainingvenueunkid " & _
                       ", @venue " & _
                       ", @istravelling " & _
                       ", @trainingtypeid " & _
                       ", @expectedreturn " & _
                       ", @remarks " & _
                       ", @financingsourcestranunkids " & _
                       ", @targetedgroupunkid " & _
                       ", @allocationtranunkids " & _
                       ", @issubmit_approval " & _
                       ", @userunkid " & _
                       ", @loginemployeeunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidloginemployeeunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                    "); SELECT @@identity"
           
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintGroupTrainingRequestunkid = dsList.Tables(0).Rows(0).Item(0)

            intNewUnkId = mintGroupTrainingRequestunkid

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trgrouptraining_request_master) </purpose>
    Public Function Update(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@application_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtApplication_Date.ToString)
            objDataOperation.AddParameter("@coursemasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCourseMasterunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@departmentaltrainingneedunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDepartmentaltrainingneedunkid.ToString)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStart_Date.ToString)
            objDataOperation.AddParameter("@end_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnd_Date.ToString)
            objDataOperation.AddParameter("@totaltrainingcost", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecTotalTrainingCost.ToString)
            objDataOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDataOperation.AddParameter("@trainingvenueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingvenueunkid.ToString)
            objDataOperation.AddParameter("@venue", SqlDbType.NVarChar, mstrVenue.Trim.Length, mstrVenue.ToString)
            objDataOperation.AddParameter("@istravelling", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsTravelling.ToString)
            objDataOperation.AddParameter("@trainingtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingTypeId.ToString)
            objDataOperation.AddParameter("@expectedreturn", SqlDbType.NVarChar, mstrExpectedReturn.Trim.Length, mstrExpectedReturn.ToString)
            objDataOperation.AddParameter("@remarks", SqlDbType.NVarChar, mstrRemarks.Trim.Length, mstrRemarks.ToString)
            objDataOperation.AddParameter("@financingsourcestranunkids", SqlDbType.NVarChar, mstrFinancingSourcesTranUnkids.Trim.Length, mstrFinancingSourcesTranUnkids.ToString)
            objDataOperation.AddParameter("@targetedgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTargetedgroupunkid.ToString)
            objDataOperation.AddParameter("@allocationtranunkids", SqlDbType.NVarChar, mstrAllocationtranunkids.Trim.Length, mstrAllocationtranunkids.ToString)
            objDataOperation.AddParameter("@issubmit_approval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsSubmitApproval.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)


            strQ = "UPDATE trgrouptraining_request_master SET " & _
                        "  application_date = @application_date " & _
                        ", coursemasterunkid = @coursemasterunkid " & _
                        ", periodunkid = @periodunkid " & _
                        ", departmentaltrainingneedunkid = @departmentaltrainingneedunkid " & _
                        ", start_date = @start_date " & _
                        ", end_date = @end_date " & _
                        ", totaltrainingcost = @totaltrainingcost " & _
                        ", trainingproviderunkid = @trainingproviderunkid" & _
                        ", trainingvenueunkid = @trainingvenueunkid " & _
                        ", venue = @venue " & _
                        ", istravelling = @istravelling " & _
                        ", trainingtypeid = @trainingtypeid " & _
                        ", expectedreturn = @expectedreturn " & _
                        ", remarks = @remarks " & _
                        ", financingsourcestranunkids = @financingsourcestranunkids " & _
                        ", targetedgroupunkid = @targetedgroupunkid " & _
                        ", allocationtranunkids = @allocationtranunkids " & _
                        ", issubmit_approval = @issubmit_approval " & _
                        ", userunkid = @userunkid " & _
                        ", loginemployeeunkid = @loginemployeeunkid " & _
                        ", isvoid = @isvoid " & _
                        ", voiduserunkid = @voiduserunkid " & _
                        ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                        ", voiddatetime = @voiddatetime " & _
                        ", voidreason = @voidreason " & _
                    "WHERE grouptrainingrequestunkid = @grouptrainingrequestunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Save(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Dim objCostTran As New clsgrouptraining_request_cost_tran

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            If mintGroupTrainingRequestunkid > 0 Then
                If Update(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Else
                If Insert(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mdtTrainingCostItem IsNot Nothing AndAlso mdtTrainingCostItem.Rows.Count > 0 Then
                With objCostTran
                    ._GroupTrainingRequestunkid = mintGroupTrainingRequestunkid
                    ._TranDataTable = mdtTrainingCostItem
                    ._IsWeb = mblnIsWeb
                    ._Userunkid = mintUserunkid
                    ._LoginEmployeeunkid = mintLoginEmployeeunkid
                    ._ClientIP = mstrClientIP
                    ._FormName = mstrFormName
                    ._HostName = mstrHostName
                End With

                If objCostTran.Insert_Update_Delete(objDataOperation) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If mlstGroupTEmpVoid IsNot Nothing AndAlso mlstGroupTEmpVoid.Count > 0 Then
                Dim objTEmp As New clsgrouptraining_request_employee_tran
                If objTEmp.VoidAll(mlstGroupTEmpVoid, objDataOperation) = False Then
                    Return False
                End If
            End If

            If mlstGroupTEmpNew IsNot Nothing AndAlso mlstGroupTEmpNew.Count > 0 Then
                Dim objTEmp As New clsgrouptraining_request_employee_tran
                objTEmp._GroupTrainingRequestunkid = mintGroupTrainingRequestunkid
                If objTEmp.SaveAll(mlstGroupTEmpNew, , objDataOperation) = False Then
                    Return False
                End If
            End If


            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Save; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
            objCostTran = Nothing
        End Try
    End Function

    Public Function InsertAllByEmployeeList(ByVal xDatabaseName As String _
                                           , ByVal xUserUnkid As Integer _
                                           , ByVal xYearUnkid As Integer _
                                           , ByVal xCompanyUnkid As Integer _
                                           , ByVal strUserAccessMode As String _
                                           , ByVal xEmployeeAsOnDate As String _
                                           , ByVal strEmployeeList As String _
                                           , ByVal xTrainingApproverAllocationID As Integer _
                                           , ByVal xTrainingTypeId As Integer _
                                           , Optional ByVal xDataOp As clsDataOperation = Nothing _
                                           , Optional ByVal mdtAttachmentTable As DataTable = Nothing _
                                           , Optional ByVal objTrainingRequest As clstraining_request_master = Nothing _
                                           ) As Boolean
        Dim exForce As Exception
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        Try
            If Save(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If mblnIsSubmitApproval = False Then
                If mdtAttachmentTable IsNot Nothing AndAlso mintGroupTrainingRequestunkid > 0 Then
                    Dim objDocument As New clsScan_Attach_Documents
                    Dim dtTran As DataTable = objDocument._Datatable
                    Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\"

                    Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.GROUP_TRAINING_REQUEST).Tables(0).Rows(0)("Name").ToString

                    If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
                    Dim dr As DataRow
                    For Each drow As DataRow In mdtAttachmentTable.Rows
                        dr = dtTran.NewRow
                        dr("scanattachtranunkid") = drow("scanattachtranunkid")
                        dr("documentunkid") = drow("documentunkid")
                        dr("employeeunkid") = drow("employeeunkid")
                        dr("filename") = drow("filename")
                        dr("scanattachrefid") = drow("scanattachrefid")
                        dr("modulerefid") = drow("modulerefid")
                        dr("form_name") = drow("form_name")
                        dr("userunkid") = drow("userunkid")
                        dr("transactionunkid") = mintGroupTrainingRequestunkid
                        dr("attached_date") = drow("attached_date")
                        dr("orgfilepath") = drow("localpath")
                        dr("destfilepath") = docLocalPath & strFolderName & CStr(drow.Item("filename"))
                        dr("AUD") = drow("AUD")
                        dr("userunkid") = mintUserunkid
                        dr("fileuniquename") = drow("fileuniquename")
                        dr("filepath") = drow("filepath")
                        dr("filesize") = drow("filesize_kb")
                        dr("file_data") = drow("file_data")

                        dtTran.Rows.Add(dr)
                    Next
                    objDocument._Datatable = dtTran
                    objDocument.InsertUpdateDelete_Documents(objDataOperation)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            If mblnIsSubmitApproval = True Then
                objTrainingRequest._GroupTrainingRequestunkid = mintGroupTrainingRequestunkid
                If objTrainingRequest.InsertAllByEmployeeList(xDatabaseName, xUserUnkid, xYearUnkid, xCompanyUnkid _
                                                            , strUserAccessMode, xEmployeeAsOnDate, strEmployeeList _
                                                            , xTrainingApproverAllocationID, xTrainingTypeId, _
                                                            objDataOperation, mdtAttachmentTable) = False Then

                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAllByEmployeeList; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trgrouptraining_request_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objCostTran As New clsgrouptraining_request_cost_tran

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            With objCostTran
                ._Isvoid = mblnIsvoid
                ._Voiduserunkid = mintVoiduserunkid
                ._VoidLoginEmployeeunkid = mintVoidLoginEmployeeunkid
                ._Voiddatetime = mdtVoiddatetime
                ._Voidreason = mstrVoidreason
                ._IsWeb = mblnIsWeb
                ._Userunkid = mintUserunkid
                ._LoginEmployeeunkid = mintLoginEmployeeunkid
                ._ClientIP = mstrClientIP
                ._FormName = mstrFormName
                ._HostName = mstrHostName
                .DeleteByTrainingRequestUnkid(intUnkid, objDataOperation)
            End With

            Dim objGTEmp As New clsgrouptraining_request_employee_tran
            If (objDataOperation.RecordCount("SELECT 1 FROM trgrouptraining_request_employee_tran WHERE isvoid = 0 AND grouptrainingrequestunkid = " & intUnkid & " ")) > 0 Then

                With objGTEmp
                    ._Isvoid = True
                    ._Voiddatetime = mdtVoiddatetime
                    ._Voidreason = mstrVoidreason

                    ._Voiduserunkid = mintVoiduserunkid
                    ._Voidloginemployeeunkid = mintVoidLoginEmployeeunkid
                    ._Isweb = mblnIsWeb
                    ._AuditUserId = mintAuditUserId
                    ._AuditDate = mdtVoiddatetime
                    ._ClientIP = mstrClientIP
                    ._HostName = mstrHostName
                    ._FormName = mstrFormName

                    If .VoidByMasterUnkID(intUnkid, 3, objDataOperation) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End With

            End If



            strQ = "UPDATE trgrouptraining_request_master SET " & _
                    " isvoid = @isvoid" & _
                    ", voiduserunkid = @voiduserunkid" & _
                    ", voiddatetime = @voiddatetime" & _
                    ", voidreason = @voidreason " & _
                   "WHERE grouptrainingrequestunkid = @grouptrainingrequestunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)


            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

End Class
