﻿'************************************************************************************************************************************
'Class Name : clsgrouptraining_request_cost_tran.vb
'Purpose    :
'Date       : 06-Aug-2024
'Written By : Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsgrouptraining_request_cost_tran
    Private Shared ReadOnly mstrModuleName As String = "clsgrouptraining_request_cost_tran"
    Dim mstrMessage As String = ""
    Dim objDataOperation As clsDataOperation

#Region " Private variables "
    Private mintGroupTrainingRequestCostTranunkid As Integer
    Private mintGroupTrainingRequestunkid As Integer
    Private mintTrainingCostItemunkid As Integer
    Private mdecAmount As Decimal
    Private mintUserunkid As Integer
    Private mintLoginEmployeeunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidLoginEmployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mdtTran As DataTable

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingrequestcosttranunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _GroupTrainingRequestCostTranunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer
        Get
            Return mintGroupTrainingRequestCostTranunkid
        End Get
        Set(ByVal value As Integer)
            mintGroupTrainingRequestCostTranunkid = value
            Call GetData(objDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingrequestunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _GroupTrainingRequestunkid() As Integer
        Get
            Return mintGroupTrainingRequestunkid
        End Get
        Set(ByVal value As Integer)
            mintGroupTrainingRequestunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingcostitemunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TrainingCostItemunkid() As Integer
        Get
            Return mintTrainingCostItemunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingCostItemunkid = value
        End Set
    End Property

    Public Property _Amount() As Decimal
        Get
            Return mdecAmount
        End Get
        Set(ByVal value As Decimal)
            mdecAmount = value
        End Set
    End Property

    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    Public Property _LoginEmployeeunkid() As Integer
        Get
            Return mintLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VoidLoginEmployeeunkid() As Integer
        Get
            Return mintVoidLoginEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidLoginEmployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set TranDataTable
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TranDataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                       "  grouptrainingrequestcosttranunkid " & _
                       ", grouptrainingrequestunkid " & _
                       ", trainingcostitemunkid " & _
                       ", amount " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
             "FROM trgrouptraining_request_cost_tran " & _
             "WHERE grouptrainingrequestcosttranunkid = @grouptrainingrequestcosttranunkid "

            objDataOperation.AddParameter("@grouptrainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestCostTranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintGroupTrainingRequestCostTranunkid = CInt(dtRow.Item("grouptrainingrequestcosttranunkid"))
                mintGroupTrainingRequestunkid = CInt(dtRow.Item("grouptrainingrequestunkid"))
                mintTrainingCostItemunkid = CInt(dtRow.Item("trainingcostitemunkid"))
                mdecAmount = CDec(dtRow.Item("amount"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintLoginEmployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintVoidLoginEmployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>    ''' 
    Public Function GetList(ByVal strTableName As String, ByVal xItemTypeId As Integer, ByVal intGroupTrainingRequestId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        objDataOperation.ClearParameters()

        Try
            strQ &= " SELECT " & _
                       "  infounkid " & _
                       ", infotypeid " & _
                       ", info_code " & _
                       ", info_name " & _
                       ", description " & _
                       ", defaultitemtypeid " & _
                       ", isactive " & _
                       ", trgrouptraining_request_cost_tran.amount " & _
                       ", trgrouptraining_request_cost_tran.grouptrainingrequestcosttranunkid AS trainingrequestcosttranunkid " & _
                    " FROM trtrainingitemsinfo_master " & _
                    " LEFT JOIN trgrouptraining_request_cost_tran ON trgrouptraining_request_cost_tran.trainingcostitemunkid = trtrainingitemsinfo_master.infounkid " & _
                                " AND trgrouptraining_request_cost_tran.isvoid = 0  " & _
                    " WHERE isactive = 1 " & _
                    " AND infotypeid = @infotypeid " & _
                    " AND trgrouptraining_request_cost_tran.grouptrainingrequestunkid = @grouptrainingrequestunkid " & _
                    " UNION ALL " & _
                    " SELECT " & _
                       "  infounkid " & _
                       ", infotypeid " & _
                       ", info_code " & _
                       ", info_name " & _
                       ", description " & _
                       ", defaultitemtypeid " & _
                       ", isactive " & _
                       ", 0 AS amount " & _
                       ", -1 as trainingrequestcosttranunkid " & _
                    " FROM trtrainingitemsinfo_master " & _
                    " WHERE isactive = 1 " & _
                    " AND infotypeid = @infotypeid " & _
                    " AND infounkid NOT IN " & _
                            " ( " & _
                                " SELECT " & _
                                    " trainingcostitemunkid " & _
                                "FROM trgrouptraining_request_cost_tran " & _
                                "WHERE  trgrouptraining_request_cost_tran.isvoid = 0 AND grouptrainingrequestunkid = @grouptrainingrequestunkid " & _
                            " ) "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGroupTrainingRequestId.ToString)
            objDataOperation.AddParameter("@infotypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xItemTypeId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trgrouptraining_request_cost_tran) </purpose>
    Public Function Insert(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        If isExist(mintGroupTrainingRequestunkid, mintTrainingCostItemunkid, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Training is already defined. Please define new Training.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingCostItemunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)

            strQ = "INSERT INTO trgrouptraining_request_cost_tran ( " & _
                       "  grouptrainingrequestunkid " & _
                       ", trainingcostitemunkid " & _
                       ", amount " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", isvoid " & _
                       ", voiduserunkid " & _
                       ", voidloginemployeeunkid " & _
                       ", voiddatetime " & _
                       ", voidreason " & _
                    ") VALUES (" & _
                       "  @grouptrainingrequestunkid " & _
                       ", @trainingcostitemunkid " & _
                       ", @amount " & _
                       ", @userunkid " & _
                       ", @loginemployeeunkid " & _
                       ", @isvoid " & _
                       ", @voiduserunkid " & _
                       ", @voidloginemployeeunkid " & _
                       ", @voiddatetime " & _
                       ", @voidreason " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintGroupTrainingRequestCostTranunkid = dsList.Tables(0).Rows(0).Item(0)


            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trgrouptraining_request_cost_tran) </purpose>
    Public Function Update(ByRef blnChildTableChanged As Boolean, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        If isExist(mintGroupTrainingRequestunkid, mintTrainingCostItemunkid, mintGroupTrainingRequestCostTranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Training is already defined. Please define new Training.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@grouptrainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestCostTranunkid.ToString)
            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestunkid.ToString)
            objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingCostItemunkid.ToString)
            objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecAmount.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)

            strQ = "UPDATE trgrouptraining_request_cost_tran SET " & _
                        "grouptrainingrequestunkid = @grouptrainingrequestunkid " & _
                        "trainingcostitemunkid = @trainingcostitemunkid " & _
                        "amount = @amount " & _
                        "userunkid = @userunkid " & _
                        "loginemployeeunkid = @loginemployeeunkid " & _
                        "isvoid = @isvoid " & _
                        "voiduserunkid = @voiduserunkid " & _
                        "voidloginemployeeunkid = @voidloginemployeeunkid " & _
                        "voiddatetime = @voiddatetime " & _
                        "voidreason = @voidreason " & _
                    "WHERE grouptrainingrequestcosttranunkid = @grouptrainingrequestcosttranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iGroupTrainingRequestId As Integer, ByVal iTrainingCostItemId As String, Optional ByVal intUnkid As Integer = -1 _
                             , Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                       " grouptrainingrequestcosttranunkid " & _
                       ",grouptrainingrequestunkid " & _
                       ",trainingcostitemunkid " & _
                       ",amount " & _
                       ",userunkid " & _
                       ",loginemployeeunkid " & _
                       ",isvoid " & _
                       ",voiduserunkid " & _
                       ",voidloginemployeeunkid " & _
                       ",voiddatetime " & _
                       ", voidreason " & _
                    "FROM trgrouptraining_request_cost_tran " & _
                    "WHERE isvoid = 0 " & _
                    " AND trainingcostitemunkid = @trainingcostitemunkid " & _
                    " AND grouptrainingrequestunkid  = @grouptrainingrequestunkid "

            If intUnkid > 0 Then
                strQ &= " AND grouptrainingrequestcosttranunkid <> @grouptrainingrequestcosttranunkid"
            End If

            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iGroupTrainingRequestId)
            objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iTrainingCostItemId)
            objDataOperation.AddParameter("@grouptrainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Insert_Update_Delete(ByVal objDataOperation As clsDataOperation) As Boolean

        Dim i As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exFoece As Exception

        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = " INSERT INTO trgrouptraining_request_cost_tran (" & _
                                            "  grouptrainingrequestunkid " & _
                                            " ,trainingcostitemunkid " & _
                                            " ,amount " & _
                                            " ,userunkid " & _
                                            " ,loginemployeeunkid " & _
                                            " ,isvoid " & _
                                            " ,voiddatetime " & _
                                            " ,voiduserunkid " & _
                                            " ,voidloginemployeeunkid " & _
                                            " ,voidreason " & _
                                        ") VALUES (" & _
                                            "  @grouptrainingrequestunkid " & _
                                            " ,@trainingcostitemunkid " & _
                                            " ,@amount " & _
                                            " ,@userunkid " & _
                                            " ,@loginemployeeunkid " & _
                                            " ,@isvoid " & _
                                            " ,@voiddatetime " & _
                                            " ,@voiduserunkid " & _
                                            " ,@voidloginemployeeunkid " & _
                                            " ,@voidreason " & _
                                        "); SELECT @@identity "

                                objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestunkid)
                                objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingcostitemunkid"))
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If


                            Case "U"
                                strQ = "UPDATE trgrouptraining_request_cost_tran SET " & _
                                            "  grouptrainingrequestunkid = @grouptrainingrequestunkid " & _
                                            " ,trainingcostitemunkid = @trainingcostitemunkid " & _
                                            " ,amount = @amount " & _
                                            " ,userunkid = @userunkid " & _
                                            " ,loginemployeeunkid = @loginemployeeunkid  " & _
                                            " ,isvoid = @isvoid " & _
                                            " ,voiddatetime = @voiddatetime " & _
                                            " ,voiduserunkid = @voiduserunkid " & _
                                            " ,voidloginemployeeunkid = @voidloginemployeeunkid " & _
                                            " ,voidreason = @voidreason " & _
                                        "WHERE grouptrainingrequestcosttranunkid = @grouptrainingrequestcosttranunkid "

                                objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGroupTrainingRequestunkid)
                                objDataOperation.AddParameter("@trainingcostitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainingcostitemunkid"))
                                objDataOperation.AddParameter("@amount", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("amount"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginEmployeeunkid.ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voidloginemployeeunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@grouptrainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("grouptrainingrequestcosttranunkid"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If


                            Case "D"
                                strQ = "UPDATE trgrouptraining_request_cost_tran SET " & _
                                            " isvoid = @isvoid " & _
                                            " ,voiddatetime = @voiddatetime " & _
                                            " ,voiduserunkid = @voiduserunkid " & _
                                            " ,voidloginemployeeunkid = @voidloginemployeeunkid " & _
                                            " ,voidreason = @voidreason " & _
                                        "WHERE grouptrainingrequestcosttranunkid = @grouptrainingrequestcosttranunkid "

                                objDataOperation.ClearParameters()
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                                objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@grouptrainingrequestcosttranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("grouptrainingrequestcosttranunkid"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                        End Select
                    End If

                End With
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_Update_Delete , Module Name: " & mstrModuleName)
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function DeleteByTrainingRequestUnkid(ByVal intGroupTrainingRequestid As Integer, Optional ByVal objDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception


        If objDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = objDataOp
            objDataOperation.ClearParameters()
        End If

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidLoginEmployeeunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE trgrouptraining_request_cost_tran SET " & _
                       "  isvoid = @isvoid" & _
                       " ,voiduserunkid = @voiduserunkid" & _
                       " ,voidloginemployeeunkid = @voidloginemployeeunkid" & _
                       " ,voiddatetime = @voiddatetime" & _
                       " ,voidreason = @voidreason " & _
                   "WHERE isvoid = 0 AND grouptrainingrequestunkid = @grouptrainingrequestunkid "

            objDataOperation.AddParameter("@grouptrainingrequestunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intGroupTrainingRequestid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If objDataOp Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If objDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteByCalendarUnkid; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

End Class
