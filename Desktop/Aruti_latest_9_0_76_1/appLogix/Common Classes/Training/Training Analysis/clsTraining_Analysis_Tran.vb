﻿'************************************************************************************************************************************
'Class Name : clsTraining_Analysis_Tran.vb
'Purpose    :
'Date       :06/08/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsTraining_Analysis_Tran

#Region " Private Variables "
    Private Const mstrModuleName As String = "clsTraining_Analysis_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mintAnalysisUnkid As Integer = -1
    Private mdtTran As DataTable
#End Region

#Region " Properties "
    Public Property _AnalysisUnkid() As Integer
        Get
            Return mintAnalysisUnkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisUnkid = value
            Get_Analysis_Tran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
#End Region

#Region " Contructor "
    Public Sub New()
        mdtTran = New DataTable("Analysis_Tran")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("analysistranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("analysisunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("trainerstranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("resultunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("reviewer_remark")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            'Sandeep [ 02 Oct 2010 ] -- Start
            dCol = New DataColumn("analysis_date")
            dCol.DataType = System.Type.GetType("System.DateTime")
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)
            'Sandeep [ 02 Oct 2010 ] -- End 

            'S.SANDEEP [ 24 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES [<TRAINING>]
            dCol = New DataColumn("gpa_value")
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP [ 24 FEB 2012 ] -- END 


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub
#End Region

#Region " Private Medhods "
    Private Sub Get_Analysis_Tran()
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim dRow_Tran As DataRow
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation


            'Sandeep [ 02 Oct 2010 ] -- Start
            'strQ = "SELECT " & _
            '        "  analysistranunkid " & _
            '        ", analysisunkid " & _
            '        ", trainerstranunkid " & _
            '        ", resultunkid " & _
            '        ", reviewer_remark " & _
            '        ", isvoid " & _
            '        ", voiddatetime " & _
            '        ", voiduserunkid " & _
            '        ", voidreason " & _
            '        ", '' AS AUD " & _
            '       "FROM hrtraining_analysis_tran " & _
            '       "WHERE analysisunkid = @analysisunkid " & _
            '       "AND ISNULL(isvoid,0) = 0 "
            strQ = "SELECT " & _
                   "  analysistranunkid " & _
                   ", analysisunkid " & _
                   ", trainerstranunkid " & _
                   ", resultunkid " & _
                   ", reviewer_remark " & _
                   ", isvoid " & _
                   ", voiddatetime " & _
                   ", voiduserunkid " & _
                   ", voidreason " & _
                   ", '' AS AUD " & _
                   ",analysis_date " & _
                   ", gpa_value " & _
                  "FROM hrtraining_analysis_tran " & _
                  "WHERE analysisunkid = @analysisunkid " & _
                  "AND ISNULL(isvoid,0) = 0 "
            'S.SANDEEP [ 24 FEB 2012 ] -- START [<TRAINING> gpa_value ] -- END 

            'Sandeep [ 02 Oct 2010 ] -- End 

            

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRow_Tran = mdtTran.NewRow()

                    dRow_Tran.Item("analysistranunkid") = .Item("analysistranunkid")
                    dRow_Tran.Item("analysisunkid") = .Item("analysisunkid")
                    dRow_Tran.Item("trainerstranunkid") = .Item("trainerstranunkid")
                    dRow_Tran.Item("resultunkid") = .Item("resultunkid")
                    dRow_Tran.Item("reviewer_remark") = .Item("reviewer_remark")
                    dRow_Tran.Item("AUD") = .Item("AUD")

                    'Sandeep [ 02 Oct 2010 ] -- Start
                    dRow_Tran.Item("analysis_date") = .Item("analysis_date")
                    'Sandeep [ 02 Oct 2010 ] -- End 

                    'S.SANDEEP [ 24 FEB 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES [<TRAINING>]
                    dRow_Tran.Item("gpa_value") = .Item("gpa_value")
                    'S.SANDEEP [ 24 FEB 2012 ] -- END 

                    mdtTran.Rows.Add(dRow_Tran)
                End With
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Sub

    Public Function InsertUpdateDelete_AnalysisTran() As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                'Sandeep [ 02 Oct 2010 ] -- Start
                                'strQ = "INSERT INTO hrtraining_analysis_tran ( " & _
                                '                                            "  analysisunkid " & _
                                '                                            ", trainerstranunkid " & _
                                '                                            ", resultunkid " & _
                                '                                            ", reviewer_remark " & _
                                '                                            ", isvoid " & _
                                '                                            ", voiddatetime " & _
                                '                                            ", voiduserunkid " & _
                                '                                            ", voidreason" & _
                                '                                       ") VALUES (" & _
                                '                                            "  @analysisunkid " & _
                                '                                            ", @trainerstranunkid " & _
                                '                                            ", @resultunkid " & _
                                '                                            ", @reviewer_remark " & _
                                '                                            ", @isvoid " & _
                                '                                            ", @voiddatetime " & _
                                '                                            ", @voiduserunkid " & _
                                '                                            ", @voidreason" & _
                                '                                       "); SELECT @@identity"
                                strQ = "INSERT INTO hrtraining_analysis_tran ( " & _
                                            "  analysisunkid " & _
                                            ", trainerstranunkid " & _
                                            ", resultunkid " & _
                                            ", reviewer_remark " & _
                                            ", isvoid " & _
                                            ", voiddatetime " & _
                                            ", voiduserunkid " & _
                                            ", voidreason " & _
                                            ", analysis_date " & _
                                            ", gpa_value " & _
                                       ") VALUES (" & _
                                            "  @analysisunkid " & _
                                            ", @trainerstranunkid " & _
                                            ", @resultunkid " & _
                                            ", @reviewer_remark " & _
                                            ", @isvoid " & _
                                            ", @voiddatetime " & _
                                            ", @voiduserunkid " & _
                                            ", @voidreason " & _
                                            ", @analysis_date " & _
                                            ", @gpa_value " & _
                                       "); SELECT @@identity"
                                'Sandeep [ 02 Oct 2010 ] -- End 

                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)
                                objDataOperation.AddParameter("@trainerstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainerstranunkid").ToString)
                                objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                objDataOperation.AddParameter("@reviewer_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("reviewer_remark").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                'Sandeep [ 02 Oct 2010 ] -- Start
                                'Sandeep [ 16 Oct 2010 ] -- Start
                                'objDataOperation.AddParameter("@analysis_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("analysis_date").ToString)
                                objDataOperation.AddParameter("@analysis_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("analysis_date").ToString)
                                'Sandeep [ 16 Oct 2010 ] -- End 
                                'Sandeep [ 02 Oct 2010 ] -- End 

                                'S.SANDEEP [ 24 FEB 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES [<TRAINING>]
                                objDataOperation.AddParameter("@gpa_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("gpa_value"))
                                'S.SANDEEP [ 24 FEB 2012 ] -- END 

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'objDataOperation.ExecNonQuery(strQ)
                                Dim dList As DataSet = objDataOperation.ExecQuery(strQ, "List")
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                If .Item("analysisunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrtraining_analysis_tran", "analysistranunkid", dList.Tables(0).Rows(0)(0), 2, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_analysis_master", "analysisunkid", mintAnalysisUnkid, "hrtraining_analysis_tran", "analysistranunkid", dList.Tables(0).Rows(0)(0), 1, 1) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                            Case "U"
                                'Sandeep [ 02 Oct 2010 ] -- Start
                                'strQ = "UPDATE hrtraining_analysis_tran SET " & _
                                '        "  analysisunkid = @analysisunkid" & _
                                '        ", trainerstranunkid = @trainerstranunkid" & _
                                '        ", resultunkid = @resultunkid" & _
                                '        ", reviewer_remark = @reviewer_remark" & _
                                '        ", isvoid = @isvoid" & _
                                '        ", voiddatetime = @voiddatetime" & _
                                '        ", voiduserunkid = @voiduserunkid" & _
                                '        ", voidreason = @voidreason " & _
                                '      "WHERE analysistranunkid = @analysistranunkid "

                                strQ = "UPDATE hrtraining_analysis_tran SET " & _
                                        "  analysisunkid = @analysisunkid" & _
                                        ", trainerstranunkid = @trainerstranunkid" & _
                                        ", resultunkid = @resultunkid" & _
                                        ", reviewer_remark = @reviewer_remark" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voidreason = @voidreason " & _
                                        ",analysis_date = @analysis_date " & _
                                        ", gpa_value = @gpa_value " & _
                                      "WHERE analysistranunkid = @analysistranunkid "
                                'Sandeep [ 02 Oct 2010 ] -- End

                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysisunkid").ToString)
                                objDataOperation.AddParameter("@trainerstranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("trainerstranunkid").ToString)
                                objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                objDataOperation.AddParameter("@reviewer_remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("reviewer_remark").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                'Sandeep [ 02 Oct 2010 ] -- Start
                                'Sandeep [ 16 Oct 2010 ] -- Start
                                'objDataOperation.AddParameter("@analysis_date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("analysis_date").ToString)
                                objDataOperation.AddParameter("@analysis_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("analysis_date").ToString)
                                'Sandeep [ 16 Oct 2010 ] -- End 
                                'Sandeep [ 02 Oct 2010 ] -- End 

                                'S.SANDEEP [ 24 FEB 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES [<TRAINING>]
                                objDataOperation.AddParameter("@gpa_value", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("gpa_value"))
                                'S.SANDEEP [ 24 FEB 2012 ] -- END 

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrtraining_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 2) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 


                            Case "D"


                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                'strQ = "DELETE FROM hrtraining_analysis_tran " & _
                                '       "WHERE analysistranunkid = @analysistranunkid "

                                strQ = "UPDATE hrtraining_analysis_tran SET " & _
                                        "  isvoid = @isvoid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voidreason = @voidreason " & _
                                        "WHERE analysistranunkid = @analysistranunkid "

                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                'S.SANDEEP [ 12 OCT 2011 ] -- START
                                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                                If .Item("analysistranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrtraining_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 3) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 12 OCT 2011 ] -- END 
                        End Select
                    End If
                End With
            Next

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_AnalysisTran", mstrModuleName)
        End Try
    End Function

    Public Function Void_All(ByVal isVoid As Boolean, _
                             ByVal dtVoidDate As DateTime, _
                             ByVal strVoidRemark As String, _
                             ByVal intVoidUserId As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            strQ = "UPDATE hrtraining_analysis_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voidreason = @voidreason " & _
                    "WHERE analysisunkid = @analysisunkid "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isVoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strVoidRemark.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserId)
            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Void_All", mstrModuleName)
        End Try
    End Function

    Public Function Void_Single_Analysis(ByVal intUnkid As Integer, _
                             ByVal isVoid As Boolean, _
                             ByVal dtVoidDate As DateTime, _
                             ByVal strVoidRemark As String, _
                             ByVal intVoidUserId As Integer) As Boolean
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            'Sandeep [ 21 APRIL 2011 ] -- Start
            Dim dsList As New DataSet
            objDataOperation.BindTransaction()
            strQ = "SELECT analysisunkid As AId FROM hrtraining_analysis_tran WHERE analysistranunkid = @analysistranunkid  "

            objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Sandeep [ 21 APRIL 2011 ] -- End 

            strQ = "UPDATE hrtraining_analysis_tran SET " & _
                     "  isvoid = @isvoid" & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voiduserunkid = @voiduserunkid" & _
                     ", voidreason = @voidreason " & _
                    "WHERE analysistranunkid = @analysistranunkid "

            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, isVoid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, strVoidRemark.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserId)

            'Sandeep [ 21 APRIL 2011 ] -- Start
            'objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid.ToString)
            'Sandeep [ 21 APRIL 2011 ] -- End 

            Call objDataOperation.ExecNonQuery(strQ)


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Sandeep [ 21 APRIL 2011 ] -- Start
            If dsList.Tables(0).Rows.Count > 0 Then
                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
                'If SetMasterEntryVoid(dsList.Tables(0).Rows(0)(0), isVoid, intVoidUserId, dtVoidDate, strVoidRemark, objDataOperation) = False Then
                '    objDataOperation.ReleaseTransaction(False)
                '    Return False
                'End If
                If SetMasterEntryVoid(dsList.Tables(0).Rows(0)(0), isVoid, intVoidUserId, dtVoidDate, strVoidRemark, objDataOperation, intUnkid) = False Then
                    objDataOperation.ReleaseTransaction(False)
                    Return False
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 
            End If

            objDataOperation.ReleaseTransaction(True)

            'Sandeep [ 21 APRIL 2011 ] -- End 

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Void_All", mstrModuleName)
        End Try
    End Function

    'Sandeep [ 21 APRIL 2011 ] -- Start

    'S.SANDEEP [ 12 OCT 2011 ] -- START
    'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
    'Public Function SetMasterEntryVoid(ByVal intTranAnalysisId As Integer, ByVal blnIsvoid As Boolean, ByVal intvoiduserunkid As Integer, _
    '                                           ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, ByVal objDataOperation As clsDataOperation) As Boolean
    Public Function SetMasterEntryVoid(ByVal intTranAnalysisId As Integer, ByVal blnIsvoid As Boolean, ByVal intvoiduserunkid As Integer, _
                                               ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, ByVal objDataOperation As clsDataOperation, ByVal intUnkid As Integer) As Boolean
        'S.SANDEEP [ 12 OCT 2011 ] -- END 


        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try

            objDataOperation.ClearParameters()

            strQ = "SELECT analysisunkid As AId FROM hrtraining_analysis_tran WHERE analysisunkid = @analysisunkid  And isvoid = 0 "

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranAnalysisId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If dsList.Tables(0).Rows.Count = 0 Then
                strQ = "UPDATE hrtraining_analysis_master SET " & _
                            "   isvoid = @isvoid" & _
                            "  ,voiddatetime = @voiddatetime" & _
                            "  ,voiduserunkid = @voiduserunkid" & _
                            "  ,voidreason = @voidreason " & _
                            " Where analysisunkid = @analysisunkid "

                objDataOperation.AddParameter("@voiddatetime", SqlDbType.SmallDateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intvoiduserunkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsvoid)

                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                'S.SANDEEP [ 12 OCT 2011 ] -- START
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE

                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_analysis_master", "analysisunkid", intTranAnalysisId, "hrtraining_analysis_tran", "analysistranunkid", intUnkid, 3, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Else
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtraining_analysis_master", "analysisunkid", intTranAnalysisId, "hrtraining_analysis_tran", "analysistranunkid", intUnkid, 2, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
                'S.SANDEEP [ 12 OCT 2011 ] -- END 

            End If

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "SetMasterEntryVoid", mstrModuleName)
            Return False
        End Try
    End Function
    'Sandeep [ 21 APRIL 2011 ] -- End 

    'S.SANDEEP [ 12 OCT 2011 ] -- START
    Public Function CheckTrainerTrans(ByVal intTrainerId As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iCnt As Integer = -1
        Dim blnFlag As Boolean = False
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT analysistranunkid FROM hrtraining_analysis_tran Where trainerstranunkid = '" & intTrainerId & "'"

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CheckTrainerTrans", mstrModuleName)
            Return False
        Finally
            StrQ = String.Empty
        End Try
    End Function
    'S.SANDEEP [ 12 OCT 2011 ] -- END 

#End Region

End Class
