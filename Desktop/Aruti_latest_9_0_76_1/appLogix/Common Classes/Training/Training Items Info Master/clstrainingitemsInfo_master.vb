﻿'************************************************************************************************************************************
'Class Name : clstrainingitemsInfo_master.vb
'Purpose    :
'Date       :02/01/2021
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clstrainingitemsInfo_master
    Private Shared ReadOnly mstrModuleName As String = "clstrainingitemsInfo_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region "Enum"

    Public Enum enTrainingItem
        Training_Cost = 1
        Learning_Method = 2
    End Enum

    Public Enum enTrainingCost
        Custom = 0
        Fees = 1
        Travels = 2
        Accommodation = 3
        Books = 4
        Allowances = 5
        Meals = 6
        Venue = 7
        Exams = 8
    End Enum

    Public Enum enTrainingLearningMethod
        Custom = 0
        Classroom = 1
        Online = 2
        Mixed_Classroom_Online = 3
        Coaching = 4
        Mentoring = 5
        Counselling = 6
        On_job_training = 7
        Secondment = 8
    End Enum

#End Region


#Region " Private variables "
    Private mintInfounkid As Integer
    Private mintInfotypeid As Integer
    Private mstrInfo_Code As String = String.Empty
    Private mstrInfo_Name As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mintDefaultItemTypeId As Integer = 0
    Private mblnIsactive As Boolean = True
    Private mintUserunkid As Integer = 0
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mstrWebFormName As String = ""
    Private mblnIsWeb As Boolean = False
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set infounkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Infounkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintInfounkid
        End Get
        Set(ByVal value As Integer)
            mintInfounkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set infotypeid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Infotypeid() As Integer
        Get
            Return mintInfotypeid
        End Get
        Set(ByVal value As Integer)
            mintInfotypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set info_code
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Info_Code() As String
        Get
            Return mstrInfo_Code
        End Get
        Set(ByVal value As String)
            mstrInfo_Code = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set info_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Info_Name() As String
        Get
            Return mstrInfo_Name
        End Get
        Set(ByVal value As String)
            mstrInfo_Name = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DefaultItemTypeId
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _DefaultItemTypeId() As Integer
        Get
            Return mintDefaultItemTypeId
        End Get
        Set(ByVal value As Integer)
            mintDefaultItemTypeId = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  infounkid " & _
                      ", infotypeid " & _
                      ", info_code " & _
                      ", info_name " & _
                      ", description " & _
                      ", defaultitemtypeid " & _
                      ", isactive " & _
                      " FROM trtrainingitemsinfo_master " & _
                      " WHERE infounkid = @infounkid "

            objDataOperation.AddParameter("@infounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInfounkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintInfounkid = CInt(dtRow.Item("infounkid"))
                mintInfotypeid = CInt(dtRow.Item("infotypeid"))
                mstrInfo_Code = dtRow.Item("info_code").ToString
                mstrInfo_Name = dtRow.Item("info_name").ToString
                mstrDescription = dtRow.Item("description").ToString
                mintDefaultItemTypeId = dtRow.Item("defaultitemtypeid").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal xItemTypeId As enTrainingItem, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  infounkid " & _
                      ", infotypeid " & _
                      ", info_code " & _
                      ", info_name " & _
                      ", description " & _
                      ", defaultitemtypeid " & _
                      ", isactive " & _
                      " FROM trtrainingitemsinfo_master "

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            strQ &= " AND infotypeid = @infotypeid "

            strQ &= " ORDER BY info_name "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@infotypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xItemTypeId)

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Sohail (01 Mar 2021) -- Start
    'NMB Enhancement : : New screen Departmental Training Need in New UI.
    Public Function getListForCombo(ByVal xItemTypeId As enTrainingItem, Optional ByVal strListName As String = "List", Optional ByVal blnAddSelect As Boolean = False, Optional ByVal xDataOper As clsDataOperation = Nothing) As DataSet
        Dim objDataOperation As clsDataOperation
        Dim dsList As New DataSet

        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If

        Dim strQ As String = String.Empty
        Dim exForce As Exception
        objDataOperation.ClearParameters()

        Try
            If blnAddSelect = True Then
                strQ = "SELECT 0 AS infounkid, ' ' AS code, ' ' + @name AS name   UNION "
            End If

            strQ &= " SELECT infounkid, info_code AS code, info_name as name  " & _
                        " FROM trtrainingitemsinfo_master " & _
                    " WHERE trtrainingitemsinfo_master.isactive = 1  " & _
                    " AND infotypeid = @infotypeid "

            strQ &= " ORDER BY name "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@infotypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xItemTypeId)

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If xDataOper Is Nothing Then objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function
    'Sohail (01 Mar 2021) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trtrainingitemsinfo_master) </purpose>
    Public Function Insert() As Boolean
        Dim mstrItemName As String = ""

        If mintInfotypeid = enTrainingItem.Training_Cost Then
            mstrItemName = enTrainingItem.Training_Cost.ToString().Replace("_", " ")
        ElseIf mintInfotypeid = enTrainingItem.Learning_Method Then
            mstrItemName = enTrainingItem.Learning_Method.ToString().Replace("_", " ")
        End If
        If isExist(mintInfotypeid, mstrInfo_Code, "", mintInfounkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 5, "This") & " " & mstrItemName & " " & Language.getMessage(mstrModuleName, 2, "code") & " " & Language.getMessage(mstrModuleName, 3, "is already defined. Please define new") & " " & Language.getMessage(mstrModuleName, 2, "code") & "."
            Return False
        ElseIf isExist(mintInfotypeid, "", mstrInfo_Name, mintInfounkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 5, "This") & " " & mstrItemName & " " & Language.getMessage(mstrModuleName, 4, "name") & " " & Language.getMessage(mstrModuleName, 3, "is already defined. Please define new") & " " & Language.getMessage(mstrModuleName, 4, "name") & "."
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@infotypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInfotypeid.ToString)
            objDataOperation.AddParameter("@info_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInfo_Code.ToString)
            objDataOperation.AddParameter("@info_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInfo_Name.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@defaultitemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDefaultItemTypeId.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "INSERT INTO trtrainingitemsinfo_master ( " & _
                      "  infotypeid " & _
                      ", info_code " & _
                      ", info_name " & _
                      ", description " & _
                      ", defaultitemtypeid " & _
                      ", isactive" & _
                    ") VALUES (" & _
                      "  @infotypeid " & _
                      ", @info_code " & _
                      ", @info_name " & _
                      ", @description " & _
                      ", @defaultitemtypeid " & _
                      ", @isactive" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintInfounkid = dsList.Tables(0).Rows(0).Item(0)

            If AtInsertTrainingItemsInfo(objDataOperation, 1) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trtrainingitemsinfo_master) </purpose>
    Public Function Update() As Boolean
        Dim mstrItemName As String = ""

        If mintInfotypeid = enTrainingItem.Training_Cost Then
            mstrItemName = enTrainingItem.Training_Cost.ToString().Replace("_", " ")
        ElseIf mintInfotypeid = enTrainingItem.Learning_Method Then
            mstrItemName = enTrainingItem.Learning_Method.ToString().Replace("_", " ")
        End If
        If isExist(mintInfotypeid, mstrInfo_Code, "", mintInfounkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 5, "This") & " " & mstrItemName & " " & Language.getMessage(mstrModuleName, 2, "code") & " " & Language.getMessage(mstrModuleName, 3, "is already defined. Please define new") & " " & Language.getMessage(mstrModuleName, 2, "code") & "."
            Return False
        ElseIf isExist(mintInfotypeid, "", mstrInfo_Name, mintInfounkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 5, "This") & " " & mstrItemName & " " & Language.getMessage(mstrModuleName, 4, "name") & " " & Language.getMessage(mstrModuleName, 3, "is already defined. Please define new") & " " & Language.getMessage(mstrModuleName, 4, "name") & "."
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@infounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInfounkid.ToString)
            objDataOperation.AddParameter("@infotypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInfotypeid.ToString)
            objDataOperation.AddParameter("@info_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInfo_Code.ToString)
            objDataOperation.AddParameter("@info_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInfo_Name.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@defaultitemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDefaultItemTypeId.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            strQ = "UPDATE trtrainingitemsinfo_master SET " & _
              "  infotypeid = @infotypeid" & _
              ", info_code = @info_code" & _
              ", info_name = @info_name" & _
              ", description = @description" & _
              ", defaultitemtypeid = @defaultitemtypeid " & _
              ", isactive = @isactive " & _
              " WHERE infounkid = @infounkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If AtInsertTrainingItemsInfo(objDataOperation, 2) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trtrainingitemsinfo_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 6, "Sorry, you cannot delete this Entry. Reason : This Entry is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = " UPDATE  trtrainingitemsinfo_master SET isactive = 0 WHERE infounkid = @infounkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@infounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If AtInsertTrainingItemsInfo(objDataOperation, 3) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "select isnull(costitemunkid,0) FROM trdepttrainingneed_costitem_tran WHERE costitemunkid = @infounkid AND isvoid = 0 "

            objDataOperation.AddParameter("@infounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "select isnull(trainingcostitemunkid,0) FROM trtraining_request_cost_tran WHERE trainingcostitemunkid = @infounkid AND isvoid = 0 "
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "select isnull(learningmethodunkid,0) FROM trdepartmentaltrainingneed_master WHERE learningmethodunkid = @infounkid AND isvoid = 0 "
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xItemTypeId As enTrainingItem, ByVal xItemCode As String, ByVal xItemName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  infounkid " & _
                      ", infotypeid " & _
                      ", info_code " & _
                      ", info_name " & _
                      ", description " & _
                      ", defaultitemtypeid " & _
                      ", isactive " & _
                      " FROM trtrainingitemsinfo_master " & _
                      " WHERE isactive = 1 AND infotypeid = @infotypeid "

            If xItemCode.Trim.Length > 0 Then
                strQ &= " AND info_code = @info_code"
                objDataOperation.AddParameter("@info_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xItemCode)
            End If

            If xItemName.Trim.Length > 0 Then
                strQ &= " AND info_name = @info_name"
                objDataOperation.AddParameter("@info_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xItemName)
            End If

            If intUnkid > 0 Then
                strQ &= " AND infounkid <> @infounkid"
            End If

            objDataOperation.AddParameter("@infotypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xItemTypeId)
            objDataOperation.AddParameter("@infounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function AtInsertTrainingItemsInfo(ByVal objDoOperation As clsDataOperation, ByVal xAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDoOperation.ClearParameters()
            objDoOperation.AddParameter("@infounkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInfounkid.ToString)
            objDoOperation.AddParameter("@infotypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintInfotypeid.ToString)
            objDoOperation.AddParameter("@info_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInfo_Code.ToString)
            objDoOperation.AddParameter("@info_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrInfo_Name.ToString)
            objDoOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDoOperation.AddParameter("@defaultitemtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintDefaultItemTypeId.ToString)
            objDoOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType.ToString)
            objDoOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            If mstrWebClientIP.Trim.Length > 0 Then
                objDoOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            Else
                objDoOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            End If

            If mstrWebHostName.Length > 0 Then
                objDoOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)
            Else
                objDoOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName())
            End If

            objDoOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
            objDoOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            strQ = "INSERT INTO attrtrainingitemsinfo_master ( " & _
                      "  infounkid " & _
                      ", infotypeid " & _
                      ", info_code " & _
                      ", info_name " & _
                      ", description " & _
                      ", defaultitemtypeid " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb " & _
                    ") VALUES (" & _
                      "  @infounkid " & _
                      ", @infotypeid " & _
                      ", @info_code " & _
                      ", @info_name " & _
                      ", @description " & _
                      ", @defaultitemtypeid " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDoOperation.ExecQuery(strQ, "List")

            If objDoOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AtInsertTrainingItemsInfo; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub InsertDefaultParameters()
        Dim dsList As New DataSet
        Dim objDataOperation As clsDataOperation
        Dim strQ As String = ""
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.ExecNonQuery("INSERT INTO trtrainingitemsinfo_master(infotypeid, info_code,  info_name, description, defaultitemtypeid, isactive) SELECT " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & ", 'Accommodation', 'Accommodation', '' , " & clstrainingitemsInfo_master.enTrainingCost.Accommodation & ", 1 WHERE NOT EXISTS (SELECT * FROM trtrainingitemsinfo_master WHERE infotypeid = " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " AND defaultitemtypeid = " & clstrainingitemsInfo_master.enTrainingCost.Accommodation & ")")
            objDataOperation.ExecNonQuery("INSERT INTO trtrainingitemsinfo_master(infotypeid, info_code,  info_name, description, defaultitemtypeid, isactive) SELECT " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " , 'Allowances', 'Allowances', '', " & clstrainingitemsInfo_master.enTrainingCost.Allowances & ", 1 WHERE NOT EXISTS (SELECT * FROM trtrainingitemsinfo_master WHERE infotypeid = " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " AND defaultitemtypeid = " & clstrainingitemsInfo_master.enTrainingCost.Allowances & ")")
            objDataOperation.ExecNonQuery("INSERT INTO trtrainingitemsinfo_master(infotypeid, info_code,  info_name, description, defaultitemtypeid, isactive) SELECT " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " , 'Books', 'Books', '', " & clstrainingitemsInfo_master.enTrainingCost.Books & ", 1 WHERE NOT EXISTS (SELECT * FROM trtrainingitemsinfo_master WHERE infotypeid = " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " AND defaultitemtypeid = " & clstrainingitemsInfo_master.enTrainingCost.Books & ")")
            objDataOperation.ExecNonQuery("INSERT INTO trtrainingitemsinfo_master(infotypeid, info_code,  info_name, description, defaultitemtypeid, isactive) SELECT " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " , 'Exams', 'Exams',  '', " & clstrainingitemsInfo_master.enTrainingCost.Exams & ",  1 WHERE NOT EXISTS (SELECT * FROM trtrainingitemsinfo_master WHERE infotypeid = " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " AND defaultitemtypeid = " & clstrainingitemsInfo_master.enTrainingCost.Exams & ")")
            objDataOperation.ExecNonQuery("INSERT INTO trtrainingitemsinfo_master(infotypeid, info_code,  info_name, description, defaultitemtypeid, isactive) SELECT " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " , 'Fees', 'Fees', '',  " & clstrainingitemsInfo_master.enTrainingCost.Fees & ", 1 WHERE NOT EXISTS (SELECT * FROM trtrainingitemsinfo_master WHERE infotypeid = " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " AND defaultitemtypeid = " & clstrainingitemsInfo_master.enTrainingCost.Fees & ")")
            objDataOperation.ExecNonQuery("INSERT INTO trtrainingitemsinfo_master(infotypeid, info_code,  info_name, description, defaultitemtypeid, isactive) SELECT " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " , 'Meals', 'Meals', '',  " & clstrainingitemsInfo_master.enTrainingCost.Meals & ", 1 WHERE NOT EXISTS (SELECT * FROM trtrainingitemsinfo_master WHERE infotypeid = " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " AND defaultitemtypeid = " & clstrainingitemsInfo_master.enTrainingCost.Meals & ")")
            objDataOperation.ExecNonQuery("INSERT INTO trtrainingitemsinfo_master(infotypeid, info_code,  info_name, description, defaultitemtypeid, isactive) SELECT " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " , 'Travels', 'Travels', '', " & clstrainingitemsInfo_master.enTrainingCost.Travels & ", 1 WHERE NOT EXISTS (SELECT * FROM trtrainingitemsinfo_master WHERE infotypeid = " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " AND defaultitemtypeid = " & clstrainingitemsInfo_master.enTrainingCost.Travels & ")")
            objDataOperation.ExecNonQuery("INSERT INTO trtrainingitemsinfo_master(infotypeid, info_code,  info_name, description, defaultitemtypeid, isactive) SELECT " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " , 'Venue', 'Venue', '', " & clstrainingitemsInfo_master.enTrainingCost.Venue & ", 1 WHERE NOT EXISTS (SELECT * FROM trtrainingitemsinfo_master WHERE infotypeid = " & clstrainingitemsInfo_master.enTrainingItem.Training_Cost & " AND defaultitemtypeid = " & clstrainingitemsInfo_master.enTrainingCost.Venue & ")")


            'objDataOperation.ExecNonQuery("INSERT INTO trtrainingitemsinfo_master(infotypeid, info_code,  info_name, description, defaultitemtypeid, isactive) SELECT '', '', '', " & clstrainingitemsInfo_master.enTrainingCost.Highest & ", 1 WHERE NOT EXISTS (SELECT * FROM trtrainingcategory_master WHERE categorydefaultypeid = " & enTrainingCategoryDefaultId.Business_Strategy & ")")
            'objDataOperation.ExecNonQuery("INSERT INTO trtrainingitemsinfo_master(infotypeid, info_code,  info_name, description, defaultitemtypeid, isactive) SELECT '', '', '', " & clstrainingitemsInfo_master.enTrainingCost.Highest & ", 1 WHERE NOT EXISTS (SELECT * FROM trtrainingcategory_master WHERE categorydefaultypeid = " & enTrainingCategoryDefaultId.Business_Strategy & ")")

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertDefaultParameters; Module Name: " & mstrModuleName)
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
			Language.setMessage(mstrModuleName, 1, "Select")
            Language.setMessage(mstrModuleName, 2, "code")
            Language.setMessage(mstrModuleName, 3, "is already defined. Please define new")
            Language.setMessage(mstrModuleName, 4, "name")
			Language.setMessage(mstrModuleName, 5, "This")
			Language.setMessage(mstrModuleName, 6, "Sorry, you cannot delete this Entry. Reason : This Entry is already linked with some transaction.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class