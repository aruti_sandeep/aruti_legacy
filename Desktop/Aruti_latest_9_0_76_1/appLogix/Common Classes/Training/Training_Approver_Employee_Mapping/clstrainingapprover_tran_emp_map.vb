﻿'************************************************************************************************************************************
'Class Name :clstrainingapprover_tran_emp_map.vb
'Purpose    :
'Date       :11-Jul-2022
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports eZee.Common.eZeeForm

''' <summary>
''' Purpose: 
''' Developer: Hemant Morker
''' </summary>
Public Class clstrainingapprover_tran_emp_map
    Private Shared ReadOnly mstrModuleName As String = "clstrainingapprover_tran_emp_map"
    Dim mstrMessage As String = ""

#Region "Private Variables"

    Private mintApproverTranunkid As Integer = 0
    Private mintApproverunkid As Integer = 0
    Private mdtTran As DataTable
    Private mintUserId As Integer = 0
    Private objDataOperation As clsDataOperation
    Private mstrFormName As String = String.Empty
    Private mstrClientIP As String = String.Empty
    Private mstrHostName As String = String.Empty
    Private mblnIsWeb As Boolean = False

#End Region

#Region "Properties"

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    '' <summary>
    '' Purpose: Get or Set approvertranunkid
    '' Modify By: Hemant
    '' </summary>
    '' 

    Public Property _ApproverTranunkid(Optional ByVal objDataOp As clsDataOperation = Nothing) As Integer

        Get
            Return mintApproverTranunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverTranunkid = value
            Call GetData(objDataOp)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approverunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Approverunkid() As Integer
        Get
            Return mintApproverunkid
        End Get
        Set(ByVal value As Integer)
            mintApproverunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trandatatable
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _TranDataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set FormName
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _FormName() As String
        Get
            Return mstrFormName
        End Get
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ClientIP
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set HostName
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Hemant
    ''' </summary>
    Public WriteOnly Property _IsWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set UserId
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _UserId() As Integer
        Get
            Return mintUserId
        End Get
        Set(ByVal value As Integer)
            mintUserId = value
        End Set
    End Property

#End Region

#Region "Constructor"

    Public Sub New()
        mdtTran = New DataTable("List")
        Dim dCol As DataColumn
        Try
            dCol = New DataColumn("ischeck")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("approvertranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("approverunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("employeeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            dCol.AllowDBNull = True
            dCol.DefaultValue = DBNull.Value
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ecode")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ename")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("edept")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("ejob")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "Public Methods"

    Public Sub GetData(Optional ByVal objDataOp As clsDataOperation = Nothing)


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDataOp IsNot Nothing Then
            objDataOperation = objDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        objDataOperation.ClearParameters()
        Try
            strQ = " SELECT " & _
                    "  CAST (0 AS BIT) AS ischeck " & _
                    " ,trtrainingapprover_tran.approvertranunkid " & _
                    " ,trtrainingapprover_tran.approverunkid " & _
                    " ,trtrainingapprover_tran.employeeunkid " & _
                    " ,trtrainingapprover_tran.userunkid " & _
                    " ,trtrainingapprover_tran.isvoid " & _
                    " ,trtrainingapprover_tran.voiddatetime " & _
                    " ,trtrainingapprover_tran.voiduserunkid " & _
                    " ,trtrainingapprover_tran.voidreason " & _
                    " ,'' AS AUD " & _
                    " ,hremployee_master.employeecode AS ecode " & _
                    " ,hremployee_master.firstname +' '+ hremployee_master.surname AS ename  " & _
                    " ,hrdepartment_master.name AS edept " & _
                    " ,hrjob_master.job_name AS ejob " & _
                " FROM trtrainingapprover_tran " & _
                    " JOIN hremployee_master ON trtrainingapprover_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    " JOIN hrdepartment_master ON hremployee_master.departmentunkid = hrdepartment_master.departmentunkid " & _
                    " JOIN hrjob_master ON hremployee_master.jobunkid = hrjob_master.jobunkid " & _
                    " JOIN trtrainingapprover_master ON trtrainingapprover_tran.approverunkid = trtrainingapprover_master.approverunkid " & _
                " WHERE trtrainingapprover_master.approverunkid = '" & mintApproverunkid & "' AND trtrainingapprover_tran.isvoid = 0 "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()
            For Each drRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(drRow)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetData", mstrModuleName)
        Finally
            If objDataOp Is Nothing Then objDataOperation = Nothing
        End Try

    End Sub

    Public Function Insert_Update_Delete(ByVal objDataOperation As clsDataOperation) As Boolean

        Dim i As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exFoece As Exception

        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = " INSERT INTO trtrainingapprover_tran (" & _
                                            "  approverunkid " & _
                                            " ,employeeunkid " & _
                                            " ,userunkid " & _
                                            " ,isvoid " & _
                                            " ,voiddatetime " & _
                                            " ,voiduserunkid " & _
                                            " ,voidreason " & _
                                        ") VALUES (" & _
                                            "  @approverunkid " & _
                                            " ,@employeeunkid " & _
                                            " ,@userunkid " & _
                                            " ,@isvoid " & _
                                            " ,@voiddatetime " & _
                                            " ,@voiduserunkid " & _
                                            " ,@voidreason " & _
                                        "); SELECT @@identity "

                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                                mintApproverTranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If InsertAuditTrails(objDataOperation, 1, CInt(.Item("employeeunkid"))) = False Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If


                            Case "U"
                                strQ = "UPDATE trtrainingapprover_tran SET " & _
                                            "  approverunkid = @approverunkid " & _
                                            " ,employeeunkid = @employeeunkid " & _
                                            " ,userunkid = @ userunkid " & _
                                            " ,isvoid = @ isvoid " & _
                                            " ,voiddatetime = @ voiddatetime " & _
                                            " ,voiduserunkid = @ voiduserunkid " & _
                                            " ,voidreason = @ voidreason " & _
                                        "WHERE approvertranunkid = @ approvertranunkid "

                                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid)
                                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("employeeunkid"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("approvertranunkid"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                                mintApproverTranunkid = CInt(.Item("approvertranunkid"))

                                If InsertAuditTrails(objDataOperation, 2, CInt(.Item("employeeunkid"))) = False Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                            Case "D"
                                strQ = "UPDATE trtrainingapprover_tran SET " & _
                                            " isvoid = @isvoid " & _
                                            " ,voiddatetime = @voiddatetime " & _
                                            " ,voiduserunkid = @voiduserunkid " & _
                                            " ,voidreason = @voidreason " & _
                                        "WHERE approvertranunkid = @approvertranunkid "

                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("approvertranunkid"))

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                                If InsertAuditTrails(objDataOperation, 3, CInt(.Item("employeeunkid"))) = False Then
                                    exFoece = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exFoece
                                End If

                        End Select
                    End If

                End With
            Next

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert_Update_Delete , Module Name: " & mstrModuleName)
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer, ByVal intEmployeeId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverTranunkid.ToString)
            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeId.ToString)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, IIf(mstrClientIP.Trim.Length <= 0, getIP, mstrClientIP))
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrHostName.Trim.Length <= 0, getHostName, mstrHostName))
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)



            strQ = "INSERT INTO attrtrainingapprover_tran (" & _
                        "  tranguid " & _
                        ",approvertranunkid " & _
                        ",approverunkid " & _
                        ",employeeunkid " & _
                        ",audittype " & _
                        ",audituserunkid " & _
                        ",auditdatetime " & _
                        ",ip " & _
                        ",machine_name " & _
                        ",form_name " & _
                        ",isweb " & _
                    ") VALUES (" & _
                        "  LOWER(NEWID()) " & _
                        ", @approvertranunkid " & _
                        ", @approverunkid " & _
                        ", @employeeunkid " & _
                        ", @audittype " & _
                        ", @audituserunkid " & _
                        ", @auditdatetime " & _
                        ", @ip " & _
                        ", @machine_name " & _
                        ", @form_name " & _
                        ", @isweb " & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    'Hemant (16 Sep 2022) -- Start
    'ISSUE/ENHANCEMENT(NMB) : Pending Training approval should be go for New User instead of Old User  after changing User  in Training Approver Add/Edit Screen
    Public Function UpdateMapedUser(ByVal mintMapUserunkid As String, ByVal intApproverTranUnkid As Integer, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = String.Empty
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If


        Try
            

            strQ = " UPDATE trtrainingapproval_process_tran SET " & _
                   "    mapuserunkid = @mapuserunkid " & _
                   " FROM trtrainingapproval_process_tran " & _
                   " JOIN trtraining_request_master ON trtraining_request_master.trainingrequestunkid = trtrainingapproval_process_tran.trainingrequestunkid and trtraining_request_master.isvoid = 0 " & _
                   " WHERE trtrainingapproval_process_tran.isvoid = 0 " & _
                   "     AND trtraining_request_master.statusunkid =  " & enTrainingRequestStatus.PENDING & _
                   "     AND trtrainingapproval_process_tran.approvertranunkid = @approvertranunkid " & _
                   "     AND trtrainingapproval_process_tran.visibleid  <= 1 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMapUserunkid)
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverTranUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")


            If objDataOperation.ErrorMessage <> "" Then
                mstrMessage = objDataOperation.ErrorMessage
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "INSERT INTO attrtrainingapproval_process_tran ( " & _
                    " tranguid " & _
                    ", pendingtrainingtranunkid " & _
                    ", trainingrequestunkid " & _
                    ", employeeunkid " & _
                    ", approvertranunkid " & _
                    ", priority " & _
                    ", approvaldate " & _
                    ", totalcostamount " & _
                    ", approvedamount " & _
                    ", statusunkid " & _
                    ", remark " & _
                    ", visibleid " & _
                    ", audittypeid " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", formname " & _
                    ", ip " & _
                    ", host " & _
                    ", isweb " & _
                    ", mapuserunkid " & _
                    ", completed_statusunkid " & _
                    ", completed_remark " & _
                    ", completed_visibleid " & _
                    ") " & _
                    "SELECT " & _
                    "   LOWER(NEWID()) " & _
                    ",  pendingtrainingtranunkid " & _
                    ",  trtrainingapproval_process_tran.trainingrequestunkid " & _
                    ",  trtrainingapproval_process_tran.employeeunkid " & _
                    ",  trtrainingapproval_process_tran.approvertranunkid " & _
                    ",  trtrainingapproval_process_tran.priority " & _
                    ",  trtrainingapproval_process_tran.approvaldate " & _
                    ",  trtrainingapproval_process_tran.totalcostamount " & _
                    ",  trtrainingapproval_process_tran.approvedamount " & _
                    ",  trtrainingapproval_process_tran.statusunkid " & _
                    ",  trtrainingapproval_process_tran.remark " & _
                    ",  trtrainingapproval_process_tran.visibleid " & _
                    ",  2 " & _
                    ",  @audituserunkid " & _
                    ",  GETDATE() " & _
                    ",  @form_name " & _
                    ",  @ip " & _
                    ",  @host " & _
                    ",  @isweb " & _
                    ",  trtrainingapproval_process_tran.mapuserunkid " & _
                    ",  trtrainingapproval_process_tran.completed_statusunkid " & _
                    ",  trtrainingapproval_process_tran.completed_remark " & _
                    ",  trtrainingapproval_process_tran.completed_visibleid " & _
                     "FROM trtrainingapproval_process_tran " & _
                     "JOIN trtraining_request_master ON trtraining_request_master.trainingrequestunkid = trtrainingapproval_process_tran.trainingrequestunkid     AND trtraining_request_master.isvoid = 0 " & _
                     "WHERE trtrainingapproval_process_tran.isvoid = 0 " & _
                     "AND trtraining_request_master.statusunkid = " & enTrainingRequestStatus.PENDING & _
                     "AND trtrainingapproval_process_tran.approvertranunkid = @approvertranunkid " & _
                     "AND trtrainingapproval_process_tran.visibleid  <= 1 "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApproverTranUnkid)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(mstrClientIP.Trim.Length <= 0, getIP, mstrClientIP))
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrFormName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            Call objDataOperation.ExecNonQuery(strQ)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateMapedUser , Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing

        End Try
    End Function
    'Hemant (16 Sep 2022) -- End

    'Hemant (22 Dec 2022) -- Start
    'ENHANCEMENT(NMB) : A1X-22 - As a user, I want to have the ability to perform approver migrations incase of employee/approver movements
    Public Function Approver_Migration(ByVal dtTable As DataTable, ByVal intNewApproverId As Integer, ByVal intNewEmpapproverId As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim mstrEmployeeID As String = ""
        Try


            Dim objDataOperation As clsDataOperation

            If xDataOpr IsNot Nothing Then
                objDataOperation = xDataOpr
            Else
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            End If


            If dtTable Is Nothing Then Return True

            Dim objApprovalProcessTran As New clstrainingapproval_process_tran

            Dim mintOldApproverId As Integer = 0


            For i = 0 To dtTable.Rows.Count - 1


                strQ = " Update trtrainingapprover_tran set " & _
                     " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                     " WHERE approverunkid = @approverunkid AND employeeunkid = @employeeunkid"

                mintApproverunkid = CInt(dtTable.Rows(i)("approverunkid"))

                mintOldApproverId = CInt(dtTable.Rows(i)("approverunkid"))

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtTable.Rows(i)("approverunkid")))
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 4, "Migration"))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                _ApproverTranunkid(objDataOperation) = CInt(dtTable.Rows(i)("approvertranunkid"))

                If InsertAuditTrails(objDataOperation, 3, CInt(dtTable.Rows(i)("employeeunkid"))) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                '===START-- GETTING ALREADY EXIST EMPLOYEE FROM NEW TRAINING APPROVER AND ADDED TO MIGRATED EMPLOYEE VARIABLE.IT IS USED WHEN ALL OLD TRAINING APPROVER'S EMPLOYEE TO NEW TRAINING APPROVER.
                strQ = " SELECT ISNULL(employeeunkid,0) employeeunkid FROM trtrainingapprover_tran WHERE approverunkid = @approverunkid AND employeeunkid = @employeeunkid AND isvoid = 0 "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewApproverId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                Dim dtEmpCount As DataSet = objDataOperation.ExecQuery(strQ, "List")
                If dtEmpCount.Tables(0).Rows.Count > 0 Then
                    If CInt(dtEmpCount.Tables(0).Rows(0)("employeeunkid")) > 0 Then
                        mstrEmployeeID &= dtTable.Rows(i)("employeeunkid").ToString() & ","
                        Continue For
                    End If
                End If
                '===END-- GETTING ALREADY EXIST EMPLOYEE FROM NEW TRAINING APPROVER AND ADDED TO MIGRATED EMPLOYEE VARIABLE.IT IS USED WHEN ALL OLD TRAINING APPROVER'S EMPLOYEE TO NEW TRAINING APPROVER.


                strQ = "INSERT INTO trtrainingapprover_tran ( " & _
                             "  approverunkid " & _
                             ", employeeunkid " & _
                             ", userunkid " & _
                             ", isvoid " & _
                             ", voiduserunkid " & _
                         ") VALUES (" & _
                             "  @approverunkid " & _
                             ", @employeeunkid " & _
                             ", @userunkid " & _
                             ", @isvoid " & _
                             ", @voiduserunkid " & _
                             "); SELECT @@identity"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewApproverId)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtTable.Rows(i)("employeeunkid").ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim mintApprovertranunkid As Integer = dsList.Tables(0).Rows(0).Item(0)

                mintApproverunkid = intNewApproverId
                _ApproverTranunkid(objDataOperation) = mintApprovertranunkid

                If InsertAuditTrails(objDataOperation, 1, CInt(dtTable.Rows(i)("employeeunkid"))) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                '/*  START FOR CHECK TRAINING REQUESTS   */

                Dim mstrFormId As String = objApprovalProcessTran.GetProcessPendingTrainingIds(CInt(dtTable.Rows(i)("approverunkid")), dtTable.Rows(i)("employeeunkid").ToString(), objDataOperation)

                If mstrFormId.Trim.Length > 0 Then

                    strQ = " Update trtrainingapproval_process_tran set mapuserunkid  = @newmapuserunkid,approvertranunkid = @newapprovertranunkid  WHERE trainingrequestunkid in (" & mstrFormId & ") AND approvertranunkid = @approvertranunkid AND trtrainingapproval_process_tran.isvoid = 0 "
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@newmapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewEmpapproverId)
                    objDataOperation.AddParameter("@newapprovertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewApproverId)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, CInt(dtTable.Rows(i)("approverunkid")))
                    objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    strQ = "Select ISNULL(pendingtrainingtranunkid,0) pendingtrainingtranunkid FROM trtrainingapproval_process_tran WHERE mapuserunkid = @mapuserunkid AND approvertranunkid = @approvertranunkid AND trainingrequestunkid  in (" & mstrFormId & ") AND isvoid = 0"
                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@mapuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewEmpapproverId)
                    objDataOperation.AddParameter("@approvertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intNewApproverId)
                    dsList = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables(0).Rows.Count > 0 Then
                        For k As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "trtrainingapproval_process_tran", "pendingtrainingtranunkid", dsList.Tables(0).Rows(k)("pendingtrainingtranunkid").ToString(), False) = False Then
                                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                        Next
                    End If

                End If

                '/*  END FOR CHECK TRAINING REQUESTS   */

                mstrEmployeeID &= dtTable.Rows(i)("employeeunkid").ToString() & ","

            Next


            If mstrEmployeeID.Trim.Length > 0 Then
                mstrEmployeeID = mstrEmployeeID.Substring(0, mstrEmployeeID.Trim.Length - 1)
            End If

            Dim objMigration As New clsMigration
            objMigration._Migrationdate = ConfigParameter._Object._CurrentDateAndTime
            
            objMigration._Migrationfromunkid = mintOldApproverId

            objMigration._Migrationtounkid = intNewApproverId
            objMigration._Usertypeid = enUserType.Training_Approver
            objMigration._Userunkid = mintUserId
            objMigration._Migratedemployees = mstrEmployeeID

            If objMigration.Insert(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = " SELECT ISNULL(employeeunkid,0) employeeunkid FROM trtrainingapprover_tran WHERE approverunkid = @approverunkid AND isvoid = 0  "
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverId)


            Dim dtCount As DataSet = objDataOperation.ExecQuery(strQ, "List")

            If dtCount IsNot Nothing AndAlso dtCount.Tables(0).Rows.Count <= 0 Then

                Dim objTrainingApprover As New clstrainingapprover_master_emp_map

                strQ = " Update trtrainingapprover_master set " & _
                          " isvoid = 1,voiddatetime=@voiddatetime,voidreason = @voidreason,voiduserunkid = @voiduserunkid " & _
                          " WHERE approverunkid = @approverunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverId)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserId)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, Language.getMessage(mstrModuleName, 4, "Migration"))
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objTrainingApprover._Approverunkid = mintOldApproverId
                If objTrainingApprover.InsertAuditTrails(objDataOperation, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                strQ = "SELECT ISNULL(mappingunkid,0) mappingunkid FROM hrapprover_usermapping WHERE approverunkid = @approverunkid AND usertypeid = " & CInt(enUserType.Training_Approver) & ""
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverId)
                dsList = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                    If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapprover_usermapping", "mappingunkid", CInt(dsList.Tables(0).Rows(i)("mappingunkid")), False, mintUserId) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next

                strQ = "Delete from hrapprover_usermapping WHERE approverunkid = @approverunkid AND usertypeid = " & CInt(enUserType.Training_Approver) & " "
                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldApproverId)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If


            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Approver_Migration, Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function
    'Hemant (22 Dec 2022) -- End	
    

#Region " Import Employee With Approver"

    Public Function ImportInsertEmployee(ByVal mstrEmployeeId As String, ByRef intApprTranId As Integer) As Boolean
        Dim strQ As String = String.Empty
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception
        Try
            mstrMessage = ""
            Dim objDataOperation As New clsDataOperation

            If mstrEmployeeId.Trim.Length <= 0 Then Return True

            strQ = "SELECT approvertranunkid AS Id FROM trtrainingapprover_tran WHERE approverunkid = '" & mintApproverunkid & "' AND employeeunkid = '" & CInt(mstrEmployeeId) & "' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                mstrMessage = objDataOperation.ErrorMessage
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intApprTranId = dsList.Tables(0).Rows(0).Item("Id")
                Return False
            End If

            strQ = "INSERT INTO trtrainingapprover_tran ( " & _
                              " approverunkid " & _
                              ", employeeunkid " & _
                              ", userunkid " & _
                              ", isvoid " & _
                              ", voiduserunkid " & _
                          ") VALUES (" & _
                              "  @approverunkid " & _
                              ", @employeeunkid " & _
                              ", @userunkid " & _
                              ", @isvoid " & _
                              ", @voiduserunkid " & _
                              "); SELECT @@identity"


            Dim arEmployee() As String = mstrEmployeeId.Trim.Split(",")

            For i As Integer = 0 To arEmployee.Length - 1

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@approverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApproverunkid.ToString)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, arEmployee(i).ToString)
                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                dsList = objDataOperation.ExecQuery(strQ, "List")


                If objDataOperation.ErrorMessage <> "" Then
                    mstrMessage = objDataOperation.ErrorMessage
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                mintApprovertranunkid = dsList.Tables(0).Rows(0).Item(0)

                If InsertAuditTrails(objDataOperation, 1, CInt(arEmployee(i))) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ImportInsertEmloyee; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

#End Region
#End Region

End Class
