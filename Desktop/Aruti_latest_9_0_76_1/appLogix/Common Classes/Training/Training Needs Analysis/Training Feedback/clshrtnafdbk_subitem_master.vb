﻿'************************************************************************************************************************************
'Class Name : clshrtnafdbk_subitem_master.vb
'Purpose    :
'Date       :23/02/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clshrtnafdbk_subitem_master
    Private Shared ReadOnly mstrModuleName As String = "clshrtnafdbk_subitem_master"
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintFdbksubitemunkid As Integer
    Private mintFdbkitemunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fdbksubitemunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fdbksubitemunkid() As Integer
        Get
            Return mintFdbksubitemunkid
        End Get
        Set(ByVal value As Integer)
            mintFdbksubitemunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fdbkitemunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fdbkitemunkid() As Integer
        Get
            Return mintFdbkitemunkid
        End Get
        Set(ByVal value As Integer)
            mintFdbkitemunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  fdbksubitemunkid " & _
              ", fdbkitemunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
             "FROM hrtnafdbk_subitem_master " & _
             "WHERE fdbksubitemunkid = @fdbksubitemunkid "

            objDataOperation.AddParameter("@fdbksubitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFdbksubitemunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintFdbksubitemunkid = CInt(dtRow.Item("fdbksubitemunkid"))
                mintFdbkitemunkid = CInt(dtRow.Item("fdbkitemunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  hrtnafdbk_subitem_master.fdbksubitemunkid " & _
              ", hrtnafdbk_subitem_master.fdbkitemunkid " & _
              ", hrtnafdbk_subitem_master.code " & _
              ", hrtnafdbk_subitem_master.name " & _
              ", hrtnafdbk_subitem_master.description " & _
              ", hrtnafdbk_subitem_master.isactive " & _
              ", hrtnafdbk_subitem_master.name1 " & _
              ", hrtnafdbk_subitem_master.name2 " & _
              ", ISNULL(hrtnafdbk_item_master.name,'') AS FeedBackItem " & _
              ", ISNULL(hrtnafdbk_group_master.fdbkgroupunkid,0) AS fdbkgroupunkid " & _
             "FROM hrtnafdbk_subitem_master " & _
             "LEFT JOIN hrtnafdbk_item_master ON hrtnafdbk_subitem_master.fdbkitemunkid = hrtnafdbk_item_master.fdbkitemunkid AND hrtnafdbk_item_master.fromimpact = 0 " & _
             "LEFT JOIN hrtnafdbk_group_master ON hrtnafdbk_item_master.fdbkgroupunkid = hrtnafdbk_group_master.fdbkgroupunkid "

            If blnOnlyActive Then
                strQ &= " WHERE hrtnafdbk_subitem_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtnafdbk_subitem_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode, , mintFdbkitemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This feedback subitem code already exists. Please define new subitem code.")
            Return False
        End If

        If isExist(, mstrName, mintFdbkitemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This feedback subitem name already exists. Please define new subitem name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@fdbkitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFdbkitemunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)

            strQ = "INSERT INTO hrtnafdbk_subitem_master ( " & _
                      "  fdbkitemunkid " & _
                      ", code " & _
                      ", name " & _
                      ", description " & _
                      ", isactive " & _
                      ", name1 " & _
                      ", name2" & _
                    ") VALUES (" & _
                      "  @fdbkitemunkid " & _
                      ", @code " & _
                      ", @name " & _
                      ", @description " & _
                      ", @isactive " & _
                      ", @name1 " & _
                      ", @name2" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintFdbksubitemunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrtnafdbk_subitem_master", "fdbksubitemunkid", mintFdbksubitemunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtnafdbk_subitem_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCode, , mintFdbkitemunkid, mintFdbksubitemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This feedback subitem code already exists. Please define new subitem code.")
            Return False
        End If

        If isExist(, mstrName, mintFdbkitemunkid, mintFdbksubitemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This feedback subitem name already exists. Please define new subitem name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@fdbksubitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFdbksubitemunkid.ToString)
            objDataOperation.AddParameter("@fdbkitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFdbkitemunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)

            strQ = "UPDATE hrtnafdbk_subitem_master SET " & _
                      "  fdbkitemunkid = @fdbkitemunkid" & _
                      ", code = @code" & _
                      ", name = @name" & _
                      ", description = @description" & _
                      ", isactive = @isactive" & _
                      ", name1 = @name1" & _
                      ", name2 = @name2 " & _
                    "WHERE fdbksubitemunkid = @fdbksubitemunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrtnafdbk_subitem_master", mintFdbksubitemunkid, "fdbksubitemunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrtnafdbk_subitem_master", "fdbksubitemunkid", mintFdbksubitemunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtnafdbk_subitem_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this feedback subitem. Reason : This feedback subitem is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrtnafdbk_subitem_master SET " & _
                   " isactive = 0 " & _
                   "WHERE fdbksubitemunkid = @fdbksubitemunkid "

            objDataOperation.AddParameter("@fdbksubitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrtnafdbk_subitem_master", "fdbksubitemunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                   "    'SELECT * FROM '+TABLE_NAME +' WHERE ' +COLUMN_NAME +' = ' AS TableName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='fdbksubitemunkid' AND TABLE_NAME <> 'hrtnafdbk_subitem_master' "

            objDataOperation.AddParameter("@fdbkitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows
                strQ = dRow.Item("TableName") & "'" & intUnkid & "'"
                If objDataOperation.RecordCount(strQ) > 0 Then blnFlag = True

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            Next
            mstrMessage = ""
            Return blnFlag
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intItemUnkid As Integer = -1, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                  "  fdbksubitemunkid " & _
                  ", fdbkitemunkid " & _
                  ", code " & _
                  ", name " & _
                  ", description " & _
                  ", isactive " & _
                  ", name1 " & _
                  ", name2 " & _
             "FROM hrtnafdbk_subitem_master " & _
             "WHERE isactive = 1 "

            If strName.Trim.Length > 0 Then
                strQ &= "AND name = @name "
            End If

            If strCode.Trim.Length > 0 Then
                strQ &= " AND code = @code "
            End If

            If intItemUnkid > 0 Then
                strQ &= " AND fdbkitemunkid = @fdbkitemunkid "
            End If

            If intUnkid > 0 Then
                strQ &= " AND fdbksubitemunkid <> @fdbksubitemunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@fdbksubitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@fdbkitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intItemUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False, Optional ByVal intItemUnkid As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As fdbksubitemunkid , @ItemName As  name  UNION "
            End If
            strQ &= "SELECT fdbksubitemunkid,name FROM hrtnafdbk_subitem_master WHERE isactive = 1 "

            If intItemUnkid > 0 Then
                strQ &= " AND fdbkitemunkid = '" & intItemUnkid & "' "
            End If

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getComboList; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetFeedbackSubItemUnkid(ByVal StrName As String, ByVal intItemUnkid As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            strQ = "SELECT " & _
                   "  fdbksubitemunkid " & _
                   "FROM hrtnafdbk_subitem_master " & _
                   "WHERE isactive = 1 AND name = @name AND fdbkitemunkid = '" & intItemUnkid & "' "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("fdbksubitemunkid")
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFeedbackSubItemUnkid; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This feedback subitem code already exists. Please define new subitem code.")
            Language.setMessage(mstrModuleName, 2, "This feedback subitem name already exists. Please define new subitem name.")
            Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this feedback subitem. Reason : This feedback subitem is already linked with some transaction.")
            Language.setMessage(mstrModuleName, 4, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
