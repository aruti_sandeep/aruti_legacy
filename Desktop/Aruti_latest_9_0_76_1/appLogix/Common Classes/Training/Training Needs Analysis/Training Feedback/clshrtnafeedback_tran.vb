﻿'************************************************************************************************************************************
'Class Name : clshrtnafeedback_master.vb
'Purpose    :
'Date       :23/02/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib

''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clshrtnafeedback_tran
    Private Const mstrModuleName = "clshrtnafeedback_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintFeedbackmasterunkid As Integer
    Private mintFeedbackTranunkid As Integer
    Private mdtTran As DataTable

#End Region

#Region " Properties "

    Public Property _Feedbackmasterunkid() As Integer
        Get
            Return mintFeedbackmasterunkid
        End Get
        Set(ByVal value As Integer)
            mintFeedbackmasterunkid = value
            Call Feedback_Tran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public Property _Message() As String
        Get
            Return mstrMessage
        End Get
        Set(ByVal value As String)
            mstrMessage = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("FeedBack")

            mdtTran.Columns.Add("feedbacktranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("feedbackmasterunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("fdbkgroupunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("fdbkitemunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("fdbksubitemunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("resultunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("other_result", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("feedback_group", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("feedback_item", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("feedback_subitem", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("feedback_result", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private/Public Function "

    Private Sub Feedback_Tran()
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT " & _
                      " feedbacktranunkid " & _
                      ",hrtnafeedback_tran.feedbackmasterunkid " & _
                      ",hrtnafeedback_tran.fdbkgroupunkid " & _
                      ",hrtnafeedback_tran.fdbkitemunkid " & _
                      ",hrtnafeedback_tran.fdbksubitemunkid " & _
                      ",hrtnafeedback_tran.resultunkid " & _
                      ",hrtnafeedback_tran.other_result " & _
                      ",hrtnafeedback_tran.isvoid " & _
                      ",hrtnafeedback_tran.voiduserunkid " & _
                      ",hrtnafeedback_tran.voiddatetime " & _
                      ",hrtnafeedback_tran.voidreason " & _
                      ",hrtnafdbk_group_master.name AS feedback_group " & _
                      ",hrtnafdbk_item_master.name AS feedback_item " & _
                      ",ISNULL(hrtnafdbk_subitem_master.name,'') AS feedback_subitem " & _
                      ",ISNULL(hrresult_master.resultname,'') AS feedback_result " & _
                      ",'' AS AUD " & _
                    "FROM hrtnafeedback_tran " & _
                      "LEFT JOIN hrresult_master ON hrtnafeedback_tran.resultunkid = hrresult_master.resultunkid " & _
                      "JOIN hrtnafdbk_group_master ON hrtnafeedback_tran.fdbkgroupunkid = hrtnafdbk_group_master.fdbkgroupunkid " & _
                      "LEFT JOIN hrtnafdbk_subitem_master ON hrtnafeedback_tran.fdbksubitemunkid = hrtnafdbk_subitem_master.fdbksubitemunkid " & _
                      "JOIN hrtnafdbk_item_master ON hrtnafeedback_tran.fdbkitemunkid = hrtnafdbk_item_master.fdbkitemunkid AND hrtnafdbk_item_master.fromimpact = 0 " & _
                    "WHERE ISNULL(hrtnafeedback_tran.isvoid,0) = 0 AND feedbackmasterunkid = '" & mintFeedbackmasterunkid & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            mdtTran.Rows.Clear()
            For Each dRow As DataRow In dsList.Tables("List").Rows
                mdtTran.ImportRow(dRow)
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Feedback_Tran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDeleteFeedback(ByVal objDataOperation As clsDataOperation _
                                               , Optional ByVal intUserUnkId As Integer = 0) As Boolean 'Sohail (23 Apr 2012) - [intUserUnkId]
        Dim i As Integer
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO hrtnafeedback_tran ( " & _
                                            "  feedbackmasterunkid " & _
                                            ", fdbkgroupunkid " & _
                                            ", fdbkitemunkid " & _
                                            ", fdbksubitemunkid " & _
                                            ", resultunkid " & _
                                            ", other_result " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason" & _
                                        ") VALUES (" & _
                                            "  @feedbackmasterunkid " & _
                                            ", @fdbkgroupunkid " & _
                                            ", @fdbkitemunkid " & _
                                            ", @fdbksubitemunkid " & _
                                            ", @resultunkid " & _
                                            ", @other_result " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason" & _
                                          "); SELECT @@identity"

                                objDataOperation.AddParameter("@feedbackmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFeedbackmasterunkid)
                                objDataOperation.AddParameter("@fdbkgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("fdbkgroupunkid").ToString)
                                objDataOperation.AddParameter("@fdbkitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("fdbkitemunkid").ToString)
                                objDataOperation.AddParameter("@fdbksubitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("fdbksubitemunkid").ToString)
                                objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                objDataOperation.AddParameter("@other_result", SqlDbType.Text, eZeeDataType.NAME_SIZE, .Item("other_result").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                dsList = objDataOperation.ExecQuery(strQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintFeedbackTranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("feedbackmasterunkid") > 0 Then
                                    'Sohail (23 Apr 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnafeedback_master", "feedbackmasterunkid", .Item("feedbackmasterunkid"), "hrtnafeedback_tran", "feedbacktranunkid", mintFeedbackTranunkid, 2, 1, , intUserUnkId) = False Then
                                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnafeedback_master", "feedbackmasterunkid", .Item("feedbackmasterunkid"), "hrtnafeedback_tran", "feedbacktranunkid", mintFeedbackTranunkid, 2, 1) = False Then
                                        'Sohail (23 Apr 2012) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    'Sohail (23 Apr 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnafeedback_master", "feedbackmasterunkid", mintFeedbackmasterunkid, "hrtnafeedback_tran", "feedbacktranunkid", mintFeedbackTranunkid, 1, 1, , intUserUnkId) = False Then
                                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnafeedback_master", "feedbackmasterunkid", mintFeedbackmasterunkid, "hrtnafeedback_tran", "feedbacktranunkid", mintFeedbackTranunkid, 1, 1) = False Then
                                        'Sohail (23 Apr 2012) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                            Case "U"
                                strQ = "UPDATE hrtnafeedback_tran SET " & _
                                        "  feedbackmasterunkid = @feedbackmasterunkid" & _
                                        ", fdbkgroupunkid = @fdbkgroupunkid" & _
                                        ", fdbkitemunkid = @fdbkitemunkid" & _
                                        ", fdbksubitemunkid = @fdbksubitemunkid" & _
                                        ", resultunkid = @resultunkid" & _
                                        ", other_result = @other_result" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                      "WHERE feedbacktranunkid = @feedbacktranunkid "

                                objDataOperation.AddParameter("@feedbacktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("feedbacktranunkid").ToString)
                                objDataOperation.AddParameter("@feedbackmasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("feedbackmasterunkid").ToString)
                                objDataOperation.AddParameter("@fdbkgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("fdbkgroupunkid").ToString)
                                objDataOperation.AddParameter("@fdbkitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("fdbkitemunkid").ToString)
                                objDataOperation.AddParameter("@fdbksubitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("fdbksubitemunkid").ToString)
                                objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                objDataOperation.AddParameter("@other_result", SqlDbType.Text, eZeeDataType.NAME_SIZE, .Item("other_result").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)


                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'Sohail (23 Apr 2012) -- Start
                                'TRA - ENHANCEMENT
                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnafeedback_master", "feedbackmasterunkid", .Item("feedbackmasterunkid"), "hrtnafeedback_tran", "feedbacktranunkid", .Item("feedbacktranunkid"), 2, 2, , intUserUnkId) = False Then
                                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnafeedback_master", "feedbackmasterunkid", .Item("feedbackmasterunkid"), "hrtnafeedback_tran", "feedbacktranunkid", .Item("feedbacktranunkid"), 2, 2) = False Then
                                    'Sohail (23 Apr 2012) -- End
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                            Case "D"
                                If CInt(.Item("feedbacktranunkid")) > 0 Then
                                    'Sohail (23 Apr 2012) -- Start
                                    'TRA - ENHANCEMENT
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnafeedback_master", "feedbackmasterunkid", .Item("feedbackmasterunkid"), "hrtnafeedback_tran", "feedbacktranunkid", .Item("feedbacktranunkid"), 2, 3, , intUserUnkId) = False Then
                                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnafeedback_master", "feedbackmasterunkid", .Item("feedbackmasterunkid"), "hrtnafeedback_tran", "feedbacktranunkid", .Item("feedbacktranunkid"), 2, 3) = False Then
                                        'Sohail (23 Apr 2012) -- End
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                strQ = "UPDATE hrtnafeedback_tran SET " & _
                                       "  isvoid = @isvoid" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voidreason = @voidreason " & _
                                     "WHERE feedbacktranunkid = @feedbacktranunkid "

                                objDataOperation.AddParameter("@feedbacktranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("feedbacktranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                        End Select
                    End If
                End With
            Next

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDeleteFeedback; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsExists(ByVal inttrainingschduleunkid As Integer, _
                             ByVal intemployeeunkid As Integer, _
                             ByVal intfdbkgroupunkid As Integer, _
                             ByVal intfdbkitemunkid As Integer, _
                             Optional ByVal intfdbksubitemunkid As Integer = -1) As Boolean
        Dim StrQ As String = String.Empty
        Dim objDataoperation As New clsDataOperation
        Dim dsList As New DataSet
        Dim blnFlag As Boolean = False
        Try
            StrQ = "SELECT " & _
                   "    feedbacktranunkid " & _
                   "FROM hrtnafeedback_tran " & _
                   "    JOIN hrtnafeedback_master ON hrtnafeedback_tran.feedbackmasterunkid = hrtnafeedback_master.feedbackmasterunkid " & _
                   "WHERE hrtnafeedback_master.isvoid = 0 AND hrtnafeedback_tran.isvoid = 0 " & _
                   "    AND trainingschedulingunkid = '" & inttrainingschduleunkid & "' AND employeeunkid = '" & intemployeeunkid & "' AND fdbkgroupunkid = '" & intfdbkgroupunkid & "' AND fdbkitemunkid = '" & intfdbkitemunkid & "'"
            If intfdbksubitemunkid > 0 Then
                StrQ &= " AND fdbksubitemunkid ='" & intfdbksubitemunkid & "' "
            End If

            dsList = objDataoperation.ExecQuery(StrQ, "List")

            If objDataoperation.ErrorMessage <> "" Then
                Throw New Exception(objDataoperation.ErrorNumber & " : " & objDataoperation.ErrorMessage)
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                blnFlag = True
            Else
                blnFlag = False
            End If

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsExists; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class
