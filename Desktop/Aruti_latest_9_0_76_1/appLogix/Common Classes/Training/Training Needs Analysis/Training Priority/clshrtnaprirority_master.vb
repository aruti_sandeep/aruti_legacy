﻿'************************************************************************************************************************************
'Class Name : clshrtnaprirority_master.vb
'Purpose    :
'Date       :14/02/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clshrtnaprirority_master
    Private Shared ReadOnly mstrModuleName As String = "clshrtnaprirority_master"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintPrioritymasterunkid As Integer
    Private mintPeriodunkid As Integer
    Private mstrReferenceno As String = String.Empty
    Private mstrRemark As String = String.Empty
    Private mblnIsfinal As Boolean
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set prioritymasterunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Prioritymasterunkid() As Integer
        Get
            Return mintPrioritymasterunkid
        End Get
        Set(ByVal value As Integer)
            mintPrioritymasterunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referenceno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Referenceno() As String
        Get
            Return mstrReferenceno
        End Get
        Set(ByVal value As String)
            mstrReferenceno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

#End Region


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  prioritymasterunkid " & _
              ", periodunkid " & _
              ", referenceno " & _
              ", remark " & _
              ", isfinal " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrtnaprirority_master " & _
             "WHERE prioritymasterunkid = @prioritymasterunkid "

            objDataOperation.AddParameter("@prioritymasterunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintPrioritymasterUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintprioritymasterunkid = CInt(dtRow.Item("prioritymasterunkid"))
                mintperiodunkid = CInt(dtRow.Item("periodunkid"))
                mstrreferenceno = dtRow.Item("referenceno").ToString
                mstrremark = dtRow.Item("remark").ToString
                mblnisfinal = CBool(dtRow.Item("isfinal"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  hrtnaprirority_master.prioritymasterunkid " & _
                      ", hrtnaprirority_master.periodunkid " & _
                      ", hrtnaprirority_master.referenceno " & _
                      ", hrtnaprirority_master.remark " & _
                      ", hrtnaprirority_master.isfinal " & _
                      ", hrtnaprirority_master.userunkid " & _
                      ", hrtnaprirority_master.isvoid " & _
                      ", hrtnaprirority_master.voiduserunkid " & _
                      ", hrtnaprirority_master.voiddatetime " & _
                      ", hrtnaprirority_master.voidreason " & _
                      ", ISNULL(cfcommon_period_tran.period_name,'') AS PName " & _
                      ", CASE WHEN isfinal = 0 THEN @Planned " & _
                      "         WHEN isfinal = 1 THEN @Approved END AS Status " & _
                    "FROM hrtnaprirority_master " & _
                    " LEFT JOIN cfcommon_period_tran ON hrtnaprirority_master.periodunkid = cfcommon_period_tran.periodunkid "

            If blnOnlyActive Then
                strQ &= " WHERE isvoid = 0 "
            End If

            objDataOperation.AddParameter("@Planned", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Planned"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Approved"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrtnaprirority_master) </purpose>
    Public Function Insert(ByVal dtTable As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@referenceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReferenceno.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO hrtnaprirority_master ( " & _
              "  periodunkid " & _
              ", referenceno " & _
              ", remark " & _
              ", isfinal " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason" & _
            ") VALUES (" & _
              "  @periodunkid " & _
              ", @referenceno " & _
              ", @remark " & _
              ", @isfinal " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintPrioritymasterunkid = dsList.Tables(0).Rows(0).Item(0)

            Dim objTnAPriority As New clshrtnacoursepriority_tran

            objTnAPriority._TnAPriorityUnkid = mintPrioritymasterunkid
            objTnAPriority._DataTable = dtTable

            If objTnAPriority.InsertUpdate_Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrtnaprirority_master) </purpose>
    Public Function Update(ByVal dtTable As DataTable) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@prioritymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPrioritymasterunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@referenceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReferenceno.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE hrtnaprirority_master SET " & _
                      "  periodunkid = @periodunkid" & _
                      ", referenceno = @referenceno" & _
                      ", remark = @remark" & _
                      ", isfinal = @isfinal" & _
                      ", userunkid = @userunkid" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
                    "WHERE prioritymasterunkid = @prioritymasterunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim objTnAPriority As New clshrtnacoursepriority_tran

            objTnAPriority._TnAPriorityUnkid = mintPrioritymasterunkid
            objTnAPriority._DataTable = dtTable
            objTnAPriority._UserUnkid = mintUserunkid

            If objTnAPriority.InsertUpdate_Delete(objDataOperation) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrtnaprirority_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrtnaprirority_master SET " & _
                    " isvoid = @isvoid" & _
                    ",voiduserunkid = @voiduserunkid" & _
                    ",voiddatetime = @voiddatetime" & _
                    ",voidreason = @voidreason " & _
                   "WHERE prioritymasterunkid = @prioritymasterunkid "

            objDataOperation.AddParameter("@prioritymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dsSubParent As New DataSet
            dsSubParent = clsCommonATLog.GetChildList(objDataOperation, "hrtnacoursepriority_tran", "prioritymasterunkid", intUnkid)
            If dsSubParent.Tables(0).Rows.Count > 0 Then
                Dim dsChild As New DataSet
                For Each dRow As DataRow In dsSubParent.Tables(0).Rows
                    dsChild = clsCommonATLog.GetChildList(objDataOperation, "hrtnaemp_course_tran", "coursepriotranunkid", dRow("coursepriotranunkid"))
                    For Each dCRow As DataRow In dsChild.Tables(0).Rows

                        objDataOperation.ClearParameters()
                        objDataOperation.AddParameter("@prioritymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                        objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                        objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                        objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                        objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                        strQ = " UPDATE hrtnaemp_course_tran SET " & _
                               "  isvoid = @isvoid" & _
                               ", voiduserunkid = @voiduserunkid" & _
                               ", voiddatetime = @voiddatetime" & _
                               ", voidreason = @voidreason " & _
                               " WHERE courseemptranunkid = '" & dCRow("courseemptranunkid") & "'"


                        Call objDataOperation.ExecNonQuery(strQ)

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If



                        'Pinkal (24-Jul-2012) -- Start
                        'Enhancement : TRA Changes

                        'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", intUnkid, "hrtnaemp_course_tran", "courseemptranunkid", dRow("coursepriotranunkid").ToString, 3, 3) = False Then
                        '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        '    Throw exForce
                        'End If

                        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", intUnkid, "hrtnaemp_course_tran", "courseemptranunkid", dCRow("courseemptranunkid").ToString, 3, 3) = False Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        'Pinkal (24-Jul-2012) -- End
                    Next

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@prioritymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                    strQ = "UPDATE hrtnacoursepriority_tran SET " & _
                               " isvoid = @isvoid" & _
                               ", voiduserunkid = @voiduserunkid" & _
                               ", voiddatetime = @voiddatetime" & _
                               ", voidreason = @voidreason " & _
                           "WHERE coursepriotranunkid = '" & dRow.Item("coursepriotranunkid") & "'"

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", intUnkid, "hrtnacoursepriority_tran", "coursepriotranunkid", dRow.Item("coursepriotranunkid"), 3, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            Else
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrtnaprirority_master", "prioritymasterunkid", intUnkid, "", "", -1, 3, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@prioritymasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(ByVal strListName As String, Optional ByVal mblFlag As Boolean = False, Optional ByVal blnFinal As Boolean = True) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as prioritymasterunkid, ' ' +  @name as name, 0 as periodunkid   UNION "
            End If
            strQ &= "SELECT prioritymasterunkid, referenceno as name,periodunkid FROM hrtnaprirority_master where isvoid = 0  "

            If blnFinal Then
                strQ &= " AND isfinal = 1  "
            End If

            strQ &= " ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Approved")
			Language.setMessage(mstrModuleName, 3, "Planned")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class