﻿'************************************************************************************************************************************
'Class Name : clseval_grid_columns_master.vb
'Purpose    :
'Date       : 20-11-2023
'Written By : Hemant Morker
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System

''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clseval_grid_columns_master
    Private Shared ReadOnly mstrModuleName As String = "clseval_grid_columns_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintGridColumnsid As Integer
    Private mintQuestionnaireid As Integer
    Private mstrGridColumnsName As String = String.Empty
    Private mintAnswertype As Integer
    Private mblnIsForPreTrainingFeedback As Boolean
    Private mblnIsForPostTrainingFeedback As Boolean
    Private mblnIsForLineManagerFeedback As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintCompanyUnkid As Integer = 0
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    Private mstrDatabaseName As String = ""

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gridcolumnsid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _GridColumnsid() As Integer
        Get
            Return mintGridColumnsid
        End Get
        Set(ByVal value As Integer)
            mintGridColumnsid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set questionnaireId
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Questionnaireid() As Integer
        Get
            Return mintQuestionnaireid
        End Get
        Set(ByVal value As Integer)
            mintQuestionnaireid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set gridcolumnsname
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _GridColumnsName() As String
        Get
            Return mstrGridColumnsName
        End Get
        Set(ByVal value As String)
            mstrGridColumnsName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set answertype
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Answertype() As Integer
        Get
            Return mintAnswertype
        End Get
        Set(ByVal value As Integer)
            mintAnswertype = value
        End Set
    End Property

    Public Property _IsForPreTrainingFeedback() As Boolean
        Get
            Return mblnIsForPreTrainingFeedback
        End Get
        Set(ByVal value As Boolean)
            mblnIsForPreTrainingFeedback = value
        End Set
    End Property

    Public Property _IsForPostTrainingFeedback() As Boolean
        Get
            Return mblnIsForPostTrainingFeedback
        End Get
        Set(ByVal value As Boolean)
            mblnIsForPostTrainingFeedback = value
        End Set
    End Property

    Public Property _IsForLineManagerFeedback() As Boolean
        Get
            Return mblnIsForLineManagerFeedback
        End Get
        Set(ByVal value As Boolean)
            mblnIsForLineManagerFeedback = value
        End Set
    End Property
    

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property


    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property

    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  gridcolumnsid " & _
              ", gridcolumnsname " & _
              ", questionnaireId " & _
              ", answertype " & _
              ", ISNULL(isforpretraining,0) AS isforpretraining " & _
              ", ISNULL(isforposttraining,0) AS isforposttraining " & _
              ", ISNULL(isforlinemanager,0) AS isforlinemanager " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM treval_grid_columns_master " & _
             "WHERE gridcolumnsid = @gridcolumnsid "

            objDataOperation.AddParameter("@gridcolumnsid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGridColumnsid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintGridColumnsid = CInt(dtRow.Item("gridcolumnsid"))
                mstrGridColumnsName = dtRow.Item("gridcolumnsname").ToString
                mintQuestionnaireid = CInt(dtRow.Item("questionnaireId"))
                mintAnswertype = CInt(dtRow.Item("answertype"))
                mblnIsForPreTrainingFeedback = CBool(dtRow.Item("isforpretraining"))
                mblnIsForPostTrainingFeedback = CBool(dtRow.Item("isforposttraining"))
                mblnIsForLineManagerFeedback = CBool(dtRow.Item("isforlinemanager"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If Not IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString

                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String _
                            , Optional ByVal intQuestionaireId As Integer = -1 _
                            , Optional ByVal blnOnlyActive As Boolean = True _
                            , Optional ByVal blnForPreTrainingFeedback As Boolean = False _
                            , Optional ByVal blnForPostTrainingFeedback As Boolean = False _
                            , Optional ByVal blnForLineManagerFeedback As Boolean = False _
                            , Optional ByVal blnSelect As Boolean = True _
                            ) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnSelect Then
                strQ = "SELECT 0 AS gridcolumnsid " & _
                       ",       @Select AS gridcolumnsname " & _
                       ",       0 AS questionnaireId " & _
                       ",       '' AS answertype " & _
                       ",       0 AS isforpretraining " & _
                       ",       0 AS isforposttraining " & _
                       ",       0 AS isforlinemanager " & _
                       ",       0 AS isvoid " & _
                       ",       '' AS answertypeval " & _
                       " UNION ALL "
            End If

            strQ &= "SELECT " & _
                   "  gridcolumnsid " & _
                   ", gridcolumnsname " & _
                   ", questionnaireId " & _
                   ", answertype " & _
                   ", ISNULL(isforpretraining,0) AS isforpretraining " & _
                   ", ISNULL(isforposttraining,0) AS isforposttraining " & _
                   ", ISNULL(isforlinemanager,0) AS isforlinemanager " & _
                   ", treval_grid_columns_master.isvoid " & _
                   ", CASE " & _
                          "WHEN answertype = " & clseval_question_master.enAnswerType.FREETEXT & " THEN @FREETEXT " & _
                          "WHEN answertype = " & clseval_question_master.enAnswerType.NUMERIC & " THEN @NUMERIC " & _
                          "WHEN answertype = " & clseval_question_master.enAnswerType.PICKDATE & " THEN @PICKDATE " & _
                          "WHEN answertype = " & clseval_question_master.enAnswerType.RATING & " THEN @RATING " & _
                          "WHEN answertype = " & clseval_question_master.enAnswerType.SELECTION & " THEN @SELECTION " & _
                          "WHEN answertype = " & clseval_question_master.enAnswerType.GRID & " THEN @GRID " & _
                   " END AS answertypeval " & _
                "FROM treval_grid_columns_master " & _
                " WHERE 1=1 "

            objDataOperation.ClearParameters()

            If blnOnlyActive Then
                strQ &= " and treval_grid_columns_master.isvoid = 0 "
            End If

            If intQuestionaireId > -1 Then
                strQ &= " AND questionnaireid = @questionnaireid "
                objDataOperation.AddParameter("@questionnaireid", SqlDbType.Int, eZeeDataType.INT_SIZE, intQuestionaireId)
            End If


            If blnForPreTrainingFeedback = True Then
                strQ &= " and isforpretraining = 1 "
            End If

            If blnForPostTrainingFeedback = True Then
                strQ &= " and isforposttraining = 1 "
            End If

            If blnForLineManagerFeedback = True Then
                strQ &= " and isforlinemanager = 1 "
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@FREETEXT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clseval_question_master", 3, "Free Text"))
            objDataOperation.AddParameter("@NUMERIC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clseval_question_master", 4, "Numeric"))
            objDataOperation.AddParameter("@PICKDATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clseval_question_master", 5, "Date"))
            objDataOperation.AddParameter("@RATING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clseval_question_master", 6, "Rating"))
            objDataOperation.AddParameter("@SELECTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clseval_question_master", 7, "Selection"))
            objDataOperation.AddParameter("@GRID", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clseval_question_master", 8, "Grid"))


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (treval_grid_columns_master) </purpose>
    Public Function Insert(Optional ByVal xDataOpr As clsDataOperation = Nothing, Optional ByRef intNewUnkId As Integer = 0) As Boolean
        If isExist(mstrGridColumnsName, mintQuestionnaireid, -1, xDataOpr) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, This Column Name is already defined. Please define new Column Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@gridcolumnsname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGridColumnsName.ToString)
            objDataOperation.AddParameter("@questionnaireId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireid.ToString)
            objDataOperation.AddParameter("@answertype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnswertype.ToString)
            objDataOperation.AddParameter("@isforpretraining", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForPreTrainingFeedback.ToString)
            objDataOperation.AddParameter("@isforposttraining", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForPostTrainingFeedback.ToString)
            objDataOperation.AddParameter("@isforlinemanager", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForLineManagerFeedback.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)


            strQ = "INSERT INTO treval_grid_columns_master ( " & _
              "  gridcolumnsname " & _
              ", questionnaireId " & _
              ", answertype " & _
              ", isforpretraining " & _
              ", isforposttraining " & _
              ", isforlinemanager " & _
              ", isvoid" & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
            ") VALUES (" & _
              "  @gridcolumnsname " & _
              ", @questionnaireId " & _
              ", @answertype " & _
              ", @isforpretraining " & _
              ", @isforposttraining " & _
              ", @isforlinemanager " & _
              ", @isvoid" & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintGridColumnsid = dsList.Tables(0).Rows(0).Item(0)

            intNewUnkId = mintGridColumnsid

            If InsertAuditTrails(objDataOperation, enAuditType.ADD, mintGridColumnsid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function SaveAllGridColumn(Optional ByVal clsEvalQuestionMaster As clseval_question_master = Nothing _
                           , Optional ByVal xDataOp As clsDataOperation = Nothing _
                           ) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If
        objDataOperation.ClearParameters()

        Try

            If mintQuestionnaireid <= 0 AndAlso clsEvalQuestionMaster IsNot Nothing Then
                Dim intNewUnkId As Integer = 0
                If clsEvalQuestionMaster.Insert(objDataOperation, intNewUnkId) = False Then
                    Return False
                Else
                    mintQuestionnaireid = intNewUnkId
                End If
            ElseIf mintQuestionnaireid > 0 AndAlso clsEvalQuestionMaster IsNot Nothing Then
                If clsEvalQuestionMaster.Update(objDataOperation) = False Then
                    Return False
                End If
            End If


            If mintGridColumnsid <= 0 Then
                If Insert(objDataOperation) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Exit Function
                End If
            Else
                If Update(objDataOperation) = False Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    Exit Function
                End If
            End If


            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: SaveAllGridColumn; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (treval_grid_columns_master) </purpose>
    Public Function Update(Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        If isExist(mstrGridColumnsName, mintQuestionnaireid, mintGridColumnsid, xDataOpr) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, This Column Name is already defined. Please define new Column Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()


        Try
            objDataOperation.AddParameter("@gridcolumnsid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGridColumnsid.ToString)
            objDataOperation.AddParameter("@gridcolumnsname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGridColumnsName.ToString)
            objDataOperation.AddParameter("@questionnaireId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintQuestionnaireid.ToString)
            objDataOperation.AddParameter("@answertype", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnswertype.ToString)
            objDataOperation.AddParameter("@isforpretraining", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForPreTrainingFeedback.ToString)
            objDataOperation.AddParameter("@isforposttraining", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForPostTrainingFeedback.ToString)
            objDataOperation.AddParameter("@isforlinemanager", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsForLineManagerFeedback.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, mstrVoidreason.Trim.Length, mstrVoidreason.ToString)



            strQ = "UPDATE treval_grid_columns_master SET " & _
                      "  gridcolumnsname = @gridcolumnsname " & _
                      ", questionnaireId = @questionnaireId " & _
                      ", answertype = @answertype " & _
                      ", isforpretraining = @isforpretraining " & _
                      ", isforposttraining = @isforposttraining " & _
                      ", isforlinemanager = @isforlinemanager " & _
                      ", isvoid = @isvoid " & _
                      ", voiduserunkid = @voiduserunkid " & _
                      ", voiddatetime = @voiddatetime " & _
                      ", voidreason = @voidreason " & _
              " WHERE gridcolumnsid = @gridcolumnsid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If IsTableDataUpdate(mintGridColumnsid, objDataOperation) = False Then
                If InsertAuditTrails(objDataOperation, enAuditType.EDIT, mintGridColumnsid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)


            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (treval_grid_columns_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If
        Dim objEval As New clseval_answer_master
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr IsNot Nothing Then
            objDataOperation = xDataOpr
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        Try
            With objEval
                ._Isvoid = mblnIsvoid
                ._Voiduserunkid = mintVoiduserunkid
                ._Voiddatetime = mdtVoiddatetime
                ._Voidreason = mstrVoidreason
                ._IsWeb = mblnIsWeb
                ._AuditUserId = mintAuditUserId
                ._ClientIP = mstrClientIP
                ._FormName = mstrFormName
                ._HostName = mstrHostName
                .VoidAllByMasterUnkID(intUnkid, 3, objDataOperation, -1)
            End With

            strQ = "UPDATE treval_grid_columns_master SET " & _
              " isvoid = @isvoid " & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason " & _
            "WHERE gridcolumnsid = @gridcolumnsid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@gridcolumnsid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE, intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            Dim Tables() As String = {"trtraining_evaluation_tran", "treval_answer_master"}

            objDataOperation.AddParameter("@gridcolumnsid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            For Each value As String In Tables
                Select Case value

                    Case "trtraining_evaluation_tran", "treval_answer_master"
                        strQ = "SELECT gridcolumnsid FROM " & value & " WHERE isvoid = 0 AND gridcolumnsid = @gridcolumnsid  "


                    Case Else
                End Select

                If strQ.Trim.Length > 0 Then

                    dsList = objDataOperation.ExecQuery(strQ, "Used")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables("Used").Rows.Count > 0 Then
                        Return True
                        Exit For
                    End If
                End If
            Next
            Return False
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, ByVal intQuestionNaireId As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOpr As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  gridcolumnsname " & _
              ", questionnaireId " & _
             "FROM treval_grid_columns_master " & _
             "WHERE  isvoid = 0 AND gridcolumnsname = @gridcolumnsname " & _
             " AND questionnaireId = @questionnaireId  "

            If intUnkid > 0 Then
                strQ &= " AND gridcolumnsid <> @gridcolumnsid"
            End If

            objDataOperation.AddParameter("@gridcolumnsname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@questionnaireId", SqlDbType.Int, eZeeDataType.INT_SIZE, intQuestionNaireId)
            objDataOperation.AddParameter("@gridcolumnsid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType, ByVal intUnkid As Integer) As Boolean
        Dim StrQ As String = ""
        Try

            StrQ = "INSERT INTO attreval_grid_columns_master ( " & _
                    "  tranguid " & _
                    ", gridcolumnsid " & _
                    ", gridcolumnsname " & _
                    ", questionnaireId " & _
                    ", answertype " & _
                    ", isforpretraining " & _
                    ", isforposttraining " & _
                    ", isforlinemanager " & _
                    ", audittype " & _
                    ", audituserunkid " & _
                    ", auditdatetime " & _
                    ", form_name " & _
                    ", ip " & _
                    ", hostname " & _
                    ", isweb" & _
                  ") select " & _
                    "  LOWER(NEWID()) " & _
                    ", gridcolumnsid " & _
                    ", gridcolumnsname " & _
                    ", questionnaireId " & _
                    ", answertype " & _
                    ", isforpretraining " & _
                    ", isforposttraining " & _
                    ", isforlinemanager " & _
                    ", @audittype " & _
                    ", @audituserunkid " & _
                    ", GETDATE() " & _
                    ", @form_name " & _
                    ", @ip " & _
                    ", @hostname " & _
                    ", @isweb" & _
                    "  From treval_grid_columns_master WHERE 1=1 AND treval_grid_columns_master.gridcolumnsid = @gridcolumnsid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@gridcolumnsid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@hostname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsTableDataUpdate(ByVal unkid As Integer, Optional ByVal objDooperation As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            If objDooperation Is Nothing Then
                objDataOperation = New clsDataOperation
            Else
                objDataOperation = objDooperation
            End If

            strQ = "SELECT TOP 1 * FROM attreval_grid_columns_master WHERE gridcolumnsid = @gridcolumnsid AND audittype <> 3 ORDER BY auditdatetime DESC"

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@gridcolumnsid", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, unkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dr As DataRow In dsList.Tables(0).Rows

                If dr("gridcolumnsid").ToString() = mintGridColumnsid _
                    AndAlso dr("gridcolumnsname").ToString() = mstrGridColumnsName AndAlso dr("questionnaireId").ToString() = mintQuestionnaireid _
                    AndAlso dr("answertype").ToString() = mintAnswertype AndAlso CBool(dr("isforpretraining")) = mblnIsForPreTrainingFeedback _
                    AndAlso CBool(dr("isforposttraining")) = mblnIsForPostTrainingFeedback AndAlso CBool(dr("isforlinemanager")) = mblnIsForLineManagerFeedback _
                    Then

                    Return True
                Else
                    Return False
                End If
            Next
            Return False

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsTableDataUpdate; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If objDooperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Function
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
            Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Sorry, This Column Name is already defined. Please define new Column Name.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
