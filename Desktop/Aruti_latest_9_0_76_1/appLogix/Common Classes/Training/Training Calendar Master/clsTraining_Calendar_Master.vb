﻿'************************************************************************************************************************************
'Class Name : clsTraining_Calendar_Master.vb
'Purpose    :
'Date       :01-Feb-2021
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Hemant
''' </summary>
Public Class clsTraining_Calendar_Master
    Private Shared ReadOnly mstrModuleName As String = "clsTraining_Calendar_Master"
    Dim mstrMessage As String = ""
    'Hemant (19 Jul 2024) -- Start
    'ENHANCEMENT(NMB): A1X - 2376 :  Training backlog rollover to the new year upon closing of the training calendar year (for trainings planned for the entire company)
    Dim objDataOperation As clsDataOperation
    'Hemant (19 Jul 2024) -- End


#Region " Private variables "

    Private mintCalendarunkid As Integer
    Private mstrCalendarName As String = String.Empty
    Private mdtStartDate As Date
    Private mdtEndDate As Date
    Private mstrDescription As String = String.Empty
    Private mintStatusunkid As Integer
    Private mblnIsactive As Boolean = True
    Private mstrHostName As String = ""
    Private mstrClientIP As String = ""
    Private mintAuditUserId As Integer = 0
    Private mblnIsWeb As Boolean = False
    Private mstrFormName As String = ""
    'Hemant (19 Jul 2024) -- Start
    'ENHANCEMENT(NMB): A1X - 2376 :  Training backlog rollover to the new year upon closing of the training calendar year (for trainings planned for the entire company)
    Private mstrDepartmentalTrainingNeedunkIDs As String = ""
    'Hemant (19 Jul 2024) -- End


#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set calendarunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Calendarunkid() As Integer
        Get
            Return mintCalendarunkid
        End Get
        Set(ByVal value As Integer)
            mintCalendarunkid = value
            Call GetData()
        End Set
    End Property

    'Hemant (19 Jul 2024) -- Start
    'ENHANCEMENT(NMB): A1X - 2376 :  Training backlog rollover to the new year upon closing of the training calendar year (for trainings planned for the entire company)
    Private mintNextCalendarunkid As Integer
    Public Property _NextCalendarunkid() As Integer
        Get
            Return mintNextCalendarunkid
        End Get
        Set(ByVal value As Integer)
            mintNextCalendarunkid = value
        End Set
    End Property

    Private mdtNextCalendarStartDate As Date
    Public Property _NextCalendarStartDate() As Date
        Get
            Return mdtNextCalendarStartDate
        End Get
        Set(ByVal value As Date)
            mdtNextCalendarStartDate = value
        End Set
    End Property

    Private mdtNextCalendarEndDate As Date
    Public Property _NextCalendarEndDate() As Date
        Get
            Return mdtNextCalendarEndDate
        End Get
        Set(ByVal value As Date)
            mdtNextCalendarEndDate = value
        End Set
    End Property
    'Hemant (19 Jul 2024) -- End


    ''' <summary>
    ''' Purpose: Get or Set calendar_name
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _CalendarName() As String
        Get
            Return mstrCalendarName
        End Get
        Set(ByVal value As String)
            mstrCalendarName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _StartDate() As Date
        Get
            Return mdtStartDate
        End Get
        Set(ByVal value As Date)
            mdtStartDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _EndDate() As Date
        Get
            Return mdtEndDate
        End Get
        Set(ByVal value As Date)
            mdtEndDate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Description
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = value
        End Set
    End Property
   
    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Public WriteOnly Property _FromWeb() As Boolean
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property


#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
              "  calendarunkid " & _
              ", calendar_name " & _
              ", startdate " & _
              ", enddate " & _
              ", description " & _
              ", isactive " & _
              ", statusunkid " & _
                     " FROM trtraining_calendar_master " & _
                     " WHERE calendarunkid = @calendarunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCalendarunkid = CInt(dtRow.Item("calendarunkid"))
                mstrCalendarName = dtRow.Item("calendar_name").ToString
                mdtStartDate = dtRow.Item("startdate")
                mdtEndDate = dtRow.Item("enddate")
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal mstrFilter As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  trtraining_calendar_master.calendarunkid " & _
              ", trtraining_calendar_master.calendar_name " & _
              ", trtraining_calendar_master.startdate " & _
              ", trtraining_calendar_master.enddate " & _
                      ", trtraining_calendar_master.description " & _
              ", trtraining_calendar_master.isactive " & _
              ", trtraining_calendar_master.statusunkid " & _
              ", CONVERT(NVARCHAR(8),trtraining_calendar_master.startdate,112) AS sdate " & _
              ", CONVERT(NVARCHAR(8),trtraining_calendar_master.enddate,112) AS edate " & _
              ", CASE WHEN trtraining_calendar_master.statusunkid = 1 THEN @O ELSE @C END AS status " & _
                      " FROM trtraining_calendar_master " & _
             " WHERE trtraining_calendar_master.isactive = 1 "

            If mstrFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrFilter
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@O", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Open"))
            objDataOperation.AddParameter("@C", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Close"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trtraining_calendar_master) </purpose>
    Public Function Insert() As Boolean

        If isExist(mstrCalendarName, -1) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, This calendar name is already defined. Please define new calendar name.")
            Return False
        End If

        Dim objStage As New clstlstages_master
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@calendar_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCalendarName.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate.ToString)
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)

            strQ = "INSERT INTO trtraining_calendar_master ( " & _
              " calendar_name " & _
              ", startdate " & _
              ", enddate " & _
              ", description " & _
              ", isactive " & _
              ", statusunkid " & _
            ") VALUES (" & _
              " @calendar_name " & _
              ", @startdate " & _
              ", @enddate " & _
              ", @description " & _
              ", @isactive " & _
              ", @statusunkid " & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCalendarunkid = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrails(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
            objStage = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trtraining_calendar_master) </purpose>
    Public Function Update(Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
       
        If isExist(mstrCalendarName, mintCalendarunkid, xDataOp) Then
            'Hemant (19 Jul 2024) -- [xDataOp]
            mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, This calendar name is already defined. Please define new calendar name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Hemant (19 Jul 2024) -- Start
        'ENHANCEMENT(NMB): A1X - 2376 :  Training backlog rollover to the new year upon closing of the training calendar year (for trainings planned for the entire company)
        'Dim objDataOperation As clsDataOperation
        'objDataOperation = New clsDataOperation
        'objDataOperation.BindTransaction()
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        End If
        'Hemant (19 Jul 2024) -- End
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@calendar_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCalendarName.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate.ToString)
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)

            strQ = "UPDATE trtraining_calendar_master SET " & _
              " calendar_name = @calendar_name" & _
              ", startdate = @startdate" & _
              ", enddate = @enddate" & _
              ", description = @description" & _
              ", isactive = @isactive" & _
              ", statusunkid = @statusunkid" & _
                      " WHERE calendarunkid = @calendarunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Hemant (19 Jul 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2376 :  Training backlog rollover to the new year upon closing of the training calendar year (for trainings planned for the entire company)
            'objDataOperation.ReleaseTransaction(True)
            'objDataOperation.ReleaseTransaction(True)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            'Hemant (19 Jul 2024) -- End

            Return True
        Catch ex As Exception
            'Hemant (19 Jul 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2376 :  Training backlog rollover to the new year upon closing of the training calendar year (for trainings planned for the entire company)
            'objDataOperation.ReleaseTransaction(False)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            'Hemant (19 Jul 2024) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (19 Jul 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2376 :  Training backlog rollover to the new year upon closing of the training calendar year (for trainings planned for the entire company)
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (19 Jul 2024) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trtraining_calendar_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this Calendar. Reason : This Calendar is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objStage As New clstlstages_master

        dsList = objStage.GetList("List", intUnkid)

        _Calendarunkid = intUnkid
        mblnIsactive = False

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE trtraining_calendar_master SET isactive = 0 WHERE calendarunkid = @calendarunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrails(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        objDataOperation = New clsDataOperation

        Try

            strQ = "select isnull(calendarunkid,0) FROM hrtraining_approverlevel_master WHERE calendarunkid = @calendarunkid AND isactive = 1 "

            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "select isnull(calendarunkid,0) FROM trtraining_approval_matrix WHERE calendarunkid = @calendarunkid AND isvoid = 0 "
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "select isnull(calendarunkid,0) FROM hrtraining_approver_master WHERE calendarunkid = @calendarunkid AND isvoid = 0 "
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            strQ = "select isnull(periodunkid,0) FROM trdepartmentaltrainingneed_master WHERE periodunkid = @calendarunkid AND isvoid = 0 "
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
                End If

            strQ = "select isnull(periodunkid,0) FROM trtraining_request_master WHERE periodunkid = @calendarunkid AND isvoid = 0 "
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strCalendarName As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Hemant (19 Jul 2024) -- Start
        'ENHANCEMENT(NMB): A1X - 2376 :  Training backlog rollover to the new year upon closing of the training calendar year (for trainings planned for the entire company)
        'Dim objDataOperation As clsDataOperation
        'objDataOperation = New clsDataOperation
        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If
        objDataOperation.ClearParameters()
        'Hemant (19 Jul 2024) -- End


        Try
            strQ = "SELECT " & _
              "  calendarunkid " & _
              ", calendar_name " & _
              ", startdate " & _
              ", enddate " & _
              ", description " & _
              ", isactive " & _
              ", statusunkid " & _
                     " FROM trtraining_calendar_master " & _
                     " WHERE isactive = 1 "

            If strCalendarName.Trim.Length > 0 Then
                strQ &= " AND calendar_name = @calendar_name "
            End If


            If intUnkid > 0 Then
                strQ &= " AND calendarunkid <> @calendarunkid"
            End If

            objDataOperation.AddParameter("@calendar_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCalendarName)
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            'Hemant (19 Jul 2024) -- Start
            'ENHANCEMENT(NMB): A1X - 2376 :  Training backlog rollover to the new year upon closing of the training calendar year (for trainings planned for the entire company)
            'objDataOperation = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
            'Hemant (19 Jul 2024) -- End
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function InsertAuditTrails(ByVal objDataOperation As clsDataOperation, ByVal eAuditType As enAuditType) As Boolean
        Dim StrQ As String = ""
        Try
            StrQ = "INSERT INTO attrtraining_calendar_master ( " & _
                       "  tranguid " & _
                       ", calendarunkid " & _
                       ", calendar_name " & _
                       ", startdate " & _
                       ", enddate " & _
                       ", description " & _
                       ", statusunkid " & _
                       ", audittypeid " & _
                       ", audituserunkid " & _
                       ", auditdatetime " & _
                       ", formname " & _
                       ", ip " & _
                       ", host " & _
                       ", isweb" & _
                   ") VALUES (" & _
                       "  LOWER(NEWID()) " & _
                       ", @calendarunkid " & _
                       ", @calendar_name " & _
                       ", @startdate " & _
                       ", @enddate " & _
                       ", @description " & _
                       ", @statusunkid " & _
                       ", @audittypeid " & _
                       ", @audituserunkid " & _
                       ", GETDATE() " & _
                       ", @formname " & _
                       ", @ip " & _
                       ", @machine_name " & _
                       ", @isweb " & _
                    ") "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            objDataOperation.AddParameter("@calendar_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCalendarName.ToString)
            objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartDate.ToString)
            objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEndDate.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@audittypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, eAuditType)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAuditUserId)
            objDataOperation.AddParameter("@formname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFormName)

            If mstrClientIP.Trim.Length > 0 Then
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrClientIP)
            Else
                objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            End If

            If mstrHostName.Length > 0 Then
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            Else
                objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName())
            End If

            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrails; Module Name: " & mstrModuleName)
            Return False
        Finally
        End Try
    End Function

    Public Function getListForCombo(Optional ByVal strListName As String = "List", _
                                    Optional ByVal mblFlag As Boolean = False, _
                                    Optional ByVal intStatusID As Integer = 0) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            If mblFlag = True Then
                strQ = "SELECT 0 as calendarunkid, ' ' +  @name  as name, '19000101' AS startdate, '19000101' AS enddate UNION "
            End If
            strQ &= "SELECT " & _
                        "  trtraining_calendar_master.calendarunkid " & _
                        ", trtraining_calendar_master.calendar_name as name " & _
                        ", convert(char(8),trtraining_calendar_master.startdate,112) as startdate " & _
                        ", convert(char(8),trtraining_calendar_master.enddate,112) as enddate " & _
                    "FROM trtraining_calendar_master WHERE isactive = 1  "


            If intStatusID > 0 Then
                strQ &= "AND statusunkid = @statusunkid "
                objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If

            strQ &= "ORDER BY enddate "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: getListForCombo; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    Public Function getLastOpenCalendarID(Optional ByVal intStatusID As Integer = 0) As Integer
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Dim intPeriodID As Integer = 0
        Try

            strQ = "SELECT TOP 1 " & _
                        "calendarunkid " & _
                       ",calendar_name " & _
                       ",startdate " & _
                       ",enddate " & _
                       ",description " & _
                       ",statusunkid " & _
                       ",isactive " & _
                    "FROM trtraining_calendar_master " & _
                    "WHERE isactive = 1 "

            If intStatusID > 0 Then
                strQ &= "AND statusunkid = @statusid "
                objDataOperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, intStatusID)
            End If

            strQ &= " ORDER BY enddate DESC "


            dsList = objDataOperation.ExecQuery(strQ, "List")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                intPeriodID = CInt(dsList.Tables("List").Rows(0).Item("calendarunkid").ToString)
            End If

            Return intPeriodID
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getLastOpenCalendarID", mstrModuleName)
            Return intPeriodID
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function

    Public Function GetLastCalendarId(ByVal xCurrentPeriodId As Integer, ByVal dtStartdate As Date) As Integer
        Dim dsList As New DataSet
        Dim mintPreviousPeriodId As Integer = 0
        Dim strQ As String = ""
        Dim exForce As Exception = Nothing
        Try

            Dim objDataOperation As New clsDataOperation

            strQ = " SELECT Top 1 calendarunkid  " & _
                       " FROM trtraining_calendar_master  " & _
                       " WHERE isactive = 1 AND calendarunkid <> @calendarunkid AND CONVERT(CHAR(8),startdate,112) < @startDate " & _
                       " ORDER BY startdate DESC "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@calendarunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xCurrentPeriodId)
            objDataOperation.AddParameter("@startDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtStartdate))
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then
                mintPreviousPeriodId = CInt(dsList.Tables(0).Rows(0)("calendarunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLastCalendarId; Module Name: " & mstrModuleName)
        End Try
        Return mintPreviousPeriodId
    End Function

    'Hemant (18 Jul 2022) -- Start            
    'ENHANCEMENT(NMB) : AC2-722) - Approver import via excel
    Public Function GetCalendarUnkId(ByVal strCalendarName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation = New clsDataOperation
        Try
            strQ = "Select isnull(calendarunkid,0) calendarunkid from trtraining_calendar_master" & _
                       " where 1 = 1 "

            If strCalendarName <> "" Then
                strQ &= " AND calendar_name = @calendar_name"

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@calendar_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCalendarName)
            End If
          
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return CInt(dt("calendarunkid"))
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCalendarUnkId", mstrModuleName)
        End Try
        Return -1
    End Function
    'Hemant (18 Jul 2022) -- End

    'Hemant (19 Jul 2024) -- Start
    'ENHANCEMENT(NMB): A1X - 2376 :  Training backlog rollover to the new year upon closing of the training calendar year (for trainings planned for the entire company)
    Public Function Close_Period(ByVal xDatabaseName As String, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim strQ As String = ""
        Dim dsList As DataSet = Nothing
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            strQ = "SELECT departmentaltrainingneedunkid FROM trdepartmentaltrainingneed_master " & _
                   " JOIN trtraining_calendar_master ON trtraining_calendar_master.calendarunkid = trdepartmentaltrainingneed_master.periodunkid " & _
                   "WHERE periodunkid = @periodunkid " & _
                   " AND ISNULL(isvoid,0) = 0 " & _
                   " AND departmentunkid = -99 " & _
                   " AND trdepartmentaltrainingneed_master.statusunkid NOT IN ( " & clsDepartmentaltrainingneed_master.enApprovalStatus.FinalApproved & " , " & clsDepartmentaltrainingneed_master.enApprovalStatus.Rejected & ")"

            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCalendarunkid.ToString)
            dsList = objDataOperation.ExecQuery(strQ, "TrainingBacklog")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mstrDepartmentalTrainingNeedunkIDs = ""
            For Each dsRow As DataRow In dsList.Tables("TrainingBacklog").Rows
                If mstrDepartmentalTrainingNeedunkIDs.Trim = "" Then
                    mstrDepartmentalTrainingNeedunkIDs = dsRow.Item("departmentaltrainingneedunkid")
                Else
                    mstrDepartmentalTrainingNeedunkIDs &= "," & dsRow.Item("departmentaltrainingneedunkid")
                End If
            Next

            CF_Training_Backlog(xDatabaseName, mstrDepartmentalTrainingNeedunkIDs, objDataOperation)

            Update(objDataOperation)
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Close_Period; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Private Function CF_Training_Backlog(ByVal xDatabaseName As String, ByVal strDepartmentalTrainingNeedunkIDs As String, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnFlag As Boolean = False


        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
        End If

        Try
            Dim arrDepartmentalTrainingNeedunkIDs As String() = strDepartmentalTrainingNeedunkIDs.Split(",")

            For i = 0 To arrDepartmentalTrainingNeedunkIDs.Length - 1
                strQ = ""
                Dim intNewDepartmentalTrainingNeedunkID As Integer = 0
                Dim intDepartmentalTrainingNeedunkID = CInt(arrDepartmentalTrainingNeedunkIDs(i).ToString)

                Dim objDepartmentalTrainingNeedMaster As New clsDepartmentaltrainingneed_master
                objDepartmentalTrainingNeedMaster._Departmentaltrainingneedunkid = intDepartmentalTrainingNeedunkID
                objDepartmentalTrainingNeedMaster._Periodunkid = mintNextCalendarunkid
                objDepartmentalTrainingNeedMaster._Startdate = mdtNextCalendarStartDate
                objDepartmentalTrainingNeedMaster._Enddate = mdtNextCalendarEndDate
                objDepartmentalTrainingNeedMaster._Isweb = mblnIsWeb
                objDepartmentalTrainingNeedMaster._Userunkid = mintAuditUserId
                objDepartmentalTrainingNeedMaster._FormName = mstrFormName
                objDepartmentalTrainingNeedMaster._ClientIP = mstrClientIP
                objDepartmentalTrainingNeedMaster._HostName = mstrHostName
                objDepartmentalTrainingNeedMaster._AuditDate = ConfigParameter._Object._CurrentDateAndTime
                If objDepartmentalTrainingNeedMaster.Insert(objDataOperation, intNewDepartmentalTrainingNeedunkID) = False Then
                    blnFlag = False
                    Exit For
                Else
                    Dim dicTableList As New Dictionary(Of String, String)

                    dicTableList.Add("trdepttrainingneed_employee_tran", "depttrainingneedemployeetranunkid")
                    dicTableList.Add("trdepttrainingneed_allocation_tran", "depttrainingneedallocationtranunkid")
                    dicTableList.Add("trdepttrainingneed_resources_tran", "depttrainingneedresourcestranunkid")
                    dicTableList.Add("trdepttrainingneed_financingsources_tran", "depttrainingneedfinancingsourcestranunkid")
                    dicTableList.Add("trdepttrainingneed_trainingcoordinator_tran", "depttrainingneedtrainingcoordinatortranunkid")
                    dicTableList.Add("trdepttrainingneed_costitem_tran", "depttrainingneedcostitemtranunkid")
                    dicTableList.Add("trdepttrainingneed_traininginstructor_tran", "depttrainingneedtraininginstructortranunkid")

                    For Each Table As KeyValuePair(Of String, String) In dicTableList
                        Dim strColumns As String = ""
                        Dim strFinalColumns As String = ""
                        Dim strTableName As String = Table.Key
                        Dim strTableColId As String = Table.Key & "." & Table.Value
                        strColumns = Get_Column_List(strTableName, True)
                        strFinalColumns = strColumns.Replace(strTableColId & ",", "")
                        If strFinalColumns = "" Then Return False

                        strQ &= "INSERT  INTO " & xDatabaseName & ".." & strTableName & " " & _
                                        "(" & strFinalColumns & ") " & _
                                "SELECT " & _
                                         strFinalColumns.Replace(strTableName & ".departmentaltrainingneedunkid", intNewDepartmentalTrainingNeedunkID) & " " & _
                                "FROM  " & strTableName & " " & _
                                " WHERE departmentaltrainingneedunkid IN (" & intDepartmentalTrainingNeedunkID & ")"

                        
                    Next


                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    blnFlag = True
                End If

            Next

            'strColumns = Get_Column_List(strTableName, True)
            'If strColumns = "" Then Return False

            'strQ = "SET IDENTITY_INSERT " & xDatabaseName & ".." & strTableName & " ON  "
            'strQ &= "INSERT  INTO " & xDatabaseName & ".." & strTableName & " " & _
            '                "(" & strColumns & ") " & _
            '        "SELECT " & _
            '                 strColumns.Replace("trdepartmentaltrainingneed_master.periodunkid", mintNextCalendarunkid).Replace("trdepartmentaltrainingneed_master.startdate", "'" & mdtNextCalendarStartDate.ToString("yyyyMMdd") & "'").Replace("trdepartmentaltrainingneed_master.enddate", "'" & mdtNextCalendarEndDate.ToString("yyyyMMdd") & "'") & " " & _
            '        "FROM  " & strTableName & " " & _
            '        " WHERE departmentaltrainingneedunkid IN (" & strDepartmentalTrainingNeedunkIDs & ")"

            'strQ &= " SET IDENTITY_INSERT " & xDatabaseName & ".." & strTableName & " OFF "

            'Call objDataOperation.ExecNonQuery(strQ)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            If blnFlag = False Then
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Else
                If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            End If


            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            mstrMessage = ex.Message & "; Procedure Name: CF_Training_Approval_Matrix; Module Name: " & mstrModuleName
            Return False
            Throw New Exception(ex.Message & "; Procedure Name: CF_Training_Approval_Matrix; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function Get_Column_List(ByVal strTableName As String, Optional ByVal blnTableNameInColumn As Boolean = False) As String

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strColumn As String = ""
        Dim strColIDENTITY As String = ""

        Try

            objDataOperation = New clsDataOperation


            strQ = "Select * From " & strTableName & " WHERE 1=2"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dCol As DataColumn In dsList.Tables("List").Columns
                If strColIDENTITY = "" OrElse dCol.Caption.ToUpper <> strColIDENTITY.ToUpper Then
                    If strColumn = "" Then
                        If blnTableNameInColumn = True Then
                            strColumn &= strTableName & "." & dCol.Caption
                        Else
                            strColumn = dCol.Caption
                        End If
                    Else
                        If blnTableNameInColumn = True Then
                            strColumn &= "," & strTableName & "." & dCol.Caption
                        Else
                            strColumn &= "," & dCol.Caption
                        End If

                    End If
                End If
            Next

            Return strColumn
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Column_List; Module Name: " & mstrModuleName)
            Return ""
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try
    End Function
    'Hemant (19 Jul 2024) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Sorry, This calendar name is already defined. Please define new calendar name.")
			Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this Calendar. Reason : This Calendar is already linked with some transaction.")
			Language.setMessage(mstrModuleName, 4, "Open")
			Language.setMessage(mstrModuleName, 5, "Close")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
