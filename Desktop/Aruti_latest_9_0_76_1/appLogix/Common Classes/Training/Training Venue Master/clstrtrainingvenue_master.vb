﻿'************************************************************************************************************************************
'Class Name : clsTrtrainingvenue_master.vb
'Purpose    :
'Date       :02/01/2021
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clstrtrainingvenue_master
    Private Const mstrModuleName = "clstrtrainingvenue_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintVenueunkid As Integer
    Private mstrVenuename As String = String.Empty
    Private mintTrainingproviderunkid As Integer
    Private mstrAddress As String = String.Empty
    Private mintCountryunkid As Integer
    Private mintStateunkid As Integer
    Private mintCityunkid As Integer
    Private mstrFax As String = String.Empty
    Private mstrTelephoneno As String = String.Empty
    Private mstrEmail As String = String.Empty
    Private mstrContact_Person As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mintUserunkid As Integer = 0
    Private mstrWebClientIP As String = ""
    Private mstrWebHostName As String = ""
    Private mstrWebFormName As String = ""
    Private mblnIsWeb As Boolean = False
    'Hemant (03 Jun 2021) -- Start
    'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
    Private mintCapacity As Integer
    'Hemant (03 Jun 2021) -- End
    'Hemant (03 Jun 2021) -- Start
    'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
    Private mblnIsLocked As Boolean = False
    'Hemant (03 Jun 2021) -- End
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set venueunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Venueunkid(Optional ByVal objDoOperation As clsDataOperation = Nothing) As Integer
        Get
            Return mintVenueunkid
        End Get
        Set(ByVal value As Integer)
            mintVenueunkid = value
            Call GetData(objDoOperation)
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set venuename
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Venuename() As String
        Get
            Return mstrVenuename
        End Get
        Set(ByVal value As String)
            mstrVenuename = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set trainingproviderunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Trainingproviderunkid() As Integer
        Get
            Return mintTrainingproviderunkid
        End Get
        Set(ByVal value As Integer)
            mintTrainingproviderunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set address
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Address() As String
        Get
            Return mstrAddress
        End Get
        Set(ByVal value As String)
            mstrAddress = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set countryunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stateunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set cityunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fax
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Fax() As String
        Get
            Return mstrFax
        End Get
        Set(ByVal value As String)
            mstrFax = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set telephoneno
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Telephoneno() As String
        Get
            Return mstrTelephoneno
        End Get
        Set(ByVal value As String)
            mstrTelephoneno = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set email
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set contact_person
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Contact_Person() As String
        Get
            Return mstrContact_Person
        End Get
        Set(ByVal value As String)
            mstrContact_Person = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebFormName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebClientIP
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebClientIP() As String
        Get
            Return mstrWebClientIP
        End Get
        Set(ByVal value As String)
            mstrWebClientIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set WebHostName
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _WebHostName() As String
        Get
            Return mstrWebHostName
        End Get
        Set(ByVal value As String)
            mstrWebHostName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set IsWeb
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _IsWeb() As Boolean
        Get
            Return mblnIsWeb
        End Get
        Set(ByVal value As Boolean)
            mblnIsWeb = value
        End Set
    End Property

    'Hemant (03 Jun 2021) -- Start
    'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
    ''' <summary>
    ''' Purpose: Get or Set capacity
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Capacity() As Integer
        Get
            Return mintCapacity
        End Get
        Set(ByVal value As Integer)
            mintCapacity = value
        End Set
    End Property
    'Hemant (03 Jun 2021) -- End

    'Hemant (03 Jun 2021) -- Start
    'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
    ''' <summary>
    ''' Purpose: Get or Set capacity
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _IsLocked() As Boolean
        Get
            Return mblnIsLocked
        End Get
        Set(ByVal value As Boolean)
            mblnIsLocked = value
        End Set
    End Property
    'Hemant (03 Jun 2021) -- End

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData(Optional ByVal objDoOperation As clsDataOperation = Nothing)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If objDoOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = objDoOperation
        End If

        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  venueunkid " & _
                      ", venuename " & _
                      ", trainingproviderunkid " & _
                      ", address " & _
                      ", countryunkid " & _
                      ", stateunkid " & _
                      ", cityunkid " & _
                      ", fax " & _
                      ", telephoneno " & _
                      ", email " & _
                      ", contact_person " & _
                      ", isactive " & _
                      ", capacity " & _
                      ", islocked " & _
                      " FROM trtrainingvenue_master " & _
                      " WHERE venueunkid = @venueunkid "
            'Hemant (03 Jun 2021) -- [capacity,islocked]

            objDataOperation.AddParameter("@venueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVenueunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintVenueunkid = CInt(dtRow.Item("venueunkid"))
                mstrVenuename = dtRow.Item("venuename").ToString
                mintTrainingproviderunkid = CInt(dtRow.Item("trainingproviderunkid"))
                mstrAddress = dtRow.Item("address").ToString
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintStateunkid = CInt(dtRow.Item("stateunkid"))
                mintCityunkid = CInt(dtRow.Item("cityunkid"))
                mstrFax = dtRow.Item("fax").ToString
                mstrTelephoneno = dtRow.Item("telephoneno").ToString
                mstrEmail = dtRow.Item("email").ToString
                mstrContact_Person = dtRow.Item("contact_person").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                'Hemant (03 Jun 2021) -- Start
                'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
                mintCapacity = CInt(dtRow.Item("capacity"))
                'Hemant (03 Jun 2021) -- End
                'Hemant (03 Jun 2021) -- Start
                'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
                mblnIsLocked = CBool(dtRow.Item("islocked"))
                'Hemant (03 Jun 2021) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If objDoOperation Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  trtrainingvenue_master.venueunkid " & _
                      ", trtrainingvenue_master.trainingproviderunkid " & _
                      ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(trtrainingvenue_master.venuename,'') ELSE ISNULL(hrinstitute_master.institute_name,'')  END venuename " & _
                      ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(trtrainingvenue_master.address,'') ELSE ISNULL(institute_address,'') END address  " & _
                      ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(trtrainingvenue_master.countryunkid,0) ELSE ISNULL(hrinstitute_master.countryunkid,0) END countryunkid   " & _
                      ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(trtrainingvenue_master.stateunkid,0) ELSE ISNULL(hrinstitute_master.stateunkid,0) END stateunkid  " & _
                      ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(trtrainingvenue_master.cityunkid,0) ELSE ISNULL(hrinstitute_master.cityunkid,0) END cityunkid " & _
                      ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(trtrainingvenue_master.fax,'') ELSE ISNULL(hrinstitute_master.institute_fax,'') END fax  " & _
                      ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(trtrainingvenue_master.telephoneno,'') ELSE ISNULL(hrinstitute_master.telephoneno,'') END telephoneno " & _
                      ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(trtrainingvenue_master.email,'') ELSE ISNULL(hrinstitute_master.institute_email,'') END email  " & _
                      ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(trtrainingvenue_master.contact_person,'') ELSE ISNULL(hrinstitute_master.contact_person,'') END contact_person " & _
                      ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(COM.country_name,'') ELSE ISNULL(COMT.country_name,'') END country " & _
                      ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(SM.name,'') ELSE ISNULL(SMT.name,'') END  state " & _
                      ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(CM.name,'') ELSE ISNULL(CMT.name,'') END  city " & _
                      ", trtrainingvenue_master.isactive " & _
                      ", trtrainingvenue_master.capacity " & _
                      ", trtrainingvenue_master.islocked " & _
                      ", CASE " & _
                      "    WHEN trtrainingvenue_master.islocked = 1 THEN @Yes " & _
                      "		ELSE @No " & _
                      "  END AS locked " & _
                      " FROM trtrainingvenue_master " & _
                      " LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = trtrainingvenue_master.trainingproviderunkid AND hrinstitute_master.ishospital = 0 " & _
                      " LEFT JOIN hrmsConfiguration..cfcountry_master COM ON COM.countryunkid = trtrainingvenue_master.countryunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfcountry_master COMT ON COMT.countryunkid = hrinstitute_master.countryunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfstate_master SM ON SM.stateunkid = trtrainingvenue_master.stateunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfstate_master SMT ON SMT.stateunkid = hrinstitute_master.stateunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfcity_master CM ON CM.cityunkid = trtrainingvenue_master.cityunkid " & _
                      " LEFT JOIN hrmsConfiguration..cfcity_master CMT ON CMT.cityunkid = hrinstitute_master.cityunkid "
            'Hemant (03 Jun 2021) -- [capacity,islocked,locked]
            If blnOnlyActive Then
                strQ &= " WHERE trtrainingvenue_master.isactive = 1 "
            End If

            strQ &= " ORDER BY venuename "

            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Yes", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Yes"))
            objDataOperation.AddParameter("@No", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "No"))
            'Hemant (03 Jun 2021) -- End
            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    'Sohail (01 Mar 2021) -- Start
    'NMB Enhancement : : New screen Departmental Training Need in New UI.
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal blnAddSelect As Boolean = False, Optional ByVal xDataOper As clsDataOperation = Nothing) As DataSet
        Dim objDataOperation As clsDataOperation
        Dim dsList As New DataSet

        If xDataOper IsNot Nothing Then
            objDataOperation = xDataOper
        Else
            objDataOperation = New clsDataOperation
        End If

        Dim strQ As String = String.Empty
        Dim exForce As Exception
        objDataOperation.ClearParameters()

        Try
            If blnAddSelect = True Then
                strQ = "SELECT 0 AS venueunkid, ' ' + @name AS name, 0 AS islocked   UNION "
            End If

            strQ &= " SELECT trtrainingvenue_master.venueunkid " & _
                    ", CASE WHEN trtrainingvenue_master.trainingproviderunkid <= 0 THEN ISNULL(trtrainingvenue_master.venuename,'') ELSE ISNULL(hrinstitute_master.institute_name,'')  END AS name  " & _
                    ", trtrainingvenue_master.islocked " & _
                        " FROM trtrainingvenue_master " & _
                    " LEFT JOIN hrinstitute_master ON hrinstitute_master.instituteunkid = trtrainingvenue_master.trainingproviderunkid AND hrinstitute_master.ishospital = 0 " & _
                    " WHERE trtrainingvenue_master.isactive = 1  "
            'Hemant (03 Jun 2021) -- [islocked]

            strQ &= " ORDER BY name "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            If xDataOper Is Nothing Then objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try
    End Function
    'Sohail (01 Mar 2021) -- End

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (trtrainingvenue_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrVenuename, mintTrainingproviderunkid, mintVenueunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Training Venue name is already defined. Please define new Training Venue name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@venuename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVenuename.ToString)
            objDataOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFax.ToString)
            objDataOperation.AddParameter("@telephoneno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTelephoneno.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@contact_person", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_Person.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
            objDataOperation.AddParameter("@capacity", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCapacity.ToString)
            'Hemant (03 Jun 2021) -- End
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
            objDataOperation.AddParameter("@islocked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLocked.ToString)
            'Hemant (03 Jun 2021) -- End

            strQ = "INSERT INTO trtrainingvenue_master ( " & _
                      "  venuename " & _
                      ", trainingproviderunkid " & _
                      ", address " & _
                      ", countryunkid " & _
                      ", stateunkid " & _
                      ", cityunkid " & _
                      ", fax " & _
                      ", telephoneno " & _
                      ", email " & _
                      ", contact_person " & _
                      ", isactive" & _
                      ", capacity " & _
                      ", islocked " & _
                    ") VALUES (" & _
                      "  @venuename " & _
                      ", @trainingproviderunkid " & _
                      ", @address " & _
                      ", @countryunkid " & _
                      ", @stateunkid " & _
                      ", @cityunkid " & _
                      ", @fax " & _
                      ", @telephoneno " & _
                      ", @email " & _
                      ", @contact_person " & _
                      ", @isactive" & _
                      ", @capacity " & _
                      ", @islocked " & _
                    "); SELECT @@identity"
            'Hemant (03 Jun 2021) -- [capacity,islocked]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintVenueunkid = dsList.Tables(0).Rows(0).Item(0)

            If AtInsertTrainingVenue(objDataOperation, enAuditType.ADD) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (trtrainingvenue_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrVenuename, mintTrainingproviderunkid, mintVenueunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Training Venue name is already defined. Please define new Training Venue name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@venueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVenueunkid.ToString)
            objDataOperation.AddParameter("@venuename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVenuename.ToString)
            objDataOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDataOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDataOperation.AddParameter("@fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFax.ToString)
            objDataOperation.AddParameter("@telephoneno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTelephoneno.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@contact_person", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_Person.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
            objDataOperation.AddParameter("@capacity", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCapacity.ToString)
            'Hemant (03 Jun 2021) -- End
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
            objDataOperation.AddParameter("@islocked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLocked.ToString)
            'Hemant (03 Jun 2021) -- End

            strQ = " UPDATE trtrainingvenue_master SET " & _
                      "  venuename = @venuename" & _
                      ", trainingproviderunkid = @trainingproviderunkid" & _
                      ", address = @address" & _
                      ", countryunkid = @countryunkid" & _
                      ", stateunkid = @stateunkid" & _
                      ", cityunkid = @cityunkid" & _
                      ", fax = @fax" & _
                      ", telephoneno = @telephoneno" & _
                      ", email = @email" & _
                      ", contact_person = @contact_person" & _
                      ", isactive = @isactive " & _
                      ", capacity = @capacity " & _
                      ", islocked = @islocked " & _
                      " WHERE venueunkid = @venueunkid "
            'Hemant (03 Jun 2021) -- [capacity,islocked]

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            If AtInsertTrainingVenue(objDataOperation, enAuditType.EDIT) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (trtrainingvenue_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, you cannot delete this Venue. Reason : This Venue is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE  trtrainingvenue_master SET isactive = 0 WHERE venueunkid = @venueunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@venueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If AtInsertTrainingVenue(objDataOperation, enAuditType.DELETE) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "select isnull(trainingvenueunkid,0) FROM trdepartmentaltrainingneed_master WHERE trainingvenueunkid = @venueunkid AND isvoid = 0 "

            objDataOperation.AddParameter("@venueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = "select isnull(trainingvenueunkid,0) FROM trtraining_request_master WHERE trainingvenueunkid = @venueunkid AND isvoid = 0 "
            If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xVenueName As String, ByVal xTrainingProviderId As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.ClearParameters()

        Try
            strQ = " SELECT " & _
                      "  venueunkid " & _
                      ", venuename " & _
                      ", trainingproviderunkid " & _
                      ", address " & _
                      ", countryunkid " & _
                      ", stateunkid " & _
                      ", cityunkid " & _
                      ", fax " & _
                      ", telephoneno " & _
                      ", email " & _
                      ", contact_person " & _
                      ", isactive " & _
                      ", capacity " & _
                      ", islocked " & _
                      " FROM trtrainingvenue_master " & _
                      " WHERE isactive = 1"
            'Hemant (03 Jun 2021) -- [capacity,islocked]

            If xVenueName.Trim.Length > 0 Then
                strQ &= " AND venuename = @venuename "
            End If

            If xTrainingProviderId > 0 Then
                'Hemant (03 Jun 2021) -- Start
                'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
                'strQ &= " AND trainingproviderunkid <> @trainingproviderunkid "
                strQ &= " AND trainingproviderunkid = @trainingproviderunkid "
                'Hemant (03 Jun 2021) -- End
                objDataOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xTrainingProviderId)
            End If

            If intUnkid > 0 Then
                strQ &= " AND venueunkid <> @venueunkid "
            End If

            objDataOperation.AddParameter("@venuename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, xVenueName)
            objDataOperation.AddParameter("@venueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    ''' 
    Public Function AtInsertTrainingVenue(ByVal objDoOperation As clsDataOperation, ByVal xAuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            objDoOperation.ClearParameters()
            objDoOperation.AddParameter("@venueunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVenueunkid)
            objDoOperation.AddParameter("@venuename", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVenuename.ToString)
            objDoOperation.AddParameter("@trainingproviderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTrainingproviderunkid.ToString)
            objDoOperation.AddParameter("@address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAddress.ToString)
            objDoOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid.ToString)
            objDoOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid.ToString)
            objDoOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid.ToString)
            objDoOperation.AddParameter("@fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFax.ToString)
            objDoOperation.AddParameter("@telephoneno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrTelephoneno.ToString)
            objDoOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDoOperation.AddParameter("@contact_person", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrContact_Person.ToString)
            objDoOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, xAuditType.ToString)
            objDoOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)

            If mstrWebClientIP.Trim.Length > 0 Then
                objDoOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, mstrWebClientIP)
            Else
                objDoOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.IP_SIZE, getIP)
            End If

            If mstrWebHostName.Length > 0 Then
                objDoOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebHostName)
            Else
                objDoOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getHostName())
            End If

            objDoOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
            objDoOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsWeb)
            'Hemant (03 Jun 2021) -- Start
            'ENHANCEMENT : OLD-404 - Optional Room Capacity field on Training Venue Master
            objDoOperation.AddParameter("@capacity", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCapacity)
            'Hemant (03 Jun 2021) -- End
            'ENHANCEMENT : OLD-403 - Training Venue Lock/Unlock Option
            objDataOperation.AddParameter("@islocked", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsLocked.ToString)
            'Hemant (03 Jun 2021) -- End

            strQ = "INSERT INTO attrtrainingvenue_master ( " & _
                      "  venueunkid " & _
                      ", venuename " & _
                      ", trainingproviderunkid " & _
                      ", address " & _
                      ", countryunkid " & _
                      ", stateunkid " & _
                      ", cityunkid " & _
                      ", fax " & _
                      ", telephoneno " & _
                      ", email " & _
                      ", contact_person " & _
                      ", audittype " & _
                      ", audituserunkid " & _
                      ", auditdatetime " & _
                      ", ip " & _
                      ", machine_name " & _
                      ", form_name " & _
                      ", isweb " & _
                      ", capacity " & _
                      ", islocked " & _
                    ") VALUES (" & _
                      "  @venueunkid " & _
                      ", @venuename " & _
                      ", @trainingproviderunkid " & _
                      ", @address " & _
                      ", @countryunkid " & _
                      ", @stateunkid " & _
                      ", @cityunkid " & _
                      ", @fax " & _
                      ", @telephoneno " & _
                      ", @email " & _
                      ", @contact_person " & _
                      ", @audittype " & _
                      ", @audituserunkid " & _
                      ", GETDATE() " & _
                      ", @ip " & _
                      ", @machine_name " & _
                      ", @form_name " & _
                      ", @isweb " & _
                      ", @capacity " & _
                      ", @islocked " & _
                    "); SELECT @@identity"
            'Hemant (03 Jun 2021) -- [capacity,islocked]

            dsList = objDoOperation.ExecQuery(strQ, "List")

            If objDoOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDoOperation.ErrorNumber & ": " & objDoOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: AtInsertTrainingVenue; Module Name: " & mstrModuleName)
            Return False
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "This Training Venue name is already defined. Please define new Training Venue name.")
			Language.setMessage(mstrModuleName, 3, "Sorry, you cannot delete this Venue. Reason : This Venue is already linked with some transaction.")
			
		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class