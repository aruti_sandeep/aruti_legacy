﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class clsDashboard_Class

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsDashboard_Class"
    Private objDataOperation As New clsDataOperation
    Private dsDataSetCollection As New DataSet
    Private mstrStaffDelailsMonthIds As String = String.Empty
    Private mDic_Color As New Dictionary(Of Integer, Integer)

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Private mstrDatabaseName As String = ""
    Private mstrUserModeSetting As String = ""
    Private mintuserUnkid As Integer = -1
    Private mintYearUnkId As Integer = -1
    Private mintCompanyUnkid As Integer = -1
    Private mdtDatabaseStartDate As Date
    Private mdtDatabaseEndDate As Date
    Private mdtPriodStartDate As Date
    Private mdtPeriodEndDate As Date
    Private mdtCurrentDateAndTime As DateTime
    Private mblnIncludeInactiveEmployee As Boolean = True
    Private mblnOnlyApproved As Boolean = True

    Private mblnShow_Probation_Dates As Boolean = True
    Private mblnShow_Suspension_Dates As Boolean = True
    Private mblnShow_Appointment_Dates As Boolean = True
    Private mblnShow_Confirmation_Dates As Boolean = True
    Private mblnShow_BirthDates As Boolean = True
    Private mblnShow_Anniversary_Dates As Boolean = True
    Private mblnShow_Contract_Ending_Dates As Boolean = True
    Private mblnShow_TodayRetirement_Dates As Boolean = True
    Private mblnShow_ForcastedRetirement_Dates As Boolean = True
    Private mblnShow_ForcastedEOC_Dates As Boolean = True
    Private mblnShow_ForcastedELC_Dates As Boolean = True

    Private mintForcasted_ViewSetting As Integer = -1
    Private mintForcastedValue As Integer = -1
    Private mintForcastedEOC_ViewSetting As Integer = -1
    Private mintForcastedEOCValue As Integer = -1
    Private mintForcastedELCViewSetting As Integer = -1
    Private mintForcastedELCValue As Integer = -1
    Private mintLeaveBalanceSetting As Integer = -1
    'Shani(24-Aug-2015) -- End

    'S.SANDEEP [12-Apr-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#218|#ARUTI-106}
    Private mblnShow_PendingAccrueLeaves As Boolean = True
    'S.SANDEEP [12-Apr-2018] -- END

    'Pinkal (30-Sep-2023) -- Start
    '(A1X-1371) Toyota - New desktop dashboard item: Total Work Anniversaries Within 7 Days from Now.
    Private mblnShow_WorkAnniversary_Dates As Boolean = True
    Private mblnShow_WorkResidencePermitExpiry_Dates As Boolean = True
    Private mblnShow_TotalLeaversFromCurrentFY As Boolean = True
    'Pinkal (30-Sep-2023) -- End

    'Hemant (06 Dec 2024) -- Start
    'ENHANCEMENT(TOYOTA): A1X - 2899 :  Dash reminder - Total Payroll Exemptions: Current & Next Month
    Private mblnShow_Exemption_Dates As Boolean = True
    'Hemant (06 Dec 2024) -- End



#End Region

#Region " Propertie(s) "

    Public ReadOnly Property _DataSetCollection() As DataSet
        Get
            Return dsDataSetCollection
        End Get
    End Property

    Public ReadOnly Property _mDicColor() As Dictionary(Of Integer, Integer)
        Get
            Return mDic_Color
        End Get
    End Property


#End Region

#Region " Constructor "


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Sub New(Optional ByVal blnFill As Boolean = True)

    Public Sub New()

    End Sub
    Public Sub New(ByVal xDatabaseName As String, _
                   ByVal xUserModeSetting As String, _
                   ByVal xuserUnkid As Integer, _
                   ByVal xYearUnkId As Integer, _
                   ByVal xCompanyUnkid As Integer, _
                   ByVal xDatabaseStartDate As Date, _
                   ByVal xDatabaseEndDate As Date, _
                   ByVal xPriodStartDate As Date, _
                   ByVal xPeriodEndDate As Date, _
                   ByVal xCurrentDateAndTime As DateTime, _
                   ByVal xIncludeInactiveEmployee As Boolean, _
                   ByVal xOnlyApproved As Boolean, _
                   ByVal xShow_Probation_Dates As Boolean, _
                   ByVal xShow_Suspension_Dates As Boolean, _
                   ByVal xShow_Appointment_Dates As Boolean, _
                   ByVal xShow_Confirmation_Dates As Boolean, _
                   ByVal xShow_BirthDates As Boolean, _
                   ByVal xShow_Anniversary_Dates As Boolean, _
                   ByVal xShow_Contract_Ending_Dates As Boolean, _
                   ByVal xShow_TodayRetirement_Dates As Boolean, _
                   ByVal xShow_ForcastedRetirement_Dates As Boolean, _
                   ByVal xShow_ForcastedEOC_Dates As Boolean, _
                   ByVal xShow_ForcastedELC_Dates As Boolean, _
                   ByVal xForcasted_ViewSetting As Integer, _
                   ByVal xForcastedValue As Integer, _
                   ByVal xForcastedEOC_ViewSetting As Integer, _
                   ByVal xForcastedEOCValue As Integer, _
                   ByVal xForcastedELCViewSetting As Integer, _
                   ByVal xForcastedELCValue As Integer, _
                   ByVal xLeaveBalanceSetting As Integer, _
                   ByVal blnFill As Boolean, _
                   ByVal xShow_PendingAccrueLeaves As Boolean _
                   , ByVal xShow_Exemption_Dates As Boolean)

        'Hemant (06 Dec 2024) -- [xShow_Exemption_Dates]
        'S.SANDEEP [12-Apr-2018] -- START {Ref#218|#ARUTI-106} [xShow_PendingAccrueLeaves] -- END
        'Shani(24-Aug-2015) -- End

        Try
            mstrDatabaseName = xDatabaseName : mstrUserModeSetting = xUserModeSetting
            mintuserUnkid = xuserUnkid : mintYearUnkId = xYearUnkId
            mintCompanyUnkid = xCompanyUnkid : mdtDatabaseStartDate = xDatabaseStartDate
            mdtDatabaseEndDate = xDatabaseEndDate : mdtPriodStartDate = xPriodStartDate
            mdtPeriodEndDate = xPeriodEndDate : mblnIncludeInactiveEmployee = xIncludeInactiveEmployee
            mblnOnlyApproved = xOnlyApproved : mdtCurrentDateAndTime = xCurrentDateAndTime
            mblnShow_Probation_Dates = xShow_Probation_Dates
            mblnShow_Suspension_Dates = xShow_Suspension_Dates
            mblnShow_Appointment_Dates = xShow_Appointment_Dates
            mblnShow_Confirmation_Dates = xShow_Confirmation_Dates
            mblnShow_BirthDates = xShow_BirthDates
            mblnShow_Anniversary_Dates = xShow_Anniversary_Dates
            mblnShow_Contract_Ending_Dates = xShow_Contract_Ending_Dates
            mblnShow_TodayRetirement_Dates = xShow_TodayRetirement_Dates
            mblnShow_ForcastedRetirement_Dates = xShow_ForcastedRetirement_Dates
            mblnShow_ForcastedEOC_Dates = xShow_ForcastedEOC_Dates
            mblnShow_ForcastedELC_Dates = xShow_ForcastedELC_Dates
            mintForcasted_ViewSetting = xForcasted_ViewSetting
            mintForcastedValue = xForcastedValue
            mintForcastedEOC_ViewSetting = xForcastedEOC_ViewSetting
            mintForcastedEOCValue = xForcastedEOCValue
            mintForcastedELCViewSetting = xForcastedELCViewSetting
            mintForcastedELCValue = xForcastedELCValue
            mintLeaveBalanceSetting = xLeaveBalanceSetting

            'S.SANDEEP [12-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#218|#ARUTI-106}
            mblnShow_PendingAccrueLeaves = xShow_PendingAccrueLeaves
            'S.SANDEEP [12-Apr-2018] -- END

            'Hemant (06 Dec 2024) -- Start
            'ENHANCEMENT(TOYOTA): A1X - 2899 :  Dash reminder - Total Payroll Exemptions: Current & Next Month
            mblnShow_Exemption_Dates = xShow_Exemption_Dates
            'Hemant (06 Dec 2024) -- End


            If blnFill Then
                For Each item In [Enum].GetValues(GetType(enDashBoardListIndex))
                    Select Case item.ToString.ToUpper
                        Case enDashBoardListIndex.SALARY_LIST.ToString.ToUpper
                            dsDataSetCollection = Salary_Analysis(item.ToString.ToUpper).Copy
                        Case enDashBoardListIndex.LEAVE_LIST.ToString.ToUpper
                            dsDataSetCollection.Tables.Add(Leave_Analysis(item.ToString.ToUpper).Tables(0).Copy)
                        Case enDashBoardListIndex.LOGIN_LIST.ToString.ToUpper

                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'dsDataSetCollection.Tables.Add(Get_Login_Summary(ConfigParameter._Object._CurrentDateAndTime, item.ToString.ToUpper).Tables(0).Copy)
                            dsDataSetCollection.Tables.Add(Get_Login_Summary(mdtCurrentDateAndTime, item.ToString.ToUpper).Tables(0).Copy)
                            'Shani(24-Aug-2015) -- End
                        Case enDashBoardListIndex.TRAININGCOST_LIST.ToString.ToUpper
                            dsDataSetCollection.Tables.Add(Training_Costing(item.ToString.ToUpper).Tables(0).Copy)
                        Case enDashBoardListIndex.STAFF_LIST
                            'S.SANDEEP [ 05 MARCH 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                        Case enDashBoardListIndex.EMPLOYEE_DATES.ToString.ToUpper
                            'Sohail (25 Apr 2019) -- Start
                            'Enhancement - 76.1 - Performance enhancement of aruti main view.
                            'dsDataSetCollection.Tables.Add(EmployeeDates(item.ToString.ToUpper).Tables(0).Copy)
                            'Sohail (25 Apr 2019) -- End
                            'S.SANDEEP [ 05 MARCH 2012 ] -- END
                    End Select
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : New ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " ENUMS "

    Public Enum enViewDetails
        SALARY_DETAIL = 1
        LEAVE_DETAIL = 2
        TRAINING_COST = 3
        STAFF_DETAILS = 4
    End Enum

    Public Enum enDashBoardListIndex
        SALARY_LIST = 1
        LEAVE_LIST = 2
        LOGIN_LIST = 3
        TRAININGCOST_LIST = 4
        STAFF_LIST = 5
        'S.SANDEEP [ 05 MARCH 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        EMPLOYEE_DATES = 6
        'S.SANDEEP [ 05 MARCH 2012 ] -- END

        'Pinkal (14-Feb-2022) -- Start
        'Enhancement TRA : TnA Module Enhancement for TRA.
        TNADETAILS_LIST = 7
        'Pinkal (14-Feb-2022) -- End

    End Enum

    Public Enum enViewDetailsByIdx
        DEPARTMENT_WISE = 1
        BRANCH_WISE = 2
        COSTCENTER_WISE = 3
        SECTION_WISE = 4
        UNIT_WISE = 5
    End Enum

    'Sohail (25 Apr 2019) -- Start
    'Enhancement - 76.1 - Performance enhancement of aruti main view.
    Public Enum enEmpDates
        Probation_Dates = 1
        Suspension_Dates = 2
        Appointment_Dates = 3
        Confirmation_Dates = 4
        BirthDates = 5
        Anniversary_Dates = 6
        Contract_Ending_Dates = 7
        TodayRetirement_Dates = 8
        ForcastedRetirement_Dates = 9
        ForcastedEOC_Dates = 10
        ForcastedELC_Dates = 11
        PendingAccrueLeaves = 12
        ForcastedWorkAnniversary = 13 'Pinkal (30-Sep-2023) -- (A1X-1371) Toyota - New desktop dashboard item: Total Work Anniversaries Within 7 Days from Now.
        ForcastedWorkResidencePermitExpiry = 14 'Pinkal (30-Sep-2023) -- (A1X-1371) Toyota - New desktop dashboard item: Total Work Anniversaries Within 7 Days from Now.
        TotalLeaversFromCurrentFY = 15 'Pinkal (30-Sep-2023) -- (A1X-1371) Toyota - New desktop dashboard item: Total Work Anniversaries Within 7 Days from Now.
        'Hemant (06 Dec 2024) -- Start
        'ENHANCEMENT(TOYOTA): A1X - 2899 :  Dash reminder - Total Payroll Exemptions: Current & Next Month
        Exemption_Dates = 16
        'Hemant (06 Dec 2024) -- End
    End Enum
    'Sohail (25 Apr 2019) -- End

    'Sohail (01 Jan 2021) -- Start
    'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS.
    Public Enum enPendingTask
        SetScoreCardESS = 1
        ApproveScoreCardMSS = 2
        SubmitScoreCardESS = 3
        ApproveUpdateProgressMSS = 4
        MyAssessmentESS = 5
        AssessEmployeeMSS = 6
        MyCompetenceAssessmentESS = 7
        AssessEmployeeCompetenceMSS = 8
        ReviewEmployeeAssessmentMSS = 9
        ReviewEmployeeCompetenceMSS = 10
        ApproveLeaveMSS = 11
        ApproveOTApplicationMSS = 12
        ApproveClaimExpenseMSS = 13
        AssetDeclarationT2ESS = 14
        NonDisclosureDeclarationESS = 15
        ApproveSalaryChangeMSS = 16
        ApprovePayslipPaymentMSS = 17
        ApproveLoanApplicationMSS = 18
        ApproveStaffRequisitionMSS = 19
        ApproveClaimRetirementMSS = 20
        ApproveBudgetTimesheetMSS = 21
        ApproveShortlistingCriteriaMSS = 22
        ApproveEligibleApplicantsMSS = 23
        ApproveCalibrationMSS = 24
        'Hemant (04 Sep 2021) -- Start
        'ENHANCEMENT : OLD-465 - Dashboard Pending Task Enhancement - Training Evaluation.
        MyTrainingFeedbackESS = 25
        EvaluateEmployeeTrainingMSS = 26
        'Hemant (04 Sep 2021) -- End
    End Enum
    'Sohail (01 Jan 2021) -- End

#End Region

#Region " Public Methods "

    Public Function Salary_Analysis(Optional ByVal StrList As String = "List", Optional ByVal StrDivisonValue As String = "") As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   "     periodunkid AS PId " & _
                   "    ,period_code AS PCode " & _
                   "    ,period_name AS PName " & _
                   "    ,CONVERT(CHAR(8),start_date,112) AS ST_Date " & _
                   "    ,CONVERT(CHAR(8),end_date,112) AS ED_Date " & _
                   "    ,statusid AS PStatusId "
            If StrDivisonValue.Trim.Length > 0 Then
                StrQ &= ",ISNULL(TBS,0)/" & CDec(StrDivisonValue) & " AS TBS " & _
                        ",ISNULL(TCS,0)/" & CDec(StrDivisonValue) & " AS TCS " & _
                        ",ISNULL(THS,0)/" & CDec(StrDivisonValue) & " AS THS "
            Else
                StrQ &= ",ISNULL(TBS,0) AS TBS " & _
                        ",ISNULL(TCS,0) AS TCS " & _
                        ",ISNULL(THS,0) AS THS "
            End If
                     

            StrQ &= "FROM cfcommon_period_tran " & _
                    "LEFT JOIN " & _
                    "( " & _
                          "SELECT " & _
                               "COUNT(DISTINCT prempsalary_tran.employeeunkid) AS EmpId " & _
                               ",SUM(prempsalary_tran.amount) AS TBS " & _
                               ",prpayment_tran.periodunkid AS SalPId " & _
                          "FROM prempsalary_tran " & _
                               "JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                               "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                               "JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid " & _
                          "WHERE   ISNULL(prempsalary_tran.isvoid,0) = 0 " & _
                               "AND ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                               "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                               "AND hremployee_master.isactive = 1 " & _
                         "GROUP BY prpayment_tran.periodunkid " & _
                     ") AS TBS ON TBS.SalPId = cfcommon_period_tran.periodunkid " & _
                     "LEFT JOIN " & _
                   "( " & _
                   "    SELECT " & _
                               "COUNT(DISTINCT prpayment_tran.employeeunkid) AS EmpId " & _
                               ",SUM(prpayment_tran.amount) AS TCS " & _
                               ",prpayment_tran.periodunkid AS SalPId " & _
                   "    FROM prpayment_tran " & _
                               "JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                          "WHERE ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                               "AND prpayment_tran.referenceid = 3 " & _
                               "AND prpayment_tran.paymentmode = 1 " & _
                               "AND hremployee_master.isactive = 1 " & _
                         "GROUP BY prpayment_tran.periodunkid " & _
                     ") AS TCS ON TCS.SalPId = cfcommon_period_tran.periodunkid " & _
                     "LEFT JOIN " & _
                     "( " & _
                          "SELECT " & _
                                "COUNT(prtnaleave_tran.employeeunkid) AS EmpId " & _
                               ",SUM(prtnaleave_tran.balanceamount) AS THS " & _
                               ",prtnaleave_tran.payperiodunkid AS SalPId " & _
                          "FROM prtnaleave_tran " & _
                               "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                          "WHERE ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                               "AND prtnaleave_tran.payperiodunkid = 1 " & _
                               "AND prtnaleave_tran.balanceamount <> 0 " & _
                               "AND hremployee_master.isactive = 1 " & _
                         "GROUP BY prtnaleave_tran.payperiodunkid " & _
                     ") AS THS ON THS.SalPId = cfcommon_period_tran.periodunkid " & _
                     "WHERE modulerefid = " & enModuleReference.Payroll & " AND isactive = 1 AND YEAR(start_date) >= YEAR(@CurrYear) " & _
                     "AND (TBS > 0 OR TCS > 0 OR THS > 0) " & _
                     "Order by st_date "
            'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@CurrYear", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@CurrYear", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, FinancialYear._Object._Database_Start_Date)
            objDataOperation.AddParameter("@CurrYear", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDatabaseStartDate)
            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [ 28 FEB 2012 ] -- END



            dsList = objDataOperation.ExecQuery(StrQ, StrList)


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTable As New DataTable(StrList)



            If dsList.Tables(0).Rows.Count > 0 Then

            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Salary_Analysis", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function Detail_Information(ByVal intDetailIdx As Integer, ByVal intDetailValue As Integer, ByVal intViewDetailByIdx As Integer, Optional ByVal StrList As String = "List", Optional ByVal intViewUnkid As Integer = -1) As DataSet
        Dim dsDetals As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            Select Case intDetailIdx
                Case enViewDetails.SALARY_DETAIL
                    Select Case intViewDetailByIdx
                        Case enViewDetailsByIdx.DEPARTMENT_WISE
                            StrQ = "SELECT " & _
                                   "     departmentunkid AS Id " & _
                                   "    ,code AS Code " & _
                                   "    ,name AS Name " & _
                                   "    ,ISNULL(Amount,0) AS Amount " & _
                                   "    ,ISNULL(TotSal,0) AS TotSal " & _
                                   "    ,CASE When ISNULL(Amount,0) = 0 OR ISNULL(TotSal,0) = 0 THEN 0 ELSE ISNULL(Amount,0)*100/ISNULL(TotSal,0) END AS SalPer " & _
                                   "FROM hrdepartment_master "
                        Case enViewDetailsByIdx.BRANCH_WISE
                            StrQ = "SELECT " & _
                                   "     stationunkid AS Id " & _
                                   "    ,code AS Code " & _
                                   "    ,name AS Name " & _
                                   "    ,ISNULL(Amount,0) AS Amount " & _
                                   "    ,ISNULL(TotSal,0) AS TotSal " & _
                                   "    ,CASE When ISNULL(Amount,0) = 0 OR ISNULL(TotSal,0) = 0 THEN 0 ELSE ISNULL(Amount,0)*100/ISNULL(TotSal,0) END AS SalPer " & _
                                   "FROM hrstation_master "
                        Case enViewDetailsByIdx.COSTCENTER_WISE
                            StrQ = "SELECT " & _
                                   "     costcenterunkid AS Id " & _
                                   "    ,costcentercode AS Code " & _
                                   "    ,costcentername AS Name " & _
                                   "    ,ISNULL(Amount,0) AS Amount " & _
                                   "    ,ISNULL(TotSal,0) AS TotSal " & _
                                   "    ,CASE When ISNULL(Amount,0) = 0 OR ISNULL(TotSal,0) = 0 THEN 0 ELSE ISNULL(Amount,0)*100/ISNULL(TotSal,0) END AS SalPer " & _
                                   "FROM prcostcenter_master "
                        Case enViewDetailsByIdx.SECTION_WISE
                            StrQ = "SELECT " & _
                                   "     sectionunkid AS Id " & _
                                   "    ,code AS Code " & _
                                   "    ,name AS Name " & _
                                   "    ,ISNULL(Amount,0) AS Amount " & _
                                   "    ,ISNULL(TotSal,0) AS TotSal " & _
                                   "    ,CASE When ISNULL(Amount,0) = 0 OR ISNULL(TotSal,0) = 0 THEN 0 ELSE ISNULL(Amount,0)*100/ISNULL(TotSal,0) END AS SalPer " & _
                                   "FROM hrsection_master "
                        Case enViewDetailsByIdx.UNIT_WISE
                            StrQ = "SELECT " & _
                                   "     unitunkid AS Id " & _
                                   "    ,code AS Code " & _
                                   "    ,name AS Name " & _
                                   "    ,ISNULL(Amount,0) AS Amount " & _
                                   "    ,ISNULL(TotSal,0) AS TotSal " & _
                                   "    ,CASE When ISNULL(Amount,0) = 0 OR ISNULL(TotSal,0) = 0 THEN 0 ELSE ISNULL(Amount,0)*100/ISNULL(TotSal,0) END AS SalPer " & _
                                   "FROM hrunit_master "
                    End Select

                    StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT "

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Select Case intViewDetailByIdx
                    '    Case enViewDetailsByIdx.DEPARTMENT_WISE
                    '        StrQ &= " hremployee_master.departmentunkid AS Id "
                    '    Case enViewDetailsByIdx.BRANCH_WISE
                    '        StrQ &= " hremployee_master.stationunkid AS Id "
                    '    Case enViewDetailsByIdx.COSTCENTER_WISE
                    '        StrQ &= " hremployee_master.costcenterunkid AS Id "
                    '    Case enViewDetailsByIdx.SECTION_WISE
                    '        StrQ &= " hremployee_master.sectionunkid AS Id "
                    '    Case enViewDetailsByIdx.UNIT_WISE
                    '        StrQ &= " hremployee_master.unitunkid AS Id "
                    'End Select

                    Select Case intViewDetailByIdx
                        Case enViewDetailsByIdx.DEPARTMENT_WISE
                            StrQ &= " Alloc.departmentunkid AS Id "
                        Case enViewDetailsByIdx.BRANCH_WISE
                            StrQ &= " Alloc.stationunkid AS Id "
                        Case enViewDetailsByIdx.COSTCENTER_WISE
                            StrQ &= " CC.costcenterunkid AS Id "
                        Case enViewDetailsByIdx.SECTION_WISE
                            StrQ &= " Alloc.sectionunkid AS Id "
                        Case enViewDetailsByIdx.UNIT_WISE
                            StrQ &= " Alloc.unitunkid AS Id "
                    End Select

                    'Shani(18-Dec-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    ''Shani(24-Aug-2015) -- End
                    'Select Case intViewUnkid

                    '    'Shani(24-Aug-2015) -- Start
                    '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    '    Case 1  'TBS [ TOTAL BANK SALARY ]
                    '        'StrQ &= ",SUM(prempsalary_tran.amount) AS Amount " & _
                    '        '        ",( SELECT " & _
                    '        '        "       SUM(prempsalary_tran.amount) " & _
                    '        '        "   FROM prempsalary_tran " & _
                    '        '        "       JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                    '        '        "       JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '        '        "   WHERE prpayment_tran.isvoid = 0  AND prempsalary_tran.isvoid = 0 and prpayment_tran.periodunkid = @PeriodId AND hremployee_master.isactive = 1 " & _
                    '        '        "       AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    '        '        "  ) AS TotSal " & _
                    '        '        "FROM prempsalary_tran " & _
                    '        '        "   JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                    '        '        "   JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '        '        "WHERE ISNULL(prempsalary_tran.isvoid,0) = 0 " & _
                    '        '        "   AND ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                    '        '        "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                    '        '        "   AND hremployee_master.isactive = 1 AND prpayment_tran.periodunkid = @PeriodId "

                            'StrQ &= ",SUM(prempsalary_tran.amount) AS Amount " & _
                            '        ",( SELECT " & _
                            '        "       SUM(prempsalary_tran.amount) " & _
                            '        "   FROM prempsalary_tran " & _
                            '        "       JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                            '        "       JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid " & _
                            '        "   WHERE prpayment_tran.isvoid = 0  AND prempsalary_tran.isvoid = 0 and prpayment_tran.periodunkid = @PeriodId AND hremployee_master.isactive = 1 " & _
                            '        "       AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            '        "  ) AS TotSal " & _
                            '        "FROM prempsalary_tran " & _
                            '        "   JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                    '                "   JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid "
                    '        Select Case intViewDetailByIdx
                    '            Case enViewDetailsByIdx.DEPARTMENT_WISE
                    '                StrQ &= "LEFT JOIN " & _
                    '                        "(                      " & _
                    '                        "    SELECT " & _
                    '                        "         departmentunkid " & _
                    '                        "        ,employeeunkid " & _
                    '                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '                        "    FROM hremployee_transfer_tran " & _
                    '                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    '                        ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                    '            Case enViewDetailsByIdx.BRANCH_WISE
                    '                StrQ &= "LEFT JOIN " & _
                    '                        "( " & _
                    '                        "    SELECT " & _
                    '                        "         stationunkid " & _
                    '                        "        ,employeeunkid " & _
                    '                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '                        "    FROM hremployee_transfer_tran " & _
                    '                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    '                        ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                    '            Case enViewDetailsByIdx.COSTCENTER_WISE
                    '                StrQ &= "LEFT JOIN " & _
                    '                        "( " & _
                    '                        "    SELECT " & _
                    '                        "         cctranheadvalueid AS costcenterunkid " & _
                    '                        "        ,employeeunkid " & _
                    '                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '                        "    FROM hremployee_cctranhead_tran " & _
                    '                        "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                    '                        "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    '                        ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "
                    '            Case enViewDetailsByIdx.SECTION_WISE
                    '                StrQ &= "LEFT JOIN " & _
                    '                        "( " & _
                    '                        "    SELECT " & _
                    '                        "         sectionunkid " & _
                    '                        "        ,employeeunkid " & _
                    '                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '                        "    FROM hremployee_transfer_tran " & _
                    '                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    '                        ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                    '            Case enViewDetailsByIdx.UNIT_WISE
                    '                StrQ &= "LEFT JOIN " & _
                    '                        "(                      " & _
                    '                        "    SELECT " & _
                    '                        "         unitunkid " & _
                    '                        "        ,employeeunkid " & _
                    '                        "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    '                        "    FROM hremployee_transfer_tran " & _
                    '                        "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    '                        ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                    '        End Select

                    '        StrQ &= "WHERE ISNULL(prempsalary_tran.isvoid,0) = 0 " & _
                            '        "   AND ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                            '        "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                            '        "   AND hremployee_master.isactive = 1 AND prpayment_tran.periodunkid = @PeriodId "
                    '        'Shani(24-Aug-2015) -- End

                    '        'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]
                    '    Case 2  'TCS [ TOTAL CASH SALARY ]
                    '        StrQ &= "   ,SUM(amount) AS Amount " & _
                    '                "   ,( SELECT " & _
                    '                "           SUM(amount) " & _
                    '                "       FROM prpayment_tran " & _
                    '                "       JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                "       WHERE referenceid = 3 AND isvoid = 0 AND periodunkid = @PeriodId AND hremployee_master.isactive = 1 ) AS TotSal " & _
                    '                "   FROM prpayment_tran " & _
                    '                "   JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                "   WHERE referenceid = 3 AND hremployee_master.isactive = 1 " & _
                    '                "       AND isvoid = 0 " & _
                    '                "       AND periodunkid = @PeriodId "
                    '    Case 3  'THS [ TOTAL HOLD SALARY ]
                    '        StrQ &= "   ,SUM(prtnaleave_tran.balanceamount) AS Amount " & _
                    '                "   ,(SELECT SUM(prtnaleave_tran.balanceamount) FROM prtnaleave_tran JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                "Where ISNULL(prtnaleave_tran.isvoid,0) = 0 AND prtnaleave_tran.payperiodunkid = @PeriodId AND prtnaleave_tran.balanceamount <> 0 AND hremployee_master.isactive = 1 ) AS TotSal " & _
                    '                "FROM prtnaleave_tran " & _
                    '                "   JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                    '                "WHERE ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                    '                "   AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                    '                "   AND prtnaleave_tran.balanceamount <> 0 " & _
                    '                "   AND hremployee_master.isactive = 1 "
                    'End Select

                    Select Case intViewUnkid
                        Case 1  'TBS [ TOTAL BANK SALARY ]
                            StrQ &= ",SUM(prempsalary_tran.amount) AS Amount " & _
                                    ",( SELECT " & _
                                    "       SUM(prempsalary_tran.amount) " & _
                                    "   FROM prempsalary_tran " & _
                                    "       JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                    "       JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                    "   WHERE prpayment_tran.isvoid = 0  AND prempsalary_tran.isvoid = 0 and prpayment_tran.periodunkid = @PeriodId AND hremployee_master.isactive = 1 " & _
                                    "       AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                    "  ) AS TotSal " & _
                                    "FROM prempsalary_tran " & _
                                    "   JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                                    "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                                    "   JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid "

                        Case 2  'TCS [ TOTAL CASH SALARY ]
                            StrQ &= "   ,SUM(amount) AS Amount " & _
                                    "   ,( SELECT " & _
                                    "           SUM(amount) " & _
                                    "       FROM prpayment_tran " & _
                                    "       JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                    "       WHERE referenceid = 3 AND isvoid = 0 AND periodunkid = @PeriodId AND hremployee_master.isactive = 1 ) AS TotSal " & _
                                    "   FROM prpayment_tran " & _
                                    "   JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid "

                        Case 3  'THS [ TOTAL HOLD SALARY ]
                            StrQ &= "   ,SUM(prtnaleave_tran.balanceamount) AS Amount " & _
                                    "   ,(SELECT SUM(prtnaleave_tran.balanceamount) FROM prtnaleave_tran JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                    "Where ISNULL(prtnaleave_tran.isvoid,0) = 0 AND prtnaleave_tran.payperiodunkid = @PeriodId AND prtnaleave_tran.balanceamount <> 0 AND hremployee_master.isactive = 1 ) AS TotSal " & _
                                    "FROM prtnaleave_tran " & _
                                    "   JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid "
                    End Select

                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         departmentunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "( " & _
                                            "    SELECT " & _
                                            "         stationunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "( " & _
                                            "    SELECT " & _
                                            "         cctranheadvalueid AS costcenterunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_cctranhead_tran " & _
                                            "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                                            "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "( " & _
                                            "    SELECT " & _
                                            "         sectionunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         unitunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                            End Select

                    Select Case intViewUnkid
                        Case 1  'TBS [ TOTAL BANK SALARY ]
                            StrQ &= "WHERE ISNULL(prempsalary_tran.isvoid,0) = 0 " & _
                                    "   AND ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                                    "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                    "   AND hremployee_master.isactive = 1 AND prpayment_tran.periodunkid = @PeriodId "
                        Case 2  'TCS [ TOTAL CASH SALARY ]
                            StrQ &= "WHERE referenceid = 3 AND hremployee_master.isactive = 1 " & _
                            "  AND isvoid = 0 " & _
                            "  AND periodunkid = @PeriodId "
                        Case 3  'THS [ TOTAL HOLD SALARY ]
                            StrQ = "WHERE ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                                    "   AND prtnaleave_tran.payperiodunkid = @PeriodId " & _
                                    "   AND prtnaleave_tran.balanceamount <> 0 " & _
                                    "   AND hremployee_master.isactive = 1 "
                    End Select

                    'Shani(18-Dec-2015) -- End

                    Select Case intViewDetailByIdx


                        'Shani(24-Aug-2015) -- Start
                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                        'Case enViewDetailsByIdx.DEPARTMENT_WISE
                        '    StrQ &= "    Group BY hremployee_master.departmentunkid " & _
                        '            ")AS Sal_Det ON Sal_Det.Id = hrdepartment_master.departmentunkid "
                        'Case enViewDetailsByIdx.BRANCH_WISE
                        '    StrQ &= "    Group BY hremployee_master.stationunkid " & _
                        '            ")AS Sal_Det ON Sal_Det.Id = hrstation_master.stationunkid "
                        'Case enViewDetailsByIdx.COSTCENTER_WISE
                        '    StrQ &= "    Group BY hremployee_master.costcenterunkid " & _
                        '            ")AS Sal_Det ON Sal_Det.Id = prcostcenter_master.costcenterunkid "
                        'Case enViewDetailsByIdx.SECTION_WISE
                        '    StrQ &= "    Group BY hremployee_master.sectionunkid " & _
                        '            ")AS Sal_Det ON Sal_Det.Id = hrsection_master.sectionunkid "
                        'Case enViewDetailsByIdx.UNIT_WISE
                        '    StrQ &= "    Group BY hremployee_master.unitunkid " & _
                        '            ")AS Sal_Det ON Sal_Det.Id = hrunit_master.unitunkid "

                        Case enViewDetailsByIdx.DEPARTMENT_WISE
                            StrQ &= "    Group BY Alloc.departmentunkid " & _
                                    ")AS Sal_Det ON Sal_Det.Id = hrdepartment_master.departmentunkid "
                        Case enViewDetailsByIdx.BRANCH_WISE
                            StrQ &= "    Group BY Alloc.stationunkid " & _
                                    ")AS Sal_Det ON Sal_Det.Id = hrstation_master.stationunkid "
                        Case enViewDetailsByIdx.COSTCENTER_WISE
                            StrQ &= "    Group BY CC.costcenterunkid " & _
                                    ")AS Sal_Det ON Sal_Det.Id = prcostcenter_master.costcenterunkid "
                        Case enViewDetailsByIdx.SECTION_WISE
                            StrQ &= "    Group BY Alloc.sectionunkid " & _
                                    ")AS Sal_Det ON Sal_Det.Id = hrsection_master.sectionunkid "
                        Case enViewDetailsByIdx.UNIT_WISE
                            StrQ &= "    Group BY Alloc.unitunkid " & _
                                    ")AS Sal_Det ON Sal_Det.Id = hrunit_master.unitunkid "
                            'Shani(24-Aug-2015) -- End
                    End Select
                    StrQ &= " WHERE isactive = 1 "

                    objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intDetailValue)

                Case enViewDetails.LEAVE_DETAIL
                    Select Case intViewDetailByIdx
                        Case enViewDetailsByIdx.DEPARTMENT_WISE
                            StrQ = "SELECT " & _
                                   "     departmentunkid AS Id " & _
                                   "    ,code AS Code " & _
                                   "    ,name AS Name " & _
                                   "    ,ISNULL(Leave,0) Leave " & _
                                   "    ,ISNULL(TotalLeave,0) TotalLeave " & _
                                   "    ,CASE WHEN ISNULL(Leave,0) = 0 OR ISNULL(TotalLeave,0) = 0 THEN 0 ELSE CAST((Leave * 100) / TotalLeave AS DECIMAL(6,2)) END AS LvPer " & _
                                   "FROM hrdepartment_master "
                        Case enViewDetailsByIdx.BRANCH_WISE
                            StrQ = "SELECT " & _
                                   "     stationunkid AS Id " & _
                                   "    ,code AS Code " & _
                                   "    ,name AS Name " & _
                                   "    ,ISNULL(Leave,0) Leave " & _
                                   "    ,ISNULL(TotalLeave,0) TotalLeave " & _
                                   "    ,CASE WHEN ISNULL(Leave,0) = 0 OR ISNULL(TotalLeave,0) = 0 THEN 0 ELSE CAST((Leave * 100) / TotalLeave AS DECIMAL(6,2)) END AS LvPer " & _
                                   "FROM hrstation_master "
                        Case enViewDetailsByIdx.COSTCENTER_WISE
                            StrQ = "SELECT " & _
                                   "     costcenterunkid AS Id " & _
                                   "    ,costcentercode AS Code " & _
                                   "    ,costcentername AS Name " & _
                                   "    ,ISNULL(Leave,0) Leave " & _
                                   "    ,ISNULL(TotalLeave,0) TotalLeave " & _
                                   "    ,CASE WHEN ISNULL(Leave,0) = 0 OR ISNULL(TotalLeave,0) = 0 THEN 0 ELSE CAST((Leave * 100) / TotalLeave AS DECIMAL(6,2)) END AS LvPer " & _
                                   "FROM prcostcenter_master "
                        Case enViewDetailsByIdx.SECTION_WISE
                            StrQ = "SELECT " & _
                                   "     sectionunkid AS Id " & _
                                   "    ,code AS Code " & _
                                   "    ,name AS Name " & _
                                   "    ,ISNULL(Leave,0) Leave " & _
                                   "    ,ISNULL(TotalLeave,0) TotalLeave " & _
                                   "    ,CASE WHEN ISNULL(Leave,0) = 0 OR ISNULL(TotalLeave,0) = 0 THEN 0 ELSE CAST((Leave * 100) / TotalLeave AS DECIMAL(6,2)) END AS LvPer " & _
                                   "FROM hrsection_master "
                        Case enViewDetailsByIdx.UNIT_WISE
                            StrQ = "SELECT " & _
                                   "     unitunkid AS Id " & _
                                   "    ,code AS Code " & _
                                   "    ,name AS Name " & _
                                   "    ,ISNULL(Leave,0) Leave " & _
                                   "    ,ISNULL(TotalLeave,0) TotalLeave " & _
                                   "    ,CASE WHEN ISNULL(Leave,0) = 0 OR ISNULL(TotalLeave,0) = 0 THEN 0 ELSE CAST((Leave * 100) / TotalLeave AS DECIMAL(6,2)) END AS LvPer " & _
                                   "FROM hrunit_master "
                    End Select

                    StrQ &= "JOIN " & _
                            "( " & _
                            "    SELECT "

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'Select Case intViewDetailByIdx
                    '    Case enViewDetailsByIdx.DEPARTMENT_WISE
                    '        StrQ &= " hremployee_master.departmentunkid AS Id "
                    '    Case enViewDetailsByIdx.BRANCH_WISE
                    '        StrQ &= " hremployee_master.stationunkid AS Id "
                    '    Case enViewDetailsByIdx.COSTCENTER_WISE
                    '        StrQ &= " hremployee_master.costcenterunkid AS Id "
                    '    Case enViewDetailsByIdx.SECTION_WISE
                    '        StrQ &= " hremployee_master.sectionunkid AS Id "
                    '    Case enViewDetailsByIdx.UNIT_WISE
                    '        StrQ &= " hremployee_master.unitunkid AS Id "
                    'End Select
                    'StrQ &= ",CAST(COUNT(*)AS DECIMAL(6,2)) Leave " & _
                    '        ",(SELECT " & _
                    '        "   COUNT(leaveissuetranunkid) " & _
                    '        "  FROM lvleaveIssue_tran " & _
                    '        "   JOIN lvleaveissue_master on lvleaveissue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                    '        "  WHERE DATEPART(MONTH,leavedate) = @MonthId AND lvleaveIssue_tran.isvoid = 0 And lvleaveissue_master.leavetypeunkid = " & intViewUnkid & "  " & _
                    '        " ) TotalLeave " & _
                    '        "FROM lvleaveIssue_master " & _
                    '        "   JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 " & _
                    '        "   JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
                    '        "WHERE DATEPART(MONTH,lvleaveIssue_tran.leavedate) = @MonthId AND dbo.lvleaveIssue_master.leavetypeunkid = " & intViewUnkid

                    'Select Case intViewDetailByIdx
                    '    Case enViewDetailsByIdx.DEPARTMENT_WISE
                    '        StrQ &= "    Group BY hremployee_master.departmentunkid " & _
                    '                ")AS Sal_Det ON Sal_Det.Id = hrdepartment_master.departmentunkid "
                    '    Case enViewDetailsByIdx.BRANCH_WISE
                    '        StrQ &= "    Group BY hremployee_master.stationunkid " & _
                    '                ")AS Sal_Det ON Sal_Det.Id = hrstation_master.stationunkid "
                    '    Case enViewDetailsByIdx.COSTCENTER_WISE
                    '        StrQ &= "    Group BY hremployee_master.costcenterunkid " & _
                    '                ")AS Sal_Det ON Sal_Det.Id = prcostcenter_master.costcenterunkid "
                    '    Case enViewDetailsByIdx.SECTION_WISE
                    '        StrQ &= "    Group BY hremployee_master.sectionunkid " & _
                    '                ")AS Sal_Det ON Sal_Det.Id = hrsection_master.sectionunkid "
                    '    Case enViewDetailsByIdx.UNIT_WISE
                    '        StrQ &= "    Group BY hremployee_master.unitunkid " & _
                    '                ")AS Sal_Det ON Sal_Det.Id = hrunit_master.unitunkid "
                    'End Select

                    Select Case intViewDetailByIdx
                        Case enViewDetailsByIdx.DEPARTMENT_WISE
                            StrQ &= " Alloc.departmentunkid AS Id "
                        Case enViewDetailsByIdx.BRANCH_WISE
                            StrQ &= " Alloc.stationunkid AS Id "
                        Case enViewDetailsByIdx.COSTCENTER_WISE
                            StrQ &= " CC.costcenterunkid AS Id "
                        Case enViewDetailsByIdx.SECTION_WISE
                            StrQ &= " Alloc.sectionunkid AS Id "
                        Case enViewDetailsByIdx.UNIT_WISE
                            StrQ &= " Alloc.unitunkid AS Id "
                    End Select



                    StrQ &= ",CAST(COUNT(*)AS DECIMAL(6,2)) Leave " & _
                            ",(SELECT " & _
                            "   COUNT(leaveissuetranunkid) " & _
                            "  FROM lvleaveIssue_tran " & _
                            "   JOIN lvleaveissue_master on lvleaveissue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                            "  WHERE DATEPART(MONTH,leavedate) = @MonthId AND lvleaveIssue_tran.isvoid = 0 And lvleaveissue_master.leavetypeunkid = " & intViewUnkid & "  " & _
                            " ) TotalLeave " & _
                            "FROM lvleaveIssue_master " & _
                            "   JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 " & _
                            "   JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid "
                    Select Case intViewDetailByIdx
                        Case enViewDetailsByIdx.DEPARTMENT_WISE
                            StrQ &= "LEFT JOIN " & _
                                    "(                      " & _
                                    "    SELECT " & _
                                    "         departmentunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                    "    FROM hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                        Case enViewDetailsByIdx.BRANCH_WISE
                            StrQ &= "LEFT JOIN  " & _
                                    "(          " & _
                                    "    SELECT " & _
                                    "         stationunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                    "    FROM hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                        Case enViewDetailsByIdx.COSTCENTER_WISE
                            StrQ &= "LEFT JOIN " & _
                                    "( " & _
                                    "    SELECT " & _
                                    "         cctranheadvalueid AS costcenterunkid" & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                    "    FROM hremployee_cctranhead_tran " & _
                                    "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                                    "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                    ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "
                        Case enViewDetailsByIdx.SECTION_WISE
                            StrQ &= "LEFT JOIN " & _
                                    "(                      " & _
                                    "    SELECT " & _
                                    "         sectionunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                    "    FROM hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                        Case enViewDetailsByIdx.UNIT_WISE
                            StrQ &= "LEFT JOIN " & _
                                    "(                      " & _
                                    "    SELECT " & _
                                    "         unitunkid " & _
                                    "        ,employeeunkid " & _
                                    "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                    "    FROM hremployee_transfer_tran " & _
                                    "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                    ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                    End Select
                    StrQ &= "WHERE DATEPART(MONTH,lvleaveIssue_tran.leavedate) = @MonthId AND dbo.lvleaveIssue_master.leavetypeunkid = " & intViewUnkid


                    Select Case intViewDetailByIdx
                        Case enViewDetailsByIdx.DEPARTMENT_WISE
                            StrQ &= "    Group BY Alloc.departmentunkid " & _
                                    ")AS Sal_Det ON Sal_Det.Id = hrdepartment_master.departmentunkid "
                        Case enViewDetailsByIdx.BRANCH_WISE
                            StrQ &= "    Group BY Alloc.stationunkid " & _
                                    ")AS Sal_Det ON Sal_Det.Id = hrstation_master.stationunkid "
                        Case enViewDetailsByIdx.COSTCENTER_WISE
                            StrQ &= "    Group BY CC.costcenterunkid " & _
                                    ")AS Sal_Det ON Sal_Det.Id = prcostcenter_master.costcenterunkid "
                        Case enViewDetailsByIdx.SECTION_WISE
                            StrQ &= "    Group BY Alloc.sectionunkid " & _
                                    ")AS Sal_Det ON Sal_Det.Id = hrsection_master.sectionunkid "
                        Case enViewDetailsByIdx.UNIT_WISE
                            StrQ &= "    Group BY Alloc.unitunkid " & _
                                    ")AS Sal_Det ON Sal_Det.Id = hrunit_master.unitunkid "
                    End Select
                    'Shani(24-Aug-2015) -- End
                    StrQ &= " WHERE isactive = 1 "

                    'StrQ = "SELECT " & _
                    '       "     hrdepartment_master.departmentunkid " & _
                    '       "    ,Code " & _
                    '       "    ,ISNULL(NAME,'') DeptName " & _
                    '       "    ,ISNULL(Leave,0) Leave " & _
                    '       "    ,ISNULL(TotalLeave,0) TotalLeave " & _
                    '       "    ,CASE WHEN ISNULL(Leave,0) = 0 OR ISNULL(TotalLeave,0) = 0 THEN 0 " & _
                    '       "     ELSE CAST((Leave * 100) / TotalLeave AS DECIMAL(6,2)) END AS LvPer " & _
                    '       "FROM hrdepartment_master " & _
                    '       "JOIN " & _
                    '       "( " & _
                    '       "    SELECT " & _
                    '       "         hremployee_master.departmentunkid " & _
                    '       "        ,CAST(COUNT(*)AS DECIMAL(6,2)) DeptLeave " & _
                    '       "        ,(SELECT COUNT(*) FROM lvleaveIssue_tran WHERE DATEPART(MONTH,leavedate) = @MonthId AND isvoid = 0) TotalLeave " & _
                    '       "    FROM lvleaveIssue_master " & _
                    '       "        JOIN lvleaveIssue_tran ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid AND lvleaveIssue_tran.isvoid = 0 " & _
                    '       "        JOIN hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid " & _
                    '       "    WHERE DATEPART(MONTH,lvleaveIssue_tran.leavedate) = @MonthId " & _
                    '       "GROUP BY hremployee_master.departmentunkid " & _
                    '       ") AS dept ON dept.departmentunkid = hrdepartment_master.departmentunkid " & _
                    '       "WHERE hrdepartment_master.isactive = 1 "
                    objDataOperation.AddParameter("@MonthId", SqlDbType.Int, eZeeDataType.INT_SIZE, intDetailValue)

                Case enViewDetails.TRAINING_COST
                    StrQ = "SELECT " & _
                            "     CAST(CASE WHEN Fees = 0 OR TotCosting = 0 THEN 0 ELSE (Fees*100)/TotCosting END AS DECIMAL(5,2)) AS Fees " & _
                            "    ,CAST(CASE WHEN Misc = 0 OR TotCosting = 0 THEN 0 ELSE (Misc*100)/TotCosting END AS DECIMAL(5,2)) AS Misc " & _
                            "    ,CAST(CASE WHEN Accomodation = 0 OR TotCosting = 0 THEN 0 ELSE (Accomodation*100)/TotCosting END AS DECIMAL(5,2)) AS Accomodation " & _
                            "    ,CAST(CASE WHEN Allowance = 0 OR TotCosting = 0 THEN 0 ELSE (Allowance*100)/TotCosting END AS DECIMAL(5,2)) AS Allowance " & _
                            "    ,CAST(CASE WHEN Exam = 0 OR TotCosting = 0 THEN 0 ELSE (Exam*100)/TotCosting END AS DECIMAL(5,2)) AS Exam " & _
                            "    ,CAST(CASE WHEN Travel = 0 OR TotCosting = 0 THEN 0 ELSE (Travel*100)/TotCosting END AS DECIMAL(5,2)) AS Travel " & _
                            "    ,CAST(CASE WHEN Material = 0 OR TotCosting = 0 THEN 0 ELSE (Material*100)/TotCosting END AS DECIMAL(5,2)) AS Material " & _
                            "    ,CAST(CASE WHEN Meals = 0 OR TotCosting = 0 THEN 0 ELSE (Meals*100)/TotCosting END AS DECIMAL(5,2)) AS Meals " & _
                            "FROM " & _
                            "( " & _
                            "   SELECT " & _
                            "     trainingschedulingunkid AS TrainingId " & _
                            "    ,ISNULL(fees,0) AS Fees " & _
                            "    ,ISNULL(misc,0) AS Misc " & _
                            "    ,ISNULL(accomodation,0) AS Accomodation " & _
                            "    ,ISNULL(allowance,0) AS Allowance " & _
                            "    ,ISNULL(exam,0) AS Exam " & _
                            "    ,ISNULL(travel,0) AS Travel " & _
                            "    ,ISNULL(material,0) AS Material " & _
                            "    ,ISNULL(meals,0) AS Meals " & _
                            "    ,ISNULL(fees,0)+ISNULL(misc,0)+ISNULL(accomodation,0)+ " & _
                            "     ISNULL(allowance,0)+ISNULL(exam,0)+ISNULL(travel,0)+ " & _
                            "     ISNULL(material,0)+ISNULL(meals,0) AS TotCosting " & _
                            "FROM hrtraining_scheduling " & _
                            "WHERE iscancel = 0 AND isvoid = 0 " & _
                            "    AND yearunkid = @YearId " & _
                            "    AND trainingschedulingunkid = @TrainingId " & _
                            ") AS TPer "

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, FinancialYear._Object._YearUnkid)
                    objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearUnkId)
                    'Shani(24-Aug-2015) -- End

                    objDataOperation.AddParameter("@TrainingId", SqlDbType.Int, eZeeDataType.INT_SIZE, intDetailValue)
            End Select

            dsDetals = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If intDetailIdx = enViewDetails.TRAINING_COST And dsDetals.Tables(0).Rows.Count > 0 Then
                Dim dtTable As New DataTable("TC")
                dtTable.Columns.Add("TName", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("TPercentage", System.Type.GetType("System.Double")).DefaultValue = 0
                For Each dtCol As DataColumn In dsDetals.Tables(0).Columns
                    Dim dtRow As DataRow = dtTable.NewRow
                    dtRow.Item("TName") = dtCol.ColumnName.ToString
                    dtRow.Item("TPercentage") = dsDetals.Tables(0).Rows(0)(dtCol.ColumnName)
                    dtTable.Rows.Add(dtRow)
                Next
                If dsDetals.Tables.Count > 0 Then
                    dsDetals.Tables.Clear()
                    dsDetals.Tables.Add(dtTable)
                End If
            End If

            Return dsDetals

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Detail_Information", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function Leave_Analysis(Optional ByVal mstrList As String = "List") As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dtTable As DataTable
        Try
            objDataOperation = New clsDataOperation

            dtTable = New DataTable(mstrList)

            dtTable.Columns.Add("MId", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("MName", System.Type.GetType("System.String")).DefaultValue = ""

            StrQ = "SELECT " & _
                   " leavetypeunkid AS Id " & _
                   ",lvleavetype_master.leavetypecode AS LCode " & _
                   ",lvleavetype_master.leavename AS LName " & _
                   ",color AS ColorId " & _
                   "FROM lvleavetype_master WHERE isactive = 1 "
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                dtTable.Columns.Add("Column" & dtRow.Item("Id").ToString, System.Type.GetType("System.String")).DefaultValue = ""
            Next

            'For i As Integer = 1 To 12
            '    StrQ = "SELECT " & _
            '           "  leavetypeunkid AS LId " & _
            '           " ,COUNT(leaveissuetranunkid) TotalLeave " & _
            '           "FROM lvleaveIssue_tran " & _
            '           "    JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
            '           "WHERE Month(leavedate) = '" & i.ToString & "' AND YEAR(leavedate) = '" & ConfigParameter._Object._CurrentDateAndTime.Year & "' AND lvleaveIssue_tran.isvoid = 0 " & _
            '           "GROUP BY lvleaveIssue_master.leavetypeunkid "

            '    dsList = objDataOperation.ExecQuery(StrQ, "List")

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            '    Dim dRow As DataRow = dtTable.NewRow

            '    dRow.Item("MId") = i.ToString
            '    dRow.Item("MName") = MonthName(i, True)

            '    For Each dtRow As DataRow In dsList.Tables(0).Rows
            '        dRow.Item("Column" & dtRow.Item("LId")) = dtRow.Item("TotalLeave")
            '    Next
            '    dtTable.Rows.Add(dRow)
            'Next


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim iMonth As Integer = Month(FinancialYear._Object._Database_Start_Date)
            Dim iMonth As Integer = Month(mdtDatabaseStartDate)
            'Shani(24-Aug-2015) -- End


            For i As Integer = 1 To 12
                If iMonth > 12 Then iMonth = 1

                'Pinkal (16-May-2017) -- Start
                'Enhancement - Solved Problem in Leave & TnA Graph Detail showing Wrong in Finca Zambia .

                'StrQ = "SELECT " & _
                '       "  leavetypeunkid AS LId " & _
                '       " ,COUNT(leaveissuetranunkid) TotalLeave " & _
                '       "FROM lvleaveIssue_tran " & _
                '       "    JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                '       "WHERE Month(leavedate) = '" & iMonth.ToString & "' AND YEAR(leavedate) <= '" & mdtDatabaseEndDate.Year & "' AND lvleaveIssue_tran.isvoid = 0 " & _
                '       "GROUP BY lvleaveIssue_master.leavetypeunkid "

                StrQ = "SELECT " & _
                       "  leavetypeunkid AS LId " & _
                       " ,COUNT(leaveissuetranunkid) TotalLeave " & _
                      " FROM lvleaveIssue_tran " & _
                       "    JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                      " WHERE Month(leavedate) = '" & iMonth.ToString & "' AND YEAR(leavedate) <= '" & mdtDatabaseEndDate.Year & "' AND lvleaveIssue_master.isvoid = 0 AND lvleaveIssue_tran.isvoid = 0 " & _
                      " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) BETWEEN '" & eZeeDate.convertDate(mdtDatabaseStartDate) & "' AND  '" & eZeeDate.convertDate(mdtDatabaseEndDate) & "' " & _
                      " GROUP BY lvleaveIssue_master.leavetypeunkid "

                'Pinkal (16-May-2017) -- End

                'Shani(24-Aug-2015) -- [FinancialYear._Object._Database_End_Date.Year-->mdtDatabaseEndDate]
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dRow As DataRow = dtTable.NewRow

                dRow.Item("MId") = iMonth.ToString
                dRow.Item("MName") = MonthName(iMonth, True)

                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    'Pinkal (16-May-2017) -- Start
                    'Enhancement - Solved Problem in Leave & TnA Graph Detail showing Wrong in Finca Zambia .
                    If dtTable.Columns.Contains("Column" & dtRow.Item("LId").ToString()) Then
                    dRow.Item("Column" & dtRow.Item("LId")) = dtRow.Item("TotalLeave")
                    End If
                    'Pinkal (16-May-2017) -- End
                Next
                dtTable.Rows.Add(dRow)
                iMonth += 1
            Next


            dsList = New DataSet

            dsList.Tables.Add(dtTable.Copy)

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Leave_Analysis", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function Leave_Grid(Optional ByVal mstrList As String = "List") As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dtTable As DataTable
        mDic_Color = New Dictionary(Of Integer, Integer)

        Try
            objDataOperation = New clsDataOperation

            dtTable = New DataTable(mstrList)

            dtTable.Columns.Add("LId", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("LCode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("LName", System.Type.GetType("System.String")).DefaultValue = ""

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'For i As Integer = 1 To 12
            '    dtTable.Columns.Add(MonthName(i, True), System.Type.GetType("System.String")).DefaultValue = ""
            'Next

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim iMonth As Integer = Month(FinancialYear._Object._Database_Start_Date)
            Dim iMonth As Integer = Month(mdtDatabaseStartDate)
            'Shani(24-Aug-2015) -- End


            For i As Integer = 1 To 12
                If iMonth > 12 Then iMonth = 1
                dtTable.Columns.Add(MonthName(iMonth, True), System.Type.GetType("System.String")).DefaultValue = ""
                iMonth += 1
            Next

            'S.SANDEEP [ 28 FEB 2012 ] -- END

            StrQ = "SELECT " & _
                   " leavetypeunkid AS Id " & _
                   ",lvleavetype_master.leavetypecode AS LCode " & _
                   ",lvleavetype_master.leavename AS LName " & _
                   ",color AS ColorId " & _
                   "FROM lvleavetype_master WHERE isactive = 1 "

            dsList = objDataOperation.ExecQuery(StrQ, "mList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables(0).Rows
                Dim dtRow As DataRow = dtTable.NewRow

                dtRow.Item("LId") = dRow.Item("Id")
                dtRow.Item("LCode") = dRow.Item("LCode")
                dtRow.Item("LName") = dRow.Item("LName")
                mDic_Color.Add(CInt(dRow.Item("Id")), CInt(dRow.Item("ColorId")))

                dtTable.Rows.Add(dtRow)
            Next

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'For i As Integer = 1 To 12
            '    StrQ = "SELECT " & _
            '           "  leavetypeunkid AS LId " & _
            '           " ,COUNT(leaveissuetranunkid) TotalLeave " & _
            '           "FROM lvleaveIssue_tran " & _
            '           "    JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
            '           "WHERE Month(leavedate) = '" & i.ToString & "' AND YEAR(leavedate) = '" & ConfigParameter._Object._CurrentDateAndTime.Year & "' AND lvleaveIssue_tran.isvoid = 0 " & _
            '           "GROUP BY lvleaveIssue_master.leavetypeunkid "

            '    dsList = objDataOperation.ExecQuery(StrQ, "Lst")

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            '    For Each dRow As DataRow In dsList.Tables(0).Rows
            '        Dim dtTemp() As DataRow = dtTable.Select("LId = '" & dRow.Item("LId") & "'")
            '        If dtTemp.Length > 0 Then
            '            dtTemp(0)(MonthName(i, True)) = dRow.Item("TotalLeave")
            '        Else
            '            dtTemp(0)(MonthName(i, True)) = ""
            '        End If
            '        dtTable.AcceptChanges()
            '    Next
            'Next

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'iMonth = FinancialYear._Object._Database_Start_Date.Month
            iMonth = mdtDatabaseStartDate.Month
            'Shani(24-Aug-2015) -- End


            For i As Integer = 1 To 12
                If iMonth > 12 Then iMonth = 1


                'Pinkal (16-May-2017) -- Start
                'Enhancement - Solved Problem in Leave & TnA Graph Detail showing Wrong in Finca Zambia .

                'StrQ = "SELECT " & _
                '       "  leavetypeunkid AS LId " & _
                '       " ,COUNT(leaveissuetranunkid) TotalLeave " & _
                '       " FROM lvleaveIssue_tran " & _
                '       "    JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                '       " WHERE Month(leavedate) = '" & iMonth.ToString & "' AND YEAR(leavedate) <= '" & mdtDatabaseEndDate.Year & "' AND lvleaveIssue_tran.isvoid = 0 " & _
                '       " GROUP BY lvleaveIssue_master.leavetypeunkid "

                StrQ = "SELECT " & _
                       "  leavetypeunkid AS LId " & _
                       " ,COUNT(leaveissuetranunkid) TotalLeave " & _
                   " FROM lvleaveIssue_tran " & _
                       "    JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                   " WHERE Month(leavedate) = '" & iMonth.ToString & "' AND YEAR(leavedate) <= '" & mdtDatabaseEndDate.Year & "' AND lvleaveIssue_master.isvoid =  0 AND lvleaveIssue_tran.isvoid = 0 " & _
                   " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) BETWEEN '" & eZeeDate.convertDate(mdtDatabaseStartDate) & "' AND  '" & eZeeDate.convertDate(mdtDatabaseEndDate) & "' " & _
                   " GROUP BY lvleaveIssue_master.leavetypeunkid "

                'Pinkal (16-May-2017) -- End


                'Shani(24-Aug-2015) -- [FinancialYear._Object._Database_End_Date-->mdtDatabaseEndDate]
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                dsList = objDataOperation.ExecQuery(StrQ, "Lst")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For Each dRow As DataRow In dsList.Tables(0).Rows

                    'Pinkal (16-May-2017) -- Start
                    'Enhancement - Solved Problem in Leave & TnA Graph Detail showing Wrong in Finca Zambia .

                    Dim dtTemp() As DataRow = dtTable.Select("LId = '" & dRow.Item("LId") & "'")
                    If dtTemp.Length > 0 Then
                        dtTemp(0)(MonthName(iMonth, True)) = dRow.Item("TotalLeave")
                    End If

                    'Pinkal (16-May-2017) -- End

                    dtTable.AcceptChanges()
                Next
                iMonth += 1
            Next
            'S.SANDEEP [ 28 FEB 2012 ] -- END


            

            dsList = New DataSet

            dsList.Tables.Add(dtTable.Copy)

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Leave_Grid", mstrModuleName)
        End Try
        Return Nothing
    End Function

    Public Function Leave_Grid_SelfService(ByVal mblnApplyAccessFilter As Boolean, Optional ByVal mstrAllocationFilter As String = "", Optional ByVal mstrList As String = "List") As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dtTable As DataTable
        mDic_Color = New Dictionary(Of Integer, Integer)

        Try
            objDataOperation = New clsDataOperation

            Dim dtDatabase As New DataTable
            dtDatabase.Columns.Add("database_name")
            dtDatabase.Columns.Add("start_date")
            dtDatabase.Columns.Add("end_date")
            dtDatabase.Columns.Add("yearunkid")
            dtDatabase.Columns.Add("dbstdate")


            'StrQ = "SELECT " & _
            '          "database_name," & _
            '          "DATEADD(MONTH, -12, @start_date) as start_date " & _
            '          ",end_date " & _
            '          ",yearunkid " & _
            '        "FROM hrmsConfiguration..cffinancial_year_tran " & _
            '        "WHERE CONVERT(CHAR(8), DATEADD(MONTH, -12, @start_date), 112)  BETWEEN  CONVERT(CHAR(8), start_date, 112) AND CONVERT(CHAR(8), end_date, 112) " & _
            '        "and companyunkid = " & mintCompanyUnkid & " "


            'S.SANDEEP |06-DEC-2022| -- START
            'ISSUE : DATA WAS NOT COMING AS FULL DATE WAS CHECKED AND IT WAS NOT RETURNING DUE TO IN BETWEEN DATES {2 TO 27 OR 30}, THEN ADDED YEAR FILTER INSTEAD OF DATE

            'StrQ = "SELECT " & _
            '       "     database_name " & _
            '       "    ,DATEADD(MONTH, -12, @start_date) as start_date " & _
            '       "    ,end_date " & _
            '       "    ,yearunkid " & _
            '       "    ,start_date AS dbstdate " & _
            '        "FROM hrmsConfiguration..cffinancial_year_tran " & _
            '       "WHERE (CONVERT(CHAR(8), start_date, 112) BETWEEN CONVERT(CHAR(8), DATEADD(MONTH, -12, @start_date), 112) AND @start_date " & _
            '       "OR CONVERT(CHAR(8), end_date, 112) BETWEEN CONVERT(CHAR(8), DATEADD(MONTH, -12, @start_date), 112) AND @start_date) " & _
            '       "AND companyunkid = " & mintCompanyUnkid & " ORDER BY hrmsConfiguration..cffinancial_year_tran.start_date ASC "


            StrQ = "SELECT " & _
                   "     database_name " & _
                   "    ,DATEADD(MONTH, -12, @start_date) as start_date " & _
                   "    ,end_date " & _
                   "    ,yearunkid " & _
                   "    ,start_date AS dbstdate " & _
                    "FROM hrmsConfiguration..cffinancial_year_tran " & _
                   "WHERE companyunkid = " & mintCompanyUnkid & " " & _
                   " AND (DATEPART(YEAR,start_date) BETWEEN DATEPART(YEAR,DATEADD(MONTH, -11, @start_date)) AND DATEPART(YEAR,@start_date) " & _
                   "   OR DATEPART(YEAR,end_date) BETWEEN DATEPART(YEAR,DATEADD(MONTH, -11, @start_date))  AND DATEPART(YEAR,@start_date)) " & _
                   "ORDER BY hrmsConfiguration..cffinancial_year_tran.start_date ASC "

            'S.SANDEEP |06-DEC-2022| -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDateAndTime)

            dsList = objDataOperation.ExecQuery(StrQ, "FinYear")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("FinYear").Rows
                Dim dr As DataRow = dtDatabase.NewRow()
                dr("database_name") = dRow("database_name")

                If dRow("yearunkid") = mintYearUnkId Then
                    dr("start_date") = New DateTime(CDate(dRow("start_date")).Year, CDate(dRow("start_date")).Month, 1)
                Else
                    dr("start_date") = New DateTime(CDate(dRow("start_date")).Year, CDate(dRow("start_date")).Month, 1).AddMonths(1)
                End If

                dr("end_date") = dRow("end_date")
                dr("yearunkid") = dRow("yearunkid")
                dtDatabase.Rows.Add(dr)
            Next

            'If IsNothing(dsList.Tables("FinYear")) = False AndAlso dsList.Tables("FinYear").Rows.Count > 0 Then
            '    If dsList.Tables("FinYear").AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("yearunkid") = mintYearUnkId).Count = 0 Then
            '        dtDatabase.Rows.Add(mstrDatabaseName, mdtDatabaseStartDate, mdtCurrentDateAndTime, mintYearUnkId)
            '    End If
            'End If

            Dim MonthDiff As Integer
            Dim startDate As DateTime = mdtCurrentDateAndTime

            If dtDatabase.Rows.Count > 1 Then
                startDate = CDate(dtDatabase.Rows(0)("start_date"))
                MonthDiff = DateDiff(DateInterval.Month, startDate, mdtCurrentDateAndTime.AddMonths(1))
            Else
                startDate = mdtCurrentDateAndTime.AddMonths(-12)
                MonthDiff = DateDiff(DateInterval.Month, startDate, mdtCurrentDateAndTime.AddMonths(1))
            End If


            dtTable = New DataTable(mstrList)

            dtTable.Columns.Add("LId", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("LCode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("LName", System.Type.GetType("System.String")).DefaultValue = ""

            Dim iMonth As Integer = Month(startDate)

            For i As Integer = 1 To 12
                If iMonth > 12 Then iMonth = 1
                'Pinkal (01-Jun-2021)-- Start
                'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
                'dtTable.Columns.Add(MonthName(iMonth, True) + "-" + Year(startDate.AddMonths(i - 1)).ToString(), System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add(MonthName(iMonth, True) + "-" + Year(startDate.AddMonths(i - 1)).ToString(), System.Type.GetType("System.String")).DefaultValue = "0"
                'Pinkal (01-Jun-2021) -- End
                iMonth += 1
            Next

            dsList = New DataSet
            StrQ = ""
            Dim xCount As Integer = 0
            For Each drDatabase As DataRow In dtDatabase.Rows


                StrQ &= "SELECT " & _
                       " leavetypeunkid AS Id " & _
                       ",lvleavetype_master.leavetypecode AS LCode " & _
                       ",lvleavetype_master.leavename AS LName " & _
                       ",color AS ColorId " & _
                       "FROM  " & drDatabase("database_name") & "..lvleavetype_master WHERE isactive = 1 "

                If xCount <> dtDatabase.Rows.Count - 1 Then
                    StrQ &= " UNION "
                End If

                xCount += 1

            Next

            If StrQ.Trim.Length > 0 Then dsList = objDataOperation.ExecQuery(StrQ, "mList")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then
            For Each dRow As DataRow In dsList.Tables(0).Rows
                Dim dtRow As DataRow = dtTable.NewRow

                dtRow.Item("LId") = dRow.Item("Id")
                dtRow.Item("LCode") = dRow.Item("LCode")
                dtRow.Item("LName") = dRow.Item("LName")
                mDic_Color.Add(CInt(dRow.Item("Id")), CInt(dRow.Item("ColorId")))

                dtTable.Rows.Add(dtRow)
            Next
            End If
            




            'iMonth = startDate.Month
            'For i As Integer = 1 To 12
            '    If iMonth > 12 Then iMonth = 1
            '    dsList = New DataSet
            '    StrQ = ""
            '    xCount = 0
            '    For Each drDatabase As DataRow In dtDatabase.Rows

            '        StrQ &= "SELECT " & _
            '               "  leavetypeunkid AS LId " & _
            '               " ,COUNT(leaveissuetranunkid) TotalLeave " & _
            '           " FROM " & drDatabase("database_name") & "..lvleaveIssue_tran " & _
            '               "    JOIN " & drDatabase("database_name") & "..lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
            '           " WHERE Month(leavedate) = '" & iMonth.ToString & "' AND YEAR(leavedate) <= '" & startDate.AddMonths(i - 1).Year & "' AND lvleaveIssue_master.isvoid =  0 AND lvleaveIssue_tran.isvoid = 0 " & _
            '           " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) BETWEEN '" & eZeeDate.convertDate(CDate(drDatabase("start_date"))) & "' AND  '" & eZeeDate.convertDate(New Date(CDate(drDatabase("end_date")).Year, CDate(drDatabase("end_date")).Month, Date.DaysInMonth(CDate(drDatabase("end_date")).Year, CDate(drDatabase("end_date")).Month))) & "' " & _
            '           " GROUP BY lvleaveIssue_master.leavetypeunkid "

            '        If xCount <> dtDatabase.Rows.Count - 1 Then
            '            StrQ &= " UNION "
            '        End If

            '        xCount += 1

            '    Next

            '    If StrQ.Trim.Length > 0 Then dsList = objDataOperation.ExecQuery(StrQ, "Lst")

            '   


            '    If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 Then

            '        'Pinkal (01-Jun-2021)-- Start
            '        'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
            '        'For Each dRow As DataRow In dsList.Tables(0).Rows
            '        '    Dim dtTemp() As DataRow = dtTable.Select("LId = '" & dRow.Item("LId") & "'")
            '        '    If dtTemp.Length > 0 Then
            '        '        dtTemp(0)(MonthName(iMonth, True) + "-" + Year(startDate.AddMonths(i - 1)).ToString()) = dRow.Item("TotalLeave")
            '        '    End If
            '        '    dtTable.AcceptChanges()
            '        'Next
            '        dsList.Tables(0).AsEnumerable().ToList.ForEach(Function(x) UpdateLeaveData(dtTable, x, iMonth, i, startDate))
            '        'Pinkal (01-Jun-2021) -- End

            '    End If

            '    iMonth += 1
            'Next

            dsList.Clear()
            dsList = Nothing
                StrQ = ""
                xCount = 0

            'Pinkal (09-Aug-2021)-- Start
            'NMB New UI Enhancements.

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String

                For Each drDatabase As DataRow In dtDatabase.Rows

                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""

                'Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, CDate(drDatabase("end_date")), CDate(drDatabase("end_date")), , , drDatabase("database_name").ToString())

                If mblnApplyAccessFilter Then
                    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, CDate(drDatabase("end_date")), True, drDatabase("database_name"), mintuserUnkid, mintCompanyUnkid, drDatabase("yearunkid"), mstrUserModeSetting)
                Else
                    Call GetAdvanceFilterQry(xUACQry, CDate(drDatabase("end_date")), drDatabase("database_name"))
                End If

                'StrQ &= "SELECT " & _
                '       "  leavetypeunkid AS LId " & _
                '       " ,COUNT(leaveissuetranunkid) TotalLeave " & _
                '       " , MONTH(leavedate) AS Mnth " & _
                '       ",CONVERT(CHAR(3), leavedate, 0) + '-' + CONVERT(CHAR(4),YEAR(leavedate)) AS LMNth " & _
                '       " FROM " & drDatabase("database_name") & "..lvleaveIssue_tran " & _
                '       " JOIN " & drDatabase("database_name") & "..lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                '       " WHERE  lvleaveIssue_master.isvoid =  0 AND lvleaveIssue_tran.isvoid = 0 " & _
                '       " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) BETWEEN '" & eZeeDate.convertDate(CDate(drDatabase("start_date"))) & "' AND  '" & eZeeDate.convertDate(New Date(CDate(drDatabase("end_date")).Year, CDate(drDatabase("end_date")).Month, Date.DaysInMonth(CDate(drDatabase("end_date")).Year, CDate(drDatabase("end_date")).Month))) & "' " & _
                '       " GROUP BY lvleaveIssue_master.leavetypeunkid,MONTH(leavedate),CONVERT(CHAR(3), leavedate, 0) + '-' + CONVERT(CHAR(4),YEAR(leavedate)) "


                    StrQ &= "SELECT " & _
                           "  leavetypeunkid AS LId " & _
                           " ,COUNT(leaveissuetranunkid) TotalLeave " & _
                       " , MONTH(leavedate) AS Mnth " & _
                       ",CONVERT(CHAR(3), leavedate, 0) + '-' + CONVERT(CHAR(4),YEAR(leavedate)) AS LMNth " & _
                       " FROM " & drDatabase("database_name") & "..lvleaveIssue_tran " & _
                            " JOIN " & drDatabase("database_name") & "..lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                            " JOIN " & drDatabase("database_name") & "..hremployee_master ON hremployee_master.employeeunkid = lvleaveIssue_master.employeeunkid "

                'If xDateJoinQry.Trim.Length > 0 Then
                '    StrQ &= xDateJoinQry & " "
                'End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= " WHERE  lvleaveIssue_master.isvoid =  0 AND lvleaveIssue_tran.isvoid = 0 " & _
                            " AND CONVERT(CHAR(8),lvleaveIssue_tran.leavedate,112) BETWEEN '" & eZeeDate.convertDate(CDate(drDatabase("start_date"))) & "' AND  '" & eZeeDate.convertDate(New Date(CDate(drDatabase("end_date")).Year, CDate(drDatabase("end_date")).Month, Date.DaysInMonth(CDate(drDatabase("end_date")).Year, CDate(drDatabase("end_date")).Month))) & "' "

                'If xDateFilterQry.Trim.Length > 0 Then
                '    StrQ &= xDateFilterQry & " "
                'End If

                If mstrAllocationFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAllocationFilter
                End If

                StrQ &= " GROUP BY lvleaveIssue_master.leavetypeunkid,MONTH(leavedate),CONVERT(CHAR(3), leavedate, 0) + '-' + CONVERT(CHAR(4),YEAR(leavedate)) "


                    If xCount <> dtDatabase.Rows.Count - 1 Then
                        StrQ &= " UNION "
                    End If
                xCount += 1
            Next

            'Pinkal (09-Aug-2021) -- End

            'Pinkal (01-Jun-2021)-- Start
            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
            If StrQ.Trim.Length > 0 Then
                StrQ &= " ORDER BY lvleaveIssue_master.leavetypeunkid,MONTH(leavedate) "
            End If
            'Pinkal (01-Jun-2021) -- End



                If StrQ.Trim.Length > 0 Then dsList = objDataOperation.ExecQuery(StrQ, "Lst")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Dim xRowCount As Integer = 0
            Dim xLeaveTypeId As Integer = 0
            Dim dtTemp As DataTable = Nothing

                For Each dRow As DataRow In dsList.Tables(0).Rows
                If xLeaveTypeId <> CInt(dRow.Item("LId")) Then
                    xLeaveTypeId = CInt(dRow.Item("LId"))
                    xRowCount += 1
                    dtTemp = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("LId") = xLeaveTypeId).CopyToDataTable()
                End If
                If dtTemp IsNot Nothing AndAlso dtTemp.Rows.Count > 0 Then
                    'For xRow As Integer = 0 To dtTemp.Columns.Count - 1
                    '    If dtTable.Columns(xRow).ColumnName.ToString() = dRow("LMNth").ToString() Then
                    '        dtTable.Rows(xRowCount - 1)(xRow) = dRow("TotalLeave")
                    '        Exit For
                    '    End If
                    'Next
                    Dim mstrMonth As String = dRow("LMNth").ToString()
                    Dim xColumn = dtTemp.Columns.Cast(Of DataColumn).Where(Function(x) x.ColumnName = mstrMonth).Select(Function(x) x.ColumnName)
                    If xColumn IsNot Nothing AndAlso xColumn.Count > 0 Then
                        dtTable.Rows(xRowCount - 1)(xColumn(0).ToString()) = dRow("TotalLeave")
                    End If
                End If
                dtTable.AcceptChanges()
            Next

            dsList = New DataSet

            dsList.Tables.Add(dtTable.Copy)

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Leave_Grid_SelfService", mstrModuleName)
        End Try
        Return Nothing
    End Function

    Public Function Get_Login_Summary(ByVal mdtDate As Date, Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try

            objDataOperation = New clsDataOperation

            'Pinkal (16-May-2017) -- Start
            'Enhancement - Solved Problem in Leave & TnA Graph Detail showing Wrong in Finca Zambia .

            'StrQ = "SELECT " & _
            '        "    tnashift_master.shiftunkid AS Sid " & _
            '        "   ,ISNULL(tnashift_master.shiftcode,'') AS SCode " & _
            '        "	,ISNULL(tnashift_master.shiftname, '') AS Shift " & _
            '        "	,COUNT(employeeunkid) AS TotEmp " & _
            '        "	,COUNT(EmpId) + COUNT(LempId) AS TotPresent " & _
            '        "   ,COUNT(employeeunkid) - ( COUNT(EmpId) + COUNT(LempId) ) AS TotAbsent " & _
            '        "FROM tnashift_master " & _
            '        "JOIN " & _
            '        "	(SELECT " & _
            '        "		 shiftunkid AS shiftunkid " & _
            '        "        ,employeeunkid AS employeeunkid " & _
            '        "      FROM hremployee_master " & _
            '        "      WHERE hremployee_master.isactive = 1 " & _
            '        "     ) AS SE ON SE.shiftunkid = tnashift_master.shiftunkid " & _
            '        "LEFT JOIN " & _
            '        "	(SELECT DISTINCT " & _
            '        "		employeeunkid AS EmpId " & _
            '        "     FROM tnalogin_tran " & _
            '        "     WHERE CONVERT(CHAR(8), logindate, 112) = @Date " & _
            '        "    ) AS LE ON LE.EmpId = SE.employeeunkid " & _
            '        "LEFT JOIN " & _
            '        "	(SELECT " & _
            '        "		lvleaveIssue_master.employeeunkid AS LempId " & _
            '        "	 FROM lvleaveIssue_tran " & _
            '        "		JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
            '        "	 WHERE CONVERT(CHAR(8), leavedate, 112) = @Date " & _
            '        "		AND ispaid = 1 " & _
            '        "		AND lvleaveIssue_tran.isvoid = 0 " & _
            '        "	) AS PE ON PE.LempId = SE.employeeunkid " & _
            '        "GROUP BY ISNULL(tnashift_master.shiftname, ''),tnashift_master.shiftunkid,ISNULL(tnashift_master.shiftcode,'') "


            StrQ = "SELECT " & _
                    "    tnashift_master.shiftunkid AS Sid " & _
                      ",ISNULL(tnashift_master.shiftcode, '') AS SCode " & _
                      ",ISNULL(tnashift_master.shiftname, '') AS Shift " & _
                      ",COUNT(employeeunkid) AS TotEmp " & _
                      ",COUNT(EmpId) AS TotPresent " & _
                      ",COUNT(employeeunkid) - COUNT(EmpId)  AS TotAbsent " & _
                      " FROM tnashift_master " & _
                      " JOIN (SELECT " & _
                      "             s.shiftunkid AS shiftunkid " & _
                      "             ,hremployee_master.employeeunkid AS employeeunkid " & _
                    "      FROM hremployee_master " & _
                      "          LEFT JOIN (SELECT " & _
                      "                                hremployee_shift_tran.employeeunkid " & _
                      "                               ,hremployee_shift_tran.shiftunkid " & _
                      "                               ,ROW_NUMBER() OVER (PARTITION BY hremployee_shift_tran.employeeunkid ORDER BY hremployee_shift_tran.effectivedate DESC) AS Rno " & _
                      "                           FROM hremployee_shift_tran " & _
                      "                           WHERE hremployee_shift_tran.isvoid = 0 AND hremployee_shift_tran.effectivedate IS NOT NULL " & _
                      "                           AND CONVERT(NVARCHAR(8), hremployee_shift_tran.effectivedate, 112) <= @Date" & _
                      "                          ) AS S ON S.employeeunkid = hremployee_master.employeeunkid AND S.Rno = 1 " & _
                      "         LEFT JOIN (SELECT " & _
                      "                                T.EmpId " & _
                      "                               ,T.EOC " & _
                      "                               ,T.LEAVING " & _
                      "                               ,T.isexclude_payroll AS IsExPayroll " & _
                      "                           FROM (SELECT " & _
                      "                                         employeeunkid AS EmpId " & _
                      "                                        ,date1 AS EOC " & _
                      "                                        ,date2 AS LEAVING " & _
                      "                                        ,effectivedate " & _
                      "                                        ,isexclude_payroll " & _
                      "                                        ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                      "                                     FROM hremployee_dates_tran " & _
                      "                                     WHERE datetypeunkid IN ('" & enEmp_Dates_Transaction.DT_TERMINATION & "') AND isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @Date " & _
                      "                                    ) AS T " & _
                      "                         WHERE T.xNo = 1) AS TRM ON TRM.EmpId = hremployee_master.employeeunkid " & _
                      "         LEFT JOIN (SELECT " & _
                      "                                 R.EmpId " & _
                      "                                 ,R.RETIRE " & _
                      "                          FROM (SELECT " & _
                      "                                         employeeunkid AS EmpId " & _
                      "                                        ,date1 AS RETIRE " & _
                      "                                        ,effectivedate " & _
                      "                                        ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                      "                                     FROM hremployee_dates_tran " & _
                      "                                     WHERE datetypeunkid IN ('" & enEmp_Dates_Transaction.DT_RETIREMENT & "') AND isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @Date " & _
                      "                                   ) AS R " & _
                      "                         WHERE R.xNo = 1) AS RET ON RET.EmpId = hremployee_master.employeeunkid " & _
                      "         LEFT JOIN (SELECT " & _
                      "                                 RH.EmpId " & _
                      "                                ,RH.REHIRE " & _
                      "                          FROM (SELECT " & _
                      "                                         employeeunkid AS EmpId " & _
                      "                                        ,reinstatment_date AS REHIRE " & _
                      "                                        ,effectivedate " & _
                      "                                        ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                      "                                     FROM hremployee_rehire_tran " & _
                      "                                     WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= @Date " & _
                      "                                    ) AS RH " & _
                      "                            WHERE RH.xNo = 1 " & _
                      "                         ) AS HIRE ON HIRE.EmpId = hremployee_master.employeeunkid " & _
                      "         WHERE 1 = 1 AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= @Date " & _
                      "         AND (CASE WHEN CONVERT(CHAR(8), TRM.LEAVING, 112) IS NULL THEN @Date ELSE CONVERT(CHAR(8), TRM.LEAVING, 112) END) >= @Date " & _
                      "         AND (CASE WHEN CONVERT(CHAR(8), RET.RETIRE, 112) IS NULL THEN @Date ELSE CONVERT(CHAR(8), RET.RETIRE, 112) END) >= @Date " & _
                      "         AND (CASE WHEN CONVERT(CHAR(8), TRM.EOC, 112) IS NULL THEN @Date ELSE CONVERT(CHAR(8), TRM.EOC, 112) END) >= @Date" & _
                    "     ) AS SE ON SE.shiftunkid = tnashift_master.shiftunkid " & _
                      " LEFT JOIN (SELECT " & _
                    "		employeeunkid AS EmpId " & _
                    "     FROM tnalogin_tran " & _
                      "                   WHERE CONVERT(CHAR(8), logindate, 112) = @Date AND isvoid = 0 " & _
                      "                  UNION " & _
                      "                  SELECT " & _
                      "                          lvleaveIssue_master.employeeunkid AS EmpId " & _
                    "	 FROM lvleaveIssue_tran " & _
                    "		JOIN lvleaveIssue_master ON lvleaveIssue_tran.leaveissueunkid = lvleaveIssue_master.leaveissueunkid " & _
                      "                  WHERE CONVERT(CHAR(8), leavedate, 112) = @Date AND ispaid = 1 AND lvleaveIssue_master.isvoid = 0 AND lvleaveIssue_tran.isvoid = 0 " & _
                      "                  ) AS LE ON LE.EmpId = SE.employeeunkid " & _
                      " GROUP BY  ISNULL(tnashift_master.shiftname, ''),tnashift_master.shiftunkid ,ISNULL(tnashift_master.shiftcode, '')"


            objDataOperation.ClearParameters()

            'Pinkal (16-May-2017) -- End

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtDate))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Login_Summary", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function Training_Costing(Optional ByVal StrList As String = "List", Optional ByVal StrDivisonValue As String = "") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   "   trainingschedulingunkid AS TrainingId " & _
                   "  ,ISNULL(cm.name,'') AS Title "
            If StrDivisonValue.Length > 0 Then
                StrQ &= "  ,(ISNULL(fees,0)+ISNULL(misc,0)+ISNULL(accomodation,0)+ " & _
                        "   ISNULL(allowance,0)+ISNULL(exam,0)+ISNULL(travel,0)+ " & _
                        "   ISNULL(material,0)+ISNULL(meals,0))/ " & CDec(StrDivisonValue) & " AS TotCosting "
            Else
                StrQ &= "  ,ISNULL(fees,0)+ISNULL(misc,0)+ISNULL(accomodation,0)+ " & _
                        "   ISNULL(allowance,0)+ISNULL(exam,0)+ISNULL(travel,0)+ " & _
                        "   ISNULL(material,0)+ISNULL(meals,0) AS TotCosting "
            End If
            StrQ &= "  ,ISNULL(fees,0) AS Fees " & _
                    "  ,ISNULL(misc,0) AS Misc " & _
                    "  ,ISNULL(accomodation,0) AS Acc " & _
                    "  ,ISNULL(allowance,0) AS allowance " & _
                    "  ,ISNULL(exam,0) AS Exam " & _
                    "  ,ISNULL(travel,0) AS Travel " & _
                    "  ,ISNULL(material,0) AS Material " & _
                    "  ,ISNULL(meals,0) AS Meals " & _
                    "FROM hrtraining_scheduling " & _
                    "LEFT JOIN cfcommon_master as CM ON CM.masterunkid = hrtraining_scheduling.course_title AND CM.mastertype = " & clsCommon_Master.enCommonMaster.TRAINING_COURSEMASTER & "  " & _
                    "WHERE iscancel = 0 AND isvoid = 0 " & _
                    "   AND yearunkid = @YearId "


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, FinancialYear._Object._YearUnkid)
            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearUnkId)
            'Shani(24-Aug-2015) -- End


            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Training_Costing", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function FillOccasion(ByVal mdtDate As Date, Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                    "    0 AS IsCheck " & _
                    "	,ECode AS ECode " & _
                    "	,EName AS EName " & _
                    "	,Occasion AS Occasion " & _
                    "	,OccId AS OccId " & _
                    "   ,EmpId AS EmpId " & _
                    "   ,ISNULL(email,'') AS Email " & _
                    "FROM " & _
                    "( " & _
                    "	SELECT " & _
                    "		 ISNULL(employeecode,'') AS ECode " & _
                    "		,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
                    "		,@Caption AS Greet " & _
                    "		,@BDay AS Occasion " & _
                    "		,1 AS OccId " & _
                    "       ,employeeunkid AS EmpId " & _
                    "       ,email AS email " & _
                    "	FROM hremployee_master " & _
                    "	WHERE isactive = 1 AND DATEPART(dd,birthdate) = DATEPART(dd, @Date) " & _
                    "		AND DATEPART(mm,birthdate) = DATEPART(mm, @Date) " & _
                    "UNION ALL " & _
                    "	SELECT " & _
                    "		 ISNULL(employeecode,'') AS ECode " & _
                    "		,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
                    "		,@Caption AS Greet " & _
                    "		,@Anniversary AS Occasion " & _
                    "		,2 AS OccId " & _
                    "       ,employeeunkid AS EmpId " & _
                    "       ,email AS email " & _
                    "	FROM hremployee_master " & _
                    "	WHERE isactive = 1 AND DATEPART(dd,anniversary_date) = DATEPART(dd, @Date) " & _
                    "		AND DATEPART(mm,anniversary_date) = DATEPART(mm, @Date) " & _
                    ") AS Occ "

            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDate)
            objDataOperation.AddParameter("@Caption", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Send Mail"))
            objDataOperation.AddParameter("@BDay", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Birthday"))
            objDataOperation.AddParameter("@Anniversary", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Anniversary"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : FillOccasion ; Module Name : " & mstrModuleName)
        Finally
        End Try
    End Function

    Public Function ViewDetailsByIdx(Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 1 AS Id,@Dept AS NAME " & _
                   " UNION SELECT 2 AS Id,@Branch AS NAME " & _
                   " UNION SELECT 3 AS Id,@CCenter AS NAME " & _
                   " UNION SELECT 4 AS Id,@Section AS NAME " & _
                   " UNION SELECT 5 AS Id,@Unit AS NAME "

            objDataOperation.AddParameter("@Dept", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Department"))
            objDataOperation.AddParameter("@Branch", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Branch"))
            objDataOperation.AddParameter("@CCenter", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Cost Center"))
            objDataOperation.AddParameter("@Section", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Section"))
            objDataOperation.AddParameter("@Unit", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Unit"))

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure : ViewDetailsByIdx ; Module Name : " & mstrModuleName)
        Finally
            dsList.Dispose()
        End Try
    End Function

    Public Function StaffTO_List(Optional ByVal dtTables As DataTable = Nothing, Optional ByVal StrList As String = "List") As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            'S.SANDEEP [ 21 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'Dim MonthDiff As Integer = DateDiff(DateInterval.Month, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date.AddMonths(1))

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim MonthDiff As Integer = DateDiff(DateInterval.Month, FinancialYear._Object._Database_Start_Date, ConfigParameter._Object._CurrentDateAndTime.AddMonths(1))
            Dim MonthDiff As Integer = DateDiff(DateInterval.Month, mdtDatabaseStartDate, mdtCurrentDateAndTime.AddMonths(1))
            'Shani(24-Aug-2015) -- End

            If MonthDiff > 12 Then MonthDiff = 12
            'S.SANDEEP [ 21 MAY 2012 ] -- END

            StrQ = "DECLARE @myTable TABLE " & _
                   " ( " & _
                   "    monthno Int " & _
                   "  , monthname varchar(20) " & _
                   "  , yearNo VARCHAR(5) " & _
                   " ) " & _
                   " declare @StartDate datetime " & _
                   " declare @Month int " & _
                   " declare @CurrentMonth int " & _
                   " set @StartDate = @start_date " & _
                   " set @Month = @MonthDiff " & _
                   " set @CurrentMonth = 0 " & _
                   " while @CurrentMonth < @Month " & _
                   "   begin " & _
                   "        insert @myTable (monthno,monthname,yearNo) values " & _
                   "     ( datepart(MONTH,DATEADD(mm, @CurrentMonth,@StartDate)) ,DATEADD(mm, @CurrentMonth,@StartDate),datepart(Year,DATEADD(mm, @CurrentMonth,@StartDate))) " & _
                   "        set @CurrentMonth = @CurrentMonth + 1 " & _
                   "   End " & _
                   " SELECT Monthno,SUBSTRING(monthname,1,3) AS MonthName,0 AS Hired,0 AS Term,0 AS Active FROM @myTable "

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, FinancialYear._Object._Database_Start_Date)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDatabaseStartDate)
            'Shani(24-Aug-2015) -- End
            objDataOperation.AddParameter("@MonthDiff", SqlDbType.Int, eZeeDataType.INT_SIZE, MonthDiff)

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtTables Is Nothing Then
                dtTables = Staff_TO_Grid()
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows

                'Pinkal (28-Dec-2020) -- Start
                'Bug  -  Working on Sport Pesa - Active Employee Count not reflected on Dashboard.
                'If dtTables.Rows(0).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName).ToString.Trim.Length > 0 Then
                '    dtRow.Item("Hired") = dtTables.Rows(0).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName)
                'End If
                'If dtTables.Rows(1).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName).ToString.Trim.Length > 0 Then
                '    dtRow.Item("Term") = dtTables.Rows(1).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName)
                'End If
                'If dtTables.Rows(2).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName).ToString.Trim.Length > 0 Then
                '    dtRow.Item("Active") = dtTables.Rows(2).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName)
                'End If

                If dtTables.Rows(0).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName).ToString.Trim.Length > 0 Then
                    dtRow.Item("Hired") = dtTables.Rows(0).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName)
                End If
                If dtTables.Rows(1).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName).ToString.Trim.Length > 0 Then
                    dtRow.Item("Hired") = CInt(dtRow.Item("Hired")) + dtTables.Rows(1).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName)
                End If
                If dtTables.Rows(2).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName).ToString.Trim.Length > 0 Then
                    dtRow.Item("Term") = dtTables.Rows(2).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName)
                End If
                If dtTables.Rows(3).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName).ToString.Trim.Length > 0 Then
                    dtRow.Item("Active") = dtTables.Rows(3).Item(dtTables.Columns(dtRow.Item("MonthName")).ColumnName)
                End If
                'Pinkal (28-Dec-2020) -- End

            Next
            dsList.AcceptChanges()

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "StaffTO_List", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function Staff_TO_Grid(Optional ByVal StrListName As String = "List") As DataTable
        Dim dtTable As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim intTotalActiveEmpAsOn As Integer = 0
        Try
            objDataOperation = New clsDataOperation

            dtTable = New DataTable(StrListName)

            dtTable.Columns.Add("Particulars", System.Type.GetType("System.String")).DefaultValue = ""

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'For i As Integer = 1 To 12
            '    dtTable.Columns.Add(MonthName(i, True), System.Type.GetType("System.String")).DefaultValue = ""
            'Next

            'S.SANDEEP [ 21 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
            'Dim MonthDiff As Integer = DateDiff(DateInterval.Month, FinancialYear._Object._Database_Start_Date, FinancialYear._Object._Database_End_Date.AddMonths(1))

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim MonthDiff As Integer = DateDiff(DateInterval.Month, FinancialYear._Object._Database_Start_Date, ConfigParameter._Object._CurrentDateAndTime.AddMonths(1))
            Dim MonthDiff As Integer = DateDiff(DateInterval.Month, mdtDatabaseStartDate, mdtCurrentDateAndTime.AddMonths(1))
            'Shani(24-Aug-2015) -- End

            If MonthDiff > 12 Then MonthDiff = 12
            'S.SANDEEP [ 21 MAY 2012 ] -- END
            StrQ = "DECLARE @myTable TABLE " & _
                   " ( " & _
                   "    monthno Int " & _
                   "  , monthname varchar(20) " & _
                   "  , yearNo VARCHAR(5) " & _
                   " ) " & _
                   " declare @StartDate datetime " & _
                   " declare @Month int " & _
                   " declare @CurrentMonth int " & _
                   " set @StartDate = @start_date " & _
                   " set @Month = @MonthDiff " & _
                   " set @CurrentMonth = 0 " & _
                   " while @CurrentMonth < @Month " & _
                   "   begin " & _
                   "        insert @myTable (monthno,monthname,yearNo) values " & _
                   "     ( datepart(MONTH,DATEADD(mm, @CurrentMonth,@StartDate)) ,DATEADD(mm, @CurrentMonth,@StartDate),datepart(Year,DATEADD(mm, @CurrentMonth,@StartDate))) " & _
                   "        set @CurrentMonth = @CurrentMonth + 1 " & _
                   "   End " & _
                   " SELECT Monthno,SUBSTRING(monthname,1,3) AS MonthName FROM @myTable "


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, FinancialYear._Object._Database_Start_Date)
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDatabaseStartDate)
            'Shani(24-Aug-2015) -- End

            objDataOperation.AddParameter("@MonthDiff", SqlDbType.Int, eZeeDataType.INT_SIZE, MonthDiff)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables(0).Rows
                dtTable.Columns.Add(dRow.Item("MonthName").ToString, System.Type.GetType("System.String")).DefaultValue = ""
            Next
            'S.SANDEEP [ 28 FEB 2012 ] -- END

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtCurrentDateAndTime, True, mstrDatabaseName, mintuserUnkid, mintCompanyUnkid, mintYearUnkId, mstrUserModeSetting)

            dtTable.Columns.Add("ViewId", System.Type.GetType("System.Int32")).DefaultValue = -1

            StrQ = "SELECT " & _
                    "	 @Caption1 AS Particular " & _
                    "	,COUNT(employeeunkid) AS Cnt " & _
                    "	,SUBSTRING(Datename(month,appointeddate),1,3)  AS Months " & _
                    "	,MONTH(appointeddate) AS MId " & _
                    "FROM hremployee_master "
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            StrQ &= "WHERE CONVERT(CHAR(8),appointeddate,112) >= '" & eZeeDate.convertDate(mdtDatabaseStartDate) & "' AND CONVERT(CHAR(8),appointeddate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    "GROUP BY Datename(month,appointeddate),MONTH(appointeddate) "

            'S.SANDEEP [06 Jan 2016] -- START
            '"WHERE YEAR(appointeddate) = @Year AND isactive = 1 AND MONTH(appointeddate) <= @Month " & _ -- REMOVED
            '"WHERE CONVERT(CHAR(8),appointeddate,112) >= '" & eZeeDate.convertDate(mdtDatabaseStartDate) & "' AND CONVERT(CHAR(8),appointeddate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _ -- ADDED
            'S.SANDEEP [06 Jan 2016] -- END



            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@Year", SqlDbType.Int, eZeeDataType.INT_SIZE, ConfigParameter._Object._CurrentDateAndTime.Year)


            'S.SANDEEP [06 Jan 2016] -- START
            'objDataOperation.AddParameter("@Year", SqlDbType.Int, eZeeDataType.INT_SIZE, mdtCurrentDateAndTime.Year)
            objDataOperation.AddParameter("@Year", SqlDbType.Int, eZeeDataType.INT_SIZE, mdtPeriodEndDate.Year)
            'S.SANDEEP [06 Jan 2016] -- END

            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [ 24 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@Month", SqlDbType.Int, eZeeDataType.INT_SIZE, ConfigParameter._Object._CurrentDateAndTime.Month)

            'S.SANDEEP [06 Jan 2016] -- START
            'objDataOperation.AddParameter("@Month", SqlDbType.Int, eZeeDataType.INT_SIZE, mdtCurrentDateAndTime.Month)
            objDataOperation.AddParameter("@Month", SqlDbType.Int, eZeeDataType.INT_SIZE, mdtPeriodEndDate.Month)
            'S.SANDEEP [06 Jan 2016] -- END

            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [ 24 MAY 2012 ] -- END
            objDataOperation.AddParameter("@Caption1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "NEWLY HIRED"))

            dsList = objDataOperation.ExecQuery(StrQ, "NH")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Dim blnFlag As Boolean = False
            Dim drRow As DataRow = dtTable.NewRow
            Dim k As Integer = 0
            If dsList.Tables("NH").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("NH").Rows
                    If blnFlag = False Then
                        drRow.Item("Particulars") = dtRow.Item("Particular")
                        blnFlag = True
                    End If
                    If dtTable.Columns.Contains(dtRow.Item("Months")) Then
                    drRow.Item(dtRow.Item("Months")) = dtRow.Item("Cnt").ToString
                    drRow.Item("ViewId") = 1
                    If k >= 0 Then
                        intTotalActiveEmpAsOn += dtRow.Item("Cnt")
                    End If
                    k += 1
                    End If
                Next
                dtTable.Rows.Add(drRow)
                blnFlag = False
            Else
                drRow.Item("Particulars") = Language.getMessage(mstrModuleName, 1, "NEWLY HIRED")
                dtTable.Rows.Add(drRow)
            End If




            'Pinkal (28-Dec-2020) -- Start
            'Bug  -  Working on Sport Pesa - Active Employee Count not reflected on Dashboard.

            objDataOperation.ClearParameters()

            StrQ = "SELECT " & _
                   "	 @Caption1 AS Particular " & _
                   "	,COUNT(employeeunkid) AS Cnt " & _
                   "	,SUBSTRING(Datename(month,HIRE.REHIRE),1,3)  AS Months " & _
                   "	,MONTH(HIRE.REHIRE) AS MId " & _
                   " FROM hremployee_master "
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            StrQ &= " JOIN ( SELECT RH.EmpId ,RH.REHIRE FROM " & _
                   "                ( SELECT hremployee_rehire_tran.employeeunkid AS EmpId ,reinstatment_date AS REHIRE ,hremployee_rehire_tran.effectivedate  " & _
                   "                 ,ROW_NUMBER() OVER(PARTITION BY hremployee_rehire_tran.employeeunkid ORDER BY hremployee_rehire_tran.effectivedate DESC) AS xNo  " & _
                   "                 FROM hremployee_rehire_tran  " & _
                   "                 JOIN hremployee_dates_tran ON hremployee_dates_tran.rehiretranunkid = hremployee_rehire_tran.rehiretranunkid and hremployee_dates_tran.isvoid = 0 " & _
                   "                 WHERE hremployee_rehire_tran.isvoid = 0   " & _
                   "                 AND (ISNULL(date1,date2) IS NOT NULL OR ISNULL(date2,date1) IS NOT NULL) " & _
                   "                AND CONVERT(CHAR(8),hremployee_rehire_tran.effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' ) AS RH WHERE RH.xNo = 1 ) AS HIRE ON HIRE.EmpId = hremployee_master.employeeunkid " & _
                   " WHERE CONVERT(CHAR(8),HIRE.REHIRE,112) >= '" & eZeeDate.convertDate(mdtDatabaseStartDate) & "' AND CONVERT(CHAR(8),HIRE.REHIRE,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   " GROUP BY Datename(month,HIRE.REHIRE),MONTH(HIRE.REHIRE) "



            objDataOperation.AddParameter("@Caption1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 46, "RE-HIRED"))

            dsList = objDataOperation.ExecQuery(StrQ, "NH")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            blnFlag = False
            drRow = dtTable.NewRow
            k = 0
            If dsList.Tables("NH").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("NH").Rows
                    If blnFlag = False Then
                        drRow.Item("Particulars") = dtRow.Item("Particular")
                        blnFlag = True
                    End If
                    If dtTable.Columns.Contains(dtRow.Item("Months")) Then
                        drRow.Item(dtRow.Item("Months")) = dtRow.Item("Cnt").ToString
                        drRow.Item("ViewId") = 2
                        If k >= 0 Then
                            intTotalActiveEmpAsOn += dtRow.Item("Cnt")
                        End If
                        k += 1
                    End If
                Next
                dtTable.Rows.Add(drRow)
                blnFlag = False
            Else
                drRow.Item("Particulars") = Language.getMessage(mstrModuleName, 46, "RE-HIRED")
                dtTable.Rows.Add(drRow)
            End If

            objDataOperation.ClearParameters()

            'Pinkal (28-Dec-2020)  -- End



            'Pinkal (03-Sep-2012) -- Start
            'Enhancement : TRA Changes

            '"SELECT " & _
            '                 "	 @Caption2 AS Particular " & _
            '                 "	,COUNT(employeeunkid) AS Cnt " & _
            '                 "	,SUBSTRING(Datename(month,termination_from_date),1,3)  AS Months " & _
            '                 "	,MONTH(termination_from_date) AS MId " & _
            '                 "FROM hremployee_master " & _
            '                 "WHERE YEAR(termination_from_date) = @Year AND MONTH(termination_from_date) <= @Month " & _
            '                 "GROUP BY Datename(month,termination_from_date),MONTH(termination_from_date) " & _

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = " SELECT MId,Particular,Months,SUM(Cnt) AS Cnt FROM " & _
            '         "  ( " & _
            '            "SELECT " & _
            '       "	 @Caption2 AS Particular " & _
            '       "	,COUNT(employeeunkid) AS Cnt " & _
            '       "	,SUBSTRING(Datename(month,termination_from_date),1,3)  AS Months " & _
            '       "	,MONTH(termination_from_date) AS MId " & _
            '       "FROM hremployee_master " & _
            '       "WHERE YEAR(termination_from_date) = @Year AND MONTH(termination_from_date) <= @Month " & _
            '                   "GROUP BY Datename(month,termination_from_date),MONTH(termination_from_date) " & _
            '            " UNION " & _
            '            "SELECT " & _
            '                   "	 @Caption2 AS Particular " & _
            '                   "	,COUNT(employeeunkid) AS Cnt " & _
            '                   "	,SUBSTRING(Datename(month,termination_to_date),1,3)  AS Months " & _
            '                   "	,MONTH(termination_to_date) AS MId " & _
            '                   "FROM hremployee_master " & _
            '                   "WHERE YEAR(termination_to_date) = @Year AND MONTH(termination_to_date) <= @Month " & _
            '                   "GROUP BY Datename(month,termination_to_date),MONTH(termination_to_date) " & _
            '            " UNION " & _
            '            "SELECT " & _
            '                   "	 @Caption2 AS Particular " & _
            '                   "	,COUNT(employeeunkid) AS Cnt " & _
            '                   "	,SUBSTRING(Datename(month,empl_enddate),1,3)  AS Months " & _
            '                   "	,MONTH(empl_enddate) AS MId " & _
            '                   "FROM hremployee_master " & _
            '                   "WHERE YEAR(empl_enddate) = @Year AND MONTH(empl_enddate) <= @Month " & _
            '                   "GROUP BY Datename(month,empl_enddate),MONTH(empl_enddate) " & _
            '              "  ) A GROUP BY Months,MId,Particular "

            'S.SANDEEP [06 Jan 2016] -- START
            'StrQ = "SELECT  " & _
            '       "    MId  " & _
            '       "   ,Particular  " & _
            '       "   ,Months  " & _
            '       "   ,SUM(Cnt) AS Cnt " & _
            '       "FROM " & _
            '         "  ( " & _
            '            "SELECT " & _
            '       "	 @Caption2 AS Particular " & _
            '       "           ,COUNT(hremployee_master.employeeunkid) AS Cnt " & _
            '       "           ,SUBSTRING(DATENAME(month, ETERM.termination_from_date), 1, 3) AS Months " & _
            '       "           ,MONTH(ETERM.termination_from_date) AS MId " & _
            '       "FROM hremployee_master " & _
            '       "           LEFT JOIN " & _
            '       "               ( " & _
            '       "                   SELECT " & _
            '       "                        employeeunkid AS TEEmpId " & _
            '       "                       ,date2 AS termination_from_date " & _
            '       "                       ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS Rno " & _
            '       "                   FROM    hremployee_dates_tran " & _
            '       "                   WHERE   isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
            '       "               ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.Rno = 1 " & _
            '       "       WHERE   YEAR(ETERM.termination_from_date) = @Year " & _
            '       "               AND MONTH(ETERM.termination_from_date) <= @Month " & _
            '       "       GROUP BY DATENAME(month, ETERM.termination_from_date) , " & _
            '       "                MONTH(ETERM.termination_from_date) " & _
            '            " UNION " & _
            '            "SELECT " & _
            '                   "	 @Caption2 AS Particular " & _
            '       "           ,COUNT(hremployee_master.employeeunkid) AS Cnt  " & _
            '       "           ,SUBSTRING(DATENAME(month, ETERM.termination_to_date), 1, 3) AS Months  " & _
            '       "           ,MONTH(ETERM.termination_to_date) AS MId " & _
            '                   "FROM hremployee_master " & _
            '       "           LEFT JOIN " & _
            '       "               ( " & _
            '       "                   SELECT " & _
            '       "                       employeeunkid AS TEEmpId " & _
            '       "                       ,date1 AS termination_to_date " & _
            '       "                       ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS Rno " & _
            '       "                   FROM    hremployee_dates_tran " & _
            '       "                   WHERE   isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT & " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
            '       "               ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.Rno = 1 " & _
            '       "       WHERE   YEAR(ETERM.termination_to_date) = @Year " & _
            '       "               AND MONTH(ETERM.termination_to_date) <= @Month " & _
            '       "       GROUP BY DATENAME(month, ETERM.termination_to_date) , " & _
            '       "                MONTH(ETERM.termination_to_date) " & _
            '            " UNION " & _
            '            "SELECT " & _
            '                   "	 @Caption2 AS Particular " & _
            '       "           ,COUNT(hremployee_master.employeeunkid) AS Cnt  " & _
            '       "           ,SUBSTRING(DATENAME(month, ETERM.empl_enddate), 1, 3) AS Months " & _
            '       "           ,MONTH(ETERM.empl_enddate) AS MId " & _
            '                   "FROM hremployee_master " & _
            '       "           LEFT JOIN " & _
            '       "               ( " & _
            '       "                   SELECT " & _
            '       "                       employeeunkid AS TEEmpId  " & _
            '       "                       ,date1 AS empl_enddate  " & _
            '       "                       ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS Rno " & _
            '       "                   FROM    hremployee_dates_tran " & _
            '       "                   WHERE   isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
            '       "               ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.Rno = 1 " & _
            '       "       WHERE   YEAR(ETERM.empl_enddate) = @Year " & _
            '       "               AND MONTH(ETERM.empl_enddate) <= @Month " & _
            '       "       GROUP BY DATENAME(month, ETERM.empl_enddate) , " & _
            '       "                MONTH(ETERM.empl_enddate) " & _
            '       "   ) A " & _
            '       "GROUP BY Months,MId,Particular "
            'Shani(24-Aug-2015) -- End


            StrQ = "SELECT  " & _
                   "    MId  " & _
                   "   ,Particular  " & _
                   "   ,Months  " & _
                   "   ,SUM(Cnt) AS Cnt " & _
                   "FROM " & _
                     "  ( " & _
                        "SELECT " & _
                   "	 @Caption2 AS Particular " & _
                   "           ,COUNT(hremployee_master.employeeunkid) AS Cnt " & _
                   "           ,SUBSTRING(DATENAME(month, ETERM.termination_from_date), 1, 3) AS Months " & _
                   "           ,MONTH(ETERM.termination_from_date) AS MId " & _
                   "FROM hremployee_master "
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            StrQ &= "           LEFT JOIN " & _
                   "               ( " & _
                   "                   SELECT " & _
                   "                        employeeunkid AS TEEmpId " & _
                   "                       ,date2 AS termination_from_date " & _
                   "                       ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS Rno " & _
                   "                   FROM    hremployee_dates_tran " & _
                   "                   WHERE   isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "               ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.Rno = 1 " & _
                   "            WHERE CONVERT(CHAR(8),ETERM.termination_from_date,112) >= '" & eZeeDate.convertDate(mdtDatabaseStartDate) & "' AND CONVERT(CHAR(8),ETERM.termination_from_date,112) < '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "       GROUP BY DATENAME(month, ETERM.termination_from_date) , " & _
                   "                MONTH(ETERM.termination_from_date) " & _
                        " UNION " & _
                        "SELECT " & _
                               "	 @Caption2 AS Particular " & _
                   "           ,COUNT(hremployee_master.employeeunkid) AS Cnt  " & _
                   "           ,SUBSTRING(DATENAME(month, ETERM.termination_to_date), 1, 3) AS Months  " & _
                   "           ,MONTH(ETERM.termination_to_date) AS MId " & _
                        "FROM hremployee_master "
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            StrQ &= "           LEFT JOIN " & _
                   "               ( " & _
                   "                   SELECT " & _
                   "                       employeeunkid AS TEEmpId " & _
                   "                       ,date1 AS termination_to_date " & _
                   "                       ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS Rno " & _
                   "                   FROM    hremployee_dates_tran " & _
                   "                   WHERE   isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT & " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "               ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.Rno = 1 " & _
                   "            WHERE CONVERT(CHAR(8),ETERM.termination_to_date,112) >= '" & eZeeDate.convertDate(mdtDatabaseStartDate) & "' AND CONVERT(CHAR(8),ETERM.termination_to_date,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "       GROUP BY DATENAME(month, ETERM.termination_to_date) , " & _
                   "                MONTH(ETERM.termination_to_date) " & _
                        " UNION " & _
                        "SELECT " & _
                               "	 @Caption2 AS Particular " & _
                   "           ,COUNT(hremployee_master.employeeunkid) AS Cnt  " & _
                   "           ,SUBSTRING(DATENAME(month, ETERM.empl_enddate), 1, 3) AS Months " & _
                   "           ,MONTH(ETERM.empl_enddate) AS MId " & _
                               "FROM hremployee_master " & _
                   "           LEFT JOIN " & _
                   "               ( " & _
                   "                   SELECT " & _
                   "                       employeeunkid AS TEEmpId  " & _
                   "                       ,date1 AS empl_enddate  " & _
                   "                       ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS Rno " & _
                   "                   FROM    hremployee_dates_tran " & _
                   "                   WHERE   isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "               ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.Rno = 1 " & _
                   "            WHERE CONVERT(CHAR(8),ETERM.empl_enddate,112) >= '" & eZeeDate.convertDate(mdtDatabaseStartDate) & "' AND CONVERT(CHAR(8),ETERM.empl_enddate,112) <'" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                   "       GROUP BY DATENAME(month, ETERM.empl_enddate) , " & _
                   "                MONTH(ETERM.empl_enddate) " & _
                   "   ) A " & _
                   "GROUP BY Months,MId,Particular "
            'S.SANDEEP [06 Jan 2016] -- END

            

            'Pinkal (03-Sep-2012) -- End


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@Year", SqlDbType.Int, eZeeDataType.INT_SIZE, ConfigParameter._Object._CurrentDateAndTime.Year)

            'S.SANDEEP [06 Jan 2016] -- START
            'objDataOperation.AddParameter("@Year", SqlDbType.Int, eZeeDataType.INT_SIZE, mdtCurrentDateAndTime.Year)
            objDataOperation.AddParameter("@Year", SqlDbType.Int, eZeeDataType.INT_SIZE, mdtPeriodEndDate.Year)
            'S.SANDEEP [06 Jan 2016] -- END

            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [ 24 MAY 2012 ] -- START
            'ENHANCEMENT : TRA USER PRIVILEGE CHANGES

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@Month", SqlDbType.Int, eZeeDataType.INT_SIZE, ConfigParameter._Object._CurrentDateAndTime.Month)

            'S.SANDEEP [06 Jan 2016] -- START
            'objDataOperation.AddParameter("@Month", SqlDbType.Int, eZeeDataType.INT_SIZE, mdtCurrentDateAndTime.Month)
            objDataOperation.AddParameter("@Month", SqlDbType.Int, eZeeDataType.INT_SIZE, mdtPeriodEndDate.Month)
            'S.SANDEEP [06 Jan 2016] -- END

            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [ 24 MAY 2012 ] -- END
            objDataOperation.AddParameter("@Caption2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "TERMINATED"))

            dsList = objDataOperation.ExecQuery(StrQ, "TM")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            drRow = dtTable.NewRow

            If dsList.Tables("TM").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("TM").Rows
                    If blnFlag = False Then
                        drRow.Item("Particulars") = dtRow.Item("Particular")
                        blnFlag = True
                    End If
                    If dtTable.Columns.Contains(dtRow.Item("Months")) Then
                    drRow.Item(dtRow.Item("Months")) = dtRow.Item("Cnt").ToString

                        'Pinkal (28-Dec-2020) -- Start
                        'Bug  -  Working on Sport Pesa - Active Employee Count not reflected on Dashboard.
                        'drRow.Item("ViewId") = 2
                        drRow.Item("ViewId") = 3
                        'Pinkal (28-Dec-2020) -- End

                    End If
                Next
                dtTable.Rows.Add(drRow)
                blnFlag = False
            Else
                drRow.Item("Particulars") = Language.getMessage(mstrModuleName, 2, "TERMINATED")
                dtTable.Rows.Add(drRow)
            End If

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""


            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtDatabaseStartDate, mdtDatabaseStartDate)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtDatabaseStartDate, True, mstrDatabaseName, mintuserUnkid, mintCompanyUnkid, mintYearUnkId, mstrUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtDatabaseStartDate, mstrDatabaseName)

            StrQ = "SELECT hremployee_master.employeeunkid FROM hremployee_master"

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= xDateJoinQry & " "
            End If

            'S.SANDEEP [15 NOV 2016] -- START
            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
            End If
            'S.SANDEEP [15 NOV 2016] -- END

            'If xAdvanceJoinQry.Trim.Length > 0 Then
            '    StrQ &= xAdvanceJoinQry
            'End If

            StrQ &= " WHERE 1 = 1 "

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry
            'End If
            'S.SANDEEP [15 NOV 2016] -- END


            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= xDateFilterQry & " "
            End If

            'Shani(24-Aug-2015) -- End


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim iCnt As Integer = 0 : iCnt = objDataOperation.RecordCount("SELECT employeeunkid FROM hremployee_master")
            Dim iCnt As Integer = 0 : iCnt = objDataOperation.RecordCount(StrQ)

            Dim dsDetals As New DataSet
            dsDetals = objDataOperation.ExecQuery(StrQ, "list")

            'Shani(24-Aug-2015) -- End

            Dim j As Integer = 0
            If iCnt > 0 Then
                drRow = dtTable.NewRow
                For Each dtCol As DataColumn In dtTable.Columns
                    Select Case j
                        Case 0
                            drRow.Item(dtCol.ColumnName) = Language.getMessage(mstrModuleName, 3, "TOTAL ACTIVE EMPLOYEE")
                        Case Else
                            'Pinkal (28-Dec-2020) -- Start
                            'Bug  -  Working on Sport Pesa - Active Employee Count not reflected on Dashboard.
                            'drRow.Item(dtCol.ColumnName) = (iCnt + CInt(IIf(dtTable.Rows(0).Item(dtCol.ColumnName).ToString.Length <= 0, 0, dtTable.Rows(0).Item(dtCol.ColumnName)))) - CInt(IIf(dtTable.Rows(1).Item(dtCol.ColumnName).ToString.Length <= 0, 0, dtTable.Rows(1).Item(dtCol.ColumnName)))
                            drRow.Item(dtCol.ColumnName) = (iCnt + CInt(IIf(dtTable.Rows(0).Item(dtCol.ColumnName).ToString.Length <= 0, 0, dtTable.Rows(0).Item(dtCol.ColumnName))) + CInt(IIf(dtTable.Rows(1).Item(dtCol.ColumnName).ToString.Length <= 0, 0, dtTable.Rows(1).Item(dtCol.ColumnName)))) - CInt(IIf(dtTable.Rows(2).Item(dtCol.ColumnName).ToString.Length <= 0, 0, dtTable.Rows(2).Item(dtCol.ColumnName)))
                            'Pinkal (28-Dec-2020) -- End
                                        iCnt = CInt(drRow.Item(dtCol.ColumnName))
                    End Select
                    j += 1
                Next

                'Pinkal (28-Dec-2020) -- Start
                'Bug  -  Working on Sport Pesa - Active Employee Count not reflected on Dashboard.
                'drRow.Item("ViewId") = 3
                drRow.Item("ViewId") = 4
                'Pinkal (28-Dec-2020) -- End
                dtTable.Rows.Add(drRow)
            Else
                drRow = dtTable.NewRow
                drRow.Item("Particulars") = Language.getMessage(mstrModuleName, 3, "TOTAL ACTIVE EMPLOYEE")
                dtTable.Rows.Add(drRow)
            End If

            Return dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Staff_TO_Grid", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function Staff_TO_Grid_Self_Service(ByVal mblnApplyAccessFilter As Boolean, Optional ByVal mstrAllocationFilter As String = "", Optional ByVal StrListName As String = "List") As DataTable
        Dim dtTable As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim intTotalActiveEmpAsOn As Integer = 0
        Try
            objDataOperation = New clsDataOperation

            dtTable = New DataTable(StrListName)

            dtTable.Columns.Add("Particulars", System.Type.GetType("System.String")).DefaultValue = ""

            Dim dtDatabase As New DataTable
            dtDatabase.Columns.Add("database_name")
            dtDatabase.Columns.Add("start_date")
            dtDatabase.Columns.Add("end_date")
            dtDatabase.Columns.Add("yearunkid")
            dtDatabase.Columns.Add("dbstdate")


            'StrQ = "SELECT " & _
            '          "database_name," & _
            '          "DATEADD(MONTH, -11, @start_date) as start_date " & _
            '          ",end_date " & _
            '          ",yearunkid " & _
            '        "FROM hrmsConfiguration..cffinancial_year_tran " & _
            '        "WHERE CONVERT(CHAR(8), DATEADD(MONTH, -11, @start_date), 112)  BETWEEN  CONVERT(CHAR(8), start_date, 112) AND CONVERT(CHAR(8), end_date, 112) " & _
            '        "and companyunkid = " & mintCompanyUnkid & " "


            'S.SANDEEP |06-DEC-2022| -- START
            'ISSUE : DATA WAS NOT COMING AS FULL DATE WAS CHECKED AND IT WAS NOT RETURNING DUE TO IN BETWEEN DATES {2 TO 27 OR 30}, THEN ADDED YEAR FILTER INSTEAD OF DATE

            'StrQ = "SELECT " & _
            '       "     database_name " & _
            '       "    ,DATEADD(MONTH, -11, @start_date) as start_date " & _
            '       "    ,CASE WHEN isclosed = 1 THEN end_date " & _
            '       "          WHEN isclosed = 0 THEN CASE WHEN CAST(@start_date AS DATETIME) > end_date THEN end_date ELSE @start_date END " & _
            '       "     END AS end_date " & _
            '       "    ,yearunkid " & _
            '       "    ,start_date AS dbstdate " & _
            '        "FROM hrmsConfiguration..cffinancial_year_tran " & _
            '       "WHERE (CONVERT(CHAR(8), start_date, 112) BETWEEN CONVERT(CHAR(8), DATEADD(MONTH, -11, @start_date), 112) AND @start_date " & _
            '       "OR CONVERT(CHAR(8), end_date, 112) BETWEEN CONVERT(CHAR(8), DATEADD(MONTH, -11, @start_date), 112) AND @start_date) " & _
            '       "AND companyunkid = " & mintCompanyUnkid & " ORDER BY hrmsConfiguration..cffinancial_year_tran.start_date ASC "


            StrQ = "SELECT " & _
                   "     database_name " & _
                   "    ,DATEADD(MONTH, -11, @start_date) as start_date " & _
                   "    ,CASE WHEN isclosed = 1 THEN end_date " & _
                   "          WHEN isclosed = 0 THEN CASE WHEN CAST(@start_date AS DATETIME) > end_date THEN end_date ELSE @start_date END " & _
                   "     END AS end_date " & _
                   "    ,yearunkid " & _
                   "    ,start_date AS dbstdate " & _
                    "FROM hrmsConfiguration..cffinancial_year_tran " & _
                   "WHERE companyunkid = " & mintCompanyUnkid & " " & _
                   " AND (DATEPART(YEAR,start_date) BETWEEN DATEPART(YEAR,DATEADD(MONTH, -11, @start_date)) AND DATEPART(YEAR,@start_date) " & _
                   "   OR DATEPART(YEAR,end_date) BETWEEN DATEPART(YEAR,DATEADD(MONTH, -11, @start_date))  AND DATEPART(YEAR,@start_date)) " & _
                   "ORDER BY hrmsConfiguration..cffinancial_year_tran.start_date ASC "

            'S.SANDEEP |06-DEC-2022| -- END

            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDateAndTime)

            dsList = objDataOperation.ExecQuery(StrQ, "FinYear")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("FinYear").Rows
                Dim dr As DataRow = dtDatabase.NewRow()
                dr("database_name") = dRow("database_name")
                dr("start_date") = dRow("start_date")
                dr("end_date") = dRow("end_date")
                dr("yearunkid") = dRow("yearunkid")
                dr("dbstdate") = dRow("dbstdate")
                dtDatabase.Rows.Add(dr)
            Next


            'If IsNothing(dsList.Tables("FinYear")) = False AndAlso dsList.Tables("FinYear").Rows.Count > 0 Then
            '    If dsList.Tables("FinYear").AsEnumerable().Cast(Of DataRow).Where(Function(x) x.Field(Of Integer)("yearunkid") = mintYearUnkId).Count = 0 Then
            '        dtDatabase.Rows.Add(mstrDatabaseName, mdtDatabaseStartDate, mdtCurrentDateAndTime, mintYearUnkId)
            '    End If
            'End If

            Dim MonthDiff As Integer
            Dim startDate As DateTime = mdtCurrentDateAndTime
            Dim dispstartcol As String = MonthName(Month(CDate(dtDatabase.Rows(0)("start_date"))), True) & "-" & CDate(dtDatabase.Rows(0)("start_date")).Year.ToString()

            startDate = CDate(dtDatabase.Rows(0)("dbstdate"))
            If dtDatabase.Rows.Count > 1 Then
                'startDate = CDate(dtDatabase.Rows(0)("start_date"))
                MonthDiff = DateDiff(DateInterval.Month, startDate, mdtCurrentDateAndTime.AddMonths(1))
            Else
                'startDate = mdtCurrentDateAndTime.AddMonths(-12)
                MonthDiff = DateDiff(DateInterval.Month, startDate, mdtCurrentDateAndTime.AddMonths(1))
            End If


            'If MonthDiff > 12 Then MonthDiff = 12
            StrQ = "DECLARE @myTable TABLE " & _
                   " ( " & _
                   "    monthno Int " & _
                   "  , monthname varchar(20) " & _
                   "  , yearNo VARCHAR(5) " & _
                   " ) " & _
                   " declare @StartDate datetime " & _
                   " declare @Month int " & _
                   " declare @CurrentMonth int " & _
                   " set @StartDate = @start_date " & _
                   " set @Month = @MonthDiff " & _
                   " set @CurrentMonth = 0 " & _
                   " while @CurrentMonth < @Month " & _
                   "   begin " & _
                   "        insert @myTable (monthno,monthname,yearNo) values " & _
                   "     ( datepart(MONTH,DATEADD(mm, @CurrentMonth,@StartDate)) ,DATEADD(mm, @CurrentMonth,@StartDate),datepart(Year,DATEADD(mm, @CurrentMonth,@StartDate))) " & _
                   "        set @CurrentMonth = @CurrentMonth + 1 " & _
                   "   End " & _
                   " SELECT Monthno,SUBSTRING(monthname,1,3)+'-'+yearNo AS MonthName FROM @myTable "


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, startDate)
            'If dtDatabase.Rows.Count > 1 Then
            '    objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, CDate(dtDatabase.Rows(0)("start_date")))
            'Else
            '    objDataOperation.AddParameter("@start_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCurrentDateAndTime.AddMonths(-12))
            'End If


            objDataOperation.AddParameter("@MonthDiff", SqlDbType.Int, eZeeDataType.INT_SIZE, MonthDiff)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables(0).Rows
                'Pinkal (01-Jun-2021)-- Start
                'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
                'dtTable.Columns.Add(dRow.Item("MonthName").ToString, System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add(dRow.Item("MonthName").ToString, System.Type.GetType("System.String")).DefaultValue = "0"
                'Pinkal (01-Jun-2021) -- End
            Next

            dsList = New DataSet
            StrQ = ""
            Dim xCount As Integer = 0
            For Each drDatabase As DataRow In dtDatabase.Rows


                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.
                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""
                'Pinkal (09-Aug-2021) -- End

                If mblnApplyAccessFilter Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, CDate(drDatabase("end_date")), True, drDatabase("database_name"), mintuserUnkid, mintCompanyUnkid, drDatabase("yearunkid"), mstrUserModeSetting)
                Else
                    Call GetAdvanceFilterQry(xUACQry, CDate(drDatabase("end_date")), drDatabase("database_name"))
                End If

                If dtTable.Columns.Contains("ViewId") = False Then
                    dtTable.Columns.Add("ViewId", System.Type.GetType("System.Int32")).DefaultValue = -1
                End If


                StrQ &= "SELECT " & _
                        "	 @Caption1 AS Particular " & _
                        "	,COUNT(hremployee_master.employeeunkid) AS Cnt " & _
                        "	,SUBSTRING(Datename(month,appointeddate),1,3) + '-' + CONVERT(NVARCHAR, YEAR(appointeddate)) AS Months " & _
                        "	,MONTH(appointeddate) AS MId " & _
                        "FROM " & drDatabase("database_name") & "..hremployee_master "

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "WHERE CONVERT(CHAR(8),appointeddate,112) >= '" & eZeeDate.convertDate(IIf(CDate(startDate) < CDate(drDatabase("dbstdate")), CDate(drDatabase("dbstdate")), CDate(startDate))) & "' AND CONVERT(CHAR(8),appointeddate,112) <= '" & eZeeDate.convertDate(CDate(drDatabase("end_date"))) & "' "


                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.
                If mstrAllocationFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAllocationFilter
                End If
                'Pinkal (09-Aug-2021) -- End

                StrQ &= "GROUP BY Datename(month,appointeddate),MONTH(appointeddate),YEAR(appointeddate) "


                If xCount <> dtDatabase.Rows.Count - 1 Then
                    StrQ &= " UNION "
                End If

                xCount += 1
            Next


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Caption1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "NEWLY HIRED"))

            If StrQ.Trim.Length > 0 Then dsList = objDataOperation.ExecQuery(StrQ, "NH")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim blnFlag As Boolean = False
            Dim drRow As DataRow = dtTable.NewRow
            Dim k As Integer = 0
            If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 AndAlso dsList.Tables("NH").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("NH").Rows
                    If blnFlag = False Then
                        drRow.Item("Particulars") = dtRow.Item("Particular")
                        blnFlag = True
                    End If
                    If dtTable.Columns.Contains(dtRow.Item("Months")) Then
                        drRow.Item(dtRow.Item("Months")) = dtRow.Item("Cnt").ToString
                        drRow.Item("ViewId") = 1
                        If k >= 0 Then
                            intTotalActiveEmpAsOn += dtRow.Item("Cnt")
                        End If
                        k += 1
                    End If
                Next
                dtTable.Rows.Add(drRow)
                blnFlag = False
            Else

                drRow.Item("Particulars") = Language.getMessage(mstrModuleName, 1, "NEWLY HIRED")
                dtTable.Rows.Add(drRow)

            End If



            dsList = New DataSet
            StrQ = ""
            Dim xRehireCount As Integer = 0
            For Each drDatabase As DataRow In dtDatabase.Rows


                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.

                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String

                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""

                If mblnApplyAccessFilter Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, CDate(drDatabase("end_date")), True, drDatabase("database_name"), mintuserUnkid, mintCompanyUnkid, drDatabase("yearunkid"), mstrUserModeSetting)
                Else
                    Call GetAdvanceFilterQry(xUACQry, CDate(drDatabase("end_date")), drDatabase("database_name"))
                End If
                'Pinkal (09-Aug-2021) -- End

                StrQ &= "SELECT " & _
                       "	 @Caption1 AS Particular " & _
                       "	,COUNT(hremployee_master.employeeunkid) AS Cnt " & _
                       "	,SUBSTRING(Datename(month,HIRE.REHIRE),1,3)+ '-' + CONVERT(NVARCHAR, YEAR(HIRE.REHIRE))  AS Months " & _
                       "	,MONTH(HIRE.REHIRE) AS MId " & _
                       " FROM " & drDatabase("database_name") & "..hremployee_master "

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= " JOIN ( SELECT RH.EmpId ,RH.REHIRE FROM " & _
                "                ( SELECT hremployee_rehire_tran.employeeunkid AS EmpId ,reinstatment_date AS REHIRE ,hremployee_rehire_tran.effectivedate  " & _
                "                 ,ROW_NUMBER() OVER(PARTITION BY hremployee_rehire_tran.employeeunkid ORDER BY hremployee_rehire_tran.effectivedate DESC) AS xNo  " & _
                "                 FROM " & drDatabase("database_name") & "..hremployee_rehire_tran  " & _
                "                 JOIN " & drDatabase("database_name") & "..hremployee_dates_tran ON hremployee_dates_tran.rehiretranunkid = hremployee_rehire_tran.rehiretranunkid and hremployee_dates_tran.isvoid = 0 " & _
                "                 WHERE hremployee_rehire_tran.isvoid = 0   " & _
                "                 AND (ISNULL(date1,date2) IS NOT NULL OR ISNULL(date2,date1) IS NOT NULL) " & _
                "                AND CONVERT(CHAR(8),hremployee_rehire_tran.effectivedate,112) <= '" & eZeeDate.convertDate(CDate(drDatabase("end_date"))) & "' ) AS RH WHERE RH.xNo = 1 ) AS HIRE ON HIRE.EmpId = hremployee_master.employeeunkid " & _
                " WHERE CONVERT(CHAR(8),HIRE.REHIRE,112) >= '" & eZeeDate.convertDate(IIf(CDate(startDate) < CDate(drDatabase("dbstdate")), CDate(drDatabase("dbstdate")), CDate(startDate))) & "' AND CONVERT(CHAR(8),HIRE.REHIRE,112) <= '" & eZeeDate.convertDate(CDate(drDatabase("end_date"))) & "' "

                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.

                If mstrAllocationFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAllocationFilter
                End If

                StrQ &= " GROUP BY Datename(month,HIRE.REHIRE),MONTH(HIRE.REHIRE),YEAR(HIRE.REHIRE) "

                'Pinkal (09-Aug-2021) -- End

                If xRehireCount <> dtDatabase.Rows.Count - 1 Then
                    StrQ &= " UNION "
                End If

                xRehireCount += 1
            Next

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Caption1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11111, "RE-HIRED"))

            If StrQ.Trim.Length > 0 Then dsList = objDataOperation.ExecQuery(StrQ, "NH")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            blnFlag = False
            drRow = dtTable.NewRow
            k = 0
            If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 AndAlso dsList.Tables("NH").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("NH").Rows
                    If blnFlag = False Then
                        drRow.Item("Particulars") = dtRow.Item("Particular")
                        blnFlag = True
                    End If
                    If dtTable.Columns.Contains(dtRow.Item("Months")) Then
                        drRow.Item(dtRow.Item("Months")) = dtRow.Item("Cnt").ToString
                        drRow.Item("ViewId") = 2
                        If k >= 0 Then
                            intTotalActiveEmpAsOn += dtRow.Item("Cnt")
                        End If
                        k += 1
                    End If
                Next
                dtTable.Rows.Add(drRow)
                blnFlag = False
            Else
                drRow.Item("Particulars") = Language.getMessage(mstrModuleName, 11111, "RE-HIRED")
                dtTable.Rows.Add(drRow)
            End If

            'Pinkal (28-Dec-2020)  -- End



            dsList = New DataSet

            StrQ = ""
            Dim xTerminationCount As Integer = 0
            For Each drDatabase As DataRow In dtDatabase.Rows

                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.
                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry As String

                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = ""

                If mblnApplyAccessFilter Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, CDate(drDatabase("end_date")), True, drDatabase("database_name"), mintuserUnkid, mintCompanyUnkid, drDatabase("yearunkid"), mstrUserModeSetting)
                'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, CDate(drDatabase("dbstdate")), True, drDatabase("database_name"), mintuserUnkid, mintCompanyUnkid, drDatabase("yearunkid"), mstrUserModeSetting)
                Else
                    Call GetAdvanceFilterQry(xUACQry, CDate(drDatabase("end_date")), drDatabase("database_name"))
                End If
                'Pinkal (09-Aug-2021) -- End


                If dtDatabase.Rows.Count > 1 Then
                    StrQ &= " ( "
                End If


                StrQ &= "SELECT  " & _
                           "    MId  " & _
                           "   ,Particular  " & _
                           "   ,Months  " & _
                           "   ,SUM(Cnt) AS Cnt " & _
                           "FROM " & _
                             "  ( " & _
                                "SELECT " & _
                           "	 @Caption2 AS Particular " & _
                           "           ,COUNT(hremployee_master.employeeunkid) AS Cnt " & _
                           "           ,SUBSTRING(DATENAME(month, ETERM.termination_from_date), 1, 3)+ '-' + CONVERT(NVARCHAR, YEAR(ETERM.termination_from_date)) AS Months " & _
                           "           ,MONTH(ETERM.termination_from_date) AS MId " & _
                           "FROM " & drDatabase("database_name") & "..hremployee_master "

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If

                StrQ &= "           LEFT JOIN " & _
                "               ( " & _
                "                   SELECT " & _
                "                        employeeunkid AS TEEmpId " & _
                "                       ,date2 AS termination_from_date " & _
                "                       ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS Rno " & _
                "                   FROM    " & drDatabase("database_name") & "..hremployee_dates_tran " & _
                "                   WHERE   isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(CDate(drDatabase("end_date"))) & "' " & _
                "               ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.Rno = 1 " & _
                "            WHERE CONVERT(CHAR(8),ETERM.termination_from_date,112) >= '" & eZeeDate.convertDate(IIf(CDate(startDate) < CDate(drDatabase("dbstdate")), CDate(drDatabase("dbstdate")), CDate(startDate))) & "' AND CONVERT(CHAR(8),ETERM.termination_from_date,112) < '" & eZeeDate.convertDate(CDate(drDatabase("end_date"))) & "' "

                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.

                If mstrAllocationFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAllocationFilter
                End If

                StrQ &= "       GROUP BY DATENAME(month, ETERM.termination_from_date) , " & _
                "                MONTH(ETERM.termination_from_date) " & _
                            "                ,YEAR(ETERM.termination_from_date) "

                'Pinkal (09-Aug-2021) -- End

         

                StrQ &= " UNION " & _
                             " SELECT " & _
                            "	 @Caption2 AS Particular " & _
                "           ,COUNT(hremployee_master.employeeunkid) AS Cnt  " & _
                "           ,SUBSTRING(DATENAME(month, ETERM.termination_to_date), 1, 3)+ '-' + CONVERT(NVARCHAR, YEAR(ETERM.termination_to_date)) AS Months  " & _
                "           ,MONTH(ETERM.termination_to_date) AS MId " & _
                             "  FROM " & drDatabase("database_name") & "..hremployee_master "

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If


                StrQ &= "           LEFT JOIN " & _
                        "               ( " & _
                        "                   SELECT " & _
                        "                       employeeunkid AS TEEmpId " & _
                        "                       ,date1 AS termination_to_date " & _
                        "                       ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS Rno " & _
                        "                   FROM    " & drDatabase("database_name") & "..hremployee_dates_tran " & _
                        "                   WHERE   isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT & " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(CDate(drDatabase("end_date"))) & "' " & _
                        "               ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.Rno = 1 " & _
                        "            WHERE CONVERT(CHAR(8),ETERM.termination_to_date,112) >= '" & eZeeDate.convertDate(IIf(CDate(startDate) < CDate(drDatabase("dbstdate")), CDate(drDatabase("dbstdate")), CDate(startDate))) & "' AND CONVERT(CHAR(8),ETERM.termination_to_date,112) <= '" & eZeeDate.convertDate(CDate(drDatabase("end_date"))) & "' "

                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.
                If mstrAllocationFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAllocationFilter
                End If
                'Pinkal (09-Aug-2021) -- End

                StrQ &= "       GROUP BY DATENAME(month, ETERM.termination_to_date) , " & _
                        "                MONTH(ETERM.termination_to_date) " & _
                        "                ,YEAR(ETERM.termination_to_date) " & _
                        "        UNION " & _
                        "       SELECT " & _
                        "	        @Caption2 AS Particular " & _
                        "           ,COUNT(hremployee_master.employeeunkid) AS Cnt  " & _
                        "           ,SUBSTRING(DATENAME(month, ETERM.empl_enddate), 1, 3)+ '-' + CONVERT(NVARCHAR, YEAR(ETERM.empl_enddate)) AS Months " & _
                        "           ,MONTH(ETERM.empl_enddate) AS MId " & _
                        "        FROM " & drDatabase("database_name") & "..hremployee_master "

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If


                StrQ &= "           LEFT JOIN " & _
                        "               ( " & _
                        "                   SELECT " & _
                        "                       employeeunkid AS TEEmpId  " & _
                        "                       ,date1 AS empl_enddate  " & _
                        "                       ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS Rno " & _
                        "                   FROM    " & drDatabase("database_name") & "..hremployee_dates_tran " & _
                        "                   WHERE   isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(CDate(drDatabase("end_date"))) & "' " & _
                        "               ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.Rno = 1 " & _
                        "            WHERE CONVERT(CHAR(8),ETERM.empl_enddate,112) >= '" & eZeeDate.convertDate(IIf(CDate(startDate) < CDate(drDatabase("dbstdate")), CDate(drDatabase("dbstdate")), CDate(startDate))) & "' AND CONVERT(CHAR(8),ETERM.empl_enddate,112) <'" & eZeeDate.convertDate(CDate(drDatabase("end_date"))) & "' "

                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.
                If mstrAllocationFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAllocationFilter
                End If


                StrQ &= "       GROUP BY DATENAME(month, ETERM.empl_enddate) , " & _
                        "                MONTH(ETERM.empl_enddate) " & _
                        "                ,YEAR(ETERM.empl_enddate) " & _
                        "   ) A  GROUP BY Months,MId,Particular "

                'Pinkal (09-Aug-2021) -- End

                If xTerminationCount <> dtDatabase.Rows.Count - 1 Then
                    StrQ &= ") "
                    StrQ &= " UNION "
                End If

                xTerminationCount += 1

            Next
            If dtDatabase.Rows.Count > 1 Then
                StrQ &= ")"
            End If


            objDataOperation.AddParameter("@Caption2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "TERMINATED"))

            If StrQ.Trim.Length > 0 Then dsList = objDataOperation.ExecQuery(StrQ, "TM")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            drRow = dtTable.NewRow

            If dsList IsNot Nothing AndAlso dsList.Tables.Count > 0 AndAlso dsList.Tables("TM").Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables("TM").Rows
                    If blnFlag = False Then
                        drRow.Item("Particulars") = dtRow.Item("Particular")
                        blnFlag = True
                    End If
                    If dtTable.Columns.Contains(dtRow.Item("Months")) Then
                        drRow.Item(dtRow.Item("Months")) = dtRow.Item("Cnt").ToString
                        drRow.Item("ViewId") = 3
                    End If
                Next
                dtTable.Rows.Add(drRow)
                blnFlag = False
            Else
                drRow.Item("Particulars") = Language.getMessage(mstrModuleName, 2, "TERMINATED")
                dtTable.Rows.Add(drRow)
            End If


            dsList = New DataSet
            StrQ = ""
            Dim xTotalActiveCount As Integer = 0
            For Each drDatabase As DataRow In dtDatabase.Rows

                Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
                xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""


                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.

                If mblnApplyAccessFilter Then
                'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, CDate(drDatabase("end_date")), True, drDatabase("database_name"), mintuserUnkid, mintCompanyUnkid, drDatabase("yearunkid"), mstrUserModeSetting)
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, IIf(CDate(startDate) < CDate(drDatabase("dbstdate")), CDate(drDatabase("dbstdate")), CDate(startDate)), True, drDatabase("database_name"), mintuserUnkid, mintCompanyUnkid, drDatabase("yearunkid"), mstrUserModeSetting)
                Else
                    Call GetAdvanceFilterQry(xUACQry, IIf(CDate(startDate) < CDate(drDatabase("dbstdate")), CDate(drDatabase("dbstdate")), CDate(startDate)), drDatabase("database_name"))
                End If

                Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, IIf(CDate(startDate) < CDate(drDatabase("dbstdate")), CDate(drDatabase("dbstdate")), CDate(startDate)), IIf(CDate(startDate) < CDate(drDatabase("dbstdate")), CDate(drDatabase("dbstdate")), CDate(startDate)), True, False, drDatabase("database_name"))
                'Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, IIf(CDate(startDate) < CDate(drDatabase("dbstdate")), CDate(drDatabase("dbstdate")), CDate(startDate)), True, mstrDatabaseName, mintuserUnkid, mintCompanyUnkid, mintYearUnkId, mstrUserModeSetting)


                StrQ = "SELECT hremployee_master.employeeunkid FROM " & drDatabase("database_name") & "..hremployee_master"

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= xDateJoinQry & " "
                End If

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= xUACQry
                End If


                StrQ &= " WHERE 1 = 1 "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= xDateFilterQry & " "
                End If


                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.
                If mstrAllocationFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & mstrAllocationFilter & "  "
                End If
                'Pinkal (09-Aug-2021) -- End

                Exit For

                If xTotalActiveCount <> dtDatabase.Rows.Count - 1 Then
                    StrQ &= " UNION "
                End If

                xTotalActiveCount += 1

            Next



            Dim iCnt As Integer = 0
            If StrQ.Trim.Length > 0 Then iCnt = objDataOperation.RecordCount(StrQ)

            Dim j As Integer = 0
            If iCnt > 0 Then
                drRow = dtTable.NewRow
                For Each dtCol As DataColumn In dtTable.Columns
                    Select Case j
                        Case 0
                            drRow.Item(dtCol.ColumnName) = Language.getMessage(mstrModuleName, 3, "TOTAL ACTIVE EMPLOYEE")
                        Case Else
                            drRow.Item(dtCol.ColumnName) = (iCnt + CInt(IIf(dtTable.Rows(0).Item(dtCol.ColumnName).ToString.Length <= 0, 0, dtTable.Rows(0).Item(dtCol.ColumnName))) + CInt(IIf(dtTable.Rows(1).Item(dtCol.ColumnName).ToString.Length <= 0, 0, dtTable.Rows(1).Item(dtCol.ColumnName)))) - CInt(IIf(dtTable.Rows(2).Item(dtCol.ColumnName).ToString.Length <= 0, 0, dtTable.Rows(2).Item(dtCol.ColumnName)))
                            iCnt = CInt(drRow.Item(dtCol.ColumnName))
                    End Select
                    j += 1
                Next

                drRow.Item("ViewId") = 4
                dtTable.Rows.Add(drRow)
            Else
                drRow = dtTable.NewRow
                drRow.Item("Particulars") = Language.getMessage(mstrModuleName, 3, "TOTAL ACTIVE EMPLOYEE")
                dtTable.Rows.Add(drRow)
            End If

            Dim intOrdinal As Integer = 0
            Dim intColmIdx As Integer = 1
            If dtTable.Columns.Contains(dispstartcol) Then
                intOrdinal = dtTable.Columns(dispstartcol).Ordinal
                For iColIdx As Integer = 0 To intOrdinal                    
                    If iColIdx > 0 Then
                        If iColIdx = intOrdinal Then
                            Exit For
                        Else                            
                            dtTable.Columns.RemoveAt(intColmIdx)
                        End If
                    End If
                Next
            End If

            Return dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Staff_TO_Grid_Self_Service", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function Staff_Detail_Informatiom(ByVal intDetailIdx As Integer, ByVal intDetailValue As Integer, ByVal intViewDetailByIdx As Integer, ByVal StrSeriesTag As String, Optional ByVal StrList As String = "List") As DataSet
        Dim dsDetals As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            mstrStaffDelailsMonthIds = ""

            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If intDetailValue = FinancialYear._Object._Database_End_Date.Month Then
            If intDetailValue = mdtDatabaseEndDate.Month Then
                'Shani(24-Aug-2015) -- End

                For i As Integer = 1 To 12
                    mstrStaffDelailsMonthIds &= "," & i.ToString
                Next
            Else
                For i As Integer = 1 To intDetailValue
                    mstrStaffDelailsMonthIds &= "," & i.ToString
                Next
            End If
            'S.SANDEEP [ 28 FEB 2012 ] -- END


            

            If mstrStaffDelailsMonthIds.Length > 0 Then
                mstrStaffDelailsMonthIds = Mid(mstrStaffDelailsMonthIds, 2)
            End If

            Select Case intDetailIdx
                Case enViewDetails.STAFF_DETAILS
                    Select Case StrSeriesTag.ToUpper
                        Case "HIRED"
                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ = "SELECT " & _
                                             " hrdepartment_master.departmentunkid AS Id " & _
                                             ",hrdepartment_master.code AS Code " & _
                                             ",hrdepartment_master.name AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM hrdepartment_master "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ = "SELECT " & _
                                             " hrstation_master.stationunkid AS Id " & _
                                             ",hrstation_master.code AS Code " & _
                                             ",hrstation_master.name AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM hrstation_master "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ = "SELECT " & _
                                             " prcostcenter_master.costcenterunkid AS Id " & _
                                             ",prcostcenter_master.costcentercode AS Code " & _
                                             ",prcostcenter_master.costcentername AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM prcostcenter_master "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ = "SELECT " & _
                                             " hrsection_master.sectionunkid AS Id " & _
                                             ",hrsection_master.code AS Code " & _
                                             ",hrsection_master.name AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM hrsection_master "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ = "SELECT " & _
                                             " hrunit_master.unitunkid AS Id " & _
                                             ",hrunit_master.code AS Code " & _
                                             ",hrunit_master.name AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM hrunit_master "
                            End Select

                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                            'StrQ &= "JOIN " & _
                            '        "( " & _
                            '             "SELECT " & _
                            '                   " COUNT(employeeunkid)aCnt "
                            'Select Case intViewDetailByIdx
                            '    Case enViewDetailsByIdx.DEPARTMENT_WISE
                            '        StrQ &= ", hremployee_master.departmentunkid AS Id "
                            '    Case enViewDetailsByIdx.BRANCH_WISE
                            '        StrQ &= ", hremployee_master.stationunkid AS Id "
                            '    Case enViewDetailsByIdx.COSTCENTER_WISE
                            '        StrQ &= ", hremployee_master.costcenterunkid AS Id "
                            '    Case enViewDetailsByIdx.SECTION_WISE
                            '        StrQ &= ", hremployee_master.sectionunkid AS Id "
                            '    Case enViewDetailsByIdx.UNIT_WISE
                            '        StrQ &= ", hremployee_master.unitunkid AS Id "
                            'End Select

                            'StrQ &= "FROM hremployee_master " & _
                            '        " WHERE MONTH(appointeddate) = @Month AND YEAR(appointeddate) = @Year AND isactive = 1 "

                            'Select Case intViewDetailByIdx
                            '    Case enViewDetailsByIdx.DEPARTMENT_WISE
                            '        StrQ &= "    Group BY hremployee_master.departmentunkid " & _
                            '                ")AS A ON A.Id = hrdepartment_master.departmentunkid "
                            '    Case enViewDetailsByIdx.BRANCH_WISE
                            '        StrQ &= "    Group BY hremployee_master.stationunkid " & _
                            '                ")AS A ON A.Id = hrstation_master.stationunkid "
                            '    Case enViewDetailsByIdx.COSTCENTER_WISE
                            '        StrQ &= "    Group BY hremployee_master.costcenterunkid " & _
                            '                ")AS A ON A.Id = prcostcenter_master.costcenterunkid "
                            '    Case enViewDetailsByIdx.SECTION_WISE
                            '        StrQ &= "    Group BY hremployee_master.sectionunkid " & _
                            '                ")AS A ON A.Id = hrsection_master.sectionunkid "
                            '    Case enViewDetailsByIdx.UNIT_WISE
                            '        StrQ &= "    Group BY hremployee_master.unitunkid " & _
                            '                ")AS A ON A.Id = hrunit_master.unitunkid "
                            'End Select

                            StrQ &= "JOIN " & _
                                    "( " & _
                                         "SELECT " & _
                                               " COUNT(hremployee_master.employeeunkid)aCnt "
                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ &= ", Alloc.departmentunkid AS Id "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ &= ", Alloc.stationunkid AS Id "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ &= ", CC.costcenterunkid AS Id "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ &= ", Alloc.sectionunkid AS Id "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ &= ", Alloc.unitunkid AS Id "
                            End Select

                            StrQ &= "FROM hremployee_master "

                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         departmentunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         stationunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "( " & _
                                            "    SELECT " & _
                                            "         cctranheadvalueid AS costcenterunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_cctranhead_tran " & _
                                            "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                                            "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         sectionunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         unitunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                            End Select

                            StrQ &= " WHERE MONTH(appointeddate) = @Month AND YEAR(appointeddate) = @Year AND isactive = 1 "

                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ &= "    Group BY Alloc.departmentunkid " & _
                                            ")AS A ON A.Id = hrdepartment_master.departmentunkid "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ &= "    Group BY Alloc.stationunkid " & _
                                            ")AS A ON A.Id = hrstation_master.stationunkid "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ &= "    Group BY CC.costcenterunkid " & _
                                            ")AS A ON A.Id = prcostcenter_master.costcenterunkid "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ &= "    Group BY Alloc.sectionunkid " & _
                                            ")AS A ON A.Id = hrsection_master.sectionunkid "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ &= "    Group BY Alloc.unitunkid " & _
                                            ")AS A ON A.Id = hrunit_master.unitunkid "
                            End Select

                            'Shani(24-Aug-2015) -- End

                        Case "TERM"
                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ = "SELECT " & _
                                             " hrdepartment_master.departmentunkid AS Id " & _
                                             ",hrdepartment_master.code AS Code " & _
                                             ",hrdepartment_master.name AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM hrdepartment_master "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ = "SELECT " & _
                                             " hrstation_master.stationunkid AS Id " & _
                                             ",hrstation_master.code AS Code " & _
                                             ",hrstation_master.name AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM hrstation_master "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ = "SELECT " & _
                                             " prcostcenter_master.costcenterunkid AS Id " & _
                                             ",prcostcenter_master.costcentercode AS Code " & _
                                             ",prcostcenter_master.costcentername AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM prcostcenter_master "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ = "SELECT " & _
                                             " hrsection_master.sectionunkid AS Id " & _
                                             ",hrsection_master.code AS Code " & _
                                             ",hrsection_master.name AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM hrsection_master "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ = "SELECT " & _
                                             " hrunit_master.unitunkid AS Id " & _
                                             ",hrunit_master.code AS Code " & _
                                             ",hrunit_master.name AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM hrunit_master "
                            End Select

                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS

                            'StrQ &= "JOIN " & _
                            '        "( " & _
                            '             "SELECT " & _
                            '                   " COUNT(employeeunkid)aCnt "
                            'Select Case intViewDetailByIdx
                            '    Case enViewDetailsByIdx.DEPARTMENT_WISE
                            '        StrQ &= ", hremployee_master.departmentunkid AS Id "
                            '    Case enViewDetailsByIdx.BRANCH_WISE
                            '        StrQ &= ", hremployee_master.stationunkid AS Id "
                            '    Case enViewDetailsByIdx.COSTCENTER_WISE
                            '        StrQ &= ", hremployee_master.costcenterunkid AS Id "
                            '    Case enViewDetailsByIdx.SECTION_WISE
                            '        StrQ &= ", hremployee_master.sectionunkid AS Id "
                            '    Case enViewDetailsByIdx.UNIT_WISE
                            '        StrQ &= ", hremployee_master.unitunkid AS Id "
                            'End Select
                            'StrQ &= "FROM hremployee_master " & _
                            '        " WHERE MONTH(termination_from_date) = @Month AND YEAR(termination_from_date) = @Year "

                            'Select Case intViewDetailByIdx
                            '    Case enViewDetailsByIdx.DEPARTMENT_WISE
                            '        StrQ &= "    Group BY hremployee_master.departmentunkid " & _
                            '                ")AS A ON A.Id = hrdepartment_master.departmentunkid "
                            '    Case enViewDetailsByIdx.BRANCH_WISE
                            '        StrQ &= "    Group BY hremployee_master.stationunkid " & _
                            '                ")AS A ON A.Id = hrstation_master.stationunkid "
                            '    Case enViewDetailsByIdx.COSTCENTER_WISE
                            '        StrQ &= "    Group BY hremployee_master.costcenterunkid " & _
                            '                ")AS A ON A.Id = prcostcenter_master.costcenterunkid "
                            '    Case enViewDetailsByIdx.SECTION_WISE
                            '        StrQ &= "    Group BY hremployee_master.sectionunkid " & _
                            '                ")AS A ON A.Id = hrsection_master.sectionunkid "
                            '    Case enViewDetailsByIdx.UNIT_WISE
                            '        StrQ &= "    Group BY hremployee_master.unitunkid " & _
                            '                ")AS A ON A.Id = hrunit_master.unitunkid "
                            'End Select

                            StrQ &= "JOIN " & _
                                    "( " & _
                                         "SELECT " & _
                                               " COUNT(hremployee_master.employeeunkid)aCnt "
                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ &= ", Alloc.departmentunkid AS Id "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ &= ", Alloc.stationunkid AS Id "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ &= ", CC.costcenterunkid AS Id "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ &= ", Alloc.sectionunkid AS Id "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ &= ", Alloc.unitunkid AS Id "
                            End Select
                            StrQ &= "FROM hremployee_master "

                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         departmentunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         stationunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "( " & _
                                            "    SELECT " & _
                                            "         cctranheadvalueid AS costcenterunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_cctranhead_tran " & _
                                            "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                                            "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         sectionunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         unitunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                            End Select
                            StrQ &= "LEFT JOIN " & _
                                    "   ( " & _
                                    "       SELECT " & _
                                    "            employeeunkid AS TEEmpId " & _
                                    "           ,date2 AS termination_from_date " & _
                                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                                    "       FROM hremployee_dates_tran " & _
                                    "       WHERE isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                    "       UNION" & _
                                    "                   SELECT " & _
                                    "                       employeeunkid AS TEEmpId " & _
                                    "                       ,date1 AS termination_to_date " & _
                                    "                       ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS Rno " & _
                                    "                   FROM    hremployee_dates_tran " & _
                                    "                   WHERE   isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT & " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                    "       UNION" & _
                                    "                   SELECT " & _
                                    "                       employeeunkid AS TEEmpId  " & _
                                    "                       ,date1 AS empl_enddate  " & _
                                    "                       ,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY effectivedate DESC ) AS Rno " & _
                                    "                   FROM    hremployee_dates_tran " & _
                                    "                   WHERE   isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                    "   ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.Rno = 1 " & _
                                    " WHERE MONTH(ETERM.termination_from_date) = @Month AND YEAR(ETERM.termination_from_date) = @Year "

                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ &= "    Group BY Alloc.departmentunkid " & _
                                            ")AS A ON A.Id = hrdepartment_master.departmentunkid "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ &= "    Group BY Alloc.stationunkid " & _
                                            ")AS A ON A.Id = hrstation_master.stationunkid "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ &= "    Group BY CC.costcenterunkid " & _
                                            ")AS A ON A.Id = prcostcenter_master.costcenterunkid "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ &= "    Group BY Alloc.sectionunkid " & _
                                            ")AS A ON A.Id = hrsection_master.sectionunkid "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ &= "    Group BY Alloc.unitunkid " & _
                                            ")AS A ON A.Id = hrunit_master.unitunkid "
                            End Select

                            'Shani(24-Aug-2015) -- End

                        Case "ACTIVE"
                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ = "SELECT " & _
                                             " hrdepartment_master.departmentunkid AS Id " & _
                                             ",hrdepartment_master.code AS Code " & _
                                             ",hrdepartment_master.name AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM hrdepartment_master "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ = "SELECT " & _
                                             " hrstation_master.stationunkid AS Id " & _
                                             ",hrstation_master.code AS Code " & _
                                             ",hrstation_master.name AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM hrstation_master "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ = "SELECT " & _
                                             " prcostcenter_master.costcenterunkid AS Id " & _
                                             ",prcostcenter_master.costcentercode AS Code " & _
                                             ",prcostcenter_master.costcentername AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM prcostcenter_master "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ = "SELECT " & _
                                             " hrsection_master.sectionunkid AS Id " & _
                                             ",hrsection_master.code AS Code " & _
                                             ",hrsection_master.name AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM hrsection_master "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ = "SELECT " & _
                                             " hrunit_master.unitunkid AS Id " & _
                                             ",hrunit_master.code AS Code " & _
                                             ",hrunit_master.name AS NAME " & _
                                             ",ISNULL(aCnt,0) AS aCnt " & _
                                           "FROM hrunit_master "
                            End Select

                            'Shani(24-Aug-2015) -- Start
                            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


                            'StrQ &= "JOIN " & _
                            '        "( " & _
                            '             "SELECT " & _
                            '                   " COUNT(employeeunkid) aCnt "
                            'Select Case intViewDetailByIdx
                            '    Case enViewDetailsByIdx.DEPARTMENT_WISE
                            '        StrQ &= ", hremployee_master.departmentunkid AS Id "
                            '    Case enViewDetailsByIdx.BRANCH_WISE
                            '        StrQ &= ", hremployee_master.stationunkid AS Id "
                            '    Case enViewDetailsByIdx.COSTCENTER_WISE
                            '        StrQ &= ", hremployee_master.costcenterunkid AS Id "
                            '    Case enViewDetailsByIdx.SECTION_WISE
                            '        StrQ &= ", hremployee_master.sectionunkid AS Id "
                            '    Case enViewDetailsByIdx.UNIT_WISE
                            '        StrQ &= ", hremployee_master.unitunkid AS Id "
                            'End Select
                            'StrQ &= " FROM hremployee_master " & _
                            '        " WHERE employeeunkid NOT IN (SELECT employeeunkid FROM hremployee_master WHERE YEAR(appointeddate) = @Year AND MONTH(appointeddate) NOT IN (" & mstrStaffDelailsMonthIds & ") AND isactive = 1) " & _
                            '        "   AND employeeunkid NOT IN (SELECT employeeunkid FROM hremployee_master WHERE YEAR(termination_from_date) = @Year AND MONTH(termination_from_date) IN (" & mstrStaffDelailsMonthIds & ")) "
                            'Select Case intViewDetailByIdx
                            '    Case enViewDetailsByIdx.DEPARTMENT_WISE
                            '        StrQ &= "    Group BY hremployee_master.departmentunkid " & _
                            '                ")AS A ON A.Id = hrdepartment_master.departmentunkid "
                            '    Case enViewDetailsByIdx.BRANCH_WISE
                            '        StrQ &= "    Group BY hremployee_master.stationunkid " & _
                            '                ")AS A ON A.Id = hrstation_master.stationunkid "
                            '    Case enViewDetailsByIdx.COSTCENTER_WISE
                            '        StrQ &= "    Group BY hremployee_master.costcenterunkid " & _
                            '                ")AS A ON A.Id = prcostcenter_master.costcenterunkid "
                            '    Case enViewDetailsByIdx.SECTION_WISE
                            '        StrQ &= "    Group BY hremployee_master.sectionunkid " & _
                            '                ")AS A ON A.Id = hrsection_master.sectionunkid "
                            '    Case enViewDetailsByIdx.UNIT_WISE
                            '        StrQ &= "    Group BY hremployee_master.unitunkid " & _
                            '                ")AS A ON A.Id = hrunit_master.unitunkid "
                            'End Select
                            StrQ &= "JOIN " & _
                                    "( " & _
                                         "SELECT " & _
                                               " COUNT(hremployee_master.employeeunkid) aCnt "
                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ &= ", Alloc.departmentunkid AS Id "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ &= ", Alloc.stationunkid AS Id "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ &= ", CC.costcenterunkid AS Id "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ &= ", Alloc.sectionunkid AS Id "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ &= ", Alloc.unitunkid AS Id "
                            End Select

                            StrQ &= " FROM hremployee_master "

                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         departmentunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         stationunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "( " & _
                                            "    SELECT " & _
                                            "         cctranheadvalueid AS costcenterunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_cctranhead_tran " & _
                                            "    WHERE istransactionhead = 0 AND isvoid = 0 " & _
                                            "    AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         sectionunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ &= "LEFT JOIN " & _
                                            "(                      " & _
                                            "    SELECT " & _
                                            "         unitunkid " & _
                                            "        ,employeeunkid " & _
                                            "        ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                                            "    FROM hremployee_transfer_tran " & _
                                            "    WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                            ") AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 "
                            End Select
                            StrQ &= "LEFT JOIN " & _
                                    "   ( " & _
                                    "       SELECT " & _
                                    "            employeeunkid AS TEEmpId " & _
                                    "           ,date2 AS termination_from_date " & _
                                    "           ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                                    "       FROM hremployee_dates_tran " & _
                                    "       WHERE isvoid = 0 AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                                    "   ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid AND ETERM.Rno = 1"

                            StrQ &= " WHERE hremployee_master.employeeunkid NOT IN (SELECT employeeunkid FROM hremployee_master WHERE YEAR(appointeddate) = @Year AND MONTH(appointeddate) NOT IN (" & mstrStaffDelailsMonthIds & ") AND isactive = 1) " & _
                                   "   AND hremployee_master.employeeunkid NOT IN (SELECT employeeunkid FROM hremployee_master WHERE YEAR(ETERM.termination_from_date) = @Year AND MONTH(ETERM.termination_from_date) IN (" & mstrStaffDelailsMonthIds & ")) "

                            Select Case intViewDetailByIdx
                                Case enViewDetailsByIdx.DEPARTMENT_WISE
                                    StrQ &= "    Group BY Alloc.departmentunkid " & _
                                            ")AS A ON A.Id = hrdepartment_master.departmentunkid "
                                Case enViewDetailsByIdx.BRANCH_WISE
                                    StrQ &= "    Group BY Alloc.stationunkid " & _
                                            ")AS A ON A.Id = hrstation_master.stationunkid "
                                Case enViewDetailsByIdx.COSTCENTER_WISE
                                    StrQ &= "    Group BY CC.costcenterunkid " & _
                                            ")AS A ON A.Id = prcostcenter_master.costcenterunkid "
                                Case enViewDetailsByIdx.SECTION_WISE
                                    StrQ &= "    Group BY Alloc.sectionunkid " & _
                                            ")AS A ON A.Id = hrsection_master.sectionunkid "
                                Case enViewDetailsByIdx.UNIT_WISE
                                    StrQ &= "    Group BY Alloc.unitunkid " & _
                                            ")AS A ON A.Id = hrunit_master.unitunkid "
                            End Select
                            'Shani(24-Aug-2015) -- End
                    End Select


            End Select

            objDataOperation.AddParameter("@Month", SqlDbType.Int, eZeeDataType.INT_SIZE, intDetailValue)
            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@Year", SqlDbType.Int, eZeeDataType.INT_SIZE, ConfigParameter._Object._CurrentDateAndTime.Year)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@Year", SqlDbType.Int, eZeeDataType.INT_SIZE, FinancialYear._Object._Database_Start_Date.Year)
            objDataOperation.AddParameter("@Year", SqlDbType.Int, eZeeDataType.INT_SIZE, mdtDatabaseStartDate.Year)
            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [ 28 FEB 2012 ] -- END


            dsDetals = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsDetals

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Staff_Detail_Informatiom", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function Fill_Divison_Combo(Optional ByVal StrList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT 0 AS Id, @Select AS DValue " & _
                   " UNION SELECT 1 AS Id,'1,000' AS DValue " & _
                   " UNION SELECT 2 AS Id,'10,000' AS DValue " & _
                   " UNION SELECT 3 AS Id,'100,000' AS DValue " & _
                   " UNION SELECT 4 AS Id,'1,000,000' AS DValue " & _
                   " UNION SELECT 5 AS Id,'10,000,000' AS DValue " & _
                   " UNION SELECT 6 AS Id,'100,000,000' AS DValue "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 16, "SELECT"))

            If Trim(StrList).Length <= 0 Then StrList = "List"

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Divison_Combo", mstrModuleName)
            Return Nothing
        Finally
            dsList.Dispose()
            StrQ = String.Empty
        End Try
    End Function

    Public Function Salary_AnalysisToGrid(Optional ByVal StrList As String = "List", Optional ByVal StrDivisonValue As String = "") As DataTable
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Try

            objDataOperation = New clsDataOperation

            StrQ = "SELECT " & _
                   "  periodunkid AS PId " & _
                   " ,period_code AS PCode " & _
                   " ,period_name AS PName " & _
                   " ,CONVERT(CHAR(8),start_date,112) AS ST_Date " & _
                   " ,CONVERT(CHAR(8),end_date,112) AS ED_Date " & _
                   " ,statusid AS PStatusId "
            If StrDivisonValue.Trim.Length > 0 Then
                StrQ &= "    ,ISNULL(PSal.Salary,0)/ " & CDec(StrDivisonValue) & " AS Salary "
            Else
                StrQ &= "    ,ISNULL(PSal.Salary,0) AS Salary "
            End If
            StrQ &= "   ,ISNULL(EmpId,0) Total_Cnt " & _
                    "   ,ISNULL(Descr,'') AS Description " & _
                    "FROM cfcommon_period_tran " & _
                    "JOIN " & _
                    "( " & _
                         "SELECT " & _
                              "COUNT(DISTINCT prempsalary_tran.employeeunkid) AS EmpId " & _
                              ",SUM(prempsalary_tran.amount) AS Salary " & _
                              ",prpayment_tran.periodunkid AS SalPId " & _
                              ",@Caption1 As Descr " & _
                         "FROM prempsalary_tran " & _
                              "JOIN prpayment_tran ON prempsalary_tran.paymenttranunkid = prpayment_tran.paymenttranunkid " & _
                              "   AND CONVERT(CHAR(8), prempsalary_tran.paymentdate, 112) = CONVERT(CHAR(8), prpayment_tran.paymentdate, 112) " & _
                              "JOIN hremployee_master ON prempsalary_tran.employeeunkid = hremployee_master.employeeunkid " & _
                         "WHERE   ISNULL(prempsalary_tran.isvoid,0) = 0 " & _
                              "AND ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                              "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                              "AND hremployee_master.isactive = 1 " & _
                        "GROUP BY prpayment_tran.periodunkid " & _
                    "UNION ALL " & _
                         "SELECT " & _
                              "COUNT(DISTINCT prpayment_tran.employeeunkid) AS EmpId " & _
                              ",SUM(prpayment_tran.amount) AS Salary " & _
                              ",prpayment_tran.periodunkid AS SalPId " & _
                              ",@Caption2 As Descr " & _
                         "FROM  prpayment_tran " & _
                              "JOIN hremployee_master ON prpayment_tran.employeeunkid = hremployee_master.employeeunkid " & _
                         "WHERE ISNULL(prpayment_tran.isvoid,0) = 0 " & _
                              "AND prpayment_tran.referenceid = 3 " & _
                              "AND prpayment_tran.paymentmode = 1 " & _
                              "AND hremployee_master.isactive = 1 " & _
                        "GROUP BY prpayment_tran.periodunkid " & _
                    "UNION ALL " & _
                         "SELECT " & _
                               "COUNT(prtnaleave_tran.employeeunkid) AS EmpId " & _
                              ",SUM(prtnaleave_tran.balanceamount) AS Salary " & _
                              ",prtnaleave_tran.payperiodunkid AS SalPId " & _
                              ",@Caption3 As Descr " & _
                         "FROM prtnaleave_tran " & _
                              "JOIN hremployee_master ON prtnaleave_tran.employeeunkid = hremployee_master.employeeunkid " & _
                         "WHERE ISNULL(prtnaleave_tran.isvoid,0) = 0 " & _
                              "AND prtnaleave_tran.payperiodunkid = 1 " & _
                              "AND prtnaleave_tran.balanceamount <> 0 " & _
                              "AND hremployee_master.isactive = 1 " & _
                        "GROUP BY prtnaleave_tran.payperiodunkid " & _
                    ") AS PSal ON PSal.SalPId = cfcommon_period_tran.periodunkid " & _
                    "WHERE modulerefid = " & enModuleReference.Payroll & " AND isactive = 1 AND YEAR(start_date) >= YEAR(@CurrYear)  Order by st_date DESC "
            'Sohail (25 Mar 2015) - ["AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _]

            objDataOperation.AddParameter("@Caption1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Total Bank Salary"))
            objDataOperation.AddParameter("@Caption2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Total Cash Salary"))
            objDataOperation.AddParameter("@Caption3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Total Hold Salary"))



            'S.SANDEEP [ 28 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOperation.AddParameter("@CurrYear", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@CurrYear", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, FinancialYear._Object._Database_Start_Date)
            objDataOperation.AddParameter("@CurrYear", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDatabaseStartDate)
            'Shani(24-Aug-2015) -- End

            'S.SANDEEP [ 28 FEB 2012 ] -- END



            dsList = objDataOperation.ExecQuery(StrQ, StrList)


            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim dtTable As New DataTable(StrList)

            dtTable.Columns.Add("PId", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("PCode", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("PName", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("Salary", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("Description", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim mIntGrpId As Integer = -1
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    Dim dRow As DataRow = dtTable.NewRow

                    If mIntGrpId <> dtRow.Item("PId") Then
                        dRow.Item("PId") = dtRow.Item("PId")
                        dRow.Item("PCode") = dtRow.Item("PCode")
                        dRow.Item("PName") = dtRow.Item("PName")
                        dRow.Item("Salary") = ""
                        dRow.Item("GrpId") = dtRow.Item("PId")
                        dRow.Item("IsGrp") = True
                        mIntGrpId = dtRow.Item("PId")

                        dtTable.Rows.Add(dRow)

                        dRow = dtTable.NewRow

                        dRow.Item("PId") = dtRow.Item("PId")
                        dRow.Item("PCode") = ""
                        dRow.Item("PName") = Space(5) & dtRow.Item("Description") & " [ " & dtRow.Item("Total_Cnt") & " ]"
                        dRow.Item("Salary") = Format(CDec(dtRow.Item("Salary")), GUI.fmtCurrency)
                        dRow.Item("GrpId") = dtRow.Item("PId")
                        dRow.Item("IsGrp") = False

                        dtTable.Rows.Add(dRow)

                    Else
                        dRow.Item("PId") = dtRow.Item("PId")
                        dRow.Item("PCode") = ""
                        dRow.Item("PName") = Space(5) & dtRow.Item("Description") & " [ " & dtRow.Item("Total_Cnt") & " ]"
                        dRow.Item("Salary") = Format(CDec(dtRow.Item("Salary")), GUI.fmtCurrency)
                        dRow.Item("GrpId") = dtRow.Item("PId")
                        dRow.Item("IsGrp") = False

                        dtTable.Rows.Add(dRow)
                    End If
                Next
            End If

            Return dtTable

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Salary_AnalysisToGrid", mstrModuleName)
            Return Nothing
        End Try
    End Function

    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS


    'Public Function EmployeeDates(ByVal StrList As String) As DataSet
    '    Dim dsList As New DataSet
    '    Dim dsTran As New DataSet
    '    Dim dTable As New DataTable
    '    Dim StrQ As String = String.Empty
    '    Try
    '        objDataOperation = New clsDataOperation


    '        'S.SANDEEP [ 16 MAY 2012 ] -- START
    '        'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    '        'StrQ = "SELECT 1 AS ID,@PROBATION AS MODE UNION " & _
    '        '       "SELECT 2 AS ID,@SUSPENSION AS MODE UNION " & _
    '        '       "SELECT 3 AS ID,@APPOINTED AS MODE UNION " & _
    '        '       "SELECT 4 AS ID,@CONFIRMED AS MODE UNION " & _
    '        '       "SELECT 5 AS ID,@BIRTHDAY AS MODE UNION " & _
    '        '       "SELECT 6 AS ID,@ANNIVERSARY AS MODE UNION " & _
    '        '       "SELECT 7 AS ID,@TERMINATING AS MODE UNION " & _
    '        '       "SELECT 8 AS ID,@RETIRING AS MODE UNION " & _
    '        '       "SELECT 9 AS ID,@FORCASTED_RETIRING AS MODE "
    '        Dim StrPrivilegeIds As String = ""
    '        If User._Object.Privilege._Show_Probation_Dates Then
    '            StrPrivilegeIds &= ",1"
    '        End If
    '        If User._Object.Privilege._Show_Suspension_Dates Then
    '            StrPrivilegeIds &= ",2"
    '        End If
    '        If User._Object.Privilege._Show_Appointment_Dates Then
    '            StrPrivilegeIds &= ",3"
    '        End If
    '        If User._Object.Privilege._Show_Confirmation_Dates Then
    '            StrPrivilegeIds &= ",4"
    '        End If
    '        If User._Object.Privilege._Show_BirthDates Then
    '            StrPrivilegeIds &= ",5"
    '        End If
    '        If User._Object.Privilege._Show_Anniversary_Dates Then
    '            StrPrivilegeIds &= ",6"
    '        End If
    '        If User._Object.Privilege._Show_Contract_Ending_Dates Then
    '            StrPrivilegeIds &= ",7"
    '        End If
    '        If User._Object.Privilege._Show_TodayRetirement_Dates Then
    '            StrPrivilegeIds &= ",8"
    '        End If
    '        If User._Object.Privilege._Show_ForcastedRetirement_Dates Then
    '            StrPrivilegeIds &= ",9"
    '        End If
    '        If User._Object.Privilege._Show_ForcastedEOC_Dates Then
    '            StrPrivilegeIds &= ",10"
    '        End If

    '        'Pinkal (03-Nov-2014) -- Start
    '        'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
    '        If User._Object.Privilege._Show_ForcastedELC_Dates Then
    '            StrPrivilegeIds &= ",11"
    '        End If
    '        'Pinkal (03-Nov-2014) -- End

    '        If StrPrivilegeIds.Trim.Length > 0 Then
    '            StrPrivilegeIds = Mid(StrPrivilegeIds, 2)
    '        Else
    '            StrPrivilegeIds = "0"
    '        End If

    '        StrQ = "SELECT ID, MODE,SortId FROM " & _
    '               "( " & _
    '               "    SELECT 1 AS ID,@PROBATION AS MODE,1 AS SortId UNION " & _
    '               "    SELECT 2 AS ID,@SUSPENSION AS MODE,2 AS SortId UNION " & _
    '               "    SELECT 3 AS ID,@APPOINTED AS MODE,3 AS SortId UNION " & _
    '               "    SELECT 4 AS ID,@CONFIRMED AS MODE,4 AS SortId UNION " & _
    '               "    SELECT 5 AS ID,@BIRTHDAY AS MODE,5 AS SortId UNION " & _
    '               "    SELECT 6 AS ID,@ANNIVERSARY AS MODE,6 AS SortId UNION " & _
    '               "    SELECT 7 AS ID,@TERMINATING AS MODE,7 AS SortId UNION " & _
    '               "    SELECT 8 AS ID,@RETIRING AS MODE,9 AS SortId UNION " & _
    '               "    SELECT 9 AS ID,@FORCASTED_RETIRING AS MODE,10 AS SortId UNION " & _
    '               "    SELECT 10 AS ID,@FORCASTED_EOC AS MODE,8 AS SortId UNION" & _
    '               "    SELECT 11 AS ID,@FORCASTED_ELC AS MODE,11 AS SortId " & _
    '               ") AS A WHERE A.ID IN (" & StrPrivilegeIds & ") " & _
    '               " ORDER BY SortId ASC "
    '        'S.SANDEEP [ 16 MAY 2012 ] -- END

    '        objDataOperation.AddParameter("@PROBATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "PROBATION WITHIN 30 DAYS FROM NOW."))
    '        objDataOperation.AddParameter("@SUSPENSION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "SUSPENSION WITHIN 30 DAYS FROM NOW."))
    '        objDataOperation.AddParameter("@APPOINTED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "TOTAL APPOINTED WITHIN 30 DAYS FROM NOW."))
    '        objDataOperation.AddParameter("@CONFIRMED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "TOTAL CONFIRMED WITHIN 30 DAYS FROM NOW."))
    '        objDataOperation.AddParameter("@BIRTHDAY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "TODAY'S BIRTHDAY."))
    '        objDataOperation.AddParameter("@ANNIVERSARY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "TODAY'S ANNIVERSARY."))
    '        objDataOperation.AddParameter("@TERMINATING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "TOTAL CONTRACTS ENDING WITHIN 30 DAYS FROM NOW."))
    '        objDataOperation.AddParameter("@RETIRING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "TODAY'S RETIREMENT."))

    '        'S.SANDEEP [ 16 MAY 2012 ] -- START
    '        'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    '        'objDataOperation.AddParameter("@FORCASTED_RETIRING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 25, "TOTAL FORCASTED RETIREMENT WITHIN SIX MONTHS FROM NOW."))
    '        Dim StrGrp As String = String.Empty
    '        Select Case ConfigParameter._Object._Forcasted_ViewSetting
    '            Case clsConfigOptions.enForcastedSetting.BY_MONTH
    '                StrGrp = Language.getMessage(mstrModuleName, 25, "TOTAL FORCASTED RETIREMENT WITHIN ") & NumToWord(CDec(ConfigParameter._Object._ForcastedValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 26, " MONTH(S) FROM NOW.")
    '            Case clsConfigOptions.enForcastedSetting.BY_YEAR
    '                StrGrp = Language.getMessage(mstrModuleName, 25, "TOTAL FORCASTED RETIREMENT WITHIN ") & NumToWord(CDec(ConfigParameter._Object._ForcastedValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 27, " YEAR(S) FROM NOW.")
    '        End Select
    '        objDataOperation.AddParameter("@FORCASTED_RETIRING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrGrp)
    '        'S.SANDEEP [ 16 MAY 2012 ] -- END


    '        'S.SANDEEP [ 24 MAY 2012 ] -- START
    '        'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    '        Dim StrEOCGrp As String = String.Empty
    '        Select Case ConfigParameter._Object._ForcastedEOC_ViewSetting
    '            Case clsConfigOptions.enForcastedSetting.BY_MONTH
    '                StrEOCGrp = Language.getMessage(mstrModuleName, 28, "TOTAL FORCASTED CONTRACTS ENDING WITHIN ") & NumToWord(CDec(ConfigParameter._Object._ForcastedEOCValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 26, " MONTH(S) FROM NOW.")
    '            Case clsConfigOptions.enForcastedSetting.BY_YEAR
    '                StrEOCGrp = Language.getMessage(mstrModuleName, 28, "TOTAL FORCASTED CONTRACTS ENDING WITHIN ") & NumToWord(CDec(ConfigParameter._Object._ForcastedEOCValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 27, " YEAR(S) FROM NOW.")
    '        End Select
    '        objDataOperation.AddParameter("@FORCASTED_EOC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrEOCGrp)
    '        'S.SANDEEP [ 24 MAY 2012 ] -- END



    '        'Pinkal (03-Nov-2014) -- Start
    '        'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
    '        Dim StrELCGrp As String = String.Empty
    '        Select Case ConfigParameter._Object._ForcastedELCViewSetting
    '            Case clsConfigOptions.enForcastedSetting.BY_DAY
    '                StrELCGrp = Language.getMessage(mstrModuleName, 43, "TOTAL FORCASTED EMPLOYEE LEAVE CYCLE ENDING WITHIN ") & NumToWord(CDec(ConfigParameter._Object._ForcastedELCValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 44, " DAY(S) FROM NOW.")
    '        End Select
    '        objDataOperation.AddParameter("@FORCASTED_ELC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrELCGrp)
    '        'Pinkal (03-Nov-2014) -- End


    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If



    '        'S.SANDEEP [ 20 MARCH 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES 

    '        'StrQ = "SELECT " & _
    '        '           " employeecode AS Ecode " & _
    '        '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
    '        '           ",1 AS ID " & _
    '        '           ",CONVERT(CHAR(8),@Date,112) AS Date " & _
    '        '           ",employeeunkid AS EmpId " & _
    '        '       "FROM hremployee_master " & _
    '        '       "WHERE CONVERT(CHAR(8),probation_to_date,112) BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8), DATEADD(dd,30,@Date),112) " & _
    '        '       "UNION ALL " & _
    '        '       "SELECT " & _
    '        '           " employeecode AS Ecode " & _
    '        '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
    '        '           ",2 AS ID " & _
    '        '           ",CONVERT(CHAR(8),@Date,112) AS Date " & _
    '        '           ",employeeunkid AS EmpId " & _
    '        '       "FROM hremployee_master " & _
    '        '       "WHERE CONVERT(CHAR(8),suspended_to_date,112)BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8), DATEADD(dd,30,@Date),112) " & _
    '        '       "UNION ALL " & _
    '        '       "SELECT " & _
    '        '           " employeecode AS Ecode " & _
    '        '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
    '        '           ",3 AS ID " & _
    '        '           ",CONVERT(CHAR(8),appointeddate,112) AS Date " & _
    '        '           ",employeeunkid AS EmpId " & _
    '        '       "FROM hremployee_master " & _
    '        '       "WHERE CONVERT(CHAR(8),appointeddate,112) BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8), DATEADD(dd,30,@Date),112) " & _
    '        '       "UNION ALL " & _
    '        '       "SELECT " & _
    '        '           " employeecode AS Ecode " & _
    '        '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
    '        '           ",4 AS ID " & _
    '        '           ",CONVERT(CHAR(8),@Date,112) AS Date " & _
    '        '           ",employeeunkid AS EmpId " & _
    '        '       "FROM hremployee_master " & _
    '        '       "WHERE CONVERT(CHAR(8),confirmation_date,112) BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8), DATEADD(dd,30,@Date),112) " & _
    '        '       "UNION ALL " & _
    '        '       "SELECT " & _
    '        '           " employeecode AS Ecode " & _
    '        '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
    '        '           ",5 AS ID " & _
    '        '           ",CONVERT(CHAR(8),birthdate,112) AS Date " & _
    '        '           ",employeeunkid AS EmpId " & _
    '        '       "FROM hremployee_master " & _
    '        '       "WHERE DATEPART(dd, @Date) = DATEPART(dd,birthdate) AND DATEPART(mm, @Date) = DATEPART(mm,birthdate) " & _
    '        '       "UNION ALL " & _
    '        '       "SELECT " & _
    '        '           " employeecode AS Ecode " & _
    '        '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
    '        '           ",6 AS ID " & _
    '        '           ",CONVERT(CHAR(8),anniversary_date,112) AS Date " & _
    '        '           ",employeeunkid AS EmpId " & _
    '        '       "FROM hremployee_master " & _
    '        '       "WHERE DATEPART(dd, @Date) = DATEPART(dd,anniversary_date) AND DATEPART(mm, @Date) = DATEPART(mm,anniversary_date) " & _
    '        '       "UNION ALL " & _
    '        '       "SELECT " & _
    '        '           " employeecode AS Ecode " & _
    '        '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
    '        '           ",7 AS ID " & _
    '        '           ",CONVERT(CHAR(8),termination_from_date,112) AS Date " & _
    '        '           ",employeeunkid AS EmpId " & _
    '        '       "FROM hremployee_master WHERE CONVERT(CHAR(8),termination_from_date,112) BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8),DATEADD(dd,30,@Date),112) " & _
    '        '       "UNION ALL " & _
    '        '       "SELECT " & _
    '        '           " employeecode AS Ecode " & _
    '        '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
    '        '           ",8 AS ID " & _
    '        '           ",CONVERT(CHAR(8),termination_to_date,112) AS Date " & _
    '        '           ",employeeunkid AS EmpId " & _
    '        '       "FROM hremployee_master " & _
    '        '       "WHERE CONVERT(CHAR(8),@Date,112)= CONVERT(CHAR(8),termination_to_date,112) " & _
    '        '       "UNION ALL " & _
    '        '       "SELECT " & _
    '        '           " employeecode AS Ecode " & _
    '        '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
    '        '           ",9 AS ID " & _
    '        '           ",CONVERT(CHAR(8),termination_to_date,112) AS Date " & _
    '        '           ",employeeunkid AS EmpId " & _
    '        '       "FROM hremployee_master " & _
    '        '       "WHERE termination_to_date BETWEEN CONVERT(CHAR(8),@Date+1,112) AND CONVERT(CHAR(8),DATEADD(mm,6,@Date),112) "

            'StrQ = "SELECT " & _
    '                "	 Ecode " & _
    '                "	,EName " & _
    '                "	,ID " & _
    '                "	,Date " & _
    '                "	,EmpId " & _
    '                "	,SortDate " & _
    '                "	,stationunkid " & _
    '                "	,deptgroupunkid " & _
    '                "	,departmentunkid " & _
    '                "	,sectiongroupunkid " & _
    '                "	,sectionunkid " & _
    '                "	,unitgroupunkid " & _
    '                "	,unitunkid " & _
    '                "	,teamunkid " & _
    '                "	,jobgroupunkid " & _
    '                "	,jobunkid " & _
    '                "   ,classgroupunkid " & _
    '                "   ,classunkid " & _
    '                "FROM " & _
    '                "( " & _
    '                "	SELECT " & _
            '           " employeecode AS Ecode " & _
            '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '           ",1 AS ID " & _
    '                "	   ,CONVERT(CHAR(8), probation_to_date, 112) AS Date " & _
    '                "	   ,probation_to_date AS SortDate " & _
            '           ",employeeunkid AS EmpId " & _
    '                "	   ,stationunkid " & _
    '                "	   ,deptgroupunkid " & _
    '                "	   ,departmentunkid " & _
    '                "	   ,sectiongroupunkid " & _
    '                "	   ,sectionunkid " & _
    '                "	   ,unitgroupunkid " & _
    '                "	   ,unitunkid " & _
    '                "	   ,teamunkid " & _
    '                "	   ,jobgroupunkid " & _
    '                "	   ,jobunkid " & _
    '                "      ,classgroupunkid " & _
    '                "      ,classunkid " & _
            '       "FROM hremployee_master " & _
            '       "WHERE CONVERT(CHAR(8),probation_to_date,112) BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8), DATEADD(dd,30,@Date),112) " & _
    '                " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= '" & ConfigParameter._Object._EmployeeAsOnDate & "' " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),'" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),'" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), '" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
            '       "UNION ALL " & _
            '       "SELECT " & _
            '           " employeecode AS Ecode " & _
            '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '           ",2 AS ID " & _
    '                "	   ,CONVERT(CHAR(8), suspended_to_date, 112) AS Date " & _
    '                "	   ,suspended_to_date AS sortdate " & _
            '           ",employeeunkid AS EmpId " & _
    '                "	   ,stationunkid " & _
    '                "	   ,deptgroupunkid " & _
    '                "	   ,departmentunkid " & _
    '                "	   ,sectiongroupunkid " & _
    '                "	   ,sectionunkid " & _
    '                "	   ,unitgroupunkid " & _
    '                "	   ,unitunkid " & _
    '                "	   ,teamunkid " & _
    '                "	   ,jobgroupunkid " & _
    '                "	   ,jobunkid " & _
    '                "      ,classgroupunkid " & _
    '                "      ,classunkid " & _
            '       "FROM hremployee_master " & _
            '       "WHERE CONVERT(CHAR(8),suspended_to_date,112)BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8), DATEADD(dd,30,@Date),112) " & _
    '                " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),'" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),'" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), '" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
            '       "UNION ALL " & _
            '       "SELECT " & _
            '           " employeecode AS Ecode " & _
            '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '           ",3 AS ID " & _
            '           ",CONVERT(CHAR(8),appointeddate,112) AS Date " & _
    '                "	   ,appointeddate AS sortdate " & _
            '           ",employeeunkid AS EmpId " & _
    '                "	   ,stationunkid " & _
    '                "	   ,deptgroupunkid " & _
    '                "	   ,departmentunkid " & _
    '                "	   ,sectiongroupunkid " & _
    '                "	   ,sectionunkid " & _
    '                "	   ,unitgroupunkid " & _
    '                "	   ,unitunkid " & _
    '                "	   ,teamunkid " & _
    '                "	   ,jobgroupunkid " & _
    '                "	   ,jobunkid " & _
    '                "      ,classgroupunkid " & _
    '                "      ,classunkid " & _
            '       "FROM hremployee_master " & _
            '       "WHERE CONVERT(CHAR(8),appointeddate,112) BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8), DATEADD(dd,30,@Date),112) " & _
    '                " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),'" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),'" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), '" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
            '       "UNION ALL " & _
            '       "SELECT " & _
            '           " employeecode AS Ecode " & _
            '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '           ",4 AS ID " & _
    '                "	   ,CONVERT(CHAR(8), confirmation_date, 112) AS Date " & _
    '                "	   ,confirmation_date AS sortdate " & _
            '           ",employeeunkid AS EmpId " & _
    '                "	   ,stationunkid " & _
    '                "	   ,deptgroupunkid " & _
    '                "	   ,departmentunkid " & _
    '                "	   ,sectiongroupunkid " & _
    '                "	   ,sectionunkid " & _
    '                "	   ,unitgroupunkid " & _
    '                "	   ,unitunkid " & _
    '                "	   ,teamunkid " & _
    '                "	   ,jobgroupunkid " & _
    '                "	   ,jobunkid " & _
    '                "      ,classgroupunkid " & _
    '                "      ,classunkid " & _
            '       "FROM hremployee_master " & _
            '       "WHERE CONVERT(CHAR(8),confirmation_date,112) BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8), DATEADD(dd,30,@Date),112) " & _
    '                " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),'" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),'" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), '" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
            '       "UNION ALL " & _
            '       "SELECT " & _
            '           " employeecode AS Ecode " & _
            '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '           ",5 AS ID " & _
            '           ",CONVERT(CHAR(8),birthdate,112) AS Date " & _
    '                "	   ,birthdate AS sortdate " & _
            '           ",employeeunkid AS EmpId " & _
    '                "	   ,stationunkid " & _
    '                "	   ,deptgroupunkid " & _
    '                "	   ,departmentunkid " & _
    '                "	   ,sectiongroupunkid " & _
    '                "	   ,sectionunkid " & _
    '                "	   ,unitgroupunkid " & _
    '                "	   ,unitunkid " & _
    '                "	   ,teamunkid " & _
    '                "	   ,jobgroupunkid " & _
    '                "	   ,jobunkid " & _
    '                "      ,classgroupunkid " & _
    '                "      ,classunkid " & _
            '       "FROM hremployee_master " & _
            '       "WHERE DATEPART(dd, @Date) = DATEPART(dd,birthdate) AND DATEPART(mm, @Date) = DATEPART(mm,birthdate) " & _
    '                " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),'" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),'" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), '" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
            '       "UNION ALL " & _
            '       "SELECT " & _
            '           " employeecode AS Ecode " & _
            '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '           ",6 AS ID " & _
            '           ",CONVERT(CHAR(8),anniversary_date,112) AS Date " & _
    '                "	   ,anniversary_date AS sortdate " & _
            '           ",employeeunkid AS EmpId " & _
    '                "	   ,stationunkid " & _
    '                "	   ,deptgroupunkid " & _
    '                "	   ,departmentunkid " & _
    '                "	   ,sectiongroupunkid " & _
    '                "	   ,sectionunkid " & _
    '                "	   ,unitgroupunkid " & _
    '                "	   ,unitunkid " & _
    '                "	   ,teamunkid " & _
    '                "	   ,jobgroupunkid " & _
    '                "	   ,jobunkid " & _
    '                "      ,classgroupunkid " & _
    '                "      ,classunkid " & _
            '       "FROM hremployee_master " & _
            '       "WHERE DATEPART(dd, @Date) = DATEPART(dd,anniversary_date) AND DATEPART(mm, @Date) = DATEPART(mm,anniversary_date) " & _
    '                " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),'" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),'" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
    '                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), '" & ConfigParameter._Object._EmployeeAsOnDate & "' ) >= '" & ConfigParameter._Object._EmployeeAsOnDate & "'  " & _
            '       "UNION ALL " & _
            '       "SELECT " & _
            '           " employeecode AS Ecode " & _
            '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '           ",7 AS ID " & _
    '                "      ,CONVERT(CHAR(8),empl_enddate,112) AS Date " & _
    '                "	   ,empl_enddate AS sortdate " & _
            '           ",employeeunkid AS EmpId " & _
    '                "	   ,stationunkid " & _
    '                "	   ,deptgroupunkid " & _
    '                "	   ,departmentunkid " & _
    '                "	   ,sectiongroupunkid " & _
    '                "	   ,sectionunkid " & _
    '                "	   ,unitgroupunkid " & _
    '                "	   ,unitunkid " & _
    '                "	   ,teamunkid " & _
    '                "	   ,jobgroupunkid " & _
    '                "	   ,jobunkid " & _
    '                "      ,classgroupunkid " & _
    '                "      ,classunkid " & _
    '                "	FROM hremployee_master " & _
    '                "	WHERE CONVERT(CHAR(8), empl_enddate, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd,30, @Date), 112) " & _
            '       "UNION ALL " & _
            '       "SELECT " & _
            '           " employeecode AS Ecode " & _
            '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '           ",8 AS ID " & _
            '           ",CONVERT(CHAR(8),termination_to_date,112) AS Date " & _
    '                "	   ,termination_to_date AS sortdate " & _
            '           ",employeeunkid AS EmpId " & _
    '                "	   ,stationunkid " & _
    '                "	   ,deptgroupunkid " & _
    '                "	   ,departmentunkid " & _
    '                "	   ,sectiongroupunkid " & _
    '                "	   ,sectionunkid " & _
    '                "	   ,unitgroupunkid " & _
    '                "	   ,unitunkid " & _
    '                "	   ,teamunkid " & _
    '                "	   ,jobgroupunkid " & _
    '                "	   ,jobunkid " & _
    '                "      ,classgroupunkid " & _
    '                "      ,classunkid " & _
            '       "FROM hremployee_master " & _
    '                "WHERE CONVERT(CHAR(8),@Date,112)= CONVERT(CHAR(8),termination_to_date,112) AND empl_enddate IS NULL " & _
            '       "UNION ALL " & _
            '       "SELECT " & _
            '           " employeecode AS Ecode " & _
            '           ",ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
            '           ",9 AS ID " & _
            '           ",CONVERT(CHAR(8),termination_to_date,112) AS Date " & _
    '                "	   ,termination_to_date AS sortdate " & _
            '           ",employeeunkid AS EmpId " & _
    '                "	   ,stationunkid " & _
    '                "	   ,deptgroupunkid " & _
    '                "	   ,departmentunkid " & _
    '                "	   ,sectiongroupunkid " & _
    '                "	   ,sectionunkid " & _
    '                "	   ,unitgroupunkid " & _
    '                "	   ,unitunkid " & _
    '                "	   ,teamunkid " & _
    '                "	   ,jobgroupunkid " & _
    '                "	   ,jobunkid " & _
    '                "      ,classgroupunkid " & _
    '                "      ,classunkid " & _
    '               "FROM hremployee_master "
    '        Select Case ConfigParameter._Object._Forcasted_ViewSetting
    '            Case clsConfigOptions.enForcastedSetting.BY_MONTH
    '                StrQ &= "	WHERE termination_to_date BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(mm," & ConfigParameter._Object._ForcastedValue & ", @Date), 112) AND empl_enddate IS NULL "
    '            Case clsConfigOptions.enForcastedSetting.BY_YEAR
    '                StrQ &= "	WHERE termination_to_date BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(yy," & ConfigParameter._Object._ForcastedValue & ", @Date), 112) AND empl_enddate IS NULL "
    '        End Select
    '        StrQ &= "UNION ALL " & _
    '                "SELECT " & _
    '                "       employeecode AS Ecode " & _
    '                "      ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
    '                "      ,10 AS ID " & _
    '                "      ,CONVERT(CHAR(8),empl_enddate,112) AS Date " & _
    '                "	   ,empl_enddate AS sortdate " & _
    '                "      ,employeeunkid AS EmpId " & _
    '                "	   ,stationunkid " & _
    '                "	   ,deptgroupunkid " & _
    '                "	   ,departmentunkid " & _
    '                "	   ,sectiongroupunkid " & _
    '                "	   ,sectionunkid " & _
    '                "	   ,unitgroupunkid " & _
    '                "	   ,unitunkid " & _
    '                "	   ,teamunkid " & _
    '                "	   ,jobgroupunkid " & _
    '                "	   ,jobunkid " & _
    '                "      ,classgroupunkid " & _
    '                "      ,classunkid " & _
    '                "FROM hremployee_master "
    '        Select Case ConfigParameter._Object._ForcastedEOC_ViewSetting
    '            Case clsConfigOptions.enForcastedSetting.BY_MONTH
    '                StrQ &= "	WHERE empl_enddate BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(mm," & ConfigParameter._Object._ForcastedEOCValue & ", @Date), 112) "
    '            Case clsConfigOptions.enForcastedSetting.BY_YEAR
    '                StrQ &= "	WHERE empl_enddate BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(yy," & ConfigParameter._Object._ForcastedEOCValue & ", @Date), 112) "
    '        End Select



    '        'Pinkal (03-Nov-2014) -- Start
    '        'Enhancement -  ENHANCMENT ON LEAVE MODULE GIVEN BY TRA.
    '        'If (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo) AndAlso User._Object.Privilege._Show_ForcastedELC_Dates Then
    '        StrQ &= "UNION ALL " & _
    '           "SELECT " & _
    '           "       employeecode AS Ecode " & _
    '           "      ,ISNULL(firstname,'')+' '+ISNULL(othername,'')+' '+ISNULL(surname,'') AS EName " & _
    '           "      ,11 AS ID " & _
    '           "      ,CONVERT(CHAR(8),enddate,112) AS Date " & _
    '           "	   ,enddate AS sortdate " & _
    '           "      ,hremployee_master.employeeunkid AS EmpId " & _
    '           "	   ,stationunkid " & _
    '           "	   ,deptgroupunkid " & _
    '           "	   ,departmentunkid " & _
    '           "	   ,sectiongroupunkid " & _
    '           "	   ,sectionunkid " & _
    '           "	   ,unitgroupunkid " & _
    '           "	   ,unitunkid " & _
    '           "	   ,teamunkid " & _
    '           "	   ,jobgroupunkid " & _
    '           "	   ,jobunkid " & _
    '           "      ,classgroupunkid " & _
    '           "      ,classunkid " & _
            '       "FROM hremployee_master " & _
    '           " JOIN  lvleavebalance_tran ON lvleavebalance_tran.employeeunkid = hremployee_master.employeeunkid AND isshortleave = 0 and isvoid = 0 "

    '        If ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '            StrQ &= " AND isclose_fy = 0 "
    '        ElseIf ConfigParameter._Object._LeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '            StrQ &= " AND isopenelc = 1 "
    '        End If

    '        Select Case ConfigParameter._Object._ForcastedELCViewSetting
    '            Case clsConfigOptions.enForcastedSetting.BY_DAY
    '                StrQ &= "	WHERE enddate BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(dd," & ConfigParameter._Object._ForcastedELCValue & ", @Date), 112) "
    '        End Select
    '        'End If
    '        'Pinkal (03-Nov-2014) -- End



    '        StrQ &= ") AS A " & _
    '                "WHERE 1 = 1 "

    '        'S.SANDEEP [ 01 JUNE 2012 ] -- START
    '        'ENHANCEMENT : TRA DISCIPLINE CHANGES
    '        'Select Case ConfigParameter._Object._UserAccessModeSetting
    '        '    Case enAllocation.BRANCH
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.DEPARTMENT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.SECTION
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.UNIT
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.TEAM
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOB_GROUP
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND  jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        '    Case enAllocation.JOBS
    '        '        If UserAccessLevel._AccessLevel.Length > 0 Then
    '        '            StrQ &= " AND jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
    '        '        End If
    '        'End Select
    '        StrQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master.", "")
    '        'S.SANDEEP [ 01 JUNE 2012 ] -- END


    '        StrQ &= "ORDER BY A.ID,SortDate "
    '        'S.SANDEEP [ 20 MARCH 2012 ] -- END



    '        'S.SANDEEP [ 03 SEP 2014 ] -- START
    '        'AND empl_enddate IS NULL -- ADDED IN (8 & 9 SECTION)
    '        'S.SANDEEP [ 03 SEP 2014 ] -- END


    '        objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date)

    '        dsTran = objDataOperation.ExecQuery(StrQ, "Trans")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If


    '        'S.SANDEEP [ 28 MARCH 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
    '        dTable = New DataTable(StrList)
    '        dTable.Columns.Add("ECode", System.Type.GetType("System.String")).DefaultValue = ""
    '        dTable.Columns.Add("EName", System.Type.GetType("System.String")).DefaultValue = ""
    '        dTable.Columns.Add("Date", System.Type.GetType("System.String")).DefaultValue = ""
    '        dTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
    '        dTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
    '        dTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = -1
    '        'S.SANDEEP [ 28 MARCH 2012 ] -- END

    '        If dsTran.Tables("Trans").Rows.Count > 0 Then
    '            'dTable = New DataTable(StrList)
    '            'dTable.Columns.Add("ECode", System.Type.GetType("System.String")).DefaultValue = ""
    '            'dTable.Columns.Add("EName", System.Type.GetType("System.String")).DefaultValue = ""
    '            'dTable.Columns.Add("Date", System.Type.GetType("System.String")).DefaultValue = ""
    '            'dTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
    '            'dTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            'dTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = -1
    '            Dim dtRow As DataRow = Nothing
    '            For Each dRow As DataRow In dsList.Tables(0).Rows
    '                dtRow = dTable.NewRow
    '                dtRow.Item("ECode") = dRow.Item("MODE")
    '                dtRow.Item("EName") = ""
    '                dtRow.Item("Date") = ""
    '                dtRow.Item("IsGrp") = True
    '                dtRow.Item("GrpId") = dRow.Item("ID")
    '                dTable.Rows.Add(dtRow)

    '                Dim dFilter As DataTable = New DataView(dsTran.Tables(0), "ID = '" & dRow.Item("ID") & "'", "", DataViewRowState.CurrentRows).ToTable

    '                'S.SANDEEP [ 20 MARCH 2012 ] -- START
    '                'ENHANCEMENT : TRA CHANGES 
    '                Dim dCount() As DataRow = dTable.Select("GrpId = '" & dRow.Item("ID") & "' AND IsGrp = True")
    '                If dCount.Length > 0 Then
    '                    dCount(0)("ECode") = dCount(0)("ECode") & " [ " & dFilter.Rows.Count & " ]"
    '                End If
    '                'S.SANDEEP [ 20 MARCH 2012 ] -- END

    '                For Each dFRow As DataRow In dFilter.Rows
    '                    dtRow = dTable.NewRow

    '                    dtRow.Item("ECode") = Space(5) & dFRow.Item("Ecode")
    '                    dtRow.Item("EName") = dFRow.Item("EName")
    '                    dtRow.Item("Date") = eZeeDate.convertDate(dFRow.Item("Date").ToString).ToShortDateString
    '                    dtRow.Item("IsGrp") = False
    '                    dtRow.Item("GrpId") = dRow.Item("ID")
    '                    dtRow.Item("EmpId") = dFRow.Item("EmpId")
    '                    dTable.Rows.Add(dtRow)
    '                Next
    '            Next
    '            'S.SANDEEP [ 28 MARCH 2012 ] -- START
    '            'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
    '        Else
    '            For Each drRow As DataRow In dsList.Tables(0).Rows
    '                Dim dtRow As DataRow = dTable.NewRow
    '                dtRow.Item("ECode") = drRow.Item("MODE") & " [ " & "0" & " ]"
    '                dtRow.Item("EName") = ""
    '                dtRow.Item("Date") = ""
    '                dtRow.Item("IsGrp") = True
    '                dtRow.Item("GrpId") = drRow.Item("ID")
    '                dTable.Rows.Add(dtRow)
    '            Next
    '            'S.SANDEEP [ 28 MARCH 2012 ] -- END
    '        End If

    '        dsList.Tables.RemoveAt(0)

    '        dsList.Tables.Add(dTable.Copy)

    '        Return dsList

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "EmployeeDates", mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function

    Public Function EmployeeDates(ByVal StrList As String) As DataSet
        Dim dsList As New DataSet
        Dim dsTran As New DataSet
        Dim dTable As New DataTable
        Dim StrQ As String = String.Empty
        Try
            objDataOperation = New clsDataOperation

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtPeriodEndDate, mdtPeriodEndDate, , , mstrDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtPeriodEndDate, mblnOnlyApproved, mstrDatabaseName, mintuserUnkid, mintCompanyUnkid, mintYearUnkId, mstrUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtPeriodEndDate, mstrDatabaseName)

            Dim StrPrivilegeIds As String = ""
            If mblnShow_Probation_Dates Then
                StrPrivilegeIds &= ",1"
            End If
            If mblnShow_Suspension_Dates Then
                StrPrivilegeIds &= ",2"
            End If
            If mblnShow_Appointment_Dates Then
                StrPrivilegeIds &= ",3"
            End If
            If mblnShow_Confirmation_Dates Then
                StrPrivilegeIds &= ",4"
            End If
            If mblnShow_BirthDates Then
                StrPrivilegeIds &= ",5"
            End If
            If mblnShow_Anniversary_Dates Then
                StrPrivilegeIds &= ",6"
            End If
            If mblnShow_Contract_Ending_Dates Then
                StrPrivilegeIds &= ",7"
            End If
            If mblnShow_TodayRetirement_Dates Then
                StrPrivilegeIds &= ",8"
            End If
            If mblnShow_ForcastedRetirement_Dates Then
                StrPrivilegeIds &= ",9"
            End If
            If mblnShow_ForcastedEOC_Dates Then
                StrPrivilegeIds &= ",10"
            End If

            'S.SANDEEP [12-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#218|#ARUTI-106}
            'If mblnShow_ForcastedELC_Dates Then
            '    StrPrivilegeIds &= ",11"
            'End If
            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo = True Then
                If mblnShow_ForcastedELC_Dates Then
                    StrPrivilegeIds &= ",11"
                End If
            End If
            'S.SANDEEP [12-Apr-2018] -- END

 
            If StrPrivilegeIds.Trim.Length > 0 Then
                StrPrivilegeIds = Mid(StrPrivilegeIds, 2)
            Else
                StrPrivilegeIds = "0"
            End If

            StrQ = "SELECT ID, MODE,SortId FROM " & _
                   "( " & _
                   "    SELECT 1 AS ID,@PROBATION AS MODE,1 AS SortId UNION " & _
                   "    SELECT 2 AS ID,@SUSPENSION AS MODE,2 AS SortId UNION " & _
                   "    SELECT 3 AS ID,@APPOINTED AS MODE,3 AS SortId UNION " & _
                   "    SELECT 4 AS ID,@CONFIRMED AS MODE,4 AS SortId UNION " & _
                   "    SELECT 5 AS ID,@BIRTHDAY AS MODE,5 AS SortId UNION " & _
                   "    SELECT 6 AS ID,@ANNIVERSARY AS MODE,6 AS SortId UNION " & _
                   "    SELECT 7 AS ID,@TERMINATING AS MODE,7 AS SortId UNION " & _
                   "    SELECT 8 AS ID,@RETIRING AS MODE,9 AS SortId UNION " & _
                   "    SELECT 9 AS ID,@FORCASTED_RETIRING AS MODE,10 AS SortId UNION " & _
                   "    SELECT 10 AS ID,@FORCASTED_EOC AS MODE,8 AS SortId UNION" & _
                   "    SELECT 11 AS ID,@FORCASTED_ELC AS MODE,11 AS SortId " & _
                   ") AS A WHERE A.ID IN (" & StrPrivilegeIds & ") " & _
                   " ORDER BY SortId ASC "
            'S.SANDEEP [12-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#218|#ARUTI-106} [PENDINGLEAVES] -- END
 
            objDataOperation.AddParameter("@PROBATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "PROBATION WITHIN 30 DAYS FROM NOW."))
            objDataOperation.AddParameter("@SUSPENSION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "SUSPENSION WITHIN 30 DAYS FROM NOW."))
            objDataOperation.AddParameter("@APPOINTED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "TOTAL APPOINTED WITHIN 30 DAYS FROM NOW."))
            objDataOperation.AddParameter("@CONFIRMED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "TOTAL CONFIRMED WITHIN 30 DAYS FROM NOW."))
            objDataOperation.AddParameter("@BIRTHDAY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "TODAY'S BIRTHDAY."))
            objDataOperation.AddParameter("@ANNIVERSARY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "TODAY'S ANNIVERSARY."))
            objDataOperation.AddParameter("@TERMINATING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "TOTAL CONTRACTS ENDING WITHIN 30 DAYS FROM NOW."))
            objDataOperation.AddParameter("@RETIRING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "TODAY'S RETIREMENT."))

            Dim StrGrp As String = String.Empty
            Select Case mintForcasted_ViewSetting
                Case clsConfigOptions.enForcastedSetting.BY_MONTH
                    StrGrp = Language.getMessage(mstrModuleName, 25, "TOTAL FORCASTED RETIREMENT WITHIN ") & NumToWord(CDec(mintForcastedValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 26, " MONTH(S) FROM NOW.")
                Case clsConfigOptions.enForcastedSetting.BY_YEAR
                    StrGrp = Language.getMessage(mstrModuleName, 25, "TOTAL FORCASTED RETIREMENT WITHIN ") & NumToWord(CDec(mintForcastedValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 27, " YEAR(S) FROM NOW.")
            End Select
            objDataOperation.AddParameter("@FORCASTED_RETIRING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrGrp)

            Dim StrEOCGrp As String = String.Empty
            Select Case mintForcastedEOC_ViewSetting
                Case clsConfigOptions.enForcastedSetting.BY_MONTH
                    StrEOCGrp = Language.getMessage(mstrModuleName, 28, "TOTAL FORCASTED CONTRACTS ENDING WITHIN ") & NumToWord(CDec(mintForcastedEOCValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 26, " MONTH(S) FROM NOW.")
                Case clsConfigOptions.enForcastedSetting.BY_YEAR
                    StrEOCGrp = Language.getMessage(mstrModuleName, 28, "TOTAL FORCASTED CONTRACTS ENDING WITHIN ") & NumToWord(CDec(mintForcastedEOCValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 27, " YEAR(S) FROM NOW.")
            End Select
            objDataOperation.AddParameter("@FORCASTED_EOC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrEOCGrp)

            Dim StrELCGrp As String = String.Empty
            Select Case mintForcastedELCViewSetting
                Case clsConfigOptions.enForcastedSetting.BY_DAY
                    StrELCGrp = Language.getMessage(mstrModuleName, 43, "TOTAL FORCASTED EMPLOYEE LEAVE CYCLE ENDING WITHIN ") & NumToWord(CDec(mintForcastedELCValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 44, " DAY(S) FROM NOW.")
            End Select
            objDataOperation.AddParameter("@FORCASTED_ELC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrELCGrp)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            StrQ = "SELECT " & _
                    "	 hremployee_master.employeecode AS Ecode " & _
                    "	,ISNULL(firstname, '') + ' ' + ISNULL(othername, '') + ' ' + ISNULL(surname, '') AS EName " & _
                    "	,tblDate.ID " & _
                    "	,tblDate.Date " & _
                    "	,hremployee_master.employeeunkid AS EmpId " & _
                    "	,tblDate.SortDate " & _
                    "	,Alloc.stationunkid " & _
                    "	,Alloc.deptgroupunkid " & _
                    "	,Alloc.departmentunkid " & _
                    "	,Alloc.sectiongroupunkid " & _
                    "	,Alloc.sectionunkid " & _
                    "	,Alloc.unitgroupunkid " & _
                    "	,Alloc.unitunkid " & _
                    "	,Alloc.teamunkid " & _
                    "	,Jobs.jobgroupunkid " & _
                    "	,Jobs.jobunkid " & _
                    "   ,Alloc.classgroupunkid " & _
                    "   ,Alloc.classunkid " & _
                    "FROM hremployee_master " & _
                    "JOIN ( " & _
                    "           SELECT " & _
                    "                1 AS ID " & _
                    "	            ,CONVERT(CHAR(8), PrbDt.date2, 112) AS Date " & _
                    "	            ,PrbDt.date2 AS SortDate " & _
                    "               ,hremployee_master.employeeunkid AS EmpId " & _
                    "           FROM hremployee_master " & _
                    "               LEFT JOIN " & _
                    "( " & _
                    "	SELECT " & _
                    "                            date1 " & _
                    "                           ,date2 " & _
                    "                           ,employeeunkid " & _
                    "                           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "                       FROM hremployee_dates_tran " & _
                    "                       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_PROBATION & "' " & _
                    "                             AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "'" & _
                    "                   ) AS PrbDt ON PrbDt.employeeunkid = hremployee_master.employeeunkid  AND PrbDt.rno = 1 " & _
                    "               " & xDateJoinQry & " " & _
                    "           WHERE CONVERT(CHAR(8),PrbDt.date2,112) BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8), DATEADD(dd,30,@Date),112) " & _
                    "               " & IIf(xDateFilterQry.Trim.Length > 0, xDateFilterQry, " ") & " " & _
                    "       UNION ALL " & _
                    "           SELECT " & _
                    "                2 AS ID " & _
                    "	            ,CONVERT(CHAR(8), SuspDt.date2, 112) AS Date " & _
                    "	            ,SuspDt.date2 AS sortdate " & _
                    "               ,hremployee_master.employeeunkid AS EmpId " & _
                    "FROM hremployee_master " & _
                    "               LEFT JOIN " & _
                    "                   ( " & _
                    "                       SELECT " & _
                    "                            date1 " & _
                    "                           ,date2 " & _
                    "                           ,employeeunkid " & _
                    "                           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "                       FROM hremployee_dates_tran " & _
                    "                       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_SUSPENSION & "' " & _
                    "                           AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "'" & _
                    "                   ) AS SuspDt ON SuspDt.employeeunkid = hremployee_master.employeeunkid AND SuspDt.rno = 1 " & _
                    "               " & xDateJoinQry & " " & _
                    "           WHERE CONVERT(CHAR(8),SuspDt.date2,112)BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8), DATEADD(dd,30,@Date),112) " & _
                    "               " & xDateFilterQry & " " & _
                    "UNION ALL " & _
                    "SELECT " & _
                    "                3 AS ID " & _
                    "               ,CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS Date " & _
                    "	            ,hremployee_master.appointeddate AS sortdate " & _
                    "               ,hremployee_master.employeeunkid AS EmpId " & _
                    "FROM hremployee_master " & _
                    "               " & xDateJoinQry & " " & _
                    "           WHERE CONVERT(CHAR(8),hremployee_master.appointeddate,112) BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8), DATEADD(dd,30,@Date),112) " & _
                    "               " & xDateFilterQry & " " & _
                    "UNION ALL " & _
                    "SELECT " & _
                    "                4 AS ID " & _
                    "	            ,CONVERT(CHAR(8), CnfDt.date1, 112) AS Date " & _
                    "	            ,CnfDt.date1 AS sortdate " & _
                    "               ,hremployee_master.employeeunkid AS EmpId " & _
                    "FROM hremployee_master " & _
                    "               LEFT JOIN " & _
                    "                   ( " & _
                    "                       SELECT " & _
                    "                           date1 " & _
                    "                           ,employeeunkid " & _
                    "                           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "                       FROM hremployee_dates_tran " & _
                    "                       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_CONFIRMATION & "' AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "'" & _
                    "                   ) AS CnfDt ON CnfDt.employeeunkid = hremployee_master.employeeunkid AND CnfDt.rno = 1 " & _
                    "               " & xDateJoinQry & " " & _
                    "           WHERE CONVERT(CHAR(8),CnfDt.date1,112) BETWEEN CONVERT(CHAR(8),@Date,112) AND CONVERT(CHAR(8), DATEADD(dd,30,@Date),112) " & _
                    "               " & xDateFilterQry & " " & _
                    "UNION ALL " & _
                    "SELECT " & _
                    "                5 AS ID " & _
                    "               ,CONVERT(CHAR(8),hremployee_master.birthdate,112) AS Date " & _
                    "	            ,hremployee_master.birthdate AS sortdate " & _
                    "               ,hremployee_master.employeeunkid AS EmpId " & _
                    "FROM hremployee_master " & _
                    "               " & xDateJoinQry & " " & _
                    "           WHERE DATEPART(dd, @Date) = DATEPART(dd,hremployee_master.birthdate) AND DATEPART(mm, @Date) = DATEPART(mm,birthdate) " & _
                    "               " & xDateFilterQry & " " & _
                    "UNION ALL " & _
                    "SELECT " & _
                    "                6 AS ID " & _
                    "               ,CONVERT(CHAR(8),hremployee_master.anniversary_date,112) AS Date " & _
                    "	            ,hremployee_master.anniversary_date AS sortdate " & _
                    "               ,hremployee_master.employeeunkid AS EmpId " & _
                    "FROM hremployee_master " & _
                    "               " & xDateJoinQry & " " & _
                    "           WHERE DATEPART(dd, @Date) = DATEPART(dd,hremployee_master.anniversary_date) AND DATEPART(mm, @Date) = DATEPART(mm,anniversary_date) " & _
                    "               " & xDateFilterQry & " " & _
                    "UNION ALL " & _
                    "SELECT " & _
                    "                7 AS ID " & _
                    "               ,CONVERT(CHAR(8),Eoc.date1,112) AS Date " & _
                    "	            ,Eoc.date1 AS sortdate " & _
                    "               ,hremployee_master.employeeunkid AS EmpId " & _
                    "FROM hremployee_master " & _
                    "               LEFT JOIN " & _
                    "                   ( " & _
                    "                       SELECT " & _
                    "                           date1 " & _
                    "                           ,employeeunkid " & _
                    "                           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "                       FROM hremployee_dates_tran " & _
                    "                       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    "                   ) AS Eoc ON Eoc.employeeunkid = hremployee_master.employeeunkid AND Eoc.rno = 1 " & _
                    "               " & xDateJoinQry & " " & _
                    "	        WHERE CONVERT(CHAR(8), Eoc.date1, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd,30, @Date), 112) " & _
                    "               " & xDateFilterQry & " " & _
                    "UNION ALL " & _
                    "SELECT " & _
                    "                8 AS ID " & _
                    "               ,CONVERT(CHAR(8),Retr.date1,112) AS Date " & _
                    "	            ,Retr.date1 AS sortdate " & _
                    "               ,hremployee_master.employeeunkid AS EmpId " & _
                    "	FROM hremployee_master " & _
                    "               LEFT JOIN " & _
                    "                   (" & _
                    "                       SELECT " & _
                    "                            date1 " & _
                    "                           ,date2 " & _
                    "                           ,employeeunkid " & _
                    "                           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "                       FROM hremployee_dates_tran " & _
                    "                       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    "                   ) AS Retr ON Retr.employeeunkid = hremployee_master.employeeunkid AND Retr.rno = 1 " & _
                    "               " & xDateJoinQry & " " & _
                    "           WHERE CONVERT(CHAR(8),@Date,112)= CONVERT(CHAR(8),Retr.date1,112) " & _
                    "               " & xDateFilterQry & " " & _
                    "UNION ALL " & _
                    "SELECT " & _
                    "                9 AS ID " & _
                    "               ,CONVERT(CHAR(8),Retr.date1,112) AS Date " & _
                    "	            ,Retr.date1 AS sortdate " & _
                    "               ,hremployee_master.employeeunkid AS EmpId " & _
                    "FROM hremployee_master " & _
                    "               LEFT JOIN " & _
                    "                   ( " & _
                    "SELECT " & _
                    "                           date1 " & _
                    "                           ,date2 " & _
                    "                           ,employeeunkid " & _
                    "                           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "                       FROM hremployee_dates_tran " & _
                    "                       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    ") AS Retr ON Retr.employeeunkid = hremployee_master.employeeunkid  AND Retr.rno = 1 " & _
                    "               " & xDateJoinQry & " "

            'Shani(30-Jan-2016) -- Start
            'Old (3673) : - "                       WHERE isvoid = 0 AND datetypeunkid = '3' " & _
            'New (3673) : - "                       WHERE isvoid = 0 AND datetypeunkid = 'enEmp_Dates_Transaction.DT_SUSPENSION' " & _
            'Shani(08-Jan-2016) -- End

            Select Case mintForcasted_ViewSetting
                Case clsConfigOptions.enForcastedSetting.BY_MONTH
                    StrQ &= "	WHERE Retr.date1 BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(mm," & mintForcastedValue & ", @Date), 112) "
                Case clsConfigOptions.enForcastedSetting.BY_YEAR
                    StrQ &= "	WHERE Retr.date1 BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(yy," & mintForcastedValue & ", @Date), 112) "
            End Select
            StrQ &= "               " & xDateFilterQry & " " & _
                    "       UNION ALL " & _
                    "SELECT " & _
                    "                10 AS ID " & _
                    "               ,CONVERT(CHAR(8),EocDt.date1,112) AS Date " & _
                    "	            ,EocDt.date1 AS sortdate " & _
                    "               ,hremployee_master.employeeunkid AS EmpId " & _
                    "           FROM hremployee_master " & _
                    "               LEFT JOIN " & _
                    "                   ( " & _
                    "                       SELECT " & _
                    "                            date1 " & _
                    "                           ,date2 " & _
                    "                           ,employeeunkid " & _
                    "                           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "                       FROM hremployee_dates_tran " & _
                    "                       WHERE isvoid = 0 AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_TERMINATION & "' AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    "                   ) AS EocDt ON EocDt.employeeunkid = hremployee_master.employeeunkid " & _
                    "               " & xDateJoinQry & " "
            Select Case mintForcastedEOC_ViewSetting
                Case clsConfigOptions.enForcastedSetting.BY_MONTH
                    StrQ &= "	WHERE EocDt.date1 BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(mm," & mintForcastedEOCValue & ", @Date), 112) "
                Case clsConfigOptions.enForcastedSetting.BY_YEAR
                    StrQ &= "	WHERE EocDt.date1 BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(yy," & mintForcastedEOCValue & ", @Date), 112) "
            End Select

            StrQ &= "               " & xDateFilterQry & " "


            'Gajanan [03-SEP-2019] -- Start      
            'Bug: ref #4122 
            'There are employees showing with more than one entry of Forecasted EOC dates.
            StrQ &= " and EocDt.rno = 1"
            'Gajanan [03-SEP-2019] -- End

            If (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo) AndAlso User._Object.Privilege._Show_ForcastedELC_Dates Then
                StrQ &= "       UNION ALL " & _
                        "           SELECT " & _
                        "                11 AS ID " & _
                        "               ,CONVERT(CHAR(8),enddate,112) AS Date " & _
                        "	            ,enddate AS sortdate " & _
                        "               ,hremployee_master.employeeunkid AS EmpId " & _
                        "           FROM hremployee_master " & _
                        "           JOIN  lvleavebalance_tran ON lvleavebalance_tran.employeeunkid = hremployee_master.employeeunkid AND isshortleave = 0 and isvoid = 0 "

                If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    StrQ &= " AND isclose_fy = 0 "
                ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    StrQ &= " AND isopenelc = 1 "
                End If

                Select Case mintForcastedELCViewSetting
                    Case clsConfigOptions.enForcastedSetting.BY_DAY
                        StrQ &= "	WHERE enddate BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(dd," & mintForcastedELCValue & ", @Date), 112) "
                End Select
            End If
                   
            StrQ &= ") AS tblDate ON tblDate.EmpId = hremployee_master.employeeunkid " & _
                    "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "       stationunkid " & _
                    "	   ,deptgroupunkid " & _
                    "	   ,departmentunkid " & _
                    "	   ,sectiongroupunkid " & _
                    "	   ,sectionunkid " & _
                    "	   ,unitgroupunkid " & _
                    "	   ,unitunkid " & _
                    "	   ,teamunkid " & _
                    "      ,classgroupunkid " & _
                    "      ,classunkid " & _
                    "      ,employeeunkid " & _
                    "      ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "      FROM hremployee_transfer_tran " & _
                    "      WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    "   ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                    "LEFT JOIN " & _
                    "   ( " & _
                    "       SELECT " & _
                    "            jobunkid " & _
                    "           ,jobgroupunkid " & _
                    "           ,employeeunkid " & _
                    "           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                    "       FROM hremployee_categorization_tran " & _
                    "       WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' " & _
                    "   ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 "

            If xUACQry.Trim.Length > 0 Then
                StrQ &= xUACQry
                End If

            StrQ &= " WHERE 1 = 1"

            'S.SANDEEP [15 NOV 2016] -- START
            'If xUACFiltrQry.Trim.Length > 0 Then
            '    StrQ &= " AND " & xUACFiltrQry & " "
            'End If
            'S.SANDEEP [15 NOV 2016] -- END

            StrQ &= "ORDER BY tblDate.ID,tblDate.SortDate "

            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPeriodEndDate.Date)

            dsTran = objDataOperation.ExecQuery(StrQ, "Trans")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

                dTable = New DataTable(StrList)
                dTable.Columns.Add("ECode", System.Type.GetType("System.String")).DefaultValue = ""
                dTable.Columns.Add("EName", System.Type.GetType("System.String")).DefaultValue = ""
                dTable.Columns.Add("Date", System.Type.GetType("System.String")).DefaultValue = ""
                dTable.Columns.Add("IsGrp", System.Type.GetType("System.Boolean")).DefaultValue = False
                dTable.Columns.Add("GrpId", System.Type.GetType("System.Int32")).DefaultValue = -1
                dTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = -1

            If dsTran.Tables("Trans").Rows.Count > 0 Then
            
                Dim dtRow As DataRow = Nothing
                For Each dRow As DataRow In dsList.Tables(0).Rows

                    dtRow = dTable.NewRow
                    dtRow.Item("ECode") = dRow.Item("MODE")
                    dtRow.Item("EName") = ""
                    dtRow.Item("Date") = ""
                    dtRow.Item("IsGrp") = True
                    dtRow.Item("GrpId") = dRow.Item("ID")
                    dTable.Rows.Add(dtRow)

                    Dim dFilter As DataTable = New DataView(dsTran.Tables(0), "ID = '" & dRow.Item("ID") & "'", "", DataViewRowState.CurrentRows).ToTable

                    Dim dCount() As DataRow = dTable.Select("GrpId = '" & dRow.Item("ID") & "' AND IsGrp = True")
                    If dCount.Length > 0 Then
                        dCount(0)("ECode") = dCount(0)("ECode") & " [ " & dFilter.Rows.Count & " ]"
                    End If

                    For Each dFRow As DataRow In dFilter.Rows
                        dtRow = dTable.NewRow

                        dtRow.Item("ECode") = Space(5) & dFRow.Item("Ecode")
                        dtRow.Item("EName") = dFRow.Item("EName")
                        dtRow.Item("Date") = eZeeDate.convertDate(dFRow.Item("Date").ToString).ToShortDateString
                        dtRow.Item("IsGrp") = False
                        dtRow.Item("GrpId") = dRow.Item("ID")
                        dtRow.Item("EmpId") = dFRow.Item("EmpId")
                        dTable.Rows.Add(dtRow)
                    Next
                Next
             
            Else
                For Each drRow As DataRow In dsList.Tables(0).Rows
                    Dim dtRow As DataRow = dTable.NewRow
                    dtRow.Item("ECode") = drRow.Item("MODE") & " [ " & "0" & " ]"
                    dtRow.Item("EName") = ""
                    dtRow.Item("Date") = ""
                    dtRow.Item("IsGrp") = True
                    dtRow.Item("GrpId") = drRow.Item("ID")
                    dTable.Rows.Add(dtRow)
                Next

            End If

            dsList.Tables.RemoveAt(0)

            dsList.Tables.Add(dTable.Copy)

            'S.SANDEEP [12-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#218|#ARUTI-106}
            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo = True Then
                If mblnShow_PendingAccrueLeaves Then
                    StrQ = "IF OBJECT_ID('tempdb..#table') IS NOT NULL " & _
                           "DROP TABLE #table " & _
                           "CREATE TABLE #table (employeeunkid int,leavetypeunkid int,leavename nvarchar(max)) " & _
                           "INSERT INTO #table (employeeunkid,leavetypeunkid,leavename) " & _
                           "SELECT " & _
                           "     hremployee_master.employeeunkid " & _
                           "    ,A.leavetypeunkid " & _
                           "    ,A.leavename " & _
                           "FROM hremployee_master "

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= " " & xAdvanceJoinQry & " "
                    End If

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= " " & xDateJoinQry & " "
                    End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= " " & xUACQry & " "
                    End If

                    StrQ &= "LEFT JOIN " & _
                            "( " & _
                            "    SELECT " & _
                            "         leavetypeunkid " & _
                            "        ,ISNULL(gender,0) gender " & _
                            "        ,leavename " & _
                            "    FROM lvleavetype_master WHERE isactive = 1 AND isaccrueamount = 1 " & _
                            ") AS A ON 1 = 1 AND (A.gender = hremployee_master.gender OR A.gender = 0) " & _
                            "WHERE 1 = 1 "

                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= " " & xDateFilterQry & " "
                    End If

                    StrQ &= "EXCEPT " & _
                            "SELECT " & _
                            "     employeeunkid " & _
                            "    ,leavetypeunkid " & _
                            "    ,leavename " & _
                            "FROM " & _
                            "( " & _
                            "    SELECT " & _
                            "         employeeunkid " & _
                            "        ,lvleavetype_master.leavetypeunkid " & _
                            "        ,leavename " & _
                            "    FROM lvleavebalance_tran " & _
                            "        join lvleavetype_master on lvleavebalance_tran.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                            "    WHERE isvoid = 0 AND isopenelc = 1 and iselc = 1 and isactive = 1 AND isaccrueamount = 1 " & _
                            "    UNION " & _
                            "    SELECT " & _
                            "         employeeunkid " & _
                            "        ,lvleavetype_master.leavetypeunkid " & _
                            "        ,leavename " & _
                            "    FROM lvleavebalance_tran " & _
                            "        join lvleavetype_master on lvleavebalance_tran.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                            "    WHERE isvoid = 0 AND iselc = 0 AND isclose_fy = 0 and isactive = 1 AND isaccrueamount = 1 " & _
                            ") AS A " & _
                            "SELECT " & _
                            "     SPACE(5) +employeecode AS ECode " & _
                            "    ,firstname + ' ' + surname + ' >> ' + ISNULL(STUFF((SELECT DISTINCT ' | ' + leavename " & _
                            "            FROM #table " & _
                            "            WHERE #table.employeeunkid = hremployee_master.employeeunkid FOR XML PATH ('')) " & _
                            "            , 1, 2, ''),'') AS EName " & _
                            "   ,'' AS Date " & _
                            "   ,CAST(0 AS BIT) AS IsGrp " & _
                            "   ,12 AS GrpId " & _
                            "   ,hremployee_master.employeeunkid " & _
                            "FROM hremployee_master " & _
                            "    JOIN #table ON #table.employeeunkid = hremployee_master.employeeunkid " & _
                            "GROUP BY SPACE(5)+ employeecode, firstname + ' ' + surname,hremployee_master.employeeunkid " & _
                            "DROP TABLE #table "

                    dsTran = objDataOperation.ExecQuery(StrQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    End If

                    Dim dtRow As DataRow = Nothing

                    dtRow = dsList.Tables(0).NewRow
                    dtRow.Item("ECode") = Language.getMessage(mstrModuleName, 45, "LEAVE TYPE(S) PENDING FOR ACCRUAL") & " [ " & dsTran.Tables(0).Rows.Count & " ]"
                    dtRow.Item("EName") = ""
                    dtRow.Item("Date") = ""
                    dtRow.Item("IsGrp") = True
                    dtRow.Item("GrpId") = 12
                    dsList.Tables(0).Rows.Add(dtRow)

                    dsList.Tables(0).Merge(dsTran.Tables(0), True)
                End If
                'S.SANDEEP [12-Apr-2018] -- END
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "EmployeeDates", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Shani(24-Aug-2015) -- End

    'S.SANDEEP [ 05 MARCH 2012 ] -- END

    'Sohail (25 Apr 2019) -- Start
    'Enhancement - 76.1 - Performance enhancement of aruti main view.
    Public Function GetDatesList(ByVal blnGetAllDates As Boolean, ByVal xCompanyId As Integer) As DataSet          'Pinkal (30-Sep-2023) -- (A1X-1370) Toyota - New desktop dashboard item- Total Birthdays Within 7 Days from Now. [ByVal xCompanyId As Integer]
        Dim StrQ As String = String.Empty
        Dim dsList As DataSet = Nothing

        objDataOperation = New clsDataOperation

        'Pinkal (30-Sep-2023) -- Start 
        '(A1X-1370) Toyota - New desktop dashboard item- Total Birthdays Within 7 Days from Now. 
        Dim mintBirthdayForcastedValue As Integer = 0
        Dim mintWorkAnniversaryForcastedValue As Integer = 0
        Dim mintWorkResidencePermitExpiryForcastedValue As Integer = 0

        Dim objConfig As New clsConfigOptions

        Dim mstrBirthdayForcastedValue As String = objConfig.GetKeyValue(xCompanyId, "ForcastedBirthDateValue", Nothing)
        If mstrBirthdayForcastedValue.Trim.Length > 0 Then mintBirthdayForcastedValue = mstrBirthdayForcastedValue.Trim

        Dim mstrWorkAnniversaryForcastedValue As String = objConfig.GetKeyValue(xCompanyId, "ForcastedWorkAnniversaryValue", Nothing)
        If mstrWorkAnniversaryForcastedValue.Trim.Length > 0 Then mintWorkAnniversaryForcastedValue = mstrWorkAnniversaryForcastedValue.Trim

        Dim mstrWorkResidencePermitExpiryForcastedValue As String = objConfig.GetKeyValue(xCompanyId, "ForcastedWorkResidencePermitExpiryValue", Nothing)
        If mstrWorkResidencePermitExpiryForcastedValue.Trim.Length > 0 Then mintWorkResidencePermitExpiryForcastedValue = mstrWorkResidencePermitExpiryForcastedValue.Trim

        objConfig = Nothing
        'Pinkal (30-Sep-2023) -- End 


        Try
            Dim StrPrivilegeIds As String = ""
            If mblnShow_Probation_Dates Then
                StrPrivilegeIds &= ",1"
            End If
            If mblnShow_Suspension_Dates Then
                StrPrivilegeIds &= ",2"
            End If
            If mblnShow_Appointment_Dates Then
                StrPrivilegeIds &= ",3"
            End If
            If mblnShow_Confirmation_Dates Then
                StrPrivilegeIds &= ",4"
            End If
            If mblnShow_BirthDates Then
                StrPrivilegeIds &= ",5"
            End If
            If mblnShow_Anniversary_Dates Then
                StrPrivilegeIds &= ",6"
            End If
            If mblnShow_Contract_Ending_Dates Then
                StrPrivilegeIds &= ",7"
            End If
            If mblnShow_TodayRetirement_Dates Then
                StrPrivilegeIds &= ",8"
            End If
            If mblnShow_ForcastedRetirement_Dates Then
                StrPrivilegeIds &= ",9"
            End If
            If mblnShow_ForcastedEOC_Dates Then
                StrPrivilegeIds &= ",10"
            End If

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo = True Then
                If mblnShow_ForcastedELC_Dates Then
                    StrPrivilegeIds &= ",11"
                End If
            End If

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo = True Then
                If mblnShow_PendingAccrueLeaves Then
                    StrPrivilegeIds &= ",12"
                End If
            End If

            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1371) Toyota - New desktop dashboard item: Total Work Anniversaries Within 7 Days from Now.
            If mblnShow_WorkAnniversary_Dates Then
                StrPrivilegeIds &= ",13"
            End If

            If mblnShow_WorkResidencePermitExpiry_Dates Then
                StrPrivilegeIds &= ",14"
            End If

            If mblnShow_TotalLeaversFromCurrentFY Then
                StrPrivilegeIds &= ",15"
            End If

            'Pinkal (30-Sep-2023) -- End

            'Hemant (06 Dec 2024) -- Start
            'ENHANCEMENT(TOYOTA): A1X - 2899 :  Dash reminder - Total Payroll Exemptions: Current & Next Month
            If mblnShow_Exemption_Dates Then
                StrPrivilegeIds &= ",16"
            End If
            'Hemant (06 Dec 2024) -- End


            If StrPrivilegeIds.Trim.Length > 0 Then
                StrPrivilegeIds = Mid(StrPrivilegeIds, 2)
            Else
                StrPrivilegeIds = "0"
            End If

            StrQ = "SELECT ID, MODE,SortId FROM " & _
                   "( " & _
                   "    SELECT " & enEmpDates.Probation_Dates & " AS ID,@PROBATION AS MODE,1 AS SortId UNION " & _
                   "    SELECT " & enEmpDates.Suspension_Dates & " AS ID,@SUSPENSION AS MODE,2 AS SortId UNION " & _
                   "    SELECT " & enEmpDates.Appointment_Dates & " AS ID,@APPOINTED AS MODE,3 AS SortId UNION " & _
                   "    SELECT " & enEmpDates.Confirmation_Dates & " AS ID,@CONFIRMED AS MODE,4 AS SortId UNION " & _
                   "    SELECT " & enEmpDates.BirthDates & " AS ID,@BIRTHDAY AS MODE,5 AS SortId UNION " & _
                   "    SELECT " & enEmpDates.Anniversary_Dates & " AS ID,@ANNIVERSARY AS MODE,6 AS SortId UNION " & _
                   "    SELECT " & enEmpDates.Contract_Ending_Dates & " AS ID,@TERMINATING AS MODE,7 AS SortId UNION " & _
                   "    SELECT " & enEmpDates.TodayRetirement_Dates & " AS ID,@RETIRING AS MODE,9 AS SortId UNION " & _
                   "    SELECT " & enEmpDates.ForcastedRetirement_Dates & " AS ID,@FORCASTED_RETIRING AS MODE,10 AS SortId UNION " & _
                   "    SELECT " & enEmpDates.ForcastedEOC_Dates & " AS ID,@FORCASTED_EOC AS MODE,8 AS SortId UNION" & _
                   "    SELECT " & enEmpDates.ForcastedELC_Dates & " AS ID,@FORCASTED_ELC AS MODE,11 AS SortId UNION" & _
                   "    SELECT " & enEmpDates.PendingAccrueLeaves & " AS ID,@PENDINGACCRUELEAVES AS MODE,12 AS SortId UNION " & _
                   "    SELECT " & enEmpDates.ForcastedWorkAnniversary & " AS ID,@FORCASTEDWORKANNIVERSARY AS MODE,13 AS SortId UNION " & _
                   "    SELECT " & enEmpDates.ForcastedWorkResidencePermitExpiry & " AS ID,@FORCASTEDWORKRESIDENCEPERMITEXPIRY AS MODE,14 AS SortId UNION " & _
                   "    SELECT " & enEmpDates.TotalLeaversFromCurrentFY & " AS ID,@TOTALLEAVERSFROMCURRENTFY AS MODE,15 AS SortId " & _
                   "    UNION SELECT " & enEmpDates.Exemption_Dates & " AS ID,@EXEMPTION AS MODE,16 AS SortId " & _
                   ") AS A WHERE 1 = 1 "


            'Hemant (06 Dec 2024) -- [EXEMPTION]
            'Pinkal (30-Sep-2023) -- (A1X-1371) Toyota - New desktop dashboard item: Total Work Anniversaries Within 7 Days from Now.["    SELECT " & enEmpDates.ForcastedWorkAnniversary & " AS ID,@FORCASTEDWORKANNIVERSARY AS MODE,13 AS SortId UNION " & _
            '"    SELECT " & enEmpDates.ForcastedWorkResidencePermitExpiry & " AS ID,@FORCASTEDWORKRESIDENCEPERMITEXPIRY AS MODE,14 AS SortId UNION " & _
            '"    SELECT " & enEmpDates.TotalLeaversFromCurrentFY & " AS ID,@TOTALLEAVERSFROMCURRENTFY AS MODE,15 AS SortId " & _]

            If blnGetAllDates = False Then
                StrQ &= " AND A.ID IN (" & StrPrivilegeIds & ") "
            End If

            StrQ &= " ORDER BY SortId ASC "

            objDataOperation.AddParameter("@PROBATION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 17, "PROBATION WITHIN 30 DAYS FROM NOW."))
            objDataOperation.AddParameter("@SUSPENSION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 18, "SUSPENSION WITHIN 30 DAYS FROM NOW."))
            objDataOperation.AddParameter("@APPOINTED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 19, "TOTAL APPOINTED WITHIN 30 DAYS FROM NOW."))
            objDataOperation.AddParameter("@CONFIRMED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "TOTAL CONFIRMED WITHIN 30 DAYS FROM NOW."))

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1370) Toyota - New desktop dashboard item- Total Birthdays Within 7 Days from Now. 
            'objDataOperation.AddParameter("@BIRTHDAY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "TODAY'S BIRTHDAY."))
            If mintBirthdayForcastedValue > 0 Then
                objDataOperation.AddParameter("@BIRTHDAY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 88, "BIRTHDAYS WITHIN") & " " & mintBirthdayForcastedValue & " " & Language.getMessage(mstrModuleName, 44, " DAY(S) FROM NOW."))
            Else
            objDataOperation.AddParameter("@BIRTHDAY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 21, "TODAY'S BIRTHDAY."))
            End If
            'Pinkal (30-Sep-2023) -- End 

            objDataOperation.AddParameter("@ANNIVERSARY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 22, "TODAY'S ANNIVERSARY."))
            objDataOperation.AddParameter("@TERMINATING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 23, "TOTAL CONTRACTS ENDING WITHIN 30 DAYS FROM NOW."))
            objDataOperation.AddParameter("@RETIRING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 24, "TODAY'S RETIREMENT."))

            Dim StrGrp As String = String.Empty
            Select Case mintForcasted_ViewSetting
                Case clsConfigOptions.enForcastedSetting.BY_MONTH
                    StrGrp = Language.getMessage(mstrModuleName, 25, "TOTAL FORCASTED RETIREMENT WITHIN ") & NumToWord(CDec(mintForcastedValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 26, " MONTH(S) FROM NOW.")
                Case clsConfigOptions.enForcastedSetting.BY_YEAR
                    StrGrp = Language.getMessage(mstrModuleName, 25, "TOTAL FORCASTED RETIREMENT WITHIN ") & NumToWord(CDec(mintForcastedValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 27, " YEAR(S) FROM NOW.")
            End Select
            objDataOperation.AddParameter("@FORCASTED_RETIRING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrGrp)

            Dim StrEOCGrp As String = String.Empty
            Select Case mintForcastedEOC_ViewSetting
                Case clsConfigOptions.enForcastedSetting.BY_MONTH
                    StrEOCGrp = Language.getMessage(mstrModuleName, 28, "TOTAL FORCASTED CONTRACTS ENDING WITHIN ") & NumToWord(CDec(mintForcastedEOCValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 26, " MONTH(S) FROM NOW.")
                Case clsConfigOptions.enForcastedSetting.BY_YEAR
                    StrEOCGrp = Language.getMessage(mstrModuleName, 28, "TOTAL FORCASTED CONTRACTS ENDING WITHIN ") & NumToWord(CDec(mintForcastedEOCValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 27, " YEAR(S) FROM NOW.")
            End Select
            objDataOperation.AddParameter("@FORCASTED_EOC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrEOCGrp)

            Dim StrELCGrp As String = String.Empty
            Select Case mintForcastedELCViewSetting
                Case clsConfigOptions.enForcastedSetting.BY_DAY
                    StrELCGrp = Language.getMessage(mstrModuleName, 43, "TOTAL FORCASTED EMPLOYEE LEAVE CYCLE ENDING WITHIN ") & NumToWord(CDec(mintForcastedELCValue)).ToString.ToUpper & Language.getMessage(mstrModuleName, 44, " DAY(S) FROM NOW.")
            End Select
            objDataOperation.AddParameter("@FORCASTED_ELC", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, StrELCGrp)
            objDataOperation.AddParameter("@PENDINGACCRUELEAVES", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 45, "LEAVE TYPE(S) PENDING FOR ACCRUAL"))

            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1371) Toyota - New desktop dashboard item: Total Work Anniversaries Within 7 Days from Now.
            If mintWorkAnniversaryForcastedValue > 0 Then
                objDataOperation.AddParameter("@FORCASTEDWORKANNIVERSARY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 90, "WORK ANNIVERSARIES WITHIN") & " " & mintWorkAnniversaryForcastedValue & " " & Language.getMessage(mstrModuleName, 44, " DAY(S) FROM NOW."))
            Else
                objDataOperation.AddParameter("@FORCASTEDWORKANNIVERSARY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 91, "TODAY'S WORK ANNIVERSARIES."))
            End If

            If mintWorkResidencePermitExpiryForcastedValue > 0 Then
                objDataOperation.AddParameter("@FORCASTEDWORKRESIDENCEPERMITEXPIRY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 92, "WORK RESIDENCE PERMIT EXPIRY WITHIN") & " " & mintWorkResidencePermitExpiryForcastedValue & " " & Language.getMessage(mstrModuleName, 44, " DAY(S) FROM NOW."))
            Else
                objDataOperation.AddParameter("@FORCASTEDWORKRESIDENCEPERMITEXPIRY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 93, "TODAY'S RESIDENCE PERMIT EXPIRY."))
            End If

            objDataOperation.AddParameter("@TOTALLEAVERSFROMCURRENTFY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 94, "TOTAL LEAVERS AS ON DATE FROM CURRENT F.Y"))

            'Pinkal (30-Sep-2023) -- End

            'Hemant (06 Dec 2024) -- Start
            'ENHANCEMENT(TOYOTA): A1X - 2899 :  Dash reminder - Total Payroll Exemptions: Current & Next Month
            objDataOperation.AddParameter("@EXEMPTION", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 98, "TOTAL PAYROLL EXEMPTIONS: CURRENT AND NEXT MONTH"))
            'Hemant (06 Dec 2024) -- End


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetDatesList; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    Public Function EmployeeDatesCount(ByVal xCompanyId As Integer) As DataSet    'Pinkal (30-Sep-2023) -- (A1X-1370) Toyota - New desktop dashboard item- Total Birthdays Within 7 Days from Now. [ByVal xCompanyId As Integer]
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Try

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            Dim mstrGroup As String = String.Empty
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            mstrGroup = objGroup._Groupname
            objGroup = Nothing

            Dim mintBirthdayForcastedValue As Integer = 0
            Dim mintWorkAnniversaryForcastedValue As Integer = 0
            Dim mintWorkResidencePermitExpiryForcastedValue As Integer = 0

            Dim objConfig As New clsConfigOptions
            Dim mstrBirthdayForcastedValue As String = objConfig.GetKeyValue(xCompanyId, "ForcastedBirthDateValue", Nothing)
            If mstrBirthdayForcastedValue.Trim.Length > 0 Then mintBirthdayForcastedValue = mstrBirthdayForcastedValue.Trim

            Dim mstrWorkAnniversaryForcastedValue As String = objConfig.GetKeyValue(xCompanyId, "ForcastedWorkAnniversaryValue", Nothing)
            If mstrWorkAnniversaryForcastedValue.Trim.Length > 0 Then mintWorkAnniversaryForcastedValue = mstrWorkAnniversaryForcastedValue.Trim

            Dim mstrWorkResidencePermitExpiryForcastedValue As String = objConfig.GetKeyValue(xCompanyId, "ForcastedWorkResidencePermitExpiryValue", Nothing)
            If mstrWorkResidencePermitExpiryForcastedValue.Trim.Length > 0 Then mintWorkResidencePermitExpiryForcastedValue = mstrWorkResidencePermitExpiryForcastedValue.Trim

            objConfig = Nothing

            Dim mstrQueryColumns As String = ""
            Dim mstrDisplayMainColumns As String = ""
            Dim mstrDisplayInternalColumns As String = ""
            Dim mstrAllocationMstJoin As String = ""


            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then

                mstrQueryColumns = ", ISNULL(SM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 5, "Branch") = "", "Branch", Language.getMessage(mstrModuleName, 5, "Branch")) & "] " & _
                                             ", ISNULL(DGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 75, "Department Group") = "", "Department Group", Language.getMessage(mstrModuleName, 75, "Department Group")) & "] " & _
                                             ", ISNULL(DM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 4, "Department") = "", "Department", Language.getMessage(mstrModuleName, 4, "Department")) & "] " & _
                                             ", ISNULL(SGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 76, "Section Group") = "", "Section Group", Language.getMessage(mstrModuleName, 76, "Section Group")) & "] " & _
                                             ", ISNULL(SECM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 7, "Section") = "", "Section", Language.getMessage(mstrModuleName, 7, "Section")) & "] " & _
                                             ", ISNULL(UGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 77, "Unit Group") = "", "Unit Group", Language.getMessage(mstrModuleName, 77, "Unit Group")) & "] " & _
                                             ", ISNULL(UM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 78, "Unit") = "", "Unit", Language.getMessage(mstrModuleName, 78, "Unit")) & "] " & _
                                             ", ISNULL(TM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 79, "Team") = "", "Team", Language.getMessage(mstrModuleName, 79, "Team")) & "] " & _
                                             ", ISNULL(CGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 80, "Class Group") = "", "Class Group", Language.getMessage(mstrModuleName, 80, "Class Group")) & "] " & _
                                             ", ISNULL(CM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 81, "Class") = "", "Class", Language.getMessage(mstrModuleName, 81, "Class")) & "] " & _
                                             ", ISNULL(JGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 82, "Job Group") = "", "Job Group", Language.getMessage(mstrModuleName, 82, "Job Group")) & "] " & _
                                             ", ISNULL(JM.job_name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 83, "Job") = "", "Job", Language.getMessage(mstrModuleName, 83, "Job")) & "] " & _
                                             ", ISNULL(GGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 84, "Grade Group") = "", "Grade Group", Language.getMessage(mstrModuleName, 84, "Grade Group")) & "] " & _
                                             ", ISNULL(GM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 85, "Grade") = "", "Grade", Language.getMessage(mstrModuleName, 85, "Grade")) & "] " & _
                                             ", ISNULL(GLM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 86, "Grade Group Level") = "", "Grade Group Level", Language.getMessage(mstrModuleName, 86, "Grade Group Level")) & "] " & _
                                             ", ISNULL(CCM.costcentername,'') AS [" & IIf(Language.getMessage(mstrModuleName, 87, "Cost Center") = "", "Cost Center", Language.getMessage(mstrModuleName, 87, "Cost Center")) & "] "



                mstrDisplayMainColumns = ", [" & Language.getMessage(mstrModuleName, 5, "Branch") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 75, "Department Group") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 4, "Department") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 76, "Section Group") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 7, "Section") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 77, "Unit Group") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 78, "Unit") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 79, "Team") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 80, "Class Group") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 81, "Class") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 82, "Job Group") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 83, "Job") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 84, "Grade Group") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 85, "Grade") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 86, "Grade Group Level") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 87, "Cost Center") & "] "



                mstrDisplayInternalColumns = ", #TableEmp. [" & Language.getMessage(mstrModuleName, 5, "Branch") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 75, "Department Group") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 4, "Department") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 76, "Section Group") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 7, "Section") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 77, "Unit Group") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 78, "Unit") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 79, "Team") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 80, "Class Group") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 81, "Class") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 82, "Job Group") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 83, "Job") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 84, "Grade Group") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 85, "Grade") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 86, "Grade Group Level") & "]" & _
                                                            ", #TableEmp. [" & Language.getMessage(mstrModuleName, 87, "Cost Center") & "]"



                mstrAllocationMstJoin = " LEFT JOIN hrstation_master AS SM ON SM.stationunkid = ADF.stationunkid " & _
                                                 " LEFT JOIN hrdepartment_group_master AS DGM ON DGM.deptgroupunkid = ADF.deptgroupunkid  " & _
                                                 " LEFT JOIN hrdepartment_master AS DM ON  DM.departmentunkid = ADF.departmentunkid " & _
                                                 " LEFT JOIN hrsectiongroup_master AS SGM ON SGM.sectiongroupunkid = ADF.sectiongroupunkid " & _
                                                 " LEFT JOIN hrsection_master AS SECM ON SECM.sectionunkid  = ADF.sectionunkid " & _
                                                 " LEFT JOIN hrunitgroup_master AS UGM ON UGM.unitgroupunkid =  ADF.unitgroupunkid " & _
                                                 " LEFT JOIN hrunit_master AS UM ON UM.unitunkid = ADF.unitunkid " & _
                                                 " LEFT JOIN hrteam_master AS TM ON TM.teamunkid = ADF.teamunkid " & _
                                                 " LEFT JOIN hrclassgroup_master AS CGM ON CGM.classgroupunkid = ADF.classgroupunkid " & _
                                                 " LEFT JOIN hrclasses_master AS CM ON CM.classesunkid = ADF.classunkid  " & _
                                                 " LEFT JOIN hrjobgroup_master AS JGM ON JGM.jobgroupunkid = ADF.jobgroupunkid " & _
                                                 " LEFT JOIN hrjob_master AS JM ON JM.jobunkid = ADF.jobunkid " & _
                                                 " LEFT JOIN hrgradegroup_master AS GGM ON GGM.gradegroupunkid = ADF.gradegroupunkid " & _
                                                 " LEFT JOIN hrgrade_master AS GM ON  GM.gradeunkid = ADF.gradeunkid " & _
                                                 " LEFT JOIN hrgradelevel_master AS GLM ON GLM.gradelevelunkid = ADF.gradelevelunkid " & _
                                                 " LEFT JOIN prcostcenter_master AS CCM ON CCM.costcenterunkid = ADF.costcenterunkid "


            End If


            'Pinkal (30-Sep-2023) -- End 

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtPeriodEndDate, mdtPeriodEndDate, , , mstrDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtPeriodEndDate, mblnOnlyApproved, mstrDatabaseName, mintuserUnkid, mintCompanyUnkid, mintYearUnkId, mstrUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtPeriodEndDate, mstrDatabaseName)

            objDataOperation = New clsDataOperation

            StrQ = "SELECT hremployee_master.employeeunkid " & _
                            ", hremployee_master.employeecode " & _
                            ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename "

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrQueryColumns
            End If
            'Pinkal (30-Sep-2023) -- End 

            StrQ &= "INTO #TableEmp " & _
                    "FROM hremployee_master "

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= " " & xAdvanceJoinQry & " "
            End If


            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrAllocationMstJoin
            End If
            'Pinkal (30-Sep-2023) -- End 


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= " " & xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= " " & xUACQry & " "
            End If

            StrQ &= "WHERE 1 = 1 "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= " " & xDateFilterQry & " "
            End If

            StrQ &= "SELECT 1 AS ID " & _
                         "/*, COUNT(PrbDt.employeeunkid) AS TotalCount*/ " & _
                         ", PrbDt.employeeunkid AS EmpId " & _
                         ", PrbDt.employeename AS [Emp Name] " & _
                         ", PrbDt.employeecode AS [Emp Code] " & _
                         ", CONVERT(CHAR(8), PrbDt.date2, 112) AS Date " & _
                         ", PrbDt.date2 AS SortDate " & _
                         ", '' AS EndDate "
            'Hemant (06 Dec 2024) -- [EndDate]

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayMainColumns
            End If
            'Pinkal (30-Sep-2023) -- End

            StrQ &= "FROM " & _
                    "( " & _
                        "SELECT date1 " & _
                             ", date2 " & _
                             ", hremployee_dates_tran.employeeunkid " & _
                             ", #TableEmp.employeecode " & _
                             ", #TableEmp.employeename " & _
                     ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno "

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayInternalColumns
            End If
            'Pinkal (30-Sep-2023) -- End 

            StrQ &= "FROM hremployee_dates_tran " & _
                            "JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = '1' " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' "

            'Sohail (13 Jan 2020) -- Start
            'NMB Issue # : Don't show employee dates counts if user does not have access.
            If mblnShow_Probation_Dates = False Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (13 Jan 2020) -- End

            StrQ &= ") AS PrbDt " & _
                    "WHERE PrbDt.rno = 1 " & _
                          "AND CONVERT(CHAR(8), PrbDt.date2, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) " & _
                    "UNION ALL " & _
                    "SELECT 2 AS ID " & _
                         "/*, COUNT(SuspDt.employeeunkid) AS TotalCount*/ " & _
                         ", SuspDt.employeeunkid AS EmpId " & _
                         ", SuspDt.employeename AS [Emp Name] " & _
                         ", SuspDt.employeecode AS [Emp Code] " & _
                         ", CONVERT(CHAR(8), SuspDt.date2, 112) AS Date " & _
                         ", SuspDt.date2 AS sortdate " & _
                         ", '' AS EndDate "
            'Hemant (06 Dec 2024) -- [EndDate]

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayMainColumns
            End If
            'Pinkal (30-Sep-2023) -- End

            StrQ &= "FROM " & _
                    "( " & _
                        "SELECT date1 " & _
                             ", date2 " & _
                             ", hremployee_dates_tran.employeeunkid " & _
                             ", #TableEmp.employeecode " & _
                             ", #TableEmp.employeename " & _
                             ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno "

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayInternalColumns
            End If
            'Pinkal (30-Sep-2023) -- End 

            StrQ &= "FROM hremployee_dates_tran " & _
                            "JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & " " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' "

            'Sohail (13 Jan 2020) -- Start
            'NMB Issue # : Don't show employee dates counts if user does not have access.
            If mblnShow_Suspension_Dates = False Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (13 Jan 2020) -- End

            StrQ &= ") AS SuspDt " & _
                    "WHERE SuspDt.rno = 1 " & _
                          "AND CONVERT(CHAR(8), SuspDt.date2, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) " & _
                    "UNION ALL " & _
                    "SELECT 3 AS ID " & _
                         "/*, COUNT(hremployee_master.employeeunkid) AS TotalCount*/ " & _
                         ", hremployee_master.employeeunkid AS EmpId " & _
                         ", #TableEmp.employeename AS [Emp Name] " & _
                         ", #TableEmp.employeecode AS [Emp Code] " & _
                         ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS Date " & _
                         ", hremployee_master.appointeddate AS sortdate " & _
                         ", '' AS EndDate "
            'Hemant (06 Dec 2024) -- [EndDate]

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayInternalColumns
            End If
            'Pinkal (30-Sep-2023) -- End 

            StrQ &= "FROM hremployee_master " & _
                        "JOIN #TableEmp ON hremployee_master.employeeunkid = #TableEmp.employeeunkid " & _
                    "WHERE CONVERT(CHAR(8), hremployee_master.appointeddate, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) "

            'Sohail (13 Jan 2020) -- Start
            'NMB Issue # : Don't show employee dates counts if user does not have access.
            If mblnShow_Appointment_Dates = False Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (13 Jan 2020) -- End

            StrQ &= "UNION ALL " & _
                    "SELECT 4 AS ID " & _
                         "/*, COUNT(CnfDt.employeeunkid) AS TotalCount*/ " & _
                         ", CnfDt.employeeunkid AS EmpId " & _
                         ", CnfDt.employeename AS [Emp Name] " & _
                         ", CnfDt.employeecode AS [Emp Code] " & _
                         ", CONVERT(CHAR(8), CnfDt.date1, 112) AS Date " & _
                         ", CnfDt.date1 AS sortdate " & _
                         ", '' AS EndDate "
            'Hemant (06 Dec 2024) -- [EndDate]

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayMainColumns
            End If
            'Pinkal (30-Sep-2023) -- End

            StrQ &= "FROM " & _
                    "( " & _
                        "SELECT date1 " & _
                             ", hremployee_dates_tran.employeeunkid " & _
                             ", #TableEmp.employeecode " & _
                             ", #TableEmp.employeename " & _
                             ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno "

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayInternalColumns
            End If
            'Pinkal (30-Sep-2023) -- End 

            StrQ &= "FROM hremployee_dates_tran " & _
                            "JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_CONFIRMATION & " " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' "

            'Sohail (13 Jan 2020) -- Start
            'NMB Issue # : Don't show employee dates counts if user does not have access.
            If mblnShow_Confirmation_Dates = False Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (13 Jan 2020) -- End

            StrQ &= ") AS CnfDt " & _
                    "WHERE CnfDt.rno = 1 " & _
                          "AND CONVERT(CHAR(8), CnfDt.date1, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) " & _
                    "UNION ALL " & _
                    "SELECT 5 AS ID " & _
                         "/*, COUNT(hremployee_master.employeeunkid) AS TotalCount*/ " & _
                         ", hremployee_master.employeeunkid AS EmpId " & _
                         ", #TableEmp.employeename AS [Emp Name] " & _
                         ", #TableEmp.employeecode AS [Emp Code] " & _
                         ", CONVERT(CHAR(8),hremployee_master.birthdate,112) AS Date " & _
                         ", hremployee_master.birthdate AS sortdate " & _
                         ", '' AS EndDate "
            'Hemant (06 Dec 2024) -- [EndDate]

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayInternalColumns
            End If

            StrQ &= "FROM hremployee_master " & _
                        " JOIN #TableEmp ON hremployee_master.employeeunkid = #TableEmp.employeeunkid "

            If mstrBirthdayForcastedValue.Trim.Length > 0 Then
                StrQ &= " WHERE DATEPART(dd, hremployee_master.birthdate) BETWEEN DATEPART(dd, @Date) AND DATEPART(dd, @Date + " & mstrBirthdayForcastedValue & " )" & _
                             " AND DATEPART(mm, hremployee_master.birthdate) BETWEEN DATEPART(mm, @Date)  AND DATEPART(mm, @Date + " & mstrBirthdayForcastedValue & " )"
            Else
                StrQ &= " WHERE DATEPART(dd, @Date) = DATEPART(dd, hremployee_master.birthdate) " & _
                          "AND DATEPART(mm, @Date) = DATEPART(mm, birthdate) "
            End If
            'Pinkal (30-Sep-2023) -- End 

            

            'Sohail (13 Jan 2020) -- Start
            'NMB Issue # : Don't show employee dates counts if user does not have access.
            If mblnShow_BirthDates = False Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (13 Jan 2020) -- End

            StrQ &= "UNION ALL " & _
                    "SELECT 6 AS ID " & _
                         "/*, COUNT(hremployee_master.employeeunkid) AS TotalCount*/ " & _
                         ", hremployee_master.employeeunkid AS EmpId " & _
                         ", #TableEmp.employeename AS [Emp Name] " & _
                         ", #TableEmp.employeecode AS [Emp Code] " & _
                         ", CONVERT(CHAR(8),hremployee_master.anniversary_date,112) AS Date " & _
                         ", hremployee_master.anniversary_date AS sortdate " & _
                         ", '' AS EndDate "
            'Hemant (06 Dec 2024) -- [EndDate]

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayInternalColumns
            End If
            'Pinkal (30-Sep-2023) -- End 

            StrQ &= "FROM hremployee_master " & _
                        "JOIN #TableEmp ON hremployee_master.employeeunkid = #TableEmp.employeeunkid " & _
                    "WHERE DATEPART(dd, @Date) = DATEPART(dd, hremployee_master.anniversary_date) " & _
                          "AND DATEPART(mm, @Date) = DATEPART(mm, anniversary_date) "

            'Sohail (13 Jan 2020) -- Start
            'NMB Issue # : Don't show employee dates counts if user does not have access.
            If mblnShow_Anniversary_Dates = False Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (13 Jan 2020) -- End

            StrQ &= "UNION ALL " & _
                    "SELECT 7 AS ID " & _
                         "/*, COUNT(Eoc.employeeunkid) AS TotalCount*/ " & _
                         ", Eoc.employeeunkid AS EmpId " & _
                         ", Eoc.employeename AS [Emp Name] " & _
                         ", Eoc.employeecode AS [Emp Code] " & _
                         ", CONVERT(CHAR(8),Eoc.date1,112) AS Date " & _
                         ", Eoc.date1 AS sortdate " & _
                         ", '' AS EndDate "
            'Hemant (06 Dec 2024) -- [EndDate]

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayMainColumns
            End If
            'Pinkal (30-Sep-2023) -- End

            StrQ &= "FROM " & _
                    "( " & _
                        "SELECT date1 " & _
                             ", hremployee_dates_tran.employeeunkid " & _
                             ", #TableEmp.employeecode " & _
                             ", #TableEmp.employeename " & _
                             ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC) AS rno "

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayInternalColumns
            End If
            'Pinkal (30-Sep-2023) -- End 

            StrQ &= "FROM hremployee_dates_tran " & _
                            "JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' "

            'Sohail (13 Jan 2020) -- Start
            'NMB Issue # : Don't show employee dates counts if user does not have access.
            If mblnShow_Contract_Ending_Dates = False Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (13 Jan 2020) -- End

            StrQ &= ") AS Eoc " & _
                    "WHERE Eoc.rno = 1 " & _
                          "AND CONVERT(CHAR(8), Eoc.date1, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) " & _
                    "UNION ALL " & _
                    "SELECT 8 AS ID " & _
                         "/*, COUNT(Retr.employeeunkid) AS TotalCount*/ " & _
                         ", Retr.employeeunkid AS EmpId " & _
                         ", Retr.employeename AS [Emp Name] " & _
                         ", Retr.employeecode AS [Emp Code] " & _
                         ", CONVERT(CHAR(8),Retr.date1,112) AS Date " & _
                         ", Retr.date1 AS sortdate " & _
                         ", '' AS EndDate "
            'Hemant (06 Dec 2024) -- [EndDate]

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayMainColumns
            End If
            'Pinkal (30-Sep-2023) -- End

            StrQ &= "FROM " & _
                    "( " & _
                        "SELECT date1 " & _
                             ", date2 " & _
                             ", hremployee_dates_tran.employeeunkid " & _
                             ", #TableEmp.employeecode " & _
                             ", #TableEmp.employeename " & _
                             ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno "

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayInternalColumns
            End If
            'Pinkal (30-Sep-2023) -- End 

            StrQ &= "FROM hremployee_dates_tran " & _
                            "JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT & " " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' "

            'Sohail (13 Jan 2020) -- Start
            'NMB Issue # : Don't show employee dates counts if user does not have access.
            If mblnShow_TodayRetirement_Dates = False Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (13 Jan 2020) -- End

            StrQ &= ") AS Retr " & _
                    "WHERE Retr.rno = 1 " & _
                          "AND CONVERT(CHAR(8), @Date, 112) = CONVERT(CHAR(8), Retr.date1, 112) " & _
                    "UNION ALL " & _
                    "SELECT 9 AS ID " & _
                         "/*, COUNT(Retr.employeeunkid) AS TotalCount*/ " & _
                         ", Retr.employeeunkid AS EmpId " & _
                         ", Retr.employeename AS [Emp Name] " & _
                         ", Retr.employeecode AS [Emp Code] " & _
                         ", CONVERT(CHAR(8),Retr.date1,112) AS Date " & _
                         ", Retr.date1 AS sortdate " & _
                         ", '' AS EndDate "
            'Hemant (06 Dec 2024) -- [EndDate]

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayMainColumns
            End If
            'Pinkal (30-Sep-2023) -- End


            StrQ &= "FROM " & _
                    "( " & _
                        "SELECT date1 " & _
                             ", date2 " & _
                             ", hremployee_dates_tran.employeeunkid " & _
                             ", #TableEmp.employeecode " & _
                             ", #TableEmp.employeename " & _
                             ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno "


            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayInternalColumns
            End If
            'Pinkal (30-Sep-2023) -- End 

            StrQ &= "FROM hremployee_dates_tran " & _
                            "JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' "

            'Sohail (13 Jan 2020) -- Start
            'NMB Issue # : Don't show employee dates counts if user does not have access.
            If mblnShow_ForcastedRetirement_Dates = False Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (13 Jan 2020) -- End

            StrQ &= ") AS Retr " & _
                    "WHERE Retr.rno = 1 "

            Select Case mintForcasted_ViewSetting
                Case clsConfigOptions.enForcastedSetting.BY_MONTH
                    'Sohail (01 Jan 2021) -- Start
                    'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS.
                    'StrQ &= "	AND Retr.date1 BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(mm," & mintForcastedValue & ", @Date), 112) "
                    StrQ &= "	AND CONVERT(CHAR(8), Retr.date1, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(mm," & mintForcastedValue & ", @Date), 112) "
                    'Sohail (01 Jan 2021) -- End
                Case clsConfigOptions.enForcastedSetting.BY_YEAR
                    'Sohail (01 Jan 2021) -- Start
                    'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS.
                    'StrQ &= "	AND Retr.date1 BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(yy," & mintForcastedValue & ", @Date), 112) "
                    StrQ &= "	AND CONVERT(CHAR(8), Retr.date1, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(yy," & mintForcastedValue & ", @Date), 112) "
                    'Sohail (01 Jan 2021) -- End
            End Select

            StrQ &= "UNION ALL " & _
                    "SELECT 10 AS ID " & _
                         "/*, COUNT(EocDt.employeeunkid) AS TotalCount*/ " & _
                         ", EocDt.employeeunkid AS EmpId " & _
                         ", EocDt.employeename AS [Emp Name] " & _
                         ", EocDt.employeecode AS [Emp Code] " & _
                         ", CONVERT(CHAR(8),EocDt.date1,112) AS Date " & _
                         ", EocDt.date1 AS sortdate " & _
                         ", '' AS EndDate "
            'Hemant (06 Dec 2024) -- [EndDate]

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayMainColumns
            End If
            'Pinkal (30-Sep-2023) -- End

            StrQ &= "FROM " & _
                    "( " & _
                        "SELECT date1 " & _
                             ", date2 " & _
                             ", hremployee_dates_tran.employeeunkid " & _
                             ", #TableEmp.employeecode " & _
                             ", #TableEmp.employeename " & _
                             ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno "

            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayInternalColumns
            End If
            'Pinkal (30-Sep-2023) -- End 

            StrQ &= "FROM hremployee_dates_tran " & _
                            "JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
                              "AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(mdtPeriodEndDate) & "' "

            'Sohail (13 Jan 2020) -- Start
            'NMB Issue # : Don't show employee dates counts if user does not have access.
            If mblnShow_ForcastedEOC_Dates = False Then
                StrQ &= " AND 1 = 2 "
            End If
            'Sohail (13 Jan 2020) -- End

            StrQ &= ") AS EocDt " & _
                    "WHERE EocDt.rno = 1 "

            Select Case mintForcastedEOC_ViewSetting
                Case clsConfigOptions.enForcastedSetting.BY_MONTH
                    'Sohail (01 Jan 2021) -- Start
                    'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS.
                    'StrQ &= "	AND EocDt.date1 BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(mm," & mintForcastedEOCValue & ", @Date), 112) "
                    StrQ &= "	AND CONVERT(CHAR(8), EocDt.date1, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(mm," & mintForcastedEOCValue & ", @Date), 112) "
                    'Sohail (01 Jan 2021) -- End
                Case clsConfigOptions.enForcastedSetting.BY_YEAR
                    'Sohail (01 Jan 2021) -- Start
                    'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS.
                    'StrQ &= "	AND EocDt.date1 BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(yy," & mintForcastedEOCValue & ", @Date), 112) "
                    StrQ &= "	AND CONVERT(CHAR(8), EocDt.date1, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(yy," & mintForcastedEOCValue & ", @Date), 112) "
                    'Sohail (01 Jan 2021) -- End
            End Select



            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1371) Toyota - New desktop dashboard item: Total Work Anniversaries Within 7 Days from Now.

            StrQ &= "UNION ALL " & _
                       "SELECT 13 AS ID " & _
                            "/*, COUNT(hremployee_master.employeeunkid) AS TotalCount*/ " & _
                            ", hremployee_master.employeeunkid AS EmpId " & _
                            ", #TableEmp.employeename AS [Emp Name] " & _
                            ", #TableEmp.employeecode AS [Emp Code] " & _
                            ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS Date " & _
                            ", hremployee_master.appointeddate AS sortdate " & _
                            ", '' AS EndDate "
            'Hemant (06 Dec 2024) -- [EndDate]

            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayInternalColumns
            End If

            StrQ &= " FROM hremployee_master " & _
                         " JOIN #TableEmp ON hremployee_master.employeeunkid = #TableEmp.employeeunkid "

            If mstrWorkAnniversaryForcastedValue.Trim.Length > 0 Then
                StrQ &= " WHERE DATEPART(dd, hremployee_master.appointeddate) BETWEEN DATEPART(dd, @Date) AND DATEPART(dd, @Date + " & mstrWorkAnniversaryForcastedValue & " )" & _
                             " AND DATEPART(mm, hremployee_master.appointeddate) BETWEEN DATEPART(mm, @Date)  AND DATEPART(mm, @Date + " & mstrWorkAnniversaryForcastedValue & " )"
            Else
                StrQ &= " WHERE DATEPART(dd, @Date) = DATEPART(dd, hremployee_master.appointeddate) " & _
                             " AND DATEPART(mm, @Date) = DATEPART(mm, hremployee_master.appointeddate) "
            End If


            StrQ &= "UNION ALL " & _
                    "SELECT 14 AS ID " & _
                         "/*, COUNT(hremployee_master.employeeunkid) AS TotalCount*/ " & _
                         ", hremployee_master.employeeunkid AS EmpId " & _
                         ", #TableEmp.employeename AS [Emp Name] " & _
                         ", #TableEmp.employeecode AS [Emp Code] " & _
                         ", CONVERT(CHAR(8),hremployee_master.work_permit_expiry_date,112) AS Date " & _
                         ", hremployee_master.work_permit_expiry_date AS sortdate " & _
                         ", '' AS EndDate "
            'Hemant (06 Dec 2024) -- [EndDate]

            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= mstrDisplayInternalColumns
            End If

            StrQ &= " FROM hremployee_master " & _
                         " JOIN #TableEmp ON hremployee_master.employeeunkid = #TableEmp.employeeunkid " & _
                         " WHERE hremployee_master.work_permit_expiry_date IS NOT NULL "

            If mstrWorkResidencePermitExpiryForcastedValue.Trim.Length > 0 Then
                StrQ &= " AND CONVERT(CHAR(8), hremployee_master.work_permit_expiry_date,112) BETWEEN CONVERT(CHAR(8), @Date ,112) AND CONVERT(CHAR(8), @Date + " & mstrWorkResidencePermitExpiryForcastedValue & ",112) "
            Else
                StrQ &= " AND CONVERT(CHAR(8), hremployee_master.work_permit_expiry_date,112) = CONVERT(CHAR(8), @Date ,112) "
            End If


            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then

                StrQ &= " UNION ALL " & _
                            " SELECT 15 AS ID " & _
                            ", hremployee_master.employeeunkid AS EmpId " & _
                            ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS [Emp Name] " & _
                            ", hremployee_master.employeecode AS [Emp Code] " & _
                            ", CASE	WHEN ETERM.empl_enddate < ERET.termination_to_date THEN  " & _
                            "           CASE WHEN ETERM.empl_enddate IS NULL THEN ERET.termination_to_date ELSE ETERM.empl_enddate  END " & _
                            "  ELSE " & _
                            "           CASE WHEN ERET.termination_to_date IS NULL THEN ETERM.empl_enddate ELSE ERET.termination_to_date END " & _
                            " END AS Date " & _
                            ", CASE	WHEN ETERM.dtempl_enddate < ERET.dttermination_to_date THEN  " & _
                            "           CASE WHEN ETERM.dtempl_enddate IS NULL THEN ERET.dttermination_to_date ELSE ETERM.dtempl_enddate  END " & _
                            "  ELSE " & _
                            "           CASE WHEN ERET.dttermination_to_date IS NULL THEN ETERM.dtempl_enddate ELSE ERET.dttermination_to_date END " & _
                            " END AS sortdate " & _
                            ", '' AS EndDate "
                'Hemant (06 Dec 2024) -- [EndDate]

                StrQ &= mstrQueryColumns

                StrQ &= " FROM hremployee_master "

                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= " " & xAdvanceJoinQry & " "
                End If

                StrQ &= mstrAllocationMstJoin

                StrQ &= " LEFT JOIN ( " & _
                            "                   SELECT " & _
                            "                        TERM.TEEmpId " & _
                            "                       ,TERM.dtempl_enddate " & _
                            "                       ,TERM.empl_enddate " & _
                            "                       ,TERM.termination_from_date " & _
                            "                       ,TERM.TEfDt " & _
                            "                       ,TERM.isexclude_payroll " & _
                            "                   FROM ( " & _
                            "                                   SELECT " & _
                            "                                            TRM.employeeunkid AS TEEmpId " & _
                            "                                           ,TRM.date1 AS dtempl_enddate " & _
                            "                                           ,CONVERT(CHAR(8), TRM.date1, 112) AS empl_enddate " & _
                            "                                           ,CONVERT(CHAR(8), TRM.date2, 112) AS termination_from_date " & _
                            "                                           ,CONVERT(CHAR(8), TRM.effectivedate, 112) AS TEfDt " & _
                            "                                           ,TRM.isexclude_payroll " & _
                            "                                           ,ROW_NUMBER() OVER (PARTITION BY TRM.employeeunkid ORDER BY TRM.effectivedate DESC) AS Rno " & _
                            "                                    FROM hremployee_dates_tran AS TRM " & _
                            "                                    WHERE isvoid = 0 AND TRM.datetypeunkid =  " & enEmp_Dates_Transaction.DT_TERMINATION & _
                            "                                    AND CONVERT(CHAR(8), TRM.effectivedate, 112) <= CONVERT(NVARCHAR(8), GETDATE(), 112) " & _
                            "                               ) AS TERM  WHERE TERM.Rno = 1 " & _
                            " ) AS ETERM ON ETERM.TEEmpId = hremployee_master.employeeunkid " & _
                            " LEFT JOIN ( " & _
                            "                       SELECT " & _
                            "                            RET.REmpId " & _
                            "                           ,RET.dttermination_to_date " & _
                            "                           ,RET.termination_to_date " & _
                            "                           ,RET.REfDt " & _
                            "                       FROM ( " & _
                            "                                   SELECT " & _
                            "                                           RTD.employeeunkid AS REmpId " & _
                            "                                           ,RTD.date1 AS dttermination_to_date " & _
                            "                                           ,CONVERT(CHAR(8), RTD.date1, 112) AS termination_to_date " & _
                            "                                           ,CONVERT(CHAR(8), RTD.effectivedate, 112) AS REfDt " & _
                            "                                           ,ROW_NUMBER() OVER (PARTITION BY RTD.employeeunkid ORDER BY RTD.effectivedate DESC) AS Rno " & _
                            "                                   FROM hremployee_dates_tran AS RTD " & _
                            "                                   WHERE isvoid = 0 AND RTD.datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT & _
                            "                                   AND CONVERT(CHAR(8), RTD.effectivedate, 112) <= CONVERT(NVARCHAR(8), GETDATE(), 112) " & _
                            "                                  ) AS RET WHERE RET.Rno = 1 " & _
                            " ) AS ERET ON ERET.REmpId = hremployee_master.employeeunkid " & _
                            " LEFT JOIN ( " & _
                            "                       SELECT " & _
                            "                                RH.EmpId " & _
                            "                               ,RH.REHIRE " & _
                            "                       FROM ( " & _
                            "                                      SELECT " & _
                            "                                            employeeunkid AS EmpId " & _
                            "                                           ,reinstatment_date AS REHIRE " & _
                            "                                           ,effectivedate " & _
                            "                                           ,ROW_NUMBER() OVER (PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS xNo " & _
                            "                                       FROM hremployee_rehire_tran " & _
                            "                                       WHERE isvoid = 0 " & _
                            "                                       AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(NVARCHAR(8), GETDATE(), 112) " & _
                            "                                  ) AS RH WHERE RH.xNo = 1 " & _
                            " ) AS HIRE ON HIRE.EmpId = hremployee_master.employeeunkid " & _
                            " WHERE isapproved = 1 AND " & _
                            " CASE WHEN CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= CONVERT(NVARCHAR(8), GETDATE(), 112) AND " & _
                            "           ISNULL(CONVERT(CHAR(8), ETERM.termination_from_date, 112), CONVERT(NVARCHAR(8), GETDATE(), 112)) >= CONVERT(NVARCHAR(8), GETDATE(), 112) AND " & _
                            "           ISNULL(CONVERT(CHAR(8), ERET.termination_to_date, 112), CONVERT(NVARCHAR(8), GETDATE(), 112)) >= CONVERT(NVARCHAR(8), GETDATE(), 112) AND " & _
                            "           ISNULL(CONVERT(CHAR(8), ETERM.empl_enddate, 112), CONVERT(NVARCHAR(8), GETDATE(), 112)) >= CONVERT(NVARCHAR(8), GETDATE(), 112)  AND " & _
                            " ( " & _
                            "           CASE  WHEN CONVERT(CHAR(8), HIRE.REHIRE, 112) IS NULL THEN CONVERT(NVARCHAR(8), GETDATE(), 112) " & _
                            "                       ELSE CONVERT(CHAR(8), HIRE.REHIRE, 112) END) <= CONVERT(NVARCHAR(8), GETDATE(), 112) " & _
                            " THEN 1 ELSE 0 END = 0 "

            End If

            'Pinkal (30-Sep-2023) -- End

            'Hemant (06 Dec 2024) -- Start
            'ENHANCEMENT(TOYOTA): A1X - 2899 :  Dash reminder - Total Payroll Exemptions: Current & Next Month
            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                StrQ &= "UNION ALL " & _
                                   "SELECT 16 AS ID " & _
                                        "/*, COUNT(ExemptDt.employeeunkid) AS TotalCount*/ " & _
                                        ", ExemptDt.employeeunkid AS EmpId " & _
                                        ", ExemptDt.employeename AS [Emp Name] " & _
                                        ", ExemptDt.employeecode AS [Emp Code] " & _
                                        ", CONVERT(CHAR(8), ExemptDt.date1, 112) AS Date " & _
                                        ", ExemptDt.date2 AS sortdate " & _
                                        ", CONVERT(CHAR(8), ExemptDt.date2, 112) AS EndDate "

                If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                    StrQ &= mstrDisplayMainColumns
                End If

                StrQ &= "FROM " & _
                        "( " & _
                            "SELECT date1 " & _
                                 ", date2 " & _
                                 ", hremployee_dates_tran.employeeunkid " & _
                                 ", #TableEmp.employeecode " & _
                                 ", #TableEmp.employeename " & _
                                 ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate ) AS rno "

                If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                    StrQ &= mstrDisplayInternalColumns
                End If

                StrQ &= "FROM hremployee_dates_tran " & _
                                "JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid " & _
                            "WHERE isvoid = 0 " & _
                                  "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_EXEMPTION & " " & _
                                  "AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), DATEADD(mm, 1, @Date), 112) " & _
                                  "AND CONVERT(CHAR(8), date1, 112) >=  CONVERT(CHAR(8), DATEFROMPARTS(YEAR(@Date),MONTH(@Date),1), 112) "


                If mblnShow_Exemption_Dates = False Then
                    StrQ &= " AND 1 = 2 "
                End If

                StrQ &= ") AS ExemptDt " & _
                        "WHERE ExemptDt.rno = 1 " & _
                              "AND CONVERT(CHAR(8), ExemptDt.date2, 112) BETWEEN CONVERT(CHAR(8), DATEFROMPARTS(YEAR(@Date),MONTH(@Date),1), 112) " & _
                              " AND CONVERT(CHAR(8), DATEADD(mm, 1, @Date), 112) "
            End If
            'Hemant (06 Dec 2024) -- End


            'Sohail (13 Jan 2020) -- Start
            'NMB Issue # : Don't show employee dates counts if user does not have access.
            'If (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo) AndAlso User._Object.Privilege._Show_ForcastedELC_Dates Then
            If (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo) AndAlso mblnShow_ForcastedELC_Dates Then
                'Sohail (13 Jan 2020) -- End

                StrQ &= "UNION ALL " & _
                        "SELECT 11 AS ID " & _
                             "/*, COUNT(lvleavebalance_tran.employeeunkid) AS TotalCount*/ " & _
                             ", #TableEmp.employeeunkid AS EmpId " & _
                             ", #TableEmp.employeename AS [Emp Name] " & _
                             ", #TableEmp.employeecode AS [Emp Code] " & _
                             ", CONVERT(CHAR(8),enddate,112) AS Date " & _
                             ", enddate AS sortdate " & _
                             ", '' AS EndDate "
                'Hemant (06 Dec 2024) -- [EndDate]

                'Pinkal (30-Sep-2023) -- Start 
                '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
                If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                    StrQ &= mstrDisplayInternalColumns
                End If
                'Pinkal (30-Sep-2023) -- End 

                StrQ &= "FROM #TableEmp " & _
                            "JOIN lvleavebalance_tran ON lvleavebalance_tran.employeeunkid = #TableEmp.employeeunkid " & _
                                   "AND isshortleave = 0 " & _
                                   "and isvoid = 0 "

                If mintLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
                    StrQ &= " AND isclose_fy = 0 "
                ElseIf mintLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
                    StrQ &= " AND isopenelc = 1 "
                End If

                Select Case mintForcastedELCViewSetting
                    Case clsConfigOptions.enForcastedSetting.BY_DAY
                        'Sohail (01 Jan 2021) -- Start
                        'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS.
                        'StrQ &= "	WHERE enddate BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(dd," & mintForcastedELCValue & ", @Date), 112) "
                        StrQ &= "	WHERE CONVERT(CHAR(8), enddate, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(dd," & mintForcastedELCValue & ", @Date), 112) "
                        'Sohail (01 Jan 2021) -- End
                End Select


            End If

            StrQ &= " DROP TABLE #TableEmp "

            objDataOperation.AddParameter("@Date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtPeriodEndDate.Date)

            'Pinkal (30-Sep-2023) -- Start
            '(A1X-1371) Toyota - New desktop dashboard item: Total Work Anniversaries Within 7 Days from Now.
            objDataOperation.AddParameter("@FYDate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDatabaseStartDate.Date)
            'Pinkal (30-Sep-2023) -- End


            dsList = objDataOperation.ExecQuery(StrQ, "Trans")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: EmployeeDatesCount; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function


    'Pinkal (09-Aug-2021)-- Start
    'NMB New UI Enhancements.

    'Sohail (01 Jan 2021) -- Start
    'NMB New UI Enhancement : - New UI Dashboard for MSS and ESS.
    'Public Shared Function EmployeeDatesCountForSelfService(ByVal strDatabaseName As String _
    '                                                 , ByVal intCompanyUnkid As Integer _
    '                                                 , ByVal intYearUnkId As Integer _
    '                                                 , ByVal intuserUnkid As Integer _
    '                                                 , ByVal dtPeriodStart As Date _
    '                                                 , ByVal dtPeriodEnd As Date _
    '                                                 , ByVal strUserModeSetting As String _
    '                                                 , ByVal blnOnlyApproved As Boolean _
    '                                                 , ByVal blnApplyUserAccessFilter As Boolean _
    '                                                 , ByVal blnIsImgInDataBase As Boolean _
    '                                                 , ByVal xNoImageString As String _
    '                                                 , Optional ByVal strAdvanceFilter As String = "" _
    '                                                 , Optional ByVal strORQueryForUserAccess As String = "" _
    '                                                 , Optional ByVal blnShow_BirthDates As Boolean = False _
    '                                                 , Optional ByVal blnShow_Anniversary_Dates As Boolean = False _
    '                                                 , Optional ByVal blnShow_Appointment_Dates As Boolean = False _
    '                                                 , Optional ByVal blnShow_Probation_Dates As Boolean = False _
    '                                                 , Optional ByVal blnShow_Suspension_Dates As Boolean = False _
    '                                                 , Optional ByVal blnShow_Confirmation_Dates As Boolean = False _
    '                                                 , Optional ByVal blnShow_Contract_Ending_Dates As Boolean = False _
    '                                                 , Optional ByVal blnShow_TodayRetirement_Dates As Boolean = False _
    '                                                 , Optional ByVal blnShow_ForcastedRetirement_Dates As Boolean = False _
    '                                                 , Optional ByVal intForcasted_ViewSetting As Integer = 1 _
    '                                                 , Optional ByVal blnShow_ForcastedEOC_Dates As Boolean = False _
    '                                                 , Optional ByVal intForcastedEOC_ViewSetting As Integer = 1 _
    '                                                 , Optional ByVal blnShow_ForcastedELC_Dates As Boolean = False _
    '                                                 , Optional ByVal intForcastedEOCValue As Integer = 1 _
    '                                                 , Optional ByVal intLeaveBalanceSetting As Integer = 1 _
    '                                                 , Optional ByVal intForcastedELCViewSetting As Integer = 1 _
    '                                                 , Optional ByVal intForcastedELCValue As Integer = 1 _
    '                                                 , Optional ByVal intForcastedValue As Integer = 1 _
    '                                                 ) As DataSet
    '    Dim dsList As New DataSet
    '    Dim StrQ As String = String.Empty
    '    Dim strTableEmpFields As String = ""

    '    Try

    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodEnd, dtPeriodEnd, , , strDatabaseName)
    '        If blnApplyUserAccessFilter = True Then
    '            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intuserUnkid, intCompanyUnkid, intYearUnkId, strUserModeSetting, , , strORQueryForUserAccess)
    '        End If
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

    '        Dim objDataOperation As New clsDataOperation

    '        strTableEmpFields = ", #TableEmp.employeeunkid " & _
    '                            ", #TableEmp.employeename " & _
    '                            ", #TableEmp.employeecode " & _
    '                            ", #TableEmp.deptname " & _
    '                            ", #TableEmp.jobname " & _
    '                            ", #TableEmp.teamname " & _
    '                            ", empimage.photo " & _
    '                            ", #TableEmp.imagename "

    '        '", #TableEmp.departmentunkid " & _
    '        '", #TableEmp.jobunkid " & _
    '        '", #TableEmp.teamunkid " & _

    '        If blnIsImgInDataBase = True Then
    '            StrQ = "IF OBJECT_ID('tempdb..#empimage') IS NOT NULL " & _
    '                    "DROP TABLE #empimage " & _
    '                                        "CREATE TABLE #empimage ( " & _
    '                                            "photo image, " & _
    '                                            "employeeunkid int " & _
    '                                        "); " & _
    '                    " " & _
    '                    "INSERT INTO #empimage " & _
    '                         "SELECT " & _
    '                              "photo " & _
    '                            ",employeeunkid " & _
    '                         "FROM arutiimages..hremployee_images " & _
    '                         "WHERE isvoid = 0 " & _
    '                         "and companyunkid = @companyunkid "
    '        End If

    '        StrQ &= "SELECT hremployee_master.employeeunkid " & _
    '                        ", hremployee_master.employeecode " & _
    '                        ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename " & _
    '                        ", ISNULL(hrdepartment_master.name, '') AS deptname " & _
    '                        ", ISNULL(hrjob_master.job_name, '') AS jobname " & _
    '                        ", ISNULL(hrteam_master.name, '') AS teamname "

    '        '", ADF.departmentunkid " & _
    '        '", ADF.jobunkid " & _
    '        '", ADF.teamunkid " & _

    '        If blnIsImgInDataBase = True Then
    '            StrQ &= ", '' AS imagename "
    '        Else
    '            StrQ &= ", ISNULL(empimage.imagename, '') AS imagename "
    '        End If

    '        StrQ &= "INTO #TableEmp " & _
    '                "FROM hremployee_master "


    '        'Pinkal (09-Aug-2021)-- Start
    '        'NMB New UI Enhancements.

    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= " " & xDateJoinQry & " "
    '        End If

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrQ &= " " & xAdvanceJoinQry & " "
    '        End If

    '        'Pinkal (09-Aug-2021) -- End

    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= " " & xUACQry & " "
    '        End If

    '        StrQ &= "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = ADF.departmentunkid " & _
    '                "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = ADF.jobunkid " & _
    '                "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = ADF.teamunkid "


    '        StrQ &= "WHERE 1 = 1 "

    '        If xDateFilterQry.Trim.Length > 0 Then
    '            StrQ &= " " & xDateFilterQry & " "
    '        End If

    '        If strAdvanceFilter.Trim.Length > 0 Then
    '            StrQ &= " AND " & strAdvanceFilter & " "
    '        End If

    '        'Pinkal (01-Jun-2021)-- Start
    '        'New UI Self Service Enhancement : Working on New UI Dashboard Settings.

    '        If blnShow_BirthDates = True Then
    '            '*** Birth Date
    '            StrQ &= "SELECT 5 AS ID " & _
    '                             strTableEmpFields & _
    '                             ", CONVERT(CHAR(8),hremployee_master.birthdate,112) AS Date " & _
    '                             ", hremployee_master.birthdate AS sortdate " & _
    '                        "FROM hremployee_master " & _
    '                            "JOIN #TableEmp ON hremployee_master.employeeunkid = #TableEmp.employeeunkid "

    '            If blnIsImgInDataBase = True Then
    '                StrQ &= "LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_master.employeeunkid "
    '            Else
    '                StrQ &= "LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "AND empimage.referenceid = 1 "
    '            End If


    '            'StrQ &= " WHERE DATEPART(dd, @Date) = DATEPART(dd, hremployee_master.birthdate) " & _
    '            '                              "AND DATEPART(mm, @Date) = DATEPART(mm, birthdate) "

    '            StrQ &= " WHERE DATEPART(dd, GETDATE()) = DATEPART(dd, hremployee_master.birthdate) " & _
    '                                        "AND DATEPART(mm,  GETDATE()) = DATEPART(mm, birthdate) "

    '            'If blnShow_BirthDates = False Then
    '            '    StrQ &= " AND 1 = 2 "
    '            'End If

    '        End If
    '        'Pinkal (01-Jun-2021) -- End

    '        '*** Anniversary Date
    '        If blnShow_Anniversary_Dates = True Then

    '            'Pinkal (01-Jun-2021)-- Start
    '            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '            'StrQ &= "UNION ALL " & _
    '            'Pinkal (01-Jun-2021) -- End

    '            StrQ &= " SELECT 6 AS ID " & _
    '                         strTableEmpFields & _
    '                         ", CONVERT(CHAR(8),hremployee_master.anniversary_date,112) AS Date " & _
    '                         ", hremployee_master.anniversary_date AS sortdate " & _
    '                         " FROM hremployee_master " & _
    '                         " JOIN #TableEmp ON hremployee_master.employeeunkid = #TableEmp.employeeunkid "

    '            If blnIsImgInDataBase = True Then
    '                StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_master.employeeunkid "
    '            Else
    '                StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_master.employeeunkid " & _
    '                             " AND empimage.referenceid = 1 "
    '            End If


    '            'Pinkal (01-Jun-2021)-- Start
    '            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '            'StrQ &= "WHERE DATEPART(dd, @Date) = DATEPART(dd, hremployee_master.anniversary_date) " & _
    '            '                   "AND DATEPART(mm, @Date) = DATEPART(mm, anniversary_date) "
    '            StrQ &= "WHERE DATEPART(dd, GETDATE()) = DATEPART(dd, hremployee_master.anniversary_date) " & _
    '                             "AND DATEPART(mm, GETDATE()) = DATEPART(mm, anniversary_date) "
    '            'Pinkal (01-Jun-2021) -- End`


    '        End If

    '        If blnShow_Appointment_Dates = True Then

    '            'Pinkal (01-Jun-2021)-- Start
    '            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '            'StrQ &= "UNION ALL " & _
    '            'Pinkal (01-Jun-2021) -- End

    '            StrQ &= " SELECT 3 AS ID " & _
    '                             strTableEmpFields & _
    '                             ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS Date " & _
    '                             ", hremployee_master.appointeddate AS sortdate " & _
    '                         " FROM hremployee_master " & _
    '                         " JOIN #TableEmp ON hremployee_master.employeeunkid = #TableEmp.employeeunkid "

    '            If blnIsImgInDataBase = True Then
    '                StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_master.employeeunkid "
    '            Else
    '                StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_master.employeeunkid " & _
    '                             " AND empimage.referenceid = 1 "
    '            End If

    '            StrQ &= " WHERE CONVERT(CHAR(8), hremployee_master.appointeddate, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) "

    '        End If

    '        If blnShow_Probation_Dates = True Then

    '            'Pinkal (01-Jun-2021)-- Start
    '            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '            'StrQ &= "UNION ALL " & _
    '            'Pinkal (01-Jun-2021) -- End

    '            StrQ &= " SELECT 1 AS ID " & _
    '                         strTableEmpFields.Replace("#TableEmp.", "PrbDt.") & _
    '                         ", CONVERT(CHAR(8), PrbDt.date2, 112) AS Date " & _
    '                         ", PrbDt.date2 AS SortDate " & _
    '                         " FROM " & _
    '                         " ( " & _
    '                         "       SELECT date1 " & _
    '                         "      , date2 " & _
    '                             strTableEmpFields & _
    '                             ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
    '                          " FROM hremployee_dates_tran " & _
    '                          " JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

    '            If blnIsImgInDataBase = True Then
    '                StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
    '            Else
    '                StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
    '                            " AND empimage.referenceid = 1 "
    '            End If


    '            StrQ &= " WHERE isvoid = 0 " & _
    '                         " AND datetypeunkid = '1' " & _
    '                         " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
    '                        " ) AS PrbDt " & _
    '                        " WHERE PrbDt.rno = 1 " & _
    '                        " AND CONVERT(CHAR(8), PrbDt.date2, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) "

    '        End If

    '        If blnShow_Suspension_Dates = True Then

    '            'Pinkal (01-Jun-2021)-- Start
    '            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '            'StrQ &= "UNION ALL " & _
    '            'Pinkal (01-Jun-2021) -- End


    '            StrQ &= " SELECT 2 AS ID " & _
    '                         strTableEmpFields.Replace("#TableEmp.", "SuspDt.") & _
    '                         ", CONVERT(CHAR(8), SuspDt.date2, 112) AS Date " & _
    '                         ", SuspDt.date2 AS sortdate " & _
    '                         " FROM " & _
    '                         " ( " & _
    '                         " SELECT date1 " & _
    '                             ", date2 " & _
    '                             strTableEmpFields & _
    '                             ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
    '                         " FROM hremployee_dates_tran " & _
    '                         " JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

    '            If blnIsImgInDataBase = True Then
    '                StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
    '            Else
    '                StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
    '                             " AND empimage.referenceid = 1 "
    '            End If

    '            StrQ &= " WHERE isvoid = 0 " & _
    '                         " AND datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & " " & _
    '                         " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
    '                         " ) AS SuspDt " & _
    '                         " WHERE SuspDt.rno = 1 " & _
    '                          "AND CONVERT(CHAR(8), SuspDt.date2, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) "
    '        End If

    '        If blnShow_Confirmation_Dates = True Then

    '            'Pinkal (01-Jun-2021)-- Start
    '            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '            'StrQ &= "UNION ALL " & _
    '            'Pinkal (01-Jun-2021) -- End

    '            StrQ &= " SELECT 4 AS ID " & _
    '                         strTableEmpFields.Replace("#TableEmp.", "CnfDt.") & _
    '                         ", CONVERT(CHAR(8), CnfDt.date1, 112) AS Date " & _
    '                         ", CnfDt.date1 AS sortdate " & _
    '                         " FROM " & _
    '                         " ( " & _
    '                         "    SELECT date1 " & _
    '                             strTableEmpFields & _
    '                             ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
    '                         " FROM hremployee_dates_tran " & _
    '                         " JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

    '            If blnIsImgInDataBase = True Then
    '                StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
    '            Else
    '                StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
    '                             " AND empimage.referenceid = 1 "
    '            End If


    '            StrQ &= " WHERE isvoid = 0 " & _
    '                         " AND datetypeunkid = " & enEmp_Dates_Transaction.DT_CONFIRMATION & " " & _
    '                         " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
    '                         " ) AS CnfDt " & _
    '                         " WHERE CnfDt.rno = 1 " & _
    '                         " AND CONVERT(CHAR(8), CnfDt.date1, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) "
    '        End If

    '        If blnShow_Contract_Ending_Dates = True Then


    '            'Pinkal (01-Jun-2021)-- Start
    '            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '            'StrQ &= "UNION ALL " & _
    '            'Pinkal (01-Jun-2021) -- End

    '            StrQ &= "SELECT 7 AS ID " & _
    '                         strTableEmpFields.Replace("#TableEmp.", "Eoc.") & _
    '                         ", CONVERT(CHAR(8),Eoc.date1,112) AS Date " & _
    '                         ", Eoc.date1 AS sortdate " & _
    '                         " FROM " & _
    '                         " ( " & _
    '                         "      SELECT date1 " & _
    '                             strTableEmpFields & _
    '                         "  , ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC) AS rno " & _
    '                         " FROM hremployee_dates_tran " & _
    '                         "  JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

    '            If blnIsImgInDataBase = True Then
    '                StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
    '            Else
    '                StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
    '                             " AND empimage.referenceid = 1 "
    '            End If

    '            StrQ &= " WHERE isvoid = 0 " & _
    '                         " AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
    '                         " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
    '                         " ) AS Eoc " & _
    '                         " WHERE Eoc.rno = 1 " & _
    '                         " AND CONVERT(CHAR(8), Eoc.date1, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) "

    '        End If

    '        If blnShow_TodayRetirement_Dates = True Then

    '            'Pinkal (01-Jun-2021)-- Start
    '            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '            'StrQ &= "UNION ALL " & _
    '            'Pinkal (01-Jun-2021) -- End

    '            StrQ &= " SELECT 8 AS ID " & _
    '                         strTableEmpFields.Replace("#TableEmp.", "Retr.") & _
    '                         ", CONVERT(CHAR(8),Retr.date1,112) AS Date " & _
    '                         ", Retr.date1 AS sortdate " & _
    '                         " FROM " & _
    '                         " ( " & _
    '                         "      SELECT date1 " & _
    '                         "      , date2 " & _
    '                             strTableEmpFields & _
    '                         "      , ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
    '                         "      FROM hremployee_dates_tran " & _
    '                         "      JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

    '            If blnIsImgInDataBase = True Then
    '                StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
    '            Else
    '                StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
    '                             " AND empimage.referenceid = 1 "
    '            End If

    '            StrQ &= " WHERE isvoid = 0 " & _
    '                         " AND datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT & " " & _
    '                         " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
    '                         " ) AS Retr " & _
    '                         " WHERE Retr.rno = 1 " & _
    '                         " AND CONVERT(CHAR(8), @Date, 112) = CONVERT(CHAR(8), Retr.date1, 112) "

    '        End If

    '        If blnShow_ForcastedRetirement_Dates = True Then

    '            'Pinkal (01-Jun-2021)-- Start
    '            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '            'StrQ &= "UNION ALL " & _
    '            'Pinkal (01-Jun-2021) -- End


    '            StrQ &= " SELECT 9 AS ID " & _
    '                         strTableEmpFields.Replace("#TableEmp.", "Retr.") & _
    '                         ", CONVERT(CHAR(8),Retr.date1,112) AS Date " & _
    '                         ", Retr.date1 AS sortdate " & _
    '                         " FROM " & _
    '                         " ( " & _
    '                         "  SELECT date1 " & _
    '                         "   , date2 " & _
    '                             strTableEmpFields & _
    '                             ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
    '                         "  FROM hremployee_dates_tran " & _
    '                         "  JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

    '            If blnIsImgInDataBase = True Then
    '                StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
    '            Else
    '                StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
    '                             " AND empimage.referenceid = 1 "
    '            End If


    '            StrQ &= " WHERE isvoid = 0 " & _
    '                         " AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' " & _
    '                         " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
    '                         " ) AS Retr " & _
    '                         " WHERE Retr.rno = 1 "

    '            Select Case intForcasted_ViewSetting
    '                Case clsConfigOptions.enForcastedSetting.BY_MONTH
    '                    StrQ &= "	AND CONVERT(CHAR(8), Retr.date1, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(mm," & intForcastedValue & ", @Date), 112) "
    '                Case clsConfigOptions.enForcastedSetting.BY_YEAR
    '                    StrQ &= "	AND CONVERT(CHAR(8), Retr.date1, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(yy," & intForcastedValue & ", @Date), 112) "
    '            End Select

    '        End If

    '        If blnShow_ForcastedEOC_Dates = True Then

    '            'Pinkal (01-Jun-2021)-- Start
    '            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '            'StrQ &= "UNION ALL " & _
    '            'Pinkal (01-Jun-2021) -- End


    '            StrQ &= " SELECT 10 AS ID " & _
    '                         strTableEmpFields.Replace("#TableEmp.", "EocDt.") & _
    '                         ", CONVERT(CHAR(8),EocDt.date1,112) AS Date " & _
    '                         ", EocDt.date1 AS sortdate " & _
    '                         " FROM " & _
    '                         " ( " & _
    '                         "  SELECT date1 " & _
    '                             ", date2 " & _
    '                             strTableEmpFields & _
    '                             ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
    '                         " FROM hremployee_dates_tran " & _
    '                         " JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

    '            If blnIsImgInDataBase = True Then
    '                StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
    '            Else
    '                StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
    '                             " AND empimage.referenceid = 1 "
    '            End If

    '            StrQ &= " WHERE isvoid = 0 " & _
    '                         " AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
    '                         " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
    '                         " ) AS EocDt " & _
    '                         " WHERE EocDt.rno = 1 "

    '            Select Case intForcastedEOC_ViewSetting
    '                Case clsConfigOptions.enForcastedSetting.BY_MONTH
    '                    StrQ &= "	AND CONVERT(CHAR(8), EocDt.date1, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(mm," & intForcastedEOCValue & ", @Date), 112) "
    '                Case clsConfigOptions.enForcastedSetting.BY_YEAR
    '                    StrQ &= "	AND CONVERT(CHAR(8), EocDt.date1, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(yy," & intForcastedEOCValue & ", @Date), 112) "
    '            End Select

    '        End If



    '        If (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo) AndAlso blnShow_ForcastedELC_Dates = True Then

    '            'Pinkal (01-Jun-2021)-- Start
    '            'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '            'StrQ &= "UNION ALL " & _
    '            'Pinkal (01-Jun-2021) -- End

    '            StrQ &= " SELECT 11 AS ID " & _
    '                         strTableEmpFields & _
    '                         ", CONVERT(CHAR(8),enddate,112) AS Date " & _
    '                         ", enddate AS sortdate " & _
    '                         " FROM #TableEmp " & _
    '                         " JOIN lvleavebalance_tran ON lvleavebalance_tran.employeeunkid = #TableEmp.employeeunkid " & _
    '                         " AND isshortleave = 0 " & _
    '                         " AND isvoid = 0 "

    '            If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
    '                StrQ &= " AND isclose_fy = 0 "
    '            ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
    '                StrQ &= " AND isopenelc = 1 "
    '            End If

    '            Select Case intForcastedELCViewSetting
    '                Case clsConfigOptions.enForcastedSetting.BY_DAY
    '                    StrQ &= "	WHERE CONVERT(CHAR(8), enddate, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(dd," & intForcastedELCValue & ", @Date), 112) "
    '            End Select

    '        End If

    '        StrQ &= " DROP TABLE #TableEmp "

    '        objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
    '        If blnIsImgInDataBase = True Then
    '            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkid)
    '        End If

    '        dsList = objDataOperation.ExecQuery(StrQ, "Trans")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If

    '        If blnIsImgInDataBase = True Then
    '            dsList.Tables(0).AsEnumerable().Cast(Of DataRow).ToList().ForEach(Function(x) convertImageToBase64(x, xNoImageString))
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: EmployeeDatesCountForSelfService; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function

    Public Shared Function EmployeeDatesCountForSelfService(ByVal strDatabaseName As String _
                                                     , ByVal intCompanyUnkid As Integer _
                                                     , ByVal intYearUnkId As Integer _
                                                     , ByVal intuserUnkid As Integer _
                                                     , ByVal dtPeriodStart As Date _
                                                     , ByVal dtPeriodEnd As Date _
                                                     , ByVal strUserModeSetting As String _
                                                     , ByVal blnOnlyApproved As Boolean _
                                                     , ByVal blnApplyUserAccessFilter As Boolean _
                                                     , ByVal blnIsImgInDataBase As Boolean _
                                                     , ByVal xNoImageString As String _
                                                     , Optional ByVal strAdvanceFilter As String = "" _
                                                     , Optional ByVal strORQueryForUserAccess As String = "" _
                                                     , Optional ByVal blnShow_BirthDates As Boolean = False _
                                                     , Optional ByVal blnShow_Anniversary_Dates As Boolean = False _
                                                     , Optional ByVal blnShow_Appointment_Dates As Boolean = False _
                                                     , Optional ByVal blnShow_Probation_Dates As Boolean = False _
                                                     , Optional ByVal blnShow_Suspension_Dates As Boolean = False _
                                                     , Optional ByVal blnShow_Confirmation_Dates As Boolean = False _
                                                     , Optional ByVal blnShow_Contract_Ending_Dates As Boolean = False _
                                                     , Optional ByVal blnShow_TodayRetirement_Dates As Boolean = False _
                                                     , Optional ByVal blnShow_ForcastedRetirement_Dates As Boolean = False _
                                                     , Optional ByVal intForcasted_ViewSetting As Integer = 1 _
                                                     , Optional ByVal blnShow_ForcastedEOC_Dates As Boolean = False _
                                                     , Optional ByVal intForcastedEOC_ViewSetting As Integer = 1 _
                                                     , Optional ByVal blnShow_ForcastedELC_Dates As Boolean = False _
                                                     , Optional ByVal intForcastedEOCValue As Integer = 1 _
                                                     , Optional ByVal intLeaveBalanceSetting As Integer = 1 _
                                                     , Optional ByVal intForcastedELCViewSetting As Integer = 1 _
                                                     , Optional ByVal intForcastedELCValue As Integer = 1 _
                                                     , Optional ByVal intForcastedValue As Integer = 1 _
                                                     ) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim strTableEmpFields As String = ""
        Dim strTableCondition As String = ""
        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodEnd, dtPeriodEnd, , , strDatabaseName)
            If blnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intuserUnkid, intCompanyUnkid, intYearUnkId, strUserModeSetting, , , strORQueryForUserAccess)
            End If
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim objDataOperation As New clsDataOperation


            StrQ &= "SELECT hremployee_master.employeeunkid " & _
                         ", hremployee_master.employeecode " & _
                         ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename " & _
                         ", ISNULL(eDisp.deptname, '') AS deptname " & _
                         ", ISNULL(eDisp.teamname, '') AS teamname " & _
                         ", ISNULL(eJob.jobname, '') AS jobname "

            If blnIsImgInDataBase = True Then
                StrQ &= ",empimage.photo, '' AS imagename "
            Else
                StrQ &= ",NULL AS photo, ISNULL(empimage.imagename, '') AS imagename "
            End If

            StrQ &= ", #TableFields "

            StrQ &= " FROM hremployee_master "


            StrQ &= " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "              ETT.employeeunkid AS TrfEmpId " & _
                          "             ,ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                          "             ,ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                          "             ,ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                          "             ,ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                          "             ,ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                          "             ,ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                          "             ,ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                          "             ,ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                          "             ,ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                          "             ,ISNULL(ETT.classunkid, 0) AS classunkid " & _
                          "             ,CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
                          "             ,ETT.employeeunkid " & _
                          "             ,ISNULL(hrdepartment_master.name, '') AS deptname " & _
                          "             ,ISNULL(hrteam_master.name, '') AS teamname " & _
                          "             ,ROW_NUMBER() OVER (PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                          "         FROM hremployee_transfer_tran AS ETT " & _
                          "         LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = ETT.departmentunkid " & _
                          "         LEFT JOIN hrteam_master ON hrteam_master.teamunkid = ETT.teamunkid " & _
                          " WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), @Date, 112)  " & _
                          " ) AS eDisp ON eDisp.employeeunkid = hremployee_master.employeeunkid AND eDisp.Rno = 1 " & _
                          " LEFT JOIN " & _
                          " ( " & _
                          "         SELECT " & _
                          "             ECT.employeeunkid AS CatEmpId " & _
                          "            ,ECT.jobgroupunkid " & _
                          "            ,ECT.jobunkid " & _
                          "            ,CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
                          "            ,ECT.employeeunkid " & _
                          "            ,ISNULL(hrjob_master.job_name, '') AS jobname " & _
                          "            ,ROW_NUMBER() OVER (PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                          "         FROM hremployee_categorization_tran AS ECT " & _
                          "         LEFT JOIN hrjob_master ON hrjob_master.jobunkid = ECT.jobunkid " & _
                          "         WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), @Date, 112) " & _
                          ") AS eJob on eJob.employeeunkid = hremployee_master.employeeunkid AND eJob.Rno = 1 "



            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= " " & xDateJoinQry & " "
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= " " & xAdvanceJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= " " & xUACQry & " "
            End If


            If blnIsImgInDataBase = True Then

                StrQ &= " LEFT JOIN " & _
                             " ( " & _
                             "      SELECT " & _
                             "              photo " & _
                             "             ,employeeunkid " & _
                             "      FROM arutiimages..hremployee_images " & _
                             "      WHERE isvoid = 0 AND companyunkid = @companyunkid " & _
                             ") AS empimage ON empimage.employeeunkid =  hremployee_master.employeeunkid "
            Else
                StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_master.employeeunkid AND empimage.referenceid = 1 "
            End If


            StrQ &= "WHERE 1 = 1 "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= " " & xDateFilterQry & " "
            End If

            If strAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & strAdvanceFilter & " "
            End If

            StrQ &= "#TableCondition "


            If blnShow_BirthDates = True Then
            '*** Birth Date
                strTableEmpFields = "5 AS ID , CONVERT(CHAR(8),hremployee_master.birthdate,112) AS Date " & _
                                               ", hremployee_master.birthdate AS sortdate "


                strTableCondition = " AND  DATEPART(dd, GETDATE()) = DATEPART(dd, hremployee_master.birthdate) " & _
                                        "AND DATEPART(mm,  GETDATE()) = DATEPART(mm, birthdate) "

            End If

            '*** Anniversary Date
            If blnShow_Anniversary_Dates = True Then
                'Hemant (17 Jan 2022) -- Start
                'ISSUE#OLD-388 : New Ui -Dashboard item (Today's Work Anniversary) shows marriage anniversary.
                'strTableEmpFields = "6 AS ID " & _
                '             ", CONVERT(CHAR(8),hremployee_master.anniversary_date,112) AS Date " & _
                '                               ", hremployee_master.anniversary_date AS sortdate "

                'strTableCondition = "AND DATEPART(dd, GETDATE()) = DATEPART(dd, hremployee_master.anniversary_date) AND DATEPART(mm, GETDATE()) = DATEPART(mm, anniversary_date) "
                strTableEmpFields = "6 AS ID " & _
                            ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS Date " & _
                                              ", hremployee_master.appointeddate AS sortdate "

                strTableCondition = "AND DATEPART(dd, GETDATE()) = DATEPART(dd, hremployee_master.appointeddate) AND DATEPART(mm, GETDATE()) = DATEPART(mm, appointeddate) "
                'Hemant (17 Jan 2022) -- End
               
            End If

            If blnShow_Appointment_Dates = True Then

                strTableEmpFields = " 3 AS ID " & _
                                 ", CONVERT(CHAR(8),hremployee_master.appointeddate,112) AS Date " & _
                                              ", hremployee_master.appointeddate AS sortdate "


                strTableCondition = " AND CONVERT(CHAR(8), hremployee_master.appointeddate, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) "

            End If

            'If blnShow_Probation_Dates = True Then

            '    StrQ &= " SELECT 1 AS ID " & _
            '                 strTableEmpFields.Replace("#TableEmp.", "PrbDt.") & _
            '                 ", CONVERT(CHAR(8), PrbDt.date2, 112) AS Date " & _
            '                 ", PrbDt.date2 AS SortDate " & _
            '                 " FROM " & _
            '                 " ( " & _
            '                 "       SELECT date1 " & _
            '                 "      , date2 " & _
            '                     strTableEmpFields & _
            '                     ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
            '                  " FROM hremployee_dates_tran " & _
            '                  " JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

            '    If blnIsImgInDataBase = True Then
            '        StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
            '    Else
            '        StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
            '                    " AND empimage.referenceid = 1 "
            '    End If


            '    StrQ &= " WHERE isvoid = 0 " & _
            '                 " AND datetypeunkid = '1' " & _
            '                 " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '                " ) AS PrbDt " & _
            '                " WHERE PrbDt.rno = 1 " & _
            '                " AND CONVERT(CHAR(8), PrbDt.date2, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) "

            'End If

            'If blnShow_Suspension_Dates = True Then
            '    StrQ &= " SELECT 2 AS ID " & _
            '                 strTableEmpFields.Replace("#TableEmp.", "SuspDt.") & _
            '                 ", CONVERT(CHAR(8), SuspDt.date2, 112) AS Date " & _
            '                 ", SuspDt.date2 AS sortdate " & _
            '                 " FROM " & _
            '                 " ( " & _
            '                 " SELECT date1 " & _
            '                     ", date2 " & _
            '                     strTableEmpFields & _
            '                     ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
            '                 " FROM hremployee_dates_tran " & _
            '                 " JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

            '    If blnIsImgInDataBase = True Then
            '        StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
            '    Else
            '        StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
            '                     " AND empimage.referenceid = 1 "
            '    End If

            '    StrQ &= " WHERE isvoid = 0 " & _
            '                 " AND datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & " " & _
            '                 " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '                 " ) AS SuspDt " & _
            '                 " WHERE SuspDt.rno = 1 " & _
            '                  "AND CONVERT(CHAR(8), SuspDt.date2, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) "
            'End If

            'If blnShow_Confirmation_Dates = True Then
            '    StrQ &= " SELECT 4 AS ID " & _
            '                 strTableEmpFields.Replace("#TableEmp.", "CnfDt.") & _
            '                 ", CONVERT(CHAR(8), CnfDt.date1, 112) AS Date " & _
            '                 ", CnfDt.date1 AS sortdate " & _
            '                 " FROM " & _
            '                 " ( " & _
            '                 "    SELECT date1 " & _
            '                     strTableEmpFields & _
            '                     ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
            '                 " FROM hremployee_dates_tran " & _
            '                 " JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

            '    If blnIsImgInDataBase = True Then
            '        StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
            '    Else
            '        StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
            '                     " AND empimage.referenceid = 1 "
            '    End If


            '    StrQ &= " WHERE isvoid = 0 " & _
            '                 " AND datetypeunkid = " & enEmp_Dates_Transaction.DT_CONFIRMATION & " " & _
            '                 " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '                 " ) AS CnfDt " & _
            '                 " WHERE CnfDt.rno = 1 " & _
            '                 " AND CONVERT(CHAR(8), CnfDt.date1, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) "
            'End If

            'If blnShow_Contract_Ending_Dates = True Then
            '    StrQ &= "SELECT 7 AS ID " & _
            '                 strTableEmpFields.Replace("#TableEmp.", "Eoc.") & _
            '                 ", CONVERT(CHAR(8),Eoc.date1,112) AS Date " & _
            '                 ", Eoc.date1 AS sortdate " & _
            '                 " FROM " & _
            '                 " ( " & _
            '                 "      SELECT date1 " & _
            '                     strTableEmpFields & _
            '                 "  , ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC) AS rno " & _
            '                 " FROM hremployee_dates_tran " & _
            '                 "  JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

            '    If blnIsImgInDataBase = True Then
            '        StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
            '    Else
            '        StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
            '                     " AND empimage.referenceid = 1 "
            '    End If

            '    StrQ &= " WHERE isvoid = 0 " & _
            '                 " AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
            '                 " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '                 " ) AS Eoc " & _
            '                 " WHERE Eoc.rno = 1 " & _
            '                 " AND CONVERT(CHAR(8), Eoc.date1, 112) BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(dd, 30, @Date), 112) "

            'End If

            'If blnShow_TodayRetirement_Dates = True Then

            '    StrQ &= " SELECT 8 AS ID " & _
            '                 strTableEmpFields.Replace("#TableEmp.", "Retr.") & _
            '                 ", CONVERT(CHAR(8),Retr.date1,112) AS Date " & _
            '                 ", Retr.date1 AS sortdate " & _
            '                 " FROM " & _
            '                 " ( " & _
            '                 "      SELECT date1 " & _
            '                 "      , date2 " & _
            '                     strTableEmpFields & _
            '                 "      , ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
            '                 "      FROM hremployee_dates_tran " & _
            '                 "      JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

            '    If blnIsImgInDataBase = True Then
            '        StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
            '    Else
            '        StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
            '                     " AND empimage.referenceid = 1 "
            '    End If

            '    StrQ &= " WHERE isvoid = 0 " & _
            '                 " AND datetypeunkid = " & enEmp_Dates_Transaction.DT_RETIREMENT & " " & _
            '                 " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '                 " ) AS Retr " & _
            '                 " WHERE Retr.rno = 1 " & _
            '                 " AND CONVERT(CHAR(8), @Date, 112) = CONVERT(CHAR(8), Retr.date1, 112) "

            'End If

            'If blnShow_ForcastedRetirement_Dates = True Then

            '    StrQ &= " SELECT 9 AS ID " & _
            '                 strTableEmpFields.Replace("#TableEmp.", "Retr.") & _
            '                 ", CONVERT(CHAR(8),Retr.date1,112) AS Date " & _
            '                 ", Retr.date1 AS sortdate " & _
            '                 " FROM " & _
            '                 " ( " & _
            '                 "  SELECT date1 " & _
            '                 "   , date2 " & _
            '                     strTableEmpFields & _
            '                     ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
            '                 "  FROM hremployee_dates_tran " & _
            '                 "  JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

            '    If blnIsImgInDataBase = True Then
            '        StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
            '    Else
            '        StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
            '                     " AND empimage.referenceid = 1 "
            '    End If


            '    StrQ &= " WHERE isvoid = 0 " & _
            '                 " AND datetypeunkid = '" & enEmp_Dates_Transaction.DT_RETIREMENT & "' " & _
            '                 " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '                 " ) AS Retr " & _
            '                 " WHERE Retr.rno = 1 "

            '    Select Case intForcasted_ViewSetting
            '        Case clsConfigOptions.enForcastedSetting.BY_MONTH
            '            StrQ &= "	AND CONVERT(CHAR(8), Retr.date1, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(mm," & intForcastedValue & ", @Date), 112) "
            '        Case clsConfigOptions.enForcastedSetting.BY_YEAR
            '            StrQ &= "	AND CONVERT(CHAR(8), Retr.date1, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(yy," & intForcastedValue & ", @Date), 112) "
            '    End Select

            'End If

            'If blnShow_ForcastedEOC_Dates = True Then
            '    StrQ &= " SELECT 10 AS ID " & _
            '                 strTableEmpFields.Replace("#TableEmp.", "EocDt.") & _
            '                 ", CONVERT(CHAR(8),EocDt.date1,112) AS Date " & _
            '                 ", EocDt.date1 AS sortdate " & _
            '                 " FROM " & _
            '                 " ( " & _
            '                 "  SELECT date1 " & _
            '                     ", date2 " & _
            '                     strTableEmpFields & _
            '                     ", ROW_NUMBER() OVER (PARTITION BY hremployee_dates_tran.employeeunkid ORDER BY effectivedate DESC ) AS rno " & _
            '                 " FROM hremployee_dates_tran " & _
            '                 " JOIN #TableEmp ON hremployee_dates_tran.employeeunkid = #TableEmp.employeeunkid "

            '    If blnIsImgInDataBase = True Then
            '        StrQ &= " LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid "
            '    Else
            '        StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_dates_tran.employeeunkid " & _
            '                     " AND empimage.referenceid = 1 "
            '    End If

            '    StrQ &= " WHERE isvoid = 0 " & _
            '                 " AND datetypeunkid = " & enEmp_Dates_Transaction.DT_TERMINATION & " " & _
            '                 " AND CONVERT(CHAR(8), effectivedate, 112) <= '" & eZeeDate.convertDate(dtPeriodEnd) & "' " & _
            '                 " ) AS EocDt " & _
            '                 " WHERE EocDt.rno = 1 "

            '    Select Case intForcastedEOC_ViewSetting
            '        Case clsConfigOptions.enForcastedSetting.BY_MONTH
            '            StrQ &= "	AND CONVERT(CHAR(8), EocDt.date1, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(mm," & intForcastedEOCValue & ", @Date), 112) "
            '        Case clsConfigOptions.enForcastedSetting.BY_YEAR
            '            StrQ &= "	AND CONVERT(CHAR(8), EocDt.date1, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(yy," & intForcastedEOCValue & ", @Date), 112) "
            '    End Select

            'End If



            'If (ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo) AndAlso blnShow_ForcastedELC_Dates = True Then
            '    StrQ &= " SELECT 11 AS ID " & _
            '                 strTableEmpFields & _
            '                 ", CONVERT(CHAR(8),enddate,112) AS Date " & _
            '                 ", enddate AS sortdate " & _
            '                 " FROM #TableEmp " & _
            '                 " JOIN lvleavebalance_tran ON lvleavebalance_tran.employeeunkid = #TableEmp.employeeunkid " & _
            '                 " AND isshortleave = 0 " & _
            '                 " AND isvoid = 0 "

            '    If intLeaveBalanceSetting = enLeaveBalanceSetting.Financial_Year Then
            '        StrQ &= " AND isclose_fy = 0 "
            '    ElseIf intLeaveBalanceSetting = enLeaveBalanceSetting.ELC Then
            '        StrQ &= " AND isopenelc = 1 "
            '    End If

            '    Select Case intForcastedELCViewSetting
            '        Case clsConfigOptions.enForcastedSetting.BY_DAY
            '            StrQ &= "	WHERE CONVERT(CHAR(8), enddate, 112) BETWEEN CONVERT(CHAR(8), @Date + 1, 112) AND CONVERT(CHAR(8), DATEADD(dd," & intForcastedELCValue & ", @Date), 112) "
            '    End Select

            'End If

            'StrQ &= " DROP TABLE #TableEmp "

            StrQ = StrQ.Replace("#TableFields", strTableEmpFields).Replace("#TableCondition", strTableCondition)


            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
            If blnIsImgInDataBase = True Then
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkid)
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "Trans")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If blnIsImgInDataBase = True Then
                dsList.Tables(0).AsEnumerable().Cast(Of DataRow).ToList().ForEach(Function(x) convertImageToBase64(x, xNoImageString))
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: EmployeeDatesCountForSelfService; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Pinkal (09-Aug-2021) -- End

    Private Shared Function convertImageToBase64(ByVal dr As DataRow, ByVal strNoimage As String) As Boolean
        Try
            If IsNothing(dr.Field(Of Byte())("photo")) = False Then
                Dim bytes As Byte() = dr.Field(Of Byte())("photo")
                Dim base64String As String = Convert.ToBase64String(bytes, 0, bytes.Length)
                dr("imagename") = "data:image/png;base64," & base64String
            Else
                dr("imagename") = "data:image/png;base64," & strNoimage
            End If
            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "convertImageToBase64", mstrModuleName)
        End Try
    End Function


    'Pinkal (09-Aug-2021)-- Start
    'NMB New UI Enhancements.

    'Public Shared Function TeamMembers(ByVal strDatabaseName As String _
    '                                     , ByVal intCompanyUnkid As Integer _
    '                                     , ByVal intYearUnkId As Integer _
    '                                     , ByVal intuserUnkid As Integer _
    '                                     , ByVal dtPeriodStart As Date _
    '                                     , ByVal dtPeriodEnd As Date _
    '                                     , ByVal strUserModeSetting As String _
    '                                     , ByVal blnOnlyApproved As Boolean _
    '                                     , ByVal blnApplyUserAccessFilter As Boolean _
    '                                     , ByVal blnIsImgInDataBase As Boolean _
    '                                     , ByVal xNoImageString As String _
    '                                     , Optional ByVal strAdvanceFilter As String = "" _
    '                                     , Optional ByVal strORQueryForUserAccess As String = "" _
    '                                     ) As DataSet
    '    Dim dsList As New DataSet
    '    Dim StrQ As String = String.Empty
    '    Dim strTableEmpFields As String = ""

    '    Try

    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodEnd, dtPeriodEnd, , , strDatabaseName)
    '        If blnApplyUserAccessFilter = True Then
    '            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intuserUnkid, intCompanyUnkid, intYearUnkId, strUserModeSetting, , , strORQueryForUserAccess)
    '        End If
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

    '        Dim objDataOperation As New clsDataOperation


    '        If blnIsImgInDataBase = True Then
    '            StrQ = "IF OBJECT_ID('tempdb..#empimage') IS NOT NULL " & _
    '                    "DROP TABLE #empimage " & _
    '                                        "CREATE TABLE #empimage ( " & _
    '                                            "photo image, " & _
    '                                            "employeeunkid int " & _
    '                                        "); " & _
    '                    " " & _
    '                    "INSERT INTO #empimage " & _
    '                         "SELECT " & _
    '                              "photo " & _
    '                            ",employeeunkid " & _
    '                         "FROM arutiimages..hremployee_images " & _
    '                         "WHERE isvoid = 0 " & _
    '                         "and companyunkid = @companyunkid "
    '        End If

    '        StrQ &= "SELECT hremployee_master.employeeunkid " & _
    '                        ", hremployee_master.employeecode " & _
    '                        ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename " & _
    '                        ", ISNULL(hrdepartment_master.name, '') AS deptname " & _
    '                        ", ISNULL(hrjob_master.job_name, '') AS jobname " & _
    '                        ", ISNULL(hrteam_master.name, '') AS teamname "

    '        'Pinkal (01-Jun-2021)-- Start
    '        'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '        '", ADF.departmentunkid " & _
    '        '", ADF.jobunkid " & _
    '        '", ADF.teamunkid " & _
    '        'Pinkal (01-Jun-2021) -- End


    '        If blnIsImgInDataBase = True Then
    '            StrQ &= ", empimage.photo " & _
    '                    ", '' AS imagename "
    '        Else
    '            StrQ &= ", NULL AS photo " & _
    '                    ", ISNULL(empimage.imagename, '') AS imagename "
    '        End If

    '        StrQ &= "FROM hremployee_master "

    '        'Pinkal (09-Aug-2021)-- Start
    '        'NMB New UI Enhancements.

    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= " " & xDateJoinQry & " "
    '        End If

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrQ &= " " & xAdvanceJoinQry & " "
    '        End If

    '        'Pinkal (09-Aug-2021) -- End


    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= " " & xUACQry & " "
    '        End If

    '        StrQ &= "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = ADF.departmentunkid " & _
    '                "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = ADF.jobunkid " & _
    '                "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = ADF.teamunkid "

    '        If blnIsImgInDataBase = True Then
    '            StrQ &= "LEFT JOIN #empimage as empimage ON empimage.employeeunkid = hremployee_master.employeeunkid "
    '        Else
    '            StrQ &= "LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_master.employeeunkid " & _
    '                    "AND empimage.referenceid = 1 "
    '        End If

    '        StrQ &= "WHERE 1 = 1 "

    '        If xDateFilterQry.Trim.Length > 0 Then
    '            StrQ &= " " & xDateFilterQry & " "
    '        End If

    '        If strAdvanceFilter.Trim.Length > 0 Then
    '            StrQ &= " AND " & strAdvanceFilter & " "
    '        End If

    '        If blnIsImgInDataBase = True Then
    '            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkid)
    '        End If

    '        dsList = objDataOperation.ExecQuery(StrQ, "Trans")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If

    '        If blnIsImgInDataBase = True Then
    '            dsList.Tables(0).AsEnumerable().Cast(Of DataRow).ToList().ForEach(Function(x) convertImageToBase64(x, xNoImageString))
    '        End If


    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: TeamMembers; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function

    Public Shared Function TeamMembers(ByVal strDatabaseName As String _
                                         , ByVal intCompanyUnkid As Integer _
                                         , ByVal intYearUnkId As Integer _
                                         , ByVal intuserUnkid As Integer _
                                         , ByVal dtPeriodStart As Date _
                                         , ByVal dtPeriodEnd As Date _
                                         , ByVal strUserModeSetting As String _
                                         , ByVal blnOnlyApproved As Boolean _
                                         , ByVal blnApplyUserAccessFilter As Boolean _
                                         , ByVal blnIsImgInDataBase As Boolean _
                                         , ByVal xNoImageString As String _
                                         , Optional ByVal strAdvanceFilter As String = "" _
                                         , Optional ByVal strORQueryForUserAccess As String = "" _
                                         ) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim strTableEmpFields As String = ""

        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodEnd, dtPeriodEnd, , , strDatabaseName)
            If blnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intuserUnkid, intCompanyUnkid, intYearUnkId, strUserModeSetting, , , strORQueryForUserAccess)
            End If
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim objDataOperation As New clsDataOperation



            StrQ = "SELECT hremployee_master.employeeunkid " & _
                            ", hremployee_master.employeecode " & _
                            ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename " & _
                            ", ISNULL(eDisp.deptname, '') AS deptname " & _
                            ", ISNULL(eJob.jobname, '') AS jobname " & _
                            ", ISNULL(eDisp.teamname, '') AS teamname "


            If blnIsImgInDataBase = True Then
                StrQ &= ", empimage.photo , '' AS imagename "
            Else
                StrQ &= ", NULL AS photo , ISNULL(empimage.imagename, '') AS imagename "
            End If

            StrQ &= "FROM hremployee_master "

            If blnIsImgInDataBase = True Then

                StrQ &= " LEFT JOIN " & _
                             " ( " & _
                             "      SELECT " & _
                             "              photo " & _
                             "             ,employeeunkid " & _
                             "      FROM arutiimages..hremployee_images " & _
                             "      WHERE isvoid = 0 AND companyunkid = @companyunkid " & _
                             ") AS empimage ON empimage.employeeunkid =  hremployee_master.employeeunkid "
            Else
                StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_master.employeeunkid AND empimage.referenceid = 1 "
            End If


            StrQ &= " LEFT JOIN " & _
                       " ( " & _
                       "         SELECT " & _
                       "              ETT.employeeunkid AS TrfEmpId " & _
                       "             ,ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                       "             ,ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                       "             ,ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                       "             ,ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                       "             ,ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                       "             ,ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                       "             ,ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                       "             ,ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                       "             ,ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                       "             ,ISNULL(ETT.classunkid, 0) AS classunkid " & _
                       "             ,CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
                       "             ,ETT.employeeunkid " & _
                       "             ,ISNULL(hrdepartment_master.name, '') AS deptname " & _
                       "             ,ISNULL(hrteam_master.name, '') AS teamname " & _
                       "             ,ROW_NUMBER() OVER (PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                       "         FROM hremployee_transfer_tran AS ETT " & _
                       "         LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = ETT.departmentunkid " & _
                       "         LEFT JOIN hrteam_master ON hrteam_master.teamunkid = ETT.teamunkid " & _
                       " WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), @Date, 112)  " & _
                       " ) AS eDisp ON eDisp.employeeunkid = hremployee_master.employeeunkid AND eDisp.Rno = 1 " & _
                       " LEFT JOIN " & _
                       " ( " & _
                       "         SELECT " & _
                       "             ECT.employeeunkid AS CatEmpId " & _
                       "            ,ECT.jobgroupunkid " & _
                       "            ,ECT.jobunkid " & _
                       "            ,CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
                       "            ,ECT.employeeunkid " & _
                       "            ,ISNULL(hrjob_master.job_name, '') AS jobname " & _
                       "            ,ROW_NUMBER() OVER (PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                       "         FROM hremployee_categorization_tran AS ECT " & _
                       "         LEFT JOIN hrjob_master ON hrjob_master.jobunkid = ECT.jobunkid " & _
                       "         WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), @Date, 112) " & _
                       ") AS eJob on eJob.employeeunkid = hremployee_master.employeeunkid AND eJob.Rno = 1"


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= " " & xDateJoinQry & " "
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= " " & xAdvanceJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= " " & xUACQry & " "
            End If

            StrQ &= "WHERE 1 = 1 "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= " " & xDateFilterQry & " "
            End If

            If strAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & strAdvanceFilter & " "
            End If

            If blnIsImgInDataBase = True Then
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkid)
            End If

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))

            dsList = objDataOperation.ExecQuery(StrQ, "Trans")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If blnIsImgInDataBase = True Then
                dsList.Tables(0).AsEnumerable().Cast(Of DataRow).ToList().ForEach(Function(x) convertImageToBase64(x, xNoImageString))
            End If


            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: TeamMembers; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Pinkal (09-Aug-2021) -- End


    'Pinkal (09-Aug-2021)-- Start
    'NMB New UI Enhancements.

    'Public Shared Function UpcomingHolidays(ByVal strDatabaseName As String _
    '                                         , ByVal intCompanyUnkid As Integer _
    '                                         , ByVal intYearUnkId As Integer _
    '                                         , ByVal intuserUnkid As Integer _
    '                                         , ByVal dtPeriodStart As Date _
    '                                         , ByVal dtPeriodEnd As Date _
    '                                         , ByVal strUserModeSetting As String _
    '                                         , ByVal blnOnlyApproved As Boolean _
    '                                         , ByVal blnApplyUserAccessFilter As Boolean _
    '                                         , Optional ByVal strAdvanceFilter As String = "" _
    '                                         , Optional ByVal strORQueryForUserAccess As String = "" _
    '                                         , Optional ByVal intEmployeeunkid As Integer = 0 _
    '                                         ) As DataSet
    '    Dim dsList As New DataSet
    '    Dim StrQ As String = String.Empty

    '    Try

    '        'Pinkal (01-Jun-2021)-- Start
    '        'New UI Self Service Enhancement : Working on New UI Dashboard Settings.

    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodEnd, dtPeriodEnd, , , strDatabaseName)


    '        'Pinkal (09-Aug-2021)-- Start
    '        'NMB New UI Enhancements.

    '        'If blnApplyUserAccessFilter = True Then
    '        '    Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intuserUnkid, intCompanyUnkid, intYearUnkId, strUserModeSetting, , , strORQueryForUserAccess)
    '        'End If
    '        'If strAdvanceFilter.Trim <> "" Then
    '        '    Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
    '        'End If

    '        If blnApplyUserAccessFilter = True Then
    '            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intuserUnkid, intCompanyUnkid, intYearUnkId, strUserModeSetting, , , strORQueryForUserAccess)
    '        Else
    '            Call GetAdvanceFilterQry(xUACQry, dtPeriodEnd, strDatabaseName)
    '        End If
    '        'Pinkal (09-Aug-2021) -- End

    '        Dim objDataOperation As New clsDataOperation

    '        StrQ = "SELECT hremployee_master.employeeunkid "

    '        StrQ &= "INTO #TableEmp " & _
    '                "FROM hremployee_master "

    '        'If xAdvanceJoinQry.Trim.Length > 0 Then
    '        '    StrQ &= " " & xAdvanceJoinQry & " "
    '        'End If

    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= " " & xDateJoinQry & " "
    '        End If

    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= " " & xUACQry & " "
    '        End If

    '        StrQ &= "WHERE 1 = 1 "

    '        If intEmployeeunkid > 0 Then
    '            StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
    '        End If

    '        If xDateFilterQry.Trim.Length > 0 Then
    '            StrQ &= " " & xDateFilterQry & " "
    '        End If

    '        If strAdvanceFilter.Trim.Length > 0 Then
    '            StrQ &= " AND " & strAdvanceFilter & " "
    '        End If

    '        StrQ &= " SELECT DISTINCT " & _
    '                     " lvholiday_master.holidayname " & _
    '                  ", lvholiday_master.holidaydate " & _
    '                     " FROM lvemployee_holiday " & _
    '                     " JOIN #TableEmp ON lvemployee_holiday.employeeunkid = #TableEmp.employeeunkid " & _
    '                     " JOIN lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid " & _
    '                     " WHERE lvholiday_master.isactive = 1 " & _
    '                     " AND CONVERT(CHAR(8), lvholiday_master.holidaydate, 112) " & _
    '                     " BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(yy, 1, @Date), 112) " & _
    '                     " ORDER BY lvholiday_master.holidaydate "

    '        'Pinkal (01-Jun-2021)-- New UI Self Service Enhancement : Working on New UI Dashboard Settings.[" LEFT JOIN lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid " & _]

    '        StrQ &= " DROP TABLE #TableEmp "

    '        objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
    '        If intEmployeeunkid > 0 Then
    '            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
    '        End If

    '        dsList = objDataOperation.ExecQuery(StrQ, "Trans")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: UpcomingHolidays; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function


    Public Shared Function UpcomingHolidays(ByVal strDatabaseName As String _
                                             , ByVal intCompanyUnkid As Integer _
                                             , ByVal intYearUnkId As Integer _
                                             , ByVal intuserUnkid As Integer _
                                             , ByVal dtPeriodStart As Date _
                                             , ByVal dtPeriodEnd As Date _
                                             , ByVal strUserModeSetting As String _
                                             , ByVal blnOnlyApproved As Boolean _
                                             , ByVal blnApplyUserAccessFilter As Boolean _
                                             , Optional ByVal strAdvanceFilter As String = "" _
                                             , Optional ByVal strORQueryForUserAccess As String = "" _
                                             , Optional ByVal intEmployeeunkid As Integer = 0 _
                                             ) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty

        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodEnd, dtPeriodEnd, , , strDatabaseName)

            If blnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intuserUnkid, intCompanyUnkid, intYearUnkId, strUserModeSetting, , , strORQueryForUserAccess)
            Else
                Call GetAdvanceFilterQry(xUACQry, dtPeriodEnd, strDatabaseName)
            End If

            Dim objDataOperation As New clsDataOperation

            StrQ = "SELECT hremployee_master.employeeunkid "

            StrQ &= "INTO #TableEmp " & _
                    "FROM hremployee_master "


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= " " & xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= " " & xUACQry & " "
            End If

            StrQ &= "WHERE 1 = 1 "

            If intEmployeeunkid > 0 Then
                StrQ &= " AND hremployee_master.employeeunkid = @employeeunkid "
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= " " & xDateFilterQry & " "
            End If

            If strAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & strAdvanceFilter & " "
            End If

            StrQ &= " SELECT DISTINCT " & _
                         " lvholiday_master.holidayname " & _
                      ", lvholiday_master.holidaydate " & _
                         " FROM lvemployee_holiday " & _
                         " JOIN #TableEmp ON lvemployee_holiday.employeeunkid = #TableEmp.employeeunkid " & _
                         " JOIN lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid " & _
                         " WHERE lvholiday_master.isactive = 1 " & _
                         " AND CONVERT(CHAR(8), lvholiday_master.holidaydate, 112) " & _
                         " BETWEEN CONVERT(CHAR(8), @Date, 112) AND CONVERT(CHAR(8), DATEADD(yy, 1, @Date), 112) " & _
                         " ORDER BY lvholiday_master.holidaydate "

            'Pinkal (01-Jun-2021)-- New UI Self Service Enhancement : Working on New UI Dashboard Settings.[" LEFT JOIN lvholiday_master ON lvemployee_holiday.holidayunkid = lvholiday_master.holidayunkid " & _]

            StrQ &= " DROP TABLE #TableEmp "

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
            If intEmployeeunkid > 0 Then
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeunkid)
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "Trans")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpcomingHolidays; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function


    'Pinkal (09-Aug-2021)-- Start
    'NMB New UI Enhancements.

    'Public Shared Function EmployeeOnLeave(ByVal strDatabaseName As String _
    '                                         , ByVal intCompanyUnkid As Integer _
    '                                         , ByVal intYearUnkId As Integer _
    '                                         , ByVal intuserUnkid As Integer _
    '                                         , ByVal dtPeriodStart As Date _
    '                                         , ByVal dtPeriodEnd As Date _
    '                                         , ByVal strUserModeSetting As String _
    '                                         , ByVal blnOnlyApproved As Boolean _
    '                                         , ByVal blnApplyUserAccessFilter As Boolean _
    '                                         , ByVal blnIsImgInDataBase As Boolean _
    '                                         , ByVal xNoImageString As String _
    '                                         , Optional ByVal strAdvanceFilter As String = "" _
    '                                         , Optional ByVal strORQueryForUserAccess As String = "" _
    '                                         ) As DataSet
    '    Dim dsList As New DataSet
    '    Dim StrQ As String = String.Empty
    '    Dim strTableEmpFields As String = ""

    '    Try

    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodEnd, dtPeriodEnd, , , strDatabaseName)

    '        If blnApplyUserAccessFilter = True Then
    '            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intuserUnkid, intCompanyUnkid, intYearUnkId, strUserModeSetting, , , strORQueryForUserAccess)
    '        End If
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

    '        Dim objDataOperation As New clsDataOperation

    '        strTableEmpFields = ", #TableEmp.employeeunkid " & _
    '                            ", #TableEmp.employeename " & _
    '                            ", #TableEmp.employeecode " & _
    '                            ", #TableEmp.deptname " & _
    '                            ", #TableEmp.jobname " & _
    '                            ", #TableEmp.teamname " & _
    '                            ", empimage.photo " & _
    '                            ", #TableEmp.imagename "

    '        'Pinkal (01-Jun-2021)-- Start
    '        'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '        '", #TableEmp.departmentunkid " & _
    '        '", #TableEmp.jobunkid " & _
    '        '", #TableEmp.teamunkid " & _
    '        'Pinkal (01-Jun-2021) -- End


    '        If blnIsImgInDataBase = True Then
    '            StrQ = "IF OBJECT_ID('tempdb..#empimage') IS NOT NULL " & _
    '                    "DROP TABLE #empimage " & _
    '                                        "CREATE TABLE #empimage ( " & _
    '                                            "photo image, " & _
    '                                            "employeeunkid int " & _
    '                                        "); " & _
    '                    " " & _
    '                    "INSERT INTO #empimage " & _
    '                         "SELECT " & _
    '                              "photo " & _
    '                            ",employeeunkid " & _
    '                         "FROM arutiimages..hremployee_images " & _
    '                         "WHERE isvoid = 0 " & _
    '                         "and companyunkid = @companyunkid "
    '        End If

    '        StrQ &= "SELECT hremployee_master.employeeunkid " & _
    '                        ", hremployee_master.employeecode " & _
    '                        ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename " & _
    '                        ", ISNULL(hrdepartment_master.name, '') AS deptname " & _
    '                        ", ISNULL(hrjob_master.job_name, '') AS jobname " & _
    '                        ", ISNULL(hrteam_master.name, '') AS teamname "

    '        'Pinkal (01-Jun-2021)-- Start
    '        'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    '        '", ADF.departmentunkid " & _
    '        '", ADF.jobunkid " & _
    '        '", ADF.teamunkid " & _
    '        'Pinkal (01-Jun-2021) -- End


    '        If blnIsImgInDataBase = True Then
    '            StrQ &= ", '' AS imagename "
    '        Else
    '            StrQ &= ", ISNULL(empimage.imagename, '') AS imagename "
    '        End If

    '        StrQ &= "INTO #TableEmp " & _
    '                "FROM hremployee_master "


    '        'Pinkal (09-Aug-2021)-- Start
    '        'NMB New UI Enhancements.

    '        If xDateJoinQry.Trim.Length > 0 Then
    '            StrQ &= " " & xDateJoinQry & " "
    '        End If

    '        If xAdvanceJoinQry.Trim.Length > 0 Then
    '            StrQ &= " " & xAdvanceJoinQry & " "
    '        End If

    '        'Pinkal (09-Aug-2021) -- End


    '        If xUACQry.Trim.Length > 0 Then
    '            StrQ &= " " & xUACQry & " "
    '        End If

    '        StrQ &= "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = ADF.departmentunkid " & _
    '                "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = ADF.jobunkid " & _
    '                "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = ADF.teamunkid "


    '        StrQ &= "WHERE 1 = 1 "

    '        If xDateFilterQry.Trim.Length > 0 Then
    '            StrQ &= " " & xDateFilterQry & " "
    '        End If

    '        If strAdvanceFilter.Trim.Length > 0 Then
    '            StrQ &= " AND " & strAdvanceFilter & " "
    '        End If

    '        StrQ &= "SELECT lvleaveIssue_tran.leaveissuetranunkid " & _
    '                     ", lvleaveIssue_tran.leaveissueunkid " & _
    '                     ", lvleaveIssue_master.employeeunkid " & _
    '                     ", CASE WHEN LTRIM(RTRIM(ISNULL(lvleaveform.remark, ''))) = '' THEN lvleavetype_master.leavename ELSE lvleaveform.remark END AS remark " & _
    '                "INTO #leave " & _
    '                "FROM lvleaveIssue_tran " & _
    '                    "JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
    '                    "JOIN #TableEmp ON #TableEmp.employeeunkid = lvleaveIssue_master.employeeunkid " & _
    '                    "JOIN lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid " & _
    '                    "JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid " & _
    '                "WHERE lvleaveIssue_tran.isvoid = 0 " & _
    '                      "AND lvleaveIssue_master.isvoid = 0 " & _
    '                      "AND CONVERT(CHAR(8), lvleaveIssue_tran.leavedate, 112) = @date " & _
    '                " " & _
    '                "SELECT A.leaveissueunkid " & _
    '                     ", A.employeeunkid " & _
    '                     ", MAX(A.leavedate) AS leavedateupto " & _
    '                     ", SUM(A.dayfraction) AS dayfraction " & _
    '                "INTO #currleave " & _
    '                "FROM " & _
    '                "( " & _
    '                    "SELECT lvleaveIssue_tran.leaveissueunkid, #leave.employeeunkid, lvleaveIssue_tran.leavedate, lvleaveIssue_tran.dayfraction " & _
    '                         ", DATEDIFF( " & _
    '                                       "DAY " & _
    '                                     ", CONVERT(CHAR(8), lvleaveIssue_tran.leavedate, 112) " & _
    '                                     ", (CAST(@date AS DATETIME) + ROW_NUMBER() OVER (PARTITION BY lvleaveIssue_tran.leaveissueunkid " & _
    '                                                                                     "ORDER BY lvleaveIssue_tran.leavedate " & _
    '                                                                                    ") - 1 " & _
    '                                       ") " & _
    '                                   ") AS ROWNOdiff " & _
    '                    "FROM lvleaveIssue_tran " & _
    '                        "JOIN #leave ON lvleaveIssue_tran.leaveissueunkid = #leave.leaveissueunkid " & _
    '                    "WHERE CONVERT(CHAR(8), lvleaveIssue_tran.leavedate, 112) >= @date " & _
    '                          "AND lvleaveIssue_tran.isvoid = 0 " & _
    '                ") AS A " & _
    '                "WHERE A.ROWNOdiff = 0 " & _
    '                "GROUP BY A.leaveissueunkid " & _
    '                       ", A.employeeunkid "

    '        StrQ &= "SELECT #currleave.leaveissueunkid " & _
    '                     ", #currleave.employeeunkid " & _
    '                     ", #currleave.leavedateupto " & _
    '                     ", #currleave.dayfraction " & _
    '                     strTableEmpFields & _
    '                "FROM #currleave " & _
    '                    "JOIN #TableEmp ON #currleave.employeeunkid = #TableEmp.employeeunkid "

    '        If blnIsImgInDataBase = True Then
    '            StrQ &= "LEFT JOIN #empimage as empimage ON empimage.employeeunkid = #currleave.employeeunkid "
    '        Else
    '            StrQ &= "LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = #currleave.employeeunkid " & _
    '                    "AND empimage.referenceid = 1 "
    '        End If

    '        StrQ &= "DROP TABLE #TableEmp " & _
    '                "DROP TABLE #leave " & _
    '                "DROP TABLE #currleave "

    '        objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
    '        If blnIsImgInDataBase = True Then
    '            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkid)
    '        End If

    '        dsList = objDataOperation.ExecQuery(StrQ, "Trans")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '        End If

    '        If blnIsImgInDataBase = True Then
    '            dsList.Tables(0).AsEnumerable().Cast(Of DataRow).ToList().ForEach(Function(x) convertImageToBase64(x, xNoImageString))
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: EmployeeOnLeave; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function

    Public Shared Function EmployeeOnLeave(ByVal strDatabaseName As String _
                                             , ByVal intCompanyUnkid As Integer _
                                             , ByVal intYearUnkId As Integer _
                                             , ByVal intuserUnkid As Integer _
                                             , ByVal dtPeriodStart As Date _
                                             , ByVal dtPeriodEnd As Date _
                                             , ByVal strUserModeSetting As String _
                                             , ByVal blnOnlyApproved As Boolean _
                                             , ByVal blnApplyUserAccessFilter As Boolean _
                                             , ByVal blnIsImgInDataBase As Boolean _
                                             , ByVal xNoImageString As String _
                                             , Optional ByVal strAdvanceFilter As String = "" _
                                             , Optional ByVal strORQueryForUserAccess As String = "" _
                                             ) As DataSet
        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim strTableEmpFields As String = ""

        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodEnd, dtPeriodEnd, , , strDatabaseName)

            If blnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intuserUnkid, intCompanyUnkid, intYearUnkId, strUserModeSetting, , , strORQueryForUserAccess)
            End If
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)

            Dim objDataOperation As New clsDataOperation

            'If blnIsImgInDataBase = True Then
            '    StrQ = "IF OBJECT_ID('tempdb..#empimage') IS NOT NULL " & _
            '            "DROP TABLE #empimage " & _
            '                                "CREATE TABLE #empimage ( " & _
            '                                    "photo image, " & _
            '                                    "employeeunkid int " & _
            '                                "); " & _
            '            " " & _
            '            "INSERT INTO #empimage " & _
            '                 "SELECT " & _
            '                      "photo " & _
            '                    ",employeeunkid " & _
            '                 "FROM arutiimages..hremployee_images " & _
            '                 "WHERE isvoid = 0 " & _
            '                 "and companyunkid = @companyunkid "
            'End If

            StrQ = " SELECT hremployee_master.employeeunkid " & _
                       ", hremployee_master.employeecode " & _
                       ", hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname AS employeename " & _
                       ", ISNULL(eDisp.deptname, '') AS deptname " & _
                       ", ISNULL(eJob.jobname, '') AS jobname " & _
                       ", ISNULL(eDisp.teamname, '') AS teamname " & _
                       ", Issue.leaveissueunkid " & _
                       ", Issue.leavedateupto " & _
                       ", Issue.dayfraction  "

            If blnIsImgInDataBase = True Then
                StrQ &= ",empimage.photo, '' AS imagename "
            Else
                StrQ &= ",NULL AS photo, ISNULL(empimage.imagename, '') AS imagename "
            End If

            StrQ &= " FROM hremployee_master "

            If blnIsImgInDataBase = True Then

                StrQ &= " LEFT JOIN " & _
                             " ( " & _
                             "      SELECT " & _
                             "              photo " & _
                             "             ,employeeunkid " & _
                             "      FROM arutiimages..hremployee_images " & _
                             "      WHERE isvoid = 0 AND companyunkid = @companyunkid " & _
                             ") AS empimage ON empimage.employeeunkid =  hremployee_master.employeeunkid "
            Else
                StrQ &= " LEFT JOIN hr_images_tran as empimage ON empimage.employeeunkid = hremployee_master.employeeunkid AND empimage.referenceid = 1 "
            End If


            StrQ &= " LEFT JOIN " & _
                         " ( " & _
                         "         SELECT " & _
                         "              ETT.employeeunkid AS TrfEmpId " & _
                         "             ,ISNULL(ETT.stationunkid, 0) AS stationunkid " & _
                         "             ,ISNULL(ETT.deptgroupunkid, 0) AS deptgroupunkid " & _
                         "             ,ISNULL(ETT.departmentunkid, 0) AS departmentunkid " & _
                         "             ,ISNULL(ETT.sectiongroupunkid, 0) AS sectiongroupunkid " & _
                         "             ,ISNULL(ETT.sectionunkid, 0) AS sectionunkid " & _
                         "             ,ISNULL(ETT.unitgroupunkid, 0) AS unitgroupunkid " & _
                         "             ,ISNULL(ETT.unitunkid, 0) AS unitunkid " & _
                         "             ,ISNULL(ETT.teamunkid, 0) AS teamunkid " & _
                         "             ,ISNULL(ETT.classgroupunkid, 0) AS classgroupunkid " & _
                         "             ,ISNULL(ETT.classunkid, 0) AS classunkid " & _
                         "             ,CONVERT(CHAR(8), ETT.effectivedate, 112) AS EfDt " & _
                         "             ,ETT.employeeunkid " & _
                         "             ,ISNULL(hrdepartment_master.name, '') AS deptname " & _
                         "             ,ISNULL(hrteam_master.name, '') AS teamname " & _
                         "             ,ROW_NUMBER() OVER (PARTITION BY ETT.employeeunkid ORDER BY ETT.effectivedate DESC) AS Rno " & _
                         "         FROM hremployee_transfer_tran AS ETT " & _
                         "         LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = ETT.departmentunkid " & _
                         "         LEFT JOIN hrteam_master ON hrteam_master.teamunkid = ETT.teamunkid " & _
                         " WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), @Date, 112)  " & _
                         " ) AS eDisp ON eDisp.employeeunkid = hremployee_master.employeeunkid AND eDisp.Rno = 1 " & _
                         " LEFT JOIN " & _
                         " ( " & _
                         "         SELECT " & _
                         "             ECT.employeeunkid AS CatEmpId " & _
                         "            ,ECT.jobgroupunkid " & _
                         "            ,ECT.jobunkid " & _
                         "            ,CONVERT(CHAR(8), ECT.effectivedate, 112) AS CEfDt " & _
                         "            ,ECT.employeeunkid " & _
                         "            ,ISNULL(hrjob_master.job_name, '') AS jobname " & _
                         "            ,ROW_NUMBER() OVER (PARTITION BY ECT.employeeunkid ORDER BY ECT.effectivedate DESC) AS Rno " & _
                         "         FROM hremployee_categorization_tran AS ECT " & _
                         "         LEFT JOIN hrjob_master ON hrjob_master.jobunkid = ECT.jobunkid " & _
                         "         WHERE isvoid = 0 AND CONVERT(CHAR(8), effectivedate, 112) <= CONVERT(CHAR(8), @Date, 112) " & _
                         ") AS eJob on eJob.employeeunkid = hremployee_master.employeeunkid AND eJob.Rno = 1"


            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= " " & xDateJoinQry & " "
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                StrQ &= " " & xAdvanceJoinQry & " "
            End If


            If xUACQry.Trim.Length > 0 Then
                StrQ &= " " & xUACQry & " "
            End If

            StrQ &= " LEFT JOIN  " & _
                         " ( " & _
                         "      SELECT lvleaveIssue_tran.leaveissuetranunkid " & _
                         "          , lvleaveIssue_tran.leaveissueunkid " & _
                         "          , lvleaveIssue_master.employeeunkid " & _
                         "          , CASE WHEN LTRIM(RTRIM(ISNULL(lvleaveform.remark, ''))) = '' THEN lvleavetype_master.leavename ELSE lvleaveform.remark END AS remark " & _
                         "      FROM lvleaveIssue_tran " & _
                         "      JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                         "      JOIN lvleaveform ON lvleaveform.formunkid = lvleaveIssue_master.formunkid " & _
                         "      JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lvleaveform.leavetypeunkid " & _
                         "      WHERE lvleaveIssue_tran.isvoid = 0 AND lvleaveIssue_master.isvoid = 0 " & _
                         "      AND CONVERT(CHAR(8), lvleaveIssue_tran.leavedate, 112) = @date " & _
                         " ) " & _
                         " AS leave ON leave.employeeunkid = hremployee_master.employeeunkid " & _
                         " JOIN " & _
                         " ( " & _
                         "      SELECT A.leaveissueunkid " & _
                         "                  , MAX(A.leavedate) AS leavedateupto " & _
                         "                  , SUM(A.dayfraction) AS dayfraction " & _
                         "      FROM " & _
                         "              ( " & _
                         "                  SELECT lvleaveIssue_tran.leaveissueunkid,lvleaveIssue_tran.leavedate, lvleaveIssue_tran.dayfraction " & _
                         "                             , DATEDIFF( DAY , CONVERT(CHAR(8), lvleaveIssue_tran.leavedate, 112) " & _
                         "                             , (CAST(@date AS DATETIME) + ROW_NUMBER() OVER (PARTITION BY lvleaveIssue_tran.leaveissueunkid " & _
                         "                             ORDER BY lvleaveIssue_tran.leavedate ) - 1 )) AS ROWNOdiff " & _
                         "                   FROM lvleaveIssue_tran " & _
                         "                   WHERE CONVERT(CHAR(8), lvleaveIssue_tran.leavedate, 112) >= @date AND lvleaveIssue_tran.isvoid = 0 " & _
                         "              ) AS A WHERE A.ROWNOdiff = 0 " & _
                         "       GROUP BY A.leaveissueunkid " & _
                         ") " & _
                         " AS Issue on Issue.leaveissueunkid = leave.leaveissueunkid "

            StrQ &= "WHERE 1 = 1 "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= " " & xDateFilterQry & " "
            End If

            If strAdvanceFilter.Trim.Length > 0 Then
                StrQ &= " AND " & strAdvanceFilter & " "
            End If


            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
            If blnIsImgInDataBase = True Then
                objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkid)
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "Trans")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If blnIsImgInDataBase = True Then
                dsList.Tables(0).AsEnumerable().Cast(Of DataRow).ToList().ForEach(Function(x) convertImageToBase64(x, xNoImageString))
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: EmployeeOnLeave; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    'Pinkal (09-Aug-2021) -- End

    Public Shared Function PendingTask(ByVal strDatabaseName As String _
                                       , ByVal intCompanyUnkid As Integer _
                                       , ByVal intYearUnkId As Integer _
                                       , ByVal intUserUnkid As Integer _
                                       , ByVal dtPeriodStart As Date _
                                       , ByVal dtPeriodEnd As Date _
                                       , ByVal strUserModeSetting As String _
                                       , ByVal blnOnlyApproved As Boolean _
                                       , ByVal blnApplyUserAccessFilter As Boolean _
                                       , Optional ByVal intEmployeeUnkId As Integer = 0 _
                                       , Optional ByVal strAdvanceFilter As String = "" _
                                       , Optional ByVal strORQueryForUserAccess As String = "" _
                                       , Optional ByVal blnSetScoreCardESS As Boolean = False _
                                       , Optional ByVal blnApproveScoreCardMSS As Boolean = False _
                                       , Optional ByVal blnSubmitScoreCardESS As Boolean = False _
                                       , Optional ByVal blnApproveUpdateProgressMSS As Boolean = False _
                                       , Optional ByVal blnMyAssessmentESS As Boolean = False _
                                       , Optional ByVal blnAssessEmployeeMSS As Boolean = False _
                                       , Optional ByVal blnMyCompetenceAssessmentESS As Boolean = False _
                                       , Optional ByVal blnAssessEmployeeCompetenceMSS As Boolean = False _
                                       , Optional ByVal blnReviewEmployeeAssessmentMSS As Boolean = False _
                                       , Optional ByVal blnReviewEmployeeCompetenceMSS As Boolean = False _
                                       , Optional ByVal blnApproveLeaveMSS As Boolean = False _
                                       , Optional ByVal blnApproveOTApplicationMSS As Boolean = False _
                                       , Optional ByVal blnApproveClaimExpenseMSS As Boolean = False _
                                       , Optional ByVal blnAssetDeclarationT2ESS As Boolean = False _
                                       , Optional ByVal blnNonDisclosureDeclarationESS As Boolean = False _
                                       , Optional ByVal blnApproveSalaryChangeMSS As Boolean = False _
                                       , Optional ByVal blnApprovePayslipPaymentMSS As Boolean = False _
                                       , Optional ByVal blnApproveLoanApplicationMSS As Boolean = False _
                                       , Optional ByVal blnApproveStaffRequisitionMSS As Boolean = False _
                                       , Optional ByVal blnApproveClaimRetirementMSS As Boolean = False _
                                       , Optional ByVal blnApproveBudgetTimesheetMSS As Boolean = False _
                                       , Optional ByVal blnApproveShortlistingCriteriaMSS As Boolean = False _
                                       , Optional ByVal blnApproveEligibleApplicantsMSS As Boolean = False _
                                       , Optional ByVal blnApproveMSS As Boolean = False _
                                       , Optional ByVal blnApproveCalibrationMSS As Boolean = False _
                                       , Optional ByVal blnMyTrainingFeedbackESS As Boolean = False _
                                       , Optional ByVal blnEvaluateEmployeeTrainingMSS As Boolean = False _
                                       ) As DataSet
        'Hemant (04 Sep 2021) -- [blnMyTrainingFeedbackESS,blnEvaluateEmployeeTrainingMSS]

        Dim dsList As New DataSet
        Dim StrQ As String = String.Empty
        Dim strTableEmpFields As String = ""
        Dim strDropQ As String = ""

        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            If intUserUnkid > 0 Then
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodEnd, dtPeriodEnd, , , strDatabaseName)
            If blnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intUserUnkid, intCompanyUnkid, intYearUnkId, strUserModeSetting, , , strORQueryForUserAccess)
            End If
            Call GetAdvanceFilterQry(xAdvanceJoinQry, dtPeriodEnd, strDatabaseName)
            End If

            Const _AssetDeclarationFromDate As String = "AssetDeclarationFromDate"
            Const _AssetDeclarationToDate As String = "AssetDeclarationToDate"
            Const _UnLockDaysAfterAssetDeclarationToDate As String = "EmpUnLockDaysAfterAssetDeclarationToDate"
            Const _NonDisclosureDeclarationFromDate As String = "NonDisclosureDeclarationFromDate"
            Const _NonDisclosureDeclarationToDate As String = "NonDisclosureDeclarationToDate"
            Const _LockDaysAfterNonDisclosureDeclarationToDate As String = "EmpLockDaysAfterNonDisclosureDeclarationToDate"
            'Hemant (09 Feb 2022) -- Start            
            'OLD-559(NMB) : Allow user to Submit Post Training Evaluation Form/After Training Evaluation Form before Completing Training
            Const _PostTrainingEvaluationBeforeCompleteTraining As String = "PostTrainingEvaluationBeforeCompleteTraining"
            'Hemant (09 Feb 2022) -- End

            'Pinkal (15-May-2023) -- Start
            ' (A1X-850) Tujijenge - Dashboard pending actions to show list of records after redirecting to the action page.
            Const _RoleBasedLoanApproval As String = "RoleBasedLoanApproval"
            'Pinkal (15-May-2023) -- End


            Dim dicConfigKeyValue As New Dictionary(Of String, String)
            Dim strKeys() As String = {_AssetDeclarationFromDate _
                                       , _AssetDeclarationToDate _
                                       , _UnLockDaysAfterAssetDeclarationToDate _
                                       , _NonDisclosureDeclarationFromDate _
                                       , _NonDisclosureDeclarationToDate _
                                       , _LockDaysAfterNonDisclosureDeclarationToDate _
                                       , _PostTrainingEvaluationBeforeCompleteTraining _
                                               , _RoleBasedLoanApproval _
                                       }

            'Pinkal (15-May-2023) -- (A1X-850) Tujijenge - Dashboard pending actions to show list of records after redirecting to the action page.
            'Hemant (09 Feb 2022) -- [_PostTrainingEvaluationBeforeCompleteTraining]

            Dim objConfig As New clsConfigOptions
            dicConfigKeyValue = objConfig.GetKeyValue(intCompanyUnkid, strKeys)

            Dim dtFinYearStart As Date = objConfig._CurrentDateAndTime
            Dim dtFinYearEnd As Date = objConfig._CurrentDateAndTime

            Dim objDataOperation As New clsDataOperation

            StrQ = "SELECT TOP 1 * FROM hrmsConfiguration..cffinancial_year_tran " & _
                    "WHERE cffinancial_year_tran.companyunkid = " & intCompanyUnkid & " " & _
                    "AND cffinancial_year_tran.yearunkid = " & intYearUnkId & " "

            Dim dsYear As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            If dsYear.Tables(0).Rows.Count > 0 Then
                dtFinYearStart = CDate(dsYear.Tables(0).Rows(0).Item("start_date"))
                dtFinYearEnd = CDate(dsYear.Tables(0).Rows(0).Item("end_date"))
            End If

            Dim intFirstPeriodId As Integer = 0
            Dim intFirstPeriodIdAssesment As Integer = 0
            If blnApprovePayslipPaymentMSS = True Then
                intFirstPeriodId = (New clsMasterData).getFirstPeriodID(enModuleReference.Payroll, intYearUnkId, enStatusType.OPEN)
            End If

            If blnApproveCalibrationMSS = True Then
                intFirstPeriodIdAssesment = (New clsMasterData).getFirstPeriodID(enModuleReference.Assessment, intYearUnkId, enStatusType.OPEN)
            End If

            StrQ = ""
            strDropQ = ""

            If intUserUnkid > 0 AndAlso (blnApproveSalaryChangeMSS = True OrElse blnApprovePayslipPaymentMSS = True) Then

                strDropQ &= "DROP TABLE #TableEmp "

                StrQ = "SELECT hremployee_master.employeeunkid "

                StrQ &= "INTO #TableEmp " & _
                        "FROM hremployee_master "


                'Pinkal (09-Aug-2021)-- Start
                'NMB New UI Enhancements.
                If xAdvanceJoinQry.Trim.Length > 0 Then
                    StrQ &= " " & xAdvanceJoinQry & " "
                End If
                'Pinkal (09-Aug-2021) -- End

                If xUACQry.Trim.Length > 0 Then
                    StrQ &= " " & xUACQry & " "
                End If

                If xDateJoinQry.Trim.Length > 0 Then
                    StrQ &= " " & xDateJoinQry & " "
                End If

                StrQ &= "WHERE 1 = 1 "

                If xDateFilterQry.Trim.Length > 0 Then
                    StrQ &= " " & xDateFilterQry & " "
                End If

                If strAdvanceFilter.Trim.Length > 0 Then
                    StrQ &= " AND " & strAdvanceFilter & " "
                End If
            End If

            If blnSetScoreCardESS = True OrElse blnApproveScoreCardMSS = True OrElse blnSubmitScoreCardESS = True OrElse blnApproveUpdateProgressMSS = True Then

                StrQ &= "DECLARE @Pid AS INT, @Dt AS Datetime " & _
                        "SELECT @Pid = periodunkid " & _
                             ", @Dt = CASE WHEN lockdaysafter > 0 THEN DATEADD(DAY, lockdaysafter, start_date) ELSE NULL END " & _
                        "FROM cfcommon_period_tran " & _
                        "WHERE cfcommon_period_tran.modulerefid = 5 " & _
                              "AND cfcommon_period_tran.isactive = 1 " & _
                              "AND cfcommon_period_tran.statusid = 1 " & _
                              "AND yearunkid IN ( " & _
                                                   "SELECT TOP 1 " & _
                                                       "yearunkid " & _
                                                   "FROM hrmsConfiguration..cffinancial_year_tran " & _
                                                   "WHERE database_name = " & _
                                                   "( " & _
                                                       "SELECT DB_NAME() " & _
                                                   ") " & _
                                                         "AND isclosed = 0 " & _
                                                         "AND companyunkid = @companyunkid " & _
                                               ") " & _
                        "ORDER BY end_date ASC "

            End If

            If blnMyAssessmentESS = True OrElse blnAssessEmployeeMSS = True OrElse blnMyCompetenceAssessmentESS = True OrElse blnAssessEmployeeCompetenceMSS = True OrElse blnReviewEmployeeAssessmentMSS = True OrElse blnReviewEmployeeCompetenceMSS = True Then

                StrQ &= "DECLARE @value AS NVARCHAR(10) " & _
                        "DECLARE @PidAssess AS INT " & _
                          ", @DtAssess AS Datetime " & _
                        "SELECT @PidAssess = periodunkid " & _
                            ", @DtAssess = CASE WHEN lockdaysbefore > 0 THEN DATEADD(DAY, - (lockdaysbefore), end_date) " & _
                                           "ELSE NULL END " & _
                        "FROM cfcommon_period_tran " & _
                        "WHERE modulerefid = 5 " & _
                          "AND isactive = 1 " & _
                          "AND yearunkid IN ( " & _
                                               "SELECT TOP 1 " & _
                                                   "yearunkid " & _
                                               "FROM hrmsConfiguration..cffinancial_year_tran " & _
                                               "WHERE database_name = " & _
                                               "( " & _
                                                   "SELECT DB_NAME() " & _
                                               ") " & _
                                                     "AND isclosed = 0 " & _
                                                     "AND companyunkid = @companyunkid  " & _
                                           ") " & _
                        "ORDER BY end_date ASC " & _
                        " " & _
                        "SET @value = '' " & _
                        "SELECT @value = key_value " & _
                        "FROM hrmsConfiguration..cfconfiguration " & _
                        "WHERE key_name = 'RunCompetenceAssessmentSeparately' " & _
                          "AND companyunkid = @companyunkid "

            End If

            If blnApproveStaffRequisitionMSS = True AndAlso intUserUnkid > 0 Then

                strDropQ &= "DROP TABLE #lowerpriority "

                StrQ &= "SELECT rcstaffrequisition_approver_mapping.userapproverunkid  " & _
                                 ", rcstaffrequisition_approver_mapping.allocationid " & _
                                 ", rcstaffrequisition_approver_mapping.allocationunkid " & _
                                 ", rcstaffrequisitionlevel_master.levelunkid " & _
                                 ", rcstaffrequisitionlevel_master.priority " & _
                                 ", ISNULL( " & _
                                   "( " & _
                                       "SELECT TOP 1 lvl.levelunkid " & _
                                       "FROM rcstaffrequisitionlevel_master AS lvl " & _
                                       "WHERE lvl.priority < rcstaffrequisitionlevel_master.priority " & _
                                       "ORDER BY lvl.priority DESC " & _
                                   ") " & _
                                 ", -1 ) AS lowerlevelunkid " & _
                            "INTO #lowerpriority " & _
                            "FROM rcstaffrequisition_approver_mapping " & _
                                "LEFT JOIN rcstaffrequisitionlevel_master ON rcstaffrequisition_approver_mapping.levelunkid = rcstaffrequisitionlevel_master.levelunkid " & _
                            "WHERE rcstaffrequisition_approver_mapping.isvoid = 0 " & _
                                  "AND rcstaffrequisitionlevel_master.isactive = 1 " & _
                                  "AND rcstaffrequisition_approver_mapping.userapproverunkid = @userunkid "

            End If

            'Hemant (04 Sep 2021) -- Start
            'ENHANCEMENT : OLD-465 - Dashboard Pending Task Enhancement - Training Evaluation.
            If blnMyTrainingFeedbackESS = True And intEmployeeUnkId > 0 Then
                strDropQ &= "DROP TABLE #MyTrainingFeedback "

                StrQ &= "DECLARE @DaysAfterTraining AS INT " & _
                        " SELECT @DaysAfterTraining = min(feedbacknodaysaftertrainingattended) from treval_group_master where isvoid = 0 and feedbackmode = " & clseval_group_master.enFeedBack.DAYSAFTERTRAINING & " " & _
                        " " & _
                        " SELECT * INTO #MyTrainingFeedback " & _
                            "   FROM ( " & _
                            "          SELECT " & _
                            "            COUNT(*) AS TotalCount " & _
                            "          FROM trtraining_request_master " & _
                            "          WHERE isvoid = 0 " & _
                            "                AND employeeunkid = @employeeunkid " & _
                            "                AND statusunkid = " & enTrainingRequestStatus.APPROVED & " AND ispretrainingfeedback_submitted = 0 " & _
                            "                AND (SELECT " & _
                            "                       COUNT(*) " & _
                            "                     FROM treval_question_master " & _
                            "                     JOIN treval_group_master ON treval_group_master.categoryid = treval_question_master.categoryid	AND treval_group_master.isvoid = 0 " & _
                            "                     WHERE treval_question_master.isvoid = 0	AND treval_group_master.feedbackmode = " & clseval_group_master.enFeedBack.PRETRAINING & ") > 0 " & _
                            "           UNION ALL " & _
                            "          SELECT " & _
                            "            COUNT(*) AS TotalCount " & _
                            "          FROM trtraining_request_master " & _
                            "          WHERE isvoid = 0 " & _
                            "                AND employeeunkid = @employeeunkid "
                'Hemant (09 Feb 2022) -- Start            
                'OLD-559(NMB) : Allow user to Submit Post Training Evaluation Form/After Training Evaluation Form before Completing Training
                If dicConfigKeyValue.ContainsKey(_PostTrainingEvaluationBeforeCompleteTraining) = True AndAlso CBool(dicConfigKeyValue.Item(_PostTrainingEvaluationBeforeCompleteTraining)) = True Then
                    StrQ &= "                AND isenroll_confirm = 1 "
                Else
                    'Hemant (09 Feb 2022) -- End
                    StrQ &= "                AND completed_statusunkid = " & enTrainingRequestStatus.APPROVED
                End If 'Hemant (09 Feb 2022)
                StrQ &= "                    AND isposttrainingfeedback_submitted = 0 " & _
                            "                AND (SELECT " & _
                            "                       COUNT(*) " & _
                            "                     FROM treval_question_master " & _
                            "                     JOIN treval_group_master ON treval_group_master.categoryid = treval_question_master.categoryid	AND treval_group_master.isvoid = 0 " & _
                            "                     WHERE treval_question_master.isvoid = 0	AND treval_group_master.feedbackmode = " & clseval_group_master.enFeedBack.POSTTRAINING & ") > 0 " & _
                            "           UNION ALL " & _
                            "          SELECT " & _
                            "            COUNT(*) AS TotalCount " & _
                            "          FROM trtraining_request_master " & _
                            "          WHERE isvoid = 0 " & _
                            "                AND employeeunkid = @employeeunkid " & _
                            "                AND completed_statusunkid = " & enTrainingRequestStatus.APPROVED & " " & _
                            "                AND isdaysafterfeedback_submitted = 0 AND completed_approval_date IS NOT NULL " & _
                            "                AND  ISNULL(CONVERT(CHAR(8),DATEADD(DAY,@DaysAfterTraining,completed_approval_date),112),'') <= ISNULL(CONVERT(CHAR(8),GETDATE(),112),'') " & _
                            "                AND (SELECT " & _
                            "                       COUNT(*) " & _
                            "                     FROM treval_question_master " & _
                            "                     JOIN treval_group_master ON treval_group_master.categoryid = treval_question_master.categoryid	AND treval_group_master.isvoid = 0 " & _
                            "                     WHERE treval_question_master.isvoid = 0	AND treval_group_master.feedbackmode = " & clseval_group_master.enFeedBack.DAYSAFTERTRAINING & ") > 0 " & _
                            "           ) as X  "

            End If
            'Hemant (04 Sep 2021) -- End

            '***************************************************************
            If blnSetScoreCardESS = True AndAlso intEmployeeUnkId > 0 Then

                StrQ &= "SELECT COUNT(*) AS TotalCount, " & enPendingTask.SetScoreCardESS & " AS Id, @SetScoreCardESS AS Name " & _
                                "FROM hremployee_master " & _
                                "WHERE employeeunkid NOT IN ( " & _
                                                               "SELECT employeeunkid " & _
                                                               "FROM hrassess_empfield1_master " & _
                                                               "WHERE isvoid = 0 " & _
                                                                     "AND periodunkid = @Pid " & _
                                                                     "AND employeeunkid = @employeeunkid " & _
                                                           ") " & _
                                      "AND employeeunkid = @employeeunkid " & _
                                      "AND (@Dt IS NOT NULL) " & _
                                      "AND (@Dt > (SELECT (@Date))) " & _
                            "UNION " & _
                            "SELECT COUNT(*) AS TotalCount, " & enPendingTask.SetScoreCardESS & " AS Id, @SetScoreCardESS AS Name " & _
                            "FROM hremployee_master " & _
                            "WHERE employeeunkid NOT IN ( " & _
                                                           "SELECT employeeunkid " & _
                                                           "FROM hrassess_empfield1_master " & _
                                                           "WHERE isvoid = 0 " & _
                                                                 "AND periodunkid = @Pid " & _
                                                                 "AND employeeunkid = @employeeunkid " & _
                                                       ") " & _
                                  "AND employeeunkid = @employeeunkid " & _
                                  "AND NOT (@Dt IS NOT NULL) "

            Else
                StrQ &= "SELECT COUNT(*) AS TotalCount, " & enPendingTask.SetScoreCardESS & " AS Id, @SetScoreCardESS AS Name WHERE 1 = 2 "
            End If

            If blnApproveScoreCardMSS = True AndAlso intUserUnkid > 0 Then

                StrQ &= "UNION " & _
                        "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ApproveScoreCardMSS & " AS Id, @ApproveScoreCardMSS AS Name " & _
                        "FROM " & _
                        "( " & _
                            "SELECT statustypeid " & _
                                 ", ROW_NUMBER() OVER (PARTITION BY S.employeeunkid, S.periodunkid ORDER BY S.status_date DESC ) AS RNO " & _
                            "FROM hrassess_empstatus_tran AS S " & _
                                "JOIN " & _
                                "( " & _
                                    "SELECT T.employeeunkid " & _
                                         ", M.userunkid " & _
                                    "FROM hrassessor_tran AS T " & _
                                        "JOIN hrassessor_master AS A ON A.assessormasterunkid = T.assessormasterunkid " & _
                                        "JOIN hrapprover_usermapping AS M ON M.approverunkid = A.assessormasterunkid " & _
                                    "WHERE M.usertypeid = 2 " & _
                                          "AND A.isvoid = 0 " & _
                                          "AND T.isvoid = 0 " & _
                                          "AND A.visibletypeid = 1 " & _
                                          "AND T.visibletypeid = 1 " & _
                                          "AND M.userunkid = @userunkid " & _
                                ") AS U " & _
                                    "ON S.employeeunkid = U.employeeunkid " & _
                                       "AND S.periodunkid = @Pid " & _
                        ") AS X " & _
                        "WHERE X.RNO = 1 " & _
                              "AND X.statustypeid IN ( 1 ) "

                objDataOperation.AddParameter("@ApproveScoreCardMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 47, "Approve Score Card"))
            End If

            If blnSubmitScoreCardESS = True AndAlso intEmployeeUnkId > 0 Then

                StrQ &= "UNION " & _
                       "SELECT COUNT(*) AS TotalCount, " & enPendingTask.SubmitScoreCardESS & " AS Id, @SubmitScoreCardESS " & _
                       "FROM " & _
                       "( " & _
                           "SELECT employeeunkid " & _
                                ", statustypeid " & _
                                ", ROW_NUMBER() OVER (PARTITION BY employeeunkid, periodunkid ORDER BY status_date DESC) AS RNO " & _
                           "FROM hrassess_empstatus_tran " & _
                           "WHERE employeeunkid = @employeeunkid " & _
                                 "AND periodunkid = @Pid " & _
                       ") AS A " & _
                       "WHERE A.RNO = 1 " & _
                             "AND A.statustypeid IN ( 4, 3 ) "

                objDataOperation.AddParameter("@SubmitScoreCardESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 48, "Submit Score Card"))
            End If

            If blnApproveUpdateProgressMSS = True AndAlso intUserUnkid > 0 Then

                StrQ &= "UNION " & _
                       "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ApproveUpdateProgressMSS & " AS Id, @ApproveUpdateProgressMSS " & _
                        "FROM hrassess_empupdate_tran AS S " & _
                            "JOIN " & _
                            "( " & _
                                "SELECT T.employeeunkid " & _
                                     ", M.userunkid " & _
                                "FROM hrassessor_tran AS T " & _
                                    "JOIN hrassessor_master AS A " & _
                                        "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                    "JOIN hrapprover_usermapping AS M " & _
                                        "ON M.approverunkid = A.assessormasterunkid " & _
                                "WHERE M.usertypeid = 2 " & _
                                      "AND A.isvoid = 0 " & _
                                      "AND T.isvoid = 0 " & _
                                      "AND A.visibletypeid = 1 " & _
                                      "AND T.visibletypeid = 1 " & _
                                      "AND M.userunkid = @userunkid " & _
                            ") AS U " & _
                                "ON S.employeeunkid = U.employeeunkid " & _
                                   "AND S.periodunkid = @Pid " & _
                                   "AND S.approvalstatusunkid IN ( 1 ) "

                objDataOperation.AddParameter("@ApproveUpdateProgressMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 49, "Approve Update Progress"))
            End If

            If blnMyAssessmentESS = True AndAlso intEmployeeUnkId > 0 Then

                StrQ &= "UNION " & _
                        "SELECT COUNT(*) AS TotalCount, " & enPendingTask.MyAssessmentESS & " AS Id, @MyAssessmentESS " & _
                        "FROM hremployee_master " & _
                        "WHERE employeeunkid NOT IN ( " & _
                                                       "SELECT selfemployeeunkid " & _
                                                       "FROM hrevaluation_analysis_master " & _
                                                       "WHERE isvoid = 0 " & _
                                                             "AND periodunkid = @PidAssess " & _
                                                             "AND employeeunkid = @employeeunkid " & _
                                                             "AND evaltypeid = 2 " & _
                                                             "AND assessmodeid = 1 " & _
                                                             "AND iscommitted = 0 " & _
                                                             "AND isvoid = 0 " & _
                                                   ") " & _
                              "AND employeeunkid = @employeeunkid " & _
                              "AND LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                              "AND (@DtAssess IS NOT NULL) " & _
                              "AND (@DtAssess >= (SELECT (@Date))) " & _
                              "UNION " & _
                              "SELECT COUNT(*) AS TotalCount, " & enPendingTask.MyAssessmentESS & " AS Id, @MyAssessmentESS " & _
                              "FROM hremployee_master " & _
                              "WHERE employeeunkid NOT IN ( " & _
                                                             "SELECT selfemployeeunkid " & _
                                                             "FROM hrevaluation_analysis_master " & _
                                                             "WHERE isvoid = 0 " & _
                                                                   "AND periodunkid = @PidAssess " & _
                                                                   "AND employeeunkid = @employeeunkid " & _
                                                                   "AND evaltypeid = 2 " & _
                                                                   "AND assessmodeid = 1 " & _
                                                                   "AND iscommitted = 0 " & _
                                                                   "AND isvoid = 0 " & _
                                                         ") " & _
                               "AND employeeunkid = @employeeunkid " & _
                               "AND LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                               "AND NOT (@DtAssess IS NOT NULL) " & _
                               "UNION " & _
                                "SELECT COUNT(*) AS TotalCount, " & enPendingTask.MyAssessmentESS & " AS Id, @MyAssessmentESS " & _
                                "FROM hremployee_master " & _
                                "WHERE employeeunkid NOT IN ( " & _
                                                           "SELECT selfemployeeunkid " & _
                                                           "FROM hrevaluation_analysis_master " & _
                                                           "WHERE isvoid = 0 " & _
                                                                 "AND periodunkid = @PidAssess " & _
                                                                 "AND employeeunkid = @employeeunkid " & _
                                                                 "AND evaltypeid = 1 " & _
                                                                 "AND assessmodeid = 1 " & _
                                                                 "AND iscommitted = 0 " & _
                                                                 "AND isvoid = 0 " & _
                                                       ") " & _
                                "AND employeeunkid = @employeeunkid " & _
                                "AND NOT (LEN(@value) > 0 AND CAST(@value AS BIT) = 1) " & _
                                "AND (@DtAssess IS NOT NULL) " & _
                                "AND (@DtAssess >= (SELECT (@Date))) " & _
                                "UNION " & _
                                "SELECT COUNT(*) AS TotalCount, " & enPendingTask.MyAssessmentESS & " AS Id, @MyAssessmentESS " & _
                                "FROM hremployee_master " & _
                                "WHERE employeeunkid NOT IN ( " & _
                                                           "SELECT selfemployeeunkid " & _
                                                           "FROM hrevaluation_analysis_master " & _
                                                           "WHERE isvoid = 0 " & _
                                                                 "AND periodunkid = @PidAssess " & _
                                                                 "AND employeeunkid = @employeeunkid " & _
                                                                 "AND evaltypeid = 1 " & _
                                                                 "AND assessmodeid = 1 " & _
                                                                 "AND iscommitted = 0 " & _
                                                                 "AND isvoid = 0 " & _
                                                       ") " & _
                                  "AND employeeunkid = @employeeunkid " & _
                                  "AND NOT (LEN(@value) > 0 AND CAST(@value AS BIT) = 1) " & _
                                  "AND NOT (@DtAssess IS NOT NULL) "

                objDataOperation.AddParameter("@MyAssessmentESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 50, "My Assessment"))
            End If

            If blnMyCompetenceAssessmentESS = True AndAlso intEmployeeUnkId > 0 Then

                StrQ &= "UNION " & _
                        "SELECT COUNT(*) AS TotalCount, " & enPendingTask.MyCompetenceAssessmentESS & " AS Id, @MyCompetenceAssessmentESS " & _
                        "FROM hremployee_master " & _
                        "WHERE employeeunkid NOT IN ( " & _
                                                       "SELECT selfemployeeunkid " & _
                                                       "FROM hrevaluation_analysis_master " & _
                                                       "WHERE isvoid = 0 " & _
                                                             "AND periodunkid = @PidAssess " & _
                                                             "AND employeeunkid = @employeeunkid " & _
                                                             "AND evaltypeid = 3 " & _
                                                             "AND assessmodeid = 1 " & _
                                                             "AND iscommitted = 0 " & _
                                                             "AND isvoid = 0 " & _
                                                   ") " & _
                          "AND employeeunkid = @employeeunkid " & _
                          "AND LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                          "AND (@DtAssess IS NOT NULL) " & _
                          "AND (@DtAssess >= (SELECT (@Date))) " & _
                          "UNION " & _
                          "SELECT COUNT(*) AS TotalCount, " & enPendingTask.MyCompetenceAssessmentESS & " AS Id, @MyCompetenceAssessmentESS " & _
                          "FROM hremployee_master " & _
                          "WHERE employeeunkid NOT IN ( " & _
                                                         "SELECT selfemployeeunkid " & _
                                                         "FROM hrevaluation_analysis_master " & _
                                                         "WHERE isvoid = 0 " & _
                                                               "AND periodunkid = @PidAssess " & _
                                                               "AND employeeunkid = @employeeunkid " & _
                                                               "AND evaltypeid = 3 " & _
                                                               "AND assessmodeid = 1 " & _
                                                               "AND iscommitted = 0 " & _
                                                               "AND isvoid = 0 " & _
                                                     ") " & _
                          "AND employeeunkid = @employeeunkid " & _
                          "AND LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                          "AND NOT (@DtAssess IS NOT NULL) "

                objDataOperation.AddParameter("@MyCompetenceAssessmentESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 51, "My Competence Assessment"))
            End If

            If blnAssessEmployeeMSS = True AndAlso intUserUnkid > 0 Then

                StrQ &= "UNION " & _
                        "SELECT COUNT(*) AS TotalCount, " & enPendingTask.AssessEmployeeMSS & " AS Id, @AssessEmployeeMSS " & _
                        "FROM hrevaluation_analysis_master " & _
                        "WHERE assessedemployeeunkid NOT IN ( " & _
                                                               "SELECT selfemployeeunkid " & _
                                                               "FROM hrevaluation_analysis_master " & _
                                                               "WHERE selfemployeeunkid IN ( " & _
                                                                                              "SELECT T.employeeunkid " & _
                                                                                              "FROM hrassessor_tran AS T " & _
                                                                                                  "JOIN hrassessor_master AS A " & _
                                                                                                      "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                                                                                  "JOIN hrapprover_usermapping AS M " & _
                                                                                                      "ON M.approverunkid = A.assessormasterunkid " & _
                                                                                              "WHERE M.usertypeid = 2 " & _
                                                                                                    "AND A.isvoid = 0 " & _
                                                                                                    "AND T.isvoid = 0 " & _
                                                                                                    "AND A.visibletypeid = 1 " & _
                                                                                                    "AND T.visibletypeid = 1 " & _
                                                                                                    "AND M.userunkid = @userunkid " & _
                                                                                          ") " & _
                                                                     "AND periodunkid = @PidAssess " & _
                                                                     "AND isvoid = 0 " & _
                                                                     "AND iscommitted = 1 " & _
                                                                     "AND evaltypeid = 2 " & _
                                                                     "AND assessmodeid = 1 " & _
                                                           ") " & _
                              "AND periodunkid = @PidAssess " & _
                              "AND isvoid = 0 " & _
                              "AND iscommitted = 1 " & _
                              "AND evaltypeid = 2 " & _
                              "AND assessmodeid = 2 " & _
                              "AND LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                              "AND (@DtAssess IS NOT NULL) " & _
                              "AND (@DtAssess >= (SELECT (@Date))) " & _
                              "UNION " & _
                                "SELECT COUNT(*) AS TotalCount, " & enPendingTask.AssessEmployeeMSS & " AS Id, @AssessEmployeeMSS " & _
                                "FROM hrevaluation_analysis_master " & _
                                "WHERE assessedemployeeunkid NOT IN ( " & _
                                                                       "SELECT selfemployeeunkid " & _
                                                                       "FROM hrevaluation_analysis_master " & _
                                                                       "WHERE selfemployeeunkid IN ( " & _
                                                                                                      "SELECT T.employeeunkid " & _
                                                                                                      "FROM hrassessor_tran AS T " & _
                                                                                                          "JOIN hrassessor_master AS A " & _
                                                                                                              "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                                                                                          "JOIN hrapprover_usermapping AS M " & _
                                                                                                              "ON M.approverunkid = A.assessormasterunkid " & _
                                                                                                      "WHERE M.usertypeid = 2 " & _
                                                                                                            "AND A.isvoid = 0 " & _
                                                                                                            "AND T.isvoid = 0 " & _
                                                                                                            "AND A.visibletypeid = 1 " & _
                                                                                                            "AND T.visibletypeid = 1 " & _
                                                                                                            "AND M.userunkid = @userunkid " & _
                                                                                                  ") " & _
                                                                             "AND periodunkid = @PidAssess " & _
                                                                             "AND isvoid = 0 " & _
                                                                             "AND iscommitted = 1 " & _
                                                                             "AND evaltypeid = 2 " & _
                                                                             "AND assessmodeid = 1 " & _
                                                                   ") " & _
                                      "AND periodunkid = @PidAssess " & _
                                      "AND isvoid = 0 " & _
                                      "AND iscommitted = 1 " & _
                                      "AND evaltypeid = 2 " & _
                                      "AND assessmodeid = 2 " & _
                                      "AND NOT (@DtAssess IS NOT NULL) " & _
                                      "AND LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                                "UNION " & _
                                "SELECT COUNT(*) AS TotalCount, " & enPendingTask.AssessEmployeeMSS & " AS Id, @AssessEmployeeMSS " & _
                                "FROM hrevaluation_analysis_master " & _
                                "WHERE assessedemployeeunkid NOT IN ( " & _
                                                                       "SELECT selfemployeeunkid " & _
                                                                       "FROM hrevaluation_analysis_master " & _
                                                                       "WHERE selfemployeeunkid IN ( " & _
                                                                                                      "SELECT T.employeeunkid " & _
                                                                                                      "FROM hrassessor_tran AS T " & _
                                                                                                          "JOIN hrassessor_master AS A " & _
                                                                                                              "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                                                                                          "JOIN hrapprover_usermapping AS M " & _
                                                                                                              "ON M.approverunkid = A.assessormasterunkid " & _
                                                                                                      "WHERE M.usertypeid = 2 " & _
                                                                                                            "AND A.isvoid = 0 " & _
                                                                                                            "AND T.isvoid = 0 " & _
                                                                                                            "AND A.visibletypeid = 1 " & _
                                                                                                            "AND T.visibletypeid = 1 " & _
                                                                                                            "AND M.userunkid = @userunkid " & _
                                                                                                  ") " & _
                                                                             "AND periodunkid = @PidAssess " & _
                                                                             "AND isvoid = 0 " & _
                                                                             "AND iscommitted = 1 " & _
                                                                             "AND evaltypeid = 1 " & _
                                                                             "AND assessmodeid = 1 " & _
                                                                   ") " & _
                                      "AND periodunkid = @PidAssess " & _
                                      "AND isvoid = 0 " & _
                                      "AND iscommitted = 1 " & _
                                      "AND evaltypeid = 1 " & _
                                      "AND assessmodeid = 2 " & _
                                      "AND NOT(LEN(@value) > 0 AND CAST(@value AS BIT) = 1) " & _
                                      "AND (@DtAssess IS NOT NULL) " & _
                                      "AND (@DtAssess >= (SELECT (@Date))) " & _
                                "UNION " & _
                                "SELECT COUNT(*) AS TotalCount, " & enPendingTask.AssessEmployeeMSS & " AS Id, @AssessEmployeeMSS " & _
                                "FROM hrevaluation_analysis_master " & _
                                "WHERE assessedemployeeunkid NOT IN ( " & _
                                                                       "SELECT selfemployeeunkid " & _
                                                                       "FROM hrevaluation_analysis_master " & _
                                                                       "WHERE selfemployeeunkid IN ( " & _
                                                                                                      "SELECT T.employeeunkid " & _
                                                                                                      "FROM hrassessor_tran AS T " & _
                                                                                                          "JOIN hrassessor_master AS A " & _
                                                                                                              "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                                                                                          "JOIN hrapprover_usermapping AS M " & _
                                                                                                              "ON M.approverunkid = A.assessormasterunkid " & _
                                                                                                      "WHERE M.usertypeid = 2 " & _
                                                                                                            "AND A.isvoid = 0 " & _
                                                                                                            "AND T.isvoid = 0 " & _
                                                                                                            "AND A.visibletypeid = 1 " & _
                                                                                                            "AND T.visibletypeid = 1 " & _
                                                                                                            "AND M.userunkid = @userunkid " & _
                                                                                                  ") " & _
                                                                             "AND periodunkid = @PidAssess " & _
                                                                             "AND isvoid = 0 " & _
                                                                             "AND iscommitted = 1 " & _
                                                                             "AND evaltypeid = 1 " & _
                                                                             "AND assessmodeid = 1 " & _
                                                                   ") " & _
                                      "AND periodunkid = @PidAssess " & _
                                      "AND isvoid = 0 " & _
                                      "AND iscommitted = 1 " & _
                                      "AND evaltypeid = 1 " & _
                                      "AND assessmodeid = 2 " & _
                                      "AND NOT(LEN(@value) > 0 AND CAST(@value AS BIT) = 1) " & _
                                      "AND NOT (@DtAssess IS NOT NULL) "

                objDataOperation.AddParameter("@AssessEmployeeMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 52, "Assess Employee"))
            End If

            If blnAssessEmployeeCompetenceMSS = True AndAlso intUserUnkid > 0 Then

                StrQ &= "UNION " & _
                        "SELECT COUNT(*) AS TotalCount, " & enPendingTask.AssessEmployeeCompetenceMSS & " AS Id, @AssessEmployeeCompetenceMSS " & _
                        "FROM hrevaluation_analysis_master " & _
                        "WHERE assessedemployeeunkid NOT IN ( " & _
                                                               "SELECT selfemployeeunkid " & _
                                                               "FROM hrevaluation_analysis_master " & _
                                                               "WHERE selfemployeeunkid IN ( " & _
                                                                                              "SELECT T.employeeunkid " & _
                                                                                              "FROM hrassessor_tran AS T " & _
                                                                                                  "JOIN hrassessor_master AS A " & _
                                                                                                      "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                                                                                  "JOIN hrapprover_usermapping AS M " & _
                                                                                                      "ON M.approverunkid = A.assessormasterunkid " & _
                                                                                              "WHERE M.usertypeid = 2 " & _
                                                                                                    "AND A.isvoid = 0 " & _
                                                                                                    "AND T.isvoid = 0 " & _
                                                                                                    "AND A.visibletypeid = 1 " & _
                                                                                                    "AND T.visibletypeid = 1 " & _
                                                                                                    "AND M.userunkid = @userunkid " & _
                                                                                          ") " & _
                                                                     "AND periodunkid = @PidAssess " & _
                                                                     "AND isvoid = 0 " & _
                                                                     "AND iscommitted = 1 " & _
                                                                     "AND evaltypeid = 3 " & _
                                                                     "AND assessmodeid = 1 " & _
                                                           ") " & _
                              "AND periodunkid = @PidAssess " & _
                              "AND isvoid = 0 " & _
                              "AND iscommitted = 1 " & _
                              "AND evaltypeid = 3 " & _
                              "AND assessmodeid = 2 " & _
                              "AND LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                              "AND (@DtAssess IS NOT NULL) " & _
                              "AND (@DtAssess >= (SELECT (@Date))) " & _
                            "UNION " & _
                            "SELECT COUNT(*) AS TotalCount, " & enPendingTask.AssessEmployeeCompetenceMSS & " AS Id, @AssessEmployeeCompetenceMSS " & _
                            "FROM hrevaluation_analysis_master " & _
                            "WHERE assessedemployeeunkid NOT IN ( " & _
                                                                   "SELECT selfemployeeunkid " & _
                                                                   "FROM hrevaluation_analysis_master " & _
                                                                   "WHERE selfemployeeunkid IN ( " & _
                                                                                                  "SELECT T.employeeunkid " & _
                                                                                                  "FROM hrassessor_tran AS T " & _
                                                                                                      "JOIN hrassessor_master AS A " & _
                                                                                                          "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                                                                                      "JOIN hrapprover_usermapping AS M " & _
                                                                                                          "ON M.approverunkid = A.assessormasterunkid " & _
                                                                                                  "WHERE M.usertypeid = 2 " & _
                                                                                                        "AND A.isvoid = 0 " & _
                                                                                                        "AND T.isvoid = 0 " & _
                                                                                                        "AND A.visibletypeid = 1 " & _
                                                                                                        "AND T.visibletypeid = 1 " & _
                                                                                                        "AND M.userunkid = @userunkid " & _
                                                                                              ") " & _
                                                                         "AND periodunkid = @PidAssess " & _
                                                                         "AND isvoid = 0 " & _
                                                                         "AND iscommitted = 1 " & _
                                                                         "AND evaltypeid = 3 " & _
                                                                         "AND assessmodeid = 1 " & _
                                                               ") " & _
                                  "AND periodunkid = @PidAssess " & _
                                  "AND isvoid = 0 " & _
                                  "AND iscommitted = 1 " & _
                                  "AND evaltypeid = 3 " & _
                                  "AND assessmodeid = 2 " & _
                                  "AND LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                                  "AND NOT (@DtAssess IS NOT NULL) "

                objDataOperation.AddParameter("@AssessEmployeeCompetenceMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 53, "Assess Employee Competence"))
            End If

            If blnReviewEmployeeAssessmentMSS = True AndAlso intUserUnkid > 0 Then

                StrQ &= "UNION " & _
                                    "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ReviewEmployeeAssessmentMSS & " AS Id, @ReviewEmployeeAssessmentMSS " & _
                                    "FROM hrevaluation_analysis_master " & _
                                    "WHERE assessedemployeeunkid NOT IN ( " & _
                                                                           "SELECT assessedemployeeunkid " & _
                                                                           "FROM hrevaluation_analysis_master " & _
                                                                           "WHERE assessedemployeeunkid IN ( " & _
                                                                                                              "SELECT T.employeeunkid " & _
                                                                                                              "FROM hrassessor_tran AS T " & _
                                                                                                                  "JOIN hrassessor_master AS A " & _
                                                                                                                      "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                                                                                                  "JOIN hrapprover_usermapping AS M " & _
                                                                                                                      "ON M.approverunkid = A.assessormasterunkid " & _
                                                                                                              "WHERE M.usertypeid = 2 " & _
                                                                                                                    "AND A.isvoid = 0 " & _
                                                                                                                    "AND T.isvoid = 0 " & _
                                                                                                                    "AND A.visibletypeid = 1 " & _
                                                                                                                    "AND T.visibletypeid = 1 " & _
                                                                                                                    "AND M.userunkid = @userunkid " & _
                                                                                                                    "AND A.isreviewer = 1 " & _
                                                                                                          ") " & _
                                                                                 "AND periodunkid = @PidAssess " & _
                                                                                 "AND isvoid = 0 " & _
                                                                                 "AND iscommitted = 1 " & _
                                                                                 "AND evaltypeid = 2 " & _
                                                                                 "AND assessmodeid = 2 " & _
                                                                       ") " & _
                                          "AND periodunkid = @PidAssess " & _
                                          "AND isvoid = 0 " & _
                                          "AND iscommitted = 1 " & _
                                          "AND evaltypeid = 2 " & _
                                          "AND assessmodeid = 3 " & _
                                          "AND LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                                          "AND (@DtAssess >= (SELECT (@Date))) " & _
                                          "AND (@DtAssess IS NOT NULL) " & _
                                "UNION " & _
                                "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ReviewEmployeeAssessmentMSS & " AS Id, @ReviewEmployeeAssessmentMSS " & _
                                "FROM hrevaluation_analysis_master " & _
                                "WHERE assessedemployeeunkid NOT IN ( " & _
                                                                       "SELECT assessedemployeeunkid " & _
                                                                       "FROM hrevaluation_analysis_master " & _
                                                                       "WHERE assessedemployeeunkid IN ( " & _
                                                                                                          "SELECT T.employeeunkid " & _
                                                                                                          "FROM hrassessor_tran AS T " & _
                                                                                                              "JOIN hrassessor_master AS A " & _
                                                                                                                  "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                                                                                              "JOIN hrapprover_usermapping AS M " & _
                                                                                                                  "ON M.approverunkid = A.assessormasterunkid " & _
                                                                                                          "WHERE M.usertypeid = 2 " & _
                                                                                                                "AND A.isvoid = 0 " & _
                                                                                                                "AND T.isvoid = 0 " & _
                                                                                                                "AND A.visibletypeid = 1 " & _
                                                                                                                "AND T.visibletypeid = 1 " & _
                                                                                                                "AND M.userunkid = @userunkid " & _
                                                                                                                "AND A.isreviewer = 1 " & _
                                                                                                      ") " & _
                                                                             "AND periodunkid = @PidAssess " & _
                                                                             "AND isvoid = 0 " & _
                                                                             "AND iscommitted = 1 " & _
                                                                             "AND evaltypeid = 2 " & _
                                                                             "AND assessmodeid = 2 " & _
                                                                   ") " & _
                                      "AND periodunkid = @PidAssess " & _
                                      "AND isvoid = 0 " & _
                                      "AND iscommitted = 1 " & _
                                      "AND evaltypeid = 2 " & _
                                      "AND assessmodeid = 3 " & _
                                      "AND LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                                      "AND NOT (@DtAssess IS NOT NULL) " & _
                            "UNION " & _
                                    "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ReviewEmployeeAssessmentMSS & " AS Id, @ReviewEmployeeAssessmentMSS " & _
                                    "FROM hrevaluation_analysis_master " & _
                                    "WHERE assessedemployeeunkid NOT IN ( " & _
                                                                           "SELECT assessedemployeeunkid " & _
                                                                           "FROM hrevaluation_analysis_master " & _
                                                                           "WHERE assessedemployeeunkid IN ( " & _
                                                                                                              "SELECT T.employeeunkid " & _
                                                                                                              "FROM hrassessor_tran AS T " & _
                                                                                                                  "JOIN hrassessor_master AS A " & _
                                                                                                                      "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                                                                                                  "JOIN hrapprover_usermapping AS M " & _
                                                                                                                      "ON M.approverunkid = A.assessormasterunkid " & _
                                                                                                              "WHERE M.usertypeid = 2 " & _
                                                                                                                    "AND A.isvoid = 0 " & _
                                                                                                                    "AND T.isvoid = 0 " & _
                                                                                                                    "AND A.visibletypeid = 1 " & _
                                                                                                                    "AND T.visibletypeid = 1 " & _
                                                                                                                    "AND M.userunkid = @userunkid " & _
                                                                                                                    "AND A.isreviewer = 1 " & _
                                                                                                          ") " & _
                                                                                 "AND periodunkid = @PidAssess " & _
                                                                                 "AND isvoid = 0 " & _
                                                                                 "AND iscommitted = 1 " & _
                                                                                 "AND evaltypeid = 1 " & _
                                                                                 "AND assessmodeid = 2 " & _
                                                                       ") " & _
                                          "AND periodunkid = @PidAssess " & _
                                          "AND isvoid = 0 " & _
                                          "AND iscommitted = 1 " & _
                                          "AND evaltypeid = 1 " & _
                                          "AND assessmodeid = 3 " & _
                                          "AND NOT LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                                          "AND (@DtAssess >= (SELECT (@Date))) " & _
                                          "AND (@DtAssess IS NOT NULL) " & _
                                "UNION " & _
                                "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ReviewEmployeeAssessmentMSS & " AS Id, @ReviewEmployeeAssessmentMSS " & _
                                "FROM hrevaluation_analysis_master " & _
                                "WHERE assessedemployeeunkid NOT IN ( " & _
                                                                       "SELECT assessedemployeeunkid " & _
                                                                       "FROM hrevaluation_analysis_master " & _
                                                                       "WHERE assessedemployeeunkid IN ( " & _
                                                                                                          "SELECT T.employeeunkid " & _
                                                                                                          "FROM hrassessor_tran AS T " & _
                                                                                                              "JOIN hrassessor_master AS A " & _
                                                                                                                  "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                                                                                              "JOIN hrapprover_usermapping AS M " & _
                                                                                                                  "ON M.approverunkid = A.assessormasterunkid " & _
                                                                                                          "WHERE M.usertypeid = 2 " & _
                                                                                                                "AND A.isvoid = 0 " & _
                                                                                                                "AND T.isvoid = 0 " & _
                                                                                                                "AND A.visibletypeid = 1 " & _
                                                                                                                "AND T.visibletypeid = 1 " & _
                                                                                                                "AND M.userunkid = @userunkid " & _
                                                                                                                "AND A.isreviewer = 1 " & _
                                                                                                      ") " & _
                                                                             "AND periodunkid = @PidAssess " & _
                                                                             "AND isvoid = 0 " & _
                                                                             "AND iscommitted = 1 " & _
                                                                             "AND evaltypeid = 1 " & _
                                                                             "AND assessmodeid = 2 " & _
                                                                   ") " & _
                                      "AND periodunkid = @PidAssess " & _
                                      "AND isvoid = 0 " & _
                                      "AND iscommitted = 1 " & _
                                      "AND evaltypeid = 1 " & _
                                      "AND assessmodeid = 3 " & _
                                      "AND NOT LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                                      "AND NOT (@DtAssess IS NOT NULL) "

                objDataOperation.AddParameter("@ReviewEmployeeAssessmentMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 54, "Review Employee Assessment"))
            End If

            If blnReviewEmployeeCompetenceMSS = True AndAlso intUserUnkid > 0 Then

                StrQ &= "UNION " & _
                                    "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ReviewEmployeeCompetenceMSS & " AS Id, @ReviewEmployeeCompetenceMSS " & _
                                    "FROM hrevaluation_analysis_master " & _
                                    "WHERE assessedemployeeunkid NOT IN ( " & _
                                                                           "SELECT assessedemployeeunkid " & _
                                                                           "FROM hrevaluation_analysis_master " & _
                                                                           "WHERE assessedemployeeunkid IN ( " & _
                                                                                                              "SELECT T.employeeunkid " & _
                                                                                                              "FROM hrassessor_tran AS T " & _
                                                                                                                  "JOIN hrassessor_master AS A " & _
                                                                                                                      "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                                                                                                  "JOIN hrapprover_usermapping AS M " & _
                                                                                                                      "ON M.approverunkid = A.assessormasterunkid " & _
                                                                                                              "WHERE M.usertypeid = 2 " & _
                                                                                                                    "AND A.isvoid = 0 " & _
                                                                                                                    "AND T.isvoid = 0 " & _
                                                                                                                    "AND A.visibletypeid = 1 " & _
                                                                                                                    "AND T.visibletypeid = 1 " & _
                                                                                                                    "AND M.userunkid = @userunkid " & _
                                                                                                                    "AND A.isreviewer = 1 " & _
                                                                                                          ") " & _
                                                                                 "AND periodunkid = @PidAssess " & _
                                                                                 "AND isvoid = 0 " & _
                                                                                 "AND iscommitted = 1 " & _
                                                                                 "AND evaltypeid = 3 " & _
                                                                                 "AND assessmodeid = 2 " & _
                                                                       ") " & _
                                          "AND periodunkid = @PidAssess " & _
                                          "AND isvoid = 0 " & _
                                          "AND iscommitted = 1 " & _
                                          "AND evaltypeid = 3 " & _
                                          "AND assessmodeid = 3 " & _
                                          "AND LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                                          "AND (@DtAssess IS NOT NULL) " & _
                                          "AND (@DtAssess >= (SELECT (@Date))) " & _
                                "UNION " & _
                                "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ReviewEmployeeCompetenceMSS & " AS Id, @ReviewEmployeeCompetenceMSS " & _
                                "FROM hrevaluation_analysis_master " & _
                                "WHERE assessedemployeeunkid NOT IN ( " & _
                                                                       "SELECT assessedemployeeunkid " & _
                                                                       "FROM hrevaluation_analysis_master " & _
                                                                       "WHERE assessedemployeeunkid IN ( " & _
                                                                                                          "SELECT T.employeeunkid " & _
                                                                                                          "FROM hrassessor_tran AS T " & _
                                                                                                              "JOIN hrassessor_master AS A " & _
                                                                                                                  "ON A.assessormasterunkid = T.assessormasterunkid " & _
                                                                                                              "JOIN hrapprover_usermapping AS M " & _
                                                                                                                  "ON M.approverunkid = A.assessormasterunkid " & _
                                                                                                          "WHERE M.usertypeid = 2 " & _
                                                                                                                "AND A.isvoid = 0 " & _
                                                                                                                "AND T.isvoid = 0 " & _
                                                                                                                "AND A.visibletypeid = 1 " & _
                                                                                                                "AND T.visibletypeid = 1 " & _
                                                                                                                "AND M.userunkid = @userunkid " & _
                                                                                                                "AND A.isreviewer = 1 " & _
                                                                                                      ") " & _
                                                                             "AND periodunkid = @PidAssess " & _
                                                                             "AND isvoid = 0 " & _
                                                                             "AND iscommitted = 1 " & _
                                                                             "AND evaltypeid = 3 " & _
                                                                             "AND assessmodeid = 2 " & _
                                                                   ") " & _
                                      "AND periodunkid = @PidAssess " & _
                                      "AND isvoid = 0 " & _
                                      "AND iscommitted = 1 " & _
                                      "AND evaltypeid = 3 " & _
                                      "AND assessmodeid = 3 " & _
                                      "AND LEN(@value) > 0 AND CAST(@value AS BIT) = 1 " & _
                                      "AND NOT (@DtAssess IS NOT NULL) "

                objDataOperation.AddParameter("@ReviewEmployeeCompetenceMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 55, "Review Employee Competence"))
            End If

            If blnApproveLeaveMSS = True AndAlso intUserUnkid > 0 Then

                StrQ &= "UNION " & _
                        "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ApproveLeaveMSS & " AS Id, @ApproveLeaveMSS " & _
                        "FROM lvpendingleave_tran " & _
                            "JOIN lvleaveform ON lvleaveform.formunkid = lvpendingleave_tran.formunkid " & _
                            "LEFT JOIN lvleaveapprover_master ON lvleaveapprover_master.leaveapproverunkid = lvpendingleave_tran.approverunkid " & _
                            "LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = lvleaveapprover_master.approverunkid " & _
                                   "AND hrapprover_usermapping.usertypeid = 1 " & _
                                   "AND lvleaveapprover_master.approverunkid = lvpendingleave_tran.approvertranunkid " & _
                        "WHERE lvpendingleave_tran.isvoid = 0 " & _
                              "AND lvleaveform.isvoid = 0 " & _
                              "AND lvleaveapprover_master.isvoid = 0 " & _
                              "AND hrapprover_usermapping.userunkid = @userunkid " & _
                              "AND lvleaveform.statusunkid = 2 " & _
                              "AND lvpendingleave_tran.statusunkid = 2 " & _
                              "AND lvpendingleave_tran.visibleid = 2 " & _
                              "AND lvpendingleave_tran.iscancelform = 0 "

                objDataOperation.AddParameter("@ApproveLeaveMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 56, "Approve Leave"))
            End If

            If blnApproveOTApplicationMSS = True AndAlso intUserUnkid > 0 Then

                StrQ &= "UNION " & _
                        "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ApproveOTApplicationMSS & " AS Id, @ApproveOTApplicationMSS " & _
                        "FROM tnaotrequisition_approval_tran " & _
                            "JOIN tnaot_requisition_tran ON tnaot_requisition_tran.otrequisitiontranunkid = tnaotrequisition_approval_tran.otrequisitiontranunkid " & _
                            "LEFT JOIN tnaapprover_master ON tnaapprover_master.tnamappingunkid = tnaotrequisition_approval_tran.tnamappingunkid " & _
                        "WHERE tnaotrequisition_approval_tran.isvoid = 0 " & _
                              "AND tnaot_requisition_tran.isvoid = 0 " & _
                              "AND tnaapprover_master.isvoid = 0 " & _
                              "AND tnaapprover_master.mapuserunkid = @userunkid " & _
                              "AND tnaot_requisition_tran.statusunkid = 2 " & _
                              "AND tnaotrequisition_approval_tran.statusunkid = 2 " & _
                              "AND tnaotrequisition_approval_tran.visibleunkid = 2 " & _
                              "AND tnaotrequisition_approval_tran.iscancel = 0 "

                objDataOperation.AddParameter("@ApproveOTApplicationMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 57, "Approve OT Application"))
            End If

            If blnApproveClaimExpenseMSS = True AndAlso intUserUnkid > 0 Then

                StrQ &= "UNION " & _
                        "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ApproveClaimExpenseMSS & " AS Id, @ApproveClaimExpenseMSS " & _
                        "FROM cmclaim_approval_tran " & _
                            "JOIN cmclaim_request_master ON cmclaim_approval_tran.crmasterunkid = cmclaim_request_master.crmasterunkid " & _
                            "LEFT JOIN cmexpapprover_master ON cmexpapprover_master.employeeunkid = cmclaim_approval_tran.approveremployeeunkid " & _
                                    "AND cmexpapprover_master.crapproverunkid = cmclaim_approval_tran.crapproverunkid " & _
                                    "AND cmexpapprover_master.isexternalapprover = 0 " & _
                            "LEFT JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = cmexpapprover_master.crapproverunkid " & _
                                    "AND hrapprover_usermapping.usertypeid = 3 " & _
                        "WHERE cmclaim_approval_tran.isvoid = 0 " & _
                              "AND cmclaim_request_master.isvoid = 0 " & _
                              "AND cmexpapprover_master.isvoid = 0 " & _
                              "AND cmclaim_approval_tran.crapproverunkid = @userunkid " & _
                              "AND cmclaim_approval_tran.statusunkid = 2 " & _
                              "AND cmclaim_request_master.statusunkid = 2 " & _
                              "AND cmclaim_approval_tran.visibleid = 2 " & _
                              "AND cmclaim_approval_tran.iscancel = 0 "

                objDataOperation.AddParameter("@ApproveClaimExpenseMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 58, "Approve Claim Expense"))
            End If

            If blnAssetDeclarationT2ESS = True AndAlso intEmployeeUnkId > 0 Then

                Dim dtDeclareFrom As Date = dtFinYearStart.AddYears(-1)
                Dim dtDeclareTo As Date = dtFinYearEnd.AddYears(-1)

                If dicConfigKeyValue.ContainsKey(_AssetDeclarationFromDate) = True AndAlso dicConfigKeyValue.ContainsKey(_AssetDeclarationToDate) = True AndAlso dicConfigKeyValue.Item(_AssetDeclarationFromDate).ToString.Trim <> "" AndAlso dicConfigKeyValue.Item(_AssetDeclarationToDate).ToString.Trim <> "" Then
                    dtDeclareFrom = eZeeDate.convertDate(dicConfigKeyValue.Item(_AssetDeclarationFromDate).ToString.ToString)
                    dtDeclareTo = eZeeDate.convertDate(dicConfigKeyValue.Item(_AssetDeclarationToDate).ToString.ToString)
                End If

                If dicConfigKeyValue.ContainsKey(_UnLockDaysAfterAssetDeclarationToDate) = True AndAlso CInt(dicConfigKeyValue.Item(_UnLockDaysAfterAssetDeclarationToDate).ToString) <> 0 Then
                    dtDeclareTo.AddDays(CInt(dicConfigKeyValue.Item(_UnLockDaysAfterAssetDeclarationToDate).ToString))
                End If

                If dtPeriodStart >= dtDeclareFrom.AddYears(1) AndAlso dtPeriodEnd <= dtDeclareTo.AddYears(1) Then

                    StrQ &= "UNION " & _
                       "SELECT COUNT(*) AS TotalCount, " & enPendingTask.AssetDeclarationT2ESS & " AS Id, @AssetDeclarationT2ESS " & _
                       "FROM hremployee_master " & _
                           "LEFT JOIN hrassetdeclarationT2_master ON hrassetdeclarationT2_master.employeeunkid = hremployee_master.employeeunkid " & _
                                   "AND hrassetdeclarationT2_master.isvoid = 0 " & _
                                   "AND hrassetdeclarationT2_master.isfinalsaved = 1 " & _
                                       "AND CONVERT(CHAR(8),finyear_start,112)  >= CONVERT(CHAR(8), DATEADD(YEAR, -1, @yearstartdate), 112) " & _
                                       "AND CONVERT(CHAR(8),finyear_end,112) <=  CONVERT(CHAR(8), DATEADD(YEAR, -1, @yearenddate), 112) " & _
                       "WHERE 1 = 1 " & _
                             "AND hremployee_master.employeeunkid = @employeeunkid " & _
                             "AND hrassetdeclarationT2_master.employeeunkid IS NULL "

                objDataOperation.AddParameter("@AssetDeclarationT2ESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 59, "Asset Declaration"))
                End If

            End If

            If blnNonDisclosureDeclarationESS = True AndAlso intEmployeeUnkId > 0 Then

                Dim dtDeclareFrom As Date = dtFinYearStart
                Dim dtDeclareTo As Date = dtFinYearEnd

                If dicConfigKeyValue.ContainsKey(_NonDisclosureDeclarationFromDate) = True AndAlso dicConfigKeyValue.ContainsKey(_NonDisclosureDeclarationToDate) = True AndAlso dicConfigKeyValue.Item(_NonDisclosureDeclarationFromDate).ToString.Trim <> "" AndAlso dicConfigKeyValue.Item(_NonDisclosureDeclarationToDate).ToString.Trim <> "" Then
                    dtDeclareFrom = eZeeDate.convertDate(dicConfigKeyValue.Item(_NonDisclosureDeclarationFromDate).ToString.ToString)
                    dtDeclareTo = eZeeDate.convertDate(dicConfigKeyValue.Item(_NonDisclosureDeclarationToDate).ToString.ToString)
                End If

                If dicConfigKeyValue.ContainsKey(_LockDaysAfterNonDisclosureDeclarationToDate) = True AndAlso CInt(dicConfigKeyValue.Item(_LockDaysAfterNonDisclosureDeclarationToDate).ToString) <> 0 Then
                    dtDeclareTo.AddDays(CInt(dicConfigKeyValue.Item(_LockDaysAfterNonDisclosureDeclarationToDate).ToString))
                End If

                If dtPeriodStart >= dtDeclareFrom AndAlso dtPeriodEnd <= dtDeclareTo Then

                    StrQ &= "UNION " & _
                           "SELECT COUNT(*) AS TotalCount, " & enPendingTask.NonDisclosureDeclarationESS & " AS Id, @NonDisclosureDeclarationESS " & _
                           "FROM hremployee_master " & _
                               "LEFT JOIN hrempnondisclosure_declaration_tran ON hrempnondisclosure_declaration_tran.employeeunkid = hremployee_master.employeeunkid " & _
                                       "AND hrempnondisclosure_declaration_tran.isvoid = 0 " & _
                                       "AND hrempnondisclosure_declaration_tran.yearunkid  = @yearunkid " & _
                           "WHERE 1 = 1 " & _
                                 "AND hremployee_master.employeeunkid = @employeeunkid " & _
                                 "AND hrempnondisclosure_declaration_tran.employeeunkid IS NULL "

                    objDataOperation.AddParameter("@NonDisclosureDeclarationESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 61, "Non-Disclosure Declaration"))

                End If

            End If

            If blnApproveSalaryChangeMSS = True AndAlso intUserUnkid > 0 Then

                StrQ &= "UNION " & _
                           "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ApproveSalaryChangeMSS & " AS Id, @ApproveSalaryChangeMSS " & _
                           "FROM prsalaryincrement_tran " & _
                                "JOIN #TableEmp ON #TableEmp.employeeunkid = prsalaryincrement_tran.employeeunkid " & _
                            "WHERE prsalaryincrement_tran.isvoid = 0 " & _
                                  "AND prsalaryincrement_tran.isapproved = 0 "

                objDataOperation.AddParameter("@ApproveSalaryChangeMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 62, "Approve Salary Change"))

            End If

            If blnApprovePayslipPaymentMSS = True AndAlso intUserUnkid > 0 Then

                Dim sQ As String = ""
                Dim intCurrentPriority As Integer = -1

                sQ = "SELECT prpaymentapproverlevel_master.priority " & _
                    "FROM prpayment_approver_mapping " & _
                        "LEFT JOIN prpaymentapproverlevel_master ON prpaymentapproverlevel_master.levelunkid = prpayment_approver_mapping.levelunkid " & _
                    "WHERE prpayment_approver_mapping.isvoid = 0 " & _
                          "AND prpaymentapproverlevel_master.isactive = 1 " & _
                          "AND prpaymentapproverlevel_master.isinactive = 0 " & _
                          "AND userapproverunkid = " & intUserUnkid & " "

                Dim ds As DataSet = objDataOperation.ExecQuery(sQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                If ds.Tables(0).Rows.Count > 0 Then
                    intCurrentPriority = CInt(ds.Tables(0).Rows(0).Item("priority"))

                    StrQ &= "UNION " & _
                           "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ApprovePayslipPaymentMSS & " AS Id, @ApprovePayslipPaymentMSS " & _
                           "FROM prpayment_tran " & _
                                "JOIN #TableEmp ON #TableEmp.employeeunkid = prpayment_tran.employeeunkid " & _
                                "LEFT JOIN prpayment_approval_tran ON prpayment_tran.paymenttranunkid = prpayment_approval_tran.paymenttranunkid " & _
                                       "AND prpayment_approval_tran.isvoid = 0 " & _
                                "LEFT JOIN prpaymentapproverlevel_master ON prpayment_approval_tran.levelunkid = prpaymentapproverlevel_master.levelunkid " & _
                                       "AND prpaymentapproverlevel_master.isactive = 1 " & _
                            "WHERE prpayment_tran.isvoid = 0 " & _
                                  "AND prpayment_tran.isauthorized = 0 " & _
                                  "AND prpayment_tran.referenceid = " & clsPayment_tran.enPaymentRefId.PAYSLIP & " " & _
                                  "AND prpayment_tran.paytypeid = " & clsPayment_tran.enPayTypeId.PAYMENT & " " & _
                                  "AND prpayment_approval_tran.statusunkid IN ( 0, 1 ) " & _
                                  "AND prpayment_tran.isapproved = 0 " & _
                                  "AND prpayment_tran.periodunkid = " & intFirstPeriodId & " " & _
                                  "AND prpayment_approval_tran.priority = " & _
                                      "( " & _
                                          "SELECT ISNULL(MAX(priority), -1) AS LowerLevelPriority " & _
                                          "FROM prpaymentapproverlevel_master " & _
                                          "WHERE isactive = 1 " & _
                                                "AND priority < " & intCurrentPriority & " " & _
                                                "AND prpaymentapproverlevel_master.isinactive = 0 " & _
                                      ") "

                    objDataOperation.AddParameter("@ApprovePayslipPaymentMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 63, "Approve Payslip Payment"))

                End If

            End If

            
            If blnApproveLoanApplicationMSS = True AndAlso intUserUnkid > 0 Then

                'Pinkal (15-May-2023) -- Start
                ' (A1X-850) Tujijenge - Dashboard pending actions to show list of records after redirecting to the action page.
                If dicConfigKeyValue.ContainsKey(_RoleBasedLoanApproval) = True AndAlso dicConfigKeyValue(_RoleBasedLoanApproval) = True Then
                    StrQ &= " UNION " & _
                                 " SELECT SUM(A.TotalCount) , " & enPendingTask.ApproveLoanApplicationMSS & " AS Id, @ApproveLoanApplicationMSS From " & _
                                 " ( " & _
                                 "           SELECT COUNT(*) AS TotalCount " & _
                                 "           FROM lnroleloanapproval_process_tran " & _
                                 "           LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnroleloanapproval_process_tran.processpendingloanunkid " & _
                                 "           AND lnloan_process_pending_loan.isvoid = 0 " & _
                                 "           WHERE lnroleloanapproval_process_tran.isvoid = 0 " & _
                                 "           AND lnloan_process_pending_loan.loan_statusunkid =  " & enLoanApplicationStatus.PENDING & _
                                 "           AND lnroleloanapproval_process_tran.visibleunkid =  " & enLoanApplicationStatus.PENDING & _
                                 "           AND lnroleloanapproval_process_tran.mappingunkid <=0 AND lnroleloanapproval_process_tran.roleunkid <= 0 AND lnroleloanapproval_process_tran.levelunkid <= 0 " & _
                                 "           AND lnroleloanapproval_process_tran.mapuserunkid = @employeeunkid " & _
                                 "           UNION ALL " & _
                                 "           SELECT COUNT(*) AS TotalCount " & _
                                 "           FROM lnroleloanapproval_process_tran " & _
                                 "           LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnroleloanapproval_process_tran.processpendingloanunkid " & _
                                 "           AND lnloan_process_pending_loan.isvoid = 0 " & _
                                 "           WHERE lnroleloanapproval_process_tran.isvoid = 0 " & _
                                 "           AND lnloan_process_pending_loan.loan_statusunkid =  " & enLoanApplicationStatus.PENDING & _
                                 "           AND lnroleloanapproval_process_tran.visibleunkid =  " & enLoanApplicationStatus.PENDING & _
                                 "           AND lnroleloanapproval_process_tran.mappingunkid > 0 AND lnroleloanapproval_process_tran.roleunkid > 0 AND lnroleloanapproval_process_tran.levelunkid > 0 " & _
                                 "           AND lnroleloanapproval_process_tran.mapuserunkid = @userunkid " & _
                                 ") AS A "
                Else
                StrQ &= "UNION " & _
                           "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ApproveLoanApplicationMSS & " AS Id, @ApproveLoanApplicationMSS " & _
                           "FROM lnloanapproval_process_tran " & _
                                "LEFT JOIN lnloan_process_pending_loan ON lnloan_process_pending_loan.processpendingloanunkid = lnloanapproval_process_tran.processpendingloanunkid " & _
                                       "AND lnloan_process_pending_loan.isvoid = 0 " & _
                                "LEFT JOIN lnloanapprover_master ON lnloanapprover_master.lnapproverunkid = lnloanapproval_process_tran.approvertranunkid " & _
                                "LEFT JOIN lnloan_approver_mapping ON lnloan_approver_mapping.approvertranunkid = lnloanapprover_master.lnapproverunkid " & _
                            "WHERE lnloanapproval_process_tran.isvoid = 0 " & _
                                  "AND lnloan_process_pending_loan.loan_statusunkid = 1 " & _
                                  "AND lnloanapproval_process_tran.visibleid = 1 " & _
                                  "AND lnloan_approver_mapping.userunkid = @userunkid "
                End If
                'Pinkal (15-May-2023) -- End

                objDataOperation.AddParameter("@ApproveLoanApplicationMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 64, "Approve Loan Application"))

            End If

            If blnApproveStaffRequisitionMSS = True AndAlso intUserUnkid > 0 Then

                StrQ &= "UNION " & _
                          "SELECT COUNT(*) AS TotalCount, " & enPendingTask.ApproveStaffRequisitionMSS & " AS Id, @ApproveStaffRequisitionMSS " & _
                            "FROM rcstaffrequisition_tran " & _
                                "JOIN #lowerpriority ON #lowerpriority.allocationid = rcstaffrequisition_tran.staffrequisitionbyid " & _
                                       "AND #lowerpriority.allocationunkid = rcstaffrequisition_tran.allocationunkid " & _
                                "LEFT JOIN rcstaffrequisition_approval_tran ON rcstaffrequisition_approval_tran.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                                       "AND rcstaffrequisition_approval_tran.levelunkid = #lowerpriority.lowerlevelunkid " & _
                                       "AND rcstaffrequisition_approval_tran.isvoid = 0 " & _
                                "LEFT JOIN rcstaffrequisition_approval_tran AS currapproval ON currapproval.staffrequisitiontranunkid = rcstaffrequisition_tran.staffrequisitiontranunkid " & _
                                       "AND currapproval.levelunkid = #lowerpriority.levelunkid " & _
                                       "AND currapproval.isvoid = 0 " & _
                            "WHERE rcstaffrequisition_tran.isvoid = 0 " & _
                                  "AND rcstaffrequisition_tran.form_statusunkid = 1 " & _
                                  "AND ( " & _
                                          "lowerlevelunkid = -1 " & _
                                          "OR rcstaffrequisition_approval_tran.staffrequisitionapprovaltranunkid IS NOT NULL " & _
                                      ") " & _
                                  "AND ISNULL(currapproval.statusunkid, 1) = 1 "

                objDataOperation.AddParameter("@ApproveStaffRequisitionMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 65, "Approve Staff Requisition"))

            End If

            If blnApproveClaimRetirementMSS = True AndAlso intUserUnkid > 0 Then

                objDataOperation.AddParameter("@ApproveClaimRetirementMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 66, "Approve ClaimRetirement"))

            End If

            If blnApproveBudgetTimesheetMSS = True AndAlso intUserUnkid > 0 Then

                objDataOperation.AddParameter("@ApproveBudgetTimesheetMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 67, "Approve Budget Timesheet"))

            End If

            If blnApproveShortlistingCriteriaMSS = True AndAlso intUserUnkid > 0 Then

                objDataOperation.AddParameter("@ApproveShortlistingCriteriaMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 68, "Approve Shortlisting Criteria"))

            End If

            If blnApproveEligibleApplicantsMSS = True AndAlso intUserUnkid > 0 Then

                objDataOperation.AddParameter("@ApproveEligibleApplicantsMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 69, "Approve Eligible Applicants"))

            End If

            If blnApproveCalibrationMSS = True AndAlso intUserUnkid > 0 Then

                Dim sQ As String = ""
                Dim intCurrentPriority As Integer = -1

                sQ = "SELECT CAL.priority " & _
                      "FROM hrscore_calibration_approver_master AS CAM " & _
                          "JOIN hrscore_calibration_approverlevel_master CAL ON CAM.levelunkid = CAL.levelunkid " & _
                      "WHERE CAM.isvoid = 0 " & _
                            "AND CAM.isactive = 1 " & _
                            "AND CAM.iscalibrator = 0 " & _
                            "AND CAM.mapuserunkid = " & intUserUnkid & " "

                Dim ds As DataSet = objDataOperation.ExecQuery(sQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                End If

                If ds.Tables(0).Rows.Count > 0 Then
                    intCurrentPriority = CInt(ds.Tables(0).Rows(0).Item("priority"))

                    Dim objScoreCalib As New clsScoreCalibrationApproval
                    Dim dtScore As DataTable = objScoreCalib.GetEmployeeApprovalDataNew(intFirstPeriodIdAssesment, strDatabaseName, intCompanyUnkid, intYearUnkId, strUserModeSetting, enUserPriviledge.AllowToApproveRejectCalibratedScore, eZeeDate.convertDate(dtPeriodEnd), intUserUnkid, True, False, Nothing, intCurrentPriority, True, "", "", , False)
                    dtScore = New DataView(dtScore, " isgrp = 0 AND userunkid = " & intUserUnkid & " AND iStatusId = '" & CInt(clsScoreCalibrationApproval.enCalibrationStatus.Submitted) & "'", "", DataViewRowState.CurrentRows).ToTable

                    If dtScore.Rows.Count > 0 Then

                        StrQ &= "UNION " & _
                                "SELECT " & dtScore.Rows.Count & " AS TotalCount, " & enPendingTask.ApproveCalibrationMSS & " AS Id, @ApproveCalibrationMSS AS Name "

                        objDataOperation.AddParameter("@ApproveCalibrationMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 70, "Approve Calibration"))

                    End If
                End If

            End If
            'Hemant (04 Sep 2021) -- Start
            'ENHANCEMENT : OLD-465 - Dashboard Pending Task Enhancement - Training Evaluation.
            If blnMyTrainingFeedbackESS = True And intEmployeeUnkId > 0 Then

                StrQ &= "UNION " & _
                            " SELECT " & _
                            "       SUM(TotalCount) AS TotalCount " & _
                            ",      " & enPendingTask.MyTrainingFeedbackESS & " AS Id " & _
                            ",      @MyTrainingFeedbackESS " & _
                            "  from #MyTrainingFeedback "

                objDataOperation.AddParameter("@MyTrainingFeedbackESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1065, "My Training Feedback"))

            End If

            If blnEvaluateEmployeeTrainingMSS = True And intUserUnkid > 0 Then

                StrQ &= "UNION " & _
                            " SELECT " & _
                            "       COUNT(*) AS TotalCount " & _
                            ",      " & enPendingTask.EvaluateEmployeeTrainingMSS & " AS Id " & _
                            ",      @EvaluateEmployeeTrainingMSS " & _
                            " FROM trtraining_request_master "
                'Hemant (03 Feb 2023) -- Start
                'ENHANCEMENT(NMB) : A1X-612 - As a user, I want to have an effective date for reporting to detail on employee reporting to screen
                'StrQ &= " JOIN hremployee_reportto ON hremployee_reportto.employeeunkid = trtraining_request_master.employeeunkid  " & _
                '                            "      AND  hremployee_reportto.isvoid = 0 " & _
                '                            "      AND reporttoemployeeunkid = @employeeunkid " & _
                '                            "      AND ishierarchy = 1   "
                StrQ &= " JOIN (  " & _
                            "      SELECT " & _
                            "           hremployee_reportto.employeeunkid AS employeeunkid " & _
                            "           ,reporttoemployeeunkid " & _
                            "           ,ROW_NUMBER() OVER (PARTITION BY hremployee_reportto.employeeunkid ORDER BY effectivedate DESC) AS Rno " & _
                            "      FROM hremployee_reportto  " & _
                            "      LEFT JOIN hremployee_master AS R_Emp ON hremployee_reportto.reporttoemployeeunkid = R_Emp.employeeunkid  " & _
                            "      WHERE hremployee_reportto.isvoid = 0  " & _
                            "      AND ishierarchy = 1   " & _
                            "            AND CONVERT(CHAR(8), effectivedate, 112) <= @Date " & _
                            "  ) AS RepTable  ON RepTable.employeeunkid = trtraining_request_master.employeeunkid  " & _
                            "  AND RepTable.Rno = 1 " & _
                            "  AND RepTable.reporttoemployeeunkid = @employeeunkid " 
                'Hemant (03 Feb 2023) -- End
                StrQ &= " WHERE trtraining_request_master.isvoid = 0 " & _
                            "       AND  isdaysafterfeedback_submitted = 1  " & _
                            "       AND isdaysafter_linemanager_submitted = 0 "

                objDataOperation.AddParameter("@EvaluateEmployeeTrainingMSS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1066, "Evaluate Employee Training"))

            End If
            'Hemant (04 Sep 2021) -- End


            'Pinkal (01-Jun-2021)-- New UI Self Service Enhancement : Working on New UI Dashboard Settings.[REPLACE UNION ALL TO UNION]


            StrQ &= strDropQ

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
            objDataOperation.AddParameter("@SetScoreCardESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 60, "Set Score Card"))
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intCompanyUnkid)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserUnkid)
                objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearUnkId)
            objDataOperation.AddParameter("@yearstartdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtFinYearStart))
            objDataOperation.AddParameter("@yearenddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtFinYearEnd))

            dsList = objDataOperation.ExecQuery(StrQ, "Trans")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: PendingTask; Module Name: " & mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'Sohail (01 Jan 2021) -- End


    'Pinkal (07-Oct-2021)-- Start
    'TRA System Performance Issue.

    'Public Function GetPeningAccrueLeaveCount() As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim StrQ As String = String.Empty
    '    Try

    '        Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
    '        xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
    '        Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtPeriodEndDate, mdtPeriodEndDate, , , mstrDatabaseName)
    '        Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtPeriodEndDate, mblnOnlyApproved, mstrDatabaseName, mintuserUnkid, mintCompanyUnkid, mintYearUnkId, mstrUserModeSetting)
    '        Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtPeriodEndDate, mstrDatabaseName)

    '        objDataOperation = New clsDataOperation

    '        If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo = True Then

    '            If mblnShow_PendingAccrueLeaves Then

    '                StrQ &= "SELECT " & _
    '                       "     hremployee_master.employeeunkid " & _
    '                       "    ,A.leavetypeunkid " & _
    '                       "    ,A.leavename " & _
    '                       " INTO #table " & _
    '                       "FROM hremployee_master "

    '                If xAdvanceJoinQry.Trim.Length > 0 Then
    '                    StrQ &= " " & xAdvanceJoinQry & " "
    '                End If

    '                If xDateJoinQry.Trim.Length > 0 Then
    '                    StrQ &= " " & xDateJoinQry & " "
    '                End If

    '                If xUACQry.Trim.Length > 0 Then
    '                    StrQ &= " " & xUACQry & " "
    '                End If

    '                StrQ &= "LEFT JOIN " & _
    '                        "( " & _
    '                        "    SELECT " & _
    '                        "         leavetypeunkid " & _
    '                        "        ,ISNULL(gender,0) gender " & _
    '                        "        ,leavename " & _
    '                        "    FROM lvleavetype_master WHERE isactive = 1 AND isaccrueamount = 1 " & _
    '                        ") AS A ON 1 = 1 AND (A.gender = hremployee_master.gender OR A.gender = 0) " & _
    '                        "WHERE 1 = 1 "

    '                If xDateFilterQry.Trim.Length > 0 Then
    '                    StrQ &= " " & xDateFilterQry & " "
    '                End If

    '                StrQ &= "EXCEPT " & _
    '                        "SELECT " & _
    '                        "     employeeunkid " & _
    '                        "    ,leavetypeunkid " & _
    '                        "    ,leavename " & _
    '                        "FROM " & _
    '                        "( " & _
    '                        "    SELECT " & _
    '                        "         employeeunkid " & _
    '                        "        ,lvleavetype_master.leavetypeunkid " & _
    '                        "        ,leavename " & _
    '                        "    FROM lvleavebalance_tran " & _
    '                        "        join lvleavetype_master on lvleavebalance_tran.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
    '                        "    WHERE isvoid = 0 AND isopenelc = 1 and iselc = 1 and isactive = 1 AND isaccrueamount = 1 " & _
    '                        "    UNION " & _
    '                        "    SELECT " & _
    '                        "         employeeunkid " & _
    '                        "        ,lvleavetype_master.leavetypeunkid " & _
    '                        "        ,leavename " & _
    '                        "    FROM lvleavebalance_tran " & _
    '                        "        join lvleavetype_master on lvleavebalance_tran.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
    '                        "    WHERE isvoid = 0 AND iselc = 0 AND isclose_fy = 0 and isactive = 1 AND isaccrueamount = 1 " & _
    '                        ") AS A "

    '                StrQ &= "SELECT DISTINCT " & _
    '                            " 12 AS ID " & _
    '                            "/*, COUNT(DISTINCT hremployee_master.employeeunkid) AS TotalCount*/ " & _
    '                            ", firstname + ' ' + ISNULL(othername, '') + ' ' + surname AS [Emp Name] " & _
    '                            ", employeecode AS [Emp Code] " & _
    '                            ", leavename AS [Leave Type] " & _
    '                        "FROM hremployee_master " & _
    '                        "    JOIN #table ON #table.employeeunkid = hremployee_master.employeeunkid " & _
    '                        "DROP TABLE #table "


    '                dsList = objDataOperation.ExecQuery(StrQ, "Trans")

    '                If objDataOperation.ErrorMessage <> "" Then
    '                    Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                End If

    '            End If

    '        End If

    '        If dsList Is Nothing Then
    '            Dim dtTable As New DataTable
    '            dtTable.Columns.Add("ID", System.Type.GetType("System.Int32")).DefaultValue = 0
    '            dtTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = 0
    '            dtTable.Columns.Add("Emp Name", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("Emp Code", System.Type.GetType("System.String")).DefaultValue = ""
    '            dtTable.Columns.Add("Leave Type", System.Type.GetType("System.String")).DefaultValue = ""

    '            'Sohail (13 Jan 2020) -- Start
    '            'NMB Issue # : object reference error when user does not have privilege.
    '            dsList = New DataSet
    '            'Sohail (13 Jan 2020) -- End
    '            dsList.Tables.Add(dtTable)
    '        End If

    '        Return dsList
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetPeningAccrueLeaveCount; Module Name: " & mstrModuleName)
    '        Return Nothing
    '    End Try
    'End Function


    Public Function GetPeningAccrueLeaveCount() As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = String.Empty
        Try


            'Pinkal (30-Sep-2023) -- Start 
            '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
            Dim mstrGroup As String = String.Empty
            Dim objGroup As New clsGroup_Master
            objGroup._Groupunkid = 1
            mstrGroup = objGroup._Groupname
            objGroup = Nothing

            Dim mstrQueryColumns As String = ""
            Dim mstrDisplayMainColumns As String = ""
            Dim mstrDisplayInternalColumns As String = ""
            Dim mstrAllocationMstJoin As String = ""


            If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then

                mstrQueryColumns = ", ISNULL(SM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 5, "Branch") = "", "Branch", Language.getMessage(mstrModuleName, 5, "Branch")) & "] " & _
                                             ", ISNULL(DGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 75, "Department Group") = "", "Department Group", Language.getMessage(mstrModuleName, 75, "Department Group")) & "] " & _
                                             ", ISNULL(DM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 4, "Department") = "", "Department", Language.getMessage(mstrModuleName, 4, "Department")) & "] " & _
                                             ", ISNULL(SGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 76, "Section Group") = "", "Section Group", Language.getMessage(mstrModuleName, 76, "Section Group")) & "] " & _
                                             ", ISNULL(SECM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 7, "Section") = "", "Section", Language.getMessage(mstrModuleName, 7, "Section")) & "] " & _
                                             ", ISNULL(UGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 77, "Unit Group") = "", "Unit Group", Language.getMessage(mstrModuleName, 77, "Unit Group")) & "] " & _
                                             ", ISNULL(UM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 78, "Unit") = "", "Unit", Language.getMessage(mstrModuleName, 78, "Unit")) & "] " & _
                                             ", ISNULL(TM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 79, "Team") = "", "Team", Language.getMessage(mstrModuleName, 79, "Team")) & "] " & _
                                             ", ISNULL(CGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 80, "Class Group") = "", "Class Group", Language.getMessage(mstrModuleName, 80, "Class Group")) & "] " & _
                                             ", ISNULL(CM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 81, "Class") = "", "Class", Language.getMessage(mstrModuleName, 81, "Class")) & "] " & _
                                             ", ISNULL(JGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 82, "Job Group") = "", "Job Group", Language.getMessage(mstrModuleName, 82, "Job Group")) & "] " & _
                                             ", ISNULL(JM.job_name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 83, "Job") = "", "Job", Language.getMessage(mstrModuleName, 83, "Job")) & "] " & _
                                             ", ISNULL(GGM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 84, "Grade Group") = "", "Grade Group", Language.getMessage(mstrModuleName, 84, "Grade Group")) & "] " & _
                                             ", ISNULL(GM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 85, "Grade") = "", "Grade", Language.getMessage(mstrModuleName, 85, "Grade")) & "] " & _
                                             ", ISNULL(GLM.name,'') AS [" & IIf(Language.getMessage(mstrModuleName, 86, "Grade Group Level") = "", "Grade Group Level", Language.getMessage(mstrModuleName, 86, "Grade Group Level")) & "] " & _
                                             ", ISNULL(CCM.costcentername,'') AS [" & IIf(Language.getMessage(mstrModuleName, 87, "Cost Center") = "", "Cost Center", Language.getMessage(mstrModuleName, 87, "Cost Center")) & "] "



                mstrDisplayMainColumns = ", [" & Language.getMessage(mstrModuleName, 5, "Branch") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 75, "Department Group") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 4, "Department") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 76, "Section Group") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 7, "Section") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 77, "Unit Group") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 78, "Unit") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 79, "Team") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 80, "Class Group") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 81, "Class") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 82, "Job Group") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 83, "Job") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 84, "Grade Group") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 85, "Grade") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 86, "Grade Group Level") & "] " & _
                                                  ", [" & Language.getMessage(mstrModuleName, 87, "Cost Center") & "] "



                mstrDisplayInternalColumns = ", #table. [" & Language.getMessage(mstrModuleName, 5, "Branch") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 75, "Department Group") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 4, "Department") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 76, "Section Group") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 7, "Section") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 77, "Unit Group") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 78, "Unit") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 79, "Team") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 80, "Class Group") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 81, "Class") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 82, "Job Group") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 83, "Job") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 84, "Grade Group") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 85, "Grade") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 86, "Grade Group Level") & "]" & _
                                                            ", #table. [" & Language.getMessage(mstrModuleName, 87, "Cost Center") & "]"



                mstrAllocationMstJoin = " LEFT JOIN hrstation_master AS SM ON SM.stationunkid = ADF.stationunkid " & _
                                                 " LEFT JOIN hrdepartment_group_master AS DGM ON DGM.deptgroupunkid = ADF.deptgroupunkid  " & _
                                                 " LEFT JOIN hrdepartment_master AS DM ON  DM.departmentunkid = ADF.departmentunkid " & _
                                                 " LEFT JOIN hrsectiongroup_master AS SGM ON SGM.sectiongroupunkid = ADF.sectiongroupunkid " & _
                                                 " LEFT JOIN hrsection_master AS SECM ON SECM.sectionunkid  = ADF.sectionunkid " & _
                                                 " LEFT JOIN hrunitgroup_master AS UGM ON UGM.unitgroupunkid =  ADF.unitgroupunkid " & _
                                                 " LEFT JOIN hrunit_master AS UM ON UM.unitunkid = ADF.unitunkid " & _
                                                 " LEFT JOIN hrteam_master AS TM ON TM.teamunkid = ADF.teamunkid " & _
                                                 " LEFT JOIN hrclassgroup_master AS CGM ON CGM.classgroupunkid = ADF.classgroupunkid " & _
                                                 " LEFT JOIN hrclasses_master AS CM ON CM.classesunkid = ADF.classunkid  " & _
                                                 " LEFT JOIN hrjobgroup_master AS JGM ON JGM.jobgroupunkid = ADF.jobgroupunkid " & _
                                                 " LEFT JOIN hrjob_master AS JM ON JM.jobunkid = ADF.jobunkid " & _
                                                 " LEFT JOIN hrgradegroup_master AS GGM ON GGM.gradegroupunkid = ADF.gradegroupunkid " & _
                                                 " LEFT JOIN hrgrade_master AS GM ON  GM.gradeunkid = ADF.gradeunkid " & _
                                                 " LEFT JOIN hrgradelevel_master AS GLM ON GLM.gradelevelunkid = ADF.gradelevelunkid " & _
                                                 " LEFT JOIN prcostcenter_master AS CCM ON CCM.costcenterunkid = ADF.costcenterunkid "


            End If


            'Pinkal (30-Sep-2023) -- End 


            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, mdtPeriodEndDate, mdtPeriodEndDate, , , mstrDatabaseName)
            Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, mdtPeriodEndDate, mblnOnlyApproved, mstrDatabaseName, mintuserUnkid, mintCompanyUnkid, mintYearUnkId, mstrUserModeSetting)
            Call GetAdvanceFilterQry(xAdvanceJoinQry, mdtPeriodEndDate, mstrDatabaseName)

            objDataOperation = New clsDataOperation

            If ArtLic._Object.ModuleStatus(ArutiModule.ArutiModule.Leave_Management) OrElse ArtLic._Object.IsDemo = True Then

                If mblnShow_PendingAccrueLeaves Then


                    StrQ &= " SELECT employeeunkid " & _
                                 ",   leavetypeunkid " & _
                                 ",   leavename " & _
                                 "    INTO #Leave " & _
                                 " FROM " & _
                                 " ( " & _
                                 "    SELECT " & _
                                 "         employeeunkid " & _
                                 "        ,lvleavetype_master.leavetypeunkid " & _
                                 "        ,leavename " & _
                                 "    FROM lvleavebalance_tran " & _
                                 "        join lvleavetype_master on lvleavebalance_tran.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                                 "    WHERE isvoid = 0 AND isopenelc = 1 and iselc = 1 and isactive = 1 AND isaccrueamount = 1 " & _
                                 "    UNION " & _
                                 "    SELECT " & _
                                 "         employeeunkid " & _
                                 "        ,lvleavetype_master.leavetypeunkid " & _
                                 "        ,leavename " & _
                                 "    FROM lvleavebalance_tran " & _
                                 "        join lvleavetype_master on lvleavebalance_tran.leavetypeunkid = lvleavetype_master.leavetypeunkid " & _
                                 "    WHERE isvoid = 0 AND iselc = 0 AND isclose_fy = 0 and isactive = 1 AND isaccrueamount = 1 " & _
                                 ") AS A "

                    StrQ &= "SELECT " & _
                           "     hremployee_master.employeeunkid " & _
                           "    ,lv.leavetypeunkid " & _
                           "    ,lv.leavename "

                    'Pinkal (30-Sep-2023) -- Start 
                    '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
                    If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                        StrQ &= mstrQueryColumns
                    End If
                    'Pinkal (30-Sep-2023) -- ENd

                    StrQ &= " INTO #table " & _
                           "FROM hremployee_master "

                    If xAdvanceJoinQry.Trim.Length > 0 Then
                        StrQ &= " " & xAdvanceJoinQry & " "
                    End If

                    'Pinkal (30-Sep-2023) -- Start 
                    '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
                    If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                        StrQ &= " " & mstrAllocationMstJoin
                    End If
                    'Pinkal (30-Sep-2023) -- End


                    StrQ &= "LEFT JOIN " & _
                                 "( " & _
                                 "    SELECT " & _
                                 "         leavetypeunkid " & _
                                 "        ,ISNULL(gender,0) gender " & _
                                 "        ,leavename " & _
                                 "    FROM lvleavetype_master WHERE isactive = 1 AND isaccrueamount = 1 " & _
                                 " ) AS lv ON 1 = 1 AND (lv.gender = hremployee_master.gender OR lv.gender = 0) "

                    If xDateJoinQry.Trim.Length > 0 Then
                        StrQ &= " " & xDateJoinQry & " "
                    End If

                    If xUACQry.Trim.Length > 0 Then
                        StrQ &= " " & xUACQry & " "
                    End If

                    If xDateFilterQry.Trim.Length > 0 Then
                        StrQ &= " " & xDateFilterQry & " "
                    End If

                    StrQ &= " AND NOT EXISTS " & _
                                 " ( " & _
                                 "       SELECT  leavetypeunkid  " & _
                                 "         ,   leavename " & _
                                 "        FROM #Leave " & _
                                 "        WHERE #Leave.employeeunkid = hremployee_master.employeeunkid and #Leave.leavetypeunkid = lv.leavetypeunkid " & _
                                 " ) "

                    StrQ &= "SELECT DISTINCT " & _
                                " 12 AS ID " & _
                                "/*, COUNT(DISTINCT hremployee_master.employeeunkid) AS TotalCount*/ " & _
                                ", firstname + ' ' + ISNULL(othername, '') + ' ' + surname AS [Emp Name] " & _
                                ", employeecode AS [Emp Code] " & _
                                ", leavename AS [Leave Type] "

                    'Pinkal (30-Sep-2023) -- Start 
                    '(A1X-1315) Toyota - All existing Desktop dashboard items to show affected employee and all their allocations. 
                    If mstrGroup.Trim.ToUpper() = "KARIMJEE JIVANJEE GROUP" Then
                        StrQ &= mstrDisplayInternalColumns
                    End If
                    'Pinkal (30-Sep-2023) -- End

                    StrQ &= " FROM hremployee_master " & _
                            "    JOIN #table ON #table.employeeunkid = hremployee_master.employeeunkid " & _
                                " DROP TABLE #table " & _
                                " DROP TABLE #Leave "


                    dsList = objDataOperation.ExecQuery(StrQ, "Trans")

                    If objDataOperation.ErrorMessage <> "" Then
                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    End If

                End If

            End If

            If dsList Is Nothing Then
                Dim dtTable As New DataTable
                dtTable.Columns.Add("ID", System.Type.GetType("System.Int32")).DefaultValue = 0
                dtTable.Columns.Add("EmpId", System.Type.GetType("System.Int32")).DefaultValue = 0
                dtTable.Columns.Add("Emp Name", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("Emp Code", System.Type.GetType("System.String")).DefaultValue = ""
                dtTable.Columns.Add("Leave Type", System.Type.GetType("System.String")).DefaultValue = ""
                dsList = New DataSet
                dsList.Tables.Add(dtTable)
            End If

            Return dsList
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetPeningAccrueLeaveCount; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Pinkal (07-Oct-2021) -- End


    
    'Sohail (25 Apr 2019) -- End


    'S.SANDEEP [ 16 MAY 2012 ] -- START
    'ENHANCEMENT : TRA USER PRIVILEGE CHANGES
    Function strReplicate(ByVal str As String, ByVal intD As Integer) As String
        'This fucntion padded "0" after the number to evaluate hundred, thousand and on....
        'using this function you can replicate any Charactor with given string.
        Dim i As Integer
        strReplicate = ""
        For i = 1 To intD
            strReplicate = strReplicate + str
        Next
        Return strReplicate
    End Function

    Function AmtInWord(ByVal Num As Decimal) As String
        'I have created this function for converting amount in indian rupees (INR). 
        'You can manipulate as you wish like decimal setting, Doller (any currency) Prefix.

        Dim strNum As String
        Dim strNumDec As String
        Dim StrWord As String
        strNum = Num

        If InStr(1, strNum, ".") <> 0 Then
            strNumDec = Mid(strNum, InStr(1, strNum, ".") + 1)

            If Len(strNumDec) = 1 Then
                strNumDec = strNumDec + "0"
            End If
            If Len(strNumDec) > 2 Then
                strNumDec = Mid(strNumDec, 1, 2)
            End If

            strNum = Mid(strNum, 1, InStr(1, strNum, ".") - 1)
            StrWord = IIf(CDbl(strNum) = 1, " Rupee ", " Rupees ") + NumToWord(CDbl(strNum)) + IIf(CDbl(strNumDec) > 0, " and Paise" + cWord3(CDbl(strNumDec)), "")
        Else
            StrWord = IIf(CDbl(strNum) = 1, " Rupee ", " Rupees ") + NumToWord(CDbl(strNum))
        End If
        AmtInWord = StrWord & " Only"
        Return AmtInWord
    End Function

    Public Function NumToWord(ByVal Num As Decimal) As String
        'I divided this function in two part.
        '1. Three or less digit number.
        '2. more than three digit number.
        Dim strNum As String
        Dim StrWord As String
        strNum = Num

        If Len(strNum) <= 3 Then
            StrWord = cWord3(CDbl(strNum))
        Else
            StrWord = cWordG3(CDbl(Mid(strNum, 1, Len(strNum) - 3))) + " " + cWord3(CDbl(Mid(strNum, Len(strNum) - 2)))
        End If
        NumToWord = StrWord
    End Function

    Private Function cWordG3(ByVal Num As Decimal) As String
        '2. more than three digit number.
        Dim strNum As String = ""
        Dim StrWord As String = ""
        Dim readNum As String = ""
        strNum = Num
        If Len(strNum) Mod 2 <> 0 Then
            readNum = CDbl(Mid(strNum, 1, 1))
            If readNum <> "0" Then
                StrWord = retWord(readNum)
                readNum = CDbl("1" + strReplicate("0", Len(strNum) - 1) + "000")
                StrWord = StrWord + " " + retWord(readNum)
            End If
            strNum = Mid(strNum, 2)
        End If
        While Not Len(strNum) = 0
            readNum = CDbl(Mid(strNum, 1, 2))
            If readNum <> "0" Then
                StrWord = StrWord + " " + cWord3(readNum)
                readNum = CDbl("1" + strReplicate("0", Len(strNum) - 2) + "000")
                StrWord = StrWord + " " + retWord(readNum)
            End If
            strNum = Mid(strNum, 3)
        End While
        cWordG3 = StrWord
        Return cWordG3
    End Function

    Private Function cWord3(ByVal Num As Decimal) As String
        '1. Three or less digit number.
        Dim strNum As String = ""
        Dim StrWord As String = ""
        Dim readNum As String = ""
        If Num < 0 Then Num = Num * -1
        strNum = Num

        If Len(strNum) = 3 Then
            readNum = CDbl(Mid(strNum, 1, 1))
            StrWord = retWord(readNum) + " Hundred"
            strNum = Mid(strNum, 2, Len(strNum))
        End If

        If Len(strNum) <= 2 Then
            If CDbl(strNum) >= 0 And CDbl(strNum) <= 20 Then
                StrWord = StrWord + " " + retWord(CDbl(strNum))
            Else
                StrWord = StrWord + " " + retWord(CDbl(Mid(strNum, 1, 1) + "0")) + " " + retWord(CDbl(Mid(strNum, 2, 1)))
            End If
        End If

        strNum = CStr(Num)
        cWord3 = StrWord
        Return cWord3
    End Function

    Private Function retWord(ByVal Num As Decimal) As String
        'This two dimensional array store the primary word convertion of number.
        retWord = ""
        Dim ArrWordList(,) As Object = {{0, ""}, {1, "One"}, {2, "Two"}, {3, "Three"}, {4, "Four"}, _
                                        {5, "Five"}, {6, "Six"}, {7, "Seven"}, {8, "Eight"}, {9, "Nine"}, _
                                        {10, "Ten"}, {11, "Eleven"}, {12, "Twelve"}, {13, "Thirteen"}, {14, "Fourteen"}, _
                                        {15, "Fifteen"}, {16, "Sixteen"}, {17, "Seventeen"}, {18, "Eighteen"}, {19, "Nineteen"}, _
                                        {20, "Twenty"}, {30, "Thirty"}, {40, "Forty"}, {50, "Fifty"}, {60, "Sixty"}, _
                                        {70, "Seventy"}, {80, "Eighty"}, {90, "Ninety"}, {100, "Hundred"}, {1000, "Thousand"}, _
                                        {100000, "Lakh"}, {10000000, "Crore"}}

        Dim i As Integer
        For i = 0 To UBound(ArrWordList)
            If Num = ArrWordList(i, 0) Then
                retWord = ArrWordList(i, 1)
                Exit For
            End If
        Next
        Return retWord
    End Function
    'S.SANDEEP [ 16 MAY 2012 ] -- END


    'S.SANDEEP [ 30 OCT 2013 ] -- START
    Public Function AutoNotification_Data() As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iDataTable As DataTable = Nothing
        Dim dsList As New DataSet
        Try
            Using iDataOperation As New clsDataOperation
                StrQ = "SELECT " & _
                        "	 ischeck " & _
                        "	,ECode " & _
                        "	,EName " & _
                        "	,DType " & _
                        "	,FReason " & _
                        "	,Email " & _
                        "	,Mode " & _
                        "	,datetypeid " & _
                        "	,antf.companyunkid " & _
                        "	,Letter.key_value AS LetterId " & _
                        "	,Sett.key_value AS Setting " & _
                        "	,'' AS Email_Subject " & _
                        "FROM " & _
                        "( " & _
                        "	SELECT DISTINCT " & _
                        "		 CAST(0 AS Bit) AS ischeck " & _
                        "		,employeecode AS ECode " & _
                        "		,firstname+' '+surname AS EName " & _
                        "		,CASE WHEN datetypeid = 1  THEN @APPOINTED_DATE " & _
                        " 			  WHEN datetypeid = 2  THEN @CONFIRM_DATE " & _
                        " 			  WHEN datetypeid = 3  THEN @BIRTH_DATE " & _
                        " 			  WHEN datetypeid = 4  THEN @SUSPENSION_DATE " & _
                        " 			  WHEN datetypeid = 5  THEN @PROBATION_DATE " & _
                        " 			  WHEN datetypeid = 6  THEN @EOC_DATE " & _
                        " 			  WHEN datetypeid = 7  THEN @LEAVING_DATE " & _
                        " 			  WHEN datetypeid = 8  THEN @RETIREMENT_DATE " & _
                        " 			  WHEN datetypeid = 9  THEN @REINSTATEMENT_DATE " & _
                        " 			  WHEN datetypeid = 10 THEN @MARRIAGE_DATE " & _
                        " 			  WHEN datetypeid = 11 THEN @FIRST_APPOINTMENT_DATE " & _
                        "		 END AS DType " & _
                        "		,fail_reason AS FReason " & _
                        "		,CASE WHEN datetypeid = 1  THEN '' " & _
                        " 			  WHEN datetypeid = 2  THEN 'ERC_CNF_TemplateId' " & _
                        " 			  WHEN datetypeid = 3  THEN 'ERC_BRT_TemplateId' " & _
                        " 			  WHEN datetypeid = 4  THEN '' " & _
                        " 			  WHEN datetypeid = 5  THEN 'ERC_PED_TemplateId' " & _
                        " 			  WHEN datetypeid = 6  THEN 'ERC_EOC_TemplateId' " & _
                        " 			  WHEN datetypeid = 7  THEN '' " & _
                        " 			  WHEN datetypeid = 8  THEN 'ERC_RET_TemplateId' " & _
                        " 			  WHEN datetypeid = 9  THEN '' " & _
                        " 			  WHEN datetypeid = 10 THEN '' " & _
                        " 			  WHEN datetypeid = 11 THEN '' " & _
                        "		 END AS TmpId " & _
                        "		 ,CASE WHEN datetypeid = 1  THEN '' " & _
                        " 			  WHEN datetypeid = 2  THEN 'ERC_CNF_Setting' " & _
                        " 			  WHEN datetypeid = 3  THEN 'ERC_BRT_Setting' " & _
                        " 			  WHEN datetypeid = 4  THEN '' " & _
                        " 			  WHEN datetypeid = 5  THEN 'ERC_PED_Setting' " & _
                        " 			  WHEN datetypeid = 6  THEN 'ERC_EOC_Setting' " & _
                        " 			  WHEN datetypeid = 7  THEN '' " & _
                        " 			  WHEN datetypeid = 8  THEN 'ERC_RET_Setting' " & _
                        " 			  WHEN datetypeid = 9  THEN '' " & _
                        " 			  WHEN datetypeid = 10 THEN '' " & _
                        " 			  WHEN datetypeid = 11 THEN '' " & _
                        "		 END AS Setting " & _
                        "		,email AS Email " & _
                        "		,CASE WHEN isboth = 1 THEN @Both WHEN isboth = 0 THEN @Employee END AS Mode " & _
                        "		,datetypeid " & _
                        "		,cfemail_queue_tran.companyunkid " & _
                        "	FROM cfemail_queue_tran " & _
                        "		JOIN hremployee_master ON cfemail_queue_tran.recipientunkid = dbo.hremployee_master.employeeunkid " & _
                        "	WHERE issent = 1 AND ISNULL(fail_reason,'') <> '' AND is_user = 0 " & _
                        "	UNION ALL " & _
                        "	SELECT " & _
                        "		 CAST(0 AS Bit) AS ischeck " & _
                        "		,username AS ECode " & _
                        "		,ISNULL(firstname+' '+lastname,'') AS EName " & _
                        "		,CASE WHEN datetypeid = 1  THEN @APPOINTED_DATE " & _
                        " 			  WHEN datetypeid = 2  THEN @CONFIRM_DATE " & _
                        " 			  WHEN datetypeid = 3  THEN @BIRTH_DATE " & _
                        " 			  WHEN datetypeid = 4  THEN @SUSPENSION_DATE " & _
                        " 			  WHEN datetypeid = 5  THEN @PROBATION_DATE " & _
                        " 			  WHEN datetypeid = 6  THEN @EOC_DATE " & _
                        " 			  WHEN datetypeid = 7  THEN @LEAVING_DATE " & _
                        " 			  WHEN datetypeid = 8  THEN @RETIREMENT_DATE " & _
                        " 			  WHEN datetypeid = 9  THEN @REINSTATEMENT_DATE " & _
                        " 			  WHEN datetypeid = 10 THEN @MARRIAGE_DATE " & _
                        " 			  WHEN datetypeid = 11 THEN @FIRST_APPOINTMENT_DATE " & _
                        "		 END AS DType " & _
                        "		,fail_reason AS FReason " & _
                        "		,CASE WHEN datetypeid = 1  THEN '' " & _
                        " 			  WHEN datetypeid = 2  THEN 'ERC_CNF_TemplateId' " & _
                        " 			  WHEN datetypeid = 3  THEN 'ERC_BRT_TemplateId' " & _
                        " 			  WHEN datetypeid = 4  THEN '' " & _
                        " 			  WHEN datetypeid = 5  THEN 'ERC_PED_TemplateId' " & _
                        " 			  WHEN datetypeid = 6  THEN 'ERC_EOC_TemplateId' " & _
                        " 			  WHEN datetypeid = 7  THEN '' " & _
                        " 			  WHEN datetypeid = 8  THEN 'ERC_RET_TemplateId' " & _
                        " 			  WHEN datetypeid = 9  THEN '' " & _
                        " 			  WHEN datetypeid = 10 THEN '' " & _
                        " 			  WHEN datetypeid = 11 THEN '' " & _
                        "		 END AS TmpId " & _
                        "		 ,CASE WHEN datetypeid = 1  THEN '' " & _
                        " 			  WHEN datetypeid = 2  THEN 'ERC_CNF_Setting' " & _
                        " 			  WHEN datetypeid = 3  THEN 'ERC_BRT_Setting' " & _
                        " 			  WHEN datetypeid = 4  THEN '' " & _
                        " 			  WHEN datetypeid = 5  THEN 'ERC_PED_Setting' " & _
                        " 			  WHEN datetypeid = 6  THEN 'ERC_EOC_Setting' " & _
                        " 			  WHEN datetypeid = 7  THEN '' " & _
                        " 			  WHEN datetypeid = 8  THEN 'ERC_RET_Setting' " & _
                        " 			  WHEN datetypeid = 9  THEN '' " & _
                        " 			  WHEN datetypeid = 10 THEN '' " & _
                        " 			  WHEN datetypeid = 11 THEN '' " & _
                        "		 END AS Setting " & _
                        "		,email AS Email " & _
                        "		,@User AS Mode " & _
                        "		,datetypeid " & _
                        "		,cfemail_queue_tran.companyunkid " & _
                        "	FROM cfemail_queue_tran " & _
                        "	JOIN hrmsConfiguration..cfuser_master ON hrmsConfiguration..cfuser_master.userunkid = cfemail_queue_tran.recipientunkid " & _
                        "	WHERE issent = 1 AND ISNULL(fail_reason,'') <> '' AND is_user = 1 " & _
                        ")ANTF " & _
                        "	LEFT JOIN hrmsConfiguration..cfconfiguration AS Letter ON ANTF.companyunkid = Letter.companyunkid  AND antf.TmpId = Letter.key_name " & _
                        "	LEFT JOIN hrmsConfiguration..cfconfiguration AS Sett ON ANTF.companyunkid = Sett.companyunkid  AND antf.Setting = Sett.key_name " & _
                        "WHERE 1 = 1 ORDER BY Mode,EName "

                iDataOperation.AddParameter("@APPOINTED_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 29, "Appointment Date"))
                iDataOperation.AddParameter("@CONFIRM_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 30, "Confirmation Date"))
                iDataOperation.AddParameter("@BIRTH_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 31, "Birthdate"))
                iDataOperation.AddParameter("@SUSPENSION_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 32, "Suspension Date"))
                iDataOperation.AddParameter("@PROBATION_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 33, "Probation End Date"))
                iDataOperation.AddParameter("@EOC_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 34, "End Of Contract Date"))
                iDataOperation.AddParameter("@LEAVING_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 35, "Leaving Date"))
                iDataOperation.AddParameter("@RETIREMENT_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 36, "Retirement Date"))
                iDataOperation.AddParameter("@REINSTATEMENT_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 37, "Reinstatement Date"))
                iDataOperation.AddParameter("@MARRIAGE_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 38, "Married Date"))
                iDataOperation.AddParameter("@FIRST_APPOINTMENT_DATE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 39, "First Appointment Date"))
                iDataOperation.AddParameter("@Employee", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 40, "Employee"))
                iDataOperation.AddParameter("@User", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 41, "User"))
                iDataOperation.AddParameter("@Both", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 42, "Both(Employee & User)"))

                dsList = iDataOperation.ExecQuery(StrQ, "List")

                If iDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(iDataOperation.ErrorNumber & " : " & iDataOperation.ErrorMessage)
                    Throw exForce
                End If

                iDataTable = dsList.Tables(0).Copy

                For Each dRow As DataRow In iDataTable.Rows
                    StrQ = "DECLARE @iTable TABLE (Id int, Data NVARCHAR(MAX)); " & _
                           "WITH Split(stpos,endpos) " & _
                           "AS " & _
                           "( " & _
                           "    SELECT 0 AS stpos, CHARINDEX('|','" & dRow.Item("Setting").ToString.Trim & "') AS endpos " & _
                           "UNION ALL " & _
                           "    SELECT endpos+1, CHARINDEX('|','" & dRow.Item("Setting").ToString.Trim & "',endpos+1) FROM Split WHERE endpos > 0 " & _
                           ") INSERT @iTable(Id, Data) " & _
                           "  SELECT " & _
                           "     ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS Id " & _
                           "    ,SUBSTRING('" & dRow.Item("Setting").ToString.Trim & "',stpos,COALESCE(NULLIF(endpos,0),LEN('" & dRow.Item("Setting").ToString.Trim & "')+1)-stpos) AS Data " & _
                           " FROM Split " & _
                           " SELECT Data FROM @iTable WHERE Id = 3 "

                    dsList = iDataOperation.ExecQuery(StrQ, "List")

                    If iDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(iDataOperation.ErrorNumber & " : " & iDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    If dsList.Tables(0).Rows.Count > 0 Then
                        dRow.Item("Email_Subject") = dsList.Tables(0).Rows(0).Item("Data")
                        iDataTable.AcceptChanges()
                    End If
                Next
            End Using
            Return iDataTable
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "AutoNotification_Data", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP [ 30 OCT 2013 ] -- END


    'Pinkal (01-Jun-2021)-- Start
    'New UI Self Service Enhancement : Working on New UI Dashboard Settings.
    Private Function UpdateLeaveData(ByVal dtTable As DataTable, ByVal dr As DataRow, ByVal iMonth As Integer, ByVal i As Integer, ByVal startdate As Date) As Boolean
        Dim mblnFlag As Boolean = False
        Try
            If dtTable IsNot Nothing AndAlso dtTable.Rows.Count > 0 Then
                Dim drRow = dtTable.AsEnumerable().Where(Function(x) x.Field(Of Integer)("LId") = dr("LId"))
                If drRow IsNot Nothing AndAlso drRow.Count > 0 Then
                    drRow(0)(MonthName(iMonth, True) + "-" + Year(startdate.AddMonths(i - 1)).ToString()) = dr("TotalLeave")
                Else
                    drRow(0)(MonthName(iMonth, True) + "-" + Year(startdate.AddMonths(i - 1)).ToString()) = 0
                End If
            End If
            dtTable.AcceptChanges()
            mblnFlag = True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateLeaveData; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function
    'Pinkal (01-Jun-2021) -- End


    'Pinkal (14-Feb-2022) -- Start
    'Enhancement TRA : TnA Module Enhancement for TRA.
    Public Shared Function GetTodayTnADetails(ByVal strDatabaseName As String _
                                                                 , ByVal intCompanyUnkid As Integer _
                                                                 , ByVal intYearUnkId As Integer _
                                                                 , ByVal intuserUnkid As Integer _
                                                                 , ByVal dtPeriodStart As Date _
                                                                 , ByVal dtPeriodEnd As Date _
                                                                 , ByVal strUserModeSetting As String _
                                                                 , ByVal blnOnlyApproved As Boolean _
                                                                 , ByVal blnApplyUserAccessFilter As Boolean _
                                                                 , Optional ByVal strAdvanceFilter As String = "" _
                                                                 , Optional ByVal strORQueryForUserAccess As String = "") As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = String.Empty
        Try

            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""

            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, dtPeriodEnd, dtPeriodEnd, , , strDatabaseName)

            If blnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, dtPeriodEnd, blnOnlyApproved, strDatabaseName, intuserUnkid, intCompanyUnkid, intYearUnkId, strUserModeSetting, , , strORQueryForUserAccess)
            End If

            Dim objDataOperation As New clsDataOperation


            StrQ = " SELECT hremployee_master.employeeunkid " & _
                      ", ISNULL(hremployee_master.employeecode, '')  + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') AS Employee " & _
                      ", CONVERT(VARCHAR(8), tnalogin_tran.logindate, 112) AS login_date " & _
                      ", RTRIM(LTRIM(ISNULL(RIGHT(CONVERT(VARCHAR(20), MIN(tnalogin_tran.checkintime), 100),8), ''))) checkintime " & _
                      ", RTRIM(LTRIM(ISNULL(RIGHT(CONVERT(VARCHAR(20), MAX(tnalogin_tran.checkouttime), 100),8), ''))) checkouttime " & _
                      ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(tnalogin_tran.workhour) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (SUM(tnalogin_tran.workhour) % 3600) / 60), 2), '00:00') AS workhour " & _
                      ", ISNULL(RIGHT('00' + CONVERT(VARCHAR(max), SUM(tnalogin_tran.breakhr) / 3600), 2) + ':' + RIGHT('00' + CONVERT(VARCHAR(2), (SUM(tnalogin_tran.breakhr) % 3600) / 60), 2), '00:00') AS breakhr " & _
                      ", lvleavetype_master.color " & _
                      ", ISNULL(lvleavetype_master.leavetypeunkid,0) AS leavetypeunkid " & _
                      ", CASE WHEN ISNULL(lvleavetype_master.leavetypeunkid,0) > 0 THEN lvleavetype_master.leavetypecode " & _
                      "           WHEN ISNULL(HL.employeeunkid,0) > 0 THEN @Holiday " & _
                      "           WHEN ISNULL(SUM(tnalogin_tran.workhour),0) <= 0  THEN @Absent " & _
                      "           WHEN ISNULL(EOFF.employeeunkid,0) > 0 THEN @DayOff " & _
                      "           WHEN ISNULL(SUM(tnalogin_tran.workhour),0) > 0 THEN @Present " & _
                      " END  Status " & _
                      " FROM  hremployee_master " & _
                      " LEFT JOIN tnalogin_tran ON hremployee_master.employeeunkid = tnalogin_tran.employeeunkid AND CONVERT(VARCHAR(8),tnalogin_tran.logindate, 112)  = @Date " & _
                      " LEFT JOIN " & _
                      " (  " & _
                      "         SELECT lvleaveIssue_master.employeeunkid,lvleaveIssue_master.leavetypeunkid " & _
                      "         FROM lvleaveIssue_tran " & _
                      "         LEFT JOIN lvleaveIssue_master ON lvleaveIssue_master.leaveissueunkid = lvleaveIssue_tran.leaveissueunkid " & _
                      "         WHERE lvleaveIssue_tran.isvoid = 0 AND lvleaveIssue_master.isvoid = 0 AND CONVERT(VARCHAR(8),lvleaveIssue_tran.leavedate, 112)  = @Date " & _
                      " ) AS lv on lv.employeeunkid = hremployee_master.employeeunkid " & _
                      " LEFT JOIN lvleavetype_master ON lvleavetype_master.leavetypeunkid = lv.leavetypeunkid " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "     SELECT lvemployee_holiday.employeeunkid,lvemployee_holiday.holidayunkid " & _
                      "     FROM lvemployee_holiday " & _
                      "     LEFT JOIN lvholiday_master ON lvholiday_master.holidayunkid = lvemployee_holiday.holidayunkid " & _
                      "     WHERE CONVERT(VARCHAR(8),lvholiday_master.holidaydate, 112)  = @Date " & _
                      " ) AS HL ON HL.employeeunkid = hremployee_master.employeeunkid " & _
                      " LEFT JOIN " & _
                      " ( " & _
                      "     SELECT hremployee_dayoff_tran.employeeunkid " & _
                      "     FROM hremployee_dayoff_tran " & _
                      "     WHERE isvoid = 0 AND CONVERT(VARCHAR(8),hremployee_dayoff_tran.dayoffdate, 112)  = @Date " & _
                      " ) AS EOFF ON EOFF.employeeunkid = hremployee_master.employeeunkid "

            If xDateJoinQry.Trim.Length > 0 Then
                StrQ &= " " & xDateJoinQry & " "
            End If

            If xUACQry.Trim.Length > 0 Then
                StrQ &= " " & xUACQry & " "
            End If

            StrQ &= " WHERE CONVERT(CHAR(8), hremployee_master.appointeddate, 112) <= @Date "

            If xDateFilterQry.Trim.Length > 0 Then
                StrQ &= " " & xDateFilterQry & " "
            End If

            StrQ &= " GROUP BY ISNULL(hremployee_master.employeecode, '')  + ' - ' + ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.surname, '') " & _
                         ", hremployee_master.employeeunkid " & _
                         ", HL.employeeunkid " & _
                         ", EOFF.employeeunkid " & _
                         ", tnalogin_tran.logindate " & _
                         ", lvleavetype_master.leavetypeunkid " & _
                         ", lvleavetype_master.leavetypecode " & _
                         ", lvleavetype_master.color "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtPeriodEnd))
            objDataOperation.AddParameter("@Holiday", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 71, "HL"))
            objDataOperation.AddParameter("@Absent", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 72, "AB"))
            objDataOperation.AddParameter("@DayOff", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 73, "OFF"))
            objDataOperation.AddParameter("@Present", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 74, "PR"))

            dsList = objDataOperation.ExecQuery(StrQ, "TnADetails")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetTodayTnADetails; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function
    'Pinkal (14-Feb-2022) -- End


    'Pinkal (30-Sep-2023) -- Start
    '(A1X-1286) Toyota - MSS dashboard card with vacancy status - closed, filled, Open (Manager)
    Public Shared Function GetVacanciesFromCurrentFY(ByVal mdtFYStartDate As Date, ByVal mdtAsonDate As Date) As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = String.Empty
        Dim objDataOperation As clsDataOperation
        Try


            StrQ = " SELECT DISTINCT " & _
                       " rcvacancy_master.vacancyunkid " & _
                       " ,rcvacancy_master.vacancytitle " & _
                       " ,ISNULL(cfcommon_master.name, '') AS VacancyName " & _
                       " ,ISNULL(cfcommon_master.name, '') + ' [' + LTRIM(RTRIM(CONVERT(CHAR(12), rcvacancy_master.openingdate, 106))) + ' - ' + LTRIM(RTRIM(CONVERT(CHAR(12), rcvacancy_master.closingdate, 106))) + '] ' AS Vacancy " & _
                       " ,CASE WHEN rcinterviewanalysis_master.iseligible = 1 AND rcinterviewanalysis_master.statustypid = 1 AND rcinterviewanalysis_master.iscomplete = 1 THEN 2 " & _
                       "          WHEN CONVERT(CHAR(10), rcvacancy_master.closingdate, 112) >= CONVERT(CHAR(10), GETDATE(), 112) THEN 0 " & _
                       "          WHEN CONVERT(CHAR(10), rcvacancy_master.closingdate, 112) < CONVERT(CHAR(10), GETDATE(), 112) THEN 1 " & _
                       " END AS priority " & _
                       " ,CASE WHEN rcinterviewanalysis_master.iseligible = 1 AND rcinterviewanalysis_master.statustypid = 1 AND rcinterviewanalysis_master.iscomplete = 1 THEN @Filled " & _
                       "           WHEN CONVERT(CHAR(10), rcvacancy_master.closingdate, 112) >= CONVERT(CHAR(10), GETDATE(), 112) THEN @Open " & _
                       "           WHEN CONVERT(CHAR(10), rcvacancy_master.closingdate, 112) < CONVERT(CHAR(10), GETDATE(), 112) THEN  @Closed " & _
                       " END AS Status " & _
                       " FROM rcvacancy_master " & _
                       " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & _
                       " LEFT JOIN rcinterviewanalysis_master ON rcinterviewanalysis_master.vacancyunkid = rcvacancy_master.vacancyunkid AND rcinterviewanalysis_master.isvoid = 0 " & _
                       " AND rcinterviewanalysis_master.iseligible = 1 AND rcinterviewanalysis_master.statustypid = 1 AND rcinterviewanalysis_master.iscomplete = 1 " & _
                       " WHERE rcvacancy_master.isvoid = 0 AND (CONVERT(CHAR(10), rcvacancy_master.openingdate, 112) BETWEEN @StartDate AND @EndDate " & _
                       " OR CONVERT(CHAR(10), rcvacancy_master.closingdate, 112) BETWEEN @StartDate AND @EndDate) " & _
                       " ORDER BY CASE WHEN rcinterviewanalysis_master.iseligible = 1 AND rcinterviewanalysis_master.statustypid = 1 AND rcinterviewanalysis_master.iscomplete = 1 THEN 2 " & _
                       "                         WHEN CONVERT(CHAR(10), rcvacancy_master.closingdate, 112) >= CONVERT(CHAR(10), GETDATE(), 112) THEN 0 " & _
                       "                         WHEN CONVERT(CHAR(10), rcvacancy_master.closingdate, 112) < CONVERT(CHAR(10), GETDATE(), 112) THEN 1 END, Vacancy "


            objDataOperation = New clsDataOperation
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@Filled", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 95, "Filled"))
            objDataOperation.AddParameter("@Open", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 96, "Open"))
            objDataOperation.AddParameter("@Closed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 97, "Closed"))
            objDataOperation.AddParameter("@StartDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtFYStartDate))
            objDataOperation.AddParameter("@EndDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(mdtAsonDate))
            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetVacanciesFromCurrentFY; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function
    'Pinkal (30-Sep-2023) -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "NEWLY HIRED")
			Language.setMessage(mstrModuleName, 2, "TERMINATED")
			Language.setMessage(mstrModuleName, 3, "TOTAL ACTIVE EMPLOYEE")
			Language.setMessage(mstrModuleName, 4, "Department")
			Language.setMessage(mstrModuleName, 5, "Branch")
			Language.setMessage(mstrModuleName, 6, "Cost Center")
			Language.setMessage(mstrModuleName, 7, "Section")
			Language.setMessage(mstrModuleName, 9, "Unit")
			Language.setMessage(mstrModuleName, 10, "Send Mail")
			Language.setMessage(mstrModuleName, 11, "Birthday")
			Language.setMessage(mstrModuleName, 12, "Anniversary")
			Language.setMessage(mstrModuleName, 13, "Total Bank Salary")
			Language.setMessage(mstrModuleName, 14, "Total Cash Salary")
			Language.setMessage(mstrModuleName, 15, "Total Hold Salary")
			Language.setMessage(mstrModuleName, 16, "SELECT")
			Language.setMessage(mstrModuleName, 17, "PROBATION WITHIN 30 DAYS FROM NOW.")
			Language.setMessage(mstrModuleName, 18, "SUSPENSION WITHIN 30 DAYS FROM NOW.")
			Language.setMessage(mstrModuleName, 19, "TOTAL APPOINTED WITHIN 30 DAYS FROM NOW.")
			Language.setMessage(mstrModuleName, 20, "TOTAL CONFIRMED WITHIN 30 DAYS FROM NOW.")
			Language.setMessage(mstrModuleName, 21, "TODAY'S BIRTHDAY.")
			Language.setMessage(mstrModuleName, 22, "TODAY'S ANNIVERSARY.")
			Language.setMessage(mstrModuleName, 23, "TOTAL CONTRACTS ENDING WITHIN 30 DAYS FROM NOW.")
			Language.setMessage(mstrModuleName, 24, "TODAY'S RETIREMENT.")
			Language.setMessage(mstrModuleName, 25, "TOTAL FORCASTED RETIREMENT WITHIN")
			Language.setMessage(mstrModuleName, 26, " MONTH(S) FROM NOW.")
			Language.setMessage(mstrModuleName, 27, " YEAR(S) FROM NOW.")
			Language.setMessage(mstrModuleName, 28, "TOTAL FORCASTED CONTRACTS ENDING WITHIN")
			Language.setMessage(mstrModuleName, 29, "Appointment Date")
			Language.setMessage(mstrModuleName, 30, "Confirmation Date")
			Language.setMessage(mstrModuleName, 31, "Birthdate")
			Language.setMessage(mstrModuleName, 32, "Suspension Date")
			Language.setMessage(mstrModuleName, 33, "Probation End Date")
			Language.setMessage(mstrModuleName, 34, "End Of Contract Date")
			Language.setMessage(mstrModuleName, 35, "Leaving Date")
			Language.setMessage(mstrModuleName, 36, "Retirement Date")
			Language.setMessage(mstrModuleName, 37, "Reinstatement Date")
			Language.setMessage(mstrModuleName, 38, "Married Date")
			Language.setMessage(mstrModuleName, 39, "First Appointment Date")
			Language.setMessage(mstrModuleName, 40, "Employee")
			Language.setMessage(mstrModuleName, 41, "User")
			Language.setMessage(mstrModuleName, 42, "Both(Employee & User)")
			Language.setMessage(mstrModuleName, 43, "TOTAL FORCASTED EMPLOYEE LEAVE CYCLE ENDING WITHIN")
			Language.setMessage(mstrModuleName, 44, " DAY(S) FROM NOW.")
			Language.setMessage(mstrModuleName, 45, "LEAVE TYPE(S) PENDING FOR ACCRUAL")
			Language.setMessage(mstrModuleName, 46, "RE-HIRED")
			Language.setMessage(mstrModuleName, 47, "Approve Score Card")
			Language.setMessage(mstrModuleName, 48, "Submit Score Card")
			Language.setMessage(mstrModuleName, 49, "Approve Update Progress")
			Language.setMessage(mstrModuleName, 50, "My Assessment")
			Language.setMessage(mstrModuleName, 51, "My Competence Assessment")
			Language.setMessage(mstrModuleName, 52, "Assess Employee")
			Language.setMessage(mstrModuleName, 53, "Assess Employee Competence")
			Language.setMessage(mstrModuleName, 54, "Review Employee Assessment")
			Language.setMessage(mstrModuleName, 55, "Review Employee Competence")
			Language.setMessage(mstrModuleName, 56, "Approve Leave")
			Language.setMessage(mstrModuleName, 57, "Approve OT Application")
			Language.setMessage(mstrModuleName, 58, "Approve Claim Expense")
			Language.setMessage(mstrModuleName, 59, "Asset Declaration")
			Language.setMessage(mstrModuleName, 60, "Set Score Card")
            Language.setMessage(mstrModuleName, 61, "Non-Disclosure Declaration")
			Language.setMessage(mstrModuleName, 62, "Approve Salary Change")
			Language.setMessage(mstrModuleName, 63, "Approve Payslip Payment")
			Language.setMessage(mstrModuleName, 64, "Approve Loan Application")
			Language.setMessage(mstrModuleName, 65, "Approve Staff Requisition")
			Language.setMessage(mstrModuleName, 66, "Approve ClaimRetirement")
			Language.setMessage(mstrModuleName, 67, "Approve Budget Timesheet")
			Language.setMessage(mstrModuleName, 68, "Approve Shortlisting Criteria")
			Language.setMessage(mstrModuleName, 69, "Approve Eligible Applicants")
			Language.setMessage(mstrModuleName, 70, "Approve Calibration")
			Language.setMessage(mstrModuleName, 71, "HL")
			Language.setMessage(mstrModuleName, 72, "AB")
			Language.setMessage(mstrModuleName, 73, "OFF")
			Language.setMessage(mstrModuleName, 74, "PR")
			Language.setMessage(mstrModuleName, 75, "Department Group")
			Language.setMessage(mstrModuleName, 76, "Section Group")
			Language.setMessage(mstrModuleName, 77, "Unit Group")
			Language.setMessage(mstrModuleName, 78, "Unit")
			Language.setMessage(mstrModuleName, 79, "Team")
			Language.setMessage(mstrModuleName, 80, "Class Group")
			Language.setMessage(mstrModuleName, 81, "Class")
			Language.setMessage(mstrModuleName, 82, "Job Group")
			Language.setMessage(mstrModuleName, 83, "Job")
			Language.setMessage(mstrModuleName, 84, "Grade Group")
			Language.setMessage(mstrModuleName, 85, "Grade")
			Language.setMessage(mstrModuleName, 86, "Grade Group Level")
			Language.setMessage(mstrModuleName, 87, "Cost Center")
			Language.setMessage(mstrModuleName, 88, "BIRTHDAYS WITHIN")
			Language.setMessage(mstrModuleName, 90, "WORK ANNIVERSARIES WITHIN")
			Language.setMessage(mstrModuleName, 91, "TODAY'S WORK ANNIVERSARIES.")
			Language.setMessage(mstrModuleName, 92, "WORK RESIDENCE PERMIT EXPIRY WITHIN")
			Language.setMessage(mstrModuleName, 93, "TODAY'S RESIDENCE PERMIT EXPIRY.")
			Language.setMessage(mstrModuleName, 94, "TOTAL LEAVERS AS ON DATE FROM CURRENT F.Y")
			Language.setMessage(mstrModuleName, 95, "Filled")
			Language.setMessage(mstrModuleName, 96, "Open")
			Language.setMessage(mstrModuleName, 97, "Closed")
            Language.setMessage(mstrModuleName, 98, "TOTAL PAYROLL EXEMPTIONS: CURRENT & NEXT MONTH")
			Language.setMessage(mstrModuleName, 1065, "My Training Feedback")
			Language.setMessage(mstrModuleName, 1066, "Evaluate Employee Training")
			Language.setMessage(mstrModuleName, 11111, "RE-HIRED")

		Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class