﻿'************************************************************************************************************************************
'Class Name : clsStation.vb
'Purpose    :
'Date       :26/06/2010
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsStation
    Private Shared ReadOnly mstrModuleName As String = "clsStation"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintStationunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty


    'Anjan (09 Jun 2011)-Start
    'Issue : Enhancement of FINCA requested by Rutta.
    Private mstrAddress1 As String = String.Empty
    Private mstrAddress2 As String = String.Empty
    Private mintCityunkid As Integer
    Private mintPostalunkid As Integer
    Private mintStateunkid As Integer
    Private mintCountryunkid As Integer
    Private mstrPhone1 As String = String.Empty
    Private mstrPhone2 As String = String.Empty
    Private mstrPhone3 As String = String.Empty
    Private mstrFax As String = String.Empty
    Private mstrEmail As String = String.Empty
    Private mstrWebsite As String = String.Empty
    Private mstrCountryName As String = String.Empty
    Private mstrStateName As String = String.Empty
    Private mstrCityName As String = String.Empty
    Private mstrPostCode As String = String.Empty

    'Anjan (09 Jun 2011)-End 


#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set stationunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Stationunkid() As Integer
        Get
            Return mintStationunkid
        End Get
        Set(ByVal value As Integer)
            mintStationunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property

    'Anjan (09 Jun 2011)-Start
    'Issue : Enhancement of FINCA requested by Rutta.
    Public Property _Address1() As String
        Get
            Return mstrAddress1
        End Get
        Set(ByVal value As String)
            mstrAddress1 = value
        End Set
    End Property

    Public Property _Address2() As String
        Get
            Return mstrAddress2
        End Get
        Set(ByVal value As String)
            mstrAddress2 = value
        End Set
    End Property

    Public Property _Cityunkid() As Integer
        Get
            Return mintCityunkid
        End Get
        Set(ByVal value As Integer)
            mintCityunkid = value
        End Set
    End Property

    Public Property _Postalunkid() As Integer
        Get
            Return mintPostalunkid
        End Get
        Set(ByVal value As Integer)
            mintPostalunkid = value
        End Set
    End Property

    Public Property _Stateunkid() As Integer
        Get
            Return mintStateunkid
        End Get
        Set(ByVal value As Integer)
            mintStateunkid = value
        End Set
    End Property

    Public Property _Countryunkid() As Integer
        Get
            Return mintCountryunkid
        End Get
        Set(ByVal value As Integer)
            mintCountryunkid = value
        End Set
    End Property

    Public Property _Phone1() As String
        Get
            Return mstrPhone1
        End Get
        Set(ByVal value As String)
            mstrPhone1 = value
        End Set
    End Property

    Public Property _Phone2() As String
        Get
            Return mstrPhone2
        End Get
        Set(ByVal value As String)
            mstrPhone2 = value
        End Set
    End Property
    Public Property _Phone3() As String
        Get
            Return mstrPhone3
        End Get
        Set(ByVal value As String)
            mstrPhone3 = value
        End Set
    End Property

    Public Property _Fax() As String
        Get
            Return mstrFax
        End Get
        Set(ByVal value As String)
            mstrFax = value
        End Set
    End Property

    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = value
        End Set
    End Property
    Public Property _Website() As String
        Get
            Return mstrWebsite
        End Get
        Set(ByVal value As String)
            mstrWebsite = value
        End Set
    End Property
    Public Property _Country_Name() As String
        Get
            Return mstrCountryName
        End Get
        Set(ByVal value As String)
            mstrCountryName = value
        End Set
    End Property

    Public Property _State_Name() As String
        Get
            Return mstrStateName
        End Get
        Set(ByVal value As String)
            mstrStateName = value
        End Set
    End Property

    Public Property _City_Name() As String
        Get
            Return mstrCityName
        End Get
        Set(ByVal value As String)
            mstrCityName = value
        End Set
    End Property

    Public Property _Post_Code_No() As String
        Get
            Return mstrPostCode
        End Get
        Set(ByVal value As String)
            mstrPostCode = value
        End Set
    End Property
    'Anjan (09 Jun 2011)-End 

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Anjan (09 Jun 2011)-Start
            'Issue : Enhancement of FINCA requested by Rutta.

            'strQ = "SELECT " & _
            '              "  stationunkid " & _
            '              ", code " & _
            '              ", name " & _
            '              ", description " & _
            '              ", isactive " & _
            '              ", name1 " & _
            '              ", name2 " & _
            '   "FROM hrstation_master " & _
            '        "WHERE stationunkid = @stationunkid "

            strQ &= "SELECT " & _
                          "  hrstation_master.stationunkid " & _
                          ", hrstation_master.code " & _
                          ", hrstation_master.name " & _
                          ", hrstation_master.description " & _
                          ", hrstation_master.isactive " & _
                          ", hrstation_master.name1 " & _
                          ", hrstation_master.name2 " & _
                          ", ISNULL(hrstation_master.address1,'') as address1 " & _
                          ", ISNULL(hrstation_master.address2,'') as address2 " & _
                          ", ISNULL(hrstation_master.cityunkid,0) as cityunkid " & _
                          ", ISNULL(hrstation_master.postalunkid,0) as postalunkid " & _
                          ", ISNULL(hrstation_master.stateunkid,0) as stateunkid " & _
                          ", ISNULL(hrstation_master.countryunkid,0) as countryunkid " & _
                          ", ISNULL(hrstation_master.phone1,'') as phone1" & _
                          ", ISNULL(hrstation_master.phone2,'') as phone2 " & _
                          ", ISNULL(hrstation_master.phone3,'') as phone3 " & _
                          ", ISNULL(hrstation_master.fax,'') as fax" & _
                          ", ISNULL(hrstation_master.email,'') as email" & _
                          ", ISNULL(hrstation_master.website,'') as website " & _
                          ", ISNULL(hrmsConfiguration..cfcountry_master.country_name,'') AS CountryName " & _
                          ", ISNULL(hrmsConfiguration..cfstate_master.name,'') AS StateName " & _
                          ", ISNULL(hrmsConfiguration..cfcity_master.name,'') AS CityName " & _
                          ", ISNULL(hrmsConfiguration..cfzipcode_master.zipcode_no,'') AS PostCode " & _
                    "FROM hrstation_master " & _
                     " LEFT JOIN hrmsConfiguration..cfzipcode_master ON hrstation_master.postalunkid = hrmsConfiguration..cfzipcode_master.zipcodeunkid " & _
                     " LEFT JOIN hrmsConfiguration..cfcity_master ON hrstation_master.cityunkid = hrmsConfiguration..cfcity_master.cityunkid " & _
                     " LEFT JOIN hrmsConfiguration..cfstate_master ON hrstation_master.stateunkid = hrmsConfiguration..cfstate_master.stateunkid " & _
                     " LEFT JOIN hrmsConfiguration..cfcountry_master ON hrstation_master.countryunkid = hrmsConfiguration..cfcountry_master.countryunkid " & _
                    "WHERE stationunkid = @stationunkid "


            'Anjan (09 Jun 2011)-End 

            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintStationunkid = CInt(dtRow.Item("stationunkid"))
                mstrCode = dtRow.Item("code").ToString
                mstrName = dtRow.Item("name").ToString
                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString

                'Anjan (09 Jun 2011)-Start
                'Issue : Enhancement of FINCA requested by Rutta.
                mstrAddress1 = dtRow.Item("address1").ToString
                mstrAddress2 = dtRow.Item("address2").ToString
                mintCityunkid = CInt(dtRow.Item("cityunkid"))
                mintCountryunkid = CInt(dtRow.Item("countryunkid"))
                mintPostalunkid = CInt(dtRow.Item("postalunkid"))
                mintStateunkid = CInt(dtRow.Item("stateunkid"))
                mstrPhone1 = dtRow.Item("phone1").ToString
                mstrPhone2 = dtRow.Item("phone2").ToString
                mstrPhone3 = dtRow.Item("phone3").ToString
                mstrFax = dtRow.Item("fax").ToString
                mstrEmail = dtRow.Item("email").ToString
                mstrWebsite = dtRow.Item("website").ToString
                mstrCountryName = CStr(dtRow.Item("CountryName"))
                mstrStateName = CStr(dtRow.Item("StateName"))
                mstrCityName = CStr(dtRow.Item("CityName"))
                mstrPostCode = CStr(dtRow.Item("PostCode"))
                'Anjan (09 Jun 2011)-End 
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Anjan (09 Jun 2011)-Start
            'Issue :  Enhancement of FINCA requested by Rutta.
            'strQ = "SELECT " & _
            '              "  stationunkid " & _
            '              ", code " & _
            '              ", name " & _
            '              ", description " & _
            '              ", isactive " & _
            '              ", name1 " & _
            '              ", name2 " & _
            '"FROM hrstation_master "

            strQ &= "SELECT " & _
                          "  stationunkid " & _
                          ", code " & _
                          ", name " & _
                          ", description " & _
                          ", isactive " & _
                          ", name1 " & _
                          ", name2 " & _
                          ", ISNULL(address1,'') as address1 " & _
                          ", ISNULL(address2,'') as address2 " & _
                          ", ISNULL(cityunkid,0) as cityunkid " & _
                          ", ISNULL(postalunkid,0) as postalunkid " & _
                          ", ISNULL(stateunkid,0) as stateunkid " & _
                          ", ISNULL(countryunkid,0) as countryunkid " & _
                          ", ISNULL(phone1,'') as phone1 " & _
                          ", ISNULL(phone2,'') as phone2 " & _
                          ", ISNULL(phone3,'') as phone3 " & _
                          ", ISNULL(fax,'') as fax " & _
                          ", ISNULL(email,'') as email " & _
                          ", ISNULL(website,'') as website " & _
                      "FROM hrstation_master "
            'Anjan (09 Jun 2011)-End 

            If blnOnlyActive Then
                strQ &= " WHERE isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrstation_master) </purpose>
    Public Function Insert() As Boolean
        If isExist(mstrCode) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Branch Code is already defined. Please define new Branch Code.")
            Return False
        End If

        If isExist(, mstrName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Branch Name is already defined. Please define new Branch Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@code", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrcode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrdescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrname2.ToString)


            'Anjan (11 Jun 2011)-Start
            'Issue : Enhancement of FINCA requested by Rutta.
            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.ADD_SIZE, mstrAddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.ADD_SIZE, mstrAddress2.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid)
            objDataOperation.AddParameter("@postalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostalunkid)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid)
            objDataOperation.AddParameter("@phone1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone1.ToString)
            objDataOperation.AddParameter("@phone2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone2.ToString)
            objDataOperation.AddParameter("@phone3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone3.ToString)
            objDataOperation.AddParameter("@fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFax.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@website", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebsite.ToString)

            'Anjan (11 Jun 2011)-End 



            'Anjan (11 Jun 2011)-Start
            'Issue :  Enhancement of FINCA requested by Rutta.
            'strQ = "INSERT INTO hrstation_master ( " & _
            '                  "  code " & _
            '                  ", name " & _
            '                  ", description " & _
            '                  ", isactive " & _
            '                  ", name1 " & _
            '                  ", name2 "
            '") VALUES (" & _
            '                  "  @code " & _
            '                  ", @name " & _
            '                  ", @description " & _
            '                  ", @isactive " & _
            '                  ", @name1 " & _
            '                  ", @name2" & _
            '            "); SELECT @@identity"

           
            strQ &= "INSERT INTO hrstation_master ( " & _
                              "  code " & _
                              ", name " & _
                              ", description " & _
                              ", isactive " & _
                              ", name1 " & _
                              ", name2" & _
                    ", address1 " & _
                    ", address2 " & _
                    ", cityunkid " & _
                    ", postalunkid " & _
                    ", stateunkid " & _
                    ", countryunkid " & _
                    ", phone1 " & _
                    ", phone2 " & _
                    ", phone3 " & _
                    ", fax " & _
                    ", email " & _
                    ", website " & _
                        ") VALUES (" & _
                              "  @code " & _
                              ", @name " & _
                              ", @description " & _
                              ", @isactive " & _
                              ", @name1 " & _
                              ", @name2" & _
                              ", @address1 " & _
                              ", @address2 " & _
                              ", @cityunkid " & _
                              ", @postalunkid " & _
                              ", @stateunkid " & _
                              ", @countryunkid " & _
                              ", @phone1 " & _
                              ", @phone2 " & _
                              ", @phone3 " & _
                              ", @fax " & _
                              ", @email " & _
                              ", @website " & _
                        "); SELECT @@identity"

            'Anjan (11 Jun 2011)-End 

            
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintStationunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrstation_master", "stationunkid", mintStationunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrstation_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCode, , mintStationunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Branch Code is already defined. Please define new Branch Code.")
            Return False
        End If

        If isExist(, mstrName, mintStationunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Branch Name is already defined. Please define new Branch Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)


            'Anjan (11 Jun 2011)-Start
            'Issue : Enhancement of FINCA requested by Rutta.
            objDataOperation.AddParameter("@address1", SqlDbType.NVarChar, eZeeDataType.ADD_SIZE, mstrAddress1.ToString)
            objDataOperation.AddParameter("@address2", SqlDbType.NVarChar, eZeeDataType.ADD_SIZE, mstrAddress2.ToString)
            objDataOperation.AddParameter("@cityunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCityunkid)
            objDataOperation.AddParameter("@postalunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPostalunkid)
            objDataOperation.AddParameter("@stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStateunkid)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCountryunkid)
            objDataOperation.AddParameter("@phone1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone1.ToString)
            objDataOperation.AddParameter("@phone2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone2.ToString)
            objDataOperation.AddParameter("@phone3", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPhone3.ToString)
            objDataOperation.AddParameter("@fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFax.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@website", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrWebsite.ToString)

            'Anjan (11 Jun 2011)-End 



            'Anjan (11 Jun 2011)-Start
            'Issue : Enhancement of FINCA requested by Rutta.
            'strQ = "UPDATE hrstation_master SET " & _
            '                  "  code = @code" & _
            '                  ", name = @name" & _
            '                  ", description = @description" & _
            '                  ", isactive = @isactive" & _
            '                  ", name1 = @name1" & _
            '                  ", name2 = @name2 " & _
            '            "WHERE stationunkid = @stationunkid "

            strQ = "UPDATE hrstation_master SET " & _
                              "  code = @code" & _
                              ", name = @name" & _
                              ", description = @description" & _
                              ", isactive = @isactive" & _
                              ", name1 = @name1" & _
                              ", name2 = @name2 " & _
                              ", address1 = @address1 " & _
                              ", address2 = @address2 " & _
                              ", cityunkid = @cityunkid " & _
                              ", postalunkid = @postalunkid " & _
                              ", stateunkid  = @stateunkid  " & _
                              ", countryunkid = @countryunkid " & _
                              ", phone1 = @phone1 " & _
                              ", phone2 = @phone2 " & _
                              ", phone3 = @phone3 " & _
                              ", fax = @fax " & _
                              ", email = @email " & _
                              ", website = @website " & _
                        "WHERE stationunkid = @stationunkid "


            'Anjan (11 Jun 2011)-End 
            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrstation_master", mintStationunkid, "stationunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrstation_master", "stationunkid", mintStationunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrstation_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Branch. Reason : This Branch is already linked with some transaction.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 12 OCT 2011 ] -- START
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 12 OCT 2011 ] -- END 

        Try
            strQ = "UPDATE hrstation_master SET " & _
                     " isactive = 0 " & _
                        "WHERE stationunkid = @stationunkid "

            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE   
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrstation_master", "stationunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 12 OCT 2011 ] -- END 
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Sandeep [ 18 Aug 2010 ] -- Start
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        'Sandeep [ 18 Aug 2010 ] -- End 

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   " TABLE_NAME AS TableName " & _
                   " FROM INFORMATION_SCHEMA.COLUMNS " & _
                   " WHERE COLUMN_NAME='stationunkid' "

            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            strQ = ""
            objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            For Each dtRow As DataRow In dsTables.Tables("List").Rows
                If dtRow.Item("TableName") = "hrstation_master" Then Continue For
                strQ = "SELECT stationunkid FROM " & dtRow.Item("TableName").ToString & " WHERE stationunkid = @stationunkid "
                dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""
            Return blnIsUsed

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Anjan (11 Jun 2011)-Start
            'Issue : Enhancement of FINCA requested by Rutta.
            'strQ = "SELECT " & _
            '              "  stationunkid " & _
            '              ", code " & _
            '              ", name " & _
            '              ", description " & _
            '              ", isactive " & _
            '              ", name1 " & _
            '              ", name2 " & _
            ' "FROM hrstation_master " & _
            ' "WHERE 1=1 "

            strQ = "SELECT " & _
                          "  stationunkid " & _
                          ", code " & _
                          ", name " & _
                          ", description " & _
                          ", isactive " & _
                          ", name1 " & _
                          ", name2 " & _
                        ", address1  " & _
                        ", address2  " & _
                        ", cityunkid " & _
                        ", postalunkid " & _
                        ", stateunkid  " & _
                        ", countryunkid " & _
                        ", phone1 " & _
                        ", phone2 " & _
                        ", phone3 " & _
                        ", fax " & _
                        ", email " & _
                        ", website " & _
             "FROM hrstation_master " & _
             "WHERE 1=1 "
            'Anjan (11 Jun 2011)-End 

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strName.Length > 0 Then
                strQ &= "AND name = @name "
                objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            End If

            If strCode.Length > 0 Then
                strQ &= "AND code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If


            If intUnkid > 0 Then
                strQ &= " AND stationunkid <> @stationunkid"
                objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(Optional ByVal strListName As String = "List", Optional ByVal mblnFlag As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If mblnFlag = True Then
                strQ = "SELECT 0 As stationunkid , @ItemName As  name  UNION "
            End If
            strQ &= "SELECT stationunkid,name FROM hrstation_master WHERE isactive =1 "

            objDataOperation.AddParameter("@ItemName", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        End Try
    End Function

    'Pinkal (10-Mar-2011) --Start

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetStationUnkId(ByVal mstrState As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try

            strQ = " SELECT " & _
                      " stationunkid " & _
                      "  FROM hrstation_master " & _
                      " WHERE name = @name "

            'S.SANDEEP [19 AUG 2016] -- START
            'ISSUE : ISACTIVE WAS NOT KEPT FOR CHECKING
            strQ &= " AND isactive = 1 "
            'S.SANDEEP [19 AUG 2016] -- START

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrState)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dt As DataRow In dsList.Tables(0).Rows
                Return dt("stationunkid")
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetStationUnkId", mstrModuleName)
        End Try
        Return -1

    End Function

    'Pinkal (10-Mar-2011) --End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This Branch Name is already defined. Please define new Branch Name.")
			Language.setMessage(mstrModuleName, 2, "This Branch Code is already defined. Please define new Branch Code.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this Branch. Reason : This Branch is already linked with some transaction.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
