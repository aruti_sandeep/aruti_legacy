﻿'************************************************************************************************************************************
'Class Name : clsassess_subitem_master.vb
'Purpose    :
'Date       :06/01/2012
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsassess_subitem_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_subitem_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintAssess_Subitemunkid As Integer
    Private mintAssessitemunkid As Integer
    Private mstrCode As String = String.Empty
    Private mstrName As String = String.Empty
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assess_subitemunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assess_Subitemunkid() As Integer
        Get
            Return mintAssess_Subitemunkid
        End Get
        Set(ByVal value As Integer)
            mintAssess_Subitemunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessitemunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessitemunkid() As Integer
        Get
            Return mintAssessitemunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessitemunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set code
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Code() As String
        Get
            Return mstrCode
        End Get
        Set(ByVal value As String)
            mstrCode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = Value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            StrQ = "SELECT " & _
              "  assess_subitemunkid " & _
              ", assessitemunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2 " & _
             "FROM hrassess_subitem_master " & _
             "WHERE assess_subitemunkid = @assess_subitemunkid "

            objDataOperation.AddParameter("@assess_subitemunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintAssess_subitemUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintassess_subitemunkid = CInt(dtRow.Item("assess_subitemunkid"))
                mintassessitemunkid = CInt(dtRow.Item("assessitemunkid"))
                mstrcode = dtRow.Item("code").ToString
                mstrname = dtRow.Item("name").ToString
                mstrdescription = dtRow.Item("description").ToString
                mblnisactive = CBool(dtRow.Item("isactive"))
                mstrname1 = dtRow.Item("name1").ToString
                mstrname2 = dtRow.Item("name2").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '                   "  hrassess_subitem_master.assess_subitemunkid " & _
            '                   ", hrassess_subitem_master.assessitemunkid " & _
            '                   ", hrassess_subitem_master.code " & _
            '                   ", hrassess_subitem_master.name " & _
            '                   ", hrassess_subitem_master.description " & _
            '                   ", hrassess_subitem_master.isactive " & _
            '                   ", hrassess_subitem_master.name1 " & _
            '                   ", hrassess_subitem_master.name2 " & _
            '                   ", hrassess_item_master.name as assessitem " & _            
            '                   "FROM hrassess_subitem_master " & _
            '                   " JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_subitem_master.assessitemunkid " & _

            strQ = "SELECT " & _
                   "  hrassess_subitem_master.assess_subitemunkid " & _
                   ", hrassess_subitem_master.assessitemunkid " & _
                   ", hrassess_subitem_master.code " & _
                   ", hrassess_subitem_master.name " & _
                   ", hrassess_subitem_master.description " & _
                   ", hrassess_subitem_master.isactive " & _
                   ", hrassess_subitem_master.name1 " & _
                   ", hrassess_subitem_master.name2 " & _
                   ", hrassess_item_master.name as assessitem " & _
                   ", hrassess_group_master.assessgroupunkid " & _
                   ", hrassess_item_master.periodunkid as periodunkid " & _
                   ", hrassess_item_master.yearunkid " & _
                   "FROM hrassess_subitem_master " & _
                   " JOIN hrassess_item_master ON hrassess_item_master.assessitemunkid = hrassess_subitem_master.assessitemunkid " & _
                   " JOIN hrassess_group_master ON hrassess_group_master.assessgroupunkid = hrassess_item_master.assessgroupunkid "
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            'S.SANDEEP [ 14 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES - ADDED
            '", hrassess_item_master.periodunkid as periodunkid " 
            '", hrassess_item_master.yearunkid "
            'S.SANDEEP [ 14 AUG 2013 ] -- END



            If blnOnlyActive Then
                strQ &= " WHERE hrassess_subitem_master.isactive = 1 "
            End If

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_subitem_master) </purpose>
    Public Function Insert() As Boolean

        If isExist(mstrCode, , , mintAssessitemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
            Return False
        End If

        If isExist(, mstrName, , mintAssessitemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 19 JULY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 19 JULY 2012 ] -- END


        Try
            objDataOperation.AddParameter("@assessitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessitemunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)

            strQ = "INSERT INTO hrassess_subitem_master ( " & _
              "  assessitemunkid " & _
              ", code " & _
              ", name " & _
              ", description " & _
              ", isactive " & _
              ", name1 " & _
              ", name2" & _
            ") VALUES (" & _
              "  @assessitemunkid " & _
              ", @code " & _
              ", @name " & _
              ", @description " & _
              ", @isactive " & _
              ", @name1 " & _
              ", @name2" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssess_Subitemunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_subitem_master", "assess_subitemunkid", mintAssess_Subitemunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            Return True
        Catch ex As Exception
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 19 JULY 2012 ] -- END
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_subitem_master) </purpose>
    Public Function Update() As Boolean
        If isExist(mstrCode, , mintAssess_Subitemunkid, mintAssessitemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
            Return False
        End If

        If isExist(, mstrName, mintAssess_Subitemunkid, mintAssessitemunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        'S.SANDEEP [ 19 JULY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 19 JULY 2012 ] -- END

        Try
            objDataOperation.AddParameter("@assess_subitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssess_Subitemunkid.ToString)
            objDataOperation.AddParameter("@assessitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessitemunkid.ToString)
            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrCode.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName.ToString)
            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, mstrDescription.Length, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrName2.ToString)

            strQ = "UPDATE hrassess_subitem_master SET " & _
                   "  assessitemunkid = @assessitemunkid" & _
                   ", code = @code" & _
                   ", name = @name" & _
                   ", description = @description" & _
                   ", isactive = @isactive" & _
                   ", name1 = @name1" & _
                   ", name2 = @name2 " & _
                   "WHERE assess_subitemunkid = @assess_subitemunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_subitem_master", mintAssess_Subitemunkid, "assess_subitemunkid", 2) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_subitem_master", "assess_subitemunkid", mintAssess_Subitemunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 19 JULY 2012 ] -- END


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_subitem_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        If isUsed(intUnkid) Then
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'mstrMessage = "<Message>"
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this subitem as it is already linked with some transaction(s).")
            'S.SANDEEP [ 19 JULY 2012 ] -- END
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        'S.SANDEEP [ 19 JULY 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        objDataOperation.BindTransaction()
        'S.SANDEEP [ 19 JULY 2012 ] -- END


        Try

            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "DELETE FROM hrassess_subitem_master " & _
            '"WHERE assess_subitemunkid = @assess_subitemunkid "

            strQ = "UPDATE hrassess_subitem_master " & _
                   "SET isactive = 0 " & _
            "WHERE assess_subitemunkid = @assess_subitemunkid "
            'S.SANDEEP [ 19 JULY 2012 ] -- END

            objDataOperation.AddParameter("@assess_subitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_subitem_master", "assess_subitemunkid", intUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 19 JULY 2012 ] -- END


            Return True
        Catch ex As Exception
            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.ReleaseTransaction(False)
            'S.SANDEEP [ 19 JULY 2012 ] -- END
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                   " 'SELECT * FROM ' + TABLE_NAME + ' WHERE '+ COLUMN_NAME +' = ' AS Qry " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS " & _
                   "WHERE COLUMN_NAME='assess_subitemunkid' AND TABLE_NAME <> 'hrassess_subitem_master' "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            For Each dRow As DataRow In dsList.Tables(0).Rows
                strQ = dRow.Item("Qry").ToString & "'" & intUnkid & "' "

                If CInt(objDataOperation.RecordCount(strQ)) > 0 Then Return True

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByVal intAssessItemId As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  assess_subitemunkid " & _
                      ", assessitemunkid " & _
                      ", code " & _
                      ", name " & _
                      ", description " & _
                      ", isactive " & _
                      ", name1 " & _
                      ", name2 " & _
                     "FROM hrassess_subitem_master " & _
                     "WHERE isactive = 1 "

            'Anjan (10 Feb 2012)-Start
            'ENHANCEMENT : TRA COMMENTS on Andrew sir's Request
            strQ &= " AND isactive = 1 "
            'Anjan (10 Feb 2012)-End 

            If strName.Trim.Length > 0 Then
                strQ &= "AND name = @name "
            End If

            If strCode.Trim.Length > 0 Then
                strQ &= "AND code = @code "
            End If

            If intAssessItemId > 0 Then
                strQ &= " AND assessitemunkid = @assessitemunkid "
            End If

            If intUnkid > 0 Then
                strQ &= " AND assess_subitemunkid <> @assess_subitemunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@assess_subitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@assessitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessItemId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(ByVal strList As String, Optional ByVal blnFlag As Boolean = False, Optional ByVal intAssessmentItem As Integer = -1) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnFlag Then
                strQ = "SELECT 0 AS Id, @Select AS Name UNION "
            End If
            strQ &= "SELECT assess_subitemunkid AS Id,name AS Name FROM hrassess_subitem_master Where isactive = 1 "

            If intAssessmentItem > 0 Then
                strQ &= " AND assessitemunkid = '" & intAssessmentItem & "' "
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Get_SubItem_Id(ByVal iCode As String, ByVal iName As String, ByVal iItemId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = "" : Dim exForce As Exception
        Dim iValue As Integer
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT assess_subitemunkid " & _
                   "FROM hrassess_subitem_master WHERE isactive = 1 "

            If iCode.Trim.Length > 0 Then
                StrQ &= " AND code = '" & iCode & "' "
            End If

            If iName.Trim.Length > 0 Then
                StrQ &= " AND name = '" & iName & "' "
            End If

            If iItemId > 0 Then
                StrQ &= " AND assessitemunkid = '" & iItemId & "' "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iValue = dsList.Tables("List").Rows(0).Item("assess_subitemunkid")
            End If

            Return iValue

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Item_Id; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 14 AUG 2013 ] -- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This code is already defined. Please define new code.")
			Language.setMessage(mstrModuleName, 2, "This name is already defined. Please define new name.")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this subitem as it is already linked with some transaction(s).")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class