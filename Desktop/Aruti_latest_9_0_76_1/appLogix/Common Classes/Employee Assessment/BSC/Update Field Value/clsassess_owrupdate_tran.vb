﻿'************************************************************************************************************************************
'Class Name : clsassess_owrupdate_tran.vb
'Purpose    :
'Date       :17-Jun-2015
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsassess_owrupdate_tran
    Private Shared ReadOnly mstrModuleName As String = "clsassess_owrupdate_tran"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintOwrupdatetranunkid As Integer = 0
    Private mdtUpdatedate As Date = Nothing
    Private mintOwnerunkid As Integer = 0
    Private mintPeriodunkid As Integer = 0
    Private mintOwrfieldunkid As Integer = 0
    Private mintFieldunkid As Integer = 0
    Private mdecPct_Completed As Decimal = 0
    Private mintStatusunkid As Integer = 0
    Private mstrRemark As String = String.Empty
    Private mintOwrfieldtypeid As Integer = 0
    Private mintUserunkid As Integer = 0
    Private mblnIsvoid As Boolean = False
    Private mintVoiduserunkid As Integer = 0
    Private mdtVoiddatetime As Date = Nothing
    Private mstrVoidreason As String = String.Empty
    Private xDataOpr As clsDataOperation = Nothing

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set owrupdatetranunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Owrupdatetranunkid() As Integer
        Get
            Return mintOwrupdatetranunkid
        End Get
        Set(ByVal value As Integer)
            mintOwrupdatetranunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set updatedate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Updatedate() As Date
        Get
            Return mdtUpdatedate
        End Get
        Set(ByVal value As Date)
            mdtUpdatedate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ownerunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Ownerunkid() As Integer
        Get
            Return mintOwnerunkid
        End Get
        Set(ByVal value As Integer)
            mintOwnerunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set owrfieldunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Owrfieldunkid() As Integer
        Get
            Return mintOwrfieldunkid
        End Get
        Set(ByVal value As Integer)
            mintOwrfieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Fieldunkid() As Integer
        Get
            Return mintFieldunkid
        End Get
        Set(ByVal value As Integer)
            mintFieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pct_completed
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Pct_Completed() As Decimal
        Get
            Return mdecPct_Completed
        End Get
        Set(ByVal value As Decimal)
            mdecPct_Completed = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set remark
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Remark() As String
        Get
            Return mstrRemark
        End Get
        Set(ByVal value As String)
            mstrRemark = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set owrfieldtypeid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Owrfieldtypeid() As Integer
        Get
            Return mintOwrfieldtypeid
        End Get
        Set(ByVal value As Integer)
            mintOwrfieldtypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOpr = value
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If

        Try
            strQ = "SELECT " & _
              "  owrupdatetranunkid " & _
              ", updatedate " & _
              ", ownerunkid " & _
              ", periodunkid " & _
              ", owrfieldunkid " & _
              ", fieldunkid " & _
              ", pct_completed " & _
              ", statusunkid " & _
              ", remark " & _
              ", owrfieldtypeid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrassess_owrupdate_tran " & _
             "WHERE owrupdatetranunkid = @owrupdatetranunkid "

            objDataOperation.AddParameter("@owrupdatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrupdatetranunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintOwrupdatetranunkid = CInt(dtRow.Item("owrupdatetranunkid"))
                mdtUpdatedate = dtRow.Item("updatedate")
                mintOwnerunkid = CInt(dtRow.Item("ownerunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintOwrfieldunkid = CInt(dtRow.Item("owrfieldunkid"))
                mintFieldunkid = CInt(dtRow.Item("fieldunkid"))
                mdecPct_Completed = dtRow.Item("pct_completed")
                mintStatusunkid = CInt(dtRow.Item("statusunkid"))
                mstrRemark = dtRow.Item("remark").ToString
                mintOwrfieldtypeid = CInt(dtRow.Item("owrfieldtypeid"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = False Then
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                Else
                    mdtVoiddatetime = Nothing
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal iOwnerId As Integer, ByVal iPeriodId As Integer, ByVal iOwrFieldTranId As Integer) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If

        Try
            strQ = "SELECT " & _
                    "  owrupdatetranunkid " & _
                    ", updatedate " & _
                    ", ownerunkid " & _
                    ", periodunkid " & _
                    ", owrfieldunkid " & _
                    ", fieldunkid " & _
                    ", CAST(pct_completed AS DECIMAL(10,2)) AS pct_completed " & _
                    ", statusunkid " & _
                    ", remark " & _
                    ", owrfieldtypeid " & _
                    ", userunkid " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voiddatetime " & _
                    ", voidreason " & _
                    ", CASE WHEN statusunkid = '" & enCompGoalStatus.ST_PENDING & "' THEN @ST_PENDING " & _
                    "       WHEN statusunkid = '" & enCompGoalStatus.ST_INPROGRESS & "' THEN @ST_INPROGRESS " & _
                    "       WHEN statusunkid = '" & enCompGoalStatus.ST_COMPLETE & "' THEN @ST_COMPLETE " & _
                    "       WHEN statusunkid = '" & enCompGoalStatus.ST_CLOSED & "' THEN @ST_CLOSED " & _
                    "       WHEN statusunkid = '" & enCompGoalStatus.ST_ONTRACK & "' THEN @ST_ONTRACK " & _
                    "       WHEN statusunkid = '" & enCompGoalStatus.ST_ATRISK & "' THEN @ST_ATRISK " & _
                    "       WHEN statusunkid = '" & enCompGoalStatus.ST_NOTAPPLICABLE & "' THEN @ST_NOTAPPLICABLE " & _
                    "  END AS dstatus " & _
                    ", CONVERT(CHAR(8),updatedate,112) AS odate " & _
                    ", '' AS ddate " & _
                    "FROM hrassess_owrupdate_tran " & _
                    "WHERE isvoid = 0 "

            If iOwrFieldTranId > 0 Then
                strQ &= " AND owrfieldunkid = '" & iOwrFieldTranId & "' "
            End If

            If iOwnerId > 0 Then
                strQ &= " AND ownerunkid = '" & iOwnerId & "' "
            End If

            If iPeriodId > 0 Then
                strQ &= " AND periodunkid = '" & iPeriodId & "' "
            End If

            strQ &= " ORDER BY updatedate DESC "

            objDataOperation.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 518, "Pending"))
            objDataOperation.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 519, "In progress"))
            objDataOperation.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 520, "Complete"))
            objDataOperation.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 521, "Closed"))
            objDataOperation.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "On Track"))
            objDataOperation.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 523, "At Risk"))
            objDataOperation.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 524, "Not Applicable"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each row As DataRow In dsList.Tables(0).Rows
                row.Item("ddate") = eZeeDate.convertDate(row.Item("odate").ToString).ToShortDateString
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_owrupdate_tran) </purpose>
    Public Function Insert() As Boolean
        'S.SANDEEP |18-JAN-2019| -- START
        'If isExist(mdtUpdatedate, 0, 0, mintOwrfieldunkid, mintOwrfieldtypeid) Then
        '    If xDataOpr Is Nothing Then
        '        mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Progress is already defined for the selected date.")
        '    Else
        '        mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Progress is already defined for the current date.")
        '    End If
        '    Return False
        'End If
        'S.SANDEEP |18-JAN-2019| -- END
        

        If isExist(Nothing, mintStatusunkid, mdecPct_Completed, mintOwrfieldunkid, mintOwrfieldtypeid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Combination of status and percentage is already defined.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If

        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@updatedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtUpdatedate)
            objDataOperation.AddParameter("@ownerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwnerunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@owrfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfieldunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPct_Completed)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@owrfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfieldtypeid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "INSERT INTO hrassess_owrupdate_tran ( " & _
                    "  updatedate " & _
                    ", ownerunkid " & _
                    ", periodunkid " & _
                    ", owrfieldunkid " & _
                    ", fieldunkid " & _
                    ", pct_completed " & _
                    ", statusunkid " & _
                    ", remark " & _
                    ", owrfieldtypeid " & _
                    ", userunkid " & _
                    ", isvoid " & _
                    ", voiduserunkid " & _
                    ", voiddatetime " & _
                    ", voidreason" & _
                    ") VALUES (" & _
                    "  @updatedate " & _
                    ", @ownerunkid " & _
                    ", @periodunkid " & _
                    ", @owrfieldunkid " & _
                    ", @fieldunkid " & _
                    ", @pct_completed " & _
                    ", @statusunkid " & _
                    ", @remark " & _
                    ", @owrfieldtypeid " & _
                    ", @userunkid " & _
                    ", @isvoid " & _
                    ", @voiduserunkid " & _
                    ", @voiddatetime " & _
                    ", @voidreason" & _
                    "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintOwrupdatetranunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_owrupdate_tran", "owrupdatetranunkid", mintOwrupdatetranunkid, , mintUserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_owrupdate_tran) </purpose>
    Public Function Update() As Boolean
        If isExist(mdtUpdatedate, 0, 0, mintOwrfieldunkid, mintOwrfieldtypeid, mintOwrupdatetranunkid) Then
            If xDataOpr Is Nothing Then
                mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, Progress is already defined for the selected date.")
            Else
                mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, Progress is already defined for the current date.")
            End If
            Return False
        End If

        If isExist(Nothing, mintStatusunkid, mdecPct_Completed, mintOwrfieldunkid, mintOwrfieldtypeid, mintOwrupdatetranunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, Combination of status and percentage is already defined.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            objDataOperation.AddParameter("@owrupdatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrupdatetranunkid.ToString)
            objDataOperation.AddParameter("@updatedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtUpdatedate)
            objDataOperation.AddParameter("@ownerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwnerunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@owrfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfieldunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@pct_completed", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPct_Completed)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, mstrRemark.ToString)
            objDataOperation.AddParameter("@owrfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfieldtypeid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            strQ = "UPDATE hrassess_owrupdate_tran SET " & _
                       "  updatedate = @updatedate" & _
                       ", ownerunkid = @ownerunkid" & _
                       ", periodunkid = @periodunkid" & _
                       ", owrfieldunkid = @owrfieldunkid" & _
                       ", fieldunkid = @fieldunkid" & _
                       ", pct_completed = @pct_completed" & _
                       ", statusunkid = @statusunkid" & _
                       ", remark = @remark" & _
                       ", owrfieldtypeid = @owrfieldtypeid" & _
                       ", userunkid = @userunkid" & _
                       ", isvoid = @isvoid" & _
                       ", voiduserunkid = @voiduserunkid" & _
                       ", voiddatetime = @voiddatetime" & _
                       ", voidreason = @voidreason " & _
                   "WHERE owrupdatetranunkid = @owrupdatetranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_owrupdate_tran", mintOwrupdatetranunkid, "owrupdatetranunkid", 2, objDataOperation) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_owrupdate_tran", "owrupdatetranunkid", mintOwrupdatetranunkid, , mintUserunkid) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_owrupdate_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()
        Try
            strQ = "UPDATE hrassess_owrupdate_tran SET " & _
                   "  isvoid = @isvoid" & _
                   ", voiduserunkid = @voiduserunkid" & _
                   ", voiddatetime = @voiddatetime" & _
                   ", voidreason = @voidreason " & _
                   "WHERE owrupdatetranunkid = @owrupdatetranunkid "

            objDataOperation.AddParameter("@owrupdatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_owrupdate_tran", "owrupdatetranunkid", intUnkid, , mintVoiduserunkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(True)
            End If

            Return True
        Catch ex As Exception
            If xDataOpr Is Nothing Then
                objDataOperation.ReleaseTransaction(False)
            End If
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal iDate As Date, ByVal iStatusId As Integer, ByVal iPercent As Decimal, _
                            ByVal iOwrfieldunkid As Integer, ByVal iOwrfieldtypeid As Integer, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As clsDataOperation
        If xDataOpr Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOpr
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  owrupdatetranunkid " & _
              ", updatedate " & _
              ", ownerunkid " & _
              ", periodunkid " & _
              ", owrfieldunkid " & _
              ", fieldunkid " & _
              ", pct_completed " & _
              ", statusunkid " & _
              ", remark " & _
              ", owrfieldtypeid " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrassess_owrupdate_tran " & _
             "WHERE isvoid = 0 "

            If iDate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),updatedate,112) = '" & eZeeDate.convertDate(iDate).ToString & "' "
            End If

            If iStatusId > 0 Then
                strQ &= " AND statusunkid = '" & iStatusId & "' "
            End If

            If iPercent > 0 Then
                strQ &= " AND pct_completed = '" & iPercent & "' "
            End If

            If iOwrfieldunkid > 0 Then
                strQ &= " AND owrfieldunkid = '" & iOwrfieldunkid & "' "
            End If

            If iOwrfieldtypeid > 0 Then
                strQ &= " AND owrfieldtypeid = '" & iOwrfieldtypeid & "' "
            End If

            If intUnkid > 0 Then
                strQ &= " AND owrupdatetranunkid <> @owrupdatetranunkid"
            End If

            objDataOperation.AddParameter("@owrupdatetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOpr Is Nothing Then
                objDataOperation = Nothing
            End If
        End Try
    End Function

    Public Function GetLatestProgress(ByVal iDate As Date, ByVal iOwnerId As Integer, ByVal iPeriodId As Integer) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Using objDataOpr As New clsDataOperation
                StrQ = "SELECT " & _
                       "     A.pct_completed " & _
                       "    ,A.dstatus " & _
                       "    ,A.remark " & _
                       "    ,A.cdate " & _
                       "    ,A.owrfieldunkid " & _
                       "    ,A.owrfieldtypeid " & _
                       "    ,A.fieldunkid " & _
                       "    ,A.statusunkid " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "         CAST(pct_completed AS DECIMAL(10,2)) AS pct_completed " & _
                       "        ,CASE WHEN statusunkid = '" & enCompGoalStatus.ST_PENDING & "' THEN @ST_PENDING " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_INPROGRESS & "' THEN @ST_INPROGRESS " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_COMPLETE & "' THEN @ST_COMPLETE " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_CLOSED & "' THEN @ST_CLOSED " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_ONTRACK & "' THEN @ST_ONTRACK " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_ATRISK & "' THEN @ST_ATRISK " & _
                       "              WHEN statusunkid = '" & enCompGoalStatus.ST_NOTAPPLICABLE & "' THEN @ST_NOTAPPLICABLE " & _
                       "         END AS dstatus " & _
                       "        ,remark " & _
                       "        ,CONVERT(CHAR(8),updatedate,112) AS cdate " & _
                       "        ,statusunkid " & _
                       "        ,owrfieldunkid " & _
                       "        ,fieldunkid " & _
                       "        ,owrfieldtypeid " & _
                       "        ,ROW_NUMBER()OVER(PARTITION BY ownerunkid,periodunkid,owrfieldunkid ORDER BY updatedate DESC) AS RN " & _
                       "    FROM hrassess_owrupdate_tran WHERE isvoid = 0 " & _
                       "        AND ownerunkid = '" & iOwnerId & "' AND periodunkid = '" & iPeriodId & "' AND CONVERT(CHAR(8),updatedate,112) <= '" & eZeeDate.convertDate(iDate).ToString & "' " & _
                       ") AS A WHERE A.RN = 1 "

                objDataOpr.AddParameter("@ST_PENDING", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 518, "Pending"))
                objDataOpr.AddParameter("@ST_INPROGRESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 519, "In progress"))
                objDataOpr.AddParameter("@ST_COMPLETE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 520, "Complete"))
                objDataOpr.AddParameter("@ST_CLOSED", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 521, "Closed"))
                objDataOpr.AddParameter("@ST_ONTRACK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 522, "On Track"))
                objDataOpr.AddParameter("@ST_ATRISK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 523, "At Risk"))
                objDataOpr.AddParameter("@ST_NOTAPPLICABLE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 524, "Not Applicable"))

                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

            End Using
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetLatestProgress; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return dsList
    End Function

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage("clsMasterData", 518, "Pending")
            Language.setMessage("clsMasterData", 519, "In progress")
            Language.setMessage("clsMasterData", 520, "Complete")
            Language.setMessage("clsMasterData", 521, "Closed")
            Language.setMessage("clsMasterData", 522, "On Track")
            Language.setMessage("clsMasterData", 523, "At Risk")
            Language.setMessage("clsMasterData", 524, "Not Applicable")
            Language.setMessage(mstrModuleName, 1, "Sorry, Progress is already defined for the selected date.")
            Language.setMessage(mstrModuleName, 2, "Sorry, Combination of status and percentage is already defined.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class