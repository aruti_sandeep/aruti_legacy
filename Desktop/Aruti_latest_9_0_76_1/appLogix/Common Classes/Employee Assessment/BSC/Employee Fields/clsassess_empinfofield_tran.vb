﻿'************************************************************************************************************************************
'Class Name :clsassess_empinfofield_tran.vb
'Purpose    :
'Date       :14-MAY-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_empinfofield_tran

#Region " Private Variables "

    Private Const mstrModuleName = "clsassess_empinfofield_tran"
    Private mstrMessage As String = ""
    Private mintEmpInfoFieldtranunkid As Integer
    Private mdicInfoField As Dictionary(Of Integer, String)

#End Region

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _dicInfoField() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicInfoField = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Public Function Get_Data(ByVal iEmpFieldUnkid As Integer, ByVal iEmpFieldTypeId As Integer) As Dictionary(Of Integer, String)
        Dim StrQ As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim mdicInfoField As New Dictionary(Of Integer, String)
        Try
            StrQ = "SELECT " & _
                   "  empinfofieldunkid " & _
                   ", empfieldunkid " & _
                   ", fieldunkid " & _
                   ", field_data " & _
                   ", empfieldtypeid " & _
                   "FROM hrassess_empinfofield_tran WITH (NOLOCK) " & _
                   "WHERE empfieldunkid = @empfieldunkid AND empfieldtypeid = @empfieldtypeid "

            objDataOpr.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldUnkid.ToString)
            objDataOpr.AddParameter("@empfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldTypeId.ToString)

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If mdicInfoField.ContainsKey(dRow.Item("fieldunkid")) = False Then
                        mdicInfoField.Add(dRow.Item("fieldunkid"), dRow.Item("field_data"))
                    End If
                Next
            End If

            Return mdicInfoField
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Data", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function InsertDelete_InfoField(ByVal objDataOpr As clsDataOperation, ByVal iUserId As Integer, ByVal iEmpFieldUnkid As Integer, ByVal iEmpFieldTypeId As Integer) As Boolean
        Dim StrQI, StrQU, StrQ As String
        Dim exForce As Exception
        Dim iEmpInfoFieldId As Integer = 0
        Try
            StrQI = "INSERT INTO hrassess_empinfofield_tran ( " & _
                        "  empfieldunkid " & _
                        ", fieldunkid " & _
                        ", field_data " & _
                        ", empfieldtypeid" & _
                    ") VALUES (" & _
                        "  @empfieldunkid " & _
                        ", @fieldunkid " & _
                        ", @field_data " & _
                        ", @empfieldtypeid" & _
                    "); SELECT @@identity"

            StrQU = "UPDATE hrassess_empinfofield_tran SET " & _
                        "  empfieldunkid = @empfieldunkid" & _
                        ", fieldunkid = @fieldunkid" & _
                        ", field_data = @field_data" & _
                        ", empfieldtypeid = @empfieldtypeid " & _
                    "WHERE empinfofieldunkid = @empinfofieldunkid "

            For Each iKey As Integer In mdicInfoField.Keys
                objDataOpr.ClearParameters()
                StrQ = "SELECT empinfofieldunkid FROM hrassess_empinfofield_tran WITH (NOLOCK) WHERE empfieldunkid = '" & iEmpFieldUnkid & "' AND empfieldtypeid = '" & iEmpFieldTypeId & "' AND fieldunkid = '" & iKey & "' "

                Dim dsList As New DataSet
                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    iEmpInfoFieldId = dsList.Tables(0).Rows(0).Item("empinfofieldunkid")
                End If

                If iEmpInfoFieldId > 0 Then
                    objDataOpr.AddParameter("@empinfofieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpInfoFieldId.ToString)
                    objDataOpr.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldUnkid.ToString)
                    objDataOpr.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iKey.ToString)
                    objDataOpr.AddParameter("@field_data", SqlDbType.NVarChar, mdicInfoField(iKey).Length, mdicInfoField(iKey).ToString)
                    objDataOpr.AddParameter("@empfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iempFieldTypeId.ToString)

                    Call objDataOpr.ExecNonQuery(StrQU)

                    If objDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_empinfofield_tran", iEmpInfoFieldId, "empinfofieldunkid", 2, objDataOpr) Then
                        If clsCommonATLog.Insert_AtLog(objDataOpr, 2, "hrassess_empinfofield_tran", "empinfofieldunkid", iEmpInfoFieldId, , iUserId) = False Then
                            exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                Else
                    objDataOpr.AddParameter("@empfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmpFieldUnkid.ToString)
                    objDataOpr.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iKey.ToString)
                    objDataOpr.AddParameter("@field_data", SqlDbType.NVarChar, mdicInfoField(iKey).Length, mdicInfoField(iKey).ToString)
                    objDataOpr.AddParameter("@empfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iempFieldTypeId.ToString)

                    dsList = objDataOpr.ExecQuery(StrQI, "List")

                    If objDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    mintEmpInfoFieldtranunkid = dsList.Tables(0).Rows(0).Item(0)

                    If clsCommonATLog.Insert_AtLog(objDataOpr, 1, "hrassess_empinfofield_tran", "empinfofieldunkid", mintEmpInfoFieldtranunkid, , iUserId) = False Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                End If
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertDelete_InfoField", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class