﻿'************************************************************************************************************************************
'Class Name :clsassess_owrinfofield_tran.vb
'Purpose    :
'Date       :12-MAY-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_owrinfofield_tran

#Region " Private Variables "

    Private Const mstrModuleName = "clsassess_owrinfofield_tran"
    Private mstrMessage As String = ""
    Private mintOwrInfoFieldtranunkid As Integer
    Private mdicInfoField As Dictionary(Of Integer, String)

#End Region

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _dicInfoField() As Dictionary(Of Integer, String)
        Set(ByVal value As Dictionary(Of Integer, String))
            mdicInfoField = value
        End Set
    End Property

#End Region

#Region " Private Methods "

    Public Function Get_Data(ByVal iOwrFieldUnkid As Integer, ByVal iOwrFieldTypeId As Integer) As Dictionary(Of Integer, String)
        Dim StrQ As String = String.Empty
        Dim objDataOpr As New clsDataOperation
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim mdicInfoField As New Dictionary(Of Integer, String)
        Try
            StrQ = "SELECT " & _
                   "  owrinfofieldunkid " & _
                   ", owrfieldunkid " & _
                   ", fieldunkid " & _
                   ", field_data " & _
                   ", owrfieldtypeid " & _
                   "FROM hrassess_owrinfofield_tran " & _
                   "WHERE owrfieldunkid = @owrfieldunkid AND owrfieldtypeid = @owrfieldtypeid "

            objDataOpr.AddParameter("@owrfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iOwrFieldUnkid.ToString)
            objDataOpr.AddParameter("@owrfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iOwrFieldTypeId.ToString)

            dsList = objDataOpr.ExecQuery(StrQ, "List")

            If objDataOpr.ErrorMessage <> "" Then
                exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If mdicInfoField.ContainsKey(dRow.Item("fieldunkid")) = False Then
                        mdicInfoField.Add(dRow.Item("fieldunkid"), dRow.Item("field_data"))
                    End If
                Next
            End If

            Return mdicInfoField
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Data", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

    Public Function InsertDelete_InfoField(ByVal objDataOpr As clsDataOperation, ByVal iUserId As Integer, ByVal iOwrFieldUnkid As Integer, ByVal iOwrFieldTypeId As Integer) As Boolean
        Dim StrQI, StrQU, StrQ As String
        Dim exForce As Exception
        Dim iCoyInfoFieldId As Integer = 0
        Try
            StrQI = "INSERT INTO hrassess_owrinfofield_tran ( " & _
                        "  owrfieldunkid " & _
                        ", fieldunkid " & _
                        ", field_data " & _
                        ", owrfieldtypeid" & _
                    ") VALUES (" & _
                        "  @owrfieldunkid " & _
                        ", @fieldunkid " & _
                        ", @field_data " & _
                        ", @owrfieldtypeid" & _
                    "); SELECT @@identity"

            StrQU = "UPDATE hrassess_owrinfofield_tran SET " & _
                        "  owrfieldunkid = @owrfieldunkid" & _
                        ", fieldunkid = @fieldunkid" & _
                        ", field_data = @field_data" & _
                        ", owrfieldtypeid = @owrfieldtypeid " & _
                    "WHERE owrinfofieldunkid = @owrinfofieldunkid "

            For Each iKey As Integer In mdicInfoField.Keys
                objDataOpr.ClearParameters()
                StrQ = "SELECT owrinfofieldunkid FROM hrassess_owrinfofield_tran WHERE owrfieldunkid = '" & iOwrFieldUnkid & "' AND owrfieldtypeid = '" & iOwrFieldTypeId & "' AND fieldunkid = '" & iKey & "' "

                Dim dsList As New DataSet
                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    iCoyInfoFieldId = dsList.Tables(0).Rows(0).Item("owrinfofieldunkid")
                End If

                If iCoyInfoFieldId > 0 Then
                    objDataOpr.AddParameter("@owrinfofieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iCoyInfoFieldId.ToString)
                    objDataOpr.AddParameter("@owrfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iOwrFieldUnkid.ToString)
                    objDataOpr.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iKey.ToString)
                    objDataOpr.AddParameter("@field_data", SqlDbType.NVarChar, mdicInfoField(iKey).Length, mdicInfoField(iKey).ToString)
                    objDataOpr.AddParameter("@owrfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iOwrFieldTypeId.ToString)

                    Call objDataOpr.ExecNonQuery(StrQU)

                    If objDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_owrinfofield_tran", iCoyInfoFieldId, "owrinfofieldunkid", 2, objDataOpr) Then
                        If clsCommonATLog.Insert_AtLog(objDataOpr, 2, "hrassess_owrinfofield_tran", "owrinfofieldunkid", iCoyInfoFieldId, , iUserId) = False Then
                            exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                Else
                    objDataOpr.AddParameter("@owrfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iOwrFieldUnkid.ToString)
                    objDataOpr.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iKey.ToString)
                    objDataOpr.AddParameter("@field_data", SqlDbType.NVarChar, mdicInfoField(iKey).Length, mdicInfoField(iKey).ToString)
                    objDataOpr.AddParameter("@owrfieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, iOwrFieldTypeId.ToString)

                    dsList = objDataOpr.ExecQuery(StrQI, "List")

                    If objDataOpr.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                    mintOwrInfoFieldtranunkid = dsList.Tables(0).Rows(0).Item(0)

                    If clsCommonATLog.Insert_AtLog(objDataOpr, 1, "hrassess_owrinfofield_tran", "owrinfofieldunkid", mintOwrInfoFieldtranunkid, , iUserId) = False Then
                        exForce = New Exception(objDataOpr.ErrorNumber & ": " & objDataOpr.ErrorMessage)
                        Throw exForce
                    End If

                End If
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertDelete_InfoField", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class
