﻿'************************************************************************************************************************************
'Class Name : clsassess_analysis_master.vb
'Purpose    :
'Date       :13/01/2012
'Written By :Sandeep J. Sharma
'Modified   : ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
''' Developer: Sandeep J. Sharma
''' </summary>

Public Class clsBSC_Analysis_Master
    Private Shared ReadOnly mstrModuleName As String = "clsBSC_Analysis_Master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Dim objBSCAnalysisTran As New clsBSC_analysis_tran

#Region " Private variables "

    Private mintAnalysisunkid As Integer
    Private mintYearunkid As Integer
    Private mintPeriodunkid As Integer
    Private mintSelfemployeeunkid As Integer
    Private mintAssessormasterunkid As Integer
    Private mintAssessoremployeeunkid As Integer
    Private mintAssessedemployeeunkid As Integer
    Private mdtAssessmentdate As Date
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mblnIscommitted As Boolean
    Private mintReviewerunkid As Integer
    Private mintAssessmodeid As Integer    
    Private mintExt_Assessorunkid As Integer
    Private mdicAllItems As New Dictionary(Of Integer, Integer)
    'S.SANDEEP [ 14 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdtCommitteddatetime As Date
    'S.SANDEEP [ 14 JUNE 2012 ] -- END

    'S.SANDEEP [ 28 JAN 2014 ] -- START
    Private mstrWebFrmName As String = String.Empty
    'S.SANDEEP [ 28 JAN 2014 ] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set analysisunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Analysisunkid() As Integer
        Get
            Return mintAnalysisunkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisunkid = value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set selfemployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Selfemployeeunkid() As Integer
        Get
            Return mintSelfemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintSelfemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessormasterunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessormasterunkid() As Integer
        Get
            Return mintAssessormasterunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessormasterunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessoremployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessoremployeeunkid() As Integer
        Get
            Return mintAssessoremployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessoremployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessedemployeeunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessedemployeeunkid() As Integer
        Get
            Return mintAssessedemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessedemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessmentdate
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessmentdate() As Date
        Get
            Return mdtAssessmentdate
        End Get
        Set(ByVal value As Date)
            mdtAssessmentdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set iscommitted
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Iscommitted() As Boolean
        Get
            Return mblnIscommitted
        End Get
        Set(ByVal value As Boolean)
            mblnIscommitted = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reviewerunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Reviewerunkid() As Integer
        Get
            Return mintReviewerunkid
        End Get
        Set(ByVal value As Integer)
            mintReviewerunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessmodeid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Assessmodeid() As Integer
        Get
            Return mintAssessmodeid
        End Get
        Set(ByVal value As Integer)
            mintAssessmodeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ext_assessorunkid
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Ext_Assessorunkid() As Integer
        Get
            Return mintExt_Assessorunkid
        End Get
        Set(ByVal value As Integer)
            mintExt_Assessorunkid = value
        End Set
    End Property

    Public Property _AllItemCount() As Dictionary(Of Integer, Integer)
        Get
            Return mdicAllItems
        End Get
        Set(ByVal value As Dictionary(Of Integer, Integer))
            mdicAllItems = value
        End Set
    End Property

    'S.SANDEEP [ 14 JUNE 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set committeddatetime
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Committeddatetime() As Date
        Get
            Return mdtCommitteddatetime
        End Get
        Set(ByVal value As Date)
            mdtCommitteddatetime = Value
        End Set
    End Property
    'S.SANDEEP [ 14 JUNE 2012 ] -- END

    'S.SANDEEP [ 28 JAN 2014 ] -- START
    Public WriteOnly Property _WebFrmName() As String
        Set(ByVal value As String)
            mstrWebFrmName = value
        End Set
    End Property
    'S.SANDEEP [ 28 JAN 2014 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  analysisunkid " & _
                      ", yearunkid " & _
                      ", periodunkid " & _
                      ", selfemployeeunkid " & _
                      ", assessormasterunkid " & _
                      ", assessoremployeeunkid " & _
                      ", assessedemployeeunkid " & _
                      ", assessmentdate " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", iscommitted " & _
                      ", reviewerunkid " & _
                      ", assessmodeid " & _
                      ", ext_assessorunkid " & _
                      ", committeddatetime " & _
                     "FROM hrbsc_analysis_master " & _
                     "WHERE analysisunkid = @analysisunkid "

            'S.SANDEEP [ 14 JUNE 2012 committeddatetime ] -- START -- END

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAnalysisunkid = CInt(dtRow.Item("analysisunkid"))
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                mintPeriodunkid = CInt(dtRow.Item("periodunkid"))
                mintSelfemployeeunkid = CInt(dtRow.Item("selfemployeeunkid"))
                mintAssessormasterunkid = CInt(dtRow.Item("assessormasterunkid"))
                mintAssessoremployeeunkid = CInt(dtRow.Item("assessoremployeeunkid"))
                mintAssessedemployeeunkid = CInt(dtRow.Item("assessedemployeeunkid"))
                mdtAssessmentdate = dtRow.Item("assessmentdate")
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mblnIsvoid = CBool(dtRow.Item("isvoid"))
                mintVoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason").ToString
                mblnIscommitted = CBool(dtRow.Item("iscommitted"))
                mintReviewerunkid = CInt(dtRow.Item("reviewerunkid"))
                mintAssessmodeid = CInt(dtRow.Item("assessmodeid"))
                mintExt_Assessorunkid = CInt(dtRow.Item("ext_assessorunkid"))
                'S.SANDEEP [ 14 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                If IsDBNull(dtRow.Item("committeddatetime")) = False Then
                    mdtCommitteddatetime = dtRow.Item("committeddatetime")
                Else
                    mdtCommitteddatetime = Nothing
                End If
                'S.SANDEEP [ 14 JUNE 2012 ] -- END
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, _
                            ByVal enAssessMode As enAssessmentMode, _
                            Optional ByVal strIncludeInactiveEmployee As String = "", _
                            Optional ByVal strEmployeeAsOnDate As String = "", _
                            Optional ByVal strUserAccessLevelFilterString As String = "", _
                            Optional ByVal intUserUnkId As Integer = 0) As DataSet 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
        'Public Function GetList(ByVal strTableName As String, ByVal enAssessMode As enAssessmentMode) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                        " hrbsc_analysis_master.analysisunkid " & _
                        ",hrbsc_analysis_master.yearunkid " & _
                        ",hrbsc_analysis_master.periodunkid " & _
                        ",hrbsc_analysis_master.selfemployeeunkid " & _
                        ",hrbsc_analysis_master.assessormasterunkid " & _
                        ",hrbsc_analysis_master.assessoremployeeunkid " & _
                        ",hrbsc_analysis_master.assessedemployeeunkid " & _
                        ",Convert(Char(8),hrbsc_analysis_master.assessmentdate,112) AS assessmentdate " & _
                        ",hrbsc_analysis_master.userunkid " & _
                        ",hrbsc_analysis_master.isvoid " & _
                        ",hrbsc_analysis_master.voiduserunkid " & _
                        ",hrbsc_analysis_master.voiddatetime " & _
                        ",hrbsc_analysis_master.voidreason " & _
                        ",ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') As EmpName " & _
                        ",ISNULL(hrmsConfiguration..cffinancial_year_tran.financialyear_name,'') AS YearName " & _
                        ",ISNULL(cfcommon_period_tran.period_name,'') AS PName " & _
                        ",ISNULL(iscommitted,0) AS iscommitted " & _
                        ",ISNULL(hremployee_master.employeecode,'') AS Ecode " & _
                        ",cfcommon_period_tran.statusid AS Sid "
            'S.SANDEEP [ 05 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= ",hremployee_master.stationunkid " & _
                    ",hremployee_master.deptgroupunkid " & _
                    ",hremployee_master.departmentunkid " & _
                    ",hremployee_master.sectionunkid " & _
                    ",hremployee_master.unitunkid " & _
                    ",hremployee_master.jobgroupunkid " & _
                    ",hremployee_master.jobunkid " & _
                    ",hremployee_master.gradegroupunkid " & _
                    ",hremployee_master.gradeunkid " & _
                    ",hremployee_master.gradelevelunkid " & _
                    ",hremployee_master.classgroupunkid " & _
                    ",hremployee_master.classunkid " & _
                    ",hremployee_master.sectiongroupunkid " & _
                    ",hremployee_master.unitgroupunkid " & _
                    ",hremployee_master.teamunkid "
            'S.SANDEEP [ 05 MARCH 2012 ] -- END

            'S.SANDEEP [ 14 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= ", committeddatetime "
            'S.SANDEEP [ 14 JUNE 2012 ] -- END

            Select Case enAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    strQ &= ", ' ' as Assessor "
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    strQ &= ", CASE WHEN hrexternal_assessor_master.ext_assessorunkid >0 THEN hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END AS Assessor "
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    strQ &= ", ISNULL(Reviewer.firstname,'')+' '+ISNULL(Reviewer.othername,'')+' '+ISNULL(Reviewer.surname,'') AS Reviewer "
            End Select

            strQ &= "FROM hrbsc_analysis_master " & _
                    " JOIN hrmsConfiguration..cffinancial_year_tran on hrmsConfiguration..cffinancial_year_tran.yearunkid = hrbsc_analysis_master.yearunkid " & _
                    " JOIN cfcommon_period_tran on cfcommon_period_tran.periodunkid = hrbsc_analysis_master.periodunkid "

            Select Case enAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    strQ &= " JOIN hremployee_master ON hrbsc_analysis_master.selfemployeeunkid = hremployee_master.employeeunkid "
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    strQ &= " JOIN hremployee_master ON hrbsc_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid " & _
                                       " LEFT JOIN hrexternal_assessor_master ON hrexternal_assessor_master.ext_assessorunkid = hrbsc_analysis_master.ext_assessorunkid " & _
                                       " LEFT JOIN hremployee_master AS AEMP ON AEMP.employeeunkid = hrbsc_analysis_master.assessoremployeeunkid AND hrbsc_analysis_master.ext_assessorunkid = 0 "

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES

                    If strIncludeInactiveEmployee.Trim.Length <= 0 Then
                        strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
                    End If

                    If CBool(strIncludeInactiveEmployee) = False Then
                        strQ &= " AND CONVERT(CHAR(8),AEMP.appointeddate,112) <= @enddate " & _
                                       " AND ISNULL(CONVERT(CHAR(8),AEMP.termination_from_date,112),@startdate) >= @startdate " & _
                                       " AND ISNULL(CONVERT(CHAR(8),AEMP.termination_to_date,112),@startdate) >= @startdate " & _
                                       " AND ISNULL(CONVERT(CHAR(8),AEMP.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END
                    End If

                    strQ &= " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrbsc_analysis_master.assessormasterunkid " & _
                            " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                            " AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
                            " AND hrapprover_usermapping.userunkid = " & IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId) & " AND hrassessor_master.isvoid = 0 "


                    If strUserAccessLevelFilterString = "" Then
                        'Sohail (08 May 2015) -- Start
                        'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                        'strQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "AEMP")
                        'strQ &= NewAccessLevelFilterString().Replace("hremployee_master", "AEMP")
                        'Sohail (08 May 2015) -- End
                    Else
                        strQ &= strUserAccessLevelFilterString.Replace("hremployee_master", "AEMP")
                    End If


                    'S.SANDEEP [ 04 FEB 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                    '    strQ &= " AND CONVERT(CHAR(8),AEMP.appointeddate,112) <= @enddate " & _
                    '                   " AND ISNULL(CONVERT(CHAR(8),AEMP.termination_from_date,112),@startdate) >= @startdate " & _
                    '                   " AND ISNULL(CONVERT(CHAR(8),AEMP.termination_to_date,112),@startdate) >= @startdate "
                    'End If

                    ''S.SANDEEP [ 04 FEB 2012 ] -- START
                    ''ENHANCEMENT : TRA CHANGES

                    ' ''S.SANDEEP [ 23 JAN 2012 ] -- START
                    ' ''ENHANCEMENT : TRA CHANGES : AUTOMATIC ASSIGNMENT OF BENEFITS        
                    ''If UserAccessLevel._AccessLevel.Length > 0 Then
                    ''    strQ &= " AND AEMP.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    ''End If
                    ' ''S.SANDEEP [ 23 JAN 2012 ] -- END
                    'Select Case ConfigParameter._Object._UserAccessModeSetting
                    '    Case enAllocation.BRANCH
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  AEMP.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.DEPARTMENT_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  AEMP.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.DEPARTMENT
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  AEMP.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.SECTION_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  AEMP.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.SECTION
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  AEMP.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.UNIT_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  AEMP.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.UNIT
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  AEMP.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.TEAM
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  AEMP.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.JOB_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  AEMP.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.JOBS
                    'If UserAccessLevel._AccessLevel.Length > 0 Then
                    '    strQ &= " AND AEMP.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    'End If
                    'End Select
                    ''S.SANDEEP [ 04 FEB 2012 ] -- END

                    'strQ &= " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrbsc_analysis_master.assessormasterunkid " & _
                    '                   " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                    '                   " AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
                    '        " AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid & " AND hrassessor_master.isvoid = 0 "
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END



                Case enAssessmentMode.REVIEWER_ASSESSMENT

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'strQ &= " JOIN hremployee_master ON hrbsc_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid " & _
                    '        " JOIN hremployee_master AS Reviewer ON hrbsc_analysis_master.reviewerunkid = Reviewer.employeeunkid " & _
                    '        " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrbsc_analysis_master.assessormasterunkid " & _
                    '        " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                    '        "  AND hrapprover_usermapping.userunkid = " & User._Object._Userunkid & " " & _
                    '        "  AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 "

                    strQ &= " JOIN hremployee_master ON hrbsc_analysis_master.assessedemployeeunkid = hremployee_master.employeeunkid " & _
                            " JOIN hremployee_master AS Reviewer ON hrbsc_analysis_master.reviewerunkid = Reviewer.employeeunkid " & _
                            " JOIN hrassessor_master ON hrassessor_master.assessormasterunkid = hrbsc_analysis_master.assessormasterunkid " & _
                            " JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                            " AND hrapprover_usermapping.userunkid = " & IIf(intUserUnkId = 0, User._Object._Userunkid, intUserUnkId) & " " & _
                            "  AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 "
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

            End Select

            strQ &= " WHERE hrbsc_analysis_master.isvoid = 0 "


            'S.SANDEEP [ 23 JAN 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Select Case enAssessMode
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    strQ &= " AND CASE WHEN hrexternal_assessor_master.ext_assessorunkid >0 THEN hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END <> '' "
            End Select
            'S.SANDEEP [ 23 JAN 2012 ] -- END

            If strIncludeInactiveEmployee.Trim.Length <= 0 Then
                strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
            End If

            If CBool(strIncludeInactiveEmployee) = False Then 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                Select Case enAssessMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                        'S.SANDEEP [ 27 APRIL 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                        'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

                        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                        'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                                " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                        'S.SANDEEP [ 27 APRIL 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                        'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

                        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                        'S.SANDEEP [ 27 APRIL 2012 ] -- END
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        strQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                         " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                         " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                         " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " & _
                         " AND CONVERT(CHAR(8),Reviewer.appointeddate,112) <= @enddate " & _
                         " AND ISNULL(CONVERT(CHAR(8),Reviewer.termination_from_date,112),@startdate) >= @startdate " & _
                         " AND ISNULL(CONVERT(CHAR(8),Reviewer.termination_to_date,112),@startdate) >= @startdate " & _
                         " AND ISNULL(CONVERT(CHAR(8),Reviewer.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                        'S.SANDEEP [ 27 APRIL 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                        'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                        'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

                        objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                        objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                        'S.SANDEEP [ 27 APRIL 2012 ] -- END
                End Select
            End If

            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            '    Select Case enAssessMode
            '        Case enAssessmentMode.SELF_ASSESSMENT
            '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '        Case enAssessmentMode.APPRAISER_ASSESSMENT
            '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
            '        Case enAssessmentMode.REVIEWER_ASSESSMENT
            '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
            '    End Select
            'End If



            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If UserAccessLevel._AccessLevel.Length > 0 Then
            'S.SANDEEP [ 27 APRIL 2012 ] -- END
                Select Case enAssessMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Select Case ConfigParameter._Object._UserAccessModeSetting
                    '    Case enAllocation.BRANCH
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.DEPARTMENT_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.DEPARTMENT
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.SECTION_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.SECTION
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.UNIT_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.UNIT
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.TEAM
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.JOB_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.JOBS
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    'End Select

                    
                    If strUserAccessLevelFilterString = "" Then
                        'Sohail (08 May 2015) -- Start
                        'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                        'strQ &= UserAccessLevel._AccessLevelFilterString
                        'strQ &= NewAccessLevelFilterString()
                        'Sohail (08 May 2015) -- End
                    Else
                        strQ &= strUserAccessLevelFilterString
                                End If
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    Case enAssessmentMode.APPRAISER_ASSESSMENT

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Select Case ConfigParameter._Object._UserAccessModeSetting
                    '    Case enAllocation.BRANCH
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
                    '        End If
                    '    Case enAllocation.DEPARTMENT_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
                    '        End If
                    '    Case enAllocation.DEPARTMENT
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
                    '        End If
                    '    Case enAllocation.SECTION_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
                    '        End If
                    '    Case enAllocation.SECTION
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
                    '        End If
                    '    Case enAllocation.UNIT_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
                    '        End If
                    '    Case enAllocation.UNIT
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
                    '        End If
                    '    Case enAllocation.TEAM
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
                    '        End If
                    '    Case enAllocation.JOB_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
                    '        End If
                    '    Case enAllocation.JOBS
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND reviewerunkid < = 0 "
                    '        End If
                    'End Select

                   
                    If strUserAccessLevelFilterString = "" Then
                        'Sohail (08 May 2015) -- Start
                        'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                        'strQ &= UserAccessLevel._AccessLevelFilterString & " AND reviewerunkid < = 0 "
                        'strQ &= NewAccessLevelFilterString() & " AND reviewerunkid < = 0 "
                        'Sohail (08 May 2015) -- End
                    Else
                        strQ &= strUserAccessLevelFilterString & " AND reviewerunkid < = 0 "
                                End If
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END



                    Case enAssessmentMode.REVIEWER_ASSESSMENT

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'Select Case ConfigParameter._Object._UserAccessModeSetting
                    '    Case enAllocation.BRANCH
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.stationunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.stationunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.DEPARTMENT_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.deptgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.DEPARTMENT
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.departmentunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.SECTION_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.sectiongroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.SECTION
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.sectionunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.UNIT_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.unitgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.UNIT
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.unitunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.unitunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.TEAM
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.teamunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.teamunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.JOB_GROUP
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND  hremployee_master.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.jobgroupunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    '    Case enAllocation.JOBS
                    '        If UserAccessLevel._AccessLevel.Length > 0 Then
                    '            strQ &= " AND hremployee_master.jobunkid IN (" & UserAccessLevel._AccessLevel & ") AND Reviewer.jobunkid IN (" & UserAccessLevel._AccessLevel & ") "
                    '        End If
                    'End Select

                    
                    If strUserAccessLevelFilterString = "" Then
                        'Sohail (08 May 2015) -- Start
                        'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                        'strQ &= UserAccessLevel._AccessLevelFilterString
                        'strQ &= NewAccessLevelFilterString()
                        'Sohail (08 May 2015) -- End
                    Else
                        strQ &= strUserAccessLevelFilterString
                                End If

                    If strUserAccessLevelFilterString = "" Then
                        'Sohail (08 May 2015) -- Start
                        'Enhancement - Get User Access Filter string from new employee transfer table and employee categorization table.
                        'strQ &= UserAccessLevel._AccessLevelFilterString.Replace("hremployee_master", "Reviewer")
                        'strQ &= NewAccessLevelFilterString().Replace("hremployee_master", "Reviewer")
                        'Sohail (08 May 2015) -- End
                    Else
                        strQ &= strUserAccessLevelFilterString.Replace("hremployee_master", "Reviewer")
                                End If
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                        End Select
            'End If
            'S.SANDEEP [ 04 FEB 2012 ] -- END


            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrbsc_analysis_master) </purpose>
    Public Function Insert(Optional ByVal dtTable As DataTable = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [ 10 SEPT 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Select Case mintAssessmodeid
            Case enAssessmentMode.SELF_ASSESSMENT
                If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid) = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 13, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                    Return False
                End If
            Case enAssessmentMode.APPRAISER_ASSESSMENT
                If mintExt_Assessorunkid > 0 Then
                    If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, mintExt_Assessorunkid, , , True) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 13, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                        Return False
                    End If
                Else
                    If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessormasterunkid) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 13, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                        Return False
                    End If
                End If
            Case enAssessmentMode.REVIEWER_ASSESSMENT
                If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, , mintAssessormasterunkid) = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 13, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                    Return False
                End If
        End Select
        'S.SANDEEP [ 10 SEPT 2013 ] -- END

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@selfemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelfemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
            objDataOperation.AddParameter("@assessoremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessoremployeeunkid.ToString)
            objDataOperation.AddParameter("@assessedemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessedemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessmentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAssessmentdate)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscommitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscommitted.ToString)
            objDataOperation.AddParameter("@reviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReviewerunkid.ToString)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessmodeid.ToString)
            objDataOperation.AddParameter("@ext_assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExt_Assessorunkid.ToString)

            'S.SANDEEP [ 14 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdtCommitteddatetime <> Nothing Then
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCommitteddatetime)
            Else
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If

            'strQ = "INSERT INTO hrbsc_analysis_master ( " & _
            '  "  yearunkid " & _
            '  ", periodunkid " & _
            '  ", selfemployeeunkid " & _
            '  ", assessormasterunkid " & _
            '  ", assessoremployeeunkid " & _
            '  ", assessedemployeeunkid " & _
            '  ", assessmentdate " & _
            '  ", userunkid " & _
            '  ", isvoid " & _
            '  ", voiduserunkid " & _
            '  ", voiddatetime " & _
            '  ", voidreason " & _
            '  ", iscommitted " & _
            '  ", reviewerunkid " & _
            '  ", assessmodeid " & _
            '  ", ext_assessorunkid" & _
            '") VALUES (" & _
            '  "  @yearunkid " & _
            '  ", @periodunkid " & _
            '  ", @selfemployeeunkid " & _
            '  ", @assessormasterunkid " & _
            '  ", @assessoremployeeunkid " & _
            '  ", @assessedemployeeunkid " & _
            '  ", @assessmentdate " & _
            '  ", @userunkid " & _
            '  ", @isvoid " & _
            '  ", @voiduserunkid " & _
            '  ", @voiddatetime " & _
            '  ", @voidreason " & _
            '  ", @iscommitted " & _
            '  ", @reviewerunkid " & _
            '  ", @assessmodeid " & _
            '  ", @ext_assessorunkid" & _
            '"); SELECT @@identity"
            strQ = "INSERT INTO hrbsc_analysis_master ( " & _
              "  yearunkid " & _
              ", periodunkid " & _
              ", selfemployeeunkid " & _
              ", assessormasterunkid " & _
              ", assessoremployeeunkid " & _
              ", assessedemployeeunkid " & _
              ", assessmentdate " & _
              ", userunkid " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", iscommitted " & _
              ", reviewerunkid " & _
              ", assessmodeid " & _
              ", ext_assessorunkid" & _
              ", committeddatetime" & _
            ") VALUES (" & _
              "  @yearunkid " & _
              ", @periodunkid " & _
              ", @selfemployeeunkid " & _
              ", @assessormasterunkid " & _
              ", @assessoremployeeunkid " & _
              ", @assessedemployeeunkid " & _
              ", @assessmentdate " & _
              ", @userunkid " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @iscommitted " & _
              ", @reviewerunkid " & _
              ", @assessmodeid " & _
              ", @ext_assessorunkid" & _
              ", @committeddatetime" & _
            "); SELECT @@identity"
            'S.SANDEEP [ 14 JUNE 2012 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAnalysisunkid = dsList.Tables(0).Rows(0).Item(0)

            If dtTable IsNot Nothing Then
                If dtTable.Rows.Count > 0 Then

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    objBSCAnalysisTran._DataOperation = objDataOperation
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    objBSCAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                    objBSCAnalysisTran._DataTable = dtTable
                    If objBSCAnalysisTran.InsertUpdateDelete_AnalysisTran(mintUserunkid) = False Then 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
                        'If objBSCAnalysisTran.InsertUpdateDelete_AnalysisTran() = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrbsc_analysis_master) </purpose>
    Public Function Update(Optional ByVal dtTable As DataTable = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'S.SANDEEP [ 10 SEPT 2013 ] -- START
        'ENHANCEMENT : TRA CHANGES
        Select Case mintAssessmodeid
            Case enAssessmentMode.SELF_ASSESSMENT
                If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, , , mintAnalysisunkid) = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 13, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                    Return False
                End If
            Case enAssessmentMode.APPRAISER_ASSESSMENT
                If mintExt_Assessorunkid > 0 Then
                    If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, mintExt_Assessorunkid, , mintAnalysisunkid, True) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 13, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                        Return False
                    End If
                Else
                    If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, mintAssessormasterunkid, , mintAnalysisunkid) = True Then
                        mstrMessage = Language.getMessage(mstrModuleName, 13, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                        Return False
                    End If
                End If
            Case enAssessmentMode.REVIEWER_ASSESSMENT
                If isExist(mintAssessmodeid, mintSelfemployeeunkid, mintYearunkid, mintPeriodunkid, , mintAssessormasterunkid, mintAnalysisunkid) = True Then
                    mstrMessage = Language.getMessage(mstrModuleName, 13, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
                    Return False
                End If
        End Select
        'S.SANDEEP [ 10 SEPT 2013 ] -- END

        objDataOperation = New clsDataOperation

        Try
            objDataOperation.BindTransaction()

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@selfemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintSelfemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessormasterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessormasterunkid.ToString)
            objDataOperation.AddParameter("@assessoremployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessoremployeeunkid.ToString)
            objDataOperation.AddParameter("@assessedemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessedemployeeunkid.ToString)
            objDataOperation.AddParameter("@assessmentdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAssessmentdate)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)
            objDataOperation.AddParameter("@iscommitted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIscommitted.ToString)
            objDataOperation.AddParameter("@reviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReviewerunkid.ToString)
            objDataOperation.AddParameter("@assessmodeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessmodeid.ToString)
            objDataOperation.AddParameter("@ext_assessorunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExt_Assessorunkid.ToString)

            'S.SANDEEP [ 14 JUNE 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            If mdtCommitteddatetime <> Nothing Then
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtCommitteddatetime)
            Else
                objDataOperation.AddParameter("@committeddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            'strQ = "UPDATE hrbsc_analysis_master SET " & _
            '  "  yearunkid = @yearunkid" & _
            '  ", periodunkid = @periodunkid" & _
            '  ", selfemployeeunkid = @selfemployeeunkid" & _
            '  ", assessormasterunkid = @assessormasterunkid" & _
            '  ", assessoremployeeunkid = @assessoremployeeunkid" & _
            '  ", assessedemployeeunkid = @assessedemployeeunkid" & _
            '  ", assessmentdate = @assessmentdate" & _
            '  ", userunkid = @userunkid" & _
            '  ", isvoid = @isvoid" & _
            '  ", voiduserunkid = @voiduserunkid" & _
            '  ", voiddatetime = @voiddatetime" & _
            '  ", voidreason = @voidreason" & _
            '  ", iscommitted = @iscommitted" & _
            '  ", reviewerunkid = @reviewerunkid" & _
            '  ", assessmodeid = @assessmodeid" & _
            '  ", ext_assessorunkid = @ext_assessorunkid " & _
            '"WHERE analysisunkid = @analysisunkid "
            strQ = "UPDATE hrbsc_analysis_master SET " & _
              "  yearunkid = @yearunkid" & _
              ", periodunkid = @periodunkid" & _
              ", selfemployeeunkid = @selfemployeeunkid" & _
              ", assessormasterunkid = @assessormasterunkid" & _
              ", assessoremployeeunkid = @assessoremployeeunkid" & _
              ", assessedemployeeunkid = @assessedemployeeunkid" & _
              ", assessmentdate = @assessmentdate" & _
              ", userunkid = @userunkid" & _
              ", isvoid = @isvoid" & _
              ", voiduserunkid = @voiduserunkid" & _
              ", voiddatetime = @voiddatetime" & _
              ", voidreason = @voidreason" & _
              ", iscommitted = @iscommitted" & _
              ", reviewerunkid = @reviewerunkid" & _
              ", assessmodeid = @assessmodeid" & _
              ", ext_assessorunkid = @ext_assessorunkid " & _
              ", committeddatetime = @committeddatetime " & _
            "WHERE analysisunkid = @analysisunkid "
            'S.SANDEEP [ 14 JUNE 2012 ] -- END

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dtTable IsNot Nothing Then
                If dtTable.Rows.Count > 0 Then

                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    objDataOperation.ClearParameters()
                    objBSCAnalysisTran._DataOperation = objDataOperation
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                    objBSCAnalysisTran._AnalysisUnkid = mintAnalysisunkid
                    objBSCAnalysisTran._DataTable = dtTable
                    If objBSCAnalysisTran.InsertUpdateDelete_AnalysisTran(mintUserunkid) = False Then 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
                        'If objBSCAnalysisTran.InsertUpdateDelete_AnalysisTran() = False Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                End If
            End If
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrbsc_analysis_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal enAssessment As enAssessmentMode, ByVal iPeriodId As Integer) As Boolean 'S.SANDEEP [ 01 JUL 2014 ] -- START {iPeriodId} -- END
        'Public Function Delete(ByVal intUnkid As Integer, ByVal enAssessment As enAssessmentMode) As Boolean
        If isUsed(intUnkid, enAssessment, iPeriodId) Then 'S.SANDEEP [ 01 JUL 2014 ] -- START {iPeriodId} -- END
            'If isUsed(intUnkid, enAssessment) Then
            Select Case enAssessment
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    mstrMessage = Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assessment. Reason : Assessor has already assessed this employee.")
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete this Assessment. Reason : Reviewer has already assessed this employee.")
            End Select
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            strQ = "UPDATE hrbsc_analysis_master SET " & _
                  "  isvoid = @isvoid" & _
                  ", voiduserunkid = @voiduserunkid" & _
                  ", voiddatetime = @voiddatetime" & _
                  ", voidreason = @voidreason " & _
                "WHERE analysisunkid = @analysisunkid "

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = clsCommonATLog.GetChildList(objDataOperation, "hrbsc_analysis_tran", "analysisunkid", intUnkid)

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dtRow As DataRow In dsList.Tables(0).Rows
                    strQ = "UPDATE hrbsc_analysis_tran SET " & _
                           "  isvoid = @isvoid" & _
                           ", voiduserunkid = @voiduserunkid " & _
                           ", voiddatetime = @voiddatetime " & _
                           ", voidreason = @voidreason " & _
                           "WHERE analysistranunkid = '" & dtRow.Item("analysistranunkid") & "' "

                    objDataOperation.ClearParameters()
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid.ToString)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid.ToString)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                    Call objDataOperation.ExecNonQuery(strQ)

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If


                    'S.SANDEEP [ 27 APRIL 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrbsc_analysis_master", "analysisunkid", intUnkid, "hrbsc_analysis_tran", "analysistranunkid", dtRow.Item("analysistranunkid"), 3, 3) = False Then
                    '    If objDataOperation.ErrorMessage <> "" Then
                    '        objDataOperation.ReleaseTransaction(False)
                    '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '        Throw exForce
                    '    End If
                    'End If

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrbsc_analysis_master", "analysisunkid", intUnkid, "hrbsc_analysis_tran", "analysistranunkid", dtRow.Item("analysistranunkid"), 3, 3, , mintVoiduserunkid) = False Then
                        If objDataOperation.ErrorMessage <> "" Then
                            objDataOperation.ReleaseTransaction(False)
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                    'S.SANDEEP [ 27 APRIL 2012 ] -- END

                Next
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer, ByVal enAssessment As enAssessmentMode, ByVal iPrdId As Integer) As Boolean 'S.SANDEEP [ 01 JUL 2014 ] -- START {iPrdId} -- END
        'Public Function isUsed(ByVal intUnkid As Integer, ByVal enAssessment As enAssessmentMode) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                       "    assessedemployeeunkid " & _
                       "FROM hrbsc_analysis_master " & _
                       "WHERE assessedemployeeunkid " & _
                       "IN " & _
                       "( " & _
                             "SELECT " & _
                                  "selfemployeeunkid " & _
                             "FROM hrbsc_analysis_master " & _
                             "WHERE analysisunkid = @analysisunkid AND periodunkid = @PrdId " & _
                       ") " & _
                       "AND isVoid = 0 " & _
                       "AND assessedemployeeunkid > 0 AND periodunkid = @PrdId "

            'S.SANDEEP [ 01 JUL 2014 ] -- START
            'AND periodunkid = @PrdId -- ADDED
            'S.SANDEEP [ 01 JUL 2014 ] -- END

            Select Case enAssessment
                Case enAssessmentMode.SELF_ASSESSMENT
                    strQ &= " AND assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "' "
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    strQ &= " AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' "
            End Select

            'S.SANDEEP [ 01 JUL 2014 ] -- START
            objDataOperation.AddParameter("@PrdId", SqlDbType.Int, eZeeDataType.INT_SIZE, iPrdId)
            'S.SANDEEP [ 01 JUL 2014 ] -- END

            objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal enAssessMode As enAssessmentMode, _
                            ByVal intEmpId As Integer, _
                            ByVal intYearId As Integer, _
                            ByVal intPeriodId As Integer, _
                            Optional ByVal intAssessorId As Integer = -1, _
                            Optional ByVal intReviewerId As Integer = -1, _
                            Optional ByVal intUnkid As Integer = -1, Optional ByVal isExAssessor As Boolean = False) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "  analysisunkid " & _
                      ", yearunkid " & _
                      ", periodunkid " & _
                      ", selfemployeeunkid " & _
                      ", assessormasterunkid " & _
                      ", assessoremployeeunkid " & _
                      ", assessedemployeeunkid " & _
                      ", assessmentdate " & _
                      ", userunkid " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voiddatetime " & _
                      ", voidreason " & _
                      ", iscommitted " & _
                      ", reviewerunkid " & _
                      ", assessmodeid " & _
                      ", ext_assessorunkid " & _
                      "FROM hrbsc_analysis_master " & _
                      "WHERE ISNULL(isvoid,0) = 0 AND hrbsc_analysis_master.yearunkid  = @YearId AND hrbsc_analysis_master.periodunkid  = @PeriodId "

            If intUnkid > 0 Then
                strQ &= " AND analysisunkid <> @analysisunkid"
                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            Select Case enAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    strQ &= " AND hrbsc_analysis_master.selfemployeeunkid = @EmpId "
                Case enAssessmentMode.APPRAISER_ASSESSMENT, enAssessmentMode.REVIEWER_ASSESSMENT
                    strQ &= " AND hrbsc_analysis_master.assessedemployeeunkid = @EmpId "
                    'S.SANDEEP [ 03 AUG 2013 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    strQ &= " AND assessmodeid = " & enAssessMode
                    'S.SANDEEP [ 03 AUG 2013 ] -- END
            End Select

            If intAssessorId > 0 Then
                If isExAssessor = False Then
                strQ &= "AND hrbsc_analysis_master.assessormasterunkid = @AssessorId "
                Else
                    strQ &= "AND hrbsc_analysis_master.ext_assessorunkid = @AssessorId "
                End If
                objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)
            End If

            If intReviewerId > 0 Then
                strQ &= "AND hrbsc_analysis_master.assessormasterunkid = @reviewerunkid "
                objDataOperation.AddParameter("@reviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intReviewerId)
            End If

            objDataOperation.AddParameter("@EmpId", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmpId)
            objDataOperation.AddParameter("@YearId", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearId)
            objDataOperation.AddParameter("@PeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function GetAssessorEmpId(ByVal intAssessorId As Integer) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intEmpId As Integer = -1
        objDataOperation = New clsDataOperation
        Try

            strQ = "SELECT employeeunkid AS EId FROM hrassessor_master WHERE assessormasterunkid = @AssessorId AND hrassessor_master.isvoid = 0 "

            objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intEmpId = dsList.Tables(0).Rows(0)(0)
            End If

            Return intEmpId

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        End Try
    End Function

    Public Function getAssessorComboList(Optional ByVal strList As String = "List", _
                                         Optional ByVal blnFlag As Boolean = False, _
                                         Optional ByVal blnIsReviewer As Boolean = False, _
                                         Optional ByVal intUserUnkid As Integer = 0, _
                                         Optional ByVal strIncludeInactiveEmployee As String = "", _
                                         Optional ByVal strEmployeeAsOnDate As String = "") As DataSet 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Function getAssessorComboList(Optional ByVal strList As String = "List", Optional ByVal blnFlag As Boolean = False, Optional ByVal blnIsReviewer As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnFlag = True Then
                StrQ = "SELECT 0 AS Id, @Select AS Name,'' AS Code UNION "
            End If

            StrQ &= "SELECT " & _
                    "	 assessormasterunkid AS Id " & _
                    "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS Name " & _
                    "   ,ISNULL(hremployee_master.employeecode,'') AS Code " & _
                    "FROM hrassessor_master " & _
                    "	JOIN hrapprover_usermapping ON hrassessor_master.assessormasterunkid = hrapprover_usermapping.approverunkid " & _
                    "	JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                    "WHERE hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " " & _
                    "	AND hrapprover_usermapping.userunkid = " & IIf(intUserUnkid = 0, User._Object._Userunkid, intUserUnkid) & "  AND hrassessor_master.isvoid = 0 "

            If blnIsReviewer Then
                StrQ &= "	AND hrassessor_master.isreviewer = 1 "
            Else
                StrQ &= "	AND hrassessor_master.isreviewer = 0 "
            End If



            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
            '    StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
            '               " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate "

            '    objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            '    objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
            'End If


            If strIncludeInactiveEmployee.Trim.Length <= 0 Then
                strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
            End If

            If CBool(strIncludeInactiveEmployee) = False Then 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then
                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END




            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getAssessorComboList", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function getEmployeeBasedAssessor(ByVal intAssessorId As Integer, _
                                             Optional ByVal strList As String = "List", _
                                             Optional ByVal blnFlag As Boolean = False, _
                                             Optional ByVal strIncludeInactiveEmployee As String = "", _
                                             Optional ByVal strEmployeeAsOnDate As String = "", _
                                             Optional ByVal intUserUnkId As Integer = 0) As DataSet 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END


        'Public Function getEmployeeBasedAssessor(ByVal intAssessorId As Integer, _
        '                                     Optional ByVal strList As String = "List", _
        '                                     Optional ByVal blnFlag As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            If blnFlag = True Then
                StrQ = "SELECT  0 AS Id,@Select AS NAME,'' AS Code UNION "
            End If

            StrQ &= "SELECT " & _
                    "	 employeeunkid AS Id " & _
                    "	,ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') AS NAME " & _
                    "	,ISNULL(hremployee_master.employeecode,'') AS Code " & _
                    "FROM hremployee_master " & _
                    "WHERE employeeunkid " & _
                    "	IN " & _
                    "	( " & _
                    "		SELECT " & _
                    "			hrassessor_tran.employeeunkid " & _
                    "		FROM hrassessor_tran " & _
                    "			JOIN hrassessor_master ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                    "			JOIN hrapprover_usermapping ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                    "		WHERE hrassessor_master.assessormasterunkid = @AssessorId " & _
                    "			AND hrapprover_usermapping.userunkid = " & IIf(intUserUnkId <= 0, User._Object._Userunkid, intUserUnkId) & " " & _
                    "           AND hrapprover_usermapping.usertypeid = " & enUserType.Assessor & " AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 " & _
                    "	) "

            If strIncludeInactiveEmployee.Trim.Length <= 0 Then
                strIncludeInactiveEmployee = ConfigParameter._Object._IsIncludeInactiveEmp.ToString
            End If

            If CBool(strIncludeInactiveEmployee) = False Then 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
                'If ConfigParameter._Object._IsIncludeInactiveEmp = False Then 
                StrQ &= " AND CONVERT(CHAR(8),hremployee_master.appointeddate,112) <= @enddate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_from_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.termination_to_date,112),@startdate) >= @startdate " & _
                           " AND ISNULL(CONVERT(CHAR(8),hremployee_master.empl_enddate,112), @startdate) >= @startdate " 'S.SANDEEP [ 15 MAY 2012 empl_enddate ] -- START -- END

                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)
                'objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, ConfigParameter._Object._EmployeeAsOnDate)

                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, IIf(strEmployeeAsOnDate = "", ConfigParameter._Object._EmployeeAsOnDate, strEmployeeAsOnDate))
                'S.SANDEEP [ 27 APRIL 2012 ] -- END
            End If

            objDataOperation.AddParameter("@AssessorId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAssessorId)
            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getEmployeeBasedAssessor", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function GetUncommitedInfo(ByVal intPeriodId As Integer) As Integer
        Dim intUCnt As Integer = -1
        Dim exForce As Exception
        Dim StrQ As String = String.Empty
        Try
            objDataOperation = New clsDataOperation
            StrQ = "SELECT " & _
                   "	analysisunkid " & _
                   "FROM hrbsc_analysis_master " & _
                   "WHERE ISNULL(isvoid,0) = 0 " & _
                   "	AND ISNULL(iscommitted,0) = 0 " & _
                   "    AND periodunkid = @Pid "

            objDataOperation.AddParameter("@Pid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)

            intUCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return intUCnt

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetUncommitedInfo; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Public Function GetAssessmentResultView(ByVal intEmployeeId As Integer, ByVal intYearId As Integer, ByVal intPeriodId As Integer) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception
    '    Dim dtTable As DataTable
    '    Dim dCol As DataColumn
    '    objDataOperation = New clsDataOperation
    '    Try
    '        dtTable = New DataTable("View")

    '        dCol = New DataColumn
    '        dCol.ColumnName = "PId" : dCol.DefaultValue = -1 : dCol.DataType = System.Type.GetType("System.Int32") : dCol.Caption = "PId"
    '        dtTable.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        dCol.ColumnName = "ObjUnkid" : dCol.DefaultValue = -1 : dCol.DataType = System.Type.GetType("System.Int32") : dCol.Caption = "ObjUnkid"
    '        dtTable.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        dCol.ColumnName = "KpiUnkid" : dCol.DefaultValue = -1 : dCol.DataType = System.Type.GetType("System.Int32") : dCol.Caption = "KpiUnkid"
    '        dtTable.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        dCol.ColumnName = "TargetUnkid" : dCol.DefaultValue = -1 : dCol.DataType = System.Type.GetType("System.Int32") : dCol.Caption = "TargetUnkid"
    '        dtTable.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        dCol.ColumnName = "InitiativeUnkid" : dCol.DefaultValue = -1 : dCol.DataType = System.Type.GetType("System.Int32") : dCol.Caption = "InitiativeUnkid"
    '        dtTable.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        dCol.ColumnName = "perspective" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Perspective"
    '        dtTable.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        dCol.ColumnName = "objective" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Objective"
    '        dtTable.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        dCol.ColumnName = "kpi" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Key Performace Indicators"
    '        dtTable.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        dCol.ColumnName = "targets" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Targets"
    '        dtTable.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        dCol.ColumnName = "initiative" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Initiative/Actions"
    '        dtTable.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        dCol.ColumnName = "self" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Self"
    '        dtTable.Columns.Add(dCol)

    '        dCol = New DataColumn
    '        dCol.ColumnName = "selfremark" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Self Remark"
    '        dtTable.Columns.Add(dCol)

    '        strQ &= "SELECT " & _
    '                         " Id AS PId " & _
    '                         ",ObjUnkid " & _
    '                         ",KpiUnkid " & _
    '                         ",-1 AS TargetUnkid " & _
    '                         ",-1 AS InitiativeUnkid " & _
    '                         ",NAME as perspective " & _
    '                         ",ISNULL(Objective,'') AS objective " & _
    '                         ",ISNULL(KPI,'') AS kpi " & _
    '                    "FROM " & _
    '                    "( " & _
    '                         "SELECT 1 AS Id ,@FINANCIAL AS NAME UNION " & _
    '                         "SELECT 2 AS Id ,@CUSTOMER AS NAME UNION " & _
    '                         "SELECT 3 AS Id ,@BUSINESS_PROCESS AS NAME UNION " & _
    '                         "SELECT 4 AS Id ,@ORGANIZATION_CAPACITY AS NAME " & _
    '                    ") AS MGrp " & _
    '                    "LEFT JOIN " & _
    '                    "( " & _
    '                         "SELECT " & _
    '                          "perspectiveunkid AS Pid " & _
    '                         ",objectiveunkid AS ObjUnkid " & _
    '                         ",name AS Objective " & _
    '                         "FROM hrobjective_master WHERE isactive = 1 "
    '        If ConfigParameter._Object._IsBSC_ByEmployee Then
    '            strQ &= " AND employeeunkid = '" & intEmployeeId & "' "
    '        End If
    '        strQ &= ") AS OBJ ON MGrp.Id = OBJ.Pid " & _
    '                    "JOIN " & _
    '                    "( " & _
    '                         "SELECT " & _
    '                               "objectiveunkid AS KPIobjId " & _
    '                              ",kpiunkid AS KpiUnkid " & _
    '                              ",name AS KPI " & _
    '                         "FROM hrkpi_master " & _
    '                         "WHERE isactive = 1 " & _
    '                    ") AS KPI ON OBJ.ObjUnkid=KPI.KPIobjId " & _
    '                    "WHERE 1 = 1 ORDER BY Id "


    '        objDataOperation.AddParameter("@FINANCIAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 315, "Financial"))
    '        objDataOperation.AddParameter("@CUSTOMER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 316, "Customer"))
    '        objDataOperation.AddParameter("@BUSINESS_PROCESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 317, "Business Process"))
    '        objDataOperation.AddParameter("@ORGANIZATION_CAPACITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 318, "Organization Capacity"))

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            dtTable.ImportRow(dtRow)
    '        Next

    '        strQ = "SELECT " & _
    '                         " CASE WHEN ISNULL(hrobjective_master.perspectiveunkid,0) <=0 THEN OM.perspectiveunkid ELSE hrobjective_master.perspectiveunkid END AS Pid " & _
    '                         ",CASE WHEN hrtarget_master.objectiveunkid <= 0 THEN OM.objectiveunkid ELSE hrtarget_master.objectiveunkid END AS ObjUnkid " & _
    '                         ",hrtarget_master.kpiunkid AS KpiUnkid " & _
    '                         ",hrtarget_master.targetunkid AS TargetUnkid " & _
    '                         ",CASE WHEN ISNULL(hrobjective_master.name,'') = '' THEN ISNULL(OM.NAME,'') ELSE ISNULL(hrobjective_master.name,'') END AS Objective " & _
    '                         ",ISNULL(hrkpi_master.name,'') AS KPI " & _
    '                         ",hrtarget_master.name AS TARGET " & _
    '                    "FROM hrtarget_master " & _
    '                         "LEFT JOIN hrobjective_master ON hrtarget_master.objectiveunkid = hrobjective_master.objectiveunkid AND hrobjective_master.isactive =1 " & _
    '                         "LEFT JOIN hrkpi_master ON hrtarget_master.kpiunkid = hrkpi_master.kpiunkid AND hrkpi_master.isactive=1 " & _
    '                         "LEFT JOIN hrobjective_master OM ON hrkpi_master.objectiveunkid = OM.objectiveunkid AND OM.isactive=1 " & _
    '                    "WHERE hrtarget_master.isactive = 1 "

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If


    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            Dim drRow() As DataRow = dtTable.Select("PId='" & dtRow.Item("Pid") & "' AND ObjUnkid='" & dtRow.Item("ObjUnkid") & "' AND KpiUnkid = '" & dtRow.Item("KpiUnkid") & "' AND targets = ''")
    '            If drRow.Length > 0 Then
    '                For i As Integer = 0 To drRow.Length - 1
    '                    drRow(i)("targets") = dtRow.Item("TARGET")
    '                    drRow(i)("TargetUnkid") = dtRow.Item("TargetUnkid")
    '                Next
    '            Else
    '                Dim drTemp() As DataRow = dtTable.Select("PId='" & dtRow.Item("Pid") & "' AND ObjUnkid='" & dtRow.Item("ObjUnkid") & "'")
    '                If drTemp.Length > 0 Then
    '                    Dim dRow As DataRow = dtTable.NewRow
    '                    dRow.Item("PId") = drTemp(0)("PId")
    '                    dRow.Item("ObjUnkid") = drTemp(0)("ObjUnkid")
    '                    dRow.Item("KpiUnkid") = dtRow.Item("KpiUnkid")
    '                    dRow.Item("TargetUnkid") = dtRow.Item("TargetUnkid")
    '                    dRow.Item("InitiativeUnkid") = -1
    '                    dRow.Item("perspective") = drTemp(0)("perspective")
    '                    dRow.Item("objective") = drTemp(0)("objective")
    '                    dRow.Item("kpi") = dtRow.Item("kpi")
    '                    dRow.Item("targets") = dtRow.Item("TARGET")
    '                    dtTable.Rows.Add(dRow)
    '                End If
    '            End If
    '        Next

    '        strQ = "SELECT " & _
    '                         " CASE WHEN ISNULL(hrobjective_master.perspectiveunkid,0) <=0 THEN ISNULL(OM.perspectiveunkid,TgtOM.perspectiveunkid) ELSE hrobjective_master.perspectiveunkid END AS Pid " & _
    '                         ",CASE WHEN hrinitiative_master.objectiveunkid <= 0 THEN ISNULL(OM.objectiveunkid,TgtOM.objectiveunkid) ELSE hrinitiative_master.objectiveunkid END AS ObjUnkid " & _
    '                         ",hrinitiative_master.targetunkid AS TargetUnkid " & _
    '                         ",hrinitiative_master.initiativeunkid AS Initiativeunkid " & _
    '                         ",CASE WHEN ISNULL(hrobjective_master.name,'') = '' THEN ISNULL(OM.NAME,'') ELSE ISNULL(hrobjective_master.name,'') END AS Objective " & _
    '                         ",ISNULL(hrtarget_master.name,'') AS TARGET " & _
    '                         ",hrinitiative_master.name AS Initiative " & _
    '                    "FROM hrinitiative_master " & _
    '                         "LEFT JOIN hrobjective_master ON hrinitiative_master.objectiveunkid = hrobjective_master.objectiveunkid AND hrobjective_master.isactive = 1 " & _
    '                         "LEFT JOIN hrtarget_master ON hrinitiative_master.targetunkid = hrtarget_master.targetunkid AND hrtarget_master.isactive = 1 " & _
    '                         "LEFT JOIN hrkpi_master ON hrtarget_master.kpiunkid = hrkpi_master.kpiunkid AND hrkpi_master.isactive = 1 " & _
    '                         "LEFT JOIN hrobjective_master OM ON dbo.hrkpi_master.objectiveunkid = OM.objectiveunkid AND OM.isactive = 1 " & _
    '                         "LEFT JOIN hrobjective_master TgtOM ON TgtOM.objectiveunkid = hrtarget_master.objectiveunkid " & _
    '                    "WHERE hrinitiative_master.isactive = 1 "

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            Dim drRow() As DataRow = dtTable.Select("PId='" & dtRow.Item("Pid") & "' AND ObjUnkid='" & dtRow.Item("ObjUnkid") & "' AND TargetUnkid = '" & dtRow.Item("TargetUnkid") & "' and initiative = ''")
    '            If drRow.Length > 0 Then
    '                For i As Integer = 0 To drRow.Length - 1
    '                    drRow(i)("initiative") = dtRow.Item("Initiative")
    '                    drRow(i)("InitiativeUnkid") = dtRow.Item("InitiativeUnkid")
    '                Next
    '            Else
    '                Dim drTemp() As DataRow = dtTable.Select("PId='" & dtRow.Item("Pid") & "' AND ObjUnkid='" & dtRow.Item("ObjUnkid") & "'")
    '                If drTemp.Length > 0 Then
    '                    Dim dRow As DataRow = dtTable.NewRow
    '                    dRow.Item("PId") = drTemp(0)("PId")
    '                    dRow.Item("ObjUnkid") = drTemp(0)("ObjUnkid")
    '                    dRow.Item("KpiUnkid") = drTemp(0)("KpiUnkid")
    '                    dRow.Item("TargetUnkid") = dtRow.Item("TargetUnkid")
    '                    dRow.Item("InitiativeUnkid") = dtRow("InitiativeUnkid")
    '                    dRow.Item("perspective") = drTemp(0)("perspective")
    '                    dRow.Item("objective") = drTemp(0)("objective")
    '                    dRow.Item("kpi") = drTemp(0)("kpi")
    '                    dRow.Item("targets") = dtRow.Item("TARGET")
    '                    dRow.Item("initiative") = dtRow.Item("Initiative")
    '                    dtTable.Rows.Add(dRow)
    '                End If
    '            End If
    '        Next

    '        Dim dView As DataView = dtTable.DefaultView
    '        dView.Sort = "PId,ObjUnkid"
    '        dtTable = dView.ToTable


    '        strQ = "SELECT analysisunkid,selfemployeeunkid,assessedemployeeunkid,assessoremployeeunkid,assessmodeid FROM " & _
    '                  "( " & _
    '                         "SELECT " & _
    '                              "analysisunkid,selfemployeeunkid,assessedemployeeunkid,assessoremployeeunkid,assessmodeid " & _
    '                         "FROM hrbsc_analysis_master WHERE assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "'  AND  isvoid = 0 AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND selfemployeeunkid = '" & intEmployeeId & "' " & _
    '                         "UNION ALL " & _
    '                         "SELECT " & _
    '                              "analysisunkid,selfemployeeunkid,assessedemployeeunkid,assessoremployeeunkid,assessmodeid " & _
    '                         "FROM hrbsc_analysis_master WHERE assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND  isvoid = 0 AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND assessedemployeeunkid = '" & intEmployeeId & "' " & _
    '                   ")AS Grp " & _
    '                   "ORDER BY Grp.assessmodeid "

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dtRow As DataRow In dsList.Tables("List").Rows
    '            objDataOperation.ClearParameters()
    '            strQ = "SELECT " & _
    '                             " hrbsc_analysis_tran.objectiveunkid " & _
    '                             ",hrbsc_analysis_tran.kpiunkid " & _
    '                             ",hrbsc_analysis_tran.targetunkid " & _
    '                             ",hrbsc_analysis_tran.initiativeunkid " & _
    '                             ",ISNULL(hrresult_master.resultname,'') AS Result " & _
    '                             ",ISNULL(hrbsc_analysis_tran.remark,'') AS Remark " & _
    '                             ",assessoremployeeunkid "
    '            Select Case CInt(dtRow.Item("assessmodeid"))
    '                Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                    strQ &= ",CASE WHEN hrexternal_assessor_master.ext_assessorunkid > 0 THEN hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END AS Appraiser " & _
    '                                ",hrexternal_assessor_master.ext_assessorunkid "
    '            End Select
    '            strQ &= "FROM hrbsc_analysis_tran " & _
    '                             "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '                             "JOIN hrinitiative_master ON hrbsc_analysis_tran.initiativeunkid = hrinitiative_master.initiativeunkid " & _
    '                             "JOIN hrtarget_master ON hrbsc_analysis_tran.targetunkid = hrtarget_master.targetunkid " & _
    '                             "JOIN hrkpi_master ON hrbsc_analysis_tran.kpiunkid = hrkpi_master.kpiunkid " & _
    '                             "JOIN hrobjective_master ON hrbsc_analysis_tran.objectiveunkid = hrobjective_master.objectiveunkid " & _
    '                             "JOIN hrbsc_analysis_master ON hrbsc_analysis_tran.analysisunkid = hrbsc_analysis_master.analysisunkid "
    '            Select Case CInt(dtRow.Item("assessmodeid"))
    '                Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                    strQ &= "LEFT JOIN hremployee_master AS AEmp ON AEmp.employeeunkid = hrbsc_analysis_master.assessoremployeeunkid " & _
    '                                "LEFT JOIN hrexternal_assessor_master ON hrexternal_assessor_master.ext_assessorunkid = hrbsc_analysis_master.ext_assessorunkid "
    '            End Select
    '            strQ &= " WHERE hrbsc_analysis_tran.isvoid = 0 AND hrbsc_analysis_master.analysisunkid = '" & dtRow.Item("analysisunkid") & "'"

    '            Dim dsData As DataSet = objDataOperation.ExecQuery(strQ, "List")

    '            If objDataOperation.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '                Throw exForce
    '            End If

    '            For Each dRow As DataRow In dsData.Tables("List").Rows
    '                Select Case CInt(dtRow.Item("assessmodeid"))
    '                    Case enAssessmentMode.SELF_ASSESSMENT
    '                        Dim dSTemp() As DataRow = dtTable.Select("ObjUnkid ='" & dRow.Item("objectiveunkid") & "' AND KpiUnkid='" & dRow.Item("kpiunkid") & "' AND TargetUnkid='" & dRow.Item("targetunkid") & "' AND InitiativeUnkid = '" & dRow.Item("initiativeunkid") & "'")
    '                        If dSTemp.Length > 0 Then
    '                            dSTemp(0).Item("self") = dRow.Item("Result")
    '                            dSTemp(0).Item("selfremark") = dRow.Item("Remark")
    '                            dSTemp(0).AcceptChanges()
    '                        End If
    '                    Case enAssessmentMode.APPRAISER_ASSESSMENT

    '                        If dRow.Item("assessoremployeeunkid") <= 0 Then
    '                            If dtTable.Columns.Contains("Column" & dRow.Item("ext_assessorunkid").ToString & "ExtId") = False Then
    '                                dtTable.Columns.Add("Column" & dRow.Item("ext_assessorunkid").ToString & "ExtId", System.Type.GetType("System.String")).Caption = "Appraiser [ " & dRow.Item("Appraiser") & " ]"
    '                                dtTable.Columns.Add("Column" & dRow.Item("ext_assessorunkid").ToString & "ExRemark", System.Type.GetType("System.String")).Caption = "Appraiser's Remark"
    '                            End If
    '                        Else
    '                            If dtTable.Columns.Contains("Column" & dRow.Item("assessoremployeeunkid").ToString & "IntId") = False Then
    '                                dtTable.Columns.Add("Column" & dRow.Item("assessoremployeeunkid").ToString & "IntId", System.Type.GetType("System.String")).Caption = "Appraiser [ " & dRow.Item("Appraiser") & " ]"
    '                                dtTable.Columns.Add("Column" & dRow.Item("assessoremployeeunkid").ToString & "IntRemark", System.Type.GetType("System.String")).Caption = "Appraiser's Remark"
    '                            End If
    '                        End If

    '                        Dim dSTemp() As DataRow = dtTable.Select("ObjUnkid ='" & dRow.Item("objectiveunkid") & "' AND KpiUnkid='" & dRow.Item("kpiunkid") & "' AND TargetUnkid='" & dRow.Item("targetunkid") & "' AND InitiativeUnkid = '" & dRow.Item("initiativeunkid") & "'")
    '                        If dSTemp.Length > 0 Then
    '                            If dRow.Item("assessoremployeeunkid") <= 0 Then
    '                                dSTemp(0).Item("Column" & dRow.Item("ext_assessorunkid").ToString & "ExtId") = dRow.Item("Result")
    '                                dSTemp(0).Item("Column" & dRow.Item("ext_assessorunkid").ToString & "ExRemark") = dRow.Item("Remark")
    '                            Else
    '                                dSTemp(0).Item("Column" & dtRow.Item("assessoremployeeunkid").ToString & "IntId") = dRow.Item("Result")
    '                                dSTemp(0).Item("Column" & dtRow.Item("assessoremployeeunkid").ToString & "IntRemark") = dRow.Item("Remark")
    '                            End If
    '                            dSTemp(0).AcceptChanges()
    '                        End If
    '                End Select
    '            Next

    '        Next

    '        dsList = New DataSet

    '        dsList.Tables.Add(dtTable)

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetAssessmentResultView; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function


    'S.SANDEEP [ 18 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    'Public Function GetAssessmentResultView(ByVal intEmployeeId As Integer, _
    '                                        ByVal intYearId As Integer, _
    '                                        ByVal intPeriodId As Integer, _
    '                                        Optional ByVal strIsBSC_ByEmployee As String = "") As DataSet 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
    'Public Function GetAssessmentResultView(ByVal intEmployeeId As Integer, ByVal intYearId As Integer, ByVal intPeriodId As Integer) As DataSet

    Public Function GetAssessmentResultView(ByVal intEmployeeId As Integer, _
                                            ByVal intYearId As Integer, _
                                            ByVal intPeriodId As Integer, _
                                        Optional ByVal strIsBSC_ByEmployee As String = "", _
                                        Optional ByVal StrDataBaseName As String = "") As DataSet

        If StrDataBaseName.Trim.Length <= 0 Then StrDataBaseName = FinancialYear._Object._DatabaseName
        'S.SANDEEP [ 18 AUG 2012 ] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dtTable As DataTable
        Dim dCol As DataColumn
        objDataOperation = New clsDataOperation
        Try
            dtTable = New DataTable("View")

            dCol = New DataColumn
            dCol.ColumnName = "PId" : dCol.DefaultValue = -1 : dCol.DataType = System.Type.GetType("System.Int32") : dCol.Caption = "PId"
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "ObjUnkid" : dCol.DefaultValue = -1 : dCol.DataType = System.Type.GetType("System.Int32") : dCol.Caption = "ObjUnkid"
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "KpiUnkid" : dCol.DefaultValue = -1 : dCol.DataType = System.Type.GetType("System.Int32") : dCol.Caption = "KpiUnkid"
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "TargetUnkid" : dCol.DefaultValue = -1 : dCol.DataType = System.Type.GetType("System.Int32") : dCol.Caption = "TargetUnkid"
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "InitiativeUnkid" : dCol.DefaultValue = -1 : dCol.DataType = System.Type.GetType("System.Int32") : dCol.Caption = "InitiativeUnkid"
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "perspective" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Perspective"
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "objective" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Objective"
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "kpi" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Key Performace Indicators"
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "targets" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Targets"
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "initiative" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Initiative/Actions"
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "objisGrp" : dCol.DefaultValue = False : dCol.DataType = System.Type.GetType("System.Boolean") : dCol.Caption = ""
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "objGrpId" : dCol.DefaultValue = -1 : dCol.DataType = System.Type.GetType("System.Int32") : dCol.Caption = ""
            dtTable.Columns.Add(dCol)

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'dCol = New DataColumn
            'dCol.ColumnName = "weight" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Weight"
            'dtTable.Columns.Add(dCol)

            'S.SANDEEP [ 21 MAR 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            dCol = New DataColumn
            dCol.ColumnName = "weight" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Weight"
            dtTable.Columns.Add(dCol)
            'S.SANDEEP [ 21 MAR 2013 ] -- END

            
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            'S.SANDEEP [ 04 JULY 2012 ] -- END

            dCol = New DataColumn
            dCol.ColumnName = "self" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Self"
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "selfremark" : dCol.DefaultValue = "" : dCol.DataType = System.Type.GetType("System.String") : dCol.Caption = "Self Remark"
            dtTable.Columns.Add(dCol)

            'S.SANDEEP [ 04 JULY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '       "    Id AS PId " & _
            '       "    ,ISNULL(ObjUnkid,0) AS ObjUnkid " & _
            '       "    ,-1 AS KpiUnkid " & _
            '       "    ,-1 AS TargetUnkid " & _
            '       "    ,-1 AS InitiativeUnkid " & _
            '       "    ,NAME as perspective " & _
            '       "    ,ISNULL(Objective,'') AS objective " & _
            '       "    ,1 as objisGrp " & _
            '       "    ,Id as objGrpId " & _
            '       "FROM " & _
            '       "( " & _
            '       "    SELECT 1 AS Id ,@FINANCIAL AS NAME UNION " & _
            '       "    SELECT 2 AS Id ,@CUSTOMER AS NAME UNION " & _
            '       "    SELECT 3 AS Id ,@BUSINESS_PROCESS AS NAME UNION " & _
            '       "    SELECT 4 AS Id ,@ORGANIZATION_CAPACITY AS NAME " & _
            '       ") AS MGrp " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "    SELECT " & _
            '       "         perspectiveunkid AS Pid " & _
            '       "        ,objectiveunkid AS ObjUnkid " & _
            '       "        ,name AS Objective " & _
            '       "    FROM hrobjective_master WHERE isactive = 1 "

            'S.SANDEEP [ 18 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT " & _
            '       "  Id AS PId " & _
            '       ", ISNULL(ObjUnkid,0) AS ObjUnkid " & _
            '       ", -1 AS KpiUnkid " & _
            '       ", -1 AS TargetUnkid " & _
            '       ", -1 AS InitiativeUnkid " & _
            '       ", NAME as perspective " & _
            '       ", ISNULL(Objective,'') AS objective " & _
            '       ", 1 as objisGrp " & _
            '       ", Id as objGrpId " & _
            '       ", weight AS weight " & _
            '       "FROM " & _
            '       "( " & _
            '       "    SELECT 1 AS Id ,@FINANCIAL AS NAME UNION " & _
            '       "    SELECT 2 AS Id ,@CUSTOMER AS NAME UNION " & _
            '       "    SELECT 3 AS Id ,@BUSINESS_PROCESS AS NAME UNION " & _
            '       "    SELECT 4 AS Id ,@ORGANIZATION_CAPACITY AS NAME " & _
            '       ") AS MGrp " & _
            '       "LEFT JOIN " & _
            '       "( " & _
            '       "SELECT " & _
            '       "    perspectiveunkid AS Pid " & _
            '       ",   objectiveunkid AS ObjUnkid " & _
            '       ",   name AS Objective " & _
            '       ",   weight as weight " & _
            '       "FROM hrobjective_master WHERE isactive = 1 "
            strQ = "SELECT " & _
                              "Id AS PId " & _
                             ",ISNULL(ObjUnkid,0) AS ObjUnkid " & _
                             ",-1 AS KpiUnkid " & _
                             ",-1 AS TargetUnkid " & _
                             ",-1 AS InitiativeUnkid " & _
                             ",NAME as perspective " & _
                             ",ISNULL(Objective,'') AS objective " & _
                             ",1 as objisGrp " & _
                             ",Id as objGrpId " & _
                             ",ISNULL(weight,'') AS weight " & _
                        "FROM " & _
                        "( " & _
                             "SELECT 1 AS Id ,@FINANCIAL AS NAME UNION " & _
                             "SELECT 2 AS Id ,@CUSTOMER AS NAME UNION " & _
                             "SELECT 3 AS Id ,@BUSINESS_PROCESS AS NAME UNION " & _
                             "SELECT 4 AS Id ,@ORGANIZATION_CAPACITY AS NAME " & _
                        ") AS MGrp " & _
                        "LEFT JOIN " & _
                        "( " & _
                             "SELECT " & _
                              "perspectiveunkid AS Pid " & _
                             ",objectiveunkid AS ObjUnkid " & _
                             ",name AS Objective " & _
                             ",CASE WHEN weight <= 0 THEN '' ELSE CAST(weight AS NVARCHAR(MAX)) END AS weight " & _
                   "FROM " & StrDataBaseName & "..hrobjective_master WHERE isactive = 1 AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' "
            'S.SANDEEP [ 18 AUG 2012 ] -- END

            
            'S.SANDEEP [ 04 JULY 2012 ] -- END

            
            If strIsBSC_ByEmployee.Trim.Length <= 0 Then
                strIsBSC_ByEmployee = ConfigParameter._Object._IsBSC_ByEmployee.ToString
            End If
            If CBool(strIsBSC_ByEmployee) = True Then 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
                'If ConfigParameter._Object._IsBSC_ByEmployee Then
                strQ &= " AND employeeunkid = '" & intEmployeeId & "' "
            End If

            strQ &= ") AS OBJ ON MGrp.Id = OBJ.Pid " & _
                        "WHERE 1 = 1 ORDER BY Id "


            objDataOperation.AddParameter("@FINANCIAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 315, "Financial"))
            objDataOperation.AddParameter("@CUSTOMER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 316, "Customer"))
            objDataOperation.AddParameter("@BUSINESS_PROCESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 317, "Business Process"))
            objDataOperation.AddParameter("@ORGANIZATION_CAPACITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 318, "Organization Capacity"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dtRow As DataRow In dsList.Tables(0).Rows
                dtTable.ImportRow(dtRow)
            Next

            Dim dPlanning As DataTable = Nothing

            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'If dtTable.Rows.Count > 0 Then
            '    Dim iRCnt As Integer = dtTable.Rows.Count

            '    For i As Integer = 0 To iRCnt - 1
            '        If CInt(dtTable.Rows(i).Item("ObjUnkid")) > 0 Then

            '            dPlanning = GetBSCPlanning(CInt(dtTable.Rows(i).Item("ObjUnkid")))

            '            If dPlanning.Rows.Count > 0 Then
            '                For Each dFRow As DataRow In dPlanning.Rows
            '                    Dim dNRow As DataRow = dtTable.NewRow

            '                    dNRow.Item("PId") = dtTable.Rows(i).Item("PId")
            '                    dNRow.Item("objGrpId") = dtTable.Rows(i).Item("PId")
            '                    dNRow.Item("ObjUnkid") = dtTable.Rows(i).Item("ObjUnkid")
            '                    dNRow.Item("KpiUnkid") = dFRow.Item("KPIUnkid")
            '                    dNRow.Item("TargetUnkid") = dFRow.Item("TargetUnkid")
            '                    dNRow.Item("InitiativeUnkid") = dFRow.Item("InitiativeUnkid")

            '                    dNRow.Item("perspective") = dtTable.Rows(i).Item("perspective")
            '                    dNRow.Item("objective") = ""
            '                    dNRow.Item("kpi") = dFRow.Item("KPI")
            '                    dNRow.Item("targets") = dFRow.Item("Target")
            '                    dNRow.Item("initiative") = dFRow.Item("Initiative")

            '                    dtTable.Rows.Add(dNRow)
            '                Next
            '            End If
            '        End If
            '    Next
            'End If

            If dtTable.Rows.Count > 0 Then
                Dim iRCnt As Integer = dtTable.Rows.Count

                'S.SANDEEP [ 02 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'dPlanning = GetFullBSC()
                dPlanning = GetFullBSC(intEmployeeId, intPeriodId)
                'S.SANDEEP [ 02 AUG 2013 ] -- END



                'S.SANDEEP [ 21 MAR 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES

                'S.SANDEEP [ 30 MAY 2014 ] -- START
                'Dim oWSett As New clsWeight_Setting(True)
                Dim oWSett As New clsWeight_Setting(intPeriodId)
                'S.SANDEEP [ 30 MAY 2014 ] -- END

                'S.SANDEEP [ 21 MAR 2013 ] -- END
                For i As Integer = 0 To iRCnt - 1
                    If CInt(dtTable.Rows(i).Item("ObjUnkid")) > 0 Then
                        Dim dTemp() As DataRow = dPlanning.Select("objectiveunkid = '" & dtTable.Rows(i).Item("ObjUnkid") & "'")
                        If dTemp.Length > 0 Then
                            For iCnt As Integer = 0 To dTemp.Length - 1
                            Dim dNRow As DataRow = dtTable.NewRow

                            dNRow.Item("PId") = dtTable.Rows(i).Item("PId")
                            dNRow.Item("objGrpId") = dtTable.Rows(i).Item("PId")
                            dNRow.Item("ObjUnkid") = dtTable.Rows(i).Item("ObjUnkid")
                                dNRow.Item("KpiUnkid") = dTemp(iCnt).Item("kpiunkid")
                                dNRow.Item("TargetUnkid") = dTemp(iCnt).Item("targetunkid")
                                dNRow.Item("InitiativeUnkid") = dTemp(iCnt).Item("initiativeunkid")

                            dNRow.Item("perspective") = dtTable.Rows(i).Item("perspective")
                            dNRow.Item("objective") = ""
                                dNRow.Item("kpi") = dTemp(iCnt).Item("KPI")
                                dNRow.Item("targets") = dTemp(iCnt).Item("Target")
                                dNRow.Item("initiative") = dTemp(iCnt).Item("Initiative")

                                'S.SANDEEP [ 21 MAR 2013 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                If oWSett._Weight_Optionid <> enWeight_Options.WEIGHT_EACH_ITEM Then
                                    Select Case oWSett._Weight_Typeid
                                        Case enWeight_Types.WEIGHT_FIELD2
                                            dNRow.Item("weight") = dTemp(iCnt).Item("kweight")
                                        Case enWeight_Types.WEIGHT_FIELD3
                                            dNRow.Item("weight") = dTemp(iCnt).Item("tweight")
                                        Case enWeight_Types.WEIGHT_FIELD5
                                            dNRow.Item("weight") = dTemp(iCnt).Item("iweight")
                                    End Select
                                End If
                                'S.SANDEEP [ 21 MAR 2013 ] -- END


                            dtTable.Rows.Add(dNRow)
                    Next
                            'For Each dFRow As DataRow In dPlanning.Rows
                            '    Dim dNRow As DataRow = dtTable.NewRow

                            '    dNRow.Item("PId") = dtTable.Rows(i).Item("PId")
                            '    dNRow.Item("objGrpId") = dtTable.Rows(i).Item("PId")
                            '    dNRow.Item("ObjUnkid") = dtTable.Rows(i).Item("ObjUnkid")
                            '    dNRow.Item("KpiUnkid") = dFRow.Item("KPIUnkid")
                            '    dNRow.Item("TargetUnkid") = dFRow.Item("TargetUnkid")
                            '    dNRow.Item("InitiativeUnkid") = dFRow.Item("InitiativeUnkid")

                            '    dNRow.Item("perspective") = dtTable.Rows(i).Item("perspective")
                            '    dNRow.Item("objective") = ""
                            '    dNRow.Item("kpi") = dFRow.Item("KPI")
                            '    dNRow.Item("targets") = dFRow.Item("Target")
                            '    dNRow.Item("initiative") = dFRow.Item("Initiative")

                            '    dtTable.Rows.Add(dNRow)
                            'Next
                End If
                    End If                    
            Next
            End If
            'S.SANDEEP [ 28 DEC 2012 ] -- END


            Dim dView As DataView = dtTable.DefaultView
            dView.Sort = "PId,ObjUnkid"
            dtTable = dView.ToTable


            'S.SANDEEP [ 18 AUG 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT analysisunkid,selfemployeeunkid,assessedemployeeunkid,assessoremployeeunkid,assessmodeid FROM " & _
            '          "( " & _
            '                 "SELECT " & _
            '                      "analysisunkid,selfemployeeunkid,assessedemployeeunkid,assessoremployeeunkid,assessmodeid " & _
            '                 "FROM hrbsc_analysis_master WHERE assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "'  AND  isvoid = 0 AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND selfemployeeunkid = '" & intEmployeeId & "' " & _
            '                 "UNION ALL " & _
            '                 "SELECT " & _
            '                      "analysisunkid,selfemployeeunkid,assessedemployeeunkid,assessoremployeeunkid,assessmodeid " & _
            '                 "FROM hrbsc_analysis_master WHERE assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND  isvoid = 0 AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND assessedemployeeunkid = '" & intEmployeeId & "' " & _
            '           ")AS Grp " & _
            '           "ORDER BY Grp.assessmodeid "


            'S.SANDEEP [ 28 DEC 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ = "SELECT analysisunkid,selfemployeeunkid,assessedemployeeunkid,assessoremployeeunkid,assessmodeid FROM " & _
            '          "( " & _
            '                 "SELECT " & _
            '                      "analysisunkid,selfemployeeunkid,assessedemployeeunkid,assessoremployeeunkid,assessmodeid " & _
            '                 "FROM " & StrDataBaseName & "..hrbsc_analysis_master WHERE assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "'  AND  isvoid = 0 AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND selfemployeeunkid = '" & intEmployeeId & "' " & _
            '                 "UNION ALL " & _
            '                 "SELECT " & _
            '                      "analysisunkid,selfemployeeunkid,assessedemployeeunkid,assessoremployeeunkid,assessmodeid " & _
            '                 "FROM " & StrDataBaseName & "..hrbsc_analysis_master WHERE assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND  isvoid = 0 AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND assessedemployeeunkid = '" & intEmployeeId & "' " & _
            '           ")AS Grp " & _
            '           "ORDER BY Grp.assessmodeid "

            strQ = "SELECT analysisunkid,selfemployeeunkid,assessedemployeeunkid,assessoremployeeunkid,assessmodeid FROM " & _
                      "( " & _
                             "SELECT " & _
                                  "analysisunkid,selfemployeeunkid,assessedemployeeunkid,assessoremployeeunkid,assessmodeid " & _
                             "FROM " & StrDataBaseName & "..hrbsc_analysis_master WHERE assessmodeid = '" & enAssessmentMode.SELF_ASSESSMENT & "'  AND  isvoid = 0 AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND selfemployeeunkid = '" & intEmployeeId & "' " & _
                             "UNION ALL " & _
                             "SELECT " & _
                                  "analysisunkid,selfemployeeunkid,assessedemployeeunkid,assessoremployeeunkid,assessmodeid " & _
                             "FROM " & StrDataBaseName & "..hrbsc_analysis_master WHERE assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND  isvoid = 0 AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND assessedemployeeunkid = '" & intEmployeeId & "' " & _
                             "UNION ALL " & _
                             "SELECT " & _
                                  "analysisunkid,selfemployeeunkid,assessedemployeeunkid,reviewerunkid,assessmodeid " & _
                             "FROM " & StrDataBaseName & "..hrbsc_analysis_master WHERE assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND  isvoid = 0 AND yearunkid = '" & intYearId & "' AND periodunkid = '" & intPeriodId & "' AND assessedemployeeunkid = '" & intEmployeeId & "' " & _
                       ")AS Grp " & _
                       "ORDER BY Grp.assessmodeid "
            'S.SANDEEP [ 28 DEC 2012 ] -- END

            'S.SANDEEP [ 18 AUG 2012 ] -- END

            

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables("List").Rows
                objDataOperation.ClearParameters()

                'S.SANDEEP [ 18 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'strQ = "SELECT " & _
                '                 " hrbsc_analysis_tran.objectiveunkid " & _
                '                 ",hrbsc_analysis_tran.kpiunkid " & _
                '                 ",hrbsc_analysis_tran.targetunkid " & _
                '                 ",hrbsc_analysis_tran.initiativeunkid " & _
                '                 ",ISNULL(hrresult_master.resultname,'') AS Result " & _
                '                 ",ISNULL(hrbsc_analysis_tran.remark,'') AS Remark " & _
                '                 ",assessoremployeeunkid "
                'Select Case CInt(dtRow.Item("assessmodeid"))
                '    Case enAssessmentMode.APPRAISER_ASSESSMENT
                '        strQ &= ",CASE WHEN hrexternal_assessor_master.ext_assessorunkid > 0 THEN hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END AS Appraiser " & _
                '                    ",hrexternal_assessor_master.ext_assessorunkid "
                'End Select
                'strQ &= "FROM hrbsc_analysis_tran " & _
                '                 "JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                '                 "JOIN hrobjective_master ON hrbsc_analysis_tran.objectiveunkid = hrobjective_master.objectiveunkid AND kpiunkid=-1 AND targetunkid = -1 AND initiativeunkid = -1 " & _
                '                 "JOIN hrbsc_analysis_master ON hrbsc_analysis_tran.analysisunkid = hrbsc_analysis_master.analysisunkid "
                'Select Case CInt(dtRow.Item("assessmodeid"))
                '    Case enAssessmentMode.APPRAISER_ASSESSMENT
                '        strQ &= "LEFT JOIN hremployee_master AS AEmp ON AEmp.employeeunkid = hrbsc_analysis_master.assessoremployeeunkid " & _
                '                    "LEFT JOIN hrexternal_assessor_master ON hrexternal_assessor_master.ext_assessorunkid = hrbsc_analysis_master.ext_assessorunkid "
                'End Select
                'strQ &= " WHERE hrbsc_analysis_tran.isvoid = 0 AND hrbsc_analysis_master.analysisunkid = '" & dtRow.Item("analysisunkid") & "'"

                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES                
                'strQ = "SELECT " & _
                '           " " & StrDataBaseName & "..hrbsc_analysis_tran.objectiveunkid " & _
                '           "," & StrDataBaseName & "..hrbsc_analysis_tran.kpiunkid " & _
                '           "," & StrDataBaseName & "..hrbsc_analysis_tran.targetunkid " & _
                '           "," & StrDataBaseName & "..hrbsc_analysis_tran.initiativeunkid " & _
                '           ",ISNULL(" & StrDataBaseName & "..hrresult_master.resultname,'') AS Result " & _
                '           ",ISNULL(" & StrDataBaseName & "..hrbsc_analysis_tran.remark,'') AS Remark " & _
                '           ",assessoremployeeunkid "
                strQ = "SELECT " & _
                                 " " & StrDataBaseName & "..hrbsc_analysis_tran.objectiveunkid " & _
                           ",CASE WHEN ISNULL(" & StrDataBaseName & "..hrbsc_analysis_tran.kpiunkid,-1) = 0 THEN -1 ELSE ISNULL(" & StrDataBaseName & "..hrbsc_analysis_tran.kpiunkid,-1) END AS kpiunkid" & _
                           ",CASE WHEN ISNULL(" & StrDataBaseName & "..hrbsc_analysis_tran.targetunkid,-1) = 0 THEN -1 ELSE ISNULL(" & StrDataBaseName & "..hrbsc_analysis_tran.targetunkid,-1) END AS targetunkid " & _
                           ",CASE WHEN ISNULL(" & StrDataBaseName & "..hrbsc_analysis_tran.initiativeunkid,-1) = 0 THEN -1 ELSE ISNULL(" & StrDataBaseName & "..hrbsc_analysis_tran.initiativeunkid,-1) END AS initiativeunkid  " & _
                                 ",ISNULL(" & StrDataBaseName & "..hrresult_master.resultname,'') AS Result " & _
                                 ",ISNULL(" & StrDataBaseName & "..hrbsc_analysis_tran.remark,'') AS Remark " & _
                           ",assessoremployeeunkid " & _
                           ",reviewerunkid "
                'S.SANDEEP [ 28 DEC 2012 ] -- END

                Select Case CInt(dtRow.Item("assessmodeid"))
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        strQ &= ",CASE WHEN " & StrDataBaseName & "..hrexternal_assessor_master.ext_assessorunkid > 0 THEN " & StrDataBaseName & "..hrexternal_assessor_master.displayname ELSE ISNULL(AEMP.firstname,'')+' '+ISNULL(AEMP.othername,'')+' '+ISNULL(AEMP.surname,'') END AS Appraiser " & _
                                    "," & StrDataBaseName & "..hrexternal_assessor_master.ext_assessorunkid "
                        'S.SANDEEP [ 28 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        strQ &= ", ISNULL(REmp.firstname,'')+' '+ISNULL(REmp.othername,'')+' '+ISNULL(REmp.surname,'')  AS Reviewer "
                        'S.SANDEEP [ 28 DEC 2012 ] -- END
                End Select

                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'strQ &= "FROM " & StrDataBaseName & "..hrbsc_analysis_tran " & _
                '                 "JOIN " & StrDataBaseName & "..hrresult_master ON " & StrDataBaseName & "..hrbsc_analysis_tran.resultunkid = " & StrDataBaseName & "..hrresult_master.resultunkid " & _
                '                 "JOIN " & StrDataBaseName & "..hrobjective_master ON " & StrDataBaseName & "..hrbsc_analysis_tran.objectiveunkid = " & StrDataBaseName & "..hrobjective_master.objectiveunkid AND kpiunkid=-1 AND targetunkid = -1 AND initiativeunkid = -1 " & _
                '                 "JOIN " & StrDataBaseName & "..hrbsc_analysis_master ON " & StrDataBaseName & "..hrbsc_analysis_tran.analysisunkid = " & StrDataBaseName & "..hrbsc_analysis_master.analysisunkid "

                strQ &= "FROM " & StrDataBaseName & "..hrbsc_analysis_tran " & _
                                 "JOIN " & StrDataBaseName & "..hrresult_master ON " & StrDataBaseName & "..hrbsc_analysis_tran.resultunkid = " & StrDataBaseName & "..hrresult_master.resultunkid " & _
                                 "JOIN " & StrDataBaseName & "..hrobjective_master ON " & StrDataBaseName & "..hrbsc_analysis_tran.objectiveunkid = " & StrDataBaseName & "..hrobjective_master.objectiveunkid  " & _
                                 "JOIN " & StrDataBaseName & "..hrbsc_analysis_master ON " & StrDataBaseName & "..hrbsc_analysis_tran.analysisunkid = " & StrDataBaseName & "..hrbsc_analysis_master.analysisunkid "
                'S.SANDEEP [ 28 DEC 2012 ] -- END


                Select Case CInt(dtRow.Item("assessmodeid"))
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        strQ &= "LEFT JOIN " & StrDataBaseName & "..hremployee_master AS AEmp ON AEmp.employeeunkid = " & StrDataBaseName & "..hrbsc_analysis_master.assessoremployeeunkid " & _
                                    "LEFT JOIN " & StrDataBaseName & "..hrexternal_assessor_master ON " & StrDataBaseName & "..hrexternal_assessor_master.ext_assessorunkid = " & StrDataBaseName & "..hrbsc_analysis_master.ext_assessorunkid "
                        'S.SANDEEP [ 28 DEC 2012 ] -- START
                        'ENHANCEMENT : TRA CHANGES
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        strQ &= " LEFT JOIN " & StrDataBaseName & "..hremployee_master AS REmp ON REmp.employeeunkid = " & StrDataBaseName & "..hrbsc_analysis_master.reviewerunkid "
                        'S.SANDEEP [ 28 DEC 2012 ] -- END
                End Select
                strQ &= " WHERE " & StrDataBaseName & "..hrbsc_analysis_tran.isvoid = 0 AND " & StrDataBaseName & "..hrbsc_analysis_master.analysisunkid = '" & dtRow.Item("analysisunkid") & "'"
                'S.SANDEEP [ 18 AUG 2012 ] -- END

                
                'S.SANDEEP [ 28 DEC 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES

                'S.SANDEEP [ 30 MAY 2014 ] -- START
                'Dim objWSetting As New clsWeight_Setting(True)
                Dim objWSetting As New clsWeight_Setting(intPeriodId)
                'S.SANDEEP [ 30 MAY 2014 ] -- END

                If objWSetting._Weight_Typeid <= 0 Then
                    strQ &= " AND kpiunkid > 0 AND targetunkid > 0 AND initiativeunkid  > 0  "
                ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
                    strQ &= " AND kpiunkid=-1 AND targetunkid = -1 AND initiativeunkid = -1 "
                ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
                    strQ &= " AND kpiunkid > 0 "
                ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
                    strQ &= " AND targetunkid > 0 "
                ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD5 Then
                    strQ &= " AND initiativeunkid > 0 "
                End If
                'S.SANDEEP [ 28 DEC 2012 ] -- END


                'S.SANDEEP [ 14 JUNE 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                strQ &= " AND hrbsc_analysis_master.iscommitted = 1 "
                'S.SANDEEP [ 14 JUNE 2012 ] -- END


                Dim dsData As DataSet = objDataOperation.ExecQuery(strQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For Each dRow As DataRow In dsData.Tables("List").Rows
                    Select Case CInt(dtRow.Item("assessmodeid"))
                        Case enAssessmentMode.SELF_ASSESSMENT
                            'S.SANDEEP [ 24 APR 2014 ] -- START
                            Dim dSTemp() As DataRow = Nothing
                            'Dim dSTemp() As DataRow = dtTable.Select("ObjUnkid ='" & dRow.Item("objectiveunkid") & "' AND KpiUnkid='" & dRow.Item("kpiunkid") & "' AND TargetUnkid='" & dRow.Item("targetunkid") & "' AND InitiativeUnkid = '" & dRow.Item("initiativeunkid") & "'")
                            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                                dSTemp = dtTable.Select("InitiativeUnkid = '" & dRow.Item("initiativeunkid") & "'")
                            ElseIf objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                                Select Case objWSetting._Weight_Typeid
                                    Case enWeight_Types.WEIGHT_FIELD1
                                        dSTemp = dtTable.Select("ObjUnkid ='" & dRow.Item("objectiveunkid") & "'")
                                    Case enWeight_Types.WEIGHT_FIELD2
                                        dSTemp = dtTable.Select("KpiUnkid='" & dRow.Item("kpiunkid") & "'")
                                    Case enWeight_Types.WEIGHT_FIELD3
                                        dSTemp = dtTable.Select("TargetUnkid='" & dRow.Item("targetunkid") & "'")
                                    Case enWeight_Types.WEIGHT_FIELD5
                                        dSTemp = dtTable.Select("InitiativeUnkid = '" & dRow.Item("initiativeunkid") & "'")
                                End Select
                            End If
                            'S.SANDEEP [ 24 APR 2014 ] -- END
                            If dSTemp.Length > 0 Then
                                dSTemp(0).Item("self") = dRow.Item("Result")
                                dSTemp(0).Item("selfremark") = dRow.Item("Remark")
                                dSTemp(0).AcceptChanges()
                            End If
                        Case enAssessmentMode.APPRAISER_ASSESSMENT

                            If dRow.Item("assessoremployeeunkid") <= 0 Then
                                If dtTable.Columns.Contains("Column" & dRow.Item("ext_assessorunkid").ToString & "ExtId") = False Then
                                    dtTable.Columns.Add("Column" & dRow.Item("ext_assessorunkid").ToString & "ExtId", System.Type.GetType("System.String")).Caption = "Assessor [ " & dRow.Item("Appraiser") & " ]"
                                    dtTable.Columns.Add("Column" & dRow.Item("ext_assessorunkid").ToString & "ExRemark", System.Type.GetType("System.String")).Caption = dRow.Item("Appraiser") & "  " & "Remark"
                                End If
                            Else
                                If dtTable.Columns.Contains("Column" & dRow.Item("assessoremployeeunkid").ToString & "IntId") = False Then
                                    dtTable.Columns.Add("Column" & dRow.Item("assessoremployeeunkid").ToString & "IntId", System.Type.GetType("System.String")).Caption = "Assessor [ " & dRow.Item("Appraiser") & " ]"
                                    dtTable.Columns.Add("Column" & dRow.Item("assessoremployeeunkid").ToString & "IntRemark", System.Type.GetType("System.String")).Caption = dRow.Item("Appraiser") & "  " & "Remark"
                                End If
                            End If
                            
                            'S.SANDEEP [ 24 APR 2014 ] -- START
                            Dim dSTemp() As DataRow = Nothing
                            'Dim dSTemp() As DataRow = dtTable.Select("ObjUnkid ='" & dRow.Item("objectiveunkid") & "' AND KpiUnkid='" & dRow.Item("kpiunkid") & "' AND TargetUnkid='" & dRow.Item("targetunkid") & "' AND InitiativeUnkid = '" & dRow.Item("initiativeunkid") & "'")
                            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                                dSTemp = dtTable.Select("InitiativeUnkid = '" & dRow.Item("initiativeunkid") & "'")
                            ElseIf objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                                Select Case objWSetting._Weight_Typeid
                                    Case enWeight_Types.WEIGHT_FIELD1
                                        dSTemp = dtTable.Select("ObjUnkid ='" & dRow.Item("objectiveunkid") & "'")
                                    Case enWeight_Types.WEIGHT_FIELD2
                                        dSTemp = dtTable.Select("KpiUnkid='" & dRow.Item("kpiunkid") & "'")
                                    Case enWeight_Types.WEIGHT_FIELD3
                                        dSTemp = dtTable.Select("TargetUnkid='" & dRow.Item("targetunkid") & "'")
                                    Case enWeight_Types.WEIGHT_FIELD5
                                        dSTemp = dtTable.Select("InitiativeUnkid = '" & dRow.Item("initiativeunkid") & "'")
                                End Select
                            End If
                            'S.SANDEEP [ 24 APR 2014 ] -- END

                            If dSTemp.Length > 0 Then
                                If dRow.Item("assessoremployeeunkid") <= 0 Then
                                    dSTemp(0).Item("Column" & dRow.Item("ext_assessorunkid").ToString & "ExtId") = dRow.Item("Result")
                                    dSTemp(0).Item("Column" & dRow.Item("ext_assessorunkid").ToString & "ExRemark") = dRow.Item("Remark")
                                Else
                                    dSTemp(0).Item("Column" & dtRow.Item("assessoremployeeunkid").ToString & "IntId") = dRow.Item("Result")
                                    dSTemp(0).Item("Column" & dtRow.Item("assessoremployeeunkid").ToString & "IntRemark") = dRow.Item("Remark")
                                End If
                                dSTemp(0).AcceptChanges()
                            End If

                            'S.SANDEEP [ 28 DEC 2012 ] -- START
                            'ENHANCEMENT : TRA CHANGES
                        Case enAssessmentMode.REVIEWER_ASSESSMENT
                            If dRow.Item("reviewerunkid") > 0 Then
                                If dtTable.Columns.Contains("Column" & dRow.Item("reviewerunkid").ToString & "RewId") = False Then
                                    dtTable.Columns.Add("Column" & dRow.Item("reviewerunkid").ToString & "RewId", System.Type.GetType("System.String")).Caption = "Reviewer [ " & dRow.Item("Reviewer") & " ]"
                                    dtTable.Columns.Add("Column" & dRow.Item("reviewerunkid").ToString & "RewIdRemark", System.Type.GetType("System.String")).Caption = dRow.Item("Reviewer") & "  " & "Remark"
                                End If
                            End If

                            'S.SANDEEP [ 24 APR 2014 ] -- START
                            Dim dSTemp() As DataRow = Nothing
                            'Dim dSTemp() As DataRow = dtTable.Select("ObjUnkid ='" & dRow.Item("objectiveunkid") & "' AND KpiUnkid='" & dRow.Item("kpiunkid") & "' AND TargetUnkid='" & dRow.Item("targetunkid") & "' AND InitiativeUnkid = '" & dRow.Item("initiativeunkid") & "'")
                            If objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                                dSTemp = dtTable.Select("InitiativeUnkid = '" & dRow.Item("initiativeunkid") & "'")
                            ElseIf objWSetting._Weight_Optionid = enWeight_Options.WEIGHT_BASED_ON Then
                                Select Case objWSetting._Weight_Typeid
                                    Case enWeight_Types.WEIGHT_FIELD1
                                        dSTemp = dtTable.Select("ObjUnkid ='" & dRow.Item("objectiveunkid") & "'")
                                    Case enWeight_Types.WEIGHT_FIELD2
                                        dSTemp = dtTable.Select("KpiUnkid='" & dRow.Item("kpiunkid") & "'")
                                    Case enWeight_Types.WEIGHT_FIELD3
                                        dSTemp = dtTable.Select("TargetUnkid='" & dRow.Item("targetunkid") & "'")
                                    Case enWeight_Types.WEIGHT_FIELD5
                                        dSTemp = dtTable.Select("InitiativeUnkid = '" & dRow.Item("initiativeunkid") & "'")
                                End Select
                            End If
                            'S.SANDEEP [ 24 APR 2014 ] -- END

                            If dSTemp.Length > 0 Then
                                dSTemp(0).Item("Column" & dRow.Item("reviewerunkid").ToString & "RewId") = dRow.Item("Result")
                                dSTemp(0).Item("Column" & dRow.Item("reviewerunkid").ToString & "RewIdRemark") = dRow.Item("Remark")
                                dSTemp(0).AcceptChanges()
                            End If
                            'S.SANDEEP [ 28 DEC 2012 ] -- END

                    End Select
                Next

            Next

            dsList = New DataSet

            dsList.Tables.Add(dtTable)

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAssessmentResultView; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>    
    ''' <purpose>  </purpose>
    Public Sub Get_Items_Count(ByVal intPerspectiveId As Integer, _
                               Optional ByVal intEmployeeId As Integer = -1, _
                               Optional ByVal strIsBSC_ByEmployee As String = "") 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
        'Public Sub Get_Items_Count(ByVal intPerspectiveId As Integer, Optional ByVal intEmployeeId As Integer = -1)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        objDataOperation = New clsDataOperation
        Try
            Dim strObjectiveIds, strKpiIds, strTargetIds, strInitiativeIds As String

            strObjectiveIds = String.Empty : strKpiIds = String.Empty : strTargetIds = String.Empty : strInitiativeIds = String.Empty

            strQ = "SELECT " & _
                            "STUFF((SELECT ',' + CAST(hrobjective_master.objectiveunkid AS NVARCHAR(50)) " & _
                        "FROM hrobjective_master " & _
                        "WHERE isactive = 1 "
            If strIsBSC_ByEmployee.Trim.Length <= 0 Then
                strIsBSC_ByEmployee = ConfigParameter._Object._IsBSC_ByEmployee.ToString
            End If

            If CBool(strIsBSC_ByEmployee) = True Then 'S.SANDEEP [ 27 APRIL 2012 ] -- START -- END
                'If ConfigParameter._Object._IsBSC_ByEmployee Then
                strQ &= " AND employeeunkid = '" & intEmployeeId & "' "
            End If
            strQ &= " ORDER BY hrobjective_master.objectiveunkid " & _
                        "FOR " & _
                        "XML PATH('')), 1, 1, '') AS CSV "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                strObjectiveIds = dsList.Tables(0).Rows(0)("CSV")
            End If

            If strObjectiveIds.Trim.Length > 0 Then
                '    strQ = "SELECT " & _
                '                                 "STUFF((SELECT ',' + CAST(hrkpi_master.kpiunkid AS NVARCHAR(50)) " & _
                '                            "FROM hrkpi_master " & _
                '                            "WHERE objectiveunkid IN (" & strObjectiveIds & ") AND isactive = 1 " & _
                '                            "ORDER BY hrkpi_master.kpiunkid " & _
                '                            "FOR " & _
                '                            "XML PATH('')), 1, 1, '') AS CSV "

                '    dsList = objDataOperation.ExecQuery(strQ, "List")

                '    If objDataOperation.ErrorMessage <> "" Then
                '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                '        Throw exForce
                '    End If

                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        strKpiIds = dsList.Tables(0).Rows(0)("CSV")
                '    End If

                '    If strKpiIds.Trim.Length > 0 Then
                '        strQ = "SELECT " & _
                '                     "STUFF((SELECT ','+ CAST(targetunkid AS NVARCHAR(50)) " & _
                '                "FROM " & _
                '                "( " & _
                '                          "SELECT * FROM dbo.hrtarget_master WHERE kpiunkid IN (" & strKpiIds & ") AND objectiveunkid IN (" & strObjectiveIds & ") AND isactive = 1 " & _
                '                     "UNION " & _
                '                          "SELECT * FROM dbo.hrtarget_master WHERE kpiunkid IN (" & strKpiIds & ") AND objectiveunkid <=0 AND isactive = 1 " & _
                '                     "UNION " & _
                '                          "SELECT * FROM dbo.hrtarget_master WHERE objectiveunkid IN (" & strObjectiveIds & ") AND kpiunkid <=0 AND isactive = 1 " & _
                '                ") AS Ids " & _
                '                "FOR " & _
                '                "XML PATH('')), 1, 1, '') AS CSV "
                '    Else
                '        strQ = "SELECT " & _
                '                     "STUFF((SELECT ','+ CAST(targetunkid AS NVARCHAR(50)) " & _
                '                "FROM " & _
                '                "( " & _
                '                        "SELECT * FROM dbo.hrtarget_master WHERE objectiveunkid IN (" & strObjectiveIds & ") AND kpiunkid <=0 AND isactive = 1 " & _
                '                ") AS Ids " & _
                '                "FOR " & _
                '                "XML PATH('')), 1, 1, '') AS CSV "
                '    End If

                '    dsList = objDataOperation.ExecQuery(strQ, "List")

                '    If objDataOperation.ErrorMessage <> "" Then
                '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                '        Throw exForce
                '    End If

                '    If dsList.Tables(0).Rows.Count > 0 Then
                '        strTargetIds = dsList.Tables(0).Rows(0)("CSV")
                '    End If

                '    If strTargetIds.Trim.Length > 0 Then
                '        strQ = "SELECT " & _
                '                         "STUFF((SELECT ',' + CAST(initiativeunkid AS NVARCHAR(50)) FROM " & _
                '                    "( " & _
                '                              "SELECT * FROM hrinitiative_master WHERE targetunkid IN (" & strTargetIds & ") AND objectiveunkid IN (" & strObjectiveIds & ") AND isactive = 1 " & _
                '                         "UNION " & _
                '                              "SELECT * FROM hrinitiative_master WHERE targetunkid IN (" & strTargetIds & ") AND objectiveunkid <=0 AND isactive = 1 " & _
                '                         "UNION " & _
                '                              "SELECT * FROM hrinitiative_master WHERE objectiveunkid IN (" & strObjectiveIds & ") AND targetunkid <=0 AND isactive = 1 " & _
                '                    ") AS Ids " & _
                '                    "FOR " & _
                '                    "XML PATH('')), 1, 1, '') AS CSV "
                '    Else
                '        strQ = "SELECT " & _
                '                         "STUFF((SELECT ',' + CAST(initiativeunkid AS NVARCHAR(50)) FROM " & _
                '                    "( " & _
                '                           "SELECT * FROM hrinitiative_master WHERE objectiveunkid IN (" & strObjectiveIds & ") AND targetunkid <=0 AND isactive = 1 " & _
                '                    ") AS Ids " & _
                '                    "FOR " & _
                '                    "XML PATH('')), 1, 1, '') AS CSV "
                '    End If

                'dsList = objDataOperation.ExecQuery(strQ, "List")

                'If objDataOperation.ErrorMessage <> "" Then
                '    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                '    Throw exForce
                'End If

                'If dsList.Tables(0).Rows.Count > 0 Then
                '    strInitiativeIds = dsList.Tables(0).Rows(0)("CSV")
                'End If

                Dim iCnt As Integer = -1

                strQ = "SELECT * FROM hrobjective_master WHERE objectiveunkid IN(" & strObjectiveIds & ") AND isactive = 1"
                iCnt = objDataOperation.RecordCount(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If mdicAllItems.ContainsKey(0) = False Then
                    mdicAllItems.Add(0, iCnt)
                End If

                End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Items_Count; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub


    'S.SANDEEP [ 28 DEC 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function GetFullBSC(Optional ByVal iEmpId As Integer = 0, Optional ByVal iPeriodId As Integer = 0) As DataTable        'S.SANDEEP [ 25 JULY 2013 ] -- START -- END

        'S.SANDEEP [ 24 APR 2014 ] -- START
        Dim StrQ As String = String.Empty
        Dim dtTable As DataTable
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            ''Public Function GetFullBSC() As DataTable
            'Dim StrQ As String = String.Empty
            'Dim dtTable As DataTable
            'Dim dsList As New DataSet
            'Dim exForce As Exception
            'Try
            '    objDataOperation = New clsDataOperation

            '    'S.SANDEEP [ 03 AUG 2013 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    Dim sObjectives As String = String.Empty
            '    StrQ = "SELECT ISNULL(STUFF((SELECT ',' + CAST(objectiveunkid AS NVARCHAR(MAX)) FROM hrobjective_master WHERE isactive = 1 AND periodunkid = '" & iPeriodId & "' AND employeeunkid = '" & iEmpId & "' FOR XML PATH('')),1,1,''),'') AS oCsV  "
            '    dsList = objDataOperation.ExecQuery(StrQ, "sTab")

            '    If objDataOperation.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            '    If dsList.Tables("sTab").Rows.Count > 0 Then
            '        sObjectives = dsList.Tables("sTab").Rows(0).Item("oCsV")
            '    End If
            '    'S.SANDEEP [ 03 AUG 2013 ] -- END




            '    'S.SANDEEP [ 21 MAR 2013 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    'StrQ = "SELECT " & _
            '    '        " hrobjective_master.perspectiveunkid " & _
            '    '        ",hrobjective_master.objectiveunkid AS objectiveunkid " & _
            '    '        ",hrobjective_master.name AS objective " & _
            '    '        ",ISNULL(kpi.name,'') AS KPI " & _
            '    '        ",ISNULL(kpi.kpiunkid,-1) AS kpiunkid " & _
            '    '        ",ISNULL(A.targets,'') AS Target " & _
            '    '        ",ISNULL(A.targetunkid,-1) AS targetunkid " & _
            '    '        ",ISNULL(B.name,'') AS Initiative " & _
            '    '        ",ISNULL(B.initiativeunkid,-1) AS initiativeunkid " & _
            '    '        "FROM hrobjective_master " & _
            '    '        "LEFT JOIN " & _
            '    '        "( " & _
            '    '        "   SELECT kpiunkid,name,objectiveunkid " & _
            '    '        "   FROM hrkpi_master " & _
            '    '        ")AS kpi ON dbo.hrobjective_master.objectiveunkid = kpi.objectiveunkid " & _
            '    '        "LEFT JOIN " & _
            '    '        "( " & _
            '    '        "   SELECT " & _
            '    '        "    perspectiveunkid " & _
            '    '        "   ,hrobjective_master.objectiveunkid AS objectiveunkid " & _
            '    '        "   ,hrobjective_master.name AS objective " & _
            '    '        "   ,tgt.name AS targets " & _
            '    '        "   ,tgt.targetunkid AS targetunkid " & _
            '    '        "   ,tgt.kpiunkid AS kpiunkid " & _
            '    '        "FROM hrobjective_master " & _
            '    '        "JOIN " & _
            '    '        "( " & _
            '    '        "   SELECT " & _
            '    '        "         targetunkid " & _
            '    '        "       ,name " & _
            '    '        "       ,objectiveunkid " & _
            '    '        "       ,kpiunkid " & _
            '    '        "   FROM hrtarget_master WHERE kpiunkid > 0 " & _
            '    '        ")AS tgt  ON hrobjective_master.objectiveunkid = tgt.objectiveunkid " & _
            '    '        ") AS A ON kpi.objectiveunkid = A.objectiveunkid AND kpi.kpiunkid = A.kpiunkid " & _
            '    '        "LEFT JOIN " & _
            '    '        "( " & _
            '    '        "       SELECT " & _
            '    '        "           initiativeunkid " & _
            '    '        "          ,name " & _
            '    '        "          ,objectiveunkid " & _
            '    '        "         ,targetunkid " & _
            '    '        "FROM hrinitiative_master WHERE targetunkid > 0 " & _
            '    '        ")AS B ON hrobjective_master.objectiveunkid = B.objectiveunkid AND A.targetunkid = B.targetunkid " & _
            '    '        "UNION " & _
            '    '        "SELECT " & _
            '    '        "    hrobjective_master.perspectiveunkid " & _
            '    '        "   ,hrobjective_master.objectiveunkid AS objectiveunkid " & _
            '    '        "   ,hrobjective_master.name AS objective " & _
            '    '        "   ,'' AS kpi " & _
            '    '        "   ,-1 AS kpiunkid " & _
            '    '        "   ,ISNULL(A.name,'') AS targets " & _
            '    '        "   ,ISNULL(A.targetunkid,0) AS targetunkid " & _
            '    '        "   ,ISNULL(B.name,'') AS initiative " & _
            '    '        "   ,ISNULL(B.initiativeunkid,0) AS initiativeunkid " & _
            '    '        "FROM hrobjective_master " & _
            '    '        "LEFT JOIN " & _
            '    '        "( " & _
            '    '        "   SELECT " & _
            '    '        "        targetunkid " & _
            '    '        "       ,name " & _
            '    '        "       ,objectiveunkid " & _
            '    '        "       ,kpiunkid " & _
            '    '        "FROM hrtarget_master " & _
            '    '        "WHERE kpiunkid <= 0 " & _
            '    '        ") AS A ON hrobjective_master.objectiveunkid = A.objectiveunkid " & _
            '    '        "LEFT JOIN " & _
            '    '        "( " & _
            '    '        "   SELECT " & _
            '    '        "        initiativeunkid " & _
            '    '        "       ,name " & _
            '    '        "       ,objectiveunkid " & _
            '    '        "       ,targetunkid " & _
            '    '        "FROM hrinitiative_master WHERE targetunkid <= 0 " & _
            '    '        ")AS B ON hrobjective_master.objectiveunkid = B.objectiveunkid " & _
            '    '        "WHERE A.targetunkid > 0 OR B.initiativeunkid > 0 "


            '    'S.SANDEEP [ 25 JULY 2013 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    'StrQ = "SELECT " & _
            '    '                " hrobjective_master.perspectiveunkid " & _
            '    '                ",hrobjective_master.objectiveunkid AS objectiveunkid " & _
            '    '                ",hrobjective_master.name AS objective " & _
            '    '                ",ISNULL(kpi.name,'') AS KPI " & _
            '    '                ",ISNULL(kpi.kpiunkid,-1) AS kpiunkid " & _
            '    '                ",ISNULL(A.targets,'') AS Target " & _
            '    '                ",ISNULL(A.targetunkid,-1) AS targetunkid " & _
            '    '                ",ISNULL(B.name,'') AS Initiative " & _
            '    '                ",ISNULL(B.initiativeunkid,-1) AS initiativeunkid " & _
            '    '        ",ISNULL(CAST(kpi.kweight AS NVARCHAR(MAX)),'') AS kweight " & _
            '    '        ",ISNULL(CAST(A.tweight AS NVARCHAR(MAX)),'') AS tweight " & _
            '    '        ",ISNULL(CAST(B.iweight AS NVARCHAR(MAX)),'') AS iweight " & _
            '    '            "FROM hrobjective_master " & _
            '    '            "LEFT JOIN " & _
            '    '            "( " & _
            '    '        "   SELECT kpiunkid,name,objectiveunkid,weight AS kweight " & _
            '    '        "   FROM hrkpi_master WHERE hrkpi_master.isactive = 1 " & _
            '    '            ")AS kpi ON dbo.hrobjective_master.objectiveunkid = kpi.objectiveunkid " & _
            '    '            "LEFT JOIN " & _
            '    '            "( " & _
            '    '            "   SELECT " & _
            '    '            "    perspectiveunkid " & _
            '    '            "   ,hrobjective_master.objectiveunkid AS objectiveunkid " & _
            '    '            "   ,hrobjective_master.name AS objective " & _
            '    '            "   ,tgt.name AS targets " & _
            '    '            "   ,tgt.targetunkid AS targetunkid " & _
            '    '            "   ,tgt.kpiunkid AS kpiunkid " & _
            '    '        "   ,tweight AS tweight " & _
            '    '            "FROM hrobjective_master " & _
            '    '            "JOIN " & _
            '    '            "( " & _
            '    '            "   SELECT " & _
            '    '            "         targetunkid " & _
            '    '            "       ,name " & _
            '    '            "       ,objectiveunkid " & _
            '    '            "       ,kpiunkid " & _
            '    '        "       ,weight AS tweight " & _
            '    '        "   FROM hrtarget_master WHERE kpiunkid > 0 AND hrtarget_master.isactive = 1 " & _
            '    '        ")AS tgt  ON hrobjective_master.objectiveunkid = tgt.objectiveunkid AND hrobjective_master.isactive = 1 " & _
            '    '            ") AS A ON kpi.objectiveunkid = A.objectiveunkid AND kpi.kpiunkid = A.kpiunkid " & _
            '    '            "LEFT JOIN " & _
            '    '            "( " & _
            '    '            "       SELECT " & _
            '    '            "           initiativeunkid " & _
            '    '            "          ,name " & _
            '    '            "          ,objectiveunkid " & _
            '    '            "         ,targetunkid " & _
            '    '        "         ,weight AS iweight " & _
            '    '        "FROM hrinitiative_master WHERE targetunkid > 0 AND hrinitiative_master.isactive = 1 " & _
            '    '        ")AS B ON hrobjective_master.objectiveunkid = B.objectiveunkid AND A.targetunkid = B.targetunkid  AND hrobjective_master.isactive = 1 " & _
            '    '        "UNION " & _
            '    '            "SELECT " & _
            '    '            "    hrobjective_master.perspectiveunkid " & _
            '    '            "   ,hrobjective_master.objectiveunkid AS objectiveunkid " & _
            '    '            "   ,hrobjective_master.name AS objective " & _
            '    '            "   ,'' AS kpi " & _
            '    '            "   ,-1 AS kpiunkid " & _
            '    '            "   ,ISNULL(A.name,'') AS targets " & _
            '    '            "   ,ISNULL(A.targetunkid,0) AS targetunkid " & _
            '    '            "   ,ISNULL(B.name,'') AS initiative " & _
            '    '            "   ,ISNULL(B.initiativeunkid,0) AS initiativeunkid " & _
            '    '        "   ,'' AS kweight " & _
            '    '        "   ,ISNULL(CAST(A.tweight AS NVARCHAR(MAX)),'') AS tweight " & _
            '    '        "   ,ISNULL(CAST(B.iweight AS NVARCHAR(MAX)),'') AS iweight " & _
            '    '            "FROM hrobjective_master " & _
            '    '            "LEFT JOIN " & _
            '    '            "( " & _
            '    '            "   SELECT " & _
            '    '            "        targetunkid " & _
            '    '            "       ,name " & _
            '    '            "       ,objectiveunkid " & _
            '    '            "       ,kpiunkid " & _
            '    '        "       ,weight AS tweight " & _
            '    '            "FROM hrtarget_master " & _
            '    '        "WHERE kpiunkid <= 0 AND hrtarget_master.isactive = 1 " & _
            '    '            ") AS A ON hrobjective_master.objectiveunkid = A.objectiveunkid " & _
            '    '            "LEFT JOIN " & _
            '    '            "( " & _
            '    '            "   SELECT " & _
            '    '            "        initiativeunkid " & _
            '    '            "       ,name " & _
            '    '            "       ,objectiveunkid " & _
            '    '            "       ,targetunkid " & _
            '    '        "       ,weight AS iweight " & _
            '    '        "FROM hrinitiative_master WHERE targetunkid <= 0 AND hrinitiative_master.isactive = 1 " & _
            '    '            ")AS B ON hrobjective_master.objectiveunkid = B.objectiveunkid " & _
            '    '        "WHERE A.targetunkid > 0 OR B.initiativeunkid > 0 AND hrobjective_master.isactive = 1 "

            'StrQ = "SELECT " & _
            '                " hrobjective_master.perspectiveunkid " & _
            '                ",hrobjective_master.objectiveunkid AS objectiveunkid " & _
            '                ",hrobjective_master.name AS objective " & _
            '                ",ISNULL(kpi.name,'') AS KPI " & _
            '                ",ISNULL(kpi.kpiunkid,-1) AS kpiunkid " & _
            '                ",ISNULL(A.targets,'') AS Target " & _
            '                ",ISNULL(A.targetunkid,-1) AS targetunkid " & _
            '                ",ISNULL(B.name,'') AS Initiative " & _
            '                ",ISNULL(B.initiativeunkid,-1) AS initiativeunkid " & _
            '        ",ISNULL(CAST(kpi.kweight AS NVARCHAR(MAX)),'') AS kweight " & _
            '        ",ISNULL(CAST(A.tweight AS NVARCHAR(MAX)),'') AS tweight " & _
            '        ",ISNULL(CAST(B.iweight AS NVARCHAR(MAX)),'') AS iweight " & _
            '            "FROM hrobjective_master " & _
            '            "LEFT JOIN " & _
            '            "( " & _
            '        "   SELECT kpiunkid,name,objectiveunkid,weight AS kweight " & _
            '        "   FROM hrkpi_master WHERE hrkpi_master.isactive = 1 " & _
            '            ")AS kpi ON dbo.hrobjective_master.objectiveunkid = kpi.objectiveunkid " & _
            '            "LEFT JOIN " & _
            '            "( " & _
            '            "   SELECT " & _
            '            "    perspectiveunkid " & _
            '            "   ,hrobjective_master.objectiveunkid AS objectiveunkid " & _
            '            "   ,hrobjective_master.name AS objective " & _
            '            "   ,tgt.name AS targets " & _
            '            "   ,tgt.targetunkid AS targetunkid " & _
            '            "   ,tgt.kpiunkid AS kpiunkid " & _
            '        "   ,tweight AS tweight " & _
            '            "FROM hrobjective_master " & _
            '            "JOIN " & _
            '            "( " & _
            '            "   SELECT " & _
            '            "         targetunkid " & _
            '            "       ,name " & _
            '            "       ,objectiveunkid " & _
            '            "       ,kpiunkid " & _
            '        "       ,weight AS tweight " & _
            '        "   FROM hrtarget_master WHERE kpiunkid > 0 AND hrtarget_master.isactive = 1 " & _
            '        ")AS tgt  ON hrobjective_master.objectiveunkid = tgt.objectiveunkid AND hrobjective_master.isactive = 1 " & _
            '            ") AS A ON kpi.objectiveunkid = A.objectiveunkid AND kpi.kpiunkid = A.kpiunkid " & _
            '            "LEFT JOIN " & _
            '            "( " & _
            '            "       SELECT " & _
            '            "           initiativeunkid " & _
            '            "          ,name " & _
            '            "          ,objectiveunkid " & _
            '            "         ,targetunkid " & _
            '        "         ,weight AS iweight " & _
            '        "FROM hrinitiative_master WHERE targetunkid > 0 AND hrinitiative_master.isactive = 1 " & _
            '            ")AS B ON hrobjective_master.objectiveunkid = B.objectiveunkid AND A.targetunkid = B.targetunkid  " & _
            '            "  WHERE hrobjective_master.isactive = 1 "

            '    If iEmpId > 0 Then
            '        StrQ &= " AND hrobjective_master.employeeunkid = '" & iEmpId & "' "
            '    End If
            '    If iPeriodId > 0 Then
            '        StrQ &= " AND hrobjective_master.periodunkid = '" & iPeriodId & "' "
            '    End If

            '    'S.SANDEEP [ 02 AUG 2013 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    'StrQ &= "UNION " & _
            '    '            "SELECT " & _
            '    '            "    hrobjective_master.perspectiveunkid " & _
            '    '            "   ,hrobjective_master.objectiveunkid AS objectiveunkid " & _
            '    '            "   ,hrobjective_master.name AS objective " & _
            '    '            "   ,'' AS kpi " & _
            '    '            "   ,-1 AS kpiunkid " & _
            '    '            "   ,ISNULL(A.name,'') AS targets " & _
            '    '            "   ,ISNULL(A.targetunkid,0) AS targetunkid " & _
            '    '            "   ,ISNULL(B.name,'') AS initiative " & _
            '    '            "   ,ISNULL(B.initiativeunkid,0) AS initiativeunkid " & _
            '    '            "   ,'' AS kweight " & _
            '    '            "   ,ISNULL(CAST(A.tweight AS NVARCHAR(MAX)),'') AS tweight " & _
            '    '            "   ,ISNULL(CAST(B.iweight AS NVARCHAR(MAX)),'') AS iweight " & _
            '    '            "FROM hrobjective_master " & _
            '    '            "LEFT JOIN " & _
            '    '            "( " & _
            '    '            "   SELECT " & _
            '    '            "        targetunkid " & _
            '    '            "       ,name " & _
            '    '            "       ,objectiveunkid " & _
            '    '            "       ,kpiunkid " & _
            '    '            "       ,weight AS tweight " & _
            '    '            "FROM hrtarget_master " & _
            '    '            "WHERE kpiunkid <= 0 AND hrtarget_master.isactive = 1 " & _
            '    '            ") AS A ON hrobjective_master.objectiveunkid = A.objectiveunkid " & _
            '    '            "LEFT JOIN " & _
            '    '            "( " & _
            '    '            "   SELECT " & _
            '    '            "        initiativeunkid " & _
            '    '            "       ,name " & _
            '    '            "       ,objectiveunkid " & _
            '    '            "       ,targetunkid " & _
            '    '            "       ,weight AS iweight " & _
            '    '            "FROM hrinitiative_master WHERE targetunkid <= 0 AND hrinitiative_master.isactive = 1 " & _
            '    '            ")AS B ON hrobjective_master.objectiveunkid = B.objectiveunkid " & _
            '    '            "AND A.targetunkid > 0 OR B.initiativeunkid > 0 WHERE hrobjective_master.isactive = 1 "

            '    'S.SANDEEP [ 03 AUG 2013 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    If sObjectives.Trim.Length > 0 Then
            '        StrQ &= "UNION " & _
            '            "SELECT " & _
            '            "    hrobjective_master.perspectiveunkid " & _
            '            "   ,hrobjective_master.objectiveunkid AS objectiveunkid " & _
            '            "   ,hrobjective_master.name AS objective " & _
            '            "   ,'' AS kpi " & _
            '            "   ,-1 AS kpiunkid " & _
            '            "   ,ISNULL(A.name,'') AS targets " & _
            '            "   ,ISNULL(A.targetunkid,0) AS targetunkid " & _
            '            "   ,ISNULL(B.name,'') AS initiative " & _
            '            "   ,ISNULL(B.initiativeunkid,0) AS initiativeunkid " & _
            '        "   ,'' AS kweight " & _
            '        "   ,ISNULL(CAST(A.tweight AS NVARCHAR(MAX)),'') AS tweight " & _
            '        "   ,ISNULL(CAST(B.iweight AS NVARCHAR(MAX)),'') AS iweight " & _
            '            "FROM hrobjective_master " & _
            '               "JOIN " & _
            '            "( " & _
            '            "   SELECT " & _
            '            "        targetunkid " & _
            '            "       ,name " & _
            '            "       ,objectiveunkid " & _
            '            "       ,kpiunkid " & _
            '        "       ,weight AS tweight " & _
            '            "FROM hrtarget_master " & _
            '               "WHERE kpiunkid <= 0 AND hrtarget_master.isactive = 1 AND objectiveunkid IN (" & sObjectives & ") " & _
            '            ") AS A ON hrobjective_master.objectiveunkid = A.objectiveunkid " & _
            '               "JOIN " & _
            '            "( " & _
            '            "   SELECT " & _
            '            "        initiativeunkid " & _
            '            "       ,name " & _
            '            "       ,objectiveunkid " & _
            '            "       ,targetunkid " & _
            '        "       ,weight AS iweight " & _
            '           "FROM hrinitiative_master WHERE targetunkid <= 0 AND hrinitiative_master.isactive = 1 AND objectiveunkid IN (" & sObjectives & ")  " & _
            '            ")AS B ON hrobjective_master.objectiveunkid = B.objectiveunkid " & _
            '           "AND A.targetunkid > 0 OR B.initiativeunkid > 0 WHERE hrobjective_master.isactive = 1 "
            '    Else
            'StrQ &= "UNION " & _
            '            "SELECT " & _
            '            "    hrobjective_master.perspectiveunkid " & _
            '            "   ,hrobjective_master.objectiveunkid AS objectiveunkid " & _
            '            "   ,hrobjective_master.name AS objective " & _
            '            "   ,'' AS kpi " & _
            '            "   ,-1 AS kpiunkid " & _
            '            "   ,ISNULL(A.name,'') AS targets " & _
            '            "   ,ISNULL(A.targetunkid,0) AS targetunkid " & _
            '            "   ,ISNULL(B.name,'') AS initiative " & _
            '            "   ,ISNULL(B.initiativeunkid,0) AS initiativeunkid " & _
            '            "   ,'' AS kweight " & _
            '            "   ,ISNULL(CAST(A.tweight AS NVARCHAR(MAX)),'') AS tweight " & _
            '            "   ,ISNULL(CAST(B.iweight AS NVARCHAR(MAX)),'') AS iweight " & _
            '            "FROM hrobjective_master " & _
            '                    "JOIN " & _
            '            "( " & _
            '            "   SELECT " & _
            '            "        targetunkid " & _
            '            "       ,name " & _
            '            "       ,objectiveunkid " & _
            '            "       ,kpiunkid " & _
            '            "       ,weight AS tweight " & _
            '            "FROM hrtarget_master " & _
            '            "WHERE kpiunkid <= 0 AND hrtarget_master.isactive = 1 " & _
            '            ") AS A ON hrobjective_master.objectiveunkid = A.objectiveunkid " & _
            '                    "JOIN " & _
            '            "( " & _
            '            "   SELECT " & _
            '            "        initiativeunkid " & _
            '            "       ,name " & _
            '            "       ,objectiveunkid " & _
            '            "       ,targetunkid " & _
            '            "       ,weight AS iweight " & _
            '            "FROM hrinitiative_master WHERE targetunkid <= 0 AND hrinitiative_master.isactive = 1 " & _
            '            ")AS B ON hrobjective_master.objectiveunkid = B.objectiveunkid " & _
            '            "AND A.targetunkid > 0 OR B.initiativeunkid > 0 WHERE hrobjective_master.isactive = 1 "
            '    End If
            '    'S.SANDEEP [ 03 AUG 2013 ] -- END



            '    'S.SANDEEP [ 02 AUG 2013 ] -- END


            '    If iEmpId > 0 Then
            '        StrQ &= " AND hrobjective_master.employeeunkid = '" & iEmpId & "' "
            '    End If
            '    If iPeriodId > 0 Then
            '        StrQ &= " AND hrobjective_master.periodunkid = '" & iPeriodId & "' "
            '    End If
            '    'S.SANDEEP [ 25 JULY 2013 ] -- END

            '    'S.SANDEEP [ 21 MAR 2013 ] -- END


            'S.SANDEEP [ 17 JUL 2014 ] -- START
            Dim objSett As New clsWeight_Setting(iPeriodId)
            Dim iColName As String = ""
            Select Case objSett._Weight_Typeid
                Case enWeight_Types.WEIGHT_FIELD1
                    iColName = "objectiveunkid"
                Case enWeight_Types.WEIGHT_FIELD2
                    iColName = "kpiunkid"
                Case enWeight_Types.WEIGHT_FIELD3
                    iColName = "targetunkid"
                Case enWeight_Types.WEIGHT_FIELD5
                    iColName = "initiativeunkid"
            End Select
            '", ROW_NUMBER() OVER(PARTITION BY targetunkid ORDER BY targetunkid) AS iRno " & _ =============== ADDED
            '=================================================================== ADDED
            '"SELECT " & _
            '"      perspectiveunkid " & _
            '",     objectiveunkid " & _
            '",     kpiunkid " & _
            '",     targetunkid " & _
            '",     initiativeunkid " & _
            '",     objective " & _
            '",     kpi " & _
            '",     target " & _
            '",     initiative " & _
            '",     CASE WHEN iRno <> 1 THEN '' ELSE oweight END AS oweight " & _
            '",     CASE WHEN iRno <> 1 THEN '' ELSE kweight END AS kweight " & _
            '",     CASE WHEN iRno <> 1 THEN '' ELSE tweight END AS tweight " & _
            '",     CASE WHEN iRno <> 1 THEN '' ELSE iweight END AS iweight " & _
            '"FROM " & _
            '"( " & _
            '=================================================================== ADDED
            'S.SANDEEP [ 17 JUL 2014 ] -- END
            StrQ = "SELECT " & _
                   "      perspectiveunkid " & _
                   ",     objectiveunkid " & _
                   ",     kpiunkid " & _
                   ",     targetunkid " & _
                   ",     initiativeunkid " & _
                   ",     objective " & _
                   ",     kpi " & _
                   ",     target " & _
                   ",     initiative " & _
                   ",     CASE WHEN iRno <> 1 THEN '' ELSE oweight END AS oweight " & _
                   ",     CASE WHEN iRno <> 1 THEN '' ELSE kweight END AS kweight " & _
                   ",     CASE WHEN iRno <> 1 THEN '' ELSE tweight END AS tweight " & _
                   ",     CASE WHEN iRno <> 1 THEN '' ELSE iweight END AS iweight " & _
                   "FROM " & _
                   "( " & _
                   "SELECT perspectiveunkid " & _
                       "       ,objectiveunkid " & _
                       "       ,kpiunkid " & _
                   ", targetunkid " & _
                   ", initiativeunkid " & _
                   ", objective " & _
                   ", kpi " & _
                   ", target " & _
                   ", initiative " & _
                   ", CASE WHEN oweight <=0 THEN '' ELSE CAST(oweight AS NVARCHAR(MAX)) END oweight " & _
                   ", CASE WHEN kweight <=0 THEN '' ELSE CAST(kweight AS NVARCHAR(MAX)) END kweight " & _
                   ", CASE WHEN tweight <=0 THEN '' ELSE CAST(tweight AS NVARCHAR(MAX)) END tweight " & _
                   ", CASE WHEN iweight <=0 THEN '' ELSE CAST(iweight AS NVARCHAR(MAX)) END iweight " & _
                   ", ROW_NUMBER() OVER(PARTITION BY " & iColName & " ORDER BY " & iColName & ") AS iRno " & _
                   "FROM " & _
                       "( " & _
                       "   SELECT " & _
                   "    perspectiveunkid " & _
                   "   ,hrobjective_master.objectiveunkid " & _
                   "   ,ISNULL(hrkpi_master.kpiunkid,0) AS kpiunkid " & _
                   "   ,ISNULL(hrtarget_master.targetunkid,0) AS targetunkid " & _
                   "   ,ISNULL(hrinitiative_master.initiativeunkid,0) AS initiativeunkid " & _
                   "   ,ISNULL(hrobjective_master.name,'') AS objective " & _
                   "   ,ISNULL(hrkpi_master.name,'') AS kpi " & _
                   "   ,ISNULL(hrtarget_master.name,'') AS target " & _
                   "   ,ISNULL(hrinitiative_master.name,'') AS initiative " & _
                   "   ,ISNULL(hrobjective_master.weight,0) AS oweight " & _
                   "   ,ISNULL(hrkpi_master.weight,0) AS kweight " & _
                   "   ,ISNULL(hrtarget_master.weight,0) AS tweight " & _
                   "   ,ISNULL(hrinitiative_master.weight,0) AS iweight " & _
                   "FROM hrobjective_master " & _
                   "	LEFT JOIN hrkpi_master ON hrobjective_master.objectiveunkid = hrkpi_master.objectiveunkid AND hrkpi_master.isactive = 1 " & _
                   "	LEFT JOIN hrtarget_master ON hrkpi_master.kpiunkid = hrtarget_master.kpiunkid AND hrtarget_master.kpiunkid > 0 AND hrtarget_master.isactive = 1 " & _
                   "	LEFT JOIN hrinitiative_master ON hrtarget_master.targetunkid = hrinitiative_master.targetunkid AND hrinitiative_master.targetunkid > 0 AND hrinitiative_master.isactive = 1 " & _
                   "WHERE hrobjective_master.isactive = 1 AND hrobjective_master.employeeunkid = '" & iEmpId & "' AND hrobjective_master.periodunkid = '" & iPeriodId & "' " & _
                   "UNION " & _
                        "SELECT " & _
                   "    perspectiveunkid " & _
                   "   ,hrobjective_master.objectiveunkid " & _
                   "   ,0 AS kpiunkid " & _
                   "   ,ISNULL(hrtarget_master.targetunkid,'') AS targetunkid " & _
                   "    ,0 AS initiativeunkid " & _
                   "    ,hrobjective_master.name AS objective " & _
                   "    ,'' AS kpi " & _
                   "    ,ISNULL(hrtarget_master.name,'') AS target " & _
                   "    ,'' AS initiative " & _
                   "    ,ISNULL(hrobjective_master.weight,0) AS oweight " & _
                   "    ,0 AS kweight " & _
                   "    ,ISNULL(hrtarget_master.weight,0) AS tweight " & _
                   "    ,0 AS iweight " & _
                   "FROM hrobjective_master " & _
                   "    JOIN hrtarget_master ON hrobjective_master.objectiveunkid = hrtarget_master.objectiveunkid AND kpiunkid <= 0 AND hrtarget_master.isactive = 1 " & _
                   "WHERE hrobjective_master.isactive = 1 AND hrobjective_master.employeeunkid = '" & iEmpId & "' AND hrobjective_master.periodunkid = '" & iPeriodId & "' " & _
                   "UNION " & _
                   "SELECT " & _
                   "    perspectiveunkid " & _
                   "    ,hrobjective_master.objectiveunkid " & _
                   "    ,0 AS kpiunkid " & _
                   "    ,0 AS targetunkid " & _
                   "   ,ISNULL(hrinitiative_master.initiativeunkid,0) AS initiativeunkid " & _
                        "   ,hrobjective_master.name AS objective " & _
                        "   ,'' AS kpi " & _
                   "    ,'' AS target " & _
                   "   ,ISNULL(hrinitiative_master.name,'') AS initiative " & _
                   "   ,ISNULL(hrobjective_master.weight,0) AS oweight " & _
                   "   ,0 AS kweight " & _
                   "    ,0 AS tweight " & _
                   "   ,ISNULL(hrinitiative_master.weight,0) AS iweight " & _
                        "FROM hrobjective_master " & _
                   "    JOIN hrinitiative_master ON hrobjective_master.objectiveunkid = hrinitiative_master.objectiveunkid AND hrinitiative_master.targetunkid <= 0 AND hrinitiative_master.isactive = 1 " & _
                   "WHERE hrobjective_master.isactive = 1 AND hrobjective_master.employeeunkid = '" & iEmpId & "' AND hrobjective_master.periodunkid = '" & iPeriodId & "' " & _
                   ") AS Final_Data ) AS A WHERE 1 = 1 "

            'S.SANDEEP [ 24 APR 2014 ] -- END
            

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtTable = dsList.Tables(0)

            Return dtTable

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetFullBSC; Module Name: " & mstrModuleName)
        End Try
    End Function
    'S.SANDEEP [ 28 DEC 2012 ] -- END


    Public Function GetBSCPlanning(ByVal intObjectiveId As Integer) As DataTable
        Dim dtTable As DataTable
        Try
            dtTable = New DataTable("Data")

            dtTable.Columns.Add("KPI", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("Target", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("Initiative", System.Type.GetType("System.String")).DefaultValue = ""
            dtTable.Columns.Add("KPIUnkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("TargetUnkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            dtTable.Columns.Add("InitiativeUnkid", System.Type.GetType("System.Int32")).DefaultValue = -1

            Dim ObjKPI As New clsKPI_Master
            Dim dsKPI As New DataSet
            dsKPI = ObjKPI.getComboList("List", False, intObjectiveId)
            ObjKPI = Nothing

            Dim ObjTarget As New clstarget_master
            Dim dsTgt As New DataSet
            dsTgt = ObjTarget.getComboList("List", False, intObjectiveId)
            ObjTarget = Nothing

            Dim objInitiative As New clsinitiative_master
            Dim dsINI As New DataSet
            dsINI = objInitiative.getComboList("List", False, intObjectiveId)
            objInitiative = Nothing


            Dim dTemp As DataRow = Nothing

            If dsKPI.Tables(0).Rows.Count >= dsTgt.Tables(0).Rows.Count AndAlso dsKPI.Tables(0).Rows.Count >= dsINI.Tables(0).Rows.Count Then
                For Each dKRow As DataRow In dsKPI.Tables(0).Rows
                    dTemp = dtTable.NewRow

                    dTemp.Item("KPI") = dKRow.Item("name")
                    dTemp.Item("KPIUnkid") = dKRow.Item("id")

                    If dsKPI.Tables(0).Rows.IndexOf(dKRow) <= dsTgt.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("Target") = dsTgt.Tables(0).Rows(dsKPI.Tables(0).Rows.IndexOf(dKRow))("name")
                        dTemp.Item("TargetUnkid") = dsTgt.Tables(0).Rows(dsKPI.Tables(0).Rows.IndexOf(dKRow))("id")
                    Else
                        dTemp.Item("Target") = ""
                End If

                    If dsKPI.Tables(0).Rows.IndexOf(dKRow) <= dsINI.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("Initiative") = dsINI.Tables(0).Rows(dsKPI.Tables(0).Rows.IndexOf(dKRow))("name")
                        dTemp.Item("InitiativeUnkid") = dsINI.Tables(0).Rows(dsKPI.Tables(0).Rows.IndexOf(dKRow))("id")
                Else
                        dTemp.Item("Initiative") = ""
                End If
                    dtTable.Rows.Add(dTemp)
                Next
            ElseIf dsTgt.Tables(0).Rows.Count >= dsKPI.Tables(0).Rows.Count AndAlso dsTgt.Tables(0).Rows.Count >= dsINI.Tables(0).Rows.Count Then
                For Each dTRow As DataRow In dsTgt.Tables(0).Rows
                    dTemp = dtTable.NewRow

                    dTemp.Item("Target") = dTRow.Item("name")
                    dTemp.Item("TargetUnkid") = dTRow.Item("id")

                    If dsTgt.Tables(0).Rows.IndexOf(dTRow) <= dsKPI.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("KPI") = dsKPI.Tables(0).Rows(dsTgt.Tables(0).Rows.IndexOf(dTRow))("name")
                        dTemp.Item("KPIUnkid") = dsKPI.Tables(0).Rows(dsTgt.Tables(0).Rows.IndexOf(dTRow))("id")
                    Else
                        dTemp.Item("KPI") = ""
                End If

                    If dsTgt.Tables(0).Rows.IndexOf(dTRow) <= dsINI.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("Initiative") = dsINI.Tables(0).Rows(dsTgt.Tables(0).Rows.IndexOf(dTRow))("name")
                        dTemp.Item("InitiativeUnkid") = dsINI.Tables(0).Rows(dsTgt.Tables(0).Rows.IndexOf(dTRow))("id")
                    Else
                        dTemp.Item("Initiative") = ""
                End If
                    dtTable.Rows.Add(dTemp)
                Next
            Else
                For Each dIRow As DataRow In dsINI.Tables(0).Rows
                    dTemp = dtTable.NewRow

                    dTemp.Item("Initiative") = dIRow.Item("name")
                    dTemp.Item("InitiativeUnkid") = dIRow.Item("id")

                    If dsINI.Tables(0).Rows.IndexOf(dIRow) <= dsKPI.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("KPI") = dsKPI.Tables(0).Rows(dsINI.Tables(0).Rows.IndexOf(dIRow))("name")
                        dTemp.Item("KPIUnkid") = dsKPI.Tables(0).Rows(dsINI.Tables(0).Rows.IndexOf(dIRow))("id")
                    Else
                        dTemp.Item("KPI") = ""
                    End If

                    If dsINI.Tables(0).Rows.IndexOf(dIRow) <= dsTgt.Tables(0).Rows.Count - 1 Then
                        dTemp.Item("Target") = dsTgt.Tables(0).Rows(dsINI.Tables(0).Rows.IndexOf(dIRow))("name")
                        dTemp.Item("TargetUnkid") = dsTgt.Tables(0).Rows(dsINI.Tables(0).Rows.IndexOf(dIRow))("id")
                    Else
                        dTemp.Item("Target") = ""
                    End If
                    dtTable.Rows.Add(dTemp)
                Next
            End If


            Return dtTable
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetBSCPlanning; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function


    'S.SANDEEP [ 24 APR 2014 ] -- START
    ''S.SANDEEP [ 04 FEB 2012 ] -- START
    ''ENHANCEMENT : TRA CHANGES
    'Public Function Get_Percentage(ByVal menAssessMode As enAssessmentMode) As DataSet
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    Dim objDataOperation As New clsDataOperation
    '    Dim dTable As DataTable
    '    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    '    'ENHANCEMENT : TRA CHANGES
    '    'Dim mDicIsAdded As New Dictionary(Of Integer, Integer)
    '    Dim mDicIsAdded As New Dictionary(Of String, Integer)
    '    'S.SANDEEP [ 05 MARCH 2012 ] -- END

    '    Dim objWSetting As New clsWeight_Setting(True)

    '    Try

    '        dTable = New DataTable("Total")
    '        dTable.Columns.Add("EmpId", System.Type.GetType("System.Int32"))
    '        dTable.Columns.Add("Pid", System.Type.GetType("System.Int32"))
    '        dTable.Columns.Add("TotalPercent", System.Type.GetType("System.Double"))
    '        'S.SANDEEP [ 08 APR 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        dTable.Columns.Add("analysisunkid", System.Type.GetType("System.Int32"))
    '        'S.SANDEEP [ 08 APR 2013 ] -- END


    '        'S.SANDEEP [ 10 SEPT 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'StrQ = "SELECT "
    '        StrQ = "SELECT DISTINCT "
    '        'S.SANDEEP [ 10 SEPT 2013 ] -- END

    '        'S.SANDEEP [ 08 APR 2013 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        StrQ &= " hrbsc_analysis_master.analysisunkid " & _
    '                ",hrbsc_analysis_tran.objectiveunkid " 'S.SANDEEP [ 09 OCT 2013 ] -- START -- END
    '        'S.SANDEEP [ 08 APR 2013 ] -- END
    '        Select Case menAssessMode
    '            Case enAssessmentMode.SELF_ASSESSMENT
    '                StrQ &= ", hrbsc_analysis_master.selfemployeeunkid AS EmpId "
    '            Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                StrQ &= ", hrbsc_analysis_master.assessedemployeeunkid AS EmpId ,CASE WHEN assessoremployeeunkid >0 THEN 'I_'+CAST(assessoremployeeunkid AS NVARCHAR(10)) ELSE 'E_'+CAST(ext_assessorunkid as NVARCHAR(10)) END AS AEmpId "
    '            Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                StrQ &= ", hrbsc_analysis_master.assessedemployeeunkid AS EmpId "
    '        End Select
    '        'S.SANDEEP [ 28 DEC 2012 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        'StrQ &= " ,periodunkid AS Pid " & _
    '        '                        " ,ISNULL(hrresult_master.resultname,'') AS resultname " & _
    '        '                        "FROM hrbsc_analysis_master " & _
    '        '                        " JOIN hrbsc_analysis_tran ON dbo.hrbsc_analysis_master.analysisunkid = hrbsc_analysis_tran.analysisunkid " & _
    '        '                        " JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '        '                        "WHERE kpiunkid  <= 0 AND initiativeunkid  <= 0 AND targetunkid <= 0 AND hrbsc_analysis_master.isvoid = 0 AND hrbsc_analysis_tran.isvoid = 0 AND assessmodeid = '" & menAssessMode & "' "

    '        StrQ &= " ,periodunkid AS Pid " & _
    '                " ,ISNULL(hrresult_master.resultname,'') AS resultname " & _
    '                "FROM hrbsc_analysis_master " & _
    '                " JOIN hrbsc_analysis_tran ON dbo.hrbsc_analysis_master.analysisunkid = hrbsc_analysis_tran.analysisunkid " & _
    '                " JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
    '                    "WHERE hrbsc_analysis_master.isvoid = 0 AND hrbsc_analysis_tran.isvoid = 0 AND assessmodeid = '" & menAssessMode & "' "


    '        If objWSetting._Weight_Typeid <= 0 Then
    '            StrQ &= " AND (kpiunkid  > 0 AND initiativeunkid  > 0 AND targetunkid > 0) "
    '        ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD1 Then
    '            StrQ &= " AND (kpiunkid  <= 0 AND initiativeunkid  <= 0 AND targetunkid <= 0) "
    '        ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD2 Then
    '            StrQ &= " AND kpiunkid  > 0  "
    '        ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD3 Then
    '            StrQ &= " AND targetunkid > 0 "
    '        ElseIf objWSetting._Weight_Typeid = enWeight_Types.WEIGHT_FIELD5 Then
    '            StrQ &= " AND initiativeunkid  > 0 "
    '        End If
    '        'S.SANDEEP [ 28 DEC 2012 ] -- END

    '        dsList = objDataOperation.ExecQuery(StrQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows

    '            Select Case menAssessMode
    '                Case enAssessmentMode.SELF_ASSESSMENT
    '                    If mDicIsAdded.ContainsKey(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid")) Then Continue For
    '                    mDicIsAdded.Add(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid"), dtRow.Item("EmpId"))
    '                Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                    If mDicIsAdded.ContainsKey(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid") & "|" & dtRow.Item("AEmpId")) Then Continue For
    '                    mDicIsAdded.Add(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid") & "|" & dtRow.Item("AEmpId"), dtRow.Item("EmpId"))
    '                Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                    If mDicIsAdded.ContainsKey(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid")) Then Continue For
    '                    mDicIsAdded.Add(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid"), dtRow.Item("EmpId"))
    '            End Select

    '            Dim dRow As DataRow = dTable.NewRow
    '            dRow.Item("EmpId") = dtRow.Item("EmpId")
    '            dRow.Item("Pid") = dtRow.Item("Pid")
    '            'S.SANDEEP [ 08 APR 2013 ] -- START
    '            'ENHANCEMENT : TRA CHANGES
    '            dRow.Item("analysisunkid") = dtRow.Item("analysisunkid")
    '            'S.SANDEEP [ 08 APR 2013 ] -- END


    '            Dim dMRow() As DataRow = Nothing

    '            Select Case menAssessMode
    '                Case enAssessmentMode.SELF_ASSESSMENT
    '                    dMRow = dsList.Tables(0).Select("EmpId = '" & dtRow.Item("EmpId") & "' AND  Pid = '" & dtRow.Item("Pid") & "'")
    '                Case enAssessmentMode.APPRAISER_ASSESSMENT
    '                    dMRow = dsList.Tables(0).Select("EmpId = '" & dtRow.Item("EmpId") & "' AND  Pid = '" & dtRow.Item("Pid") & "' AND AEmpId = '" & dtRow.Item("AEmpId") & "'")
    '                Case enAssessmentMode.REVIEWER_ASSESSMENT
    '                    dMRow = dsList.Tables(0).Select("EmpId = '" & dtRow.Item("EmpId") & "' AND  Pid = '" & dtRow.Item("Pid") & "'")
    '            End Select
    '            If dMRow.Length > 0 Then
    '                Dim dblTotal As Double = 0
    '                For i As Integer = 0 To dMRow.Length - 1
    '                    If IsNumeric(dMRow(i)("resultname")) = True Then
    '                        dblTotal = dblTotal + CDbl(dMRow(i)("resultname"))
    '                    End If
    '                Next
    '                dRow.Item("TotalPercent") = dblTotal
    '            End If

    '            dTable.Rows.Add(dRow)
    '        Next

    '        dsList.Tables.RemoveAt(0)

    '        dsList.Tables.Add(dTable)

    '        Return dsList

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Get_Percentage; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function
    ''S.SANDEEP [ 04 FEB 2012 ] -- END
    Public Function Get_Percentage(ByVal menAssessMode As enAssessmentMode) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim dTable As DataTable
        Dim mDicIsAdded As New Dictionary(Of String, Integer)
        Try
            dTable = New DataTable("Total")
            dTable.Columns.Add("EmpId", System.Type.GetType("System.Int32"))
            dTable.Columns.Add("Pid", System.Type.GetType("System.Int32"))
            dTable.Columns.Add("TotalPercent", System.Type.GetType("System.Double"))
            dTable.Columns.Add("analysisunkid", System.Type.GetType("System.Int32"))
            StrQ = "SELECT " & _
                   " hrbsc_analysis_master.analysisunkid " & _
                   ",hrbsc_analysis_tran.objectiveunkid "
            Select Case menAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    StrQ &= ", hrbsc_analysis_master.selfemployeeunkid AS EmpId "
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    StrQ &= ", hrbsc_analysis_master.assessedemployeeunkid AS EmpId ,CASE WHEN assessoremployeeunkid >0 THEN 'I_'+CAST(assessoremployeeunkid AS NVARCHAR(10)) ELSE 'E_'+CAST(ext_assessorunkid as NVARCHAR(10)) END AS AEmpId "
                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    StrQ &= ", hrbsc_analysis_master.assessedemployeeunkid AS EmpId "
            End Select
            StrQ &= " ,periodunkid AS Pid " & _
                    " ,ISNULL(hrresult_master.resultname,'') AS resultname " & _
                    "FROM hrbsc_analysis_master " & _
                    " JOIN hrbsc_analysis_tran ON dbo.hrbsc_analysis_master.analysisunkid = hrbsc_analysis_tran.analysisunkid " & _
                    " JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                        "WHERE hrbsc_analysis_master.isvoid = 0 AND hrbsc_analysis_tran.isvoid = 0 AND assessmodeid = '" & menAssessMode & "' "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows

                Select Case menAssessMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        If mDicIsAdded.ContainsKey(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid")) Then Continue For
                        mDicIsAdded.Add(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid"), dtRow.Item("EmpId"))
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        If mDicIsAdded.ContainsKey(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid") & "|" & dtRow.Item("AEmpId")) Then Continue For
                        mDicIsAdded.Add(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid") & "|" & dtRow.Item("AEmpId"), dtRow.Item("EmpId"))
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                If mDicIsAdded.ContainsKey(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid")) Then Continue For
                mDicIsAdded.Add(dtRow.Item("EmpId") & "|" & dtRow.Item("Pid"), dtRow.Item("EmpId"))
                End Select

                Dim dRow As DataRow = dTable.NewRow
                dRow.Item("EmpId") = dtRow.Item("EmpId")
                dRow.Item("Pid") = dtRow.Item("Pid")
                dRow.Item("analysisunkid") = dtRow.Item("analysisunkid")
                Dim dMRow() As DataRow = Nothing

                Select Case menAssessMode
                    Case enAssessmentMode.SELF_ASSESSMENT
                        dMRow = dsList.Tables(0).Select("EmpId = '" & dtRow.Item("EmpId") & "' AND  Pid = '" & dtRow.Item("Pid") & "'")
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        dMRow = dsList.Tables(0).Select("EmpId = '" & dtRow.Item("EmpId") & "' AND  Pid = '" & dtRow.Item("Pid") & "' AND AEmpId = '" & dtRow.Item("AEmpId") & "'")
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        dMRow = dsList.Tables(0).Select("EmpId = '" & dtRow.Item("EmpId") & "' AND  Pid = '" & dtRow.Item("Pid") & "'")
                End Select
                If dMRow.Length > 0 Then
                    Dim dblTotal As Double = 0
                    For i As Integer = 0 To dMRow.Length - 1
                        If IsNumeric(dMRow(i)("resultname")) = True Then
                            dblTotal = dblTotal + CDbl(dMRow(i)("resultname"))
                        End If
                    Next
                    dRow.Item("TotalPercent") = dblTotal
                End If

                dTable.Rows.Add(dRow)
            Next

            dsList.Tables.RemoveAt(0)
            dsList.Tables.Add(dTable)
            Return dsList
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Percentage", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP [ 24 APR 2014 ] -- END




    'S.SANDEEP [ 05 MARCH 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Unlock_Commit(ByVal intPeriodUnkid As Integer, ByVal iEmployeeId As Integer) As Boolean 'S.SANDEEP [ 10 SEPT 2013 ] -- START -- END
        'Public Function Unlock_Commit(ByVal intPeriodUnkid As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim iCnt As Integer = -1
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Dim blnFlag As Boolean = True
        Try
            'S.SANDEEP [ 10 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ &= "SELECT * FROM hrapps_shortlist_master WHERE periodunkid = '" & intPeriodUnkid & "' AND isvoid = 0"
            StrQ = "SELECT 1 FROM hrapps_finalemployee " & _
                   "	JOIN hrapps_shortlist_master ON hrapps_finalemployee.shortlistunkid = hrapps_shortlist_master.shortlistunkid " & _
                   "WHERE hrapps_finalemployee.isvoid = 0 AND employeeunkid = '" & iEmployeeId & "' AND apprperiodunkid <= 0 AND (ISNULL(operationmodeid,0) > 0 OR isfinalshortlisted = 1) " & _
                   "AND hrapps_shortlist_master.periodunkid = '" & intPeriodUnkid & "' "
            'S.SANDEEP [ 10 SEPT 2013 ] -- END

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                blnFlag = False
            End If

            'S.SANDEEP [ 10 SEPT 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            iCnt = -1
            StrQ = "SELECT 1 " & _
                   "FROM hrapps_finalemployee " & _
                   "WHERE isvoid = 0 AND employeeunkid = '" & iEmployeeId & "' AND apprperiodunkid > 0 AND (ISNULL(operationmodeid,0) > 0 OR isfinalshortlisted = 1) " & _
                   "AND apprperiodunkid = '" & intPeriodUnkid & "' "

            iCnt = objDataOperation.RecordCount(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If iCnt > 0 Then
                blnFlag = False
            End If
            'S.SANDEEP [ 10 SEPT 2013 ] -- END

            Return blnFlag

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Unlock_Commit; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 05 MARCH 2012 ] -- END

'Pinkal (12-Jun-2012) -- Start
    'Enhancement : TRA Changes
    Public Function IsGE_Pending(ByVal intEmployeeId As Integer, ByVal eAssMode As enAssessmentMode) As Boolean
        Dim mblnFlag As Boolean = False
        Dim StrQ As String = ""
        Try
            objDataOperation = New clsDataOperation

            If eAssMode = enAssessmentMode.SELF_ASSESSMENT Then
                StrQ = "SELECT * FROM hrassess_analysis_master WHERE selfemployeeunkid = '" & intEmployeeId & "' AND isvoid = 0 AND iscommitted = 0 "
            Else
                StrQ = "SELECT * FROM hrassess_analysis_master WHERE assessedemployeeunkid = '" & intEmployeeId & "' AND isvoid = 0 AND iscommitted = 0 "
            End If

            StrQ &= " AND assessmodeid = " & eAssMode

            Dim intCount As Integer = objDataOperation.RecordCount(StrQ)

            If intCount > 0 Then
                mblnFlag = True
            Else
                mblnFlag = False
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: IsGE_Pending; Module Name: " & mstrModuleName)
        End Try
        Return mblnFlag
    End Function
    'Pinkal (12-Jun-2012) -- End


    'S.SANDEEP [ 13 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Sub Email_Notification(ByVal eAssessMode As enAssessmentMode, _
                                  ByVal iEmployeeId As Integer, _
                                  ByVal iPeriodId As Integer, _
                                  ByVal iYearId As Integer, _
                                  ByVal isReviewerMandatory As Boolean, _
                                  ByVal strDatabaseName As String, _
                                  Optional ByVal iCompanyId As Integer = 0, _
                                  Optional ByVal sArutiSSURL As String = "", _
                                  Optional ByVal sName As String = "", _
                                  Optional ByVal iLoginTypeId As Integer = 0, _
                                  Optional ByVal iLoginEmployeeId As Integer = 0) 'S.SANDEEP [ 28 JAN 2014 ] -- START {iLoginTypeId,iLoginEmployeeId} -- END
        'Sohail (21 Aug 2015) - [strDatabaseName]

        Dim dsList As DataSet = Nothing
        Dim strLink As String = String.Empty
        Dim StrQ As String = String.Empty
        Dim sYearName As String = String.Empty

        objDataOperation = New clsDataOperation

        Try
            If iCompanyId <= 0 Then iCompanyId = Company._Object._Companyunkid
            If sArutiSSURL = "" Then sArutiSSURL = ConfigParameter._Object._ArutiSelfServiceURL
            Dim objNet As New clsNetConnectivity : If objNet._Conected = False Then Exit Sub

            Select Case eAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    StrQ = "SELECT " & _
                           "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
                           " ,ISNULL(email,'') AS Email " & _
                           " ,hrassessor_master.assessormasterunkid " & _
                           " ,hrapprover_usermapping.userunkid " & _
                           "FROM hrapprover_usermapping " & _
                           " JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                           " JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                           " JOIN hrassessor_tran ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                           "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrassessor_tran.employeeunkid = '" & iEmployeeId & "' AND isreviewer = 0 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "

                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    StrQ = "SELECT " & _
                           "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
                           " ,ISNULL(email,'') AS Email " & _
                           " ,hrassessor_master.assessormasterunkid " & _
                           " ,hrapprover_usermapping.userunkid " & _
                           "FROM hrapprover_usermapping " & _
                           " JOIN hrassessor_master ON hrapprover_usermapping.approverunkid = hrassessor_master.assessormasterunkid " & _
                           " JOIN hremployee_master ON hrassessor_master.employeeunkid = hremployee_master.employeeunkid " & _
                           " JOIN hrassessor_tran ON hrassessor_tran.assessormasterunkid = hrassessor_master.assessormasterunkid " & _
                           "WHERE usertypeid = '" & enUserType.Assessor & "' AND hrassessor_tran.employeeunkid = '" & iEmployeeId & "' AND isreviewer = 1 AND hrassessor_master.isvoid = 0 AND hrassessor_tran.isvoid = 0 "

                Case enAssessmentMode.REVIEWER_ASSESSMENT
                    StrQ = "SELECT " & _
                           "  ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.surname,'') AS Ename " & _
                           " ,ISNULL(email,'') AS Email " & _
                           " ,employeeunkid AS userunkid " & _
                           "FROM hremployee_master WHERE hremployee_master.employeeunkid = '" & iEmployeeId & "' "
            End Select

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                mstrMessage = objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage
            End If

            Dim objEmp As New clsEmployee_Master
            Dim objMail As New clsSendMail
            Dim objPeriod As New clscommom_period_Tran

            'Sohail (21 Aug 2015) -- Start
            'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
            'objEmp._Employeeunkid = iEmployeeId : objPeriod._Periodunkid = iPeriodId

            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objEmp._Employeeunkid = iEmployeeId : objPeriod._Periodunkid(strDatabaseName) = iPeriodId
            objPeriod._Periodunkid(strDatabaseName) = iPeriodId
            objEmp._Employeeunkid(objPeriod._End_Date) = iEmployeeId
            'S.SANDEEP [04 JUN 2015] -- END

            'Sohail (21 Aug 2015) -- End

            Dim dsYear As New DataSet
            Dim objFY As New clsCompany_Master
            dsYear = objFY.GetFinancialYearList(iCompanyId, , "List", objPeriod._Yearunkid)
            If dsYear.Tables("List").Rows.Count > 0 Then
                sYearName = dsYear.Tables("List").Rows(0).Item("financialyear_name")
            End If
            objFY = Nothing

            Select Case eAssessMode
                Case enAssessmentMode.SELF_ASSESSMENT
                    For Each dtRow As DataRow In dsList.Tables("List").Rows
                        If dtRow.Item("Email") = "" Then Continue For
                        Dim strMessage As String = ""
                        objMail._Subject = Language.getMessage(mstrModuleName, 4, "Notifications for BSC.")
                        strMessage = "<HTML> <BODY>"
                        strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " <B>" & dtRow.Item("Ename").ToString() & "</B>, <BR><BR>"
                        strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 6, "This is to inform you that, I have completed my Balanced Score Card Self-Assessment for the period of  ")
                        strMessage &= "<B>" & objPeriod._Period_Name & "</B> and Year <B>" & sYearName & "</B>" & Language.getMessage(mstrModuleName, 7, ". Please click the link below to assess me as my supervisor.")
                        strMessage &= "<BR><BR>"
                        strMessage &= "<B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>"
                        strMessage &= "<BR>"
                        'S.SANDEEP [ 24 APR 2014 ] -- START
                        'strLink = sArutiSSURL & "/Assessment/BSC/wPgAssessor_BSC_AddEdit.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iYearId.ToString & "|" & sYearName.ToString & "|" & iPeriodId.ToString))
                        strLink = sArutiSSURL & "/Assessment/BSC/wPgTabularAssessor_BSC_AddEdit.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iYearId.ToString & "|" & sYearName.ToString & "|" & iPeriodId.ToString))
                        'S.SANDEEP [ 24 APR 2014 ] -- END
                        strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
                        strMessage &= "<BR><BR>"
                        strMessage &= "<B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>"
                        strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        strMessage &= "</BODY></HTML>"
                        objMail._Message = strMessage
                        objMail._ToEmail = dtRow.Item("Email")
                        'S.SANDEEP [ 28 JAN 2014 ] -- START
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrWebFrmName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrWebFrmName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        'S.SANDEEP [ 28 JAN 2014 ] -- END
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objMail.SendMail()
                        objMail.SendMail(iCompanyId)
                        'Sohail (30 Nov 2017) -- End
                    Next
                Case enAssessmentMode.APPRAISER_ASSESSMENT
                    For Each dtRow As DataRow In dsList.Tables("List").Rows
                        If dtRow.Item("Email") = "" Then Continue For
                        Dim strMessage As String = ""
                        objMail._Subject = Language.getMessage(mstrModuleName, 4, "Notifications for BSC.")
                        strMessage = "<HTML> <BODY>"
                        strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " <B>" & dtRow.Item("Ename").ToString() & "</B>, <BR><BR>"
                        strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 10, "This is to inform you that, the Balanced Score Card Assessment for Employee ")
                        strMessage &= "<B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>" & Language.getMessage(mstrModuleName, 19, " for ") & "<B>" & objPeriod._Period_Name & "</B> and Year <B>" & sYearName & "</B>" & Language.getMessage(mstrModuleName, 8, "is complete. Please click the link below to review it.")
                        strMessage &= "<BR><BR>"
                        strMessage &= "<B>" & objEmp._Firstname & " " & objEmp._Surname & "</B>"
                        'S.SANDEEP [ 24 APR 2014 ] -- START
                        'strLink = sArutiSSURL & "/Assessment/BSC/wPgReviewer_AddEdit.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iYearId.ToString & "|" & sYearName.ToString & "|" & iPeriodId.ToString))
                        strLink = sArutiSSURL & "/Assessment/BSC/wPgTabularReviewer_BSC_AddEdit.aspx?" & System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iCompanyId.ToString & "|" & dtRow.Item("userunkid").ToString & "|" & iEmployeeId.ToString & "|" & dtRow.Item("assessormasterunkid").ToString & "|" & iYearId.ToString & "|" & sYearName.ToString & "|" & iPeriodId.ToString))
                        'S.SANDEEP [ 24 APR 2014 ] -- END
                        strMessage &= "<BR></BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='" & strLink & "'>" & strLink & "</a>"
                        strMessage &= "<BR><BR>"
                        strMessage &= "<B>" & sName & "</B>"
                        strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                        strMessage &= "</BODY></HTML>"
                        objMail._Message = strMessage
                        objMail._ToEmail = dtRow.Item("Email")
                        'S.SANDEEP [ 28 JAN 2014 ] -- START
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrWebFrmName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrWebFrmName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        'S.SANDEEP [ 28 JAN 2014 ] -- END
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objMail.SendMail()
                        objMail.SendMail(iCompanyId)
                        'Sohail (30 Nov 2017) -- End
                    Next
            End Select

            Dim sMsg As String = ""
            If isReviewerMandatory = True Then
                Select Case eAssessMode
                    Case enAssessmentMode.REVIEWER_ASSESSMENT
                        If objEmp._Email.ToString.Trim.Length <= 0 Then Exit Sub
                        'S.SANDEEP [ 13 NOV 2013 ] -- START
                        'sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName)
                        sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName, iEmployeeId, iPeriodId)
                        'S.SANDEEP [ 13 NOV 2013 ] -- END
                        objMail._Subject = Language.getMessage(mstrModuleName, 4, "Notifications for BSC.")
                        objMail._Message = sMsg
                        objMail._ToEmail = objEmp._Email
                        'S.SANDEEP [ 28 JAN 2014 ] -- START
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrWebFrmName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrWebFrmName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        'S.SANDEEP [ 28 JAN 2014 ] -- END
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objMail.SendMail()
                        objMail.SendMail(iCompanyId)
                        'Sohail (30 Nov 2017) -- End
                End Select
            Else
                Select Case eAssessMode
                    Case enAssessmentMode.APPRAISER_ASSESSMENT
                        If objEmp._Email.ToString.Trim.Length <= 0 Then Exit Sub
                        'S.SANDEEP [ 13 NOV 2013 ] -- START
                        'sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName)
                        sMsg = Notify_Employee(objEmp._Firstname & " " & objEmp._Surname, objPeriod._Period_Name, sYearName, sName, iEmployeeId, iPeriodId)
                        'S.SANDEEP [ 13 NOV 2013 ] -- END
                        objMail._Subject = Language.getMessage(mstrModuleName, 4, "Notifications for BSC.")
                        objMail._Message = sMsg
                        objMail._ToEmail = objEmp._Email
                        'S.SANDEEP [ 28 JAN 2014 ] -- START
                        If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                        If mstrWebFrmName.Trim.Length > 0 Then
                            objMail._Form_Name = mstrWebFrmName
                        End If
                        objMail._LogEmployeeUnkid = iLoginEmployeeId
                        objMail._OperationModeId = iLoginTypeId
                        objMail._UserUnkid = mintUserunkid
                        objMail._SenderAddress = IIf(objEmp._Email = "", objEmp._Firstname & " " & objEmp._Surname, objEmp._Email)
                        objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                        'S.SANDEEP [ 28 JAN 2014 ] -- END
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objMail.SendMail()
                        objMail.SendMail(iCompanyId)
                        'Sohail (30 Nov 2017) -- End
                End Select
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Email_Notification", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function Notify_Employee(ByVal eName As String, ByVal ePeriodName As String, ByVal eYearName As String, ByVal eARName As String, ByVal iEmpId As Integer, ByVal iPeriodId As Integer) As String 'S.SANDEEP [ 13 NOV 2013 ] -- START {iEmpId,iPeriodId} -- END


        Dim strMessage As String = ""
        Try
            strMessage = "<HTML> <BODY>"
            strMessage &= Language.getMessage(mstrModuleName, 5, "Dear") & " <B>" & eName & "</B>, <BR><BR>"
            strMessage &= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & Language.getMessage(mstrModuleName, 9, "This is to inform you that, Your Balanced Score Card Assessment for the Period of ")
            strMessage &= "<B>" & ePeriodName & "</B> and Year <B>" & eYearName & "</B>" & Language.getMessage(mstrModuleName, 12, " is complete. Please login to Aruti Employee Self Service to view it.")
            strMessage &= "<BR><BR>"
            strMessage &= "<B>" & eARName & "</B>"
            'S.SANDEEP [ 13 NOV 2013 ] -- START
            strMessage &= "<BR><BR>"
            strMessage &= GetNotification_Score(iEmpId, iPeriodId)
            'S.SANDEEP [ 13 NOV 2013 ] -- END

            'S.SANDEEP [ 09 OCT 2014 ] -- START
            strMessage &= "<BR><BR>"
            strMessage &= "<B>" & Language.getMessage(mstrModuleName, 20, "Note :") & "</B>" & Language.getMessage(mstrModuleName, 21, "This is not final Score you will get final Score upon completion both BSC and General Assessment")
            'S.SANDEEP [ 09 OCT 2014 ] -- END
            strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
            strMessage &= "</BODY></HTML>"

            Return strMessage

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Notify_Employee", mstrModuleName)
            Return ""
        Finally
        End Try
    End Function
    'S.SANDEEP [ 13 AUG 2013 ] -- END

    'S.SANDEEP [ 13 NOV 2013 ] -- START
    Private Function GetNotification_Score(ByVal iEmpId As Integer, ByVal iPrdId As Integer) As String
        Dim iHTML_Msg As String = String.Empty
        Dim dsAsr As New DataSet
        Dim dsRev As New DataSet
        Dim StrQ As String = String.Empty
        Try
            Using iDataOpr As New clsDataOperation
                StrQ = "SELECT " & _
                       "     employeecode AS [Code] " & _
                       "    ,firstname+' '+surname AS [Name] " & _
                       "    ,ISNULL(SUM(CAST(ISNULL(resultname,0) AS DECIMAL(10,2))),0) AS [Score] " & _
                       "FROM hrbsc_analysis_master " & _
                       "    JOIN hremployee_master ON hrbsc_analysis_master.assessoremployeeunkid = hremployee_master.employeeunkid " & _
                       "    JOIN hrbsc_analysis_tran ON hrbsc_analysis_master.analysisunkid = hrbsc_analysis_tran.analysisunkid " & _
                       "    JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                       "WHERE assessedemployeeunkid = '" & iEmpId & "' AND assessmodeid = '" & enAssessmentMode.APPRAISER_ASSESSMENT & "' AND periodunkid = '" & iPrdId & "' AND hrbsc_analysis_master.isvoid = 0 " & _
                       "GROUP BY employeecode,firstname+' '+surname "

                dsAsr = iDataOpr.ExecQuery(StrQ, "List")

                If iDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(iDataOpr.ErrorNumber & " : " & iDataOpr.ErrorMessage)
                End If

                StrQ = "SELECT " & _
                       "     employeecode AS [Code] " & _
                       "    ,firstname+' '+surname AS [Name] " & _
                       "    ,ISNULL(SUM(CAST(ISNULL(resultname,0) AS DECIMAL(10,2))),0) AS [Score] " & _
                       "FROM hrbsc_analysis_master " & _
                       "    JOIN hremployee_master ON hrbsc_analysis_master.reviewerunkid = hremployee_master.employeeunkid " & _
                       "    JOIN hrbsc_analysis_tran ON hrbsc_analysis_master.analysisunkid = hrbsc_analysis_tran.analysisunkid " & _
                       "    JOIN hrresult_master ON hrbsc_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                       "WHERE assessedemployeeunkid = '" & iEmpId & "' AND assessmodeid = '" & enAssessmentMode.REVIEWER_ASSESSMENT & "' AND periodunkid = '" & iPrdId & "' AND hrbsc_analysis_master.isvoid = 0 " & _
                       "GROUP BY employeecode,firstname+' '+surname "

                dsRev = iDataOpr.ExecQuery(StrQ, "List")

                If iDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(iDataOpr.ErrorNumber & " : " & iDataOpr.ErrorMessage)
                End If

                If dsAsr.Tables(0).Rows.Count > 0 Or dsRev.Tables(0).Rows.Count > 0 Then
                    iHTML_Msg &= "<TABLE border = '0' WIDTH = '100%'>" & vbCrLf
                    iHTML_Msg &= "<TR WIDTH = '100%'>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'LEFT' STYLE='WIDTH:50%'>" & vbCrLf
                    iHTML_Msg &= "<TABLE border = '1' WIDTH = '50%'>" & vbCrLf
                    iHTML_Msg &= "<TR WIDTH = '50%' bgcolor= 'SteelBlue'>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'LEFT' STYLE='WIDTH:2%'><B><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px; display:block;'>" & Language.getMessage(mstrModuleName, 14, "Assessor Code") & "</span></B></TD>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'LEFT' STYLE='WIDTH:5%'><B><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px; display:block;'>" & Language.getMessage(mstrModuleName, 15, "Assessor Name") & "</span></B></TD>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'RIGHT' STYLE='WIDTH:1%'><B><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px; display:block;'>" & Language.getMessage(mstrModuleName, 16, "Score") & "</span></B></TD>" & vbCrLf
                    iHTML_Msg &= "</TR>" & vbCrLf
                    For Each aRow As DataRow In dsAsr.Tables(0).Rows
                        iHTML_Msg &= "<TR WIDTH = '50%'>" & vbCrLf
                        iHTML_Msg &= "<TD align = 'LEFT' WIDTH = '2%'><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px; display:block;'>" & aRow.Item("Code").ToString & "</span></TD>" & vbCrLf
                        iHTML_Msg &= "<TD align = 'LEFT' WIDTH = '5%'><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px; display:block;'>" & aRow.Item("Name").ToString & "</span></TD>" & vbCrLf
                        iHTML_Msg &= "<TD align = 'RIGHT' WIDTH = '1%'><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px; display:block;'>" & aRow.Item("Score").ToString & "</span></TD>" & vbCrLf
                        iHTML_Msg &= "</TR>" & vbCrLf
                    Next
                    iHTML_Msg &= "</TABLE>" & vbCrLf
                    iHTML_Msg &= "<BR>" & vbCrLf
                    iHTML_Msg &= "<TABLE border = '1' WIDTH = '50%'>" & vbCrLf
                    iHTML_Msg &= "<TR WIDTH = '50%' bgcolor= 'SteelBlue'>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'LEFT' STYLE='WIDTH:2%'><B><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px; display:block;'>" & Language.getMessage(mstrModuleName, 17, "Reviewer Code") & "</span></B></TD>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'LEFT' STYLE='WIDTH:5%'><B><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px; display:block;'>" & Language.getMessage(mstrModuleName, 18, "Reviewer Name") & "</span></B></TD>" & vbCrLf
                    iHTML_Msg &= "<TD align = 'RIGHT' STYLE='WIDTH:1%'><B><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#FFFFFF; margin-left:0px;margin-right:0px; display:block;'>" & Language.getMessage(mstrModuleName, 16, "Score") & "</span></B></TD>" & vbCrLf
                    iHTML_Msg &= "</TR>" & vbCrLf
                    For Each aRow As DataRow In dsRev.Tables(0).Rows
                        iHTML_Msg &= "<TR WIDTH = '50%'>" & vbCrLf
                        iHTML_Msg &= "<TD align = 'LEFT' WIDTH = '2%'><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px; display:block;'>" & aRow.Item("Code").ToString & "</span></TD>" & vbCrLf
                        iHTML_Msg &= "<TD align = 'LEFT' WIDTH = '5%'><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px; display:block;'>" & aRow.Item("Name").ToString & "</span></TD>" & vbCrLf
                        iHTML_Msg &= "<TD align = 'RIGHT' WIDTH = '1%'><span style='font-size:8.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#000000; margin-left:0px;margin-right:0px; display:block;'>" & aRow.Item("Score").ToString & "</span></TD>" & vbCrLf
                        iHTML_Msg &= "</TR>" & vbCrLf
                    Next
                    iHTML_Msg &= "</TABLE>" & vbCrLf
                    iHTML_Msg &= "</TD>" & vbCrLf
                    iHTML_Msg &= "</TR>" & vbCrLf
                    iHTML_Msg &= "</TABLE>"
                End If

            End Using

            Return iHTML_Msg
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetNotification_Score", mstrModuleName)
            Return iHTML_Msg
        Finally
        End Try
    End Function
    'S.SANDEEP [ 13 NOV 2013 ] -- END


    'S.SANDEEP [ 24 APR 2014 ] -- START
    Public Function GetTabularData(ByVal intEmployeeId As Integer, _
                                   ByVal intYearId As Integer, _
                                   ByVal intPeriodId As Integer, _
                                   Optional ByVal strIsBSC_ByEmployee As String = "", _
                                   Optional ByVal StrDataBaseName As String = "") As DataSet
        If StrDataBaseName.Trim.Length <= 0 Then StrDataBaseName = FinancialYear._Object._DatabaseName
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dtTable As DataTable
        Dim dCol As DataColumn
        objDataOperation = New clsDataOperation
        Try
            dtTable = New DataTable("View")
            dCol = New DataColumn
            With dCol
                .ColumnName = "Objective"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "KPI"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Target"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Initative"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Weight"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Result"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "Remark"
                .DefaultValue = ""
                .DataType = System.Type.GetType("System.String")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "PerspectiveId"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "PeriodId"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "ObjectiveId"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "KpiId"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "TargetId"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "InitiativeId"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "RGrpId"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "IsGrp"
                .DefaultValue = False
                .DataType = System.Type.GetType("System.Boolean")
            End With
            dtTable.Columns.Add(dCol)

            dCol = New DataColumn
            With dCol
                .ColumnName = "GrpId"
                .DefaultValue = 0
                .DataType = System.Type.GetType("System.Int32")
            End With
            dtTable.Columns.Add(dCol)

            strQ = " SELECT '" & enBSCPerspective.FINANCIAL & "' AS Id ,@FINANCIAL AS NAME UNION " & _
                   " SELECT '" & enBSCPerspective.CUSTOMER & "' AS Id ,@CUSTOMER AS NAME UNION " & _
                   " SELECT '" & enBSCPerspective.BUSINESS_PROCESS & "' AS Id ,@BUSINESS_PROCESS AS NAME UNION " & _
                   " SELECT '" & enBSCPerspective.ORGANIZATION_CAPACITY & "' AS Id ,@ORGANIZATION_CAPACITY AS NAME "

            objDataOperation.AddParameter("@FINANCIAL", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 315, "Financial"))
            objDataOperation.AddParameter("@CUSTOMER", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 316, "Customer"))
            objDataOperation.AddParameter("@BUSINESS_PROCESS", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 317, "Business Process"))
            objDataOperation.AddParameter("@ORGANIZATION_CAPACITY", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 318, "Organization Capacity"))

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            For Each dtRow As DataRow In dsList.Tables(0).Rows
                Dim dRow As DataRow = dtTable.NewRow

                dRow.Item("Objective") = dtRow.Item("NAME")
                dRow.Item("PerspectiveId") = dtRow.Item("Id")
                dRow.Item("IsGrp") = True
                dRow.Item("GrpId") = dtRow.Item("Id")

                dtTable.Rows.Add(dRow)
            Next

            Dim dPlanning As DataTable = Nothing

            If dtTable.Rows.Count > 0 Then
                Dim iRCnt As Integer = dtTable.Rows.Count
                strQ = "SELECT perspectiveunkid " & _
                        ", objectiveunkid " & _
                        ", kpiunkid " & _
                        ", targetunkid " & _
                        ", initiativeunkid " & _
                        ", objective " & _
                        ", kpi " & _
                        ", target " & _
                        ", initiative " & _
                        ", CASE WHEN oweight <=0 THEN '' ELSE CAST(oweight AS NVARCHAR(MAX)) END oweight " & _
                        ", CASE WHEN kweight <=0 THEN '' ELSE CAST(kweight AS NVARCHAR(MAX)) END kweight " & _
                        ", CASE WHEN tweight <=0 THEN '' ELSE CAST(tweight AS NVARCHAR(MAX)) END tweight " & _
                        ", CASE WHEN iweight <=0 THEN '' ELSE CAST(iweight AS NVARCHAR(MAX)) END iweight " & _
                        ", RGrpId " & _
                    "FROM " & _
                    "( " & _
                        "SELECT " & _
                        "	 perspectiveunkid " & _
                        "	,hrobjective_master.objectiveunkid " & _
                        "	,ISNULL(hrkpi_master.kpiunkid,0) AS kpiunkid " & _
                        "	,ISNULL(hrtarget_master.targetunkid,0) AS targetunkid " & _
                        "	,ISNULL(hrinitiative_master.initiativeunkid,0) AS initiativeunkid " & _
                        "	,ISNULL(hrobjective_master.name,'') AS objective " & _
                        "	,ISNULL(hrkpi_master.name,'') AS kpi " & _
                        "	,ISNULL(hrtarget_master.name,'') AS target " & _
                        "	,ISNULL(hrinitiative_master.name,'') AS initiative " & _
                        "   ,ISNULL(hrobjective_master.weight,0) AS oweight " & _
                        "   ,ISNULL(hrkpi_master.weight,0) AS kweight " & _
                        "   ,ISNULL(hrtarget_master.weight,0) AS tweight " & _
                        "   ,ISNULL(hrinitiative_master.weight,0) AS iweight " & _
                        "   ,hrobjective_master.resultgroupunkid AS RGrpId " & _
                        "FROM hrobjective_master " & _
                        "	LEFT JOIN hrkpi_master ON hrobjective_master.objectiveunkid = hrkpi_master.objectiveunkid AND hrkpi_master.isactive = 1 " & _
                        "	LEFT JOIN hrtarget_master ON hrkpi_master.kpiunkid = hrtarget_master.kpiunkid AND hrtarget_master.kpiunkid > 0 AND hrtarget_master.isactive = 1 " & _
                        "	LEFT JOIN hrinitiative_master ON hrtarget_master.targetunkid = hrinitiative_master.targetunkid AND hrinitiative_master.targetunkid > 0 AND hrinitiative_master.isactive = 1 " & _
                        "WHERE hrobjective_master.isactive = 1 AND hrobjective_master.employeeunkid = '" & intEmployeeId & "' AND hrobjective_master.periodunkid = '" & intPeriodId & "' AND hrobjective_master.yearunkid = '" & intYearId & "' AND hrobjective_master.isfinal = 1 " & _
                        "UNION " & _
                        "SELECT " & _
                        "	 perspectiveunkid " & _
                        "	,hrobjective_master.objectiveunkid " & _
                        "	,0 AS kpiunkid " & _
                        "	,ISNULL(hrtarget_master.targetunkid,'') AS targetunkid " & _
                        "    ,0 AS initiativeunkid " & _
                        "    ,hrobjective_master.name AS objective " & _
                        "    ,'' AS kpi " & _
                        "    ,ISNULL(hrtarget_master.name,'') AS target " & _
                        "    ,'' AS initiative " & _
                        "    ,ISNULL(hrobjective_master.weight,0) AS oweight " & _
                        "    ,0 AS kweight " & _
                        "    ,ISNULL(hrtarget_master.weight,0) AS tweight " & _
                        "    ,0 AS iweight " & _
                        "    ,hrobjective_master.resultgroupunkid AS RGrpId " & _
                        "FROM hrobjective_master " & _
                        "    JOIN hrtarget_master ON hrobjective_master.objectiveunkid = hrtarget_master.objectiveunkid AND kpiunkid <= 0 AND hrtarget_master.isactive = 1 " & _
                        "WHERE hrobjective_master.isactive = 1 AND hrobjective_master.employeeunkid = '" & intEmployeeId & "' AND hrobjective_master.periodunkid = '" & intPeriodId & "' AND hrobjective_master.yearunkid = '" & intYearId & "' AND hrobjective_master.isfinal = 1 " & _
                        "UNION " & _
                        "SELECT " & _
                        "     perspectiveunkid " & _
                        "    ,hrobjective_master.objectiveunkid " & _
                        "    ,0 AS kpiunkid " & _
                        "    ,0 AS targetunkid " & _
                        "	,ISNULL(hrinitiative_master.initiativeunkid,0) AS initiativeunkid " & _
                        "	,hrobjective_master.name AS objective " & _
                        "	,'' AS kpi " & _
                        "    ,'' AS target " & _
                        "	,ISNULL(hrinitiative_master.name,'') AS initiative " & _
                        "   ,ISNULL(hrobjective_master.weight,0) AS oweight " & _
                        "   ,0 AS kweight " & _
                        "    ,0 AS tweight " & _
                        "   ,ISNULL(hrinitiative_master.weight,0) AS iweight " & _
                        "   ,hrobjective_master.resultgroupunkid AS RGrpId " & _
                        "FROM hrobjective_master " & _
                        "    JOIN hrinitiative_master ON hrobjective_master.objectiveunkid = hrinitiative_master.objectiveunkid AND hrinitiative_master.targetunkid <= 0 AND hrinitiative_master.isactive = 1 " & _
                        "WHERE hrobjective_master.isactive = 1 AND hrobjective_master.employeeunkid = '" & intEmployeeId & "' AND hrobjective_master.periodunkid = '" & intPeriodId & "' AND hrobjective_master.yearunkid = '" & intYearId & "' AND hrobjective_master.isfinal = 1 " & _
                        ") AS Final_Data WHERE 1 = 1 "

                'dPlanning = GetFullBSC(intEmployeeId, intPeriodId)

                dPlanning = objDataOperation.ExecQuery(strQ, "List").Tables(0)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If


                'S.SANDEEP [ 30 MAY 2014 ] -- START
                'Dim oWSett As New clsWeight_Setting(True)
                Dim oWSett As New clsWeight_Setting(intPeriodId)
                'S.SANDEEP [ 30 MAY 2014 ] -- END
                For i As Integer = 0 To iRCnt - 1
                    If CInt(dtTable.Rows(i).Item("PerspectiveId")) > 0 Then
                        Dim dTemp() As DataRow = dPlanning.Select("perspectiveunkid = '" & dtTable.Rows(i).Item("PerspectiveId") & "'")
                        If dTemp.Length > 0 Then
                            For iCnt As Integer = 0 To dTemp.Length - 1
                                Dim dNRow As DataRow = dtTable.NewRow
                                dNRow.Item("PeriodId") = intPeriodId
                                dNRow.Item("IsGrp") = False
                                dNRow.Item("GrpId") = dtTable.Rows(i).Item("GrpId")
                                dNRow.Item("PerspectiveId") = dtTable.Rows(i).Item("PerspectiveId")
                                dNRow.Item("ObjectiveId") = dTemp(iCnt).Item("objectiveunkid")
                                dNRow.Item("KpiId") = dTemp(iCnt).Item("kpiunkid")
                                dNRow.Item("TargetId") = dTemp(iCnt).Item("targetunkid")
                                dNRow.Item("InitiativeId") = dTemp(iCnt).Item("initiativeunkid")
                                dNRow.Item("Objective") = Space(5) & dTemp(iCnt).Item("objective")
                                dNRow.Item("KPI") = dTemp(iCnt).Item("kpi")
                                dNRow.Item("Target") = dTemp(iCnt).Item("target")
                                dNRow.Item("Initative") = dTemp(iCnt).Item("initiative")

                                If oWSett._Weight_Optionid <> enWeight_Options.WEIGHT_EACH_ITEM Then
                                    Select Case oWSett._Weight_Typeid
                                        Case enWeight_Types.WEIGHT_FIELD1
                                            dNRow.Item("weight") = dTemp(iCnt).Item("oweight")
                                        Case enWeight_Types.WEIGHT_FIELD2
                                            dNRow.Item("weight") = dTemp(iCnt).Item("kweight")
                                        Case enWeight_Types.WEIGHT_FIELD3
                                            dNRow.Item("weight") = dTemp(iCnt).Item("tweight")
                                        Case enWeight_Types.WEIGHT_FIELD5
                                            dNRow.Item("weight") = dTemp(iCnt).Item("iweight")
                                    End Select
                                ElseIf oWSett._Weight_Optionid = enWeight_Options.WEIGHT_EACH_ITEM Then
                                    dNRow.Item("weight") = dTemp(iCnt).Item("iweight")
                                End If
                                dNRow.Item("RGrpId") = dTemp(iCnt).Item("RGrpId")

                                dtTable.Rows.Add(dNRow)
                            Next
                        End If
                    End If
                Next


                '    For i As Integer = 0 To iRCnt - 1
                '        If CInt(dtTable.Rows(i).Item("ObjUnkid")) > 0 Then
                '            Dim dTemp() As DataRow = dPlanning.Select("objectiveunkid = '" & dtTable.Rows(i).Item("ObjUnkid") & "'")
                '            If dTemp.Length > 0 Then
                '                For iCnt As Integer = 0 To dTemp.Length - 1
                '                    Dim dNRow As DataRow = dtTable.NewRow

                '                    dNRow.Item("PId") = dtTable.Rows(i).Item("PId")
                '                    dNRow.Item("objGrpId") = dtTable.Rows(i).Item("PId")
                '                    dNRow.Item("ObjUnkid") = dtTable.Rows(i).Item("ObjUnkid")
                '                    dNRow.Item("KpiUnkid") = dTemp(iCnt).Item("kpiunkid")
                '                    dNRow.Item("TargetUnkid") = dTemp(iCnt).Item("targetunkid")
                '                    dNRow.Item("InitiativeUnkid") = dTemp(iCnt).Item("initiativeunkid")

                '                    dNRow.Item("perspective") = dtTable.Rows(i).Item("perspective")
                '                    dNRow.Item("objective") = ""
                '                    dNRow.Item("kpi") = dTemp(iCnt).Item("KPI")
                '                    dNRow.Item("targets") = dTemp(iCnt).Item("Target")
                '                    dNRow.Item("initiative") = dTemp(iCnt).Item("Initiative")

                '                    If oWSett._Weight_Optionid <> enWeight_Options.WEIGHT_EACH_ITEM Then
                '                        Select Case oWSett._Weight_Typeid
                '                            Case enWeight_Types.WEIGHT_FIELD2
                '                                dNRow.Item("weight") = dTemp(iCnt).Item("kweight")
                '                            Case enWeight_Types.WEIGHT_FIELD3
                '                                dNRow.Item("weight") = dTemp(iCnt).Item("tweight")
                '                            Case enWeight_Types.WEIGHT_FIELD5
                '                                dNRow.Item("weight") = dTemp(iCnt).Item("iweight")
                '                        End Select
                '                    End If

                '                    dtTable.Rows.Add(dNRow)
                '                Next
                '            End If
                '        End If
                '    Next
            End If

            Dim dView As DataView = dtTable.DefaultView
            dView.Sort = "PerspectiveId,IsGrp DESC"
            dtTable = dView.ToTable

            dsList = New DataSet
            dsList.Tables.Add(dtTable)

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAssessmentResultView; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 24 APR 2014 ] -- END

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Sorry, You cannot delete this Assessment. Reason : Assessor has already assessed this employee.")
			Language.setMessage(mstrModuleName, 3, "Sorry, You cannot delete this Assessment. Reason : Reviewer has already assessed this employee.")
			Language.setMessage(mstrModuleName, 4, "Notifications for BSC.")
			Language.setMessage(mstrModuleName, 5, "Dear")
			Language.setMessage(mstrModuleName, 6, "This is to inform you that, I have completed my Balanced Score Card Self-Assessment for the period of")
			Language.setMessage(mstrModuleName, 7, ". Please click the link below to assess me as my supervisor.")
			Language.setMessage(mstrModuleName, 8, "is complete. Please click the link below to review it.")
			Language.setMessage(mstrModuleName, 9, "This is to inform you that, Your Balanced Score Card Assessment for the Period of")
			Language.setMessage(mstrModuleName, 10, "This is to inform you that, the Balanced Score Card Assessment for Employee")
			Language.setMessage(mstrModuleName, 12, " is complete. Please login to Aruti Employee Self Service to view it.")
			Language.setMessage(mstrModuleName, 13, "Sorry, assessment already present for the selected period and employee. Please define new assessment.")
			Language.setMessage(mstrModuleName, 14, "Assessor Code")
			Language.setMessage(mstrModuleName, 15, "Assessor Name")
			Language.setMessage(mstrModuleName, 16, "Score")
			Language.setMessage(mstrModuleName, 17, "Reviewer Code")
			Language.setMessage(mstrModuleName, 18, "Reviewer Name")
			Language.setMessage(mstrModuleName, 19, " for")
			Language.setMessage("clsMasterData", 315, "Financial")
			Language.setMessage("clsMasterData", 316, "Customer")
			Language.setMessage("clsMasterData", 317, "Business Process")
			Language.setMessage("clsMasterData", 318, "Organization Capacity")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
