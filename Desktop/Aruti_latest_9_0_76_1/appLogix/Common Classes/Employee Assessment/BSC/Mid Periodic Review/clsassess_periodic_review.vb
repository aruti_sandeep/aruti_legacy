﻿'************************************************************************************************************************************
'Class Name : clsassess_periodic_review.vb
'Purpose    :
'Date       :18-Sep-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
Imports System.Threading
Imports System.ComponentModel

''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_periodic_review
    Private Shared ReadOnly mstrModuleName As String = "clsassess_periodic_review"
    Dim mstrMessage As String = ""
    Private trd As Thread

#Region " Private variables "

    Private mintReviewunkid As Integer = 0
    Private mintEmployeeunkid As Integer = 0
    Private mintOwrfield1unkid As Integer = 0
    Private mintFieldtranunkid As Integer = 0
    Private mintFieldunkid As Integer = 0
    Private mstrField_Data As String = String.Empty
    Private mintPeriodunkid As Integer = 0
    Private mdecWeight As Decimal = 0
    Private mdecPct_Complete As Decimal = 0
    Private mblnIsfinal As Boolean = False
    Private mdtStartdate As Date = Nothing
    Private mdtEnddate As Date = Nothing
    Private mintStatusunkid As Integer = 0
    Private mintPerspectiveunkid As Integer = 0
    Private mdtReviewdate As Date = Nothing
    Private mintFieldtypeid As Integer = 0
    Private mintReviewtypeid As Integer = 0
    Private mintUserunkid As Integer = 0
    Private mintLoginemployeeunkid As Integer = 0
    Private mstrWebFrmName As String = String.Empty
    Private mstrWebIP As String = ""
    Private mstrWebhostName As String = ""
    Private mintStatustranunkid As Integer = 0
    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    Private mintGoalTypeid As Integer = CInt(enGoalType.GT_QUALITATIVE)
    Private mdblGoalValue As Double = 0
    'S.SANDEEP [01-OCT-2018] -- END

#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reviewunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Reviewunkid() As Integer
        Get
            Return mintReviewunkid
        End Get
        Set(ByVal value As Integer)
            mintReviewunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set owrfield1unkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Owrfield1unkid() As Integer
        Get
            Return mintOwrfield1unkid
        End Get
        Set(ByVal value As Integer)
            mintOwrfield1unkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldtranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Fieldtranunkid() As Integer
        Get
            Return mintFieldtranunkid
        End Get
        Set(ByVal value As Integer)
            mintFieldtranunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Fieldunkid() As Integer
        Get
            Return mintFieldunkid
        End Get
        Set(ByVal value As Integer)
            mintFieldunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set field_data
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Field_Data() As String
        Get
            Return mstrField_Data
        End Get
        Set(ByVal value As String)
            mstrField_Data = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set periodunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Periodunkid() As Integer
        Get
            Return mintPeriodunkid
        End Get
        Set(ByVal value As Integer)
            mintPeriodunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight() As Decimal
        Get
            Return mdecWeight
        End Get
        Set(ByVal value As Decimal)
            mdecWeight = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set pct_complete
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Pct_Complete() As Decimal
        Get
            Return mdecPct_Complete
        End Get
        Set(ByVal value As Decimal)
            mdecPct_Complete = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isfinal
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isfinal() As Boolean
        Get
            Return mblnIsfinal
        End Get
        Set(ByVal value As Boolean)
            mblnIsfinal = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set startdate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Startdate() As Date
        Get
            Return mdtStartdate
        End Get
        Set(ByVal value As Date)
            mdtStartdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set enddate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Enddate() As Date
        Get
            Return mdtEnddate
        End Get
        Set(ByVal value As Date)
            mdtEnddate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statusunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statusunkid() As Integer
        Get
            Return mintStatusunkid
        End Get
        Set(ByVal value As Integer)
            mintStatusunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perspectiveunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Perspectiveunkid() As Integer
        Get
            Return mintPerspectiveunkid
        End Get
        Set(ByVal value As Integer)
            mintPerspectiveunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reviewdate
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Reviewdate() As Date
        Get
            Return mdtReviewdate
        End Get
        Set(ByVal value As Date)
            mdtReviewdate = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set fieldtypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Fieldtypeid() As Integer
        Get
            Return mintFieldtypeid
        End Get
        Set(ByVal value As Integer)
            mintFieldtypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set reviewtypeid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Reviewtypeid() As Integer
        Get
            Return mintReviewtypeid
        End Get
        Set(ByVal value As Integer)
            mintReviewtypeid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set statustranunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Statustranunkid() As Integer
        Get
            Return mintStatustranunkid
        End Get
        Set(ByVal value As Integer)
            mintStatustranunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set WebFrmName
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _WebFrmName() As String
        Set(ByVal value As String)
            mstrWebFrmName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set webip
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _WebIP() As String
        Set(ByVal value As String)
            mstrWebIP = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Set webhostname
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public WriteOnly Property _WebHostName() As String
        Set(ByVal value As String)
            mstrWebhostName = value
        End Set
    End Property

    'S.SANDEEP [01-OCT-2018] -- START
    'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
    Public Property _GoalTypeid() As Integer
        Get
            Return mintGoalTypeid
        End Get
        Set(ByVal value As Integer)
            mintGoalTypeid = value
        End Set
    End Property

    Public Property _GoalValue() As Double
        Get
            Return mdblGoalValue
        End Get
        Set(ByVal value As Double)
            mdblGoalValue = value
        End Set
    End Property
    'S.SANDEEP [01-OCT-2018] -- END

#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_periodic_review) </purpose>
    Public Function Insert(ByVal objDataOperation As clsDataOperation) As Boolean
        If isExist(objDataOperation, _
                   mintFieldtranunkid, _
                   mintFieldunkid, _
                   mstrField_Data, _
                   mintPeriodunkid, _
                   mintFieldtypeid, _
                   mintReviewtypeid, _
                   mintOwrfield1unkid, _
                   mdecWeight, _
                   mdecPct_Complete, _
                   mdtStartdate, _
                   mdtEnddate, _
                   mintStatusunkid, _
                   mintPerspectiveunkid, _
                   mintEmployeeunkid, _
                   mintStatustranunkid, mintGoalTypeid, mdblGoalValue) Then 'S.SANDEEP [01-OCT-2018] -- START {Ref#2585} [goaltypeid,goalvalue] -- END
            Return True
        End If
        Dim StrQ As String = ""
        Dim exForce As Exception
        Try
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@owrfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfield1unkid.ToString)
            objDataOperation.AddParameter("@fieldtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldtranunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdecWeight.ToString)
            objDataOperation.AddParameter("@pct_complete", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPct_Complete.ToString)
            objDataOperation.AddParameter("@isfinal", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsfinal.ToString)
            If mdtStartdate <> Nothing Then
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtStartdate)
            Else
                objDataOperation.AddParameter("@startdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            If mdtEnddate <> Nothing Then
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtEnddate)
            Else
                objDataOperation.AddParameter("@enddate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerspectiveunkid.ToString)
            objDataOperation.AddParameter("@reviewdate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtReviewdate)
            objDataOperation.AddParameter("@fieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldtypeid.ToString)
            objDataOperation.AddParameter("@reviewtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReviewtypeid.ToString)
            objDataOperation.AddParameter("@statustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatustranunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid.ToString)

            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objDataOperation.AddParameter("@goaltypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintGoalTypeid.ToString)
            objDataOperation.AddParameter("@goalvalue", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, mdblGoalValue.ToString)
            'S.SANDEEP [01-OCT-2018] -- END

            StrQ = "INSERT INTO hrassess_periodic_review ( " & _
                       "  employeeunkid " & _
                       ", owrfield1unkid " & _
                       ", fieldtranunkid " & _
                       ", fieldunkid " & _
                       ", field_data " & _
                       ", periodunkid " & _
                       ", weight " & _
                       ", pct_complete " & _
                       ", isfinal " & _
                       ", startdate " & _
                       ", enddate " & _
                       ", statusunkid " & _
                       ", perspectiveunkid " & _
                       ", reviewdate " & _
                       ", fieldtypeid " & _
                       ", reviewtypeid " & _
                       ", statustranunkid " & _
                       ", userunkid " & _
                       ", loginemployeeunkid" & _
                       ", goaltypeid" & _
                       ", goalvalue" & _
                   ") VALUES (" & _
                       "  @employeeunkid " & _
                       ", @owrfield1unkid " & _
                       ", @fieldtranunkid " & _
                       ", @fieldunkid " & _
                       ", @field_data " & _
                       ", @periodunkid " & _
                       ", @weight " & _
                       ", @pct_complete " & _
                       ", @isfinal " & _
                       ", @startdate " & _
                       ", @enddate " & _
                       ", @statusunkid " & _
                       ", @perspectiveunkid " & _
                       ", @reviewdate " & _
                       ", @fieldtypeid " & _
                       ", @reviewtypeid " & _
                       ", @statustranunkid " & _
                       ", @userunkid " & _
                       ", @loginemployeeunkid" & _
                       ", @goaltypeid" & _
                       ", @goalvalue" & _
                   "); "
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585} [goaltypeid,goalvalue] -- END

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
        End Try
    End Function

    Private Function isExist(ByVal objDataOperation As clsDataOperation, _
                             ByVal xFieldTranId As Integer, _
                             ByVal xFieldUnkid As Integer, _
                             ByVal xField_Data As String, _
                             ByVal xPeriodId As Integer, _
                             ByVal xFieldTypeId As Integer, _
                             ByVal xReviewTypeId As Integer, _
                             ByVal xOwnerFieldId As Integer, _
                             ByVal xWeight As Decimal, _
                             ByVal xPct_Complete As Decimal, _
                             ByVal xStartDate As Date, _
                             ByVal xEndDate As Date, _
                             ByVal xStatusId As Integer, _
                             ByVal xPerpectiveId As Integer, _
                             ByVal xEmployeeId As Integer, _
                             ByVal xStatusTranId As Integer, _
                             ByVal xGoalTypeid As Integer, _
                             ByVal xGoalValue As Double) As Boolean 'S.SANDEEP [01-OCT-2018] -- START {Ref#2585} [xGoalTypeid,xGoalValue] -- END

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Try
            strQ = "SELECT " & _
                       "  reviewunkid " & _
                       ", employeeunkid " & _
                       ", owrfield1unkid " & _
                       ", fieldtranunkid " & _
                       ", fieldunkid " & _
                       ", field_data " & _
                       ", periodunkid " & _
                       ", weight " & _
                       ", pct_complete " & _
                       ", isfinal " & _
                       ", startdate " & _
                       ", enddate " & _
                       ", statusunkid " & _
                       ", perspectiveunkid " & _
                       ", reviewdate " & _
                       ", fieldtypeid " & _
                       ", reviewtypeid " & _
                       ", statustranunkid " & _
                       ", userunkid " & _
                       ", loginemployeeunkid " & _
                       ", goaltypeid " & _
                       ", goalvalue " & _
                   "FROM hrassess_periodic_review " & _
                   "WHERE fieldtranunkid = @fieldtranunkid " & _
                       "AND fieldunkid = @fieldunkid " & _
                       "AND field_data = @field_data " & _
                       "AND periodunkid = @periodunkid " & _
                       "AND fieldtypeid = @fieldtypeid " & _
                       "AND reviewtypeid = @reviewtypeid " & _
                       "AND owrfield1unkid = @owrfield1unkid " & _
                       "AND weight = @weight " & _
                       "AND pct_complete = @pct_complete " & _
                       "AND statusunkid = @statusunkid " & _
                       "AND perspectiveunkid = @perspectiveunkid " & _
                       "AND employeeunkid = @employeeunkid " & _
                       "AND statustranunkid = @statustranunkid " & _
                       "AND goaltypeid = @goaltypeid " & _
                       "AND goalvalue  = @goalvalue "
            'S.SANDEEP [01-OCT-2018] -- START {Ref#2585} [goaltypeid,goalvalue] -- END

            If mdtStartdate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),startdate,112) = @startdate "
                objDataOperation.AddParameter("@startdate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtStartdate))
            End If
            If mdtEnddate <> Nothing Then
                strQ &= " AND CONVERT(CHAR(8),enddate,112) = @enddate "
                objDataOperation.AddParameter("@enddate", SqlDbType.NVarChar, eZeeDataType.DATETIME_SIZE, eZeeDate.convertDate(mdtEnddate))
            End If

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@statustranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatustranunkid.ToString)
            objDataOperation.AddParameter("@fieldtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldtranunkid.ToString)
            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldunkid.ToString)
            objDataOperation.AddParameter("@field_data", SqlDbType.NVarChar, mstrField_Data.Length, mstrField_Data.ToString)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPeriodunkid.ToString)
            objDataOperation.AddParameter("@fieldtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFieldtypeid.ToString)
            objDataOperation.AddParameter("@reviewtypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReviewtypeid.ToString)
            objDataOperation.AddParameter("@owrfield1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOwrfield1unkid.ToString)
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecWeight.ToString)
            objDataOperation.AddParameter("@pct_complete", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecPct_Complete.ToString)
            objDataOperation.AddParameter("@statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStatusunkid.ToString)
            objDataOperation.AddParameter("@perspectiveunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerspectiveunkid.ToString)
            'S.SANDEEP [01-OCT-2018] -- START
            'ISSUE/ENHANCEMENT : {Ref#2585|ARUTI-}
            objDataOperation.AddParameter("@goaltypeid", SqlDbType.Int, eZeeDataType.INT_SIZE, xGoalTypeid.ToString)
            objDataOperation.AddParameter("@goalvalue", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xGoalValue.ToString)
            'S.SANDEEP [01-OCT-2018] -- END

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Sub SendPeriodicNotification(ByVal xEmailAddress As String, ByVal iPeriodId As Integer, ByVal strDatabaseName As String, Optional ByVal iCompanyId As Integer = 0, Optional ByVal iLoginTypeId As Integer = 0, _
                              Optional ByVal iLoginEmployeeId As Integer = 0)
        'Sohail (21 Aug 2015) - [strDatabaseName]
        Try
            If xEmailAddress.Trim.Length > 0 Then
                If iCompanyId <= 0 Then iCompanyId = Company._Object._Companyunkid

                Dim objPrd As New clscommom_period_Tran
                'Sohail (21 Aug 2015) -- Start
                'Enhancement - NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS.
                'objPrd._Periodunkid = iPeriodId
                objPrd._Periodunkid(strDatabaseName) = iPeriodId
                'Sohail (21 Aug 2015) -- End

                Dim sYearName As String = String.Empty
                Dim dsYear As New DataSet
                Dim objFY As New clsCompany_Master
                dsYear = objFY.GetFinancialYearList(iCompanyId, , "List", objPrd._Yearunkid)
                If dsYear.Tables("List").Rows.Count > 0 Then
                    sYearName = dsYear.Tables("List").Rows(0).Item("financialyear_name")
                End If
                objFY = Nothing
                Dim strMessage As String = ""
                strMessage = "<HTML> <BODY>"
                strMessage &= "<BR></BR>"


                'Pinkal (22-Mar-2019) -- Start
                'Enhancement - Working on PA Changes FOR NMB.

                'strMessage &= Language.getMessage(mstrModuleName, 1, "This is to notify you that your performance plans for the period of") & "&nbsp; <B>" & objPrd._Period_Name & "</B> &nbsp; " & _
                '              Language.getMessage(mstrModuleName, 2, "year") & "&nbsp; <B>" & sYearName & "</B> &nbsp;" & Language.getMessage(mstrModuleName, 3, "has been opened for review.") & "<BR>" & _
                '              Language.getMessage(mstrModuleName, 4, "You are advised to review your performance plans/goals before submitting it to your supervisor for approval.")

                'S.SANDEEP |25-MAR-2019| -- START
                'strMessage &= Language.getMessage(mstrModuleName, 1, "This is to notify you that your performance plans for the period of") & "&nbsp; <B>(" & objPrd._Period_Name & ")</B> &nbsp; " & _
                '              Language.getMessage(mstrModuleName, 2, "year") & "&nbsp; <B>" & sYearName & "</B> &nbsp;" & Language.getMessage(mstrModuleName, 3, "has been opened for review.") & "<BR>" & _
                '              Language.getMessage(mstrModuleName, 4, "You are advised to review your performance plans/goals before submitting it to your supervisor for approval.")

                strMessage &= Language.getMessage(mstrModuleName, 1, "This is to notify you that your performance plans for the period of") & "&nbsp; <B>(" & objPrd._Period_Name & ")</B> &nbsp; " & _
                              Language.getMessage(mstrModuleName, 3, "has been opened for review.") & "<BR>" & _
                              Language.getMessage(mstrModuleName, 4, "You are advised to review your performance plans/goals before submitting it to your supervisor for approval.")
                'S.SANDEEP |25-MAR-2019| -- END


                'Pinkal (22-Mar-2019) -- End

                strMessage &= "<BR><BR><B>Performance Management Team</B>"
                strMessage &= "<BR></BR><BR></BR><center>""POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE""</center>"
                strMessage &= "</BODY></HTML>"

                For Each xMailId As String In xEmailAddress.Split(",")
                    Dim objMail As New clsSendMail
                    objMail._ToEmail = xMailId
                    objMail._Subject = Language.getMessage(mstrModuleName, 5, "Performance Plans Review")
                    objMail._Message = strMessage
                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                    If mstrWebFrmName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrWebFrmName
                    End If
                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = iLoginTypeId
                    objMail._UserUnkid = mintUserunkid
                    objMail._SenderAddress = Company._Object._Senderaddress
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT
                    If mstrWebIP.Trim = "" Then mstrWebIP = getIP()
                    If mstrWebhostName.Trim = "" Then mstrWebhostName = getHostName()

                    gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFrmName, iLoginEmployeeId, mstrWebIP, mstrWebhostName, mintUserunkid, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.ASSESSMENT_MGT, Company._Object._Senderaddress))
                Next
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                'Sohail (30 Nov 2017) -- Start
                'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                'trd.Start()
                Dim arr(1) As Object
                arr(0) = CInt(iCompanyId)
                trd.Start(arr)
                'Sohail (30 Nov 2017) -- End
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SendPeriodicNotification", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Send_Notification(ByVal iCompanyId As Object)
        'Sohail (30 Nov 2017) - [iCompanyId]
        Try
            If gobjEmailList.Count > 0 Then
                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId
                    Try
                        'Sohail (30 Nov 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail()
                        'Sohail (13 Dec 2017) -- Start
                        'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                        'objSendMail.SendMail(CInt(iCompanyId))
                        If TypeOf iCompanyId Is Integer Then
                        objSendMail.SendMail(CInt(iCompanyId))
                        Else
                            objSendMail.SendMail(CInt(iCompanyId(0)))
                        End If
                        'Sohail (13 Dec 2017) -- End
                        'Sohail (30 Nov 2017) -- End
                    Catch ex As Exception

                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "This is to notify you that your performance plans for the period of")
			Language.setMessage(mstrModuleName, 2, "year")
			Language.setMessage(mstrModuleName, 3, "has been opened for review.")
			Language.setMessage(mstrModuleName, 4, "You are advised to review your performance plans/goals before submitting it to your supervisor for approval.")
			Language.setMessage(mstrModuleName, 5, "Performance Plans Review")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
