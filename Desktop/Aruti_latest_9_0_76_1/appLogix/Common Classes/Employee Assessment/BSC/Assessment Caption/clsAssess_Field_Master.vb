﻿'************************************************************************************************************************************
'Class Name :clsAssess_Field_Master.vb
'Purpose    :
'Date       :01-Apr-2014
'Written By :Sandeep J. Sharma
'Modified   :ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsAssess_Field_Master
    Private Shared ReadOnly mstrModuleName As String = "clsAssess_Field_Master"
    Private mstrMessage As String = ""
    Private mintfieldunkid As Integer = 0

#Region " Enum "

    Public Enum enFieldCheckType
        FIELD_DATA = 1
        FIELD_LINK = 2
    End Enum

    Public Enum enCommitMode
        COY_COMMIT = 1
        OWR_COMMIT = 2
    End Enum

    Public Enum enOtherInfoField
        ST_DATE = 1001
        ED_DATE = 1002
        STATUS = 1003
        PCT_COMPLETE = 1004
        WEIGHT = 1005
        SCORE = 1006
        PERSPECTIVE = 1007
        'S.SANDEEP [07 FEB 2015] -- START
        EMP_REMARK = 1008
        ASR_REMARK = 1009
        REV_REMARK = 1010
        'S.SANDEEP [07 FEB 2015] -- END

        'S.SANDEEP [11-OCT-2018] -- START
        GOAL_TYPE = 1011
        GOAL_VALUE = 1012
        'S.SANDEEP [11-OCT-2018] -- END
    End Enum

    Public Enum enColWidthType
        COL_PLAN = 1
        COL_EVAL = 2
    End Enum

#End Region

#Region " Contructor "

    Public Sub New(Optional ByVal blnSetCaption As Boolean = False)
        Try
            If blnSetCaption = True Then Call Get_Caption()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private Variables "

    Private mstrField1_Caption As String = String.Empty
    Private mstrField2_Caption As String = String.Empty
    Private mstrField3_Caption As String = String.Empty
    Private mstrField4_Caption As String = String.Empty
    Private mstrField5_Caption As String = String.Empty
    Private mstrField6_Caption As String = String.Empty
    Private mstrField7_Caption As String = String.Empty
    Private mstrField8_Caption As String = String.Empty
    Private mstrLinked_Caption As String = String.Empty

    Private mintLinkedFieldId As Integer = 0
    Private mintLinkedFieldOrder As Integer = 0

    Private mintField1Unkid As Integer = 0
    Private mintField2Unkid As Integer = 0
    Private mintField3Unkid As Integer = 0
    Private mintField4Unkid As Integer = 0
    Private mintField5Unkid As Integer = 0
    Private mintField6Unkid As Integer = 0
    Private mintField7Unkid As Integer = 0
    Private mintField8Unkid As Integer = 0

#End Region

#Region " Properties "

    Public WriteOnly Property _fieldunkid() As Integer
        Set(ByVal value As Integer)
            mintfieldunkid = value
            Call Get_Caption()
        End Set
    End Property

    Public ReadOnly Property _Field1_Caption() As String
        Get
            Return mstrField1_Caption
        End Get
    End Property

    Public ReadOnly Property _Field2_Caption() As String
        Get
            Return mstrField2_Caption
        End Get
    End Property

    Public ReadOnly Property _Field3_Caption() As String
        Get
            Return mstrField3_Caption
        End Get
    End Property

    Public ReadOnly Property _Field4_Caption() As String
        Get
            Return mstrField4_Caption
        End Get
    End Property

    Public ReadOnly Property _Field5_Caption() As String
        Get
            Return mstrField5_Caption
        End Get
    End Property

    Public ReadOnly Property _Field6_Caption() As String
        Get
            Return mstrField6_Caption
        End Get
    End Property

    Public ReadOnly Property _Field7_Caption() As String
        Get
            Return mstrField7_Caption
        End Get
    End Property

    Public ReadOnly Property _Field8_Caption() As String
        Get
            Return mstrField8_Caption
        End Get
    End Property

    Public ReadOnly Property _Linked_Caption() As String
        Get
            Return mstrLinked_Caption
        End Get
    End Property

    Public ReadOnly Property _LinkedFieldId() As Integer
        Get
            Return mintLinkedFieldId
        End Get
    End Property

    Public ReadOnly Property _LinkedFieldOrder() As Integer
        Get
            Return mintLinkedFieldOrder
        End Get
    End Property

    Public ReadOnly Property _Field1Unkid() As Integer
        Get
            Return mintField1Unkid
        End Get
    End Property

    Public ReadOnly Property _Field2Unkid() As Integer
        Get
            Return mintField2Unkid
        End Get
    End Property

    Public ReadOnly Property _Field3Unkid() As Integer
        Get
            Return mintField3Unkid
        End Get
    End Property

    Public ReadOnly Property _Field4Unkid() As Integer
        Get
            Return mintField4Unkid
        End Get
    End Property

    Public ReadOnly Property _Field5Unkid() As Integer
        Get
            Return mintField5Unkid
        End Get
    End Property

    Public ReadOnly Property _Field6Unkid() As Integer
        Get
            Return mintField6Unkid
        End Get
    End Property

    Public ReadOnly Property _Field7Unkid() As Integer
        Get
            Return mintField7Unkid
        End Get
    End Property

    Public ReadOnly Property _Field8Unkid() As Integer
        Get
            Return mintField8Unkid
        End Get
    End Property

#End Region

#Region " Private/Public Methods "

    Public Function Get_Fields_Details() As DataTable
        Dim StrQ As String = ""
        Dim objDataOperation As New clsDataOperation
        Dim mdtTran As DataTable
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            mdtTran = New DataTable("Fields")
            mdtTran.Columns.Add("fieldunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("fieldcaption", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isparent", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("isinformational", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("isused", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("linkedto", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""

            StrQ = "SELECT " & _
                   "  fieldunkid " & _
                   " ,fieldcaption " & _
                   " ,isparent " & _
                   " ,isinformational " & _
                   " ,isused " & _
                   " ,linkedto " & _
                   " ,'' AS AUD " & _
                   "FROM hrassess_field_master "

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count <= 0 Then
                For i As Integer = 1 To 8
                    Dim dRow As DataRow = mdtTran.NewRow()
                    dRow.Item("fieldcaption") = ""
                    If i = 1 Then
                        dRow.Item("isparent") = True
                        dRow.Item("isinformational") = False
                        dRow.Item("isused") = True
                        dRow.Item("AUD") = "A"
                    Else
                        dRow.Item("isparent") = False
                        dRow.Item("isinformational") = False
                        dRow.Item("isused") = False
                        dRow.Item("AUD") = "A"
                    End If
                    mdtTran.Rows.Add(dRow)
                Next
            Else
                mdtTran = dsList.Tables(0)
            End If

            Return mdtTran
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Fields_Details", mstrModuleName)
            Return Nothing
        Finally
            objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertUpdateField_Caption(ByVal intUserUnkid As Integer, ByVal mdtTran As DataTable) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.BindTransaction()

            For i = 0 To mdtTran.Rows.Count - 1
                objDataOperation.ClearParameters()
                With mdtTran.Rows(i)
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrassess_field_master ( " & _
                                           "  fieldcaption " & _
                                           ", isparent " & _
                                           ", isinformational " & _
                                           ", isused " & _
                                           ", linkedto " & _
                                       ") VALUES (" & _
                                           "  @fieldcaption " & _
                                           ", @isparent " & _
                                           ", @isinformational " & _
                                           ", @isused " & _
                                           ", @linkedto " & _
                                       "); SELECT @@identity "

                                objDataOperation.AddParameter("@fieldcaption", SqlDbType.NVarChar, .Item("fieldcaption").ToString.Length, .Item("fieldcaption"))
                                objDataOperation.AddParameter("@isparent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isparent"))
                                objDataOperation.AddParameter("@isinformational", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isinformational"))
                                objDataOperation.AddParameter("@isused", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isused"))
                                objDataOperation.AddParameter("@linkedto", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("linkedto"))

                                Dim dsList As New DataSet
                                dsList = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintfieldunkid = dsList.Tables(0).Rows(0).Item(0)

                                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_field_master", "fieldunkid", mintfieldunkid, , intUserUnkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "U"

                                StrQ = "UPDATE hrassess_field_master SET " & _
                                           "  fieldcaption = @fieldcaption" & _
                                           ", isparent = @isparent" & _
                                           ", isinformational = @isinformational" & _
                                           ", isused = @isused " & _
                                           ", linkedto = @linkedto " & _
                                       "WHERE fieldunkid = @fieldunkid "

                                objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("fieldunkid"))
                                objDataOperation.AddParameter("@fieldcaption", SqlDbType.NVarChar, .Item("fieldcaption").ToString.Length, .Item("fieldcaption"))
                                objDataOperation.AddParameter("@isparent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isparent"))
                                objDataOperation.AddParameter("@isinformational", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isinformational"))
                                objDataOperation.AddParameter("@isused", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isused"))
                                objDataOperation.AddParameter("@linkedto", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("linkedto"))

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_field_master", "fieldunkid", .Item("fieldunkid"), , intUserUnkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "InsertUpdateField_Caption", mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
    End Function

    Private Sub Get_Caption()
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Try
            StrQ = "SELECT " & _
                   "  hrassess_field_master.fieldunkid " & _
                   ", hrassess_field_master.fieldcaption " & _
                   ", ROW_NUMBER()OVER(ORDER BY hrassess_field_master.fieldunkid ASC) AS ExOrder " & _
                   ", linkedto " & _
                   "FROM hrassess_field_master "

            objDataOperation.AddParameter("@Date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(DateTime.Today))

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    Select Case CInt(dRow.Item("ExOrder"))
                        Case 1
                            mstrField1_Caption = dRow.Item("fieldcaption") : mintField1Unkid = dRow.Item("fieldunkid")
                        Case 2
                            mstrField2_Caption = dRow.Item("fieldcaption") : mintField2Unkid = dRow.Item("fieldunkid")
                        Case 3
                            mstrField3_Caption = dRow.Item("fieldcaption") : mintField3Unkid = dRow.Item("fieldunkid")
                        Case 4
                            mstrField4_Caption = dRow.Item("fieldcaption") : mintField4Unkid = dRow.Item("fieldunkid")
                        Case 5
                            mstrField5_Caption = dRow.Item("fieldcaption") : mintField5Unkid = dRow.Item("fieldunkid")
                        Case 6
                            mstrField6_Caption = dRow.Item("fieldcaption") : mintField6Unkid = dRow.Item("fieldunkid")
                        Case 7
                            mstrField7_Caption = dRow.Item("fieldcaption") : mintField7Unkid = dRow.Item("fieldunkid")
                        Case 8
                            mstrField8_Caption = dRow.Item("fieldcaption") : mintField8Unkid = dRow.Item("fieldunkid")
                    End Select
                    If CInt(dRow.Item("linkedto")) > 0 Then
                        mintLinkedFieldId = dRow.Item("linkedto")
                        Dim dtmp() As DataRow = dsList.Tables(0).Select("fieldunkid = '" & dRow.Item("linkedto") & "'")
                        If dtmp.Length > 0 Then
                            mstrLinked_Caption = dtmp(0).Item("fieldcaption")
                            mintLinkedFieldOrder = dtmp(0).Item("ExOrder")
                        End If
                    End If
                Next
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Caption", mstrModuleName)
        Finally
            exForce = Nothing : dsList.Dispose() : objDataOperation = Nothing
        End Try
    End Sub

    Public Function Get_Field_ExOrder(ByVal iFieldId As Integer, Optional ByVal blnExcludeInformational As Boolean = False) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim iExOrder As Integer = -1
        Dim objDataOperation As New clsDataOperation
        Try
            StrQ = "SELECT " & _
                   "    fieldunkid ,fieldcaption,linkedto,ROW_NUMBER()OVER(ORDER BY fieldunkid ASC) AS ExOrder,isinformational " & _
                   "FROM hrassess_field_master WITH (NOLOCK) " & _
                   "/*WHERE isused = 1 */ WHERE 1 = 1 "

            If blnExcludeInformational = True Then
                StrQ &= " AND isinformational = 0 "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Dim iExRow() As DataRow = dsList.Tables(0).Select("fieldunkid = '" & iFieldId & "'")
                If iExRow.Length > 0 Then
                    iExOrder = iExRow(0).Item("ExOrder")
                End If
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Field_Mapping", mstrModuleName)
        Finally
            exForce = Nothing : dsList.Dispose() : objDataOperation = Nothing
        End Try
        Return iExOrder
    End Function

    Public Function Get_Field_Mapping(ByVal iList As String, Optional ByVal iFieldId As Integer = 0, Optional ByVal blnExcludeInformational As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Try
            StrQ = "SELECT " & _
                   "    fieldunkid ,fieldcaption,linkedto,ROW_NUMBER()OVER(ORDER BY fieldunkid ASC) AS ExOrder,isinformational " & _
                   "FROM hrassess_field_master " & _
                   "WHERE isused = 1 "

            If blnExcludeInformational = True Then
                StrQ &= " AND isinformational = 0 "
            End If

            If iFieldId > 0 Then
                StrQ &= " AND fieldunkid = '" & iFieldId & "' "
            End If

            If iList = "" Then iList = "List"

            dsList = objDataOperation.ExecQuery(StrQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Field_Mapping", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing : dsList.Dispose() : objDataOperation = Nothing
        End Try
    End Function

    Public Function UpdateLinkedField(ByVal iOldFieldUnkid As Integer, ByVal iNewFieldUnkid As Integer, ByVal intUserUnkid As Integer) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try
            StrQ = "UPDATE hrassess_field_master SET linkedto = @nfieldunkid " & _
                   "WHERE linkedto = @ofieldunkid AND isinformational = 1 "

            objDataOperation.AddParameter("@ofieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iOldFieldUnkid)
            objDataOperation.AddParameter("@nfieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iNewFieldUnkid)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_field_master", "fieldunkid", iNewFieldUnkid, , intUserUnkid) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "UpdateLinkedField", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Get_FieldUnkid(ByVal iFieldCaption As String) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Try
            StrQ = "SELECT " & _
                   "    fieldunkid ,fieldcaption,linkedto " & _
                   "FROM hrassess_field_master " & _
                   "WHERE fieldcaption = '" & iFieldCaption & "' "


            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows(0).Item("fieldunkid")

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_FieldUnkid", mstrModuleName)
        Finally
            exForce = Nothing : dsList.Dispose() : objDataOperation = Nothing
        End Try
    End Function

    Public Function isExist(ByVal iCaption As String, ByVal iFieldId As Integer) As Boolean
        Try
            Dim StrQ As String = String.Empty
            Dim exForce As Exception
            Dim dsList As New DataSet
            Dim objDataOperation As New clsDataOperation
            Dim iFlag As Boolean = False
            Try
                StrQ = "SELECT " & _
                       " fieldunkid ,fieldcaption,linkedto " & _
                       "FROM hrassess_field_master " & _
                       "WHERE fieldcaption = '" & iCaption & "' AND isused = 1 "
                If iFieldId > 0 Then
                    StrQ &= " AND fieldunkid <> '" & iFieldId & "' "
                End If

                dsList = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then
                    iFlag = True
                End If

                Return iFlag

            Catch ex As Exception
                DisplayError.Show("-1", ex.Message, "Get_FieldUnkid", mstrModuleName)
            Finally
                exForce = Nothing : dsList.Dispose() : objDataOperation = Nothing
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "isExist", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function IsPresent(ByVal iCheckType As enFieldCheckType) As Boolean
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim blnFlag As Boolean = False
        Dim iRowCnt As Integer = -1
        Try
            Using objDo As New clsDataOperation
                Select Case iCheckType
                    Case enFieldCheckType.FIELD_DATA
                        StrQ = "SELECT fieldunkid FROM hrassess_field_master WHERE isused  = 1"
                    Case enFieldCheckType.FIELD_LINK
                        StrQ = "SELECT linkedto FROM hrassess_field_master WHERE linkedto > 0 "
                    Case Else
                        StrQ = "SELECT fieldunkid FROM hrassess_field_master WHERE isused  = 1"
                End Select

                iRowCnt = objDo.RecordCount(StrQ)

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                    Throw exForce
                End If

                If iRowCnt > 0 Then blnFlag = True
            End Using
            Return blnFlag
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsDataPresent", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function Allow_Commit(ByVal eMode As enCommitMode) As String
        Dim StrQ As String = String.Empty
        Dim iMsg As String = String.Empty
        Dim dsList As New DataSet
        Dim iCnt As Integer = -1
        Dim mblnIsMissing As Boolean = False
        Try
            Using objDataOpr As New clsDataOperation
                StrQ = "SELECT ROW_NUMBER()OVER (ORDER BY fieldunkid) iExOrd FROM hrassess_field_master WHERE isused = 1 AND isinformational = 0 "
                dsList = objDataOpr.ExecQuery(StrQ, "List")
                If objDataOpr.ErrorMessage <> "" Then
                    Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                End If
                If dsList.Tables(0).Rows.Count > 0 Then
                    iMsg = Language.getMessage(mstrModuleName, 1, "Sorry, In Assessment Caption Setting you have set to use") & " " & dsList.Tables(0).Rows.Count & " " & _
                           Language.getMessage(mstrModuleName, 2, "fields.") & vbCrLf & _
                           Language.getMessage(mstrModuleName, 3, "Please provide all data for the used fields in order to do commit Operation.")
                End If
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    iCnt = -1
                    Select Case eMode
                        Case enCommitMode.COY_COMMIT
                            Select Case CInt(dRow.Item("iExOrd"))
                                Case 1
                                    StrQ = "SELECT 1 FROM hrassess_coyfield1_master where isvoid = 0 "
                                    iCnt = objDataOpr.RecordCount(StrQ)
                                    If objDataOpr.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                                    End If
                                    If iCnt <= 0 Then
                                        mblnIsMissing = True
                                        Exit Try
                                    End If
                                Case 2
                                    StrQ = "SELECT 1 FROM hrassess_coyfield2_master where isvoid = 0 "
                                    iCnt = objDataOpr.RecordCount(StrQ)
                                    If objDataOpr.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                                    End If
                                    If iCnt <= 0 Then
                                        mblnIsMissing = True
                                        Exit Try
                                    End If
                                Case 3
                                    StrQ = "SELECT 1 FROM hrassess_coyfield3_master where isvoid = 0 "
                                    iCnt = objDataOpr.RecordCount(StrQ)
                                    If objDataOpr.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                                    End If
                                    If iCnt <= 0 Then
                                        mblnIsMissing = True
                                        Exit Try
                                    End If
                                Case 4
                                    StrQ = "SELECT 1 FROM hrassess_coyfield4_master where isvoid = 0 "
                                    iCnt = objDataOpr.RecordCount(StrQ)
                                    If objDataOpr.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                                    End If
                                    If iCnt <= 0 Then
                                        mblnIsMissing = True
                                        Exit Try
                                    End If
                                Case 5
                                    StrQ = "SELECT 1 FROM hrassess_coyfield5_master where isvoid = 0 "
                                    iCnt = objDataOpr.RecordCount(StrQ)
                                    If objDataOpr.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                                    End If
                                    If iCnt <= 0 Then
                                        mblnIsMissing = True
                                        Exit Try
                                    End If
                            End Select
                        Case enCommitMode.OWR_COMMIT
                            Select Case CInt(dRow.Item("iExOrd"))
                                Case 1
                                    StrQ = "SELECT 1 FROM hrassess_owrfield1_master where isvoid = 0 UNION "
                                    iCnt = objDataOpr.RecordCount(StrQ)
                                    If objDataOpr.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                                    End If
                                    If iCnt <= 0 Then
                                        mblnIsMissing = True
                                        Exit Try
                                    End If
                                Case 2
                                    StrQ = "SELECT 1 FROM hrassess_owrfield2_master where isvoid = 0 "
                                    iCnt = objDataOpr.RecordCount(StrQ)
                                    If objDataOpr.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                                    End If
                                    If iCnt <= 0 Then
                                        mblnIsMissing = True
                                        Exit Try
                                    End If
                                Case 3
                                    StrQ = "SELECT 1 FROM hrassess_owrfield3_master where isvoid = 0 "
                                    iCnt = objDataOpr.RecordCount(StrQ)
                                    If objDataOpr.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                                    End If
                                    If iCnt <= 0 Then
                                        mblnIsMissing = True
                                        Exit Try
                                    End If
                                Case 4
                                    StrQ = "SELECT 1 FROM hrassess_owrfield4_master where isvoid = 0 "
                                    iCnt = objDataOpr.RecordCount(StrQ)
                                    If objDataOpr.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                                    End If
                                    If iCnt <= 0 Then
                                        mblnIsMissing = True
                                        Exit Try
                                    End If
                                Case 5
                                    StrQ = "SELECT 1 FROM hrassess_owrfield5_master where isvoid = 0 "
                                    iCnt = objDataOpr.RecordCount(StrQ)
                                    If objDataOpr.ErrorMessage <> "" Then
                                        Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                                    End If
                                    If iCnt <= 0 Then
                                        mblnIsMissing = True
                                        Exit Try
                                    End If
                            End Select
                    End Select
                Next
            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Allow_Commit", mstrModuleName)
        Finally
        End Try
        If mblnIsMissing = False Then
            iMsg = ""
        End If
        Return iMsg
    End Function

    Public Function GetFieldsForViewSetting() As DataTable
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim sDataBase As String = String.Empty
        Dim dtFinal As DataTable = Nothing
        Try
            Using objDataOpr As New clsDataOperation
                StrQ = "SELECT " & _
                       "Id ,Name ,SP ,SE ,RP ,RE, PW, EW ,ROW_NUMBER() OVER (ORDER BY id) - 1 AS iDx " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT fieldunkid AS Id ,fieldcaption AS Name ,CAST(0 AS BIT) AS SP ,CAST(0 AS BIT) AS SE ,CAST(0 AS bit) AS RP ,CAST(0 AS bit) AS RE ,100 AS PW ,100 AS EW " & _
                       "        FROM hrassess_field_master WHERE isused = 1 " & _
                       "    UNION SELECT '" & enOtherInfoField.ST_DATE & "' AS Id ,@SDate AS Name ,CAST(0 AS BIT) AS SP ,CAST(0 AS BIT) AS SE ,CAST(0 AS bit) AS RP ,CAST(0 AS bit) AS RE ,100 AS PW ,100 AS EW " & _
                       "    UNION SELECT '" & enOtherInfoField.ED_DATE & "' AS Id ,@EDate AS Name ,CAST(0 AS BIT) AS SP ,CAST(0 AS BIT) AS SE ,CAST(0 AS bit) AS RP ,CAST(0 AS bit) AS RE ,100 AS PW ,100 AS EW " & _
                       "    UNION SELECT '" & enOtherInfoField.STATUS & "' AS Id ,@Status AS Name ,CAST(0 AS BIT) AS SP ,CAST(0 AS BIT) AS SE ,CAST(0 AS bit) AS RP ,CAST(0 AS bit) AS RE ,100 AS PW ,100 AS EW " & _
                       "    UNION SELECT '" & enOtherInfoField.PCT_COMPLETE & "' AS Id ,@Completed AS Name ,CAST(0 AS BIT) AS SP ,CAST(0 AS BIT) AS SE ,CAST(0 AS bit) AS RP ,CAST(0 AS bit) AS RE ,100 AS PW ,100 AS EW " & _
                       "    UNION SELECT '" & enOtherInfoField.WEIGHT & "' AS Id ,@Weight AS Name ,CAST(0 AS BIT) AS SP ,CAST(0 AS BIT) AS SE ,CAST(0 AS bit) AS RP ,CAST(1 AS bit) AS RE ,100 AS PW ,100 AS EW " & _
                       "    UNION SELECT '" & enOtherInfoField.SCORE & "' AS Id ,@Score AS Name ,CAST(0 AS BIT) AS SP ,CAST(0 AS BIT) AS SE ,CAST(1 AS bit) AS RP ,CAST(1 AS bit) AS RE ,100 AS PW ,100 AS EW " & _
                       "    UNION SELECT '" & enOtherInfoField.PERSPECTIVE & "' AS Id ,@Perspective AS Name ,CAST(0 AS BIT) AS SP ,CAST(0 AS BIT) AS SE ,CAST(0 AS bit) AS RP ,CAST(1 AS bit) AS RE ,100 AS PW ,100 AS EW "

                'S.SANDEEP [07 FEB 2015] -- START
                StrQ &= " UNION SELECT '" & enOtherInfoField.EMP_REMARK & "' AS Id ,@EMP_REMARK AS Name ,CAST(0 AS BIT) AS SP ,CAST(1 AS BIT) AS SE ,CAST(1 AS bit) AS RP ,CAST(1 AS bit) AS RE ,0 AS PW ,200 AS EW " & _
                        " UNION SELECT '" & enOtherInfoField.ASR_REMARK & "' AS Id ,@ASR_REMARK AS Name ,CAST(0 AS BIT) AS SP ,CAST(1 AS BIT) AS SE ,CAST(1 AS bit) AS RP ,CAST(1 AS bit) AS RE ,0 AS PW ,200 AS EW " & _
                        " UNION SELECT '" & enOtherInfoField.REV_REMARK & "' AS Id ,@REV_REMARK AS Name ,CAST(0 AS BIT) AS SP ,CAST(1 AS BIT) AS SE ,CAST(1 AS bit) AS RP ,CAST(1 AS bit) AS RE ,0 AS PW ,200 AS EW "
                'S.SANDEEP [07 FEB 2015] -- END

                'S.SANDEEP [11-OCT-2018] -- START
                StrQ &= " UNION SELECT '" & enOtherInfoField.GOAL_TYPE & "' AS Id ,@GOAL_TYPE AS Name ,CAST(0 AS BIT) AS SP ,CAST(0 AS BIT) AS SE ,CAST(0 AS bit) AS RP ,CAST(0 AS bit) AS RE ,100 AS PW ,100 AS EW " & _
                        " UNION SELECT '" & enOtherInfoField.GOAL_VALUE & "' AS Id ,@GOAL_VALUE AS Name ,CAST(0 AS BIT) AS SP ,CAST(0 AS BIT) AS SE ,CAST(0 AS bit) AS RP ,CAST(0 AS bit) AS RE ,100 AS PW ,100 AS EW "
                'S.SANDEEP [11-OCT-2018] -- END

                StrQ &= ") AS Lst "

                objDataOpr.AddParameter("@SDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Start Date"))
                objDataOpr.AddParameter("@EDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "End Date"))
                objDataOpr.AddParameter("@Status", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 6, "Status"))
                objDataOpr.AddParameter("@Completed", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "% Completed"))
                objDataOpr.AddParameter("@Weight", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "Weight"))
                objDataOpr.AddParameter("@Score", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 9, "Score"))
                objDataOpr.AddParameter("@Perspective", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 10, "Perspective"))

                'S.SANDEEP [07 FEB 2015] -- START
                objDataOpr.AddParameter("@EMP_REMARK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 11, "Employee Remark"))
                objDataOpr.AddParameter("@ASR_REMARK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 12, "Assessor Remark"))
                objDataOpr.AddParameter("@REV_REMARK", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 13, "Reviewer Remark"))
                'S.SANDEEP [07 FEB 2015] -- END

                'S.SANDEEP [11-OCT-2018] -- START
                objDataOpr.AddParameter("@GOAL_TYPE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 14, "Goal Type"))
                objDataOpr.AddParameter("@GOAL_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 15, "Goal Value"))
                'S.SANDEEP [11-OCT-2018] -- END

                dsList = objDataOpr.ExecQuery(StrQ, "List")

                If objDataOpr.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                    Throw exForce
                End If

                dtFinal = dsList.Tables(0).Copy
            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetFieldsForConfig", mstrModuleName)
        Finally
        End Try
        Return dtFinal
    End Function

    Public Function GetColumnWidth(ByVal eSType As enColWidthType, ByVal iFieldId As Integer, Optional ByVal xCompanyId As Integer = 0) As Integer
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim iColWidth As Integer = 0
        Dim objDataOpr As New clsDataOperation
        Dim xWidths() As String = Nothing
        Dim xIndex As Integer = -1
        Try
            If xCompanyId <= 0 Then xCompanyId = Company._Object._Companyunkid
            Dim iDefTab As DataTable = Nothing
            iDefTab = GetFieldsForViewSetting()
            If iDefTab.Rows.Count > 0 Then
                Dim xRow() As DataRow = iDefTab.Select("Id = '" & iFieldId & "'")
                If xRow.Length > 0 Then
                    Select Case eSType
                        Case enColWidthType.COL_PLAN
                            StrQ = "SELECT key_value FROM hrmsConfiguration..cfconfiguration WHERE UPPER(key_name) = 'COLWIDTH_INPLANNING' AND companyunkid = '" & xCompanyId & "' "

                            If objDataOpr.ErrorMessage <> "" Then
                                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                            End If

                            dsList = objDataOpr.ExecQuery(StrQ, "List")
                            If dsList.Tables(0).Rows.Count > 0 Then
                                xWidths = dsList.Tables(0).Rows(0).Item(0).ToString.Split("|")
                                Try
                                    iColWidth = CInt(xWidths.GetValue(xRow(0).Item("iDx")))
                                Catch ex As Exception
                                    iColWidth = 100
                                End Try
                            Else
                                iColWidth = CInt(xRow(0).Item("PW"))
                            End If

                        Case enColWidthType.COL_EVAL
                            StrQ = "SELECT key_value FROM hrmsConfiguration..cfconfiguration WHERE UPPER(key_name) = 'COLWIDTH_INEVALUATION' AND companyunkid = '" & xCompanyId & "' "
                            dsList = objDataOpr.ExecQuery(StrQ, "List")

                            If objDataOpr.ErrorMessage <> "" Then
                                Throw New Exception(objDataOpr.ErrorNumber & " : " & objDataOpr.ErrorMessage)
                            End If

                            dsList = objDataOpr.ExecQuery(StrQ, "List")
                            If dsList.Tables(0).Rows.Count > 0 Then
                                xWidths = dsList.Tables(0).Rows(0).Item(0).ToString.Split("|")
                                Try
                                    iColWidth = CInt(xWidths.GetValue(xRow(0).Item("iDx")))
                                Catch ex As Exception
                                    iColWidth = 100
                                End Try
                            Else
                                iColWidth = CInt(xRow(0).Item("PW"))
                            End If
                    End Select
                End If
            End If
            Return iColWidth
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetColumnWidth", mstrModuleName)
        Finally
        End Try
    End Function

    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT 1 FROM hrassess_coyfield1_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_coyfield2_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_coyfield3_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_coyfield4_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_coyfield5_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_owrfield1_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_owrfield2_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_owrfield3_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_owrfield4_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_owrfield5_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_empfield1_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_empfield2_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_empfield3_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_empfield4_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_empfield5_master WHERE fieldunkid = @fieldunkid AND isvoid = 0 UNION " & _
                   "SELECT 1 FROM hrassess_field_mapping WHERE fieldunkid = @fieldunkid AND isactive = 0 "

            objDataOperation.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry, In Assessment Caption Setting you have set to use")
            Language.setMessage(mstrModuleName, 2, "fields.")
            Language.setMessage(mstrModuleName, 3, "Please provide all data for the used fields in order to do commit Operation.")
            Language.setMessage(mstrModuleName, 4, "Start Date")
            Language.setMessage(mstrModuleName, 5, "End Date")
            Language.setMessage(mstrModuleName, 6, "Status")
            Language.setMessage(mstrModuleName, 7, "% Completed")
            Language.setMessage(mstrModuleName, 8, "Weight")
            Language.setMessage(mstrModuleName, 9, "Score")
            Language.setMessage(mstrModuleName, 10, "Perspective")
	    Language.setMessage(mstrModuleName, 11, "Employee Remark")
	    Language.setMessage(mstrModuleName, 12, "Assessor Remark")
	    Language.setMessage(mstrModuleName, 13, "Reviewer Remark")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class