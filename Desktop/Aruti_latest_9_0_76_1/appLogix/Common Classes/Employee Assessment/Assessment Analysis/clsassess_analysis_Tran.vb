﻿'************************************************************************************************************************************
'Class Name : clsassess_analysis_tran.vb
'Purpose    :
'Date       :07/01/2012
'Written By :Sandeep J. Sharma
'Modified   : ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: ENHANCEMENT : ANY CHANGES IN DESKTOP SHOULD ALSO BE IMPLEMENTED IN WEB WITHOUT FAIL.
''' Developer: Sandeep J. Sharma
''' </summary>
Public Class clsassess_analysis_tran
    Private Shared ReadOnly mstrModuleName As String = "clsassess_analysis_tran"
    Dim mstrMessage As String = ""
    'S.SANDEEP [ 27 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Dim objDataOperation As New clsDataOperation
    'S.SANDEEP [ 27 APRIL 2012 ] -- END

#Region " Private Variables "

    Private mintAnalysisTranId As Integer = -1
    Private mintAnalysisUnkid As Integer = -1
    Private mdtTran As DataTable
    Private mdicGroupTotal As New Dictionary(Of Integer, Decimal)
    Private mdicItemWeight As New Dictionary(Of Integer, Decimal)
    Private mintGrpCounter As Integer = 0
    'S.SANDEEP [ 22 OCT 2013 ] -- START
    'ENHANCEMENT : ENHANCEMENT
    Private mblnConsiderWeightAsNumber As Boolean = False
    'S.SANDEEP [ 22 OCT 2013 ] -- END

#End Region

#Region " Properties "

    Public Property _AnalysisUnkid() As Integer
        Get
            Return mintAnalysisUnkid
        End Get
        Set(ByVal value As Integer)
            mintAnalysisUnkid = value
            Call GetAnalysisTran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public Property _GrpWiseTotal() As Dictionary(Of Integer, Decimal)
        Get
            Return mdicGroupTotal
        End Get
        Set(ByVal value As Dictionary(Of Integer, Decimal))
            mdicGroupTotal = value
        End Set
    End Property

    Public Property _ItemWeight() As Dictionary(Of Integer, Decimal)
        Get
            Return mdicItemWeight
        End Get
        Set(ByVal value As Dictionary(Of Integer, Decimal))
            mdicItemWeight = value
        End Set
    End Property

    Public ReadOnly Property _Grp_Counter() As Integer
        Get
            Return mintGrpCounter
        End Get
    End Property

    'S.SANDEEP [ 27 APRIL 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property
    'S.SANDEEP [ 27 APRIL 2012 ] -- END

    'S.SANDEEP [ 22 OCT 2013 ] -- START
    'ENHANCEMENT : ENHANCEMENT
    Public Property _ConsiderWeightAsNumber() As Boolean
        Get
            Return mblnConsiderWeightAsNumber
        End Get
        Set(ByVal value As Boolean)
            mblnConsiderWeightAsNumber = value
        End Set
    End Property
    'S.SANDEEP [ 22 OCT 2013 ] -- END

#End Region

#Region " Contructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("AnalysisTran")

            mdtTran.Columns.Add("analysistranunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("analysisunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("assessitemunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("assess_subitemunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("resultunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("remark", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("assessitem", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("assess_subitem", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("result", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("group_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private/Public Methods "

    Private Sub GetAnalysisTran()
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'Dim objDataOperation As New clsDataOperation
            If objDataOperation Is Nothing Then
                objDataOperation = New clsDataOperation
            End If
            'S.SANDEEP [ 27 APRIL 2012 ] -- END

            'S.SANDEEP [ 22 OCT 2013 ] -- START
            'ENHANCEMENT : ENHANCEMENT
            'StrQ = "SELECT " & _
            '             " hrassess_analysis_tran.analysistranunkid " & _
            '             ",hrassess_analysis_tran.analysisunkid " & _
            '             ",hrassess_analysis_tran.assessitemunkid " & _
            '             ",hrassess_analysis_tran.assess_subitemunkid " & _
            '             ",hrassess_analysis_tran.resultunkid " & _
            '             ",hrassess_analysis_tran.remark " & _
            '             ",'' AS AUD " & _
            '             ",ISNULL(hrassess_item_master.name,'') AS assessitem " & _
            '             ",CASE WHEN ISNULL(hrassess_subitem_master.name, '')<>'' THEN ISNULL(hrassess_subitem_master.name, '') ELSE ISNULL(hrassess_item_master.name, '') END AS assess_subitem " & _
            '             ",hrresult_master.resultname AS result " & _
            '             ",ISNULL(hrassess_item_master.weight,0) AS weight " & _
            '             ",'' AS group_name " & _
            '             ",hrassess_analysis_tran.isvoid " & _
            '             ",hrassess_analysis_tran.voiduserunkid " & _
            '             ",hrassess_analysis_tran.voiddatetime " & _
            '             ",hrassess_analysis_tran.voidreason " & _
            '        "FROM hrassess_analysis_tran " & _
            '             "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
            '             "LEFT JOIN hrassess_subitem_master ON hrassess_analysis_tran.assess_subitemunkid = hrassess_subitem_master.assess_subitemunkid " & _
            '             "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
            '             "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
            '        "WHERE hrassess_analysis_tran.isvoid = 0 " & _
            '             "AND hrassess_analysis_tran.analysisunkid = @AnalysisUnkid " & _
            '             "AND hrassess_analysis_tran.isvoid = 0 " & _
            '             "ORDER BY hrassess_analysis_tran.assessitemunkid "

            StrQ = "SELECT " & _
                         " hrassess_analysis_tran.analysistranunkid " & _
                         ",hrassess_analysis_tran.analysisunkid " & _
                         ",hrassess_analysis_tran.assessitemunkid " & _
                         ",hrassess_analysis_tran.assess_subitemunkid " & _
                         ",hrassess_analysis_tran.resultunkid " & _
                         ",hrassess_analysis_tran.remark " & _
                         ",'' AS AUD " & _
                         ",ISNULL(hrassess_item_master.name,'') AS assessitem " & _
                        ",CASE WHEN ISNULL(hrassess_subitem_master.name, '') <>'' THEN ISNULL(hrassess_subitem_master.name, '') ELSE ISNULL(hrassess_item_master.name, '') END AS assess_subitem "
            If mblnConsiderWeightAsNumber = True Then
                StrQ &= ",(hrresult_master.resultname * ISNULL(hrassess_item_master.weight,1)) AS result "
            Else
                StrQ &= ",hrresult_master.resultname AS result "
            End If
            StrQ &= ",ISNULL(hrassess_item_master.weight,0) AS weight " & _
                         ",'' AS group_name " & _
                         ",hrassess_analysis_tran.isvoid " & _
                         ",hrassess_analysis_tran.voiduserunkid " & _
                         ",hrassess_analysis_tran.voiddatetime " & _
                         ",hrassess_analysis_tran.voidreason " & _
                    "FROM hrassess_analysis_tran " & _
                         "JOIN hrresult_master ON hrassess_analysis_tran.resultunkid = hrresult_master.resultunkid " & _
                         "LEFT JOIN hrassess_subitem_master ON hrassess_analysis_tran.assess_subitemunkid = hrassess_subitem_master.assess_subitemunkid " & _
                         "JOIN hrassess_item_master ON hrassess_analysis_tran.assessitemunkid = hrassess_item_master.assessitemunkid " & _
                         "JOIN hrassess_analysis_master ON hrassess_analysis_tran.analysisunkid = hrassess_analysis_master.analysisunkid " & _
                    "WHERE hrassess_analysis_tran.isvoid = 0 " & _
                         "AND hrassess_analysis_tran.analysisunkid = @AnalysisUnkid " & _
                         "AND hrassess_analysis_tran.isvoid = 0 " & _
                         "ORDER BY hrassess_analysis_tran.assessitemunkid "
            'S.SANDEEP [ 22 OCT 2013 ] -- END





            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.ClearParameters()
            'S.SANDEEP [ 27 APRIL 2012 ] -- END
            objDataOperation.AddParameter("@AnalysisUnkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear() : mdicGroupTotal = New Dictionary(Of Integer, Decimal) : mdicItemWeight = New Dictionary(Of Integer, Decimal)
            Dim iCnt As Integer = 0
            Dim strGrpItem As String = String.Empty
            For Each dtRow As DataRow In dsList.Tables("List").Rows
                If strGrpItem <> dtRow.Item("assessitem") Then
                    iCnt += 1
                    dtRow.Item("group_name") = iCnt.ToString & ".0" & Space(5) & dtRow.Item("assessitem").ToString & Space(3) & "[" & dtRow.Item("weight").ToString & "]"
                    strGrpItem = dtRow.Item("assessitem").ToString
                    dtRow.AcceptChanges()
                    mintGrpCounter = iCnt
                Else
                    dtRow.Item("group_name") = iCnt.ToString & ".0" & Space(5) & dtRow.Item("assessitem").ToString & Space(3) & "[" & dtRow.Item("weight").ToString & "]"
                End If

                If mdicGroupTotal.ContainsKey(dtRow.Item("assessitemunkid")) = False Then
                    If IsNumeric(dtRow.Item("result")) Then
                        'S.SANDEEP [ 22 OCT 2013 ] -- START
                        'ENHANCEMENT : ENHANCEMENT
                        'mdicGroupTotal.Add(dtRow.Item("assessitemunkid"), dtRow.Item("result"))
                        mdicGroupTotal.Add(dtRow.Item("assessitemunkid"), Format(CDec(dtRow.Item("result")), "###########.#0"))
                        'S.SANDEEP [ 22 OCT 2013 ] -- END
                    End If
                Else
                    If IsNumeric(dtRow.Item("result")) Then
                        'S.SANDEEP [ 22 OCT 2013 ] -- START
                        'ENHANCEMENT : ENHANCEMENT
                        'mdicGroupTotal(dtRow.Item("assessitemunkid")) = mdicGroupTotal(dtRow.Item("assessitemunkid")) + dtRow.Item("result")
                        mdicGroupTotal(dtRow.Item("assessitemunkid")) = mdicGroupTotal(dtRow.Item("assessitemunkid")) + Format(CDec(dtRow.Item("result")), "###########.#0")
                        'S.SANDEEP [ 22 OCT 2013 ] -- END
                    End If
                End If

                If mdicItemWeight.ContainsKey(dtRow.Item("assessitemunkid")) = False Then
                    mdicItemWeight.Add(dtRow.Item("assessitemunkid"), dtRow.Item("weight"))
                End If

                'S.SANDEEP [ 22 OCT 2013 ] -- START
                'ENHANCEMENT : ENHANCEMENT
                dtRow.Item("result") = Format(CDec(dtRow.Item("result")), "###########.#0")
                'S.SANDEEP [ 22 OCT 2013 ] -- END


                mdtTran.ImportRow(dtRow)

            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAnalysisTran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_AnalysisTran(Optional ByVal intUserUnkid As Integer = 0) As Boolean 'S.SANDEEP [ 27 APRIL 2012 ] -- START-- END
        'Public Function InsertUpdateDelete_AnalysisTran() As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        'S.SANDEEP [ 27 APRIL 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES
        'Dim objDataOp As New clsDataOperation
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        'S.SANDEEP [ 27 APRIL 2012 ] -- END
        Try
            'S.SANDEEP [ 27 APRIL 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'objDataOp.BindTransaction()
            'S.SANDEEP [ 27 APRIL 2012 ] -- END
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrassess_analysis_tran ( " & _
                                            "  analysisunkid " & _
                                            ", assessitemunkid " & _
                                            ", assess_subitemunkid " & _
                                            ", resultunkid " & _
                                            ", remark " & _
                                            ", isvoid " & _
                                            ", voiduserunkid " & _
                                            ", voiddatetime " & _
                                            ", voidreason" & _
                                          ") VALUES (" & _
                                            "  @analysisunkid " & _
                                            ", @assessitemunkid " & _
                                            ", @assess_subitemunkid " & _
                                            ", @resultunkid " & _
                                            ", @remark " & _
                                            ", @isvoid " & _
                                            ", @voiduserunkid " & _
                                            ", @voiddatetime " & _
                                            ", @voidreason" & _
                                          "); SELECT @@identity"

                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAnalysisUnkid.ToString)
                                objDataOperation.AddParameter("@assessitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assessitemunkid").ToString)
                                objDataOperation.AddParameter("@assess_subitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assess_subitemunkid").ToString)
                                objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, "")

                                Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                                mintAnalysisTranId = dsList.Tables(0).Rows(0).Item(0)

                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'If .Item("analysisunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOp, "hrassess_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrassess_analysis_tran", "analysistranunkid", mintAnalysisTranId, 2, 1) = False Then
                                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'Else
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOp, "hrassess_analysis_master", "analysisunkid", mintAnalysisUnkid, "hrassess_analysis_tran", "analysistranunkid", mintAnalysisTranId, 1, 1) = False Then
                                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If

                                If .Item("analysisunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrassess_analysis_tran", "analysistranunkid", mintAnalysisTranId, 2, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_analysis_master", "analysisunkid", mintAnalysisUnkid, "hrassess_analysis_tran", "analysistranunkid", mintAnalysisTranId, 1, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                            Case "U"
                                StrQ = "UPDATE hrassess_analysis_tran SET " & _
                                        "  analysisunkid = @analysisunkid" & _
                                        ", assessitemunkid = @assessitemunkid" & _
                                        ", assess_subitemunkid = @assess_subitemunkid" & _
                                        ", resultunkid = @resultunkid" & _
                                        ", remark = @remark" & _
                                        ", isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                      "WHERE analysistranunkid = @analysistranunkid "

                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'objDataOp.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                'objDataOp.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysisunkid").ToString)
                                'objDataOp.AddParameter("@assessitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assessitemunkid").ToString)
                                'objDataOp.AddParameter("@assess_subitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assess_subitemunkid").ToString)
                                'objDataOp.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                'objDataOp.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                'objDataOp.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                'objDataOp.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                'If IsDBNull(.Item("voiddatetime")) Then
                                '    objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'Else
                                '    objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                'End If
                                'objDataOp.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)


                                'Call objDataOp.ExecNonQuery(StrQ)

                                'If objDataOp.ErrorMessage <> "" Then
                                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '    Throw exForce
                                'End If

                                'If clsCommonATLog.Insert_TranAtLog(objDataOp, "hrassess_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrassess_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 2) = False Then
                                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '    Throw exForce
                                'End If

                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                objDataOperation.AddParameter("@analysisunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysisunkid").ToString)
                                objDataOperation.AddParameter("@assessitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assessitemunkid").ToString)
                                objDataOperation.AddParameter("@assess_subitemunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("assess_subitemunkid").ToString)
                                objDataOperation.AddParameter("@resultunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("resultunkid").ToString)
                                objDataOperation.AddParameter("@remark", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("remark").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)


                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrassess_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 2, , intUserUnkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END


                            Case "D"

                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'If .Item("analysistranunkid") > 0 Then
                                '    If clsCommonATLog.Insert_TranAtLog(objDataOp, "hrassess_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrassess_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 3) = False Then
                                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '        Throw exForce
                                '    End If
                                'End If

                                If .Item("analysistranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_analysis_master", "analysisunkid", .Item("analysisunkid"), "hrassess_analysis_tran", "analysistranunkid", .Item("analysistranunkid"), 2, 3, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                                StrQ = "UPDATE hrassess_analysis_tran SET " & _
                                        "  isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                      "WHERE analysistranunkid = @analysistranunkid "

                                'S.SANDEEP [ 27 APRIL 2012 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'objDataOp.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                'objDataOp.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                'objDataOp.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                'If IsDBNull(.Item("voiddatetime")) Then
                                '    objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                'Else
                                '    objDataOp.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                'End If
                                'objDataOp.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)
                                'Call objDataOp.ExecNonQuery(StrQ)

                                'If objDataOp.ErrorMessage <> "" Then
                                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                                '    Throw exForce
                                'End If

                                objDataOperation.AddParameter("@analysistranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("analysistranunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid").ToString)
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If IsDBNull(.Item("voiddatetime")) Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If
                                'S.SANDEEP [ 27 APRIL 2012 ] -- END

                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_AnalysisTran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class