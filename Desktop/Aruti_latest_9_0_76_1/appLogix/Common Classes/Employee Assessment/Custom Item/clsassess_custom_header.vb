﻿'************************************************************************************************************************************
'Class Name : clsassess_custom_header.vb
'Purpose    :
'Date       :04-Jul-2014
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_custom_header
    Private Shared ReadOnly mstrModuleName As String = "clsassess_custom_header"
    Dim mstrMessage As String = ""

#Region " Private variables "

    Private mintCustomheaderunkid As Integer = 0
    Private mstrName As String = String.Empty
    Private mstrName1 As String = String.Empty
    Private mstrName2 As String = String.Empty
    Private mblnIs_Allow_Ess As Boolean = False
    Private mblnIs_Allow_Mss As Boolean = False
    Private mblnIs_Allow_Multiple As Boolean = False
    Private mblnIsactive As Boolean = True

    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    Private mblnIsInclude_Planning As Boolean = False
    Private mblnIsMatch_With_Competency As Boolean = False
    Private mblnIsAllowAddPastPeriod As Boolean = False
    Private mintMappingPeriodUnkId As Integer = 0
    Private mdecMin_Score As Decimal = 0
    Private mintScaleUnkId As Integer
    'Shani (26-Sep-2016) -- End


#End Region

#Region " Properties "

    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set customheaderunkid
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Customheaderunkid() As Integer
        Get
            Return mintCustomheaderunkid
        End Get
        Set(ByVal value As Integer)
            mintCustomheaderunkid = value
            Call GetData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name() As String
        Get
            Return mstrName
        End Get
        Set(ByVal value As String)
            mstrName = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name1
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name1() As String
        Get
            Return mstrName1
        End Get
        Set(ByVal value As String)
            mstrName1 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set name2
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Name2() As String
        Get
            Return mstrName2
        End Get
        Set(ByVal value As String)
            mstrName2 = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set is_allow_ess
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Is_Allow_Ess() As Boolean
        Get
            Return mblnIs_Allow_Ess
        End Get
        Set(ByVal value As Boolean)
            mblnIs_Allow_Ess = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set is_allow_mss
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Is_Allow_Mss() As Boolean
        Get
            Return mblnIs_Allow_Mss
        End Get
        Set(ByVal value As Boolean)
            mblnIs_Allow_Mss = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set is_allow_multiple
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Is_Allow_Multiple() As Boolean
        Get
            Return mblnIs_Allow_Multiple
        End Get
        Set(ByVal value As Boolean)
            mblnIs_Allow_Multiple = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = value
        End Set
    End Property


    'Shani (26-Sep-2016) -- Start
    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)

    Public Property _IsInclude_Planning() As Boolean
        Get
            Return mblnIsInclude_Planning
        End Get
        Set(ByVal value As Boolean)
            mblnIsInclude_Planning = value
        End Set
    End Property

    Public Property _IsMatch_With_Competency() As Boolean
        Get
            Return mblnIsMatch_With_Competency
        End Get
        Set(ByVal value As Boolean)
            mblnIsMatch_With_Competency = value
        End Set
    End Property

    Public Property _IsAllowAddPastPeriod() As Boolean
        Get
            Return mblnIsAllowAddPastPeriod
        End Get
        Set(ByVal value As Boolean)
            mblnIsAllowAddPastPeriod = value
        End Set
    End Property

    Public Property _Min_Score() As Decimal
        Get
            Return mdecMin_Score
        End Get
        Set(ByVal value As Decimal)
            mdecMin_Score = value
        End Set
    End Property

    Public Property _ScaleUnkId() As Integer
        Get
            Return mintScaleUnkId
        End Get
        Set(ByVal value As Integer)
            mintScaleUnkId = value
        End Set
    End Property

    Public Property _MappedPeriodUnkid() As Integer
        Get
            Return mintMappingPeriodUnkId
        End Get
        Set(ByVal value As Integer)
            mintMappingPeriodUnkId = value
        End Set
    End Property

    'Shani (26-Sep-2016) -- End


#End Region

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'strQ = "SELECT " & _
            '       "  customheaderunkid " & _
            '       ", name " & _
            '       ", name1 " & _
            '       ", name2 " & _
            '       ", is_allow_ess " & _
            '       ", is_allow_mss " & _
            '       ", is_allow_multiple " & _
            '       ", isactive " & _
            '       "FROM hrassess_custom_headers " & _
            '       "WHERE customheaderunkid = @customheaderunkid "

            strQ = "SELECT " & _
                   "  customheaderunkid " & _
                   ", name " & _
                   ", name1 " & _
                   ", name2 " & _
                   ", is_allow_ess " & _
                   ", is_allow_mss " & _
                   ", is_allow_multiple " & _
                   ", isactive " & _
                   "    , isinclude_planning " & _
                   "    , ismatch_with_competency " & _
                   "    , isallowaddpastperiod " & _
                   "    , mappedperiodunkid " & _
                   "    , min_score " & _
                   "    , scaletranunkid " & _
                   "FROM hrassess_custom_headers " & _
                   "WHERE customheaderunkid = @customheaderunkid "

            'Shani (26-Sep-2016) -- End

            objDataOperation.AddParameter("@customheaderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCustomheaderunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintCustomheaderunkid = CInt(dtRow.Item("customheaderunkid"))
                mstrName = dtRow.Item("name").ToString
                mstrName1 = dtRow.Item("name1").ToString
                mstrName2 = dtRow.Item("name2").ToString
                mblnIs_Allow_Ess = CBool(dtRow.Item("is_allow_ess"))
                mblnIs_Allow_Mss = CBool(dtRow.Item("is_allow_mss"))
                mblnIs_Allow_Multiple = CBool(dtRow.Item("is_allow_multiple"))
                mblnIsactive = CBool(dtRow.Item("isactive"))
                'Shani (26-Sep-2016) -- Start
                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                mblnIsInclude_Planning = CBool(dtRow.Item("isinclude_planning"))
                mblnIsMatch_With_Competency = CBool(dtRow.Item("ismatch_with_competency"))
                mblnIsAllowAddPastPeriod = CBool(dtRow.Item("isallowaddpastperiod"))
                mintMappingPeriodUnkId = CInt(dtRow.Item("mappedperiodunkid"))
                mdecMin_Score = CInt(dtRow.Item("min_score"))
                mintScaleUnkId = CInt(dtRow.Item("scaletranunkid"))
                'Shani (26-Sep-2016) -- End
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)



            'strQ = "SELECT " & _
            '       "  hrassess_custom_headers.customheaderunkid " & _
            '       ", hrassess_custom_headers.name " & _
            '       ", hrassess_custom_headers.name1 " & _
            '       ", hrassess_custom_headers.name2 " & _
            '       ", hrassess_custom_headers.is_allow_ess " & _
            '       ", hrassess_custom_headers.is_allow_mss " & _
            '       ", hrassess_custom_headers.is_allow_multiple " & _
            '       ", hrassess_custom_headers.isactive " & _
            '       ", CASE WHEN hrassess_custom_headers.is_allow_ess = 1 THEN @Y ELSE @N END AS ess " & _
            '       ", CASE WHEN hrassess_custom_headers.is_allow_mss = 1 THEN @Y ELSE @N END AS mss " & _
            '       ", CASE WHEN hrassess_custom_headers.is_allow_multiple = 1 THEN @Y ELSE @N END AS multiple " & _
            '       "FROM hrassess_custom_headers " & _
            '       "WHERE hrassess_custom_headers.isactive = 1 "

            strQ = "SELECT " & _
                   "  hrassess_custom_headers.customheaderunkid " & _
                   ", hrassess_custom_headers.name " & _
                   ", hrassess_custom_headers.name1 " & _
                   ", hrassess_custom_headers.name2 " & _
                   ", hrassess_custom_headers.is_allow_ess " & _
                   ", hrassess_custom_headers.is_allow_mss " & _
                   ", hrassess_custom_headers.is_allow_multiple " & _
                   ", hrassess_custom_headers.isactive " & _
                   ", CASE WHEN hrassess_custom_headers.is_allow_ess = 1 THEN @Y ELSE @N END AS ess " & _
                   ", CASE WHEN hrassess_custom_headers.is_allow_mss = 1 THEN @Y ELSE @N END AS mss " & _
                   ", CASE WHEN hrassess_custom_headers.is_allow_multiple = 1 THEN @Y ELSE @N END AS multiple " & _
                   "    , hrassess_custom_headers.isinclude_planning " & _
                   "    , hrassess_custom_headers.ismatch_with_competency " & _
                   "    , hrassess_custom_headers.isallowaddpastperiod " & _
                   "    , hrassess_custom_headers.mappedperiodunkid " & _
                   "    , hrassess_custom_headers.min_score " & _
                   "    , hrassess_custom_headers.scaletranunkid " & _
                   "    , CASE WHEN hrassess_custom_headers.ismatch_with_competency = 1 THEN @Y ELSE @N END AS match_With_Comp " & _
                   "    , CASE WHEN hrassess_custom_headers.isallowaddpastperiod = 1 THEN @Y ELSE @N END AS AllowAddPastPeriod " & _
                   "    , CASE WHEN hrassess_custom_headers.isinclude_planning = 1 THEN @Y ELSE @N END AS Include_Planning " & _
                   "    , ISNULL(hrassess_scale_master.scale,0) AS scale  " & _
                   "    , CASE WHEN hrassess_custom_headers.scaletranunkid > 0 THEN CAST(hrassess_scale_master.scale AS NVARCHAR) + ' - ' + ISNULL(hrassess_scale_master.description,'') " & _
                   "            ELSE '' END scaledescription " & _
                   "FROM hrassess_custom_headers " & _
                   "    LEFT JOIN hrassess_scale_master ON hrassess_scale_master.scaletranunkid = hrassess_custom_headers.scaletranunkid AND hrassess_scale_master.isactive = 1 " & _
                   "WHERE hrassess_custom_headers.isactive = 1 "
            'Shani (26-Sep-2016) -- End

            objDataOperation.AddParameter("@Y", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Yes"))
            objDataOperation.AddParameter("@N", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "No"))

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_custom_headers) </purpose>
    Public Function Insert(ByVal iUserId As Integer, Optional ByVal intCurrentPeriodID As Integer = 0) As Boolean
        If isExist(mstrName) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Custom Header is already defined for the selected period. Please define new Custom Header.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, mstrName.Length, mstrName.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, mstrName1.Length, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, mstrName2.Length, mstrName2.ToString)
            objDataOperation.AddParameter("@is_allow_ess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIs_Allow_Ess.ToString)
            objDataOperation.AddParameter("@is_allow_mss", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIs_Allow_Mss.ToString)
            objDataOperation.AddParameter("@is_allow_multiple", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIs_Allow_Multiple.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            objDataOperation.AddParameter("@isinclude_planning", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInclude_Planning.ToString)
            objDataOperation.AddParameter("@ismatch_with_competency", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsMatch_With_Competency.ToString)
            objDataOperation.AddParameter("@isallowaddpastperiod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAllowAddPastPeriod.ToString)
            objDataOperation.AddParameter("@mappedperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingPeriodUnkId.ToString)
            objDataOperation.AddParameter("@min_score", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMin_Score)
            objDataOperation.AddParameter("@scaletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScaleUnkId)
            'strQ = "INSERT INTO hrassess_custom_headers ( " & _
            '  "  name " & _
            '  ", name1 " & _
            '  ", name2 " & _
            '  ", is_allow_ess " & _
            '  ", is_allow_mss " & _
            '  ", is_allow_multiple " & _
            '  ", isactive" & _
            '") VALUES (" & _
            '  "  @name " & _
            '  ", @name1 " & _
            '  ", @name2 " & _
            '  ", @is_allow_ess " & _
            '  ", @is_allow_mss " & _
            '  ", @is_allow_multiple " & _
            '  ", @isactive" & _
            '"); SELECT @@identity"

            strQ = "INSERT INTO hrassess_custom_headers ( " & _
              "  name " & _
              ", name1 " & _
              ", name2 " & _
              ", is_allow_ess " & _
              ", is_allow_mss " & _
              ", is_allow_multiple " & _
              ", isactive" & _
              ", isinclude_planning" & _
              ", ismatch_with_competency" & _
              ", isallowaddpastperiod" & _
              ", mappedperiodunkid" & _
              ", min_score" & _
              ", scaletranunkid" & _
            ") VALUES (" & _
              "  @name " & _
              ", @name1 " & _
              ", @name2 " & _
              ", @is_allow_ess " & _
              ", @is_allow_mss " & _
              ", @is_allow_multiple " & _
              ", @isactive" & _
              ", @isinclude_planning" & _
              ", @ismatch_with_competency" & _
              ", @isallowaddpastperiod" & _
              ", @mappedperiodunkid" & _
              ", @min_score" & _
              ", @scaletranunkid " & _
            "); SELECT @@identity"

            'Shani (26-Sep-2016) -- End
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintCustomheaderunkid = dsList.Tables(0).Rows(0).Item(0)

            If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_custom_headers", "customheaderunkid", mintCustomheaderunkid, , iUserId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            If mblnIsMatch_With_Competency Then
                Dim objCustomItem As New clsassess_custom_items

                objCustomItem._Customheaderunkid = mintCustomheaderunkid
                objCustomItem._Custom_Field = Language.getMessage(mstrModuleName, 6, "Competency Category")
                objCustomItem._Periodunkid = intCurrentPeriodID
                objCustomItem._ItemTypeId = clsassess_custom_items.enCustomType.FREE_TEXT
                objCustomItem._SelectionModeid = 0
                objCustomItem._ViewModeId = 0
                objCustomItem._IsDefualtEntry = 1
                objCustomItem.Insert(iUserId, objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                objCustomItem._Customheaderunkid = mintCustomheaderunkid
                objCustomItem._Custom_Field = Language.getMessage(mstrModuleName, 7, "Competence")
                objCustomItem._Periodunkid = intCurrentPeriodID
                objCustomItem._ItemTypeId = clsassess_custom_items.enCustomType.FREE_TEXT
                objCustomItem._SelectionModeid = 0
                objCustomItem._ViewModeId = 0
                objCustomItem._IsDefualtEntry = 1
                objCustomItem.Insert(iUserId, objDataOperation)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            'Shani (26-Sep-2016) -- End

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_custom_headers) </purpose>
    Public Function Update(ByVal iUserId As Integer, Optional ByVal intCurrentPeriodID As Integer = 0) As Boolean
        If isExist(mstrName, mintCustomheaderunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "Sorry, This Custom Header is already defined for the selected period. Please define new Custom Header.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            objDataOperation.AddParameter("@customheaderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCustomheaderunkid.ToString)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, mstrName.Length, mstrName.ToString)
            objDataOperation.AddParameter("@name1", SqlDbType.NVarChar, mstrName1.Length, mstrName1.ToString)
            objDataOperation.AddParameter("@name2", SqlDbType.NVarChar, mstrName2.Length, mstrName2.ToString)
            objDataOperation.AddParameter("@is_allow_ess", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIs_Allow_Ess.ToString)
            objDataOperation.AddParameter("@is_allow_mss", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIs_Allow_Mss.ToString)
            objDataOperation.AddParameter("@is_allow_multiple", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIs_Allow_Multiple.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            objDataOperation.AddParameter("@isinclude_planning", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsInclude_Planning.ToString)
            objDataOperation.AddParameter("@ismatch_with_competency", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsMatch_With_Competency.ToString)
            objDataOperation.AddParameter("@isallowaddpastperiod", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsAllowAddPastPeriod.ToString)
            objDataOperation.AddParameter("@mappedperiodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMappingPeriodUnkId.ToString)
            objDataOperation.AddParameter("@min_score", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecMin_Score)
            objDataOperation.AddParameter("@scaletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintScaleUnkId)

            'strQ = "UPDATE hrassess_custom_headers SET " & _
            '       "  name = @name" & _
            '       ", name1 = @name1" & _
            '       ", name2 = @name2" & _
            '       ", is_allow_ess = @is_allow_ess" & _
            '       ", is_allow_mss = @is_allow_mss" & _
            '       ", is_allow_multiple = @is_allow_multiple" & _
            '       ", isactive = @isactive " & _
            '       "WHERE customheaderunkid = @customheaderunkid "

            strQ = "UPDATE hrassess_custom_headers SET " & _
                   "  name = @name" & _
                   ", name1 = @name1" & _
                   ", name2 = @name2" & _
                   ", is_allow_ess = @is_allow_ess" & _
                   ", is_allow_mss = @is_allow_mss" & _
                   ", is_allow_multiple = @is_allow_multiple" & _
                   ", isactive = @isactive " & _
                   ", isinclude_planning = @isinclude_planning " & _
                   ", ismatch_with_competency = @ismatch_with_competency " & _
                   ", isallowaddpastperiod = @isallowaddpastperiod " & _
                   ", mappedperiodunkid = @mappedperiodunkid " & _
                   ", min_score = @min_score " & _
                   ", scaletranunkid = @scaletranunkid " & _
                   "WHERE customheaderunkid = @customheaderunkid "

            'Shani (26-Sep-2016) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_custom_headers", mintCustomheaderunkid, "customheaderunkid", 2, objDataOperation) Then
                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_custom_headers", "customheaderunkid", mintCustomheaderunkid, , iUserId) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If


            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            If mblnIsMatch_With_Competency Then
                Dim objCustomItem As New clsassess_custom_items
                Dim bln As Boolean = False
                bln = objCustomItem.isExist(Language.getMessage(mstrModuleName, 6, "Competency Category"), mintCustomheaderunkid, mintMappingPeriodUnkId)

                If bln = False Then
                    objCustomItem._Customheaderunkid = mintCustomheaderunkid
                    objCustomItem._Custom_Field = Language.getMessage(mstrModuleName, 6, "Competency Category")
                    objCustomItem._Periodunkid = intCurrentPeriodID
                    objCustomItem._ItemTypeId = clsassess_custom_items.enCustomType.FREE_TEXT
                    objCustomItem._SelectionModeid = 0
                    objCustomItem._ViewModeId = 0
                    objCustomItem._IsDefualtEntry = True
                    objCustomItem.Insert(iUserId, objDataOperation)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                bln = objCustomItem.isExist(Language.getMessage(mstrModuleName, 7, "Competence"), mintCustomheaderunkid, intCurrentPeriodID)

                If bln = False Then
                    objCustomItem._Customheaderunkid = mintCustomheaderunkid
                    objCustomItem._Custom_Field = Language.getMessage(mstrModuleName, 7, "Competence")
                    objCustomItem._Periodunkid = intCurrentPeriodID
                    objCustomItem._ItemTypeId = clsassess_custom_items.enCustomType.FREE_TEXT
                    objCustomItem._SelectionModeid = 0
                    objCustomItem._ViewModeId = 0
                    objCustomItem._IsDefualtEntry = True
                    objCustomItem.Insert(iUserId, objDataOperation)

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            Else
                Dim objCustomItem As New clsassess_custom_items
                Dim bln As Boolean = False
                Dim intItemUnkid As Integer = -1

                bln = objCustomItem.isExist(Language.getMessage(mstrModuleName, 6, "Competency Category"), mintCustomheaderunkid, intCurrentPeriodID, -1, intItemUnkid)
                If bln Then
                    If objCustomItem.Delete(intItemUnkid, iUserId) = False Then
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                End If

                intItemUnkid = -1
                bln = objCustomItem.isExist(Language.getMessage(mstrModuleName, 7, "Competence"), mintCustomheaderunkid, intCurrentPeriodID, -1, intItemUnkid)

                If bln Then
                    If objCustomItem.Delete(intItemUnkid, iUserId) = False Then
                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If
                    End If
                End If
            End If
            'Shani (26-Sep-2016) -- End



            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_custom_headers) </purpose>
    Public Function Delete(ByVal intUnkid As Integer, ByVal iUserId As Integer) As Boolean
        mstrMessage = ""
        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 5, "Sorry, you cannot do delete operation. Reason : selected goal is already linked with transactions.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()
        Try
            strQ = "UPDATE hrassess_custom_headers SET " & _
                   " isactive = 0 " & _
                   "WHERE customheaderunkid = @customheaderunkid "

            objDataOperation.AddParameter("@customheaderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrassess_custom_headers", "customheaderunkid", intUnkid, , iUserId) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT 1 FROM hrassess_custom_items WHERE customheaderunkid = @customheaderunkid AND isactive = 1"

            objDataOperation.AddParameter("@customheaderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal strName As String, Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            'strQ = "SELECT " & _
            '  "  customheaderunkid " & _
            '  ", name " & _
            '  ", name1 " & _
            '  ", name2 " & _
            '  ", is_allow_ess " & _
            '  ", is_allow_mss " & _
            '  ", is_allow_multiple " & _
            '  ", isactive " & _
            '  "FROM hrassess_custom_headers " & _
            '  "WHERE name = @name " & _
            '  "AND isactive = 1 "

            strQ = "SELECT " & _
              "  customheaderunkid " & _
              ", name " & _
              ", name1 " & _
              ", name2 " & _
              ", is_allow_ess " & _
              ", is_allow_mss " & _
              ", is_allow_multiple " & _
              ", isactive " & _
              ", isinclude_planning " & _
              ", ismatch_with_competency " & _
              ", isallowaddpastperiod " & _
              ", mappedperiodunkid " & _
              ", min_score " & _
              ", scaletranunkid " & _
             "FROM hrassess_custom_headers " & _
             "WHERE name = @name " & _
             "AND isactive = 1 "
            'Shani (26-Sep-2016) -- End

            If intUnkid > 0 Then
                strQ &= " AND customheaderunkid <> @customheaderunkid"
            End If

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, strName.Length, strName)
            objDataOperation.AddParameter("@customheaderunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function getComboList(ByVal iPeriodId As Integer, Optional ByVal blnAddSelect As Boolean = False, Optional ByVal iList As String = "List", Optional ByVal blnHavingCustomItem As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Try
            If iList.Trim.Length <= 0 Then iList = "List"

            If blnAddSelect = True Then

                'Shani (01-Dec-2016) -- Start
                'Issue : CCBRT not showing Custom item 
                'StrQ = "SELECT 0 AS Id, @Select AS Name,0 AS periodunkid UNION "
                'End If
                'StrQ &= "SELECT customheaderunkid AS Id, name AS Name,'" & iPeriodId & "' AS periodunkid FROM hrassess_custom_headers WHERE isactive = 1 "
                StrQ = "SELECT 0 AS Id, @Select AS Name,0 AS periodunkid,0 AS isinclude_planning UNION "
            End If
            StrQ &= "SELECT customheaderunkid AS Id, name AS Name,'" & iPeriodId & "' AS periodunkid,isinclude_planning FROM hrassess_custom_headers WHERE isactive = 1 "
            'Shani (01-Dec-2016) -- End

            If blnHavingCustomItem = True Then
                StrQ &= " AND customheaderunkid IN (SELECT DISTINCT customheaderunkid FROM hrassess_custom_items WHERE isactive = 1 AND periodunkid = '" & iPeriodId & "')"
            End If

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, iList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
        Finally
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function

    ''' <summary>
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function Get_Headers_For_Menu(ByVal blnOnlyESS, ByVal blnOnlyMSS) As DataSet
        Dim StrQ As String = String.Empty
        Dim objDataOperation As New clsDataOperation
        Dim dsList As New DataSet
        Try
            StrQ = "SELECT customheaderunkid,name FROM hrassess_custom_headers WHERE isactive = 1 "

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            StrQ &= " AND isinclude_planning = 1 "
            'Shani (26-Sep-2016) -- End

            If blnOnlyESS = True Then
                StrQ &= " AND is_allow_ess = 1 "
            End If
            If blnOnlyMSS = True Then
                StrQ &= " AND is_allow_mss = 1 "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Headers_For_Menu", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry, This Custom Header is already defined for the selected period. Please define new Custom Header.")
			Language.setMessage(mstrModuleName, 2, "Yes")
			Language.setMessage(mstrModuleName, 3, "No")
			Language.setMessage(mstrModuleName, 4, "Select")
			Language.setMessage(mstrModuleName, 5, "Sorry, you cannot do delete operation. Reason : selected goal is already linked with transactions.")
            Language.setMessage(mstrModuleName, 6, "Competency Category")
            Language.setMessage(mstrModuleName, 7, "Competence")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
