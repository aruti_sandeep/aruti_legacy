﻿#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

Public Class clsDataTransfer

#Region " Private Variables "

    Private Const mstrModuleName As String = "clsDataTransfer"
    'Private mintOldPeriodId As Integer = 0
    'Private mintNewPeriodId As Integer = 0
    'Private xDicCoyField1Unkids As Dictionary(Of Integer, Integer)
    'Private xDicCoyField2Unkids As Dictionary(Of Integer, Integer)
    'Private xDicCoyField3Unkids As Dictionary(Of Integer, Integer)
    'Private xDicCoyField4Unkids As Dictionary(Of Integer, Integer)
    'Private xDicCoyField5Unkids As Dictionary(Of Integer, Integer)

    'Private xDicOwrField1Unkids As Dictionary(Of Integer, Integer)
    'Private xDicOwrField2Unkids As Dictionary(Of Integer, Integer)
    'Private xDicOwrField3Unkids As Dictionary(Of Integer, Integer)
    'Private xDicOwrField4Unkids As Dictionary(Of Integer, Integer)
    'Private xDicOwrField5Unkids As Dictionary(Of Integer, Integer)

    'Private xDicEmpField1Unkids As Dictionary(Of Integer, Integer)
    'Private xDicEmpField2Unkids As Dictionary(Of Integer, Integer)
    'Private xDicEmpField3Unkids As Dictionary(Of Integer, Integer)
    'Private xDicEmpField4Unkids As Dictionary(Of Integer, Integer)
    'Private xDicEmpField5Unkids As Dictionary(Of Integer, Integer)

    Private dsData As New DataSet
    Private mdtOwner As DataTable
    Private mDicInfoField As New Dictionary(Of Integer, String)
    Private mstrMessage As String = String.Empty
    Private mintUserUnkId As Integer = 0

    Dim xLinkedFieldId As Integer
    Dim xExOrder As Integer

#End Region

#Region " Enums "

    Public Enum enCFInformation
        CF_FIELD_MAPPING = 1
        CF_ASSESSMENT_RATIOS = 2
        CF_ASSESSMENT_SCALES = 3
        CF_ASSESSMENT_SCALE_MAPPING = 4
        CF_COMPANY_GOALS = 5
        CF_ALLOCATION_GOALS = 6
        CF_EMPLOYEE_GOALS = 7
        CF_COMPETENCIES = 8
        CF_ASSIGNED_COMPETENCIES = 9
        CF_COMPUTATION_FORMULAS = 10
        CF_CUSTOM_ITEMS = 11
        CF_PLANNED_CUSTOM_ITEMS = 12
    End Enum

#End Region

#Region " Properties "

    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

#End Region

#Region " Public/Private Methods "

    Public Function Perform_Transfer_Process(ByVal intClosePeriodId As Integer, ByVal intOpenPeriodId As Integer, ByVal iDicTransfer As Dictionary(Of Integer, String), ByVal xUserUnkid As Integer, Optional ByVal lbl As Label = Nothing) As Boolean
        Dim blnFlag As Boolean = False
        Dim objDataOperation As New clsDataOperation
        Try
            objDataOperation.ClearParameters()
            objDataOperation.BindTransaction()
            mintUserUnkId = xUserUnkid
            objDataOperation.AddParameter("@cUid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserUnkId)
            objDataOperation.AddParameter("@oPid", SqlDbType.Int, eZeeDataType.INT_SIZE, intClosePeriodId)
            objDataOperation.AddParameter("@nPid", SqlDbType.Int, eZeeDataType.INT_SIZE, intOpenPeriodId)

            Dim intStep As Integer = 1 : Dim strDisplayValue As String = Language.getMessage(mstrModuleName, 1, "Copying Data :")
            For Each iKey As Integer In iDicTransfer.Keys
                lbl.Text = strDisplayValue & " - " & iDicTransfer(iKey).ToString() & " "
                Application.DoEvents()
                Select Case iKey
                    Case enCFInformation.CF_FIELD_MAPPING
                        blnFlag = CF_FieldMapping(objDataOperation)
                    Case enCFInformation.CF_ASSESSMENT_RATIOS
                        blnFlag = CF_AssessmentRatio(objDataOperation)
                    Case enCFInformation.CF_ASSESSMENT_SCALES
                        blnFlag = CF_ScaleMaster(objDataOperation)
                    Case enCFInformation.CF_ASSESSMENT_SCALE_MAPPING
                        blnFlag = CF_ScaleMapping(objDataOperation)
                    Case enCFInformation.CF_COMPANY_GOALS
                        blnFlag = CF_CompanyGoals(objDataOperation)
                    Case enCFInformation.CF_ALLOCATION_GOALS
                        blnFlag = CF_AllocationGoals(objDataOperation)
                    Case enCFInformation.CF_EMPLOYEE_GOALS
                        blnFlag = CF_EmployeeGoals(objDataOperation)
                    Case enCFInformation.CF_COMPETENCIES
                        blnFlag = CF_Competencies(objDataOperation)
                    Case enCFInformation.CF_ASSIGNED_COMPETENCIES
                        blnFlag = CF_AssignedCompetencies(objDataOperation)
                    Case enCFInformation.CF_COMPUTATION_FORMULAS
                        blnFlag = CF_ComputationFormula(objDataOperation)
                    Case enCFInformation.CF_CUSTOM_ITEMS
                        blnFlag = CF_CustomItems(objDataOperation)
                    Case enCFInformation.CF_PLANNED_CUSTOM_ITEMS
                        blnFlag = CF_PlannedCustomItems(objDataOperation)
                End Select
                intStep += 1
            Next
            If blnFlag Then
                objDataOperation.ReleaseTransaction(True)
            End If
            Return blnFlag
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Perform_Transfer_Process; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CF_FieldMapping(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Try
            StrQ = "INSERT INTO hrassess_field_mapping " & _
                   "( " & _
                   "     fieldunkid " & _
                   "    ,periodunkid " & _
                   "    ,weight " & _
                   "    ,userunkid " & _
                   "    ,isactive " & _
                   ") " & _
                   "SELECT " & _
                   "     fieldunkid " & _
                   "    ,@nPid " & _
                   "    ,weight " & _
                   "    ,@cUid " & _
                   "    ,isactive " & _
                   "FROM hrassess_field_mapping WHERE isactive = 1 " & _
                   "AND periodunkid = @oPid " & _
                   "AND NOT EXISTS(SELECT * FROM hrassess_field_mapping AS FM WHERE FM.isactive = 1 AND FM.periodunkid = @nPid " & _
                   "AND FM.fieldunkid = hrassess_field_mapping.fieldunkid) "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "SELECT @fieldunkid = fieldunkid FROM hrassess_field_mapping where periodunkid = @nPid AND isactive = 1 "
            xDataOpr.AddParameter("@fieldunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xLinkedFieldId, ParameterDirection.InputOutput)
            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            If IsDBNull(xDataOpr.GetParameterValue("@fieldunkid")) = False Then
                xLinkedFieldId = xDataOpr.GetParameterValue("@fieldunkid")
            Else
                xLinkedFieldId = 0
            End If

            If xLinkedFieldId <= 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 100, "Sorry, No linked field found for new period.")
                Return False
            End If

            StrQ = "SELECT fieldunkid ,fieldcaption,linkedto,ROW_NUMBER()OVER(ORDER BY fieldunkid ASC) AS ExOrder,isinformational FROM hrassess_field_master WHERE isused = 1 "
            Dim dxList As New DataSet

            dxList = xDataOpr.ExecQuery(StrQ, "List")

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            If dxList.Tables("List").Rows.Count > 0 Then
                Dim dtmp() As DataRow = dxList.Tables("List").Select("fieldunkid = '" & xLinkedFieldId & "'")
                If dtmp.Length > 0 Then
                    xExOrder = Convert.ToInt32(dtmp(0)("ExOrder"))
                End If
            End If

            If xExOrder <= 0 Then
                mstrMessage = Language.getMessage(mstrModuleName, 101, "Sorry, No execution order field found for new period.")
                Return False
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CF_FieldMapping; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CF_AssessmentRatio(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Try
            StrQ = "INSERT INTO hrassess_ratio_master " & _
                   "( " & _
                   "     allocrefunkid " & _
                   "    ,periodunkid " & _
                   "    ,ge_weight " & _
                   "    ,bsc_weight " & _
                   "    ,employeeunkid " & _
                   "    ,userunkid " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voiddatetime " & _
                   "    ,voidreason " & _
                   ") " & _
                   "SELECT " & _
                   "     allocrefunkid " & _
                   "    ,@nPid " & _
                   "    ,ge_weight " & _
                   "    ,bsc_weight " & _
                   "    ,employeeunkid " & _
                   "    ,@cUid " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voiddatetime " & _
                   "    ,voidreason " & _
                   "FROM hrassess_ratio_master " & _
                   "WHERE isvoid = 0 AND periodunkid = @oPid " & _
                   "AND NOT EXISTS(SELECT * FROM hrassess_ratio_master AS RM WHERE RM.isvoid = 0 " & _
                   "AND RM.allocrefunkid = hrassess_ratio_master.allocrefunkid AND RM.periodunkid = @nPid) "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_ratio_tran " & _
                   "( " & _
                   "     ratiounkid " & _
                   "    ,allocationid " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voiddatetime " & _
                   "    ,voidreason " & _
                   ") " & _
                   "SELECT " & _
                   "     hrassess_ratio_master.ratiounkid " & _
                   "    ,B.allocationid " & _
                   "    ,B.isvoid " & _
                   "    ,B.voiduserunkid " & _
                   "    ,B.voiddatetime " & _
                   "    ,B.voidreason " & _
                   "FROM hrassess_ratio_master " & _
                   "JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         hrassess_ratio_tran.ratiounkid " & _
                   "        ,hrassess_ratio_tran.allocationid " & _
                   "        ,hrassess_ratio_tran.isvoid " & _
                   "        ,hrassess_ratio_tran.voiduserunkid " & _
                   "        ,hrassess_ratio_tran.voiddatetime " & _
                   "        ,hrassess_ratio_tran.voidreason " & _
                   "        ,hrassess_ratio_master.allocrefunkid " & _
                   "        ,hrassess_ratio_master.ge_weight " & _
                   "        ,hrassess_ratio_master.bsc_weight " & _
                   "    FROM hrassess_ratio_tran " & _
                   "        JOIN hrassess_ratio_master ON hrassess_ratio_master.ratiounkid = hrassess_ratio_tran.ratiounkid " & _
                   "    WHERE hrassess_ratio_master.isvoid = 0 AND hrassess_ratio_tran.isvoid = 0 " & _
                   "    AND hrassess_ratio_master.periodunkid = @oPid " & _
                   ") AS B ON B.allocrefunkid = hrassess_ratio_master.allocrefunkid " & _
                   "AND B.ge_weight = hrassess_ratio_master.ge_weight AND B.bsc_weight = hrassess_ratio_master.bsc_weight " & _
                   "WHERE hrassess_ratio_master.isvoid = 0 AND periodunkid = @nPid " & _
                   "AND NOT EXISTS(SELECT * FROM hrassess_ratio_tran AS RT WHERE RT.isvoid = 0 AND RT.ratiounkid = hrassess_ratio_master.ratiounkid) "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CF_AssessmentRatio; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CF_ScaleMaster(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Try
            StrQ = "INSERT INTO hrassess_scale_master " & _
                   "( " & _
                   "     scalemasterunkid " & _
                   "    ,periodunkid " & _
                   "    ,scale " & _
                   "    ,description " & _
                   "    ,isactive " & _
                   "    ,pct_from " & _
                   "    ,pct_to " & _
                   ") " & _
                   "SELECT " & _
                   "     scalemasterunkid " & _
                   "    ,@nPid " & _
                   "    ,scale " & _
                   "    ,description " & _
                   "    ,isactive " & _
                   "    ,pct_from " & _
                   "    ,pct_to " & _
                   "FROM hrassess_scale_master WHERE isactive = 1 " & _
                   "AND periodunkid = @oPid " & _
                   "AND NOT EXISTS(SELECT * FROM hrassess_scale_master AS SM WHERE SM.isactive = 1 AND SM.periodunkid = @nPid) "
            'S.SANDEEP [11-OCT-2018] -- START {pct_from,pct_to} -- END
            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CF_ScaleMaster; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CF_ScaleMapping(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Try
            StrQ = "INSERT INTO hrassess_scalemapping_tran " & _
                   "    (periodunkid,mappingunkid,perspectiveunkid,scalemasterunkid,isvoid,voiduserunkid,voiddatetime,voidreason) " & _
                   "SELECT " & _
                   "    @nPid,mappingunkid,A.perspectiveunkid,A.scalemasterunkid,0,-1,NULL,'' " & _
                   "FROM hrassess_field_mapping " & _
                   "JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         fieldunkid " & _
                   "        ,perspectiveunkid " & _
                   "        ,scalemasterunkid " & _
                   "    FROM hrassess_scalemapping_tran " & _
                   "        JOIN hrassess_field_mapping ON hrassess_field_mapping.mappingunkid = hrassess_scalemapping_tran.mappingunkid " & _
                   "    AND hrassess_field_mapping.periodunkid = @oPid " & _
                   ") AS A ON A.fieldunkid = hrassess_field_mapping.fieldunkid AND hrassess_field_mapping.periodunkid = @nPid " & _
                   "AND NOT EXISTS(SELECT * FROM hrassess_scalemapping_tran AS SM WHERE SM.isvoid = 0 AND SM.mappingunkid = hrassess_field_mapping.mappingunkid) "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CF_ScaleMapping; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CF_CompanyGoals(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Try
            StrQ = "INSERT INTO hrassess_coyfield1_master ( " & _
                       "    perspectiveunkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,periodunkid " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,ownerrefid " & _
                       "    ,userunkid " & _
                       "    ,weight " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       ") SELECT " & _
                       "    perspectiveunkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,@nPid " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,ownerrefid " & _
                       "    ,userunkid " & _
                       "    ,weight " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       "FROM hrassess_coyfield1_master " & _
                       "WHERE hrassess_coyfield1_master.isvoid = 0  AND hrassess_coyfield1_master.periodunkid = @oPid " & _
                       "    AND NOT EXISTS(SELECT * FROM hrassess_coyfield1_master AS CF1 WHERE CF1.isvoid = 0 AND CF1.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_coyfield2_master ( " & _
                       "     coyfield1unkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,userunkid " & _
                       "    ,weight " & _
                       "    ,periodunkid " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,ownerrefid " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       ") SELECT " & _
                       "     coyfield1unkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,userunkid " & _
                       "    ,weight " & _
                        "   ,@nPid " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,ownerrefid " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       "FROM hrassess_coyfield2_master " & _
                       "WHERE hrassess_coyfield2_master.isvoid = 0  AND hrassess_coyfield2_master.periodunkid = @oPid " & _
                       "    AND NOT EXISTS(SELECT * FROM hrassess_coyfield2_master AS CF2 WHERE CF2.isvoid = 0 AND CF2.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_coyfield2_master " & _
                       "SET coyfield1unkid = ISNULL(C.nId,0) " & _
                       "FROM " & _
                       "( " & _
                       "    SELECT " & _
                       "        hrassess_coyfield1_master.coyfield1unkid AS nId " & _
                       "        ,B.coyfield1unkid AS oId " & _
                       "        ,hrassess_coyfield1_master.field_data " & _
                       "        ,B.periodunkid " & _
                       "    FROM hrassess_coyfield1_master " & _
                       "        JOIN " & _
                       "        ( " & _
                       "            SELECT DISTINCT " & _
                       "                hrassess_coyfield2_master.coyfield1unkid " & _
                       "                ,hrassess_coyfield1_master.field_data " & _
                       "                ,hrassess_coyfield2_master.periodunkid " & _
                       "                ,hrassess_coyfield1_master.perspectiveunkid " & _
                       "                ,hrassess_coyfield1_master.fieldunkid " & _
                       "            FROM hrassess_coyfield2_master " & _
                       "                JOIN hrassess_coyfield1_master ON hrassess_coyfield1_master.coyfield1unkid = hrassess_coyfield2_master.coyfield1unkid " & _
                       "            WHERE hrassess_coyfield2_master.periodunkid = @nPid " & _
                       "                AND hrassess_coyfield2_master.isvoid = 0 " & _
                       "                AND hrassess_coyfield1_master.isvoid = 0 " & _
                       "        ) AS B ON B.field_data = hrassess_coyfield1_master.field_data " & _
                       "            AND B.periodunkid = hrassess_coyfield1_master.periodunkid " & _
                       "            AND B.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid " & _
                       "            AND B.fieldunkid = hrassess_coyfield1_master.fieldunkid " & _
                       "        WHERE hrassess_coyfield1_master.isvoid = 0 " & _
                       "            AND hrassess_coyfield1_master.periodunkid = @nPid " & _
                       ")AS C WHERE C.periodunkid = hrassess_coyfield2_master.periodunkid " & _
                       "    AND C.oId = hrassess_coyfield2_master.coyfield1unkid "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_coyfield3_master ( " & _
                       "    coyfield2unkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,userunkid " & _
                       "    ,weight " & _
                       "    ,periodunkid " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,ownerrefid " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       ") " & _
                       "    SELECT " & _
                       "        coyfield2unkid " & _
                       "        ,fieldunkid " & _
                       "        ,field_data " & _
                       "        ,userunkid " & _
                       "        ,weight " & _
                       "        ,@nPid " & _
                       "        ,startdate " & _
                       "        ,enddate " & _
                       "        ,statusunkid " & _
                       "        ,ownerrefid " & _
                       "        ,isvoid " & _
                       "        ,voiduserunkid " & _
                       "        ,voidreason " & _
                       "        ,voiddatetime " & _
                       "FROM hrassess_coyfield3_master " & _
                       "WHERE hrassess_coyfield3_master.isvoid = 0  AND hrassess_coyfield3_master.periodunkid = @oPid " & _
                       "    AND NOT EXISTS(SELECT * FROM hrassess_coyfield3_master AS CF3 WHERE CF3.isvoid = 0 AND CF3.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_coyfield3_master " & _
                   "SET coyfield2unkid = ISNULL(C.nId,0) " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "        hrassess_coyfield2_master.coyfield2unkid AS nId " & _
                   "        ,B.coyfield2unkid AS oId " & _
                   "        ,hrassess_coyfield2_master.field_data " & _
                   "        ,B.periodunkid " & _
                   "    FROM hrassess_coyfield2_master " & _
                   "        JOIN " & _
                   "        ( " & _
                   "            SELECT DISTINCT " & _
                   "                hrassess_coyfield3_master.coyfield2unkid " & _
                   "                ,hrassess_coyfield2_master.field_data " & _
                   "                ,hrassess_coyfield3_master.periodunkid " & _
                   "                ,hrassess_coyfield2_master.fieldunkid " & _
                   "            FROM hrassess_coyfield3_master " & _
                   "                JOIN hrassess_coyfield2_master ON hrassess_coyfield2_master.coyfield2unkid = hrassess_coyfield3_master.coyfield2unkid " & _
                   "            WHERE hrassess_coyfield3_master.periodunkid = @nPid " & _
                   "                AND hrassess_coyfield3_master.isvoid = 0 " & _
                   "                AND hrassess_coyfield2_master.isvoid = 0 " & _
                   "        ) AS B ON B.field_data = hrassess_coyfield2_master.field_data " & _
                   "            AND B.periodunkid = hrassess_coyfield2_master.periodunkid " & _
                   "            AND B.fieldunkid = hrassess_coyfield2_master.fieldunkid " & _
                   "        WHERE hrassess_coyfield2_master.isvoid = 0 " & _
                   "            AND hrassess_coyfield2_master.periodunkid = @nPid " & _
                   ")AS C WHERE C.periodunkid = hrassess_coyfield3_master.periodunkid " & _
                   "    AND C.oId = hrassess_coyfield3_master.coyfield2unkid "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_coyfield4_master ( " & _
                   "     coyfield3unkid " & _
                   "    ,fieldunkid " & _
                   "    ,field_data " & _
                   "    ,userunkid " & _
                   "    ,weight " & _
                   "    ,periodunkid " & _
                   "    ,startdate " & _
                   "    ,enddate " & _
                   "    ,statusunkid " & _
                   "    ,ownerrefid " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voidreason " & _
                   "    ,voiddatetime " & _
                   ") " & _
                   "    SELECT " & _
                   "         coyfield3unkid " & _
                   "        ,fieldunkid " & _
                   "        ,field_data " & _
                   "        ,userunkid " & _
                   "        ,weight " & _
                   "        ,@nPid " & _
                   "        ,startdate " & _
                   "        ,enddate " & _
                   "        ,statusunkid " & _
                   "        ,ownerrefid " & _
                   "        ,isvoid " & _
                   "        ,voiduserunkid " & _
                   "        ,voidreason " & _
                   "        ,voiddatetime " & _
                   "    FROM hrassess_coyfield4_master " & _
                   "    WHERE hrassess_coyfield4_master.isvoid = 0  AND hrassess_coyfield4_master.periodunkid = @oPid " & _
                   "        AND NOT EXISTS(SELECT * FROM hrassess_coyfield4_master AS CF4 WHERE CF4.isvoid = 0 AND CF4.periodunkid = @nPid); "
            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_coyfield4_master " & _
                   "SET coyfield3unkid = ISNULL(C.nId,0) " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "        hrassess_coyfield3_master.coyfield3unkid AS nId " & _
                   "        ,B.coyfield3unkid  AS oId " & _
                   "        ,hrassess_coyfield3_master.field_data " & _
                   "        ,B.periodunkid " & _
                   "    FROM hrassess_coyfield3_master " & _
                   "        JOIN " & _
                   "        ( " & _
                   "            SELECT DISTINCT " & _
                   "                hrassess_coyfield4_master.coyfield3unkid " & _
                   "                ,hrassess_coyfield3_master.field_data " & _
                   "                ,hrassess_coyfield4_master.periodunkid " & _
                   "                ,hrassess_coyfield3_master.fieldunkid " & _
                   "            FROM hrassess_coyfield4_master " & _
                   "                JOIN hrassess_coyfield3_master ON hrassess_coyfield3_master.coyfield3unkid = hrassess_coyfield4_master.coyfield3unkid " & _
                   "            WHERE hrassess_coyfield4_master.periodunkid = @nPid " & _
                   "                AND hrassess_coyfield4_master.isvoid = 0 " & _
                   "                AND hrassess_coyfield3_master.isvoid = 0 " & _
                   "        ) AS B ON B.field_data = hrassess_coyfield3_master.field_data " & _
                   "            AND B.periodunkid = hrassess_coyfield3_master.periodunkid " & _
                   "            AND B.fieldunkid = hrassess_coyfield3_master.fieldunkid " & _
                   "    WHERE hrassess_coyfield3_master.isvoid = 0 " & _
                   "        AND hrassess_coyfield3_master.periodunkid = @nPid " & _
                   ")AS C WHERE C.periodunkid = hrassess_coyfield4_master.periodunkid " & _
                   "    AND C.oId = hrassess_coyfield4_master.coyfield3unkid "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_coyfield5_master ( " & _
                   "     coyfield4unkid " & _
                   "    ,fieldunkid " & _
                   "    ,field_data " & _
                   "    ,userunkid " & _
                   "    ,weight " & _
                   "    ,periodunkid " & _
                   "    ,startdate " & _
                   "    ,enddate " & _
                   "    ,statusunkid " & _
                   "    ,ownerrefid " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voidreason " & _
                   "    ,voiddatetime " & _
                   ") " & _
                   "    SELECT " & _
                   "        coyfield4unkid " & _
                   "        ,fieldunkid " & _
                   "        ,field_data " & _
                   "        ,userunkid " & _
                   "        ,weight " & _
                   "        ,@nPid " & _
                   "        ,startdate " & _
                   "        ,enddate " & _
                   "        ,statusunkid " & _
                   "        ,ownerrefid " & _
                   "        ,isvoid " & _
                   "        ,voiduserunkid " & _
                   "        ,voidreason " & _
                   "        ,voiddatetime " & _
                   "    FROM hrassess_coyfield5_master " & _
                   "    WHERE hrassess_coyfield5_master.isvoid = 0  AND hrassess_coyfield5_master.periodunkid =  @oPid " & _
                   "        AND NOT EXISTS(SELECT * FROM hrassess_coyfield5_master AS CF5 WHERE CF5.isvoid = 0 AND CF5.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_coyfield5_master " & _
                   "SET coyfield4unkid = ISNULL(C.nId,0) " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "        hrassess_coyfield4_master.coyfield4unkid AS nId " & _
                   "        ,B.coyfield4unkid  AS oId " & _
                   "        ,hrassess_coyfield4_master.field_data " & _
                   "        ,B.periodunkid " & _
                   "    FROM hrassess_coyfield4_master " & _
                   "        JOIN " & _
                   "            ( " & _
                   "                SELECT DISTINCT " & _
                   "                    hrassess_coyfield5_master.coyfield4unkid " & _
                   "                    ,hrassess_coyfield4_master.field_data " & _
                   "                    ,hrassess_coyfield5_master.periodunkid " & _
                   "                    ,hrassess_coyfield4_master.fieldunkid " & _
                   "                FROM hrassess_coyfield5_master " & _
                   "                    JOIN hrassess_coyfield4_master ON hrassess_coyfield4_master.coyfield4unkid = hrassess_coyfield5_master.coyfield4unkid " & _
                   "                WHERE hrassess_coyfield5_master.periodunkid = @nPid " & _
                   "                    AND hrassess_coyfield5_master.isvoid = 0 " & _
                   "                    AND hrassess_coyfield4_master.isvoid = 0 " & _
                   "            ) AS B ON B.field_data = hrassess_coyfield4_master.field_data " & _
                   "                AND B.periodunkid = hrassess_coyfield4_master.periodunkid " & _
                   "                AND B.fieldunkid = hrassess_coyfield4_master.fieldunkid " & _
                   "    WHERE hrassess_coyfield4_master.isvoid = 0 " & _
                   "    AND hrassess_coyfield4_master.periodunkid = @nPid " & _
                   ")AS C WHERE C.periodunkid = hrassess_coyfield5_master.periodunkid " & _
                   "    AND C.oId = hrassess_coyfield5_master.coyfield4unkid "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Dim xTableName, xColName, xParentCol, xParentTabName As String
            Dim xFieldTypeId As Integer = 0
            xTableName = "" : xColName = "" : xParentCol = "" : xParentTabName = ""

            'S.SANDEEP [21-NOV-2017] -- START
            If xExOrder > 0 Then
            Select Case xExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    xTableName = "hrassess_coyfield1_master"
                    xColName = "coyfield1unkid"
                    xParentCol = "coyfield1unkid"
                    xParentTabName = "hrassess_coyfield1_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD1
                Case enWeight_Types.WEIGHT_FIELD2
                    xTableName = "hrassess_coyfield2_master"
                    xColName = "coyfield2unkid"
                    xParentCol = "coyfield1unkid"
                    xParentTabName = "hrassess_coyfield2_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD2
                Case enWeight_Types.WEIGHT_FIELD3
                    xTableName = "hrassess_coyfield3_master"
                    xColName = "coyfield3unkid"
                    xParentCol = "coyfield2unkid"
                    xParentTabName = "hrassess_coyfield3_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD3
                Case enWeight_Types.WEIGHT_FIELD4
                    xTableName = "hrassess_coyfield4_master"
                    xColName = "coyfield4unkid"
                    xParentCol = "coyfield3unkid"
                    xParentTabName = "hrassess_coyfield4_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD4
                Case enWeight_Types.WEIGHT_FIELD5
                    xTableName = "hrassess_coyfield5_master"
                    xColName = "coyfield5unkid"
                    xParentCol = "coyfield4unkid"
                    xParentTabName = "hrassess_coyfield5_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD5
            End Select

            StrQ = "INSERT INTO hrassess_coyinfofield_tran " & _
                   "( " & _
                   "    coyfieldunkid " & _
                   "   ,fieldunkid " & _
                   "   ,field_data " & _
                   "   ,coyfieldtypeid " & _
                   ") " & _
                   "SELECT " & _
                   "    " & xTableName & "." & xColName & " AS nId " & _
                   "   ,B.fieldunkid " & _
                   "   ,B.infodata " & _
                   "   ,B.coyfieldtypeid " & _
                   "FROM " & xTableName & " " & _
                   "   JOIN " & _
                   "   ( " & _
                   "       SELECT DISTINCT " & _
                   "            hrassess_coyinfofield_tran.coyfieldunkid " & _
                   "           ," & xTableName & ".field_data " & _
                   "           ," & xTableName & ".periodunkid " & _
                   "           ,hrassess_coyinfofield_tran.fieldunkid " & _
                   "           ,hrassess_coyinfofield_tran.field_data AS infodata " & _
                   "           ,hrassess_coyinfofield_tran.coyfieldtypeid " & _
                   "       FROM hrassess_coyinfofield_tran " & _
                   "           JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = hrassess_coyinfofield_tran.coyfieldunkid " & _
                   "       WHERE " & xTableName & ".periodunkid = @oPid AND " & xTableName & ".isvoid = 0 AND hrassess_coyinfofield_tran.coyfieldtypeid = " & xFieldTypeId & " " & _
                   "   ) AS B ON B.field_data = " & xTableName & ".field_data " & _
                   "WHERE " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = @oPid " & _
                   "   AND NOT EXISTS(SELECT * FROM hrassess_coyinfofield_tran AS CF WHERE CF.coyfieldunkid = " & xTableName & "." & xColName & " " & _
                   "   AND CF.coyfieldtypeid = " & xFieldTypeId & ") "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_coyowner_tran " & _
                   "( " & _
                   "     coyfieldunkid " & _
                   "    ,allocationid " & _
                   "    ,coyfieldtypeid " & _
                   ") " & _
                   "SELECT " & _
                   "     " & xTableName & "." & xColName & " AS nId " & _
                   "    ,B.allocationid " & _
                   "    ,B.coyfieldtypeid " & _
                   "FROM " & xTableName & " " & _
                   "    JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             hrassess_coyowner_tran.coyfieldunkid " & _
                   "            ," & xTableName & ".field_data " & _
                   "            ," & xTableName & ".periodunkid " & _
                   "            ,hrassess_coyowner_tran.coyfieldtypeid " & _
                   "            ,hrassess_coyowner_tran.allocationid " & _
                   "        FROM hrassess_coyowner_tran " & _
                   "            JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = hrassess_coyowner_tran.coyfieldunkid " & _
                   "        WHERE " & xTableName & ".periodunkid = @oPid AND " & xTableName & ".isvoid = 0 AND hrassess_coyowner_tran.coyfieldtypeid = " & xFieldTypeId & " " & _
                   "    ) AS B ON B.field_data = " & xTableName & ".field_data " & _
                   "WHERE " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = @nPid " & _
                   "    AND NOT EXISTS(SELECT * FROM hrassess_coyowner_tran AS CF WHERE CF.coyfieldunkid = " & xTableName & "." & xColName & " " & _
                   "    AND CF.coyfieldtypeid = " & xFieldTypeId & ") "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If
            End If
            'S.SANDEEP [21-NOV-2017] -- END

            StrQ = "INSERT INTO hrassess_coystatus_tran ( " & _
                   "     statustypeid " & _
                   "    ,status_date " & _
                   "    ,commtents " & _
                   "    ,userunkid " & _
                   "    ,periodunkid " & _
                   ") " & _
                   "SELECT " & _
                   "     A.statustypeid " & _
                   "    ,GETDATE() " & _
                   "    ,A.commtents " & _
                   "    ,@cUid " & _
                   "    ,@nPid " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "        hrassess_coystatus_tran.status_date " & _
                   "        ,hrassess_coystatus_tran.statustypeid " & _
                   "        ,hrassess_coystatus_tran.commtents " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hrassess_coystatus_tran.periodunkid ORDER BY hrassess_coystatus_tran.status_date DESC) AS rno " & _
                   "    FROM hrassess_coystatus_tran " & _
                   "    WHERE hrassess_coystatus_tran.periodunkid = @oPid " & _
                   ") AS A WHERE A.rno = 1 "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Return True

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name:CF_CompanyGoals; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CF_AllocationGoals(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Try
            StrQ = "INSERT INTO hrassess_owrfield1_master " & _
                       "( " & _
                       "     coyfield1unkid " & _
                       "    ,ownerrefid " & _
                       "    ,ownerunkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,yearunkid " & _
                       "    ,periodunkid " & _
                       "    ,weight " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,userunkid " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       "    ,pct_completed " & _
                       ") " & _
                       "SELECT " & _
                       "     coyfield1unkid " & _
                       "    ,ownerrefid " & _
                       "    ,ownerunkid " & _
                       "    ,fieldunkid " & _
                       "    ,field_data " & _
                       "    ,yearunkid " & _
                       "    ,@nPid " & _
                       "    ,weight " & _
                       "    ,startdate " & _
                       "    ,enddate " & _
                       "    ,statusunkid " & _
                       "    ,userunkid " & _
                       "    ,isvoid " & _
                       "    ,voiduserunkid " & _
                       "    ,voidreason " & _
                       "    ,voiddatetime " & _
                       "    ,pct_completed " & _
                       "FROM hrassess_owrfield1_master WHERE hrassess_owrfield1_master.isvoid = 0 " & _
                       "    AND hrassess_owrfield1_master.periodunkid = @oPid " & _
                       "    AND NOT EXISTS(SELECT * FROM hrassess_owrfield1_master AS OF1 WHERE OF1.isvoid = 0 AND OF1.ownerunkid = hrassess_owrfield1_master.ownerunkid AND OF1.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_owrfield1_master " & _
                   "SET coyfield1unkid = ISNULL(C.nId,0) " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "        hrassess_coyfield1_master.coyfield1unkid AS nId " & _
                   "        ,B.coyfield1unkid AS oId " & _
                   "        ,hrassess_coyfield1_master.field_data " & _
                   "        ,B.periodunkid " & _
                   "    FROM hrassess_coyfield1_master " & _
                   "        JOIN " & _
                   "        ( " & _
                   "            SELECT DISTINCT " & _
                   "                hrassess_owrfield1_master.coyfield1unkid " & _
                   "                ,hrassess_coyfield1_master.field_data " & _
                   "                ,hrassess_owrfield1_master.periodunkid " & _
                   "                ,hrassess_coyfield1_master.perspectiveunkid " & _
                   "                ,hrassess_coyfield1_master.fieldunkid " & _
                   "            FROM hrassess_owrfield1_master " & _
                   "                JOIN hrassess_coyfield1_master ON hrassess_coyfield1_master.coyfield1unkid = hrassess_owrfield1_master.coyfield1unkid " & _
                   "            WHERE hrassess_owrfield1_master.periodunkid = @nPid " & _
                   "                AND hrassess_owrfield1_master.isvoid = 0 " & _
                   "                AND hrassess_coyfield1_master.isvoid = 0 " & _
                   "        ) AS B ON B.field_data = hrassess_coyfield1_master.field_data " & _
                   "            AND B.periodunkid = hrassess_coyfield1_master.periodunkid " & _
                   "            AND B.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid " & _
                   "            AND B.fieldunkid = hrassess_coyfield1_master.fieldunkid " & _
                   "    WHERE hrassess_coyfield1_master.isvoid = 0 " & _
                   "        AND hrassess_coyfield1_master.periodunkid = @nPid " & _
                   ")AS C WHERE C.periodunkid = hrassess_owrfield1_master.periodunkid " & _
                   "    AND C.oId = hrassess_owrfield1_master.coyfield1unkid "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_owrfield2_master " & _
                   "( " & _
                   "     owrfield1unkid " & _
                   "    ,ownerunkid " & _
                   "    ,periodunkid " & _
                   "    ,fieldunkid " & _
                   "    ,field_data " & _
                   "    ,weight " & _
                   "    ,userunkid " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voidreason " & _
                   "    ,voiddatetime " & _
                   "    ,startdate " & _
                   "    ,enddate " & _
                   "    ,statusunkid " & _
                   "    ,pct_completed " & _
                   ") " & _
                   "SELECT " & _
                   "     owrfield1unkid " & _
                   "    ,ownerunkid " & _
                   "    ,@nPid " & _
                   "    ,fieldunkid " & _
                   "    ,field_data " & _
                   "    ,weight " & _
                   "    ,userunkid " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voidreason " & _
                   "    ,voiddatetime " & _
                   "    ,startdate " & _
                   "    ,enddate " & _
                   "    ,statusunkid " & _
                   "    ,pct_completed " & _
                   "FROM hrassess_owrfield2_master WHERE hrassess_owrfield2_master.isvoid = 0 " & _
                   "    AND hrassess_owrfield2_master.periodunkid = @oPid " & _
                   "    AND NOT EXISTS(SELECT * FROM hrassess_owrfield2_master AS OF2 WHERE OF2.isvoid = 0 AND OF2.ownerunkid = hrassess_owrfield2_master.ownerunkid AND OF2.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_owrfield2_master " & _
                   "SET owrfield1unkid = ISNULL(C.nId,0) " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "        hrassess_owrfield1_master.owrfield1unkid AS nId " & _
                   "        ,B.owrfield1unkid AS oId " & _
                   "        ,hrassess_owrfield1_master.field_data " & _
                   "        ,B.periodunkid " & _
                   "    FROM hrassess_owrfield1_master " & _
                   "        JOIN " & _
                   "        ( " & _
                   "            SELECT DISTINCT " & _
                   "                hrassess_owrfield2_master.owrfield1unkid " & _
                   "                ,hrassess_owrfield1_master.field_data " & _
                   "                ,hrassess_owrfield2_master.periodunkid " & _
                   "                ,hrassess_owrfield1_master.ownerunkid " & _
                   "                ,hrassess_owrfield1_master.fieldunkid " & _
                   "            FROM hrassess_owrfield2_master " & _
                   "                JOIN hrassess_owrfield1_master ON hrassess_owrfield1_master.owrfield1unkid = hrassess_owrfield2_master.owrfield1unkid " & _
                   "            WHERE hrassess_owrfield2_master.periodunkid = @nPid " & _
                   "                AND hrassess_owrfield2_master.isvoid = 0 " & _
                   "                AND hrassess_owrfield1_master.isvoid = 0 " & _
                   "        ) AS B ON B.field_data = hrassess_owrfield1_master.field_data " & _
                   "            AND B.periodunkid = hrassess_owrfield1_master.periodunkid " & _
                   "            AND B.ownerunkid = hrassess_owrfield1_master.ownerunkid " & _
                   "            AND B.fieldunkid = hrassess_owrfield1_master.fieldunkid " & _
                   "    WHERE hrassess_owrfield1_master.isvoid = 0 " & _
                   "        AND hrassess_owrfield1_master.periodunkid = @nPid " & _
                   ") AS C WHERE C.periodunkid = hrassess_owrfield2_master.periodunkid " & _
                   "    AND C.oId = hrassess_owrfield2_master.owrfield1unkid "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_owrfield3_master " & _
                    "( " & _
                    "    owrfield2unkid " & _
                    "    ,ownerunkid " & _
                    "    ,periodunkid " & _
                    "    ,fieldunkid " & _
                    "    ,field_data " & _
                    "    ,weight " & _
                    "    ,startdate " & _
                    "    ,enddate " & _
                    "    ,statusunkid " & _
                    "    ,userunkid " & _
                    "    ,isvoid " & _
                    "    ,voiduserunkid " & _
                    "    ,voidreason " & _
                    "    ,voiddatetime " & _
                    "    ,pct_completed " & _
                    ") " & _
                    "SELECT " & _
                    "    owrfield2unkid " & _
                    "    ,ownerunkid " & _
                    "    ,@nPid " & _
                    "    ,fieldunkid " & _
                    "    ,field_data " & _
                    "    ,weight " & _
                    "    ,startdate " & _
                    "    ,enddate " & _
                    "    ,statusunkid " & _
                    "    ,userunkid " & _
                    "    ,isvoid " & _
                    "    ,voiduserunkid " & _
                    "    ,voidreason " & _
                    "    ,voiddatetime " & _
                    "    ,pct_completed " & _
                    "FROM hrassess_owrfield3_master WHERE hrassess_owrfield3_master.isvoid = 0 " & _
                    "   AND hrassess_owrfield3_master.periodunkid = @oPid " & _
                    "   AND NOT EXISTS(SELECT * FROM hrassess_owrfield3_master AS OF3 WHERE OF3.isvoid = 0 AND OF3.ownerunkid = hrassess_owrfield3_master.ownerunkid AND OF3.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_owrfield3_master " & _
                   "SET owrfield2unkid = ISNULL(C.nId,0) " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         hrassess_owrfield2_master.owrfield2unkid AS nId " & _
                   "        ,B.owrfield2unkid AS oId " & _
                   "        ,hrassess_owrfield2_master.field_data " & _
                   "        ,B.periodunkid " & _
                   "    FROM hrassess_owrfield2_master " & _
                   "        JOIN " & _
                   "        ( " & _
                   "            SELECT DISTINCT " & _
                   "                 hrassess_owrfield3_master.owrfield2unkid " & _
                   "                ,hrassess_owrfield2_master.field_data " & _
                   "                ,hrassess_owrfield3_master.periodunkid " & _
                   "                ,hrassess_owrfield2_master.ownerunkid " & _
                   "                ,hrassess_owrfield2_master.fieldunkid " & _
                   "            FROM hrassess_owrfield3_master " & _
                   "                JOIN hrassess_owrfield2_master ON hrassess_owrfield2_master.owrfield2unkid = hrassess_owrfield3_master.owrfield2unkid " & _
                   "            WHERE hrassess_owrfield3_master.periodunkid = @nPid " & _
                   "                AND hrassess_owrfield3_master.isvoid = 0 " & _
                   "                AND hrassess_owrfield2_master.isvoid = 0 " & _
                   "        ) AS B ON B.field_data = hrassess_owrfield2_master.field_data " & _
                   "            AND B.periodunkid = hrassess_owrfield2_master.periodunkid " & _
                   "            AND B.ownerunkid = hrassess_owrfield2_master.ownerunkid " & _
                   "            AND B.fieldunkid = hrassess_owrfield2_master.fieldunkid " & _
                   "    WHERE hrassess_owrfield2_master.isvoid = 0 " & _
                   "        AND hrassess_owrfield2_master.periodunkid = @nPid " & _
                   ") AS C WHERE C.periodunkid = hrassess_owrfield3_master.periodunkid " & _
                   "    AND C.oId = hrassess_owrfield3_master.owrfield2unkid "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If


            StrQ = "INSERT INTO hrassess_owrfield4_master " & _
                    "( " & _
                    "     owrfield3unkid " & _
                    "    ,ownerunkid " & _
                    "    ,periodunkid " & _
                    "    ,fieldunkid " & _
                    "    ,field_data " & _
                    "    ,weight " & _
                    "    ,startdate " & _
                    "    ,enddate " & _
                    "    ,statusunkid " & _
                    "    ,userunkid " & _
                    "    ,isvoid " & _
                    "    ,voiduserunkid " & _
                    "    ,voidreason " & _
                    "    ,voiddatetime " & _
                    "    ,pct_completed " & _
                    ") " & _
                    "SELECT " & _
                    "    owrfield3unkid " & _
                    "    ,ownerunkid " & _
                    "    ,@nPid " & _
                    "    ,fieldunkid " & _
                    "    ,field_data " & _
                    "    ,weight " & _
                    "    ,startdate " & _
                    "    ,enddate " & _
                    "    ,statusunkid " & _
                    "    ,userunkid " & _
                    "    ,isvoid " & _
                    "    ,voiduserunkid " & _
                    "    ,voidreason " & _
                    "    ,voiddatetime " & _
                    "    ,pct_completed " & _
                    "FROM hrassess_owrfield4_master WHERE hrassess_owrfield4_master.isvoid = 0 " & _
                    "   AND hrassess_owrfield4_master.periodunkid = @oPid " & _
                    "   AND NOT EXISTS(SELECT * FROM hrassess_owrfield4_master AS OF4 WHERE OF4.isvoid = 0 AND OF4.ownerunkid = hrassess_owrfield4_master.ownerunkid AND OF4.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_owrfield4_master " & _
                   "SET owrfield3unkid = ISNULL(C.nId,0) " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "        hrassess_owrfield3_master.owrfield3unkid AS nId " & _
                   "        ,B.owrfield3unkid AS oId " & _
                   "        ,hrassess_owrfield3_master.field_data " & _
                   "        ,B.periodunkid " & _
                   "    FROM hrassess_owrfield3_master " & _
                   "        JOIN " & _
                   "        ( " & _
                   "            SELECT DISTINCT " & _
                   "                hrassess_owrfield4_master.owrfield3unkid " & _
                   "                ,hrassess_owrfield3_master.field_data " & _
                   "                ,hrassess_owrfield4_master.periodunkid " & _
                   "                ,hrassess_owrfield3_master.ownerunkid " & _
                   "                ,hrassess_owrfield3_master.fieldunkid " & _
                   "            FROM hrassess_owrfield4_master " & _
                   "                JOIN hrassess_owrfield3_master ON hrassess_owrfield3_master.owrfield3unkid = hrassess_owrfield4_master.owrfield3unkid " & _
                   "            WHERE hrassess_owrfield4_master.periodunkid = @nPid " & _
                   "                AND hrassess_owrfield4_master.isvoid = 0 " & _
                   "                AND hrassess_owrfield3_master.isvoid = 0 " & _
                   "        ) AS B ON B.field_data = hrassess_owrfield3_master.field_data " & _
                   "            AND B.periodunkid = hrassess_owrfield3_master.periodunkid " & _
                   "            AND B.ownerunkid = hrassess_owrfield3_master.ownerunkid " & _
                   "            AND B.fieldunkid = hrassess_owrfield3_master.fieldunkid " & _
                   "    WHERE hrassess_owrfield3_master.periodunkid = @nPid " & _
                   ") AS C WHERE C.periodunkid = hrassess_owrfield4_master.periodunkid " & _
                   "    AND C.oId = hrassess_owrfield4_master.owrfield3unkid "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_owrfield5_master " & _
                    "( " & _
                    "    owrfield4unkid " & _
                    "    ,ownerunkid " & _
                    "    ,periodunkid " & _
                    "    ,fieldunkid " & _
                    "    ,field_data " & _
                    "    ,weight " & _
                    "    ,startdate " & _
                    "    ,enddate " & _
                    "    ,statusunkid " & _
                    "    ,userunkid " & _
                    "    ,isvoid " & _
                    "    ,voiduserunkid " & _
                    "    ,voidreason " & _
                    "    ,voiddatetime " & _
                    "    ,pct_completed " & _
                    ") " & _
                    "SELECT " & _
                    "    owrfield4unkid " & _
                    "    ,ownerunkid " & _
                    "    ,@nPid " & _
                    "    ,fieldunkid " & _
                    "    ,field_data " & _
                    "    ,weight " & _
                    "    ,startdate " & _
                    "    ,enddate " & _
                    "    ,statusunkid " & _
                    "    ,userunkid " & _
                    "    ,isvoid " & _
                    "    ,voiduserunkid " & _
                    "    ,voidreason " & _
                    "    ,voiddatetime " & _
                    "    ,pct_completed " & _
                    "FROM hrassess_owrfield5_master WHERE hrassess_owrfield5_master.isvoid = 0 " & _
                    "   AND hrassess_owrfield5_master.periodunkid = @oPid " & _
                    "   AND NOT EXISTS(SELECT * FROM hrassess_owrfield5_master AS OF5 WHERE OF5.isvoid = 0 AND OF5.ownerunkid = hrassess_owrfield5_master.ownerunkid AND OF5.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_owrfield5_master " & _
                   "SET owrfield4unkid = ISNULL(C.nId,0) " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "        hrassess_owrfield4_master.owrfield4unkid AS nId " & _
                   "        ,B.owrfield4unkid AS oId " & _
                   "        ,hrassess_owrfield4_master.field_data " & _
                   "        ,B.periodunkid " & _
                   "    FROM hrassess_owrfield4_master " & _
                   "        JOIN " & _
                   "        ( " & _
                   "            SELECT DISTINCT " & _
                   "                hrassess_owrfield5_master.owrfield4unkid " & _
                   "                ,hrassess_owrfield4_master.field_data " & _
                   "                ,hrassess_owrfield5_master.periodunkid " & _
                   "                ,hrassess_owrfield4_master.ownerunkid " & _
                   "                ,hrassess_owrfield4_master.fieldunkid " & _
                   "            FROM hrassess_owrfield5_master " & _
                   "                JOIN hrassess_owrfield4_master ON hrassess_owrfield4_master.owrfield4unkid = hrassess_owrfield5_master.owrfield4unkid " & _
                   "            WHERE hrassess_owrfield5_master.periodunkid = @nPid " & _
                   "                AND hrassess_owrfield5_master.isvoid = 0 " & _
                   "                AND hrassess_owrfield4_master.isvoid = 0 " & _
                   "        ) AS B ON B.field_data = hrassess_owrfield4_master.field_data " & _
                   "            AND B.periodunkid = hrassess_owrfield4_master.periodunkid " & _
                   "            AND B.ownerunkid = hrassess_owrfield4_master.ownerunkid " & _
                   "            AND B.fieldunkid = hrassess_owrfield4_master.fieldunkid " & _
                   "    WHERE hrassess_owrfield4_master.isvoid = 0 " & _
                   "        AND hrassess_owrfield4_master.periodunkid = @nPid " & _
                   ") AS C WHERE C.periodunkid = hrassess_owrfield5_master.periodunkid " & _
                   "    AND C.oId = hrassess_owrfield5_master.owrfield4unkid "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Dim xTableName, xColName, xParentCol, xParentTabName As String
            Dim xFieldTypeId As Integer = 0
            xTableName = "" : xColName = "" : xParentCol = "" : xParentTabName = ""

            'S.SANDEEP [21-NOV-2017] -- START
            If xExOrder > 0 Then
            Select Case xExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    xTableName = "hrassess_owrfield1_master"
                    xColName = "owrfield1unkid"
                    xParentCol = "owrfield1unkid"
                    xParentTabName = "hrassess_owrfield1_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD1
                Case enWeight_Types.WEIGHT_FIELD2
                    xTableName = "hrassess_owrfield2_master"
                    xColName = "owrfield2unkid"
                    xParentCol = "owrfield1unkid"
                    xParentTabName = "hrassess_owrfield2_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD2
                Case enWeight_Types.WEIGHT_FIELD3
                    xTableName = "hrassess_owrfield3_master"
                    xColName = "owrfield3unkid"
                    xParentCol = "owrfield2unkid"
                    xParentTabName = "hrassess_owrfield3_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD3
                Case enWeight_Types.WEIGHT_FIELD4
                    xTableName = "hrassess_owrfield4_master"
                    xColName = "owrfield4unkid"
                    xParentCol = "owrfield3unkid"
                    xParentTabName = "hrassess_owrfield4_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD4
                Case enWeight_Types.WEIGHT_FIELD5
                    xTableName = "hrassess_owrfield5_master"
                    xColName = "owrfield5unkid"
                    xParentCol = "owrfield4unkid"
                    xParentTabName = "hrassess_owrfield5_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD5
            End Select

            StrQ = "INSERT INTO hrassess_owrinfofield_tran " & _
                   "( " & _
                   "    owrfieldunkid " & _
                   "   ,fieldunkid " & _
                   "   ,field_data " & _
                   "   ,owrfieldtypeid " & _
                   ") " & _
                   "SELECT " & _
                   "    " & xTableName & "." & xColName & " AS nId " & _
                   "   ,B.fieldunkid " & _
                   "   ,B.infodata " & _
                   "   ,B.owrfieldtypeid " & _
                   "FROM " & xTableName & " " & _
                   "   JOIN " & _
                   "   ( " & _
                   "       SELECT DISTINCT " & _
                   "            hrassess_owrinfofield_tran.owrfieldunkid " & _
                   "           ," & xTableName & ".field_data " & _
                   "           ," & xTableName & ".periodunkid " & _
                   "           ," & xTableName & ".ownerunkid " & _
                   "           ,hrassess_owrinfofield_tran.fieldunkid " & _
                   "           ,hrassess_owrinfofield_tran.field_data AS infodata " & _
                   "           ,hrassess_owrinfofield_tran.owrfieldtypeid " & _
                   "       FROM hrassess_owrinfofield_tran " & _
                   "           JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = hrassess_owrinfofield_tran.owrfieldunkid " & _
                   "       WHERE " & xTableName & ".periodunkid = @oPid AND " & xTableName & ".isvoid = 0 AND hrassess_owrinfofield_tran.owrfieldtypeid = " & xFieldTypeId & " " & _
                   "   ) AS B ON B.field_data = " & xTableName & ".field_data " & _
                   "       AND B.ownerunkid = " & xTableName & ".ownerunkid " & _
                   "WHERE " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = @nPid " & _
                   "   AND NOT EXISTS(SELECT * FROM hrassess_owrinfofield_tran AS OWF WHERE OWF.owrfieldunkid = " & xTableName & "." & xColName & " " & _
                   "   AND OWF.owrfieldtypeid = " & xFieldTypeId & ") "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_owrowner_tran " & _
                   "( " & _
                   "     owrfieldunkid " & _
                   "    ,employeeunkid " & _
                   "    ,owrfieldtypeid " & _
                   ") " & _
                   "SELECT " & _
                   "     " & xTableName & "." & xColName & " AS nId " & _
                   "    ,B.empid " & _
                   "    ,B.owrfieldtypeid " & _
                   "FROM " & xTableName & " " & _
                   "    JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             hrassess_owrowner_tran.owrfieldunkid " & _
                   "            ," & xTableName & ".field_data " & _
                   "            ," & xTableName & ".periodunkid " & _
                   "            ," & xTableName & ".ownerunkid " & _
                   "            ,hrassess_owrowner_tran.owrfieldtypeid " & _
                   "            ,hrassess_owrowner_tran.employeeunkid AS empid " & _
                   "        FROM hrassess_owrowner_tran " & _
                   "            JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = hrassess_owrowner_tran.owrfieldunkid " & _
                   "        WHERE " & xTableName & ".periodunkid = @oPid AND " & xTableName & ".isvoid = 0 AND hrassess_owrowner_tran.owrfieldtypeid = " & xFieldTypeId & " " & _
                   "    ) AS B ON B.ownerunkid = " & xTableName & ".ownerunkid AND B.field_data = " & xTableName & ".field_data " & _
                   "WHERE " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = @nPid " & _
                   "    AND NOT EXISTS(SELECT * FROM hrassess_owrowner_tran AS OWF WHERE OWF.owrfieldunkid = " & xTableName & "." & xColName & " " & _
                   "    AND OWF.owrfieldtypeid = " & xFieldTypeId & ") "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If
            End If
            'S.SANDEEP [21-NOV-2017] -- END
            

            StrQ = "INSERT INTO hrassess_owrstatus_tran( " & _
                   "     periodunkid " & _
                   "    ,ownerunkid " & _
                   "    ,statustypeid " & _
                   "    ,status_date " & _
                   "    ,commtents " & _
                   "    ,userunkid " & _
                   ") " & _
                   "SELECT " & _
                   "     @nPid " & _
                   "    ,A.ownerunkid " & _
                   "    ,A.statustypeid " & _
                   "    ,GETDATE() " & _
                   "    ,A.commtents " & _
                   "    ,@cUid " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         hrassess_owrstatus_tran.ownerunkid " & _
                   "        ,hrassess_owrstatus_tran.statustypeid " & _
                   "        ,hrassess_owrstatus_tran.commtents " & _
                   "        ,hrassess_owrstatus_tran.status_date " & _
                   "        ,ROW_NUMBER()OVER(PARTITION by hrassess_owrstatus_tran.ownerunkid ORDER BY hrassess_owrstatus_tran.status_date DESC) AS RowNo " & _
                   "    FROM hrassess_owrstatus_tran WHERE hrassess_owrstatus_tran.periodunkid = @oPid " & _
                   ") AS A WHERE A.RowNo = 1 "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CF_AllocationGoals; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CF_EmployeeGoals(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Try
            StrQ = "INSERT INTO hrassess_empfield1_master " & _
                        "( " & _
                              "owrfield1unkid " & _
                             ",perspectiveunkid " & _
                             ",employeeunkid " & _
                             ",fieldunkid " & _
                             ",field_data " & _
                             ",yearunkid " & _
                             ",periodunkid " & _
                             ",weight " & _
                             ",pct_completed " & _
                             ",isfinal " & _
                             ",startdate " & _
                             ",enddate " & _
                             ",statusunkid " & _
                             ",userunkid " & _
                             ",isvoid " & _
                             ",voiduserunkid " & _
                             ",voidreason " & _
                             ",voiddatetime " & _
                        ") " & _
                        "SELECT " & _
                              "owrfield1unkid " & _
                             ",perspectiveunkid " & _
                             ",employeeunkid " & _
                             ",fieldunkid " & _
                             ",field_data " & _
                             ",yearunkid " & _
                             ",@nPid " & _
                             ",weight " & _
                             ",pct_completed " & _
                             ",isfinal " & _
                             ",startdate " & _
                             ",enddate " & _
                             ",statusunkid " & _
                             ",userunkid " & _
                             ",isvoid " & _
                             ",voiduserunkid " & _
                             ",voidreason " & _
                             ",voiddatetime " & _
                        "FROM hrassess_empfield1_master WHERE hrassess_empfield1_master.isvoid = 0 " & _
                        "AND hrassess_empfield1_master.periodunkid = @oPid " & _
                        "AND NOT EXISTS(SELECT * FROM hrassess_empfield1_master AS EF1 WHERE EF1.isvoid = 0 AND EF1.employeeunkid = hrassess_empfield1_master.employeeunkid AND EF1.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_empfield1_master " & _
                    "SET owrfield1unkid = ISNULL(C.nId,0) " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                               "hrassess_owrfield1_master.owrfield1unkid AS nId " & _
                              ",B.owrfield1unkid AS oId " & _
                              ",hrassess_owrfield1_master.field_data " & _
                              ",B.periodunkid " & _
                         "FROM hrassess_owrfield1_master " & _
                         "JOIN " & _
                         "( " & _
                              "SELECT DISTINCT " & _
                                    "hrassess_empfield1_master.owrfield1unkid " & _
                                   ",hrassess_owrfield1_master.field_data " & _
                                   ",hrassess_empfield1_master.periodunkid " & _
                                   ",hrassess_owrfield1_master.ownerrefid " & _
                                   ",hrassess_owrfield1_master.ownerunkid " & _
                              "FROM hrassess_empfield1_master " & _
                              "JOIN hrassess_owrfield1_master ON hrassess_empfield1_master.owrfield1unkid = hrassess_owrfield1_master.owrfield1unkid " & _
                              "WHERE hrassess_empfield1_master.isvoid = 0 AND hrassess_empfield1_master.periodunkid = @nPid AND hrassess_empfield1_master.isvoid = 0 AND hrassess_owrfield1_master.isvoid = 0 " & _
                         ") AS B ON B.field_data = hrassess_owrfield1_master.field_data AND B.periodunkid = hrassess_owrfield1_master.periodunkid " & _
                         "AND B.ownerrefid = hrassess_owrfield1_master.ownerrefid AND B.ownerunkid = hrassess_owrfield1_master.ownerunkid " & _
                         "WHERE hrassess_owrfield1_master.isvoid = 0 AND hrassess_owrfield1_master.periodunkid = @nPid " & _
                    ")AS C WHERE C.periodunkid = hrassess_empfield1_master.periodunkid AND C.oId = hrassess_empfield1_master.owrfield1unkid "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_empfield2_master " & _
                    "( " & _
                          "empfield1unkid " & _
                         ",employeeunkid " & _
                         ",periodunkid " & _
                         ",fieldunkid " & _
                         ",field_data " & _
                         ",weight " & _
                         ",pct_completed " & _
                         ",startdate " & _
                         ",enddate " & _
                         ",statusunkid " & _
                         ",userunkid " & _
                         ",isvoid " & _
                         ",voiduserunkid " & _
                         ",voidreason " & _
                         ",voiddatetime " & _
                    ") " & _
                    "SELECT " & _
                           "empfield1unkid " & _
                         ",employeeunkid " & _
                         ",@nPid " & _
                         ",fieldunkid " & _
                         ",field_data " & _
                         ",weight " & _
                         ",pct_completed " & _
                         ",startdate " & _
                         ",enddate " & _
                         ",statusunkid " & _
                         ",userunkid " & _
                         ",isvoid " & _
                         ",voiduserunkid " & _
                         ",voidreason " & _
                         ",voiddatetime " & _
                    "FROM hrassess_empfield2_master WHERE hrassess_empfield2_master.isvoid = 0 " & _
                    "AND hrassess_empfield2_master.periodunkid = @oPid " & _
                    "AND NOT EXISTS(SELECT * FROM hrassess_empfield2_master AS EF2 WHERE EF2.isvoid = 0 AND EF2.employeeunkid = hrassess_empfield2_master.employeeunkid AND EF2.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_empfield2_master " & _
                    "SET empfield1unkid = C.nId " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                               "hrassess_empfield1_master.empfield1unkid AS nId " & _
                              ",B.empfield1unkid AS oId " & _
                              ",hrassess_empfield1_master.employeeunkid " & _
                              ",hrassess_empfield1_master.field_data " & _
                              ",B.periodunkid " & _
                         "FROM hrassess_empfield1_master " & _
                         "JOIN " & _
                         "( " & _
                              "SELECT DISTINCT " & _
                                    "hrassess_empfield2_master.empfield1unkid " & _
                                   ",hrassess_empfield1_master.field_data " & _
                                   ",hrassess_empfield2_master.periodunkid " & _
                                   ",hrassess_empfield2_master.employeeunkid " & _
                              "FROM hrassess_empfield2_master " & _
                              "JOIN hrassess_empfield1_master ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid " & _
                              "WHERE hrassess_empfield2_master.periodunkid = @nPid AND hrassess_empfield1_master.isvoid = 0 AND hrassess_empfield2_master.isvoid = 0 " & _
                         ") AS B ON B.field_data = hrassess_empfield1_master.field_data AND hrassess_empfield1_master.periodunkid = B.periodunkid " & _
                              "AND B.employeeunkid = hrassess_empfield1_master.employeeunkid " & _
                         "WHERE hrassess_empfield1_master.isvoid = 0 AND hrassess_empfield1_master.periodunkid = @nPid " & _
                    ") AS C WHERE C.employeeunkid = hrassess_empfield2_master.employeeunkid AND C.periodunkid = hrassess_empfield2_master.periodunkid " & _
                    "AND C.oId = hrassess_empfield2_master.empfield1unkid; "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_empfield3_master " & _
                    "( " & _
                     "empfield2unkid " & _
                    ",employeeunkid " & _
                    ",periodunkid " & _
                    ",fieldunkid " & _
                    ",field_data " & _
                    ",weight " & _
                    ",pct_completed " & _
                    ",startdate " & _
                    ",enddate " & _
                    ",statusunkid " & _
                    ",userunkid " & _
                    ",isvoid " & _
                    ",voiduserunkid " & _
                    ",voidreason " & _
                    ",voiddatetime " & _
                    ") " & _
                    "SELECT " & _
                     "empfield2unkid " & _
                    ",employeeunkid " & _
                    ",@nPid " & _
                    ",fieldunkid " & _
                    ",field_data " & _
                    ",weight " & _
                    ",pct_completed " & _
                    ",startdate " & _
                    ",enddate " & _
                    ",statusunkid " & _
                    ",userunkid " & _
                    ",isvoid " & _
                    ",voiduserunkid " & _
                    ",voidreason " & _
                    ",voiddatetime " & _
                    "FROM hrassess_empfield3_master WHERE hrassess_empfield3_master.isvoid = 0 " & _
                    "AND hrassess_empfield3_master.periodunkid = @oPid " & _
                    "AND NOT EXISTS(SELECT * FROM hrassess_empfield3_master AS EF3 WHERE EF3.isvoid = 0 AND EF3.employeeunkid = hrassess_empfield3_master.employeeunkid AND EF3.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_empfield3_master " & _
                    "SET empfield2unkid = C.nId " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                               "hrassess_empfield2_master.empfield2unkid AS nId " & _
                              ",B.empfield2unkid AS oId " & _
                              ",hrassess_empfield2_master.employeeunkid " & _
                              ",hrassess_empfield2_master.field_data " & _
                              ",B.periodunkid " & _
                         "FROM hrassess_empfield2_master " & _
                         "JOIN " & _
                         "( " & _
                              "SELECT DISTINCT " & _
                                    "hrassess_empfield3_master.empfield2unkid " & _
                                   ",hrassess_empfield2_master.field_data " & _
                                   ",hrassess_empfield3_master.periodunkid " & _
                                   ",hrassess_empfield3_master.employeeunkid " & _
                              "FROM hrassess_empfield3_master " & _
                              "JOIN hrassess_empfield2_master ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid " & _
                              "WHERE hrassess_empfield3_master.periodunkid = @nPid AND hrassess_empfield2_master.isvoid = 0 AND hrassess_empfield3_master.isvoid = 0 " & _
                         ") AS B ON B.field_data = hrassess_empfield2_master.field_data AND hrassess_empfield2_master.periodunkid = B.periodunkid " & _
                              "AND B.employeeunkid = hrassess_empfield2_master.employeeunkid " & _
                         "WHERE hrassess_empfield2_master.isvoid = 0 AND hrassess_empfield2_master.periodunkid = @nPid " & _
                    ") AS C WHERE C.employeeunkid = hrassess_empfield3_master.employeeunkid AND C.periodunkid = hrassess_empfield3_master.periodunkid " & _
                    "AND C.oId = hrassess_empfield3_master.empfield2unkid; "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_empfield4_master " & _
                    "( " & _
                          "empfield3unkid " & _
                         ",employeeunkid " & _
                         ",periodunkid " & _
                         ",fieldunkid " & _
                         ",field_data " & _
                         ",weight " & _
                         ",pct_completed " & _
                         ",startdate " & _
                         ",enddate " & _
                         ",statusunkid " & _
                         ",userunkid " & _
                         ",isvoid " & _
                         ",voiduserunkid " & _
                         ",voidreason " & _
                         ",voiddatetime " & _
                    ") " & _
                    "SELECT " & _
                          "empfield3unkid " & _
                         ",employeeunkid " & _
                         ",@nPid " & _
                         ",fieldunkid " & _
                         ",field_data " & _
                         ",weight " & _
                         ",pct_completed " & _
                         ",startdate " & _
                         ",enddate " & _
                         ",statusunkid " & _
                         ",userunkid " & _
                         ",isvoid " & _
                         ",voiduserunkid " & _
                         ",voidreason " & _
                         ",voiddatetime " & _
                    "FROM hrassess_empfield4_master WHERE hrassess_empfield4_master.isvoid = 0 " & _
                    "AND hrassess_empfield4_master.periodunkid = @oPid " & _
                    "AND NOT EXISTS(SELECT * FROM hrassess_empfield4_master AS EF4 WHERE EF4.isvoid = 0 AND EF4.employeeunkid = hrassess_empfield4_master.employeeunkid AND EF4.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_empfield4_master " & _
                    "SET empfield3unkid = C.nId " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                               "hrassess_empfield3_master.empfield3unkid AS nId " & _
                              ",B.empfield3unkid AS oId " & _
                              ",hrassess_empfield3_master.employeeunkid " & _
                              ",hrassess_empfield3_master.field_data " & _
                              ",B.periodunkid " & _
                         "FROM hrassess_empfield3_master " & _
                         "JOIN " & _
                         "( " & _
                              "SELECT DISTINCT " & _
                                    "hrassess_empfield4_master.empfield3unkid " & _
                                   ",hrassess_empfield3_master.field_data " & _
                                   ",hrassess_empfield4_master.periodunkid " & _
                                   ",hrassess_empfield4_master.employeeunkid " & _
                              "FROM hrassess_empfield4_master " & _
                              "JOIN hrassess_empfield3_master ON hrassess_empfield3_master.empfield3unkid = hrassess_empfield4_master.empfield3unkid " & _
                              "WHERE hrassess_empfield4_master.periodunkid = @nPid AND hrassess_empfield3_master.isvoid = 0 AND hrassess_empfield4_master.isvoid = 0 " & _
                         ") AS B ON B.field_data = hrassess_empfield3_master.field_data AND hrassess_empfield3_master.periodunkid = B.periodunkid " & _
                              "AND B.employeeunkid = hrassess_empfield3_master.employeeunkid " & _
                         "WHERE hrassess_empfield3_master.isvoid = 0 AND hrassess_empfield3_master.periodunkid = @nPid " & _
                    ") AS C WHERE C.employeeunkid = hrassess_empfield4_master.employeeunkid AND C.periodunkid = hrassess_empfield4_master.periodunkid " & _
                    "AND C.oId = hrassess_empfield4_master.empfield3unkid; "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_empfield5_master " & _
                    "( " & _
                          "empfield4unkid " & _
                         ",employeeunkid " & _
                         ",periodunkid " & _
                         ",fieldunkid " & _
                         ",field_data " & _
                         ",weight " & _
                         ",pct_completed " & _
                         ",startdate " & _
                         ",enddate " & _
                         ",statusunkid " & _
                         ",userunkid " & _
                         ",isvoid " & _
                         ",voiduserunkid " & _
                         ",voidreason " & _
                         ",voiddatetime " & _
                    ") " & _
                    "SELECT " & _
                          "empfield4unkid " & _
                         ",employeeunkid " & _
                         ",@nPid " & _
                         ",fieldunkid " & _
                         ",field_data " & _
                         ",weight " & _
                         ",pct_completed " & _
                         ",startdate " & _
                         ",enddate " & _
                         ",statusunkid " & _
                         ",userunkid " & _
                         ",isvoid " & _
                         ",voiduserunkid " & _
                         ",voidreason " & _
                         ",voiddatetime " & _
                    "FROM hrassess_empfield5_master WHERE hrassess_empfield5_master.isvoid = 0 " & _
                    "AND hrassess_empfield5_master.periodunkid = @oPid " & _
                    "AND NOT EXISTS(SELECT * FROM hrassess_empfield5_master AS EF5 WHERE EF5.isvoid = 0 AND EF5.employeeunkid = hrassess_empfield5_master.employeeunkid AND EF5.periodunkid = @nPid); "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_empfield5_master " & _
                    "SET empfield4unkid = C.nId " & _
                    "FROM " & _
                    "( " & _
                         "SELECT " & _
                               "hrassess_empfield4_master.empfield4unkid AS nId " & _
                              ",B.empfield4unkid AS oId " & _
                              ",hrassess_empfield4_master.employeeunkid " & _
                              ",hrassess_empfield4_master.field_data " & _
                              ",B.periodunkid " & _
                         "FROM hrassess_empfield4_master " & _
                         "JOIN " & _
                         "( " & _
                              "SELECT DISTINCT " & _
                                    "hrassess_empfield5_master.empfield4unkid " & _
                                   ",hrassess_empfield4_master.field_data " & _
                                   ",hrassess_empfield5_master.periodunkid " & _
                                   ",hrassess_empfield5_master.employeeunkid " & _
                              "FROM hrassess_empfield5_master " & _
                              "JOIN hrassess_empfield4_master ON hrassess_empfield4_master.empfield4unkid = hrassess_empfield5_master.empfield4unkid " & _
                              "WHERE hrassess_empfield5_master.periodunkid = @nPid AND hrassess_empfield4_master.isvoid = 0 AND hrassess_empfield5_master.isvoid = 0 " & _
                         ") AS B ON B.field_data = hrassess_empfield4_master.field_data AND hrassess_empfield4_master.periodunkid = B.periodunkid " & _
                              "AND B.employeeunkid = hrassess_empfield4_master.employeeunkid " & _
                         "WHERE hrassess_empfield4_master.isvoid = 0 AND hrassess_empfield4_master.periodunkid = @nPid " & _
                    ") AS C WHERE C.employeeunkid = hrassess_empfield5_master.employeeunkid AND C.periodunkid = hrassess_empfield5_master.periodunkid " & _
                    "AND C.oId = hrassess_empfield5_master.empfield4unkid "


            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Dim xTableName, xColName, xParentCol, xParentTabName As String
            Dim xFieldTypeId As Integer = 0
            xTableName = "" : xColName = "" : xParentCol = "" : xParentTabName = ""

            'S.SANDEEP [21-NOV-2017] -- START
            If xExOrder > 0 Then
            Select Case xExOrder
                Case enWeight_Types.WEIGHT_FIELD1
                    xTableName = "hrassess_empfield1_master"
                    xColName = "empfield1unkid"
                    xParentCol = "empfield1unkid"
                    xParentTabName = "hrassess_empfield1_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD1
                Case enWeight_Types.WEIGHT_FIELD2
                    xTableName = "hrassess_empfield2_master"
                    xColName = "empfield2unkid"
                    xParentCol = "empfield1unkid"
                    xParentTabName = "hrassess_empfield1_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD2
                Case enWeight_Types.WEIGHT_FIELD3
                    xTableName = "hrassess_empfield3_master"
                    xColName = "empfield3unkid"
                    xParentCol = "empfield2unkid"
                    xParentTabName = "hrassess_empfield2_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD3
                Case enWeight_Types.WEIGHT_FIELD4
                    xTableName = "hrassess_empfield4_master"
                    xColName = "empfield4unkid"
                    xParentCol = "empfield3unkid"
                    xParentTabName = "hrassess_empfield3_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD4
                Case enWeight_Types.WEIGHT_FIELD5
                    xTableName = "hrassess_empfield5_master"
                    xColName = "empfield5unkid"
                    xParentCol = "empfield4unkid"
                    xParentTabName = "hrassess_empfield4_master"
                    xFieldTypeId = enWeight_Types.WEIGHT_FIELD5
            End Select

            StrQ = "INSERT INTO hrassess_empinfofield_tran " & _
                    "( " & _
                     "empfieldunkid " & _
                    ",fieldunkid " & _
                    ",field_data " & _
                    ",empfieldtypeid " & _
                    ") " & _
                    "SELECT " & _
                         " " & xTableName & "." & xColName & " AS nId " & _
                         ",B.fieldunkid " & _
                         ",B.infodata " & _
                         ",B.empfieldtypeid " & _
                    "FROM " & xTableName & " " & _
                    "JOIN " & _
                    "( " & _
                         "SELECT DISTINCT " & _
                              " hrassess_empinfofield_tran.empfieldunkid " & _
                              "," & xTableName & ".field_data " & _
                              "," & xTableName & ".periodunkid " & _
                              "," & xTableName & ".employeeunkid " & _
                              ",hrassess_empinfofield_tran.fieldunkid " & _
                              ",hrassess_empinfofield_tran.field_data AS infodata " & _
                              ",hrassess_empinfofield_tran.empfieldtypeid " & _
                         "FROM hrassess_empinfofield_tran " & _
                         "JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = hrassess_empinfofield_tran.empfieldunkid " & _
                         "WHERE " & xTableName & ".periodunkid = @oPid AND " & xTableName & ".isvoid = 0 AND hrassess_empinfofield_tran.empfieldtypeid = " & xFieldTypeId & " " & _
                    ") AS B ON B.field_data = " & xTableName & ".field_data " & _
                         "AND B.employeeunkid = " & xTableName & ".employeeunkid " & _
                    "WHERE " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = @nPid " & _
                    "AND NOT EXISTS(SELECT * FROM hrassess_empinfofield_tran AS EF WHERE EF.empfieldunkid = " & xTableName & "." & xColName & " " & _
                         "AND EF.empfieldtypeid = " & xFieldTypeId & ") "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_empowner_tran " & _
                    "( " & _
                     "empfieldunkid " & _
                    ",employeeunkid " & _
                    ",empfieldtypeid " & _
                    ") " & _
                    "SELECT " & _
                          "" & xTableName & "." & xColName & " AS nId " & _
                         ",B.empid " & _
                         ",B.empfieldtypeid " & _
                    "FROM " & xTableName & " " & _
                    "JOIN " & _
                    "( " & _
                         "SELECT " & _
                               "hrassess_empowner_tran.empfieldunkid " & _
                              "," & xTableName & ".field_data " & _
                              "," & xTableName & ".periodunkid " & _
                              "," & xTableName & ".employeeunkid " & _
                              ",hrassess_empowner_tran.empfieldtypeid " & _
                              ",hrassess_empowner_tran.employeeunkid AS empid " & _
                         "FROM hrassess_empowner_tran " & _
                              "JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = hrassess_empowner_tran.empfieldunkid " & _
                         "WHERE " & xTableName & ".periodunkid = @oPid AND " & xTableName & ".isvoid = 0 AND hrassess_empowner_tran.empfieldtypeid = " & xFieldTypeId & " " & _
                    ") AS B ON B.employeeunkid = " & xTableName & ".employeeunkid AND B.field_data = " & xTableName & ".field_data " & _
                    "WHERE " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = @nPid " & _
                    "AND NOT EXISTS(SELECT * FROM hrassess_empowner_tran AS EF WHERE EF.empfieldunkid = " & xTableName & "." & xColName & " " & _
                    "AND EF.empfieldtypeid = " & xFieldTypeId & ") "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If
            End If
            'S.SANDEEP [21-NOV-2017] -- END
            

            StrQ = "INSERT INTO hrassess_empstatus_tran( " & _
                   "     employeeunkid " & _
                   "    ,periodunkid " & _
                   "    ,statustypeid " & _
                   "    ,assessormasterunkid " & _
                   "    ,assessoremployeeunkid " & _
                   "    ,status_date " & _
                   "    ,commtents " & _
                   "    ,isunlock " & _
                   "    ,userunkid " & _
                   "    ,loginemployeeunkid " & _
                   "    ,isapprovalskipped " & _
                   ")SELECT " & _
                   "     A.employeeunkid " & _
                   "    ,@nPid " & _
                   "    ,A.statustypeid " & _
                   "    ,A.assessormasterunkid " & _
                   "    ,A.assessoremployeeunkid " & _
                   "    ,GETDATE() " & _
                   "    ,A.commtents " & _
                   "    ,A.isunlock " & _
                   "    ,@cUid " & _
                   "    ,A.loginemployeeunkid " & _
                   "    ,A.isapprovalskipped " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         hrassess_empstatus_tran.employeeunkid " & _
                   "        ,hrassess_empstatus_tran.statustypeid " & _
                   "        ,hrassess_empstatus_tran.assessormasterunkid " & _
                   "        ,hrassess_empstatus_tran.assessoremployeeunkid " & _
                   "        ,hrassess_empstatus_tran.commtents " & _
                   "        ,hrassess_empstatus_tran.isunlock " & _
                   "        ,hrassess_empstatus_tran.loginemployeeunkid " & _
                   "        ,hrassess_empstatus_tran.isapprovalskipped " & _
                   "        ,ROW_NUMBER()OVER(PARTITION BY hrassess_empstatus_tran.employeeunkid ORDER BY hrassess_empstatus_tran.status_date DESC) AS rno " & _
                   "    FROM hrassess_empstatus_tran " & _
                   "    WHERE hrassess_empstatus_tran.periodunkid = @oPid " & _
                   ") AS A WHERE A.rno = 1 "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CF_EmployeeGoals; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CF_Competencies(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Try
            StrQ = "DECLARE @tbl AS Table(pid int, gnuid nvarchar(max)) " & _
                   "INSERT INTO @tbl(pid,gnuid) SELECT periodunkid,NEWID() FROM hrassess_competencies_master GROUP BY periodunkid " & _
                   "INSERT INTO hrassess_movedcompetencies_master " & _
                   "    (competenciesguid,competenciesunkid,competence_categoryunkid,periodunkid,scalemasterunkid,name,description,name1,name2,isactive,competencegroupunkid) " & _
                   "SELECT " & _
                   "    gnuid,competenciesunkid,competence_categoryunkid,periodunkid,scalemasterunkid,name,description,name1,name2,isactive,competencegroupunkid FROM hrassess_competencies_master " & _
                   "JOIN @tbl ON pid = periodunkid " & _
                   "WHERE NOT EXISTS(SELECT * FROM hrassess_movedcompetencies_master WHERE hrassess_movedcompetencies_master.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
                   "AND hrassess_movedcompetencies_master.periodunkid = hrassess_competencies_master.periodunkid) "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "UPDATE hrassess_competencies_master SET isactive = 0 WHERE isactive = 1 " & _
                   "UPDATE hrassess_competencies_master SET isactive = 1,periodunkid = @nPid WHERE isactive = 0 AND periodunkid = @oPid "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CF_Competencies; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CF_AssignedCompetencies(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Try
            StrQ = "INSERT INTO hrassess_competence_assign_master " & _
                   "    (assessgroupunkid,periodunkid,weight,userunkid,isvoid,voiduserunkid,voidreason,voiddatetime,employeeunkid,isfinal,iszeroweightallowed) " & _
                   "SELECT assessgroupunkid,@nPid,weight,userunkid,isvoid,voiduserunkid,voidreason,voiddatetime,employeeunkid,isfinal,iszeroweightallowed FROM hrassess_competence_assign_master WHERE isvoid = 0 AND periodunkid = @oPid " & _
                   "AND NOT EXISTS(SELECT * FROM hrassess_competence_assign_master AS CM WHERE CM.isvoid = 0 AND CM.periodunkid = @nPid) "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_competence_assign_tran " & _
                   "    (assigncompetenceunkid,competenciesunkid,weight,isvoid,voiduserunkid,voidreason,voiddatetime) " & _
                   "SELECT " & _
                   "     CAM.assigncompetenceunkid " & _
                   "    ,A.competenciesunkid " & _
                   "    ,A.weight " & _
                   "    ,A.isvoid " & _
                   "    ,A.voiduserunkid " & _
                   "    ,A.voidreason " & _
                   "    ,A.voiddatetime " & _
                   "FROM hrassess_competence_assign_master AS CAM " & _
                   "JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         ACM.assigncompetenceunkid " & _
                   "        ,ACM.assessgroupunkid " & _
                   "        ,ACT.competenciesunkid " & _
                   "        ,ACT.weight " & _
                   "        ,ACT.isvoid " & _
                   "        ,ACT.voiduserunkid " & _
                   "        ,ACT.voidreason " & _
                   "        ,ACT.voiddatetime " & _
                   "    FROM hrassess_competence_assign_tran AS ACT " & _
                   "        JOIN hrassess_competence_assign_master ACM ON ACM.assigncompetenceunkid = ACT.assigncompetenceunkid " & _
                   "    WHERE ACM.isvoid = 0 AND ACT.isvoid = 0 AND ACM.periodunkid = @oPid " & _
                   ") AS A ON A.assessgroupunkid = CAM.assessgroupunkid AND CAM.periodunkid = @nPid " & _
                   "WHERE CAM.isvoid = 0 AND NOT EXISTS " & _
                   "(SELECT * FROM hrassess_competence_assign_tran AS CAT WHERE CAT.isvoid = 0 AND CAT.assigncompetenceunkid = CAM.assigncompetenceunkid) "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CF_AssignedCompetencies; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CF_ComputationFormula(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Try
            StrQ = "INSERT INTO hrassess_computation_master " & _
                   "    (periodunkid,formula_typeid,computation_formula,isvoid,voiduserunkid,voiddatetime,voidreason) " & _
                   "SELECT " & _
                   "    @nPid,formula_typeid,computation_formula,isvoid,voiduserunkid,voiddatetime,voidreason " & _
                   "FROM hrassess_computation_master WHERE isvoid = 0 AND periodunkid = @oPid " & _
                   "AND NOT EXISTS(SELECT * FROM hrassess_computation_master AS CM WHERE CM.isvoid = 0 AND CM.periodunkid = @nPid) "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            StrQ = "INSERT INTO hrassess_computation_tran " & _
                   "    (computationunkid,computation_typeid,isvoid,voiduserunkid,voiddatetime,voidreason,formulaid) " & _
                   "SELECT " & _
                   "     CO.computationunkid " & _
                   "    ,A.computation_typeid " & _
                   "    ,A.isvoid " & _
                   "    ,A.voiduserunkid " & _
                   "    ,A.voiddatetime " & _
                   "    ,A.voidreason " & _
                   "    ,A.formulaid " & _
                   "FROM hrassess_computation_master AS CO " & _
                   "JOIN " & _
                   "( " & _
                   "    SELECT " & _
                   "         CT.computationunkid " & _
                   "        ,CM.formula_typeid " & _
                   "        ,CT.computation_typeid " & _
                   "        ,CT.isvoid " & _
                   "        ,CT.voiduserunkid " & _
                   "        ,CT.voiddatetime " & _
                   "        ,CT.voidreason " & _
                   "        ,CT.formulaid " & _
                   "    FROM hrassess_computation_tran AS CT " & _
                   "        JOIN hrassess_computation_master AS CM ON CM.computationunkid = CT.computationunkid " & _
                   "    WHERE CM.isvoid = 0 AND CT.isvoid = 0  AND CM.periodunkid = @oPid " & _
                   ") AS A ON A.formula_typeid = CO.formula_typeid AND CO.periodunkid = @nPid " & _
                   "WHERE CO.isvoid = 0 AND NOT EXISTS " & _
                   "(SELECT * FROM hrassess_computation_tran WHERE isvoid = 0 AND CO.computationunkid = computationunkid) "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CF_ComputationFormula; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CF_CustomItems(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Try
            StrQ = "INSERT INTO hrassess_custom_items " & _
                   "    (customheaderunkid,periodunkid,itemtypeid,selectionmodeid,custom_item,viewmodeid,isactive,isdefaultentry,iscompletedtraining) " & _
                   "SELECT " & _
                   "     customheaderunkid " & _
                   "    ,@nPid " & _
                   "    ,itemtypeid " & _
                   "    ,selectionmodeid " & _
                   "    ,custom_item " & _
                   "    ,viewmodeid " & _
                   "    ,isactive " & _
                   "    ,isdefaultentry " & _
                   "    ,iscompletedtraining " & _
                   "FROM hrassess_custom_items " & _
                   "WHERE isactive = 1 AND periodunkid = @oPid " & _
                   "AND NOT EXISTS(SELECT * FROM hrassess_custom_items WHERE isactive = 1 AND periodunkid = @nPid) "
            'S.SANDEEP |05-APR-2019| -- START {iscompletedtraining} -- END

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CF_CustomItems; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    Private Function CF_PlannedCustomItems(ByVal xDataOpr As clsDataOperation) As Boolean
        Dim StrQ As String = String.Empty
        Try
            StrQ = "DECLARE @tbl AS TABLE(pid int,eid int, gnuid nvarchar(max),tdate datetime,pguid nvarchar(max)) " & _
                   "    IF NOT EXISTS(SELECT * FROM hrassess_plan_customitem_tran WHERE isvoid = 0 AND periodunkid = @nPid) " & _
                   "    BEGIN " & _
                   "        INSERT INTO @tbl(pid,eid,gnuid,tdate,pguid) " & _
                   "        SELECT periodunkid,employeeunkid,NEWID(),GETDATE(),plancustomguid FROM hrassess_plan_customitem_tran WHERE isvoid = 0 GROUP BY periodunkid,employeeunkid,plancustomguid " & _
                   "    END " & _
                   "INSERT INTO  hrassess_plan_customitem_tran " & _
                   "    (plancustomguid,customitemunkid,custom_value,employeeunkid,periodunkid,transactiondate,isvoid,voiddatetime,voiduserunkid,voidreason,isfinal,customheaderunkid,ismanual) " & _
                   "SELECT " & _
                   "    gnuid,customitemunkid,custom_value,employeeunkid,@nPid,transactiondate,isvoid,voiddatetime,voiduserunkid,voidreason,isfinal,customheaderunkid,ismanual " & _
                   "FROM hrassess_plan_customitem_tran " & _
                   "    JOIN @tbl ON pid = periodunkid AND eid = employeeunkid AND pguid = plancustomguid " & _
                   "WHERE isvoid = 0 AND periodunkid = @oPid " & _
                   "AND NOT EXISTS(SELECT * FROM hrassess_plan_customitem_tran WHERE isvoid = 0 AND periodunkid = @nPid) "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If

            'S.SANDEEP |04-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : OT APPROVER MIGRATION
            StrQ = "UPDATE hrassess_plan_customitem_tran SET customitemunkid = B.nId " & _
                   "FROM " & _
                   "( " & _
                   "    SELECT " & _
                   "         hrassess_custom_items.custom_item " & _
                   "        ,customitemunkid AS oId " & _
                   "        ,periodunkid AS oPid " & _
                   "        ,A.nId " & _
                   "        ,A.nPid " & _
                   "    FROM hrassess_custom_items " & _
                   "    JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "            custom_item,customitemunkid AS nId, periodunkid AS nPid " & _
                   "        FROM hrassess_custom_items " & _
                   "        WHERE isactive = 1 AND periodunkid = @nPid " & _
                   "    ) AS A ON A.custom_item = hrassess_custom_items.custom_item " & _
                   "    WHERE isactive = 1 AND periodunkid = @oPid " & _
                   ") AS B WHERE B.nPid = hrassess_plan_customitem_tran.periodunkid AND B.oId = hrassess_plan_customitem_tran.customitemunkid "

            xDataOpr.ExecNonQuery(StrQ)

            If xDataOpr.ErrorMessage <> "" Then
                Throw New Exception(xDataOpr.ErrorNumber & " : " & xDataOpr.ErrorMessage)
            End If
            'S.SANDEEP |04-MAR-2020| -- END

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: CF_PlannedCustomItems; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class

'S.SANDEEP [18-AUG-2017] <*********************************************************>

'Public Class clsDataTransfer

'#Region " Private Variables "

'    Private Const mstrModuleName As String = "clsDataTransfer"
'    Private mintOldPeriodId As Integer = 0
'    Private mintNewPeriodId As Integer = 0

'    Private xDicCoyField1Unkids As Dictionary(Of Integer, Integer)
'    Private xDicCoyField2Unkids As Dictionary(Of Integer, Integer)
'    Private xDicCoyField3Unkids As Dictionary(Of Integer, Integer)
'    Private xDicCoyField4Unkids As Dictionary(Of Integer, Integer)
'    Private xDicCoyField5Unkids As Dictionary(Of Integer, Integer)

'    Private xDicOwrField1Unkids As Dictionary(Of Integer, Integer)
'    Private xDicOwrField2Unkids As Dictionary(Of Integer, Integer)
'    Private xDicOwrField3Unkids As Dictionary(Of Integer, Integer)
'    Private xDicOwrField4Unkids As Dictionary(Of Integer, Integer)
'    Private xDicOwrField5Unkids As Dictionary(Of Integer, Integer)

'    Private xDicEmpField1Unkids As Dictionary(Of Integer, Integer)
'    Private xDicEmpField2Unkids As Dictionary(Of Integer, Integer)
'    Private xDicEmpField3Unkids As Dictionary(Of Integer, Integer)
'    Private xDicEmpField4Unkids As Dictionary(Of Integer, Integer)
'    Private xDicEmpField5Unkids As Dictionary(Of Integer, Integer)

'    Private dsData As New DataSet
'    Private mdtOwner As DataTable
'    Private mDicInfoField As New Dictionary(Of Integer, String)
'    Private mstrMessage As String = String.Empty

'    Private xPB As System.Windows.Forms.ProgressBar

'    'Shani (05-Sep-2016) -- Start
'    'Enhancement - Assessment Close Period to Save Last status data given by sandra (FZ)
'    Private mintUserUnkId As Integer = 0
'    'Shani (05-Sep-2016) -- End


'#End Region

'#Region " Properties "

'    Public Property _OldPeriodId() As Integer
'        Get
'            Return mintOldPeriodId
'        End Get
'        Set(ByVal value As Integer)
'            mintOldPeriodId = value
'        End Set
'    End Property

'    Public Property _NewPeriodId() As Integer
'        Get
'            Return mintNewPeriodId
'        End Get
'        Set(ByVal value As Integer)
'            mintNewPeriodId = value
'        End Set
'    End Property

'    Public ReadOnly Property _Message() As String
'        Get
'            Return mstrMessage
'        End Get
'    End Property

'    Public WriteOnly Property _xPB() As ProgressBar
'        Set(ByVal value As ProgressBar)
'            xPB = value
'        End Set
'    End Property

'#End Region

'#Region " Public/Private Methods "

'    'Shani (05-Sep-2016) -- Start
'    'Enhancement - Assessment Close Period to Save Last status data given by sandra (FZ)
'    'Public Function Perform_Transfer_Process() As Boolean
'    Public Function Perform_Transfer_Process(ByVal xUserUnkid As Integer) As Boolean
'        'Shani (05-Sep-2016) -- End
'        Dim blnFlag As Boolean = False
'        Try

'            'Shani (05-Sep-2016) -- Start
'            'Enhancement - Assessment Close Period to Save Last status data given by sandra (FZ)
'            mintUserUnkId = xUserUnkid
'            'Shani (05-Sep-2016) -- End


'            xPB.Maximum = 9 '--------- Total Methods Count Below(Increase if Method Increased)
'            blnFlag = Transfer_Scale_Master() : If blnFlag = False Then Return blnFlag
'            blnFlag = Transfer_Scale_Mapping() : If blnFlag = False Then Return blnFlag

'            'S.SANDEEP [14 MAR 2016] -- START
'            'blnFlag = Transfer_Assessment_Ratio() : If blnFlag = False Then Return blnFlag
'            'S.SANDEEP [14 MAR 2016] -- END

'            blnFlag = Transfer_Field_Mapping() : If blnFlag = False Then Return blnFlag
'            blnFlag = Transfer_Company_Level_Data() : If blnFlag = False Then Return blnFlag
'            blnFlag = Tranfer_Owner_Level_Data() : If blnFlag = False Then Return blnFlag
'            blnFlag = Transfer_Employee_Level_Data() : If blnFlag = False Then Return blnFlag
'            blnFlag = Transfer_Custom_Items() : If blnFlag = False Then Return blnFlag
'            blnFlag = Transfer_Competencies_Items() : If blnFlag = False Then Return blnFlag
'            'S.SANDEEP [13 JAN 2016] -- START
'            'blnFlag = Transfer_Competencies_Assignment() : If blnFlag = False Then Return blnFlag
'            'S.SANDEEP [13 JAN 2016] -- END
'            blnFlag = Transfer_Computation_Formula() : If blnFlag = False Then Return blnFlag
'            xPB.Value = 0
'            Return True
'        Catch ex As Exception
'            xPB.Value = 0
'            DisplayError.Show("-1", ex.Message, "Perform_Transfer_Process", mstrModuleName)
'        Finally
'            xPB.Value = 0
'        End Try
'    End Function

'    Private Function Transfer_Scale_Master() As Boolean
'        Dim StrQ As String = String.Empty
'        Dim dsScale As New DataSet
'        Dim objScale As clsAssessment_Scale
'        Try
'            Using objDo As New clsDataOperation
'                StrQ = "SELECT scalemasterunkid,scale,description FROM hrassess_scale_master WHERE isactive = 1 AND periodunkid = @periodunkid "

'                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldPeriodId)

'                dsScale = objDo.ExecQuery(StrQ, "List")

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If
'            End Using

'            If dsScale.Tables(0).Rows.Count <= 0 Then Return True

'            For Each xRow As DataRow In dsScale.Tables(0).Rows
'                Application.DoEvents()
'                objScale = New clsAssessment_Scale
'                If objScale.isExist(mintNewPeriodId, xRow.Item("scale"), xRow.Item("description"), xRow.Item("scalemasterunkid")) = False Then

'                    objScale._Description = xRow.Item("description")
'                    objScale._Isactive = True
'                    objScale._Periodunkid = mintNewPeriodId
'                    objScale._Scale = xRow.Item("scale")
'                    objScale._Scalemasterunkid = xRow.Item("scalemasterunkid")

'                    If objScale.Insert() = False Then
'                        If objScale._Message <> "" Then
'                            mstrMessage = objScale._Message
'                            Continue For
'                        End If
'                    End If
'                End If
'                objScale = Nothing
'            Next
'            xPB.Value += 1
'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Transfer_Scale_Master", mstrModuleName)
'        Finally
'            objScale = Nothing
'        End Try
'    End Function

'    Private Function Transfer_Scale_Mapping() As Boolean
'        Dim StrQ As String = String.Empty
'        Dim dsMapping As New DataSet
'        Dim objMapping As clsassess_scalemapping_tran
'        Try
'            Using objDo As New clsDataOperation
'                StrQ = "SELECT perspectiveunkid,scalemasterunkid,mappingunkid FROM hrassess_scalemapping_tran WHERE isvoid = 0 AND periodunkid = @periodunkid "

'                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldPeriodId)

'                dsMapping = objDo.ExecQuery(StrQ, "List")

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If
'            End Using
'            If dsMapping.Tables(0).Rows.Count <= 0 Then Return True
'            For Each xRow As DataRow In dsMapping.Tables(0).Rows
'                Application.DoEvents()
'                objMapping = New clsassess_scalemapping_tran

'                If objMapping.isExist(mintNewPeriodId, xRow.Item("perspectiveunkid"), xRow.Item("scalemasterunkid")) = False Then

'                    objMapping._Isvoid = False
'                    objMapping._Periodunkid = mintNewPeriodId
'                    objMapping._Perspectiveunkid = xRow.Item("perspectiveunkid")
'                    objMapping._Scalemasterunkid = xRow.Item("scalemasterunkid")
'                    objMapping._Mappingunkid = xRow.Item("mappingunkid")
'                    objMapping._Voiddatetime = Nothing
'                    objMapping._Voidreason = ""
'                    objMapping._Voiduserunkid = -1

'                    If objMapping.Insert() = False Then
'                        If objMapping._Message <> "" Then
'                            mstrMessage = objMapping._Message
'                            Continue For
'                        End If
'                    End If

'                End If

'                objMapping = Nothing
'            Next
'            xPB.Value += 1
'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Transfer_Scale_Mapping", mstrModuleName)
'        Finally
'            objMapping = Nothing
'        End Try
'    End Function

'    'S.SANDEEP [14 MAR 2016] -- START
'    'Private Function Transfer_Assessment_Ratio() As Boolean
'    '    Dim StrQ As String = String.Empty
'    '    Dim dsRatio As New DataSet
'    '    Dim objRatio As clsAssessment_Ratio
'    '    Try
'    '        Using objDo As New clsDataOperation
'    '            StrQ = "SELECT " & _
'    '                   "	jobgroupunkid,ge_weight,bsc_weight " & _
'    '                   "FROM hrassessment_ratio " & _
'    '                   "WHERE periodunkid = @xPeriodId AND isactive = 1 "

'    '            objDo.AddParameter("@xPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldPeriodId)

'    '            dsRatio = objDo.ExecQuery(StrQ, "List")

'    '            If objDo.ErrorMessage <> "" Then
'    '                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'    '            End If
'    '        End Using
'    '        If dsRatio.Tables(0).Rows.Count <= 0 Then Return True

'    '        For Each xRow As DataRow In dsRatio.Tables(0).Rows
'    '            Application.DoEvents()
'    '            objRatio = New clsAssessment_Ratio
'    '            If objRatio.isExist(xRow.Item("jobgroupunkid"), mintNewPeriodId) = False Then

'    '                objRatio._Periodunkid = mintNewPeriodId
'    '                objRatio._Jobgroupunkid = xRow.Item("jobgroupunkid")
'    '                objRatio._Bsc_Weight = xRow.Item("bsc_weight")
'    '                objRatio._Ge_Weight = xRow.Item("ge_weight")
'    '                objRatio._Isactive = True

'    '                If objRatio.Insert() = False Then
'    '                    If objRatio._Message <> "" Then
'    '                        mstrMessage = objRatio._Message
'    '                        Continue For
'    '                    End If
'    '                End If
'    '            End If
'    '            objRatio = Nothing
'    '        Next
'    '        xPB.Value += 1
'    '        Return True
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "Transfer_Assessment_Ratio", mstrModuleName)
'    '    Finally
'    '        objRatio = Nothing
'    '    End Try
'    'End Function
'    'S.SANDEEP [14 MAR 2016] -- END

'    Private Function Transfer_Field_Mapping() As Boolean
'        Dim objFMapping As clsAssess_Field_Mapping
'        Dim StrQ As String = String.Empty
'        Dim xFieldId As Integer = 0 : Dim xWeight As Decimal = 0
'        Try
'            Using objDo As New clsDataOperation
'                StrQ = "SELECT @xFieldId = fieldunkid, @xWeight = weight FROM hrassess_field_mapping WHERE periodunkid = @xPeriodId AND isactive = 1 "

'                objDo.AddParameter("@xFieldId", SqlDbType.Int, eZeeDataType.INT_SIZE, xFieldId, ParameterDirection.InputOutput)
'                objDo.AddParameter("@xWeight", SqlDbType.Float, eZeeDataType.FLOAT_SIZE, xWeight, ParameterDirection.InputOutput)
'                objDo.AddParameter("@xPeriodId", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldPeriodId)

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                xFieldId = objDo.GetParameterValue("@xFieldId")
'                xWeight = objDo.GetParameterValue("@xWeight")
'            End Using

'            objFMapping = New clsAssess_Field_Mapping
'            Application.DoEvents()
'            If objFMapping.isExist(mintNewPeriodId) = False Then
'                objFMapping._Fieldunkid = xFieldId
'                objFMapping._Isactive = True
'                objFMapping._Periodunkid = mintNewPeriodId
'                objFMapping._Userunkid = User._Object._Userunkid
'                objFMapping._Weight = xWeight

'                If objFMapping.Insert() = False Then
'                    If objFMapping._Message <> "" Then
'                        mstrMessage = objFMapping._Message
'                    End If
'                    Return False
'                End If
'            End If
'            objFMapping = Nothing
'            xPB.Value += 1
'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Transfer_Field_Mapping", mstrModuleName)
'        Finally
'            objFMapping = Nothing
'        End Try
'    End Function

'    'S.SANDEEP [03 JUN 2016] -- START

'    Private Function Transfer_Company_Level_Data() As Boolean
'        Try
'            Dim objFMapping As New clsAssess_Field_Mapping
'            Dim xLinkedFieldId As Integer = objFMapping.Get_Map_FieldId(mintNewPeriodId)
'            objFMapping = Nothing
'            Dim objFMaster As New clsAssess_Field_Master
'            Dim xExOrder As Integer = objFMaster.Get_Field_ExOrder(xLinkedFieldId)
'            objFMaster = Nothing

'            'Shani (15-Nov-2016) -- Start
'            'Enhancement - 
'            If xLinkedFieldId <= 0 Then Return True
'            'Shani (15-Nov-2016) -- End


'            Using objDo As New clsDataOperation
'                Dim StrQ As String = String.Empty
'                StrQ = "INSERT INTO hrassess_coyfield1_master ( " & _
'                       "    perspectiveunkid " & _
'                       "    ,fieldunkid " & _
'                       "    ,field_data " & _
'                       "    ,periodunkid " & _
'                       "    ,startdate " & _
'                       "    ,enddate " & _
'                       "    ,statusunkid " & _
'                       "    ,ownerrefid " & _
'                       "    ,userunkid " & _
'                       "    ,weight " & _
'                       "    ,isvoid " & _
'                       "    ,voiduserunkid " & _
'                       "    ,voidreason " & _
'                       "    ,voiddatetime " & _
'                       ") SELECT " & _
'                       "    perspectiveunkid " & _
'                       "    ,fieldunkid " & _
'                       "    ,field_data " & _
'                       "    ,'" & mintNewPeriodId & "' " & _
'                       "    ,startdate " & _
'                       "    ,enddate " & _
'                       "    ,statusunkid " & _
'                       "    ,ownerrefid " & _
'                       "    ,userunkid " & _
'                       "    ,weight " & _
'                       "    ,isvoid " & _
'                       "    ,voiduserunkid " & _
'                       "    ,voidreason " & _
'                       "    ,voiddatetime " & _
'                       "FROM hrassess_coyfield1_master " & _
'                       "WHERE hrassess_coyfield1_master.isvoid = 0  AND hrassess_coyfield1_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                       "    AND NOT EXISTS(SELECT * FROM hrassess_coyfield1_master AS CF1 WHERE CF1.isvoid = 0 AND CF1.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_coyfield2_master ( " & _
'                       "     coyfield1unkid " & _
'                       "    ,fieldunkid " & _
'                       "    ,field_data " & _
'                       "    ,userunkid " & _
'                       "    ,weight " & _
'                       "    ,periodunkid " & _
'                       "    ,startdate " & _
'                       "    ,enddate " & _
'                       "    ,statusunkid " & _
'                       "    ,ownerrefid " & _
'                       "    ,isvoid " & _
'                       "    ,voiduserunkid " & _
'                       "    ,voidreason " & _
'                       "    ,voiddatetime " & _
'                       ") SELECT " & _
'                       "     coyfield1unkid " & _
'                       "    ,fieldunkid " & _
'                       "    ,field_data " & _
'                       "    ,userunkid " & _
'                       "    ,weight " & _
'                        "    ,'" & mintNewPeriodId & "' " & _
'                       "    ,startdate " & _
'                       "    ,enddate " & _
'                       "    ,statusunkid " & _
'                       "    ,ownerrefid " & _
'                       "    ,isvoid " & _
'                       "    ,voiduserunkid " & _
'                       "    ,voidreason " & _
'                       "    ,voiddatetime " & _
'                       "FROM hrassess_coyfield2_master " & _
'                       "WHERE hrassess_coyfield2_master.isvoid = 0  AND hrassess_coyfield2_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                       "    AND NOT EXISTS(SELECT * FROM hrassess_coyfield2_master AS CF2 WHERE CF2.isvoid = 0 AND CF2.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_coyfield2_master " & _
'                       "SET coyfield1unkid = ISNULL(C.nId,0) " & _
'                       "FROM " & _
'                       "( " & _
'                       "    SELECT " & _
'                       "        hrassess_coyfield1_master.coyfield1unkid AS nId " & _
'                       "        ,B.coyfield1unkid AS oId " & _
'                       "        ,hrassess_coyfield1_master.field_data " & _
'                       "        ,B.periodunkid " & _
'                       "    FROM hrassess_coyfield1_master " & _
'                       "        JOIN " & _
'                       "        ( " & _
'                       "            SELECT DISTINCT " & _
'                       "                hrassess_coyfield2_master.coyfield1unkid " & _
'                       "                ,hrassess_coyfield1_master.field_data " & _
'                       "                ,hrassess_coyfield2_master.periodunkid " & _
'                       "                ,hrassess_coyfield1_master.perspectiveunkid " & _
'                       "                ,hrassess_coyfield1_master.fieldunkid " & _
'                       "            FROM hrassess_coyfield2_master " & _
'                       "                JOIN hrassess_coyfield1_master ON hrassess_coyfield1_master.coyfield1unkid = hrassess_coyfield2_master.coyfield1unkid " & _
'                       "            WHERE hrassess_coyfield2_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       "                AND hrassess_coyfield2_master.isvoid = 0 " & _
'                       "                AND hrassess_coyfield1_master.isvoid = 0 " & _
'                       "        ) AS B ON B.field_data = hrassess_coyfield1_master.field_data " & _
'                       "            AND B.periodunkid = hrassess_coyfield1_master.periodunkid " & _
'                       "            AND B.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid " & _
'                       "            AND B.fieldunkid = hrassess_coyfield1_master.fieldunkid " & _
'                       "        WHERE hrassess_coyfield1_master.isvoid = 0 " & _
'                       "            AND hrassess_coyfield1_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       ")AS C WHERE C.periodunkid = hrassess_coyfield2_master.periodunkid " & _
'                       "    AND C.oId = hrassess_coyfield2_master.coyfield1unkid "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_coyfield3_master ( " & _
'                       "    coyfield2unkid " & _
'                       "    ,fieldunkid " & _
'                       "    ,field_data " & _
'                       "    ,userunkid " & _
'                       "    ,weight " & _
'                       "    ,periodunkid " & _
'                       "    ,startdate " & _
'                       "    ,enddate " & _
'                       "    ,statusunkid " & _
'                       "    ,ownerrefid " & _
'                       "    ,isvoid " & _
'                       "    ,voiduserunkid " & _
'                       "    ,voidreason " & _
'                       "    ,voiddatetime " & _
'                       ") " & _
'                       "    SELECT " & _
'                       "        coyfield2unkid " & _
'                       "        ,fieldunkid " & _
'                       "        ,field_data " & _
'                       "        ,userunkid " & _
'                       "        ,weight " & _
'                       "        ,'" & mintNewPeriodId & "' " & _
'                       "        ,startdate " & _
'                       "        ,enddate " & _
'                       "        ,statusunkid " & _
'                       "        ,ownerrefid " & _
'                       "        ,isvoid " & _
'                       "        ,voiduserunkid " & _
'                       "        ,voidreason " & _
'                       "        ,voiddatetime " & _
'                       "FROM hrassess_coyfield3_master " & _
'                       "WHERE hrassess_coyfield3_master.isvoid = 0  AND hrassess_coyfield3_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                       "    AND NOT EXISTS(SELECT * FROM hrassess_coyfield3_master AS CF3 WHERE CF3.isvoid = 0 AND CF3.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_coyfield3_master " & _
'                       "SET coyfield2unkid = ISNULL(C.nId,0) " & _
'                       "FROM " & _
'                       "( " & _
'                       "    SELECT " & _
'                       "        hrassess_coyfield2_master.coyfield2unkid AS nId " & _
'                       "        ,B.coyfield2unkid AS oId " & _
'                       "        ,hrassess_coyfield2_master.field_data " & _
'                       "        ,B.periodunkid " & _
'                       "    FROM hrassess_coyfield2_master " & _
'                       "        JOIN " & _
'                       "        ( " & _
'                       "            SELECT DISTINCT " & _
'                       "                hrassess_coyfield3_master.coyfield2unkid " & _
'                       "                ,hrassess_coyfield2_master.field_data " & _
'                       "                ,hrassess_coyfield3_master.periodunkid " & _
'                       "                ,hrassess_coyfield2_master.fieldunkid " & _
'                       "            FROM hrassess_coyfield3_master " & _
'                       "                JOIN hrassess_coyfield2_master ON hrassess_coyfield2_master.coyfield2unkid = hrassess_coyfield3_master.coyfield2unkid " & _
'                       "            WHERE hrassess_coyfield3_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       "                AND hrassess_coyfield3_master.isvoid = 0 " & _
'                       "                AND hrassess_coyfield2_master.isvoid = 0 " & _
'                       "        ) AS B ON B.field_data = hrassess_coyfield2_master.field_data " & _
'                       "            AND B.periodunkid = hrassess_coyfield2_master.periodunkid " & _
'                       "            AND B.fieldunkid = hrassess_coyfield2_master.fieldunkid " & _
'                       "        WHERE hrassess_coyfield2_master.isvoid = 0 " & _
'                       "            AND hrassess_coyfield2_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       ")AS C WHERE C.periodunkid = hrassess_coyfield3_master.periodunkid " & _
'                       "    AND C.oId = hrassess_coyfield3_master.coyfield2unkid "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_coyfield4_master ( " & _
'                       "     coyfield3unkid " & _
'                       "    ,fieldunkid " & _
'                       "    ,field_data " & _
'                       "    ,userunkid " & _
'                       "    ,weight " & _
'                       "    ,periodunkid " & _
'                       "    ,startdate " & _
'                       "    ,enddate " & _
'                       "    ,statusunkid " & _
'                       "    ,ownerrefid " & _
'                       "    ,isvoid " & _
'                       "    ,voiduserunkid " & _
'                       "    ,voidreason " & _
'                       "    ,voiddatetime " & _
'                       ") " & _
'                       "    SELECT " & _
'                       "         coyfield3unkid " & _
'                       "        ,fieldunkid " & _
'                       "        ,field_data " & _
'                       "        ,userunkid " & _
'                       "        ,weight " & _
'                       "        ,'" & mintNewPeriodId & "' " & _
'                       "        ,startdate " & _
'                       "        ,enddate " & _
'                       "        ,statusunkid " & _
'                       "        ,ownerrefid " & _
'                       "        ,isvoid " & _
'                       "        ,voiduserunkid " & _
'                       "        ,voidreason " & _
'                       "        ,voiddatetime " & _
'                       "    FROM hrassess_coyfield4_master " & _
'                       "    WHERE hrassess_coyfield4_master.isvoid = 0  AND hrassess_coyfield4_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                       "        AND NOT EXISTS(SELECT * FROM hrassess_coyfield4_master AS CF4 WHERE CF4.isvoid = 0 AND CF4.periodunkid = '" & mintNewPeriodId & "'); "
'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_coyfield4_master " & _
'                       "SET coyfield3unkid = ISNULL(C.nId,0) " & _
'                       "FROM " & _
'                       "( " & _
'                       "    SELECT " & _
'                       "        hrassess_coyfield3_master.coyfield3unkid AS nId " & _
'                       "        ,B.coyfield3unkid  AS oId " & _
'                       "        ,hrassess_coyfield3_master.field_data " & _
'                       "        ,B.periodunkid " & _
'                       "    FROM hrassess_coyfield3_master " & _
'                       "        JOIN " & _
'                       "        ( " & _
'                       "            SELECT DISTINCT " & _
'                       "                hrassess_coyfield4_master.coyfield3unkid " & _
'                       "                ,hrassess_coyfield3_master.field_data " & _
'                       "                ,hrassess_coyfield4_master.periodunkid " & _
'                       "                ,hrassess_coyfield3_master.fieldunkid " & _
'                       "            FROM hrassess_coyfield4_master " & _
'                       "                JOIN hrassess_coyfield3_master ON hrassess_coyfield3_master.coyfield3unkid = hrassess_coyfield4_master.coyfield3unkid " & _
'                       "            WHERE hrassess_coyfield4_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       "                AND hrassess_coyfield4_master.isvoid = 0 " & _
'                       "                AND hrassess_coyfield3_master.isvoid = 0 " & _
'                       "        ) AS B ON B.field_data = hrassess_coyfield3_master.field_data " & _
'                       "            AND B.periodunkid = hrassess_coyfield3_master.periodunkid " & _
'                       "            AND B.fieldunkid = hrassess_coyfield3_master.fieldunkid " & _
'                       "    WHERE hrassess_coyfield3_master.isvoid = 0 " & _
'                       "        AND hrassess_coyfield3_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       ")AS C WHERE C.periodunkid = hrassess_coyfield4_master.periodunkid " & _
'                       "    AND C.oId = hrassess_coyfield4_master.coyfield3unkid "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_coyfield5_master ( " & _
'                       "     coyfield4unkid " & _
'                       "    ,fieldunkid " & _
'                       "    ,field_data " & _
'                       "    ,userunkid " & _
'                       "    ,weight " & _
'                       "    ,periodunkid " & _
'                       "    ,startdate " & _
'                       "    ,enddate " & _
'                       "    ,statusunkid " & _
'                       "    ,ownerrefid " & _
'                       "    ,isvoid " & _
'                       "    ,voiduserunkid " & _
'                       "    ,voidreason " & _
'                       "    ,voiddatetime " & _
'                       ") " & _
'                       "    SELECT " & _
'                       "        coyfield4unkid " & _
'                       "        ,fieldunkid " & _
'                       "        ,field_data " & _
'                       "        ,userunkid " & _
'                       "        ,weight " & _
'                       "        ,'" & mintNewPeriodId & "' " & _
'                       "        ,startdate " & _
'                       "        ,enddate " & _
'                       "        ,statusunkid " & _
'                       "        ,ownerrefid " & _
'                       "        ,isvoid " & _
'                       "        ,voiduserunkid " & _
'                       "        ,voidreason " & _
'                       "        ,voiddatetime " & _
'                       "    FROM hrassess_coyfield5_master " & _
'                       "    WHERE hrassess_coyfield5_master.isvoid = 0  AND hrassess_coyfield5_master.periodunkid =  '" & mintOldPeriodId & "' " & _
'                       "        AND NOT EXISTS(SELECT * FROM hrassess_coyfield5_master AS CF5 WHERE CF5.isvoid = 0 AND CF5.periodunkid = '" & mintNewPeriodId & "'); "
'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_coyfield5_master " & _
'                       "SET coyfield4unkid = ISNULL(C.nId,0) " & _
'                       "FROM " & _
'                       "( " & _
'                       "    SELECT " & _
'                       "        hrassess_coyfield4_master.coyfield4unkid AS nId " & _
'                       "        ,B.coyfield4unkid  AS oId " & _
'                       "        ,hrassess_coyfield4_master.field_data " & _
'                       "        ,B.periodunkid " & _
'                       "    FROM hrassess_coyfield4_master " & _
'                       "        JOIN " & _
'                       "            ( " & _
'                       "                SELECT DISTINCT " & _
'                       "                    hrassess_coyfield5_master.coyfield4unkid " & _
'                       "                    ,hrassess_coyfield4_master.field_data " & _
'                       "                    ,hrassess_coyfield5_master.periodunkid " & _
'                       "                    ,hrassess_coyfield4_master.fieldunkid " & _
'                       "                FROM hrassess_coyfield5_master " & _
'                       "                    JOIN hrassess_coyfield4_master ON hrassess_coyfield4_master.coyfield4unkid = hrassess_coyfield5_master.coyfield4unkid " & _
'                       "                WHERE hrassess_coyfield5_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       "                    AND hrassess_coyfield5_master.isvoid = 0 " & _
'                       "                    AND hrassess_coyfield4_master.isvoid = 0 " & _
'                       "            ) AS B ON B.field_data = hrassess_coyfield4_master.field_data " & _
'                       "                AND B.periodunkid = hrassess_coyfield4_master.periodunkid " & _
'                       "                AND B.fieldunkid = hrassess_coyfield4_master.fieldunkid " & _
'                       "    WHERE hrassess_coyfield4_master.isvoid = 0 " & _
'                       "    AND hrassess_coyfield4_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       ")AS C WHERE C.periodunkid = hrassess_coyfield5_master.periodunkid " & _
'                       "    AND C.oId = hrassess_coyfield5_master.coyfield4unkid "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                Dim xTableName, xColName, xParentCol, xParentTabName As String
'                Dim xFieldTypeId As Integer = 0
'                xTableName = "" : xColName = "" : xParentCol = "" : xParentTabName = ""

'                Select Case xExOrder
'                    Case enWeight_Types.WEIGHT_FIELD1
'                        xTableName = "hrassess_coyfield1_master"
'                        xColName = "coyfield1unkid"
'                        xParentCol = "coyfield1unkid"
'                        xParentTabName = "hrassess_coyfield1_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD1
'                    Case enWeight_Types.WEIGHT_FIELD2
'                        xTableName = "hrassess_coyfield2_master"
'                        xColName = "coyfield2unkid"
'                        xParentCol = "coyfield1unkid"
'                        xParentTabName = "hrassess_coyfield2_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD2
'                    Case enWeight_Types.WEIGHT_FIELD3
'                        xTableName = "hrassess_coyfield3_master"
'                        xColName = "coyfield3unkid"
'                        xParentCol = "coyfield2unkid"
'                        xParentTabName = "hrassess_coyfield3_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD3
'                    Case enWeight_Types.WEIGHT_FIELD4
'                        xTableName = "hrassess_coyfield4_master"
'                        xColName = "coyfield4unkid"
'                        xParentCol = "coyfield3unkid"
'                        xParentTabName = "hrassess_coyfield4_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD4
'                    Case enWeight_Types.WEIGHT_FIELD5
'                        xTableName = "hrassess_coyfield5_master"
'                        xColName = "coyfield5unkid"
'                        xParentCol = "coyfield4unkid"
'                        xParentTabName = "hrassess_coyfield5_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD5
'                End Select

'                StrQ = "INSERT INTO hrassess_coyinfofield_tran " & _
'                       "( " & _
'                       "    coyfieldunkid " & _
'                       "   ,fieldunkid " & _
'                       "   ,field_data " & _
'                       "   ,coyfieldtypeid " & _
'                       ") " & _
'                       "SELECT " & _
'                       "    " & xTableName & "." & xColName & " AS nId " & _
'                       "   ,B.fieldunkid " & _
'                       "   ,B.infodata " & _
'                       "   ,B.coyfieldtypeid " & _
'                       "FROM " & xTableName & " " & _
'                       "   JOIN " & _
'                       "   ( " & _
'                       "       SELECT DISTINCT " & _
'                       "            hrassess_coyinfofield_tran.coyfieldunkid " & _
'                       "           ," & xTableName & ".field_data " & _
'                       "           ," & xTableName & ".periodunkid " & _
'                       "           ,hrassess_coyinfofield_tran.fieldunkid " & _
'                       "           ,hrassess_coyinfofield_tran.field_data AS infodata " & _
'                       "           ,hrassess_coyinfofield_tran.coyfieldtypeid " & _
'                       "       FROM hrassess_coyinfofield_tran " & _
'                       "           JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = hrassess_coyinfofield_tran.coyfieldunkid " & _
'                       "       WHERE " & xTableName & ".periodunkid = '" & mintOldPeriodId & "' AND " & xTableName & ".isvoid = 0 AND hrassess_coyinfofield_tran.coyfieldtypeid = " & xFieldTypeId & " " & _
'                       "   ) AS B ON B.field_data = " & xTableName & ".field_data " & _
'                       "WHERE " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = '" & mintNewPeriodId & "' " & _
'                       "   AND NOT EXISTS(SELECT * FROM hrassess_coyinfofield_tran AS CF WHERE CF.coyfieldunkid = " & xTableName & "." & xColName & " " & _
'                       "   AND CF.coyfieldtypeid = " & xFieldTypeId & ") "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_coyowner_tran " & _
'                       "( " & _
'                       "     coyfieldunkid " & _
'                       "    ,allocationid " & _
'                       "    ,coyfieldtypeid " & _
'                       ") " & _
'                       "SELECT " & _
'                       "     " & xTableName & "." & xColName & " AS nId " & _
'                       "    ,B.allocationid " & _
'                       "    ,B.coyfieldtypeid " & _
'                       "FROM " & xTableName & " " & _
'                       "    JOIN " & _
'                       "    ( " & _
'                       "        SELECT " & _
'                       "             hrassess_coyowner_tran.coyfieldunkid " & _
'                       "            ," & xTableName & ".field_data " & _
'                       "            ," & xTableName & ".periodunkid " & _
'                       "            ,hrassess_coyowner_tran.coyfieldtypeid " & _
'                       "            ,hrassess_coyowner_tran.allocationid " & _
'                       "        FROM hrassess_coyowner_tran " & _
'                       "            JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = hrassess_coyowner_tran.coyfieldunkid " & _
'                       "        WHERE " & xTableName & ".periodunkid = '" & mintOldPeriodId & "' AND " & xTableName & ".isvoid = 0 AND hrassess_coyowner_tran.coyfieldtypeid = " & xFieldTypeId & " " & _
'                       "    ) AS B ON B.field_data = " & xTableName & ".field_data " & _
'                       "WHERE " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = '" & mintNewPeriodId & "' " & _
'                       "    AND NOT EXISTS(SELECT * FROM hrassess_coyowner_tran AS CF WHERE CF.coyfieldunkid = " & xTableName & "." & xColName & " " & _
'                       "    AND CF.coyfieldtypeid = " & xFieldTypeId & ") "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If


'                'Shani (05-Sep-2016) -- Start
'                'Enhancement - Assessment Close Period to Save Last status data given by sandra (FZ)
'                StrQ = "INSERT INTO hrassess_coystatus_tran ( " & _
'                       "     statustypeid " & _
'                       "    ,status_date " & _
'                       "    ,commtents " & _
'                       "    ,userunkid " & _
'                       "    ,periodunkid " & _
'                       ") " & _
'                       "SELECT " & _
'                       "     A.statustypeid " & _
'                       "    ,GETDATE() " & _
'                       "    ,A.commtents " & _
'                       "    ,'" & mintUserUnkId & "' " & _
'                       "    ,'" & mintNewPeriodId & "' " & _
'                       "FROM " & _
'                       "( " & _
'                       "    SELECT " & _
'                       "        hrassess_coystatus_tran.status_date " & _
'                       "        ,hrassess_coystatus_tran.statustypeid " & _
'                       "        ,hrassess_coystatus_tran.commtents " & _
'                       "        ,ROW_NUMBER()OVER(PARTITION BY hrassess_coystatus_tran.periodunkid ORDER BY hrassess_coystatus_tran.status_date DESC) AS rno " & _
'                       "    FROM hrassess_coystatus_tran " & _
'                       "    WHERE hrassess_coystatus_tran.periodunkid = '" & mintOldPeriodId & "' " & _
'                       ") AS A WHERE A.rno = 1 "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If
'                'Shani (05-Sep-2016) -- End


'            End Using
'            xPB.Value += 1
'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Transfer_Company_Level_Data", mstrModuleName)
'        End Try
'    End Function

'    Private Function Tranfer_Owner_Level_Data() As Boolean
'        Try
'            Dim objFMapping As New clsAssess_Field_Mapping
'            Dim xLinkedFieldId As Integer = objFMapping.Get_Map_FieldId(mintNewPeriodId)
'            objFMapping = Nothing
'            Dim objFMaster As New clsAssess_Field_Master
'            Dim xExOrder As Integer = objFMaster.Get_Field_ExOrder(xLinkedFieldId)
'            objFMaster = Nothing

'            'Shani (15-Nov-2016) -- Start
'            'Enhancement - 
'            If xLinkedFieldId <= 0 Then Return True
'            'Shani (15-Nov-2016) -- End


'            Using objDo As New clsDataOperation
'                Dim StrQ As String = String.Empty
'                StrQ = "INSERT INTO hrassess_owrfield1_master " & _
'                       "( " & _
'                       "     coyfield1unkid " & _
'                       "    ,ownerrefid " & _
'                       "    ,ownerunkid " & _
'                       "    ,fieldunkid " & _
'                       "    ,field_data " & _
'                       "    ,yearunkid " & _
'                       "    ,periodunkid " & _
'                       "    ,weight " & _
'                       "    ,startdate " & _
'                       "    ,enddate " & _
'                       "    ,statusunkid " & _
'                       "    ,userunkid " & _
'                       "    ,isvoid " & _
'                       "    ,voiduserunkid " & _
'                       "    ,voidreason " & _
'                       "    ,voiddatetime " & _
'                       "    ,pct_completed " & _
'                       ") " & _
'                       "SELECT " & _
'                       "     coyfield1unkid " & _
'                       "    ,ownerrefid " & _
'                       "    ,ownerunkid " & _
'                       "    ,fieldunkid " & _
'                       "    ,field_data " & _
'                       "    ,yearunkid " & _
'                       "    ,'" & mintNewPeriodId & "' " & _
'                       "    ,weight " & _
'                       "    ,startdate " & _
'                       "    ,enddate " & _
'                       "    ,statusunkid " & _
'                       "    ,userunkid " & _
'                       "    ,isvoid " & _
'                       "    ,voiduserunkid " & _
'                       "    ,voidreason " & _
'                       "    ,voiddatetime " & _
'                       "    ,pct_completed " & _
'                       "FROM hrassess_owrfield1_master WHERE hrassess_owrfield1_master.isvoid = 0 " & _
'                       "    AND hrassess_owrfield1_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                       "    AND NOT EXISTS(SELECT * FROM hrassess_owrfield1_master AS OF1 WHERE OF1.isvoid = 0 AND OF1.ownerunkid = hrassess_owrfield1_master.ownerunkid AND OF1.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_owrfield1_master " & _
'                       "SET coyfield1unkid = ISNULL(C.nId,0) " & _
'                       "FROM " & _
'                       "( " & _
'                       "    SELECT " & _
'                       "        hrassess_coyfield1_master.coyfield1unkid AS nId " & _
'                       "        ,B.coyfield1unkid AS oId " & _
'                       "        ,hrassess_coyfield1_master.field_data " & _
'                       "        ,B.periodunkid " & _
'                       "    FROM hrassess_coyfield1_master " & _
'                       "        JOIN " & _
'                       "        ( " & _
'                       "            SELECT DISTINCT " & _
'                       "                hrassess_owrfield1_master.coyfield1unkid " & _
'                       "                ,hrassess_coyfield1_master.field_data " & _
'                       "                ,hrassess_owrfield1_master.periodunkid " & _
'                       "                ,hrassess_coyfield1_master.perspectiveunkid " & _
'                       "                ,hrassess_coyfield1_master.fieldunkid " & _
'                       "            FROM hrassess_owrfield1_master " & _
'                       "                JOIN hrassess_coyfield1_master ON hrassess_coyfield1_master.coyfield1unkid = hrassess_owrfield1_master.coyfield1unkid " & _
'                       "            WHERE hrassess_owrfield1_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       "                AND hrassess_owrfield1_master.isvoid = 0 " & _
'                       "                AND hrassess_coyfield1_master.isvoid = 0 " & _
'                       "        ) AS B ON B.field_data = hrassess_coyfield1_master.field_data " & _
'                       "            AND B.periodunkid = hrassess_coyfield1_master.periodunkid " & _
'                       "            AND B.perspectiveunkid = hrassess_coyfield1_master.perspectiveunkid " & _
'                       "            AND B.fieldunkid = hrassess_coyfield1_master.fieldunkid " & _
'                       "    WHERE hrassess_coyfield1_master.isvoid = 0 " & _
'                       "        AND hrassess_coyfield1_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       ")AS C WHERE C.periodunkid = hrassess_owrfield1_master.periodunkid " & _
'                       "    AND C.oId = hrassess_owrfield1_master.coyfield1unkid "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_owrfield2_master " & _
'                       "( " & _
'                       "     owrfield1unkid " & _
'                       "    ,ownerunkid " & _
'                       "    ,periodunkid " & _
'                       "    ,fieldunkid " & _
'                       "    ,field_data " & _
'                       "    ,weight " & _
'                       "    ,userunkid " & _
'                       "    ,isvoid " & _
'                       "    ,voiduserunkid " & _
'                       "    ,voidreason " & _
'                       "    ,voiddatetime " & _
'                       "    ,startdate " & _
'                       "    ,enddate " & _
'                       "    ,statusunkid " & _
'                       "    ,pct_completed " & _
'                       ") " & _
'                       "SELECT " & _
'                       "     owrfield1unkid " & _
'                       "    ,ownerunkid " & _
'                       "    ,'" & mintNewPeriodId & "' " & _
'                       "    ,fieldunkid " & _
'                       "    ,field_data " & _
'                       "    ,weight " & _
'                       "    ,userunkid " & _
'                       "    ,isvoid " & _
'                       "    ,voiduserunkid " & _
'                       "    ,voidreason " & _
'                       "    ,voiddatetime " & _
'                       "    ,startdate " & _
'                       "    ,enddate " & _
'                       "    ,statusunkid " & _
'                       "    ,pct_completed " & _
'                       "FROM hrassess_owrfield2_master WHERE hrassess_owrfield2_master.isvoid = 0 " & _
'                       "    AND hrassess_owrfield2_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                       "    AND NOT EXISTS(SELECT * FROM hrassess_owrfield2_master AS OF2 WHERE OF2.isvoid = 0 AND OF2.ownerunkid = hrassess_owrfield2_master.ownerunkid AND OF2.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_owrfield2_master " & _
'                       "SET owrfield1unkid = ISNULL(C.nId,0) " & _
'                       "FROM " & _
'                       "( " & _
'                       "    SELECT " & _
'                       "        hrassess_owrfield1_master.owrfield1unkid AS nId " & _
'                       "        ,B.owrfield1unkid AS oId " & _
'                       "        ,hrassess_owrfield1_master.field_data " & _
'                       "        ,B.periodunkid " & _
'                       "    FROM hrassess_owrfield1_master " & _
'                       "        JOIN " & _
'                       "        ( " & _
'                       "            SELECT DISTINCT " & _
'                       "                hrassess_owrfield2_master.owrfield1unkid " & _
'                       "                ,hrassess_owrfield1_master.field_data " & _
'                       "                ,hrassess_owrfield2_master.periodunkid " & _
'                       "                ,hrassess_owrfield1_master.ownerunkid " & _
'                       "                ,hrassess_owrfield1_master.fieldunkid " & _
'                       "            FROM hrassess_owrfield2_master " & _
'                       "                JOIN hrassess_owrfield1_master ON hrassess_owrfield1_master.owrfield1unkid = hrassess_owrfield2_master.owrfield1unkid " & _
'                       "            WHERE hrassess_owrfield2_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       "                AND hrassess_owrfield2_master.isvoid = 0 " & _
'                       "                AND hrassess_owrfield1_master.isvoid = 0 " & _
'                       "        ) AS B ON B.field_data = hrassess_owrfield1_master.field_data " & _
'                       "            AND B.periodunkid = hrassess_owrfield1_master.periodunkid " & _
'                       "            AND B.ownerunkid = hrassess_owrfield1_master.ownerunkid " & _
'                       "            AND B.fieldunkid = hrassess_owrfield1_master.fieldunkid " & _
'                       "    WHERE hrassess_owrfield1_master.isvoid = 0 " & _
'                       "        AND hrassess_owrfield1_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       ") AS C WHERE C.periodunkid = hrassess_owrfield2_master.periodunkid " & _
'                       "    AND C.oId = hrassess_owrfield2_master.owrfield1unkid "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_owrfield3_master " & _
'                        "( " & _
'                        "    owrfield2unkid " & _
'                        "    ,ownerunkid " & _
'                        "    ,periodunkid " & _
'                        "    ,fieldunkid " & _
'                        "    ,field_data " & _
'                        "    ,weight " & _
'                        "    ,startdate " & _
'                        "    ,enddate " & _
'                        "    ,statusunkid " & _
'                        "    ,userunkid " & _
'                        "    ,isvoid " & _
'                        "    ,voiduserunkid " & _
'                        "    ,voidreason " & _
'                        "    ,voiddatetime " & _
'                        "    ,pct_completed " & _
'                        ") " & _
'                        "SELECT " & _
'                        "    owrfield2unkid " & _
'                        "    ,ownerunkid " & _
'                        "    ,'" & mintNewPeriodId & "' " & _
'                        "    ,fieldunkid " & _
'                        "    ,field_data " & _
'                        "    ,weight " & _
'                        "    ,startdate " & _
'                        "    ,enddate " & _
'                        "    ,statusunkid " & _
'                        "    ,userunkid " & _
'                        "    ,isvoid " & _
'                        "    ,voiduserunkid " & _
'                        "    ,voidreason " & _
'                        "    ,voiddatetime " & _
'                        "    ,pct_completed " & _
'                        "FROM hrassess_owrfield3_master WHERE hrassess_owrfield3_master.isvoid = 0 " & _
'                        "   AND hrassess_owrfield3_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                        "   AND NOT EXISTS(SELECT * FROM hrassess_owrfield3_master AS OF3 WHERE OF3.isvoid = 0 AND OF3.ownerunkid = hrassess_owrfield3_master.ownerunkid AND OF3.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_owrfield3_master " & _
'                       "SET owrfield2unkid = ISNULL(C.nId,0) " & _
'                       "FROM " & _
'                       "( " & _
'                       "    SELECT " & _
'                       "         hrassess_owrfield2_master.owrfield2unkid AS nId " & _
'                       "        ,B.owrfield2unkid AS oId " & _
'                       "        ,hrassess_owrfield2_master.field_data " & _
'                       "        ,B.periodunkid " & _
'                       "    FROM hrassess_owrfield2_master " & _
'                       "        JOIN " & _
'                       "        ( " & _
'                       "            SELECT DISTINCT " & _
'                       "                 hrassess_owrfield3_master.owrfield2unkid " & _
'                       "                ,hrassess_owrfield2_master.field_data " & _
'                       "                ,hrassess_owrfield3_master.periodunkid " & _
'                       "                ,hrassess_owrfield2_master.ownerunkid " & _
'                       "                ,hrassess_owrfield2_master.fieldunkid " & _
'                       "            FROM hrassess_owrfield3_master " & _
'                       "                JOIN hrassess_owrfield2_master ON hrassess_owrfield2_master.owrfield2unkid = hrassess_owrfield3_master.owrfield2unkid " & _
'                       "            WHERE hrassess_owrfield3_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       "                AND hrassess_owrfield3_master.isvoid = 0 " & _
'                       "                AND hrassess_owrfield2_master.isvoid = 0 " & _
'                       "        ) AS B ON B.field_data = hrassess_owrfield2_master.field_data " & _
'                       "            AND B.periodunkid = hrassess_owrfield2_master.periodunkid " & _
'                       "            AND B.ownerunkid = hrassess_owrfield2_master.ownerunkid " & _
'                       "            AND B.fieldunkid = hrassess_owrfield2_master.fieldunkid " & _
'                       "    WHERE hrassess_owrfield2_master.isvoid = 0 " & _
'                       "        AND hrassess_owrfield2_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       ") AS C WHERE C.periodunkid = hrassess_owrfield3_master.periodunkid " & _
'                       "    AND C.oId = hrassess_owrfield3_master.owrfield2unkid "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If


'                StrQ = "INSERT INTO hrassess_owrfield4_master " & _
'                        "( " & _
'                        "     owrfield3unkid " & _
'                        "    ,ownerunkid " & _
'                        "    ,periodunkid " & _
'                        "    ,fieldunkid " & _
'                        "    ,field_data " & _
'                        "    ,weight " & _
'                        "    ,startdate " & _
'                        "    ,enddate " & _
'                        "    ,statusunkid " & _
'                        "    ,userunkid " & _
'                        "    ,isvoid " & _
'                        "    ,voiduserunkid " & _
'                        "    ,voidreason " & _
'                        "    ,voiddatetime " & _
'                        "    ,pct_completed " & _
'                        ") " & _
'                        "SELECT " & _
'                        "    owrfield3unkid " & _
'                        "    ,ownerunkid " & _
'                        "    ,'" & mintNewPeriodId & "' " & _
'                        "    ,fieldunkid " & _
'                        "    ,field_data " & _
'                        "    ,weight " & _
'                        "    ,startdate " & _
'                        "    ,enddate " & _
'                        "    ,statusunkid " & _
'                        "    ,userunkid " & _
'                        "    ,isvoid " & _
'                        "    ,voiduserunkid " & _
'                        "    ,voidreason " & _
'                        "    ,voiddatetime " & _
'                        "    ,pct_completed " & _
'                        "FROM hrassess_owrfield4_master WHERE hrassess_owrfield4_master.isvoid = 0 " & _
'                        "   AND hrassess_owrfield4_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                        "   AND NOT EXISTS(SELECT * FROM hrassess_owrfield4_master AS OF4 WHERE OF4.isvoid = 0 AND OF4.ownerunkid = hrassess_owrfield4_master.ownerunkid AND OF4.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_owrfield4_master " & _
'                       "SET owrfield3unkid = ISNULL(C.nId,0) " & _
'                       "FROM " & _
'                       "( " & _
'                       "    SELECT " & _
'                       "        hrassess_owrfield3_master.owrfield3unkid AS nId " & _
'                       "        ,B.owrfield3unkid AS oId " & _
'                       "        ,hrassess_owrfield3_master.field_data " & _
'                       "        ,B.periodunkid " & _
'                       "    FROM hrassess_owrfield3_master " & _
'                       "        JOIN " & _
'                       "        ( " & _
'                       "            SELECT DISTINCT " & _
'                       "                hrassess_owrfield4_master.owrfield3unkid " & _
'                       "                ,hrassess_owrfield3_master.field_data " & _
'                       "                ,hrassess_owrfield4_master.periodunkid " & _
'                       "                ,hrassess_owrfield3_master.ownerunkid " & _
'                       "                ,hrassess_owrfield3_master.fieldunkid " & _
'                       "            FROM hrassess_owrfield4_master " & _
'                       "                JOIN hrassess_owrfield3_master ON hrassess_owrfield3_master.owrfield3unkid = hrassess_owrfield4_master.owrfield3unkid " & _
'                       "            WHERE hrassess_owrfield4_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       "                AND hrassess_owrfield4_master.isvoid = 0 " & _
'                       "                AND hrassess_owrfield3_master.isvoid = 0 " & _
'                       "        ) AS B ON B.field_data = hrassess_owrfield3_master.field_data " & _
'                       "            AND B.periodunkid = hrassess_owrfield3_master.periodunkid " & _
'                       "            AND B.ownerunkid = hrassess_owrfield3_master.ownerunkid " & _
'                       "            AND B.fieldunkid = hrassess_owrfield3_master.fieldunkid " & _
'                       "    WHERE hrassess_owrfield3_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       ") AS C WHERE C.periodunkid = hrassess_owrfield4_master.periodunkid " & _
'                       "    AND C.oId = hrassess_owrfield4_master.owrfield3unkid "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_owrfield5_master " & _
'                        "( " & _
'                        "    owrfield4unkid " & _
'                        "    ,ownerunkid " & _
'                        "    ,periodunkid " & _
'                        "    ,fieldunkid " & _
'                        "    ,field_data " & _
'                        "    ,weight " & _
'                        "    ,startdate " & _
'                        "    ,enddate " & _
'                        "    ,statusunkid " & _
'                        "    ,userunkid " & _
'                        "    ,isvoid " & _
'                        "    ,voiduserunkid " & _
'                        "    ,voidreason " & _
'                        "    ,voiddatetime " & _
'                        "    ,pct_completed " & _
'                        ") " & _
'                        "SELECT " & _
'                        "    owrfield4unkid " & _
'                        "    ,ownerunkid " & _
'                        "    ,'" & mintNewPeriodId & "' " & _
'                        "    ,fieldunkid " & _
'                        "    ,field_data " & _
'                        "    ,weight " & _
'                        "    ,startdate " & _
'                        "    ,enddate " & _
'                        "    ,statusunkid " & _
'                        "    ,userunkid " & _
'                        "    ,isvoid " & _
'                        "    ,voiduserunkid " & _
'                        "    ,voidreason " & _
'                        "    ,voiddatetime " & _
'                        "    ,pct_completed " & _
'                        "FROM hrassess_owrfield5_master WHERE hrassess_owrfield5_master.isvoid = 0 " & _
'                        "   AND hrassess_owrfield5_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                        "   AND NOT EXISTS(SELECT * FROM hrassess_owrfield5_master AS OF5 WHERE OF5.isvoid = 0 AND OF5.ownerunkid = hrassess_owrfield5_master.ownerunkid AND OF5.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_owrfield5_master " & _
'                       "SET owrfield4unkid = ISNULL(C.nId,0) " & _
'                       "FROM " & _
'                       "( " & _
'                       "    SELECT " & _
'                       "        hrassess_owrfield4_master.owrfield4unkid AS nId " & _
'                       "        ,B.owrfield4unkid AS oId " & _
'                       "        ,hrassess_owrfield4_master.field_data " & _
'                       "        ,B.periodunkid " & _
'                       "    FROM hrassess_owrfield4_master " & _
'                       "        JOIN " & _
'                       "        ( " & _
'                       "            SELECT DISTINCT " & _
'                       "                hrassess_owrfield5_master.owrfield4unkid " & _
'                       "                ,hrassess_owrfield4_master.field_data " & _
'                       "                ,hrassess_owrfield5_master.periodunkid " & _
'                       "                ,hrassess_owrfield4_master.ownerunkid " & _
'                       "                ,hrassess_owrfield4_master.fieldunkid " & _
'                       "            FROM hrassess_owrfield5_master " & _
'                       "                JOIN hrassess_owrfield4_master ON hrassess_owrfield4_master.owrfield4unkid = hrassess_owrfield5_master.owrfield4unkid " & _
'                       "            WHERE hrassess_owrfield5_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       "                AND hrassess_owrfield5_master.isvoid = 0 " & _
'                       "                AND hrassess_owrfield4_master.isvoid = 0 " & _
'                       "        ) AS B ON B.field_data = hrassess_owrfield4_master.field_data " & _
'                       "            AND B.periodunkid = hrassess_owrfield4_master.periodunkid " & _
'                       "            AND B.ownerunkid = hrassess_owrfield4_master.ownerunkid " & _
'                       "            AND B.fieldunkid = hrassess_owrfield4_master.fieldunkid " & _
'                       "    WHERE hrassess_owrfield4_master.isvoid = 0 " & _
'                       "        AND hrassess_owrfield4_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                       ") AS C WHERE C.periodunkid = hrassess_owrfield5_master.periodunkid " & _
'                       "    AND C.oId = hrassess_owrfield5_master.owrfield4unkid "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                Dim xTableName, xColName, xParentCol, xParentTabName As String
'                Dim xFieldTypeId As Integer = 0
'                xTableName = "" : xColName = "" : xParentCol = "" : xParentTabName = ""

'                Select Case xExOrder
'                    Case enWeight_Types.WEIGHT_FIELD1
'                        xTableName = "hrassess_owrfield1_master"
'                        xColName = "owrfield1unkid"
'                        xParentCol = "owrfield1unkid"
'                        xParentTabName = "hrassess_owrfield1_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD1
'                    Case enWeight_Types.WEIGHT_FIELD2
'                        xTableName = "hrassess_owrfield2_master"
'                        xColName = "owrfield2unkid"
'                        xParentCol = "owrfield1unkid"
'                        xParentTabName = "hrassess_owrfield2_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD2
'                    Case enWeight_Types.WEIGHT_FIELD3
'                        xTableName = "hrassess_owrfield3_master"
'                        xColName = "owrfield3unkid"
'                        xParentCol = "owrfield2unkid"
'                        xParentTabName = "hrassess_owrfield3_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD3
'                    Case enWeight_Types.WEIGHT_FIELD4
'                        xTableName = "hrassess_owrfield4_master"
'                        xColName = "owrfield4unkid"
'                        xParentCol = "owrfield3unkid"
'                        xParentTabName = "hrassess_owrfield4_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD4
'                    Case enWeight_Types.WEIGHT_FIELD5
'                        xTableName = "hrassess_owrfield5_master"
'                        xColName = "owrfield5unkid"
'                        xParentCol = "owrfield4unkid"
'                        xParentTabName = "hrassess_owrfield5_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD5
'                End Select

'                StrQ = "INSERT INTO hrassess_owrinfofield_tran " & _
'                       "( " & _
'                       "    owrfieldunkid " & _
'                       "   ,fieldunkid " & _
'                       "   ,field_data " & _
'                       "   ,owrfieldtypeid " & _
'                       ") " & _
'                       "SELECT " & _
'                       "    " & xTableName & "." & xColName & " AS nId " & _
'                       "   ,B.fieldunkid " & _
'                       "   ,B.infodata " & _
'                       "   ,B.owrfieldtypeid " & _
'                       "FROM " & xTableName & " " & _
'                       "   JOIN " & _
'                       "   ( " & _
'                       "       SELECT DISTINCT " & _
'                       "            hrassess_owrinfofield_tran.owrfieldunkid " & _
'                       "           ," & xTableName & ".field_data " & _
'                       "           ," & xTableName & ".periodunkid " & _
'                       "           ," & xTableName & ".ownerunkid " & _
'                       "           ,hrassess_owrinfofield_tran.fieldunkid " & _
'                       "           ,hrassess_owrinfofield_tran.field_data AS infodata " & _
'                       "           ,hrassess_owrinfofield_tran.owrfieldtypeid " & _
'                       "       FROM hrassess_owrinfofield_tran " & _
'                       "           JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = hrassess_owrinfofield_tran.owrfieldunkid " & _
'                       "       WHERE " & xTableName & ".periodunkid = '" & mintOldPeriodId & "' AND " & xTableName & ".isvoid = 0 AND hrassess_owrinfofield_tran.owrfieldtypeid = " & xFieldTypeId & " " & _
'                       "   ) AS B ON B.field_data = " & xTableName & ".field_data " & _
'                       "       AND B.ownerunkid = " & xTableName & ".ownerunkid " & _
'                       "WHERE " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = '" & mintNewPeriodId & "' " & _
'                       "   AND NOT EXISTS(SELECT * FROM hrassess_owrinfofield_tran AS OWF WHERE OWF.owrfieldunkid = " & xTableName & "." & xColName & " " & _
'                       "   AND OWF.owrfieldtypeid = " & xFieldTypeId & ") "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_owrowner_tran " & _
'                       "( " & _
'                       "     owrfieldunkid " & _
'                       "    ,employeeunkid " & _
'                       "    ,owrfieldtypeid " & _
'                       ") " & _
'                       "SELECT " & _
'                       "     " & xTableName & "." & xColName & " AS nId " & _
'                       "    ,B.empid " & _
'                       "    ,B.owrfieldtypeid " & _
'                       "FROM " & xTableName & " " & _
'                       "    JOIN " & _
'                       "    ( " & _
'                       "        SELECT " & _
'                       "             hrassess_owrowner_tran.owrfieldunkid " & _
'                       "            ," & xTableName & ".field_data " & _
'                       "            ," & xTableName & ".periodunkid " & _
'                       "            ," & xTableName & ".ownerunkid " & _
'                       "            ,hrassess_owrowner_tran.owrfieldtypeid " & _
'                       "            ,hrassess_owrowner_tran.employeeunkid AS empid " & _
'                       "        FROM hrassess_owrowner_tran " & _
'                       "            JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = hrassess_owrowner_tran.owrfieldunkid " & _
'                       "        WHERE " & xTableName & ".periodunkid = '" & mintOldPeriodId & "' AND " & xTableName & ".isvoid = 0 AND hrassess_owrowner_tran.owrfieldtypeid = " & xFieldTypeId & " " & _
'                       "    ) AS B ON B.ownerunkid = " & xTableName & ".ownerunkid AND B.field_data = " & xTableName & ".field_data " & _
'                       "WHERE " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = '" & mintNewPeriodId & "' " & _
'                       "    AND NOT EXISTS(SELECT * FROM hrassess_owrowner_tran AS OWF WHERE OWF.owrfieldunkid = " & xTableName & "." & xColName & " " & _
'                       "    AND OWF.owrfieldtypeid = " & xFieldTypeId & ") "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If


'                'Shani (05-Sep-2016) -- Start
'                'Enhancement - Assessment Close Period to Save Last status data given by sandra (FZ)
'                StrQ = "INSERT INTO hrassess_owrstatus_tran( " & _
'                       "     periodunkid " & _
'                       "    ,ownerunkid " & _
'                       "    ,statustypeid " & _
'                       "    ,status_date " & _
'                       "    ,commtents " & _
'                       "    ,userunkid " & _
'                       ") " & _
'                       "SELECT " & _
'                       "     '" & mintNewPeriodId & "' " & _
'                       "    ,A.ownerunkid " & _
'                       "    ,A.statustypeid " & _
'                       "    ,GETDATE() " & _
'                       "    ,A.commtents " & _
'                       "    ,'" & mintUserUnkId & "' " & _
'                       "FROM " & _
'                       "( " & _
'                       "    SELECT " & _
'                       "         hrassess_owrstatus_tran.ownerunkid " & _
'                       "        ,hrassess_owrstatus_tran.statustypeid " & _
'                       "        ,hrassess_owrstatus_tran.commtents " & _
'                       "        ,hrassess_owrstatus_tran.status_date " & _
'                       "        ,ROW_NUMBER()OVER(PARTITION by hrassess_owrstatus_tran.ownerunkid ORDER BY hrassess_owrstatus_tran.status_date DESC) AS RowNo " & _
'                       "    FROM hrassess_owrstatus_tran WHERE hrassess_owrstatus_tran.periodunkid = '" & mintOldPeriodId & "' " & _
'                       ") AS A WHERE A.RowNo = 1 "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                'Shani (05-Sep-2016) -- End


'            End Using
'            xPB.Value += 1
'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Tranfer_Owner_Level_Data", mstrModuleName)
'        Finally

'        End Try
'    End Function

'    Private Function Transfer_Employee_Level_Data() As Boolean
'        Try

'            Dim objFMapping As New clsAssess_Field_Mapping
'            Dim xLinkedFieldId As Integer = objFMapping.Get_Map_FieldId(mintNewPeriodId)
'            objFMapping = Nothing
'            Dim objFMaster As New clsAssess_Field_Master
'            Dim xExOrder As Integer = objFMaster.Get_Field_ExOrder(xLinkedFieldId)
'            objFMaster = Nothing

'            'Shani (15-Nov-2016) -- Start
'            'Enhancement - 
'            If xLinkedFieldId <= 0 Then Return True
'            'Shani (15-Nov-2016) -- End


'            Using objDo As New clsDataOperation
'                Dim StrQ As String = String.Empty
'                StrQ = "INSERT INTO hrassess_empfield1_master " & _
'                        "( " & _
'                              "owrfield1unkid " & _
'                             ",perspectiveunkid " & _
'                             ",employeeunkid " & _
'                             ",fieldunkid " & _
'                             ",field_data " & _
'                             ",yearunkid " & _
'                             ",periodunkid " & _
'                             ",weight " & _
'                             ",pct_completed " & _
'                             ",isfinal " & _
'                             ",startdate " & _
'                             ",enddate " & _
'                             ",statusunkid " & _
'                             ",userunkid " & _
'                             ",isvoid " & _
'                             ",voiduserunkid " & _
'                             ",voidreason " & _
'                             ",voiddatetime " & _
'                        ") " & _
'                        "SELECT " & _
'                              "owrfield1unkid " & _
'                             ",perspectiveunkid " & _
'                             ",employeeunkid " & _
'                             ",fieldunkid " & _
'                             ",field_data " & _
'                             ",yearunkid " & _
'                             ",'" & mintNewPeriodId & "' " & _
'                             ",weight " & _
'                             ",pct_completed " & _
'                             ",isfinal " & _
'                             ",startdate " & _
'                             ",enddate " & _
'                             ",statusunkid " & _
'                             ",userunkid " & _
'                             ",isvoid " & _
'                             ",voiduserunkid " & _
'                             ",voidreason " & _
'                             ",voiddatetime " & _
'                        "FROM hrassess_empfield1_master WHERE hrassess_empfield1_master.isvoid = 0 " & _
'                        "AND hrassess_empfield1_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                        "AND NOT EXISTS(SELECT * FROM hrassess_empfield1_master AS EF1 WHERE EF1.isvoid = 0 AND EF1.employeeunkid = hrassess_empfield1_master.employeeunkid AND EF1.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_empfield1_master " & _
'                        "SET owrfield1unkid = ISNULL(C.nId,0) " & _
'                        "FROM " & _
'                        "( " & _
'                             "SELECT " & _
'                                   "hrassess_owrfield1_master.owrfield1unkid AS nId " & _
'                                  ",B.owrfield1unkid AS oId " & _
'                                  ",hrassess_owrfield1_master.field_data " & _
'                                  ",B.periodunkid " & _
'                             "FROM hrassess_owrfield1_master " & _
'                             "JOIN " & _
'                             "( " & _
'                                  "SELECT DISTINCT " & _
'                                        "hrassess_empfield1_master.owrfield1unkid " & _
'                                       ",hrassess_owrfield1_master.field_data " & _
'                                       ",hrassess_empfield1_master.periodunkid " & _
'                                       ",hrassess_owrfield1_master.ownerrefid " & _
'                                       ",hrassess_owrfield1_master.ownerunkid " & _
'                                  "FROM hrassess_empfield1_master " & _
'                                  "JOIN hrassess_owrfield1_master ON hrassess_empfield1_master.owrfield1unkid = hrassess_owrfield1_master.owrfield1unkid " & _
'                                  "WHERE hrassess_empfield1_master.isvoid = 0 AND hrassess_empfield1_master.periodunkid = '" & mintNewPeriodId & "' AND hrassess_empfield1_master.isvoid = 0 AND hrassess_owrfield1_master.isvoid = 0 " & _
'                             ") AS B ON B.field_data = hrassess_owrfield1_master.field_data AND B.periodunkid = hrassess_owrfield1_master.periodunkid " & _
'                             "AND B.ownerrefid = hrassess_owrfield1_master.ownerrefid AND B.ownerunkid = hrassess_owrfield1_master.ownerunkid " & _
'                             "WHERE hrassess_owrfield1_master.isvoid = 0 AND hrassess_owrfield1_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                        ")AS C WHERE C.periodunkid = hrassess_empfield1_master.periodunkid AND C.oId = hrassess_empfield1_master.owrfield1unkid "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_empfield2_master " & _
'                        "( " & _
'                              "empfield1unkid " & _
'                             ",employeeunkid " & _
'                             ",periodunkid " & _
'                             ",fieldunkid " & _
'                             ",field_data " & _
'                             ",weight " & _
'                             ",pct_completed " & _
'                             ",startdate " & _
'                             ",enddate " & _
'                             ",statusunkid " & _
'                             ",userunkid " & _
'                             ",isvoid " & _
'                             ",voiduserunkid " & _
'                             ",voidreason " & _
'                             ",voiddatetime " & _
'                        ") " & _
'                        "SELECT " & _
'                               "empfield1unkid " & _
'                             ",employeeunkid " & _
'                             ",'" & mintNewPeriodId & "' " & _
'                             ",fieldunkid " & _
'                             ",field_data " & _
'                             ",weight " & _
'                             ",pct_completed " & _
'                             ",startdate " & _
'                             ",enddate " & _
'                             ",statusunkid " & _
'                             ",userunkid " & _
'                             ",isvoid " & _
'                             ",voiduserunkid " & _
'                             ",voidreason " & _
'                             ",voiddatetime " & _
'                        "FROM hrassess_empfield2_master WHERE hrassess_empfield2_master.isvoid = 0 " & _
'                        "AND hrassess_empfield2_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                        "AND NOT EXISTS(SELECT * FROM hrassess_empfield2_master AS EF2 WHERE EF2.isvoid = 0 AND EF2.employeeunkid = hrassess_empfield2_master.employeeunkid AND EF2.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_empfield2_master " & _
'                        "SET empfield1unkid = C.nId " & _
'                        "FROM " & _
'                        "( " & _
'                             "SELECT " & _
'                                   "hrassess_empfield1_master.empfield1unkid AS nId " & _
'                                  ",B.empfield1unkid AS oId " & _
'                                  ",hrassess_empfield1_master.employeeunkid " & _
'                                  ",hrassess_empfield1_master.field_data " & _
'                                  ",B.periodunkid " & _
'                             "FROM hrassess_empfield1_master " & _
'                             "JOIN " & _
'                             "( " & _
'                                  "SELECT DISTINCT " & _
'                                        "hrassess_empfield2_master.empfield1unkid " & _
'                                       ",hrassess_empfield1_master.field_data " & _
'                                       ",hrassess_empfield2_master.periodunkid " & _
'                                       ",hrassess_empfield2_master.employeeunkid " & _
'                                  "FROM hrassess_empfield2_master " & _
'                                  "JOIN hrassess_empfield1_master ON hrassess_empfield1_master.empfield1unkid = hrassess_empfield2_master.empfield1unkid " & _
'                                  "WHERE hrassess_empfield2_master.periodunkid = '" & mintNewPeriodId & "' AND hrassess_empfield1_master.isvoid = 0 AND hrassess_empfield2_master.isvoid = 0 " & _
'                             ") AS B ON B.field_data = hrassess_empfield1_master.field_data AND hrassess_empfield1_master.periodunkid = B.periodunkid " & _
'                                  "AND B.employeeunkid = hrassess_empfield1_master.employeeunkid " & _
'                             "WHERE hrassess_empfield1_master.isvoid = 0 AND hrassess_empfield1_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                        ") AS C WHERE C.employeeunkid = hrassess_empfield2_master.employeeunkid AND C.periodunkid = hrassess_empfield2_master.periodunkid " & _
'                        "AND C.oId = hrassess_empfield2_master.empfield1unkid; "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_empfield3_master " & _
'                        "( " & _
'                         "empfield2unkid " & _
'                        ",employeeunkid " & _
'                        ",periodunkid " & _
'                        ",fieldunkid " & _
'                        ",field_data " & _
'                        ",weight " & _
'                        ",pct_completed " & _
'                        ",startdate " & _
'                        ",enddate " & _
'                        ",statusunkid " & _
'                        ",userunkid " & _
'                        ",isvoid " & _
'                        ",voiduserunkid " & _
'                        ",voidreason " & _
'                        ",voiddatetime " & _
'                        ") " & _
'                        "SELECT " & _
'                         "empfield2unkid " & _
'                        ",employeeunkid " & _
'                        ",'" & mintNewPeriodId & "' " & _
'                        ",fieldunkid " & _
'                        ",field_data " & _
'                        ",weight " & _
'                        ",pct_completed " & _
'                        ",startdate " & _
'                        ",enddate " & _
'                        ",statusunkid " & _
'                        ",userunkid " & _
'                        ",isvoid " & _
'                        ",voiduserunkid " & _
'                        ",voidreason " & _
'                        ",voiddatetime " & _
'                        "FROM hrassess_empfield3_master WHERE hrassess_empfield3_master.isvoid = 0 " & _
'                        "AND hrassess_empfield3_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                        "AND NOT EXISTS(SELECT * FROM hrassess_empfield3_master AS EF3 WHERE EF3.isvoid = 0 AND EF3.employeeunkid = hrassess_empfield3_master.employeeunkid AND EF3.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_empfield3_master " & _
'                        "SET empfield2unkid = C.nId " & _
'                        "FROM " & _
'                        "( " & _
'                             "SELECT " & _
'                                   "hrassess_empfield2_master.empfield2unkid AS nId " & _
'                                  ",B.empfield2unkid AS oId " & _
'                                  ",hrassess_empfield2_master.employeeunkid " & _
'                                  ",hrassess_empfield2_master.field_data " & _
'                                  ",B.periodunkid " & _
'                             "FROM hrassess_empfield2_master " & _
'                             "JOIN " & _
'                             "( " & _
'                                  "SELECT DISTINCT " & _
'                                        "hrassess_empfield3_master.empfield2unkid " & _
'                                       ",hrassess_empfield2_master.field_data " & _
'                                       ",hrassess_empfield3_master.periodunkid " & _
'                                       ",hrassess_empfield3_master.employeeunkid " & _
'                                  "FROM hrassess_empfield3_master " & _
'                                  "JOIN hrassess_empfield2_master ON hrassess_empfield2_master.empfield2unkid = hrassess_empfield3_master.empfield2unkid " & _
'                                  "WHERE hrassess_empfield3_master.periodunkid = '" & mintNewPeriodId & "' AND hrassess_empfield2_master.isvoid = 0 AND hrassess_empfield3_master.isvoid = 0 " & _
'                             ") AS B ON B.field_data = hrassess_empfield2_master.field_data AND hrassess_empfield2_master.periodunkid = B.periodunkid " & _
'                                  "AND B.employeeunkid = hrassess_empfield2_master.employeeunkid " & _
'                             "WHERE hrassess_empfield2_master.isvoid = 0 AND hrassess_empfield2_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                        ") AS C WHERE C.employeeunkid = hrassess_empfield3_master.employeeunkid AND C.periodunkid = hrassess_empfield3_master.periodunkid " & _
'                        "AND C.oId = hrassess_empfield3_master.empfield2unkid; "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_empfield4_master " & _
'                        "( " & _
'                              "empfield3unkid " & _
'                             ",employeeunkid " & _
'                             ",periodunkid " & _
'                             ",fieldunkid " & _
'                             ",field_data " & _
'                             ",weight " & _
'                             ",pct_completed " & _
'                             ",startdate " & _
'                             ",enddate " & _
'                             ",statusunkid " & _
'                             ",userunkid " & _
'                             ",isvoid " & _
'                             ",voiduserunkid " & _
'                             ",voidreason " & _
'                             ",voiddatetime " & _
'                        ") " & _
'                        "SELECT " & _
'                              "empfield3unkid " & _
'                             ",employeeunkid " & _
'                             ",'" & mintNewPeriodId & "' " & _
'                             ",fieldunkid " & _
'                             ",field_data " & _
'                             ",weight " & _
'                             ",pct_completed " & _
'                             ",startdate " & _
'                             ",enddate " & _
'                             ",statusunkid " & _
'                             ",userunkid " & _
'                             ",isvoid " & _
'                             ",voiduserunkid " & _
'                             ",voidreason " & _
'                             ",voiddatetime " & _
'                        "FROM hrassess_empfield4_master WHERE hrassess_empfield4_master.isvoid = 0 " & _
'                        "AND hrassess_empfield4_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                        "AND NOT EXISTS(SELECT * FROM hrassess_empfield4_master AS EF4 WHERE EF4.isvoid = 0 AND EF4.employeeunkid = hrassess_empfield4_master.employeeunkid AND EF4.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_empfield4_master " & _
'                        "SET empfield3unkid = C.nId " & _
'                        "FROM " & _
'                        "( " & _
'                             "SELECT " & _
'                                   "hrassess_empfield3_master.empfield3unkid AS nId " & _
'                                  ",B.empfield3unkid AS oId " & _
'                                  ",hrassess_empfield3_master.employeeunkid " & _
'                                  ",hrassess_empfield3_master.field_data " & _
'                                  ",B.periodunkid " & _
'                             "FROM hrassess_empfield3_master " & _
'                             "JOIN " & _
'                             "( " & _
'                                  "SELECT DISTINCT " & _
'                                        "hrassess_empfield4_master.empfield3unkid " & _
'                                       ",hrassess_empfield3_master.field_data " & _
'                                       ",hrassess_empfield4_master.periodunkid " & _
'                                       ",hrassess_empfield4_master.employeeunkid " & _
'                                  "FROM hrassess_empfield4_master " & _
'                                  "JOIN hrassess_empfield3_master ON hrassess_empfield3_master.empfield3unkid = hrassess_empfield4_master.empfield3unkid " & _
'                                  "WHERE hrassess_empfield4_master.periodunkid = '" & mintNewPeriodId & "' AND hrassess_empfield3_master.isvoid = 0 AND hrassess_empfield4_master.isvoid = 0 " & _
'                             ") AS B ON B.field_data = hrassess_empfield3_master.field_data AND hrassess_empfield3_master.periodunkid = B.periodunkid " & _
'                                  "AND B.employeeunkid = hrassess_empfield3_master.employeeunkid " & _
'                             "WHERE hrassess_empfield3_master.isvoid = 0 AND hrassess_empfield3_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                        ") AS C WHERE C.employeeunkid = hrassess_empfield4_master.employeeunkid AND C.periodunkid = hrassess_empfield4_master.periodunkid " & _
'                        "AND C.oId = hrassess_empfield4_master.empfield3unkid; "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_empfield5_master " & _
'                        "( " & _
'                              "empfield4unkid " & _
'                             ",employeeunkid " & _
'                             ",periodunkid " & _
'                             ",fieldunkid " & _
'                             ",field_data " & _
'                             ",weight " & _
'                             ",pct_completed " & _
'                             ",startdate " & _
'                             ",enddate " & _
'                             ",statusunkid " & _
'                             ",userunkid " & _
'                             ",isvoid " & _
'                             ",voiduserunkid " & _
'                             ",voidreason " & _
'                             ",voiddatetime " & _
'                        ") " & _
'                        "SELECT " & _
'                              "empfield4unkid " & _
'                             ",employeeunkid " & _
'                             ",'" & mintNewPeriodId & "' " & _
'                             ",fieldunkid " & _
'                             ",field_data " & _
'                             ",weight " & _
'                             ",pct_completed " & _
'                             ",startdate " & _
'                             ",enddate " & _
'                             ",statusunkid " & _
'                             ",userunkid " & _
'                             ",isvoid " & _
'                             ",voiduserunkid " & _
'                             ",voidreason " & _
'                             ",voiddatetime " & _
'                        "FROM hrassess_empfield5_master WHERE hrassess_empfield5_master.isvoid = 0 " & _
'                        "AND hrassess_empfield5_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                        "AND NOT EXISTS(SELECT * FROM hrassess_empfield5_master AS EF5 WHERE EF5.isvoid = 0 AND EF5.employeeunkid = hrassess_empfield5_master.employeeunkid AND EF5.periodunkid = '" & mintNewPeriodId & "'); "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "UPDATE hrassess_empfield5_master " & _
'                        "SET empfield4unkid = C.nId " & _
'                        "FROM " & _
'                        "( " & _
'                             "SELECT " & _
'                                   "hrassess_empfield4_master.empfield4unkid AS nId " & _
'                                  ",B.empfield4unkid AS oId " & _
'                                  ",hrassess_empfield4_master.employeeunkid " & _
'                                  ",hrassess_empfield4_master.field_data " & _
'                                  ",B.periodunkid " & _
'                             "FROM hrassess_empfield4_master " & _
'                             "JOIN " & _
'                             "( " & _
'                                  "SELECT DISTINCT " & _
'                                        "hrassess_empfield5_master.empfield4unkid " & _
'                                       ",hrassess_empfield4_master.field_data " & _
'                                       ",hrassess_empfield5_master.periodunkid " & _
'                                       ",hrassess_empfield5_master.employeeunkid " & _
'                                  "FROM hrassess_empfield5_master " & _
'                                  "JOIN hrassess_empfield4_master ON hrassess_empfield4_master.empfield4unkid = hrassess_empfield5_master.empfield4unkid " & _
'                                  "WHERE hrassess_empfield5_master.periodunkid = '" & mintNewPeriodId & "' AND hrassess_empfield4_master.isvoid = 0 AND hrassess_empfield5_master.isvoid = 0 " & _
'                             ") AS B ON B.field_data = hrassess_empfield4_master.field_data AND hrassess_empfield4_master.periodunkid = B.periodunkid " & _
'                                  "AND B.employeeunkid = hrassess_empfield4_master.employeeunkid " & _
'                             "WHERE hrassess_empfield4_master.isvoid = 0 AND hrassess_empfield4_master.periodunkid = '" & mintNewPeriodId & "' " & _
'                        ") AS C WHERE C.employeeunkid = hrassess_empfield5_master.employeeunkid AND C.periodunkid = hrassess_empfield5_master.periodunkid " & _
'                        "AND C.oId = hrassess_empfield5_master.empfield4unkid "


'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                Dim xTableName, xColName, xParentCol, xParentTabName As String
'                Dim xFieldTypeId As Integer = 0
'                xTableName = "" : xColName = "" : xParentCol = "" : xParentTabName = ""

'                Select Case xExOrder
'                    Case enWeight_Types.WEIGHT_FIELD1
'                        xTableName = "hrassess_empfield1_master"
'                        xColName = "empfield1unkid"
'                        xParentCol = "empfield1unkid"
'                        xParentTabName = "hrassess_empfield1_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD1
'                    Case enWeight_Types.WEIGHT_FIELD2
'                        xTableName = "hrassess_empfield2_master"
'                        xColName = "empfield2unkid"
'                        xParentCol = "empfield1unkid"
'                        xParentTabName = "hrassess_empfield1_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD2
'                    Case enWeight_Types.WEIGHT_FIELD3
'                        xTableName = "hrassess_empfield3_master"
'                        xColName = "empfield3unkid"
'                        xParentCol = "empfield2unkid"
'                        xParentTabName = "hrassess_empfield2_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD3
'                    Case enWeight_Types.WEIGHT_FIELD4
'                        xTableName = "hrassess_empfield4_master"
'                        xColName = "empfield4unkid"
'                        xParentCol = "empfield3unkid"
'                        xParentTabName = "hrassess_empfield3_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD4
'                    Case enWeight_Types.WEIGHT_FIELD5
'                        xTableName = "hrassess_empfield5_master"
'                        xColName = "empfield5unkid"
'                        xParentCol = "empfield4unkid"
'                        xParentTabName = "hrassess_empfield4_master"
'                        xFieldTypeId = enWeight_Types.WEIGHT_FIELD5
'                End Select

'                StrQ = "INSERT INTO hrassess_empinfofield_tran " & _
'                        "( " & _
'                         "empfieldunkid " & _
'                        ",fieldunkid " & _
'                        ",field_data " & _
'                        ",empfieldtypeid " & _
'                        ") " & _
'                        "SELECT " & _
'                             " " & xTableName & "." & xColName & " AS nId " & _
'                             ",B.fieldunkid " & _
'                             ",B.infodata " & _
'                             ",B.empfieldtypeid " & _
'                        "FROM " & xTableName & " " & _
'                        "JOIN " & _
'                        "( " & _
'                             "SELECT DISTINCT " & _
'                                  " hrassess_empinfofield_tran.empfieldunkid " & _
'                                  "," & xTableName & ".field_data " & _
'                                  "," & xTableName & ".periodunkid " & _
'                                  "," & xTableName & ".employeeunkid " & _
'                                  ",hrassess_empinfofield_tran.fieldunkid " & _
'                                  ",hrassess_empinfofield_tran.field_data AS infodata " & _
'                                  ",hrassess_empinfofield_tran.empfieldtypeid " & _
'                             "FROM hrassess_empinfofield_tran " & _
'                             "JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = hrassess_empinfofield_tran.empfieldunkid " & _
'                             "WHERE " & xTableName & ".periodunkid = '" & mintOldPeriodId & "' AND " & xTableName & ".isvoid = 0 AND hrassess_empinfofield_tran.empfieldtypeid = " & xFieldTypeId & " " & _
'                        ") AS B ON B.field_data = " & xTableName & ".field_data " & _
'                             "AND B.employeeunkid = " & xTableName & ".employeeunkid " & _
'                        "WHERE " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = '" & mintNewPeriodId & "' " & _
'                        "AND NOT EXISTS(SELECT * FROM hrassess_empinfofield_tran AS EF WHERE EF.empfieldunkid = " & xTableName & "." & xColName & " " & _
'                             "AND EF.empfieldtypeid = " & xFieldTypeId & ") "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "INSERT INTO hrassess_empowner_tran " & _
'                        "( " & _
'                         "empfieldunkid " & _
'                        ",employeeunkid " & _
'                        ",empfieldtypeid " & _
'                        ") " & _
'                        "SELECT " & _
'                              "" & xTableName & "." & xColName & " AS nId " & _
'                             ",B.empid " & _
'                             ",B.empfieldtypeid " & _
'                        "FROM " & xTableName & " " & _
'                        "JOIN " & _
'                        "( " & _
'                             "SELECT " & _
'                                   "hrassess_empowner_tran.empfieldunkid " & _
'                                  "," & xTableName & ".field_data " & _
'                                  "," & xTableName & ".periodunkid " & _
'                                  "," & xTableName & ".employeeunkid " & _
'                                  ",hrassess_empowner_tran.empfieldtypeid " & _
'                                  ",hrassess_empowner_tran.employeeunkid AS empid " & _
'                             "FROM hrassess_empowner_tran " & _
'                                  "JOIN " & xTableName & " ON " & xTableName & "." & xColName & " = hrassess_empowner_tran.empfieldunkid " & _
'                             "WHERE " & xTableName & ".periodunkid = '" & mintOldPeriodId & "' AND " & xTableName & ".isvoid = 0 AND hrassess_empowner_tran.empfieldtypeid = " & xFieldTypeId & " " & _
'                        ") AS B ON B.employeeunkid = " & xTableName & ".employeeunkid AND B.field_data = " & xTableName & ".field_data " & _
'                        "WHERE " & xTableName & ".isvoid = 0 AND " & xTableName & ".periodunkid = '" & mintNewPeriodId & "' " & _
'                        "AND NOT EXISTS(SELECT * FROM hrassess_empowner_tran AS EF WHERE EF.empfieldunkid = " & xTableName & "." & xColName & " " & _
'                        "AND EF.empfieldtypeid = " & xFieldTypeId & ") "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                'Shani (05-Sep-2016) -- Start
'                'Enhancement - Assessment Close Period to Save Last status data given by sandra (FZ)
'                StrQ = "INSERT INTO hrassess_empstatus_tran( " & _
'                       "     employeeunkid " & _
'                       "    ,periodunkid " & _
'                       "    ,statustypeid " & _
'                       "    ,assessormasterunkid " & _
'                       "    ,assessoremployeeunkid " & _
'                       "    ,status_date " & _
'                       "    ,commtents " & _
'                       "    ,isunlock " & _
'                       "    ,userunkid " & _
'                       "    ,loginemployeeunkid " & _
'                       "    ,isapprovalskipped " & _
'                       ")SELECT " & _
'                       "     A.employeeunkid " & _
'                       "    ,'" & mintNewPeriodId & "' " & _
'                       "    ,A.statustypeid " & _
'                       "    ,A.assessormasterunkid " & _
'                       "    ,A.assessoremployeeunkid " & _
'                       "    ,GETDATE() " & _
'                       "    ,A.commtents " & _
'                       "    ,A.isunlock " & _
'                       "    ,'" & mintUserUnkId & "' " & _
'                       "    ,A.loginemployeeunkid " & _
'                       "    ,A.isapprovalskipped " & _
'                       "FROM " & _
'                       "( " & _
'                       "    SELECT " & _
'                       "         hrassess_empstatus_tran.employeeunkid " & _
'                       "        ,hrassess_empstatus_tran.statustypeid " & _
'                       "        ,hrassess_empstatus_tran.assessormasterunkid " & _
'                       "        ,hrassess_empstatus_tran.assessoremployeeunkid " & _
'                       "        ,hrassess_empstatus_tran.commtents " & _
'                       "        ,hrassess_empstatus_tran.isunlock " & _
'                       "        ,hrassess_empstatus_tran.loginemployeeunkid " & _
'                       "        ,hrassess_empstatus_tran.isapprovalskipped " & _
'                       "        ,ROW_NUMBER()OVER(PARTITION BY hrassess_empstatus_tran.employeeunkid ORDER BY hrassess_empstatus_tran.status_date DESC) AS rno " & _
'                       "    FROM hrassess_empstatus_tran " & _
'                       "    WHERE hrassess_empstatus_tran.periodunkid = '" & mintOldPeriodId & "' " & _
'                       ") AS A WHERE A.rno = 1 "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If
'                'Shani (05-Sep-2016) -- End


'            End Using
'            xPB.Value += 1
'            Return True
'        Catch ex As Exception
'            Throw New Exception(ex.Message & "; Procedure Name: Transfer_Employee_Level_Data; Module Name: " & mstrModuleName)
'        Finally
'        End Try
'    End Function

'    'Private Function Transfer_Company_Level_Data() As Boolean
'    '    Dim objCoyFld1 As clsassess_coyfield1_master
'    '    Dim objCoyFld2 As clsassess_coyfield2_master
'    '    Dim objCoyFld3 As clsassess_coyfield3_master
'    '    Dim objCoyFld4 As clsassess_coyfield4_master
'    '    Dim objCoyFld5 As clsassess_coyfield5_master
'    '    Try
'    '        objCoyFld1 = New clsassess_coyfield1_master
'    '        dsData = objCoyFld1.GetListForTansfer(mintOldPeriodId)

'    '        Dim objCoyInfoField As New clsassess_coyinfofield_tran
'    '        Dim objCoyOwner As New clsassess_coyowner_tran

'    '        If dsData.Tables(0).Rows.Count > 0 Then
'    '            xDicCoyField1Unkids = New Dictionary(Of Integer, Integer)
'    '            xDicCoyField2Unkids = New Dictionary(Of Integer, Integer)
'    '            xDicCoyField3Unkids = New Dictionary(Of Integer, Integer)
'    '            xDicCoyField4Unkids = New Dictionary(Of Integer, Integer)
'    '            xDicCoyField5Unkids = New Dictionary(Of Integer, Integer)
'    '            For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                Application.DoEvents()
'    '                If objCoyFld1 IsNot Nothing Then objCoyFld1 = Nothing
'    '                objCoyFld1 = New clsassess_coyfield1_master
'    '                '************ GETTING VALUE IF PRESENT *************' START
'    '                mDicInfoField = objCoyInfoField.Get_Data(xRow.Item("coyfield1unkid"), enWeight_Types.WEIGHT_FIELD1)
'    '                mdtOwner = objCoyOwner.Get_Data(xRow.Item("coyfield1unkid"), enWeight_Types.WEIGHT_FIELD1)
'    '                Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                For Each row As DataRow In rowsToUpdate
'    '                    row.SetField("AUD", "A")
'    '                Next
'    '                '************ GETTING VALUE IF PRESENT *************' END

'    '                '*************** START DATA TRANSFER PROCESS **************' START
'    '                If IsDBNull(xRow.Item("enddate")) = False Then
'    '                    objCoyFld1._Enddate = xRow.Item("enddate")
'    '                Else
'    '                    objCoyFld1._Enddate = Nothing
'    '                End If
'    '                objCoyFld1._Field_Data = xRow.Item("field_data")
'    '                objCoyFld1._Fieldunkid = xRow.Item("fieldunkid")
'    '                objCoyFld1._Isvoid = False
'    '                objCoyFld1._Periodunkid = mintNewPeriodId
'    '                objCoyFld1._Perspectiveunkid = xRow.Item("perspectiveunkid")
'    '                If IsDBNull(xRow.Item("startdate")) = False Then
'    '                    objCoyFld1._Startdate = xRow.Item("startdate")
'    '                Else
'    '                    objCoyFld1._Startdate = Nothing
'    '                End If
'    '                objCoyFld1._Userunkid = User._Object._Userunkid
'    '                objCoyFld1._Voiddatetime = Nothing
'    '                objCoyFld1._Voidreason = ""
'    '                objCoyFld1._Voiduserunkid = 0
'    '                objCoyFld1._Weight = xRow.Item("weight")
'    '                objCoyFld1._CoyFieldTypeId = enWeight_Types.WEIGHT_FIELD1
'    '                objCoyFld1._Ownerrefid = xRow.Item("ownerrefid")
'    '                objCoyFld1._Statusunkid = xRow.Item("statusunkid")

'    '                If objCoyFld1.Insert(mdtOwner, mDicInfoField) Then
'    '                    If xDicCoyField1Unkids.ContainsKey(xRow.Item("coyfield1unkid")) = False Then
'    '                        xDicCoyField1Unkids.Add(xRow.Item("coyfield1unkid"), objCoyFld1._Coyfield1unkid)
'    '                    End If
'    '                Else
'    '                    If xDicCoyField1Unkids.ContainsKey(xRow.Item("coyfield1unkid")) = False Then
'    '                        xDicCoyField1Unkids.Add(xRow.Item("coyfield1unkid"), objCoyFld1.GetCoyFieldUnkid(objCoyFld1._Field_Data, mintNewPeriodId))
'    '                    End If
'    '                End If
'    '                '*************** START DATA TRANSFER PROCESS **************' END
'    '            Next
'    '        End If

'    '        objCoyFld2 = New clsassess_coyfield2_master
'    '        dsData = objCoyFld2.GetListForTansfer(mintOldPeriodId)

'    '        If dsData.Tables(0).Rows.Count > 0 Then
'    '            For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                Application.DoEvents()
'    '                If objCoyFld2 IsNot Nothing Then objCoyFld2 = Nothing
'    '                objCoyFld2 = New clsassess_coyfield2_master
'    '                '************ GETTING VALUE IF PRESENT *************' START
'    '                mDicInfoField = objCoyInfoField.Get_Data(xRow.Item("coyfield2unkid"), enWeight_Types.WEIGHT_FIELD2)
'    '                mdtOwner = objCoyOwner.Get_Data(xRow.Item("coyfield2unkid"), enWeight_Types.WEIGHT_FIELD2)
'    '                Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                For Each row As DataRow In rowsToUpdate
'    '                    row.SetField("AUD", "A")
'    '                Next
'    '                '************ GETTING VALUE IF PRESENT *************' END

'    '                '*************** START DATA TRANSFER PROCESS **************' START
'    '                If IsDBNull(xRow.Item("enddate")) = False Then
'    '                    objCoyFld2._Enddate = xRow.Item("enddate")
'    '                Else
'    '                    objCoyFld2._Enddate = Nothing
'    '                End If
'    '                objCoyFld2._Field_Data = xRow.Item("field_data")
'    '                objCoyFld2._Fieldunkid = xRow.Item("fieldunkid")
'    '                If xDicCoyField1Unkids.Keys.Count > 0 Then
'    '                    If xDicCoyField1Unkids.ContainsKey(xRow.Item("coyfield1unkid")) Then
'    '                        objCoyFld2._Coyfield1unkid = xDicCoyField1Unkids(xRow.Item("coyfield1unkid"))
'    '                    End If
'    '                End If
'    '                objCoyFld2._Isvoid = False
'    '                objCoyFld2._Periodunkid = mintNewPeriodId
'    '                If IsDBNull(xRow.Item("startdate")) = False Then
'    '                    objCoyFld2._Startdate = xRow.Item("startdate")
'    '                Else
'    '                    objCoyFld2._Startdate = Nothing
'    '                End If
'    '                objCoyFld2._Userunkid = User._Object._Userunkid
'    '                objCoyFld2._Voiddatetime = Nothing
'    '                objCoyFld2._Voidreason = ""
'    '                objCoyFld2._Voiduserunkid = 0
'    '                objCoyFld2._Weight = xRow.Item("weight")
'    '                objCoyFld2._CoyFieldTypeId = enWeight_Types.WEIGHT_FIELD2
'    '                objCoyFld2._Ownerrefid = xRow.Item("ownerrefid")
'    '                objCoyFld2._Statusunkid = xRow.Item("statusunkid")

'    '                If objCoyFld2.Insert(mdtOwner, mDicInfoField) Then
'    '                    If xDicCoyField2Unkids.ContainsKey(xRow.Item("coyfield2unkid")) = False Then
'    '                        xDicCoyField2Unkids.Add(xRow.Item("coyfield2unkid"), objCoyFld2._Coyfield2unkid)
'    '                    End If
'    '                Else
'    '                    If xDicCoyField2Unkids.ContainsKey(xRow.Item("coyfield2unkid")) = False Then
'    '                        xDicCoyField2Unkids.Add(xRow.Item("coyfield2unkid"), objCoyFld2.GetCoyFieldUnkid(objCoyFld2._Field_Data, mintNewPeriodId))
'    '                    End If
'    '                End If
'    '                '*************** START DATA TRANSFER PROCESS **************' END
'    '            Next
'    '        End If

'    '        objCoyFld3 = New clsassess_coyfield3_master
'    '        dsData = objCoyFld3.GetListForTansfer(mintOldPeriodId)

'    '        If dsData.Tables(0).Rows.Count > 0 Then
'    '            For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                Application.DoEvents()
'    '                If objCoyFld3 IsNot Nothing Then objCoyFld3 = Nothing
'    '                objCoyFld3 = New clsassess_coyfield3_master
'    '                '************ GETTING VALUE IF PRESENT *************' START
'    '                mDicInfoField = objCoyInfoField.Get_Data(xRow.Item("coyfield3unkid"), enWeight_Types.WEIGHT_FIELD3)
'    '                mdtOwner = objCoyOwner.Get_Data(xRow.Item("coyfield3unkid"), enWeight_Types.WEIGHT_FIELD3)
'    '                Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                For Each row As DataRow In rowsToUpdate
'    '                    row.SetField("AUD", "A")
'    '                Next
'    '                '************ GETTING VALUE IF PRESENT *************' END

'    '                '*************** START DATA TRANSFER PROCESS **************' START
'    '                If IsDBNull(xRow.Item("enddate")) = False Then
'    '                    objCoyFld3._Enddate = xRow.Item("enddate")
'    '                Else
'    '                    objCoyFld3._Enddate = Nothing
'    '                End If
'    '                objCoyFld3._Field_Data = xRow.Item("field_data")
'    '                objCoyFld3._Fieldunkid = xRow.Item("fieldunkid")
'    '                If xDicCoyField2Unkids.Keys.Count > 0 Then
'    '                    If xDicCoyField2Unkids.ContainsKey(xRow.Item("coyfield2unkid")) Then
'    '                        objCoyFld3._Coyfield2unkid = xDicCoyField2Unkids(xRow.Item("coyfield2unkid"))
'    '                    End If
'    '                End If
'    '                objCoyFld3._Isvoid = False
'    '                objCoyFld3._Periodunkid = mintNewPeriodId
'    '                If IsDBNull(xRow.Item("startdate")) = False Then
'    '                    objCoyFld3._Startdate = xRow.Item("startdate")
'    '                Else
'    '                    objCoyFld3._Startdate = Nothing
'    '                End If
'    '                objCoyFld3._Userunkid = User._Object._Userunkid
'    '                objCoyFld3._Voiddatetime = Nothing
'    '                objCoyFld3._Voidreason = ""
'    '                objCoyFld3._Voiduserunkid = 0
'    '                objCoyFld3._Weight = xRow.Item("weight")
'    '                objCoyFld3._CoyFieldTypeId = enWeight_Types.WEIGHT_FIELD2
'    '                objCoyFld3._Ownerrefid = xRow.Item("ownerrefid")
'    '                objCoyFld3._Statusunkid = xRow.Item("statusunkid")

'    '                If objCoyFld3.Insert(mdtOwner, mDicInfoField) Then
'    '                    If xDicCoyField3Unkids.ContainsKey(xRow.Item("coyfield3unkid")) = False Then
'    '                        xDicCoyField3Unkids.Add(xRow.Item("coyfield3unkid"), objCoyFld3._Coyfield3unkid)
'    '                    End If
'    '                Else
'    '                    If xDicCoyField3Unkids.ContainsKey(xRow.Item("coyfield3unkid")) = False Then
'    '                        xDicCoyField3Unkids.Add(xRow.Item("coyfield3unkid"), objCoyFld3.GetCoyFieldUnkid(objCoyFld3._Field_Data, mintNewPeriodId))
'    '                    End If
'    '                End If
'    '                '*************** START DATA TRANSFER PROCESS **************' END
'    '            Next
'    '        End If

'    '        objCoyFld4 = New clsassess_coyfield4_master
'    '        dsData = objCoyFld4.GetListForTansfer(mintOldPeriodId)

'    '        If dsData.Tables(0).Rows.Count > 0 Then
'    '            For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                Application.DoEvents()
'    '                If objCoyFld4 IsNot Nothing Then objCoyFld4 = Nothing
'    '                objCoyFld4 = New clsassess_coyfield4_master
'    '                '************ GETTING VALUE IF PRESENT *************' START
'    '                mDicInfoField = objCoyInfoField.Get_Data(xRow.Item("coyfield4unkid"), enWeight_Types.WEIGHT_FIELD4)
'    '                mdtOwner = objCoyOwner.Get_Data(xRow.Item("coyfield4unkid"), enWeight_Types.WEIGHT_FIELD4)
'    '                Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                For Each row As DataRow In rowsToUpdate
'    '                    row.SetField("AUD", "A")
'    '                Next
'    '                '************ GETTING VALUE IF PRESENT *************' END

'    '                '*************** START DATA TRANSFER PROCESS **************' START
'    '                If IsDBNull(xRow.Item("enddate")) = False Then
'    '                    objCoyFld4._Enddate = xRow.Item("enddate")
'    '                Else
'    '                    objCoyFld4._Enddate = Nothing
'    '                End If
'    '                objCoyFld4._Field_Data = xRow.Item("field_data")
'    '                objCoyFld4._Fieldunkid = xRow.Item("fieldunkid")
'    '                If xDicCoyField3Unkids.Keys.Count > 0 Then
'    '                    If xDicCoyField3Unkids.ContainsKey(xRow.Item("coyfield3unkid")) Then
'    '                        objCoyFld4._Coyfield3unkid = xDicCoyField3Unkids(xRow.Item("coyfield3unkid"))
'    '                    End If
'    '                End If
'    '                objCoyFld4._Isvoid = False
'    '                objCoyFld4._Periodunkid = mintNewPeriodId
'    '                If IsDBNull(xRow.Item("startdate")) = False Then
'    '                    objCoyFld4._Startdate = xRow.Item("startdate")
'    '                Else
'    '                    objCoyFld4._Startdate = Nothing
'    '                End If
'    '                objCoyFld4._Userunkid = User._Object._Userunkid
'    '                objCoyFld4._Voiddatetime = Nothing
'    '                objCoyFld4._Voidreason = ""
'    '                objCoyFld4._Voiduserunkid = 0
'    '                objCoyFld4._Weight = xRow.Item("weight")
'    '                objCoyFld4._CoyFieldTypeId = enWeight_Types.WEIGHT_FIELD2
'    '                objCoyFld4._Ownerrefid = xRow.Item("ownerrefid")
'    '                objCoyFld4._Statusunkid = xRow.Item("statusunkid")

'    '                If objCoyFld4.Insert(mdtOwner, mDicInfoField) Then
'    '                    If xDicCoyField4Unkids.ContainsKey(xRow.Item("coyfield4unkid")) = False Then
'    '                        xDicCoyField4Unkids.Add(xRow.Item("coyfield4unkid"), objCoyFld4._Coyfield4unkid)
'    '                    End If
'    '                Else
'    '                    If xDicCoyField4Unkids.ContainsKey(xRow.Item("coyfield4unkid")) = False Then
'    '                        xDicCoyField4Unkids.Add(xRow.Item("coyfield4unkid"), objCoyFld4.GetCoyFieldUnkid(objCoyFld4._Field_Data, mintNewPeriodId))
'    '                    End If
'    '                End If
'    '                '*************** START DATA TRANSFER PROCESS **************' END
'    '            Next
'    '        End If

'    '        objCoyFld5 = New clsassess_coyfield5_master
'    '        dsData = objCoyFld5.GetListForTansfer(mintOldPeriodId)

'    '        If dsData.Tables(0).Rows.Count > 0 Then
'    '            For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                Application.DoEvents()
'    '                If objCoyFld5 IsNot Nothing Then objCoyFld5 = Nothing
'    '                objCoyFld5 = New clsassess_coyfield5_master

'    '                '************ GETTING VALUE IF PRESENT *************' START
'    '                mDicInfoField = objCoyInfoField.Get_Data(xRow.Item("coyfield5unkid"), enWeight_Types.WEIGHT_FIELD5)
'    '                mdtOwner = objCoyOwner.Get_Data(xRow.Item("coyfield5unkid"), enWeight_Types.WEIGHT_FIELD5)
'    '                Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                For Each row As DataRow In rowsToUpdate
'    '                    row.SetField("AUD", "A")
'    '                Next
'    '                '************ GETTING VALUE IF PRESENT *************' END

'    '                '*************** START DATA TRANSFER PROCESS **************' START
'    '                If IsDBNull(xRow.Item("enddate")) = False Then
'    '                    objCoyFld5._Enddate = xRow.Item("enddate")
'    '                Else
'    '                    objCoyFld5._Enddate = Nothing
'    '                End If
'    '                objCoyFld5._Field_Data = xRow.Item("field_data")
'    '                objCoyFld5._Fieldunkid = xRow.Item("fieldunkid")
'    '                If xDicCoyField4Unkids.Keys.Count > 0 Then
'    '                    If xDicCoyField4Unkids.ContainsKey(xRow.Item("coyfield4unkid")) Then
'    '                        objCoyFld5._Coyfield4unkid = xDicCoyField4Unkids(xRow.Item("coyfield4unkid"))
'    '                    End If
'    '                End If
'    '                objCoyFld5._Isvoid = False
'    '                objCoyFld5._Periodunkid = mintNewPeriodId
'    '                If IsDBNull(xRow.Item("startdate")) = False Then
'    '                    objCoyFld5._Startdate = xRow.Item("startdate")
'    '                Else
'    '                    objCoyFld5._Startdate = Nothing
'    '                End If
'    '                objCoyFld5._Userunkid = User._Object._Userunkid
'    '                objCoyFld5._Voiddatetime = Nothing
'    '                objCoyFld5._Voidreason = ""
'    '                objCoyFld5._Voiduserunkid = 0
'    '                objCoyFld5._Weight = xRow.Item("weight")
'    '                objCoyFld5._CoyFieldTypeId = enWeight_Types.WEIGHT_FIELD5
'    '                objCoyFld5._Ownerrefid = xRow.Item("ownerrefid")
'    '                objCoyFld5._Statusunkid = xRow.Item("statusunkid")

'    '                If objCoyFld5.Insert(mdtOwner, mDicInfoField) Then
'    '                    If xDicCoyField5Unkids.ContainsKey(xRow.Item("coyfield5unkid")) = False Then
'    '                        xDicCoyField5Unkids.Add(xRow.Item("coyfield5unkid"), objCoyFld5._Coyfield5unkid)
'    '                    End If
'    '                Else
'    '                    If xDicCoyField5Unkids.ContainsKey(xRow.Item("coyfield5unkid")) = False Then
'    '                        xDicCoyField5Unkids.Add(xRow.Item("coyfield5unkid"), objCoyFld5.GetCoyFieldUnkid(objCoyFld5._Field_Data, mintNewPeriodId))
'    '                    End If
'    '                End If
'    '                '*************** START DATA TRANSFER PROCESS **************' END
'    '            Next
'    '        End If

'    '        objCoyInfoField = Nothing
'    '        objCoyOwner = Nothing
'    '        xPB.Value += 1
'    '        Return True
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "Transfer_Company_Level_Data", mstrModuleName)
'    '    Finally
'    '        objCoyFld1 = Nothing : objCoyFld2 = Nothing
'    '        objCoyFld3 = Nothing : objCoyFld4 = Nothing
'    '        objCoyFld5 = Nothing

'    '    End Try
'    'End Function

'    'Private Function Tranfer_Owner_Level_Data() As Boolean
'    '    Dim objOwrFld1 As clsassess_owrfield1_master
'    '    Dim objOwrFld2 As clsassess_owrfield2_master
'    '    Dim objOwrFld3 As clsassess_owrfield3_master
'    '    Dim objOwrFld4 As clsassess_owrfield4_master
'    '    Dim objOwrFld5 As clsassess_owrfield5_master
'    '    Try
'    '        objOwrFld1 = New clsassess_owrfield1_master
'    '        dsData = objOwrFld1.GetListForTansfer(mintOldPeriodId)

'    '        Dim objOwrInfoField As New clsassess_owrinfofield_tran
'    '        Dim objOwrOwner As New clsassess_owrowner_tran

'    '        If dsData.Tables(0).Rows.Count > 0 Then
'    '            xDicOwrField1Unkids = New Dictionary(Of Integer, Integer)
'    '            xDicOwrField2Unkids = New Dictionary(Of Integer, Integer)
'    '            xDicOwrField3Unkids = New Dictionary(Of Integer, Integer)
'    '            xDicOwrField4Unkids = New Dictionary(Of Integer, Integer)
'    '            xDicOwrField5Unkids = New Dictionary(Of Integer, Integer)

'    '            For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                Application.DoEvents()
'    '                If objOwrFld1 IsNot Nothing Then objOwrFld1 = Nothing
'    '                objOwrFld1 = New clsassess_owrfield1_master

'    '                '************ GETTING VALUE IF PRESENT *************' START
'    '                mDicInfoField = objOwrInfoField.Get_Data(xRow.Item("owrfield1unkid"), enWeight_Types.WEIGHT_FIELD1)
'    '                mdtOwner = objOwrOwner.Get_Data(xRow.Item("owrfield1unkid"), enWeight_Types.WEIGHT_FIELD1)
'    '                Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                For Each row As DataRow In rowsToUpdate
'    '                    row.SetField("AUD", "A")
'    '                Next
'    '                '************ GETTING VALUE IF PRESENT *************' END

'    '                '*************** START DATA TRANSFER PROCESS **************' START
'    '                If xDicCoyField1Unkids.Keys.Count > 0 Then
'    '                    If xDicCoyField1Unkids.ContainsKey(xRow.Item("coyfield1unkid")) Then
'    '                        objOwrFld1._Coyfield1unkid = xDicCoyField1Unkids(xRow.Item("coyfield1unkid"))
'    '                    End If
'    '                End If
'    '                If IsDBNull(xRow.Item("enddate")) = False Then
'    '                    objOwrFld1._Enddate = xRow.Item("enddate")
'    '                Else
'    '                    objOwrFld1._Enddate = Nothing
'    '                End If
'    '                objOwrFld1._Field_Data = xRow.Item("field_data")
'    '                objOwrFld1._Fieldunkid = xRow.Item("fieldunkid")
'    '                objOwrFld1._Isvoid = False
'    '                objOwrFld1._Periodunkid = mintNewPeriodId
'    '                objOwrFld1._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD1
'    '                If IsDBNull(xRow.Item("startdate")) = False Then
'    '                    objOwrFld1._Startdate = xRow.Item("startdate")
'    '                Else
'    '                    objOwrFld1._Startdate = Nothing
'    '                End If
'    '                objOwrFld1._Owenerrefid = xRow.Item("ownerrefid")
'    '                objOwrFld1._Statusunkid = xRow.Item("statusunkid")
'    '                objOwrFld1._Ownerunkid = xRow.Item("ownerunkid")
'    '                objOwrFld1._Userunkid = User._Object._Userunkid
'    '                objOwrFld1._Voiddatetime = Nothing
'    '                objOwrFld1._Voiduserunkid = 0
'    '                objOwrFld1._Voidreason = ""
'    '                objOwrFld1._Weight = xRow.Item("weight")
'    '                objOwrFld1._Pct_Completed = xRow.Item("pct_completed")

'    '                If objOwrFld1.Insert(mDicInfoField, mdtOwner) Then
'    '                    If xDicOwrField1Unkids.ContainsKey(xRow.Item("owrfield1unkid")) = False Then
'    '                        xDicOwrField1Unkids.Add(xRow.Item("owrfield1unkid"), objOwrFld1._Owrfield1unkid)
'    '                    End If
'    '                Else
'    '                    If xDicOwrField1Unkids.ContainsKey(xRow.Item("owrfield1unkid")) = False Then
'    '                        xDicOwrField1Unkids.Add(xRow.Item("owrfield1unkid"), objOwrFld1.GetOwrFieldUnkid(objOwrFld1._Field_Data, mintNewPeriodId, objOwrFld1._Ownerunkid))
'    '                    End If
'    '                End If
'    '                '*************** START DATA TRANSFER PROCESS **************' END
'    '            Next
'    '        End If

'    '        objOwrFld2 = New clsassess_owrfield2_master
'    '        dsData = objOwrFld2.GetListForTansfer(mintOldPeriodId)

'    '        If dsData.Tables(0).Rows.Count > 0 Then
'    '            For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                Application.DoEvents()
'    '                If objOwrFld2 IsNot Nothing Then objOwrFld2 = Nothing
'    '                objOwrFld2 = New clsassess_owrfield2_master

'    '                '************ GETTING VALUE IF PRESENT *************' START
'    '                mDicInfoField = objOwrInfoField.Get_Data(xRow.Item("owrfield2unkid"), enWeight_Types.WEIGHT_FIELD2)
'    '                mdtOwner = objOwrOwner.Get_Data(xRow.Item("owrfield2unkid"), enWeight_Types.WEIGHT_FIELD2)
'    '                Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                For Each row As DataRow In rowsToUpdate
'    '                    row.SetField("AUD", "A")
'    '                Next
'    '                '************ GETTING VALUE IF PRESENT *************' END

'    '                '*************** START DATA TRANSFER PROCESS **************' START
'    '                If IsDBNull(xRow.Item("enddate")) = False Then
'    '                    objOwrFld2._Enddate = xRow.Item("enddate")
'    '                Else
'    '                    objOwrFld2._Enddate = Nothing
'    '                End If
'    '                objOwrFld2._Field_Data = xRow.Item("field_data")
'    '                objOwrFld2._Fieldunkid = xRow.Item("fieldunkid")
'    '                objOwrFld2._Isvoid = False
'    '                objOwrFld2._Ownerunkid = xRow.Item("ownerunkid")
'    '                If xDicOwrField1Unkids.Keys.Count > 0 Then
'    '                    If xDicOwrField1Unkids.ContainsKey(xRow.Item("owrfield1unkid")) Then
'    '                        objOwrFld2._Owrfield1unkid = xDicOwrField1Unkids(xRow.Item("owrfield1unkid"))
'    '                    End If
'    '                End If
'    '                objOwrFld2._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD2
'    '                objOwrFld2._Pct_Completed = xRow.Item("pct_completed")
'    '                objOwrFld2._Periodunkid = mintNewPeriodId
'    '                If IsDBNull(xRow.Item("startdate")) = False Then
'    '                    objOwrFld2._Startdate = xRow.Item("startdate")
'    '                Else
'    '                    objOwrFld2._Startdate = Nothing
'    '                End If
'    '                objOwrFld2._Statusunkid = xRow.Item("statusunkid")
'    '                objOwrFld2._Userunkid = User._Object._Userunkid
'    '                objOwrFld2._Voiddatetime = Nothing
'    '                objOwrFld2._Voidreason = ""
'    '                objOwrFld2._Voiduserunkid = -1
'    '                objOwrFld2._Weight = xRow.Item("weight")

'    '                If objOwrFld2.Insert(mDicInfoField, mdtOwner) Then
'    '                    If xDicOwrField2Unkids.ContainsKey(xRow.Item("owrfield2unkid")) = False Then
'    '                        xDicOwrField2Unkids.Add(xRow.Item("owrfield2unkid"), objOwrFld2._Owrfield2unkid)
'    '                    End If
'    '                Else
'    '                    If xDicOwrField2Unkids.ContainsKey(xRow.Item("owrfield2unkid")) = False Then
'    '                        xDicOwrField2Unkids.Add(xRow.Item("owrfield2unkid"), objOwrFld2.GetOwrFieldUnkid(objOwrFld2._Field_Data, mintNewPeriodId, objOwrFld2._Ownerunkid))
'    '                    End If
'    '                End If
'    '                '*************** START DATA TRANSFER PROCESS **************' END
'    '            Next
'    '        End If

'    '        objOwrFld3 = New clsassess_owrfield3_master
'    '        dsData = objOwrFld3.GetListForTansfer(mintOldPeriodId)

'    '        If dsData.Tables(0).Rows.Count > 0 Then
'    '            For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                Application.DoEvents()
'    '                If objOwrFld3 IsNot Nothing Then objOwrFld3 = Nothing
'    '                objOwrFld3 = New clsassess_owrfield3_master

'    '                '************ GETTING VALUE IF PRESENT *************' START
'    '                mDicInfoField = objOwrInfoField.Get_Data(xRow.Item("owrfield3unkid"), enWeight_Types.WEIGHT_FIELD3)
'    '                mdtOwner = objOwrOwner.Get_Data(xRow.Item("owrfield3unkid"), enWeight_Types.WEIGHT_FIELD3)
'    '                Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                For Each row As DataRow In rowsToUpdate
'    '                    row.SetField("AUD", "A")
'    '                Next
'    '                '************ GETTING VALUE IF PRESENT *************' END

'    '                '*************** START DATA TRANSFER PROCESS **************' START
'    '                If IsDBNull(xRow.Item("enddate")) = False Then
'    '                    objOwrFld3._Enddate = xRow.Item("enddate")
'    '                Else
'    '                    objOwrFld3._Enddate = Nothing
'    '                End If
'    '                objOwrFld3._Field_Data = xRow.Item("field_data")
'    '                objOwrFld3._Fieldunkid = xRow.Item("fieldunkid")
'    '                objOwrFld3._Isvoid = False
'    '                objOwrFld3._Ownerunkid = xRow.Item("ownerunkid")
'    '                If xDicOwrField2Unkids.Keys.Count > 0 Then
'    '                    If xDicOwrField2Unkids.ContainsKey(xRow.Item("owrfield2unkid")) Then
'    '                        objOwrFld3._Owrfield2unkid = xDicOwrField2Unkids(xRow.Item("owrfield2unkid"))
'    '                    End If
'    '                End If
'    '                objOwrFld3._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD3
'    '                objOwrFld3._Pct_Completed = xRow.Item("pct_completed")
'    '                objOwrFld3._Periodunkid = mintNewPeriodId
'    '                If IsDBNull(xRow.Item("startdate")) = False Then
'    '                    objOwrFld3._Startdate = xRow.Item("startdate")
'    '                Else
'    '                    objOwrFld3._Startdate = Nothing
'    '                End If
'    '                objOwrFld3._Statusunkid = xRow.Item("statusunkid")
'    '                objOwrFld3._Userunkid = User._Object._Userunkid
'    '                objOwrFld3._Voiddatetime = Nothing
'    '                objOwrFld3._Voidreason = ""
'    '                objOwrFld3._Voiduserunkid = -1
'    '                objOwrFld3._Weight = xRow.Item("weight")

'    '                If objOwrFld3.Insert(mDicInfoField, mdtOwner) Then
'    '                    If xDicOwrField3Unkids.ContainsKey(xRow.Item("owrfield3unkid")) = False Then
'    '                        xDicOwrField3Unkids.Add(xRow.Item("owrfield3unkid"), objOwrFld3._Owrfield3unkid)
'    '                    End If
'    '                Else
'    '                    If xDicOwrField3Unkids.ContainsKey(xRow.Item("owrfield3unkid")) = False Then
'    '                        xDicOwrField3Unkids.Add(xRow.Item("owrfield3unkid"), objOwrFld3.GetOwrFieldUnkid(objOwrFld3._Field_Data, mintNewPeriodId, objOwrFld3._Ownerunkid))
'    '                    End If
'    '                End If
'    '                '*************** START DATA TRANSFER PROCESS **************' END
'    '            Next
'    '        End If

'    '        objOwrFld4 = New clsassess_owrfield4_master
'    '        dsData = objOwrFld4.GetListForTansfer(mintOldPeriodId)

'    '        If dsData.Tables(0).Rows.Count > 0 Then
'    '            For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                Application.DoEvents()
'    '                If objOwrFld4 IsNot Nothing Then objOwrFld4 = Nothing
'    '                objOwrFld4 = New clsassess_owrfield4_master

'    '                '************ GETTING VALUE IF PRESENT *************' START
'    '                mDicInfoField = objOwrInfoField.Get_Data(xRow.Item("owrfield4unkid"), enWeight_Types.WEIGHT_FIELD4)
'    '                mdtOwner = objOwrOwner.Get_Data(xRow.Item("owrfield4unkid"), enWeight_Types.WEIGHT_FIELD4)
'    '                Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                For Each row As DataRow In rowsToUpdate
'    '                    row.SetField("AUD", "A")
'    '                Next
'    '                '************ GETTING VALUE IF PRESENT *************' END

'    '                '*************** START DATA TRANSFER PROCESS **************' START
'    '                If IsDBNull(xRow.Item("enddate")) = False Then
'    '                    objOwrFld4._Enddate = xRow.Item("enddate")
'    '                Else
'    '                    objOwrFld4._Enddate = Nothing
'    '                End If
'    '                objOwrFld4._Field_Data = xRow.Item("field_data")
'    '                objOwrFld4._Fieldunkid = xRow.Item("fieldunkid")
'    '                objOwrFld4._Isvoid = False
'    '                objOwrFld4._Ownerunkid = xRow.Item("ownerunkid")
'    '                If xDicOwrField3Unkids.Keys.Count > 0 Then
'    '                    If xDicOwrField3Unkids.ContainsKey(xRow.Item("owrfield3unkid")) Then
'    '                        objOwrFld4._Owrfield3unkid = xDicOwrField3Unkids(xRow.Item("owrfield3unkid"))
'    '                    End If
'    '                End If
'    '                objOwrFld4._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD4
'    '                objOwrFld4._Pct_Completed = xRow.Item("pct_completed")
'    '                objOwrFld4._Periodunkid = mintNewPeriodId
'    '                If IsDBNull(xRow.Item("startdate")) = False Then
'    '                    objOwrFld4._Startdate = xRow.Item("startdate")
'    '                Else
'    '                    objOwrFld4._Startdate = Nothing
'    '                End If
'    '                objOwrFld4._Statusunkid = xRow.Item("statusunkid")
'    '                objOwrFld4._Userunkid = User._Object._Userunkid
'    '                objOwrFld4._Voiddatetime = Nothing
'    '                objOwrFld4._Voidreason = ""
'    '                objOwrFld4._Voiduserunkid = -1
'    '                objOwrFld4._Weight = xRow.Item("weight")

'    '                If objOwrFld4.Insert(mDicInfoField, mdtOwner) Then
'    '                    If xDicOwrField4Unkids.ContainsKey(xRow.Item("owrfield4unkid")) = False Then
'    '                        xDicOwrField4Unkids.Add(xRow.Item("owrfield4unkid"), objOwrFld4._Owrfield4unkid)
'    '                    End If
'    '                Else
'    '                    If xDicOwrField4Unkids.ContainsKey(xRow.Item("owrfield4unkid")) = False Then
'    '                        xDicOwrField4Unkids.Add(xRow.Item("owrfield4unkid"), objOwrFld4.GetOwrFieldUnkid(objOwrFld4._Field_Data, mintNewPeriodId, objOwrFld4._Ownerunkid))
'    '                    End If
'    '                End If
'    '                '*************** START DATA TRANSFER PROCESS **************' END
'    '            Next
'    '        End If

'    '        objOwrFld5 = New clsassess_owrfield5_master
'    '        dsData = objOwrFld5.GetListForTansfer(mintOldPeriodId)

'    '        If dsData.Tables(0).Rows.Count > 0 Then
'    '            For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                Application.DoEvents()
'    '                If objOwrFld5 IsNot Nothing Then objOwrFld4 = Nothing
'    '                objOwrFld5 = New clsassess_owrfield5_master

'    '                '************ GETTING VALUE IF PRESENT *************' START
'    '                mDicInfoField = objOwrInfoField.Get_Data(xRow.Item("owrfield5unkid"), enWeight_Types.WEIGHT_FIELD5)
'    '                mdtOwner = objOwrOwner.Get_Data(xRow.Item("owrfield5unkid"), enWeight_Types.WEIGHT_FIELD5)
'    '                Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                For Each row As DataRow In rowsToUpdate
'    '                    row.SetField("AUD", "A")
'    '                Next
'    '                '************ GETTING VALUE IF PRESENT *************' END

'    '                '*************** START DATA TRANSFER PROCESS **************' START
'    '                If IsDBNull(xRow.Item("enddate")) = False Then
'    '                    objOwrFld5._Enddate = xRow.Item("enddate")
'    '                Else
'    '                    objOwrFld5._Enddate = Nothing
'    '                End If
'    '                objOwrFld5._Field_Data = xRow.Item("field_data")
'    '                objOwrFld5._Fieldunkid = xRow.Item("fieldunkid")
'    '                objOwrFld5._Isvoid = False
'    '                objOwrFld5._Ownerunkid = xRow.Item("ownerunkid")
'    '                If xDicOwrField4Unkids.Keys.Count > 0 Then
'    '                    If xDicOwrField4Unkids.ContainsKey(xRow.Item("owrfield4unkid")) Then
'    '                        objOwrFld5._Owrfield4unkid = xDicOwrField4Unkids(xRow.Item("owrfield4unkid"))
'    '                    End If
'    '                End If
'    '                objOwrFld5._OwrFieldTypeId = enWeight_Types.WEIGHT_FIELD5
'    '                objOwrFld5._Pct_Completed = xRow.Item("pct_completed")
'    '                objOwrFld5._Periodunkid = mintNewPeriodId
'    '                If IsDBNull(xRow.Item("startdate")) = False Then
'    '                    objOwrFld5._Startdate = xRow.Item("startdate")
'    '                Else
'    '                    objOwrFld5._Startdate = Nothing
'    '                End If
'    '                objOwrFld5._Statusunkid = xRow.Item("statusunkid")
'    '                objOwrFld5._Userunkid = User._Object._Userunkid
'    '                objOwrFld5._Voiddatetime = Nothing
'    '                objOwrFld5._Voidreason = ""
'    '                objOwrFld5._Voiduserunkid = -1
'    '                objOwrFld5._Weight = xRow.Item("weight")

'    '                If objOwrFld5.Insert(mDicInfoField, mdtOwner) Then
'    '                    If xDicOwrField5Unkids.ContainsKey(xRow.Item("owrfield5unkid")) = False Then
'    '                        xDicOwrField5Unkids.Add(xRow.Item("owrfield5unkid"), objOwrFld5._Owrfield5unkid)
'    '                    End If
'    '                Else
'    '                    If xDicOwrField5Unkids.ContainsKey(xRow.Item("owrfield5unkid")) = False Then
'    '                        xDicOwrField5Unkids.Add(xRow.Item("owrfield5unkid"), objOwrFld5.GetOwrFieldUnkid(objOwrFld5._Field_Data, mintNewPeriodId, objOwrFld5._Ownerunkid))
'    '                    End If
'    '                End If
'    '                '*************** START DATA TRANSFER PROCESS **************' END
'    '            Next
'    '        End If


'    '        objOwrInfoField = Nothing
'    '        objOwrOwner = Nothing
'    '        xPB.Value += 1
'    '        Return True
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "Tranfer_Owner_Level_Data", mstrModuleName)
'    '    Finally
'    '        objOwrFld1 = Nothing : objOwrFld2 = Nothing
'    '        objOwrFld3 = Nothing : objOwrFld4 = Nothing
'    '        objOwrFld5 = Nothing
'    '    End Try
'    'End Function

'    'S.SANDEEP [03 JUN 2016] -- START
'    'Private Function Transfer_Employee_Level_Data_old() As Boolean
'    '    Dim dsEmp As New DataSet
'    '    Dim objEmpFld1 As clsassess_empfield1_master
'    '    Dim objEmpFld2 As clsassess_empfield2_master
'    '    Dim objEmpFld3 As clsassess_empfield3_master
'    '    Dim objEmpFld4 As clsassess_empfield4_master
'    '    Dim objEmpFld5 As clsassess_empfield5_master
'    '    Try
'    '        Using objDo As New clsDataOperation
'    '            Dim StrQ As String = String.Empty

'    '            StrQ = "SELECT DISTINCT employeeunkid AS EmpId FROM hrassess_empfield1_master WHERE isvoid = 0 ORDER BY employeeunkid "

'    '            dsEmp = objDo.ExecQuery(StrQ, "List")

'    '            If objDo.ErrorMessage <> "" Then
'    '                Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'    '            End If
'    '        End Using

'    '        If dsEmp.Tables(0).Rows.Count <= 0 Then Return True

'    '        Dim objEmpInfoField As New clsassess_empinfofield_tran
'    '        Dim objEmpOwner As New clsassess_empowner_tran

'    '        For Each xEmp As DataRow In dsEmp.Tables(0).Rows
'    '            Application.DoEvents()
'    '            objEmpFld1 = New clsassess_empfield1_master
'    '            dsData = objEmpFld1.GetListForTansfer(mintOldPeriodId, xEmp.Item("EmpId"))

'    '            If dsData.Tables(0).Rows.Count > 0 Then

'    '                xDicEmpField1Unkids = New Dictionary(Of Integer, Integer)
'    '                xDicEmpField2Unkids = New Dictionary(Of Integer, Integer)
'    '                xDicEmpField3Unkids = New Dictionary(Of Integer, Integer)
'    '                xDicEmpField4Unkids = New Dictionary(Of Integer, Integer)
'    '                xDicEmpField5Unkids = New Dictionary(Of Integer, Integer)

'    '                For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                    Application.DoEvents()
'    '                    If objEmpFld1 IsNot Nothing Then objEmpFld1 = Nothing
'    '                    objEmpFld1 = New clsassess_empfield1_master

'    '                    '************ GETTING VALUE IF PRESENT *************' START
'    '                    mDicInfoField = objEmpInfoField.Get_Data(xRow.Item("empfield1unkid"), enWeight_Types.WEIGHT_FIELD1)
'    '                    mdtOwner = objEmpOwner.Get_Data(xRow.Item("empfield1unkid"), enWeight_Types.WEIGHT_FIELD1)
'    '                    Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                    For Each row As DataRow In rowsToUpdate
'    '                        row.SetField("AUD", "A")
'    '                    Next
'    '                    '************ GETTING VALUE IF PRESENT *************' END

'    '                    '*************** START DATA TRANSFER PROCESS **************' START
'    '                    objEmpFld1._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD1
'    '                    objEmpFld1._Employeeunkid = xEmp.Item("EmpId")
'    '                    If IsDBNull(xRow.Item("enddate")) = False Then
'    '                        objEmpFld1._Enddate = xRow.Item("enddate")
'    '                    Else
'    '                        objEmpFld1._Enddate = Nothing
'    '                    End If
'    '                    objEmpFld1._Field_Data = xRow.Item("field_data")
'    '                    objEmpFld1._Fieldunkid = xRow.Item("fieldunkid")
'    '                    objEmpFld1._Isfinal = False
'    '                    objEmpFld1._Isvoid = False

'    '                    If xDicOwrField1Unkids IsNot Nothing Then
'    '                        If xDicOwrField1Unkids.Keys.Count > 0 Then
'    '                            If xDicCoyField1Unkids.ContainsKey(xRow.Item("owrfield1unkid")) Then
'    '                                objEmpFld1._Owrfield1unkid = xDicOwrField1Unkids(xRow.Item("owrfield1unkid"))
'    '                            End If
'    '                        End If
'    '                    End If

'    '                    objEmpFld1._Pct_Completed = xRow.Item("pct_completed")
'    '                    objEmpFld1._Periodunkid = mintNewPeriodId
'    '                    objEmpFld1._Perspectiveunkid = xRow.Item("perspectiveunkid")
'    '                    If IsDBNull(xRow.Item("startdate")) = False Then
'    '                        objEmpFld1._Startdate = xRow.Item("startdate")
'    '                    Else
'    '                        objEmpFld1._Startdate = Nothing
'    '                    End If
'    '                    objEmpFld1._Statusunkid = xRow.Item("statusunkid")
'    '                    objEmpFld1._Userunkid = User._Object._Userunkid
'    '                    objEmpFld1._Voiddatetime = Nothing
'    '                    objEmpFld1._Voidreason = ""
'    '                    objEmpFld1._Voiduserunkid = -1
'    '                    objEmpFld1._Weight = xRow.Item("weight")

'    '                    If objEmpFld1.Insert(mDicInfoField, mdtOwner) Then
'    '                        If xDicEmpField1Unkids.ContainsKey(xRow.Item("empfield1unkid")) = False Then
'    '                            xDicEmpField1Unkids.Add(xRow.Item("empfield1unkid"), objEmpFld1._Empfield1unkid)
'    '                        End If
'    '                    Else
'    '                        If xDicEmpField1Unkids.ContainsKey(xRow.Item("empfield1unkid")) = False Then
'    '                            xDicEmpField1Unkids.Add(xRow.Item("empfield1unkid"), objEmpFld1.GetEmpFieldUnkid(objEmpFld1._Field_Data, mintNewPeriodId, xEmp.Item("EmpId")))
'    '                        End If
'    '                    End If
'    '                    '*************** START DATA TRANSFER PROCESS **************' END
'    '                Next
'    '            End If

'    '            objEmpFld2 = New clsassess_empfield2_master
'    '            dsData = objEmpFld2.GetListForTansfer(mintOldPeriodId, xEmp.Item("EmpId"))

'    '            If dsData.Tables(0).Rows.Count > 0 Then
'    '                For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                    Application.DoEvents()
'    '                    If objEmpFld2 IsNot Nothing Then objEmpFld2 = Nothing
'    '                    objEmpFld2 = New clsassess_empfield2_master

'    '                    '************ GETTING VALUE IF PRESENT *************' START
'    '                    mDicInfoField = objEmpInfoField.Get_Data(xRow.Item("empfield2unkid"), enWeight_Types.WEIGHT_FIELD2)
'    '                    mdtOwner = objEmpOwner.Get_Data(xRow.Item("empfield2unkid"), enWeight_Types.WEIGHT_FIELD2)
'    '                    Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                    For Each row As DataRow In rowsToUpdate
'    '                        row.SetField("AUD", "A")
'    '                    Next
'    '                    '************ GETTING VALUE IF PRESENT *************' End

'    '                    '*************** START DATA TRANSFER PROCESS **************' START
'    '                    If xDicEmpField1Unkids.Keys.Count > 0 Then
'    '                        If xDicEmpField1Unkids.ContainsKey(xRow.Item("empfield1unkid")) Then
'    '                            objEmpFld2._Empfield1unkid = xDicEmpField1Unkids(xRow.Item("empfield1unkid"))
'    '                        End If
'    '                    End If
'    '                    objEmpFld2._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD2
'    '                    objEmpFld2._Employeeunkid = xEmp.Item("EmpId")
'    '                    If IsDBNull(xRow.Item("enddate")) = False Then
'    '                        objEmpFld2._Enddate = xRow.Item("enddate")
'    '                    Else
'    '                        objEmpFld2._Enddate = Nothing
'    '                    End If
'    '                    objEmpFld2._Field_Data = xRow.Item("field_data")
'    '                    objEmpFld2._Fieldunkid = xRow.Item("fieldunkid")
'    '                    objEmpFld2._Isvoid = False
'    '                    objEmpFld2._Pct_Completed = xRow.Item("pct_completed")
'    '                    objEmpFld2._Periodunkid = mintNewPeriodId
'    '                    If IsDBNull(xRow.Item("startdate")) = False Then
'    '                        objEmpFld2._Startdate = xRow.Item("startdate")
'    '                    Else
'    '                        objEmpFld2._Startdate = Nothing
'    '                    End If
'    '                    objEmpFld2._Statusunkid = xRow.Item("statusunkid")
'    '                    objEmpFld2._Userunkid = User._Object._Userunkid
'    '                    objEmpFld2._Voiddatetime = Nothing
'    '                    objEmpFld2._Voidreason = ""
'    '                    objEmpFld2._Voiduserunkid = -1
'    '                    objEmpFld2._Weight = xRow.Item("weight")

'    '                    If objEmpFld2.Insert(mDicInfoField, mdtOwner) Then
'    '                        If xDicEmpField2Unkids.ContainsKey(xRow.Item("empfield2unkid")) = False Then
'    '                            xDicEmpField2Unkids.Add(xRow.Item("empfield2unkid"), objEmpFld2._Empfield2unkid)
'    '                        End If
'    '                    Else
'    '                        If xDicEmpField2Unkids.ContainsKey(xRow.Item("empfield2unkid")) = False Then
'    '                            xDicEmpField2Unkids.Add(xRow.Item("empfield2unkid"), objEmpFld2.GetEmpFieldUnkid(objEmpFld2._Field_Data, mintNewPeriodId, xEmp.Item("EmpId")))
'    '                        End If
'    '                    End If
'    '                    '*************** START DATA TRANSFER PROCESS **************' End
'    '                Next
'    '            End If

'    '            objEmpFld3 = New clsassess_empfield3_master
'    '            dsData = objEmpFld3.GetListForTansfer(mintOldPeriodId, xEmp.Item("EmpId"))

'    '            If dsData.Tables(0).Rows.Count > 0 Then
'    '                For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                    Application.DoEvents()
'    '                    If objEmpFld3 IsNot Nothing Then objEmpFld3 = Nothing
'    '                    objEmpFld3 = New clsassess_empfield3_master

'    '                    '************ GETTING VALUE IF PRESENT *************' START
'    '                    mDicInfoField = objEmpInfoField.Get_Data(xRow.Item("empfield3unkid"), enWeight_Types.WEIGHT_FIELD3)
'    '                    mdtOwner = objEmpOwner.Get_Data(xRow.Item("empfield3unkid"), enWeight_Types.WEIGHT_FIELD3)
'    '                    Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                    For Each row As DataRow In rowsToUpdate
'    '                        row.SetField("AUD", "A")
'    '                    Next
'    '                    '************ GETTING VALUE IF PRESENT *************' END

'    '                    '*************** START DATA TRANSFER PROCESS **************' START
'    '                    If xDicEmpField2Unkids.Keys.Count > 0 Then
'    '                        If xDicEmpField2Unkids.ContainsKey(xRow.Item("empfield2unkid")) Then
'    '                            objEmpFld3._Empfield2unkid = xDicEmpField2Unkids(xRow.Item("empfield2unkid"))
'    '                        End If
'    '                    End If
'    '                    objEmpFld3._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD3
'    '                    objEmpFld3._Employeeunkid = xEmp.Item("EmpId")
'    '                    If IsDBNull(xRow.Item("enddate")) = False Then
'    '                        objEmpFld3._Enddate = xRow.Item("enddate")
'    '                    Else
'    '                        objEmpFld3._Enddate = Nothing
'    '                    End If
'    '                    objEmpFld3._Field_Data = xRow.Item("field_data")
'    '                    objEmpFld3._Fieldunkid = xRow.Item("fieldunkid")
'    '                    objEmpFld3._Isvoid = False
'    '                    objEmpFld3._Pct_Completed = xRow.Item("pct_completed")
'    '                    objEmpFld3._Periodunkid = mintNewPeriodId
'    '                    If IsDBNull(xRow.Item("startdate")) = False Then
'    '                        objEmpFld3._Startdate = xRow.Item("startdate")
'    '                    Else
'    '                        objEmpFld3._Startdate = Nothing
'    '                    End If
'    '                    objEmpFld3._Statusunkid = xRow.Item("statusunkid")
'    '                    objEmpFld3._Userunkid = User._Object._Userunkid
'    '                    objEmpFld3._Voiddatetime = Nothing
'    '                    objEmpFld3._Voidreason = ""
'    '                    objEmpFld3._Voiduserunkid = -1
'    '                    objEmpFld3._Weight = xRow.Item("weight")

'    '                    If objEmpFld3.Insert(mDicInfoField, mdtOwner) Then
'    '                        If xDicEmpField3Unkids.ContainsKey(xRow.Item("empfield3unkid")) = False Then
'    '                            xDicEmpField3Unkids.Add(xRow.Item("empfield3unkid"), objEmpFld3._Empfield3unkid)
'    '                        End If
'    '                    Else
'    '                        If xDicEmpField3Unkids.ContainsKey(xRow.Item("empfield3unkid")) = False Then
'    '                            xDicEmpField3Unkids.Add(xRow.Item("empfield3unkid"), objEmpFld3.GetEmpFieldUnkid(objEmpFld3._Field_Data, mintNewPeriodId, xEmp.Item("EmpId")))
'    '                        End If
'    '                    End If
'    '                    '*************** START DATA TRANSFER PROCESS **************' END
'    '                Next
'    '            End If

'    '            objEmpFld4 = New clsassess_empfield4_master
'    '            dsData = objEmpFld4.GetListForTansfer(mintOldPeriodId, xEmp.Item("EmpId"))

'    '            If dsData.Tables(0).Rows.Count > 0 Then
'    '                For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                    Application.DoEvents()
'    '                    If objEmpFld4 IsNot Nothing Then objEmpFld4 = Nothing
'    '                    objEmpFld4 = New clsassess_empfield4_master

'    '                    '************ GETTING VALUE IF PRESENT *************' START
'    '                    mDicInfoField = objEmpInfoField.Get_Data(xRow.Item("empfield4unkid"), enWeight_Types.WEIGHT_FIELD4)
'    '                    mdtOwner = objEmpOwner.Get_Data(xRow.Item("empfield4unkid"), enWeight_Types.WEIGHT_FIELD4)
'    '                    Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                    For Each row As DataRow In rowsToUpdate
'    '                        row.SetField("AUD", "A")
'    '                    Next
'    '                    '************ GETTING VALUE IF PRESENT *************' END

'    '                    '*************** START DATA TRANSFER PROCESS **************' START
'    '                    If xDicEmpField3Unkids.Keys.Count > 0 Then
'    '                        If xDicEmpField3Unkids.ContainsKey(xRow.Item("empfield3unkid")) Then
'    '                            objEmpFld4._Empfield3unkid = xDicEmpField3Unkids(xRow.Item("empfield3unkid"))
'    '                        End If
'    '                    End If
'    '                    objEmpFld4._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD4
'    '                    objEmpFld4._Employeeunkid = xEmp.Item("EmpId")
'    '                    If IsDBNull(xRow.Item("enddate")) = False Then
'    '                        objEmpFld4._Enddate = xRow.Item("enddate")
'    '                    Else
'    '                        objEmpFld4._Enddate = Nothing
'    '                    End If
'    '                    objEmpFld4._Field_Data = xRow.Item("field_data")
'    '                    objEmpFld4._Fieldunkid = xRow.Item("fieldunkid")
'    '                    objEmpFld4._Isvoid = False
'    '                    objEmpFld4._Pct_Completed = xRow.Item("pct_completed")
'    '                    objEmpFld4._Periodunkid = mintNewPeriodId
'    '                    If IsDBNull(xRow.Item("startdate")) = False Then
'    '                        objEmpFld4._Startdate = xRow.Item("startdate")
'    '                    Else
'    '                        objEmpFld4._Startdate = Nothing
'    '                    End If
'    '                    objEmpFld4._Statusunkid = xRow.Item("statusunkid")
'    '                    objEmpFld4._Userunkid = User._Object._Userunkid
'    '                    objEmpFld4._Voiddatetime = Nothing
'    '                    objEmpFld4._Voidreason = ""
'    '                    objEmpFld4._Voiduserunkid = -1
'    '                    objEmpFld4._Weight = xRow.Item("weight")

'    '                    If objEmpFld4.Insert(mDicInfoField, mdtOwner) Then
'    '                        If xDicEmpField4Unkids.ContainsKey(xRow.Item("empfield4unkid")) = False Then
'    '                            xDicEmpField4Unkids.Add(xRow.Item("empfield4unkid"), objEmpFld4._Empfield4unkid)
'    '                        End If
'    '                    Else
'    '                        If xDicEmpField4Unkids.ContainsKey(xRow.Item("empfield4unkid")) = False Then
'    '                            xDicEmpField4Unkids.Add(xRow.Item("empfield4unkid"), objEmpFld4.GetEmpFieldUnkid(objEmpFld4._Field_Data, mintNewPeriodId, xEmp.Item("EmpId")))
'    '                        End If
'    '                    End If
'    '                    '*************** START DATA TRANSFER PROCESS **************' END
'    '                Next
'    '            End If

'    '            objEmpFld5 = New clsassess_empfield5_master
'    '            dsData = objEmpFld5.GetListForTansfer(mintOldPeriodId, xEmp.Item("EmpId"))

'    '            If dsData.Tables(0).Rows.Count > 0 Then
'    '                For Each xRow As DataRow In dsData.Tables(0).Rows
'    '                    Application.DoEvents()
'    '                    If objEmpFld5 IsNot Nothing Then objEmpFld5 = Nothing
'    '                    objEmpFld5 = New clsassess_empfield5_master

'    '                    '************ GETTING VALUE IF PRESENT *************' START
'    '                    mDicInfoField = objEmpInfoField.Get_Data(xRow.Item("empfield5unkid"), enWeight_Types.WEIGHT_FIELD5)
'    '                    mdtOwner = objEmpOwner.Get_Data(xRow.Item("empfield5unkid"), enWeight_Types.WEIGHT_FIELD5)
'    '                    Dim rowsToUpdate = mdtOwner.AsEnumerable().Where(Function(r) r.Field(Of String)("AUD") = "")
'    '                    For Each row As DataRow In rowsToUpdate
'    '                        row.SetField("AUD", "A")
'    '                    Next
'    '                    '************ GETTING VALUE IF PRESENT *************' END

'    '                    '*************** START DATA TRANSFER PROCESS **************' START
'    '                    If xDicEmpField4Unkids.Keys.Count > 0 Then
'    '                        If xDicEmpField4Unkids.ContainsKey(xRow.Item("empfield4unkid")) Then
'    '                            objEmpFld5._Empfield4unkid = xDicEmpField4Unkids(xRow.Item("empfield4unkid"))
'    '                        End If
'    '                    End If
'    '                    objEmpFld5._EmpFieldTypeId = enWeight_Types.WEIGHT_FIELD5
'    '                    objEmpFld5._Employeeunkid = xEmp.Item("EmpId")
'    '                    If IsDBNull(xRow.Item("enddate")) = False Then
'    '                        objEmpFld5._Enddate = xRow.Item("enddate")
'    '                    Else
'    '                        objEmpFld5._Enddate = Nothing
'    '                    End If
'    '                    objEmpFld5._Field_Data = xRow.Item("field_data")
'    '                    objEmpFld5._Fieldunkid = xRow.Item("fieldunkid")
'    '                    objEmpFld5._Isvoid = False
'    '                    objEmpFld5._Pct_Completed = xRow.Item("pct_completed")
'    '                    objEmpFld5._Periodunkid = mintNewPeriodId
'    '                    If IsDBNull(xRow.Item("startdate")) = False Then
'    '                        objEmpFld5._Startdate = xRow.Item("startdate")
'    '                    Else
'    '                        objEmpFld5._Startdate = Nothing
'    '                    End If
'    '                    objEmpFld5._Statusunkid = xRow.Item("statusunkid")
'    '                    objEmpFld5._Userunkid = User._Object._Userunkid
'    '                    objEmpFld5._Voiddatetime = Nothing
'    '                    objEmpFld5._Voidreason = ""
'    '                    objEmpFld5._Voiduserunkid = -1
'    '                    objEmpFld5._Weight = xRow.Item("weight")

'    '                    If objEmpFld5.Insert(mDicInfoField, mdtOwner) Then
'    '                        If xDicEmpField5Unkids.ContainsKey(xRow.Item("empfield5unkid")) = False Then
'    '                            xDicEmpField5Unkids.Add(xRow.Item("empfield5unkid"), objEmpFld5._Empfield5unkid)
'    '                        End If
'    '                    Else
'    '                        If xDicEmpField5Unkids.ContainsKey(xRow.Item("empfield5unkid")) = False Then
'    '                            xDicEmpField5Unkids.Add(xRow.Item("empfield5unkid"), objEmpFld5.GetEmpFieldUnkid(objEmpFld5._Field_Data, mintNewPeriodId, xEmp.Item("EmpId")))
'    '                        End If
'    '                    End If
'    '                    '*************** START DATA TRANSFER PROCESS **************' END
'    '                Next
'    '            End If

'    '        Next

'    '        objEmpInfoField = Nothing
'    '        objEmpOwner = Nothing
'    '        xPB.Value += 1
'    '        Return True
'    '    Catch ex As Exception
'    '        DisplayError.Show("-1", ex.Message, "Transfer_Employee_Level_Data", mstrModuleName)
'    '    Finally
'    '        objEmpFld1 = Nothing : objEmpFld2 = Nothing
'    '        objEmpFld3 = Nothing : objEmpFld4 = Nothing
'    '        objEmpFld5 = Nothing
'    '    End Try
'    'End Function
'    'S.SANDEEP [03 JUN 2016] -- END

'    Private Function Transfer_Custom_Items() As Boolean
'        Dim StrQ As String = String.Empty
'        Dim dsCItems As New DataSet
'        Dim objCItems As clsassess_custom_items
'        Try
'            Using objDo As New clsDataOperation

'                'Shani (26-Sep-2016) -- Start
'                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                'StrQ = "SELECT customheaderunkid,itemtypeid,selectionmodeid,custom_item,viewmodeid " & _
'                '       "FROM hrassess_custom_items WHERE isactive = 1 AND periodunkid = @periodunkid "
'                StrQ = "SELECT customheaderunkid,itemtypeid,selectionmodeid,custom_item,viewmodeid,isdefaultentry " & _
'                       "FROM hrassess_custom_items WHERE isactive = 1 AND periodunkid = @periodunkid "
'                'Shani (26-Sep-2016) -- End

'                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldPeriodId)

'                dsCItems = objDo.ExecQuery(StrQ, "List")

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If
'            End Using

'            If dsCItems.Tables(0).Rows.Count <= 0 Then Return True

'            For Each xRow As DataRow In dsCItems.Tables(0).Rows
'                Application.DoEvents()
'                objCItems = New clsassess_custom_items

'                If objCItems.isExist(xRow.Item("custom_item"), xRow.Item("customheaderunkid"), mintNewPeriodId) = False Then

'                    objCItems._Custom_Field = xRow.Item("custom_item")
'                    objCItems._Customheaderunkid = xRow.Item("customheaderunkid")
'                    objCItems._Isactive = True
'                    objCItems._ItemTypeId = xRow.Item("itemtypeid")
'                    objCItems._Periodunkid = mintNewPeriodId
'                    objCItems._SelectionModeid = xRow.Item("selectionmodeid")
'                    objCItems._ViewModeId = xRow.Item("viewmodeid")
'                    'Shani (26-Sep-2016) -- Start
'                    'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
'                    objCItems._IsDefualtEntry = xRow.Item("isdefaultentry")
'                    'Shani (26-Sep-2016) -- End


'                    If objCItems.Insert(User._Object._Userunkid) = False Then
'                        If objCItems._Message <> "" Then
'                            mstrMessage = objCItems._Message
'                        End If
'                        Return False
'                    End If
'                End If

'                objCItems = Nothing
'            Next
'            xPB.Value += 1
'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Transfer_Custom_Items", mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Private Function Transfer_Competencies_Items() As Boolean
'        Dim StrQ As String = String.Empty
'        Dim dsCompetancy As New DataSet
'        Dim objCompetencies As clsassess_competencies_master
'        Try

'            'S.SANDEEP [15 FEB 2016] -- START
'            'Using objDo As New clsDataOperation
'            '    StrQ = "SELECT competence_categoryunkid,scalemasterunkid " & _
'            '           ",name,description,name1,name2,competencegroupunkid FROM hrassess_competencies_master WHERE isactive = 1 AND periodunkid = @periodunkid  "
'            '    'Shani(06-Feb-2016) -- [competencegroupunkid]
'            '    objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldPeriodId)

'            '    dsCompetancy = objDo.ExecQuery(StrQ, "List")

'            '    If objDo.ErrorMessage <> "" Then
'            '        Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'            '    End If

'            'End Using

'            'If dsCompetancy.Tables(0).Rows.Count <= 0 Then Return True

'            'For Each xRow As DataRow In dsCompetancy.Tables(0).Rows
'            '    Application.DoEvents()
'            '    objCompetencies = New clsassess_competencies_master

'            '    'S.SANDEEP [21 NOV 2015] -- START
'            '    'If objCompetencies.isExist(xRow.Item("competence_categoryunkid"), xRow.Item("name"), mintNewPeriodId) = False Then
'            '    If objCompetencies.isExist(xRow.Item("competence_categoryunkid"), xRow.Item("name"), mintNewPeriodId, xRow.Item("competencegroupunkid")) = False Then
'            '        'S.SANDEEP [21 NOV 2015] -- END
'            '        objCompetencies._Competence_Categoryunkid = xRow.Item("competence_categoryunkid")
'            '        objCompetencies._Isactive = True
'            '        objCompetencies._Description = xRow.Item("description")
'            '        objCompetencies._Name = xRow.Item("name")
'            '        objCompetencies._Name1 = xRow.Item("name1")
'            '        objCompetencies._Name2 = xRow.Item("name2")
'            '        objCompetencies._Periodunkid = mintNewPeriodId
'            '        objCompetencies._Scalemasterunkid = xRow.Item("scalemasterunkid")
'            '        'S.SANDEEP [13 JAN 2016] -- START
'            '        objCompetencies._Competenciesunkid = xRow.Item("competencegroupunkid")
'            '        'S.SANDEEP [13 JAN 2016] -- END


'            '        If objCompetencies.Insert() = False Then
'            '            If objCompetencies._Message <> "" Then
'            '                mstrMessage = objCompetencies._Message
'            '            End If
'            '            Return False
'            '        End If
'            '    End If
'            '    objCompetencies = Nothing
'            'Next
'            'xPB.Value += 1

'            Using objDo As New clsDataOperation

'                StrQ = "DECLARE @NGUID AS NVARCHAR(500) " & _
'                       "SET @NGUID = NEWID() " & _
'                       "INSERT INTO hrassess_movedcompetencies_master " & _
'                       "( " & _
'                       "     competenciesguid " & _
'                       "    ,competenciesunkid " & _
'                       "    ,competence_categoryunkid " & _
'                       "    ,periodunkid " & _
'                       "    ,scalemasterunkid " & _
'                       "    ,name " & _
'                       "    ,description " & _
'                       "    ,name1 " & _
'                       "    ,name2 " & _
'                       "    ,isactive " & _
'                       "    ,competencegroupunkid " & _
'                       ") " & _
'                       "SELECT " & _
'                       "     @NGUID " & _
'                       "    ,competenciesunkid " & _
'                       "    ,competence_categoryunkid " & _
'                       "    ,periodunkid " & _
'                       "    ,scalemasterunkid " & _
'                       "    ,name " & _
'                       "    ,description " & _
'                       "    ,name1 " & _
'                       "    ,name2 " & _
'                       "    ,isactive " & _
'                       "    ,competencegroupunkid " & _
'                       "FROM hrassess_competencies_master WHERE hrassess_competencies_master.isactive = 1 " & _
'                       "    AND hrassess_competencies_master.periodunkid = '" & mintOldPeriodId & "' " & _
'                       "    AND NOT EXISTS(SELECT * FROM hrassess_movedcompetencies_master " & _
'                       "                   WHERE hrassess_movedcompetencies_master.competenciesunkid = hrassess_competencies_master.competenciesunkid " & _
'                       "                   AND hrassess_movedcompetencies_master.periodunkid = '" & mintOldPeriodId & "') "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'                StrQ = "SELECT competenciesunkid,competence_categoryunkid,scalemasterunkid " & _
'                       ",name,description,name1,name2,competencegroupunkid FROM hrassess_competencies_master WHERE isactive = 1 AND periodunkid = @periodunkid  "

'                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldPeriodId)

'                dsCompetancy = objDo.ExecQuery(StrQ, "List")

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If


'                'Shani(26-APR-2016) -- Start
'                StrQ = "update hrassess_competencies_master SET isactive = 0 FROM ( " & _
'                       "    SELECT " & _
'                       "        b.competenciesunkid " & _
'                       "    FROM ( " & _
'                       "            SELECT competenciesunkid,name FROM hrassess_competencies_master WHERE isactive = 1 AND periodunkid = " & mintOldPeriodId & " " & _
'                       "        ) AS A " & _
'                       "        JOIN ( " & _
'                       "            SELECT competenciesunkid,name FROM hrassess_competencies_master WHERE isactive = 1 AND periodunkid = " & mintNewPeriodId & " " & _
'                       "        ) AS B ON B.name = A.name ) AS c WHERE c.competenciesunkid = hrassess_competencies_master.competenciesunkid AND periodunkid = " & mintNewPeriodId & " "

'                objDo.ExecNonQuery(StrQ)

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If
'                'Shani(26-APR-2016) -- End 



'                For Each xRow As DataRow In dsCompetancy.Tables(0).Rows
'                    Application.DoEvents()
'                    objCompetencies = New clsassess_competencies_master

'                    objCompetencies._Competenciesunkid(objDo) = xRow.Item("competenciesunkid")
'                    objCompetencies._Periodunkid = mintNewPeriodId

'                    If objCompetencies.Update(objDo) = False Then
'                        If objCompetencies._Message <> "" Then
'                            mstrMessage = objCompetencies._Message
'                        End If
'                        Return False
'                    End If
'                Next
'            End Using
'            xPB.Value += 1
'            'S.SANDEEP [15 FEB 2016] -- END

'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Transfer_Competencies_Items", mstrModuleName)
'        Finally
'            objCompetencies = Nothing
'        End Try
'    End Function

'    Private Function Transfer_Competencies_Assignment() As Boolean
'        Dim StrQ As String = String.Empty
'        Dim objACM As clsassess_competence_assign_master = Nothing
'        Dim objACT As clsassess_competence_assign_tran = Nothing
'        Dim dsACM As New DataSet
'        Dim dtACT As DataTable = Nothing
'        Try
'            Using objDo As New clsDataOperation

'                'S.SANDEEP [12 FEB 2016] -- START
'                'StrQ = "SELECT assigncompetenceunkid,assessgroupunkid,weight FROM hrassess_competence_assign_master WHERE isvoid = 0 AND periodunkid = @periodunkid "
'                StrQ = "SELECT assigncompetenceunkid,assessgroupunkid,weight,employeeunkid FROM hrassess_competence_assign_master WHERE isvoid = 0 AND periodunkid = @periodunkid "
'                'S.SANDEEP [12 FEB 2016] -- END

'                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldPeriodId)

'                dsACM = objDo.ExecQuery(StrQ, "List")

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If

'            End Using

'            If dsACM.Tables(0).Rows.Count <= 0 Then Return True

'            For Each xRow As DataRow In dsACM.Tables(0).Rows
'                Application.DoEvents()
'                objACM = New clsassess_competence_assign_master

'                'S.SANDEEP [12 FEB 2016] -- START
'                If objACM.isExist(xRow.Item("assessgroupunkid"), mintNewPeriodId, , xRow("employeeunkid")) = False Then
'                    'If objACM.isExist(xRow.Item("assessgroupunkid"), mintNewPeriodId) = False Then
'                    'S.SANDEEP [12 FEB 2016] -- END

'                    objACM._Assessgroupunkid = xRow.Item("assessgroupunkid")
'                    objACM._Isvoid = False
'                    objACM._Periodunkid = mintNewPeriodId
'                    objACM._Userunkid = User._Object._Userunkid
'                    objACM._Voiddatetime = Nothing
'                    objACM._Voidreason = ""
'                    objACM._Voiduserunkid = -1
'                    objACM._Weight = xRow.Item("weight")
'                    objACM._Employeeunkid = xRow("employeeunkid")

'                    objACT = New clsassess_competence_assign_tran
'                    objACT._Assigncompetenceunkid = xRow.Item("assigncompetenceunkid")
'                    dtACT = objACT._DataTable.Copy
'                    For Each dRow As DataRow In dtACT.Rows
'                        dRow.Item("AUD") = "A"
'                    Next

'                    If objACM.Insert(dtACT) = False Then
'                        If objACM._Message <> "" Then
'                            mstrMessage = objACM._Message
'                        End If
'                        Return False
'                    End If

'                    objACT = Nothing
'                End If
'                objACM = Nothing
'            Next
'            xPB.Value += 1
'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Transfer_Competencies_Assignment", mstrModuleName)
'        Finally
'        End Try
'    End Function

'    Private Function Transfer_Computation_Formula() As Boolean
'        Dim StrQ As String = String.Empty
'        Dim objCFM As clsassess_computation_master = Nothing
'        Dim objCFT As clsassess_computation_tran = Nothing
'        Dim dsCFM As New DataSet
'        Dim dtCFT As DataTable = Nothing
'        Try
'            Using objDo As New clsDataOperation
'                StrQ = "SELECT computationunkid,formula_typeid,computation_formula FROM hrassess_computation_master WHERE isvoid = 0 AND periodunkid = @periodunkid "

'                objDo.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintOldPeriodId)

'                dsCFM = objDo.ExecQuery(StrQ, "List")

'                If objDo.ErrorMessage <> "" Then
'                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
'                End If
'            End Using

'            If dsCFM.Tables(0).Rows.Count <= 0 Then Return True

'            For Each xRow As DataRow In dsCFM.Tables(0).Rows
'                Application.DoEvents()
'                objCFM = New clsassess_computation_master
'                If objCFM.isExist(mintNewPeriodId, xRow.Item("formula_typeid")) = False Then
'                    objCFM._Computation_Formula = xRow.Item("computation_formula")
'                    objCFM._Formula_Typeid = xRow.Item("formula_typeid")
'                    objCFM._Isvoid = False
'                    objCFM._Periodunkid = mintNewPeriodId
'                    objCFM._Voiddatetime = Nothing
'                    objCFM._Voidreason = ""
'                    objCFM._Voiduserunkid = -1

'                    objCFT = New clsassess_computation_tran
'                    objCFT._Computationunkid = xRow.Item("computationunkid")
'                    dtCFT = objCFT._DataTable.Copy
'                    For Each dRow As DataRow In dtCFT.Rows
'                        dRow.Item("AUD") = "A"
'                        dRow.Item("computationunkid") = 0
'                    Next

'                    'S.SANDEEP [04 JUN 2015] -- START
'                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
'                    If objCFM.Insert(dtCFT, User._Object._Userunkid) = False Then
'                        'If objCFM.Insert(dtCFT) = False Then
'                        'S.SANDEEP [04 JUN 2015] -- END

'                        If objCFM._Message <> "" Then
'                            mstrMessage = objCFM._Message
'                        End If
'                        Return False
'                    End If
'                    objCFT = Nothing
'                End If
'                objCFM = Nothing
'            Next
'            xPB.Value += 1
'            Return True
'        Catch ex As Exception
'            DisplayError.Show("-1", ex.Message, "Transfer_Computation_Formula", mstrModuleName)
'        Finally
'        End Try
'    End Function

'#End Region

'End Class