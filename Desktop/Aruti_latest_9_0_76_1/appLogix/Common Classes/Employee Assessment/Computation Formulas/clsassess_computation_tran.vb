﻿'************************************************************************************************************************************
'Class Name : clsassess_computation_tran.vb
'Purpose    :
'Date       :02-Jan-2015
'Written By :Sandeep Sharma
'Modified   :
'************************************************************************************************************************************
Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Sandeep Sharma
''' </summary>
Public Class clsassess_computation_tran
    Private Const mstrModuleName = "clsassess_computation_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private Variables "

    Private mintComputationtranunkid As Integer = -1
    Private mintComputationunkid As Integer = -1
    Private mdtTran As DataTable

#End Region

#Region " Properties "

    Public Property _Computationunkid() As Integer
        Get
            Return mintComputationunkid
        End Get
        Set(ByVal value As Integer)
            mintComputationunkid = value
            Call GetComputation_Tran()
        End Set
    End Property

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    Public WriteOnly Property _DataOperation() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            objDataOperation = value
        End Set
    End Property

#End Region

#Region " Contructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("Function")
            mdtTran.Columns.Add("computationtranunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("computationunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("computation_typeid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("isvoid", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("voiduserunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            mdtTran.Columns.Add("voiddatetime", System.Type.GetType("System.DateTime")).DefaultValue = DBNull.Value
            mdtTran.Columns.Add("voidreason", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""
            'S.SANDEEP [19 FEB 2015] -- START
            mdtTran.Columns.Add("formulaid", System.Type.GetType("System.Int32")).DefaultValue = 0
            'S.SANDEEP [19 FEB 2015] -- END

            '******************** DISPLAY *************************' -- START
            mdtTran.Columns.Add("variable_name", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("functionid", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtTran.Columns.Add("function", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtTran.Columns.Add("headid", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtTran.Columns.Add("head", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtTran.Columns.Add("period", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtTran.Columns.Add("end_date", Type.GetType("System.String")).DefaultValue = String.Empty
            mdtTran.Columns.Add("periodunkid", System.Type.GetType("System.Int32")).DefaultValue = 0
            '******************** DISPLAY *************************' -- END
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private/Public Methods "

    'S.SANDEEP [19 FEB 2015] -- START
    Public Function PredefineFormula(ByVal xCmptTyp_Id As Integer) As Integer
        Dim xFormulaId As Integer = 0
        Try
            Using objDo As New clsDataOperation
                Dim StrQ As String = "SELECT @formulaid = formulaid FROM hrassess_computation_tran WHERE computation_typeid = '" & xCmptTyp_Id & "' AND ISNULL(formulaid,0) > 0 AND isvoid = 0 "
                objDo.AddParameter("@formulaid", SqlDbType.Int, eZeeDataType.INT_SIZE, xFormulaId, ParameterDirection.InputOutput)
                objDo.ExecNonQuery(StrQ)
                If objDo.ErrorMessage <> "" Then
                    Throw New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                End If
                xFormulaId = objDo.GetParameterValue("@formulaid")
            End Using
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "PredefineFormula", mstrModuleName)
        Finally
        End Try
        Return xFormulaId
    End Function
    'S.SANDEEP [19 FEB 2015] -- END

    Private Sub GetComputation_Tran()
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        Try
            StrQ = "SELECT " & _
                   "     computationtranunkid " & _
                   "    ,computationunkid " & _
                   "    ,computation_typeid " & _
                   "    ,isvoid " & _
                   "    ,voiduserunkid " & _
                   "    ,voiddatetime " & _
                   "    ,voidreason " & _
                   "    ,'' AS AUD " & _
                   "FROM hrassess_computation_tran " & _
                   "WHERE isvoid = 0 AND computationunkid = @computationunkid "

            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@computationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComputationunkid.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            mdtTran.Rows.Clear()

            For Each xRow As DataRow In dsList.Tables(0).Rows
                mdtTran.ImportRow(xRow)
            Next

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetComputation_Tran", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete_Computation_Tran(Optional ByVal intUserUnkid As Integer = 0) As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        If objDataOperation Is Nothing Then
            objDataOperation = New clsDataOperation
        End If
        Try
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If .Item("computation_typeid") <= 0 Then Continue For
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                StrQ = "INSERT INTO hrassess_computation_tran ( " & _
                                           "  computationunkid " & _
                                           ", computation_typeid " & _
                                           ", isvoid " & _
                                           ", voiduserunkid " & _
                                           ", voiddatetime " & _
                                           ", voidreason " & _
                                           ", formulaid " & _
                                       ") VALUES (" & _
                                           "  @computationunkid " & _
                                           ", @computation_typeid " & _
                                           ", @isvoid " & _
                                           ", @voiduserunkid " & _
                                           ", @voiddatetime " & _
                                           ", @voidreason " & _
                                           ", @formulaid " & _
                                       "); SELECT @@identity" 'S.SANDEEP [19 FEB 2015] {formulaid}

                                objDataOperation.AddParameter("@computationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComputationunkid)
                                objDataOperation.AddParameter("@computation_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("computation_typeid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                'S.SANDEEP [19 FEB 2015] -- START
                                objDataOperation.AddParameter("@formulaid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("formulaid"))
                                'S.SANDEEP [19 FEB 2015] -- END

                                Dim dsList As New DataSet
                                dsList = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                mintComputationtranunkid = dsList.Tables(0).Rows(0).Item(0)

                                If .Item("computationunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_computation_master", "computationunkid", .Item("computationunkid"), "hrassess_computation_tran", "computationtranunkid", mintComputationtranunkid, 2, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_computation_master", "computationunkid", mintComputationunkid, "hrassess_computation_tran", "computationtranunkid", mintComputationtranunkid, 1, 1, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                            Case "U"

                                StrQ = "UPDATE hrassess_computation_tran SET " & _
                                       "  computationunkid = @computationunkid" & _
                                       ", computation_typeid = @computation_typeid" & _
                                       ", isvoid = @isvoid" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voidreason = @voidreason " & _
                                       ", formulaid = @formulaid " & _
                                       "WHERE computationtranunkid = @computationtranunkid " 'S.SANDEEP [19 FEB 2015] {formulaid}

                                objDataOperation.AddParameter("@computationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("computationtranunkid"))
                                objDataOperation.AddParameter("@computationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("computationunkid"))
                                objDataOperation.AddParameter("@computation_typeid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("computation_typeid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                'S.SANDEEP [19 FEB 2015] -- START
                                objDataOperation.AddParameter("@formulaid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("formulaid"))
                                'S.SANDEEP [19 FEB 2015] -- END

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_computation_master", "computationunkid", .Item("computationunkid"), "hrassess_computation_tran", "computationtranunkid", .Item("computationtranunkid"), 2, 2, , intUserUnkid) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"

                                If .Item("computationtranunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_computation_master", "computationunkid", .Item("computationunkid"), "hrassess_computation_tran", "computationtranunkid", .Item("computationtranunkid"), 2, 3, , intUserUnkid) = False Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If

                                StrQ = "UPDATE hrassess_computation_tran SET " & _
                                       "  isvoid = @isvoid" & _
                                       ", voiduserunkid = @voiduserunkid" & _
                                       ", voiddatetime = @voiddatetime" & _
                                       ", voidreason = @voidreason " & _
                                       "WHERE computationtranunkid = @computationtranunkid "

                                objDataOperation.AddParameter("@computationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("computationtranunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_Computation_Tran", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

End Class

'Private Sub GetComputation_Tran()
'    Dim StrQ As String = ""
'    Dim dsList As New DataSet
'    Dim exForce As Exception
'    If objDataOperation Is Nothing Then
'        objDataOperation = New clsDataOperation
'    End If
'    Try
'        StrQ = "SELECT " & _
'               "     computationtranunkid " & _
'               "    ,computationunkid " & _
'               "    ,computation_typeid " & _
'               "    ,isvoid " & _
'               "    ,voiduserunkid " & _
'               "    ,voiddatetime " & _
'               "    ,voidreason " & _
'               "    ,'' AS AUD " & _
'               "    ,CASE WHEN computation_typeid = '" & enAssess_Computation_Types.BSC_ITEM_WEIGHT & "' THEN @BSC_ITEM_WEIGHT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.BSC_ITEM_EMP_SCORE_VALUE & "' THEN @BSC_ITEM_EMP_SCORE_VALUE " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.BSC_ITEM_ASR_SCORE_VALUE & "' THEN @BSC_ITEM_ASR_SCORE_VALUE " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.BSC_ITEM_REV_SCORE_VALUE & "' THEN @BSC_ITEM_REV_SCORE_VALUE " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.BSC_PARENT_FIELD_WEIGHT & "' THEN @BSC_PARENT_FIELD_WEIGHT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.BSC_ITEM_MAX_SCORE_VALUE & "' THEN @BSC_ITEM_MAX_SCORE_VALUE " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.BSC_OVERALL_WEIGHT & "' THEN @BSC_OVERALL_WEIGHT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_ITEM_WEIGHT & "' THEN @CMP_ITEM_WEIGHT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_ITEM_EMP_SCORE_VALUE & "' THEN @CMP_ITEM_EMP_SCORE_VALUE " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_ITEM_ASR_SCORE_VALUE & "' THEN @CMP_ITEM_ASR_SCORE_VALUE " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_ITEM_REV_SCORE_VALUE & "' THEN @CMP_ITEM_REV_SCORE_VALUE " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_CATEGORY_WEIGHT & "' THEN @CMP_CATEGORY_WEIGHT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_ITEM_MAX_SCORE_VALUE & "' THEN @CMP_ITEM_MAX_SCORE_VALUE " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_WEIGHT & "' THEN @CMP_ASSESSMENT_GROUP_WEIGHT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_OVERALL_WEIGHT & "' THEN @CMP_OVERALL_WEIGHT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.BSC_PARENT_ITEMS_ALL_PERSPECTIVE_COUNT & "' THEN @BSC_PARENT_ITEMS_ALL_PERSPECTIVE_COUNT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.BSC_LINKED_ITEMS_ALL_PERSPECTIVE_COUNT & "' THEN @BSC_LINKED_ITEMS_ALL_PERSPECTIVE_COUNT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_ASSESSMENT_GROUP_COUNT & "' THEN @CMP_ASSESSMENT_GROUP_COUNT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_ITEMS_PER_CATEGORY_COUNT & "' THEN @CMP_ITEMS_PER_CATEGORY_COUNT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_CATEGORY_PER_GROUP_COUNT & "' THEN @CMP_CATEGORY_PER_GROUP_COUNT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_ITEMS_ALL_CATEGORY_GROUP_COUNT & "' THEN @CMP_ITEMS_ALL_CATEGORY_GROUP_COUNT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_CATEGORY_ALL_GROUP_COUNT & "' THEN @CMP_CATEGORY_ALL_GROUP_COUNT " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.BSC_RATIO_VALUE & "' THEN @BSC_RATIO_VALUE " & _
'               "          WHEN computation_typeid = '" & enAssess_Computation_Types.CMP_RATIO_VALUE & "' THEN @CMP_RATIO_VALUE " & _
'               "    END AS variable_name " & _
'               "FROM hrassess_computation_tran " & _
'               "WHERE isvoid = 0 AND computationunkid = @computationunkid "

'        objDataOperation.ClearParameters()

'        objDataOperation.AddParameter("@computationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintComputationunkid.ToString)

'        objDataOperation.AddParameter("@BSC_ITEM_WEIGHT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 592, "BSC Item Weight"))
'        objDataOperation.AddParameter("@BSC_ITEM_EMP_SCORE_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 593, "BSC Employee Score"))
'        objDataOperation.AddParameter("@BSC_ITEM_ASR_SCORE_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 594, "BSC Assessor Score"))
'        objDataOperation.AddParameter("@BSC_ITEM_REV_SCORE_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 595, "BSC Reviewer Score"))
'        objDataOperation.AddParameter("@BSC_PARENT_FIELD_WEIGHT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 596, "Parent Field Weight"))
'        objDataOperation.AddParameter("@BSC_ITEM_MAX_SCORE_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 597, "BSC Item Max Score"))
'        objDataOperation.AddParameter("@BSC_OVERALL_WEIGHT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 598, "BSC Overall Weight"))
'        objDataOperation.AddParameter("@CMP_ITEM_WEIGHT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 599, "Competence Item Weight"))
'        objDataOperation.AddParameter("@CMP_ITEM_EMP_SCORE_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 600, "Competence Employee Score"))
'        objDataOperation.AddParameter("@CMP_ITEM_ASR_SCORE_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 601, "Competence Assessor Score"))
'        objDataOperation.AddParameter("@CMP_ITEM_REV_SCORE_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 602, "Competence Reviewer Score"))
'        objDataOperation.AddParameter("@CMP_CATEGORY_WEIGHT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 603, "Competence Category Weight"))
'        objDataOperation.AddParameter("@CMP_ITEM_MAX_SCORE_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 604, "Competence Item Max Score"))
'        objDataOperation.AddParameter("@CMP_ASSESSMENT_GROUP_WEIGHT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 605, "Assessment Group Weight"))
'        objDataOperation.AddParameter("@CMP_OVERALL_WEIGHT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 606, "Competence Overall Weight"))
'        objDataOperation.AddParameter("@BSC_PARENT_ITEMS_ALL_PERSPECTIVE_COUNT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 607, "Count Of BSC Parent Items Across All Perspectives"))
'        objDataOperation.AddParameter("@BSC_LINKED_ITEMS_ALL_PERSPECTIVE_COUNT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 608, "Count Of BSC Linked Field Items Across All Perspectives"))
'        objDataOperation.AddParameter("@CMP_ASSESSMENT_GROUP_COUNT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 609, "Number Of Assessment Groups For An Employee"))
'        objDataOperation.AddParameter("@CMP_ITEMS_PER_CATEGORY_COUNT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 610, "Count Of Competence Items In Assessment Group"))
'        objDataOperation.AddParameter("@CMP_CATEGORY_PER_GROUP_COUNT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 611, "Count Of Competence Categories In A Assessment Groups"))
'        objDataOperation.AddParameter("@CMP_ITEMS_ALL_CATEGORY_GROUP_COUNT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 612, "Count Of Competence Items Across All Assessment Groups"))
'        objDataOperation.AddParameter("@CMP_CATEGORY_ALL_GROUP_COUNT", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 613, "Count Of Competence Categories Across All Assessment Groups"))
'        objDataOperation.AddParameter("@BSC_RATIO_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 614, "BSC Assessment Ratio"))
'        objDataOperation.AddParameter("@CMP_RATIO_VALUE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 615, "Competence Assessment Ratio"))

'        dsList = objDataOperation.ExecQuery(StrQ, "List")

'        If objDataOperation.ErrorMessage <> "" Then
'            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
'            Throw exForce
'        End If
'        mdtTran.Rows.Clear()

'        For Each xRow As DataRow In dsList.Tables(0).Rows

'        Next

'    Catch ex As Exception
'        DisplayError.Show("-1", ex.Message, "GetComputation_Tran", mstrModuleName)
'    Finally
'    End Try
'End Sub