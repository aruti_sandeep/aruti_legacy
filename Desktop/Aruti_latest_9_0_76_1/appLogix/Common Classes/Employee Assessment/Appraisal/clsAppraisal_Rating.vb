﻿'************************************************************************************************************************************
'Class Name :clsAppraisal_Rating.vb
'Purpose    :
'Date       :17 Aug 2013
'Written By :Sandeep J. Sharma
'Modified   :
'************************************************************************************************************************************

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class clsAppraisal_Rating

#Region " Private Variable "

    Private Shared ReadOnly mstrModuleName As String = "clsAppraisal_Rating"
    Private objDataOperation As clsDataOperation
    Private mstrMessage As String = ""
    Private mdtTran As DataTable

#End Region

#Region " Properties "

    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("List")
            Dim dCol As DataColumn

            dCol = New DataColumn
            dCol.ColumnName = "appratingunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "trandatetime"
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "score_from"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "score_to"
            dCol.DataType = System.Type.GetType("System.Decimal")
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "grade_award"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "appactionunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "userunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "isvoid"
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiduserunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voiddatetime"
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "voidreason"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            '////////////////////////// DISPLAY
            dCol = New DataColumn
            dCol.ColumnName = "action"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)
            '////////////////////////// DISPLAY

            dCol = New DataColumn
            dCol.ColumnName = "AUD"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "GUID"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
            dCol = New DataColumn
            dCol.ColumnName = "goal_appr_actionunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "competence_appr_actionunkid"
            dCol.DataType = System.Type.GetType("System.Int32")
            dCol.DefaultValue = -1
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "bsc_action"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn
            dCol.ColumnName = "ge_action"
            dCol.DataType = System.Type.GetType("System.String")
            dCol.DefaultValue = ""
            mdtTran.Columns.Add(dCol)
            'Shani (26-Sep-2016) -- End

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            dCol = New DataColumn
            dCol.ColumnName = "distribution"
            dCol.DataType = GetType(System.Decimal)
            dCol.DefaultValue = 0
            mdtTran.Columns.Add(dCol)
            'S.SANDEEP |27-JUL-2019| -- END

            Call Get_Data()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Private Methods "

    Private Sub Get_Data()
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation

            'Shani (26-Sep-2016) -- Start
            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)

            'StrQ = "SELECT " & _
            '         "	 appratingunkid " & _
            '         "	,trandatetime " & _
            '         "	,score_from " & _
            '         "	,score_to " & _
            '         "	,grade_award " & _
            '         "	,appactionunkid " & _
            '         "	,userunkid " & _
            '         "	,isvoid " & _
            '         "	,voiduserunkid " & _
            '         "	,voiddatetime " & _
            '         "	,voidreason " & _
            '         "	,ISNULL(cfcommon_master.name,'') AS [action] " & _
            '         "	,'' AS AUD " & _
            '         "FROM hrapps_ratings " & _
            '         "	LEFT JOIN cfcommon_master ON dbo.cfcommon_master.masterunkid = hrapps_ratings.appactionunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & "' " & _
            '         "WHERE isvoid = 0 "

            StrQ = "SELECT " & _
                     "	 appratingunkid " & _
                     "	,trandatetime " & _
                     "	,score_from " & _
                     "	,score_to " & _
                     "	,grade_award " & _
                     "	,appactionunkid " & _
                     "	,userunkid " & _
                     "	,isvoid " & _
                     "	,voiduserunkid " & _
                     "	,voiddatetime " & _
                     "	,voidreason " & _
                     "	,ISNULL(cfcommon_master.name,'') AS [action] " & _
                   "	,goal_appr_actionunkid " & _
                   "	,competence_appr_actionunkid " & _
                   "	,ISNULL(BSCACTION.name,'') AS [bsc_action] " & _
                   "	,ISNULL(GEACTION.name,'') AS [ge_action] " & _
                     "	,'' AS AUD " & _
                     "  ,ISNULL(hrapps_ratings.distribution,0) AS distribution " & _
                     "FROM hrapps_ratings " & _
                   "	LEFT JOIN cfcommon_master ON dbo.cfcommon_master.masterunkid = hrapps_ratings.appactionunkid AND cfcommon_master.mastertype = '" & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & "' " & _
                   "	LEFT JOIN cfcommon_master AS BSCACTION ON BSCACTION.masterunkid = hrapps_ratings.goal_appr_actionunkid AND BSCACTION.mastertype = '" & clsCommon_Master.enCommonMaster.BSC_APPRAISAL_ACTIONS & "' " & _
                   "	LEFT JOIN cfcommon_master AS GEACTION  ON GEACTION.masterunkid = hrapps_ratings.competence_appr_actionunkid AND GEACTION.mastertype = '" & clsCommon_Master.enCommonMaster.GE_APPRAISAL_ACTIONS & "' " & _
                     "WHERE isvoid = 0 "
            'Shani (26-Sep-2016) -- End
            'S.SANDEEP |27-JUL-2019| -- START {distribution} -- END

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("List").Rows
                mdtTran.ImportRow(dRow)
            Next            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Get_Data", mstrModuleName)
        Finally
        End Try
    End Sub

    Public Function InsertUpdateDelete() As Boolean
        Dim i As Integer
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'StrQ = "INSERT INTO hrapps_ratings ( " & _
                                '           "  trandatetime " & _
                                '           ", score_from " & _
                                '           ", score_to " & _
                                '           ", grade_award " & _
                                '           ", appactionunkid " & _
                                '           ", userunkid " & _
                                '           ", isvoid " & _
                                '           ", voiduserunkid " & _
                                '           ", voiddatetime " & _
                                '           ", voidreason" & _
                                '       ") VALUES (" & _
                                '           "  @trandatetime " & _
                                '           ", @score_from " & _
                                '           ", @score_to " & _
                                '           ", @grade_award " & _
                                '           ", @appactionunkid " & _
                                '           ", @userunkid " & _
                                '           ", @isvoid " & _
                                '           ", @voiduserunkid " & _
                                '           ", @voiddatetime " & _
                                '           ", @voidreason" & _
                                '       "); SELECT @@identity"
                                StrQ = "INSERT INTO hrapps_ratings ( " & _
                                           "  trandatetime " & _
                                           ", score_from " & _
                                           ", score_to " & _
                                           ", grade_award " & _
                                           ", appactionunkid " & _
                                           ", userunkid " & _
                                           ", isvoid " & _
                                           ", voiduserunkid " & _
                                           ", voiddatetime " & _
                                           ", voidreason" & _
                                           ", goal_appr_actionunkid" & _
                                           ", competence_appr_actionunkid" & _
                                           ", distribution " & _
                                       ") VALUES (" & _
                                           "  @trandatetime " & _
                                           ", @score_from " & _
                                           ", @score_to " & _
                                           ", @grade_award " & _
                                           ", @appactionunkid " & _
                                           ", @userunkid " & _
                                           ", @isvoid " & _
                                           ", @voiduserunkid " & _
                                           ", @voiddatetime " & _
                                           ", @voidreason" & _
                                           ", @goal_appr_actionunkid" & _
                                           ", @competence_appr_actionunkid" & _
                                           ", @distribution " & _
                                       "); SELECT @@identity"
                                'Shani (26-Sep-2016) -- End
                                'S.SANDEEP |27-JUL-2019| -- START {distribution} -- END

                                objDataOperation.AddParameter("@trandatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("trandatetime"))
                                objDataOperation.AddParameter("@score_from", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("score_from"))
                                objDataOperation.AddParameter("@score_to", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("score_to"))
                                objDataOperation.AddParameter("@grade_award", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("grade_award"))
                                objDataOperation.AddParameter("@appactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("appactionunkid"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                objDataOperation.AddParameter("@goal_appr_actionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("goal_appr_actionunkid"))
                                objDataOperation.AddParameter("@competence_appr_actionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("competence_appr_actionunkid"))
                                'Shani (26-Sep-2016) -- End

                                'S.SANDEEP |27-JUL-2019| -- START
                                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                                objDataOperation.AddParameter("@distribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("distribution"))
                                'S.SANDEEP |27-JUL-2019| -- END

                                dsList = objDataOperation.ExecQuery(StrQ, "List")

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrapps_ratings", "appratingunkid", dsList.Tables(0).Rows(0).Item(0)) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "U"

                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                'StrQ = "UPDATE hrapps_ratings SET " & _
                                '          "  trandatetime = @trandatetime" & _
                                '          ", score_from = @score_from" & _
                                '          ", score_to = @score_to" & _
                                '          ", grade_award = @grade_award" & _
                                '          ", appactionunkid = @appactionunkid" & _
                                '          ", userunkid = @userunkid" & _
                                '          ", isvoid = @isvoid" & _
                                '          ", voiduserunkid = @voiduserunkid" & _
                                '          ", voiddatetime = @voiddatetime" & _
                                '          ", voidreason = @voidreason " & _
                                '       "WHERE appratingunkid = @appratingunkid "

                                StrQ = "UPDATE hrapps_ratings SET " & _
                                          "  trandatetime = @trandatetime" & _
                                          ", score_from = @score_from" & _
                                          ", score_to = @score_to" & _
                                          ", grade_award = @grade_award" & _
                                          ", appactionunkid = @appactionunkid" & _
                                          ", userunkid = @userunkid" & _
                                          ", isvoid = @isvoid" & _
                                          ", voiduserunkid = @voiduserunkid" & _
                                          ", voiddatetime = @voiddatetime" & _
                                          ", voidreason = @voidreason " & _
                                          ", goal_appr_actionunkid = @goal_appr_actionunkid " & _
                                          ", competence_appr_actionunkid = @competence_appr_actionunkid " & _
                                          ", distribution = @distribution " & _
                                       "WHERE appratingunkid = @appratingunkid "
                                'Shani (26-Sep-2016) -- End
                                'S.SANDEEP |27-JUL-2019| -- START {distribution} -- END

                                objDataOperation.AddParameter("@appratingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("appratingunkid"))
                                objDataOperation.AddParameter("@trandatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("trandatetime"))
                                objDataOperation.AddParameter("@score_from", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("score_from"))
                                objDataOperation.AddParameter("@score_to", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("score_to"))
                                objDataOperation.AddParameter("@grade_award", SqlDbType.NText, eZeeDataType.NAME_SIZE, .Item("grade_award"))
                                objDataOperation.AddParameter("@appactionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("appactionunkid"))
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))
                                
                                'Shani (26-Sep-2016) -- Start
                                'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
                                objDataOperation.AddParameter("@goal_appr_actionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("goal_appr_actionunkid"))
                                objDataOperation.AddParameter("@competence_appr_actionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("competence_appr_actionunkid"))
                                'Shani (26-Sep-2016) -- End

                                'S.SANDEEP |27-JUL-2019| -- START
                                'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
                                objDataOperation.AddParameter("@distribution", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, .Item("distribution"))
                                'S.SANDEEP |27-JUL-2019| -- END

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrapps_ratings", "appratingunkid", .Item("appratingunkid")) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                            Case "D"
                                StrQ = "UPDATE hrapps_ratings SET " & _
                                        "  isvoid = @isvoid" & _
                                        ", voiduserunkid = @voiduserunkid" & _
                                        ", voiddatetime = @voiddatetime" & _
                                        ", voidreason = @voidreason " & _
                                       "WHERE appratingunkid = @appratingunkid "

                                objDataOperation.AddParameter("@appratingunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("appratingunkid"))
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid"))
                                If IsDBNull(.Item("voiddatetime")) = False Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime"))
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                End If
                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason"))

                                Call objDataOperation.ExecNonQuery(StrQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                                If clsCommonATLog.Insert_AtLog(objDataOperation, 3, "hrapps_ratings", "appratingunkid", .Item("appratingunkid")) = False Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If

                        End Select
                    End If
                End With
            Next
            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete", mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP |27-MAY-2019| -- START
    'ISSUE/ENHANCEMENT : [Score Calibration Process in Performance & Assessment Module]
    'Public Function getComboList(ByVal sList As String, Optional ByVal mFlag As Boolean = False) As DataSet
    'Public Function getComboList(ByVal sList As String, Optional ByVal mFlag As Boolean = False) As DataSet
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim dsList As New DataSet
    '    Try
    '        bjDataOperation = New clsDataOperation
    '        If mFlag = True Then

    '            'Shani (26-Sep-2016) -- Start
    '            'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '            'StrQ = "SELECT 0 AS id,+' '+@Select AS name,0 AS scrf, 0 scrt, 0 AS appactionunkid, '' AS iAction UNION "
    '            StrQ = "SELECT 0 AS id,+' '+@Select AS name,0 AS scrf, 0 scrt, 0 AS appactionunkid, '' AS iAction, 0 AS goal_appr_actionunkid, 0 AS competence_appr_actionunkid, '' AS bsc_action, '' AS ge_action UNION "
    '            'Shani (26-Sep-2016) -- End
    '        End If

    '        'Shani (26-Sep-2016) -- Start
    '        'Enhancement -Changes PA Flow And Add New screen Given By (Andrew)
    '        'StrQ &= "SELECT " & _
    '        '        "  hrapps_ratings.appratingunkid AS id " & _
    '        '        " ,hrapps_ratings.grade_award AS name " & _
    '        '        " ,hrapps_ratings.score_from AS scrf " & _
    '        '        " ,hrapps_ratings.score_to AS scrt " & _
    '        '        " ,hrapps_ratings.appactionunkid " & _
    '        '        " ,ISNULL(cfcommon_master.name,'') AS iAction " & _
    '        '        "FROM hrapps_ratings " & _
    '        '        " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrapps_ratings.appactionunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & "' " & _
    '        '        "WHERE isvoid = 0 "

            'StrQ &= "SELECT " & _
            '        "  hrapps_ratings.appratingunkid AS id " & _
            '        " ,hrapps_ratings.grade_award AS name " & _
            '        " ,hrapps_ratings.score_from AS scrf " & _
            '        " ,hrapps_ratings.score_to AS scrt " & _
            '        " ,hrapps_ratings.appactionunkid " & _
            '        " ,ISNULL(cfcommon_master.name,'') AS iAction " & _
    '                " ,goal_appr_actionunkid " & _
    '                " ,competence_appr_actionunkid " & _
    '                " ,ISNULL(BSCACTION.name,'') AS [bsc_action] " & _
    '                " ,ISNULL(GEACTION.name,'') AS [ge_action] " & _
            '        "FROM hrapps_ratings " & _
            '        " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = hrapps_ratings.appactionunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & "' " & _
    '                "	LEFT JOIN cfcommon_master AS BSCACTION ON BSCACTION.masterunkid = hrapps_ratings.goal_appr_actionunkid AND BSCACTION.mastertype = '" & clsCommon_Master.enCommonMaster.BSC_APPRAISAL_ACTIONS & "' " & _
    '                "	LEFT JOIN cfcommon_master AS GEACTION  ON GEACTION.masterunkid = hrapps_ratings.competence_appr_actionunkid AND GEACTION.mastertype = '" & clsCommon_Master.enCommonMaster.GE_APPRAISAL_ACTIONS & "' " & _
            '        "WHERE isvoid = 0 "
    '        'Shani (26-Sep-2016) -- End

    '        objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

    '        dsList = objDataOperation.ExecQuery(StrQ, sList)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return dsList

    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
    '        Return Nothing
    '    Finally
    '    End Try
    'End Function

    Public Function getComboList(ByVal sList As String, Optional ByVal mFlag As Boolean = False, Optional ByVal strDatabaseName As String = "") As DataSet
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Try
            If strDatabaseName.Trim.Length <= 0 Then strDatabaseName = FinancialYear._Object._DatabaseName

            objDataOperation = New clsDataOperation

            'S.SANDEEP |27-JUL-2019| -- START
            'ISSUE/ENHANCEMENT : CALIBRATION COMMENTS FROM NMB
            'If mFlag = True Then
            '    StrQ = "SELECT 0 AS id,+' '+@Select AS name,0 AS scrf, 0 scrt, 0 AS appactionunkid, '' AS iAction, 0 AS goal_appr_actionunkid, 0 AS competence_appr_actionunkid, '' AS bsc_action, '' AS ge_action UNION "
            'End If

            'StrQ &= "SELECT " & _
            '        "  hrapps_ratings.appratingunkid AS id " & _
            '        " ,hrapps_ratings.grade_award AS name " & _
            '        " ,hrapps_ratings.score_from AS scrf " & _
            '        " ,hrapps_ratings.score_to AS scrt " & _
            '        " ,hrapps_ratings.appactionunkid " & _
            '        " ,ISNULL(cfcommon_master.name,'') AS iAction " & _
            '       "    ,goal_appr_actionunkid " & _
            '       "	,competence_appr_actionunkid " & _
            '       "	,ISNULL(BSCACTION.name,'') AS [bsc_action] " & _
            '       "	,ISNULL(GEACTION.name,'') AS [ge_action] " & _
            '        "FROM " & strDatabaseName & "..hrapps_ratings " & _
            '        "   LEFT JOIN " & strDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = hrapps_ratings.appactionunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & "' " & _
            '        "	LEFT JOIN " & strDatabaseName & "..cfcommon_master AS BSCACTION ON BSCACTION.masterunkid = hrapps_ratings.goal_appr_actionunkid AND BSCACTION.mastertype = '" & clsCommon_Master.enCommonMaster.BSC_APPRAISAL_ACTIONS & "' " & _
            '        "	LEFT JOIN " & strDatabaseName & "..cfcommon_master AS GEACTION  ON GEACTION.masterunkid = hrapps_ratings.competence_appr_actionunkid AND GEACTION.mastertype = '" & clsCommon_Master.enCommonMaster.GE_APPRAISAL_ACTIONS & "' " & _
            '        "WHERE isvoid = 0 "
            ''Shani (26-Sep-2016) -- End


            If mFlag = True Then
                StrQ = "SELECT 0 AS id,+' '+@Select AS name,0 AS scrf, 0 scrt, 0 AS appactionunkid, '' AS iAction, 0 AS goal_appr_actionunkid, 0 AS competence_appr_actionunkid, '' AS bsc_action, '' AS ge_action, 0 AS distribution UNION "
            End If

            StrQ &= "SELECT " & _
                    "  hrapps_ratings.appratingunkid AS id " & _
                    " ,hrapps_ratings.grade_award AS name " & _
                    " ,hrapps_ratings.score_from AS scrf " & _
                    " ,hrapps_ratings.score_to AS scrt " & _
                    " ,hrapps_ratings.appactionunkid " & _
                    " ,ISNULL(cfcommon_master.name,'') AS iAction " & _
                   "    ,goal_appr_actionunkid " & _
                   "	,competence_appr_actionunkid " & _
                   "	,ISNULL(BSCACTION.name,'') AS [bsc_action] " & _
                   "	,ISNULL(GEACTION.name,'') AS [ge_action] " & _
                    " ,ISNULL(hrapps_ratings.distribution,0) AS distribution " & _
                    "FROM " & strDatabaseName & "..hrapps_ratings " & _
                    "   LEFT JOIN " & strDatabaseName & "..cfcommon_master ON cfcommon_master.masterunkid = hrapps_ratings.appactionunkid AND mastertype = '" & clsCommon_Master.enCommonMaster.APPRAISAL_ACTIONS & "' " & _
                    "	LEFT JOIN " & strDatabaseName & "..cfcommon_master AS BSCACTION ON BSCACTION.masterunkid = hrapps_ratings.goal_appr_actionunkid AND BSCACTION.mastertype = '" & clsCommon_Master.enCommonMaster.BSC_APPRAISAL_ACTIONS & "' " & _
                    "	LEFT JOIN " & strDatabaseName & "..cfcommon_master AS GEACTION  ON GEACTION.masterunkid = hrapps_ratings.competence_appr_actionunkid AND GEACTION.mastertype = '" & clsCommon_Master.enCommonMaster.GE_APPRAISAL_ACTIONS & "' " & _
                    "WHERE isvoid = 0 "

            'S.SANDEEP |27-JUL-2019| -- END

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            dsList = objDataOperation.ExecQuery(StrQ, sList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        Finally
        End Try
    End Function
    'S.SANDEEP |27-MAY-2019| -- END

    'S.SANDEEP |01-MAY-2020| -- START
    'ISSUE/ENHANCEMENT : CALIBRATION MAKEOVER
    Public Sub GetScoringRangeById(ByVal strRaing As String, ByVal mDecScrF As Decimal, ByVal mDecScrT As Decimal)
        Dim StrQ As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Try
            Using objDo As New clsDataOperation

            StrQ = "SELECT " & _
                   "	 score_from " & _
                   "	,score_to " & _
                   "FROM hrapps_ratings " & _
                   "WHERE isvoid = 0 AND grade_award = @grade_award "

                objDo.AddParameter("@grade_award", SqlDbType.NVarChar, strRaing.Length, strRaing)

                dsList = objDo.ExecQuery(StrQ, "List")

                If objDo.ErrorMessage <> "" Then
                    exForce = New Exception(objDo.ErrorNumber & " : " & objDo.ErrorMessage)
                Throw exForce
            End If

            End Using
            
            If dsList.Tables(0).Rows.Count > 0 Then
                mDecScrF = CDec(dsList.Tables(0).Rows(0)("score_from"))
                mDecScrT = CDec(dsList.Tables(0).Rows(0)("score_to"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: ; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP |01-MAY-2020| -- END

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
