﻿'************************************************************************************************************************************
'Class Name : clsassess_group_master.vb
'Purpose    :
'Date       :02/07/2010
'Written By :Pinkal
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal
''' </summary>
Public Class clsassess_group_master
    Private Shared ReadOnly mstrModuleName As String = "clsassess_group_master"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    Dim objAssessGrpTran As New clsassess_group_tran
    'S.SANDEEP [ 29 DEC 2011 ] -- END

#Region " Private variables "
    Private mintAssessgroupunkid As Integer
    Private mstrAssessgroup_Code As String = String.Empty
    Private mstrAssessgroup_Name As String = String.Empty
    ' Private mintResultgroupunkid As Integer
    Private mstrDescription As String = String.Empty
    Private mblnIsactive As Boolean = True
    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    'Private mintStationunkid As Integer
    'Private mintDepartmentunkid As Integer
    'Private mintSectionunkid As Integer
    'Private mintUnitunkid As Integer
    'Private mintJobgroupunkid As Integer
    'Private mintJobunkid As Integer
    Private mstrAllocationIds As String = String.Empty
    Private mintReferenceUnkid As Integer = -1
    'S.SANDEEP [ 29 DEC 2011 ] -- END

    'S.SANDEEP [ 09 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mdecWeight As Decimal = 0
    'S.SANDEEP [ 09 AUG 2013 ] -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Pinkal
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessgroupunkid
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Assessgroupunkid() As Integer
        Get
            Return mintAssessgroupunkid
        End Get
        Set(ByVal value As Integer)
            mintAssessgroupunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessgroup_code
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Assessgroup_Code() As String
        Get
            Return mstrAssessgroup_Code
        End Get
        Set(ByVal value As String)
            mstrAssessgroup_Code = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set assessgroup_name
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Assessgroup_Name() As String
        Get
            Return mstrAssessgroup_Name
        End Get
        Set(ByVal value As String)
            mstrAssessgroup_Name = Value
        End Set
    End Property

    '''' <summary>
    '''' Purpose: Get or Set resultgroupunkid
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Resultgroupunkid() As Integer
    '    Get
    '        Return mintResultgroupunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintResultgroupunkid = Value
    '    End Set
    'End Property


    ''' <summary>
    ''' Purpose: Get or Set description
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Description() As String
        Get
            Return mstrDescription
        End Get
        Set(ByVal value As String)
            mstrDescription = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isactive
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _Isactive() As Boolean
        Get
            Return mblnIsactive
        End Get
        Set(ByVal value As Boolean)
            mblnIsactive = Value
        End Set
    End Property

    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    '''' <summary>
    '''' Purpose: Get or Set stationunkid
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Stationunkid() As Integer
    '    Get
    '        Return mintStationunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintStationunkid = value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set departmentunkid
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Departmentunkid() As Integer
    '    Get
    '        Return mintDepartmentunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintDepartmentunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set sectionunkid
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Sectionunkid() As Integer
    '    Get
    '        Return mintSectionunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintSectionunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set unitunkid
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Unitunkid() As Integer
    '    Get
    '        Return mintUnitunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintUnitunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set jobgroupunkid
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Jobgroupunkid() As Integer
    '    Get
    '        Return mintJobgroupunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintJobgroupunkid = Value
    '    End Set
    'End Property

    '''' <summary>
    '''' Purpose: Get or Set jobunkid
    '''' Modify By: Pinkal
    '''' </summary>
    'Public Property _Jobunkid() As Integer
    '    Get
    '        Return mintJobunkid
    '    End Get
    '    Set(ByVal value As Integer)
    '        mintJobunkid = Value
    '    End Set
    'End Property
    ''' <summary>
    ''' Purpose: Get or Set Allocation Ids
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _AllocationIds() As String
        Get
            Return mstrAllocationIds
        End Get
        Set(ByVal value As String)
            mstrAllocationIds = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set Allocation Ids
    ''' Modify By: Pinkal
    ''' </summary>
    Public Property _ReferenceUnkid() As Integer
        Get
            Return mintReferenceUnkid
        End Get
        Set(ByVal value As Integer)
            mintReferenceUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 29 DEC 2011 ] -- END

    'S.SANDEEP [ 09 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set weight
    ''' Modify By: Sandeep Sharma
    ''' </summary>
    Public Property _Weight() As Decimal
        Get
            Return mdecWeight
        End Get
        Set(ByVal value As Decimal)
            mdecWeight = value
        End Set
    End Property
    'S.SANDEEP [ 09 AUG 2013 ] -- END

#End Region

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            'strQ = "SELECT " & _
            '  "  assessgroupunkid " & _
            '  ", assessgroup_code " & _
            '  ", assessgroup_name " & _
            '  ", isnull(stationunkid,0) as stationunkid " & _
            '  ", isnull(departmentunkid,0) as departmentunkid " & _
            '  ", isnull(sectionunkid,0) as sectionunkid " & _
            '  ", isnull(unitunkid,0) as unitunkid " & _
            '  ", isnull(jobgroupunkid,0) as jobgroupunkid " & _
            '  ", isnull(jobunkid,0) as jobunkid " & _
            '  ", description " & _
            '  ", isactive " & _
            ' "FROM hrassess_group_master " & _
            ' "WHERE assessgroupunkid = @assessgroupunkid "
            strQ = "SELECT " & _
                   " assessgroupunkid " & _
                   ",assessgroup_code " & _
                   ",assessgroup_name " & _
                   ",description " & _
                   ",isactive " & _
                   ",ISNULL(referenceunkid,1) AS referenceunkid " & _
                   ",ISNULL(weight,0) AS weight " & _
                   "FROM hrassess_group_master " & _
                   "WHERE assessgroupunkid = @assessgroupunkid " 'S.SANDEEP [ 09 AUG 2013 {weight} ] -- START -- END
            'S.SANDEEP [ 29 DEC 2011 ] -- END


            

            '    ", isnull(resultgroupunkid,0) as resultgroupunkid " & _

            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessgroupunkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintAssessgroupunkid = CInt(dtRow.Item("assessgroupunkid"))
                mstrAssessgroup_Code = dtRow.Item("assessgroup_code").ToString
                mstrAssessgroup_Name = dtRow.Item("assessgroup_name").ToString
                ' mintResultgroupunkid = CInt(dtRow.Item("resultgroupunkid"))


                'S.SANDEEP [ 29 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES 
                'TYPE : EMPLOYEMENT CONTRACT PROCESS
                'mintStationunkid = CInt(dtRow.Item("stationunkid"))
                'mintdepartmentunkid = CInt(dtRow.Item("departmentunkid"))
                'mintsectionunkid = CInt(dtRow.Item("sectionunkid"))
                'mintunitunkid = CInt(dtRow.Item("unitunkid"))
                'mintjobgroupunkid = CInt(dtRow.Item("jobgroupunkid"))
                'mintjobunkid = CInt(dtRow.Item("jobunkid"))
                mintReferenceUnkid = dtRow.Item("referenceunkid")
                'S.SANDEEP [ 29 DEC 2011 ] -- END


                mstrDescription = dtRow.Item("description").ToString
                mblnIsactive = CBool(dtRow.Item("isactive"))

                'S.SANDEEP [ 09 AUG 2013 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mdecWeight = dtRow.Item("weight")
                'S.SANDEEP [ 09 AUG 2013 ] -- END


                Exit For
            Next

            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            GetAllocationIds(mintAssessgroupunkid)
            'S.SANDEEP [ 29 DEC 2011 ] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, ByVal intRefUnkid As Integer, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try

            'Pinkal (10-Mar-2011) -- Start

            'strQ = "SELECT " & _
            '  "  assessgroupunkid " & _
            '  ", assessgroup_code " & _
            '  ", assessgroup_name " & _
            '  ", isnull(hrassess_group_master.stationunkid,0) as stationunkid " & _
            '  ", isnull(hrstation_master.name,'') as stationname " & _
            '  ", departmentunkid " & _
            '  ", sectionunkid " & _
            '  ", unitunkid " & _
            '  ", jobgroupunkid " & _
            '  ", jobunkid " & _
            '  ", hrassess_group_master.description " & _
            '  ", hrassess_group_master.isactive " & _
            ' "FROM hrassess_group_master " & _
            ' " LEFT JOIN hrstation_master on hrstation_master.stationunkid = hrassess_group_master.stationunkid "



            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            'strQ = "SELECT " & _
            '  "  assessgroupunkid " & _
            '  ", assessgroup_code " & _
            '  ", assessgroup_name " & _
            '  ", isnull(hrassess_group_master.stationunkid,0) as stationunkid " & _
            '  ", isnull(hrstation_master.name,'') as stationname " & _
            ' ", isnull(hrassess_group_master.departmentunkid,0) as departmentunkid " & _
            ' ", isnull(hrassess_group_master.sectionunkid,0) as sectionunkid " & _
            ' ", isnull(hrassess_group_master.unitunkid,0) as  unitunkid " & _
            ' ", isnull(hrassess_group_master.jobgroupunkid,0) as jobgroupunkid " & _
            ' ", isnull(hrassess_group_master.jobunkid,0) as jobunkid " & _
            '  ", hrassess_group_master.description " & _
            ' ", hrassess_group_master.isactive "

            'If IsForImport Then

            '    strQ &= ", hrdepartment_master.name as department " & _
            '                ", hrsection_master.name as section " & _
            '                ", hrunit_master.name as unit " & _
            '                ", hrjobgroup_master.name as jobgroup " & _
            '                ", hrjob_master.job_name as job "
            'End If

            'strQ &= " FROM hrassess_group_master " & _
            ' " LEFT JOIN hrstation_master on hrstation_master.stationunkid = hrassess_group_master.stationunkid "

            'If IsForImport Then
            '    strQ &= " LEFT JOIN hrdepartment_master on hrdepartment_master.departmentunkid = hrassess_group_master.departmentunkid " & _
            '                " LEFT JOIN hrsection_master on hrsection_master.sectionunkid = hrassess_group_master.sectionunkid " & _
            '                " LEFT JOIN hrunit_master on hrunit_master.unitunkid = hrassess_group_master.unitunkid " & _
            '                " LEFT JOIN hrjobgroup_master on hrjobgroup_master.jobgroupunkid = hrassess_group_master.jobgroupunkid " & _
            '                " LEFT JOIN hrjob_master on hrjob_master.jobunkid = hrassess_group_master.jobunkid "
            'End If

            'If blnOnlyActive Then
            '    strQ &= " WHERE hrassess_group_master.isactive = 1 "
            'End If

            'S.SANDEEP [ 05 NOV 2014 ] -- START
            'strQ = "SELECT " & _
            '         " CASE WHEN referenceunkid = 1 THEN ISNULL(hrstation_master.name,'') " & _
            '               "WHEN referenceunkid = 2 THEN ISNULL(hrdepartment_group_master.name,'') " & _
            '               "WHEN referenceunkid = 3 THEN ISNULL(hrdepartment_master.name,'') " & _
            '               "WHEN referenceunkid = 4 THEN ISNULL(hrsectiongroup_master.name,'') " & _
            '               "WHEN referenceunkid = 5 THEN ISNULL(hrsection_master.name,'') " & _
            '               "WHEN referenceunkid = 6 THEN ISNULL(hrunitgroup_master.name,'') " & _
            '               "WHEN referenceunkid = 7 THEN ISNULL(hrunit_master.name,'') " & _
            '               "WHEN referenceunkid = 8 THEN ISNULL(hrteam_master.name,'') " & _
            '               "WHEN referenceunkid = 9 THEN ISNULL(hrjobgroup_master.name,'') " & _
            '               "WHEN referenceunkid = 10 THEN ISNULL(hrjob_master.job_name,'') " & _
            '         " END AS Allocation " & _
            '         ",assessgroup_code " & _
            '         ",assessgroup_name " & _
            '         ",hrassess_group_master.description " & _
            '         ",hrassess_group_master.assessgroupunkid " & _
            '         ",CASE WHEN referenceunkid = 1 THEN ISNULL(hrstation_master.stationunkid,0) " & _
            '               "WHEN referenceunkid = 2 THEN ISNULL(hrdepartment_group_master.deptgroupunkid,0) " & _
            '               "WHEN referenceunkid = 3 THEN ISNULL(hrdepartment_master.departmentunkid,0) " & _
            '               "WHEN referenceunkid = 4 THEN ISNULL(hrsectiongroup_master.sectiongroupunkid,0) " & _
            '               "WHEN referenceunkid = 5 THEN ISNULL(hrsection_master.sectionunkid,0) " & _
            '               "WHEN referenceunkid = 6 THEN ISNULL(hrunitgroup_master.unitgroupunkid,0) " & _
            '               "WHEN referenceunkid = 7 THEN ISNULL(hrunit_master.unitunkid,0) " & _
            '               "WHEN referenceunkid = 8 THEN ISNULL(hrteam_master.teamunkid,0) " & _
            '               "WHEN referenceunkid = 9 THEN ISNULL(hrjobgroup_master.jobgroupunkid,0) " & _
            '               "WHEN referenceunkid = 10 THEN ISNULL(hrjob_master.jobunkid,0) " & _
            '         " END AS Id " & _
            '         ",hrassess_group_master.isactive " & _
            '         ",hrassess_group_tran.allocationunkid " & _
            '         ",hrassess_group_tran.assessgrouptranunkid as assessgrouptranunkid " & _
            '         ",ISNULL(hrassess_group_master.weight,0) AS weight " & _
            '    "FROM hrassess_group_tran " & _
            '         "JOIN hrassess_group_master ON hrassess_group_tran.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
            '         "LEFT JOIN hrstation_master ON hrassess_group_tran.allocationunkid = hrstation_master.stationunkid " & _
            '         "LEFT JOIN hrdepartment_group_master ON hrassess_group_tran.allocationunkid = hrdepartment_group_master.deptgroupunkid " & _
            '         "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrassess_group_tran.allocationunkid " & _
            '         "LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrassess_group_tran.allocationunkid " & _
            '         "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hrassess_group_tran.allocationunkid " & _
            '         "LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrassess_group_tran.allocationunkid " & _
            '         "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hrassess_group_tran.allocationunkid " & _
            '         "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = hrassess_group_tran.allocationunkid " & _
            '         "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrassess_group_tran.allocationunkid " & _
            '         "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hrassess_group_tran.allocationunkid " & _
            '    "WHERE 1 = 1 " 'S.SANDEEP [ 09 AUG 2013 {weight} ] -- START -- END

            If intRefUnkid > 0 AndAlso intRefUnkid <> enAllocation.EMPLOYEE Then
                strQ = "SELECT " & _
                                           " CASE WHEN referenceunkid = '" & enAllocation.BRANCH & "' THEN ISNULL(hrstation_master.name,'') " & _
                                           "      WHEN referenceunkid = '" & enAllocation.DEPARTMENT_GROUP & "' THEN ISNULL(hrdepartment_group_master.name,'') " & _
                                           "      WHEN referenceunkid = '" & enAllocation.DEPARTMENT & "' THEN ISNULL(hrdepartment_master.name,'') " & _
                                           "      WHEN referenceunkid = '" & enAllocation.SECTION_GROUP & "' THEN ISNULL(hrsectiongroup_master.name,'') " & _
                                           "      WHEN referenceunkid = '" & enAllocation.SECTION & "' THEN ISNULL(hrsection_master.name,'') " & _
                                           "      WHEN referenceunkid = '" & enAllocation.UNIT_GROUP & "' THEN ISNULL(hrunitgroup_master.name,'') " & _
                                           "      WHEN referenceunkid = '" & enAllocation.UNIT & "' THEN ISNULL(hrunit_master.name,'') " & _
                                           "      WHEN referenceunkid = '" & enAllocation.TEAM & "' THEN ISNULL(hrteam_master.name,'') " & _
                                           "      WHEN referenceunkid = '" & enAllocation.JOB_GROUP & "' THEN ISNULL(hrjobgroup_master.name,'') " & _
                                           "      WHEN referenceunkid = '" & enAllocation.JOBS & "' THEN ISNULL(hrjob_master.job_name,'') " & _
                                           "      WHEN referenceunkid = '" & enAllocation.CLASS_GROUP & "' THEN ISNULL(hrclassgroup_master.name,'') " & _
                                           "      WHEN referenceunkid = '" & enAllocation.CLASSES & "' THEN ISNULL(hrclasses_master.name,'') " & _
                                           "      WHEN referenceunkid = '" & enAllocation.COST_CENTER & "' THEN ISNULL(prcostcenter_master.costcentername,'') " & _
                       "      WHEN referenceunkid = '" & enAllocation.EMPLOYEE_GRADES & "' THEN ISNULL(hrgrade_master.name,0) " & _
                         " END AS Allocation " & _
                         ",assessgroup_code " & _
                         ",assessgroup_name " & _
                         ",hrassess_group_master.description " & _
                         ",hrassess_group_master.assessgroupunkid " & _
                                           ",CASE WHEN referenceunkid = '" & enAllocation.BRANCH & "' THEN ISNULL(hrstation_master.stationunkid,0) " & _
                                           "      WHEN referenceunkid = '" & enAllocation.DEPARTMENT_GROUP & "' THEN ISNULL(hrdepartment_group_master.deptgroupunkid,0) " & _
                                           "      WHEN referenceunkid = '" & enAllocation.DEPARTMENT & "' THEN ISNULL(hrdepartment_master.departmentunkid,0) " & _
                                           "      WHEN referenceunkid = '" & enAllocation.SECTION_GROUP & "' THEN ISNULL(hrsectiongroup_master.sectiongroupunkid,0) " & _
                                           "      WHEN referenceunkid = '" & enAllocation.SECTION & "' THEN ISNULL(hrsection_master.sectionunkid,0) " & _
                                           "      WHEN referenceunkid = '" & enAllocation.UNIT_GROUP & "' THEN ISNULL(hrunitgroup_master.unitgroupunkid,0) " & _
                                           "      WHEN referenceunkid = '" & enAllocation.UNIT & "' THEN ISNULL(hrunit_master.unitunkid,0) " & _
                                           "      WHEN referenceunkid = '" & enAllocation.TEAM & "' THEN ISNULL(hrteam_master.teamunkid,0) " & _
                                           "      WHEN referenceunkid = '" & enAllocation.JOB_GROUP & "' THEN ISNULL(hrjobgroup_master.jobgroupunkid,0) " & _
                                           "      WHEN referenceunkid = '" & enAllocation.JOBS & "' THEN ISNULL(hrjob_master.jobunkid,0) " & _
                                           "      WHEN referenceunkid = '" & enAllocation.CLASS_GROUP & "' THEN ISNULL(hrclassgroup_master.classgroupunkid,0) " & _
                                           "      WHEN referenceunkid = '" & enAllocation.CLASSES & "' THEN ISNULL(hrclasses_master.classesunkid,0) " & _
                                           "      WHEN referenceunkid = '" & enAllocation.COST_CENTER & "' THEN ISNULL(prcostcenter_master.costcenterunkid,0) " & _
                       "      WHEN referenceunkid = '" & enAllocation.EMPLOYEE_GRADES & "' THEN ISNULL(hrgrade_master.gradeunkid,0) " & _
                         " END AS Id " & _
                         ",hrassess_group_master.isactive " & _
                         ",hrassess_group_tran.allocationunkid " & _
                         ",hrassess_group_tran.assessgrouptranunkid as assessgrouptranunkid " & _
                         ",ISNULL(hrassess_group_master.weight,0) AS weight " & _
                    "FROM hrassess_group_tran " & _
                         "JOIN hrassess_group_master ON hrassess_group_tran.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                         "LEFT JOIN hrstation_master ON hrassess_group_tran.allocationunkid = hrstation_master.stationunkid " & _
                         "LEFT JOIN hrdepartment_group_master ON hrassess_group_tran.allocationunkid = hrdepartment_group_master.deptgroupunkid " & _
                         "LEFT JOIN hrdepartment_master ON hrdepartment_master.departmentunkid = hrassess_group_tran.allocationunkid " & _
                         "LEFT JOIN hrsectiongroup_master ON hrsectiongroup_master.sectiongroupunkid = hrassess_group_tran.allocationunkid " & _
                         "LEFT JOIN hrsection_master ON hrsection_master.sectionunkid = hrassess_group_tran.allocationunkid " & _
                         "LEFT JOIN hrunitgroup_master ON hrunitgroup_master.unitgroupunkid = hrassess_group_tran.allocationunkid " & _
                         "LEFT JOIN hrunit_master ON hrunit_master.unitunkid = hrassess_group_tran.allocationunkid " & _
                         "LEFT JOIN hrteam_master ON hrteam_master.teamunkid = hrassess_group_tran.allocationunkid " & _
                         "LEFT JOIN hrjobgroup_master ON hrjobgroup_master.jobgroupunkid = hrassess_group_tran.allocationunkid " & _
                         "LEFT JOIN hrjob_master ON hrjob_master.jobunkid = hrassess_group_tran.allocationunkid " & _
                                           "LEFT JOIN prcostcenter_master ON hrassess_group_tran.allocationunkid = prcostcenter_master.costcenterunkid " & _
                                           "LEFT JOIN hrclasses_master ON hrassess_group_tran.allocationunkid = hrclasses_master.classesunkid " & _
                                           "LEFT JOIN hrclassgroup_master ON hrassess_group_tran.allocationunkid = hrclassgroup_master.classgroupunkid " & _
                       "    LEFT JOIN hrgrade_master ON hrassess_group_tran.allocationunkid = hrgrade_master.gradeunkid " & _
                                       "WHERE 1 = 1 AND referenceunkid = '" & intRefUnkid & "' "
            Else
                strQ = "SELECT " & _
                       "     CASE WHEN referenceunkid = '" & enAllocation.EMPLOYEE & "' THEN " & _
                       "         ISNULL(hremployee_master.firstname,'')+' '+ISNULL(hremployee_master.othername,'')+' '+ISNULL(hremployee_master.surname,'') " & _
                       "     END AS Allocation " & _
                       "    ,assessgroup_code " & _
                       "    ,assessgroup_name " & _
                       "    ,hrassess_group_master.description " & _
                       "    ,hrassess_group_master.assessgroupunkid " & _
                       "    ,CASE WHEN referenceunkid = '" & enAllocation.EMPLOYEE & "' THEN ISNULL(hremployee_master.employeeunkid,0) END AS Id " & _
                       "    ,hrassess_group_master.isactive " & _
                       "    ,hrassess_group_tran.allocationunkid " & _
                       "    ,hrassess_group_tran.assessgrouptranunkid as assessgrouptranunkid " & _
                       "    ,ISNULL(hrassess_group_master.weight,0) AS weight " & _
                       "FROM hrassess_group_tran " & _
                       "    JOIN hrassess_group_master ON hrassess_group_tran.assessgroupunkid = hrassess_group_master.assessgroupunkid " & _
                       "    JOIN hremployee_master ON hrassess_group_tran.allocationunkid = hremployee_master.employeeunkid " & _
                       "WHERE 1 = 1 AND referenceunkid = '" & intRefUnkid & "' "
            End If

            If blnOnlyActive = True Then
                strQ &= "AND hrassess_group_tran.isactive = 1 AND hrassess_group_tran.isactive = 1 "
            End If

            'S.SANDEEP [ 05 NOV 2014 ] -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrassess_group_master) </purpose>
    Public Function Insert(ByVal dtEffDate As Date) As Boolean 'S.SANDEEP [18 Jan 2016] -- START {dtEffDate} -- END
        'Public Function Insert() As Boolean
        mstrMessage = ""
        If isExist(mstrAssessgroup_Code) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Assess Group Code is already defined. Please define new Assess Group Code.")
            Return False
        ElseIf isExist("", mstrAssessgroup_Name) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Assess Group Name is already defined. Please define new Assess Group Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@assessgroup_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssessgroup_Code.ToString)
            objDataOperation.AddParameter("@assessgroup_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssessgroup_Name.ToString)
            'objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultgroupunkid.ToString)


            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            'objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            'objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintdepartmentunkid.ToString)
            'objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintsectionunkid.ToString)
            'objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintunitunkid.ToString)
            'objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintjobgroupunkid.ToString)
            'objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintjobunkid.ToString)
            objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceUnkid.ToString)
            'S.SANDEEP [ 29 DEC 2011 ] -- END



            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)
            'S.SANDEEP [ 09 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecWeight)
            'S.SANDEEP [ 09 AUG 2013 ] -- END




            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            'strQ = "INSERT INTO hrassess_group_master ( " & _
            '  "  assessgroup_code " & _
            '  ", assessgroup_name " & _
            '  ", stationunkid " & _
            '  ", departmentunkid " & _
            '  ", sectionunkid " & _
            '  ", unitunkid " & _
            '  ", jobgroupunkid " & _
            '  ", jobunkid " & _
            '  ", description " & _
            '  ", isactive" & _
            '") VALUES (" & _
            '  "  @assessgroup_code " & _
            '  ", @assessgroup_name " & _
            '  ", @stationunkid " & _
            '  ", @departmentunkid " & _
            '  ", @sectionunkid " & _
            '  ", @unitunkid " & _
            '  ", @jobgroupunkid " & _
            '  ", @jobunkid " & _
            '  ", @description " & _
            '  ", @isactive" & _
            '"); SELECT @@identity"
            strQ = "INSERT INTO hrassess_group_master ( " & _
              "  assessgroup_code " & _
              ", assessgroup_name " & _
              ", description " & _
              ", isactive" & _
              ", referenceunkid " & _
              ", weight " & _
            ") VALUES (" & _
              "  @assessgroup_code " & _
              ", @assessgroup_name " & _
              ", @description " & _
              ", @isactive" & _
              ", @referenceunkid " & _
              ", @weight " & _
            "); SELECT @@identity"
            'S.SANDEEP [ 29 DEC 2011 ] -- END

            'S.SANDEEP [ 09 AUG 2013 {weight} ] -- START -- END


            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintAssessgroupunkid = dsList.Tables(0).Rows(0).Item(0)


            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS

            ''Pinkal (12-Oct-2011) -- Start
            ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'If clsCommonATLog.Insert_AtLog(objDataOperation, 1, "hrassess_group_master", "assessgroupunkid", mintAssessgroupunkid) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'objDataOperation.ReleaseTransaction(True)
            ''Pinkal (12-Oct-2011) -- End


            'S.SANDEEP [20 Jan 2016] -- START
            If objAssessGrpTran.Insert(objDataOperation, mintAssessgroupunkid, mstrAllocationIds, mintReferenceUnkid, dtEffDate) = False Then
                'If objAssessGrpTran.Insert(objDataOperation, mintAssessgroupunkid, mstrAllocationIds, mintReferenceUnkid) = False Then
                'S.SANDEEP [20 Jan 2016] -- END
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 29 DEC 2011 ] -- END


            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrassess_group_master) </purpose>
    Public Function Update(ByVal dtEffDate As Date) As Boolean 'S.SANDEEP [20 Jan 2016] -- START {dtEffDate} -- END
        'Public Function Update() As Boolean

        mstrMessage = ""
        If isExist(mstrAssessgroup_Code, "", mintAssessgroupunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 1, "This Assess Group Code is already defined. Please define new Assess Group Code.")
            Return False
        ElseIf isExist("", mstrAssessgroup_Name, mintAssessgroupunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 2, "This Assess Group Name is already defined. Please define new Assess Group Name.")
            Return False
        End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintAssessgroupunkid.ToString)
            objDataOperation.AddParameter("@assessgroup_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssessgroup_Code.ToString)
            objDataOperation.AddParameter("@assessgroup_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrAssessgroup_Name.ToString)
            ' objDataOperation.AddParameter("@resultgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResultgroupunkid.ToString)



            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            'objDataOperation.AddParameter("@stationunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintStationunkid.ToString)
            'objDataOperation.AddParameter("@departmentunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintdepartmentunkid.ToString)
            'objDataOperation.AddParameter("@sectionunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintsectionunkid.ToString)
            'objDataOperation.AddParameter("@unitunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintunitunkid.ToString)
            'objDataOperation.AddParameter("@jobgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintjobgroupunkid.ToString)
            'objDataOperation.AddParameter("@jobunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintjobunkid.ToString)
            objDataOperation.AddParameter("@referenceunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintReferenceUnkid.ToString)
            'S.SANDEEP [ 29 DEC 2011 ] -- END



            objDataOperation.AddParameter("@description", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrDescription.ToString)
            objDataOperation.AddParameter("@isactive", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsactive.ToString)

            'S.SANDEEP [ 09 AUG 2013 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@weight", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecWeight)
            'S.SANDEEP [ 09 AUG 2013 ] -- END



            strQ = "UPDATE hrassess_group_master SET " & _
                       "  assessgroup_code = @assessgroup_code" & _
                       ", assessgroup_name = @assessgroup_name" & _
                       ", description = @description" & _
                       ", isactive = @isactive " & _
                       ", referenceunkid = @referenceunkid " & _
                       ", weight = @weight " & _
                   " WHERE assessgroupunkid = @assessgroupunkid " 'S.SANDEEP [ 09 AUG 2013 {weight} ] -- START -- END

            ' ", resultgroupunkid = @resultgroupunkid" & _

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If



            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            'If clsCommonATLog.IsTableDataUpdate("atcommon_log", "hrassess_group_master", mintAssessgroupunkid, "assessgroupunkid", 2) Then

            '    If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "hrassess_group_master", "assessgroupunkid", mintAssessgroupunkid) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If

            'End If
            'objDataOperation.ReleaseTransaction(True)
            ''Pinkal (12-Oct-2011) -- End


            'S.SANDEEP [20 Jan 2016] -- START
            If objAssessGrpTran.Insert(objDataOperation, mintAssessgroupunkid, mstrAllocationIds, mintReferenceUnkid, dtEffDate) = False Then
                'If objAssessGrpTran.Insert(objDataOperation, mintAssessgroupunkid, mstrAllocationIds, mintReferenceUnkid) = False Then
                'S.SANDEEP [20 Jan 2016] -- END
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            Else
                If objAssessGrpTran._Message <> "" Then
                    mstrMessage = objAssessGrpTran._Message
                End If
            End If

            objDataOperation.ReleaseTransaction(True)
            'S.SANDEEP [ 29 DEC 2011 ] -- END


            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrassess_group_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 3, "Sorry, You cannot delete selected Assess Group. Reason: This Assess Group is in use.")
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'Pinkal (12-Oct-2011) -- Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
        objDataOperation = New clsDataOperation
        objDataOperation.BindTransaction()
        'Pinkal (12-Oct-2011) -- End

        Try
            strQ = "Update hrassess_group_master set isactive = 0 " & _
            "WHERE assessgroupunkid = @assessgroupunkid "

            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dsList = clsCommonATLog.GetChildList(objDataOperation, "hrassess_group_tran", "assessgroupunkid", intUnkid)

            strQ = "UPDATE hrassess_group_tran " & _
                   "    SET isactive = 0 " & _
                   "WHERE assessgroupunkid = '" & intUnkid & "'"

            Call objDataOperation.ExecNonQuery(strQ)


            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_group_master", "assessgroupunkid", dRow.Item("assessgroupunkid"), "hrassess_group_tran", "assessgrouptranunkid", dRow.Item("assessgrouptranunkid"), 3, 3) = False Then
                        objDataOperation.ReleaseTransaction(False)
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            'Pinkal (12-Oct-2011) -- Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objDataOperation.ReleaseTransaction(False)
            'Pinkal (12-Oct-2011) -- End
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Dim blnFlag As Boolean = False
        Try

            strQ = "SELECT " & _
                   " 'SELECT * FROM ' + TABLE_NAME + ' WHERE '+ COLUMN_NAME +' = ''" & intUnkid & "'' ' AS Tabs " & _
                   " ,TABLE_NAME AS TabName " & _
                   "FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'assessgroupunkid' " & _
                   "AND TABLE_NAME NOT IN ('hrassess_group_master','hrassess_group_tran') "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                For Each dRow As DataRow In dsList.Tables(0).Rows
                    Select Case dRow.Item("TabName").ToString.ToUpper
                        Case "HRASSESS_ITEM_MASTER"
                            If objDataOperation.RecordCount(dRow.Item("Tabs").ToString & " AND isactive = 1 ") > 0 Then
                                blnFlag = True
                                Exit For
                            End If
                        Case "HRASSESS_ANALYSIS_MASTER"
                            If objDataOperation.RecordCount(dRow.Item("Tabs").ToString & " AND isvoid = 0 ") > 0 Then
                                blnFlag = True
                                Exit For
                            End If
                    End Select
                Next
            End If

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return blnFlag

        Catch ex As Exception
            Return True
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Pinkal
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal strName As String = "", Optional ByVal intUnkid As Integer = -1) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try


            'S.SANDEEP [ 29 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES 
            'TYPE : EMPLOYEMENT CONTRACT PROCESS
            'strQ = "SELECT " & _
            '  "  assessgroupunkid " & _
            '  ", assessgroup_code " & _
            '  ", assessgroup_name " & _
            '  ", departmentunkid " & _
            '  ", sectionunkid " & _
            '  ", unitunkid " & _
            '  ", jobgroupunkid " & _
            '  ", jobunkid " & _
            '  ", description " & _
            '  ", isactive " & _
            ' "FROM hrassess_group_master " & _
            ' "WHERE 1=1"

            strQ = "SELECT " & _
                       " assessgroupunkid " & _
                       ",assessgroup_code " & _
                       ",assessgroup_name " & _
                       ",description " & _
                       ",isactive " & _
                   "FROM hrassess_group_master " & _
                   "WHERE isactive=1"
            'S.SANDEEP [ 29 DEC 2011 ] -- END


            ' ", resultgroupunkid " & _

            If strCode.Length > 0 Then
                strQ &= " AND assessgroup_code = @assessgroup_code "
            End If

            If strName.Length > 0 Then
                strQ &= " AND assessgroup_name = @assessgroup_name "
            End If

            If intUnkid > 0 Then
                strQ &= " AND assessgroupunkid <> @assessgroupunkid"
            End If

            objDataOperation.AddParameter("@assessgroup_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@assessgroup_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    'S.SANDEEP [ 29 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES 
    'TYPE : EMPLOYEMENT CONTRACT PROCESS
    Private Sub GetAllocationIds(ByVal intAssessGrpId As Integer)
        Dim strQ As String = ""
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT ISNULL(STUFF " & _
                   "((SELECT " & _
                         "',' + CAST(hrassess_group_tran.allocationunkid AS NVARCHAR(50)) " & _
                      "FROM hrassess_group_tran " & _
                      "WHERE hrassess_group_tran.assessgroupunkid = '" & intAssessGrpId & "' AND hrassess_group_tran.isactive = 1 " & _
                      "ORDER BY hrassess_group_tran.allocationunkid FOR XML PATH('')), 1, 1, '' " & _
                    "),'') AS Allocation "

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If dsList.Tables("List").Rows.Count > 0 Then
                mstrAllocationIds = dsList.Tables("List").Rows(0)("Allocation")
            Else
                mstrAllocationIds = ""
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetAllocationIds; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

    '''' <summary>
    '''' Modify By: Pinkal
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    Public Function getListForCombo(Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Try
            If mblFlag = True Then
                strQ = "SELECT 0 as assessgroupunkid, ' ' +  @name  as name UNION "
            End If
            strQ &= "SELECT assessgroupunkid, assessgroup_name as name FROM hrassess_group_master where isactive = 1 ORDER BY name "

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    Public Function getListForCombo(ByVal intEmployeeId As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False) As DataSet
        Dim dsList As New DataSet
        Dim objDataOperation As New clsDataOperation
        Dim strQ As String = String.Empty
        Dim exForce As Exception
        Dim StrAllocationIds As String = String.Empty
        Dim StrReferenceIds As String = String.Empty
        Dim StrGrpId As String = String.Empty
        Try
            If intEmployeeId > 0 Then
                Dim dsAllocation As New DataSet
                strQ = "SELECT " & _
                         " ISNULL(stationunkid,0) AS branchid " & _
                         ",ISNULL(deptgroupunkid,0) AS deptgrpid " & _
                         ",ISNULL(departmentunkid,0) AS deptid " & _
                         ",ISNULL(sectiongroupunkid,0) AS sectiongrpid " & _
                         ",ISNULL(sectionunkid,0) AS sectionid " & _
                         ",ISNULL(unitgroupunkid,0) AS unitgrpid " & _
                         ",ISNULL(unitunkid,0) AS unitid " & _
                         ",ISNULL(teamunkid,0) AS teamid " & _
                         ",ISNULL(jobgroupunkid,0) AS jobgrpid " & _
                         ",ISNULL(jobunkid,0) AS jobid " & _
                        "FROM hremployee_master WHERE employeeunkid = '" & intEmployeeId & "' "

                dsAllocation = objDataOperation.ExecQuery(strQ, "List")

                If dsAllocation.Tables(0).Rows.Count > 0 Then
                    For Each dCol As DataColumn In dsAllocation.Tables(0).Columns
                        If dsAllocation.Tables(0).Rows(0).Item(dCol) > 0 Then
                            StrAllocationIds = dsAllocation.Tables(0).Rows(0).Item(dCol).ToString
                            Select Case dCol.ColumnName.ToString
                                Case "branchid"
                                    StrReferenceIds = enAllocation.BRANCH
                                Case "deptgrpid"
                                    StrReferenceIds = enAllocation.DEPARTMENT_GROUP
                                Case "deptid"
                                    StrReferenceIds = enAllocation.DEPARTMENT
                                Case "sectiongrpid"
                                    StrReferenceIds = enAllocation.SECTION_GROUP
                                Case "sectionid"
                                    StrReferenceIds = enAllocation.SECTION
                                Case "unitgrpid"
                                    StrReferenceIds = enAllocation.UNIT_GROUP
                                Case "unitid"
                                    StrReferenceIds = enAllocation.UNIT
                                Case "teamid"
                                    StrReferenceIds = enAllocation.TEAM
                                Case "jobgrpid"
                                    StrReferenceIds = enAllocation.JOB_GROUP
                                Case "jobid"
                                    StrReferenceIds = enAllocation.JOBS
                            End Select
                        End If
                        If StrAllocationIds <> "" AndAlso StrReferenceIds <> "" Then
                            strQ = "SELECT " & _
                                   "    hrassess_group_master.assessgroupunkid " & _
                                   "FROM hrassess_group_master " & _
                                   "    JOIN hrassess_group_tran ON dbo.hrassess_group_master.assessgroupunkid = dbo.hrassess_group_tran.assessgroupunkid " & _
                                   "WHERE allocationunkid = '" & StrAllocationIds & "' AND referenceunkid = '" & StrReferenceIds & "'  " & _
                                   "    AND hrassess_group_tran.isactive = 1 AND hrassess_group_master.isactive = 1 "

                            Dim dsGrp As New DataSet

                            dsGrp = objDataOperation.ExecQuery(strQ, "List")

                            If objDataOperation.ErrorMessage <> "" Then
                                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                Throw exForce
                            End If
                            If dsGrp.Tables(0).Rows.Count > 0 Then
                                'S.SANDEEP [ 03 AUG 2013 ] -- START
                                'ENHANCEMENT : TRA CHANGES
                                'StrGrpId &= "," & dsGrp.Tables(0).Rows(0)("assessgroupunkid").ToString
                                For Each dRow As DataRow In dsGrp.Tables("List").Rows
                                    StrGrpId &= "," & dRow.Item("assessgroupunkid").ToString
                                Next
                                'S.SANDEEP [ 03 AUG 2013 ] -- END
                            End If
                        End If
                    Next
                    'If StrAllocationIds.ToString.Trim.Length > 0 Then StrAllocationIds = Mid(StrAllocationIds, 2)
                    'If StrReferenceIds.ToString.Trim.Length > 0 Then StrReferenceIds = Mid(StrReferenceIds, 2)
                End If

            End If

            'S.SANDEEP [ 24 APR 2014 ] -- START
            strQ = ""
            'S.SANDEEP [ 24 APR 2014 ] -- END

            If mblFlag = True Then
                If StrGrpId.ToString.Trim.Length > 0 Then
                    strQ = "SELECT 0 as assessgroupunkid, ' ' +  @name  as name UNION "
                Else
                    strQ = "SELECT 0 as assessgroupunkid, ' ' +  @name  as name "
                End If
            End If



            'S.SANDEEP [ 04 FEB 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'strQ &= "SELECT DISTINCT " & _
            '        "  hrassess_group_master.assessgroupunkid " & _
            '        ", hrassess_group_master.assessgroup_name as name " & _
            '        "FROM hrassess_group_master " & _
            '        " JOIN hrassess_group_tran ON hrassess_group_master.assessgroupunkid = hrassess_group_tran.assessgroupunkid " & _
            '        "WHERE hrassess_group_tran.isactive = 1 AND hrassess_group_master.isactive = 1 " & _
            '        "   AND allocationunkid IN (" & StrAllocationIds & ") AND referenceunkid IN (" & StrReferenceIds & ") "
            If StrGrpId.ToString.Trim.Length > 0 Then
                StrGrpId = Mid(StrGrpId, 2)
                strQ &= "SELECT " & _
                    "  hrassess_group_master.assessgroupunkid " & _
                    ", hrassess_group_master.assessgroup_name as name " & _
                    "FROM hrassess_group_master " & _
                    "WHERE hrassess_group_master.isactive = 1 " & _
                    "AND assessgroupunkid in (" & StrGrpId & ") "
            End If
            'S.SANDEEP [ 04 FEB 2012 ] -- END

            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
            dsList = objDataOperation.ExecQuery(strQ, strListName)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            Return dsList
        Catch ex As Exception
            DisplayError.Show(-1, ex.Message, "getListForCombo", mstrModuleName)
            Return Nothing
        Finally
            exForce = Nothing
            objDataOperation = Nothing
            dsList.Dispose()
            dsList = Nothing
        End Try

    End Function

    'S.SANDEEP [ 05 NOV 2014 ] -- START

    'S.SANDEEP [04 JUN 2015] -- START
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    Public Function GetCSV_AssessGroupIds(ByVal iEmployeeId As Integer, ByVal mdtEffectiveDate As Date, ByVal intPeriodId As Integer) As String 'S.SANDEEP [19 DEC 2016] -- START {intPeriodId} -- END
        'Public Function GetCSV_AssessGroupIds(ByVal iEmployeeId As Integer) As String
        'S.SANDEEP [04 JUN 2015] -- END

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim iAssessGrpIds As String = String.Empty
        Dim StrAllocationIds As String = String.Empty
        Dim StrReferenceIds As String = String.Empty
        objDataOperation = New clsDataOperation

        'S.SANDEEP [22-MAR-2017] -- START
        'ISSUE/ENHANCEMENT : PA Computation Process Data Issue
        Dim blnExecutedOnce As Boolean = False
        'S.SANDEEP [22-MAR-2017] -- END


        Try
            'S.SANDEEP [13-FEB-2017] -- START
            'ISSUE/ENHANCEMENT : GONE TO ENDLESS LOOP
            If iEmployeeId <= 0 Then Return ""
            'S.SANDEEP [13-FEB-2017] -- END

            Dim dsAllocation As New DataSet

            'S.SANDEEP [19 DEC 2016] -- START
            Dim dtDate As String = String.Empty
            StrQ = "select top 1 @date = CONVERT(char(8),status_date,112) from hrassess_empstatus_tran WITH (NOLOCK) where statustypeid in (1,2) and periodunkid = @periodunkid and employeeunkid = @employeeunkid " & _
                   " order by status_date desc, statustypeid desc "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, iEmployeeId)
            objDataOperation.AddParameter("@periodunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intPeriodId)
            objDataOperation.AddParameter("@date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtDate, ParameterDirection.InputOutput)

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            dtDate = objDataOperation.GetParameterValue("@date").ToString

            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT :
lbl:
            'S.SANDEEP [25-JAN-2017] -- END


            If dtDate.Trim.Length > 0 Then
                mdtEffectiveDate = eZeeDate.convertDate(dtDate.ToString).Date
            End If
            'S.SANDEEP [19 DEC 2016] -- END


            'S.SANDEEP [04 JUN 2015] -- START
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT " & _
            '             " ISNULL(stationunkid,0) AS branchid " & _
            '             ",ISNULL(deptgroupunkid,0) AS deptgrpid " & _
            '             ",ISNULL(departmentunkid,0) AS deptid " & _
            '             ",ISNULL(sectiongroupunkid,0) AS sectiongrpid " & _
            '             ",ISNULL(sectionunkid,0) AS sectionid " & _
            '             ",ISNULL(unitgroupunkid,0) AS unitgrpid " & _
            '             ",ISNULL(unitunkid,0) AS unitid " & _
            '             ",ISNULL(teamunkid,0) AS teamid " & _
            '             ",ISNULL(jobgroupunkid,0) AS jobgrpid " & _
            '             ",ISNULL(jobunkid,0) AS jobid " & _
            '             ",ISNULL(classgroupunkid,0) as cgrpid " & _
            '             ",ISNULL(classunkid,0) as classid " & _
            '             ",ISNULL(costcenterunkid,0) as ccid " & _
            '            "FROM hremployee_master WHERE employeeunkid = '" & iEmployeeId & "' "

            StrQ = "SELECT " & _
                   "     ISNULL(Alloc.stationunkid,0) AS branchid " & _
                   "    ,ISNULL(Alloc.deptgroupunkid,0) AS deptgrpid " & _
                   "    ,ISNULL(Alloc.departmentunkid,0) AS deptid " & _
                   "    ,ISNULL(Alloc.sectiongroupunkid,0) AS sectiongrpid " & _
                   "    ,ISNULL(Alloc.sectionunkid,0) AS sectionid " & _
                   "    ,ISNULL(Alloc.unitgroupunkid,0) AS unitgrpid " & _
                   "    ,ISNULL(Alloc.unitunkid,0) AS unitid " & _
                   "    ,ISNULL(Alloc.teamunkid,0) AS teamid " & _
                   "    ,ISNULL(Jobs.jobgroupunkid,0) AS jobgrpid " & _
                   "    ,ISNULL(Jobs.jobunkid,0) AS jobid " & _
                   "    ,ISNULL(Alloc.classgroupunkid,0) as cgrpid " & _
                   "    ,ISNULL(Alloc.classunkid,0) as classid " & _
                   "    ,ISNULL(CC.costcenterunkid,0) as ccid " & _
                   "    ,ISNULL(Grd.gradeunkid,0) AS grdid " & _
                   "FROM hremployee_master " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             stationunkid " & _
                   "            ,deptgroupunkid " & _
                   "            ,departmentunkid " & _
                   "            ,sectiongroupunkid " & _
                   "            ,sectionunkid " & _
                   "            ,unitgroupunkid " & _
                   "            ,unitunkid " & _
                   "            ,teamunkid " & _
                   "            ,classgroupunkid " & _
                   "            ,classunkid " & _
                   "            ,employeeunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_transfer_tran WITH (NOLOCK) " & _
                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
                   "        AND hremployee_transfer_tran.employeeunkid = '" & iEmployeeId & "'  " & _
                   "    ) AS Alloc ON Alloc.employeeunkid = hremployee_master.employeeunkid AND Alloc.rno = 1 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             jobunkid " & _
                   "            ,jobgroupunkid " & _
                   "            ,employeeunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_categorization_tran WITH (NOLOCK) " & _
                   "        WHERE isvoid = 0 AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
                   "        AND hremployee_categorization_tran.employeeunkid = '" & iEmployeeId & "'  " & _
                   "    ) AS Jobs ON Jobs.employeeunkid = hremployee_master.employeeunkid AND Jobs.rno = 1 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             cctranheadvalueid AS costcenterunkid " & _
                   "            ,employeeunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY effectivedate DESC) AS rno " & _
                   "        FROM hremployee_cctranhead_tran WITH (NOLOCK) " & _
                   "        WHERE istransactionhead = 0 AND isvoid = 0 " & _
                   "        AND CONVERT(CHAR(8),effectivedate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
                   "        AND hremployee_cctranhead_tran.employeeunkid = '" & iEmployeeId & "'  " & _
                   "    ) AS CC ON CC.employeeunkid = hremployee_master.employeeunkid AND CC.rno = 1 " & _
                   "    LEFT JOIN " & _
                   "    ( " & _
                   "        SELECT " & _
                   "             prsalaryincrement_tran.gradeunkid " & _
                   "            ,prsalaryincrement_tran.employeeunkid " & _
                   "            ,ROW_NUMBER()OVER(PARTITION BY employeeunkid ORDER BY prsalaryincrement_tran.incrementdate DESC, prsalaryincrement_tran.salaryincrementtranunkid DESC) AS rno " & _
                   "        FROM prsalaryincrement_tran WITH (NOLOCK) " & _
                   "        WHERE prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 AND prsalaryincrement_tran.employeeunkid = '" & iEmployeeId & "' " & _
                   "        AND CONVERT(CHAR(8),incrementdate,112) <= '" & eZeeDate.convertDate(mdtEffectiveDate) & "' " & _
                   "    ) AS Grd ON Grd.employeeunkid = hremployee_master.employeeunkid AND Grd.rno = 1 " & _
                   "WHERE hremployee_master.employeeunkid = '" & iEmployeeId & "' "

            'S.SANDEEP [31 AUG 2016] -- START
            'ENHANCEMENT : INCLUSION OF GRADES ON ASSESSMENT GROUP {BY ANDREW}
            '1. ISNULL(Grd.gradeunkid,0) AS grdid
            '2. prsalaryincrement_tran
            'S.SANDEEP [31 AUG 2016] -- START


            'S.SANDEEP [04 JUN 2015] -- END
            

            dsAllocation = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsAllocation.Tables(0).Rows.Count > 0 Then
                For Each dCol As DataColumn In dsAllocation.Tables(0).Columns
                    'If dsAllocation.Tables(0).Rows(0).Item(dCol) > 0 Then
                        StrAllocationIds = dsAllocation.Tables(0).Rows(0).Item(dCol).ToString
                        Select Case dCol.ColumnName.ToString
                            Case "branchid"
                                StrReferenceIds = enAllocation.BRANCH
                            Case "deptgrpid"
                                StrReferenceIds = enAllocation.DEPARTMENT_GROUP
                            Case "deptid"
                                StrReferenceIds = enAllocation.DEPARTMENT
                            Case "sectiongrpid"
                                StrReferenceIds = enAllocation.SECTION_GROUP
                            Case "sectionid"
                                StrReferenceIds = enAllocation.SECTION
                            Case "unitgrpid"
                                StrReferenceIds = enAllocation.UNIT_GROUP
                            Case "unitid"
                                StrReferenceIds = enAllocation.UNIT
                            Case "teamid"
                                StrReferenceIds = enAllocation.TEAM
                            Case "jobgrpid"
                                StrReferenceIds = enAllocation.JOB_GROUP
                            Case "jobid"
                                StrReferenceIds = enAllocation.JOBS
                            Case "cgrpid"
                                StrReferenceIds = enAllocation.CLASS_GROUP
                            Case "classid"
                                StrReferenceIds = enAllocation.CLASSES
                            Case "ccid"
                                StrReferenceIds = enAllocation.COST_CENTER
                                'S.SANDEEP [31 AUG 2016] -- START
                                'ENHANCEMENT : INCLUSION OF GRADES ON ASSESSMENT GROUP {BY ANDREW}
                            Case "grdid"
                                StrReferenceIds = enAllocation.EMPLOYEE_GRADES
                                'S.SANDEEP [31 AUG 2016] -- START
                        End Select
                    'End If
                    If StrAllocationIds <> "" AndAlso StrReferenceIds <> "" Then
                        StrQ = "SELECT " & _
                                   "    hrassess_group_master.assessgroupunkid " & _
                                   "FROM hrassess_group_master WITH (NOLOCK) " & _
                                   "    JOIN hrassess_group_tran WITH (NOLOCK) ON dbo.hrassess_group_master.assessgroupunkid = dbo.hrassess_group_tran.assessgroupunkid " & _
                                   "WHERE allocationunkid = '" & StrAllocationIds & "' AND referenceunkid = '" & StrReferenceIds & "'  " & _
                                   "    AND hrassess_group_tran.isactive = 1 AND hrassess_group_master.isactive = 1 "

                        Dim dsGrp As New DataSet

                        dsGrp = objDataOperation.ExecQuery(StrQ, "List")

                        If objDataOperation.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            Throw exForce
                        End If

                        If dsGrp.Tables(0).Rows.Count > 0 Then
                            For Each dRow As DataRow In dsGrp.Tables("List").Rows
                                iAssessGrpIds &= "," & dRow.Item("assessgroupunkid").ToString
                            Next
                        End If
                    End If
                Next
                StrQ = "SELECT DISTINCT " & _
                       "    hrassess_group_master.assessgroupunkid " & _
                       "FROM hrassess_group_master WITH (NOLOCK) " & _
                       "    JOIN hrassess_group_tran WITH (NOLOCK) ON hrassess_group_master.assessgroupunkid = hrassess_group_tran.assessgroupunkid " & _
                       "WHERE hrassess_group_master.isactive = 1 AND referenceunkid = '" & enAllocation.EMPLOYEE & "' AND hrassess_group_tran.allocationunkid = '" & iEmployeeId & "'  AND hrassess_group_tran.isactive = 1 "

                dsAllocation = objDataOperation.ExecQuery(StrQ, "List")

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If dsAllocation.Tables(0).Rows.Count > 0 Then
                    For Each dRow As DataRow In dsAllocation.Tables("List").Rows
                        iAssessGrpIds &= "," & dRow.Item("assessgroupunkid").ToString
                    Next
                End If
                iAssessGrpIds = Mid(iAssessGrpIds, 2)
            End If

            'S.SANDEEP [25-JAN-2017] -- START
            'ISSUE/ENHANCEMENT : Effective Date Used to Get the Data fron Employee Status Table Was Not Proper.

            'S.SANDEEP [22-MAR-2017] -- START
            'ISSUE/ENHANCEMENT : PA Computation Process Data Issue
            If blnExecutedOnce = False Then
            If iAssessGrpIds.Trim.Length <= 0 Then
                objDataOperation.ClearParameters()
                StrQ = "SELECT @date = CONVERT(NVARCHAR(8),MAX(A.Dt),112) " & _
                       "FROM " & _
                       "( " & _
                             "SELECT " & _
                                  "MAX(hremployee_transfer_tran.effectivedate) Dt " & _
                                 "FROM hremployee_transfer_tran WITH (NOLOCK) WHERE hremployee_transfer_tran.employeeunkid = '" & iEmployeeId & "' AND hremployee_transfer_tran.isvoid = 0 " & _
                             "UNION " & _
                             "SELECT " & _
                                  "MAX(hremployee_categorization_tran.effectivedate) " & _
                                 "FROM hremployee_categorization_tran WITH (NOLOCK) WHERE hremployee_categorization_tran.employeeunkid = '" & iEmployeeId & "' AND hremployee_categorization_tran.isvoid = 0 " & _
                             "UNION " & _
                             "SELECT " & _
                                  "MAX(hremployee_cctranhead_tran.effectivedate) " & _
                                 "FROM hremployee_cctranhead_tran WITH (NOLOCK) WHERE hremployee_cctranhead_tran.employeeunkid = '" & iEmployeeId & "' AND hremployee_cctranhead_tran.isvoid = 0 " & _
                             "UNION " & _
                             "SELECT MAX(prsalaryincrement_tran.incrementdate) " & _
                                 "FROM prsalaryincrement_tran WITH (NOLOCK) WHERE prsalaryincrement_tran.employeeunkid = '" & iEmployeeId & "' AND prsalaryincrement_tran.isvoid = 0 AND prsalaryincrement_tran.isapproved = 1 " & _
                       ") AS A WHERE 1 = 1 "

                objDataOperation.AddParameter("@date", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, dtDate, ParameterDirection.InputOutput)

                objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                dtDate = objDataOperation.GetParameterValue("@date").ToString

                    'S.SANDEEP [22-MAR-2017] -- START
                    'ISSUE/ENHANCEMENT : PA Computation Process Data Issue
                    blnExecutedOnce = True
                    'S.SANDEEP [22-MAR-2017] -- END
                GoTo lbl
            End If
            End If
            'S.SANDEEP [22-MAR-2017] -- END

            
            'S.SANDEEP [25-JAN-2017] -- END


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetCSV_AssessGroupIds", mstrModuleName)
        Finally
        End Try
        Return iAssessGrpIds
    End Function
    'S.SANDEEP [ 05 NOV 2014 ] -- END


    Public Function DeleteAllocationTran(ByVal intTranUnkid As Integer, ByVal intUnkid As Integer) As Boolean
        Dim StrQ As String = String.Empty : Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            objDataOperation.BindTransaction()

            StrQ = "UPDATE hrassess_group_tran " & _
                   "    SET isactive = 0 " & _
                   "WHERE assessgrouptranunkid = '" & intTranUnkid & "'"

            objDataOperation.ExecNonQuery(StrQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            StrQ = "Select * from hrassess_group_tran where assessgroupunkid = @assessgroupunkid and assessgrouptranunkid <> @assessgrouptranunkid AND isactive = 1 "
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@assessgrouptranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intTranUnkid)
            Dim dsList As DataSet = objDataOperation.ExecQuery(StrQ, "List")

            If dsList IsNot Nothing And dsList.Tables(0).Rows.Count > 0 Then

                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_group_master", "assessgroupunkid", intUnkid, "hrassess_group_tran", "assessgrouptranunkid", intTranUnkid, 2, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Else

                StrQ = "Update hrassess_group_master set isactive = 0 " & _
                          " WHERE assessgroupunkid = @assessgroupunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@assessgroupunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

                Call objDataOperation.ExecNonQuery(StrQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "hrassess_group_master", "assessgroupunkid", intUnkid, "hrassess_group_tran", "assessgrouptranunkid", intTranUnkid, 3, 3) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: DeleteAllocationTran; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 29 DEC 2011 ] -- END

    'S.SANDEEP [ 09 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Sub Get_Weight(ByRef mDecRemain As Decimal, ByRef mDecAssigned As Decimal, Optional ByVal iGroupUnkid As Integer = 0)
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = "" : Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT (100 - ISNULL(SUM(weight),0)) AS mDecRemain ,ISNULL(SUM(weight),0) AS mDecAssigned " & _
                   "FROM hrassess_group_master WHERE isactive = 1 "

            If iGroupUnkid > 0 Then
                StrQ &= " AND assessgroupunkid <> '" & iGroupUnkid & "' "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mDecRemain = CDec(dsList.Tables(0).Rows(0)("mDecRemain"))
            mDecAssigned = CDec(dsList.Tables(0).Rows(0)("mDecAssigned"))

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Weight; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [ 09 AUG 2013 ] -- END

    'S.SANDEEP [ 14 AUG 2013 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public Function Get_Group_Id(ByVal iCode As String, ByVal iName As String) As Integer
        Dim dsList As DataSet = Nothing
        Dim StrQ As String = "" : Dim exForce As Exception
        Dim iValue As Integer
        Try
            objDataOperation = New clsDataOperation

            StrQ = "SELECT assessgroupunkid " & _
                   "FROM hrassess_group_master WHERE isactive = 1 "

            If iCode.Trim.Length > 0 Then
                StrQ &= " AND assessgroup_code = '" & iCode & "' "
            End If

            If iName.Trim.Length > 0 Then
                StrQ &= " AND assessgroup_name = '" & iName & "' "
            End If

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables("List").Rows.Count > 0 Then
                iValue = dsList.Tables("List").Rows(0).Item("assessgroupunkid")
            End If

            Return iValue

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Get_Group_Id; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function
    'S.SANDEEP [ 14 AUG 2013 ] -- END

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "This Assess Group Code is already defined. Please define new Assess Group Code.")
            Language.setMessage(mstrModuleName, 2, "This Assess Group Name is already defined. Please define new Assess Group Name.")
            Language.setMessage(mstrModuleName, 3, "Select")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class