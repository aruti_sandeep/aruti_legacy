﻿'************************************************************************************************************************************
'Class Name : clsErrorlog_Tran.vb
'Purpose    :
'Date       :12-04-2019
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Web
Imports System.Threading
Imports System.ComponentModel

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsErrorlog_Tran
    Private Shared ReadOnly mstrModuleName As String = "clsErrorlog_Tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""

#Region " Private variables "
    Private mintErrorlogunkid As Integer
    Private mstrError_Message As String = String.Empty
    Private mstrError_Location As String = String.Empty
    Private mdtError_Date As Date
    Private mintCompanyunkid As Integer
    Private mstrDatabase_Version As String = String.Empty
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsemailsent As Boolean
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
    Private mstrIp As String = String.Empty
    Private mstrHost As String = String.Empty
#End Region

#Region " Properties "
    <Description("Hide")> _
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    <Description("Hide")> _
    Public Property _Errorlogunkid() As Integer
        Get
            Return mintErrorlogunkid
        End Get
        Set(ByVal value As Integer)
            mintErrorlogunkid = value
            'Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set error_message
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Error_Message() As String
        Get
            Return mstrError_Message
        End Get
        Set(ByVal value As String)
            mstrError_Message = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set error_location
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Error_Location() As String
        Get
            Return mstrError_Location
        End Get
        Set(ByVal value As String)
            mstrError_Location = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set error_date
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Error_Date() As Date
        Get
            If mstrError_DateString.Trim.Length > 0 Then
                Return CDate(mstrError_DateString)
            Else
                Return Nothing
            End If
        End Get
        'Set(ByVal value As Date)
        '    mdtError_Date = value
        'End Set
    End Property

    <Description("Hide")> _
    Public Property _Companyunkid() As Integer
        Get
            Return mintCompanyunkid
        End Get
        Set(ByVal value As Integer)
            mintCompanyunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set database_version
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Database_Version() As String
        Get
            Return mstrDatabase_Version
        End Get
        Set(ByVal value As String)
            mstrDatabase_Version = value
        End Set
    End Property

    <Description("Hide")> _
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = value
        End Set
    End Property

    <Description("Hide")> _
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isemailsent
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isemailsent() As Boolean
        Get
            Return mblnIsemailsent
        End Get
        Set(ByVal value As Boolean)
            mblnIsemailsent = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    <Description("Hide")> _
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Voiddatetime() As Date
        Get
            If mstrVoiddatetimeString.Trim.Length > 0 Then
                Return CDate(mstrVoiddatetimeString)
            Else
                Return Nothing
            End If
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set ip
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Ip() As String
        Get
            Return mstrIp
        End Get
        Set(ByVal value As String)
            mstrIp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set host
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Host() As String
        Get
            Return mstrHost
        End Get
        Set(ByVal value As String)
            mstrHost = value
        End Set
    End Property

    Private mintLoginTypeId As Integer = enLogin_Mode.DESKTOP
    <Description("Hide")> _
    Public Property _LoginTypeId() As Integer
        Get
            Return mintLoginTypeId
        End Get
        Set(ByVal value As Integer)
            mintLoginTypeId = value
        End Set
    End Property

    Private mstrForm_Name As String = String.Empty
    <Description("Hide")> _
    Public Property _Form_Name() As String
        Get
            Return mstrForm_Name
        End Get
        Set(ByVal value As String)
            mstrForm_Name = value
        End Set
    End Property

    Private mintModuleRefId As Integer
    <Description("Hide")> _
    Public Property _ModuleRefId() As Integer
        Get
            Return mintModuleRefId
        End Get
        Set(ByVal value As Integer)
            mintModuleRefId = value
        End Set
    End Property

#End Region

#Region " Read Only Properties "

    Private mblnIsChecked As Boolean = False
    <Description("Hide")> _
    Public Property _IsChecked() As Boolean
        Get
            Return mblnIsChecked
        End Get
        Set(ByVal value As Boolean)
            mblnIsChecked = value
        End Set
    End Property

    <Description("Hide")> _
    Public ReadOnly Property _Error_MessageDecrypted() As String
        Get
            If mstrError_Message.Trim.Length > 0 Then
                'S.SANDEEP |04-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
                'Return clsCrypto.Dicrypt(mstrError_Message)
                Try
                Return clsCrypto.Dicrypt(mstrError_Message)
                Catch ex As Exception
                    Return mstrError_Message
                End Try
                'S.SANDEEP |04-MAR-2020| -- END
            Else
                Return ""
            End If
        End Get
    End Property

    Private mstrError_DateString As String = String.Empty
    <Description("Hide")> _
    Public Property _Error_DateString() As String
        Get
            Return mstrError_DateString
        End Get
        Set(ByVal value As String)
            mstrError_DateString = value
        End Set
    End Property

    Private mstrVoiddatetimeString As String = String.Empty
    <Description("Hide")> _
    Public Property _VoiddatetimeString() As String
        Get
            Return mstrVoiddatetimeString
        End Get
        Set(ByVal value As String)
            mstrVoiddatetimeString = value
        End Set
    End Property

    Private mstrCompanyCode As String = String.Empty
    Public ReadOnly Property _CompanyCode() As String
        Get
            Return mstrCompanyCode
        End Get
    End Property

    Private mstrCompanyName As String = String.Empty
    Public ReadOnly Property _CompanyName() As String
        Get
            Return mstrCompanyName
        End Get
    End Property

    Private mstrUserName As String = String.Empty
    Public ReadOnly Property _UserName() As String
        Get
            Return mstrUserName
        End Get
    End Property

    Private mstrUserEmail As String = String.Empty
    Public ReadOnly Property _UserEmail() As String
        Get
            Return mstrUserEmail
        End Get
    End Property

    Private mblnIsESS As Boolean = False
    Public ReadOnly Property _IsESS() As Boolean
        Get
            Return mblnIsESS
        End Get
    End Property

    Private mstrArchivedByUserName As String = String.Empty
    Public ReadOnly Property _ArchivedByUserName() As String
        Get
            Return mstrArchivedByUserName
        End Get
    End Property
#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetListCollection(ByVal xDataOp As clsDataOperation _
                                      , ByVal intUnkId As Integer _
                                      , Optional ByVal strDatabaseName As String = "" _
                                      , Optional ByVal blnOnlyArchived As Boolean = False _
                                      , Optional ByVal blnOnlySentEmail As Boolean = False _
                                      , Optional ByVal dtFromDate As Date = Nothing _
                                      , Optional ByVal dtToDate As Date = Nothing _
                                      ) As List(Of clsErrorlog_Tran)
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objError As List(Of clsErrorlog_Tran) = Nothing
        Dim strDBName As String = ""
        If strDatabaseName.Trim <> "" Then
            strDBName = strDatabaseName & ".."
        End If

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
                      "  cferrorlog_tran.errorlogunkid " & _
                      ", cferrorlog_tran.error_message " & _
                      ", cferrorlog_tran.error_location " & _
                      ", cferrorlog_tran.error_date " & _
                      ", cferrorlog_tran.companyunkid " & _
                      ", cfcompany_master.code AS companycode " & _
                      ", cfcompany_master.name AS companyname " & _
                      ", cferrorlog_tran.database_version " & _
                      ", cferrorlog_tran.userunkid " & _
                      ", cferrorlog_tran.loginemployeeunkid " & _
                      ", cferrorlog_tran.isemailsent " & _
                      ", cferrorlog_tran.isweb " & _
                      ", cferrorlog_tran.isvoid " & _
                      ", cferrorlog_tran.voiduserunkid " & _
                      ", CASE WHEN cferrorlog_tran.voiduserunkid > 0 THEN CASE LTRIM(RTRIM(vuser.firstname)) WHEN '' THEN vuser.username ELSE vuser.firstname + ' ' + ISNULL(vuser.lastname, '') END ELSE '' END AS voidusername " & _
                      ", cferrorlog_tran.voiddatetime " & _
                      ", cferrorlog_tran.voidreason " & _
                      ", cferrorlog_tran.ip " & _
                      ", cferrorlog_tran.host "

            If strDatabaseName.Trim.ToLower = "hrmsconfiguration" Then
                strQ &= ", CASE WHEN cferrorlog_tran.userunkid > 0 THEN CASE LTRIM(RTRIM(cfuser_master.firstname)) WHEN '' THEN cfuser_master.username ELSE cfuser_master.firstname + ' ' + ISNULL(cfuser_master.lastname, '') END ELSE '' END AS username " & _
                        ", CASE WHEN cferrorlog_tran.userunkid > 0 THEN cfuser_master.email ELSE '' END AS useremail "
            Else
                strQ &= ", CASE WHEN cferrorlog_tran.userunkid > 0 THEN CASE LTRIM(RTRIM(cfuser_master.firstname)) WHEN '' THEN cfuser_master.username ELSE cfuser_master.firstname + ' ' + ISNULL(cfuser_master.lastname, '') END WHEN cferrorlog_tran.loginemployeeunkid > 0 THEN hremployee_master.firstname + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + hremployee_master.surname ELSE '' END AS username " & _
                        ", CASE WHEN cferrorlog_tran.userunkid > 0 THEN cfuser_master.email WHEN cferrorlog_tran.loginemployeeunkid > 0 THEN hremployee_master.email ELSE '' END AS useremail "
            End If

            strQ &= "FROM " & strDBName & "cferrorlog_tran " & _
                 "LEFT JOIN hrmsConfiguration..cfcompany_master ON cferrorlog_tran.companyunkid = cfcompany_master.companyunkid " & _
                 "LEFT JOIN hrmsConfiguration..cfuser_master ON cferrorlog_tran.userunkid = cfuser_master.userunkid AND cferrorlog_tran.userunkid > 0 " & _
                 "LEFT JOIN hrmsConfiguration..cfuser_master AS vuser ON cferrorlog_tran.voiduserunkid = vuser.userunkid AND cferrorlog_tran.voiduserunkid > 0 "

            If strDatabaseName.Trim.ToLower <> "hrmsconfiguration" Then
                strQ &= "LEFT JOIN " & strDBName & "hremployee_master ON hremployee_master.employeeunkid = cferrorlog_tran.loginemployeeunkid AND cferrorlog_tran.loginemployeeunkid > 0 "
            End If

            strQ &= "WHERE 1 = 1 "

            If intUnkId > 0 Then
                strQ &= " AND cferrorlog_tran.errorlogunkid = @errorlogunkid "
                objDataOperation.AddParameter("@errorlogunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkId)
            Else
                If blnOnlyArchived = False Then
                    strQ &= " AND cferrorlog_tran.isvoid = 0 "
                Else
                    strQ &= " AND cferrorlog_tran.isvoid = 1 "
                End If

                If blnOnlySentEmail = False Then
                    strQ &= " AND cferrorlog_tran.isemailsent = 0 "
                Else
                    strQ &= " AND cferrorlog_tran.isemailsent = 1 "
                End If

                If dtFromDate <> Nothing Then
                    strQ &= " AND CONVERT(CHAR(8), cferrorlog_tran.error_date, 112) >= @fromdate "
                    objDataOperation.AddParameter("@fromdate", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, dtFromDate)
                End If

                If dtToDate <> Nothing Then
                    strQ &= " AND CONVERT(CHAR(8), cferrorlog_tran.error_date, 112) <= @todate "
                    objDataOperation.AddParameter("@todate", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, dtToDate)
                End If
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then 'Sohail (07 Nov 2019)

            objError = (From p In dsList.Tables(0) Select New clsErrorlog_Tran With _
                            {._Errorlogunkid = CInt(p.Item("errorlogunkid")) _
                             , ._Error_Message = p.Item("error_message").ToString _
                             , ._Error_Location = p.Item("error_location").ToString _
                             , ._Error_DateString = Format(CDate(p.Item("error_date")), "yyyy-MM-dd HH:mm:ss") _
                             , ._Companyunkid = CInt(p.Item("companyunkid")) _
                             , ._Database_Version = p.Item("database_version").ToString _
                             , ._Userunkid = CInt(p.Item("userunkid")) _
                             , ._Loginemployeeunkid = CInt(p.Item("loginemployeeunkid")) _
                             , ._Isweb = CBool(p.Item("isweb")) _
                             , ._Isemailsent = CBool(p.Item("isemailsent")) _
                             , ._Isvoid = CBool(p.Item("isvoid")) _
                             , ._Voiduserunkid = CInt(p.Item("voiduserunkid")) _
                             , ._VoiddatetimeString = If(IsDBNull(p.Item("voiddatetime")) = True, "", Format(CDate(p.Item("voiddatetime")), "yyyy-MM-dd HH:mm:ss")) _
                             , ._Voidreason = p.Item("voidreason").ToString _
                             , ._Ip = p.Item("ip").ToString _
                             , ._Host = p.Item("host").ToString _
                             , .mstrCompanyCode = p.Item("companycode").ToString _
                             , .mstrCompanyName = p.Item("companyname").ToString _
                             , .mstrUserName = p.Item("username").ToString _
                             , .mstrUserEmail = p.Item("useremail").ToString _
                             , .mblnIsESS = CBool(If(CInt(p.Item("loginemployeeunkid")) > 0, True, False)) _
                             , .mstrArchivedByUserName = If(CInt(p.Item("voiduserunkid")) > 0, p.Item("voidusername").ToString, "").ToString _
                             }).ToList

            End If 'Sohail (07 Nov 2019)

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetListCollection; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return objError
    End Function
    'Public Sub GetData()
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        StrQ = "SELECT " & _
    '          "  errorlogunkid " & _
    '          ", error_message " & _
    '          ", error_location " & _
    '          ", error_date " & _
    '          ", companyunkid " & _
    '          ", database_version " & _
    '          ", userunkid " & _
    '          ", loginemployeeunkid " & _
    '          ", isemailsent " & _
    '          ", isweb " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '          ", ip " & _
    '          ", host " & _
    '         "FROM cferrorlog_tran " & _
    '         "WHERE errorlogunkid = @errorlogunkid "

    '        objDataOperation.AddParameter("@errorlogunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintErrorlogUnkId.ToString)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        For Each dtRow As DataRow In dsList.Tables(0).Rows
    '            minterrorlogunkid = CInt(dtRow.Item("errorlogunkid"))
    '            minterror_message = CInt(dtRow.Item("error_message"))
    '            mstrerror_location = dtRow.Item("error_location").ToString
    '            mdterror_date = dtRow.Item("error_date")
    '            mintcompanyunkid = CInt(dtRow.Item("companyunkid"))
    '            mstrdatabase_version = dtRow.Item("database_version").ToString
    '            mintuserunkid = CInt(dtRow.Item("userunkid"))
    '            mintloginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
    '            mblnisemailsent = CBool(dtRow.Item("isemailsent"))
    '            mblnisweb = CBool(dtRow.Item("isweb"))
    '            mblnisvoid = CBool(dtRow.Item("isvoid"))
    '            mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
    '            mdtvoiddatetime = dtRow.Item("voiddatetime")
    '            mstrvoidreason = dtRow.Item("voidreason").ToString
    '            mstrip = dtRow.Item("ip").ToString
    '            mstrhost = dtRow.Item("host").ToString
    '            Exit For
    '        Next
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Sub


    '''' <summary>
    '''' Modify By: Sohail
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = True) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  errorlogunkid " & _
    '          ", error_message " & _
    '          ", error_location " & _
    '          ", error_date " & _
    '          ", companyunkid " & _
    '          ", database_version " & _
    '          ", userunkid " & _
    '          ", loginemployeeunkid " & _
    '          ", isemailsent " & _
    '          ", isweb " & _
    '          ", isvoid " & _
    '          ", voiduserunkid " & _
    '          ", voiddatetime " & _
    '          ", voidreason " & _
    '          ", ip " & _
    '          ", host " & _
    '         "FROM cferrorlog_tran "

    '        If blnOnlyActive Then
    '            strQ &= " WHERE isactive = 1 "
    '        End If

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (cferrorlog_tran) </purpose>
    Public Function Insert(ByVal xDataOp As clsDataOperation, ByVal objErrorLog As clsErrorlog_Tran, Optional ByVal strDatabaseName As String = "") As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strDBName As String = ""
        If strDatabaseName.Trim <> "" Then
            strDBName = strDatabaseName & ".."
        End If

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            'S.SANDEEP |04-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
            'objDataOperation.AddParameter("@error_message", SqlDbType.NText, eZeeDataType.DESC_SIZE, objErrorLog._Error_Location)
            objDataOperation.AddParameter("@error_message", SqlDbType.NVarChar, objErrorLog._Error_Message.Length, objErrorLog._Error_Message)
            'S.SANDEEP |04-MAR-2020| -- END
            objDataOperation.AddParameter("@error_location", SqlDbType.NText, eZeeDataType.NAME_SIZE, objErrorLog._Error_Location)
            objDataOperation.AddParameter("@error_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, objErrorLog._Error_Date)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objErrorLog._Companyunkid)
            objDataOperation.AddParameter("@database_version", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, objErrorLog._Database_Version)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objErrorLog._Userunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objErrorLog._Loginemployeeunkid)
            objDataOperation.AddParameter("@isemailsent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, objErrorLog._Isemailsent)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, objErrorLog._Isweb)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, objErrorLog._Isvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objErrorLog._Voiduserunkid)
            If objErrorLog._Voiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, objErrorLog._Voiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, objErrorLog._Voidreason)
            objDataOperation.AddParameter("@ip", SqlDbType.NText, eZeeDataType.NAME_SIZE, objErrorLog._Ip)
            objDataOperation.AddParameter("@host", SqlDbType.NText, eZeeDataType.NAME_SIZE, objErrorLog._Host)

            strQ = "INSERT INTO " & strDBName & "cferrorlog_tran ( " & _
              "  error_message " & _
              ", error_location " & _
              ", error_date " & _
              ", companyunkid " & _
              ", database_version " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isemailsent " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ip " & _
              ", host" & _
            ") VALUES (" & _
              "  @error_message " & _
              ", @error_location " & _
              ", @error_date " & _
              ", @companyunkid " & _
              ", @database_version " & _
              ", @userunkid " & _
              ", @loginemployeeunkid " & _
              ", @isemailsent " & _
              ", @isweb " & _
              ", @isvoid " & _
              ", @voiduserunkid " & _
              ", @voiddatetime " & _
              ", @voidreason " & _
              ", @ip " & _
              ", @host" & _
            "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintErrorlogunkid = dsList.Tables(0).Rows(0).Item(0)

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (cferrorlog_tran) </purpose>
    Public Function Update(ByVal xDataOp As clsDataOperation, ByVal objErrorLog As clsErrorlog_Tran, Optional ByVal strDatabaseName As String = "") As Boolean
        'If isExist(mstrName, mintErrorlogunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strDBName As String = ""
        If strDatabaseName.Trim <> "" Then
            strDBName = strDatabaseName & ".."
        End If

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@errorlogunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objErrorLog._Errorlogunkid)
            'S.SANDEEP |04-MAR-2020| -- START
            'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
            'objDataOperation.AddParameter("@error_message", SqlDbType.NText, eZeeDataType.DESC_SIZE, objErrorLog._Error_Message)
            objDataOperation.AddParameter("@error_message", SqlDbType.NVarChar, objErrorLog._Error_Message.Length, objErrorLog._Error_Message)
            'S.SANDEEP |04-MAR-2020| -- END
            objDataOperation.AddParameter("@error_location", SqlDbType.NText, eZeeDataType.NAME_SIZE, objErrorLog._Error_Location)
            objDataOperation.AddParameter("@error_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, objErrorLog._Error_Date)
            objDataOperation.AddParameter("@companyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objErrorLog._Companyunkid)
            objDataOperation.AddParameter("@database_version", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, objErrorLog._Database_Version)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objErrorLog._Userunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objErrorLog._Loginemployeeunkid)
            objDataOperation.AddParameter("@isemailsent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, objErrorLog._Isemailsent)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, objErrorLog._Isweb)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, objErrorLog._Isvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objErrorLog._Voiduserunkid)
            If objErrorLog._Voiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, objErrorLog._Voiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, objErrorLog._Voidreason)
            objDataOperation.AddParameter("@ip", SqlDbType.NText, eZeeDataType.NAME_SIZE, objErrorLog._Ip)
            objDataOperation.AddParameter("@host", SqlDbType.NText, eZeeDataType.NAME_SIZE, objErrorLog._Host)

            strQ = "UPDATE " & strDBName & "cferrorlog_tran SET " & _
              "  error_message = @error_message " & _
              ", error_location = @error_location " & _
              ", error_date = @error_date " & _
              ", companyunkid = @companyunkid " & _
              ", database_version = @database_version " & _
              ", userunkid = @userunkid " & _
              ", loginemployeeunkid = @loginemployeeunkid " & _
              ", isemailsent = @isemailsent " & _
              ", isweb = @isweb " & _
              ", isvoid = @isvoid " & _
              ", voiduserunkid = @voiduserunkid " & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
              ", ip = @ip " & _
              ", host = @host " & _
            "WHERE errorlogunkid = @errorlogunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (cferrorlog_tran) </purpose>
    Public Function Delete(ByVal xDataOp As clsDataOperation, ByVal lstErrorLog As List(Of clsErrorlog_Tran), ByVal intVoidUserId As Integer, ByVal dtVoidDateTime As Date, ByVal strVoidReason As String, Optional ByVal strDatabaseName As String = "") As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strDBName As String = ""
        If strDatabaseName.Trim <> "" Then
            strDBName = strDatabaseName & ".."
        End If

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE " & strDBName & "cferrorlog_tran SET " & _
              "  isvoid = 1 " & _
              ", voiduserunkid = @voiduserunkid " & _
              ", voiddatetime = @voiddatetime " & _
              ", voidreason = @voidreason " & _
            "WHERE errorlogunkid = @errorlogunkid " & _
                " AND isvoid = 0 "

            For Each objErr As clsErrorlog_Tran In lstErrorLog
                objDataOperation.ClearParameters()

                objDataOperation.AddParameter("@errorlogunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, objErr._Errorlogunkid)
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUserId)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.Date, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason)

                Call objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            Next

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True

        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "<Query>"

            objDataOperation.AddParameter("@errorlogunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal xDataOp As clsDataOperation, ByVal strCode As String, ByVal strName As String, Optional ByVal intUnkid As Integer = -1, Optional ByVal strDatabaseName As String = "") As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim strDBName As String = ""
        If strDatabaseName.Trim <> "" Then
            strDBName = strDatabaseName & ".."
        End If

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  errorlogunkid " & _
              ", error_message " & _
              ", error_location " & _
              ", error_date " & _
              ", companyunkid " & _
              ", database_version " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isemailsent " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
              ", ip " & _
              ", host " & _
             "FROM " & strDBName & "cferrorlog_tran " & _
             "WHERE isvod = 0 "

            If intUnkid > 0 Then
                strQ &= " AND errorlogunkid <> @errorlogunkid"
            End If

            objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strName)
            objDataOperation.AddParameter("@errorlogunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function SendEmail(ByVal xDataOp As clsDataOperation, ByVal lstErrorLog As List(Of clsErrorlog_Tran), Optional ByVal strDatabaseName As String = "") As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objMail As New clsSendMail
        Dim objConfig As New clsConfigOptions
        Dim intCompanyUnkId As Integer = 0
        Dim strDBName As String = ""
        If strDatabaseName.Trim <> "" Then
            strDBName = strDatabaseName & ".."
        End If

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            gobjEmailList = New List(Of clsEmailCollection)

            For Each objErr As clsErrorlog_Tran In lstErrorLog
                intCompanyUnkId = objErr._Companyunkid

                Dim strToEmail As String = objConfig.GetKeyValue(intCompanyUnkId, "AdministratorEmailForError")

                If strToEmail.Trim = "" Then
                    mstrMessage = Language.getMessage(mstrModuleName, 8, "Administrator Email is not set. Please set Administrator Email on Aruti Configuration to Report Error.")
                    Return False
                End If

                objMail._Subject = Language.getMessage(mstrModuleName, 1, "Error in Aruti Self Service : ") & " " & objErr._CompanyName
                objMail._Message &= "<table style='width: 100%;'>"
                objMail._Message &= "<tr style='width: 100%;'>"
                objMail._Message &= "<td style='width: 20%;' valign='top' >"
                objMail._Message &= Language.getMessage(mstrModuleName, 2, "Error : ")
                objMail._Message &= "<td/>"
                objMail._Message &= "<td style='width: 80%;'>"
                'S.SANDEEP |04-MAR-2020| -- START
                'ISSUE/ENHANCEMENT : WHEN LENGTH IS INCREASED, IT WAS NOT GETTING DECRYPTED SO WE JUST INSERTED DIRECTLY WITH OUT ENCRYPTION
                'objMail._Message &= "<B>" & clsCrypto.Dicrypt(objErr._Error_Message) & "</B>"
                Try
                objMail._Message &= "<B>" & clsCrypto.Dicrypt(objErr._Error_Message) & "</B>"
                Catch ex As Exception
                    objMail._Message &= "<B>" & objErr._Error_Message & "</B>"
                End Try
                'S.SANDEEP |04-MAR-2020| -- END
                objMail._Message &= "<td/>"
                objMail._Message &= "<tr /><tr style='width: 100%;'>"
                objMail._Message &= "<td style='width: 20%;'>"
                objMail._Message &= Language.getMessage(mstrModuleName, 3, "Company Code : ")
                objMail._Message &= "<td/>"
                objMail._Message &= "<td style='width: 80%;'>"
                objMail._Message &= objErr._CompanyCode
                objMail._Message &= "<td/>"
                objMail._Message &= "<tr /><tr style='width: 100%;'>"
                objMail._Message &= "<td style='width: 20%;'>"
                objMail._Message &= Language.getMessage(mstrModuleName, 4, "Company Name : ")
                objMail._Message &= "<td/>"
                objMail._Message &= "<td style='width: 80%;'>"
                objMail._Message &= objErr._CompanyName
                objMail._Message &= "<td/>"
                objMail._Message &= "<tr /><tr style='width: 100%;'>"
                objMail._Message &= "<td style='width: 20%;'>"
                objMail._Message &= Language.getMessage(mstrModuleName, 5, "Database Version : ")
                objMail._Message &= "<td/>"
                objMail._Message &= "<td style='width: 80%;'>"
                objMail._Message &= objErr._Database_Version
                objMail._Message &= "<td/>"
                objMail._Message &= "<tr /><tr style='width: 100%;'>"
                objMail._Message &= "<td style='width: 20%;'>"
                objMail._Message &= Language.getMessage(mstrModuleName, 6, "Location : ")
                objMail._Message &= "<td/>"
                objMail._Message &= "<td style='width: 80%;'>"
                objMail._Message &= objErr._Error_Location
                objMail._Message &= "<td/>"
                objMail._Message &= "<tr /><tr style='width: 100%;'>"
                objMail._Message &= "<td style='width: 20%;'>"
                objMail._Message &= Language.getMessage(mstrModuleName, 7, "Reported by : ")
                objMail._Message &= "<td/>"
                objMail._Message &= "<td style='width: 80%;'>"
                objMail._Message &= objErr._UserName
                objMail._Message &= "<td/>"
                objMail._Message &= "<tr /><tr style='width: 100%;'>"
                objMail._Message &= "<td style='width: 20%;'>"
                objMail._Message &= Language.getMessage(mstrModuleName, 9, "Reported by Email : ")
                objMail._Message &= "<td/>"
                objMail._Message &= "<td style='width: 80%;'>"
                objMail._Message &= objErr._UserEmail
                objMail._Message &= "<td/>"
                objMail._Message &= "<tr />"
                objMail._Message &= "</table>"

                objMail._ToEmail = objConfig.GetKeyValue(objErr._Companyunkid, "AdministratorEmailForError")

                objMail._UserUnkid = objErr._Userunkid
                objMail._LogEmployeeUnkid = objErr._Loginemployeeunkid
                objMail._Form_Name = mstrForm_Name
                objMail._OperationModeId = mintLoginTypeId
                objMail._SenderAddress = objErr._UserName
                objMail._ModuleRefId = mintModuleRefId

                Dim intUnkId As Integer = 0
                If objErr._Isemailsent = False Then
                    intUnkId = objErr._Errorlogunkid
                End If
                gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrForm_Name, mintLoginemployeeunkid, objErr._Ip, objErr._Host, objErr._Userunkid, mintLoginTypeId, mintModuleRefId, objErr._UserName, "", "", intUnkId))

            Next

            If xDataOp Is Nothing Then objDataOperation = Nothing

            If HttpContext.Current Is Nothing Then
                Dim trd As Thread = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                Dim arr(2) As Object
                arr(0) = intCompanyUnkId
                arr(1) = strDatabaseName
                trd.Start(arr)
            Else
                Call Send_Notification(intCompanyUnkId, strDatabaseName)
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Private Sub Send_Notification(ByVal intCompanyUnkId As Object, Optional ByVal strDatabaseName As String = "")
        Dim strResult As String = String.Empty
        Dim strDBName As String = ""
        If strDatabaseName.Trim <> "" Then
            strDBName = strDatabaseName & ".."
        End If
        Try
            If gobjEmailList.Count > 0 Then

                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId

                    strResult = String.Empty
                    Try
                        If TypeOf intCompanyUnkId Is Integer Then
                            strResult = objSendMail.SendMail(CInt(intCompanyUnkId))
                        Else
                            strResult = objSendMail.SendMail(CInt(intCompanyUnkId(0)))
                        End If
                    Catch ex As Exception

                    End Try

                    If strResult.Trim = "" AndAlso obj._TranUnkId > 0 Then
                        'Dim objE As List(Of clsErrorlog_Tran) = GetListCollection(Nothing, obj._TranUnkId)
                        'objE(0)._Isemailsent = True
                        'If Update(Nothing, objE(0)) = False Then

                        'End If
                        Dim xDataop As New clsDataOperation
                        Dim strQ As String
                        strQ = "UPDATE " & strDBName & "cferrorlog_tran SET " & _
                             "  isemailsent = 1 " & _
                           "WHERE errorlogunkid = @errorlogunkid "

                        xDataop.AddParameter("@errorlogunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, obj._TranUnkId)

                        Call xDataop.ExecNonQuery(strQ)

                        If xDataop.ErrorMessage <> "" Then
                            Throw New Exception(xDataop.ErrorNumber & ": " & xDataop.ErrorMessage)
                        End If

                        xDataop = Nothing

                    ElseIf strResult.Trim <> "" Then
                        Exit For

                    End If
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Public Shared Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Error in Aruti Self Service :")
            Language.setMessage(mstrModuleName, 2, "Error :")
            Language.setMessage(mstrModuleName, 3, "Company Code :")
            Language.setMessage(mstrModuleName, 4, "Company Name :")
            Language.setMessage(mstrModuleName, 5, "Database Version :")
            Language.setMessage(mstrModuleName, 6, "Location :")
            Language.setMessage(mstrModuleName, 7, "Reported by :")
            Language.setMessage(mstrModuleName, 8, "Administrator Email is not set. Please set Administrator Email on Aruti Configuration to Report Error.")
	    Language.setMessage(mstrModuleName, 9, "Email :")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class
