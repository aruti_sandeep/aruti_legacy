﻿'************************************************************************************************************************************
'Class Name : clsBatchSchedule_interviewer_tran.vb
'Purpose    :
'Date       :28/09/2019
'Written By :Hemant
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
Public Class clsBatchSchedule_interviewer_tran
    Private Shared ReadOnly mstrModuleName As String = "clsBatchSchedule_interviewer_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty

#Region " Private variables "
    Private mintBatchScheduleInterviewertranunkid As Integer
    Private mintBatchScheduleUnkid As Integer
    Private mintVacancyunkid As Integer = -1
    Private mintInterviewerunkid As Integer
    Private mdtTran As DataTable

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Hemant
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set BatchScheduleUnkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _BatchScheduleUnkid() As Integer
        Get
            Return mintBatchScheduleUnkid
        End Get
        Set(ByVal value As Integer)
            mintBatchScheduleUnkid = value
            Call GetInterviewer_tran()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vacancyunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _VacancyUnkid() As Integer
        Get
            Return mintVacancyunkid
        End Get
        Set(ByVal value As Integer)
            mintVacancyunkid = value
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set vacancyunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Interviewernkid() As Integer
        Get
            Return mintInterviewerunkid
        End Get
        Set(ByVal value As Integer)
            mintInterviewerunkid = value

        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set DataTable
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _DataTable() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Hemant
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property

#End Region

#Region " Constructor "
    Public Sub New()
        mdtTran = New DataTable("")
        Dim dCol As DataColumn

        Try

            dCol = New DataColumn("batchscheduleinterviewertranunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("batchscheduleunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("vacancyunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("interviewtypeunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("interviewer_level")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("interviewerunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("otherinterviewer_name")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("othercompany")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("otherdepartment")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("othercontact_no")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("userunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("isvoid")
            dCol.DataType = System.Type.GetType("System.Boolean")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiduserunkid")
            dCol.DataType = System.Type.GetType("System.Int32")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voiddatetime")
            dCol.DataType = System.Type.GetType("System.DateTime")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("voidreason")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("GUID")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)

            dCol = New DataColumn("AUD")
            dCol.DataType = System.Type.GetType("System.String")
            mdtTran.Columns.Add(dCol)



        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "New", mstrModuleName)
        End Try

    End Sub
#End Region

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose>To Get Transaction Records </purpose>
    Public Sub GetInterviewer_tran()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim dRowID_Tran As DataRow
        Dim strErrorMessage As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            strQ = "SELECT " & _
                      "rcbatchschedule_interviewer_tran.batchscheduleinterviewertranunkid " & _
                      ", rcbatchschedule_interviewer_tran.batchscheduleunkid " & _
                      ", rcbatchschedule_interviewer_tran.vacancyunkid " & _
                      ", rcbatchschedule_interviewer_tran.interviewtypeunkid " & _
                      ", rcbatchschedule_interviewer_tran.interviewer_level " & _
                      ", rcbatchschedule_interviewer_tran.interviewerunkid " & _
                      ", rcbatchschedule_interviewer_tran.otherinterviewer_name " & _
                      ", rcbatchschedule_interviewer_tran.othercompany " & _
                      ", rcbatchschedule_interviewer_tran.otherdepartment " & _
                      ", rcbatchschedule_interviewer_tran.othercontact_no " & _
                      ", rcbatchschedule_interviewer_tran.userunkid " & _
                      ", rcbatchschedule_interviewer_tran.isvoid " & _
                      ", rcbatchschedule_interviewer_tran.voiduserunkid " & _
                      ", rcbatchschedule_interviewer_tran.voiddatetime " & _
                      ", rcbatchschedule_interviewer_tran.voidreason " & _
                      ", '' AUD " & _
                   "FROM rcbatchschedule_interviewer_tran " & _
                   "WHERE batchscheduleunkid = @batchscheduleunkid " & _
                         "AND ISNULL(isvoid, 0) = 0 "

            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchScheduleUnkid.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Clear()

            For i As Integer = 0 To dsList.Tables(0).Rows.Count - 1
                With dsList.Tables("List").Rows(i)
                    dRowID_Tran = mdtTran.NewRow
                    dRowID_Tran.Item("batchscheduleinterviewertranunkid") = .Item("batchscheduleinterviewertranunkid").ToString
                    dRowID_Tran.Item("batchscheduleunkid") = .Item("batchscheduleunkid").ToString
                    dRowID_Tran.Item("vacancyunkid") = .Item("vacancyunkid").ToString
                    dRowID_Tran.Item("interviewtypeunkid") = .Item("interviewtypeunkid").ToString
                    dRowID_Tran.Item("interviewer_level") = .Item("interviewer_level").ToString
                    dRowID_Tran.Item("interviewerunkid") = .Item("interviewerunkid").ToString
                    dRowID_Tran.Item("otherinterviewer_name") = .Item("otherinterviewer_name").ToString
                    dRowID_Tran.Item("othercompany") = .Item("othercompany").ToString
                    dRowID_Tran.Item("otherdepartment") = .Item("otherdepartment").ToString
                    dRowID_Tran.Item("othercontact_no") = .Item("othercontact_no").ToString
                    dRowID_Tran.Item("userunkid") = .Item("userunkid").ToString
                    dRowID_Tran.Item("isvoid") = .Item("isvoid").ToString
                    dRowID_Tran.Item("voiduserunkid") = .Item("voiduserunkid").ToString
                    dRowID_Tran.Item("voiddatetime") = .Item("voiddatetime")
                    dRowID_Tran.Item("voidreason") = .Item("voidreason").ToString
                    dRowID_Tran.Item("AUD") = .Item("AUD")
                    mdtTran.Rows.Add(dRowID_Tran)
                End With

            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub

    Public Function InsertUpdateDelete_InterviewerTran(ByVal dtCurrentDateAndTime As Date, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Try

            If xDataOp IsNot Nothing Then
                objDataOperation = xDataOp
            Else
                objDataOperation = New clsDataOperation
                objDataOperation.BindTransaction()
            End If

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()
                    If Not IsDBNull(.Item("AUD")) Then
                        Select Case .Item("AUD")
                            Case "A"
                                strQ = "INSERT INTO rcbatchschedule_interviewer_tran ( " & _
                                                 "  batchscheduleunkid " & _
                                                 ", vacancyunkid " & _
                                                 ", interviewtypeunkid " & _
                                                 ", interviewer_level " & _
                                                 ", interviewerunkid " & _
                                                 ", otherinterviewer_name " & _
                                                 ", othercompany " & _
                                                 ", otherdepartment " & _
                                                 ", othercontact_no " & _
                                                 ", userunkid " & _
                                                 ", isvoid " & _
                                                 ", voiduserunkid " & _
                                                 ", voiddatetime " & _
                                                 ", voidreason" & _
                                               ") VALUES (" & _
                                                 "  @batchscheduleunkid " & _
                                                 ", @vacancyunkid " & _
                                                 ", @interviewtypeunkid " & _
                                                 ", @interviewer_level " & _
                                                 ", @interviewerunkid " & _
                                                 ", @otherinterviewer_name " & _
                                                 ", @othercompany " & _
                                                 ", @otherdepartment " & _
                                                 ", @othercontact_no " & _
                                                 ", @userunkid " & _
                                                 ", @isvoid " & _
                                                 ", @voiduserunkid " & _
                                                 ", @voiddatetime " & _
                                                 ", @voidreason" & _
                                               "); SELECT @@identity"


                                objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchScheduleUnkid)
                                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("vacancyunkid").ToString)
                                objDataOperation.AddParameter("@interviewtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewtypeunkid").ToString)
                                objDataOperation.AddParameter("@interviewer_level", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewer_level").ToString)
                                objDataOperation.AddParameter("@interviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewerunkid").ToString)
                                objDataOperation.AddParameter("@otherinterviewer_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("otherinterviewer_name").ToString)
                                objDataOperation.AddParameter("@othercompany", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("othercompany").ToString)
                                objDataOperation.AddParameter("@otherdepartment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("otherdepartment").ToString)
                                objDataOperation.AddParameter("@othercontact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("othercontact_no").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)
                                objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isvoid"))
                                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("voiduserunkid").ToString)
                                If .Item("voiddatetime").ToString = Nothing Then
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                                Else
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, .Item("voiddatetime").ToString)
                                End If

                                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("voidreason").ToString)


                                Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "list")


                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                mintBatchScheduleInterviewertranunkid = dsList.Tables(0).Rows(0)(0)

                                If .Item("batchscheduleunkid") > 0 Then
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcbatchschedule_master", "batchscheduleunkid", mintBatchScheduleUnkid, "rcbatchschedule_interviewer_tran", "batchscheduleinterviewertranunkid", mintBatchScheduleInterviewertranunkid, 2, 1) = False Then
                                        Return False
                                    End If
                                Else
                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcbatchschedule_master", "batchscheduleunkid", mintBatchScheduleUnkid, "rcbatchschedule_interviewer_tran", "batchscheduleinterviewertranunkid", mintBatchScheduleInterviewertranunkid, 1, 1) = False Then
                                        Return False
                                    End If
                                End If


                            Case "U"
                                strQ = "UPDATE rcbatchschedule_interviewer_tran SET " & _
                                              "  batchscheduleunkid = @batchscheduleunkid" & _
                                              ", vacancyunkid = @vacancyunkid" & _
                                              ", interviewtypeunkid = @interviewtypeunkid" & _
                                              ", interviewer_level=@interviewer_level " & _
                                              ", interviewerunkid = @interviewerunkid" & _
                                              ", otherinterviewer_name = @otherinterviewer_name" & _
                                              ", othercompany = @othercompany" & _
                                              ", otherdepartment = @otherdepartment" & _
                                              ", othercontact_no = @othercontact_no" & _
                                              ", userunkid = @userunkid" & _
                                       " WHERE batchscheduleinterviewertranunkid = @batchscheduleinterviewertranunkid "

                                objDataOperation.AddParameter("@batchscheduleinterviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("batchscheduleinterviewertranunkid").ToString)
                                objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintBatchScheduleUnkid)
                                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("vacancyunkid").ToString)
                                objDataOperation.AddParameter("@interviewtypeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewtypeunkid").ToString)
                                objDataOperation.AddParameter("@interviewer_level", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewer_level").ToString)
                                objDataOperation.AddParameter("@interviewerunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("interviewerunkid").ToString)
                                objDataOperation.AddParameter("@otherinterviewer_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("otherinterviewer_name").ToString)
                                objDataOperation.AddParameter("@othercompany", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("othercompany").ToString)
                                objDataOperation.AddParameter("@otherdepartment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("otherdepartment").ToString)
                                objDataOperation.AddParameter("@othercontact_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, .Item("othercontact_no").ToString)
                                objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("userunkid").ToString)

                                objDataOperation.ExecNonQuery(strQ)

                                If objDataOperation.ErrorMessage <> "" Then
                                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                    Throw exForce
                                End If


                                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcbatchschedule_master", "batchscheduleunkid", .Item("batchscheduleunkid").ToString, "rcbatchschedule_interviewer_tran", "batchscheduleinterviewertranunkid", .Item("batchscheduleinterviewertranunkid").ToString, 2, 2) = False Then
                                    Return False
                                End If


                            Case "D"

                                If .Item("batchscheduleinterviewertranunkid") > 0 Then

                                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcbatchschedule_master", "batchscheduleunkid", .Item("batchscheduleunkid").ToString, "rcbatchschedule_interviewer_tran", "batchscheduleinterviewertranunkid", .Item("batchscheduleinterviewertranunkid").ToString, 2, 3) = False Then
                                        Return False
                                    End If

                                    strQ = "UPDATE rcbatchschedule_interviewer_tran SET " & _
                                              "  isvoid = 1 " & _
                                              ", voiduserunkid = @voiduserunkid" & _
                                              ", voiddatetime = @voiddatetime" & _
                                              ", voidreason = @voidreason " & _
                                            "WHERE batchscheduleinterviewertranunkid = @batchscheduleinterviewertranunkid  "

                                    objDataOperation.AddParameter("@batchscheduleinterviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("batchscheduleinterviewertranunkid").ToString)
                                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime.ToString)
                                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason.ToString)

                                    Call objDataOperation.ExecNonQuery(strQ)

                                    If objDataOperation.ErrorMessage <> "" Then
                                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                                        Throw exForce
                                    End If
                                End If
                        End Select
                    End If
                End With
            Next
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "InsertUpdateDelete_InterviewerTran", mstrModuleName)
        Finally
            exForce = Nothing
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try

    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcbatchschedule_interviewer_tran) </purpose>
    Public Function VoidAll(ByVal intUnkid As Integer, ByVal blnIsVoid As Boolean, ByVal intVoiduserunkid As Integer, ByVal dtVoidDateTime As DateTime, ByVal strVoidReason As String, Optional ByVal xDataOp As clsDataOperation = Nothing) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp IsNot Nothing Then
            objDataOperation = xDataOp
        Else
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        End If

        Try

            If clsCommonATLog.VoidAtTranAtLog(objDataOperation, "rcbatchschedule_master", "batchscheduleunkid", intUnkid, "rcbatchschedule_interviewer_tran", "batchscheduleinterviewertranunkid", 3, 3) = False Then
                Return False
            End If


            strQ = " UPDATE rcbatchschedule_interviewer_tran SET " & _
                  "  isvoid = @isvoid " & _
                  ", voiduserunkid = @voiduserunkid " & _
                  ", voiddatetime = @voiddatetime  " & _
                  ", voidreason= @voidreason " & _
            "WHERE batchscheduleunkid = @batchscheduleunkid "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.SmallDateTime, eZeeDataType.DATETIME_SIZE, dtVoidDateTime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strVoidReason.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnIsVoid)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function getComboList(Optional ByVal intBatchScheduleId As Integer = -1, _
                                 Optional ByVal blnFlag As Boolean = False, _
                                 Optional ByVal strList As String = "List", _
                                 Optional ByVal isFinalAnalysis As Boolean = False, _
                                 Optional ByVal isAllTrainer As Boolean = False, _
                                 Optional ByVal strFilter As String = "" _
                                 ) As DataSet
        'Hemant (03 Jun 2020) -- [strFilter]
        Dim strQ As String = ""
        Dim strErrorMessage As String = ""
        Dim exForce As Exception
        Dim dsList As New DataSet
        objDataOperation = New clsDataOperation
        Dim intCnt As Integer = 0
        Try
            'Hemant (13 Dec 2019) -- Start
            'ISSUE(MSD) : interview analysis screen doesn't show reviewer info
            'strQ = "SELECT interviewerunkid FROM rcbatchschedule_interviewer_tran WHERE ISNULL(isvoid ,0) = 0 AND rcbatchschedule_interviewer_tran.batchscheduleunkid = @BatchScheduleId"
            strQ = "SELECT interviewer_level FROM rcbatchschedule_interviewer_tran WHERE ISNULL(isvoid ,0) = 0 AND rcbatchschedule_interviewer_tran.batchscheduleunkid = @BatchScheduleId group by interviewer_level "
            'Hemant (13 Dec 2019) -- End
            objDataOperation.AddParameter("@BatchScheduleId", SqlDbType.Int, eZeeDataType.INT_SIZE, intBatchScheduleId)
            intCnt = objDataOperation.RecordCount(strQ)
            'Hemant (03 Jun 2020) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 1 : On the interview analysis screen, they want to record interview scores for all applicants in a single window instead of analysing the interview an applicant at a time. Also, they want to record all the interview scores for all the interviewers on the same screen.)
            strQ = ""
            'Hemant (03 Jun 2020) -- End
            If blnFlag = True Then
                strQ = "SELECT 0 AS Id,@Select AS NAME ,0 AS batchscheduleinterviewertranunkid UNION "
            End If
            strQ &= "SELECT  interviewerunkid AS Id " & _
                            ",CASE WHEN interviewerunkid = -1 " & _
                                    "THEN rcbatchschedule_interviewer_tran.otherinterviewer_name " & _
                                    "WHEN interviewerunkid > 0 " & _
                                    "THEN ISNULL(hremployee_master.firstname ,'') + ' ' " & _
                                        "+ ISNULL(hremployee_master.othername ,'') + ' ' " & _
                                        "+ ISNULL(hremployee_master.surname ,'') " & _
                            "END AS NAME " & _
                            ",batchscheduleinterviewertranunkid " & _
                    "FROM rcbatchschedule_interviewer_tran " & _
                    "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcbatchschedule_interviewer_tran.interviewerunkid " & _
                    "WHERE ISNULL(isvoid ,0) = 0 " & _
                    "AND rcbatchschedule_interviewer_tran.batchscheduleunkid = @BatchScheduleid "

            'Hemant (03 Jun 2020) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 1 : On the interview analysis screen, they want to record interview scores for all applicants in a single window instead of analysing the interview an applicant at a time. Also, they want to record all the interview scores for all the interviewers on the same screen.)
            If strFilter.Trim.Length > 0 Then
                strQ &= strFilter
            End If
            'Hemant (03 Jun 2020) -- End

            If intCnt > 1 Then
                If isAllTrainer = False Then
                    If isFinalAnalysis = False Then
                        strQ &= "AND rcbatchschedule_interviewer_tran.interviewer_level NOT IN (SELECT MAX(rcbatchschedule_interviewer_tran.interviewer_level) FROM rcbatchschedule_interviewer_tran WHERE rcbatchschedule_interviewer_tran.batchscheduleunkid = @BatchScheduleId AND ISNULL(rcbatchschedule_interviewer_tran.isvoid, 0) = 0 ) "
                    Else
                        strQ &= "AND rcbatchschedule_interviewer_tran.interviewer_level IN (SELECT MAX(rcbatchschedule_interviewer_tran.interviewer_level) FROM rcbatchschedule_interviewer_tran WHERE rcbatchschedule_interviewer_tran.batchscheduleunkid = @BatchScheduleid AND ISNULL(rcbatchschedule_interviewer_tran.isvoid, 0) = 0 ) "
                    End If
                End If
            End If

            strQ &= "ORDER BY batchscheduleinterviewertranunkid "

            objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))

            dsList = objDataOperation.ExecQuery(strQ, strList)
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getComboList", mstrModuleName)
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Hemant
    ''' </summary>
    ''' <purpose> To get the data of Interviewer from Table (rcbatchschedule_interviewer_tran) </purpose>
    Public Function GetInterviewerData(ByVal intInterviewertranId As Integer) As DataSet
        Dim strQ As String = ""
        Dim dsList As DataSet
        Dim exForce As Exception

        objDataOperation = New clsDataOperation
        Try
            strQ = "SELECT " & _
                      "rcbatchschedule_interviewer_tran.batchscheduleinterviewertranunkid " & _
                      ", rcbatchschedule_interviewer_tran.batchscheduleunkid " & _
                      ", rcbatchschedule_interviewer_tran.vacancyunkid " & _
                      ", rcbatchschedule_interviewer_tran.interviewtypeunkid " & _
                      ", rcbatchschedule_interviewer_tran.interviewer_level " & _
                      ", rcbatchschedule_interviewer_tran.interviewerunkid " & _
                      ", rcbatchschedule_interviewer_tran.otherinterviewer_name " & _
                      ", rcbatchschedule_interviewer_tran.othercompany " & _
                      ", rcbatchschedule_interviewer_tran.otherdepartment " & _
                      ", rcbatchschedule_interviewer_tran.othercontact_no " & _
                      ", rcbatchschedule_interviewer_tran.userunkid " & _
                      ", rcbatchschedule_interviewer_tran.isvoid " & _
                      ", rcbatchschedule_interviewer_tran.voiduserunkid " & _
                      ", rcbatchschedule_interviewer_tran.voiddatetime " & _
                      ", rcbatchschedule_interviewer_tran.voidreason " & _
                   "FROM rcbatchschedule_interviewer_tran " & _
                   "WHERE batchscheduleinterviewertranunkid = @batchscheduleinterviewertranunkid " & _
                   "AND ISNULL(isvoid,0)=0 "

            objDataOperation.AddParameter("@batchscheduleinterviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intInterviewertranId)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                Return dsList
            Else
                Return Nothing
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetInterviewerData", mstrModuleName)
            Return Nothing
        End Try
    End Function

    Public Function getInterviewTypeCombo(ByVal intBatchScheduleId As Integer, Optional ByVal StrList As String = "List", Optional ByVal blnFlag As Boolean = False) As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            If blnFlag = True Then
                StrQ = "SELECT 0 AS Id ,@Item AS Name UNION "
            End If
            StrQ &= "SELECT " & _
                    "	 cfcommon_master.masterunkid AS Id " & _
                    "	,ISNULL(cfcommon_master.name,'') AS Name " & _
                    "FROM rcbatchschedule_interviewer_tran " & _
                    "	JOIN cfcommon_master ON cfcommon_master.masterunkid = rcbatchschedule_interviewer_tran.interviewtypeunkid " & _
                    "WHERE batchscheduleunkid = @batchscheduleunkid AND isvoid=0 " & _
                    "ORDER BY Id "

            objDataOperation.AddParameter("@Item", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Select"))
            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBatchScheduleId.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, StrList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "getInterviewTypeCombo", mstrModuleName)
            Return Nothing
        End Try
    End Function

    'Hemant (03 Jun 2020) -- Start
    'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS FINAL UAT CHANGES(Point No - 3 : A Interview Score report showing interview candidate(s) and the scores of each interviewer to be developed)
    Public Function isUsed(ByVal intUnkid As Integer, ByVal intBatchScheduleId As Integer, ByVal strOtherInterviewerName As String) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        objDataOperation = New clsDataOperation

        Try
            'Hemant (19 Jun 2020) -- Start
            'strQ = "SELECT 1 FROM rcinterviewanalysis_tran WHERE isvoid = 0 and  interviewertranunkid IN " & _
            '              "( select batchscheduleinterviewertranunkid from rcbatchschedule_interviewer_tran " & _
            '                " where batchscheduleunkid = @batchscheduleunkid and ( interviewerunkid = @interviewertranunkid OR (interviewerunkid = -1 AND otherinterviewer_name = @otherinterviewer_name ) )  )"
            strQ = "SELECT 1 FROM rcinterviewanalysis_tran WHERE isvoid = 0 and  interviewertranunkid IN " & _
                          "( select batchscheduleinterviewertranunkid from rcbatchschedule_interviewer_tran " & _
                           " where batchscheduleunkid = @batchscheduleunkid "

            If intUnkid <= 0 Then
                strQ &= " and  interviewerunkid = -1 AND otherinterviewer_name = @otherinterviewer_name  "
            Else
                strQ &= " and  interviewerunkid = @interviewertranunkid "
            End If
            strQ &= " ) "
            'Hemant (19 Jun 2020) -- End 

            objDataOperation.AddParameter("@interviewertranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            objDataOperation.AddParameter("@batchscheduleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBatchScheduleId)
            objDataOperation.AddParameter("@otherinterviewer_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strOtherInterviewerName)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Hemant (03 Jun 2020) -- End


    

End Class
