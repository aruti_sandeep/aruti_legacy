﻿'************************************************************************************************************************************
'Class Name : clsshortlist_finalapplicant.vb
'Purpose    :
'Date       :12/23/2011
'Written By :Pinkal Jariwala
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
''' <summary>
''' Purpose: 
''' Developer: Pinkal Jariwala
''' </summary>

Public Class clsshortlist_finalapplicant

#Region " Private Variables "

    Private Shared ReadOnly mstrModuleName As String = "clsshortlist_finalapplicant"
    Dim mstrMessage As String = ""
    Private mintShortListUnkid As Integer = -1
    Private mintFilterUnkid As Integer = -1
    Private mintSFApplicantTranUnkid As Integer = -1
    Private mstrReferenceNo As String = String.Empty
    Private mdtTran As DataTable
    Private mblnOnlyShortlistedApplicant As Boolean = False
    Private mintUserunkid As Integer
    Private mblnIsvoid As Boolean
    Private mdtVoiddatetime As DateTime
    Private mstrVoidreason As String = ""
    Private mintVoiduserunkid As Integer

#End Region

#Region " Properties "

    Public Property _ShortListUnkid() As Integer
        Get
            Return mintShortListUnkid
        End Get
        Set(ByVal value As Integer)
            mintShortListUnkid = value
            'Call GetShortListFinalApplicant()
        End Set
    End Property

    Public Property _FilterUnkid() As Integer
        Get
            Return mintFilterUnkid
        End Get
        Set(ByVal value As Integer)
            mintFilterUnkid = value
            'Call GetShortListFinalApplicant()
        End Set
    End Property

    Public Property _DtApplicant() As DataTable
        Get
            Return mdtTran
        End Get
        Set(ByVal value As DataTable)
            mdtTran = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Pinkal Jariwala
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

#End Region

#Region " Contructor "

    Public Sub New()
        Try
            mdtTran = New DataTable("SFApplicant")
            mdtTran.Columns.Add("isChecked", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("finalapplicantunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("shortlistunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("applicantunkid", System.Type.GetType("System.Int32")).DefaultValue = -1
            mdtTran.Columns.Add("isshortlisted", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("isfinalshortlisted", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("ACode", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AName", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("Gender", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("BDate", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("Phone", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("Email", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("GUID", System.Type.GetType("System.String")).DefaultValue = ""


            'Pinkal (12-May-2013) -- Start
            'Enhancement : TRA Changes
            mdtTran.Columns.Add("app_statusid", System.Type.GetType("System.Int32")).DefaultValue = enShortListing_Status.SC_PENDING
            mdtTran.Columns.Add("status", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("app_issent", System.Type.GetType("System.Boolean")).DefaultValue = False
            mdtTran.Columns.Add("refno", System.Type.GetType("System.String")).DefaultValue = ""
            mdtTran.Columns.Add("AUD", System.Type.GetType("System.String")).DefaultValue = ""
            'Pinkal (12-May-2013) -- End

            'S.SANDEEP [08-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
            mdtTran.Columns.Add("submitapproval", System.Type.GetType("System.String")).DefaultValue = ""
            'S.SANDEEP [08-JUN-2018] -- END

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: New; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Private/Public Methods "


    'Pinkal (07-Jan-2012) -- Start
    'Enhancement : TRA Changes

    Public Function GetShortListFinalApplicant(Optional ByVal intShortListUnkid As Integer = -1, _
                                               Optional ByVal intFilterUnkid As Integer = -1, _
                                               Optional ByVal strReferenceNo As String = "", _
                                               Optional ByVal blnOnlyShortlistedApplicant As Boolean = False, _
                                               Optional ByVal intVacancyUnkid As Integer = -1, _
                                               Optional ByVal IsFromInterviewSchedule As Boolean = False, _
                                               Optional ByVal IsIncludeFinalShortListedApplicant As Boolean = False, _
                                               Optional ByVal IsIncludeOnlyApprovedShortListedApplicant As Boolean = False, _
                                               Optional ByVal OnlyApprovedApplicant As Boolean = False, _
                                               Optional ByVal dtAsOnDate As Date = Nothing _
                                               ) As DataTable
        'Sohail (13 Mar 2020) - [dtAsOnDate]
        'S.SANDEEP [ 14 May 2013 (OnlyApprovedApplicant) ] -- START -- END

        Dim StrQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            If dtAsOnDate <> Nothing Then
                StrQ = "SELECT datestranunkid " & _
                             ", employeeunkid " & _
                             ", effectivedate " & _
                             ", date1 " & _
                             ", date2 " & _
                        "INTO #TableSusp " & _
                        "FROM hremployee_dates_tran " & _
                        "WHERE isvoid = 0 " & _
                              "AND datetypeunkid = " & enEmp_Dates_Transaction.DT_SUSPENSION & " " & _
                              "AND 1 = CASE " & _
                                          "WHEN date2 IS NULL THEN " & _
                                              "CASE " & _
                                                  "WHEN CONVERT(CHAR(8), date1) <= @AsOnDate THEN " & _
                                                      "1 " & _
                                                  "ELSE " & _
                                                      "0 " & _
                                              "END " & _
                                          "ELSE " & _
                                              "CASE " & _
                                                  "WHEN @AsOnDate " & _
                                                       "BETWEEN date1 AND date2 THEN " & _
                                                      "1 " & _
                                                  "ELSE " & _
                                                      "0 " & _
                                              "END " & _
                                      "END "
            End If
            'Sohail (13 Mar 2020) -- End

            StrQ &= "SELECT DISTINCT " & _
                    " 0 AS isChecked "

            If IsFromInterviewSchedule = False Then
                StrQ &= ",rcshortlist_finalapplicant.shortlistunkid "
            End If


            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes


            'StrQ &= ",rcshortlist_finalapplicant.applicantunkid " & _
            '        ",rcshortlist_finalapplicant.isshortlisted " & _
            '        ",rcshortlist_finalapplicant.isfinalshortlisted " & _
            '        ",rcshortlist_finalapplicant.userunkid " & _
            '        ",rcshortlist_finalapplicant.isvoid " & _
            '        ",rcshortlist_finalapplicant.voiduserunkid " & _
            '        ",rcshortlist_finalapplicant.voiddatetime " & _
            '        ",rcshortlist_finalapplicant.voidreason " & _
            '        ",rcapplicant_master.applicant_code AS ACode " & _
            '        ",ISNULL(rcapplicant_master.firstname,'')+' '+ISNULL(rcapplicant_master.surname,'') AS AName " & _
            '        ",CASE WHEN gender = 1 THEN @Male " & _
            '        "      WHEN gender = 2 THEN @Female " & _
            '        " END AS Gender " & _
            '        ",ISNULL(CONVERT(CHAR(8),rcapplicant_master.birth_date,112),'') AS BDate " & _
            '        ",ISNULL(rcapplicant_master.present_mobileno,'') AS Phone " & _
            '        ",ISNULL(rcapplicant_master.email,'') AS Email " & _
            '        ",'' AS AUD " & _
            '        "FROM rcshortlist_finalapplicant " & _
            '        "JOIN rcshortlist_master ON rcshortlist_finalapplicant.shortlistunkid = rcshortlist_master.shortlistunkid " & _
            '        "JOIN rcapplicant_master ON rcshortlist_finalapplicant.applicantunkid = rcapplicant_master.applicantunkid " & _
            '        "WHERE 1 = 1 "

            ''''TO PREVENT THE DATA DUPLICATION IN DISPLAY ",rcshortlist_finalapplicant.userunkid " & _
            ''''TO PREVENT THE DATA DUPLICATION IN DISPLAY ",rcshortlist_finalapplicant.isvoid " & _
            ''''TO PREVENT THE DATA DUPLICATION IN DISPLAY ",rcshortlist_finalapplicant.voiduserunkid " & _
            ''''TO PREVENT THE DATA DUPLICATION IN DISPLAY ",rcshortlist_finalapplicant.voiddatetime " & _
            ''''TO PREVENT THE DATA DUPLICATION IN DISPLAY ",rcshortlist_finalapplicant.voidreason " & _

            StrQ &= ",rcshortlist_finalapplicant.applicantunkid " & _
                    ",rcshortlist_finalapplicant.isshortlisted " & _
                    ",rcshortlist_finalapplicant.isfinalshortlisted "

            'Pinkal (24-Sep-2020) -- Start
            'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
            If dtAsOnDate <> Nothing Then
                StrQ &= ", CASE WHEN ISNULL(rcapplicant_master.employeeunkid,0) >  0 THEN ISNULL(hremployee_master.employeecode,'') ELSE rcapplicant_master.applicant_code END AS ACode " & _
                             ",CASE WHEN ISNULL(rcapplicant_master.employeeunkid,0) >  0 THEN ISNULL(hremployee_master.employeecode,'') ELSE rcapplicant_master.applicant_code END + ' - ' + ISNULL(rcapplicant_master.firstname,'')+' '+ISNULL(rcapplicant_master.surname,'') AS refno "
            Else
                StrQ &= ", rcapplicant_master.applicant_code AS ACode " & _
                             ", rcapplicant_master.applicant_code + ' - ' + ISNULL(rcapplicant_master.firstname,'') + ' ' +ISNULL(rcapplicant_master.surname,'') AS refno "
            End If
            'Pinkal (24-Sep-2020) -- End


            StrQ &= ",ISNULL(rcapplicant_master.firstname,'')+' '+ISNULL(rcapplicant_master.surname,'') AS AName " & _
                    ",CASE WHEN rcapplicant_master.gender = 1 THEN @Male " & _
                    "      WHEN rcapplicant_master.gender = 2 THEN @Female " & _
                    " END AS Gender " & _
                    ",ISNULL(CONVERT(CHAR(8),rcapplicant_master.birth_date,112),'') AS BDate " & _
                    ",ISNULL(rcapplicant_master.present_mobileno,'') AS Phone " & _
                    ",ISNULL(rcapplicant_master.email,'') AS Email " & _
                    ",'' AS AUD " & _
                    " ,app_statusid " & _
                    " ,CASE WHEN App_statusid = '" & enShortListing_Status.SC_APPROVED & "' THEN @Approved WHEN App_statusid = '" & enShortListing_Status.SC_PENDING & "' THEN @Pending WHEN App_statusid = '" & enShortListing_Status.SC_REJECT & "' THEN @Reject END AS status " & _
                    ",rcshortlist_finalapplicant.app_issent " & _
                    ",CASE WHEN app_issent = 1 THEN @Y ELSE @N END AS submitapproval " & _
                    "FROM rcshortlist_finalapplicant " & _
                    "JOIN rcshortlist_master ON rcshortlist_finalapplicant.shortlistunkid = rcshortlist_master.shortlistunkid AND rcshortlist_master.isvoid = 0 " & _
                    "JOIN rcapplicant_master ON rcshortlist_finalapplicant.applicantunkid = rcapplicant_master.applicantunkid "


            'Pinkal (12-Nov-2020) -- Enhancement NMB Recruitment UAT-   Working on Recruitement UAT Changes for NMB.[CASE WHEN ISNULL(rcapplicant_master.employeeunkid,0) >  0 THEN ISNULL(hremployee_master.employeecode,'') ELSE rcapplicant_master.applicant_code END AS ACode]

            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            If dtAsOnDate <> Nothing Then

                'Pinkal (24-Sep-2020) -- Start
                'Enhancement NMB - Changes for Recruitment Enhancement required by NMB.
                'StrQ &= "LEFT JOIN hremployee_master ON hremployee_master.employeecode = rcapplicant_master.employeecode " & _
                '             "LEFT JOIN #TableSusp ON hremployee_master.employeeunkid = #TableSusp.employeeunkid "

                StrQ &= "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcapplicant_master.employeeunkid " & _
                        "LEFT JOIN #TableSusp ON hremployee_master.employeeunkid = #TableSusp.employeeunkid "
                'Pinkal (24-Sep-2020) -- End

                
            End If
            'Sohail (13 Mar 2020) -- End

            StrQ &= "WHERE 1 = 1 "


            'S.SANDEEP [08-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
            'CASE WHEN app_issent = 1 THEN @Y ELSE @N END AS submitapproval ------- ADDED
            'S.SANDEEP [08-JUN-2018] -- END


            If intShortListUnkid > 0 Then
                StrQ &= " AND rcshortlist_finalapplicant.shortlistunkid = '" & intShortListUnkid & "'"
            End If


            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            If OnlyApprovedApplicant = True Then
                StrQ &= " AND rcshortlist_finalapplicant.app_statusid = '" & enShortListing_Status.SC_APPROVED & "' "
            End If
            'S.SANDEEP [ 14 May 2013 ] -- END


            'Pinkal (12-May-2013) -- Start
            'Enhancement : TRA Changes

            If IsIncludeFinalShortListedApplicant Then
                StrQ &= " AND rcshortlist_finalapplicant.isfinalshortlisted = 1 "
            End If

            'Pinkal (12-May-2013) -- End

            'Pinkal (12-Oct-2011) -- Start
            'Enhancement : TRA Changes

            'If intFilterUnkid > 0 Then
            '    StrQ &= " AND rcshortlist_finalapplicant.filterunkid = '" & intFilterUnkid & "'"
            'End If

            'Pinkal (12-Oct-2011) -- End

            If strReferenceNo.Trim.Length > 0 Then
                StrQ &= " AND rcshortlist_master.refno = '" & strReferenceNo & "'"
            End If

            'Pinkal (12-May-2013) -- Start
            'Enhancement : TRA Changes

            If IsIncludeFinalShortListedApplicant Then
                StrQ &= " AND rcshortlist_finalapplicant.isfinalshortlisted = 1 "
            End If

            If IsIncludeOnlyApprovedShortListedApplicant Then
                StrQ &= " AND rcshortlist_master.statustypid =  " & enShortListing_Status.SC_APPROVED
            End If

            'Pinkal (12-May-2013) -- End

            If intVacancyUnkid > 0 Then

                'Pinkal (12-Oct-2011) -- Start
                'Enhancement : TRA Changes

                If IsFromInterviewSchedule Then
                    StrQ &= " AND rcshortlist_master.vacancyunkid = '" & intVacancyUnkid & "' AND isfinalshortlisted = 1 "
                Else
                    StrQ &= " AND rcshortlist_master.vacancyunkid = '" & intVacancyUnkid & "'"
                End If
                'Pinkal (12-Oct-2011) -- End


            End If

            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            If dtAsOnDate <> Nothing Then
                StrQ &= " AND #TableSusp.datestranunkid IS NULL "

                StrQ &= " DROP TABLE #TableSusp "

            End If
            'Sohail (13 Mar 2020) -- End

            objDataOperation.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Male"))
            objDataOperation.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Female"))


            'Pinkal (12-May-2013) -- Start
            'Enhancement : TRA Changes
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Disapproved"))
            'Pinkal (12-May-2013) -- End

            'S.SANDEEP [08-JUN-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002313|ARUTI-203}
            objDataOperation.AddParameter("@Y", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 7, "Yes"))
            objDataOperation.AddParameter("@N", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 8, "No"))
            'S.SANDEEP [08-JUN-2018] -- END
            'Sohail (13 Mar 2020) -- Start
            'NMB Enhancement # : System should not shortlist suspended employees on shortlisting and final shortlisting.
            If dtAsOnDate <> Nothing Then
                objDataOperation.AddParameter("@AsOnDate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, eZeeDate.convertDate(dtAsOnDate))
            End If
            'Sohail (13 Mar 2020) -- End

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTran.Rows.Clear()
            For Each dRow As DataRow In dsList.Tables("DataTable").Rows
                If dRow.Item("BDate").ToString.Trim.Length > 0 Then
                    dRow.Item("BDate") = eZeeDate.convertDate(dRow.Item("BDate").ToString).ToShortDateString
                End If
                mdtTran.ImportRow(dRow)
            Next


            Return mdtTran

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetShortListFinalApplicant; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'Pinkal (07-Jan-2012) -- End

    Public Function Insert_ShortListedApplicant(Optional ByVal objDataOperation As clsDataOperation = Nothing) As Boolean
        Dim i As Integer
        Dim strQ As String = ""
        Dim exForce As Exception
        Try

            For i = 0 To mdtTran.Rows.Count - 1
                With mdtTran.Rows(i)
                    objDataOperation.ClearParameters()


                    'Pinkal (12-May-2013) -- Start
                    'Enhancement : TRA Changes

                    'strQ = "INSERT INTO rcshortlist_finalapplicant ( " & _
                    '            "  shortlistunkid " & _
                    '            ", filterunkid " & _
                    '            ", applicantunkid " & _
                    '            ", isshortlisted " & _
                    '            ", isfinalshortlisted " & _
                    '            ", userunkid " & _
                    '            ", isvoid " & _
                    '            ", voiduserunkid " & _
                    '            ", voiddatetime " & _
                    '            ", voidreason" & _
                    '        ") VALUES (" & _
                    '            "  @shortlistunkid " & _
                    '            ", @filterunkid " & _
                    '            ", @applicantunkid " & _
                    '            ", @isshortlisted " & _
                    '            ", @isfinalshortlisted " & _
                    '            ", @userunkid " & _
                    '            ", @isvoid " & _
                    '            ", @voiduserunkid " & _
                    '            ", @voiddatetime " & _
                    '            ", @voidreason" & _
                    '        "); SELECT @@identity"

                    strQ = "INSERT INTO rcshortlist_finalapplicant ( " & _
                                "  shortlistunkid " & _
                                ", filterunkid " & _
                                ", applicantunkid " & _
                                ", isshortlisted " & _
                                ", isfinalshortlisted " & _
                              ", app_statusid " & _
                              ", app_issent " & _
                                ", userunkid " & _
                                ", isvoid " & _
                                ", voiduserunkid " & _
                                ", voiddatetime " & _
                                ", voidreason" & _
                            ") VALUES (" & _
                                "  @shortlistunkid " & _
                                ", @filterunkid " & _
                                ", @applicantunkid " & _
                                ", @isshortlisted " & _
                                ", @isfinalshortlisted " & _
                              ", @app_statusid " & _
                              ", @app_issent " & _
                                ", @userunkid " & _
                                ", @isvoid " & _
                                ", @voiduserunkid " & _
                                ", @voiddatetime " & _
                                ", @voidreason" & _
                            "); SELECT @@identity"

                    'Pinkal (12-May-2013) -- End

                    objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShortListUnkid)
                    objDataOperation.AddParameter("@filterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFilterUnkid)
                    objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("applicantunkid").ToString)
                    objDataOperation.AddParameter("@isshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    objDataOperation.AddParameter("@isfinalshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
                    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)


                    'Pinkal (12-May-2013) -- Start
                    'Enhancement : TRA Changes
                    objDataOperation.AddParameter("@app_statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("app_statusid"))
                    objDataOperation.AddParameter("@app_issent", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("app_issent"))
                    'Pinkal (12-May-2013) -- End


                    Dim dsList As DataSet = objDataOperation.ExecQuery(strQ, "List")

                    If objDataOperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    mintSFApplicantTranUnkid = dsList.Tables(0).Rows(0)(0)

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcshortlist_master", "shortlistunkid", mintShortListUnkid, "rcshortlist_finalapplicant", "finalapplicantunkid", mintSFApplicantTranUnkid, 1, 1) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                    '        Case "U"
                    'strQ = "UPDATE rcshortlist_finalapplicant SET " & _
                    '         "  shortlistunkid = @shortlistunkid" & _
                    '         ", filterunkid = @filterunkid" & _
                    '         ", applicantunkid = @applicantunkid" & _
                    '         ", isshortlisted = @isshortlisted" & _
                    '         ", isfinalshortlisted = @isfinalshortlisted" & _
                    '         ", userunkid = @userunkid" & _
                    '         ", isvoid = @isvoid" & _
                    '         ", voiduserunkid = @voiduserunkid" & _
                    '         ", voiddatetime = @voiddatetime" & _
                    '         ", voidreason = @voidreason " & _
                    '       "WHERE finalapplicantunkid = @finalapplicantunkid "

                    'objDataOperation.AddParameter("@finalapplicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("finalapplicantunkid").ToString)
                    'objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("shortlistunkid").ToString)
                    'objDataOperation.AddParameter("@filterunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("filterunkid").ToString)
                    'objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("applicantunkid").ToString)
                    'objDataOperation.AddParameter("@isshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isshortlisted").ToString)
                    'objDataOperation.AddParameter("@isfinalshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, .Item("isfinalshortlisted").ToString)
                    'objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
                    'objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
                    'objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                    'objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                    'objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

                    'objDataOperation.ExecNonQuery(strQ)

                    'If objDataOperation.ErrorMessage <> "" Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If

                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcshortlist_master", "shortlistunkid", .Item("shortlistunkid"), "rcshortlist_finalapplicant", "finalapplicantunkid", .Item("finalapplicantunkid"), 2, 2) = False Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If

                    'Case "D"

                    '    If .Item("finalapplicantunkid") > 0 Then
                    '        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcshortlist_master", "shortlistunkid", .Item("shortlistunkid"), "rcshortlist_finalapplicant", "finalapplicantunkid", .Item("finalapplicantunkid"), 2, 3) = False Then
                    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '            Throw exForce
                    '        End If
                    '    End If

                    '    strQ = "UPDATE rcshortlist_finalapplicant SET " & _
                    '             "  isvoid = @isvoid" & _
                    '             ", voiduserunkid = @voiduserunkid" & _
                    '             ", voiddatetime = @voiddatetime" & _
                    '             ", voidreason = @voidreason " & _
                    '           "WHERE finalapplicantunkid = @finalapplicantunkid "

                    '    objDataOperation.AddParameter("@finalapplicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, .Item("finalapplicantunkid").ToString)
                    '    objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
                    '    objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                    '    objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, IIf(mdtVoiddatetime <> Nothing, mdtVoiddatetime, DBNull.Value))
                    '    objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)

                    'End Select
                    ' End If
                End With
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateDelete_ShortListedApplicant; Module Name: " & mstrModuleName)
        Finally
        End Try
        Return True
    End Function

    'Public Function Set_FinalApplicant(ByVal intUnkid As Integer) As Boolean
    '    Dim StrQ As String = ""
    '    Dim exForce As Exception
    '    Dim objDataoperation As New clsDataOperation
    '    Try
    '        StrQ &= "UPDATE rcshortlist_finalapplicant SET " & _
    '                        "  isfinalshortlisted = @isfinalshortlisted " & _
    '                        "WHERE finalapplicantunkid = @finalapplicantunkid "

    '        objDataoperation.AddParameter("@isfinalshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
    '        objDataoperation.AddParameter("@finalapplicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

    '        objDataoperation.ExecNonQuery(StrQ)

    '        If objDataoperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataoperation.ErrorNumber & " :  " & objDataoperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        Return True

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: Set_FinalApplicant; Module Name: " & mstrModuleName)
    '    Finally
    '    End Try
    'End Function

    Public Function Set_FinalApplicant(ByVal mdtFinalApplicant As DataTable, Optional ByVal intVacancyId As Integer = -1) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception

        Dim objDataoperation As New clsDataOperation
        objDataoperation.BindTransaction()

        Try


            'Pinkal (12-May-2013) -- Start
            'Enhancement : TRA Changes

            If mdtFinalApplicant Is Nothing OrElse mdtFinalApplicant.Rows.Count <= 0 Then Return True

            Dim drRow() As DataRow = mdtFinalApplicant.Select("isChecked = True AND AUD <> 'D' ")

            If drRow.Length <= 0 Then Return True

            StrQ = "UPDATE rcshortlist_finalapplicant SET " & _
                            "  isfinalshortlisted = @isfinalshortlisted " & _
                       " FROM rcshortlist_finalapplicant " & _
                       " JOIN rcshortlist_master ON rcshortlist_master.shortlistunkid  = rcshortlist_finalapplicant.shortlistunkid " & _
                       " AND rcshortlist_master.isvoid = 0 AND rcshortlist_master.vacancyunkid = @vacancyunkid " & _
                       " WHERE rcshortlist_finalapplicant.applicantunkid = @applicantunkid AND rcshortlist_finalapplicant.isfinalshortlisted = 0 AND rcshortlist_finalapplicant.isvoid = 0  "

            Dim strQuery As String = "Select ISNULL(finalapplicantunkid,0) finalapplicantunkid " & _
                                                 " From rcshortlist_finalapplicant " & _
                                                 " JOIN rcshortlist_master ON rcshortlist_master.shortlistunkid  = rcshortlist_finalapplicant.shortlistunkid " & _
                                                 " AND rcshortlist_master.isvoid = 0 AND rcshortlist_master.vacancyunkid = @vacancyunkid " & _
                                                 " WHERE rcshortlist_finalapplicant.applicantunkid = @applicantunkid AND rcshortlist_finalapplicant.isfinalshortlisted = 1 AND rcshortlist_finalapplicant.isvoid = 0 "


            For Each dtRow As DataRow In drRow

                If dtRow("AUD") = "A" Then

                    objDataoperation.ClearParameters()
            objDataoperation.AddParameter("@isfinalshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                    objDataoperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyId)
                    objDataoperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow("applicantunkid").ToString())
            objDataoperation.ExecNonQuery(StrQ)

            If objDataoperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataoperation.ErrorNumber & " :  " & objDataoperation.ErrorMessage)
                Throw exForce
            End If

                    
                    Dim dsList As DataSet = objDataoperation.ExecQuery(strQuery, "List")
                    If dsList.Tables(0).Rows.Count > 0 Then

                        For Each dr As DataRow In dsList.Tables(0).Rows

                            If clsCommonATLog.Insert_AtLog(objDataoperation, 2, "rcshortlist_finalapplicant", "finalapplicantunkid", CInt(dr("finalapplicantunkid"))) = False Then
                                exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                                Throw exForce
                            End If
                        Next

                    End If

                ElseIf dtRow("AUD") = "D" Then

                    objDataoperation.ClearParameters()
                    objDataoperation.AddParameter("@isfinalshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                    objDataoperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyId)
                    objDataoperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow("applicantunkid").ToString())
                    objDataoperation.ExecNonQuery(StrQ)

                    If objDataoperation.ErrorMessage <> "" Then
                        exForce = New Exception(objDataoperation.ErrorNumber & " :  " & objDataoperation.ErrorMessage)
                        Throw exForce
                    End If


                    Dim dsList As DataSet = objDataoperation.ExecQuery(strQuery, "List")
                    If dsList.Tables(0).Rows.Count > 0 Then

                        For Each dr As DataRow In dsList.Tables(0).Rows

                            If clsCommonATLog.Insert_AtLog(objDataoperation, 2, "rcshortlist_finalapplicant", "finalapplicantunkid", CInt(dr("finalapplicantunkid"))) = False Then
                                exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                                Throw exForce
                            End If
                Next

            End If

                End If

            Next

            'Pinkal (12-May-2013) -- End


            objDataoperation.ReleaseTransaction(True)
            Return True

        Catch ex As Exception
            objDataoperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Set_FinalApplicant; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

       Public Function Delete_ShortListedApplicant(ByVal objDataOperation As clsDataOperation, ByVal intParentAuditType As Integer) As Boolean
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim dsList As DataSet = Nothing
        Try

            strQ = " Select * from rcshortlist_finalapplicant where shortlistunkid = @shortlistunkid"
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShortListUnkid)
            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList IsNot Nothing AndAlso dsList.Tables(0).Rows.Count > 0 Then

                strQ = "UPDATE rcshortlist_finalapplicant SET isvoid = 1,voiduserunkid=@voiduserunkid,voiddatetime=@voiddatetime,voidreason = @voidreason " & _
                          "WHERE shortlistunkid = @shortlistunkid "

                objDataOperation.ClearParameters()
                objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
                objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)
                objDataOperation.AddParameter("@shortlistunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintShortListUnkid)
                objDataOperation.ExecNonQuery(strQ)

                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

                For Each dr As DataRow In dsList.Tables(0).Rows

                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcshortlist_master", "shortlistunkid", mintShortListUnkid, "rcshortlist_finalapplicant", "finalapplicantunkid", CInt(dr("finalapplicantunkid")), intParentAuditType, 3) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If

                Next

            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Delete_ShortListedApplicant; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function


    Public Function GetShortListApplicantList(ByVal intVacancyUnkid As Integer, ByVal IsFinalShortListed As Boolean, Optional ByVal strFilter As String = "") As DataSet
        'Sohail (05 Dec 2016) - [strFilter]
        Dim StrQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            'StrQ = " SELECT DISTINCT  0 AS isChecked " & _
            '           ", ISNULL(rcapplicant_master.firstname, '') + ' ' + ISNULL(rcapplicant_master.surname, '') AS  applicantname " & _
            '           ", CASE WHEN gender = 1 THEN @MALE " & _
            '           "          WHEN gender = 2 THEN @FEMALE " & _
            '           " END AS Gendertype " & _
            '           ", rcvacancy_master.vacancyTitle as vacancy " & _
            '           ", rcapplicant_master.*  " & _
            '           " FROM rcapplicant_master " & _
            '           "  JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
            '           "  JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancy_title AND mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & " " & _
            '           " WHERE 1= 1"



            'S.SANDEEP [ 02 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            'StrQ = "SELECT DISTINCT  0 AS isChecked " & _
            '                       ", ISNULL(rcapplicant_master.firstname, '') + ' ' + ISNULL(rcapplicant_master.surname, '') AS  applicantname " & _
            '                       ", CASE WHEN gender = 1 THEN '@MALE' " & _
            '                                 "WHEN gender = 2 THEN '@FEMALE' " & _
            '                        "END AS Gendertype " & _
            '                       ",rcvacancy_master.vacancyTitle as vacancy " & _
            '                       ",rcapplicant_master.applicantunkid " & _
            '                       ",rcapplicant_master.applicant_code " & _
            '                       ",rcapplicant_master.titleunkid " & _
            '                       ",rcapplicant_master.firstname " & _
            '                       ",rcapplicant_master.surname " & _
            '                       ",rcapplicant_master.othername " & _
            '                       ",rcapplicant_master.gender " & _
            '                       ",rcapplicant_master.email " & _
            '                       ",rcapplicant_master.present_address1 " & _
            '                       ",rcapplicant_master.present_address2 " & _
            '                       ",rcapplicant_master.present_countryunkid " & _
            '                       ",rcapplicant_master.present_stateunkid " & _
            '                       ",rcapplicant_master.present_province " & _
            '                       ",rcapplicant_master.present_post_townunkid " & _
            '                       ",rcapplicant_master.present_zipcode " & _
            '                       ",rcapplicant_master.present_road " & _
            '                       ",rcapplicant_master.present_estate " & _
            '                       ",rcapplicant_master.present_plotno " & _
            '                       ",rcapplicant_master.present_mobileno " & _
            '                       ",rcapplicant_master.present_alternateno " & _
            '                       ",rcapplicant_master.present_tel_no " & _
            '                       ",rcapplicant_master.present_fax " & _
            '                       ",rcapplicant_master.perm_address1 " & _
            '                       ",rcapplicant_master.perm_address2 " & _
            '                       ",rcapplicant_master.perm_countryunkid " & _
            '                       ",rcapplicant_master.perm_stateunkid " & _
            '                       ",rcapplicant_master.perm_province " & _
            '                       ",rcapplicant_master.perm_post_townunkid " & _
            '                       ",rcapplicant_master.perm_zipcode " & _
            '                       ",rcapplicant_master.perm_road " & _
            '                       ",rcapplicant_master.perm_estate " & _
            '                       ",rcapplicant_master.perm_plotno " & _
            '                       ",rcapplicant_master.perm_mobileno " & _
            '                       ",rcapplicant_master.perm_alternateno " & _
            '                       ",rcapplicant_master.perm_tel_no " & _
            '                       ",rcapplicant_master.perm_fax " & _
            '                       ",rcapplicant_master.birth_date " & _
            '                       ",rcapplicant_master.marital_statusunkid " & _
            '                       ",rcapplicant_master.anniversary_date " & _
            '                       ",rcapplicant_master.language1unkid " & _
            '                       ",rcapplicant_master.language2unkid " & _
            '                       ",rcapplicant_master.language3unkid " & _
            '                       ",rcapplicant_master.language4unkid " & _
            '                       ",rcapplicant_master.nationality " & _
            '                       ",rcapplicant_master.userunkid " & _
            '                       ",rcapplicant_master.isimport " & _
            '                       ",rcapplicant_master.other_skills " & _
            '                       ",rcapplicant_master.other_qualification " & _
            '                       ",rcapplicant_master.employeecode " & _
            '                       ",rcapplicant_master.referenceno " & _
            '                       ",rcapp_vacancy_mapping.vacancyunkid " & _
            '                    "FROM rcapplicant_master " & _
            '                         "JOIN rcapp_vacancy_mapping ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid " & _
            '                         "JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
            '                         "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & " " & _
            '                    "WHERE 1= 1 "

            StrQ = "SELECT DISTINCT CAST(0 AS BIT) AS isChecked " & _
                       ", ISNULL(rcapplicant_master.firstname, '') + ' ' + ISNULL(rcapplicant_master.surname, '') AS  applicantname " & _
                       ", CASE WHEN gender = 1 THEN '@MALE' " & _
                                 "WHEN gender = 2 THEN '@FEMALE' " & _
                        "END AS Gendertype " & _
                   ", cfcommon_master.name as vacancy " & _
                       ",rcapplicant_master.applicantunkid " & _
                       ",rcapplicant_master.applicant_code " & _
                       ",rcapplicant_master.titleunkid " & _
                       ",rcapplicant_master.firstname " & _
                       ",rcapplicant_master.surname " & _
                       ",rcapplicant_master.othername " & _
                       ",rcapplicant_master.gender " & _
                       ",rcapplicant_master.email " & _
                       ",rcapplicant_master.present_address1 " & _
                       ",rcapplicant_master.present_address2 " & _
                       ",rcapplicant_master.present_countryunkid " & _
                       ",rcapplicant_master.present_stateunkid " & _
                       ",rcapplicant_master.present_province " & _
                       ",rcapplicant_master.present_post_townunkid " & _
                       ",rcapplicant_master.present_zipcode " & _
                       ",rcapplicant_master.present_road " & _
                       ",rcapplicant_master.present_estate " & _
                       ",rcapplicant_master.present_plotno " & _
                       ",rcapplicant_master.present_mobileno " & _
                       ",rcapplicant_master.present_alternateno " & _
                       ",rcapplicant_master.present_tel_no " & _
                       ",rcapplicant_master.present_fax " & _
                       ",rcapplicant_master.perm_address1 " & _
                       ",rcapplicant_master.perm_address2 " & _
                       ",rcapplicant_master.perm_countryunkid " & _
                       ",rcapplicant_master.perm_stateunkid " & _
                       ",rcapplicant_master.perm_province " & _
                       ",rcapplicant_master.perm_post_townunkid " & _
                       ",rcapplicant_master.perm_zipcode " & _
                       ",rcapplicant_master.perm_road " & _
                       ",rcapplicant_master.perm_estate " & _
                       ",rcapplicant_master.perm_plotno " & _
                       ",rcapplicant_master.perm_mobileno " & _
                       ",rcapplicant_master.perm_alternateno " & _
                       ",rcapplicant_master.perm_tel_no " & _
                       ",rcapplicant_master.perm_fax " & _
                       ",rcapplicant_master.birth_date " & _
                       ",rcapplicant_master.marital_statusunkid " & _
                       ",rcapplicant_master.anniversary_date " & _
                       ",rcapplicant_master.language1unkid " & _
                       ",rcapplicant_master.language2unkid " & _
                       ",rcapplicant_master.language3unkid " & _
                       ",rcapplicant_master.language4unkid " & _
                       ",rcapplicant_master.nationality " & _
                       ",rcapplicant_master.userunkid " & _
                       ",rcapplicant_master.isimport " & _
                       ",rcapplicant_master.other_skills " & _
                       ",rcapplicant_master.other_qualification " & _
                       ",rcapplicant_master.employeecode " & _
                       ",rcapplicant_master.referenceno " & _
                   ", rcapp_vacancy_mapping.vacancyunkid as mapvacancyunkid " & _
                   ", CONVERT(CHAR(8),openingdate, 112) AS ODate " & _
                   ", CONVERT(CHAR(8),closingdate, 112) AS CDate " & _
                       ",rcvacancy_master.isexternalvacancy AS isexternalvacancy " & _
                       ", ISNULL(rcapp_vacancy_mapping.isfromonline, 0) AS isfromonline " & _
                    "FROM rcapplicant_master " & _
                         "JOIN rcapp_vacancy_mapping ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid " & _
                         "JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                         "JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & " " & _
                    "WHERE 1= 1 AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 "
            'Sohail (09 Oct 2018) - [isfromonline, AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0]
            'S.SANDEEP [ 02 MAY 2012 ] -- END

            'S.SANDEEP [ 06 NOV 2012 (rcvacancy_master.isexternalvacancy)] -- START -- END

            'Sohail (05 Dec 2016) -- Start
            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
            If strFilter.Trim <> "" Then
                StrQ &= " " & strFilter
            End If
            'Sohail (05 Dec 2016) -- End


            objDataOperation.ClearParameters()

            If IsFinalShortListed Then
                'Pinkal (24-Aug-2013) -- Start
                'Enhancement : TRA Changes
                'StrQ &= " AND rcapplicant_master.applicantunkid IN (select distinct applicantunkid FROM rcshortlist_finalapplicant WHERE isfinalshortlisted = 1) "
                StrQ &= " AND rcapplicant_master.applicantunkid IN (select distinct rcshortlist_finalapplicant.applicantunkid FROM rcshortlist_finalapplicant JOIN rcshortlist_master ON rcshortlist_finalapplicant.shortlistunkid = rcshortlist_master.shortlistunkid WHERE vacancyunkid =@vacancyunkid AND isfinalshortlisted = 1 AND app_statusid =" & enShortListing_Status.SC_APPROVED & ") "
                'Pinkal (24-Aug-2013) -- End
            Else
                'Pinkal (24-Aug-2013) -- Start
                'Enhancement : TRA Changes
                'StrQ &= " AND rcapplicant_master.applicantunkid NOT IN (select distinct rcshortlist_finalapplicant.applicantunkid FROM rcshortlist_finalapplicant WHERE isfinalshortlisted = 1) "
                StrQ &= " AND rcapplicant_master.applicantunkid NOT IN (select distinct rcshortlist_finalapplicant.applicantunkid FROM rcshortlist_finalapplicant JOIN rcshortlist_master ON rcshortlist_finalapplicant.shortlistunkid = rcshortlist_master.shortlistunkid WHERE vacancyunkid =@vacancyunkid AND isfinalshortlisted = 1) "
                'Pinkal (24-Aug-2013) -- End
            End If

            If intVacancyUnkid > 0 Then
                StrQ &= " AND rcapp_vacancy_mapping.vacancyunkid = @vacancyunkid "
            End If
                objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyUnkid)
            objDataOperation.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Male"))
            objDataOperation.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            'S.SANDEEP [ 02 MAY 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            For Each dRow As DataRow In dsList.Tables("DataTable").Rows
                If dRow.Item("ODate").ToString.Trim.Length > 0 AndAlso dRow.Item("CDate").ToString.Trim.Length > 0 Then
                    'S.SANDEEP [ 04 JULY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'dRow.Item("vacancy") = dRow.Item("vacancy") & " (" & eZeeDate.convertDate(dRow.Item("ODate").ToString).ToShortDateString & " - " & _
                    '                                        eZeeDate.convertDate(dRow.Item("CDate").ToString).ToShortDateString & ")"

                    dRow.Item("vacancy") = dRow.Item("vacancy").ToString.Trim & " (" & eZeeDate.convertDate(dRow.Item("ODate").ToString).ToShortDateString & " - " & _
                                                            eZeeDate.convertDate(dRow.Item("CDate").ToString).ToShortDateString & ")"
                    'S.SANDEEP [ 04 JULY 2012 ] -- END
                End If
            Next
            'S.SANDEEP [ 02 MAY 2012 ] -- END

            Return dsList

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetShortListFinalApplicant; Module Name: " & mstrModuleName)
        Finally
        End Try
    End Function

    'S.SANDEEP [ 14 May 2013 ] -- START
    'ENHANCEMENT : TRA ENHANCEMENT
    Public Function Can_RefNo_Disapproved(ByVal _ShortListIds As String) As Boolean
        Dim blnFlag As Boolean = True
        Dim StrQ As String = "" : Dim iCnt As Integer = 0
        Try
            Dim objDataOperioan As New clsDataOperation

            StrQ = "SELECT * FROM dbo.rcshortlist_finalapplicant WHERE isvoid = 0 AND app_statusid = 1 AND shortlistunkid IN (" & _ShortListIds & ") "

            iCnt = objDataOperioan.RecordCount(StrQ)

            If objDataOperioan.ErrorMessage <> "" Then
                Throw New Exception(objDataOperioan.ErrorNumber & " : " & objDataOperioan.ErrorMessage)
            End If

            If iCnt > 0 Then blnFlag = False

            Return blnFlag

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Can_RefNo_Disapproved", mstrModuleName)
            Return False
        Finally
        End Try
    End Function
    'S.SANDEEP [ 14 May 2013 ] -- END


    'Pinkal (12-May-2013) -- Start
    'Enhancement : TRA Changes

    Public Function GetShortListApplicantForApproval(ByVal intShortListId As Integer, ByVal intApplicantID As Integer) As DataTable
        Dim StrQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception = Nothing
        Dim mdtTable As DataTable = Nothing
        Try

            Dim objDataOperation As New clsDataOperation

            StrQ &= "SELECT DISTINCT " & _
                        " 0 AS isChecked " & _
                        ",rcshortlist_finalapplicant.shortlistunkid " & _
                        ",rcshortlist_finalapplicant.applicantunkid " & _
                        ",rcshortlist_finalapplicant.isshortlisted " & _
                        ",rcshortlist_finalapplicant.isfinalshortlisted " & _
                        ",rcshortlist_finalapplicant.userunkid " & _
                        ",rcshortlist_finalapplicant.isvoid " & _
                        ",rcshortlist_finalapplicant.voiduserunkid " & _
                        ",rcshortlist_finalapplicant.voiddatetime " & _
                        ",rcshortlist_finalapplicant.voidreason " & _
                        ",rcshortlist_master.refno " & _
                        ", cfcommon_master.name + ' ( ' + CONVERT(CHAR(12),openingdate,103) +  ' - ' + CONVERT(CHAR(12),closingdate,103) + ' ) ' as vacancytitle " & _
                        " ,rcshortlist_master.vacancyunkid " & _
                        " ,CONVERT(CHAR(8),rcvacancy_master.openingdate,112) AS openingdate " & _
                        " ,CONVERT(CHAR(8),rcvacancy_master.closingdate,112) AS closingdate " & _
                        " ,ISNULL(rcshortlist_master.remark,'') AS remark " & _
                        ",rcapplicant_master.applicant_code AS ACode " & _
                        ",ISNULL(rcapplicant_master.firstname,'')+' '+ISNULL(rcapplicant_master.surname,'') AS AName " & _
                        ",CASE WHEN gender = 1 THEN @Male " & _
                        "      WHEN gender = 2 THEN @Female " & _
                        " END AS Gender " & _
                        ",CONVERT(CHAR(8),rcapplicant_master.birth_date,112) AS BDate " & _
                        ",ISNULL(rcapplicant_master.present_mobileno,'') AS Phone " & _
                        ",ISNULL(rcapplicant_master.email,'') AS Email " & _
                        " ,app_statusid " & _
                        " ,CASE WHEN App_statusid = '" & enShortListing_Status.SC_APPROVED & "' THEN @Approved WHEN App_statusid = '" & enShortListing_Status.SC_PENDING & "' THEN @Pending WHEN App_statusid = '" & enShortListing_Status.SC_REJECT & "' THEN @Reject END AS status " & _
                        ",rcshortlist_finalapplicant.app_issent " & _
                        "FROM rcshortlist_finalapplicant " & _
                        " JOIN rcshortlist_master ON rcshortlist_finalapplicant.shortlistunkid = rcshortlist_master.shortlistunkid AND rcshortlist_master.isvoid = 0 " & _
                        " JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcshortlist_master.vacancyunkid AND rcvacancy_master.isvoid = 0 " & _
                        " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.isactive = 1 AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & _
                        "JOIN rcapplicant_master ON rcshortlist_finalapplicant.applicantunkid = rcapplicant_master.applicantunkid " & _
                        "WHERE rcshortlist_finalapplicant.isvoid  = 0  AND rcshortlist_finalapplicant.isshortlisted = 1 AND rcshortlist_finalapplicant.isfinalshortlisted = 0"


            If intShortListId > 0 Then
                StrQ &= " AND rcshortlist_finalapplicant.shortlistunkid = @shortlistId"
            End If

            If intApplicantID > 0 Then
                StrQ &= " AND rcshortlist_finalapplicant.applicantunkid = @applicantId"
            End If


            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Male"))
            objDataOperation.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Female"))
            objDataOperation.AddParameter("@Approved", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Approved"))
            objDataOperation.AddParameter("@Pending", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 4, "Pending"))
            objDataOperation.AddParameter("@Reject", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 5, "Disapproved"))
            objDataOperation.AddParameter("@shortlistId", SqlDbType.Int, eZeeDataType.INT_SIZE, intShortListId)
            objDataOperation.AddParameter("@applicantId", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantID)
            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mdtTable = dsList.Tables(0).Clone
            If dsList IsNot Nothing And dsList.Tables(0).Rows.Count > 0 Then
                mdtTable = dsList.Tables(0)
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetShortListApplicantForApproval", mstrModuleName)
        End Try
        Return mdtTable
    End Function

    Private Function Set_Notification_Approvals(ByVal uName As String, _
                                                ByVal mstrVacancyTitle As String, _
                                                ByVal mstrVacancyType As String, _
                                                ByVal iUserId As Integer, _
                                                ByVal iVacTypeId As Integer, _
                                                ByVal iVacancyId As Integer, _
                                                ByVal iShortListId As Integer, _
                                                ByVal iStatusId As Integer, _
                                                Optional ByVal strRemarks As String = "") As String 'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
        'Hemant (02 Jul 2019) -- [strRemarks]
        'Private Function Set_Notification_Approvals(ByVal uName As String, ByVal mstrVacancyTitle As String, ByVal mstrVacancyType As String) As String
        Dim StrMessage As New System.Text.StringBuilder
        Try
            StrMessage.Append("<HTML><BODY>")
            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("Dear <b>" & uName & "</b></span></p>")
            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            'StrMessage.Append(Language.getMessage(mstrModuleName, 7, "Dear") & " <b>" & getTitleCase(uName) & "</b></span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 11, "Dear") & " <b>" & getTitleCase(uName) & "</b></span></p>")
            'Hemant (02 Jul 2019) -- End
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append(vbCrLf)
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This is to notify you that final shortlisting of the Applicants for the vacancy of <b>" & mstrVacancyTitle & "</b> which is an " & mstrVacancyType & " vacancy is complete. </span></p>")
            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            'StrMessage.Append(Language.getMessage(mstrModuleName, 8, "This is to notify you that final shortlisting of the Applicants for the vacancy of") & " <b>" & mstrVacancyTitle & "</b> " & Language.getMessage(mstrModuleName, 9, "which is an") & mstrVacancyType & " " & Language.getMessage(mstrModuleName, 10, "vacancy is complete.") & "</span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 12, "This is to notify you that final shortlisting of the Applicants for the vacancy of") & " <b>" & mstrVacancyTitle & "</b> " & Language.getMessage(mstrModuleName, 9, "which is an") & mstrVacancyType & " " & Language.getMessage(mstrModuleName, 10, "vacancy is complete.") & "</span></p>")
            'Hemant (02 Jul 2019) -- End
            'Gajanan [27-Mar-2019] -- End
            StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            'Hemant (02 Jul 2019) -- Start
            'ENHANCEMENT :  ZRA minimum Requirements
            If strRemarks.Trim.Length > 0 Then
                StrMessage.Append(Language.getMessage(mstrModuleName, 13, "Remarks : ") & " <b>" & strRemarks & "</b> ")
                StrMessage.Append("<p><span style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399; margin-left:0px;margin-right:0px;margin-top:20px;margin-bottom:20px; display:block;'>")
            End If
            'Hemant (02 Jul 2019) -- End
            'S.SANDEEP [ 31 DEC 2013 ] -- START
            'StrMessage.Append("Please login to Aruti Recruitment and <b> Approve/Disapprove </b> the Final Shortlisted Applicants before they get forwarded to the interview process.</span></p>")
            Dim iDataValue As String = Company._Object._Companyunkid & "|" & iUserId & "|" & iVacTypeId & "|" & iVacancyId & "|" & iShortListId & "|" & iStatusId
            'Hemant (15 Nov 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 15 : Provide all recruitment notifications on language. They keep on changing on the contents they want.)
            'StrMessage.Append("Please login to Aruti Recruitment or click below link  <BR> " & _
            '                              ConfigParameter._Object._ArutiSelfServiceURL & "/Recruitment/wPg_FinalApplicant.aspx?" & _
            '                              System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iDataValue)) & _
            '                              "<BR> to <b> Approve/Disapprove </b> the Final Shortlisted Applicants before they get forwarded to the interview process.</span></p>")
            StrMessage.Append(Language.getMessage(mstrModuleName, 14, "Please login to Aruti Recruitment or click below link") & " <BR> " & _
                              ConfigParameter._Object._ArutiSelfServiceURL & "/Recruitment/wPg_FinalApplicant.aspx?" & _
                              System.Web.HttpUtility.UrlEncode(clsCrypto.Encrypt(iDataValue)) & _
                                         "<BR>" & Language.getMessage(mstrModuleName, 15, "to") & " <b>" & Language.getMessage(mstrModuleName, 16, "Approve/Disapprove") & "</b> " & Language.getMessage(mstrModuleName, 17, "the Final Shortlisted Applicants before they get forwarded to the interview process.") & " </span></p>")
            'Hemant (15 Nov 2019) -- End
            'S.SANDEEP [ 31 DEC 2013 ] -- END

            'Gajanan [27-Mar-2019] -- Start
            'Enhancement - Change Email Language
            'StrMessage.Append("<p><span style='padding-top:25px;font-size:10.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px;'><b>&nbsp;&nbsp;&nbsp;&nbsp;&quot;POWERED BY ARUTI HR & PAYROLL MANAGEMENT SOFTWARE.&quot;</b>")
            StrMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
            'Gajanan [27-Mar-2019] -- End

            StrMessage.Append("</span></p>")
            StrMessage.Append("</BODY></HTML>")
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Notification_Approvals", mstrModuleName)
            Return ""
        End Try
        Return StrMessage.ToString
    End Function

    Public Function SendNotification(ByVal mstrVacancyTitle As String, _
                                     ByVal mstrVacancyType As String, _
                                     ByVal iVacTypeId As Integer, _
                                     ByVal iVacancyId As Integer, _
                                     ByVal iShortListId As Integer, _
                                     ByVal iStatusId As Integer, _
                                     ByVal intCompanyUnkId As Integer, _
                                     Optional ByVal strRemarks As String = "") As Boolean
        'Hemant (02 Jul 2019) -- [strRemarks]
        'Sohail (30 Nov 2017) - [intCompanyUnkId]
        'S.SANDEEP [ 31 DEC 2013 ] -- START -- END
        'Public Function SendNotification(ByVal mstrVacancyTitle As String, ByVal mstrVacancyType As String) As Boolean
        Dim mblnFlag As Boolean = False
        Try
            Dim dicUnQEmail As New Dictionary(Of String, String)
            Dim objUsr As New clsUserAddEdit
            Dim StrMessage As String = String.Empty
            Dim dUList As New DataSet
            Dim dtULst As DataTable = Nothing
            dUList = objUsr.Get_UserBy_PrivilegeId(805)  'Allow to Approve Final Shortlisted Applicant
            dtULst = dUList.Tables(0)
            dUList = objUsr.Get_UserBy_PrivilegeId(806)  'Allow to Disapprove Final Shortlisted Applicant
            dtULst.Merge(dUList.Tables(0), True)
            If dtULst.Rows.Count > 0 Then
                For Each dRow As DataRow In dtULst.Rows
                    If dicUnQEmail.ContainsKey(CStr(dRow.Item("UEmail"))) = True Then Continue For
                    dicUnQEmail.Add(CStr(dRow.Item("UEmail")), CStr(dRow.Item("UEmail")))
                    Dim objSMail As New clsSendMail
                    'S.SANDEEP [ 31 DEC 2013 ] -- START
                    'StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), mstrVacancyTitle, mstrVacancyType)
                    StrMessage = Set_Notification_Approvals(CStr(dRow.Item("UName")), mstrVacancyTitle, mstrVacancyType, CInt(dRow.Item("UId")), iVacTypeId, iVacancyId, iShortListId, iStatusId, strRemarks)
                    'Hemant (02 Jul 2019) -- [strRemarks]
                    'S.SANDEEP [ 31 DEC 2013 ] -- END
                    objSMail._ToEmail = CStr(dRow.Item("UEmail"))
                    objSMail._Subject = Language.getMessage(mstrModuleName, 6, "Notifications for Final shortlisted approval")
                    objSMail._Message = StrMessage
                    'Sohail (30 Nov 2017) -- Start
                    'SUMATRA Enhancement – SUMATRA – issue # 0001669: Additional feature for Exchange Server configuration setting in 70.1.
                    'objSMail.SendMail()
                    objSMail.SendMail(intCompanyUnkId)
                    'Sohail (30 Nov 2017) -- End
                    objSMail = Nothing
                Next
                mblnFlag = True
            Else
                mblnFlag = False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SendNotification", mstrModuleName)
        End Try
        Return mblnFlag
    End Function

    Public Function UpdateApplicantForSubmitForApproval(ByVal intvacancyId As Integer, ByVal intApplicantId As Integer) As Boolean
        Dim strQ As String = ""
        Dim objDataOperation As clsDataOperation = Nothing
        Dim exForce As Exception
        Try
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()

            strQ = " UPDATE rcshortlist_finalapplicant SET" & _
                      " app_issent = 1 " & _
                      " FROM rcshortlist_finalapplicant " & _
                      " JOIN rcshortlist_master ON rcshortlist_master.shortlistunkid  = rcshortlist_finalapplicant.shortlistunkid " & _
                      " AND rcshortlist_master.isvoid = 0 AND rcshortlist_master.vacancyunkid = @vacancyunkid " & _
                      " WHERE rcshortlist_finalapplicant.applicantunkid = @applicantunkid AND rcshortlist_finalapplicant.isfinalshortlisted = 1 AND rcshortlist_finalapplicant.isvoid = 0  "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intvacancyId)
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantId)
            objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " :  " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Dim strQuery As String = "Select ISNULL(finalapplicantunkid,0) finalapplicantunkid " & _
                                                 " From rcshortlist_finalapplicant " & _
                                                 " JOIN rcshortlist_master ON rcshortlist_master.shortlistunkid  = rcshortlist_finalapplicant.shortlistunkid " & _
                                                 " AND rcshortlist_master.isvoid = 0 AND rcshortlist_master.vacancyunkid = @vacancyunkid " & _
                                                 " WHERE rcshortlist_finalapplicant.applicantunkid = @applicantunkid AND rcshortlist_finalapplicant.isfinalshortlisted = 1 AND rcshortlist_finalapplicant.isvoid = 0  "

            Dim dsList As DataSet = objDataOperation.ExecQuery(strQuery, "List")

            If dsList.Tables(0).Rows.Count > 0 Then

                For Each dr As DataRow In dsList.Tables(0).Rows

                    If clsCommonATLog.Insert_AtLog(objDataOperation, 2, "rcshortlist_finalapplicant", "finalapplicantunkid", CInt(dr("finalapplicantunkid"))) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Next
            End If

            objDataOperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: UpdateApplicantForSubmitForApproval; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            objDataOperation = Nothing
        End Try
    End Function

    Public Function ApproveFinalApplicant(ByVal intVacancyID As Integer, ByVal mdtFinalApplicant As DataTable) As Boolean
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataoperation As clsDataOperation = Nothing
        Try
            objDataoperation = New clsDataOperation
            objDataoperation.BindTransaction()

            If mdtFinalApplicant Is Nothing OrElse mdtFinalApplicant.Rows.Count <= 0 Then Return True

            Dim drRow() As DataRow = mdtFinalApplicant.Select("isChecked = True AND AUD <> 'D' ")

            If drRow.Length <= 0 Then Return True

            StrQ = "  UPDATE rcshortlist_finalapplicant SET " & _
                       "  app_statusid = @statusid " & _
                       ", approveuserunkid = @approveuserunkid " & _
                       ", appr_date = @approvedate " & _
                       " FROM rcshortlist_finalapplicant " & _
                       " JOIN rcshortlist_master ON rcshortlist_master.shortlistunkid  = rcshortlist_finalapplicant.shortlistunkid " & _
                       " AND rcshortlist_master.isvoid = 0 AND rcshortlist_master.vacancyunkid = @vacancyunkid " & _
                       " WHERE rcshortlist_finalapplicant.applicantunkid = @applicantunkid AND rcshortlist_finalapplicant.isfinalshortlisted = 1 AND rcshortlist_finalapplicant.isvoid = 0  "

            Dim strQuery As String = "Select ISNULL(finalapplicantunkid,0) finalapplicantunkid " & _
                                                 " From rcshortlist_finalapplicant " & _
                                                 " JOIN rcshortlist_master ON rcshortlist_master.shortlistunkid  = rcshortlist_finalapplicant.shortlistunkid " & _
                                                 " AND rcshortlist_master.isvoid = 0 AND rcshortlist_master.vacancyunkid = @vacancyunkid " & _
                                                 " WHERE rcshortlist_finalapplicant.applicantunkid = @applicantunkid AND rcshortlist_finalapplicant.isfinalshortlisted = 1 AND rcshortlist_finalapplicant.isvoid = 0  "

            For Each dtRow As DataRow In drRow

                objDataoperation.ClearParameters()
                objDataoperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, enShortListing_Status.SC_APPROVED)
                objDataoperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyID)
                objDataoperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow("applicantunkid").ToString())
                objDataoperation.AddParameter("@approveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                objDataoperation.AddParameter("@approvedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataoperation.ExecNonQuery(StrQ)

                If objDataoperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataoperation.ErrorNumber & " :  " & objDataoperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dsList As DataSet = objDataoperation.ExecQuery(strQuery, "List")
                If dsList.Tables(0).Rows.Count > 0 Then

                    For Each dr As DataRow In dsList.Tables(0).Rows

                        If clsCommonATLog.Insert_AtLog(objDataoperation, 2, "rcshortlist_finalapplicant", "finalapplicantunkid", CInt(dr("finalapplicantunkid"))) = False Then
                            exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                End If
            Next
            objDataoperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataoperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "ApproveFinalApplicant", mstrModuleName)
            Return False
        End Try
    End Function

    Public Function DisapproveFinalApplicant(ByVal intVancayId As Integer, ByVal mdtFinalApplicant As DataTable) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataoperation As clsDataOperation = Nothing
        Try
            objDataoperation = New clsDataOperation
            objDataoperation.BindTransaction()

            If mdtFinalApplicant Is Nothing OrElse mdtFinalApplicant.Rows.Count <= 0 Then Return True

            Dim drRow() As DataRow = mdtFinalApplicant.Select("isChecked = True AND AUD <> 'D' ")

            If drRow.Length <= 0 Then Return True

            StrQ = "  UPDATE rcshortlist_finalapplicant SET " & _
                       "  app_statusid = @statusid " & _
                       ", approveuserunkid = @approveuserunkid " & _
                       ", appr_date = @approvedate " & _
                       " FROM rcshortlist_finalapplicant " & _
                       " JOIN rcshortlist_master ON rcshortlist_master.shortlistunkid  = rcshortlist_finalapplicant.shortlistunkid " & _
                       " AND rcshortlist_master.isvoid = 0 AND rcshortlist_master.vacancyunkid = @vacancyunkid " & _
                       " WHERE rcshortlist_finalapplicant.applicantunkid = @applicantunkid AND rcshortlist_finalapplicant.isfinalshortlisted = 1 AND rcshortlist_finalapplicant.isvoid = 0  "

            Dim strQuery As String = "Select ISNULL(finalapplicantunkid,0) finalapplicantunkid " & _
                                                 " From rcshortlist_finalapplicant " & _
                                                 " JOIN rcshortlist_master ON rcshortlist_master.shortlistunkid  = rcshortlist_finalapplicant.shortlistunkid " & _
                                                 " AND rcshortlist_master.isvoid = 0 AND rcshortlist_master.vacancyunkid = @vacancyunkid " & _
                                                 " WHERE rcshortlist_finalapplicant.applicantunkid = @applicantunkid AND rcshortlist_finalapplicant.isfinalshortlisted = 1 AND rcshortlist_finalapplicant.isvoid = 0  "


            mstrMessage = ""
            For Each dtRow As DataRow In drRow


                objDataoperation.ClearParameters()
                objDataoperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, enShortListing_Status.SC_REJECT)
                objDataoperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVancayId)
                objDataoperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow("applicantunkid").ToString())
                objDataoperation.AddParameter("@approveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
                objDataoperation.AddParameter("@approvedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataoperation.ExecNonQuery(StrQ)

                If objDataoperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataoperation.ErrorNumber & " :  " & objDataoperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dsList As DataSet = objDataoperation.ExecQuery(strQuery, "List")
                If dsList.Tables(0).Rows.Count > 0 Then

                    For Each dr As DataRow In dsList.Tables(0).Rows

                        If clsCommonATLog.Insert_AtLog(objDataoperation, 2, "rcshortlist_finalapplicant", "finalapplicantunkid", CInt(dr("finalapplicantunkid"))) = False Then
                            exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                End If
            Next
            objDataoperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataoperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "DisapproveFinalApplicant", mstrModuleName)
            Return False
        End Try
    End Function

    Public Function ChangeFinalshortListedApplicant_Status(ByVal intVancayId As Integer, ByVal mdtFinalApplicant As DataTable) As Boolean
        Dim blnFlag As Boolean = False
        Dim StrQ As String = ""
        Dim exForce As Exception
        Dim objDataoperation As clsDataOperation = Nothing
        Try
            objDataoperation = New clsDataOperation
            objDataoperation.BindTransaction()

            If mdtFinalApplicant Is Nothing OrElse mdtFinalApplicant.Rows.Count <= 0 Then Return True

            Dim drRow() As DataRow = mdtFinalApplicant.Select("isChecked = True AND AUD <> 'D' ")

            If drRow.Length <= 0 Then Return True

            StrQ = "  UPDATE rcshortlist_finalapplicant SET " & _
                       "  app_statusid = @statusid " & _
                       ", app_issent = 0 " & _
                       ", isfinalshortlisted  = @isfinalshortlisted " & _
                       ", approveuserunkid = @approveuserunkid " & _
                       ", appr_date = @approvedate " & _
                       " FROM rcshortlist_finalapplicant " & _
                       " JOIN rcshortlist_master ON rcshortlist_master.shortlistunkid  = rcshortlist_finalapplicant.shortlistunkid " & _
                       " AND rcshortlist_master.isvoid = 0 AND rcshortlist_master.vacancyunkid = @vacancyunkid " & _
                       " WHERE rcshortlist_finalapplicant.applicantunkid = @applicantunkid AND rcshortlist_finalapplicant.isfinalshortlisted = 1 AND rcshortlist_finalapplicant.isvoid = 0  "

            Dim strQuery As String = "Select ISNULL(finalapplicantunkid,0) finalapplicantunkid " & _
                                                 "From rcshortlist_finalapplicant " & _
                                                 " JOIN rcshortlist_master ON rcshortlist_master.shortlistunkid  = rcshortlist_finalapplicant.shortlistunkid " & _
                                                 " AND rcshortlist_master.isvoid = 0 AND rcshortlist_master.vacancyunkid = @vacancyunkid " & _
                                                 " WHERE rcshortlist_finalapplicant.applicantunkid = @applicantunkid  AND rcshortlist_finalapplicant.isvoid = 0  "

            mstrMessage = ""
            For Each dtRow As DataRow In drRow
                objDataoperation.ClearParameters()
                objDataoperation.AddParameter("@statusid", SqlDbType.Int, eZeeDataType.INT_SIZE, enShortListing_Status.SC_PENDING)
                objDataoperation.AddParameter("@isfinalshortlisted", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                objDataoperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVancayId)
                objDataoperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, dtRow("applicantunkid").ToString())
                objDataoperation.AddParameter("@approveuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, -1)
                objDataoperation.AddParameter("@approvedate", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
                objDataoperation.ExecNonQuery(StrQ)

                If objDataoperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataoperation.ErrorNumber & " :  " & objDataoperation.ErrorMessage)
                    Throw exForce
                End If

                Dim dsList As DataSet = objDataoperation.ExecQuery(strQuery, "List")

                If objDataoperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataoperation.ErrorNumber & " :  " & objDataoperation.ErrorMessage)
                    Throw exForce
                End If

                If dsList.Tables(0).Rows.Count > 0 Then

                    For Each dr As DataRow In dsList.Tables(0).Rows

                        If clsCommonATLog.Insert_AtLog(objDataoperation, 2, "rcshortlist_finalapplicant", "finalapplicantunkid", CInt(dr("finalapplicantunkid"))) = False Then
                            exForce = New Exception(objDataoperation.ErrorNumber & ": " & objDataoperation.ErrorMessage)
                            Throw exForce
                        End If
                    Next
                End If
            Next
            objDataoperation.ReleaseTransaction(True)
            Return True
        Catch ex As Exception
            objDataoperation.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "ChangeFinalshortListedApplicant_Status", mstrModuleName)
            Return False
        End Try
    End Function
    'Pinkal (12-May-2013) -- End


    'Pinkal (31-Mar-2023) -- Start
    '(A1X-717) NMB - As a user, based on the data displayed from the selection, I want to be able to shoot emails from the Feedback screen instead of going to the mail utility again. I should be able to select the template directly from this screen and input the subject.

    Public Function GetShortListedOnlyApplicantList(ByVal mblnNonShortListed As Boolean, ByVal intVacancyUnkid As Integer, Optional ByVal strFilter As String = "") As DataSet
        Dim StrQ As String = ""
        Dim strErrorMessage As String = ""
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            StrQ = "SELECT DISTINCT CAST(0 AS BIT) AS isChecked " & _
                       ", ISNULL(rcapplicant_master.firstname, '') + ' ' + ISNULL(rcapplicant_master.surname, '') AS  applicantname " & _
                       ", CASE WHEN gender = 1 THEN @MALE WHEN gender = 2 THEN @FEMALE END AS Gendertype " & _
                       ", cfcommon_master.name as vacancy " & _
                       ",rcapplicant_master.applicantunkid " & _
                       ",rcapplicant_master.applicant_code " & _
                       ",rcapplicant_master.titleunkid " & _
                       ",rcapplicant_master.firstname " & _
                       ",rcapplicant_master.surname " & _
                       ",rcapplicant_master.othername " & _
                       ",rcapplicant_master.gender " & _
                       ",rcapplicant_master.email " & _
                       ",rcapplicant_master.present_address1 " & _
                       ",rcapplicant_master.present_address2 " & _
                       ",rcapplicant_master.present_countryunkid " & _
                       ",rcapplicant_master.present_stateunkid " & _
                       ",rcapplicant_master.present_province " & _
                       ",rcapplicant_master.present_post_townunkid " & _
                       ",rcapplicant_master.present_zipcode " & _
                       ",rcapplicant_master.present_road " & _
                       ",rcapplicant_master.present_estate " & _
                       ",rcapplicant_master.present_plotno " & _
                       ",rcapplicant_master.present_mobileno " & _
                       ",rcapplicant_master.present_alternateno " & _
                       ",rcapplicant_master.present_tel_no " & _
                       ",rcapplicant_master.present_fax " & _
                       ",rcapplicant_master.perm_address1 " & _
                       ",rcapplicant_master.perm_address2 " & _
                       ",rcapplicant_master.perm_countryunkid " & _
                       ",rcapplicant_master.perm_stateunkid " & _
                       ",rcapplicant_master.perm_province " & _
                       ",rcapplicant_master.perm_post_townunkid " & _
                       ",rcapplicant_master.perm_zipcode " & _
                       ",rcapplicant_master.perm_road " & _
                       ",rcapplicant_master.perm_estate " & _
                       ",rcapplicant_master.perm_plotno " & _
                       ",rcapplicant_master.perm_mobileno " & _
                       ",rcapplicant_master.perm_alternateno " & _
                       ",rcapplicant_master.perm_tel_no " & _
                       ",rcapplicant_master.perm_fax " & _
                       ",rcapplicant_master.birth_date " & _
                       ",rcapplicant_master.marital_statusunkid " & _
                       ",rcapplicant_master.anniversary_date " & _
                       ",rcapplicant_master.language1unkid " & _
                       ",rcapplicant_master.language2unkid " & _
                       ",rcapplicant_master.language3unkid " & _
                       ",rcapplicant_master.language4unkid " & _
                       ",rcapplicant_master.nationality " & _
                       ",rcapplicant_master.userunkid " & _
                       ",rcapplicant_master.isimport " & _
                       ",rcapplicant_master.other_skills " & _
                       ",rcapplicant_master.other_qualification " & _
                       ",rcapplicant_master.employeecode " & _
                       ",rcapplicant_master.referenceno " & _
                       ", rcapp_vacancy_mapping.vacancyunkid as mapvacancyunkid " & _
                       ",CONVERT(CHAR(8),openingdate, 112) AS ODate " & _
                       ",CONVERT(CHAR(8),closingdate, 112) AS CDate " & _
                       ",rcvacancy_master.isexternalvacancy AS isexternalvacancy " & _
                       ", ISNULL(rcapp_vacancy_mapping.isfromonline, 0) AS isfromonline " & _
                       " FROM rcapplicant_master " & _
                       " JOIN rcapp_vacancy_mapping ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid " & _
                       " JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                       " JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & " "

            If mblnNonShortListed = False Then
                StrQ &= " LEFT JOIN rcshortlist_master ON rcshortlist_master.vacancyunkid = rcapp_vacancy_mapping.vacancyunkid AND rcshortlist_master.isvoid = 0 " & _
                             " LEFT JOIN rcshortlist_finalapplicant ON rcshortlist_master.shortlistunkid = rcshortlist_finalapplicant.shortlistunkid AND rcshortlist_finalapplicant.applicantunkid = rcapp_vacancy_mapping.applicantunkid AND rcshortlist_finalapplicant.isvoid = 0 " & _
                             " WHERE ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 AND ISNULL(rcshortlist_finalapplicant.isvoid, 0) = 0 AND rcshortlist_finalapplicant.isshortlisted = 1 "
            End If


            If intVacancyUnkid > 0 Then
                StrQ &= " AND rcapp_vacancy_mapping.vacancyunkid = @vacancyunkid "
            End If

            If strFilter.Trim <> "" Then
                StrQ &= " " & strFilter
            End If


            If mblnNonShortListed Then

                StrQ &= " AND rcapp_vacancy_mapping.applicantunkid NOT IN " & _
                             " ( " & _
                             "          SELECT DISTINCT " & _
                             "                  rcapplicant_master.applicantunkid " & _
                             "          FROM rcapplicant_master " & _
                             "          LEFT JOIN rcapp_vacancy_mapping ON rcapplicant_master.applicantunkid = rcapp_vacancy_mapping.applicantunkid " & _
                             "          LEFT JOIN rcvacancy_master ON rcapp_vacancy_mapping.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                             "          LEFT JOIN cfcommon_master 	ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND mastertype = " & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & " " & _
                             "          LEFT JOIN rcshortlist_master ON rcshortlist_master.vacancyunkid = rcapp_vacancy_mapping.vacancyunkid AND rcshortlist_master.isvoid = 0 " & _
                             "          LEFT JOIN rcshortlist_finalapplicant ON rcshortlist_master.shortlistunkid = rcshortlist_finalapplicant.shortlistunkid AND rcshortlist_finalapplicant.applicantunkid = rcapp_vacancy_mapping.applicantunkid AND rcshortlist_finalapplicant.isvoid = 0 " & _
                             "          WHERE ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0  AND rcshortlist_finalapplicant.isshortlisted = 1 "

                If intVacancyUnkid > 0 Then
                    StrQ &= " AND rcapp_vacancy_mapping.vacancyunkid = @vacancyunkid "
                End If

                If strFilter.Trim <> "" Then
                    StrQ &= " " & strFilter
                End If

                StrQ &= ")"

            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyUnkid)
            objDataOperation.AddParameter("@MALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 1, "Male"))
            objDataOperation.AddParameter("@FEMALE", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Female"))

            dsList = objDataOperation.ExecQuery(StrQ, "DataTable")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables("DataTable").Rows
                If dRow.Item("ODate").ToString.Trim.Length > 0 AndAlso dRow.Item("CDate").ToString.Trim.Length > 0 Then
                    dRow.Item("vacancy") = dRow.Item("vacancy").ToString.Trim & " (" & eZeeDate.convertDate(dRow.Item("ODate").ToString).ToShortDateString & " - " & _
                                                            eZeeDate.convertDate(dRow.Item("CDate").ToString).ToShortDateString & ")"
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetShortListedOnlyApplicantList; Module Name: " & mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (31-Mar-2023) -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Male")
			Language.setMessage(mstrModuleName, 2, "Female")
			Language.setMessage(mstrModuleName, 3, "Approved")
			Language.setMessage(mstrModuleName, 4, "Pending")
			Language.setMessage(mstrModuleName, 5, "Disapproved")
			Language.setMessage(mstrModuleName, 6, "Notifications for Final shortlisted approval")
			Language.setMessage(mstrModuleName, 7, "Yes")
			Language.setMessage(mstrModuleName, 8, "No")
			Language.setMessage(mstrModuleName, 9, "which is an")
			Language.setMessage(mstrModuleName, 10, "vacancy is complete.")
            Language.setMessage(mstrModuleName, 11, "Dear")
            Language.setMessage(mstrModuleName, 12, "This is to notify you that final shortlisting of the Applicants for the vacancy of")
            Language.setMessage(mstrModuleName, 13, "Remarks :")
			Language.setMessage(mstrModuleName, 14, "Please login to Aruti Recruitment or click below link")
			Language.setMessage(mstrModuleName, 15, "to")
			Language.setMessage(mstrModuleName, 16, "Approve/Disapprove")
			Language.setMessage(mstrModuleName, 17, "the Final Shortlisted Applicants before they get forwarded to the interview process.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class

