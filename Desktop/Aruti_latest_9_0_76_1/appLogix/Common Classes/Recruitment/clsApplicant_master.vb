﻿'************************************************************************************************************************************
'Class Name : clsApplicant_master.vb
'Purpose    :
'Date       :19/08/2010
'Written By :Anjan
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports Aruti.Data.clsCommon_Master
Imports System.ComponentModel 'Sohail (09 Feb 2012)
'S.SANDEEP [ 11 AUG 2012 ] -- START
'ENHANCEMENT : TRA CHANGES
Imports eZee.Common.eZeeForm
'S.SANDEEP [ 11 AUG 2012 ] -- END
Imports Aruti.Data.WebRef
Imports System.Web
Imports System.Threading
Imports System.Net
Imports System.ServiceModel.Web
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports System.Text
Imports System.IO
Imports System.Xml

''' <summary>
''' Purpose: 
''' Developer: Anjan
''' </summary>
Public Class clsApplicant_master
    Private Shared ReadOnly mstrModuleName As String = "clsApplicant_master"
    Dim mstrMessage As String = ""
    Dim objSkill_Tran As New clsApplicantSkill_tran
    Dim objQualify_Tran As New clsApplicantQualification_tran
    Dim objJobHistory As New clsJobhistory
    Dim objImgPath As New clshr_images_tran

    'Sohail (26 Aug 2011) -- Start
    Private mstrServerName As String = ""
    Private mstrMBoardSrNo As String = ""
    Private mstrMachineName As String = ""
    Private mintImportStatus As Integer = 0

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Dim mintCompID As Integer = Company._Object._Companyunkid
    'Shani(24-Aug-2015) -- End

    'Sohail (26 Aug 2011) -- End


    'Pinkal (06-Feb-2012) -- Start
    'Enhancement : TRA Changes
    Dim objReference_Tran As New clsrcapp_reference_Tran
    'Pinkal (06-Feb-2012) -- End
    'Sohail (05 Jun 2020) -- Start
    'NMB Enhancement # : Upon successful application of a vacancy for internal vacancies, system to copy the reporting-to on the reverse email sent back to the employee.
    Private mstrCompanyCode As String = ""
    Private mstrCompanyName As String = ""
    Private mstrDatabaseName As String = ""
    Private mintLoginTypeId As Integer = enLogin_Mode.DESKTOP
    Private trd As Thread
    'Sohail (05 Jun 2020) -- End
    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes
    Private mstrWebFormName As String = String.Empty
    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mintLogEmployeeUnkid As Integer = -1
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    Private m_strLogFile As String = IO.Path.Combine(My.Application.Info.DirectoryPath, "ImportApplicantErrorLog_" & Format(DateAndTime.Now.Date, "yyyyMMdd") & ".txt") 'Sohail (12 Nov 2012)

#Region " Private variables "
    Private mintApplicantunkid As Integer
    Private mstrApplicant_Code As String = String.Empty
    Private mintTitleunkid As Integer
    Private mstrFirstname As String = String.Empty
    Private mstrSurname As String = String.Empty
    Private mstrOthername As String = String.Empty
    'Sandeep [ 27 NOV 2010 ] -- Start
    'Private mstrGender As String = String.Empty
    Private mstrGender As Integer = 0
    'Sandeep [ 27 NOV 2010 ] -- End
    Private mstrEmail As String = String.Empty
    Private mstrPresent_Address1 As String = String.Empty
    Private mstrPresent_Address2 As String = String.Empty
    Private mintPresent_Countryunkid As Integer
    Private mintPresent_Stateunkid As Integer
    Private mstrPresent_Province As String = String.Empty
    Private mintPresent_Post_Townunkid As Integer
    Private mintPresent_Zipcode As Integer
    Private mstrPresent_Road As String = String.Empty
    Private mstrPresent_Estate As String = String.Empty
    Private mstrPresent_Plotno As String = String.Empty
    Private mstrPresent_Mobileno As String = String.Empty
    Private mstrPresent_Alternateno As String = String.Empty
    Private mstrPresent_Tel_No As String = String.Empty
    Private mstrPresent_Fax As String = String.Empty
    Private mstrPerm_Address1 As String = String.Empty
    Private mstrPerm_Address2 As String = String.Empty
    Private mintPerm_Countryunkid As Integer
    Private mintPerm_Stateunkid As Integer
    Private mstrPerm_Province As String = String.Empty
    Private mintPerm_Post_Townunkid As Integer
    Private mintPerm_Zipcode As Integer
    Private mstrPerm_Road As String = String.Empty
    Private mstrPerm_Estate As String = String.Empty
    Private mstrPerm_Plotno As String = String.Empty
    Private mstrPerm_Mobileno As String = String.Empty
    Private mstrPerm_Alternateno As String = String.Empty
    Private mstrPerm_Tel_No As String = String.Empty
    Private mstrPerm_Fax As String = String.Empty
    Private mdtBirth_Date As Date
    Private mintMarital_Statusunkid As Integer
    Private mdtAnniversary_Date As Date
    Private mintLanguage1unkid As Integer
    Private mintLanguage2unkid As Integer
    Private mintLanguage3unkid As Integer
    Private mintLanguage4unkid As Integer
    Private mintNationality As Integer
    Private mintUserunkid As Integer
    Private mstrImagePath As String = String.Empty
    Private mintVacancyunkid As Integer
    'Anjan (21 Jul 2011)-Start
    'Issue : for web extra information.
    Private mstrOtherSkills As String = String.Empty
    Private mstrOtherQualification As String = ""
    'Anjan (21 Jul 2011)-End 


    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private mstrEmployeecode As String = String.Empty
    Private mstrReferenceno As String = String.Empty
    'S.SANDEEP [ 25 DEC 2011 ] -- END

    'Sohail (30 May 2012) -- Start
    'TRA - ENHANCEMENT
    Private mstrMemberships As String = String.Empty
    Private mstrAchievements As String = String.Empty
    Private mstrJournalsResearchPapers As String = String.Empty
    'Sohail (30 May 2012) -- End
    'Sohail (05 Dec 2016) -- Start
    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
    Private mstrUserId As String = String.Empty
    'Sohail (05 Dec 2016) -- End

    'Nilay (13 Apr 2017) -- Start
    Private mstrMotherTongue As String = String.Empty
    Private mblnIsImpaired As Boolean = False
    Private mstrImpairment As String = String.Empty
    Private mdecCurrentSalary As Decimal = 0.0
    Private mdecExpectedSalary As Decimal = 0.0
    'Hemant (09 July 2018) -- Start 
    Private mintCurrentSalaryCurrencyId As Integer = 0
    Private mintExpectedSalaryCurrencyId As Integer = 0
    'Hemant (09 July 2018) -- End
    Private mstrExpectedBenefits As String = String.Empty
    Private mblnWillingToRelocate As Boolean = False
    Private mblnWillingToTravel As Boolean = False
    Private mintNoticePeriodDays As Integer = 0
    'Nilay (13 Apr 2017) -- End
    'Sohail (09 Oct 2018) -- Start
    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
    Private mblnIsFromOnline As Boolean = False
    'Sohail (09 Oct 2018) -- End

    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-833
    Private mstrNidaNumber As String = ""
    Private mstrNidaMobileNum As String = ""
    Private mstrTIN As String = ""
    Private mstrVillage As String = ""
    Private mstrPhoneNumber1 As String = ""
    Private mintResidency As Integer = 0
    Private mstrIndexNo As String = ""
    Private bytApplicantPhoto As Byte() = Nothing
    Private bytApplicantSigtr As Byte() = Nothing
    'S.SANDEEP |04-MAY-2023| -- END

#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Anjan
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    ''' <summary>
    ''' Purpose: Get or Set applicantunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Applicantunkid() As Integer
        Get
            Return mintApplicantunkid
        End Get
        Set(ByVal value As Integer)
            mintApplicantunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set applicant_code
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Applicant_Code() As String
        Get
            Return mstrApplicant_Code
        End Get
        Set(ByVal value As String)
            mstrApplicant_Code = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set titleunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Titleunkid() As Integer
        Get
            Return mintTitleunkid
        End Get
        Set(ByVal value As Integer)
            mintTitleunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set firstname
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Firstname() As String
        Get
            Return mstrFirstname
        End Get
        Set(ByVal value As String)
            mstrFirstname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set surname
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Surname() As String
        Get
            Return mstrSurname
        End Get
        Set(ByVal value As String)
            mstrSurname = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set othername
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Othername() As String
        Get
            Return mstrOthername
        End Get
        Set(ByVal value As String)
            mstrOthername = Value
        End Set
    End Property

    'Sandeep [ 27 NOV 2010 ] -- Start
    ''' <summary>
    ''' Purpose: Get or Set gender
    ''' Modify By: Anjan
    ''' </summary>
    '''Public Property _Gender() As String
    '''    Get
    '''        Return mstrGender
    '''    End Get
    '''    Set(ByVal value As String)
    '''        mstrGender = Value
    '''    End Set
    '''End Property

    Public Property _Gender() As Integer
        Get
            Return mstrGender
        End Get
        Set(ByVal value As Integer)
            mstrGender = value
        End Set
    End Property
    'Sandeep [ 27 NOV 2010 ] -- End

    ''' <summary>
    ''' Purpose: Get or Set email
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Email() As String
        Get
            Return mstrEmail
        End Get
        Set(ByVal value As String)
            mstrEmail = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_address1
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Address1() As String
        Get
            Return mstrPresent_Address1
        End Get
        Set(ByVal value As String)
            mstrPresent_Address1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_address2
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Address2() As String
        Get
            Return mstrPresent_Address2
        End Get
        Set(ByVal value As String)
            mstrPresent_Address2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_countryunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Countryunkid() As Integer
        Get
            Return mintPresent_Countryunkid
        End Get
        Set(ByVal value As Integer)
            mintPresent_Countryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_stateunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Stateunkid() As Integer
        Get
            Return mintPresent_Stateunkid
        End Get
        Set(ByVal value As Integer)
            mintPresent_Stateunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_province
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Province() As String
        Get
            Return mstrPresent_Province
        End Get
        Set(ByVal value As String)
            mstrPresent_Province = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_post_townunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Post_Townunkid() As Integer
        Get
            Return mintPresent_Post_Townunkid
        End Get
        Set(ByVal value As Integer)
            mintPresent_Post_Townunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_zipcode
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_ZipCode() As Integer
        Get
            Return mintPresent_Zipcode
        End Get
        Set(ByVal value As Integer)
            mintPresent_Zipcode = value
        End Set
    End Property


    ''' <summary>
    ''' Purpose: Get or Set present_road
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Road() As String
        Get
            Return mstrPresent_Road
        End Get
        Set(ByVal value As String)
            mstrPresent_Road = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_estate
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Estate() As String
        Get
            Return mstrPresent_Estate
        End Get
        Set(ByVal value As String)
            mstrPresent_Estate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_plotno
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Plotno() As String
        Get
            Return mstrPresent_Plotno
        End Get
        Set(ByVal value As String)
            mstrPresent_Plotno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_mobileno
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Mobileno() As String
        Get
            Return mstrPresent_Mobileno
        End Get
        Set(ByVal value As String)
            mstrPresent_Mobileno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_alternateno
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Alternateno() As String
        Get
            Return mstrPresent_Alternateno
        End Get
        Set(ByVal value As String)
            mstrPresent_Alternateno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_tel_no
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Tel_No() As String
        Get
            Return mstrPresent_Tel_No
        End Get
        Set(ByVal value As String)
            mstrPresent_Tel_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set present_fax
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Present_Fax() As String
        Get
            Return mstrPresent_Fax
        End Get
        Set(ByVal value As String)
            mstrPresent_Fax = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perm_address1
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Address1() As String
        Get
            Return mstrPerm_Address1
        End Get
        Set(ByVal value As String)
            mstrPerm_Address1 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perm_address2
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Address2() As String
        Get
            Return mstrPerm_Address2
        End Get
        Set(ByVal value As String)
            mstrPerm_Address2 = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perm_countryunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Countryunkid() As Integer
        Get
            Return mintPerm_Countryunkid
        End Get
        Set(ByVal value As Integer)
            mintPerm_Countryunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perm_stateunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Stateunkid() As Integer
        Get
            Return mintPerm_Stateunkid
        End Get
        Set(ByVal value As Integer)
            mintPerm_Stateunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perm_province
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Province() As String
        Get
            Return mstrPerm_Province
        End Get
        Set(ByVal value As String)
            mstrPerm_Province = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perm_post_townunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Post_Townunkid() As Integer
        Get
            Return mintPerm_Post_Townunkid
        End Get
        Set(ByVal value As Integer)
            mintPerm_Post_Townunkid = Value
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set prem_zipcode
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_ZipCode() As Integer
        Get
            Return mintPerm_Zipcode
        End Get
        Set(ByVal value As Integer)
            mintPerm_Zipcode = value
        End Set
    End Property
    ''' <summary>
    ''' Purpose: Get or Set perm_road
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Road() As String
        Get
            Return mstrPerm_Road
        End Get
        Set(ByVal value As String)
            mstrPerm_Road = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perm_estate
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Estate() As String
        Get
            Return mstrPerm_Estate
        End Get
        Set(ByVal value As String)
            mstrPerm_Estate = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perm_plotno
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Plotno() As String
        Get
            Return mstrPerm_Plotno
        End Get
        Set(ByVal value As String)
            mstrPerm_Plotno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perm_mobileno
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Mobileno() As String
        Get
            Return mstrPerm_Mobileno
        End Get
        Set(ByVal value As String)
            mstrPerm_Mobileno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perm_alternateno
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Alternateno() As String
        Get
            Return mstrPerm_Alternateno
        End Get
        Set(ByVal value As String)
            mstrPerm_Alternateno = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perm_tel_no
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Tel_No() As String
        Get
            Return mstrPerm_Tel_No
        End Get
        Set(ByVal value As String)
            mstrPerm_Tel_No = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set perm_fax
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Perm_Fax() As String
        Get
            Return mstrPerm_Fax
        End Get
        Set(ByVal value As String)
            mstrPerm_Fax = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set birth_date
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Birth_Date() As Date
        Get
            Return mdtBirth_Date
        End Get
        Set(ByVal value As Date)
            mdtBirth_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set marital_statusunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Marital_Statusunkid() As Integer
        Get
            Return mintMarital_Statusunkid
        End Get
        Set(ByVal value As Integer)
            mintMarital_Statusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set anniversary_date
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Anniversary_Date() As Date
        Get
            Return mdtAnniversary_Date
        End Get
        Set(ByVal value As Date)
            mdtAnniversary_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set language1unkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Language1unkid() As Integer
        Get
            Return mintLanguage1unkid
        End Get
        Set(ByVal value As Integer)
            mintLanguage1unkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set language2unkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Language2unkid() As Integer
        Get
            Return mintLanguage2unkid
        End Get
        Set(ByVal value As Integer)
            mintLanguage2unkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set language3unkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Language3unkid() As Integer
        Get
            Return mintLanguage3unkid
        End Get
        Set(ByVal value As Integer)
            mintLanguage3unkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set language4unkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Language4unkid() As Integer
        Get
            Return mintLanguage4unkid
        End Get
        Set(ByVal value As Integer)
            mintLanguage4unkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set nationality
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Nationality() As Integer
        Get
            Return mintNationality
        End Get
        Set(ByVal value As Integer)
            mintNationality = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    '''<summary>
    ''' Purpose: Get or Set ImagePath
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _ImagePath() As String
        Get
            Return mstrImagePath
        End Get
        Set(ByVal value As String)
            mstrImagePath = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set vacancyunkid
    ''' Modify By: Anjan
    ''' </summary>
    Public Property _Vacancyunkid() As Integer
        Get
            Return mintVacancyunkid
        End Get
        Set(ByVal value As Integer)
            mintVacancyunkid = value

        End Set
    End Property

    'Anjan (21 Jul 2011)-Start
    'Issue : for web extra information.
    Public Property _OtherSkills() As String
        Get
            Return mstrOtherSkills
        End Get
        Set(ByVal value As String)
            mstrOtherSkills = value
        End Set
    End Property
    Public Property _OtherQualifications() As String
        Get
            Return mstrOtherQualification
        End Get
        Set(ByVal value As String)
            mstrOtherQualification = value
        End Set
    End Property
    'Anjan (21 Jul 2011)-End 



    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Purpose: Get or Set employeecode
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Employeecode() As String
        Get
            Return mstrEmployeecode
        End Get
        Set(ByVal value As String)
            mstrEmployeecode = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set referenceno
    ''' Modify By: Sandeep J. Sharma
    ''' </summary>
    Public Property _Referenceno() As String
        Get
            Return mstrReferenceno
        End Get
        Set(ByVal value As String)
            mstrReferenceno = Value
        End Set
    End Property

    'S.SANDEEP [ 25 DEC 2011 ] -- END

    'Sohail (30 May 2012) -- Start
    'TRA - ENHANCEMENT
    Public Property _Memberships() As String
        Get
            Return mstrMemberships
        End Get
        Set(ByVal value As String)
            mstrMemberships = value
        End Set
    End Property

    Public Property _Achievements() As String
        Get
            Return mstrAchievements
        End Get
        Set(ByVal value As String)
            mstrAchievements = value
        End Set
    End Property

    Public Property _JournalsResearchPapers() As String
        Get
            Return mstrJournalsResearchPapers
        End Get
        Set(ByVal value As String)
            mstrJournalsResearchPapers = value
        End Set
    End Property
    'Sohail (30 May 2012) -- End

    'Sohail (05 Dec 2016) -- Start
    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
    Public Property _UserId() As String
        Get
            Return mstrUserId
        End Get
        Set(ByVal value As String)
            mstrUserId = value
        End Set
    End Property
    'Sohail (05 Dec 2016) -- End

    'S.SANDEEP [ 19 JULY 2012 ] -- START
    'Enhancement : TRA Changes

    Public Property _WebFormName() As String
        Get
            Return mstrWebFormName
        End Get
        Set(ByVal value As String)
            mstrWebFormName = value
        End Set
    End Property

    'S.SANDEEP [ 19 JULY 2012 ] -- END

    'Sohail (05 Jun 2020) -- Start
    'NMB Enhancement # : Upon successful application of a vacancy for internal vacancies, system to copy the reporting-to on the reverse email sent back to the employee.
    Public WriteOnly Property _CompanyCode() As String
        Set(ByVal value As String)
            mstrCompanyCode = value
        End Set
    End Property

    Public WriteOnly Property _CompanyName() As String
        Set(ByVal value As String)
            mstrCompanyName = value
        End Set
    End Property


    Public WriteOnly Property _DatabaseName() As String
        Set(ByVal value As String)
            mstrDatabaseName = value
        End Set
    End Property

    Public WriteOnly Property _LoginTypeId() As Integer
        Set(ByVal value As Integer)
            mintLoginTypeId = value
        End Set
    End Property
    'Sohail (05 Jun 2020) -- End

    'S.SANDEEP [ 13 AUG 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Public WriteOnly Property _LoginEmployeeUnkid() As Integer
        Set(ByVal value As Integer)
            mintLogEmployeeUnkid = value
        End Set
    End Property
    'S.SANDEEP [ 13 AUG 2012 ] -- END

    'Nilay (13 Apr 2017) -- Start
    Public Property _MotherTongue() As String
        Get
            Return mstrMotherTongue
        End Get
        Set(ByVal value As String)
            mstrMotherTongue = value
        End Set
    End Property

    Public Property _IsImpaired() As Boolean
        Get
            Return mblnIsImpaired
        End Get
        Set(ByVal value As Boolean)
            mblnIsImpaired = value
        End Set
    End Property

    Public Property _Impairment() As String
        Get
            Return mstrImpairment
        End Get
        Set(ByVal value As String)
            mstrImpairment = value
        End Set
    End Property

    Public Property _CurrentSalary() As Decimal
        Get
            Return mdecCurrentSalary
        End Get
        Set(ByVal value As Decimal)
            mdecCurrentSalary = value
        End Set
    End Property

    Public Property _ExpectedSalary() As Decimal
        Get
            Return mdecExpectedSalary
        End Get
        Set(ByVal value As Decimal)
            mdecExpectedSalary = value
        End Set
    End Property

    'Hemant (09 July 2018) -- Start
    Public Property _CurrentSalaryCurrencyId() As Integer
        Get
            Return mIntCurrentSalaryCurrencyId
        End Get
        Set(ByVal value As Integer)
            mIntCurrentSalaryCurrencyId = value
        End Set
    End Property

    Public Property _ExpectedSalaryCurrencyId() As Integer
        Get
            Return mIntExpectedSalaryCurrencyId
        End Get
        Set(ByVal value As Integer)
            mIntExpectedSalaryCurrencyId = value
        End Set
    End Property
    'Hemant (09 July 2018) -- End

    Public Property _ExpectedBenefits() As String
        Get
            Return mstrExpectedBenefits
        End Get
        Set(ByVal value As String)
            mstrExpectedBenefits = value
        End Set
    End Property

    Public Property _WillingToRelocate() As Boolean
        Get
            Return mblnWillingToRelocate
        End Get
        Set(ByVal value As Boolean)
            mblnWillingToRelocate = value
        End Set
    End Property

    Public Property _WillingToTravel() As Boolean
        Get
            Return mblnWillingToTravel
        End Get
        Set(ByVal value As Boolean)
            mblnWillingToTravel = value
        End Set
    End Property

    Public Property _NoticePeriodDays() As Integer
        Get
            Return mintNoticePeriodDays
        End Get
        Set(ByVal value As Integer)
            mintNoticePeriodDays = value
        End Set
    End Property
    'Nilay (13 Apr 2017) -- End

    'Sohail (09 Oct 2018) -- Start
    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
    Public Property _IsFromOnline() As Boolean
        Get
            Return mblnIsFromOnline
        End Get
        Set(ByVal value As Boolean)
            mblnIsFromOnline = value
        End Set
    End Property
    'Sohail (09 Oct 2018) -- End

    'Sohail (25 Sep 2020) -- Start
    'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
    Private mintEmployeeunkid As Integer
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = value
        End Set
    End Property

    Private mblnIsvoid As Boolean = False
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = value
        End Set
    End Property

    Private mintVoiduserunkid As Integer
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = value
        End Set
    End Property

    Private mdtVoiddatetime As Date
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = value
        End Set
    End Property

    Private mstrVoidreason As String = String.Empty
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = value
        End Set
    End Property
    'Sohail (25 Sep 2020) -- End


    'S.SANDEEP |04-MAY-2023| -- START
    'ISSUE/ENHANCEMENT : A1X-833
    Public Property _NidaNumber() As String
        Get
            Return mstrNidaNumber
        End Get
        Set(ByVal value As String)
            mstrNidaNumber = value
        End Set
    End Property
    Public Property _NidaMobileNum() As String
        Get
            Return mstrNidaMobileNum
        End Get
        Set(ByVal value As String)
            mstrNidaMobileNum = value
        End Set
    End Property
    Public Property _TIN() As String
        Get
            Return mstrTIN
        End Get
        Set(ByVal value As String)
            mstrTIN = value
        End Set
    End Property
    Public Property _Village() As String
        Get
            Return mstrVillage
        End Get
        Set(ByVal value As String)
            mstrVillage = value
        End Set
    End Property
    Public Property _PhoneNumber1() As String
        Get
            Return mstrPhoneNumber1
        End Get
        Set(ByVal value As String)
            mstrPhoneNumber1 = value
        End Set
    End Property
    Public Property _Residency() As Integer
        Get
            Return mintResidency
        End Get
        Set(ByVal value As Integer)
            mintResidency = value
        End Set
    End Property
    Public Property _IndexNo() As String
        Get
            Return mstrIndexNo
        End Get
        Set(ByVal value As String)
            mstrIndexNo = value
        End Set
    End Property
    Public Property _ApplicantPhoto() As Byte()
        Get
            Return bytApplicantPhoto
        End Get
        Set(ByVal value As Byte())
            bytApplicantPhoto = value
        End Set
    End Property
    Public Property _ApplicantSigtr() As Byte()
        Get
            Return bytApplicantSigtr
        End Get
        Set(ByVal value As Byte())
            bytApplicantSigtr = value
        End Set
    End Property    
    'S.SANDEEP |04-MAY-2023| -- END

#End Region

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT " & _
              "  applicantunkid " & _
              ", applicant_code " & _
              ", titleunkid " & _
              ", firstname " & _
              ", surname " & _
              ", othername " & _
              ", gender " & _
              ", email " & _
              ", present_address1 " & _
              ", present_address2 " & _
              ", present_countryunkid " & _
              ", present_stateunkid " & _
              ", present_province " & _
              ", present_post_townunkid " & _
              ", present_zipcode " & _
              ", present_road " & _
              ", present_estate " & _
              ", present_plotno " & _
              ", present_mobileno " & _
              ", present_alternateno " & _
              ", present_tel_no " & _
              ", present_fax " & _
              ", perm_address1 " & _
              ", perm_address2 " & _
              ", perm_countryunkid " & _
              ", perm_stateunkid " & _
              ", perm_province " & _
              ", perm_post_townunkid " & _
              ", perm_zipcode " & _
              ", perm_road " & _
              ", perm_estate " & _
              ", perm_plotno " & _
              ", perm_mobileno " & _
              ", perm_alternateno " & _
              ", perm_tel_no " & _
              ", perm_fax " & _
              ", birth_date " & _
              ", marital_statusunkid " & _
              ", anniversary_date " & _
              ", language1unkid " & _
              ", language2unkid " & _
              ", language3unkid " & _
              ", language4unkid " & _
              ", nationality " & _
              ", userunkid " & _
              ", vacancyunkid "
            'Anjan (21 Jul 2011)-Start
            'Issue : for web extra information.
            strQ &= ", ISNULL(other_skills,'') as other_skills  " & _
                    ", ISNULL(other_qualification,'') as other_qualification "
            'Anjan (21 Jul 2011)-End 

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= ", ISNULL(employeecode,'') AS employeecode  " & _
                        ", ISNULL(referenceno,'') AS referenceno  "
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'Sohail (30 May 2012) -- Start
            'TRA - ENHANCEMENT
            strQ &= ", ISNULL(memberships,'') AS memberships  " & _
                    ", ISNULL(achievements,'') AS achievements  " & _
                    ", ISNULL(journalsresearchpapers,'') AS journalsresearchpapers  " & _
                    ", ISNULL(UserId,'') AS UserId  "
            'Sohail (05 Dec 2016) - [UserId]
            'Sohail (30 May 2012) -- End

            'Nilay (13 Apr 2017) -- Start
            strQ &= ", ISNULL(rcapplicant_master.mother_tongue,'') AS mother_tongue " & _
                    ", rcapplicant_master.isimpaired " & _
                    ", ISNULL(rcapplicant_master.impairment,'') AS impairment " & _
                    ", ISNULL(rcapplicant_master.current_salary,0) AS current_salary " & _
                    ", ISNULL(rcapplicant_master.expected_salary,0) AS expected_salary " & _
                    ", ISNULL(rcapplicant_master.expected_benefits,'') AS expected_benefits " & _
                    ", rcapplicant_master.willing_to_relocate " & _
                    ", rcapplicant_master.willing_to_travel " & _
                    ", ISNULL(rcapplicant_master.notice_period_days, 0) AS notice_period_days "
            'Nilay (13 Apr 2017) -- End
            'Hemant (09 July 2018) -- Start
            'Enhancement : RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)
            strQ &= ", ISNULL(current_salary_currency_id,0) AS current_salary_currency_id  " & _
                   ", ISNULL(expected_salary_currency_id,0) AS expected_salary_currency_id  " & _
                   ", ISNULL(isfromonline,0) AS isfromonline  " & _
                   ", ISNULL(employeeunkid,0) AS employeeunkid  " & _
                   ", ISNULL(loginemployeeunkid,0) AS loginemployeeunkid  " & _
                   ", ISNULL(isvoid, 0) AS isvoid " & _
                   ", ISNULL(voiduserunkid, 0) AS voiduserunkid " & _
                   ", voiddatetime " & _
                   ", ISNULL(voidreason, '') AS voidreason "
            'Sohail (25 Sep 2020) - [employeeunkid, loginemployeeunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
            'Sohail (09 Oct 2018) - [isfromonline]
            'Hemant (09 July 2018) -- End

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-833
            strQ &= ", ISNULL(nin,'') AS nin " & _
                    ", ISNULL(tin,'') AS tin " & _
                    ", ISNULL(ninmobile,'') AS ninmobile " & _
                    ", ISNULL(village,'') AS village " & _
                    ", ISNULL(phonenum,'') AS phonenum " & _
                    ", ISNULL(residency,0) AS residency " & _
                    ", ISNULL(indexno,'') AS indexno " & _
                    ", applicant_photo " & _
                    ", applicant_signature "
            'S.SANDEEP |04-MAY-2023| -- END


            strQ &= "FROM rcapplicant_master " & _
             "WHERE applicantunkid = @applicantunkid "

            objDataOperation.AddParameter("@applicantunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintApplicantUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintapplicantunkid = CInt(dtRow.Item("applicantunkid"))
                mstrapplicant_code = dtRow.Item("applicant_code").ToString
                minttitleunkid = CInt(dtRow.Item("titleunkid"))
                mstrfirstname = dtRow.Item("firstname").ToString
                mstrsurname = dtRow.Item("surname").ToString
                mstrothername = dtRow.Item("othername").ToString
                mstrgender = dtRow.Item("gender").ToString
                mstremail = dtRow.Item("email").ToString
                mstrpresent_address1 = dtRow.Item("present_address1").ToString
                mstrpresent_address2 = dtRow.Item("present_address2").ToString
                mintpresent_countryunkid = CInt(dtRow.Item("present_countryunkid"))
                mintpresent_stateunkid = CInt(dtRow.Item("present_stateunkid"))
                mstrpresent_province = dtRow.Item("present_province").ToString
                mintPresent_Post_Townunkid = CInt(dtRow.Item("present_post_townunkid"))
                mintPresent_Zipcode = CInt(dtRow.Item("present_zipcode"))
                mstrpresent_road = dtRow.Item("present_road").ToString
                mstrpresent_estate = dtRow.Item("present_estate").ToString
                mstrpresent_plotno = dtRow.Item("present_plotno").ToString
                mstrpresent_mobileno = dtRow.Item("present_mobileno").ToString
                mstrpresent_alternateno = dtRow.Item("present_alternateno").ToString
                mstrpresent_tel_no = dtRow.Item("present_tel_no").ToString
                mstrpresent_fax = dtRow.Item("present_fax").ToString
                mstrperm_address1 = dtRow.Item("perm_address1").ToString
                mstrperm_address2 = dtRow.Item("perm_address2").ToString
                mintperm_countryunkid = CInt(dtRow.Item("perm_countryunkid"))
                mintperm_stateunkid = CInt(dtRow.Item("perm_stateunkid"))
                mstrperm_province = dtRow.Item("perm_province").ToString
                mintPerm_Post_Townunkid = CInt(dtRow.Item("perm_post_townunkid"))
                mintPerm_Zipcode = CInt(dtRow.Item("perm_zipcode"))
                mstrperm_road = dtRow.Item("perm_road").ToString
                mstrperm_estate = dtRow.Item("perm_estate").ToString
                mstrperm_plotno = dtRow.Item("perm_plotno").ToString
                mstrperm_mobileno = dtRow.Item("perm_mobileno").ToString
                mstrperm_alternateno = dtRow.Item("perm_alternateno").ToString
                mstrperm_tel_no = dtRow.Item("perm_tel_no").ToString
                mstrPerm_Fax = dtRow.Item("perm_fax").ToString
                If IsDBNull(dtRow.Item("birth_date")) Then
                    mdtBirth_Date = Nothing
                Else
                    mdtBirth_Date = dtRow.Item("birth_date")
                End If
                mintMarital_Statusunkid = CInt(dtRow.Item("marital_statusunkid"))

                If IsDBNull(dtRow.Item("anniversary_date")) Then
                    mdtAnniversary_Date = Nothing
                Else
                    mdtAnniversary_Date = dtRow.Item("anniversary_date")
                End If

                mintlanguage1unkid = CInt(dtRow.Item("language1unkid"))
                mintlanguage2unkid = CInt(dtRow.Item("language2unkid"))
                mintlanguage3unkid = CInt(dtRow.Item("language3unkid"))
                mintlanguage4unkid = CInt(dtRow.Item("language4unkid"))
                mintnationality = CInt(dtRow.Item("nationality"))
                mintUserunkid = CInt(dtRow.Item("userunkid"))
                mintVacancyunkid = CInt(dtRow.Item("vacancyunkid"))

                'Anjan (21 Jul 2011)-Start
                'Issue : for web extra information.
                mstrOtherSkills = dtRow.Item("other_skills").ToString
                mstrOtherQualification = dtRow.Item("other_qualification").ToString
                'Anjan (21 Jul 2011)-End 

                'S.SANDEEP [ 25 DEC 2011 ] -- START
                'ENHANCEMENT : TRA CHANGES
                mstrEmployeecode = dtRow.Item("employeecode").ToString
                mstrReferenceno = dtRow.Item("referenceno").ToString
                'S.SANDEEP [ 25 DEC 2011 ] -- END

                'Sohail (30 May 2012) -- Start
                'TRA - ENHANCEMENT
                mstrMemberships = dtRow.Item("memberships").ToString
                mstrAchievements = dtRow.Item("achievements").ToString
                mstrJournalsResearchPapers = dtRow.Item("journalsresearchpapers").ToString
                'Sohail (30 May 2012) -- End
                'Sohail (05 Dec 2016) -- Start
                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                mstrUserId = dtRow.Item("UserId").ToString
                'Sohail (05 Dec 2016) -- End

                'Nilay (13 Apr 2017) -- Start
                mstrMotherTongue = dtRow.Item("mother_tongue").ToString
                mblnIsImpaired = CBool(dtRow.Item("isimpaired"))
                mstrImpairment = dtRow.Item("impairment").ToString
                mdecCurrentSalary = CDec(dtRow.Item("current_salary"))
                mdecExpectedSalary = CDec(dtRow.Item("expected_salary"))
                mstrExpectedBenefits = dtRow.Item("expected_benefits").ToString
                mblnWillingToRelocate = CBool(dtRow.Item("willing_to_relocate"))
                mblnWillingToTravel = CBool(dtRow.Item("willing_to_travel"))
                mintNoticePeriodDays = CInt(dtRow.Item("notice_period_days"))
                'Nilay (13 Apr 2017) -- End

                'Hemant (09 July 2018) -- Start
                'Enhancement : RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)
                mIntCurrentSalaryCurrencyId = CInt(dtRow.Item("current_salary_currency_id"))
                mIntExpectedSalaryCurrencyId = CInt(dtRow.Item("expected_salary_currency_id"))
                'Hemant (09 July 2018) -- End

                'Sohail (09 Oct 2018) -- Start
                'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                mblnIsFromOnline = CBool(dtRow.Item("isfromonline"))
                'Sohail (09 Oct 2018) -- End

                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintLogEmployeeUnkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnIsvoid = dtRow.Item("isvoid")
                mintVoiduserunkid = dtRow.Item("voiduserunkid")
                If IsDBNull(dtRow.Item("voiddatetime")) Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrVoidreason = dtRow.Item("voidreason")
                'Sohail (25 Sep 2020) -- End

                'S.SANDEEP |04-MAY-2023| -- START
                'ISSUE/ENHANCEMENT : A1X-833
                mstrNidaNumber = CStr(dtRow.Item("nin"))
                mstrNidaMobileNum = CStr(dtRow.Item("tin"))
                mstrTIN = CStr(dtRow.Item("ninmobile"))
                mstrVillage = CStr(dtRow.Item("village"))
                mstrPhoneNumber1 = CStr(dtRow.Item("phonenum"))
                mintResidency = CInt(dtRow.Item("residency"))
                mstrIndexNo = CStr(dtRow.Item("indexno"))
                If IsDBNull(dtRow.Item("applicant_photo")) = False Then
                    bytApplicantPhoto = DirectCast(dtRow.Item("applicant_photo"), Byte())
                End If
                If IsDBNull(dtRow.Item("applicant_signature")) = False Then
                    bytApplicantSigtr = DirectCast(dtRow.Item("applicant_signature"), Byte())
                End If                
                'S.SANDEEP |04-MAY-2023| -- END

                Exit For
            Next

            objImgPath._Referenceid = enImg_Email_RefId.Applicant_module
            objImgPath._Transactionid = mintApplicantunkid
            objImgPath._Employeeunkid = mintApplicantunkid


            mstrImagePath = objImgPath._Imagename

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Sub



    'S.SANDEEP [ 28 FEB 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    '''' <summary>
    '''' Modify By: Anjan
    '''' </summary>
    '''' <purpose> Assign all Property variable </purpose>
    'Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = False, Optional ByVal IsForImport As Boolean = False) As DataSet
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    objDataOperation = New clsDataOperation

    '    Try

    '        'Pinkal (10-Mar-2011) -- Start


    '        'strQ = "SELECT " & _
    '        '          "  applicantunkid " & _
    '        '          ", applicant_code " & _
    '        '          ", titleunkid " & _
    '        '          ", firstname " & _
    '        '          ", surname " & _
    '        '          ", othername " & _
    '        '          ", firstname +' '+ surname AS applicantname " & _
    '        '          ", gender " & _
    '        '          ", email " & _
    '        '          ", present_address1 " & _
    '        '          ", present_address2 " & _
    '        '          ", present_countryunkid " & _
    '        '          ", present_stateunkid " & _
    '        '          ", present_province " & _
    '        '          ", present_post_townunkid " & _
    '        '          ", present_zipcode " & _
    '        '          ", present_road " & _
    '        '          ", present_estate " & _
    '        '          ", present_plotno " & _
    '        '          ", present_mobileno " & _
    '        '          ", present_alternateno " & _
    '        '          ", present_tel_no " & _
    '        '          ", present_fax " & _
    '        '          ", perm_address1 " & _
    '        '          ", perm_address2 " & _
    '        '          ", perm_countryunkid " & _
    '        '          ", perm_stateunkid " & _
    '        '          ", perm_province " & _
    '        '          ", perm_post_townunkid " & _
    '        '          ", perm_zipcode " & _
    '        '          ", perm_road " & _
    '        '          ", perm_estate " & _
    '        '          ", perm_plotno " & _
    '        '          ", perm_mobileno " & _
    '        '          ", perm_alternateno " & _
    '        '          ", perm_tel_no " & _
    '        '          ", perm_fax " & _
    '        '          ", birth_date " & _
    '        '          ", marital_statusunkid " & _
    '        '          ", anniversary_date " & _
    '        '          ", language1unkid " & _
    '        '          ", language2unkid " & _
    '        '          ", language3unkid " & _
    '        '          ", language4unkid " & _
    '        '          ", nationality " & _
    '        '          ", userunkid " & _
    '        '          ", vacancyunkid " & _
    '        '          ", isimport " & _
    '        '        "FROM rcapplicant_master " ' Sohail (03 Nov 2010) 'isimport field added.



    '        'Pinkal (12-Oct-2011) -- Start
    '        'Enhancement : TRA Changes

            'strQ = "SELECT " & _
            '          "  applicantunkid " & _
            '          ", applicant_code " & _
            '          ", titleunkid " & _
            '          ", firstname " & _
            '          ", surname " & _
            '          ", othername " & _
            '          ", firstname +' '+ surname AS applicantname " & _
            '          ", gender " & _
    '                  ", CASE WHEN gender = 1 then @male  " & _
    '                  "           WHEN gender = 2 then @female " & _
    '                  "           WHEN gender = 3 then @other " & _
    '                  "           ELSE  ''  " & _
    '                  " END as gendertype " & _
            '          ", email " & _
            '          ", present_address1 " & _
            '          ", present_address2 " & _
            '          ", present_countryunkid " & _
            '          ", present_stateunkid " & _
            '          ", present_province " & _
            '          ", present_post_townunkid " & _
            '          ", present_zipcode " & _
            '          ", present_road " & _
            '          ", present_estate " & _
            '          ", present_plotno " & _
            '          ", present_mobileno " & _
            '          ", present_alternateno " & _
            '          ", present_tel_no " & _
            '          ", present_fax " & _
            '          ", perm_address1 " & _
            '          ", perm_address2 " & _
            '          ", perm_countryunkid " & _
            '          ", perm_stateunkid " & _
            '          ", perm_province " & _
            '          ", perm_post_townunkid " & _
            '          ", perm_zipcode " & _
            '          ", perm_road " & _
            '          ", perm_estate " & _
            '          ", perm_plotno " & _
            '          ", perm_mobileno " & _
            '          ", perm_alternateno " & _
            '          ", perm_tel_no " & _
            '          ", perm_fax " & _
            '          ", birth_date " & _
            '          ", marital_statusunkid " & _
            '          ", anniversary_date " & _
            '          ", language1unkid " & _
            '          ", language2unkid " & _
            '          ", language3unkid " & _
            '          ", language4unkid " & _
            '          ", nationality " & _
    '                  ", rcapplicant_master.userunkid " & _
    '                  ", rcapplicant_master.vacancyunkid " & _
            '          ", isimport " & _
    '                  ", rcvacancy_master.vacancytitle as vacancy  "

    '        'Anjan (21 Jul 2011)-Start
    '        'purpose : for web extra information.
    '        strQ &= ", ISNULL(other_skills,'') as other_skills " & _
    '                ", ISNULL(other_qualification,'') as other_qualification "
    '        'Anjan (21 Jul 2011)-End 

    '        'S.SANDEEP [ 25 DEC 2011 ] -- START
    '        'ENHANCEMENT : TRA CHANGES
    '        strQ &= ", ISNULL(employeecode,'') AS employeecode " & _
    '                    ", ISNULL(referenceno,'') AS referenceno  "
    '        'S.SANDEEP [ 25 DEC 2011 ] -- END

    '        If IsForImport Then
    '            strQ &= ", ttype.name as title " & _
    '                        ", pct.country_name as present_country " & _
    '                        ", pst.name as present_state " & _
    '                        ", pcy.name as present_posttown " & _
    '                        ", pz.zipcode_no as present_postcode " & _
    '                        ", pect.country_name as permenant_country " & _
    '                        ", pest.name as permenant_state " & _
    '                        ", pecy.name as permenant_posttown " & _
    '                        ", pez.zipcode_no as permenant_postcode " & _
    '                        ", mtype.name as maritalstatus " & _
    '                        ", l1type.name as language1 " & _
    '                        ", l2type.name as language2 " & _
    '                        ", l3type.name as language3 " & _
    '                        ", l4type.name as language4 " & _
    '                        ",  nct.country_name as nationality "

    '        End If

    '        strQ &= " FROM rcapplicant_master " & _
    '                    " LEFT JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid = rcapplicant_master.vacancyunkid "

    '        If IsForImport Then
    '            strQ &= " LEFT JOIN cfcommon_master as ttype ON ttype.masterunkid = rcapplicant_master.titleunkid AND ttype.mastertype = " & enCommonMaster.TITLE & _
    '                    " LEFT JOIN hrmsConfiguration..cfcountry_master as pct ON pct.countryunkid = rcapplicant_master.present_countryunkid " & _
    '                    " LEFT JOIN hrmsConfiguration..cfstate_master as pst on pst.stateunkid = rcapplicant_master.present_stateunkid " & _
    '                    " LEFT JOIN hrmsConfiguration..cfcity_master as pcy on pcy.cityunkid  = rcapplicant_master.present_post_townunkid " & _
    '                    " LEFT JOIN hrmsConfiguration..cfzipcode_master as pz ON pz.zipcodeunkid = rcapplicant_master.present_zipcode " & _
    '                    " LEFT JOIN hrmsConfiguration..cfcountry_master as pect ON pect.countryunkid = rcapplicant_master.perm_countryunkid " & _
    '                    " LEFT JOIN hrmsConfiguration..cfstate_master as pest on pest.stateunkid = rcapplicant_master.perm_stateunkid " & _
    '                    " LEFT JOIN hrmsConfiguration..cfcity_master as pecy on pecy.cityunkid  = rcapplicant_master.perm_post_townunkid " & _
    '                    " LEFT JOIN hrmsConfiguration..cfzipcode_master as pez ON pez.zipcodeunkid = rcapplicant_master.perm_zipcode " & _
    '                    " LEFT JOIN cfcommon_master as mtype ON mtype.masterunkid = rcapplicant_master.marital_statusunkid AND mtype.mastertype = " & enCommonMaster.MARRIED_STATUS & _
    '                    " LEFT JOIN cfcommon_master as l1type ON l1type.masterunkid = rcapplicant_master.language1unkid AND l1type.mastertype = " & enCommonMaster.LANGUAGES & _
    '                    " LEFT JOIN cfcommon_master as l2type ON l2type.masterunkid = rcapplicant_master.language2unkid AND l2type.mastertype = " & enCommonMaster.LANGUAGES & _
    '                    " LEFT JOIN cfcommon_master as l3type ON l3type.masterunkid = rcapplicant_master.language3unkid AND l3type.mastertype = " & enCommonMaster.LANGUAGES & _
    '                    " LEFT JOIN cfcommon_master as l4type ON l4type.masterunkid = rcapplicant_master.language4unkid AND l4type.mastertype = " & enCommonMaster.LANGUAGES & _
    '                    " LEFT JOIN hrmsConfiguration..cfcountry_master as nct ON nct.countryunkid = rcapplicant_master.nationality "

    '        End If

    '        'Pinkal (12-Oct-2011) -- End

    '        objDataOperation.AddParameter("@male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 5, "Male"))
    '        objDataOperation.AddParameter("@female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 6, "Female"))
    '        objDataOperation.AddParameter("@other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 7, "Other"))

    '        'Pinkal (10-Mar-2011) -- End

    '        dsList = objDataOperation.ExecQuery(strQ, strTableName)

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    '    Return dsList
    'End Function
    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal strTableName As String, Optional ByVal blnOnlyActive As Boolean = False, Optional ByVal IsForImport As Boolean = False, Optional ByVal strFilter As String = "") As DataSet
        'Sohail (05 Dec 2016) - [strFilter]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            'strQ = "SELECT " & _
            '        "    rcapplicant_master.applicantunkid " & _
            '        "   ,rcapplicant_master.applicant_code " & _
            '        "   ,rcapplicant_master.titleunkid " & _
            '        "   ,rcapplicant_master.firstname " & _
            '        "   ,rcapplicant_master.surname " & _
            '        "   ,rcapplicant_master.othername " & _
            '        "   ,rcapplicant_master.firstname + ' ' + ISNULL(rcapplicant_master.othername,'')+' '+ rcapplicant_master.surname AS applicantname " & _
            '        "   ,rcapplicant_master.gender " & _
            '        "   ,CASE WHEN rcapplicant_master.gender = 1 THEN @male " & _
            '        "         WHEN rcapplicant_master.gender = 2 THEN @female " & _
            '        "         WHEN rcapplicant_master.gender = 3 THEN @other " & _
            '        "         ELSE '' " & _
            '        "    END AS gendertype " & _
            '        "   ,rcapplicant_master.email " & _
            '        "   ,rcapplicant_master.present_address1 " & _
            '        "   ,rcapplicant_master.present_address2 " & _
            '        "   ,rcapplicant_master.present_countryunkid " & _
            '        "   ,rcapplicant_master.present_stateunkid " & _
            '        "   ,rcapplicant_master.present_province " & _
            '        "   ,rcapplicant_master.present_post_townunkid " & _
            '        "   ,rcapplicant_master.present_zipcode " & _
            '        "   ,rcapplicant_master.present_road " & _
            '        "   ,rcapplicant_master.present_estate " & _
            '        "   ,rcapplicant_master.present_plotno " & _
            '        "   ,rcapplicant_master.present_mobileno " & _
            '        "   ,rcapplicant_master.present_alternateno " & _
            '        "   ,rcapplicant_master.present_tel_no " & _
            '        "   ,rcapplicant_master.present_fax " & _
            '        "   ,rcapplicant_master.perm_address1 " & _
            '        "   ,rcapplicant_master.perm_address2 " & _
            '        "   ,rcapplicant_master.perm_countryunkid " & _
            '        "   ,rcapplicant_master.perm_stateunkid " & _
            '        "   ,rcapplicant_master.perm_province " & _
            '        "   ,rcapplicant_master.perm_post_townunkid " & _
            '        "   ,rcapplicant_master.perm_zipcode " & _
            '        "   ,rcapplicant_master.perm_road " & _
            '        "   ,rcapplicant_master.perm_estate " & _
            '        "   ,rcapplicant_master.perm_plotno " & _
            '        "   ,rcapplicant_master.perm_mobileno " & _
            '        "   ,rcapplicant_master.perm_alternateno " & _
            '        "   ,rcapplicant_master.perm_tel_no " & _
            '        "   ,rcapplicant_master.perm_fax " & _
            '        "   ,rcapplicant_master.birth_date " & _
            '        "   ,rcapplicant_master.marital_statusunkid " & _
            '        "   ,rcapplicant_master.anniversary_date " & _
            '        "   ,rcapplicant_master.language1unkid " & _
            '        "   ,rcapplicant_master.language2unkid " & _
            '        "   ,rcapplicant_master.language3unkid " & _
            '        "   ,rcapplicant_master.language4unkid " & _
            '        "   ,rcapplicant_master.nationality " & _
            '        "   ,rcapplicant_master.userunkid " & _
            '        "   ,rcapplicant_master.vacancyunkid " & _
            '        "   ,rcapplicant_master.isimport " & _
            '        "   ,ISNULL(employeecode, '') AS employeecode " & _
            '        "   ,ISNULL(referenceno, '') AS referenceno " & _
            '        "   ,ISNULL(Vac.vacancyunkid,0) AS vacancyunkid " & _
            '        "   ,ISNULL(Vac.vacancy,'') AS vacancy " & _
            '        "   ,ISNULL(Vac.ODate,'') AS ODate " & _
            '        "   ,ISNULL(Vac.CDate,'') AS CDate " & _
            '        "FROM rcapplicant_master " & _
            '        "LEFT JOIN " & _
            '        "( " & _
            '        "	SELECT " & _
            '        "		 applicantunkid " & _
            '        "		,CVac.vacancyunkid " & _
            '        "		,cfcommon_master.name AS vacancy " & _
            '        "		,CONVERT(CHAR(8),openingdate,112) AS ODate " & _
            '        "		,CONVERT(CHAR(8),closingdate,112) AS CDate " & _
            '        "	FROM " & _
            '        "	( " & _
            '        "		SELECT " & _
            '        "			 applicantunkid " & _
            '        "			,vacancyunkid " & _
            '        "			,ROW_NUMBER() OVER (PARTITION BY applicantunkid ORDER BY appvacancytranunkid DESC) AS Srno " & _
            '        "		FROM rcapp_vacancy_mapping " & _
            '        "		WHERE isactive = 1 " & _
            '        "	) AS CVac " & _
            '        "	JOIN rcvacancy_master ON CVac.vacancyunkid = rcvacancy_master.vacancyunkid " & _
            '        "	JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype ='" & clsCommon_Master.enCommonMaster.VACANCY_MASTER & "'  " & _
            '        "	WHERE CVac.Srno = 1 " & _
            '        ") AS Vac ON rcapplicant_master.applicantunkid = Vac.applicantunkid "
            strQ = "SELECT " & _
                    "    rcapplicant_master.applicantunkid " & _
                    "   ,rcapplicant_master.applicant_code " & _
                    "   ,rcapplicant_master.titleunkid " & _
                    "   ,rcapplicant_master.firstname " & _
                    "   ,rcapplicant_master.surname " & _
                    "   ,rcapplicant_master.othername " & _
                    "   ,rcapplicant_master.firstname + ' ' + ISNULL(rcapplicant_master.othername,'')+' '+ rcapplicant_master.surname AS applicantname " & _
                    "   ,rcapplicant_master.gender " & _
                    "   ,CASE WHEN rcapplicant_master.gender = 1 THEN @male " & _
                    "         WHEN rcapplicant_master.gender = 2 THEN @female " & _
                    "         WHEN rcapplicant_master.gender = 3 THEN @other " & _
                    "         ELSE '' " & _
                    "    END AS gendertype " & _
                    "   ,rcapplicant_master.email " & _
                    "   ,rcapplicant_master.present_address1 " & _
                    "   ,rcapplicant_master.present_address2 " & _
                    "   ,rcapplicant_master.present_countryunkid " & _
                    "   ,rcapplicant_master.present_stateunkid " & _
                    "   ,rcapplicant_master.present_province " & _
                    "   ,rcapplicant_master.present_post_townunkid " & _
                    "   ,rcapplicant_master.present_zipcode " & _
                    "   ,rcapplicant_master.present_road " & _
                    "   ,rcapplicant_master.present_estate " & _
                    "   ,rcapplicant_master.present_plotno " & _
                    "   ,rcapplicant_master.present_mobileno " & _
                    "   ,rcapplicant_master.present_alternateno " & _
                    "   ,rcapplicant_master.present_tel_no " & _
                    "   ,rcapplicant_master.present_fax " & _
                    "   ,rcapplicant_master.perm_address1 " & _
                    "   ,rcapplicant_master.perm_address2 " & _
                    "   ,rcapplicant_master.perm_countryunkid " & _
                    "   ,rcapplicant_master.perm_stateunkid " & _
                    "   ,rcapplicant_master.perm_province " & _
                    "   ,rcapplicant_master.perm_post_townunkid " & _
                    "   ,rcapplicant_master.perm_zipcode " & _
                    "   ,rcapplicant_master.perm_road " & _
                    "   ,rcapplicant_master.perm_estate " & _
                    "   ,rcapplicant_master.perm_plotno " & _
                    "   ,rcapplicant_master.perm_mobileno " & _
                    "   ,rcapplicant_master.perm_alternateno " & _
                    "   ,rcapplicant_master.perm_tel_no " & _
                    "   ,rcapplicant_master.perm_fax " & _
                    "   ,rcapplicant_master.birth_date " & _
                    "   ,rcapplicant_master.marital_statusunkid " & _
                    "   ,rcapplicant_master.anniversary_date " & _
                    "   ,rcapplicant_master.language1unkid " & _
                    "   ,rcapplicant_master.language2unkid " & _
                    "   ,rcapplicant_master.language3unkid " & _
                    "   ,rcapplicant_master.language4unkid " & _
                    "   ,rcapplicant_master.nationality " & _
                    "   ,rcapplicant_master.userunkid " & _
                    "   ,rcapplicant_master.vacancyunkid " & _
                    "   ,rcapplicant_master.isimport " & _
                    "   ,ISNULL(employeecode, '') AS employeecode " & _
                    "   ,ISNULL(referenceno, '') AS referenceno " & _
                    "   ,ISNULL(rcapp_vacancy_mapping.vacancyunkid,0) AS mapvacancyunkid " & _
                    "   ,ISNULL(cfcommon_master.name,'') AS vacancy " & _
                    "	,CONVERT(CHAR(8),openingdate,112) AS ODate " & _
                    "	,CONVERT(CHAR(8),closingdate,112) AS CDate " & _
                    "   ,ISNULL(memberships, '') " & _
                    "   , ISNULL(memberships,'') AS memberships  " & _
                    "   , ISNULL(achievements,'') AS achievements  " & _
                    "   , ISNULL(journalsresearchpapers,'') AS journalsresearchpapers  " & _
                    "   ,rcvacancy_master.isexternalvacancy AS isexternalvacancy " & _
                    "   , ISNULL(UserId,'') AS UserId  " & _
                    "   ,ISNULL(rcapplicant_master.mother_tongue,'') AS mother_tongue " & _
                    "   ,rcapplicant_master.isimpaired " & _
                    "   ,ISNULL(rcapplicant_master.impairment,'') AS impairment " & _
                    "   ,ISNULL(rcapplicant_master.current_salary,0) AS current_salary " & _
                    "   ,ISNULL(rcapplicant_master.expected_salary,0) AS expected_salary " & _
                    "   ,ISNULL(rcapplicant_master.expected_benefits,'') AS expected_benefits " & _
                    "   ,rcapplicant_master.willing_to_relocate " & _
                    "   ,rcapplicant_master.willing_to_travel " & _
                    "   ,ISNULL(rcapplicant_master.notice_period_days, 0) AS notice_period_days " & _
                    "   , ISNULL(current_salary_currency_id,0) AS current_salary_currency_id  " & _
                    "   , ISNULL(expected_salary_currency_id,0) AS expected_salary_currency_id  " & _
                    "   , ISNULL(rcapplicant_master.isfromonline,0) AS isfromonline  " & _
                    "   , ISNULL(rcapplicant_master.employeeunkid,0) AS employeeunkid  " & _
                    "   , ISNULL(rcapplicant_master.loginemployeeunkid,0) AS loginemployeeunkid  " & _
                    "   , ISNULL(rcapplicant_master.isvoid, 0) AS isvoid " & _
                    "   , ISNULL(rcapplicant_master.voiduserunkid, 0) AS voiduserunkid " & _
                    "   , rcapplicant_master.voiddatetime " & _
                    "   , ISNULL(rcapplicant_master.voidreason, '') AS voidreason " & _
                    "   , ISNULL(rcapplicant_master.nin,'') AS nin " & _
                    "   , ISNULL(rcapplicant_master.tin,'') AS tin " & _
                    "   , ISNULL(rcapplicant_master.ninmobile,'') AS ninmobile " & _
                    "   , ISNULL(rcapplicant_master.village,'') AS village " & _
                    "   , ISNULL(rcapplicant_master.phonenum,'') AS phonenum " & _
                    "   , ISNULL(rcapplicant_master.residency,0) AS residency " & _
                    "   , ISNULL(rcapplicant_master.indexno,'') AS indexno " & _
                    "   ,CASE WHEN rcapplicant_master.residency = 1 THEN @TM " & _
                    "         WHEN rcapplicant_master.residency = 2 THEN @TZ " & _
                    "         ELSE '' " & _
                    "    END AS residencytype " & _
                    "   , applicant_photo " & _
                    "   , applicant_signature " & _
                    "FROM rcapplicant_master " & _
                    " LEFT JOIN rcapp_vacancy_mapping ON rcapp_vacancy_mapping.applicantunkid = rcapplicant_master.applicantunkid AND ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0 " & _
                    " LEFT JOIN rcvacancy_master ON rcvacancy_master.vacancyunkid=rcapp_vacancy_mapping.vacancyunkid " & _
                    " LEFT JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype ='" & CInt(clsCommon_Master.enCommonMaster.VACANCY_MASTER) & "'  " 'Sohail (30 May 2012) - [memberships, achievements, journalsresearchpapers]
            'S.SANDEEP |04-MAY-2023| -- START {nin,tin,ninmobile,village,phonenum,residency,indexno} -- END
            'Sohail (25 Sep 2020) - [employeeunkid, loginemployeeunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
            'Sohail (09 Oct 2018) - [isfromonline], [isactive = 1]=[ISNULL(rcapp_vacancy_mapping.isvoid, 0) = 0]
            'Hemant (09 July 2018) -- [current_salary_currency_id, expected_salary_currency_id]
            'Enhancement : RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)

            'Nilay (13 Apr 2017) -- [mother_tongue, isimpaired, impairment, current_salary, expected_salary, expected_benefits, willing_to_relocate, willing_to_travel, notice_period_days]

            'Sohail (05 Dec 2016) - [UserId]
            'S.SANDEEP [ 06 NOV 2012 (rcvacancy_master.isexternalvacancy)] -- START -- END

            'Sohail (05 Dec 2016) -- Start
            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
            strQ &= " WHERE rcapplicant_master.isvoid = 0 "
            If strFilter.Trim <> "" Then
                strQ &= " " & strFilter
            End If
            'Sohail (05 Dec 2016) -- End

            objDataOperation.AddParameter("@male", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 7, "Male"))
            objDataOperation.AddParameter("@female", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 8, "Female"))
            objDataOperation.AddParameter("@other", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsEmployee_Master", 9, "Other"))
            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-833
            objDataOperation.AddParameter("@TM", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1220, "Tanzania Mainland"))
            objDataOperation.AddParameter("@TZ", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage("clsMasterData", 1221, "Tanzania Zanzibar"))
            'S.SANDEEP |04-MAY-2023| -- END

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dRow As DataRow In dsList.Tables(0).Rows
                If dRow.Item("ODate").ToString.Trim.Length > 0 AndAlso dRow.Item("CDate").ToString.Trim.Length > 0 Then
                    'S.SANDEEP [ 04 JULY 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'dRow.Item("vacancy") = dRow.Item("vacancy") & " ( " & eZeeDate.convertDate(dRow.Item("ODate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dRow.Item("CDate").ToString).ToShortDateString & " ) "
                    dRow.Item("vacancy") = dRow.Item("vacancy").ToString.Trim & " ( " & eZeeDate.convertDate(dRow.Item("ODate").ToString).ToShortDateString & " - " & eZeeDate.convertDate(dRow.Item("CDate").ToString).ToShortDateString & " ) "
                    'S.SANDEEP [ 04 JULY 2012 ] -- END
                End If
            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'S.SANDEEP [ 28 FEB 2012 ] -- END





    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (rcapplicant_master) </purpose>
    ''' Shani(24-Aug-2015) -- Start
    ''' ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    ''' Public Function Insert(Optional ByVal dtSkillTran As DataTable = Nothing, Optional ByVal dtQualifyTran As DataTable = Nothing, Optional ByVal dtJobHistory As DataTable = Nothing, Optional ByVal dtReferences As DataTable = Nothing) As Boolean
    Public Function Insert(ByVal intApplicantCodeNotype As Integer, ByVal strApplicantCodePrifix As String, Optional ByVal dtSkillTran As DataTable = Nothing, Optional ByVal dtQualifyTran As DataTable = Nothing, Optional ByVal dtJobHistory As DataTable = Nothing, Optional ByVal dtReferences As DataTable = Nothing) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        objDataOperation.BindTransaction()

        'Anjan (21 Jan 2012)-Start
        'ENHANCEMENT : TRA COMMENTS 
        'Issue : To fix the issue on network of autocode concurrency when more than 1 user press save at same time.
        'Sandeep [ 21 Aug 2010 ] -- Start


        'S.SANDEEP [ 28 MARCH 2012 ] -- START
        'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
        'Dim intApplicantCodeNotype As Integer = 0
        'Dim strApplicantCodePrefix As String = ""
        'Dim intNextApplicantCodeNo As Integer = 0
        'intApplicantCodeNotype = ConfigParameter._Object._ApplicantCodeNotype
        'If intApplicantCodeNotype = 1 Then
        '    strApplicantCodePrefix = ConfigParameter._Object._ApplicantCodePrifix
        '    intNextApplicantCodeNo = ConfigParameter._Object._NextApplicantCodeNo
        '    mstrApplicant_Code = strApplicantCodePrefix & intNextApplicantCodeNo
        'End If
        ''S.SANDEEP [ 30 JULY 2011 ] -- START
        ''ENHANCEMENT : I/E APPLICANT RELATED DATA VIA WEB
        'If isExist(mstrApplicant_Code) Then
        '    mstrMessage = Language.getMessage(mstrModuleName, 111, "This Applicant Code already exists.Please define new Applicant Code.")
        '    Return False
        'End If
        ''S.SANDEEP [ 30 JULY 2011 ] -- END 

        'Shani(24-Aug-2015) -- Start
        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
        'ConfigParameter._Object.Refresh()
        'Dim intApplicantCodeNotype As Integer = 0
        'intApplicantCodeNotype = ConfigParameter._Object._ApplicantCodeNotype
        'Shani(24-Aug-2015) -- End

        If intApplicantCodeNotype = 0 Then
        If isExist(mstrApplicant_Code) Then
                mstrMessage = Language.getMessage(mstrModuleName, 13, "This Applicant Code already exists.Please define new Applicant Code.")
                objDataOperation.ReleaseTransaction(False)
            Return False
        End If
        End If
        'S.SANDEEP [ 28 MARCH 2012 ] -- END



        'Sandeep [ 21 Aug 2010 ] -- End 



        'Anjan (02 Jan 2012)-Start

        'Sohail (05 May 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (05 Dec 2016) -- Start
        'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
        'If isExist(, , mstrEmail, mstrFirstname, mstrSurname) Then
        If isExist(, , mstrEmail) Then
            'Sohail (05 Dec 2016) -- End
            'If isExist(, , mstrEmail, mintVacancyunkid, mstrFirstname, mstrSurname) Then
            'Sohail (05 May 2012) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 14, "This Email Address already exists.Please define new Email Address.")
            objDataOperation.ReleaseTransaction(False)
            Return False
        End If
        'Anjan (02 Jan 2012)-End
        'Anjan (21 Jan 2012)-End 

        Try
            objDataOperation.AddParameter("@applicant_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplicant_Code.ToString)
            objDataOperation.AddParameter("@titleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTitleunkid.ToString)
            objDataOperation.AddParameter("@firstname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFirstname.ToString)
            objDataOperation.AddParameter("@surname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSurname.ToString)
            objDataOperation.AddParameter("@othername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOthername.ToString)
            objDataOperation.AddParameter("@gender", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGender.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@present_address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Address1.ToString)
            objDataOperation.AddParameter("@present_address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Address2.ToString)
            objDataOperation.AddParameter("@present_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPresent_Countryunkid.ToString)
            objDataOperation.AddParameter("@present_stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPresent_Stateunkid.ToString)
            objDataOperation.AddParameter("@present_province", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Province.ToString)
            objDataOperation.AddParameter("@present_post_townunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPresent_Post_Townunkid.ToString)
            objDataOperation.AddParameter("@present_zipcode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPresent_Zipcode.ToString)
            objDataOperation.AddParameter("@present_road", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Road.ToString)
            objDataOperation.AddParameter("@present_estate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Estate.ToString)
            objDataOperation.AddParameter("@present_plotno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Plotno.ToString)
            objDataOperation.AddParameter("@present_mobileno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Mobileno.ToString)
            objDataOperation.AddParameter("@present_alternateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Alternateno.ToString)
            objDataOperation.AddParameter("@present_tel_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Tel_No.ToString)
            objDataOperation.AddParameter("@present_fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Fax.ToString)
            objDataOperation.AddParameter("@perm_address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Address1.ToString)
            objDataOperation.AddParameter("@perm_address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Address2.ToString)
            objDataOperation.AddParameter("@perm_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerm_Countryunkid.ToString)
            objDataOperation.AddParameter("@perm_stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerm_Stateunkid.ToString)
            objDataOperation.AddParameter("@perm_province", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Province.ToString)
            objDataOperation.AddParameter("@perm_post_townunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerm_Post_Townunkid.ToString)
            objDataOperation.AddParameter("@perm_zipcode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerm_Zipcode.ToString)
            objDataOperation.AddParameter("@perm_road", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Road.ToString)
            objDataOperation.AddParameter("@perm_estate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Estate.ToString)
            objDataOperation.AddParameter("@perm_plotno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Plotno.ToString)
            objDataOperation.AddParameter("@perm_mobileno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Mobileno.ToString)
            objDataOperation.AddParameter("@perm_alternateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Alternateno.ToString)
            objDataOperation.AddParameter("@perm_tel_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Tel_No.ToString)
            objDataOperation.AddParameter("@perm_fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Fax.ToString)
            If mdtBirth_Date = Nothing Then
                objDataOperation.AddParameter("@birth_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@birth_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBirth_Date)
            End If

            objDataOperation.AddParameter("@marital_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMarital_Statusunkid.ToString)

            If mdtAnniversary_Date = Nothing Then
                objDataOperation.AddParameter("@anniversary_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@anniversary_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAnniversary_Date)
            End If

            objDataOperation.AddParameter("@language1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage1unkid.ToString)
            objDataOperation.AddParameter("@language2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage2unkid.ToString)
            objDataOperation.AddParameter("@language3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage3unkid.ToString)
            objDataOperation.AddParameter("@language4unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage4unkid.ToString)
            objDataOperation.AddParameter("@nationality", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNationality.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)

            'Anjan (21 Jul 2011)-Start
            'Issue : for web extra information.
            objDataOperation.AddParameter("@otherskills", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherSkills.ToString)
            objDataOperation.AddParameter("@otherqualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualification.ToString)
            'Anjan (21 Jul 2011)-End 

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@employeecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmployeecode.ToString)
            objDataOperation.AddParameter("@referenceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReferenceno.ToString)
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'Sohail (30 May 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@memberships", SqlDbType.NVarChar, mstrMemberships.Length, mstrMemberships.ToString)
            objDataOperation.AddParameter("@achievements", SqlDbType.NVarChar, mstrAchievements.Length, mstrAchievements.ToString)
            objDataOperation.AddParameter("@journalsresearchpapers", SqlDbType.NVarChar, mstrJournalsResearchPapers.Length, mstrJournalsResearchPapers.ToString)
            'Sohail (30 May 2012) -- End

            'Sohail (01 Mar 2019) -- Start
            'VISCAR Issue - 74.1 - The incoming tabular data stream (TDS) remote procedure call (RPC) protocol stream is incorrect. Parameter 56 ("@UserId"): Data type 0xE7 has an invalid data length or metadata length.
            'objDataOperation.AddParameter("@UserId", SqlDbType.NVarChar, mstrJournalsResearchPapers.Length, mstrUserId.ToString) 'Sohail (05 Dec 2016)
            objDataOperation.AddParameter("@UserId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUserId.ToString)
            'Sohail (01 Mar 2019) -- End

            'Nilay (13 Apr 2017) -- Start
            objDataOperation.AddParameter("@mother_tongue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMotherTongue)
            objDataOperation.AddParameter("@isimpaired", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsImpaired.ToString)
            objDataOperation.AddParameter("@impairment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImpairment)
            objDataOperation.AddParameter("@current_salary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentSalary.ToString)
            objDataOperation.AddParameter("@expected_salary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpectedSalary.ToString)
            objDataOperation.AddParameter("@expected_benefits", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExpectedBenefits)
            objDataOperation.AddParameter("@willing_to_relocate", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnWillingToRelocate.ToString)
            objDataOperation.AddParameter("@willing_to_travel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnWillingToTravel.ToString)
            objDataOperation.AddParameter("@notice_period_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoticePeriodDays.ToString)
            'Nilay (13 Apr 2017) -- End
            'Hemant (09 July 2018) -- Start
            'Enhancement : RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)
            objDataOperation.AddParameter("@current_salary_currency_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mIntCurrentSalaryCurrencyId.ToString)
            objDataOperation.AddParameter("@expected_salary_currency_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mIntExpectedSalaryCurrencyId.ToString)
            'Hemant (09 July 2018) -- End

            'Sohail (09 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
            objDataOperation.AddParameter("@isfromonline", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromOnline)
            'Sohail (09 Oct 2018) -- End

            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            'Sohail (25 Sep 2020) -- End

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-833
            objDataOperation.AddParameter("@nin", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrNidaNumber)
            objDataOperation.AddParameter("@tin", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrTIN)
            objDataOperation.AddParameter("@ninmobile", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrNidaMobileNum)
            objDataOperation.AddParameter("@village", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVillage)
            objDataOperation.AddParameter("@phonenum", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrPhoneNumber1)
            objDataOperation.AddParameter("@residency", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResidency)
            objDataOperation.AddParameter("@indexno", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrIndexNo)
            If bytApplicantPhoto IsNot Nothing Then
                objDataOperation.AddParameter("@applicant_photo", SqlDbType.Binary, bytApplicantPhoto.Length, bytApplicantPhoto)
            Else
                objDataOperation.AddParameter("@applicant_photo", SqlDbType.Binary, 0, DBNull.Value)
            End If
            If bytApplicantSigtr IsNot Nothing Then
                objDataOperation.AddParameter("@applicant_signature", SqlDbType.Binary, bytApplicantSigtr.Length, bytApplicantSigtr)
            Else
                objDataOperation.AddParameter("@applicant_signature", SqlDbType.Binary, 0, DBNull.Value)
            End If
            'S.SANDEEP |04-MAY-2023| -- END

            strQ = "INSERT INTO rcapplicant_master ( " & _
              "  applicant_code " & _
              ", titleunkid " & _
              ", firstname " & _
              ", surname " & _
              ", othername " & _
              ", gender " & _
              ", email " & _
              ", present_address1 " & _
              ", present_address2 " & _
              ", present_countryunkid " & _
              ", present_stateunkid " & _
              ", present_province " & _
              ", present_post_townunkid " & _
              ", present_zipcode " & _
              ", present_road " & _
              ", present_estate " & _
              ", present_plotno " & _
              ", present_mobileno " & _
              ", present_alternateno " & _
              ", present_tel_no " & _
              ", present_fax " & _
              ", perm_address1 " & _
              ", perm_address2 " & _
              ", perm_countryunkid " & _
              ", perm_stateunkid " & _
              ", perm_province " & _
              ", perm_post_townunkid " & _
              ", perm_zipcode " & _
              ", perm_road " & _
              ", perm_estate " & _
              ", perm_plotno " & _
              ", perm_mobileno " & _
              ", perm_alternateno " & _
              ", perm_tel_no " & _
              ", perm_fax " & _
              ", birth_date " & _
              ", marital_statusunkid " & _
              ", anniversary_date " & _
              ", language1unkid " & _
              ", language2unkid " & _
              ", language3unkid " & _
              ", language4unkid " & _
              ", nationality " & _
              ", userunkid" & _
              ", vacancyunkid "
            'Anjan (21 Jul 2011)-Start
            'Issue : for web extra information.
            strQ &= ", other_skills " & _
                    ", other_qualification "
            'Anjan (21 Jul 2011)-End 

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= ", employeecode " & _
                        ", referenceno "
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'Sohail (30 May 2012) -- Start
            'TRA - ENHANCEMENT
            strQ &= " , memberships  " & _
                    " , achievements  " & _
                    " , journalsresearchpapers  " & _
                    " , UserId  "
            'Sohail (05 Dec 2016) - [UserId]
            'Sohail (30 May 2012) -- End

            'Nilay (13 Apr 2017) -- Start
            strQ &= " , mother_tongue " & _
                    " , isimpaired " & _
                    " , impairment " & _
                    " , current_salary " & _
                    " , expected_salary " & _
                    " , expected_benefits " & _
                    " , willing_to_relocate " & _
                    " , willing_to_travel " & _
                    " , notice_period_days "
            'Nilay (13 Apr 2017) -- End

            'Hemant (09 July 2018) -- Start
            strQ &= " , current_salary_currency_id  " & _
                    " , expected_salary_currency_id  " & _
                    " , isfromonline  " & _
                    " , employeeunkid  " & _
                    " , loginemployeeunkid  " & _
                    " , isvoid " & _
                    " , voiduserunkid " & _
                    " , voiddatetime " & _
                    " , voidreason "
            'Sohail (25 Sep 2020) - [employeeunkid, loginemployeeunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
            'Sohail (09 Oct 2018) - [isfromonline]
            'Hemant (09 July 2018) -- End

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-833
            strQ &= ", nin " & _
                    ", tin " & _
                    ", ninmobile " & _
                    ", village " & _
                    ", phonenum " & _
                    ", residency " & _
                    ", indexno " & _
                    ", applicant_photo " & _
                    ", applicant_signature "
            'S.SANDEEP |04-MAY-2023| -- END

            strQ &= ") VALUES (" & _
              "  @applicant_code " & _
              ", @titleunkid " & _
              ", @firstname " & _
              ", @surname " & _
              ", @othername " & _
              ", @gender " & _
              ", @email " & _
              ", @present_address1 " & _
              ", @present_address2 " & _
              ", @present_countryunkid " & _
              ", @present_stateunkid " & _
              ", @present_province " & _
              ", @present_post_townunkid " & _
              ", @present_zipcode " & _
              ", @present_road " & _
              ", @present_estate " & _
              ", @present_plotno " & _
              ", @present_mobileno " & _
              ", @present_alternateno " & _
              ", @present_tel_no " & _
              ", @present_fax " & _
              ", @perm_address1 " & _
              ", @perm_address2 " & _
              ", @perm_countryunkid " & _
              ", @perm_stateunkid " & _
              ", @perm_province " & _
              ", @perm_post_townunkid " & _
              ", @perm_zipcode " & _
              ", @perm_road " & _
              ", @perm_estate " & _
              ", @perm_plotno " & _
              ", @perm_mobileno " & _
              ", @perm_alternateno " & _
              ", @perm_tel_no " & _
              ", @perm_fax " & _
              ", @birth_date " & _
              ", @marital_statusunkid " & _
              ", @anniversary_date " & _
              ", @language1unkid " & _
              ", @language2unkid " & _
              ", @language3unkid " & _
              ", @language4unkid " & _
              ", @nationality " & _
              ", @userunkid" & _
              ", @vacancyunkid "
            'Anjan (21 Jul 2011)-Start
            'Issue : for web extra information.
            strQ &= ", @otherskills " & _
                    ", @otherqualification "
            'Anjan (21 Jul 2011)-End 

            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= ", @employeecode " & _
                        ", @referenceno "
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'Sohail (30 May 2012) -- Start
            'TRA - ENHANCEMENT
            strQ &= " , @memberships  " & _
                    " , @achievements  " & _
                    " , @journalsresearchpapers  " & _
                    " , @UserId  "
            'Sohail (05 Dec 2016) - [UserId]
            'Sohail (30 May 2012) -- End
            'Nilay (13 Apr 2017) -- Start
            strQ &= " , @mother_tongue " & _
                    " , @isimpaired " & _
                    " , @impairment " & _
                    " , @current_salary " & _
                    " , @expected_salary " & _
                    " , @expected_benefits " & _
                    " , @willing_to_relocate " & _
                    " , @willing_to_travel " & _
                    " , @notice_period_days "
            'Nilay (13 Apr 2017) -- End

            'Hemant (09 July 2018) -- Start
            strQ &= " , @current_salary_currency_id  " & _
                    " , @expected_salary_currency_id  " & _
                    " , @isfromonline  " & _
                    " , @employeeunkid  " & _
                    " , @loginemployeeunkid  " & _
                    " , @isvoid " & _
                    " , @voiduserunkid " & _
                    " , @voiddatetime " & _
                    " , @voidreason "
            'Sohail (25 Sep 2020) - [employeeunkid, loginemployeeunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
            'Sohail (09 Oct 2018) - [isfromonline]
            'Hemant (09 July 2018) -- End

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-833
            strQ &= ", @nin " & _
                    ", @tin " & _
                    ", @ninmobile " & _
                    ", @village " & _
                    ", @phonenum " & _
                    ", @residency " & _
                    ", @indexno " & _
                    ", @applicant_photo " & _
                    ", @applicant_signature "
            'S.SANDEEP |04-MAY-2023| -- END

            strQ &= "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintApplicantunkid = dsList.Tables(0).Rows(0).Item(0)

            'S.SANDEEP [ 28 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
            If intApplicantCodeNotype = 1 Then
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If Set_AutoNumber(objDataOperation, mintApplicantunkid, "rcapplicant_master", "applicant_code", "applicantunkid", "NextApplicantCodeNo", ConfigParameter._Object._ApplicantCodePrifix) = False Then
                If Set_AutoNumber(objDataOperation, mintApplicantunkid, "rcapplicant_master", "applicant_code", "applicantunkid", "NextApplicantCodeNo", strApplicantCodePrifix) = False Then
                    'Shani(24-Aug-2015) -- End

                    'S.SANDEEP [ 17 NOV 2012 ] -- START
                    'ENHANCEMENT : TRA CHANGES
                    'If objDataOperation.ErrorMessage <> "" Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If

                    If objDataOperation.ErrorMessage <> "" Then
                        objDataOperation.ReleaseTransaction(False)
                        Return False
                    End If
                    'S.SANDEEP [ 17 NOV 2012 ] -- END
                End If
            End If

            'S.SANDEEP [ 28 MARCH 2012 ] -- END

            Dim blnFlag As Boolean = False

            Dim objImage_Tran As New clshr_images_tran
            If mstrImagePath.Trim <> "" Then
                objImage_Tran._Employeeunkid = mintApplicantunkid
                objImage_Tran._Imagename = mstrImagePath
                objImage_Tran._Isapplicant = True
                objImage_Tran._Referenceid = enImg_Email_RefId.Applicant_Module
                objImage_Tran._Transactionid = mintApplicantunkid
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objImage_Tran.Insert()
                blnFlag = objImage_Tran.Insert(objDataOperation)
                'S.SANDEEP [04 JUN 2015] -- END
                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            Else
                objImage_Tran._Employeeunkid = mintApplicantunkid
                objImage_Tran._Imagename = mstrImagePath
                objImage_Tran._Isapplicant = True
                objImage_Tran._Referenceid = enImg_Email_RefId.Applicant_Module
                objImage_Tran._Transactionid = mintApplicantunkid
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objImage_Tran.Delete()
                blnFlag = objImage_Tran.Delete(objDataOperation)
                'S.SANDEEP [04 JUN 2015] -- END
                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            End If
            objImage_Tran = Nothing


            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            'If dtSkillTran IsNot Nothing Then
            '    objSkill_Tran._ApplicantUnkid = mintApplicantunkid
            '    objSkill_Tran._DataList = dtSkillTran
            '    blnFlag = objSkill_Tran.InsertUpdateDelete_SkillTran()
            '    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            'End If

            Dim blnToInsert As Boolean = False

            If dtSkillTran IsNot Nothing Then
                If dtSkillTran.Rows.Count > 0 Then
                    objSkill_Tran._ApplicantUnkid = mintApplicantunkid
                    objSkill_Tran._DataList = dtSkillTran
                    blnFlag = objSkill_Tran.InsertUpdateDelete_SkillTran()
                    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    blnToInsert = False
                Else
                    blnToInsert = True

                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapplicantskill_tran", "skilltranunkid", -1, 1, 0) = False Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If
                End If
            End If

            'Anjan (12 Oct 2011)-End 



            If dtQualifyTran IsNot Nothing Then
                'Anjan (12 Oct 2011)-Start
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.

                'objQualify_Tran._ApplicantUnkid = mintApplicantunkid
                'objQualify_Tran._DataList = dtQualifyTran
                'blnFlag = objQualify_Tran.InsertUpdateDelete_QualificationTran()
                'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                If dtQualifyTran.Rows.Count > 0 Then
                    objQualify_Tran._ApplicantUnkid = mintApplicantunkid
                    objQualify_Tran._DataList = dtQualifyTran
                    blnFlag = objQualify_Tran.InsertUpdateDelete_QualificationTran()
                    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    blnToInsert = False
                Else
                    blnToInsert = True
                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapplicantqualification_tran", "qualificationtranunkid", -1, 1, 0) = False Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If
                End If
                'Anjan (12 Oct 2011)-End 

            End If

            If dtJobHistory IsNot Nothing Then

                'Anjan (12 Oct 2011)-Start
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.

                'objJobHistory._Applicantunkid = mintApplicantunkid
                'objJobHistory._DataList = dtJobHistory
                'blnFlag = objJobHistory.InsertUpdateDelete_JobHistoryTran()
                'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                If dtJobHistory.Rows.Count > 0 Then
                objJobHistory._Applicantunkid = mintApplicantunkid
                objJobHistory._DataList = dtJobHistory
                blnFlag = objJobHistory.InsertUpdateDelete_JobHistoryTran()
                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    blnToInsert = False
                Else
                    blnToInsert = True
                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcjobhistory", "jobhistorytranunkid", -1, 1, 0) = False Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If
                End If

                If blnToInsert = True Then ' This is when all data are not given for other tabs and only main rcapplicnt master data are given then AT will fall for rcapplicant_master.
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapplicantskill_tran", "skilltranunkid", -1, 1, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If

                'Anjan (12 Oct 2011)-End 


            End If



            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            If dtReferences IsNot Nothing Then
                If dtReferences.Rows.Count > 0 Then
                    objReference_Tran._Applicantunkid = mintApplicantunkid
                    objReference_Tran._dtReferences = dtReferences
                    'objReference_Tran._Userunkid = mintUserunkid
                    blnFlag = objReference_Tran.InsertUpdateDelete_ReferencesTran
                    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    blnToInsert = False
                Else
                    blnToInsert = True
                End If

                If blnToInsert = True Then
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapp_reference_tran", "referencetranunkid", -1, 1, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
            End If

            'Pinkal (06-Feb-2012) -- End



            'S.SANDEEP [ 28 MARCH 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES {AUTO NUMBER ISSUE.}
            ''Sandeep [ 21 Aug 2010 ] -- Start
            'If intApplicantCodeNotype = 1 Then
            '    ConfigParameter._Object._NextApplicantCodeNo = intNextApplicantCodeNo + 1
            '    ConfigParameter._Object.updateParam()
            '    ConfigParameter._Object.Refresh()
            'End If
            ''Sandeep [ 21 Aug 2010 ] -- End 
            'S.SANDEEP [ 28 MARCH 2012 ] -- END


            objDataOperation.ReleaseTransaction(True)



            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (rcapplicant_master) </purpose>
    Public Function Update(Optional ByVal dtSkillTran As DataTable = Nothing, Optional ByVal dtQualifyTran As DataTable = Nothing, Optional ByVal dtJobHistory As DataTable = Nothing, Optional ByVal dtReferences As DataTable = Nothing) As Boolean
        If isExist(mstrApplicant_Code, mintApplicantunkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 13, "This Applicant Code already exists.Please define new Applicant Code.")
            Return False
        End If
        'Anjan (02 Jan 2012)-Start

        'Sohail (05 May 2012) -- Start
        'TRA - ENHANCEMENT
        'Sohail (05 Dec 2016) -- Start
        'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
        'If isExist(, mintApplicantunkid, mstrEmail, mstrFirstname, mstrSurname) Then
        If isExist(, mintApplicantunkid, mstrEmail) Then
            'Sohail (05 Dec 2016) -- End
            'If isExist(, mintApplicantunkid, mstrEmail, mintVacancyunkid, mstrFirstname, mstrSurname) Then
            'Sohail (05 May 2012) -- End
            mstrMessage = Language.getMessage(mstrModuleName, 14, "This Email Address already exists.Please define new Email Address.")
            Return False
        End If
        'Anjan (02 Jan 2012)-End

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation
        objDataOperation.BindTransaction()

        Try
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApplicantunkid.ToString)
            objDataOperation.AddParameter("@applicant_code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrApplicant_Code.ToString)
            objDataOperation.AddParameter("@titleunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintTitleunkid.ToString)
            objDataOperation.AddParameter("@firstname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFirstname.ToString)
            objDataOperation.AddParameter("@surname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSurname.ToString)
            objDataOperation.AddParameter("@othername", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOthername.ToString)
            objDataOperation.AddParameter("@gender", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrGender.ToString)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            objDataOperation.AddParameter("@present_address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Address1.ToString)
            objDataOperation.AddParameter("@present_address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Address2.ToString)
            objDataOperation.AddParameter("@present_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPresent_Countryunkid.ToString)
            objDataOperation.AddParameter("@present_stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPresent_Stateunkid.ToString)
            objDataOperation.AddParameter("@present_province", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Province.ToString)
            objDataOperation.AddParameter("@present_post_townunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPresent_Post_Townunkid.ToString)
            objDataOperation.AddParameter("@present_zipcode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPresent_Zipcode.ToString)
            objDataOperation.AddParameter("@present_road", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Road.ToString)
            objDataOperation.AddParameter("@present_estate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Estate.ToString)
            objDataOperation.AddParameter("@present_plotno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Plotno.ToString)
            objDataOperation.AddParameter("@present_mobileno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Mobileno.ToString)
            objDataOperation.AddParameter("@present_alternateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Alternateno.ToString)
            objDataOperation.AddParameter("@present_tel_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Tel_No.ToString)
            objDataOperation.AddParameter("@present_fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPresent_Fax.ToString)
            objDataOperation.AddParameter("@perm_address1", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Address1.ToString)
            objDataOperation.AddParameter("@perm_address2", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Address2.ToString)
            objDataOperation.AddParameter("@perm_countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerm_Countryunkid.ToString)
            objDataOperation.AddParameter("@perm_stateunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerm_Stateunkid.ToString)
            objDataOperation.AddParameter("@perm_province", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Province.ToString)
            objDataOperation.AddParameter("@perm_post_townunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerm_Post_Townunkid.ToString)
            objDataOperation.AddParameter("@perm_zipcode", SqlDbType.Int, eZeeDataType.INT_SIZE, mintPerm_Zipcode.ToString)
            objDataOperation.AddParameter("@perm_road", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Road.ToString)
            objDataOperation.AddParameter("@perm_estate", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Estate.ToString)
            objDataOperation.AddParameter("@perm_plotno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Plotno.ToString)
            objDataOperation.AddParameter("@perm_mobileno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Mobileno.ToString)
            objDataOperation.AddParameter("@perm_alternateno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Alternateno.ToString)
            objDataOperation.AddParameter("@perm_tel_no", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Tel_No.ToString)
            objDataOperation.AddParameter("@perm_fax", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrPerm_Fax.ToString)
            If mdtBirth_Date = Nothing Then
                objDataOperation.AddParameter("@birth_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@birth_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtBirth_Date)
            End If
            objDataOperation.AddParameter("@marital_statusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintMarital_Statusunkid.ToString)
            If mdtAnniversary_Date = Nothing Then
                objDataOperation.AddParameter("@anniversary_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@anniversary_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAnniversary_Date)
            End If

            objDataOperation.AddParameter("@language1unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage1unkid.ToString)
            objDataOperation.AddParameter("@language2unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage2unkid.ToString)
            objDataOperation.AddParameter("@language3unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage3unkid.ToString)
            objDataOperation.AddParameter("@language4unkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLanguage4unkid.ToString)
            objDataOperation.AddParameter("@nationality", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNationality.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid.ToString)
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVacancyunkid.ToString)

            'Anjan (21 Jul 2011)-Start
            'Issue : for web extra information.
            objDataOperation.AddParameter("@otherskills", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherSkills.ToString)
            objDataOperation.AddParameter("@otherqualification", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrOtherQualification.ToString)
            'Anjan (21 Jul 2011)-End 


            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            objDataOperation.AddParameter("@employeecode", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmployeecode.ToString)
            objDataOperation.AddParameter("@referenceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrReferenceno.ToString)
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'Sohail (30 May 2012) -- Start
            'TRA - ENHANCEMENT
            objDataOperation.AddParameter("@memberships", SqlDbType.NVarChar, mstrMemberships.Length, mstrMemberships.ToString)
            objDataOperation.AddParameter("@achievements", SqlDbType.NVarChar, mstrAchievements.Length, mstrAchievements.ToString)
            objDataOperation.AddParameter("@journalsresearchpapers", SqlDbType.NVarChar, mstrJournalsResearchPapers.Length, mstrJournalsResearchPapers.ToString)
            'Sohail (30 May 2012) -- End
            'Sohail (01 Mar 2019) -- Start
            'VISCAR Issue - 74.1 - The incoming tabular data stream (TDS) remote procedure call (RPC) protocol stream is incorrect. Parameter 56 ("@UserId"): Data type 0xE7 has an invalid data length or metadata length.
            'objDataOperation.AddParameter("@UserId", SqlDbType.NVarChar, mstrJournalsResearchPapers.Length, mstrUserId.ToString) 'Sohail (05 Dec 2016)
            objDataOperation.AddParameter("@UserId", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrUserId.ToString)
            'Sohail (01 Mar 2019) -- End
            'Nilay (13 Apr 2017) -- Start
            objDataOperation.AddParameter("@mother_tongue", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMotherTongue)
            objDataOperation.AddParameter("@isimpaired", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsImpaired.ToString)
            objDataOperation.AddParameter("@impairment", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrImpairment)
            objDataOperation.AddParameter("@current_salary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecCurrentSalary.ToString)
            objDataOperation.AddParameter("@expected_salary", SqlDbType.Decimal, eZeeDataType.DECIMAL_SIZE, mdecExpectedSalary.ToString)
            objDataOperation.AddParameter("@expected_benefits", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrExpectedBenefits)
            objDataOperation.AddParameter("@willing_to_relocate", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnWillingToRelocate.ToString)
            objDataOperation.AddParameter("@willing_to_travel", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnWillingToTravel.ToString)
            objDataOperation.AddParameter("@notice_period_days", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNoticePeriodDays.ToString)
            'Nilay (13 Apr 2017) -- End
            'Hemant (09 July 2018) -- Start
            'Enhancement : RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)
            objDataOperation.AddParameter("@current_salary_currency_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintCurrentSalaryCurrencyId.ToString)
            objDataOperation.AddParameter("@expected_salary_currency_id", SqlDbType.Int, eZeeDataType.INT_SIZE, mintExpectedSalaryCurrencyId.ToString)
            'Hemant (09 July 2018) -- End

            'Sohail (09 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
            objDataOperation.AddParameter("@isfromonline", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsFromOnline)
            'Sohail (09 Oct 2018) -- End
            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLogEmployeeUnkid.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsvoid)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            If mdtVoiddatetime <> Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrVoidreason)
            'Sohail (25 Sep 2020) -- End

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-833
            objDataOperation.AddParameter("@nin", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrNidaNumber)
            objDataOperation.AddParameter("@tin", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrTIN)
            objDataOperation.AddParameter("@ninmobile", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrNidaMobileNum)
            objDataOperation.AddParameter("@village", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVillage)
            objDataOperation.AddParameter("@phonenum", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrPhoneNumber1)
            objDataOperation.AddParameter("@residency", SqlDbType.Int, eZeeDataType.INT_SIZE, mintResidency)
            objDataOperation.AddParameter("@indexno", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrIndexNo)
            If bytApplicantPhoto IsNot Nothing Then
                objDataOperation.AddParameter("@applicant_photo", SqlDbType.Binary, bytApplicantPhoto.Length, bytApplicantPhoto)
            Else
                objDataOperation.AddParameter("@applicant_photo", SqlDbType.Binary, 0, DBNull.Value)
            End If
            If bytApplicantSigtr IsNot Nothing Then
                objDataOperation.AddParameter("@applicant_signature", SqlDbType.Binary, bytApplicantSigtr.Length, bytApplicantSigtr)
            Else
                objDataOperation.AddParameter("@applicant_signature", SqlDbType.Binary, 0, DBNull.Value)
            End If
            'S.SANDEEP |04-MAY-2023| -- END

            strQ = "UPDATE rcapplicant_master SET " & _
              "  applicant_code = @applicant_code" & _
              ", titleunkid = @titleunkid" & _
              ", firstname = @firstname" & _
              ", surname = @surname" & _
              ", othername = @othername" & _
              ", gender = @gender" & _
              ", email = @email" & _
              ", present_address1 = @present_address1" & _
              ", present_address2 = @present_address2" & _
              ", present_countryunkid = @present_countryunkid" & _
              ", present_stateunkid = @present_stateunkid" & _
              ", present_province = @present_province" & _
              ", present_post_townunkid = @present_post_townunkid" & _
              ", present_zipcode = @present_zipcode" & _
              ", present_road = @present_road" & _
              ", present_estate = @present_estate" & _
              ", present_plotno = @present_plotno" & _
              ", present_mobileno = @present_mobileno" & _
              ", present_alternateno = @present_alternateno" & _
              ", present_tel_no = @present_tel_no" & _
              ", present_fax = @present_fax" & _
              ", perm_address1 = @perm_address1" & _
              ", perm_address2 = @perm_address2" & _
              ", perm_countryunkid = @perm_countryunkid" & _
              ", perm_stateunkid = @perm_stateunkid" & _
              ", perm_province = @perm_province" & _
              ", perm_post_townunkid = @perm_post_townunkid" & _
              ", perm_zipcode = @perm_zipcode " & _
              ", perm_road = @perm_road" & _
              ", perm_estate = @perm_estate" & _
              ", perm_plotno = @perm_plotno" & _
              ", perm_mobileno = @perm_mobileno" & _
              ", perm_alternateno = @perm_alternateno" & _
              ", perm_tel_no = @perm_tel_no" & _
              ", perm_fax = @perm_fax" & _
              ", birth_date = @birth_date" & _
              ", marital_statusunkid = @marital_statusunkid" & _
              ", anniversary_date = @anniversary_date" & _
              ", language1unkid = @language1unkid" & _
              ", language2unkid = @language2unkid" & _
              ", language3unkid = @language3unkid" & _
              ", language4unkid = @language4unkid" & _
              ", nationality = @nationality" & _
              ", userunkid = @userunkid " & _
              ", vacancyunkid =@vacancyunkid "

            'Anjan (21 Jul 2011)-Start
            'purpose : for web extra information.
            strQ &= ", other_skills = @otherskills " & _
                    ", other_qualification  = @otherqualification "
            'Anjan (21 Jul 2011)-End 


            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            strQ &= ", employeecode = @employeecode " & _
                        ", referenceno = @referenceno "
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'Sohail (30 May 2012) -- Start
            'TRA - ENHANCEMENT
            strQ &= " , memberships = @memberships  " & _
                    " , achievements = @achievements  " & _
                    " , journalsresearchpapers = @journalsresearchpapers  " & _
                    " , UserId = @UserId  "
            'Sohail (05 Dec 2016) - [UserId]
            'Sohail (30 May 2012) -- End

            'Nilay (13 Apr 2017) -- Start
            strQ &= " , mother_tongue = @mother_tongue " & _
                    " , isimpaired = @isimpaired " & _
                    " , impairment = @impairment " & _
                    " , current_salary = @current_salary " & _
                    " , expected_salary = @expected_salary " & _
                    " , expected_benefits = @expected_benefits " & _
                    " , willing_to_relocate = @willing_to_relocate " & _
                    " , willing_to_travel = @willing_to_travel " & _
                    " , notice_period_days = @notice_period_days "
            'Nilay (13 Apr 2017) -- End
            'Hemant (09 July 2018) -- Start
            'Enhancement : RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)
            strQ &= " , current_salary_currency_id = @current_salary_currency_id  " & _
                   " , expected_salary_currency_id = @expected_salary_currency_id  " & _
                   " , isfromonline = @isfromonline  " & _
                   " , employeeunkid = @employeeunkid  " & _
                   " , loginemployeeunkid = @loginemployeeunkid  " & _
                   " , isvoid = @isvoid " & _
                   " , voiduserunkid = @voiduserunkid " & _
                   " , voiddatetime = @voiddatetime " & _
                   " , voidreason = @voidreason "
            'Sohail (25 Sep 2020) - [employeeunkid, loginemployeeunkid, isvoid, voiduserunkid, voiddatetime, voidreason]
            'Sohail (09 Oct 2018) - [isfromonline]
            'Hemant (09 July 2018) -- End

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-833
            strQ &= ", nin = @nin " & _
                    ", tin = @tin " & _
                    ", ninmobile = @ninmobile " & _
                    ", village = @village " & _
                    ", phonenum = @phonenum " & _
                    ", residency = @residency " & _
                    ", indexno = @indexno " & _
                    ", applicant_photo = @applicant_photo " & _
                    ", applicant_signature = @applicant_signature "
            'S.SANDEEP |04-MAY-2023| -- END


            strQ &= "WHERE applicantunkid = @applicantunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Dim blnFlag As Boolean = False

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            Dim blnToInsert As Boolean = False
            'Anjan (12 Oct 2011)-End 

            Dim objImage_Tran As New clshr_images_tran
            If mstrImagePath.Trim <> "" Then
                objImage_Tran._Employeeunkid = mintApplicantunkid
                objImage_Tran._Imagename = mstrImagePath
                objImage_Tran._Isapplicant = True
                objImage_Tran._Referenceid = enImg_Email_RefId.Applicant_Module
                objImage_Tran._Transactionid = mintApplicantunkid
                'S.SANDEEP [04 JUN 2015] -- START
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'blnFlag = objImage_Tran.Insert()
                blnFlag = objImage_Tran.Insert(objDataOperation)
                'S.SANDEEP [04 JUN 2015] -- END
                If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            End If
            objImage_Tran = Nothing



            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.

            'If dtSkillTran IsNot Nothing Then
            '    objSkill_Tran._ApplicantUnkid = mintApplicantunkid
            '    objSkill_Tran._DataList = dtSkillTran
            '    blnFlag = objSkill_Tran.InsertUpdateDelete_SkillTran()
            '    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
            'End If
            If dtSkillTran IsNot Nothing Then

                Dim dt() As DataRow = dtSkillTran.Select("AUD=''")
                If dt.Length = dtSkillTran.Rows.Count Then
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "rcapplicant_master", mintApplicantunkid, "applicantunkid", 2) Then
                        blnToInsert = True
                    End If
                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapplicantskill_tran", "skilltranunkid", -1, 2, 0) = False Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If
                Else
                    objSkill_Tran._ApplicantUnkid = mintApplicantunkid
                    objSkill_Tran._DataList = dtSkillTran
                    blnFlag = objSkill_Tran.InsertUpdateDelete_SkillTran()
                    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    blnToInsert = False
                End If
                'Anjan (12 Oct 2011)-End 
            End If



            If dtQualifyTran IsNot Nothing Then
                'Anjan (12 Oct 2011)-Start
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.

                'objQualify_Tran._ApplicantUnkid = mintApplicantunkid
                'objQualify_Tran._DataList = dtQualifyTran
                'blnFlag = objQualify_Tran.InsertUpdateDelete_QualificationTran()
                'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)


                Dim dt() As DataRow = dtQualifyTran.Select("AUD=''")
                If dt.Length = dtQualifyTran.Rows.Count Then
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "rcapplicant_master", mintApplicantunkid, "applicantunkid", 2) Then
                        blnToInsert = True
                    End If
                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapplicantqualification_tran", "qualificationtranunkid", -1, 2, 0) = False Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If
                Else
                    objQualify_Tran._ApplicantUnkid = mintApplicantunkid
                    objQualify_Tran._DataList = dtQualifyTran
                    blnFlag = objQualify_Tran.InsertUpdateDelete_QualificationTran()
                    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    blnToInsert = False
                End If
                'Anjan (12 Oct 2011)-End 

            End If

            If dtJobHistory IsNot Nothing Then

                'Anjan (12 Oct 2011)-Start
                'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
                'objJobHistory._Applicantunkid = mintApplicantunkid
                'objJobHistory._DataList = dtJobHistory
                'blnFlag = objJobHistory.InsertUpdateDelete_JobHistoryTran()
                'If blnFlag = False Then objDataOperation.ReleaseTransaction(False)

                Dim dt() As DataRow = dtJobHistory.Select("AUD=''")
                If dt.Length = dtJobHistory.Rows.Count Then
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "rcapplicant_master", mintApplicantunkid, "applicantunkid", 2) Then
                        blnToInsert = True
                    End If
                    'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcjobhistory", "jobhistorytranunkid", -1, 2, 0) = False Then
                    '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    '    Throw exForce
                    'End If
                Else
                    objJobHistory._Applicantunkid = mintApplicantunkid
                    objJobHistory._DataList = dtJobHistory
                    blnFlag = objJobHistory.InsertUpdateDelete_JobHistoryTran()
                    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    blnToInsert = False
                End If
                'Anjan (12 Oct 2011)-End 

            End If

            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If blnToInsert = True Then
                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapplicantskill_tran", "skilltranunkid", -1, 2, 0) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If
            'Anjan (12 Oct 2011)-End 



            'Pinkal (06-Feb-2012) -- Start
            'Enhancement : TRA Changes

            If dtReferences IsNot Nothing Then
                Dim dt() As DataRow = dtReferences.Select("AUD=''")
                If dt.Length = dtReferences.Rows.Count Then
                    If clsCommonATLog.IsTableDataUpdate("atcommon_tranlog", "rcapplicant_master", mintApplicantunkid, "applicantunkid", 2) Then
                        blnToInsert = True
                    End If
                Else
                    objReference_Tran._Applicantunkid = mintApplicantunkid
                    objReference_Tran._dtReferences = dtReferences
                    'objReference_Tran._Userunkid = mintUserunkid
                    blnFlag = objReference_Tran.InsertUpdateDelete_ReferencesTran()
                    If blnFlag = False Then objDataOperation.ReleaseTransaction(False)
                    blnToInsert = False
                End If
            End If
            If blnToInsert = True Then

                If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", mintApplicantunkid, "rcapp_reference_tran", "referencetranunkid", -1, 2, 0) = False Then
                    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If

            End If

            'Pinkal (06-Feb-2012) -- End


            objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (rcapplicant_master) </purpose>
    Public Function Delete(ByVal intUnkid As Integer _
                           , ByVal blnVoid As Boolean _
                           , ByVal intVoidUser As Integer _
                           , ByVal dtVoidDate As DateTime _
                           , ByVal strReason As String _
                           ) As Boolean
        'Sohail (25 Sep 2020) - [blnVoid, intVoidUser, dtVoidDate, strReason]
        'Sandeep [ 09 Oct 2010 ] -- Start
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        If isUsed(intUnkid) Then
            mstrMessage = Language.getMessage(mstrModuleName, 4, "Sorry, you cannot delete this Applicant. Reason : This Applicant is already linked with some transaction.")
            Return False
        End If
        'Sandeep [ 09 Oct 2010 ] -- End 

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation


        'Anjan (12 Oct 2011)-Start
        'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
        objDataOperation.BindTransaction()
        'Anjan (12 Oct 2011)-End 



        Try


            'Anjan (12 Oct 2011)-Start
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", intUnkid, "rcapplicantskill_tran", "skilltranunkid", -1, 3, 0) = False Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If
            'Anjan (12 Oct 2011)-End 


            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            'strQ = "DELETE FROM rcapplicant_master " & _
            '            "WHERE applicantunkid = @applicantunkid "
            strQ = "UPDATE rcapplicant_master SET " & _
                          "  isvoid = @isvoid " & _
                          ", voiduserunkid = @voiduserunkid " & _
                          ", voiddatetime = @voiddatetime " & _
                                ", voidreason = @voidreason " & _
                        "WHERE applicantunkid = @applicantunkid "
            'Sohail (25 Sep 2020) -- End


            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, blnVoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVoidUser.ToString)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtVoidDate)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strReason.ToString)
            'Sohail (25 Sep 2020) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            ''Anjan (12 Oct 2011)-Start
            ''ENHANCEMENT : AUDIT TRAIL MAINTENANCE.
            'dsList = clsCommonATLog.GetChildList(objDataOperation, "rcapplicantskill_tran", "skilltranunkid", intUnkid)


            'If dsList.Tables(0).Rows.Count > 0 Then
            '    For Each dtRow As DataRow In dsList.Tables(0).Rows
            '        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", intUnkid, "rcapplicantskill_tran", "skilltranunkid", dtRow.Item("skilltranunkid"), 3, 3) = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    Next
            'Else
            'If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", intUnkid, "rcapplicantskill_tran", "skilltranunkid", -1, 3, 0) = False Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'End If

            'strQ = "DELETE FROM rcapplicantskill_tran " & _
            '                           "WHERE skilltranunkid = @skilltranunkid "

            'objDataOperation.ClearParameters()
            'objDataOperation.AddParameter("@skilltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            'Call objDataOperation.ExecNonQuery(strQ)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            'dsList = clsCommonATLog.GetChildList(objDataOperation, "rcapplicantqualification_tran", "qualificationtranunkid", intUnkid)

            'If dsList.Tables(0).Rows.Count > 0 Then
            '    For Each dtrow As DataRow In dsList.Tables(0).Rows
            '        If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", intUnkid, "rcapplicantqualification_tran", "qualificationtranunkid", dtrow.Item("qualificationtranunkid"), 3, 3) = False Then
            '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '            Throw exForce
            '        End If
            '    Next
            'Else
            '    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", intUnkid, "rcapplicantqualification_tran", "qualificationtranunkid", -1, 3, 0) = False Then
            '        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If

            'strQ = "DELETE FROM rcapplicantqualification_tran " & _
            '                              "WHERE qualificationtranunkid = @qualificationtranunkid "


            'objDataOperation.ClearParameters()
            'objDataOperation.AddParameter("@qualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            'Call objDataOperation.ExecNonQuery(strQ)

            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If

            objDataOperation.ReleaseTransaction(True)


            'Anjan (12 Oct 2011)-End 


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Sandeep [ 09 Oct 2010 ] -- Start
        Dim dsTables As DataSet = Nothing
        Dim blnIsUsed As Boolean = False
        'Sandeep [ 09 Oct 2010 ] -- End 

        Dim objDataOperation As New clsDataOperation

        Try
            'Sandeep [ 09 Oct 2010 ] -- Start
            'strQ = "<Query>"
            strQ = "SELECT " & _
                       "	TABLE_NAME AS TableName " & _
                       "FROM INFORMATION_SCHEMA.COLUMNS " & _
                       "WHERE COLUMN_NAME = 'applicantunkid' "

            dsTables = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            strQ = ""
            'Sandeep [ 09 Oct 2010 ] -- End 

            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            'Sandeep [ 09 Oct 2010 ] -- Start
            'dsList = objDataOperation.ExecQuery(strQ, "List")
            'If objDataOperation.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
            '    Throw exForce
            'End If
            'Return dsList.Tables(0).Rows.Count > 0

            For Each dtRow As DataRow In dsTables.Tables("List").Rows
                If dtRow.Item("TableName") = "rcapplicant_master" Then Continue For

                strQ = "SELECT applicantunkid FROM " & dtRow.Item("TableName").ToString & " WHERE applicantunkid = @applicantunkid "

                dsList = objDataOperation.ExecQuery(strQ, "Used")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

                If dsList.Tables("Used").Rows.Count > 0 Then
                    blnIsUsed = True
                    Exit For
                End If
            Next

            mstrMessage = ""

            Return blnIsUsed
            'Sandeep [ 09 Oct 2010 ] -- End 
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <param name="strCode">Applicant code</param>
    ''' <param name="intUnkid">Applicantunkid</param>
    ''' <param name="strEmail">Unique Email (Mandatory)</param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByVal strEmail As String = "") As Boolean
        'Sohail (05 Dec 2016) [Removed = , Optional ByVal strFirstname As String = "", Optional ByVal strLastname As String = ""]
        'Public Function isExist(Optional ByVal strCode As String = "", Optional ByVal intUnkid As Integer = -1, Optional ByVal strEmail As String = "", Optional ByVal intVacancyUnkId As Integer = 0, Optional ByVal strFirstname As String = "", Optional ByVal strLastname As String = "") As Boolean -'Sohail (05 May 2012) 'TRA - ENHANCEMENT    :   Now one applicant can apply for multiple vacancies and vacancy maping is created to save multiple vacancied for an applicant.

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        'objDataOperation = New clsDataOperation
        Dim objDataOperation As New clsDataOperation


        Try
            strQ = "SELECT " & _
              "  applicantunkid " & _
              ", applicant_code " & _
              ", titleunkid " & _
              ", firstname " & _
              ", surname " & _
              ", othername " & _
              ", gender " & _
              ", email " & _
              ", present_address1 " & _
              ", present_address2 " & _
              ", present_countryunkid " & _
              ", present_stateunkid " & _
              ", present_province " & _
              ", present_post_townunkid " & _
              ", present_zipcode " & _
              ", present_road " & _
              ", present_estate " & _
              ", present_plotno " & _
              ", present_mobileno " & _
              ", present_alternateno " & _
              ", present_tel_no " & _
              ", present_fax " & _
              ", perm_address1 " & _
              ", perm_address2 " & _
              ", perm_countryunkid " & _
              ", perm_stateunkid " & _
              ", perm_province " & _
              ", perm_post_townunkid " & _
              ", perm_zipcode " & _
              ", perm_road " & _
              ", perm_estate " & _
              ", perm_plotno " & _
              ", perm_mobileno " & _
              ", perm_alternateno " & _
              ", perm_tel_no " & _
              ", perm_fax " & _
              ", birth_date " & _
              ", marital_statusunkid " & _
              ", anniversary_date " & _
              ", language1unkid " & _
              ", language2unkid " & _
              ", language3unkid " & _
              ", language4unkid " & _
              ", nationality " & _
              ", userunkid " & _
              ", vacancyunkid " & _
              ", ISNULL(memberships,'') AS memberships  " & _
              ", ISNULL(achievements,'') AS achievements  " & _
              ", ISNULL(journalsresearchpapers,'') AS journalsresearchpapers  " & _
              ", ISNULL(rcapplicant_master.mother_tongue,'') AS mother_tongue " & _
              ", rcapplicant_master.isimpaired " & _
              ", ISNULL(rcapplicant_master.impairment,'') AS impairment " & _
              ", ISNULL(rcapplicant_master.current_salary,0) AS current_salary " & _
              ", ISNULL(rcapplicant_master.expected_salary,0) AS expected_salary " & _
              ", ISNULL(rcapplicant_master.expected_benefits,'') AS expected_benefits " & _
              ", rcapplicant_master.willing_to_relocate " & _
              ", rcapplicant_master.willing_to_travel " & _
              ", ISNULL(rcapplicant_master.notice_period_days,0) AS notice_period_days " & _
             "FROM rcapplicant_master " & _
             "WHERE rcapplicant_master.isvoid = 0 " & _
             "AND ISNULL(UserId, '') <> '' "
            'Nilay (13 Apr 2017) -- [mother_tongue, isimpaired, impairment, current_salary, expected_salary, expected_benefits, willing_to_relocate, willing_to_travel, notice_period_days]
            'Sohail (05 Dec 2016) - [AND ISNULL(UserId, '') <> '']
            'Sohail (30 May 2012) - [memberships, achievements, journalsresearchpapers]

            If strCode <> "" Then
                strQ &= " AND applicant_code = @code "
                objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)
            End If

            If intUnkid > 0 Then
                strQ &= " AND applicantunkid <> @applicantunkid"
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            If strEmail <> "" Then
                strQ &= " AND email = @email"
                objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrEmail.ToString)
            End If

            'Sohail (05 May 2012) -- Start
            'TRA - ENHANCEMENT
            'If intVacancyUnkId > 0 Then
            '    strQ &= " AND vacancyunkid = @vacancyunkid"
            '    objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyUnkId)
            'End If
            'Sohail (05 May 2012) -- End

            'Sohail (05 Dec 2016) -- Start
            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
            'If strFirstname <> "" Then
            '    strQ &= " AND firstname = @firstname"
            '    objDataOperation.AddParameter("@firstname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrFirstname.ToString)
            'End If
            'If strLastname <> "" Then
            '    strQ &= " AND surname = @surname"
            '    objDataOperation.AddParameter("@surname", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrSurname.ToString)
            'End If
            'Sohail (05 Dec 2016) -- End
            'objDataOperation.AddParameter("@code", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strCode)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            ' objDataOperation = Nothing
        End Try
    End Function



    'S.SANDEEP [ 25 DEC 2011 ] -- START
    'ENHANCEMENT : TRA CHANGES
    ''' <summary>
    ''' Modify By: Anjan
    ''' </summary>
    ''' <purpose> Get Applicant List  </purpose>
    ''' <param name="intApplicantType">0 = All, 1 = External Applicant , 2 = Internal Applicant</param>
    '''Public Function GetApplicantList(Optional ByVal strListName As String = "List", _
    '''                                Optional ByVal mblnAddSelect As Boolean = False, _
    '''                                Optional ByVal intApplicantId As Integer = -1, _
    '''                                Optional ByVal blnOnlyActive As Boolean = False _
    '''                               ) As DataSet
    Public Function GetApplicantList(Optional ByVal strListName As String = "List", _
                                    Optional ByVal mblnAddSelect As Boolean = False, _
                                    Optional ByVal intApplicantId As Integer = -1, _
                                     Optional ByVal blnOnlyActive As Boolean = False, _
                                     Optional ByVal intApplicantType As Integer = 0 _
                                   ) As DataSet
        'S.SANDEEP [ 25 DEC 2011 ] -- END




        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            If mblnAddSelect = True Then
                strQ = "SELECT ' ' AS applicant_code, ' ' + @Select AS applicantname, 0 AS applicantunkid UNION "
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 2, "Select"))
            End If

            strQ += "SELECT " & _
                        " CASE WHEN rcapplicant_master.employeeunkid > 0 THEN hremployee_master.employeecode ELSE rcapplicant_master.applicant_code END AS applicant_code " & _
                        ",CASE WHEN rcapplicant_master.employeeunkid > 0 THEN ISNULL(hremployee_master.firstname,'')+'  '+ISNULL(hremployee_master.othername,'')+'  '+ISNULL(hremployee_master.surname,'') ELSE ISNULL(rcapplicant_master.firstname,'')+'  '+ISNULL(rcapplicant_master.othername,'')+'  '+ISNULL(rcapplicant_master.surname,'') END AS applicantname " & _
                        ",rcapplicant_master.applicantunkid As applicantunkid " & _
                    "FROM rcapplicant_master " & _
                    "LEFT JOIN hremployee_master ON hremployee_master.employeeunkid = rcapplicant_master.employeeunkid " & _
                    "WHERE rcapplicant_master.isvoid = 0 "
            'Sohail (12 Nov 2020) - [LEFT JOIN hremployee_master] - Show employee code instead of applocant code for internal applicants.

            'Sandeep [ 21 APRIL 2011 ] -- Start
            'If blnOnlyActive = True Then
            '    strQ += " AND IsActive = 1"
            'End If
            'Sandeep [ 21 APRIL 2011 ] -- End 

            '================ Applicant ID
            If intApplicantId > 0 Then
                strQ += " AND rcapplicant_master.applicantunkid = @applicantunkid"
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantId)
            End If



            'S.SANDEEP [ 25 DEC 2011 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Select Case intApplicantType
                Case enVacancyType.EXTERNAL_VACANCY  'EXTERNAL
                    'Sohail (25 Sep 2020) -- Start
                    'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    'strQ &= " AND ISNULL(employeecode,'') = '' "
                    strQ &= " AND ISNULL(rcapplicant_master.employeeunkid, 0) <= 0 "
                    'Sohail (25 Sep 2020) -- End
                Case enVacancyType.INTERNAL_VACANCY  'INTERNAL
                    'Sohail (25 Sep 2020) -- Start
                    'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    'strQ &= " AND ISNULL(employeecode,'') <> '' "
                    strQ &= " AND ISNULL(rcapplicant_master.employeeunkid, 0) > 0 "
                    'Sohail (25 Sep 2020) -- End
            End Select
            'S.SANDEEP [ 25 DEC 2011 ] -- END

            'Pinkal (18-Aug-2023) -- Start
            'Recruitment Issue - Not Matching Applicant Documents Count with Applicant Master List Applicants Count . 
            strQ &= " ORDER BY applicantname "
            'Pinkal (18-Aug-2023) -- End

            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApplicantList", mstrModuleName)
            Return Nothing
        Finally
            If dsList IsNot Nothing Then dsList.Dispose()
            dsList = Nothing
            If objDataOperation IsNot Nothing Then objDataOperation.Dispose()
            objDataOperation = Nothing

            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Get Applicant Qualification List  </purpose>
    Public Shared Function GetApplicantQualification(ByVal intApplicantId As Integer, Optional ByVal blnFlag As Boolean = False, Optional ByVal strList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            If blnFlag = True Then
                StrQ = "SELECT 0 As Id , @Select As Name UNION "
                'SHANI (22 APR 2015)-START
                'Issue : Error in Scan Attachment 
                'objDataOperation.AddParameter("@Select", SqlDbType.Int, eZeeDataType.INT_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 3, "Select"))
                'SHANI (22 APR 2015)--END 
            End If


            StrQ &= "SELECT " & _
                    "	 hrqualification_master.qualificationunkid AS Id " & _
                    "	,hrqualification_master.qualificationname AS Name " & _
                    "FROM rcapplicantqualification_tran " & _
                    "	JOIN hrqualification_master ON rcapplicantqualification_tran.qualificationunkid = hrqualification_master.qualificationunkid " & _
                    "WHERE applicantunkid = @AppId AND rcapplicantqualification_tran.isvoid = 0 " & _
                   "ORDER BY Id"

            'S.SANDEEP [24-Apr-2018] -- START
            'ISSUE/ENHANCEMENT : {#0002213|#ARUTI-128}
            '-> Added Information
            '-> AND rcapplicantqualification_tran.isvoid = 0
            'S.SANDEEP [24-Apr-2018] -- END

            'SHANI (22 APR 2015)-START - 
            'Issue : Error in Scan Attachment 
            '[ORDER BY hrqualification_master.qualificationunkid ]
            'SHANI (22 APR 2015)--END 

            objDataOperation.AddParameter("@AppId", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantId.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApplicantQualification", mstrModuleName)
            Return Nothing
        Finally
            dsList = Nothing
            exForce = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sandeep
    ''' </summary>
    ''' <purpose> Get Applicant Job History List  </purpose>
    Public Shared Function GetApplicantJobHistory(ByVal intAppId As Integer, Optional ByVal blnFlag As Boolean = False, Optional ByVal strList As String = "List") As DataSet
        Dim StrQ As String = String.Empty
        Dim dsList As New DataSet
        Dim exForce As Exception
        Try
            Dim objDataOperation As New clsDataOperation

            If blnFlag = True Then
                StrQ = "SELECT 0 As Id, @Select As Name UNION "

                'SHANI (22 APR 2015)-START
                'Issue : Error in Scan Attachment 
                objDataOperation.AddParameter("@Select", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Select"))
                'SHANI (22 APR 2015)--END 


            End If

            StrQ &= "SELECT " & _
                         "	 jobhistorytranunkid AS Id " & _
                         "  ,ISNULL(companyname,'')AS Name " & _
                         "FROM rcjobhistory " & _
                         "WHERE applicantunkid = @AppId "

            objDataOperation.AddParameter("@AppId", SqlDbType.Int, eZeeDataType.INT_SIZE, intAppId.ToString)

            dsList = objDataOperation.ExecQuery(StrQ, strList)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApplicantJobHistory", mstrModuleName)
            Return Nothing
        End Try
    End Function


    'Sandeep [ 09 Oct 2010 ] -- Start

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function UpdateImport_Status(ByVal intApplicantunkid As Integer, ByVal intVacancyId As Integer, ByVal intUserId As Integer) As Boolean
    Public Function UpdateImport_Status(ByVal intApplicantunkid As Integer, ByVal intVacancyId As Integer, ByVal intUserId As Integer, ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        'S.SANDEEP [ 14 May 2013 ] -- START
        'ENHANCEMENT : TRA ENHANCEMENT
        'Public Function UpdateImport_Status(ByVal intApplicantunkid As Integer) As Boolean
        'S.SANDEEP [ 14 May 2013 ] -- END


        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try

            'S.SANDEEP [ 14 May 2013 ] -- START
            'ENHANCEMENT : TRA ENHANCEMENT
            'strQ = "UPDATE rcapplicant_master SET " & _
            '       " isimport = 1 " & _
            '       "WHERE applicantunkid = @applicantunkid "
            strQ = "UPDATE rcapp_vacancy_mapping SET " & _
                   " isimport = 1,importuserunkid = @importuserunkid,importdatetime = @importdatetime " & _
                   "WHERE applicantunkid = @applicantunkid AND vacancyunkid = @vacancyunkid AND ISNULL(isimport,0) = 0 "
            'S.SANDEEP [ 14 May 2013 ] -- END

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyId)
            objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantunkid)
            objDataOperation.AddParameter("@importuserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserId)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@importdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
            objDataOperation.AddParameter("@importdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
            'Shani(24-Aug-2015) -- End

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If


            Return True
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: UpdateImport_Status; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sandeep [ 09 Oct 2010 ] -- End 

    'S.SANDEEP [ 30 JULY 2011 ] -- START
    'ENHANCEMENT : I/E APPLICANT RELATED DATA VIA WEB

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Public Function Synchronize_Data(ByVal blnIsImport As Boolean, Optional ByVal bw As BackgroundWorker = Nothing) As Boolean 'Sohail (09 Feb 2012)
    Public Function Synchronize_Data(ByVal blnIsImport As Boolean, _
                                     ByVal intApplicantCodeNotype As Integer, _
                                     ByVal strApplicantCodePrifix As String, _
                                     ByVal strDatabaseServer As String, _
                                     ByVal intDatabaseServerSetting As Integer, _
                                     ByVal strAuthenticationCode As String, _
                                     ByVal strDatabaseName As String, _
                                     ByVal strDatabaseUserName As String, _
                                     ByVal strDatabaseUserPassword As String, _
                                     ByVal strDatabaseOwner As String, _
                                     ByVal intWebClientCode As Integer, _
                                     ByVal strConfigDatabaseName As String, _
                                     ByVal blnIsImgInDataBase As Boolean, _
                                     ByVal strPhotoPath As String, _
                                     ByVal strDocument_Path As String, _
                                     ByVal intUserunkid As Integer, _
                                     ByVal strArutiSelfServiceURL As String, _
                                     ByVal strApplicantDeclaration As String, _
                                     ByVal intTimeZoneMinuteDifference As Integer, _
                                     ByVal blnQualificationCertificateAttachmentMandatory As Boolean, _
                                     ByVal dtCurrentDateAndTime As DateTime, _
                                     ByVal strCompanyCode As String, _
                                     ByVal mintCompID As Integer, _
                                     ByVal strDateFormat As String, _
                                     ByVal strDateSeparator As String, _
                                     ByVal strAdminEmail As String, _
                                     ByVal blnMiddleNameMandatory As Boolean, _
                                     ByVal blnGenderMandatory As Boolean, _
                                     ByVal blnBirthDateMandatory As Boolean, _
                                     ByVal blnAddress1Mandatory As Boolean, _
                                     ByVal blnAddress2Mandatory As Boolean, _
                                     ByVal blnCountryMandatory As Boolean, _
                                     ByVal blnStateMandatory As Boolean, _
                                     ByVal blnCityMandatory As Boolean, _
                                     ByVal blnPostCodeMandatory As Boolean, _
                                     ByVal blnMaritalStatusMandatory As Boolean, _
                                     ByVal blnLanguage1Mandatory As Boolean, _
                                     ByVal blnNationalityMandatory As Boolean, _
                                     ByVal blnOneSkillMandatory As Boolean, _
                                     ByVal blnOneQualificationMandatory As Boolean, _
                                     ByVal blnOneJobExperienceMandatory As Boolean, _
                                     ByVal blnOneReferenceMandatory As Boolean, _
                                     ByVal intApplicantQualificationSortBy As Integer, _
                                     ByVal blnRegionMandatory As Boolean, _
                                     ByVal blnHighestQualificationMandatory As Boolean, _
                                     ByVal blnEmployerMandatory As Boolean, _
                                     ByVal blnMotherTongueMandatory As Boolean, _
                                     ByVal blnCurrentSalaryMandatory As Boolean, _
                                     ByVal blnExpectedSalaryMandatory As Boolean, _
                                     ByVal blnExpectedBenefitsMandatory As Boolean, _
                                     ByVal blnNoticePeriodMandatory As Boolean, _
                                     ByVal blnEarliestPossibleStartDateMandatory As Boolean, _
                                     ByVal blnCommentsMandatory As Boolean, _
                                     ByVal blnVacancyFoundOutFromMandatory As Boolean, _
                                     ByVal blnCompanyNameJobHistoryMandatory As Boolean, _
                                     ByVal blnPositionHeldMandatory As Boolean, _
                                     ByVal blnSendJobConfirmationEmail As Boolean, _
                                          ByVal blnVacancyAlert As Boolean, _
                                     ByVal intMaxVacancyAlert As Integer, _
                                     ByVal blnAttachApplicantCV As Boolean, _
                                     ByVal blnSendEmail As Boolean, _
                                     ByVal strWebServiceLink As String, _
                                     Optional ByVal bw As BackgroundWorker = Nothing, _
                                     Optional ByVal blnOneCurriculamVitaeMandatory As Boolean = False, _
                                     Optional ByVal blnOneCoverLetterMandatory As Boolean = False, _
                                     Optional ByVal blnRecQualificationStartDateMandatory As Boolean = False, _
                                     Optional ByVal blnRecQualificationAwardDateMandatory As Boolean = False, _
                                     Optional ByVal intJobConfirmationTemplateId As Integer = 0, _
                                     Optional ByVal intJobAlertTemplateId As Integer = 0, _
                                     Optional ByVal intInternalJobAlertTemplateId As Integer = 0, _
                                     Optional ByVal blnShowArutisignature As Boolean = True _
                                     ) As Boolean
        'Hemant (30 Oct 2019) -- [intJobConfirmationTemplateId, intJobAlertTemplateId, intInternalJobAlertTemplateId, blnShowArutisignature]
        'Gajanan [29-AUG-2019] --'Enhancements: Settings for mandatory options in online recruitment [blnRecQualificationStartDateMandatory], [blnRecQualificationAwardDateMandatory]
        'Sohail (04 Jul 2019) - [blnOneCurriculamVitaeMandatory, blnOneCoverLetterMandatory]
        'Sohail (16 Nov 2018) - [strWebServiceLink]
        'Pinkal (18-May-2018) --  'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.[ ByVal blnAttachApplicantCV As Boolean, _]


        'Sohail (24 Jul 2017) - [blnCompanyNameJobHistoryMandatory, blnPositionHeldMandatory]
        'Nilay (13 Apr 2017) -- [intApplicantQualificationSortBy, blnRegionMandatory, blnHighestQualificationMandatory, blnEmployerMandatory, blnMotherTongueMandatory, 
        'blnCurrentSalaryMandatory, blnExpectedSalaryMandatory, blnExpectedBenefitsMandatory, blnNoticePeriodMandatory, blnEarliestPossibleStartDateMandatory, 
        'blnCommentsMandatory, blnVacancyFoundOutFromMandatory]

        'Sohail (05 Dec 2016) - [strDateFormat, strDateSeparator to blnOneReferenceMandatory]
        'Shani(24-Aug-2015) -- End

        'Pinkal (12-Feb-2018) -- 'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.[ByVal blnSendJobConfirmationEmail As Boolean, ByVal blnSendEmail As Boolean,  ByVal blnVacancyAlert As Boolean,  ByVal intMaxVacancyAlert As Integer]

        Dim mlinkweb, mLink As String
        Dim mdatabaseweb, mdatabaseserverweb, mdatabaseuserweb, mdatabasepasswordweb, mDatabaseOwnerweb As String 'msql As String
        'Dim ds As DataSet
        'Dim exforce As Exception
        Dim objDataOp = New clsDataOperation
        Try

            'Sohail (18 May 2015) -- Start
            'Enhancement - Importing and Exporting through Web Service.
            'If ConfigParameter._Object._DatabaseServerSetting = enDatabaseServerSetting.DEFAULT_SETTING Then 'Sohail (06 Jun 2012)
            '    msql = "select * from sys.servers where name='WWW.ARUTIHR.COM' and is_linked = 1"
            'Else
            '    msql = "select * from sys.servers where name='" & ConfigParameter._Object._DatabaseServer & "' and is_linked = 1"
            'End If 'Sohail (06 Jun 2012)

            'ds = objDataOp.ExecQuery(msql, "servers")

            'If objDataOp.ErrorMessage <> "" Then
            '    exforce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Throw exforce
            'End If

            'If ds.Tables(0).Rows.Count <= 0 Then
            '    mstrMessage = Language.getMessage(mstrModuleName, 11, "You cannot perform synchronization process. Reason: Database is not linked with web. Please contact Aruti support team.")
            '    Return False
            'End If
            'Sohail (18 May 2015) -- End


            Dim objNetCon As New clsNetConnectivity
            If objNetCon._Conected = False Then
                mstrMessage = Language.getMessage(mstrModuleName, 15, "Internet Connection : ") & objNetCon._ConnectionStatus
                Return False
            End If

            mlinkweb = String.Empty : mLink = String.Empty
            mDatabaseOwnerweb = String.Empty : mdatabasepasswordweb = String.Empty : mdatabaseserverweb = String.Empty : mdatabaseuserweb = String.Empty : mdatabaseweb = String.Empty


            'NOTE : THIS WILL NOT CHANGE ONCE DEFINED, IT CAN ONLY BE CHANGED WEB SERVER CHANGED

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._DatabaseServerSetting = enDatabaseServerSetting.DEFAULT_SETTING Then 'Sohail (06 Jun 2012)
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'If intDatabaseServerSetting = enDatabaseServerSetting.DEFAULT_SETTING Then 'Sohail (06 Jun 2012)
            '    'Shani(24-Aug-2015) -- End

            '    mdatabaseserverweb = "www.arutihr.com"
            '    mdatabaseweb = "arutihrms"
            '    mdatabaseuserweb = "ezeearuti"
            '    mdatabasepasswordweb = "aRutipRo@99"
            '    mDatabaseOwnerweb = "dbo"

            '    'Sohail (06 Jun 2012) -- Start
            '    'TRA - ENHANCEMENT
            'Else

            '    'Shani(24-Aug-2015) -- Start
            '    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            '    'mdatabaseserverweb = ConfigParameter._Object._DatabaseServer
            '    'mdatabaseweb = ConfigParameter._Object._DatabaseName
            '    'mdatabaseuserweb = ConfigParameter._Object._DatabaseUserName
            '    'mdatabasepasswordweb = ConfigParameter._Object._DatabaseUserPassword
            '    'mDatabaseOwnerweb = ConfigParameter._Object._DatabaseOwner
            '    mdatabaseserverweb = strDatabaseServer
            '    mdatabaseweb = strDatabaseName
            '    mdatabaseuserweb = strDatabaseUserName
            '    mdatabasepasswordweb = strDatabaseUserPassword
            '    mDatabaseOwnerweb = strDatabaseOwner
            '    'Shani(24-Aug-2015) -- End


            'End If
            'Sohail (16 Nov 2018) -- End
            'Sohail (06 Jun 2012) -- End

            'mlinkweb = "[" & mdatabaseserverweb & "]" & ".[" & mdatabaseweb & "].[" & mDatabaseOwnerweb & "]." 'Sohail (16 Nov 2018)


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim StrCode As String = Company._Object._Code
            Dim StrCode As String = strCompanyCode
            'Shani(24-Aug-2015) -- End


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim intClientCode As Integer = ConfigParameter._Object._WebClientCode
            Dim intClientCode As Integer = intWebClientCode
            'Shani(24-Aug-2015) -- End


            'Sohail (18 May 2015) -- Start
            'Enhancement - Importing and Exporting through Web Service.
            'If objDataOp.RecordCount("SELECT * FROM " & mlinkweb & "cfcompany_info Where companyunkid = " & intClientCode & " AND company_code = '" & StrCode & "' AND authentication_code = '" & ConfigParameter._Object._AuthenticationCode & "'") <= 0 Then
            '    'If objDataOp.RecordCount("SELECT * FROM " & mlinkweb & "cfcompany_info Where companyunkid = '" & intClientCode & "' AND authentication_code = '" & ConfigParameter._Object._AuthenticationCode & "'") <= 0 Then
            '    'Sohail (13 Jun 2012) -- Start
            '    'TRA - ENHANCEMENT
            '    If objDataOp.ErrorMessage <> "" Then
            '        mstrMessage = mstrModuleName & "; Proc:Synchronize_Data; " & objDataOp.ErrorMessage
            '        Return False
            '    End If
            '    'Sohail (13 Jun 2012) -- End
            '    mstrMessage = Language.getMessage(mstrModuleName, 10, "Sorry, you cannot do selected operation. Reason Invalid webclient code, company code or Authentication code.")
            '    Return False
            'End If
            'objDataOp = Nothing
            'Sohail (18 May 2015) -- End

            bw.ReportProgress(0)

            Select Case blnIsImport
                Case True

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If ImportFromWeb(mlinkweb, StrCode, intClientCode, mdatabaseserverweb, bw) = False Then 'Sohail (09 Feb 2012)
                    'Sohail (16 Nov 2018) -- Start
                    'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                    'If ImportFromWeb(mlinkweb, StrCode, _
                    '                 intClientCode, mdatabaseserverweb, bw, _
                    '                 intApplicantCodeNotype, strApplicantCodePrifix, _
                    '                 strConfigDatabaseName, blnIsImgInDataBase, _
                    '                 strPhotoPath, strDocument_Path, intDatabaseServerSetting, _
                    '                 strDatabaseServer, strAuthenticationCode, intUserunkid, _
                    '                 strArutiSelfServiceURL, dtCurrentDateAndTime, mintCompID) = False Then
                    If ImportFromWeb(mlinkweb, StrCode, _
                                     intClientCode, mdatabaseserverweb, bw, _
                                     intApplicantCodeNotype, strApplicantCodePrifix, _
                                     strConfigDatabaseName, blnIsImgInDataBase, _
                                     strPhotoPath, strDocument_Path, intDatabaseServerSetting, _
                                     strDatabaseServer, strAuthenticationCode, intUserunkid, _
                                    strArutiSelfServiceURL, dtCurrentDateAndTime, mintCompID, strWebServiceLink) = False Then
                        'Sohail (16 Nov 2018) -- End
                        'Shani(24-Aug-2015) -- End

                        mstrMessage = Language.getMessage(mstrModuleName, 16, "Data Import process failed.") & vbCrLf & vbCrLf & mstrMessage : Return False
                    Else
                        'Sohail (20 Jun 2023) -- Start
                        'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
                        'mstrMessage = Language.getMessage(mstrModuleName, 17, "Data Imported Successfully.") : Return True
                        If mstrMessage.Trim <> "" Then
                            mstrMessage = Language.getMessage(mstrModuleName, 17, "Data Imported Successfully.") & vbCrLf & vbCrLf & mstrMessage : Return True
                        Else
                        mstrMessage = Language.getMessage(mstrModuleName, 17, "Data Imported Successfully.") : Return True
                    End If
                        'Sohail (20 Jun 2023) -- End
                    End If
                Case False
                    'Sohail (28 Mar 2012) -- Start
                    'TRA - ENHANCEMENT

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If ExportToWeb(mlinkweb, StrCode, intClientCode, bw) = False Then
                    'Sohail (05 Dec 2016) -- Start
                    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                    'If ExportToWeb(mlinkweb, StrCode, intClientCode, bw, _
                    '               strDatabaseServer, intDatabaseServerSetting, _
                    '               strAuthenticationCode, strConfigDatabaseName, _
                    '               strApplicantDeclaration, intTimeZoneMinuteDifference, _
                    '               blnQualificationCertificateAttachmentMandatory) = False Then
                    If ExportToWeb(mlinkweb, StrCode, intClientCode, bw, _
                                   strDatabaseServer, intDatabaseServerSetting, _
                                   strAuthenticationCode, strConfigDatabaseName, _
                                   strApplicantDeclaration, intTimeZoneMinuteDifference, _
                                   blnQualificationCertificateAttachmentMandatory, _
                                   strDateFormat, _
                                   strDateSeparator, _
                                   strAdminEmail, _
                                   blnMiddleNameMandatory, _
                                   blnGenderMandatory, _
                                   blnBirthDateMandatory, _
                                   blnAddress1Mandatory, _
                                   blnAddress2Mandatory, _
                                   blnCountryMandatory, _
                                   blnStateMandatory, _
                                   blnCityMandatory, _
                                   blnPostCodeMandatory, _
                                   blnMaritalStatusMandatory, _
                                   blnLanguage1Mandatory, _
                                   blnNationalityMandatory, _
                                   blnOneSkillMandatory, _
                                   blnOneQualificationMandatory, _
                                   blnOneJobExperienceMandatory, _
                                   blnOneReferenceMandatory, _
                                   intApplicantQualificationSortBy, _
                                   blnRegionMandatory, _
                                   blnHighestQualificationMandatory, _
                                   blnEmployerMandatory, _
                                   blnMotherTongueMandatory, _
                                   blnCurrentSalaryMandatory, _
                                   blnExpectedSalaryMandatory, _
                                   blnExpectedBenefitsMandatory, _
                                   blnNoticePeriodMandatory, _
                                   blnEarliestPossibleStartDateMandatory, _
                                   blnCommentsMandatory, _
                                   blnVacancyFoundOutFromMandatory, _
                                   blnCompanyNameJobHistoryMandatory, _
                                   blnPositionHeldMandatory, _
                                   blnSendJobConfirmationEmail, _
                                      blnVacancyAlert, _
                                     intMaxVacancyAlert, _
                                   blnAttachApplicantCV, _
                                   blnSendEmail, _
                                   strWebServiceLink, _
                                   blnOneCurriculamVitaeMandatory, _
                                   blnOneCoverLetterMandatory, _
                                   blnRecQualificationStartDateMandatory, _
                                   blnRecQualificationAwardDateMandatory, _
                                   intJobConfirmationTemplateId, _
                                   intJobAlertTemplateId, _
                                   intInternalJobAlertTemplateId, _
                                   blnShowArutisignature _
                                   ) = False Then
                        'Hemant (30 Oct 2019) -- [intJobConfirmationTemplateId, intJobAlertTemplateId, intInternalJobAlertTemplateId, blnShowArutisignature]
                        'Gajanan [29-AUG-2019] -- 'Enhancements: Settings for mandatory options in online recruitment [blnRecQualificationStartDateMandatory] ,[blnRecQualificationAwardDateMandatory]
                        'Sohail (04 Jul 2019) - [blnOneCurriculamVitaeMandatory, blnOneCoverLetterMandatory]
                        'Sohail (16 Nov 2018) - [strWebServiceLink]
                        'Pinkal (18-May-2018) -- 'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.[blnAttachApplicantCV, _]


                        'Pinkal (12-Feb-2018) --  'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.[blnSendJobConfirmationEmail,blnSendEmail]

                        'Sohail (24 Jul 2017) - [blnCompanyNameJobHistoryMandatory, blnPositionHeldMandatory]
                        'Nilay (13 Apr 2017) -- [intApplicantQualificationSortBy, blnRegionMandatory, blnHighestQualificationMandatory, blnEmployerMandatory,
                        'blnMotherTongueMandatory, blnCurrentSalaryMandatory, blnExpectedSalaryMandatory, blnExpectedBenefitsMandatory, blnNoticePeriodMandatory
                        'blnEarliestPossibleStartDateMandatory, blnCommentsMandatory, blnVacancyFoundOutFromMandatory]

                        'Sohail (05 Dec 2016) -- End
                        'Shani(24-Aug-2015) -- End

                        'If ExportToWeb(mlinkweb, StrCode, intClientCode) = False Then
                        'Sohail (28 Mar 2012) -- End
                        mstrMessage = Language.getMessage(mstrModuleName, 8, "Data Export process failed.") : Return False
                    Else
                        mstrMessage = Language.getMessage(mstrModuleName, 9, "Data Exported Successfully.") : Return True
                    End If
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Synchronize_Data", mstrModuleName)
            Return False
        Finally
            mlinkweb = String.Empty : mLink = String.Empty
            mDatabaseOwnerweb = String.Empty : mdatabasepasswordweb = String.Empty : mdatabaseserverweb = String.Empty : mdatabaseuserweb = String.Empty : mdatabaseweb = String.Empty
        End Try
    End Function

    'Sohail (28 Mar 2012) -- Start
    'TRA - ENHANCEMENT
    'Private Function ExportToWeb(ByVal mlinkweb As String, ByVal StrCode As String, ByVal intClientCode As Integer) As Boolean
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim mLinkDesk As String = String.Empty
    '    Dim objDataOp As New clsDataOperation
    '    Try

    '        'objDataOp.BindTransaction()

    '        Cursor.Current = Cursors.WaitCursor

    '        mLinkDesk = Company._Object._ConfigDatabaseName & ".."

    '        ''<TODO TO BE REMOVED LATER.>
    '        'If objDataOp.RecordCount("SELECT * FROM " & mLinkDesk & "cfcountry_master") <> objDataOp.RecordCount("SELECT * FROM " & mlinkweb & "cfcountry_master") Then
    '        '    objDataOp.ExecNonQuery("delete from " & mlinkweb & "cfcountry_master")
    '        '    StrQ = "INSERT INTO " & mlinkweb & "cfcountry_master ( countryunkid , alias,country_name ,currency ,isrighttoleft ,country_name1 ,country_name2 ) " & _
    '        '                "Select countryunkid, alias ,country_name ,currency,isrighttoleft ,country_name1,country_name2 FROM " & mLinkDesk & "cfcountry_master "

    '        '    objDataOp.ExecNonQuery(StrQ)
    '        '    If objDataOp.ErrorMessage <> "" Then
    '        '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '        '        Throw exForce
    '        '    End If
    '        'End If

    '        '///////// DELETE,INSERT CITY ///////// START
    '        StrQ = "DELETE FROM " & mlinkweb & "cfcity_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        StrQ = "INSERT INTO " & mlinkweb & "cfcity_master(cityunkid,countryunkid,stateunkid,code,name,isactive,name1,name2,Comp_Code,companyunkid) select cityunkid,countryunkid,stateunkid,code,name,isactive,name1,name2,'" & StrCode & "'," & intClientCode & " from  " & mLinkDesk & "cfcity_master WHERE isactive = 1 "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '///////// DELETE,INSERT CITY ///////// END

    '        '///////// DELETE,INSERT STATE ///////// START
    '        StrQ = "DELETE FROM " & mlinkweb & "cfstate_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Sohail (28 Jan 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        StrQ = "INSERT INTO " & mlinkweb & "cfstate_master(stateunkid,countryunkid,code,name,isactive,name1,name2,Comp_Code,companyunkid) select stateunkid,countryunkid,code,name,isactive,name1,name2,'" & StrCode & "'," & intClientCode & " from  " & mLinkDesk & "cfstate_master WHERE isactive = 1 "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Sohail (28 Jan 2012) -- End
    '        '///////// DELETE,INSERT STATE///////// END

    '        '///////// DELETE,INSERT ZIPCODE ///////// START
    '        StrQ = "DELETE FROM " & mlinkweb & "cfzipcode_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        StrQ = "INSERT INTO " & mlinkweb & "cfzipcode_master(zipcodeunkid,countryunkid,stateunkid,cityunkid,zipcode_code,zipcode_no,isactive,Comp_Code,companyunkid)select zipcodeunkid,countryunkid,stateunkid,cityunkid,zipcode_code,zipcode_no,isactive,'" & StrCode & "'," & intClientCode & " from  " & mLinkDesk & "cfzipcode_master WHERE isactive = 1 "
    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '///////// DELETE,INSERT ZIPCODE ///////// END

    '        '///////// DELETE,INSERT COMMON MASTER ///////// START
    '        StrQ = "DELETE FROM " & mlinkweb & "cfcommon_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        StrQ = "INSERT INTO " & mlinkweb & "cfcommon_master(Comp_Code,companyunkid,masterunkid,code,alias,name,name1,name2,mastertype,isactive,isrefreeallowed)select '" & StrCode & "' as Comp_Code," & intClientCode & " as companyunkid, masterunkid,code,alias,name,name1,name2,mastertype,isactive,isrefreeallowed from cfcommon_master WHERE isactive = 1 "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '///////// DELETE,INSERT COMMON MASTER///////// END

    '        '///////// DELETE,INSERT SKILLS///////// START
    '        StrQ = "DELETE FROM " & mlinkweb & "hrskill_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        StrQ = "INSERT INTO " & mlinkweb & "hrskill_master(Comp_Code,companyunkid,skillunkid,skillcode,skillname,skillcategoryunkid,description,isactive,skillname1,skillname2)select '" & StrCode & "' as Comp_Code," & intClientCode & " as companyunkid,skillunkid,skillcode,skillname,skillcategoryunkid,description,isactive,skillname1,skillname2 from hrskill_master WHERE isactive = 1 "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '///////// DELETE,INSERT SKILLS///////// END

    '        '///////// DELETE,INSERT INSTITUTE///////// START
    '        StrQ = "DELETE FROM " & mlinkweb & "hrinstitute_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        StrQ = "INSERT INTO " & mlinkweb & "hrinstitute_master(Comp_Code,companyunkid,instituteunkid,institute_code,institute_name,institute_address,telephoneno,institute_fax,institute_email,description,contact_person,stateunkid,cityunkid,countryunkid,ishospital,isactive)select '" & StrCode & "' as Comp_Code," & intClientCode & " as companyunkid,instituteunkid,institute_code,institute_name,institute_address,telephoneno,institute_fax,institute_email,description,contact_person,stateunkid,cityunkid,countryunkid,ishospital,isactive from hrinstitute_master WHERE isactive = 1 "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '///////// DELETE,INSERT INSTITUTE///////// END

    '        '///////// DELETE,INSERT QUALIFICATION///////// START
    '        StrQ = "DELETE FROM " & mlinkweb & "hrqualification_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Sohail (28 Jan 2012) -- Start
    '        'TRA - ENHANCEMENT
    '        'StrQ = "INSERT INTO " & mlinkweb & "hrqualification_master(Comp_Code,companyunkid,qualificationunkid,qualificationcode,qualificationname,qualificationgroupunkid,description,isactive,qualificationname1,qualificationname2)select '" & StrCode & "' as Comp_Code," & intClientCode & " as companyunkid,qualificationunkid,qualificationcode,qualificationname,qualificationgroupunkid,description,isactive,qualificationname1,qualificationname2 from hrqualification_master WHERE isactive = 1 "
    '        StrQ = "INSERT INTO " & mlinkweb & "hrqualification_master(Comp_Code,companyunkid,qualificationunkid,qualificationcode,qualificationname,qualificationgroupunkid,description,isactive,qualificationname1,qualificationname2,resultgroupunkid)select '" & StrCode & "' as Comp_Code," & intClientCode & " as companyunkid,qualificationunkid,qualificationcode,qualificationname,qualificationgroupunkid,description,isactive,qualificationname1,qualificationname2,resultgroupunkid from hrqualification_master WHERE isactive = 1 "
    '        'Sohail (28 Jan 2012) -- End

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '///////// DELETE,INSERT QUALIFICATION///////// END

    '        '///////// DELETE,INSERT VACANCY///////// START
    '        StrQ = "DELETE FROM " & mlinkweb & "rcvacancy_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'Sohail (30 Dec 2011) -- Start
    '        'StrQ = "INSERT INTO " & mlinkweb & "rcvacancy_master(Comp_Code,vacancyunkid,vacancytitle,openingdate,closingdate,interview_startdate,interview_closedate,noofposition,experience,responsibilities_duties,remark,isvoid,companyunkid) " & _
    '        '"SELECT '" & StrCode & "' as Comp_Code,vacancyunkid,vacancytitle,openingdate,closingdate,interview_startdate,interview_closedate,noofposition,experience,responsibilities_duties,remark,isvoid ," & intClientCode & " as companyunkid from rcvacancy_master WHERE ISNULL(isvoid,0) = 0 "
    '        StrQ = "INSERT INTO " & mlinkweb & "rcvacancy_master(Comp_Code,vacancyunkid,vacancytitle,openingdate,closingdate,interview_startdate,interview_closedate,noofposition,experience,responsibilities_duties,remark,isvoid,isexternalvacancy,companyunkid) " & _
    '        "SELECT '" & StrCode & "' as Comp_Code,vacancyunkid,vacancytitle,openingdate,closingdate,interview_startdate,interview_closedate,noofposition,experience,responsibilities_duties,remark,isvoid , isexternalvacancy, " & intClientCode & " as companyunkid from rcvacancy_master WHERE ISNULL(isvoid,0) = 0 AND iswebexport = 1  "
    '        'Sohail (30 Dec 2011) -- End

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '///////// DELETE,INSERT VACANCY///////// END

    '        'Sohail (23 Dec 2011) -- Start
    '        '*** DELETE,INSERT    Result Master ***
    '        StrQ = "DELETE FROM " & mlinkweb & "hrresult_master WHERE Comp_Code = '" & StrCode & "'" & " AND companyunkid = " & intClientCode

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        StrQ = "INSERT INTO " & mlinkweb & "hrresult_master (Comp_Code,companyunkid,resultunkid,resultcode,resultname,resultgroupunkid,description,isactive,resultname1,resultname2) select '" & StrCode & "' as Comp_Code," & intClientCode & " as companyunkid,resultunkid,resultcode,resultname,resultgroupunkid,description,isactive,resultname1,resultname2 from hrresult_master WHERE isactive = 1 "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        '****   Email Sender Address Detail   *******************
    '        StrQ = "UPDATE  " & mlinkweb & "cfcompany_info " & _
    '                "SET     sendername = '" & Company._Object._Sendername & "' " & _
    '                      ", senderaddress = '" & Company._Object._Senderaddress & "' " & _
    '                      ", reference = '" & Company._Object._Reference & "' " & _
    '                      ", mailserverip = '" & Company._Object._Mailserverip & "' " & _
    '                      ", mailserverport = " & Company._Object._Mailserverport & " " & _
    '                      ", username = '" & Company._Object._Username & "' " & _
    '                      ", password = '" & Company._Object._Password & "' " & _
    '                      ", isloginssl = " & IIf(Company._Object._Isloginssl = True, 1, 0) & " " & _
    '                      ", mail_body = '" & Company._Object._Mail_Body & "' " & _
    '                      ", applicant_declaration = '" & ConfigParameter._Object._ApplicantDeclaration & "' " & _
    '                "WHERE   company_code = '" & StrCode & "' " & _
    '                        "AND companyunkid = " & intClientCode & " " 'Sohail (28 Mar 2012) - [applicant_declaration]

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        'Sohail (23 Dec 2011) -- End

    '        'objDataOp.ReleaseTransaction(True)
    '        Cursor.Current = Cursors.Default
    '        Return True

    '    Catch ex As Exception
    '        'objDataOp.ReleaseTransaction(False)
    '        DisplayError.Show("-1", ex.Message, "ExportToWeb", mstrModuleName)
    '        Return False
    '    Finally
    '        StrQ = String.Empty : mLinkDesk = String.Empty
    '    End Try
    'End Function

    'Sohail (18 May 2015) -- Start
    'Enhancement - Importing and Exporting through Web Service.
    'Private Function ExportToWeb(ByVal mlinkweb As String, ByVal StrCode As String, ByVal intClientCode As Integer, ByVal bw As BackgroundWorker) As Boolean
    '    Dim StrQ As String = String.Empty
    '    Dim exForce As Exception
    '    Dim mLinkDesk As String = String.Empty
    '    Dim objDataOp As New clsDataOperation
    '    Try

    '        'objDataOp.BindTransaction()

    '        Cursor.Current = Cursors.WaitCursor

    '        mLinkDesk = Company._Object._ConfigDatabaseName & ".."
    '        GintTotalApplicantToImport = 10

    '        ''<TODO TO BE REMOVED LATER.>
    '        'If objDataOp.RecordCount("SELECT * FROM " & mLinkDesk & "cfcountry_master") <> objDataOp.RecordCount("SELECT * FROM " & mlinkweb & "cfcountry_master") Then
    '        '    objDataOp.ExecNonQuery("delete from " & mlinkweb & "cfcountry_master")
    '        '    StrQ = "INSERT INTO " & mlinkweb & "cfcountry_master ( countryunkid , alias,country_name ,currency ,isrighttoleft ,country_name1 ,country_name2 ) " & _
    '        '                "Select countryunkid, alias ,country_name ,currency,isrighttoleft ,country_name1,country_name2 FROM " & mLinkDesk & "cfcountry_master "

    '        '    objDataOp.ExecNonQuery(StrQ)
    '        '    If objDataOp.ErrorMessage <> "" Then
    '        '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '        '        Throw exForce
    '        '    End If
    '        'End If



    '        '****** CITY MASTER   - UPDATE, INSERT - START
    '        'UPDATE EXISTING RECORDS
    '        StrQ = "UPDATE  " & mlinkweb & "cfcity_master " & _
    '                "SET     cfcity_master.countryunkid = Source.countryunkid " & _
    '                      ", cfcity_master.stateunkid = Source.stateunkid " & _
    '                      ", cfcity_master.code = Source.code " & _
    '                      ", cfcity_master.name = Source.name " & _
    '                      ", cfcity_master.name1 = Source.name1 " & _
    '                      ", cfcity_master.name2 = Source.name2 " & _
    '                      ", cfcity_master.isactive = Source.isactive " & _
    '                "FROM    " & mLinkDesk & "cfcity_master AS Source " & _
    '                "WHERE   cfcity_master.cityunkid = Source.cityunkid " & _
    '                        "AND cfcity_master.companyunkid = " & intClientCode & " " & _
    '                        "AND cfcity_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND Source.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'INSERT NEW RECORDS
    '        StrQ = "INSERT  INTO " & mlinkweb & "cfcity_master " & _
    '                        "( Comp_Code, companyunkid, cityunkid, stateunkid, countryunkid, code, name, isactive, name1, name2) " & _
    '                "SELECT  '" & StrCode & "', " & intClientCode & ", cfcity_master.cityunkid, cfcity_master.stateunkid, cfcity_master.countryunkid, cfcity_master.code, cfcity_master.name, cfcity_master.isactive, cfcity_master.name1, cfcity_master.name2 " & _
    '                "FROM    " & mLinkDesk & "cfcity_master " & _
    '                        "LEFT JOIN " & mlinkweb & "cfcity_master AS Webcfcity_master ON cfcity_master.cityunkid = Webcfcity_master.cityunkid " & _
    '                                                              "AND Webcfcity_master.companyunkid = " & intClientCode & " " & _
    '                                                              "AND Webcfcity_master.Comp_Code = '" & StrCode & "' " & _
    '                "WHERE   cfcity_master.isactive = 1 " & _
    '                        "AND ISNULL(Webcfcity_master.isactive, 1) = 1 " & _
    '                        "AND cfcity_master.Syncdatetime IS NULL " & _
    '                        "AND Webcfcity_master.cityunkid IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'UPDATE SYNCDATETIME IN SOURCE TABLE
    '        StrQ = "UPDATE  " & mLinkDesk & "cfcity_master " & _
    '                "SET     Syncdatetime = GETDATE() " & _
    '                "FROM    " & mlinkweb & "cfcity_master AS Webcfcity_master " & _
    '                "WHERE   Webcfcity_master.cityunkid = " & mLinkDesk & "cfcity_master.cityunkid " & _
    '                        "AND Webcfcity_master.companyunkid = " & intClientCode & " " & _
    '                        "AND Webcfcity_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND " & mLinkDesk & "cfcity_master.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '****** CITY MASTER   - UPDATE, INSERT - END


    '        If bw IsNot Nothing Then
    '            bw.ReportProgress(1)
    '        End If


    '        '****** STATE MASTER   - UPDATE, INSERT - START
    '        'UPDATE EXISTING RECORDS
    '        StrQ = "UPDATE  " & mlinkweb & "cfstate_master " & _
    '                "SET     cfstate_master.countryunkid = Source.countryunkid " & _
    '                      ", cfstate_master.code = Source.code " & _
    '                      ", cfstate_master.name = Source.name " & _
    '                      ", cfstate_master.name1 = Source.name1 " & _
    '                      ", cfstate_master.name2 = Source.name2 " & _
    '                      ", cfstate_master.isactive = Source.isactive " & _
    '                "FROM    " & mLinkDesk & "cfstate_master AS Source " & _
    '                "WHERE   cfstate_master.stateunkid = Source.stateunkid " & _
    '                        "AND cfstate_master.companyunkid = " & intClientCode & " " & _
    '                        "AND cfstate_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND Source.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'INSERT NEW RECORDS
    '        StrQ = "INSERT  INTO " & mlinkweb & "cfstate_master " & _
    '                        "( Comp_Code, companyunkid, stateunkid, countryunkid, code, name, isactive, name1, name2) " & _
    '                "SELECT  '" & StrCode & "', " & intClientCode & ", cfstate_master.stateunkid, cfstate_master.countryunkid, cfstate_master.code, cfstate_master.name, cfstate_master.isactive, cfstate_master.name1, cfstate_master.name2 " & _
    '                "FROM    " & mLinkDesk & "cfstate_master " & _
    '                        "LEFT JOIN " & mlinkweb & "cfstate_master AS Webcfstate_master ON cfstate_master.stateunkid = Webcfstate_master.stateunkid " & _
    '                                                              "AND Webcfstate_master.companyunkid = " & intClientCode & " " & _
    '                                                              "AND Webcfstate_master.Comp_Code = '" & StrCode & "' " & _
    '                "WHERE   " & mLinkDesk & "cfstate_master.isactive = 1 " & _
    '                        "AND ISNULL(Webcfstate_master.isactive, 1) = 1 " & _
    '                        "AND cfstate_master.Syncdatetime IS NULL " & _
    '                        "AND Webcfstate_master.stateunkid IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'UPDATE SYNCDATETIME IN SOURCE TABLE
    '        StrQ = "UPDATE  " & mLinkDesk & "cfstate_master " & _
    '                "SET     Syncdatetime = GETDATE() " & _
    '                "FROM    " & mlinkweb & "cfstate_master AS Webcfstate_master " & _
    '                "WHERE   Webcfstate_master.stateunkid = " & mLinkDesk & "cfstate_master.stateunkid " & _
    '                        "AND Webcfstate_master.companyunkid = " & intClientCode & " " & _
    '                        "AND Webcfstate_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND " & mLinkDesk & "cfstate_master.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '****** STATE MASTER   - UPDATE, INSERT - END


    '        If bw IsNot Nothing Then
    '            bw.ReportProgress(2)
    '        End If


    '        '****** ZIPCODE MASTER   - UPDATE, INSERT - START
    '        'UPDATE EXISTING RECORDS
    '        StrQ = "UPDATE  " & mlinkweb & "cfzipcode_master " & _
    '                "SET     cfzipcode_master.countryunkid = Source.countryunkid " & _
    '                      ", cfzipcode_master.stateunkid = Source.stateunkid " & _
    '                      ", cfzipcode_master.cityunkid = Source.cityunkid " & _
    '                      ", cfzipcode_master.zipcode_code = Source.zipcode_code " & _
    '                      ", cfzipcode_master.zipcode_no = Source.zipcode_no " & _
    '                      ", cfzipcode_master.isactive = Source.isactive " & _
    '                "FROM    " & mLinkDesk & "cfzipcode_master AS Source " & _
    '                "WHERE   cfzipcode_master.zipcodeunkid = Source.zipcodeunkid " & _
    '                        "AND cfzipcode_master.companyunkid = " & intClientCode & " " & _
    '                        "AND cfzipcode_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND Source.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'INSERT NEW RECORDS
    '        StrQ = "INSERT  INTO " & mlinkweb & "cfzipcode_master " & _
    '                        "( Comp_Code, companyunkid, zipcodeunkid, countryunkid, stateunkid, cityunkid, zipcode_code, zipcode_no, isactive) " & _
    '                "SELECT  '" & StrCode & "', " & intClientCode & ", cfzipcode_master.zipcodeunkid, cfzipcode_master.countryunkid, cfzipcode_master.stateunkid, cfzipcode_master.cityunkid, cfzipcode_master.zipcode_code, cfzipcode_master.zipcode_no, cfzipcode_master.isactive " & _
    '                "FROM    " & mLinkDesk & "cfzipcode_master " & _
    '                        "LEFT JOIN " & mlinkweb & "cfzipcode_master AS Webcfzipcode_master ON cfzipcode_master.zipcodeunkid = Webcfzipcode_master.zipcodeunkid " & _
    '                                                              "AND Webcfzipcode_master.companyunkid = " & intClientCode & " " & _
    '                                                              "AND Webcfzipcode_master.Comp_Code = '" & StrCode & "' " & _
    '                "WHERE   cfzipcode_master.isactive = 1 " & _
    '                        "AND ISNULL(Webcfzipcode_master.isactive, 1) = 1 " & _
    '                        "AND cfzipcode_master.Syncdatetime IS NULL " & _
    '                        "AND Webcfzipcode_master.zipcodeunkid IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'UPDATE SYNCDATETIME IN SOURCE TABLE
    '        StrQ = "UPDATE  " & mLinkDesk & "cfzipcode_master " & _
    '                "SET     Syncdatetime = GETDATE() " & _
    '                "FROM    " & mlinkweb & "cfzipcode_master AS Webcfzipcode_master " & _
    '                "WHERE   Webcfzipcode_master.zipcodeunkid = " & mLinkDesk & "cfzipcode_master.zipcodeunkid " & _
    '                        "AND Webcfzipcode_master.companyunkid = " & intClientCode & " " & _
    '                        "AND Webcfzipcode_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND " & mLinkDesk & "cfzipcode_master.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '****** ZIPCODE MASTER   - UPDATE, INSERT - END


    '        If bw IsNot Nothing Then
    '            bw.ReportProgress(3)
    '        End If


    '        '****** COMMON MASTER   - UPDATE, INSERT - START
    '        'UPDATE EXISTING RECORDS
    '        StrQ = "UPDATE  " & mlinkweb & "cfcommon_master " & _
    '                "SET     cfcommon_master.code = Source.code " & _
    '                      ", cfcommon_master.alias = Source.alias " & _
    '                      ", cfcommon_master.name = Source.name " & _
    '                      ", cfcommon_master.name1 = Source.name1 " & _
    '                      ", cfcommon_master.name2 = Source.name2 " & _
    '                      ", cfcommon_master.mastertype = Source.mastertype " & _
    '                      ", cfcommon_master.isactive = Source.isactive " & _
    '                      ", cfcommon_master.isrefreeallowed = Source.isrefreeallowed " & _
    '                "FROM    cfcommon_master AS Source " & _
    '                "WHERE   cfcommon_master.masterunkid = Source.masterunkid " & _
    '                        "AND NOT ( Source.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND Source.issyncwithrecruitment = 0) " & _
    '                        "AND cfcommon_master.companyunkid = " & intClientCode & " " & _
    '                        "AND cfcommon_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND Source.Syncdatetime IS NULL "
    '        'Sohail (22 Apr 2015) - [AND NOT ( cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND cfcommon_master.issyncwithrecruitment = 0) ]

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'INSERT NEW RECORDS
    '        StrQ = "INSERT  INTO " & mlinkweb & "cfcommon_master " & _
    '                        "( Comp_Code, companyunkid, masterunkid, code, alias, name, name1, name2, mastertype, isactive, isrefreeallowed) " & _
    '                "SELECT  '" & StrCode & "', " & intClientCode & ", cfcommon_master.masterunkid, cfcommon_master.code, cfcommon_master.alias, cfcommon_master.name, cfcommon_master.name1, cfcommon_master.name2, cfcommon_master.mastertype, cfcommon_master.isactive, cfcommon_master.isrefreeallowed " & _
    '                "FROM    cfcommon_master " & _
    '                        "LEFT JOIN " & mlinkweb & "cfcommon_master AS Webcfcommon_master ON cfcommon_master.masterunkid = Webcfcommon_master.masterunkid " & _
    '                                                              "AND Webcfcommon_master.companyunkid = " & intClientCode & " " & _
    '                                                              "AND Webcfcommon_master.Comp_Code = '" & StrCode & "' " & _
    '                "WHERE   cfcommon_master.isactive = 1 " & _
    '                        "AND ISNULL(Webcfcommon_master.isactive, 1) = 1 " & _
    '                        "AND NOT ( cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND cfcommon_master.issyncwithrecruitment = 0) " & _
    '                        "AND cfcommon_master.Syncdatetime IS NULL " & _
    '                        "AND Webcfcommon_master.masterunkid IS NULL "
    '        'Sohail (22 Apr 2015) - [AND NOT ( cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND cfcommon_master.issyncwithrecruitment = 0) ]

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'UPDATE SYNCDATETIME IN SOURCE TABLE
    '        StrQ = "UPDATE  cfcommon_master " & _
    '                "SET     Syncdatetime = GETDATE() " & _
    '                "FROM    " & mlinkweb & "cfcommon_master AS Webcfcommon_master " & _
    '                "WHERE   Webcfcommon_master.masterunkid = cfcommon_master.masterunkid " & _
    '                        "AND NOT ( cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND cfcommon_master.issyncwithrecruitment = 0) " & _
    '                        "AND Webcfcommon_master.companyunkid = " & intClientCode & " " & _
    '                        "AND Webcfcommon_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND cfcommon_master.Syncdatetime IS NULL "
    '        'Sohail (22 Apr 2015) - [AND NOT ( cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND cfcommon_master.issyncwithrecruitment = 0) ]

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '****** COMMON MASTER   - UPDATE, INSERT - END


    '        If bw IsNot Nothing Then
    '            bw.ReportProgress(4)
    '        End If


    '        '****** SKILLS MASTER   - UPDATE, INSERT - START
    '        'UPDATE EXISTING RECORDS
    '        StrQ = "UPDATE  " & mlinkweb & "hrskill_master " & _
    '                "SET     hrskill_master.skillcode = Source.skillcode " & _
    '                      ", hrskill_master.skillname = Source.skillname " & _
    '                      ", hrskill_master.skillcategoryunkid = Source.skillcategoryunkid " & _
    '                      ", hrskill_master.description = Source.description " & _
    '                      ", hrskill_master.skillname1 = Source.skillname1 " & _
    '                      ", hrskill_master.skillname2 = Source.skillname2 " & _
    '                      ", hrskill_master.isactive = Source.isactive " & _
    '                "FROM    hrskill_master AS Source " & _
    '                "WHERE   hrskill_master.skillunkid = Source.skillunkid " & _
    '                        "AND hrskill_master.companyunkid = " & intClientCode & " " & _
    '                        "AND hrskill_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND Source.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'INSERT NEW RECORDS
    '        StrQ = "INSERT  INTO " & mlinkweb & "hrskill_master " & _
    '                        "( Comp_Code, companyunkid, skillunkid, skillcode, skillname, skillcategoryunkid, description, isactive, skillname1, skillname2) " & _
    '                "SELECT  '" & StrCode & "', " & intClientCode & ", hrskill_master.skillunkid, hrskill_master.skillcode, hrskill_master.skillname, hrskill_master.skillcategoryunkid, hrskill_master.description, hrskill_master.isactive, hrskill_master.skillname1, hrskill_master.skillname2 " & _
    '                "FROM    hrskill_master " & _
    '                        "LEFT JOIN " & mlinkweb & "hrskill_master AS Webhrskill_master ON hrskill_master.skillunkid = Webhrskill_master.skillunkid " & _
    '                                                              "AND Webhrskill_master.companyunkid = " & intClientCode & " " & _
    '                                                              "AND Webhrskill_master.Comp_Code = '" & StrCode & "' " & _
    '                "WHERE   hrskill_master.isactive = 1 " & _
    '                        "AND ISNULL(Webhrskill_master.isactive, 1) = 1 " & _
    '                        "AND hrskill_master.Syncdatetime IS NULL " & _
    '                        "AND Webhrskill_master.skillunkid IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'UPDATE SYNCDATETIME IN SOURCE TABLE
    '        StrQ = "UPDATE  hrskill_master " & _
    '                "SET     Syncdatetime = GETDATE() " & _
    '                "FROM    " & mlinkweb & "hrskill_master AS Webhrskill_master " & _
    '                "WHERE   Webhrskill_master.skillunkid = hrskill_master.skillunkid " & _
    '                        "AND Webhrskill_master.companyunkid = " & intClientCode & " " & _
    '                        "AND Webhrskill_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND hrskill_master.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '****** SKILLS MASTER   - UPDATE, INSERT - END


    '        If bw IsNot Nothing Then
    '            bw.ReportProgress(5)
    '        End If


    '        '****** INSTITUTE MASTER   - UPDATE, INSERT - START
    '        'UPDATE EXISTING RECORDS
    '        StrQ = "UPDATE  " & mlinkweb & "hrinstitute_master " & _
    '                "SET     hrinstitute_master.institute_code = Source.institute_code " & _
    '                      ", hrinstitute_master.institute_name = Source.institute_name " & _
    '                      ", hrinstitute_master.institute_address = Source.institute_address " & _
    '                      ", hrinstitute_master.telephoneno = Source.telephoneno " & _
    '                      ", hrinstitute_master.institute_fax = Source.institute_fax " & _
    '                      ", hrinstitute_master.institute_email = Source.institute_email " & _
    '                      ", hrinstitute_master.contact_person = Source.contact_person " & _
    '                      ", hrinstitute_master.stateunkid = Source.stateunkid " & _
    '                      ", hrinstitute_master.cityunkid = Source.cityunkid " & _
    '                      ", hrinstitute_master.countryunkid = Source.countryunkid " & _
    '                      ", hrinstitute_master.ishospital = Source.ishospital " & _
    '                      ", hrinstitute_master.isactive = Source.isactive " & _
    '                "FROM    hrinstitute_master AS Source " & _
    '                "WHERE   hrinstitute_master.instituteunkid = Source.instituteunkid " & _
    '                        "AND hrinstitute_master.companyunkid = " & intClientCode & " " & _
    '                        "AND hrinstitute_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND Source.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'INSERT NEW RECORDS
    '        StrQ = "INSERT  INTO " & mlinkweb & "hrinstitute_master " & _
    '                        "( Comp_Code, companyunkid, instituteunkid, institute_code, institute_name, institute_address, telephoneno, institute_fax, institute_email, description, contact_person, stateunkid, cityunkid, countryunkid, ishospital, isactive) " & _
    '                "SELECT  '" & StrCode & "', " & intClientCode & ", hrinstitute_master.instituteunkid, hrinstitute_master.institute_code, hrinstitute_master.institute_name, hrinstitute_master.institute_address, hrinstitute_master.telephoneno, hrinstitute_master.institute_fax, hrinstitute_master.institute_email, hrinstitute_master.description, hrinstitute_master.contact_person, hrinstitute_master.stateunkid, hrinstitute_master.cityunkid, hrinstitute_master.countryunkid, hrinstitute_master.ishospital, hrinstitute_master.isactive " & _
    '                "FROM    hrinstitute_master " & _
    '                        "LEFT JOIN " & mlinkweb & "hrinstitute_master AS Webhrinstitute_master ON hrinstitute_master.instituteunkid = Webhrinstitute_master.instituteunkid " & _
    '                                                              "AND Webhrinstitute_master.companyunkid = " & intClientCode & " " & _
    '                                                              "AND Webhrinstitute_master.Comp_Code = '" & StrCode & "' " & _
    '                "WHERE   hrinstitute_master.isactive = 1 " & _
    '                        "AND ISNULL(Webhrinstitute_master.isactive, 1) = 1 " & _
    '                        "AND hrinstitute_master.ishospital = 0 AND hrinstitute_master.issync_recruit = 1 " & _
    '                        "AND hrinstitute_master.Syncdatetime IS NULL " & _
    '                        "AND Webhrinstitute_master.instituteunkid IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'UPDATE SYNCDATETIME IN SOURCE TABLE
    '        StrQ = "UPDATE  hrinstitute_master " & _
    '                "SET     Syncdatetime = GETDATE() " & _
    '                "FROM    " & mlinkweb & "hrinstitute_master AS Webhrinstitute_master " & _
    '                "WHERE   Webhrinstitute_master.instituteunkid = hrinstitute_master.instituteunkid " & _
    '                        "AND Webhrinstitute_master.companyunkid = " & intClientCode & " " & _
    '                        "AND Webhrinstitute_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND hrinstitute_master.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '****** INSTITUTE MASTER   - UPDATE, INSERT - END


    '        If bw IsNot Nothing Then
    '            bw.ReportProgress(6)
    '        End If


    '        '****** QUALIFICATION MASTER   - UPDATE, INSERT - START
    '        'UPDATE EXISTING RECORDS
    '        StrQ = "UPDATE  " & mlinkweb & "hrqualification_master " & _
    '                "SET     hrqualification_master.qualificationcode = Source.qualificationcode " & _
    '                      ", hrqualification_master.qualificationname = Source.qualificationname " & _
    '                      ", hrqualification_master.qualificationgroupunkid = Source.qualificationgroupunkid " & _
    '                      ", hrqualification_master.description = Source.description " & _
    '                      ", hrqualification_master.isactive = Source.isactive " & _
    '                      ", hrqualification_master.qualificationname1 = Source.qualificationname1 " & _
    '                      ", hrqualification_master.qualificationname2 = Source.qualificationname2 " & _
    '                      ", hrqualification_master.resultgroupunkid = Source.resultgroupunkid " & _
    '                "FROM    hrqualification_master AS Source " & _
    '                "WHERE   hrqualification_master.qualificationunkid = Source.qualificationunkid " & _
    '                        "AND hrqualification_master.companyunkid = " & intClientCode & " " & _
    '                        "AND hrqualification_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND Source.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'INSERT NEW RECORDS
    '        StrQ = "INSERT  INTO " & mlinkweb & "hrqualification_master " & _
    '                        "( Comp_Code, companyunkid, qualificationunkid, qualificationcode, qualificationname, qualificationgroupunkid, description, isactive, qualificationname1, qualificationname2, resultgroupunkid) " & _
    '                "SELECT  '" & StrCode & "', " & intClientCode & ", hrqualification_master.qualificationunkid, hrqualification_master.qualificationcode, hrqualification_master.qualificationname, hrqualification_master.qualificationgroupunkid, hrqualification_master.description, hrqualification_master.isactive, hrqualification_master.qualificationname1, hrqualification_master.qualificationname2, hrqualification_master.resultgroupunkid " & _
    '                "FROM    hrqualification_master " & _
    '                        "LEFT JOIN " & mlinkweb & "hrqualification_master AS Webhrqualification_master ON hrqualification_master.qualificationunkid = Webhrqualification_master.qualificationunkid " & _
    '                                                              "AND Webhrqualification_master.companyunkid = " & intClientCode & " " & _
    '                                                              "AND Webhrqualification_master.Comp_Code = '" & StrCode & "' " & _
    '                "WHERE   hrqualification_master.isactive = 1 " & _
    '                        "AND ISNULL(Webhrqualification_master.isactive, 1) = 1 AND hrqualification_master.issync_recruit = 1 " & _
    '                        "AND hrqualification_master.Syncdatetime IS NULL " & _
    '                        "AND Webhrqualification_master.qualificationunkid IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'UPDATE SYNCDATETIME IN SOURCE TABLE
    '        StrQ = "UPDATE  hrqualification_master " & _
    '                "SET     Syncdatetime = GETDATE() " & _
    '                "FROM    " & mlinkweb & "hrqualification_master AS Webhrqualification_master " & _
    '                "WHERE   Webhrqualification_master.qualificationunkid = hrqualification_master.qualificationunkid " & _
    '                        "AND Webhrqualification_master.companyunkid = " & intClientCode & " " & _
    '                        "AND Webhrqualification_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND hrqualification_master.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '****** QUALIFICATION MASTER   - UPDATE, INSERT - END


    '        If bw IsNot Nothing Then
    '            bw.ReportProgress(7)
    '        End If


    '        '****** VACANCY MASTER   - UPDATE, INSERT - START
    '        'UPDATE EXISTING RECORDS
    '        StrQ = "UPDATE  " & mlinkweb & "rcvacancy_master " & _
    '                "SET     rcvacancy_master.vacancytitle = Source.vacancytitle " & _
    '                      ", rcvacancy_master.openingdate = Source.openingdate " & _
    '                      ", rcvacancy_master.closingdate = Source.closingdate " & _
    '                      ", rcvacancy_master.interview_startdate = Source.interview_startdate " & _
    '                      ", rcvacancy_master.interview_closedate = Source.interview_closedate " & _
    '                      ", rcvacancy_master.noofposition = Source.noofposition " & _
    '                      ", rcvacancy_master.experience = Source.experience " & _
    '                      ", rcvacancy_master.responsibilities_duties = Source.responsibilities_duties " & _
    '                      ", rcvacancy_master.remark = Source.remark " & _
    '                      ", rcvacancy_master.isvoid = Source.isvoid " & _
    '                      ", rcvacancy_master.isexternalvacancy = Source.isexternalvacancy " & _
    '                "FROM    rcvacancy_master AS Source " & _
    '                "WHERE   rcvacancy_master.vacancyunkid = Source.vacancyunkid " & _
    '                        "AND rcvacancy_master.companyunkid = " & intClientCode & " " & _
    '                        "AND rcvacancy_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND Source.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'INSERT NEW RECORDS
    '        StrQ = "INSERT  INTO " & mlinkweb & "rcvacancy_master " & _
    '                        "( Comp_Code, companyunkid, vacancyunkid, vacancytitle, openingdate, closingdate, interview_startdate, interview_closedate, noofposition, experience, responsibilities_duties, remark, isvoid, isexternalvacancy) " & _
    '                "SELECT  '" & StrCode & "', " & intClientCode & ", rcvacancy_master.vacancyunkid, rcvacancy_master.vacancytitle, rcvacancy_master.openingdate, rcvacancy_master.closingdate, rcvacancy_master.interview_startdate, rcvacancy_master.interview_closedate, rcvacancy_master.noofposition, rcvacancy_master.experience, rcvacancy_master.responsibilities_duties, rcvacancy_master.remark, rcvacancy_master.isvoid, rcvacancy_master.isexternalvacancy " & _
    '                "FROM    rcvacancy_master " & _
    '                        "LEFT JOIN " & mlinkweb & "rcvacancy_master AS Webrcvacancy_master ON rcvacancy_master.vacancyunkid = Webrcvacancy_master.vacancyunkid " & _
    '                                                              "AND Webrcvacancy_master.companyunkid = " & intClientCode & " " & _
    '                                                              "AND Webrcvacancy_master.Comp_Code = '" & StrCode & "' " & _
    '                "WHERE   ISNULL(rcvacancy_master.isvoid, 0) = 0 " & _
    '                        "AND ISNULL(Webrcvacancy_master.isvoid, 0) = 0 " & _
    '                        "AND rcvacancy_master.Syncdatetime IS NULL " & _
    '                        "AND rcvacancy_master.iswebexport = 1 " & _
    '                        "AND Webrcvacancy_master.vacancyunkid IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'UPDATE SYNCDATETIME IN SOURCE TABLE
    '        StrQ = "UPDATE  rcvacancy_master " & _
    '                "SET     Syncdatetime = GETDATE() " & _
    '                "FROM    " & mlinkweb & "rcvacancy_master AS Webrcvacancy_master " & _
    '                "WHERE   Webrcvacancy_master.vacancyunkid = rcvacancy_master.vacancyunkid " & _
    '                        "AND Webrcvacancy_master.companyunkid = " & intClientCode & " " & _
    '                        "AND Webrcvacancy_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND rcvacancy_master.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '****** VACANCY MASTER   - UPDATE, INSERT - END


    '        If bw IsNot Nothing Then
    '            bw.ReportProgress(8)
    '        End If


    '        '****** RESULT MASTER   - UPDATE, INSERT - START
    '        'UPDATE EXISTING RECORDS
    '        StrQ = "UPDATE  " & mlinkweb & "hrresult_master " & _
    '                "SET     hrresult_master.resultcode = Source.resultcode " & _
    '                      ", hrresult_master.resultname = Source.resultname " & _
    '                      ", hrresult_master.resultgroupunkid = Source.resultgroupunkid " & _
    '                      ", hrresult_master.description = Source.description " & _
    '                      ", hrresult_master.isactive = Source.isactive " & _
    '                      ", hrresult_master.resultname1 = Source.resultname1 " & _
    '                      ", hrresult_master.resultname2 = Source.resultname2 " & _
    '                "FROM    hrresult_master AS Source " & _
    '                "WHERE   hrresult_master.resultunkid = Source.resultunkid " & _
    '                        "AND hrresult_master.companyunkid = " & intClientCode & " " & _
    '                        "AND hrresult_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND Source.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'INSERT NEW RECORDS
    '        StrQ = "INSERT  INTO " & mlinkweb & "hrresult_master " & _
    '                        "( Comp_Code, companyunkid, resultunkid, resultcode, resultname, resultgroupunkid, description, isactive, resultname1, resultname2) " & _
    '                "SELECT  '" & StrCode & "', " & intClientCode & ", hrresult_master.resultunkid, hrresult_master.resultcode, hrresult_master.resultname, hrresult_master.resultgroupunkid, hrresult_master.description, hrresult_master.isactive, hrresult_master.resultname1, hrresult_master.resultname2 " & _
    '                "FROM    hrresult_master " & _
    '                        "LEFT JOIN " & mlinkweb & "hrresult_master AS Webhrresult_master ON hrresult_master.resultunkid = Webhrresult_master.resultunkid " & _
    '                                                              "AND Webhrresult_master.companyunkid = " & intClientCode & " " & _
    '                                                              "AND Webhrresult_master.Comp_Code = '" & StrCode & "' " & _
    '                "WHERE   hrresult_master.isactive = 1 " & _
    '                        "AND ISNULL(Webhrresult_master.isactive, 1) = 1 " & _
    '                        "AND hrresult_master.Syncdatetime IS NULL " & _
    '                        "AND Webhrresult_master.resultunkid IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If

    '        'UPDATE SYNCDATETIME IN SOURCE TABLE
    '        StrQ = "UPDATE  hrresult_master " & _
    '                "SET     Syncdatetime = GETDATE() " & _
    '                "FROM    " & mlinkweb & "hrresult_master AS Webhrresult_master " & _
    '                "WHERE   Webhrresult_master.resultunkid = hrresult_master.resultunkid " & _
    '                        "AND Webhrresult_master.companyunkid = " & intClientCode & " " & _
    '                        "AND Webhrresult_master.Comp_Code = '" & StrCode & "' " & _
    '                        "AND hrresult_master.Syncdatetime IS NULL "

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If
    '        '****** RESULT MASTER   - UPDATE, INSERT - END


    '        If bw IsNot Nothing Then
    '            bw.ReportProgress(9)
    '        End If


    '        '****   Email Sender Address Detail   *******************
    '        StrQ = "UPDATE  " & mlinkweb & "cfcompany_info " & _
    '                "SET     sendername = '" & Company._Object._Sendername & "' " & _
    '                      ", senderaddress = '" & Company._Object._Senderaddress & "' " & _
    '                      ", reference = '" & Company._Object._Reference & "' " & _
    '                      ", mailserverip = '" & Company._Object._Mailserverip & "' " & _
    '                      ", mailserverport = " & Company._Object._Mailserverport & " " & _
    '                      ", username = '" & Company._Object._Username & "' " & _
    '                      ", password = '" & Company._Object._Password & "' " & _
    '                      ", isloginssl = " & IIf(Company._Object._Isloginssl = True, 1, 0) & " " & _
    '                      ", mail_body = '" & Company._Object._Mail_Body & "' " & _
    '                      ", applicant_declaration = '" & ConfigParameter._Object._ApplicantDeclaration & "' " & _
    '                "WHERE   company_code = '" & StrCode & "' " & _
    '                        "AND companyunkid = " & intClientCode & " " 'Sohail (28 Mar 2012) - [applicant_declaration]

    '        objDataOp.ExecNonQuery(StrQ)

    '        If objDataOp.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '            Throw exForce
    '        End If


    '        If bw IsNot Nothing Then
    '            bw.ReportProgress(10)
    '        End If


    '        'objDataOp.ReleaseTransaction(True)
    '        Cursor.Current = Cursors.Default
    '        Return True

    '    Catch ex As Exception
    '        'objDataOp.ReleaseTransaction(False)
    '        DisplayError.Show("-1", ex.Message, "ExportToWeb", mstrModuleName)
    '        Return False
    '    Finally
    '        StrQ = String.Empty : mLinkDesk = String.Empty
    '    End Try
    'End Function

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function ExportToWeb(ByVal mlinkweb As String, ByVal StrCode As String, ByVal intClientCode As Integer, ByVal bw As BackgroundWorker) As Boolean
    Private Function ExportToWeb(ByVal mlinkweb As String, _
                                 ByVal StrCode As String, _
                                 ByVal intClientCode As Integer, _
                                 ByVal bw As BackgroundWorker, _
                                 ByVal strDatabaseServer As String, _
                                 ByVal intDatabaseServerSetting As String, _
                                 ByVal strAuthenticationCode As String, _
                                 ByVal strConfigDatabaseName As String, _
                                 ByVal strApplicantDeclaration As String, _
                                 ByVal intTimeZoneMinuteDifference As Integer, _
                                 ByVal blnQualificationCertificateAttachmentMandatory As Boolean, _
                                 ByVal strDateFormat As String, _
                                 ByVal strDateSeparator As String, _
                                 ByVal strAdminEmail As String, _
                                 ByVal blnMiddleNameMandatory As Boolean, _
                                 ByVal blnGenderMandatory As Boolean, _
                                 ByVal blnBirthDateMandatory As Boolean, _
                                 ByVal blnAddress1Mandatory As Boolean, _
                                 ByVal blnAddress2Mandatory As Boolean, _
                                 ByVal blnCountryMandatory As Boolean, _
                                 ByVal blnStateMandatory As Boolean, _
                                 ByVal blnCityMandatory As Boolean, _
                                 ByVal blnPostCodeMandatory As Boolean, _
                                 ByVal blnMaritalStatusMandatory As Boolean, _
                                 ByVal blnLanguage1Mandatory As Boolean, _
                                 ByVal blnNationalityMandatory As Boolean, _
                                 ByVal blnOneSkillMandatory As Boolean, _
                                 ByVal blnOneQualificationMandatory As Boolean, _
                                 ByVal blnOneJobExperienceMandatory As Boolean, _
                                 ByVal blnOneReferenceMandatory As Boolean, _
                                 ByVal intApplicantQualificationSortBy As Integer, _
                                 ByVal blnRegionMandatory As Boolean, _
                                 ByVal blnHighestQualificationMandatory As Boolean, _
                                 ByVal blnEmployerMandatory As Boolean, _
                                 ByVal blnMotherTongueMandatory As Boolean, _
                                 ByVal blnCurrentSalaryMandatory As Boolean, _
                                 ByVal blnExpectedSalaryMandatory As Boolean, _
                                 ByVal blnExpectedBenefitsMandatory As Boolean, _
                                 ByVal blnNoticePeriodMandatory As Boolean, _
                                 ByVal blnEarliestPossibleStartDateMandatory As Boolean, _
                                 ByVal blnCommentsMandatory As Boolean, _
                                 ByVal blnVacancyFoundOutFromMandatory As Boolean, _
                                 ByVal blnCompanyNameJobHistoryMandatory As Boolean, _
                                 ByVal blnPositionHeldMandatory As Boolean, _
                                 ByVal blnSendJobConfirmationEmail As Boolean, _
                                 ByVal blnVacancyAlert As Boolean, _
                                 ByVal intMaxVacancyAlert As Integer, _
                                 ByVal blnAttachApplicantCV As Boolean, _
                                 ByVal blnSendEmail As Boolean, _
                                 ByVal strWebServiceLink As String, _
                                 ByVal blnOneCurriculamVitaeMandatory As Boolean, _
                                 ByVal blnOneCoverLetterMandatory As Boolean, _
                                 ByVal blnQualificationStartDateMandatory As Boolean, _
                                 ByVal blnQualificationAwardDateMandatory As Boolean, _
                                 ByVal intJobConfirmationTemplateId As Integer, _
                                 ByVal intJobAlertTemplateId As Integer, _
                                 ByVal intInternalJobAlertTemplateId As Integer, _
                                 ByVal blnShowArutisignature As Boolean _
                                ) As Boolean
        'Hemant (30 Oct 2019) -- [intJobConfirmationTemplateId, intJobAlertTemplateId, intInternalJobAlertTemplateId, blnShowArutisignature]
        'Gajanan [29-AUG-2019] -- [blnQualificationStartDateMandatory],[blnQualificationAwardDateMandatory] 
        'Sohail (04 Jul 2019) - [blnOneCurriculamVitaeMandatory, blnOneCoverLetterMandatory]
        'Sohail (16 Nov 2018) - [strWebServiceLink]
        'Pinkal (18-May-2018) --  'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.[ByVal blnAttachApplicantCV As Boolean, _]


        'Sohail (24 Jul 2017) - [blnCompanyNameJobHistoryMandatory, blnPositionHeldMandatory]
        'Nilay (13 Apr 2017) -- [intApplicantQualificationSortBy, blnRegionMandatory, blnHighestQualificationMandatory, blnEmployerMandatory, 
        'blnMotherTongueMandatory, blnCurrentSalaryMandatory, blnExpectedSalaryMandatory, blnExpectedBenefitsMandatory, blnExpectedBenefitsMandatory
        'blnNoticePeriodMandatory, blnEarliestPossibleStartDateMandatory, blnCommentsMandatory , blnVacancyFoundOutFromMandatory]

        'Sohail (05 Dec 2016) - [strDateFormat, strDateSeparator to blnOneReferenceMandatory]
        'Shani(24-Aug-2015) -- End


        'Pinkal (12-Feb-2018) -- 'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.[ ByVal blnSendJobConfirmationEmail As Boolean,ByVal blnVacancyAlert As Boolean, ByVal intMaxVacancyAlert As Integer, _]

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim mLinkDesk As String = String.Empty
        Dim objDataOp As New clsDataOperation
        Try

            'objDataOp.BindTransaction()

            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'mLinkDesk = Company._Object._ConfigDatabaseName & ".."
            mLinkDesk = strConfigDatabaseName & ".."
            'Shani(24-Aug-2015) -- End

            'Sohail (02 Nov 2016) -- Start
            'Enhancement - Add Employee Configuration Settings form in Aruti Configuration [Assign by Sohail]
            'GintTotalApplicantToImport = 10

            'Nilay (13 Apr 2017) -- Start
            'Enhancements: Settings for mandatory options in online recruitment
            'GintTotalApplicantToImport = 12
            'Hemant (30 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            'GintTotalApplicantToImport = 13
            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : On job master,  Need a tab to define the language(s) attached to the job. Those languages will be displayed on job report.
            'GintTotalApplicantToImport = 15
            'Sohail (06 Oct 2021) -- Start
            'NMB Enhancement : OLD-483 : Separate sender's email configuration on recruitment portal from company email.
            'GintTotalApplicantToImport = 17
            'Sohail (11 Nov 2021) -- Start
            'NMB Enhancement : : Display Job Location on published vacancy on recruitment portal e.g Job Location - Northern Zone, Arusha.
            'GintTotalApplicantToImport = 18
            GintTotalApplicantToImport = 20
            'Sohail (11 Nov 2021) -- End
            'Sohail (06 Oct 2021) -- End
            'Sohail (18 Feb 2020) -- End
            'Hemant (30 Oct 2019)
            'Nilay (13 Apr 2017) -- End

            'Sohail (02 Nov 2016) -- Start

            Dim HttpBinding As New System.ServiceModel.BasicHttpBinding
            'Dim HttpBinding As New System.ServiceModel.WSHttpBinding
            With HttpBinding
                .MaxBufferPoolSize = 2147483647
                '.MaxBufferSize = 2147483647
                .MaxReceivedMessageSize = 2147483647
                .ReaderQuotas.MaxStringContentLength = 2147483647
                .ReaderQuotas.MaxArrayLength = 2147483647
                .ReaderQuotas.MaxBytesPerRead = 2147483647
                .ReaderQuotas.MaxNameTableCharCount = 2147483647
                .OpenTimeout = New TimeSpan(0, 20, 0)
                .ReceiveTimeout = New TimeSpan(0, 20, 0)
                .SendTimeout = New TimeSpan(0, 20, 0)
                '.Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport 'Sohail (05 Dec 2016)
                .MessageEncoding = ServiceModel.WSMessageEncoding.Text 'Sohail (11 Dec 2015) - [CCBRT Error: The content type text/html of the response message does not match the content type of the binding]
                .Security.Transport.ClientCredentialType = ServiceModel.HttpClientCredentialType.None
            End With

            'Dim EndPointAdd As System.ServiceModel.EndpointAddress 'Sohail (16 Nov 2018)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._DatabaseServerSetting = enDatabaseServerSetting.DEFAULT_SETTING Then
            'If intDatabaseServerSetting = enDatabaseServerSetting.DEFAULT_SETTING Then 'Sohail (16 Nov 2018)
            'Shani(24-Aug-2015) -- End
            'Sohail (05 Dec 2016) -- Start
            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
            'EndPointAdd = New System.ServiceModel.EndpointAddress("http://www.arutihr.com/Aruti/ArutiRecruitmentService.asmx")
            'EndPointAdd = New System.ServiceModel.EndpointAddress("http://www.arutihr.com/ArutiHRM/ArutiRecruitmentService.asmx") 'Sohail (16 Nov 2018)
            'Sohail (05 Dec 2016) -- End
            'Else 'Sohail (16 Nov 2018)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'EndPointAdd = New System.ServiceModel.EndpointAddress("http://" & ConfigParameter._Object._DatabaseServer.Replace("\apayroll", "/Aruti") & "/ArutiRecruitmentService.asmx")
            'Sohail (05 Dec 2016) -- Start
            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
            'EndPointAdd = New System.ServiceModel.EndpointAddress("http://" & strDatabaseServer.Replace("\apayroll", "/ArutiHRM") & "/ArutiRecruitmentService.asmx")
            'EndPointAdd = New System.ServiceModel.EndpointAddress("http://" & strDatabaseServer.Replace("\apayroll", "") & "/ArutiHRM/ArutiRecruitmentService.asmx")
            'Sohail (05 Dec 2016) -- End
            'Shani(24-Aug-2015) -- End

            'End If 'Sohail (16 Nov 2018)

            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim WebRef As New ArutiRecruitmentServiceSoapClient(HttpBinding, EndPointAdd)
            'Dim UserCred As New UserCredentials
            Dim WebRef As New WR.ArutiRecruitmentService
            Dim UserCred As New WR.UserCredentials
            WebRef.Url = strWebServiceLink
            'Sohail (16 Nov 2018) -- End
            WebRef.Timeout = 1200000 '20 minutes 'Sohail (21 Sep 2019)
            Dim strErrorMessage As String = ""

            System.Net.ServicePointManager.Expect100Continue = False 'Sohail (25 Oct 2016) - [Issue : The remote server returned an error: (417) Expectation Failed]
            'Sohail (16 Jul 2019) -- Start
            'NMB Issue # - 76.1 - Erroe : The underlying connection was closed: An unexpected error occurred on a send.
            'System.Net.ServicePointManager.SecurityProtocol = DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (25 Jul 2019) - (the requested security protocol is not supported)
            'Sohail (07 Aug 2019) -- Start
            'ZRA issue # - 76.1 - The requested security protocol is not supported.
            'System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
            Try
                System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
            Catch ex As Exception

            End Try
            'Sohail (07 Aug 2019) -- End
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications
            'Sohail (16 Jul 2019) -- End

            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'UserCred.Password = clsSecurity.Decrypt("CNjh9qOZUGhuqVpVp/LjZoYbJJoNgPiyC/P/vxPGfWo=", "ezee")
            UserCred.Password = "CNjh9qOZUGhuqVpVp/LjZoYbJJoNgPiyC/P/vxPGfWo="
            'Sohail (16 Nov 2018) -- End
            UserCred.WebClientID = intClientCode
            UserCred.CompCode = StrCode
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'UserCred.AuthenticationCode = ConfigParameter._Object._AuthenticationCode
            UserCred.AuthenticationCode = strAuthenticationCode
            'Shani(24-Aug-2015) -- End

            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            WebRef.UserCredentialsValue = UserCred
            'Sohail (16 Nov 2018) -- End

            ''<TODO TO BE REMOVED LATER.>
            'If objDataOp.RecordCount("SELECT * FROM " & mLinkDesk & "cfcountry_master") <> objDataOp.RecordCount("SELECT * FROM " & mlinkweb & "cfcountry_master") Then
            '    objDataOp.ExecNonQuery("delete from " & mlinkweb & "cfcountry_master")
            '    StrQ = "INSERT INTO " & mlinkweb & "cfcountry_master ( countryunkid , alias,country_name ,currency ,isrighttoleft ,country_name1 ,country_name2 ) " & _
            '                "Select countryunkid, alias ,country_name ,currency,isrighttoleft ,country_name1,country_name2 FROM " & mLinkDesk & "cfcountry_master "

            '    objDataOp.ExecNonQuery(StrQ)
            '    If objDataOp.ErrorMessage <> "" Then
            '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '        Throw exForce
            '    End If
            'End If



            '****** CITY MASTER   - UPDATE, INSERT - START
            'UPDATE EXISTING RECORDS
            'StrQ = "UPDATE  " & mlinkweb & "cfcity_master " & _
            '        "SET     cfcity_master.countryunkid = Source.countryunkid " & _
            '              ", cfcity_master.stateunkid = Source.stateunkid " & _
            '              ", cfcity_master.code = Source.code " & _
            '              ", cfcity_master.name = Source.name " & _
            '              ", cfcity_master.name1 = Source.name1 " & _
            '              ", cfcity_master.name2 = Source.name2 " & _
            '              ", cfcity_master.isactive = Source.isactive " & _
            '        "FROM    " & mLinkDesk & "cfcity_master AS Source " & _
            '        "WHERE   cfcity_master.cityunkid = Source.cityunkid " & _
            '                "AND cfcity_master.companyunkid = " & intClientCode & " " & _
            '                "AND cfcity_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND Source.Syncdatetime IS NULL "
            'objDataOp.ExecNonQuery(StrQ)

            'If objDataOp.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Throw exForce
            'End If

            'INSERT NEW RECORDS
            'StrQ = "INSERT  INTO " & mlinkweb & "cfcity_master " & _
            '                "( Comp_Code, companyunkid, cityunkid, stateunkid, countryunkid, code, name, isactive, name1, name2) " & _
            '        "SELECT  '" & StrCode & "', " & intClientCode & ", cfcity_master.cityunkid, cfcity_master.stateunkid, cfcity_master.countryunkid, cfcity_master.code, cfcity_master.name, cfcity_master.isactive, cfcity_master.name1, cfcity_master.name2 " & _
            '        "FROM    " & mLinkDesk & "cfcity_master " & _
            '                "LEFT JOIN " & mlinkweb & "cfcity_master AS Webcfcity_master ON cfcity_master.cityunkid = Webcfcity_master.cityunkid " & _
            '                                                      "AND Webcfcity_master.companyunkid = " & intClientCode & " " & _
            '                                                      "AND Webcfcity_master.Comp_Code = '" & StrCode & "' " & _
            '        "WHERE   cfcity_master.isactive = 1 " & _
            '                "AND ISNULL(Webcfcity_master.isactive, 1) = 1 " & _
            '                "AND cfcity_master.Syncdatetime IS NULL " & _
            '                "AND Webcfcity_master.cityunkid IS NULL "
            StrQ = "SELECT  '" & StrCode & "', " & intClientCode & ", cfcity_master.cityunkid, cfcity_master.stateunkid, cfcity_master.countryunkid, cfcity_master.code, cfcity_master.name, cfcity_master.isactive, cfcity_master.name1, cfcity_master.name2, Syncdatetime " & _
                    "FROM    " & mLinkDesk & "cfcity_master " & _
                    "WHERE   1 = 1 " & _
                            "AND cfcity_master.Syncdatetime IS NULL "

            Dim dsCity As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsCity)
            'Sohail (08 Oct 2018) -- End

            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadCityMaster(UserCred, strErrorMessage, dsCity, True)
            WebRef.UploadCityMaster(strErrorMessage, dsCity, True)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'UPDATE SYNCDATETIME IN SOURCE TABLE
            'StrQ = "UPDATE  " & mLinkDesk & "cfcity_master " & _
            '        "SET     Syncdatetime = GETDATE() " & _
            '        "FROM    " & mlinkweb & "cfcity_master AS Webcfcity_master " & _
            '        "WHERE   Webcfcity_master.cityunkid = " & mLinkDesk & "cfcity_master.cityunkid " & _
            '                "AND Webcfcity_master.companyunkid = " & intClientCode & " " & _
            '                "AND Webcfcity_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND " & mLinkDesk & "cfcity_master.Syncdatetime IS NULL "
            StrQ = "UPDATE  " & mLinkDesk & "cfcity_master " & _
                    "SET     Syncdatetime = GETDATE() " & _
                    "WHERE   " & mLinkDesk & "cfcity_master.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If
            '****** CITY MASTER   - UPDATE, INSERT - END


            If bw IsNot Nothing Then
                bw.ReportProgress(1)
            End If


            '****** STATE MASTER   - UPDATE, INSERT - START
            'UPDATE EXISTING RECORDS
            'StrQ = "UPDATE  " & mlinkweb & "cfstate_master " & _
            '        "SET     cfstate_master.countryunkid = Source.countryunkid " & _
            '              ", cfstate_master.code = Source.code " & _
            '              ", cfstate_master.name = Source.name " & _
            '              ", cfstate_master.name1 = Source.name1 " & _
            '              ", cfstate_master.name2 = Source.name2 " & _
            '              ", cfstate_master.isactive = Source.isactive " & _
            '        "FROM    " & mLinkDesk & "cfstate_master AS Source " & _
            '        "WHERE   cfstate_master.stateunkid = Source.stateunkid " & _
            '                "AND cfstate_master.companyunkid = " & intClientCode & " " & _
            '                "AND cfstate_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND Source.Syncdatetime IS NULL "

            'objDataOp.ExecNonQuery(StrQ)

            'If objDataOp.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Throw exForce
            'End If

            'INSERT NEW RECORDS
            'StrQ = "INSERT  INTO " & mlinkweb & "cfstate_master " & _
            '                "( Comp_Code, companyunkid, stateunkid, countryunkid, code, name, isactive, name1, name2) " & _
            '        "SELECT  '" & StrCode & "', " & intClientCode & ", cfstate_master.stateunkid, cfstate_master.countryunkid, cfstate_master.code, cfstate_master.name, cfstate_master.isactive, cfstate_master.name1, cfstate_master.name2 " & _
            '        "FROM    " & mLinkDesk & "cfstate_master " & _
            '                "LEFT JOIN " & mlinkweb & "cfstate_master AS Webcfstate_master ON cfstate_master.stateunkid = Webcfstate_master.stateunkid " & _
            '                                                      "AND Webcfstate_master.companyunkid = " & intClientCode & " " & _
            '                                                      "AND Webcfstate_master.Comp_Code = '" & StrCode & "' " & _
            '        "WHERE   " & mLinkDesk & "cfstate_master.isactive = 1 " & _
            '                "AND ISNULL(Webcfstate_master.isactive, 1) = 1 " & _
            '                "AND cfstate_master.Syncdatetime IS NULL " & _
            '                "AND Webcfstate_master.stateunkid IS NULL "
            StrQ = "SELECT  '" & StrCode & "', " & intClientCode & ", cfstate_master.stateunkid, cfstate_master.countryunkid, cfstate_master.code, cfstate_master.name, cfstate_master.isactive, cfstate_master.name1, cfstate_master.name2, Syncdatetime " & _
                    "FROM    " & mLinkDesk & "cfstate_master " & _
                    "WHERE   1 = 1 " & _
                            "AND cfstate_master.Syncdatetime IS NULL "

            Dim dsState As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsState)
            'Sohail (08 Oct 2018) -- End

            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadStateMaster(UserCred, strErrorMessage, dsState, True)
            WebRef.UploadStateMaster(strErrorMessage, dsState, True)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If


            'UPDATE SYNCDATETIME IN SOURCE TABLE
            'StrQ = "UPDATE  " & mLinkDesk & "cfstate_master " & _
            '        "SET     Syncdatetime = GETDATE() " & _
            '        "FROM    " & mlinkweb & "cfstate_master AS Webcfstate_master " & _
            '        "WHERE   Webcfstate_master.stateunkid = " & mLinkDesk & "cfstate_master.stateunkid " & _
            '                "AND Webcfstate_master.companyunkid = " & intClientCode & " " & _
            '                "AND Webcfstate_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND " & mLinkDesk & "cfstate_master.Syncdatetime IS NULL "
            StrQ = "UPDATE  " & mLinkDesk & "cfstate_master " & _
                    "SET     Syncdatetime = GETDATE() " & _
                    "WHERE   " & mLinkDesk & "cfstate_master.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If
            '****** STATE MASTER   - UPDATE, INSERT - END


            If bw IsNot Nothing Then
                bw.ReportProgress(2)
            End If


            '****** ZIPCODE MASTER   - UPDATE, INSERT - START
            'UPDATE EXISTING RECORDS
            'StrQ = "UPDATE  " & mlinkweb & "cfzipcode_master " & _
            '        "SET     cfzipcode_master.countryunkid = Source.countryunkid " & _
            '              ", cfzipcode_master.stateunkid = Source.stateunkid " & _
            '              ", cfzipcode_master.cityunkid = Source.cityunkid " & _
            '              ", cfzipcode_master.zipcode_code = Source.zipcode_code " & _
            '              ", cfzipcode_master.zipcode_no = Source.zipcode_no " & _
            '              ", cfzipcode_master.isactive = Source.isactive " & _
            '        "FROM    " & mLinkDesk & "cfzipcode_master AS Source " & _
            '        "WHERE   cfzipcode_master.zipcodeunkid = Source.zipcodeunkid " & _
            '                "AND cfzipcode_master.companyunkid = " & intClientCode & " " & _
            '                "AND cfzipcode_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND Source.Syncdatetime IS NULL "

            'objDataOp.ExecNonQuery(StrQ)

            'If objDataOp.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Throw exForce
            'End If

            'INSERT NEW RECORDS
            'StrQ = "INSERT  INTO " & mlinkweb & "cfzipcode_master " & _
            '                "( Comp_Code, companyunkid, zipcodeunkid, countryunkid, stateunkid, cityunkid, zipcode_code, zipcode_no, isactive) " & _
            '        "SELECT  '" & StrCode & "', " & intClientCode & ", cfzipcode_master.zipcodeunkid, cfzipcode_master.countryunkid, cfzipcode_master.stateunkid, cfzipcode_master.cityunkid, cfzipcode_master.zipcode_code, cfzipcode_master.zipcode_no, cfzipcode_master.isactive " & _
            '        "FROM    " & mLinkDesk & "cfzipcode_master " & _
            '                "LEFT JOIN " & mlinkweb & "cfzipcode_master AS Webcfzipcode_master ON cfzipcode_master.zipcodeunkid = Webcfzipcode_master.zipcodeunkid " & _
            '                                                      "AND Webcfzipcode_master.companyunkid = " & intClientCode & " " & _
            '                                                      "AND Webcfzipcode_master.Comp_Code = '" & StrCode & "' " & _
            '        "WHERE   cfzipcode_master.isactive = 1 " & _
            '                "AND ISNULL(Webcfzipcode_master.isactive, 1) = 1 " & _
            '                "AND cfzipcode_master.Syncdatetime IS NULL " & _
            '                "AND Webcfzipcode_master.zipcodeunkid IS NULL "
            StrQ = "SELECT  '" & StrCode & "', " & intClientCode & ", cfzipcode_master.zipcodeunkid, cfzipcode_master.countryunkid, cfzipcode_master.stateunkid, cfzipcode_master.cityunkid, cfzipcode_master.zipcode_code, cfzipcode_master.zipcode_no, cfzipcode_master.isactive, Syncdatetime " & _
                    "FROM    " & mLinkDesk & "cfzipcode_master " & _
                    "WHERE   1 = 1 " & _
                            "AND cfzipcode_master.Syncdatetime IS NULL "

            Dim dsZipCode As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsZipCode)
            'Sohail (08 Oct 2018) -- End

            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadZipCodeMaster(UserCred, strErrorMessage, dsZipCode, True)
            WebRef.UploadZipCodeMaster(strErrorMessage, dsZipCode, True)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'UPDATE SYNCDATETIME IN SOURCE TABLE
            'StrQ = "UPDATE  " & mLinkDesk & "cfzipcode_master " & _
            '        "SET     Syncdatetime = GETDATE() " & _
            '        "FROM    " & mlinkweb & "cfzipcode_master AS Webcfzipcode_master " & _
            '        "WHERE   Webcfzipcode_master.zipcodeunkid = " & mLinkDesk & "cfzipcode_master.zipcodeunkid " & _
            '                "AND Webcfzipcode_master.companyunkid = " & intClientCode & " " & _
            '                "AND Webcfzipcode_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND " & mLinkDesk & "cfzipcode_master.Syncdatetime IS NULL "
            StrQ = "UPDATE  " & mLinkDesk & "cfzipcode_master " & _
                    "SET     Syncdatetime = GETDATE() " & _
                    "WHERE   " & mLinkDesk & "cfzipcode_master.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If
            '****** ZIPCODE MASTER   - UPDATE, INSERT - END


            If bw IsNot Nothing Then
                bw.ReportProgress(3)
            End If


            '****** COMMON MASTER   - UPDATE, INSERT - START
            'UPDATE EXISTING RECORDS
            'StrQ = "UPDATE  " & mlinkweb & "cfcommon_master " & _
            '        "SET     cfcommon_master.code = Source.code " & _
            '              ", cfcommon_master.alias = Source.alias " & _
            '              ", cfcommon_master.name = Source.name " & _
            '              ", cfcommon_master.name1 = Source.name1 " & _
            '              ", cfcommon_master.name2 = Source.name2 " & _
            '              ", cfcommon_master.mastertype = Source.mastertype " & _
            '              ", cfcommon_master.isactive = Source.isactive " & _
            '              ", cfcommon_master.isrefreeallowed = Source.isrefreeallowed " & _
            '        "FROM    cfcommon_master AS Source " & _
            '        "WHERE   cfcommon_master.masterunkid = Source.masterunkid " & _
            '                "AND NOT ( Source.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND Source.issyncwithrecruitment = 0) " & _
            '                "AND cfcommon_master.companyunkid = " & intClientCode & " " & _
            '                "AND cfcommon_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND Source.Syncdatetime IS NULL "
            ''Sohail (22 Apr 2015) - [AND NOT ( cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND cfcommon_master.issyncwithrecruitment = 0) ]

            'objDataOp.ExecNonQuery(StrQ)

            'If objDataOp.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Throw exForce
            'End If

            'INSERT NEW RECORDS
            'StrQ = "INSERT  INTO " & mlinkweb & "cfcommon_master " & _
            '                "( Comp_Code, companyunkid, masterunkid, code, alias, name, name1, name2, mastertype, isactive, isrefreeallowed) " & _
            '        "SELECT  '" & StrCode & "', " & intClientCode & ", cfcommon_master.masterunkid, cfcommon_master.code, cfcommon_master.alias, cfcommon_master.name, cfcommon_master.name1, cfcommon_master.name2, cfcommon_master.mastertype, cfcommon_master.isactive, cfcommon_master.isrefreeallowed " & _
            '        "FROM    cfcommon_master " & _
            '                "LEFT JOIN " & mlinkweb & "cfcommon_master AS Webcfcommon_master ON cfcommon_master.masterunkid = Webcfcommon_master.masterunkid " & _
            '                                                      "AND Webcfcommon_master.companyunkid = " & intClientCode & " " & _
            '                                                      "AND Webcfcommon_master.Comp_Code = '" & StrCode & "' " & _
            '        "WHERE   cfcommon_master.isactive = 1 " & _
            '                "AND ISNULL(Webcfcommon_master.isactive, 1) = 1 " & _
            '                "AND NOT ( cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND cfcommon_master.issyncwithrecruitment = 0) " & _
            '                "AND cfcommon_master.Syncdatetime IS NULL " & _
            '                "AND Webcfcommon_master.masterunkid IS NULL "
            ''Sohail (22 Apr 2015) - [AND NOT ( cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND cfcommon_master.issyncwithrecruitment = 0) ]
            'Sohail (10 May 2019) -- Start
            'ZRA Issue - 74.1 - Attachments with not marked with "sync with recritment" are also getting exported.
            'StrQ = "SELECT  '" & StrCode & "', " & intClientCode & ", cfcommon_master.masterunkid, cfcommon_master.code, cfcommon_master.alias, cfcommon_master.name, cfcommon_master.name1, cfcommon_master.name2, cfcommon_master.mastertype, cfcommon_master.isactive, cfcommon_master.isrefreeallowed, ISNULL(cfcommon_master.issyncwithrecruitment, 0) AS issyncwithrecruitment, Syncdatetime " & _
            '        "FROM    cfcommon_master " & _
            '        "WHERE   1 = 1 " & _
            '                "AND cfcommon_master.Syncdatetime IS NULL "
            StrQ = "SELECT  '" & StrCode & "', " & intClientCode & ", cfcommon_master.masterunkid, cfcommon_master.code, cfcommon_master.alias, cfcommon_master.name, cfcommon_master.name1, cfcommon_master.name2, cfcommon_master.mastertype, cfcommon_master.isactive, cfcommon_master.isrefreeallowed, ISNULL(cfcommon_master.issyncwithrecruitment, 0) AS issyncwithrecruitment, Syncdatetime " & _
                    "FROM    cfcommon_master " & _
                    "WHERE   1 = 1 " & _
                            "/*AND NOT ( (cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND cfcommon_master.issyncwithrecruitment = 0) OR (cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_SOURCE & " AND cfcommon_master.issyncwithrecruitment = 0) )*/ " & _
                            "AND cfcommon_master.Syncdatetime IS NULL "
            'Sohail (12 Apr 2022) - [commented line:AND NOT ( (cfcommon_master.mastertype =]
            'Sohail (27 Sep 2019) - [VACANCY_SOURCE]
            'Sohail (10 May 2019) -- End

            Dim dsCommon As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsCommon)
            'Sohail (08 Oct 2018) -- End

            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadCommonMaster(UserCred, strErrorMessage, dsCommon, True)
            WebRef.UploadCommonMaster(strErrorMessage, dsCommon, True)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'UPDATE SYNCDATETIME IN SOURCE TABLE
            'StrQ = "UPDATE  cfcommon_master " & _
            '        "SET     Syncdatetime = GETDATE() " & _
            '        "FROM    " & mlinkweb & "cfcommon_master AS Webcfcommon_master " & _
            '        "WHERE   Webcfcommon_master.masterunkid = cfcommon_master.masterunkid " & _
            '                "AND NOT ( cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND cfcommon_master.issyncwithrecruitment = 0) " & _
            '                "AND Webcfcommon_master.companyunkid = " & intClientCode & " " & _
            '                "AND Webcfcommon_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND cfcommon_master.Syncdatetime IS NULL "
            'Sohail (22 Apr 2015) - [AND NOT ( cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND cfcommon_master.issyncwithrecruitment = 0) ]
            StrQ = "UPDATE  cfcommon_master " & _
                    "SET     Syncdatetime = GETDATE() " & _
                    "WHERE   NOT ( cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.ATTACHMENT_TYPES & " AND cfcommon_master.issyncwithrecruitment = 0) " & _
                            "AND cfcommon_master.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If
            '****** COMMON MASTER   - UPDATE, INSERT - END


            If bw IsNot Nothing Then
                bw.ReportProgress(4)
            End If


            '****** SKILLS MASTER   - UPDATE, INSERT - START
            'UPDATE EXISTING RECORDS
            'StrQ = "UPDATE  " & mlinkweb & "hrskill_master " & _
            '        "SET     hrskill_master.skillcode = Source.skillcode " & _
            '              ", hrskill_master.skillname = Source.skillname " & _
            '              ", hrskill_master.skillcategoryunkid = Source.skillcategoryunkid " & _
            '              ", hrskill_master.description = Source.description " & _
            '              ", hrskill_master.skillname1 = Source.skillname1 " & _
            '              ", hrskill_master.skillname2 = Source.skillname2 " & _
            '              ", hrskill_master.isactive = Source.isactive " & _
            '        "FROM    hrskill_master AS Source " & _
            '        "WHERE   hrskill_master.skillunkid = Source.skillunkid " & _
            '                "AND hrskill_master.companyunkid = " & intClientCode & " " & _
            '                "AND hrskill_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND Source.Syncdatetime IS NULL "

            'objDataOp.ExecNonQuery(StrQ)

            'If objDataOp.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Throw exForce
            'End If

            'INSERT NEW RECORDS
            'StrQ = "INSERT  INTO " & mlinkweb & "hrskill_master " & _
            '                "( Comp_Code, companyunkid, skillunkid, skillcode, skillname, skillcategoryunkid, description, isactive, skillname1, skillname2) " & _
            '        "SELECT  '" & StrCode & "', " & intClientCode & ", hrskill_master.skillunkid, hrskill_master.skillcode, hrskill_master.skillname, hrskill_master.skillcategoryunkid, hrskill_master.description, hrskill_master.isactive, hrskill_master.skillname1, hrskill_master.skillname2 " & _
            '        "FROM    hrskill_master " & _
            '                "LEFT JOIN " & mlinkweb & "hrskill_master AS Webhrskill_master ON hrskill_master.skillunkid = Webhrskill_master.skillunkid " & _
            '                                                      "AND Webhrskill_master.companyunkid = " & intClientCode & " " & _
            '                                                      "AND Webhrskill_master.Comp_Code = '" & StrCode & "' " & _
            '        "WHERE   hrskill_master.isactive = 1 " & _
            '                "AND ISNULL(Webhrskill_master.isactive, 1) = 1 " & _
            '                "AND hrskill_master.Syncdatetime IS NULL " & _
            '                "AND Webhrskill_master.skillunkid IS NULL "
            StrQ = "SELECT  '" & StrCode & "', " & intClientCode & ", hrskill_master.skillunkid, hrskill_master.skillcode, hrskill_master.skillname, hrskill_master.skillcategoryunkid, hrskill_master.description, hrskill_master.isactive, hrskill_master.skillname1, hrskill_master.skillname2, Syncdatetime " & _
                    "FROM    hrskill_master " & _
                    "WHERE   1 = 1 " & _
                            "AND hrskill_master.Syncdatetime IS NULL "

            Dim dsSkill As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsSkill)
            'Sohail (08 Oct 2018) -- End

            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadSkillMaster(UserCred, strErrorMessage, dsSkill, True)
            WebRef.UploadSkillMaster(strErrorMessage, dsSkill, True)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'UPDATE SYNCDATETIME IN SOURCE TABLE
            StrQ = "UPDATE  hrskill_master " & _
                    "SET     Syncdatetime = GETDATE() " & _
                    "WHERE   hrskill_master.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If
            '****** SKILLS MASTER   - UPDATE, INSERT - END


            If bw IsNot Nothing Then
                bw.ReportProgress(5)
            End If


            '****** INSTITUTE MASTER   - UPDATE, INSERT - START
            'UPDATE EXISTING RECORDS
            'StrQ = "UPDATE  " & mlinkweb & "hrinstitute_master " & _
            '        "SET     hrinstitute_master.institute_code = Source.institute_code " & _
            '              ", hrinstitute_master.institute_name = Source.institute_name " & _
            '              ", hrinstitute_master.institute_address = Source.institute_address " & _
            '              ", hrinstitute_master.telephoneno = Source.telephoneno " & _
            '              ", hrinstitute_master.institute_fax = Source.institute_fax " & _
            '              ", hrinstitute_master.institute_email = Source.institute_email " & _
            '              ", hrinstitute_master.contact_person = Source.contact_person " & _
            '              ", hrinstitute_master.stateunkid = Source.stateunkid " & _
            '              ", hrinstitute_master.cityunkid = Source.cityunkid " & _
            '              ", hrinstitute_master.countryunkid = Source.countryunkid " & _
            '              ", hrinstitute_master.ishospital = Source.ishospital " & _
            '              ", hrinstitute_master.isactive = Source.isactive " & _
            '        "FROM    hrinstitute_master AS Source " & _
            '        "WHERE   hrinstitute_master.instituteunkid = Source.instituteunkid " & _
            '                "AND hrinstitute_master.companyunkid = " & intClientCode & " " & _
            '                "AND hrinstitute_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND Source.Syncdatetime IS NULL "

            'objDataOp.ExecNonQuery(StrQ)

            'If objDataOp.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Throw exForce
            'End If

            'INSERT NEW RECORDS
            'StrQ = "INSERT  INTO " & mlinkweb & "hrinstitute_master " & _
            '                "( Comp_Code, companyunkid, instituteunkid, institute_code, institute_name, institute_address, telephoneno, institute_fax, institute_email, description, contact_person, stateunkid, cityunkid, countryunkid, ishospital, isactive) " & _
            '        "SELECT  '" & StrCode & "', " & intClientCode & ", hrinstitute_master.instituteunkid, hrinstitute_master.institute_code, hrinstitute_master.institute_name, hrinstitute_master.institute_address, hrinstitute_master.telephoneno, hrinstitute_master.institute_fax, hrinstitute_master.institute_email, hrinstitute_master.description, hrinstitute_master.contact_person, hrinstitute_master.stateunkid, hrinstitute_master.cityunkid, hrinstitute_master.countryunkid, hrinstitute_master.ishospital, hrinstitute_master.isactive " & _
            '        "FROM    hrinstitute_master " & _
            '                "LEFT JOIN " & mlinkweb & "hrinstitute_master AS Webhrinstitute_master ON hrinstitute_master.instituteunkid = Webhrinstitute_master.instituteunkid " & _
            '                                                      "AND Webhrinstitute_master.companyunkid = " & intClientCode & " " & _
            '                                                      "AND Webhrinstitute_master.Comp_Code = '" & StrCode & "' " & _
            '        "WHERE   hrinstitute_master.isactive = 1 " & _
            '                "AND ISNULL(Webhrinstitute_master.isactive, 1) = 1 " & _
            '                "AND hrinstitute_master.ishospital = 0 AND hrinstitute_master.issync_recruit = 1 " & _
            '                "AND hrinstitute_master.Syncdatetime IS NULL " & _
            '                "AND Webhrinstitute_master.instituteunkid IS NULL "
            StrQ = "SELECT  '" & StrCode & "', " & intClientCode & ", hrinstitute_master.instituteunkid, hrinstitute_master.institute_code, hrinstitute_master.institute_name, hrinstitute_master.institute_address, hrinstitute_master.telephoneno, hrinstitute_master.institute_fax, hrinstitute_master.institute_email, hrinstitute_master.description, hrinstitute_master.contact_person, hrinstitute_master.stateunkid, hrinstitute_master.cityunkid, hrinstitute_master.countryunkid, hrinstitute_master.ishospital, hrinstitute_master.isactive, ISNULL(hrinstitute_master.issync_recruit, 0) AS issync_recruit, Syncdatetime " & _
                    "FROM    hrinstitute_master " & _
                    "WHERE   1 = 1 " & _
                            "AND hrinstitute_master.Syncdatetime IS NULL "

            Dim dsInstitute As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsInstitute)
            'Sohail (08 Oct 2018) -- End

            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadInstituteMaster(UserCred, strErrorMessage, dsInstitute, True)
            WebRef.UploadInstituteMaster(strErrorMessage, dsInstitute, True)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'UPDATE SYNCDATETIME IN SOURCE TABLE
            StrQ = "UPDATE  hrinstitute_master " & _
                    "SET     Syncdatetime = GETDATE() " & _
                    "WHERE   hrinstitute_master.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If
            '****** INSTITUTE MASTER   - UPDATE, INSERT - END


            If bw IsNot Nothing Then
                bw.ReportProgress(6)
            End If


            '****** QUALIFICATION MASTER   - UPDATE, INSERT - START
            'UPDATE EXISTING RECORDS
            'StrQ = "UPDATE  " & mlinkweb & "hrqualification_master " & _
            '        "SET     hrqualification_master.qualificationcode = Source.qualificationcode " & _
            '              ", hrqualification_master.qualificationname = Source.qualificationname " & _
            '              ", hrqualification_master.qualificationgroupunkid = Source.qualificationgroupunkid " & _
            '              ", hrqualification_master.description = Source.description " & _
            '              ", hrqualification_master.isactive = Source.isactive " & _
            '              ", hrqualification_master.qualificationname1 = Source.qualificationname1 " & _
            '              ", hrqualification_master.qualificationname2 = Source.qualificationname2 " & _
            '              ", hrqualification_master.resultgroupunkid = Source.resultgroupunkid " & _
            '        "FROM    hrqualification_master AS Source " & _
            '        "WHERE   hrqualification_master.qualificationunkid = Source.qualificationunkid " & _
            '                "AND hrqualification_master.companyunkid = " & intClientCode & " " & _
            '                "AND hrqualification_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND Source.Syncdatetime IS NULL "

            'objDataOp.ExecNonQuery(StrQ)

            'If objDataOp.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Throw exForce
            'End If

            'INSERT NEW RECORDS
            'StrQ = "INSERT  INTO " & mlinkweb & "hrqualification_master " & _
            '                "( Comp_Code, companyunkid, qualificationunkid, qualificationcode, qualificationname, qualificationgroupunkid, description, isactive, qualificationname1, qualificationname2, resultgroupunkid) " & _
            '        "SELECT  '" & StrCode & "', " & intClientCode & ", hrqualification_master.qualificationunkid, hrqualification_master.qualificationcode, hrqualification_master.qualificationname, hrqualification_master.qualificationgroupunkid, hrqualification_master.description, hrqualification_master.isactive, hrqualification_master.qualificationname1, hrqualification_master.qualificationname2, hrqualification_master.resultgroupunkid " & _
            '        "FROM    hrqualification_master " & _
            '                "LEFT JOIN " & mlinkweb & "hrqualification_master AS Webhrqualification_master ON hrqualification_master.qualificationunkid = Webhrqualification_master.qualificationunkid " & _
            '                                                      "AND Webhrqualification_master.companyunkid = " & intClientCode & " " & _
            '                                                      "AND Webhrqualification_master.Comp_Code = '" & StrCode & "' " & _
            '        "WHERE   hrqualification_master.isactive = 1 " & _
            '                "AND ISNULL(Webhrqualification_master.isactive, 1) = 1 AND hrqualification_master.issync_recruit = 1 " & _
            '                "AND hrqualification_master.Syncdatetime IS NULL " & _
            '                "AND Webhrqualification_master.qualificationunkid IS NULL "
            StrQ = "SELECT  '" & StrCode & "', " & intClientCode & ", hrqualification_master.qualificationunkid, hrqualification_master.qualificationcode, hrqualification_master.qualificationname, hrqualification_master.qualificationgroupunkid, hrqualification_master.description, hrqualification_master.isactive, hrqualification_master.qualificationname1, hrqualification_master.qualificationname2, ISNULL(hrqualification_master.resultgroupunkid, 0) AS resultgroupunkid, ISNULL(hrqualification_master.issync_recruit, 0) AS issync_recruit, Syncdatetime " & _
                    "FROM    hrqualification_master " & _
                    "WHERE   1 = 1 " & _
                            "AND ISNULL(hrqualification_master.issync_recruit, 0) = 1 " & _
                            "AND hrqualification_master.Syncdatetime IS NULL "
            'Sohail (30 Aug 2019) - NMB Recruitment UAT # - 76.1 - On qualification group, they don’t want to display all the qualification group masters, the only want to display Diploma, and Degrees only.

            Dim dsQuali As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsQuali)
            'Sohail (08 Oct 2018) -- End

            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadQualificationMaster(UserCred, strErrorMessage, dsQuali, True)
            WebRef.UploadQualificationMaster(strErrorMessage, dsQuali, True)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'UPDATE SYNCDATETIME IN SOURCE TABLE
            StrQ = "UPDATE  hrqualification_master " & _
                    "SET     Syncdatetime = GETDATE() " & _
                    "WHERE   hrqualification_master.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If
            '****** QUALIFICATION MASTER   - UPDATE, INSERT - END


            If bw IsNot Nothing Then
                bw.ReportProgress(7)
            End If

            'Sohail (12 Nov 2020) -- Start
            'NMB Issue # : - All companies configuration data were exporting to recruitment portal.
            Dim intCompanyUnkID As Integer = 0
            'Sohail (12 Nov 2020) -- End

            '****   Email Sender Address Detail   *******************
            'StrQ = "UPDATE  " & mlinkweb & "cfcompany_info " & _
            '        "SET     sendername = '" & Company._Object._Sendername & "' " & _
            '              ", senderaddress = '" & Company._Object._Senderaddress & "' " & _
            '              ", reference = '" & Company._Object._Reference & "' " & _
            '              ", mailserverip = '" & Company._Object._Mailserverip & "' " & _
            '              ", mailserverport = " & Company._Object._Mailserverport & " " & _
            '              ", username = '" & Company._Object._Username & "' " & _
            '              ", password = '" & Company._Object._Password & "' " & _
            '              ", isloginssl = " & IIf(Company._Object._Isloginssl = True, 1, 0) & " " & _
            '              ", mail_body = '" & Company._Object._Mail_Body & "' " & _
            '              ", applicant_declaration = '" & ConfigParameter._Object._ApplicantDeclaration & "' " & _
            '        "WHERE   company_code = '" & StrCode & "' " & _
            '                "AND companyunkid = " & intClientCode & " " 'Sohail (28 Mar 2012) - [applicant_declaration]


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT  companyunkid, code AS company_code, sendername, senderaddress, reference, mailserverip, mailserverport, username, password, isloginssl, mail_body, '" & ConfigParameter._Object._ApplicantDeclaration.Replace("'", "''") & "' AS applicant_declaration, " & ConfigParameter._Object._TimeZoneMinuteDifference & " AS timezone_minute_diff,image, " & CInt(Int(ConfigParameter._Object._QualificationCertificateAttachmentMandatory)) & " AS isQualiCertAttachMandatory " & _
            '       "FROM    " & mLinkDesk & "cfcompany_master " & _
            '       "WHERE   code = '" & StrCode & "' "
            StrQ = "SELECT  companyunkid, code AS company_code, sendername, senderaddress, reference, mailserverip, mailserverport, username, password, isloginssl, mail_body, CAST(1 AS BIT) AS isemailpwd_encrypted, '" & strApplicantDeclaration.Replace("'", "''") & "' AS applicant_declaration, " & intTimeZoneMinuteDifference & " AS timezone_minute_diff,image, " & CInt(Int(blnQualificationCertificateAttachmentMandatory)) & " AS isQualiCertAttachMandatory " & _
                    ", '" & strDateFormat & "' AS Date_Format " & _
                    ", '" & strDateSeparator & "' AS DateSeparator " & _
                    ", '" & strAdminEmail & "' AS admin_email " & _
                    ", ISNULL(email_type, 0) AS email_type " & _
                    ", ISNULL(ews_url, '') AS ews_url " & _
                    ", ISNULL(ews_domain, '') AS ews_domain " & _
                    ", " & CInt(Int(blnMiddleNameMandatory)) & " AS MiddleNameMandatory " & _
                    ", " & CInt(Int(blnGenderMandatory)) & " AS GenderMandatory " & _
                    ", " & CInt(Int(blnBirthDateMandatory)) & " AS BirthDateMandatory " & _
                    ", " & CInt(Int(blnAddress1Mandatory)) & " AS Address1Mandatory " & _
                    ", " & CInt(Int(blnAddress2Mandatory)) & " AS Address2Mandatory " & _
                    ", " & CInt(Int(blnCountryMandatory)) & " AS CountryMandatory " & _
                    ", " & CInt(Int(blnStateMandatory)) & " AS StateMandatory " & _
                    ", " & CInt(Int(blnCityMandatory)) & " AS CityMandatory " & _
                    ", " & CInt(Int(blnPostCodeMandatory)) & " AS PostCodeMandatory " & _
                    ", " & CInt(Int(blnMaritalStatusMandatory)) & " AS MaritalStatusMandatory " & _
                    ", " & CInt(Int(blnLanguage1Mandatory)) & " AS Language1Mandatory " & _
                    ", " & CInt(Int(blnNationalityMandatory)) & " AS NationalityMandatory " & _
                    ", " & CInt(Int(blnOneSkillMandatory)) & " AS OneSkillMandatory " & _
                    ", " & CInt(Int(blnOneQualificationMandatory)) & " AS OneQualificationMandatory " & _
                    ", " & CInt(Int(blnOneJobExperienceMandatory)) & " AS OneJobExperienceMandatory " & _
                    ", " & CInt(Int(blnOneReferenceMandatory)) & " AS OneReferenceMandatory " & _
                    ", " & intApplicantQualificationSortBy & " AS ApplicantQualificationSortBy " & _
                    ", " & CInt(Int(blnRegionMandatory)) & " AS RegionMandatory " & _
                    ", " & CInt(Int(blnHighestQualificationMandatory)) & " AS HighestQualificationMandatory " & _
                    ", " & CInt(Int(blnEmployerMandatory)) & " AS EmployerMandatory " & _
                    ", " & CInt(Int(blnMotherTongueMandatory)) & " AS MotherTongueMandatory " & _
                    ", " & CInt(Int(blnCurrentSalaryMandatory)) & " AS CurrentSalaryMandatory " & _
                    ", " & CInt(Int(blnExpectedSalaryMandatory)) & " AS ExpectedSalaryMandatory " & _
                    ", " & CInt(Int(blnExpectedBenefitsMandatory)) & " AS ExpectedBenefitsMandatory " & _
                    ", " & CInt(Int(blnNoticePeriodMandatory)) & " AS NoticePeriodMandatory " & _
                    ", " & CInt(Int(blnEarliestPossibleStartDateMandatory)) & " AS EarliestPossibleStartDateMandatory " & _
                    ", " & CInt(Int(blnCommentsMandatory)) & " AS CommentsMandatory " & _
                    ", " & CInt(Int(blnVacancyFoundOutFromMandatory)) & " AS VacancyFoundOutFromMandatory " & _
                    ", " & CInt(Int(blnCompanyNameJobHistoryMandatory)) & " AS CompanyNameJobHistoryMandatory " & _
                    ", " & CInt(Int(blnPositionHeldMandatory)) & " AS PositionHeldMandatory " & _
                    ", " & CInt(Int(blnSendJobConfirmationEmail)) & " AS SendJobConfirmationEmail " & _
                    ", " & CInt(Int(blnVacancyAlert)) & " AS isVacancyalert " & _
                    ", " & CInt(Int(intMaxVacancyAlert)) & " AS MaxVacancyAlert " & _
                    ", " & CInt(Int(blnAttachApplicantCV)) & " AS AttachApplicantCV " & _
                    ", " & CInt(Int(blnOneCurriculamVitaeMandatory)) & " AS OneCurriculamVitaeMandatory " & _
                    ", " & CInt(Int(blnOneCoverLetterMandatory)) & " AS OneCoverLetterMandatory " & _
                    ", " & CInt(Int(blnQualificationStartDateMandatory)) & " AS QualificationStartDateMandatory " & _
                    ", " & CInt(Int(blnQualificationAwardDateMandatory)) & " AS QualificationAwardDateMandatory " & _
                    " FROM    " & mLinkDesk & "cfcompany_master " & _
                    " WHERE   code = '" & StrCode & "' "
            'Gajanan [29-AUG-2019] -- [blnQualificationStartDateMandatory],[blnQualificationAwardDateMandatory]
            'Sohail (04 Jul 2019) - [blnOneCurriculamVitaeMandatory, blnOneCoverLetterMandatory]
            'Pinkal (18-May-2018) --  'Enhancement - (Ref # 148) An applicant submit job application should get an email of his/her CV.[", " & CInt(Int(blnAttachApplicantCV)) & " AS AttachApplicantCV " & _]


            'Sohail (30 Nov 2017) - [email_type, ews_url, ews_domain]
            'Sohail (24 Jul 2017) - [CompanyNameJobHistoryMandatory, PositionHeldMandatory]
            'Sohail (18 May 2017) - [isemailpwd_encrypted]
            'Nilay (13 Apr 2017) -- [ApplicantQualificationSortBy, RegionMandatory, HighestQualificationMandatory, EmployerMandatory, MotherTongueMandatory,
            'CurrentSalaryMandatory, ExpectedSalaryMandatory, ExpectedBenefitsMandatory, NoticePeriodMandatory, EarliestPossibleStartDateMandatory, 
            'CommentsMandatory, VacancyFoundOutFromMandatory]

            'Sohail (05 Dec 2016) - [Date_Format, DateSeparator to OneReferenceMandatory]
            'Shani(24-Aug-2015) -- End

            'Sohail (13 Oct 2015) - [ConfigParameter._Object._QualificationCertificateAttachmentMandatory]

            'Pinkal (12-Feb-2018) -- 'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.[ ", " & CInt(Int(blnSendJobConfirmationEmail)) & " AS SendJobConfirmationEmail " & _     ", " & CInt(Int(blnVacancyAlert)) & " AS isVacancyalert " & _  ", " & CInt(Int(intMaxVacancyAlert)) & " AS MaxVacancyAlert " & _]

            Dim dsCompany As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Sohail (18 May 2017) -- Start
            'Enhancement - 67.1 - Exporting encrypted email password for online recruitment.
            If dsCompany.Tables(0).Rows.Count > 0 Then
                'Sohail (12 Nov 2020) -- Start
                'NMB Issue # : - All companies configuration data were exporting to recruitment portal.
                intCompanyUnkID = CInt(dsCompany.Tables(0).Rows(0).Item("companyunkid"))
                'Sohail (12 Nov 2020) -- End
                dsCompany.Tables(0).Rows(0).Item("password") = clsSecurity.Encrypt(dsCompany.Tables(0).Rows(0).Item("password").ToString, "ezee")
                dsCompany.Tables(0).AcceptChanges()
            End If
            'Sohail (18 May 2017) -- End

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsCompany)
            'Sohail (08 Oct 2018) -- End

            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadCompanyInfo(UserCred, strErrorMessage, dsCompany)
            WebRef.UploadCompanyInfo(strErrorMessage, dsCompany)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'Hemant (30 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            '************************* CONFIGURATION - START ***********************************

            'S.SANDEEP |04-MAY-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-880
            Dim iList As New List(Of String)
            For Each tstEnum As enNIDA_WEB_Service In System.Enum.GetValues(GetType(enNIDA_WEB_Service))
                iList.Add("'_NIDASRV_" & CInt(tstEnum).ToString() & "'")
            Next
            Dim StrNiDASettings = String.Join(",", iList.ToArray())
            If StrNiDASettings Is Nothing Then StrNiDASettings = ""
            'S.SANDEEP |04-MAY-2023| -- END

            StrQ = "SELECT " & _
                   "  '" & StrCode & "' " & _
                   ", " & intClientCode & " " & _
                   ", configunkid " & _
                   ", key_name" & _
                   ", key_value " & _
                   ", companyunkid" & _
                   ",  Syncdatetime " & _
                   "FROM    " & mLinkDesk & "cfconfiguration " & _
                   "WHERE   1 = 1 " & _
                           "AND companyunkid = " & intCompanyUnkID & " " & _
                           "/*AND " & mLinkDesk & "cfconfiguration.Syncdatetime IS NULL*/ " & _
                           "AND key_name in ('JOBCONFIRMATIONTEMPLATEID','JOBALERTTEMPLATEID','INTERNALJOBALERTTEMPLATEID', 'SHOWARUTISIGNATURE' " & _
                           ", 'RECRUITMANDATORYFIELDSIDS', 'RECRUITMANDATORYSKILLS', 'RECRUITMANDATORYQUALIFICATIONS', 'RECRUITMANDATORYJOBEXPERIENCES', 'RECRUITMANDATORYREFERENCES', 'RECRUITMENTEMAILSETUPUNKID' " & _
                           ", 'HIDERECRUITEMENTPERMANENTADDRESS', 'HIDERECRUITEMENTSKILLSCREEN', 'HIDERECRUITEMENTQUALIFICATIONREMARK', 'HIDERECRUITEMENTOTHERINFOSCREEN' " & _
                           ", 'ISNIDAWRINTEGRATED', " & StrNiDASettings & " " & _
                           ") "
            'S.SANDEEP |04-MAY-2023| - [ISNIDAWRINTEGRATED, StrNiDASettings]
            'Sohail (31 Mar 2022) - [HIDERECRUITEMENTPERMANENTADDRESS, HIDERECRUITEMENTSKILLSCREEN, HIDERECRUITEMENTQUALIFICATIONREMARK, HIDERECRUITEMENTOTHERINFOSCREEN]
            'Sohail (06 Oct 2021) - [RECRUITMENTEMAILSETUPUNKID]
            'Sohail (12 Nov 2020) - [AND companyunkid = " & intCompanyUnkID & " ]
            'Hemant (17 Dec 2019) - [AND " & mLinkDesk & "cfconfiguration.Syncdatetime IS NULL --> /*AND " & mLinkDesk & "cfconfiguration.Syncdatetime IS NULL*/]


            'Hemant (17 Dec 2019) -- Start
            'Dim dsConfiguration As DataSet = objDataOp.ExecQuery(StrQ, "List")
             Dim dsConfigAll As DataSet = objDataOp.ExecQuery(StrQ, "List")
            'Hemant (17 Dec 2019) -- End

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Hemant (17 Dec 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            Dim dsConfiguration As DataSet
            dsConfiguration = dsConfigAll.Copy
            Dim d_R() As DataRow = dsConfiguration.Tables(0).Select("Syncdatetime IS NULL ")
            If dsConfiguration.Tables(0).Rows.Count <> d_R.Length Then
                If d_R.Length <= 0 Then
                    dsConfiguration.Tables(0).Rows.Clear()
                Else
                    Dim dt As DataTable = d_R.CopyToDataTable()
                    dsConfiguration.Tables.Clear()
                    dsConfiguration.Tables.Add(dt)
                End If

            End If
            'Hemant (17 Dec 2019) -- End

            Call RemoveDateTimeZones(dsConfiguration)

            strErrorMessage = ""

            WebRef.UploadConfiguration(strErrorMessage, dsConfiguration, True)


            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'UPDATE SYNCDATETIME IN SOURCE TABLE
            StrQ = "UPDATE  " & mLinkDesk & "cfconfiguration " & _
                    "SET     Syncdatetime = GETDATE() " & _
                    "WHERE   " & mLinkDesk & "cfconfiguration.Syncdatetime IS NULL " & _
                    "AND companyunkid = " & intCompanyUnkID & "  " & _
                    "AND key_name in ('JOBCONFIRMATIONTEMPLATEID','JOBALERTTEMPLATEID','INTERNALJOBALERTTEMPLATEID', 'SHOWARUTISIGNATURE' " & _
                    ", 'RECRUITMANDATORYFIELDSIDS', 'RECRUITMANDATORYSKILLS', 'RECRUITMANDATORYQUALIFICATIONS', 'RECRUITMANDATORYJOBEXPERIENCES', 'RECRUITMANDATORYREFERENCES' " & _
                    ") "
            'Sohail (12 Nov 2020) - [AND companyunkid = " & intCompanyUnkID & " ], [, 'RECRUITMANDATORYFIELDSIDS', 'RECRUITMANDATORYSKILLS', 'RECRUITMANDATORYQUALIFICATIONS', 'RECRUITMANDATORYJOBEXPERIENCES', 'RECRUITMANDATORYREFERENCES']

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            If bw IsNot Nothing Then
                bw.ReportProgress(8)
            End If
            '************************* CONFIGURATION - END *************************************

            '************************* LETTERTYPE - START ***********************************
            'Hemant (17 Dec 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES.
            Dim dr() As DataRow = dsConfigAll.Tables(0).Select(" key_name IN ('JOBCONFIRMATIONTEMPLATEID','JOBALERTTEMPLATEID','INTERNALJOBALERTTEMPLATEID') ")
            If dr.Length > 0 Then

                Dim strIDS As String = String.Join(",", (From p In dr.AsEnumerable Select (p.Item("key_value").ToString)).ToArray)
                'Hemant (17 Dec 2019) -- End

            StrQ = "SELECT " & _
                   "  '" & StrCode & "' " & _
                   ", " & intClientCode & " " & _
                   ", lettertypeunkid " & _
                   ", alias" & _
                   ", lettermasterunkid " & _
                   ", lettername" & _
                   ", lettername1" & _
                   ", lettername2" & _
                   ", lettercontent" & _
                   ", isactive" & _
                   ", fieldtypeid" & _
                   ", Syncdatetime " & _
                   "FROM  hrlettertype_master " & _
                   "WHERE   1 = 1 " & _
                                "AND hrlettertype_master.lettertypeunkid IN (" & strIDS & ") " & _
                           "AND hrlettertype_master.Syncdatetime IS NULL "
                'Hemant (17 Dec 2019) - [AND hrlettertype_master.lettertypeunkid IN (" & strIDS & ") " ]

            Dim dsLetterType As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            Call RemoveDateTimeZones(dsLetterType)

            strErrorMessage = ""

            WebRef.UploadLetterType(strErrorMessage, dsLetterType, True)


            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'UPDATE SYNCDATETIME IN SOURCE TABLE
            StrQ = "UPDATE  hrlettertype_master " & _
                    "SET     Syncdatetime = GETDATE() " & _
                        "WHERE   hrlettertype_master.Syncdatetime IS NULL " & _
                                "AND hrlettertype_master.lettertypeunkid IN (" & strIDS & ") "
                'Hemant (17 Dec 2019) - [AND hrlettertype_master.lettertypeunkid IN (" & strIDS & ") ]

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            If bw IsNot Nothing Then
                bw.ReportProgress(9)
            End If

            End If  'Hemant (17 Dec 2019) 
            '************************* LETTERTYPE - END *************************************

            'Hemant (30 Oct 2019) -- End

            '****** VACANCY MASTER   - UPDATE, INSERT - START
            'UPDATE EXISTING RECORDS
            'StrQ = "UPDATE  " & mlinkweb & "rcvacancy_master " & _
            '        "SET     rcvacancy_master.vacancytitle = Source.vacancytitle " & _
            '              ", rcvacancy_master.openingdate = Source.openingdate " & _
            '              ", rcvacancy_master.closingdate = Source.closingdate " & _
            '              ", rcvacancy_master.interview_startdate = Source.interview_startdate " & _
            '              ", rcvacancy_master.interview_closedate = Source.interview_closedate " & _
            '              ", rcvacancy_master.noofposition = Source.noofposition " & _
            '              ", rcvacancy_master.experience = Source.experience " & _
            '              ", rcvacancy_master.responsibilities_duties = Source.responsibilities_duties " & _
            '              ", rcvacancy_master.remark = Source.remark " & _
            '              ", rcvacancy_master.isvoid = Source.isvoid " & _
            '              ", rcvacancy_master.isexternalvacancy = Source.isexternalvacancy " & _
            '        "FROM    rcvacancy_master AS Source " & _
            '        "WHERE   rcvacancy_master.vacancyunkid = Source.vacancyunkid " & _
            '                "AND rcvacancy_master.companyunkid = " & intClientCode & " " & _
            '                "AND rcvacancy_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND Source.Syncdatetime IS NULL "

            'objDataOp.ExecNonQuery(StrQ)

            'If objDataOp.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Throw exForce
            'End If

            'INSERT NEW RECORDS
            'StrQ = "INSERT  INTO " & mlinkweb & "rcvacancy_master " & _
            '                "( Comp_Code, companyunkid, vacancyunkid, vacancytitle, openingdate, closingdate, interview_startdate, interview_closedate, noofposition, experience, responsibilities_duties, remark, isvoid, isexternalvacancy) " & _
            '        "SELECT  '" & StrCode & "', " & intClientCode & ", rcvacancy_master.vacancyunkid, rcvacancy_master.vacancytitle, rcvacancy_master.openingdate, rcvacancy_master.closingdate, rcvacancy_master.interview_startdate, rcvacancy_master.interview_closedate, rcvacancy_master.noofposition, rcvacancy_master.experience, rcvacancy_master.responsibilities_duties, rcvacancy_master.remark, rcvacancy_master.isvoid, rcvacancy_master.isexternalvacancy " & _
            '        "FROM    rcvacancy_master " & _
            '                "LEFT JOIN " & mlinkweb & "rcvacancy_master AS Webrcvacancy_master ON rcvacancy_master.vacancyunkid = Webrcvacancy_master.vacancyunkid " & _
            '                                                      "AND Webrcvacancy_master.companyunkid = " & intClientCode & " " & _
            '                                                      "AND Webrcvacancy_master.Comp_Code = '" & StrCode & "' " & _
            '        "WHERE   ISNULL(rcvacancy_master.isvoid, 0) = 0 " & _
            '                "AND ISNULL(Webrcvacancy_master.isvoid, 0) = 0 " & _
            '                "AND rcvacancy_master.Syncdatetime IS NULL " & _
            '                "AND rcvacancy_master.iswebexport = 1 " & _
            '                "AND Webrcvacancy_master.vacancyunkid IS NULL "

            'Shani (02-Nov-2016) -- Start
            'Enhancement - Add Employee Configuration Settings form in Aruti Configuration [Assign by Sohail]
            'StrQ = "SELECT  '" & StrCode & "', " & intClientCode & ", rcvacancy_master.vacancyunkid, rcvacancy_master.vacancytitle, rcvacancy_master.openingdate, rcvacancy_master.closingdate, rcvacancy_master.interview_startdate, rcvacancy_master.interview_closedate, rcvacancy_master.noofposition, rcvacancy_master.experience, rcvacancy_master.responsibilities_duties, rcvacancy_master.remark, rcvacancy_master.isvoid, rcvacancy_master.isexternalvacancy, ISNULL(rcvacancy_master.iswebexport, 0) AS iswebexport, Syncdatetime " & _
            '        "FROM    rcvacancy_master " & _
            '        "WHERE   1 = 1 " & _
            '                "AND rcvacancy_master.openingdate IS NOT NULL " & _
            '                "AND rcvacancy_master.Syncdatetime IS NULL "


            'Pinkal (12-Feb-2018) -- Start
            'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.

            StrQ = "SELECT " & _
                   "     '" & StrCode & "' " & _
                   "    ," & intClientCode & " " & _
                   "    ,rcvacancy_master.vacancyunkid " & _
                   "    ,rcvacancy_master.vacancytitle " & _
                   "    ,rcvacancy_master.openingdate " & _
                   "    ,rcvacancy_master.closingdate " & _
                   "    ,rcvacancy_master.interview_startdate " & _
                   "    ,rcvacancy_master.interview_closedate " & _
                   "    ,rcvacancy_master.noofposition " & _
                   "    ,rcvacancy_master.experience " & _
                   "    ,rcvacancy_master.responsibilities_duties " & _
                   "    ,rcvacancy_master.remark " & _
                   "    ,rcvacancy_master.isvoid " & _
                   "    ,rcvacancy_master.isexternalvacancy " & _
                   "    ,ISNULL(rcvacancy_master.iswebexport, 0) AS iswebexport " & _
                   "    ,rcvacancy_master.Syncdatetime " & _
                   "    ,rcvacancy_master.stationunkid " & _
                   "    ,rcvacancy_master.deptgroupunkid " & _
                   "    ,rcvacancy_master.departmentunkid " & _
                   "    ,rcvacancy_master.sectionunkid " & _
                   "    ,rcvacancy_master.unitunkid " & _
                   "    ,rcvacancy_master.jobgroupunkid " & _
                   "    ,rcvacancy_master.jobunkid " & _
                   "    ,rcvacancy_master.employeementtypeunkid " & _
                   "    ,rcvacancy_master.paytypeunkid " & _
                   "    ,rcvacancy_master.shifttypeunkid " & _
                   "    ,rcvacancy_master.pay_from " & _
                   "    ,rcvacancy_master.pay_to " & _
                   "    ,ISNULL(rcvacancy_master.isskillbold, 0) AS isskillbold " & _
                   "    ,ISNULL(rcvacancy_master.isskillitalic, 0) AS isskillitalic " & _
                   "    ,ISNULL(rcvacancy_master.isqualibold, 0) AS isqualibold " & _
                   "    ,ISNULL(rcvacancy_master.isqualiitalic, 0) AS isqualiitalic " & _
                   "    ,ISNULL(rcvacancy_master.isexpbold, 0) AS isexpbold " & _
                   "    ,ISNULL(rcvacancy_master.isexpitalic, 0) AS isexpitalic " & _
                   "    ,ISNULL(rcvacancy_master.isbothintext, 0) AS isbothintext " & _
                   ",   ISNULL(rcvacancy_master.other_qualification, '') AS other_qualification " & _
                   ",   ISNULL(rcvacancy_master.other_skill, '') AS other_skill " & _
                   ",   ISNULL(rcvacancy_master.other_language, '') AS other_language " & _
                   ",   ISNULL(rcvacancy_master.other_experience, '') AS other_experience " & _
                   ",   ISNULL(rcvacancy_master.classgroupunkid, 0) AS classgroupunkid " & _
                   ",   ISNULL(rcvacancy_master.classunkid, 0) AS classunkid " & _
                   ",   " & intTimeZoneMinuteDifference & " AS TimeZone "
            'Sohail (11 Nov 2021) -- [classgroupunkid, classunkid]
            'Sohail (13 Sep 2021) -- [other_qualification, other_skill, other_language, other_experience]
            'Sohail (18 Feb 2020) -- [isbothintext]
            'Sohail (27 Sep 2019) -- [isskillbold, isskillitalic, isqualibold, isqualiitalic, isexpbold, isexpitalic]
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'If intDatabaseServerSetting = enDatabaseServerSetting.DEFAULT_SETTING Then
            '    StrQ &= ", 'https://www.arutihr.com/ArutiHRM/Login.aspx?ext='  AS link "
            'Else
            '    StrQ &= ",'https://" & strDatabaseServer.Replace("\apayroll", "") & "/ArutiHRM/Login.aspx?ext=' AS link"
            '    'StrQ &= ",'https://localhost:44381/ArutiHRM/Login.aspx?ext=' AS link "
            'End If
            StrQ &= ", '" & strWebServiceLink.ToUpper.Replace("ARUTIRECRUITMENTSERVICE.ASMX", "") & "Login.aspx?ext='  AS link "
            'Sohail (16 Nov 2018) -- End

            StrQ &= " FROM    rcvacancy_master " & _
                    "WHERE   1 = 1 " & _
                            "AND rcvacancy_master.openingdate IS NOT NULL " & _
                            "AND rcvacancy_master.Syncdatetime IS NULL "
            'Shani (02-Nov-2016) -- End

            '[ "    , " & intTimeZoneMinuteDifference & " AS TimeZone" & _ 

            ''Pinkal (12-Feb-2018) -- End

            'S.SANDEEP |05-JUN-2023| -- START
            'ISSUE/ENHANCEMENT : A1X-948
            StrQ &= " AND ISNULL(rcvacancy_master.isapproved,0) = 1 "
            'S.SANDEEP |0-JUN-2023| -- END

            Dim dsVacancy As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            strErrorMessage = ""

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsVacancy)
            'Sohail (08 Oct 2018) -- End

            'Pinkal (12-Feb-2018) -- Start 
            'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF .
            'WebRef.UploadVacancyMaster(UserCred, strErrorMessage, dsVacancy, True)
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadVacancyMaster(UserCred, strErrorMessage, dsVacancy, True, clsCommon_Master.enCommonMaster.VACANCY_MASTER, clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE)
            WebRef.UploadVacancyMaster(strErrorMessage, dsVacancy, True, clsCommon_Master.enCommonMaster.VACANCY_MASTER, clsCommon_Master.enCommonMaster.EMPLOYEMENT_TYPE)
            'Sohail (16 Nov 2018) -- End
            'Pinkal (12-Feb-2018) -- End 

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'Hemant (30 Oct 2019) -- Start
            'ISSUE/ENHANCEMENT(NMB) :  RECRUITMENTS UAT CHANGES(Point No 4 : After publishing an external vacancy, system should send job alerts to all the applicants that have created profiles in the portal. Email content as below.)
            'UPDATE SYNCDATETIME IN SOURCE TABLE
            StrQ = "UPDATE  rcvacancy_master " & _
                    "SET     Syncdatetime = GETDATE() " & _
                    "WHERE   rcvacancy_master.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If
            '****** VACANCY MASTER   - UPDATE, INSERT - END
            'Hemant (30 Oct 2019)

            'Shani (02-Nov-2016) -- Start
            'Enhancement - Add Employee Configuration Settings form in Aruti Configuration [Assign by Sohail]
            '============= Upload Job_Skill_Tran Start ===============

            If bw IsNot Nothing Then
                bw.ReportProgress(10)
            End If

            StrQ = "SELECT " & _
                   "     jobskilltranunkid " & _
                   "    ,jobunkid " & _
                   "    ,skillcategoryunkid " & _
                   "    ,skillunkid " & _
                   "    ,Syncdatetime " & _
                   "    ,isactive " & _
                   "FROM hrjob_skill_tran " & _
                   "where 1 = 1 " & _
                   "    AND Syncdatetime IS NULL "
            'Sohail (23 Mar 2018) - [isactive]

            Dim dsJobSkillTran As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsJobSkillTran)
            'Sohail (08 Oct 2018) -- End

            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadJobSkillTran(UserCred, strErrorMessage, dsJobSkillTran, True)
            WebRef.UploadJobSkillTran(strErrorMessage, dsJobSkillTran, True)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            StrQ = "UPDATE  hrjob_skill_tran " & _
                  "SET     Syncdatetime = GETDATE() " & _
                  "WHERE   hrjob_skill_tran.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            '============= Upload Job_Skill_Tran End ===============

            '============= Upload Job_Qualification_Tran Start ===============

            If bw IsNot Nothing Then
                bw.ReportProgress(11)
            End If

            StrQ = "SELECT " & _
                   "     jobqualificationtranunkid " & _
                   "    ,jobunkid " & _
                   "    ,qualificationgroupunkid " & _
                   "    ,qualificationunkid " & _
                   "    ,Syncdatetime " & _
                   "    ,isactive " & _
                   "FROM hrjob_qualification_tran " & _
                   "where 1 = 1 " & _
                   "    AND Syncdatetime IS NULL "
            'Sohail (23 Mar 2018) - [isactive]

            Dim dsJobQualificationTran As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsJobQualificationTran)
            'Sohail (08 Oct 2018) -- End

            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadJobQualificationTran(UserCred, strErrorMessage, dsJobQualificationTran, True)
            WebRef.UploadJobQualificationTran(strErrorMessage, dsJobQualificationTran, True)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            StrQ = "UPDATE  hrjob_qualification_tran " & _
                 "SET     Syncdatetime = GETDATE() " & _
                 "WHERE   hrjob_qualification_tran.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If
            '============= Upload Job_Qualification_Tran End ===============
            'Shani (02-Nov-2016) -- End


            ''UPDATE SYNCDATETIME IN SOURCE TABLE
            'StrQ = "UPDATE  rcvacancy_master " & _
            '        "SET     Syncdatetime = GETDATE() " & _
            '        "WHERE   rcvacancy_master.Syncdatetime IS NULL "

            'objDataOp.ExecNonQuery(StrQ)

            'If objDataOp.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Throw exForce
            'End If
            ''****** VACANCY MASTER   - UPDATE, INSERT - END


            If bw IsNot Nothing Then
                bw.ReportProgress(12)
            End If


            '****** RESULT MASTER   - UPDATE, INSERT - START
            'UPDATE EXISTING RECORDS
            'StrQ = "UPDATE  " & mlinkweb & "hrresult_master " & _
            '        "SET     hrresult_master.resultcode = Source.resultcode " & _
            '              ", hrresult_master.resultname = Source.resultname " & _
            '              ", hrresult_master.resultgroupunkid = Source.resultgroupunkid " & _
            '              ", hrresult_master.description = Source.description " & _
            '              ", hrresult_master.isactive = Source.isactive " & _
            '              ", hrresult_master.resultname1 = Source.resultname1 " & _
            '              ", hrresult_master.resultname2 = Source.resultname2 " & _
            '        "FROM    hrresult_master AS Source " & _
            '        "WHERE   hrresult_master.resultunkid = Source.resultunkid " & _
            '                "AND hrresult_master.companyunkid = " & intClientCode & " " & _
            '                "AND hrresult_master.Comp_Code = '" & StrCode & "' " & _
            '                "AND Source.Syncdatetime IS NULL "

            'objDataOp.ExecNonQuery(StrQ)

            'If objDataOp.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Throw exForce
            'End If

            'INSERT NEW RECORDS
            'StrQ = "INSERT  INTO " & mlinkweb & "hrresult_master " & _
            '                "( Comp_Code, companyunkid, resultunkid, resultcode, resultname, resultgroupunkid, description, isactive, resultname1, resultname2) " & _
            '        "SELECT  '" & StrCode & "', " & intClientCode & ", hrresult_master.resultunkid, hrresult_master.resultcode, hrresult_master.resultname, hrresult_master.resultgroupunkid, hrresult_master.description, hrresult_master.isactive, hrresult_master.resultname1, hrresult_master.resultname2 " & _
            '        "FROM    hrresult_master " & _
            '                "LEFT JOIN " & mlinkweb & "hrresult_master AS Webhrresult_master ON hrresult_master.resultunkid = Webhrresult_master.resultunkid " & _
            '                                                      "AND Webhrresult_master.companyunkid = " & intClientCode & " " & _
            '                                                      "AND Webhrresult_master.Comp_Code = '" & StrCode & "' " & _
            '        "WHERE   hrresult_master.isactive = 1 " & _
            '                "AND ISNULL(Webhrresult_master.isactive, 1) = 1 " & _
            '                "AND hrresult_master.Syncdatetime IS NULL " & _
            '                "AND Webhrresult_master.resultunkid IS NULL "
            StrQ = "SELECT  '" & StrCode & "', " & intClientCode & ", hrresult_master.resultunkid, hrresult_master.resultcode, hrresult_master.resultname, hrresult_master.resultgroupunkid, hrresult_master.description, hrresult_master.isactive, hrresult_master.resultname1, hrresult_master.resultname2, Syncdatetime " & _
                    "FROM    hrresult_master " & _
                    "WHERE   1 = 1 " & _
                            "AND hrresult_master.Syncdatetime IS NULL "

            Dim dsResult As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsResult)
            'Sohail (08 Oct 2018) -- End

            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadResultMaster(UserCred, strErrorMessage, dsResult, True)
            WebRef.UploadResultMaster(strErrorMessage, dsResult, True)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'UPDATE SYNCDATETIME IN SOURCE TABLE
            StrQ = "UPDATE  hrresult_master " & _
                    "SET     Syncdatetime = GETDATE() " & _
                    "WHERE   hrresult_master.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If
            '****** RESULT MASTER   - UPDATE, INSERT - END


            If bw IsNot Nothing Then
                bw.ReportProgress(13)
            End If

            'Nilay (13 Apr 2017) -- Start
            '************************* SKILL EXPERTISE - START ***********************************
            Dim objSkillExp As New clsSkillExpertise_master
            If objSkillExp.InsertDefaultSkillExpertise() = False Then
                exForce = New Exception(objSkillExp._Message)
                Throw exForce
            End If

            StrQ = " SELECT " & _
                       "  '" & StrCode & "' " & _
                       " ," & intClientCode & " " & _
                       " ,rcskillexpertise_master.skillexpertiseunkid " & _
                       " ,rcskillexpertise_master.code " & _
                       " ,rcskillexpertise_master.name " & _
                       " ,rcskillexpertise_master.isactive " & _
                       " ,rcskillexpertise_master.name1 " & _
                       " ,rcskillexpertise_master.name2 " & _
                       " ,rcskillexpertise_master.Syncdatetime " & _
                   " FROM rcskillexpertise_master " & _
                   " WHERE   1=1 " & _
                   "    AND rcskillexpertise_master.Syncdatetime IS NULL "

            Dim dsSkillExpertise As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            'Sohail (08 Oct 2018) -- Start
            'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
            Call RemoveDateTimeZones(dsSkillExpertise)
            'Sohail (08 Oct 2018) -- End

            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'WebRef.UploadSkillExpertiseMaster(UserCred, strErrorMessage, dsSkillExpertise, True)
            WebRef.UploadSkillExpertiseMaster(strErrorMessage, dsSkillExpertise, True)
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            StrQ = " UPDATE rcskillexpertise_master SET " & _
                        " Syncdatetime = GETDATE() " & _
                   " WHERE rcskillexpertise_master.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            If bw IsNot Nothing Then
                bw.ReportProgress(14)
            End If
            '************************* SKILL EXPERTISE - END *************************************
            'Nilay (13 Apr 2017) -- End

            'Sohail (18 Feb 2020) -- Start
            'NMB Enhancement # : On job master,  Need a tab to define the language(s) attached to the job. Those languages will be displayed on job report.
            '============= Upload Job_Language_Tran Start ===============
            StrQ = "SELECT " & _
                   "     joblanguagetranunkid " & _
                   "    ,jobunkid " & _
                   "    ,masterunkid " & _
                   "    ,Syncdatetime " & _
                   "    ,isactive " & _
                   "FROM hrjob_language_tran " & _
                   "where 1 = 1 " & _
                   "    AND Syncdatetime IS NULL "

            Dim dsJobLanguageTran As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            Call RemoveDateTimeZones(dsJobLanguageTran)

            strErrorMessage = ""
            WebRef.UploadJobLanguageTran(strErrorMessage, dsJobLanguageTran, True)

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            StrQ = "UPDATE  hrjob_language_tran " & _
                  "SET     Syncdatetime = GETDATE() " & _
                  "WHERE   hrjob_language_tran.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            If bw IsNot Nothing Then
                bw.ReportProgress(15)
            End If
            '============= Upload Job_Language_Tran End ===============
            'Sohail (18 Feb 2020) -- End


            'Sohail (30 Aug 2019) -- Start
            'NMB Recruitment UAT # - 76.1 - On qualification group, they don’t want to display all the qualification group masters, the only want to display Diploma, and Degrees only.
            strErrorMessage = ""
            WebRef.UploadQualificationGroup(strErrorMessage, True)

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If
            'Sohail (30 Aug 2019) -- End

            If bw IsNot Nothing Then
                'Nilay (13 Apr 2017) -- Start
                'bw.ReportProgress(12)
                bw.ReportProgress(16)
                'Nilay (13 Apr 2017) -- End
            End If

            'Sohail (04 Sep 2020) -- Start
            'ZRA Enhancement : #  : .
            '============= Upload Job_Master Start ===============
            StrQ = "SELECT " & _
                   " jobunkid ,jobgroupunkid,jobheadunkid,jobunitunkid,jobsectionunkid,jobgradeunkid,job_code,job_name, " & _
                        "report_tounkid,create_date,total_position,terminate_date,  " & _
                        "desciription,userunkid,isactive,job_name1,job_name2,job_level,teamunkid,working_hrs,experience_comment, " & _
                        "experience_month,experience_year,jobclassgroupunkid,jobdepartmentunkid,indirectreport_tounkid,  " & _
                        "jobbranchunkid,jobdepartmentgrpunkid,jobunitgrpunkid,jobclassunkid,jobgradelevelunkid,jobsectiongrpunkid,critical  " & _
                                    ",jobtypeunkid, Syncdatetime " & _
                   "FROM hrjob_master " & _
                   "WHERE 1 = 1 " & _
                    "AND Syncdatetime IS NULL " & _
                    "AND hrjob_master.jobunkid IN (SELECT DISTINCT rcvacancy_master.jobunkid FROM rcvacancy_master  WHERE rcvacancy_master.openingdate IS NOT NULL ) "

            Dim dsJobMaster As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            Call RemoveDateTimeZones(dsJobMaster)

            strErrorMessage = ""
            WebRef.UploadJobMaster(strErrorMessage, dsJobMaster, True)

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            StrQ = "UPDATE  hrjob_master " & _
                  "SET     Syncdatetime = GETDATE() " & _
                  "WHERE   hrjob_master.Syncdatetime IS NULL " & _
                  "AND hrjob_master.jobunkid IN (SELECT DISTINCT rcvacancy_master.jobunkid FROM rcvacancy_master  WHERE rcvacancy_master.openingdate IS NOT NULL ) "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            If bw IsNot Nothing Then
                bw.ReportProgress(17)
            End If
            '============= Upload Job_Master End ===============
            'Sohail (04 Sep 2020) -- End

            'Sohail (06 Oct 2021) -- Start
            'NMB Enhancement : OLD-483 : Separate sender's email configuration on recruitment portal from company email.
            '************************* EMAIL SETUP - START ***********************************
            StrQ = "SELECT " & _
                   "  '" & StrCode & "' " & _
                   ", " & intClientCode & " " & _
                   ", emailsetupunkid " & _
                   ", sendername" & _
                   ", senderaddress " & _
                   ", reference " & _
                   ", mailserverip " & _
                   ", mailserverport " & _
                   ", username " & _
                   ", password " & _
                   ", isloginssl " & _
                   ", iscrt_authenticated " & _
                   ", isbypassproxy " & _
                   ", mail_body " & _
                   ", email_type " & _
                   ", ews_url " & _
                   ", ews_domain " & _
                   ", isvoid " & _
                   ", Syncdatetime " & _
                   "FROM  " & mLinkDesk & "cfemail_setup " & _
                   "WHERE   1 = 1 " & _
                           "AND cfemail_setup.Syncdatetime IS NULL "

            Dim dsEmailSetup As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            Call RemoveDateTimeZones(dsEmailSetup)

            strErrorMessage = ""

            WebRef.UploadEmailSetup(strErrorMessage, dsEmailSetup, True)


            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'UPDATE SYNCDATETIME IN SOURCE TABLE
            StrQ = "UPDATE  " & mLinkDesk & "cfemail_setup " & _
                    "SET     Syncdatetime = GETDATE() " & _
                        "WHERE   cfemail_setup.Syncdatetime IS NULL "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            If bw IsNot Nothing Then
                bw.ReportProgress(18)
            End If
            '************************* EMAIL SETUP - END *************************************
            'Sohail (06 Oct 2021) -- End

            'Sohail (11 Nov 2021) -- Start
            'NMB Enhancement : : Display Job Location on published vacancy on recruitment portal e.g Job Location - Northern Zone, Arusha.
            '============= Upload hrclassgroup_master Start ===============
            StrQ = "SELECT " & _
                   " classgroupunkid, code, name, description, isactive, name1, name2, Syncdatetime " & _
                    "FROM hrclassgroup_master " & _
                    "WHERE 1 = 1 " & _
                    "AND Syncdatetime IS NULL " & _
                    "AND hrclassgroup_master.classgroupunkid IN (SELECT DISTINCT rcvacancy_master.classgroupunkid FROM rcvacancy_master  WHERE rcvacancy_master.openingdate IS NOT NULL ) "

            Dim dsClassGroup As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            Call RemoveDateTimeZones(dsClassGroup)

            strErrorMessage = ""
            WebRef.UploadClassGroup(strErrorMessage, dsClassGroup, True)

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            StrQ = "UPDATE  hrclassgroup_master " & _
                  "SET     Syncdatetime = GETDATE() " & _
                  "WHERE   hrclassgroup_master.Syncdatetime IS NULL " & _
                  "AND hrclassgroup_master.classgroupunkid IN (SELECT DISTINCT rcvacancy_master.classgroupunkid FROM rcvacancy_master  WHERE rcvacancy_master.openingdate IS NOT NULL ) "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            If bw IsNot Nothing Then
                bw.ReportProgress(19)
            End If
            '============= Upload hrclassgroup_master End ===============

            '============= Upload hrclasses_master Start ===============
            StrQ = "SELECT " & _
                   " classesunkid, classgroupunkid, code, name, description, isactive, name1, name2, Syncdatetime " & _
                    "FROM hrclasses_master " & _
                    "WHERE 1 = 1 " & _
                    "AND Syncdatetime IS NULL " & _
                    "AND hrclasses_master.classesunkid IN (SELECT DISTINCT rcvacancy_master.classunkid FROM rcvacancy_master  WHERE rcvacancy_master.openingdate IS NOT NULL ) "

            Dim dsClass As DataSet = objDataOp.ExecQuery(StrQ, "List")

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            Call RemoveDateTimeZones(dsClass)

            strErrorMessage = ""
            WebRef.UploadClass(strErrorMessage, dsClass, True)

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            StrQ = "UPDATE  hrclasses_master " & _
                  "SET     Syncdatetime = GETDATE() " & _
                  "WHERE   hrclasses_master.Syncdatetime IS NULL " & _
                  "AND hrclasses_master.classesunkid IN (SELECT DISTINCT rcvacancy_master.classunkid FROM rcvacancy_master  WHERE rcvacancy_master.openingdate IS NOT NULL ) "

            objDataOp.ExecNonQuery(StrQ)

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            If bw IsNot Nothing Then
                bw.ReportProgress(20)
            End If
            '============= Upload hrclassgroup_master End ===============
            'Sohail (11 Nov 2021) -- End

            'Pinkal (12-Feb-2018) -- Start
            'Enhancement - JOB SEEKER ENHANCEMENT FOR PKF.

            If blnVacancyAlert AndAlso blnSendEmail Then
                strErrorMessage = ""

                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # TC008 : System crashes during after publishing the vacancy
                Dim arr(2) As Object
                arr(0) = WebRef
                arr(1) = UserCred
                'Sohail (14 Nov 2019) -- End
                Dim objThread As Threading.Thread
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # TC008 : System crashes during after publishing the vacancy
                'objThread = New Threading.Thread(AddressOf WebRef.SendJobAlertNotification)
                objThread = New Threading.Thread(AddressOf SendNotification)
                'Sohail (14 Nov 2019) -- End
                objThread.IsBackground = True
                'Sohail (16 Nov 2018) -- Start
                'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                'objThread.Start(UserCred)
                'Sohail (14 Nov 2019) -- Start
                'NMB UAT Enhancement # TC008 : System crashes during after publishing the vacancy
                'WebRef.UserCredentialsValue = UserCred
                'objThread.Start()
                objThread.Start(arr)
                'Sohail (14 Nov 2019) -- End
                'Sohail (16 Nov 2018) -- End
                objThread = Nothing

                If strErrorMessage <> "" Then
                    exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                    Throw exForce
                    Exit Try
                End If

            End If

            'Pinkal (12-Feb-2018) -- End

            'objDataOp.ReleaseTransaction(True)
            System.Windows.Forms.Cursor.Current = Cursors.Default
            Return True

        Catch ex As Exception
            'objDataOp.ReleaseTransaction(False)
            DisplayError.Show("-1", ex.Message, "ExportToWeb", mstrModuleName)
            Return False
        Finally
            StrQ = String.Empty : mLinkDesk = String.Empty
        End Try
    End Function
    'Sohail (18 May 2015) -- End
    'Sohail (28 Mar 2012) -- End

    'Sohail (14 Nov 2019) -- Start
    'NMB UAT Enhancement # TC008 : System crashes during after publishing the vacancy
    Private Sub SendNotification(ByVal arr As Object)
        Try
            Dim WebRef As New WR.ArutiRecruitmentService
            WebRef = CType(arr(0), WR.ArutiRecruitmentService)
            Dim UserCred As New WR.UserCredentials
            UserCred = CType(arr(1), WR.UserCredentials)

            WebRef.UserCredentialsValue = UserCred
            Dim strErrorMsg As String = ""
            WebRef.SendJobAlertNotification(strErrorMessage:=strErrorMsg)

            If strErrorMsg.Trim <> "" Then
                Throw New Exception(strErrorMsg)
                Exit Try
            End If

        Catch ex As Exception
            Dim mstrLogFileName As String = "Recruitment_ExportData_" & Format(DateAndTime.Now.Date, "yyyyMMdd") & ".log"
            IO.File.AppendAllText(mstrLogFileName, "Error in SendNotification : " & ex.Message & vbCrLf)
            Process.Start(mstrLogFileName)
        End Try
    End Sub
    'Sohail (14 Nov 2019) -- End

    'Sohail (18 May 2015) -- Start
    'Enhancement - Importing and Exporting through Web Service.
    '    Private Function ImportFromWeb(ByVal mlinkweb As String, ByVal StrCode As String, ByVal intClientCode As Integer, ByVal mdatabaseserverweb As String, ByVal bw As BackgroundWorker) As Boolean
    '        Dim StrQ As String = String.Empty
    '        Dim mLinkDesk As String = String.Empty
    '        Dim dsList As New DataSet
    '        Dim mintCurrApplicantUnkID, mintNewApplicantUnkID As Integer
    '        Dim blnResult As Boolean = False
    '        Dim exForce As Exception
    '        Dim ds As DataSet = Nothing 'Sohail (27 May 2013)

    '        Dim objDataOp As New clsDataOperation
    '        'Sohail (26 Aug 2011) -- Start
    '        Dim StartTime As DateTime = Now
    '        Dim mstrApplicantIDs As String = ""
    '        'Sohail (26 Aug 2011) -- End

    '        Try

    '            'objDataOp.BindTransaction()
    '            Cursor.Current = Cursors.WaitCursor
    '            mintCurrApplicantUnkID = -1 : mintNewApplicantUnkID = -1
    '            mLinkDesk = Company._Object._ConfigDatabaseName & ".."

    '            If ConfigParameter._Object._IsImgInDataBase = False AndAlso ConfigParameter._Object._PhotoPath = "" Then
    '                mstrMessage = Language.getMessage(mstrModuleName, 18, "Please select Image path from configuration -> Option -> Photo Path.")
    '                Return False
    '            End If

    '            'Sohail (23 Oct 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            'Call GetApplicantImportApplicantStatus()
    '            If GetApplicantImportApplicantStatus() = False Then
    '                Return False
    '            End If
    '            'Sohail (23 Oct 2012) -- End

    '            'Sohail (26 Aug 2011) -- Start
    '            Dim strMBoardNo As String = GetMotherBoardSrNo()
    '            Dim strMachine As String = My.Computer.Name
    '            Dim intTotalApplicant As Integer = 0

    '            '*** Compare current machine sr no with sr no in configuration database
    '            If mintImportStatus = 1 AndAlso strMBoardNo <> mstrMBoardSrNo Then
    '                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry! This Process is currently running on ") & mstrMachineName & Language.getMessage(mstrModuleName, 19, " Machine."), enMsgBoxStyle.Critical)
    '                Return False
    '            End If

    '            '*** Set Current Machine SrNo & Name
    '            If mstrMBoardSrNo <> strMBoardNo Then mstrMBoardSrNo = strMBoardNo
    '            If mstrMachineName <> strMachine Then mstrMachineName = strMachine

    '            '*** Put current machine import status so others can't do this process same time.
    '            objDataOp.ExecNonQuery("UPDATE " & Company._Object._ConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|1' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
    '            If objDataOp.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                Throw exForce
    '            End If
    '            'Sohail (26 Aug 2011) -- End

    '            StrQ = "SELECT applicantunkid, applicant_code,titleunkid,firstname,surname,othername,gender,email,present_address1,present_address2,present_countryunkid,present_stateunkid,present_province,present_post_townunkid,present_zipcode,present_road,present_estate,present_plotno,present_mobileno,present_alternateno,present_tel_no,present_fax,perm_address1,perm_address2,perm_countryunkid,perm_stateunkid,perm_province,perm_post_townunkid,perm_zipcode,perm_road,perm_estate,perm_plotno,perm_mobileno,perm_alternateno,perm_tel_no,perm_fax,birth_date,marital_statusunkid,anniversary_date,language1unkid,language2unkid,language3unkid,language4unkid,nationality,userunkid,vacancyunkid,isimport,other_skill,other_qualification "
    '            StrQ &= ", employeecode, referenceno " 'Sohail (23 Dec 2011)
    '            StrQ &= " , ISNULL(memberships, '') AS memberships, ISNULL(achievements, '') AS achievements, ISNULL(journalsresearchpapers, '') AS journalsresearchpapers " 'Sohail (30 May 2012)
    '            StrQ &= " FROM  " & mlinkweb & "rcapplicant_master where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and isnull(Syncdatetime,' ')=' ' AND ISNULL(referenceno,'') <> ''  " & vbCrLf

    '            dsList = objDataOp.ExecQuery(StrQ, "rcapplicant_master")

    '            'objDataOp.ExecNonQuery(StrQ) 'Sohail (26 Aug 2011)

    '            If objDataOp.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                Throw exForce
    '            End If

    '            'Sohail (09 Feb 2012) -- Start
    '            'TRA - ENHANCEMENT
    '            GintTotalApplicantToImport = dsList.Tables("rcapplicant_master").Rows.Count
    '            bw.ReportProgress(0)
    '            Dim intCount As Integer = 0
    '            'Sohail (09 Feb 2012) -- End

    '            For Each drRow As DataRow In dsList.Tables("rcapplicant_master").Rows

    '                'Sohail (12 Nov 2012) -- Start
    '                'TRA - ENHANCEMENT - To Skip the marked for deletion error AND value of a column has been changed after the containing row was last fetched error
    '                objDataOp = New clsDataOperation
    '                'Sohail (12 Nov 2012) -- End

    '                mintCurrApplicantUnkID = CInt(drRow.Item("applicantunkid").ToString)

    '                _Email = drRow.Item("email").ToString
    '                _Firstname = drRow.Item("firstname").ToString
    '                _Surname = drRow.Item("surname").ToString
    '                _Othername = drRow.Item("othername").ToString
    '                _Titleunkid = IIf(CInt(drRow.Item("titleunkid")) < 0, 0, CInt(drRow.Item("titleunkid")))
    '                _Gender = IIf(CInt(drRow.Item("gender")) < 0, 0, CInt(drRow.Item("gender")))
    '                _Vacancyunkid = CInt(drRow.Item("vacancyunkid"))

    '                '*******************| PERSONAL INFO |******************' START
    '                _Present_Address1 = drRow.Item("present_address1").ToString
    '                _Present_Address2 = drRow.Item("present_address2").ToString
    '                _Present_Alternateno = drRow.Item("present_alternateno").ToString
    '                _Present_Countryunkid = IIf(CInt(drRow.Item("present_countryunkid")) < 0, 0, CInt(drRow.Item("present_countryunkid")))
    '                _Present_Estate = drRow.Item("present_estate").ToString
    '                _Present_Fax = drRow.Item("present_fax").ToString
    '                _Present_Mobileno = drRow.Item("present_mobileno").ToString
    '                _Present_Plotno = drRow.Item("present_plotno").ToString
    '                _Present_Post_Townunkid = IIf(CInt(drRow.Item("present_post_townunkid")) < 0, 0, CInt(drRow.Item("present_post_townunkid")))
    '                _Present_Province = drRow.Item("present_province").ToString
    '                _Present_Road = drRow.Item("present_road").ToString
    '                _Present_Stateunkid = IIf(CInt(drRow.Item("present_stateunkid")) < 0, 0, CInt(drRow.Item("present_stateunkid")))
    '                _Present_Tel_No = drRow.Item("present_tel_no").ToString
    '                _Present_ZipCode = IIf(CInt(drRow.Item("present_zipcode")) < 0, 0, CInt(drRow.Item("present_zipcode")))
    '                _Perm_Address1 = drRow.Item("perm_address1").ToString
    '                _Perm_Address2 = drRow.Item("perm_address2").ToString
    '                _Perm_Alternateno = drRow.Item("perm_alternateno").ToString
    '                _Perm_Countryunkid = IIf(CInt(drRow.Item("perm_countryunkid")) < 0, 0, CInt(drRow.Item("perm_countryunkid")))
    '                _Perm_Estate = drRow.Item("perm_estate").ToString
    '                _Perm_Fax = drRow.Item("perm_fax").ToString
    '                _Perm_Mobileno = drRow.Item("perm_mobileno").ToString
    '                _Perm_Plotno = drRow.Item("perm_plotno").ToString
    '                _Perm_Post_Townunkid = IIf(CInt(drRow.Item("perm_post_townunkid")) < 0, 0, CInt(drRow.Item("perm_post_townunkid")))
    '                _Perm_Province = drRow.Item("perm_province").ToString
    '                _Perm_Road = drRow.Item("perm_road").ToString
    '                _Perm_Stateunkid = IIf(CInt(drRow.Item("perm_stateunkid")) < 0, 0, CInt(drRow.Item("perm_stateunkid")))
    '                _Perm_Tel_No = drRow.Item("perm_tel_no").ToString
    '                _Perm_ZipCode = IIf(CInt(drRow.Item("perm_zipcode")) < 0, 0, CInt(drRow.Item("perm_zipcode")))
    '                '*******************| PERSONAL INFO |******************' END

    '                '*******************| ADDITIONAL INFO |******************' START
    '                If drRow.Item("anniversary_date").ToString.Trim.Length > 0 Then
    '                    _Anniversary_Date = CDate(drRow.Item("anniversary_date"))
    '                    'Sohail (06 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT
    '                Else
    '                    _Anniversary_Date = Nothing
    '                    'Sohail (06 Nov 2012) -- End
    '                End If

    '                If drRow.Item("birth_date").ToString.Trim.Length > 0 Then
    '                    _Birth_Date = CDate(drRow.Item("birth_date"))
    '                    'Sohail (06 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT
    '                Else
    '                    _Birth_Date = Nothing
    '                    'Sohail (06 Nov 2012) -- End
    '                End If

    '                _Marital_Statusunkid = IIf(CInt(drRow.Item("marital_statusunkid")) < 0, 0, CInt(drRow.Item("marital_statusunkid")))
    '                _Nationality = IIf(CInt(drRow.Item("nationality")) < 0, 0, CInt(drRow.Item("nationality")))
    '                _Language1unkid = IIf(CInt(drRow.Item("language1unkid")) < 0, 0, CInt(drRow.Item("language1unkid")))
    '                _Language2unkid = IIf(CInt(drRow.Item("language2unkid")) < 0, 0, CInt(drRow.Item("language2unkid")))
    '                _Language3unkid = IIf(CInt(drRow.Item("language3unkid")) < 0, 0, CInt(drRow.Item("language3unkid")))
    '                _Language4unkid = IIf(CInt(drRow.Item("language4unkid")) < 0, 0, CInt(drRow.Item("language4unkid")))
    '                _Userunkid = User._Object._Userunkid 'CInt(drRow.Item("userunkid"))
    '                _OtherQualifications = drRow.Item("other_qualification").ToString
    '                _OtherSkills = drRow.Item("other_skill").ToString
    '                'Sohail (23 Dec 2011) -- Start
    '                _Employeecode = drRow.Item("employeecode").ToString
    '                _Referenceno = drRow.Item("referenceno").ToString
    '                'Sohail (23 Dec 2011) -- End
    '                'Sohail (30 May 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                _Memberships = drRow.Item("memberships").ToString
    '                _Achievements = drRow.Item("achievements").ToString
    '                _JournalsResearchPapers = drRow.Item("journalsresearchpapers").ToString
    '                'Sohail (30 May 2012) -- End
    '                '*******************| ADDITIONAL INFO |******************' END

    '                'Sohail (23 Dec 2011) -- Start
    '                'blnResult = Insert()
    '                Dim intExistingApplicantUnkId As Integer = 0
    '                Dim strApplicant_Code As String = ""
    '                Dim strFirstname As String = ""
    '                Dim strSurname As String = ""

    '                'Sohail (05 May 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                'isExistForWebImport(mstrReferenceno, mintVacancyunkid, intExistingApplicantUnkId, strApplicant_Code, strFirstname, strSurname)
    '                isExistForWebImport(mstrReferenceno, intExistingApplicantUnkId, strApplicant_Code, strFirstname, strSurname)
    '                'Sohail (05 May 2012) -- End
    '                If intExistingApplicantUnkId = 0 Then
    '                    mstrApplicant_Code = ""
    '                    blnResult = Insert()
    '                Else
    '                    mintApplicantunkid = intExistingApplicantUnkId
    '                    mstrApplicant_Code = strApplicant_Code
    '                    mstrFirstname = strFirstname
    '                    mstrSurname = strSurname
    '                    blnResult = Update()
    '                End If
    '                'Sohail (23 Dec 2011) -- End

    '                If blnResult = False And _Message <> "" Then
    '                    'Sohail (26 Aug 2011) -- Start
    '                    'Return blnResult
    '                    exForce = New Exception(_Message)
    '                    Throw exForce
    '                    'Sohail (26 Aug 2011) -- End

    '                    'Sohail (16 Sep 2013) -- Start
    '                    'TRA - Issue - If error occurred while generating Auto Applicant Code __Message return BLANK and all child table are inserted while parent table is rolled back.
    '                ElseIf blnResult = False Then
    '                    Exit For
    '                    'Sohail (16 Sep 2013) -- End

    '                End If

    '                If intExistingApplicantUnkId = 0 Then 'Sohail (23 Dec 2011)
    '                    mintNewApplicantUnkID = _Applicantunkid
    '                    'Sohail (23 Dec 2011) -- Start
    '                Else
    '                    mintNewApplicantUnkID = intExistingApplicantUnkId
    '                    'Sohail (23 Dec 2011) -- End
    '                End If 'Sohail (23 Dec 2011)


    '                'Sohail (26 Aug 2011) -- Start
    '                If mstrApplicantIDs.Trim = "" Then
    '                    mstrApplicantIDs = mintNewApplicantUnkID.ToString
    '                Else
    '                    mstrApplicantIDs &= ", " & mintNewApplicantUnkID.ToString
    '                End If
    '                'Sohail (26 Aug 2011) -- End

    '                '**********[ JOBHISTORY ]********* START
    '                'Sohail (23 Dec 2011) -- Start
    '                If intExistingApplicantUnkId > 0 Then
    '                    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcjobhistory", "jobhistorytranunkid", 3, 3) = False Then
    '                        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    StrQ = "DELETE FROM rcjobhistory WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                End If
    '                'Sohail (23 Dec 2011) -- End
    '                StrQ = "INSERT INTO rcjobhistory(applicantunkid,employername,companyname,designation,responsibility,joiningdate,terminationdate,officephone,leavingreason, achievements) " & vbCrLf
    '                StrQ &= "SELECT " & mintNewApplicantUnkID & ",employername,companyname,designation,responsibility,joiningdate,terminationdate,officephone,leavingreason, achievements FROM  " & mlinkweb & "rcjobhistory WHERE Comp_Code='" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                StrQ &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "
    '                'Sohail (22 Apr 2015) - [achievements]

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    'Sohail (09 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    'Throw exForce
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                    'Sohail (09 Nov 2012) -- End
    '                End If

    '                'Sohail (08 May 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcjobhistory", "jobhistorytranunkid", 1, 1, , , , mintUserunkid) = False Then
    '                    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                    Throw exForce
    '                End If
    '                'Sohail (08 May 2012) -- End

    '                '**********[ JOBHISTORY ]********* END

    '                '**********[ SKILL TRAN ]********* START
    '                'Sohail (23 Dec 2011) -- Start
    '                If intExistingApplicantUnkId > 0 Then
    '                    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapplicantskill_tran", "skilltranunkid", 3, 3) = False Then
    '                        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    StrQ = "DELETE FROM rcapplicantskill_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                End If
    '                'Sohail (23 Dec 2011) -- End
    '                StrQ = "INSERT into  rcapplicantskill_tran(applicantunkid,skillcategoryunkid,skillunkid,remark, other_skillcategory, other_skill )" & vbCrLf
    '                StrQ &= "SELECT " & mintNewApplicantUnkID & ",skillcategoryunkid,skillunkid,remark, other_skillcategory, other_skill  from  " & mlinkweb & "rcapplicantskill_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' '" & vbCrLf
    '                StrQ &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    'Sohail (09 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    'Throw exForce
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                    'Sohail (09 Nov 2012) -- End
    '                End If

    '                'Sohail (08 May 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapplicantskill_tran", "skilltranunkid", 1, 1, , , , mintUserunkid) = False Then
    '                    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                    Throw exForce
    '                End If
    '                'Sohail (08 May 2012) -- End
    '                '**********[ SKILL TRAN ]********* END

    '                '**********[ QUAKIFICATION TRAN ]********* START
    '                'Sohail (23 Dec 2011) -- Start
    '                If intExistingApplicantUnkId > 0 Then
    '                    'clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcjobhistory", "jobhistorytranunkid", 3, 3) = False Then
    '                    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapplicantqualification_tran", "qualificationtranunkid", 3, 3) = False Then
    '                        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    StrQ = "DELETE FROM rcapplicantqualification_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                End If
    '                'Sohail (23 Dec 2011) -- End
    '                'Sohail (21 Dec 2011) -- Start
    '                'StrQ = "INSERT INTO rcapplicantqualification_tran(applicantunkid,qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark)" & vbCrLf
    '                'StrQ &= "SELECT " & mintNewApplicantUnkID & ",qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark from  " & mlinkweb & "rcapplicantqualification_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                StrQ = "INSERT INTO rcapplicantqualification_tran(applicantunkid,qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark,resultunkid,gpacode, other_qualificationgrp, other_qualification, other_institute, other_resultcode, certificateno )" & vbCrLf
    '                StrQ &= "SELECT " & mintNewApplicantUnkID & ",qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark,resultunkid,gpacode, other_qualificationgrp, other_qualification, other_institute, other_resultcode, certificateno  FROM  " & mlinkweb & "rcapplicantqualification_tran WHERE Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                'Sohail (22 Apr 2015) - [certificateno]
    '                'Sohail (21 Dec 2011) -- End
    '                StrQ &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    'Sohail (09 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    'Throw exForce
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                    'Sohail (09 Nov 2012) -- End
    '                End If

    '                'Sohail (08 May 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapplicantqualification_tran", "qualificationtranunkid", 1, 1, , , , mintUserunkid) = False Then
    '                    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                    Throw exForce
    '                End If
    '                'Sohail (08 May 2012) -- End
    '                blnResult = True
    '                '**********[ QUAKIFICATION TRAN ]********* END

    '                'Sohail (03 Mar 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                '**********[ REFERENCE TRAN ]********* START
    '                If intExistingApplicantUnkId > 0 Then
    '                    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapp_reference_tran", "referencetranunkid", 3, 3) = False Then
    '                        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If

    '                    StrQ = "DELETE FROM rcapp_reference_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                End If
    '                StrQ = "INSERT INTO rcapp_reference_tran(applicantunkid, name, address, countryunkid, stateunkid, cityunkid, email, gender, position, telephone_no, mobile_no, relationunkid)" & vbCrLf
    '                StrQ &= "SELECT " & mintNewApplicantUnkID & ",name, address, countryunkid, stateunkid, cityunkid, email, gender, position, telephone_no, mobile_no, relationunkid from  " & mlinkweb & "rcapp_reference_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                StrQ &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    'Sohail (09 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    'Throw exForce
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                    'Sohail (09 Nov 2012) -- End
    '                End If

    '                'Sohail (08 May 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_reference_tran", "referencetranunkid", 1, 1, , , , mintUserunkid) = False Then
    '                    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                    Throw exForce
    '                End If
    '                'Sohail (08 May 2012) -- End
    '                blnResult = True
    '                '**********[ REFERENCE TRAN ]********* END

    '                '**********[ VACANCY MAPPING ]********* START
    '                'Sohail (27 May 2013) -- Start
    '                'TRA - ENHANCEMENT
    '                'If intExistingApplicantUnkId > 0 Then
    '                '    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapp_vacancy_mapping", "appvacancytranunkid", 3, 3) = False Then
    '                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                '        Throw exForce
    '                '    End If

    '                '    StrQ = "DELETE FROM rcapp_vacancy_mapping WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

    '                '    objDataOp.ExecNonQuery(StrQ)

    '                '    If objDataOp.ErrorMessage <> "" Then
    '                '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                '        Throw exForce
    '                '    End If
    '                'End If
    '                'StrQ = "INSERT INTO rcapp_vacancy_mapping(applicantunkid, vacancyunkid, userunkid, isactive)" & vbCrLf
    '                'StrQ &= "SELECT " & mintNewApplicantUnkID & ",vacancyunkid, 1, 1 from  " & mlinkweb & "rcapp_vacancy_mapping where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                'StrQ &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "
    '                'StrQ &= " AND vacancyunkid > 0 " 'Sohail (06 Nov 2012) -- To Prevent Vacancies which are updated from TEST LINK

    '                'objDataOp.ExecNonQuery(StrQ)

    '                'If objDataOp.ErrorMessage <> "" Then
    '                '    'Sohail (09 Nov 2012) -- Start
    '                '    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                '    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                '    'Throw exForce
    '                '    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                '        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                '        GoTo CONTINUE_FOR
    '                '    Else
    '                '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                '        Throw exForce
    '                '    End If
    '                '    'Sohail (09 Nov 2012) -- End
    '                'End If

    '                ''Sohail (08 May 2012) -- Start
    '                ''TRA - ENHANCEMENT
    '                'If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_vacancy_mapping", "appvacancytranunkid", 1, 1, , , , mintUserunkid) = False Then
    '                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                '    Throw exForce
    '                'End If
    '                ''Sohail (08 May 2012) -- End

    '                StrQ = "SELECT " & mintNewApplicantUnkID & " AS applicantunkid,vacancyunkid, 1 AS userunkid, 1 AS isactive from  " & mlinkweb & "rcapp_vacancy_mapping where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                StrQ &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "
    '                StrQ &= " AND vacancyunkid > 0 "

    '                Dim dsVM As DataSet = objDataOp.ExecQuery(StrQ, "vacancy_mapping")

    '                If objDataOp.ErrorMessage <> "" Then
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                End If

    '                Dim objVacancyMap As clsApplicant_Vacancy_Mapping
    '                Dim intAppVacancyMapId As Integer

    '                For Each dsRow As DataRow In dsVM.Tables("vacancy_mapping").Rows
    '                    objVacancyMap = New clsApplicant_Vacancy_Mapping

    '                    intAppVacancyMapId = -1

    '                    objVacancyMap.isExist(CInt(dsRow.Item("applicantunkid")), CInt(dsRow.Item("vacancyunkid")), , intAppVacancyMapId)

    '                    If intAppVacancyMapId > 0 Then 'Update

    '                        StrQ = "UPDATE  rcapp_vacancy_mapping " & _
    '                                "SET     applicantunkid = " & CInt(dsRow.Item("applicantunkid")) & " " & _
    '                                      ", vacancyunkid = " & CInt(dsRow.Item("vacancyunkid")) & " " & _
    '                                      ", userunkid = " & User._Object._Userunkid & " " & _
    '                                      ", isactive = " & CInt(dsRow.Item("isactive")) & " " & _
    '                                "WHERE   appvacancytranunkid = " & intAppVacancyMapId & " "

    '                        objDataOp.ExecNonQuery(StrQ)

    '                        If objDataOp.ErrorMessage <> "" Then
    '                            If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                                System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                                GoTo CONTINUE_FOR
    '                            Else
    '                                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                                Throw exForce
    '                            End If
    '                        End If

    '                        If clsCommonATLog.Insert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_vacancy_mapping", "appvacancytranunkid", intAppVacancyMapId, 2, 2, , User._Object._Userunkid) = False Then
    '                            exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                            Throw exForce
    '                        End If

    '                        'objVacancyMap._Appvacancytranunkid = intAppVacancyMapId
    '                        'objVacancyMap._Isactive = CBool(dsRow.Item("isactive"))
    '                    Else 'Insert
    '                        'objVacancyMap._Isactive = True
    '                        StrQ = "INSERT INTO rcapp_vacancy_mapping(applicantunkid, vacancyunkid, userunkid, isactive)" & vbCrLf
    '                        StrQ &= "VALUES ( " & CInt(dsRow.Item("applicantunkid")) & ", " & CInt(dsRow.Item("vacancyunkid")) & ", " & mintUserunkid & ", 1 ) ; SELECT @@identity"

    '                        ds = objDataOp.ExecQuery(StrQ, "VM")

    '                        If objDataOp.ErrorMessage <> "" Then
    '                            If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                                System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                                GoTo CONTINUE_FOR
    '                            Else
    '                                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                                Throw exForce
    '                            End If
    '                        End If

    '                        intAppVacancyMapId = ds.Tables(0).Rows(0).Item(0)

    '                        If clsCommonATLog.Insert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_vacancy_mapping", "appvacancytranunkid", intAppVacancyMapId, 1, 1, , mintUserunkid) = False Then
    '                            exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                            Throw exForce
    '                        End If
    '                    End If
    '                    'objVacancyMap._Applicantunkid = CInt(dsRow.Item("applicantunkid"))
    '                    'objVacancyMap._Vacancyunkid = CInt(dsRow.Item("vacancyunkid"))
    '                    'objVacancyMap._Userunkid = User._Object._Userunkid

    '                    'If intAppVacancyMapId > 0 Then 'Update
    '                    '    If objVacancyMap.Update() = False Then
    '                    '        exForce = New Exception(objVacancyMap._Message)
    '                    '        Throw exForce
    '                    '    End If
    '                    'Else 'Insert
    '                    '    If objVacancyMap.Insert() = False Then
    '                    '        exForce = New Exception(objVacancyMap._Message)
    '                    '        Throw exForce
    '                    '    End If
    '                    'End If
    '                Next
    '                'Sohail (27 May 2013) -- End

    '                blnResult = True
    '                '**********[ VACANCY MAPPING ]********* END
    '                'Sohail (03 Mar 2012) -- End

    '                '**********[ IMAGES TRAN ]********* START
    '                'Sohail (23 Dec 2011) -- Start
    '                If intExistingApplicantUnkId > 0 Then

    '                    StrQ = "DELETE FROM hr_images_tran WHERE employeeunkid  = " & intExistingApplicantUnkId & " and referenceid = " & enImg_Email_RefId.Applicant_Module & " AND isapplicant = 1 "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                End If
    '                'Sohail (23 Dec 2011) -- End
    '                StrQ = "INSERT INTO hr_images_tran(employeeunkid,imagename,transactionid,referenceid,isapplicant) " & vbCrLf
    '                StrQ &= "SELECT " & mintNewApplicantUnkID & ",imagename," & mintNewApplicantUnkID & ",referenceid,isapplicant from " & mlinkweb & "hr_images_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                StrQ &= " AND employeeunkid = " & mintCurrApplicantUnkID & " "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    'Sohail (09 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    'Throw exForce
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                    'Sohail (09 Nov 2012) -- End
    '                End If
    '                'Sohail (08 May 2012) -- End
    '                '**********[ IMAGES TRAN ]********* END

    '                'Sohail (22 Apr 2015) -- Start
    '                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    '                '**********[ ATTACH FILE TRAN ]********* START
    '                If intExistingApplicantUnkId > 0 Then

    '                    StrQ = "DELETE FROM rcattachfiletran WHERE applicantunkid  = " & intExistingApplicantUnkId & " and modulerefid = " & enImg_Email_RefId.Applicant_Module & " AND attachrefid = " & enScanAttactRefId.QUALIFICATIONS & " "

    '                    objDataOp.ExecNonQuery(StrQ)

    '                    If objDataOp.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : in ATTACH FILE TRAN : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                End If

    '                StrQ = "INSERT INTO rcattachfiletran(applicantunkid, documentunkid, modulerefid, attachrefid, filepath, filename, fileuniquename, attached_date) " & vbCrLf
    '                StrQ &= "SELECT " & mintNewApplicantUnkID & ", documentunkid, modulerefid , attachrefid, filepath, filename, fileuniquename, attached_date FROM " & mlinkweb & "rcattachfiletran WHERE Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' " & vbCrLf
    '                StrQ &= " AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                End If

    '                If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcattachfiletran", "attachfiletranunkid", 1, 1, , , , mintUserunkid) = False Then
    '                    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
    '                    Throw exForce
    '                End If
    '                blnResult = True
    '                '**********[ ATTACH FILE TRAN ]********* END
    '                'Sohail (22 Apr 2015) -- End

    '                'Sohail (26 Aug 2011) -- Start
    '                'StrQ = "UPDATE   " & mlinkweb & "rcapplicant_master  set Syncdatetime= getdate()  where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and isnull(Syncdatetime,' ')=' ' "

    '                'objDataOp.ExecNonQuery(StrQ)

    '                'If objDataOp.ErrorMessage <> "" Then
    '                '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                '    Throw exForce
    '                'End If
    '                'Sohail (26 Aug 2011) -- End
    '                'Sohail (09 Feb 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                '*** Applicant Master
    '                StrQ = "UPDATE   " & mlinkweb & "rcapplicant_master  set Syncdatetime= getdate()  where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and isnull(Syncdatetime,' ')=' ' AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    'Sohail (09 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    'Throw exForce
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                    'Sohail (09 Nov 2012) -- End
    '                End If

    '                '*** Job History
    '                StrQ = "UPDATE " & mlinkweb & "rcjobhistory  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and   isnull(Syncdatetime,' ')=' ' AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    'Sohail (09 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    'Throw exForce
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                    'Sohail (09 Nov 2012) -- End
    '                End If

    '                '*** Applicant Skill Tran
    '                StrQ = "UPDATE " & mlinkweb & "rcapplicantskill_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND applicantunkid = " & mintCurrApplicantUnkID & "  "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    'Sohail (09 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    'Throw exForce
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                    'Sohail (09 Nov 2012) -- End
    '                End If

    '                '*** Applicant Qualification Tran
    '                StrQ = "UPDATE " & mlinkweb & "rcapplicantqualification_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    'Sohail (09 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    'Throw exForce
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                    'Sohail (09 Nov 2012) -- End
    '                End If

    '                '*** Applicant Reference Tran
    '                StrQ = "UPDATE " & mlinkweb & "rcapp_reference_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    'Sohail (09 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    'Throw exForce
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                    'Sohail (09 Nov 2012) -- End
    '                End If

    '                '*** Applicant Vacancy Mapping Tran
    '                StrQ = "UPDATE " & mlinkweb & "rcapp_vacancy_mapping  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND applicantunkid = " & mintCurrApplicantUnkID & " "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    'Sohail (09 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
    '                    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    'Throw exForce
    '                    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
    '                        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
    '                        GoTo CONTINUE_FOR
    '                    Else
    '                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                    'Sohail (09 Nov 2012) -- End
    '                End If


    'CONTINUE_FOR:   'Sohail (09 Nov 2012)
    '                If bw IsNot Nothing Then
    '                    intCount = intCount + 1
    '                    'bw.ReportProgress(100 * intCount / intTotApplicant)
    '                    bw.ReportProgress(intCount)

    '                    'Sohail (01 Nov 2012) -- Start
    '                    'TRA - ENHANCEMENT
    '                    If bw.CancellationPending = True Then
    '                        Exit For
    '                    End If
    '                    'Sohail (01 Nov 2012) -- End
    '                End If
    '                'Sohail (09 Feb 2012) -- End

    '            Next

    '            If mstrApplicantIDs.Trim <> "" Then 'Sohail (26 Aug 2011)

    '                'Sohail (12 Nov 2012) -- Start
    '                'TRA - ENHANCEMENT - To Skip the marked for deletion error and value of a column has been changed after the containing row was last fetched error
    '                objDataOp = New clsDataOperation
    '                'Sohail (12 Nov 2012) -- End

    '                'Sohail (09 Feb 2012) -- Start
    '                'TRA - ENHANCEMENT
    '                ''Sohail (26 Aug 2011) -- Start
    '                'StrQ = "UPDATE   " & mlinkweb & "rcapplicant_master  set Syncdatetime= getdate()  where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and isnull(Syncdatetime,' ')=' ' "

    '                'objDataOp.ExecNonQuery(StrQ)

    '                'If objDataOp.ErrorMessage <> "" Then
    '                '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                '    Throw exForce
    '                'End If
    '                ''Sohail (26 Aug 2011) -- End

    '                'StrQ = "UPDATE " & mlinkweb & "rcjobhistory  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and   isnull(Syncdatetime,' ')=' ' "

    '                'objDataOp.ExecNonQuery(StrQ)

    '                'If objDataOp.ErrorMessage <> "" Then
    '                '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                '    Throw exForce
    '                'End If

    '                'StrQ = "UPDATE " & mlinkweb & "rcapplicantskill_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' '  "

    '                'objDataOp.ExecNonQuery(StrQ)

    '                'If objDataOp.ErrorMessage <> "" Then
    '                '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                '    Throw exForce
    '                'End If

    '                'StrQ = "UPDATE " & mlinkweb & "rcapplicantqualification_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' "

    '                'objDataOp.ExecNonQuery(StrQ)

    '                'If objDataOp.ErrorMessage <> "" Then
    '                '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                '    Throw exForce
    '                'End If
    '                'Sohail (09 Feb 2012) -- End

    '                Dim imgServerPath As String = "http://" & Split(mdatabaseserverweb, "\")(0)
    '                Dim imgLocalPath As String = ConfigParameter._Object._PhotoPath & "\"
    '                Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\" 'Sohail (22 Apr 2015)
    '                Dim objWebRequest As System.Net.WebRequest
    '                Dim objWebResponse As System.Net.WebResponse
    '                Dim reader As IO.Stream
    '                Dim writer As IO.Stream
    '                Dim buffer(1023) As Byte
    '                Dim bytesRead As Integer
    '                Dim lngTotalBytes As Long
    '                Dim lngMaxBytes As Long
    '                Dim intCnt, i As Integer
    '                Dim dsImages As DataSet
    '                Dim imgWebpath As String = "/Aruti/UploadImage/"  '<webserver folder path for image: ftp://arutihr@arutihr.com/httpdocs/Arutihrms/UploadImage >

    '                'Sohail (22 Apr 2015) -- Start
    '                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    '                'StrQ = "SELECT imagename FROM " & mlinkweb & "hr_images_tran where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND  ISNULL(imagename,' ') <> ' ' "
    '                StrQ = "SELECT imagename, 0 AS modulerefid, 0 AS documentunkid, 0 AS attachrefid, employeeunkid, imagename AS filename, GETDATE() AS attached_date FROM " & mlinkweb & "hr_images_tran WHERE Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND  ISNULL(imagename,' ') <> ' ' " & _
    '                       "UNION ALL SELECT fileuniquename AS imagename, modulerefid, documentunkid, attachrefid, applicantunkid AS employeeunkid, filename, attached_date FROM rcattachfiletran WHERE applicantunkid IN (" & mstrApplicantIDs & ") AND  ISNULL(fileuniquename,' ') <> ' ' "
    '                '     '"UNION ALL SELECT fileuniquename AS imagename, modulerefid, documentunkid, attachrefid, applicantunkid AS employeeunkid, filename, attached_date FROM " & mlinkweb & "rcattachfiletran WHERE Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' AND  ISNULL(fileuniquename,' ') <> ' ' "

    '                'Sohail (22 Apr 2015) -- End
    '                dsImages = objDataOp.ExecQuery(StrQ, "hr_images_tran")
    '                intCnt = dsImages.Tables("hr_images_tran").Rows.Count
    '                i = 0

    '                'Sohail (22 Apr 2015) -- Start
    '                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    '                'If intCnt > 0 Then
    '                '    StrQ = "DELETE FROM rcattachfiletran WHERE applicantunkid IN (" & mstrApplicantIDs & ") AND  ISNULL(fileuniquename,' ') <> ' ' "
    '                '    objDataOp.ExecNonQuery(StrQ)

    '                '    If objDataOp.ErrorMessage <> "" Then
    '                '        Throw New Exception(objDataOp.ErrorMessage)
    '                '    End If
    '                'End If
    '                Dim dsDoc As DataSet = (New clsScan_Attach_Documents).GetDocType("Docs")
    '                Dim strFolderName As String = (From p In dsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.QUALIFICATIONS) Select (p.Item("Name").ToString)).FirstOrDefault
    '                If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
    '                'Sohail (22 Apr 2015) -- End

    '                For Each dsRow As DataRow In dsImages.Tables("hr_images_tran").Rows
    '                    i += 1
    '                    'Sohail (22 Apr 2015) -- Start
    '                    'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    '                    'objWebRequest = System.Net.WebRequest.Create(imgServerPath & imgWebpath & CStr(dsRow.Item("imagename")))
    '                    If CInt(dsRow.Item("attachrefid")) = enScanAttactRefId.QUALIFICATIONS Then 'Qualification Certificates
    '                        objWebRequest = System.Net.WebRequest.Create(imgServerPath & imgWebpath & "UploadQualiCerti/" & CStr(dsRow.Item("imagename")))
    '                    Else
    '                        objWebRequest = System.Net.WebRequest.Create(imgServerPath & imgWebpath & CStr(dsRow.Item("imagename")))
    '                    End If
    '                    'Sohail (22 Apr 2015) -- End
    '                    objWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials

    '                    If WebFileExist(objWebRequest) = True Then
    '                        objWebResponse = objWebRequest.GetResponse

    '                        lngMaxBytes = CLng(objWebResponse.ContentLength)
    '                        reader = objWebResponse.GetResponseStream
    '                        'Sohail (22 Apr 2015) -- Start
    '                        'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    '                        'writer = IO.File.Create(imgLocalPath & CStr(dsRow.Item("imagename")))
    '                        If CInt(dsRow.Item("attachrefid")) = enScanAttactRefId.QUALIFICATIONS Then 'Qualification Certificates
    '                            If IO.Directory.Exists(docLocalPath & strFolderName) = False Then
    '                                IO.Directory.CreateDirectory(docLocalPath & strFolderName)
    '                            End If
    '                            writer = IO.File.Create(docLocalPath & strFolderName & CStr(dsRow.Item("imagename")))
    '                        Else
    '                            writer = IO.File.Create(imgLocalPath & CStr(dsRow.Item("imagename")))
    '                        End If
    '                        'Sohail (22 Apr 2015) -- End
    '                        bytesRead = 0
    '                        lngTotalBytes = 0
    '                        While True
    '                            bytesRead = reader.Read(buffer, 0, buffer.Length)
    '                            If bytesRead <= 0 Then Exit While

    '                            writer.Write(buffer, 0, bytesRead)
    '                            lngTotalBytes += bytesRead
    '                        End While
    '                        reader.Close()
    '                        writer.Close()


    '                        'Sohail (22 Apr 2015) -- Start
    '                        'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    '                        Dim objScan As New clsScan_Attach_Documents
    '                        Dim dt As DataTable = objScan._Datatable
    '                        Dim rw As DataRow = dt.NewRow
    '                        rw.Item("scanattachtranunkid") = -1
    '                        rw.Item("documentunkid") = dsRow.Item("documentunkid")
    '                        rw.Item("employeeunkid") = dsRow.Item("employeeunkid")
    '                        rw.Item("filename") = dsRow.Item("filename")
    '                        rw.Item("scanattachrefid") = dsRow.Item("attachrefid")
    '                        rw.Item("modulerefid") = dsRow.Item("modulerefid")
    '                        rw.Item("userunkid") = User._Object._Userunkid
    '                        rw.Item("transactionunkid") = -1
    '                        rw.Item("attached_date") = dsRow.Item("attached_date")
    '                        rw.Item("destfilepath") = docLocalPath & strFolderName & CStr(dsRow.Item("filename"))
    '                        If objScan.IsExist(CInt(dsRow.Item("modulerefid")), CInt(dsRow.Item("attachrefid")), dsRow.Item("filename").ToString, CInt(dsRow.Item("employeeunkid"))) = False Then
    '                            rw.Item("orgfilepath") = docLocalPath & strFolderName & CStr(dsRow.Item("imagename"))
    '                            rw.Item("AUD") = "A"
    '                        Else
    '                            rw.Item("orgfilepath") = docLocalPath & strFolderName & CStr(dsRow.Item("filename"))
    '                            rw.Item("AUD") = "U"
    '                        End If

    '                        dt.Rows.Add(rw)

    '                        objScan._Datatable = dt
    '                        If objScan.InsertUpdateDelete_Documents() = False Then
    '                            Throw New Exception(objScan._Message)
    '                            Return False
    '                        End If

    '                        IO.File.Delete(docLocalPath & strFolderName & CStr(dsRow.Item("imagename")))
    '                        'Sohail (22 Apr 2015) -- End
    '                    End If
    '                Next

    '                StrQ = "UPDATE " & mlinkweb & "hr_images_tran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    Throw exForce
    '                End If

    '                'Sohail (22 Apr 2015) -- Start
    '                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
    '                StrQ = "UPDATE " & mlinkweb & "rcattachfiletran  set Syncdatetime= getdate()   where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & "  and isnull(Syncdatetime,' ')=' ' "

    '                objDataOp.ExecNonQuery(StrQ)

    '                If objDataOp.ErrorMessage <> "" Then
    '                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                    Throw exForce
    '                End If
    '                'Sohail (22 Apr 2015) -- End

    '                'objDataOp.ExecNonQuery("UPDATE " & Company._Object._ConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|0' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
    '                'If objDataOp.ErrorMessage <> "" Then
    '                '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                '    Throw exForce
    '                'End If

    '                ''*** Insert Audit Log 
    '                'Call InsertATLog(mstrApplicantIDs, False, StartTime)
    '            End If 'Sohail (26 Aug 2011)

    '            objDataOp.ExecNonQuery("UPDATE " & Company._Object._ConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|0' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
    '            If objDataOp.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                Throw exForce
    '            End If

    '            '*** Insert Audit Log 
    '            Call InsertATLog(mstrApplicantIDs, False, StartTime)

    '            'objDataOp.ReleaseTransaction(True)
    '            Cursor.Current = Cursors.Default
    '            Return True

    '        Catch ex As Exception
    '            'objDataOp.ReleaseTransaction(False)
    '            'Sohail (26 Aug 2011) -- Start
    '            objDataOp.ExecNonQuery("UPDATE " & Company._Object._ConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|0' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
    '            If objDataOp.ErrorMessage <> "" Then
    '                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
    '                Throw exForce
    '            End If
    '            '*** Insert Audit Log 
    '            Call InsertATLog(mstrApplicantIDs, True, StartTime)
    '            'Sohail (26 Aug 2011) -- End
    '            DisplayError.Show("-1", ex.Message, "ImportFromWeb", mstrModuleName)
    '            Return False
    '        Finally
    '        End Try
    '    End Function

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function ImportFromWeb(ByVal mlinkweb As String, ByVal StrCode As String, ByVal intClientCode As Integer, ByVal mdatabaseserverweb As String, ByVal bw As BackgroundWorker) As Boolean
    Private Function ImportFromWeb(ByVal mlinkweb As String, _
                                   ByVal StrCode As String, _
                                   ByVal intClientCode As Integer, _
                                   ByVal mdatabaseserverweb As String, _
                                   ByVal bw As BackgroundWorker, _
                                   ByVal intApplicantCodeNotype As Integer, _
                                   ByVal strApplicantCodePrifix As String, _
                                   ByVal strConfigDatabaseName As String, _
                                   ByVal blnIsImgInDataBase As Boolean, _
                                   ByVal strPhotoPath As String, _
                                   ByVal strDocument_Path As String, _
                                   ByVal intDatabaseServerSetting As Integer, _
                                   ByVal strDatabaseServer As String, _
                                   ByVal strAuthenticationCode As String, _
                                   ByVal intUserunkid As Integer, _
                                   ByVal strArutiSelfServiceURL As String, _
                                   ByVal dtCurrentDateAndTime As DateTime, _
                                   ByVal mintCompID As Integer, _
                                   ByVal strWebServiceLink As String _
                                   ) As Boolean
        'Sohail (16 Nov 2018) - [strWebServiceLink]
        'Shani(24-Aug-2015) -- End

        Dim StrQ As String = String.Empty
        Dim mLinkDesk As String = String.Empty
        Dim dsList As New DataSet
        Dim mintCurrApplicantUnkID, mintNewApplicantUnkID As Integer
        Dim blnResult As Boolean = False
        Dim exForce As Exception
        Dim ds As DataSet = Nothing

        Dim objDataOp As New clsDataOperation
        Dim StartTime As DateTime = Now
        Dim mstrApplicantIDs As String = ""
        'Sohail (05 Jun 2020) -- Start
        'NMB Enhancement # : Upon successful application of a vacancy for internal vacancies, system to copy the reporting-to on the reverse email sent back to the employee.
        Dim strEmpCodes As String = ""
        'Sohail (05 Jun 2020) -- End
        Dim blnGenerateLOG As Boolean = True

        Try

            'objDataOp.BindTransaction()
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
            mintCurrApplicantUnkID = -1 : mintNewApplicantUnkID = -1


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._IsImgInDataBase = False AndAlso ConfigParameter._Object._PhotoPath = "" Then
            If blnIsImgInDataBase = False AndAlso strPhotoPath = "" Then
                'Shani(24-Aug-2015) -- End
                mstrMessage = Language.getMessage(mstrModuleName, 18, "Please select Image path from configuration -> Option -> Photo Path.")
                Return False
            End If


            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._Document_Path = "" Then
            If strDocument_Path = "" Then
                'Shani(24-Aug-2015) -- End
                mstrMessage = Language.getMessage(mstrModuleName, 21, "Please select Document path from configuration -> Option -> Documents Path.")
                Return False
            End If

            'Sohail (23 Oct 2012) -- Start
            'TRA - ENHANCEMENT
            'Call GetApplicantImportApplicantStatus()

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If GetApplicantImportApplicantStatus() = False Then
            If GetApplicantImportApplicantStatus(strConfigDatabaseName, mintCompID) = False Then
                'Shani(24-Aug-2015) -- End
                Return False
            End If
            'Sohail (23 Oct 2012) -- End

            'Sohail (26 Aug 2011) -- Start
            Dim strMBoardNo As String = GetMotherBoardSrNo()
            Dim strMachine As String = My.Computer.Name
            Dim intTotalApplicant As Integer = 0

            '*** Compare current machine sr no with sr no in configuration database
            If mintImportStatus = 1 AndAlso strMBoardNo <> mstrMBoardSrNo Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry! This Process is currently running on ") & mstrMachineName & Language.getMessage(mstrModuleName, 19, " Machine."), enMsgBoxStyle.Critical)
                Return False
            End If

            '*** Set Current Machine SrNo & Name
            If mstrMBoardSrNo <> strMBoardNo Then mstrMBoardSrNo = strMBoardNo
            If mstrMachineName <> strMachine Then mstrMachineName = strMachine

            '*** Put current machine import status so others can't do this process same time.

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOp.ExecNonQuery("UPDATE " & Company._Object._ConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|1' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
            objDataOp.ExecNonQuery("UPDATE " & strConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|1' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
            'Shani(24-Aug-2015) -- End

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If

            Dim dir As String = System.IO.Directory.GetCurrentDirectory()

            Dim HttpBinding As New System.ServiceModel.BasicHttpBinding
            With HttpBinding
                .MaxBufferSize = 2147483647
                .MaxReceivedMessageSize = 2147483647
                .ReaderQuotas.MaxStringContentLength = 2147483647
                .ReaderQuotas.MaxArrayLength = 2147483647
                .ReaderQuotas.MaxBytesPerRead = 2147483647
                .ReaderQuotas.MaxNameTableCharCount = 2147483647
                .OpenTimeout = New TimeSpan(0, 20, 0)
                .ReceiveTimeout = New TimeSpan(0, 20, 0)
                .SendTimeout = New TimeSpan(0, 20, 0)
                '.Security.Mode = ServiceModel.BasicHttpSecurityMode.Transport 'Sohail (05 Dec 2016)
                .MessageEncoding = ServiceModel.WSMessageEncoding.Text 'Sohail (11 Dec 2015) - [CCBRT Error: The content type text/html of the response message does not match the content type of the binding]
                .Security.Transport.ClientCredentialType = ServiceModel.HttpClientCredentialType.None
            End With

            'Dim EndPointAdd As System.ServiceModel.EndpointAddress 'Sohail (16 Nov 2018)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'If ConfigParameter._Object._DatabaseServerSetting = enDatabaseServerSetting.DEFAULT_SETTING Then
            'If intDatabaseServerSetting = enDatabaseServerSetting.DEFAULT_SETTING Then 'Sohail (16 Nov 2018)
                'Shani(24-Aug-2015) -- End
                'Sohail (05 Dec 2016) -- Start
                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                'EndPointAdd = New System.ServiceModel.EndpointAddress("http://www.arutihr.com/Aruti/ArutiRecruitmentService.asmx")
            'EndPointAdd = New System.ServiceModel.EndpointAddress("http://www.arutihr.com/ArutiHRM/ArutiRecruitmentService.asmx") 'Sohail (16 Nov 2018)
                'Sohail (05 Dec 2016) -- End
            'Else 'Sohail (16 Nov 2018)
                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'EndPointAdd = New System.ServiceModel.EndpointAddress("http://" & ConfigParameter._Object._DatabaseServer.Replace("\apayroll", "/Aruti") & "/ArutiRecruitmentService.asmx")
                'Sohail (05 Dec 2016) -- Start
                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                'EndPointAdd = New System.ServiceModel.EndpointAddress("http://" & strDatabaseServer.Replace("\apayroll", "/ArutiHRM") & "/ArutiRecruitmentService.asmx")
                'EndPointAdd = New System.ServiceModel.EndpointAddress("http://" & strDatabaseServer.Replace("\apayroll", "/ArutiHRM") & "/ArutiRecruitmentService.asmx")
            'EndPointAdd = New System.ServiceModel.EndpointAddress("http://" & strDatabaseServer.Replace("\apayroll", "") & "/ArutiHRM/ArutiRecruitmentService.asmx") 'Sohail (16 Nov 2018)
                'Sohail (05 Dec 2016) -- End
                'Shani(24-Aug-2015) -- End
            'End If 'Sohail (16 Nov 2018)

            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim WebRef As New ArutiRecruitmentServiceSoapClient(HttpBinding, EndPointAdd)
            'Dim UserCred As New UserCredentials
            Dim WebRef As New WR.ArutiRecruitmentService
            Dim UserCred As New WR.UserCredentials
            WebRef.Url = strWebServiceLink
            'Sohail (16 Nov 2018) -- End
            WebRef.Timeout = 1200000 '20 minutes 'Sohail (21 Sep 2019)
            Dim strErrorMessage As String = ""

            System.Net.ServicePointManager.Expect100Continue = False 'Sohail (25 Oct 2016) - [Issue : The remote server returned an error: (417) Expectation Failed]
            'Sohail (16 Jul 2019) -- Start
            'NMB Issue # - 76.1 - Erroe : The underlying connection was closed: An unexpected error occurred on a send.
            'System.Net.ServicePointManager.SecurityProtocol = DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (25 Jul 2019) - (the requested security protocol is not supported)
            'Sohail (07 Aug 2019) -- Start
            'ZRA issue # - 76.1 - The requested security protocol is not supported.
            'System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
            Try
                System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
            Catch ex As Exception

            End Try
            'Sohail (07 Aug 2019) -- End
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications
            'Sohail (16 Jul 2019) -- End

            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'UserCred.Password = clsSecurity.Decrypt("CNjh9qOZUGhuqVpVp/LjZoYbJJoNgPiyC/P/vxPGfWo=", "ezee")
            UserCred.Password = "CNjh9qOZUGhuqVpVp/LjZoYbJJoNgPiyC/P/vxPGfWo="
            'Sohail (16 Nov 2018) -- End
            UserCred.WebClientID = intClientCode
            UserCred.CompCode = StrCode
            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'UserCred.AuthenticationCode = ConfigParameter._Object._AuthenticationCode
            UserCred.AuthenticationCode = strAuthenticationCode
            'Shani(24-Aug-2015) -- End

            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            WebRef.UserCredentialsValue = UserCred
            'Sohail (16 Nov 2018) -- End

            If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "******* Import process start at " & Now.ToString & " *******" & vbCrLf)
            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsApplicant As DataSet = WebRef.GetApplicants(UserCred, strErrorMessage, True, 0)
            'Sohail (04 Jan 2022) -- Start
            'Issue : : NMB - There is an error in XML Document (144, 49867) in XmlSerialization.
            'Dim dsApplicant As DataSet = WebRef.GetApplicants(strErrorMessage, True, 0)
            If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "Applicant downloading..." & vbCrLf)
            Dim dsApplicant As DataSet = GetWebServiceResponse(WebRef, UserCred, "GetApplicants", strErrorMessage, True, 0)
            'Sohail (04 Jan 2022) -- End
            'Sohail (16 Nov 2018) -- End

            'If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "Applicant downloading..." & vbCrLf)
            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If
            If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "Applicant downloaded." & vbCrLf)

            'Sohail (20 Jun 2023) -- Start
            'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
            Dim arrApplicantsIDs As String = ""
            If dsApplicant.Tables(0).Columns.Contains("noerror") = True Then
                arrApplicantsIDs = String.Join(",", (From p In dsApplicant.Tables(0) Select (p.Item("applicantunkid").ToString)).Distinct().ToArray)
            End If
            'Sohail (20 Jun 2023) -- End


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsJobHistory As DataSet = WebRef.GetJobHistory(UserCred, strErrorMessage, True, 0)
            'Sohail (20 Jun 2023) -- Start
            'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
            'Dim dsJobHistory As DataSet = WebRef.GetJobHistory(strErrorMessage, True, 0)
            Dim dsJobHistory As DataSet = GetWebServiceResponse(WebRef, UserCred, "GetJobHistory", strErrorMessage, True, 0, , arrApplicantsIDs)
            'Sohail (20 Jun 2023) -- End
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If
            If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "Job history downloaded." & vbCrLf)


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsSkill As DataSet = WebRef.GetSkill(UserCred, strErrorMessage, True, 0)
            'Sohail (20 Jun 2023) -- Start
            'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
            'Dim dsSkill As DataSet = WebRef.GetSkill(strErrorMessage, True, 0)
            Dim dsSkill As DataSet = GetWebServiceResponse(WebRef, UserCred, "GetSkill", strErrorMessage, True, 0, , arrApplicantsIDs)
            'Sohail (20 Jun 2023) -- End
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If
            If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "Skill downloaded." & vbCrLf)


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsQualification As DataSet = WebRef.GetQualification(UserCred, strErrorMessage, True, 0)
            'Sohail (20 Jun 2023) -- Start
            'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
            'Dim dsQualification As DataSet = WebRef.GetQualification(strErrorMessage, True, 0)
            Dim dsQualification As DataSet = GetWebServiceResponse(WebRef, UserCred, "GetQualification", strErrorMessage, True, 0, , arrApplicantsIDs)
            'Sohail (20 Jun 2023) -- End
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If
            If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "Qualification downloaded." & vbCrLf)


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsReference As DataSet = WebRef.GetReference(UserCred, strErrorMessage, True, 0)
            'Sohail (20 Jun 2023) -- Start
            'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
            'Dim dsReference As DataSet = WebRef.GetReference(strErrorMessage, True, 0)
            Dim dsReference As DataSet = GetWebServiceResponse(WebRef, UserCred, "GetReference", strErrorMessage, True, 0, , arrApplicantsIDs)
            'Sohail (20 Jun 2023) -- End
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If
            If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "Reference downloaded." & vbCrLf)


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsVacancy As DataSet = WebRef.GetVacancy(UserCred, strErrorMessage, True, 0)
            'Sohail (20 Jun 2023) -- Start
            'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
            'Dim dsVacancy As DataSet = WebRef.GetVacancy(strErrorMessage, True, 0)
            Dim dsVacancy As DataSet = GetWebServiceResponse(WebRef, UserCred, "GetVacancy", strErrorMessage, True, 0, , arrApplicantsIDs)
            'Sohail (20 Jun 2023) -- End
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsImages As DataSet = WebRef.GetImages(UserCred, strErrorMessage, True, 0)
            'Sohail (20 Jun 2023) -- Start
            'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
            'Dim dsImages As DataSet = WebRef.GetImages(strErrorMessage, True, 0)
            Dim dsImages As DataSet = GetWebServiceResponse(WebRef, UserCred, "GetImages", strErrorMessage, True, 0, , arrApplicantsIDs)
            'Sohail (20 Jun 2023) -- End
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If


            strErrorMessage = ""
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim dsAttachments As DataSet = WebRef.GetAttachments(UserCred, strErrorMessage, True, 0)
            'Sohail (20 Jun 2023) -- Start
            'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
            'Dim dsAttachments As DataSet = WebRef.GetAttachments(strErrorMessage, True, 0)
            Dim dsAttachments As DataSet = GetWebServiceResponse(WebRef, UserCred, "GetAttachments", strErrorMessage, True, 0, , arrApplicantsIDs)
            'Sohail (20 Jun 2023) -- End
            'Sohail (16 Nov 2018) -- End

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If

            'If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, dsAttachments.GetXml().ToString & vbCrLf & vbCrLf) 'Sohail (20 Jun 2023) 'Sohail (20 Jun 2023)
            'StrQ = "SELECT applicantunkid, applicant_code,titleunkid,firstname,surname,othername,gender,email,present_address1,present_address2,present_countryunkid,present_stateunkid,present_province,present_post_townunkid,present_zipcode,present_road,present_estate,present_plotno,present_mobileno,present_alternateno,present_tel_no,present_fax,perm_address1,perm_address2,perm_countryunkid,perm_stateunkid,perm_province,perm_post_townunkid,perm_zipcode,perm_road,perm_estate,perm_plotno,perm_mobileno,perm_alternateno,perm_tel_no,perm_fax,birth_date,marital_statusunkid,anniversary_date,language1unkid,language2unkid,language3unkid,language4unkid,nationality,userunkid,vacancyunkid,isimport,other_skill,other_qualification "
            'StrQ &= ", employeecode, referenceno "
            'StrQ &= " , ISNULL(memberships, '') AS memberships, ISNULL(achievements, '') AS achievements, ISNULL(journalsresearchpapers, '') AS journalsresearchpapers "
            'StrQ &= " FROM  " & mlinkweb & "rcapplicant_master where Comp_Code= '" & StrCode & "'" & " AND companyunkid = " & intClientCode & " and isnull(Syncdatetime,' ')=' ' AND ISNULL(referenceno,'') <> ''  " & vbCrLf

            'dsList = objDataOp.ExecQuery(StrQ, "rcapplicant_master")

            'If objDataOp.ErrorMessage <> "" Then
            '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
            '    Throw exForce
            'End If

            'Sohail (05 Jun 2020) -- Start
            'NMB Enhancement # : Upon successful application of a vacancy for internal vacancies, system to copy the reporting-to on the reverse email sent back to the employee.
            Dim objConfig As New clsConfigOptions
            Dim strValue As String = objConfig.GetKeyValue(mintCompID, "JOBCONFIRMATIONTEMPLATEID", objDataOp)
            Dim intID As Integer
            Dim strBody As String = ""
            Integer.TryParse(strValue, intID)
            Dim intTemplateFound As Boolean = False
            Dim objVac As New clsVacancy
            Dim objEmp As New clsEmployee_Master
            Dim dsIntVac As DataSet = objVac.getComboList(dtCurrentDateAndTime, False, "List", , 0, False, False, True, False, " AND (isexternalvacancy = 0 OR ISNULL(isbothintext, 0) = 1) ")
            dsIntVac.Tables(0).Columns.Add("empcodes", System.Type.GetType("System.String")).DefaultValue = ""
            Dim dtDistVac As DataTable = New DataView(dsVacancy.Tables(0)).ToTable(True, "vacancyunkid")
            dtDistVac.Columns.Add("IsInternal", System.Type.GetType("System.Boolean")).DefaultValue = False
            For Each dtRow As DataRow In dtDistVac.Rows
                If dsIntVac.Tables(0).Select(" id = " & CInt(dtRow.Item("vacancyunkid")) & " ").Length > 0 Then
                    dtRow.Item("IsInternal") = True
                Else
                    dtRow.Item("IsInternal") = False
                End If
            Next
            dtDistVac.AcceptChanges()

            Dim dsField As DataSet = Nothing
            Dim objLF As New clsLetterFields
            Dim strLetterContent As String = ""
            If intID > 0 Then
                Dim objLetterType As New clsLetterType
                objLetterType._LettertypeUnkId = intID

                strLetterContent = objLetterType._Lettercontent
                strLetterContent = strLetterContent.Replace("#CompanyCode#", mstrCompanyCode)
                strLetterContent = strLetterContent.Replace("#CompanyName#", mstrCompanyName)

                dsField = objLF.GetList(objLetterType._Fieldtypeid)

                intTemplateFound = True
            End If
            'Sohail (05 Jun 2020) -- End

            GintTotalApplicantToImport = dsApplicant.Tables(0).Rows.Count
            bw.ReportProgress(0)
            Dim intCount As Integer = 0
            If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "Webservice done." & vbCrLf)
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim imgServerPath As String = "http://" & Split(mdatabaseserverweb, "\")(0)
            Dim imgServerPath As String = strWebServiceLink.ToUpper.Replace("ARUTIRECRUITMENTSERVICE.ASMX", "")
            'Sohail (16 Nov 2018) -- End

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim imgLocalPath As String = ConfigParameter._Object._PhotoPath & "\"
            'Dim docLocalPath As String = ConfigParameter._Object._Document_Path & "\" 'Sohail (22 Apr 2015)
            Dim imgLocalPath As String = strPhotoPath & "\"
            Dim docLocalPath As String = strDocument_Path & "\"
            'Shani(24-Aug-2015) -- End

            Dim objWebRequest As System.Net.WebRequest
            Dim objWebResponse As System.Net.WebResponse
            Dim reader As IO.Stream
            Dim writer As IO.Stream
            Dim buffer(1023) As Byte
            Dim bytesRead As Integer
            Dim lngTotalBytes As Long
            Dim lngMaxBytes As Long
            'Dim intCnt, i As Integer
            'Dim dsImagess As DataSet
            'Sohail (05 Dec 2016) -- Start
            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
            'Dim imgWebpath As String = "/Aruti/UploadImage/"  '<webserver folder path for image: ftp://arutihr@arutihr.com/httpdocs/Arutihrms/UploadImage >
            'Sohail (16 Nov 2018) -- Start
            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
            'Dim imgWebpath As String = "/ArutiHRM/UploadImage/"  '<webserver folder path for image: ftp://arutihr@arutihr.com/httpdocs/Arutihrms/UploadImage >
            Dim imgWebpath As String = "/UploadImage/"  '<webserver folder path for image: ftp://arutihr@arutihr.com/httpdocs/Arutihrms/UploadImage >
            'Sohail (16 Nov 2018) -- End
            'Sohail (05 Dec 2016) -- End


            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            'Dim dsDoc As DataSet = (New clsScan_Attach_Documents).GetDocType("Docs")
            'Dim strFolderName As String = (From p In dsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = enScanAttactRefId.QUALIFICATIONS) Select (p.Item("Name").ToString)).FirstOrDefault
            'Sohail (04 Jul 2019) -- Start
            'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (Setting on aruti configuration to set Curriculam Vitae and Cover Letter attachment mandatory).
            'Dim strFolderName As String = (New clsScan_Attach_Documents).GetDocFolderName("Docs", enScanAttactRefId.QUALIFICATIONS).Tables(0).Rows(0)("Name").ToString
            Dim dsFolderName As DataSet = (New clsScan_Attach_Documents).GetDocFolderName("Docs", 0)
            Dim dicFolderName As Dictionary(Of Integer, String) = (From p In dsFolderName.Tables(0) Select New With {.Id = CInt(p.Item("Id")), .Name = p.Item("Name").ToString}).ToDictionary(Function(x) x.Id, Function(y) y.Name)
            Dim strFolderName As String = ""
            'Sohail (04 Jul 2019) -- End
            'SHANI (16 JUL 2015) -- End

            'SHANI (16 JUL 2015) -- Start
            'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            'Upload Image folder to access those attachemts from Server and 
            'all client machines if ArutiSelfService is installed otherwise save then on Document path
            'If strFolderName IsNot Nothing Then strFolderName = strFolderName & "\"
            If strFolderName IsNot Nothing Then strFolderName = strFolderName
            'SHANI (16 JUL 2015) -- End
            'Sohail (05 Dec 2016) -- Start
            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
            Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
            'Sohail (05 Dec 2016) -- End
            If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "IIS Installed : " & blnIsIISInstalled.ToString & vbCrLf)
            For Each drRow As DataRow In dsApplicant.Tables(0).Rows

                objDataOp = New clsDataOperation

                mintCurrApplicantUnkID = CInt(drRow.Item("applicantunkid").ToString)

                _Email = drRow.Item("email").ToString
                _Firstname = drRow.Item("firstname").ToString
                _Surname = drRow.Item("surname").ToString
                _Othername = drRow.Item("othername").ToString
                _Titleunkid = IIf(CInt(drRow.Item("titleunkid")) < 0, 0, CInt(drRow.Item("titleunkid")))
                _Gender = IIf(CInt(drRow.Item("gender")) < 0, 0, CInt(drRow.Item("gender")))
                _Vacancyunkid = CInt(drRow.Item("vacancyunkid"))

                '*******************| PERSONAL INFO |******************' START
                _Present_Address1 = drRow.Item("present_address1").ToString
                _Present_Address2 = drRow.Item("present_address2").ToString
                _Present_Alternateno = drRow.Item("present_alternateno").ToString
                _Present_Countryunkid = IIf(CInt(drRow.Item("present_countryunkid")) < 0, 0, CInt(drRow.Item("present_countryunkid")))
                _Present_Estate = drRow.Item("present_estate").ToString
                _Present_Fax = drRow.Item("present_fax").ToString
                _Present_Mobileno = drRow.Item("present_mobileno").ToString
                _Present_Plotno = drRow.Item("present_plotno").ToString
                _Present_Post_Townunkid = IIf(CInt(drRow.Item("present_post_townunkid")) < 0, 0, CInt(drRow.Item("present_post_townunkid")))
                _Present_Province = drRow.Item("present_province").ToString
                _Present_Road = drRow.Item("present_road").ToString
                _Present_Stateunkid = IIf(CInt(drRow.Item("present_stateunkid")) < 0, 0, CInt(drRow.Item("present_stateunkid")))
                _Present_Tel_No = drRow.Item("present_tel_no").ToString
                _Present_ZipCode = IIf(CInt(drRow.Item("present_zipcode")) < 0, 0, CInt(drRow.Item("present_zipcode")))
                _Perm_Address1 = drRow.Item("perm_address1").ToString
                _Perm_Address2 = drRow.Item("perm_address2").ToString
                _Perm_Alternateno = drRow.Item("perm_alternateno").ToString
                _Perm_Countryunkid = IIf(CInt(drRow.Item("perm_countryunkid")) < 0, 0, CInt(drRow.Item("perm_countryunkid")))
                _Perm_Estate = drRow.Item("perm_estate").ToString
                _Perm_Fax = drRow.Item("perm_fax").ToString
                _Perm_Mobileno = drRow.Item("perm_mobileno").ToString
                _Perm_Plotno = drRow.Item("perm_plotno").ToString
                _Perm_Post_Townunkid = IIf(CInt(drRow.Item("perm_post_townunkid")) < 0, 0, CInt(drRow.Item("perm_post_townunkid")))
                _Perm_Province = drRow.Item("perm_province").ToString
                _Perm_Road = drRow.Item("perm_road").ToString
                _Perm_Stateunkid = IIf(CInt(drRow.Item("perm_stateunkid")) < 0, 0, CInt(drRow.Item("perm_stateunkid")))
                _Perm_Tel_No = drRow.Item("perm_tel_no").ToString
                _Perm_ZipCode = IIf(CInt(drRow.Item("perm_zipcode")) < 0, 0, CInt(drRow.Item("perm_zipcode")))

                'S.SANDEEP |04-MAY-2023| -- START
                'ISSUE/ENHANCEMENT : A1X-833
                _NidaNumber = drRow.Item("nin").ToString
                _NidaMobileNum = drRow.Item("ninmobile").ToString
                _TIN = drRow.Item("tin").ToString
                _Village = drRow.Item("village").ToString
                _PhoneNumber1 = drRow.Item("phonenum").ToString
                _Residency = CInt(drRow.Item("residency"))
                _IndexNo = drRow.Item("indexno").ToString()
                If IsDBNull(drRow("applicant_photo")) = False Then
                    _ApplicantPhoto = DirectCast(drRow("applicant_photo"), Byte())
                End If
                If IsDBNull(drRow("applicant_signature")) = False Then
                    _ApplicantSigtr = DirectCast(drRow("applicant_signature"), Byte())
                End If                
                'S.SANDEEP |04-MAY-2023| -- END

                '*******************| PERSONAL INFO |******************' END

                '*******************| ADDITIONAL INFO |******************' START
                If drRow.Item("anniversary_date").ToString.Trim.Length > 0 Then
                    _Anniversary_Date = CDate(drRow.Item("anniversary_date"))
                Else
                    _Anniversary_Date = Nothing
                End If

                If drRow.Item("birth_date").ToString.Trim.Length > 0 Then
                    _Birth_Date = CDate(drRow.Item("birth_date"))
                Else
                    _Birth_Date = Nothing
                End If

                _Marital_Statusunkid = IIf(CInt(drRow.Item("marital_statusunkid")) < 0, 0, CInt(drRow.Item("marital_statusunkid")))
                _Nationality = IIf(CInt(drRow.Item("nationality")) < 0, 0, CInt(drRow.Item("nationality")))
                _Language1unkid = IIf(CInt(drRow.Item("language1unkid")) < 0, 0, CInt(drRow.Item("language1unkid")))
                _Language2unkid = IIf(CInt(drRow.Item("language2unkid")) < 0, 0, CInt(drRow.Item("language2unkid")))
                _Language3unkid = IIf(CInt(drRow.Item("language3unkid")) < 0, 0, CInt(drRow.Item("language3unkid")))
                _Language4unkid = IIf(CInt(drRow.Item("language4unkid")) < 0, 0, CInt(drRow.Item("language4unkid")))

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '_Userunkid = User._Object._Userunkid 'CInt(drRow.Item("userunkid"))
                _Userunkid = intUserunkid
                'Shani(24-Aug-2015) -- End

                _OtherQualifications = drRow.Item("other_qualification").ToString
                _OtherSkills = drRow.Item("other_skill").ToString
                _Employeecode = drRow.Item("employeecode").ToString
                _Referenceno = drRow.Item("referenceno").ToString
                _Memberships = drRow.Item("memberships").ToString
                _Achievements = drRow.Item("achievements").ToString
                _JournalsResearchPapers = drRow.Item("journalsresearchpapers").ToString
                'Sohail (05 Dec 2016) -- Start
                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                _UserId = drRow.Item("UserId").ToString
                'Sohail (05 Dec 2016) -- End

                'Nilay (13 Apr 2017) -- Start
                _MotherTongue = drRow.Item("mother_tongue").ToString
                _IsImpaired = CBool(drRow.Item("isimpaired"))
                _Impairment = drRow.Item("impairment").ToString
                _CurrentSalary = CDec(drRow.Item("current_salary").ToString)
                _ExpectedSalary = CDec(drRow.Item("expected_salary"))
                _ExpectedBenefits = drRow.Item("expected_benefits").ToString
                _WillingToRelocate = CBool(drRow.Item("willing_to_relocate"))
                _WillingToTravel = CBool(drRow.Item("willing_to_travel"))
                _NoticePeriodDays = CInt(drRow.Item("notice_period_days"))
                'Nilay (13 Apr 2017) -- End

                'Hemant (09 July 2018) -- Start
                'Enhancement : RefNo: 261 - Currency on Current Salary and Expected Sal  - Change  Caption to -- >> Current Gross Salary (Indicate Currency)  --- >> Expected Gross Salary (Indicate Currency). Make the user input field to be alphanumeric (We will put Currency dropdown for both attributes)
                _CurrentSalaryCurrencyId = CInt(drRow.Item("current_salary_currency_id"))
                _ExpectedSalaryCurrencyId = CInt(drRow.Item("expected_salary_currency_id"))
                'Hemant (09 July 2018) -- End

                'Sohail (09 Oct 2018) -- Start
                'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                _IsFromOnline = True
                'Sohail (09 Oct 2018) -- End
                'Sohail (25 Sep 2020) -- Start
                'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                _LoginEmployeeUnkid = 0
                '_Employeeunkid = 0
                _Isvoid = 0
                _Voiduserunkid = 0
                _Voiddatetime = Nothing
                _Voidreason = ""
                'Sohail (25 Sep 2020) -- End



                '*******************| ADDITIONAL INFO |******************' END

                Dim intExistingApplicantUnkId As Integer = 0
                Dim strApplicant_Code As String = ""
                Dim strFirstname As String = ""
                Dim strSurname As String = ""

                'Sohail (05 Dec 2016) -- Start
                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                'isExistForWebImport(mstrReferenceno, intExistingApplicantUnkId, strApplicant_Code, strFirstname, strSurname)
                isExistForWebImport(mstrEmail, intExistingApplicantUnkId, strApplicant_Code, strFirstname, strSurname)
                'Sohail (05 Dec 2016) -- End
                If intExistingApplicantUnkId = 0 Then
                    mstrApplicant_Code = ""

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'blnResult = Insert()
                    blnResult = Insert(intApplicantCodeNotype, strApplicantCodePrifix)
                    'Shani(24-Aug-2015) -- End

                Else
                    mintApplicantunkid = intExistingApplicantUnkId
                    mstrApplicant_Code = strApplicant_Code
                    'Sohail (05 Dec 2016) -- Start
                    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                    'mstrFirstname = strFirstname
                    'mstrSurname = strSurname
                    'Sohail (05 Dec 2016) -- End
                    blnResult = Update()
                End If

                If blnResult = False And _Message <> "" Then
                    exForce = New Exception(_Message)
                    Throw exForce

                ElseIf blnResult = False Then
                    Exit For
                End If

                If intExistingApplicantUnkId = 0 Then
                mintNewApplicantUnkID = _Applicantunkid
                Else
                    mintNewApplicantUnkID = intExistingApplicantUnkId
                End If


                If mstrApplicantIDs.Trim = "" Then
                    mstrApplicantIDs = mintNewApplicantUnkID.ToString
                Else
                    mstrApplicantIDs &= ", " & mintNewApplicantUnkID.ToString
                End If
                If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "Applicantunkid : " & mintNewApplicantUnkID.ToString & vbCrLf)
                '**********[ JOBHISTORY ]********* START
                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'If intExistingApplicantUnkId > 0 Then
                '    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcjobhistory", "jobhistorytranunkid", 3, 3) = False Then
                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                '        Throw exForce
                '    End If

                '    StrQ = "DELETE FROM rcjobhistory WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                '    objDataOp.ExecNonQuery(StrQ)

                '    If objDataOp.ErrorMessage <> "" Then
                '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                '        Throw exForce
                '    End If
                'End If
                'Sohail (26 May 2017) -- End

                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'Dim dr_Job() As DataRow = dsJobHistory.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                Dim dr_Job() As DataRow
                If intExistingApplicantUnkId > 0 Then
                    If isExistServerNullJobHistoryUnkId(intExistingApplicantUnkId) = True Then
                        'Sohail (16 Nov 2018) -- Start
                        'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                        'Dim dsTmp As DataSet = WebRef.GetJobHistory(UserCred, strErrorMessage, False, mintCurrApplicantUnkID)
                        Dim dsTmp As DataSet = WebRef.GetJobHistory(strErrorMessage, False, mintCurrApplicantUnkID, "")
                        'Sohail (16 Nov 2018) -- End
                        dr_Job = dsTmp.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")

                    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcjobhistory", "jobhistorytranunkid", 3, 3) = False Then
                        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                        Throw exForce
                    End If

                    StrQ = "DELETE FROM rcjobhistory WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                    objDataOp.ExecNonQuery(StrQ)

                    If objDataOp.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        Throw exForce
                    End If
                    Else
                        dr_Job = dsJobHistory.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    End If
                Else
                    dr_Job = dsJobHistory.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                End If
                'Sohail (26 May 2017) -- End

                If dr_Job.Length > 0 Then

                    For Each dRow As DataRow In dr_Job

                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                        ''StrQ = "INSERT INTO rcjobhistory(applicantunkid,employername,companyname,designation,responsibility,joiningdate,terminationdate,officephone,leavingreason, achievements) " & vbCrLf
                        ''        StrQ &= "VALUES (" & mintNewApplicantUnkID & " , '" & dRow.Item("employername").ToString.Replace("'", "''") & "', '" & dRow.Item("companyname").ToString.Replace("'", "''") & "', '" & dRow.Item("designation").ToString.Replace("'", "''") & "', '" & dRow.Item("responsibility").ToString.Replace("'", "''") & "', '" & CDate(dRow.Item("joiningdate")).ToString("yyyy-MM-dd hh:mm:ss") & "', '" & CDate(dRow.Item("terminationdate")).ToString("yyyy-MM-dd hh:mm:ss") & "', '" & dRow.Item("officephone").ToString.Replace("'", "''") & "', '" & dRow.Item("leavingreason").ToString.Replace("'", "''") & "', '" & dRow.Item("achievements").ToString.Replace("'", "''") & "' ) " & vbCrLf
                'StrQ = "INSERT INTO rcjobhistory(applicantunkid,employername,companyname,designation,responsibility,joiningdate,terminationdate,officephone,leavingreason, achievements) " & vbCrLf
                        'StrQ &= "VALUES (" & mintNewApplicantUnkID & " , '" & dRow.Item("employername").ToString.Replace("'", "''") & "', '" & dRow.Item("companyname").ToString.Replace("'", "''") & "', '" & dRow.Item("designation").ToString.Replace("'", "''") & "', '" & dRow.Item("responsibility").ToString.Replace("'", "''") & "', " & If(IsDBNull(dRow.Item("joiningdate")) = True, "NULL", " '" & CDate(dRow.Item("joiningdate")).ToString("yyyy-MM-dd hh:mm:ss") & "'") & ",  " & If(IsDBNull(dRow.Item("terminationdate")) = True, "NULL", " '" & CDate(dRow.Item("terminationdate")).ToString("yyyy-MM-dd hh:mm:ss") & "'") & ", '" & dRow.Item("officephone").ToString.Replace("'", "''") & "', '" & dRow.Item("leavingreason").ToString.Replace("'", "''") & "', '" & dRow.Item("achievements").ToString.Replace("'", "''") & "' ) " & vbCrLf

                        'objDataOp.ExecNonQuery(StrQ)

                        'If objDataOp.ErrorMessage <> "" Then
                        '    'Sohail (09 Nov 2012) -- Start
                        '    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                        '    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        '    'Throw exForce
                        '    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                        '        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
                        '        GoTo CONTINUE_FOR
                        '    Else
                        '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        '        Throw exForce
                        '    End If
                        'End If

                        'If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcjobhistory", "jobhistorytranunkid", 1, 1, , , , mintUserunkid) = False Then
                        '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                        '    Throw exForce
                        'End If
                        dRow.Item("AUD") = "A"
                        If intExistingApplicantUnkId > 0 Then
                            Dim intUnkId As Integer = isExistServerJobHistoryUnkId(intExistingApplicantUnkId, CInt(dRow.Item("serverjobhistorytranunkid")), False)
                            If intUnkId > 0 Then
                                dRow.Item("jobhistorytranunkid") = intUnkId
                                dRow.Item("applicantunkid") = intExistingApplicantUnkId
                                dRow.Item("AUD") = "U"
                    End If
                End If
                        dRow.AcceptChanges()
                        'Sohail (26 May 2017) -- End
                    Next

                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    Dim objJobHistory As New clsJobhistory
                    'Sohail (24 Jan 2018) -- Start
                    'Issue: TANAPA#1932 - Qualification and other child table data inserted with applicantunkid 0 for new applicants in 70.1.
                    'objJobHistory._Applicantunkid = intExistingApplicantUnkId
                    objJobHistory._Applicantunkid = mintNewApplicantUnkID
                    'Sohail (24 Jan 2018) -- End
                    objJobHistory._DataList = dr_Job.CopyToDataTable
                    If objJobHistory.InsertUpdateDelete_JobHistoryTran() = False Then
                        exForce = New Exception("error in applicant job history")
                    Throw exForce
                End If
                    'Sohail (26 May 2017) -- End
                End If
                '**********[ JOBHISTORY ]********* END

                '**********[ SKILL TRAN ]********* START
                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'If intExistingApplicantUnkId > 0 Then
                '    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapplicantskill_tran", "skilltranunkid", 3, 3) = False Then
                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                '        Throw exForce
                '    End If

                '    StrQ = "DELETE FROM rcapplicantskill_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                '    objDataOp.ExecNonQuery(StrQ)

                '    If objDataOp.ErrorMessage <> "" Then
                '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                '        Throw exForce
                '    End If
                'End If
                'Sohail (26 May 2017) -- End

                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'Dim dr_Skill() As DataRow = dsSkill.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                Dim dr_Skill() As DataRow
                If intExistingApplicantUnkId > 0 Then
                    If isExistServerNullSkillUnkId(intExistingApplicantUnkId) = True Then
                        'Sohail (16 Nov 2018) -- Start
                        'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                        'Dim dsTmp As DataSet = WebRef.GetSkill(UserCred, strErrorMessage, False, mintCurrApplicantUnkID)
                        Dim dsTmp As DataSet = WebRef.GetSkill(strErrorMessage, False, mintCurrApplicantUnkID, "")
                        'Sohail (16 Nov 2018) -- End
                        dr_Skill = dsTmp.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")

                    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapplicantskill_tran", "skilltranunkid", 3, 3) = False Then
                        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                        Throw exForce
                    End If

                    StrQ = "DELETE FROM rcapplicantskill_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                    objDataOp.ExecNonQuery(StrQ)

                    If objDataOp.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        Throw exForce
                    End If
                    Else
                        dr_Skill = dsSkill.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    End If
                Else
                    dr_Skill = dsSkill.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                End If
                'Sohail (26 May 2017) -- End

                If dr_Skill.Length > 0 Then
                    For Each dRow As DataRow In dr_Skill

                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                        ''Nilay (13 Apr 2017) -- Start
                        ''Enhancements: Settings for mandatory options in online recruitment
                        ''StrQ = "INSERT INTO rcapplicantskill_tran(applicantunkid,skillcategoryunkid,skillunkid,remark, other_skillcategory, other_skill )" & vbCrLf
                        ''StrQ &= "VALUES (" & mintNewApplicantUnkID & ", " & dRow.Item("skillcategoryunkid") & ", " & dRow.Item("skillunkid") & ", '" & dRow.Item("remark").ToString.Replace("'", "''") & "', '" & dRow.Item("other_skillcategory").ToString.Replace("'", "''") & "', '" & dRow.Item("other_skill").ToString.Replace("'", "''") & "' )" & vbCrLf
                        'StrQ = "INSERT INTO rcapplicantskill_tran(applicantunkid,skillcategoryunkid,skillunkid,remark, other_skillcategory, other_skill, skillexpertiseunkid)" & vbCrLf
                        'StrQ &= "VALUES (" & mintNewApplicantUnkID & ", " & dRow.Item("skillcategoryunkid") & ", " & dRow.Item("skillunkid") & ", '" & dRow.Item("remark").ToString.Replace("'", "''") & "', '" & dRow.Item("other_skillcategory").ToString.Replace("'", "''") & "', '" & dRow.Item("other_skill").ToString.Replace("'", "''") & "', " & dRow.Item("skillexpertiseunkid") & " )" & vbCrLf
                        ''Nilay (13 Apr 2017) -- End

                        'objDataOp.ExecNonQuery(StrQ)

                        'If objDataOp.ErrorMessage <> "" Then
                        '    'Sohail (09 Nov 2012) -- Start
                        '    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                        '    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        '    'Throw exForce
                        '    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                        '        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
                        '        GoTo CONTINUE_FOR
                        '    Else
                        '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        '        Throw exForce
                        '    End If
                        'End If
                        dRow.Item("AUD") = "A"
                        If intExistingApplicantUnkId > 0 Then
                            Dim intUnkId As Integer = isExistServerSkillUnkId(intExistingApplicantUnkId, CInt(dRow.Item("serverskilltranunkid")), False)
                            If intUnkId > 0 Then
                                dRow.Item("skilltranunkid") = intUnkId
                                dRow.Item("applicantunkid") = intExistingApplicantUnkId
                                dRow.Item("AUD") = "U"
                    End If
                        End If
                        dRow.AcceptChanges()
                        'Sohail (26 May 2017) -- End
                    Next
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    Dim objSkill As New clsApplicantSkill_tran
                    'Sohail (24 Jan 2018) -- Start
                    'Issue: TANAPA#1932 - Qualification and other child table data inserted with applicantunkid 0 for new applicants in 70.1.
                    'objSkill._ApplicantUnkid = intExistingApplicantUnkId
                    objSkill._ApplicantUnkid = mintNewApplicantUnkID
                    'Sohail (24 Jan 2018) -- End
                    objSkill._DataList = dr_Skill.CopyToDataTable
                    If objSkill.InsertUpdateDelete_SkillTran() = False Then
                        exForce = New Exception("error in applicant skill")
                        Throw exForce
                    End If
                    'Sohail (26 May 2017) -- End
                End If

                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapplicantskill_tran", "skilltranunkid", 1, 1, , , , mintUserunkid) = False Then
                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                '    Throw exForce
                'End If
                'Sohail (26 May 2017) -- End
                '**********[ SKILL TRAN ]********* END

                '**********[ QUAKIFICATION TRAN ]********* START
                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'If intExistingApplicantUnkId > 0 Then
                '    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapplicantqualification_tran", "qualificationtranunkid", 3, 3) = False Then
                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                '        Throw exForce
                '    End If

                '    StrQ = "DELETE FROM rcapplicantqualification_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                '    objDataOp.ExecNonQuery(StrQ)

                '    If objDataOp.ErrorMessage <> "" Then
                '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                '        Throw exForce
                '    End If
                'End If
                'Sohail (26 May 2017) -- End

                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'Dim dr_Quali() As DataRow = dsQualification.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                Dim dr_Quali() As DataRow
                If intExistingApplicantUnkId > 0 Then
                    If isExistServerNullQualificationUnkId(intExistingApplicantUnkId) = True Then
                        'Sohail (16 Nov 2018) -- Start
                        'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                        'Dim dsTmp As DataSet = WebRef.GetQualification(UserCred, strErrorMessage, False, mintCurrApplicantUnkID)
                        Dim dsTmp As DataSet = WebRef.GetQualification(strErrorMessage, False, mintCurrApplicantUnkID, "")
                        'Sohail (16 Nov 2018) -- End
                        dr_Quali = dsTmp.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")

                    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapplicantqualification_tran", "qualificationtranunkid", 3, 3) = False Then
                        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                        Throw exForce
                    End If

                    StrQ = "DELETE FROM rcapplicantqualification_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                    objDataOp.ExecNonQuery(StrQ)

                    If objDataOp.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        Throw exForce
                    End If
                    Else
                        dr_Quali = dsQualification.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    End If
                Else
                    dr_Quali = dsQualification.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                End If
                'Sohail (26 May 2017) -- End

                If dr_Quali.Length > 0 Then
                    For Each dRow As DataRow In dr_Quali

                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                        ''StrQ = "INSERT INTO rcapplicantqualification_tran(applicantunkid,qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark,resultunkid,gpacode, other_qualificationgrp, other_qualification, other_institute, other_resultcode, certificateno )" & vbCrLf
                        ''        StrQ &= "VALUES (" & mintNewApplicantUnkID & ", " & dRow.Item("qualificationgroupunkid") & ", " & dRow.Item("qualificationunkid") & ", '" & CDate(dRow.Item("transaction_date")).ToString("yyyy-MM-dd hh:mm:ss") & "', '" & dRow.Item("reference_no").ToString.Replace("'", "''") & "', '" & CDate(dRow.Item("award_start_date")).ToString("yyyy-MM-dd hh:mm:ss") & "', '" & CDate(dRow.Item("award_end_date")).ToString("yyyy-MM-dd hh:mm:ss") & "', " & dRow.Item("instituteunkid") & ", '" & dRow.Item("remark").ToString.Replace("'", "''") & "', " & dRow.Item("resultunkid") & ", " & dRow.Item("gpacode") & ", '" & dRow.Item("other_qualificationgrp").ToString.Replace("'", "''") & "', '" & dRow.Item("other_qualification").ToString.Replace("'", "''") & "', '" & dRow.Item("other_institute").ToString.Replace("'", "''") & "', '" & dRow.Item("other_resultcode").ToString.Replace("'", "''") & "', '" & dRow.Item("certificateno").ToString.Replace("'", "''") & "' ) " & vbCrLf

                        ''Nilay (13 Apr 2017) -- Start
                        ''StrQ = "INSERT INTO rcapplicantqualification_tran(applicantunkid,qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark,resultunkid,gpacode, other_qualificationgrp, other_qualification, other_institute, other_resultcode, certificateno )" & vbCrLf
                        ''StrQ &= "VALUES (" & mintNewApplicantUnkID & ", " & dRow.Item("qualificationgroupunkid") & ", " & dRow.Item("qualificationunkid") & ", " & If(IsDBNull(dRow.Item("transaction_date")) = True, "NULL", " '" & CDate(dRow.Item("transaction_date")).ToString("yyyy-MM-dd hh:mm:ss") & "'") & ", '" & dRow.Item("reference_no").ToString.Replace("'", "''") & "', " & If(IsDBNull(dRow.Item("award_start_date")) = True, "NULL", " '" & CDate(dRow.Item("award_start_date")).ToString("yyyy-MM-dd hh:mm:ss") & "'") & ", " & If(IsDBNull(dRow.Item("award_end_date")) = True, "NULL", " '" & CDate(dRow.Item("award_end_date")).ToString("yyyy-MM-dd hh:mm:ss") & "'") & ", " & dRow.Item("instituteunkid") & ", '" & dRow.Item("remark").ToString.Replace("'", "''") & "', " & dRow.Item("resultunkid") & ", " & dRow.Item("gpacode") & ", '" & dRow.Item("other_qualificationgrp").ToString.Replace("'", "''") & "', '" & dRow.Item("other_qualification").ToString.Replace("'", "''") & "', '" & dRow.Item("other_institute").ToString.Replace("'", "''") & "', '" & dRow.Item("other_resultcode").ToString.Replace("'", "''") & "', '" & dRow.Item("certificateno").ToString.Replace("'", "''") & "' ) " & vbCrLf
                        'StrQ = "INSERT INTO rcapplicantqualification_tran(applicantunkid,qualificationgroupunkid,qualificationunkid,transaction_date,reference_no,award_start_date,award_end_date,instituteunkid,remark,resultunkid,gpacode, other_qualificationgrp, other_qualification, other_institute, other_resultcode, certificateno, ishighestqualification)" & vbCrLf
                        'StrQ &= "VALUES (" & mintNewApplicantUnkID & ", " & dRow.Item("qualificationgroupunkid") & ", " & dRow.Item("qualificationunkid") & ", " & If(IsDBNull(dRow.Item("transaction_date")) = True, "NULL", " '" & CDate(dRow.Item("transaction_date")).ToString("yyyy-MM-dd hh:mm:ss") & "'") & ", '" & dRow.Item("reference_no").ToString.Replace("'", "''") & "', " & If(IsDBNull(dRow.Item("award_start_date")) = True, "NULL", " '" & CDate(dRow.Item("award_start_date")).ToString("yyyy-MM-dd hh:mm:ss") & "'") & ", " & If(IsDBNull(dRow.Item("award_end_date")) = True, "NULL", " '" & CDate(dRow.Item("award_end_date")).ToString("yyyy-MM-dd hh:mm:ss") & "'") & ", " & dRow.Item("instituteunkid") & ", '" & dRow.Item("remark").ToString.Replace("'", "''") & "', " & dRow.Item("resultunkid") & ", " & dRow.Item("gpacode") & ", '" & dRow.Item("other_qualificationgrp").ToString.Replace("'", "''") & "', '" & dRow.Item("other_qualification").ToString.Replace("'", "''") & "', '" & dRow.Item("other_institute").ToString.Replace("'", "''") & "', '" & dRow.Item("other_resultcode").ToString.Replace("'", "''") & "', '" & dRow.Item("certificateno").ToString.Replace("'", "''") & "', " & CInt(Int(dRow.Item("ishighestqualification"))) & " ) " & vbCrLf
                        ''Nilay (13 Apr 2017) -- End

                        'objDataOp.ExecNonQuery(StrQ)

                        'If objDataOp.ErrorMessage <> "" Then
                        '    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                        '    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        '    'Throw exForce
                        '    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                        '        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
                        '        GoTo CONTINUE_FOR
                        '    Else
                        '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        '        Throw exForce
                        '    End If
                        'End If
                        dRow.Item("AUD") = "A"
                        If intExistingApplicantUnkId > 0 Then
                            Dim intUnkId As Integer = isExistServerQualificationUnkId(intExistingApplicantUnkId, CInt(dRow.Item("serverqualificationtranunkid")), False)
                            If intUnkId > 0 Then
                                dRow.Item("qualificationtranunkid") = intUnkId
                                dRow.Item("applicantunkid") = intExistingApplicantUnkId
                                dRow.Item("AUD") = "U"
                    End If
                        End If
                        dRow.AcceptChanges()
                        'Sohail (26 May 2017) -- End
                    Next
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    Dim objQuali As New clsApplicantQualification_tran
                    'Sohail (24 Jan 2018) -- Start
                    'Issue: TANAPA#1932 - Qualification and other child table data inserted with applicantunkid 0 for new applicants in 70.1.
                    'objQuali._ApplicantUnkid = intExistingApplicantUnkId
                    objQuali._ApplicantUnkid = mintNewApplicantUnkID
                    'Sohail (24 Jan 2018) -- End
                    objQuali._DataList = dr_Quali.CopyToDataTable
                    If objQuali.InsertUpdateDelete_QualificationTran() = False Then
                        exForce = New Exception("error in applicant qualification")
                        Throw exForce
                    End If
                    'Sohail (26 May 2017) -- End
                End If

                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapplicantqualification_tran", "qualificationtranunkid", 1, 1, , , , mintUserunkid) = False Then
                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                '    Throw exForce
                'End If
                'Sohail (26 May 2017) -- End

                blnResult = True
                '**********[ QUAKIFICATION TRAN ]********* END

                '**********[ REFERENCE TRAN ]********* START
                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'If intExistingApplicantUnkId > 0 Then
                '    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapp_reference_tran", "referencetranunkid", 3, 3) = False Then
                '        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                '        Throw exForce
                '    End If

                '    StrQ = "DELETE FROM rcapp_reference_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                '    objDataOp.ExecNonQuery(StrQ)

                '    If objDataOp.ErrorMessage <> "" Then
                '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                '        Throw exForce
                '    End If
                'End If
                'Sohail (26 May 2017) -- End

                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'Dim dr_Ref() As DataRow = dsReference.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                Dim dr_Ref() As DataRow
                If intExistingApplicantUnkId > 0 Then
                    If isExistServerNullReferenceUnkId(intExistingApplicantUnkId) = True Then
                        'Sohail (16 Nov 2018) -- Start
                        'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                        'Dim dsTmp As DataSet = WebRef.GetReference(UserCred, strErrorMessage, False, mintCurrApplicantUnkID)
                        Dim dsTmp As DataSet = WebRef.GetReference(strErrorMessage, False, mintCurrApplicantUnkID, "")
                        'Sohail (16 Nov 2018) -- End
                        dr_Ref = dsTmp.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")

                    If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapp_reference_tran", "referencetranunkid", 3, 3) = False Then
                        exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                        Throw exForce
                    End If

                    StrQ = "DELETE FROM rcapp_reference_tran WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                    objDataOp.ExecNonQuery(StrQ)

                    If objDataOp.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        Throw exForce
                    End If
                    Else
                        dr_Ref = dsReference.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    End If
                Else
                    dr_Ref = dsReference.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                End If
                'Sohail (26 May 2017) -- End

                If dr_Ref.Length > 0 Then
                    For Each dRow As DataRow In dr_Ref

                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                        'StrQ = "INSERT INTO rcapp_reference_tran(applicantunkid, name, address, countryunkid, stateunkid, cityunkid, email, gender, position, telephone_no, mobile_no, relationunkid)" & vbCrLf
                        'StrQ &= "VALUES (" & mintNewApplicantUnkID & ", '" & dRow.Item("name").ToString.Replace("'", "''") & "', '" & dRow.Item("address").ToString.Replace("'", "''") & "', " & dRow.Item("countryunkid") & ", " & dRow.Item("stateunkid") & ", " & dRow.Item("cityunkid") & ", '" & dRow.Item("email").ToString.Replace("'", "''") & "', '" & dRow.Item("gender").ToString.Replace("'", "''") & "', '" & dRow.Item("position").ToString.Replace("'", "''") & "', '" & dRow.Item("telephone_no").ToString.Replace("'", "''") & "', '" & dRow.Item("mobile_no").ToString.Replace("'", "''") & "', " & dRow.Item("relationunkid") & " ) " & vbCrLf

                        'objDataOp.ExecNonQuery(StrQ)

                        'If objDataOp.ErrorMessage <> "" Then
                        '    'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                        '    'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        '    'Throw exForce
                        '    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                        '        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
                        '        GoTo CONTINUE_FOR
                        '    Else
                        '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        '        Throw exForce
                        '    End If
                        'End If
                        dRow.Item("AUD") = "A"
                        If intExistingApplicantUnkId > 0 Then
                            Dim intUnkId As Integer = isExistServerReferenceUnkId(intExistingApplicantUnkId, CInt(dRow.Item("serverreferencetranunkid")), False)
                            If intUnkId > 0 Then
                                dRow.Item("referencetranunkid") = intUnkId
                                dRow.Item("applicantunkid") = intExistingApplicantUnkId
                                dRow.Item("AUD") = "U"
                    End If
                        End If
                        dRow.AcceptChanges()
                        'Sohail (26 May 2017) -- End
                    Next
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    Dim objRef As New clsrcapp_reference_Tran
                    'Sohail (24 Jan 2018) -- Start
                    'Issue: TANAPA#1932 - Qualification and other child table data inserted with applicantunkid 0 for new applicants in 70.1.
                    'objRef._Applicantunkid = intExistingApplicantUnkId
                    objRef._Applicantunkid = mintNewApplicantUnkID
                    'Sohail (24 Jan 2018) -- End
                    objRef._dtReferences = dr_Ref.CopyToDataTable
                    If objRef.InsertUpdateDelete_ReferencesTran() = False Then
                        exForce = New Exception("error in applicant reference")
                        Throw exForce
                    End If
                    'Sohail (26 May 2017) -- End
                End If

                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_reference_tran", "referencetranunkid", 1, 1, , , , mintUserunkid) = False Then
                '    exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                '    Throw exForce
                'End If
                'Sohail (26 May 2017) -- End

                blnResult = True
                '**********[ REFERENCE TRAN ]********* END

                '**********[ VACANCY MAPPING ]********* START
                'Sohail (09 Oct 2018) -- Start
                'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
                'Dim objVacancyMap As clsApplicant_Vacancy_Mapping
                'Dim intAppVacancyMapId As Integer

                'Dim dr_Vacancy() As DataRow = dsVacancy.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " AND vacancyunkid > 0 ")

                'If dr_Vacancy.Length > 0 Then
                '    For Each dRow As DataRow In dr_Vacancy
                '        objVacancyMap = New clsApplicant_Vacancy_Mapping

                '        intAppVacancyMapId = -1

                '        objVacancyMap.isExist(mintNewApplicantUnkID, CInt(dRow.Item("vacancyunkid")), , intAppVacancyMapId)

                '        If intAppVacancyMapId > 0 Then 'Update

                '            StrQ = "UPDATE  rcapp_vacancy_mapping " & _
                '                        "SET     applicantunkid = " & mintNewApplicantUnkID & " " & _
                '                                  ", vacancyunkid = " & CInt(dRow.Item("vacancyunkid")) & " " & _
                '                              ", userunkid = " & intUserunkid & " " & _
                '                                  ", isactive = " & CInt(dRow.Item("isactive")) & " " & _
                '                    "WHERE   appvacancytranunkid = " & intAppVacancyMapId & " "

                '            'Shani(24-Aug-2015) -- [ User._Object._Userunkid == > intUserunkid ]

                '            objDataOp.ExecNonQuery(StrQ)

                '            If objDataOp.ErrorMessage <> "" Then
                '                If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                '                    System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
                '                    GoTo CONTINUE_FOR
                '                Else
                '                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                '                    Throw exForce
                '                End If
                '            End If


                '            'Shani(24-Aug-2015) -- Start
                '            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                '            'If clsCommonATLog.Insert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_vacancy_mapping", "appvacancytranunkid", intAppVacancyMapId, 2, 2, , User._Object._Userunkid) = False Then
                '            If clsCommonATLog.Insert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_vacancy_mapping", "appvacancytranunkid", intAppVacancyMapId, 2, 2, , intUserunkid) = False Then
                '                'Shani(24-Aug-2015) -- End

                '                exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                '                Throw exForce
                '            End If

                '        Else 'Insert

                '            'Sohail (27 Apr 2017) -- Start
                '            'TANAPA Issue - 66.1 - applicants were not coming in shortlisting due to isimport = 1.
                '            'StrQ = "INSERT INTO rcapp_vacancy_mapping(applicantunkid, vacancyunkid, userunkid, isactive, importuserunkid, importdatetime, isimport)" & vbCrLf
                '            'StrQ &= "VALUES ( " & mintNewApplicantUnkID & ", " & CInt(dRow.Item("vacancyunkid")) & ", " & mintUserunkid & ", 1, " & mintUserunkid & ", GETDATE(), 1 ) ; SELECT @@identity"
                '            'Nilay (13 Apr 2017) -- Start
                '            'StrQ = "INSERT INTO rcapp_vacancy_mapping(applicantunkid, vacancyunkid, userunkid, isactive, importuserunkid, importdatetime, isimport)" & vbCrLf
                '            'StrQ &= "VALUES ( " & mintNewApplicantUnkID & ", " & CInt(dRow.Item("vacancyunkid")) & ", " & mintUserunkid & ", " & CInt(dRow.Item("isactive")) & ", 0, NULL, 0 ) ; SELECT @@identity"
                '            StrQ = "INSERT INTO rcapp_vacancy_mapping(applicantunkid, vacancyunkid, userunkid, isactive, importuserunkid, importdatetime, isimport, earliest_possible_startdate, comments, vacancy_found_out_from)" & vbCrLf
                '            StrQ &= "VALUES ( " & mintNewApplicantUnkID & ", " & CInt(dRow.Item("vacancyunkid")) & ", " & mintUserunkid & ", " & CInt(dRow.Item("isactive")) & ", 0, NULL, 0, " & If(IsDBNull(dRow.Item("earliest_possible_startdate")) = True, "NULL", " '" & CDate(dRow.Item("earliest_possible_startdate")).ToString("yyyy-MM-dd hh:mm:ss") & "'") & ", '" & dRow.Item("comments").ToString.Replace("'", "''") & "', '" & dRow.Item("vacancy_found_out_from").ToString.Replace("'", "''") & "' ) ; SELECT @@identity"
                '            'Nilay (13 Apr 2017) -- End
                '            'Sohail (27 Apr 2017) -- End
                '            'Sohail (05 Dec 2016) - [importuserunkid, importdatetime, isimport]

                '            ds = objDataOp.ExecQuery(StrQ, "VM")

                '            If objDataOp.ErrorMessage <> "" Then
                '                If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                '                    System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
                '                    GoTo CONTINUE_FOR
                '                Else
                '                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                '                    Throw exForce
                '                End If
                '            End If

                '            intAppVacancyMapId = ds.Tables(0).Rows(0).Item(0)

                '            If clsCommonATLog.Insert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcapp_vacancy_mapping", "appvacancytranunkid", intAppVacancyMapId, 1, 1, , mintUserunkid) = False Then
                '                exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                '                Throw exForce
                '            End If
                '        End If
                '    Next
                'End If
                Dim dr_AppVac() As DataRow
                If intExistingApplicantUnkId > 0 Then
                    If isExistServerNullAppVacancyUnkId(intExistingApplicantUnkId) = True Then
                        'Sohail (16 Nov 2018) -- Start
                        'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                        'Dim dsTmp As DataSet = WebRef.GetVacancy(UserCred, strErrorMessage, False, mintCurrApplicantUnkID)
                        Dim dsTmp As DataSet = WebRef.GetVacancy(strErrorMessage, False, mintCurrApplicantUnkID, "")
                        'Sohail (16 Nov 2018) -- End
                        dr_AppVac = dsTmp.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " AND vacancyunkid > 0 ")

                        If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcapp_vacancy_mapping", "appvacancytranunkid", 3, 3) = False Then
                            exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        StrQ = "DELETE FROM rcapp_vacancy_mapping WHERE applicantunkid  = " & intExistingApplicantUnkId & " "

                        objDataOp.ExecNonQuery(StrQ)

                If objDataOp.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        Throw exForce
                    End If
                    Else
                        dr_AppVac = dsVacancy.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " AND vacancyunkid > 0 ")
                    End If
                Else
                    dr_AppVac = dsVacancy.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " AND vacancyunkid > 0 ")
                End If

                If dr_AppVac.Length > 0 Then
                    For Each dRow As DataRow In dr_AppVac

                        dRow.Item("AUD") = "A"
                        If intExistingApplicantUnkId > 0 Then
                            Dim intUnkId As Integer = isExistServerAppVacancyUnkId(intExistingApplicantUnkId, CInt(dRow.Item("serverappvacancytranunkid")), False)
                            If intUnkId > 0 Then
                                dRow.Item("appvacancytranunkid") = intUnkId
                                dRow.Item("applicantunkid") = intExistingApplicantUnkId
                                dRow.Item("AUD") = "U"
                            End If
                        End If
                        dRow.AcceptChanges()
                    Next
                    Dim objVacancyMap As New clsApplicant_Vacancy_Mapping
                    objVacancyMap._Applicantunkid = mintNewApplicantUnkID
                    objVacancyMap._DataList = dr_AppVac.CopyToDataTable
                    If objVacancyMap.InsertUpdateDelete_AppVacancyMapping() = False Then
                        exForce = New Exception("error in applicant vacancy mapping")
                        Throw exForce
                End If

                    'Sohail (25 Sep 2020) -- Start
                    'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
                    '*** from now do not allow to apply for internal vacancy fromrecruitment portal as from now it will be allowed from self service
                    ''Sohail (05 Jun 2020) -- Start
                    ''NMB Enhancement # : Upon successful application of a vacancy for internal vacancies, system to copy the reporting-to on the reverse email sent back to the employee.
                    'If drRow.Item("employeecode").ToString.Trim <> "" Then
                    '    Dim rw() As DataRow = dr_AppVac.CopyToDataTable.Select("AUD = 'A' ")
                    '    For Each r As DataRow In rw
                    '        If dtDistVac.Select(" IsInternal = true AND vacancyunkid = " & CInt(r.Item("vacancyunkid")) & " ").Length > 0 Then
                    '            Dim strVacTitle As String = ""
                    '            Dim rr() As DataRow = dsIntVac.Tables(0).Select(" id = " & CInt(r.Item("vacancyunkid")) & " ")
                    '            If rr.Length > 0 Then
                    '                strVacTitle = rr(0).Item("name").ToString
                    '            End If
                    '            'For Each w In rr
                    '            '    w.Item("empcodes") = "," & drRow.Item("employeecode").ToString
                    '            'Next
                    '            'Dim strECode() As String = re.Item("empcodes").ToString.Substring(1).Split(",")

                    '            'For Each ecode As String In strECode
                    '            Dim intEID As Integer = objEmp.GetEmployeeUnkidFromEmpCode(drRow.Item("employeecode").ToString)
                    '            If intEID > 0 Then
                    '                Dim objApplied As New clsApplicant_Vacancy_Mapping
                    '                Dim dsAppVac As DataSet = Nothing
                    '                If strLetterContent.Trim.Length > 0 Then
                    '                    dsAppVac = objApplied.GetApplicantAppliedJob(intApplicantUnkId:=mintNewApplicantUnkID _
                    '                                                 , intAppvacancytranunkid:=0 _
                    '                                                 , intVacancyUnkId:=CInt(r.Item("vacancyunkid")) _
                    '                                                 )

                    '                    If dsAppVac.Tables(0).Rows.Count > 0 Then
                    '                        Dim strDataName As String = ""
                    '                        Dim StrCol As String = ""

                    '                        Dim strLC As String = strLetterContent

                    '                        For Each dtCol As DataColumn In dsField.Tables(0).Columns
                    '                            StrCol = dtCol.ColumnName

                    '                            If strLC.Contains("#" & StrCol & "#") Then

                    '                                If StrCol.Trim.ToUpper = "COMPANYLOGO" Then
                    '                                    strDataName = strLC.Replace("#" & StrCol & "#", "#imgcmp#")
                    '                                Else
                    '                                    If dsAppVac.Tables(0).Columns.Contains(StrCol) = True Then
                    '                                        If StrCol = "Pay_Range_From" Then
                    '                                            strDataName = strLC.Replace("#" & StrCol & "#", Math.Round(CDec(dsAppVac.Tables(0).Rows(0)(StrCol)), 2).ToString)
                    '                                        ElseIf StrCol = "Pay_Range_To" Then
                    '                                            strDataName = strLC.Replace("#" & StrCol & "#", Math.Round(CDec(dsAppVac.Tables(0).Rows(0)(StrCol)), 2).ToString)
                    '                                        ElseIf StrCol = "VacancyOpeningDate" Then
                    '                                            strDataName = strLC.Replace("#" & StrCol & "#", eZeeDate.convertDate(dsAppVac.Tables(0).Rows(0)(StrCol).ToString).ToString("dd-MMM-yyyy"))
                    '                                        ElseIf StrCol = "VacancyClosingDate" Then
                    '                                            strDataName = strLC.Replace("#" & StrCol & "#", eZeeDate.convertDate(dsAppVac.Tables(0).Rows(0)(StrCol).ToString).ToString("dd-MMM-yyyy"))
                    '                                        Else
                    '                                            strDataName = strLC.Replace("#" & StrCol & "#", dsAppVac.Tables(0).Rows(0)(StrCol).ToString)
                    '                                        End If
                    '                                    Else
                    '                                        strDataName = strLC.Replace("#" & StrCol & "#", "")
                    '                                    End If
                    '                                End If

                    '                                strLC = strDataName
                    '                            End If
                    '                        Next

                    '                        Dim htmlOutput = "Template.html"
                    '                        Dim contentUriPrefix = IO.Path.GetFileNameWithoutExtension(htmlOutput)
                    '                        Dim htmlResult = RtfToHtmlConverter.RtfToHtml(strLC, contentUriPrefix)

                    '                        strBody = htmlResult._HTML

                    '                        'If Session("isattachapplicantcv") Then
                    '                        '    mstrFilePath = GetApplicantCV()
                    '                        'End If

                    '                    End If
                    '                End If

                    '                If intTemplateFound = False Then
                    '                    strBody = "#imgcmp# <p style='font-size:14px'>Thank you for expressing interest in <b>" + strVacTitle + "</b> role </p>"
                    '                    strBody += "<p style='font-size:14px'>This email is to confirm that we have received your application. You will be contacted once it has been reviewed.</p>"

                    '                    strBody += "<p style='font-size:14px'>Please do not reply to this email as it is auto generated.</p>"
                    '                    Dim sValue As String = objConfig.GetKeyValue(mintCompID, "SHOWARUTISIGNATURE")
                    '                    Dim blnValue As Integer
                    '                    Boolean.TryParse(sValue, blnValue)
                    '                    If blnValue = True Then
                    '                        strBody += "<p  style='font-size:12px'><b>Online Job Application Powered by Aruti HR & Payroll Management Software</b></p> "
                    '                    End If
                    '                    strBody += " #imgaruti#"
                    '                End If


                    '                Dim dsRepoTo As DataSet = objEmp.GetEmployeeReporting(enEmployeeReporting.Employee_Reporting, intEID, dtCurrentDateAndTime, dtCurrentDateAndTime, mstrDatabaseName)
                    '                For Each rprow As DataRow In dsRepoTo.Tables(0).Rows
                    '                    If rprow.Item("email").ToString.Trim = "" Then Continue For

                    '                    Dim objMail As New clsSendMail

                    '                    objMail._Subject = Language.getMessage(mstrModuleName, 22, "Your application is successfully sent to") & " " & mstrCompanyName

                    '                    objMail._Message = strBody.ToString
                    '                    objMail._ToEmail = rprow.Item("email")

                    '                    If mintLoginTypeId <= 0 Then mintLoginTypeId = enLogin_Mode.DESKTOP
                    '                    If mstrWebFormName.Trim.Length > 0 Then
                    '                        objMail._Form_Name = mstrWebFormName
                    '                    End If
                    '                    objMail._LogEmployeeUnkid = mintLogEmployeeUnkid
                    '                    objMail._OperationModeId = mintLoginTypeId
                    '                    objMail._UserUnkid = intUserunkid
                    '                    Dim objUsr As New clsUserAddEdit
                    '                    objUsr._Userunkid = intUserunkid
                    '                    'objMail._SenderAddress = IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)
                    '                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT

                    '                    gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrWebFormName, mintLogEmployeeUnkid, "", "", intUserunkid, mintLoginTypeId, clsSendMail.enAT_VIEW_TYPE.RECRUITMENT_MGT, ""))

                    '                Next
                    '            End If
                    '            'Next

                    '        End If
                    '    Next
                    '    dsIntVac.Tables(0).AcceptChanges()
                    'End If
                    ''Sohail (05 Jun 2020) -- End
                    'Sohail (25 Sep 2020) -- End
                End If
                'Sohail (09 Oct 2018) -- End

                blnResult = True
                '**********[ VACANCY MAPPING ]********* END
                'Sohail (03 Mar 2012) -- End

                '**********[ IMAGES TRAN ]********* START
                If intExistingApplicantUnkId > 0 Then

                    StrQ = "DELETE FROM hr_images_tran WHERE employeeunkid  = " & intExistingApplicantUnkId & " and referenceid = " & enImg_Email_RefId.Applicant_Module & " AND isapplicant = 1 "

                    objDataOp.ExecNonQuery(StrQ)

                    If objDataOp.ErrorMessage <> "" Then
                        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        Throw exForce
                    End If
                End If

                Dim dr_Images() As DataRow = dsImages.Tables(0).Select("employeeunkid = " & mintCurrApplicantUnkID & " ")

                If dr_Images.Length > 0 Then
                    For Each dRow As DataRow In dr_Images

                        StrQ = "INSERT INTO hr_images_tran(employeeunkid,imagename,transactionid,referenceid,isapplicant) " & vbCrLf
                        StrQ &= "VALUES (" & mintNewApplicantUnkID & ", '" & dRow.Item("imagename").ToString.Replace("'", "''") & "'," & mintNewApplicantUnkID & ", " & dRow.Item("referenceid") & ", " & dRow.Item("isapplicant") & " ) " & vbCrLf

                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            'TRA - ENHANCEMENT - To Skip Applicant if It is being update on online recruitment web site.
                            'exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                            'Throw exForce
                            If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                                'System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
                                GoTo CONTINUE_FOR
                            Else
                                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                                Throw exForce
                            End If

                        End If


                        '*** [Download Images / Attachements] ***
                        objWebRequest = System.Net.WebRequest.Create(imgServerPath & imgWebpath & CStr(dRow.Item("imagename")))
                        objWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials

                        If WebFileExist(objWebRequest) = True Then
                            objWebResponse = objWebRequest.GetResponse

                            lngMaxBytes = CLng(objWebResponse.ContentLength)
                            reader = objWebResponse.GetResponseStream

                            writer = IO.File.Create(imgLocalPath & CStr(dRow.Item("imagename")))

                            bytesRead = 0
                            lngTotalBytes = 0
                            While True
                                bytesRead = reader.Read(buffer, 0, buffer.Length)
                                If bytesRead <= 0 Then Exit While

                                writer.Write(buffer, 0, bytesRead)
                                lngTotalBytes += bytesRead
                            End While
                            reader.Close()
                            writer.Close()


                            'Dim objScan As New clsScan_Attach_Documents
                            'Dim dt As DataTable = objScan._Datatable
                            'Dim rw As DataRow = dt.NewRow
                            'rw.Item("scanattachtranunkid") = -1
                            'rw.Item("documentunkid") = dRow.Item("documentunkid")
                            'rw.Item("employeeunkid") = mintNewApplicantUnkID
                            'rw.Item("filename") = dRow.Item("filename")
                            'rw.Item("scanattachrefid") = dRow.Item("attachrefid")
                            'rw.Item("modulerefid") = dRow.Item("modulerefid")
                            'rw.Item("userunkid") = User._Object._Userunkid
                            'rw.Item("transactionunkid") = -1
                            'rw.Item("attached_date") = dRow.Item("attached_date")
                            'rw.Item("destfilepath") = docLocalPath & strFolderName & CStr(dRow.Item("filename"))
                            'If objScan.IsExist(CInt(dRow.Item("modulerefid")), CInt(dRow.Item("attachrefid")), dRow.Item("filename").ToString, CInt(dRow.Item("employeeunkid"))) = False Then
                            '    rw.Item("orgfilepath") = docLocalPath & strFolderName & CStr(dRow.Item("imagename"))
                            '    rw.Item("AUD") = "A"
                            'Else
                            '    rw.Item("orgfilepath") = docLocalPath & strFolderName & CStr(dRow.Item("filename"))
                            '    rw.Item("AUD") = "U"
                            'End If

                            'dt.Rows.Add(rw)

                            'objScan._Datatable = dt
                            'If objScan.InsertUpdateDelete_Documents() = False Then
                            '    Throw New Exception(objScan._Message)
                            '    Return False
                            'End If

                            'IO.File.Delete(docLocalPath & strFolderName & CStr(dRow.Item("imagename")))
                        End If

                    Next
                End If
                '**********[ IMAGES TRAN ]********* END

                '**********[ ATTACH FILE TRAN ]********* START
                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'If intExistingApplicantUnkId > 0 Then

                '    StrQ = "DELETE FROM rcattachfiletran WHERE applicantunkid  = " & intExistingApplicantUnkId & " and modulerefid = " & enImg_Email_RefId.Applicant_Module & " AND attachrefid = " & enScanAttactRefId.QUALIFICATIONS & " "

                '    objDataOp.ExecNonQuery(StrQ)

                '    If objDataOp.ErrorMessage <> "" Then
                '        exForce = New Exception(objDataOp.ErrorNumber & " : in ATTACH FILE TRAN : " & objDataOp.ErrorMessage)
                '        Throw exForce
                '    End If
                'End If
                'Sohail (26 May 2017) -- End

                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'Dim dr_Attach() As DataRow = dsAttachments.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                Dim dr_Attach() As DataRow
                If intExistingApplicantUnkId > 0 Then
                    If isExistServerNullAttachmentUnkId(intExistingApplicantUnkId) = True Then
                        'Sohail (16 Nov 2018) -- Start
                        'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                        'Dim dsTmp As DataSet = WebRef.GetAttachments(UserCred, strErrorMessage, False, mintCurrApplicantUnkID)
                        Dim dsTmp As DataSet = WebRef.GetAttachments(strErrorMessage, False, mintCurrApplicantUnkID, "")
                        'Sohail (16 Nov 2018) -- End
                        dr_Attach = dsTmp.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")

                        If clsCommonATLog.VoidAtTranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", intExistingApplicantUnkId, "rcattachfiletran", "attachfiletranunkid", 3, 3, , " modulerefid = " & enImg_Email_RefId.Applicant_Module & " AND attachrefid = " & enScanAttactRefId.QUALIFICATIONS & " ") = False Then
                            exForce = New Exception(objDataOp.ErrorNumber & ": " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If

                        StrQ = "DELETE FROM rcattachfiletran WHERE applicantunkid  = " & intExistingApplicantUnkId & " and modulerefid = " & enImg_Email_RefId.Applicant_Module & " AND attachrefid = " & enScanAttactRefId.QUALIFICATIONS & " "

                        objDataOp.ExecNonQuery(StrQ)

                        If objDataOp.ErrorMessage <> "" Then
                            exForce = New Exception(objDataOp.ErrorNumber & " : in ATTACH FILE TRAN : " & objDataOp.ErrorMessage)
                            Throw exForce
                        End If
                    Else
                        dr_Attach = dsAttachments.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                    End If
                Else
                    dr_Attach = dsAttachments.Tables(0).Select("applicantunkid = " & mintCurrApplicantUnkID & " ")
                End If
                'Sohail (26 May 2017) -- End

                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                'Sohail (05 Dec 2016) -- Start
                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                'Dim blnIsIISInstalled As Boolean = IsSelfServiceExist()
                'Sohail (05 Dec 2016) -- End
                Dim strError As String = ""
                'SHANI (16 JUL 2015) -- End 

                If dr_Attach.Length > 0 Then
                    For Each dRow As DataRow In dr_Attach

                        'Sohail (26 May 2017) -- Start
                        'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                        ''StrQ = "INSERT INTO rcattachfiletran(applicantunkid, documentunkid, modulerefid, attachrefid, filepath, filename, fileuniquename, attached_date) " & vbCrLf
                        ''StrQ &= "VALUES (" & mintNewApplicantUnkID & ", " & dRow.Item("documentunkid") & ", " & dRow.Item("modulerefid") & " , " & dRow.Item("attachrefid") & ", '" & dRow.Item("filepath").ToString.Replace("'", "''") & "', '" & dRow.Item("filename").ToString.Replace("'", "''") & "', '" & dRow.Item("fileuniquename").ToString.Replace("'", "''") & "', '" & CDate(dRow.Item("attached_date")).ToString("yyyy-MM-dd hh:mm:ss") & "' ) " & vbCrLf
                        'StrQ = "INSERT INTO rcattachfiletran(applicantunkid, documentunkid, modulerefid, attachrefid, filepath, filename, fileuniquename, attached_date) " & vbCrLf
                        'StrQ &= "VALUES (" & mintNewApplicantUnkID & ", " & dRow.Item("documentunkid") & ", " & dRow.Item("modulerefid") & " , " & dRow.Item("attachrefid") & ", '" & dRow.Item("filepath").ToString.Replace("'", "''") & "', '" & dRow.Item("filename").ToString.Replace("'", "''") & "', '" & dRow.Item("fileuniquename").ToString.Replace("'", "''") & "', " & If(IsDBNull(dRow.Item("attached_date")) = True, "NULL", " '" & CDate(dRow.Item("attached_date")).ToString("yyyy-MM-dd hh:mm:ss") & "'") & " ) " & vbCrLf

                        'objDataOp.ExecNonQuery(StrQ)

                        'If objDataOp.ErrorMessage <> "" Then
                        '    If objDataOp.ErrorNumber = "7346" OrElse objDataOp.ErrorMessage.Contains("Cannot get the data of the row from the OLEDB provider") OrElse objDataOp.ErrorMessage.Contains("row marked for deletion") = True OrElse objDataOp.ErrorMessage.Contains("value of a column has been changed after the containing row was last fetched error") = True OrElse objDataOp.ErrorMessage.Contains("Cannot fetch a row using a bookmark from OLE DB provider") = True Then
                        '        System.IO.File.AppendAllText(m_strLogFile, mstrFirstname & " " & mstrSurname & ";" & mstrEmail & ";" & objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage & " : " & Now.ToString & vbCrLf & vbCrLf)
                        '        GoTo CONTINUE_FOR
                        '    Else
                        '        exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                        '        Throw exForce
                        '    End If
                        'End If
                        dRow.Item("AUD") = "A"
                        If intExistingApplicantUnkId > 0 Then
                            Dim intUnkId As Integer = isExistServerAttachUnkId(intExistingApplicantUnkId, CInt(dRow.Item("serverattachfiletranunkid")), False)
                            If intUnkId > 0 Then
                                dRow.Item("attachfiletranunkid") = intUnkId
                                dRow.Item("applicantunkid") = intExistingApplicantUnkId
                                dRow.Item("AUD") = "U"
                            End If

                            'Sohail (12 Apr 2022) -- Start
                            'Enhancement : AC2-188 : ZRA - Move CV, cover letter and qualification on search job page to allow to attach documents for each vacancy in recruitment portal.
                            If CInt(dRow.Item("appvacancytranunkid")) > 0 Then
                                intUnkId = isExistServerAppVacancyUnkId(intExistingApplicantUnkId, CInt(dRow.Item("appvacancytranunkid")), False)
                                If intUnkId > 0 Then
                                    dRow.Item("appvacancytranunkid") = intUnkId
                                    If dRow.Item("AUD").ToString.Trim = "" Then
                                        dRow.Item("AUD") = "U"
                                    End If
                                End If
                            End If
                            'Sohail (12 Apr 2022) -- End

                            'Pinkal (30-Sep-2023) -- Start 
                            '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
                            If CInt(dRow.Item("applicantqualiftranunkid")) > 0 Then
                                intUnkId = isExistServerQualificationUnkId(intExistingApplicantUnkId, CInt(dRow.Item("applicantqualiftranunkid")), False)
                                If intUnkId > 0 Then
                                    dRow.Item("applicantqualiftranunkid") = intUnkId
                                    If dRow.Item("AUD").ToString.Trim = "" Then
                                        dRow.Item("AUD") = "U"
                                    End If
                                End If
                            End If
                            'Pinkal (30-Sep-2023) -- End 


                            'Pinkal (15-Sep-2023) -- Start
                            'Recruitment Issue Related to Attachement Issue [As Per Guidance of Sohail on 18-Sep-2023]
                        Else
                            If CInt(dRow.Item("appvacancytranunkid")) > 0 Then
                                Dim intUnkId As Integer = isExistServerAppVacancyUnkId(mintNewApplicantUnkID, CInt(dRow.Item("appvacancytranunkid")), False)
                                'Hemant (27 Sep 2024) -- [intExistingApplicantUnkId --> mintNewApplicantUnkID]
                                If intUnkId > 0 Then
                                    dRow.Item("appvacancytranunkid") = intUnkId
                                End If
                            End If
                            'Pinkal (15-Sep-2023) -- End

                            'Pinkal (30-Sep-2023) -- Start 
                            '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
                            If CInt(dRow.Item("applicantqualiftranunkid")) > 0 Then
                                Dim intUnkId As Integer = isExistServerQualificationUnkId(mintNewApplicantUnkID, CInt(dRow.Item("applicantqualiftranunkid")), False)
                                'Hemant (27 Sep 2024) -- [intExistingApplicantUnkId --> mintNewApplicantUnkID]
                                If intUnkId > 0 Then
                                    dRow.Item("applicantqualiftranunkid") = intUnkId
                                End If
                            End If
                            'Pinkal (30-Sep-2023) -- End 

                        End If
                        dRow.AcceptChanges()
                        'Sohail (26 May 2017) -- End

                        '*** [Download Images / Attachements] ***

                        'SHANI (16 JUL 2015) -- Start
                        'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                        'Upload Image folder to access those attachemts from Server and 
                        'all client machines if ArutiSelfService is installed otherwise save then on Document path
                        'If CInt(dRow.Item("attachrefid")) = enScanAttactRefId.QUALIFICATIONS Then 'Qualification Certificates
                        '    objWebRequest = System.Net.WebRequest.Create(imgServerPath & imgWebpath & "UploadQualiCerti/" & CStr(dRow.Item("fileuniquename")))
                        'Else
                        '    objWebRequest = System.Net.WebRequest.Create(imgServerPath & imgWebpath & CStr(dRow.Item("fileuniquename")))
                        'End If
                        'Sohail (22 Apr 2015) -- End
                        'objWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials

                        'If WebFileExist(objWebRequest) = True Then
                        '    objWebResponse = objWebRequest.GetResponse

                        '    lngMaxBytes = CLng(objWebResponse.ContentLength)
                        '    reader = objWebResponse.GetResponseStream
                        '    If CInt(dRow.Item("attachrefid")) = enScanAttactRefId.QUALIFICATIONS Then 'Qualification Certificates
                        '        If IO.Directory.Exists(docLocalPath & strFolderName) = False Then
                        '            IO.Directory.CreateDirectory(docLocalPath & strFolderName)
                        '        End If
                        '        writer = IO.File.Create(docLocalPath & strFolderName & CStr(dRow.Item("fileuniquename")))
                        '    Else
                        '        writer = IO.File.Create(imgLocalPath & CStr(dRow.Item("fileuniquename")))
                        '    End If

                        '    bytesRead = 0
                        '    lngTotalBytes = 0
                        '    While True
                        '        bytesRead = reader.Read(buffer, 0, buffer.Length)
                        '        If bytesRead <= 0 Then Exit While

                        '        writer.Write(buffer, 0, bytesRead)
                        '        lngTotalBytes += bytesRead
                        '    End While
                        '    reader.Close()
                        '    writer.Close()

                        Dim strErrorMsg As String = ""
                        Dim byteImg As Byte()

                        'Sohail (04 Jul 2019) -- Start
                        'PACT Enhancement - Support Issue Id # 3954 - 76.1 - Deleted attachments are getting downloaded when Import Data is done.

                        'Pinkal (15-Sep-2023) -- Start
                        'NMB- CHANGED DUE TO ATTACHMENT FOR APPLICANTS DUPLICATED DUE TO APPLICANT VOIDED ON SERVER AND ATTACHE NEW DOCUMENT AND IN IMPORT FROM WEB SYSTEM WILL NOT GET NEW DOCUMENT THAT'S WHY WE HAVE TO CHECK ALL ATTACHMENT.
                        'If CBool(dRow.Item("isvoid")) = False Then
                        'Pinkal (15-Sep-2023) -- End

                            'Sohail (04 Jul 2019) -- End

                            'Sohail (05 Dec 2016) -- Start
                            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                            'If CInt(dRow.Item("attachrefid")) = enScanAttactRefId.QUALIFICATIONS Then 'Qualification Certificates
                            '    byteImg = WebRef.DownloadFile(UserCred, "UploadQualiCerti", CStr(dRow.Item("fileuniquename")), strErrorMsg)
                            'Else
                            '    byteImg = WebRef.DownloadFile(UserCred, "", CStr(dRow.Item("fileuniquename")), strErrorMsg)
                            'End If
                            'Sohail (16 Nov 2018) -- Start
                            'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                            'byteImg = WebRef.DownloadFile(UserCred, CStr(dRow.Item("filepath")), strErrorMsg)
                            If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "file path : " & CStr(dRow.Item("filepath")) & " : " & mstrEmail.ToString & vbCrLf)
                            'Sohail (15 Oct 2021) -- Start
                            'NMB Issue :  : There is an error in XML Document (1, 294) on downloading attachment on import data in recruitment.
                            'byteImg = WebRef.DownloadFile(CStr(dRow.Item("filepath")), strErrorMsg)
                            Dim ds1 As DataSet = GetWebServiceResponse(WebRef, UserCred, "DownloadFile", strErrorMsg, True, 0, CStr(dRow.Item("filepath")))
                            'Sohail (15 Oct 2021) -- End
                            'Sohail (16 Nov 2018) -- End
                            'Sohail (05 Dec 2016) -- End
                            'Sohail (05 Dec 2016) -- Start
                            'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                            'If byteImg IsNot Nothing Then
                            'Sohail (15 Oct 2021) -- Start
                            'NMB Issue :  : There is an error in XML Document (1, 294) on downloading attachment on import data in recruitment.
                            'If byteImg IsNot Nothing AndAlso strErrorMsg.Trim = "" Then
                            If strErrorMsg.Trim = "" Then
                                'Sohail (20 Jun 2023) -- Start
                                'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
                                'byteImg = Convert.FromBase64String(ds1.Tables(0).Rows(0)(0).ToString)
                                byteImg = Convert.FromBase64String(ds1.Tables("DownloadFileResponse").Rows(0)(0).ToString)
                                'byteImg = System.Text.Encoding.UTF8.GetBytes(ds1.Tables(0).Rows(0)(0))
                                'byteImg = System.Text.Encoding.UTF8.GetBytes(ds1.Tables(0).Rows(0)(0).ToString)
                                'Sohail (20 Jun 2023) -- End                                
                                'Sohail (15 Oct 2021) -- End
                                'Sohail (05 Dec 2016) -- End
                                'SHANI (16 JUL 2015) -- End 
                                If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "file downloaded : image length : " & byteImg.Length & " : " & mstrEmail.ToString & vbCrLf)
                                'Sohail (04 Jul 2019) -- Start
                                'PACT Enhancement - Support Issue Id # 3955 - 76.1 - Check box option "Mandatory Attachment for Recruitment Portal" in common master for attachment types (Setting on aruti configuration to set Curriculam Vitae and Cover Letter attachment mandatory).
                                If dicFolderName.ContainsKey(CInt(dRow.Item("attachrefid"))) = True Then
                                    strFolderName = dicFolderName.Item(CInt(dRow.Item("attachrefid")))
                                Else
                                    strFolderName = "Qualifications"
                                End If
                                'Sohail (04 Jul 2019) -- End

                                'Sohail (22 Apr 2015) -- Start
                                'Enhancement - Providing qualification attachment, certificate no., achivement, email confirmation in online recruitmenmt.
                                Dim objScan As New clsScan_Attach_Documents
                                Dim dt As DataTable = objScan._Datatable
                                Dim rw As DataRow = dt.NewRow
                                rw.Item("scanattachtranunkid") = -1
                                rw.Item("documentunkid") = dRow.Item("documentunkid")
                                rw.Item("employeeunkid") = mintNewApplicantUnkID
                                'Sohail (05 Dec 2016) -- Start
                                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                                'rw.Item("filename") = clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename")
                                If drRow.Item("referenceno").ToString.Trim <> "" Then
                                    rw.Item("filename") = clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename")
                                Else
                                    rw.Item("filename") = drRow.Item("UserId").ToString & "_" & dRow.Item("filename")
                                End If
                                'Sohail (05 Dec 2016) -- End
                                rw.Item("scanattachrefid") = dRow.Item("attachrefid")
                                rw.Item("modulerefid") = dRow.Item("modulerefid")

                                'Shani(24-Aug-2015) -- Start
                                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                'rw.Item("userunkid") = User._Object._Userunkid
                                rw.Item("userunkid") = intUserunkid
                                'Shani(24-Aug-2015) -- End

                                rw.Item("transactionunkid") = -1
                                rw.Item("attached_date") = dRow.Item("attached_date")
                                'Sohail (05 Dec 2016) -- Start
                                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                                'rw.Item("destfilepath") = docLocalPath & strFolderName & clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & CStr(dRow.Item("filename"))
                                If drRow.Item("referenceno").ToString.Trim <> "" Then
                                    rw.Item("destfilepath") = docLocalPath & strFolderName & clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & CStr(dRow.Item("filename"))
                                Else
                                    rw.Item("destfilepath") = docLocalPath & strFolderName & drRow.Item("UserId").ToString & "_" & CStr(dRow.Item("filename"))
                                End If
                                'Sohail (05 Dec 2016) -- End

                                'SHANI (16 JUL 2015) -- Start
                                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                                'Upload Image folder to access those attachemts from Server and 
                                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                                'If objScan.IsExist(CInt(dRow.Item("modulerefid")), CInt(dRow.Item("attachrefid")), clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename").ToString, mintNewApplicantUnkID) = False Then
                                '    rw.Item("orgfilepath") = docLocalPath & strFolderName & CStr(dRow.Item("fileuniquename"))
                                '    rw.Item("AUD") = "A"
                                'Else
                                '    rw.Item("orgfilepath") = docLocalPath & strFolderName & clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & CStr(dRow.Item("filename"))
                                '    rw.Item("AUD") = "U"
                                'End If
                                rw.Item("fileuniquename") = CStr(dRow.Item("fileuniquename"))
                                rw.Item("filesize") = dRow.Item("file_size")
                                rw.Item("orgfilepath") = ""
                                rw.Item("appvacancytranunkid") = dRow.Item("appvacancytranunkid") 'Sohail (12 Apr 2022)

                            'Pinkal (30-Sep-2023) -- Start 
                            '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
                            rw.Item("applicantqualiftranunkid") = dRow.Item("applicantqualiftranunkid")
                            'Pinkal (30-Sep-2023) -- End 


                            'Pinkal (15-Sep-2023) -- Start
                            'NMB- CHANGED DUE TO ATTACHMENT FOR APPLICANTS DUPLICATED DUE TO APPLICANT VOIDED ON SERVER AND ATTACHE NEW DOCUMENT AND IN IMPORT FROM WEB SYSTEM WILL NOT GET NEW DOCUMENT THAT'S WHY WE HAVE TO CHECK ALL ATTACHMENT.
                            rw.Item("isactive") = Not CBool(dRow.Item("isvoid"))
                            'Pinkal (15-Sep-2023) -- End


                                'Sohail (05 Dec 2016) -- Start
                                'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
                                'If objScan.IsExist(CInt(dRow.Item("modulerefid")), CInt(dRow.Item("attachrefid")), clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename").ToString, mintNewApplicantUnkID) = False Then
                                '    rw.Item("AUD") = "A"
                                'Else
                                '    rw.Item("AUD") = "U"
                                '    Dim dt_Table As DataTable = objScan.GetAttachmentTranunkIds(mintNewApplicantUnkID, CInt(dRow.Item("attachrefid")), CInt(dRow.Item("modulerefid")), -1, clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename").ToString)
                                '    If dt_Table IsNot Nothing AndAlso dt_Table.Rows.Count > 0 Then
                                '        rw.Item("scanattachtranunkid") = CInt(dt_Table.Rows(0).Item("scanattachtranunkid"))
                                '    End If
                                'End If
                                If drRow.Item("referenceno").ToString.Trim <> "" Then

                                'Pinkal (30-Sep-2023) -- Start 
                                '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
                                'If objScan.IsExist(CInt(dRow.Item("modulerefid")), CInt(dRow.Item("attachrefid")), clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename").ToString, mintNewApplicantUnkID, , , CInt(dRow.Item("appvacancytranunkid"))) = False Then
                                If objScan.IsExist(CInt(dRow.Item("modulerefid")), CInt(dRow.Item("attachrefid")), clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename").ToString, mintNewApplicantUnkID, , , CInt(dRow.Item("appvacancytranunkid")), CInt(dRow.Item("applicantqualiftranunkid"))) = False Then
                                    'Pinkal (30-Sep-2023) -- End 

                                        'Sohail (12 Apr 2022) - [appvacancytranunkid]
                                        rw.Item("AUD") = "A"
                                    Else
                                        rw.Item("AUD") = "U"

                                    'Pinkal (30-Sep-2023) -- Start 
                                    '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
                                    'Dim dt_Table As DataTable = objScan.GetAttachmentTranunkIds(mintNewApplicantUnkID, CInt(dRow.Item("attachrefid")), CInt(dRow.Item("modulerefid")), -1, clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename").ToString, CInt(dRow.Item("appvacancytranunkid")))
                                    Dim dt_Table As DataTable = objScan.GetAttachmentTranunkIds(mintNewApplicantUnkID, CInt(dRow.Item("attachrefid")), CInt(dRow.Item("modulerefid")), -1, clsCrypto.Dicrypt(drRow.Item("referenceno").ToString) & "_" & dRow.Item("filename").ToString, CInt(dRow.Item("appvacancytranunkid")), CInt(dRow.Item("applicantqualiftranunkid")))
                                    'Pinkal (30-Sep-2023) -- End 

                                        'Sohail (12 Apr 2022) - [appvacancytranunkid]
                                        If dt_Table IsNot Nothing AndAlso dt_Table.Rows.Count > 0 Then
                                            rw.Item("scanattachtranunkid") = CInt(dt_Table.Rows(0).Item("scanattachtranunkid"))
                                        End If
                                    End If
                                Else

                                'Pinkal (30-Sep-2023) -- Start 
                                '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
                                'If objScan.IsExist(CInt(dRow.Item("modulerefid")), CInt(dRow.Item("attachrefid")), drRow.Item("UserId").ToString & "_" & dRow.Item("filename").ToString, mintNewApplicantUnkID, , , CInt(dRow.Item("appvacancytranunkid"))) = False Then
                                If objScan.IsExist(CInt(dRow.Item("modulerefid")), CInt(dRow.Item("attachrefid")), drRow.Item("UserId").ToString & "_" & dRow.Item("filename").ToString, mintNewApplicantUnkID, , , CInt(dRow.Item("appvacancytranunkid")), CInt(dRow.Item("applicantqualiftranunkid"))) = False Then
                                    'Pinkal (30-Sep-2023) -- End 

                                        'Sohail (12 Apr 2022) - [appvacancytranunkid]
                                        rw.Item("AUD") = "A"
                                    Else

                                    'Pinkal (15-Sep-2023) -- Start
                                    'NMB- CHANGED DUE TO ATTACHMENT FOR APPLICANTS DUPLICATED DUE TO APPLICANT VOIDED ON SERVER AND ATTACHE NEW DOCUMENT AND IN IMPORT FROM WEB SYSTEM WILL NOT GET NEW DOCUMENT THAT'S WHY WE HAVE TO CHECK ALL ATTACHMENT.
                                    If CBool(dRow.Item("isvoid")) = False Then
                                        rw.Item("AUD") = "U"
                                    Else
                                        rw.Item("AUD") = "D"
                                    End If
                                    'Pinkal (15-Sep-2023) -- END

                                    'Pinkal (30-Sep-2023) -- Start 
                                    '(A1X-1354) TRA - Disable "Other Qualification" option from the qualifications tab on the applicant portal. 
                                    'Dim dt_Table As DataTable = objScan.GetAttachmentTranunkIds(mintNewApplicantUnkID, CInt(dRow.Item("attachrefid")), CInt(dRow.Item("modulerefid")), -1, drRow.Item("UserId").ToString & "_" & dRow.Item("filename").ToString, CInt(dRow.Item("appvacancytranunkid")))
                                    Dim dt_Table As DataTable = objScan.GetAttachmentTranunkIds(mintNewApplicantUnkID, CInt(dRow.Item("attachrefid")), CInt(dRow.Item("modulerefid")), -1, drRow.Item("UserId").ToString & "_" & dRow.Item("filename").ToString, CInt(dRow.Item("appvacancytranunkid")), CInt(dRow.Item("applicantqualiftranunkid")))
                                    'Pinkal (30-Sep-2023) -- End 

                                        'Sohail (12 Apr 2022) - [appvacancytranunkid]
                                        If dt_Table IsNot Nothing AndAlso dt_Table.Rows.Count > 0 Then
                                            rw.Item("scanattachtranunkid") = CInt(dt_Table.Rows(0).Item("scanattachtranunkid"))
                                        End If
                                    End If
                                End If
                                'Sohail (05 Dec 2016) -- End
                                If blnIsIISInstalled Then

                                    'Shani(24-Aug-2015) -- Start
                                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                    'If clsFileUploadDownload.ByteImageUpload(byteImg, strFolderName, CStr(dRow.Item("fileuniquename")), strError) = False Then
                                    If clsFileUploadDownload.ByteImageUpload(byteImg, strFolderName, CStr(dRow.Item("fileuniquename")), strError, strArutiSelfServiceURL) = False Then
                                        'Shani(24-Aug-2015) -- End

                                        eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                                        Exit Function
                                    Else

                                        'Shani(24-Aug-2015) -- Start
                                        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                                        'Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                                        Dim strPath As String = strArutiSelfServiceURL
                                        'Shani(24-Aug-2015) -- End

                                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                                            strPath += "/"
                                        End If
                                        strPath += "uploadimage/" & strFolderName & "/" + CStr(dRow.Item("fileuniquename"))
                                        rw.Item("filepath") = strPath
                                        If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "file uploaded in IIS : " & strPath & " : " & mstrEmail.ToString & vbCrLf)
                                    End If
                                Else
                                    If System.IO.Directory.Exists(docLocalPath) Then
                                        If System.IO.Directory.Exists(docLocalPath & strFolderName) = False Then
                                            System.IO.Directory.CreateDirectory(docLocalPath & strFolderName)
                                        End If

                                        Dim mstrfilepath As String = docLocalPath & strFolderName & "\" & CStr(dRow.Item("fileuniquename"))
                                        Dim ms As New System.IO.MemoryStream(byteImg)
                                        Dim fs As New System.IO.FileStream(mstrfilepath, System.IO.FileMode.Create)
                                        ms.WriteTo(fs)
                                        rw.Item("filepath") = mstrfilepath
                                        If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "file uploaded on path : " & mstrfilepath & " : " & mstrEmail.ToString & vbCrLf)
                                    Else
                                        eZeeMsgBox.Show("Configuration Path not Exist.", enMsgBoxStyle.Information)
                                        Exit Function
                                    End If
                                End If
                                'SHANI (16 JUL 2015) -- End

                                dt.Rows.Add(rw)

                                objScan._Datatable = dt
                                If objScan.InsertUpdateDelete_Documents(objDataOp) = False Then
                                    Throw New Exception(objScan._Message)
                                    Return False
                                End If

                                'Sohail (26 May 2017) -- Start
                                'Issue - 67.1 - error file not found from client machine as path not found.
                                'IO.File.Delete(docLocalPath & strFolderName & CStr(dRow.Item("fileuniquename")))
                                If IO.File.Exists(docLocalPath & strFolderName & CStr(dRow.Item("fileuniquename"))) = True Then
                                    IO.File.Delete(docLocalPath & strFolderName & CStr(dRow.Item("fileuniquename")))
                                End If
                                'Sohail (26 May 2017) -- End
                                'Sohail (22 Apr 2015) -- End
                            Else
                                If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, "file downloaded failed : " & strErrorMsg & " : " & mstrEmail.ToString & vbCrLf)
                            End If

                            'Sohail (04 Jul 2019) -- Start
                            'PACT Enhancement - Support Issue Id # 3954 - 76.1 - Deleted attachments are getting downloaded when Import Data is done.

                        'Pinkal (15-Sep-2023) -- Start
                        'NMB- CHANGED DUE TO ATTACHMENT FOR APPLICANTS DUPLICATED DUE TO APPLICANT VOIDED ON SERVER AND ATTACHE NEW DOCUMENT AND IN IMPORT FROM WEB SYSTEM WILL NOT GET NEW DOCUMENT THAT'S WHY WE HAVE TO CHECK ALL ATTACHMENT.
                        'End If
                        'Pinkal (15-Sep-2023) -- END
                        'Sohail (04 Jul 2019) -- End

                    Next
                    'Sohail (26 May 2017) -- Start
                    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                    Dim objAttach As New clsAppAttachFileTran
                    'Sohail (24 Jan 2018) -- Start
                    'Issue: TANAPA#1932 - Qualification and other child table data inserted with applicantunkid 0 for new applicants in 70.1.
                    'objAttach._ApplicantUnkid = intExistingApplicantUnkId
                    objAttach._ApplicantUnkid = mintNewApplicantUnkID
                    'Sohail (24 Jan 2018) -- End
                    objAttach._DataList = dr_Attach.CopyToDataTable
                    If objAttach.InsertUpdateDelete_AttachFileTran() = False Then
                        exForce = New Exception("error in applicant attach file")
                        Throw exForce
                    End If
                    'Sohail (26 May 2017) -- End
                End If

                'Sohail (26 May 2017) -- Start
                'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
                'If clsCommonATLog.BulkInsert_TranAtLog(objDataOp, "rcapplicant_master", "applicantunkid", mintNewApplicantUnkID, "rcattachfiletran", "attachfiletranunkid", 1, 1, , , , mintUserunkid) = False Then
                '    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                '    Throw exForce
                'End If
                'Sohail (26 May 2017) -- End
                blnResult = True
                '**********[ ATTACH FILE TRAN ]********* END






                '**********[ UPDATING SYNC DATE TIME ]********* START
                strErrorMessage = ""
                'Sohail (16 Nov 2018) -- Start
                'Medium Trust Level Hosting Server Issue - eZeecommonlib not supported in medium trust level hosting server due as it is fully trusted in 75.1.
                'WebRef.UpdateSyncDateTimeForImport(UserCred, strErrorMessage, True, mintCurrApplicantUnkID.ToString)
                WebRef.UpdateSyncDateTimeForImport(strErrorMessage, True, mintCurrApplicantUnkID.ToString)
                'Sohail (16 Nov 2018) -- End

                If strErrorMessage <> "" Then
                    exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                    Exit Try
            End If
                '**********[ UPDATING SYNC DATE TIME ]********* END






CONTINUE_FOR:
                If bw IsNot Nothing Then
                    intCount = intCount + 1
                    bw.ReportProgress(intCount)

                    If bw.CancellationPending = True Then
                        Exit For
                    End If
                End If

            Next




            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            objDataOp.ExecNonQuery("UPDATE " & strConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|0' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
            'Shani(24-Aug-2015) -- End

                If objDataOp.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                    Throw exForce
                End If

                '*** Insert Audit Log 

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Call InsertATLog(mstrApplicantIDs, False, StartTime)
            Call InsertATLog(mstrApplicantIDs, False, StartTime, intUserunkid, dtCurrentDateAndTime)
            'Shani(24-Aug-2015) -- End

            'Sohail (05 Jun 2020) -- Start
            'NMB Enhancement # : Upon successful application of a vacancy for internal vacancies, system to copy the reporting-to on the reverse email sent back to the employee.
            If HttpContext.Current Is Nothing Then
                trd = New Thread(AddressOf Send_Notification)
                trd.IsBackground = True
                Dim arr(1) As Object
                arr(0) = mintCompID
                trd.Start(arr)
            Else
                Call Send_Notification(mintCompID)
            End If
            'Sohail (05 Jun 2020) -- End

            'Sohail (20 Jun 2023) -- Start
            'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
            If arrApplicantsIDs.Trim <> "" Then
                mstrMessage = Language.getMessage(mstrModuleName, 23, "Please Redo Import Data to import the rest of the applicants.")
            End If
            'Sohail (20 Jun 2023) -- End

            'objDataOp.ReleaseTransaction(True)
            System.Windows.Forms.Cursor.Current = Cursors.Default
            Return True

        Catch ex As Exception
            'objDataOp.ReleaseTransaction(False)
            'Sohail (26 Aug 2011) -- Start

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOp.ExecNonQuery("UPDATE " & Company._Object._ConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|0' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
            objDataOp.ExecNonQuery("UPDATE " & strConfigDatabaseName & "..cfconfiguration SET key_value = '" & mstrMBoardSrNo & "|" & mstrMachineName & "|0' WHERE key_name = 'ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " ")
            'Shani(24-Aug-2015) -- End

            If objDataOp.ErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & objDataOp.ErrorMessage)
                Throw exForce
            End If
            '*** Insert Audit Log 

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Call InsertATLog(mstrApplicantIDs, True, StartTime)
            Call InsertATLog(mstrApplicantIDs, True, StartTime, intUserunkid, dtCurrentDateAndTime)
            'Shani(24-Aug-2015) -- End

            'Sohail (26 Aug 2011) -- End
            'Sohail (15 Oct 2019) -- Start
            'DisplayError.Show("-1", ex.Message, "ImportFromWeb", mstrModuleName)
            Dim strError As String = ex.Message & "; " & ex.StackTrace.ToString
            If ex.InnerException IsNot Nothing Then
                strError &= "; " & ex.InnerException.Message
            End If
            'Throw New Exception(ex.Message & " for " & mstrEmail & "; Procedure Name: ImportFromWeb; Module Name: " & mstrModuleName)
            Throw New Exception(strError & " for " & mstrEmail & "; Procedure Name: ImportFromWeb; Module Name: " & mstrModuleName)
            'Sohail (15 Oct 2019) -- End
            Return False
        Finally
        End Try
    End Function
    'Sohail (18 May 2015) -- End

    'Sohail (05 Jun 2020) -- Start
    'NMB Enhancement # : Upon successful application of a vacancy for internal vacancies, system to copy the reporting-to on the reverse email sent back to the employee.
    Private Sub Send_Notification(ByVal intCompanyUnkId As Object)
        Try
            If gobjEmailList.Count > 0 Then

                Dim objSendMail As New clsSendMail
                For Each obj In gobjEmailList
                    objSendMail._ToEmail = obj._EmailTo
                    objSendMail._Subject = obj._Subject
                    objSendMail._Message = obj._Message
                    objSendMail._Form_Name = obj._Form_Name
                    objSendMail._LogEmployeeUnkid = obj._LogEmployeeUnkid
                    objSendMail._OperationModeId = obj._OperationModeId
                    objSendMail._UserUnkid = obj._UserUnkid
                    objSendMail._SenderAddress = obj._SenderAddress
                    objSendMail._ModuleRefId = obj._ModuleRefId

                    Try
                        If TypeOf intCompanyUnkId Is Integer Then
                            objSendMail.SendMail(CInt(intCompanyUnkId))
                        Else
                            objSendMail.SendMail(CInt(intCompanyUnkId(0)))
                        End If
                    Catch ex As Exception

                    End Try
                Next
                gobjEmailList.Clear()
            End If
        Catch ex As Exception
            Throw New Exception(mstrModuleName & ":Send_Notification:- " & ex.Message)
        Finally
            If gobjEmailList.Count > 0 Then
                gobjEmailList.Clear()
            End If
        End Try
    End Sub
    'Sohail (05 Jun 2020) -- End

    Public Function WebFileExist(ByRef objRequest As System.Net.WebRequest) As Boolean
        Dim objResponse As System.Net.HttpWebResponse
        Try
            objResponse = CType(objRequest.GetResponse, Net.HttpWebResponse)

            Return True
        Catch ex As System.Net.WebException
            Dim r As System.Net.HttpWebResponse = CType(ex.Response, Net.HttpWebResponse)
            If r.StatusCode = Net.HttpStatusCode.NotFound Then
                Return False
            Else
                MessageBox.Show(ex.ToString)
            End If
        Finally
            objResponse = Nothing
        End Try

    End Function
    'S.SANDEEP [ 30 JULY 2011 ] -- END 

    'Sohail (15 Oct 2021) -- Start
    'NMB Issue :  : There is an error in XML Document (1, 294) on downloading attachment on import data in recruitment.
    Public Function GetWebServiceResponse(ByVal WebRef As WR.ArutiRecruitmentService _
                                          , ByVal UserCred As WR.UserCredentials _
                                          , ByVal strMethodName As String _
                                          , ByRef strErrorMessage As String _
                                          , ByVal blnOnlyPendingApplicants As Boolean _
                                          , ByVal intApplicantUnkId As Integer _
                                          , Optional ByVal strFilePath As String = "" _
                                          , Optional ByVal strApplicantUnkIDs As String = "" _
                                          ) As DataSet
        'Sohail (20 Jun 2023) - [strApplicantUnkIDs]

        Dim dsList As New DataSet
        Dim blnGenerateLOG As Boolean = True
        Try
            dsList.Clear()
            strErrorMessage = ""

            Dim request As HttpWebRequest = CType(WebRequest.Create(WebRef.Url), HttpWebRequest)

            request.Method = "POST"
            request.ContentType = "text/xml; charset=utf-8"
            request.Accept = "text/xml"
            request.Headers.Add("SOAPAction:https://www.arutihr.com/" & strMethodName)
            request.UserAgent = WebRef.UserAgent 'Sohail (28 Apr 2022)
            request.Timeout = WebRef.Timeout 'Sohail (08 Sep 2022) - 

            Dim strSoapBody As String = ""
            Dim SOAPReqBody As New XmlDocument

            If strMethodName.ToLower.Trim = "downloadfile" Then

                strSoapBody = "<soap:Body>" & _
                            "<" & strMethodName & "  xmlns='https://www.arutihr.com/'>" & _
                                "<strFilePath>" & strFilePath & "</strFilePath>" & _
                                "<strErrorMessage>" & strErrorMessage & "</strErrorMessage>" & _
                            "</" & strMethodName & " >" & _
                          "</soap:Body>"

            Else

                strSoapBody = "<soap:Body>" & _
                                "<" & strMethodName & " xmlns='https://www.arutihr.com/'>" & _
                                    "<strErrorMessage>" & strErrorMessage & "</strErrorMessage>" & _
                                    "<blnOnlyPendingApplicants>" & CBool(blnOnlyPendingApplicants).ToString.ToLower & "</blnOnlyPendingApplicants>" & _
                                    "<intApplicantID>" & intApplicantUnkId & "</intApplicantID>" & _
                                    "<strApplicantUnkIDs>" & strApplicantUnkIDs & "</strApplicantUnkIDs>" & _
                                "</" & strMethodName & ">" & _
                              "</soap:Body>"
                'Sohail (20 Jun 2023) - [strApplicantUnkIDs]

            End If

            Dim s As String = "<?xml version='1.0' encoding='utf-8'?>" & _
                                                    "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>" & _
                                                    "<soap:Header>" & _
                                                    "<UserCredentials xmlns='https://www.arutihr.com/'>" & _
                                                    "<Password>" & UserCred.Password & "</Password>" & _
                                                    "<WebClientID>" & UserCred.WebClientID.ToString & "</WebClientID>" & _
                                                    "<CompCode>" & UserCred.CompCode & "</CompCode>" & _
                                                    "<AuthenticationCode>" & UserCred.AuthenticationCode.ToString & "</AuthenticationCode>" & _
                                                    "</UserCredentials>" & _
                                                  "</soap:Header>" & _
                                                  strSoapBody & _
                                                "</soap:Envelope>"

            SOAPReqBody.LoadXml(s)

            Using streamWriter = New StreamWriter(request.GetRequestStream())
                SOAPReqBody.Save(streamWriter)
            End Using

            Dim xmlFileName As String = IO.Path.Combine(My.Application.Info.DirectoryPath, "RecruitImport" & Format(DateAndTime.Now.Date, "yyyyMMdd") & "_" & strMethodName & ".xml") 'Sohail (20 Jun 2023)
            Dim strResponse As String = String.Empty
            Dim httpResponse = CType(request.GetResponse(), HttpWebResponse)
            Using streamReader = New StreamReader(httpResponse.GetResponseStream(), Encoding.GetEncoding(httpResponse.CharacterSet))
                strResponse = streamReader.ReadToEnd()
                'Using xw As XmlWriter = XmlWriter.Create(xmlFileName, st)
                '    xw.WriteValue(streamReader.ReadToEnd())
                'End Using
                'Dim SOAPResponse As New XmlDocument()
                'SOAPResponse.LoadXml(streamReader.ReadToEnd())
                'SOAPResponse.Save(xmlFileName)
                'Dim block(10000) As Char
                'While streamReader.Peek() >= 0
                '    streamReader.Read(block, 0, block.Length)
                '    Dim blockString As String = New String(block)
                '    strResponse.Append(blockString)
                'End While

                'While streamReader.Peek() >= 0
                '    streamReader.ReadBlock(block, 0, block.Length)
                '    Dim blockString As String = New String(block)
                '    xw.WriteString(blockString)
                'End While


                'streamReader.DiscardBufferedData()
                streamReader.Close()
                streamReader.Dispose()
            End Using

            'Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strResponse)
            '' Convert the base64 encoded data back into the binary compressed format.
            'Dim memStream As System.IO.MemoryStream = New System.IO.MemoryStream(byteArray)
            ''Dim compressedStream As System.IO.Compression.DeflateStream = New System.IO.Compression.DeflateStream(httpResponse.GetResponseStream(), System.IO.Compression.CompressionMode.Decompress, True)
            'Dim decompressedStream As System.IO.Compression.DeflateStream = New System.IO.Compression.DeflateStream(memStream, System.IO.Compression.CompressionMode.Decompress, True)
            'Dim outputArray(byteArray.Length) As Byte
            'decompressedStream.Read(outputArray, 0, outputArray.Length)
            'Dim rslt As String = Encoding.UTF8.GetString(outputArray)
            '' Convert the compressed XML data back into an ASP.NET DataSet.
            'Dim ds As DataSet = New DataSet()
            'ds.ReadXml(rslt)


            Dim SOAPResponse As New XmlDocument()
            SOAPResponse.LoadXml(strResponse)
            'Sohail (20 Jun 2023) -- Start
            'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
            'Dim xn As XmlNode = SOAPResponse.DocumentElement
            SOAPResponse.Save(xmlFileName)
            SOAPResponse = Nothing
            'Sohail (20 Jun 2023) -- End
            'Sohail (04 Jan 2022) -- Start
            'Issue : : NMB - There is an error in XML Document (144, 49867) in XmlSerialization.
            'dsList.ReadXml(New StringReader(xn.InnerXml.Replace("*", "")))
            If strMethodName.ToLower.Trim = "downloadfile" Then
                'If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, xn.InnerXml & vbCrLf & vbCrLf) 'Sohail (20 Jun 2023)
                'Sohail (11 Apr 2022) -- Start
                'Enhancement : OLD-665 : ZRA - Recruitment changes requested in march 2022. - System.OutOfMemoryException at String.Replace
                'dsList.ReadXml(New StringReader(xn.InnerXml.Replace("*", "")))
                'Sohail (20 Jun 2023) -- Start
                'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
                'dsList.ReadXml(New StringReader(xn.InnerXml))
                dsList.ReadXml(xmlFileName)
                'Sohail (20 Jun 2023) -- End
                'Sohail (11 Apr 2022) -- End
            Else
                'If blnGenerateLOG = True Then System.IO.File.AppendAllText(m_strLogFile, xn.InnerXml & vbCrLf & vbCrLf) 'Sohail (20 Jun 2023)
                'Sohail (11 Apr 2022) -- Start
                'Enhancement : OLD-665 : ZRA - Recruitment changes requested in march 2022.
                'dsList.ReadXml(New StringReader(xn.InnerXml.Replace("*", "")), XmlReadMode.ReadSchema)
                'Sohail (20 Jun 2023) -- Start
                'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
                'dsList.ReadXml(New StringReader(xn.InnerXml), XmlReadMode.ReadSchema)
                dsList.ReadXml(xmlFileName, XmlReadMode.ReadSchema)
                'Sohail (20 Jun 2023) -- End
                If dsList.Tables.Count <= 0 Then
                    'Sohail (20 Jun 2023) -- Start
                    'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
                    'dsList.ReadXml(New StringReader(xn.InnerXml), XmlReadMode.Auto)
                    dsList.ReadXml(xmlFileName, XmlReadMode.Auto)
                    'Sohail (20 Jun 2023) -- End
                End If
                'Sohail (11 Apr 2022) -- End
                'Dim ss As String = IO.File.ReadAllText("xm1.xml")
                'Dim guids As RegularExpressions.MatchCollection
                'guids = RegularExpressions.Regex.Matches(ss, "<UserId>\{?[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}}?</UserId>")
                'For i = 0 To guids.Count - 1
                '    System.IO.File.AppendAllText(m_strLogFile, guids(i).Value & vbCrLf)
                'Next
            End If
            'Sohail (04 Jan 2022) -- End

            If dsList.Tables(0).Columns.Contains("strErrorMessage") = True AndAlso dsList.Tables(0).Rows.Count > 0 Then
                strErrorMessage = dsList.Tables(0).Rows(0).Item("strErrorMessage").ToString
            End If

            'Sohail (20 Jun 2023) -- Start
            'Issue : : TRA - Out of Memory exception on Import applicant data from recruitment.
            If dsList.Tables.Count > 1 AndAlso dsList.Tables(1).Columns.Contains("strErrorMessage") = True AndAlso dsList.Tables(1).Rows.Count > 0 Then
                strErrorMessage = dsList.Tables(1).Rows(0).Item("strErrorMessage").ToString
            End If
            'Sohail (20 Jun 2023) -- End

            Return dsList

        Catch ex As Exception
            Dim strError As String = ex.Message & "; " & ex.StackTrace.ToString
            If ex.InnerException IsNot Nothing Then
                strError &= "; " & ex.InnerException.Message
            End If
            Throw New Exception(strError & " for " & mstrEmail & "; Method: " & strMethodName & " Procedure Name: GetWebServiceResponse; Module Name: " & mstrModuleName)
            Return Nothing
        End Try
    End Function
    'Sohail (15 Oct 2021) -- End

    'Sohail (26 Aug 2011) -- Start

    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function GetApplicantImportApplicantStatus() As Boolean
    Private Function GetApplicantImportApplicantStatus(ByVal strConfigDatabaseName As String, ByVal mintCompID As Integer) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim objDataOperation As New clsDataOperation
        Dim ds As DataSet
        Dim exForce As Exception
        Dim arr() As String
        Dim StrQ As String = String.Empty
        Try


            '*** Gt Applicant Import Package Status :   Motherboard Serial Number|Machine Name|True/False

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'StrQ = "SELECT * FROM " & Company._Object._ConfigDatabaseName & "..cfconfiguration WHERE key_name='ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " AND LTRIM(RTRIM(key_value)) <> '' "
            StrQ = "SELECT * FROM " & strConfigDatabaseName & "..cfconfiguration WHERE key_name='ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & " AND LTRIM(RTRIM(key_value)) <> '' "
            'Shani(24-Aug-2015) -- End

            ds = objDataOperation.ExecQuery(StrQ, "Status")
            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If ds.Tables("Status").Rows.Count > 0 Then
                '** Motherboard Serial Number|Machine Name|0/1
                arr = Split(CStr(ds.Tables("Status").Rows(0).Item("key_value")), "|")
                mstrMBoardSrNo = arr(0)
                mstrMachineName = arr(1)
                mintImportStatus = CInt(arr(2))
            Else
                mstrMBoardSrNo = GetMotherBoardSrNo()
                mstrMachineName = My.Computer.Name
                mintImportStatus = 1

                '*** Insert status if not exist: Motherboard Serial Number|Machine Name|0/1

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'StrQ = "IF NOT EXISTS (SELECT * FROM " & Company._Object._ConfigDatabaseName & "..cfconfiguration WHERE key_name='ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & ") " & _
                '            "INSERT INTO " & Company._Object._ConfigDatabaseName & "..cfconfiguration ( key_name, key_value, companyunkid ) VALUES  ('ApplicantImportPackageStatus','" & mstrMBoardSrNo & "|" & mstrMachineName & "|" & mintImportStatus & "', " & mintCompID & " ) "
                StrQ = "IF NOT EXISTS (SELECT * FROM " & strConfigDatabaseName & "..cfconfiguration WHERE key_name='ApplicantImportPackageStatus' AND companyunkid = " & mintCompID & ") " & _
                            "INSERT INTO " & strConfigDatabaseName & "..cfconfiguration ( key_name, key_value, companyunkid ) VALUES  ('ApplicantImportPackageStatus','" & mstrMBoardSrNo & "|" & mstrMachineName & "|" & mintImportStatus & "', " & mintCompID & " ) "
                'Shani(24-Aug-2015) -- End


                objDataOperation.ExecNonQuery(StrQ)
                If objDataOperation.ErrorMessage <> "" Then
                    exForce = New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                    Throw exForce
                End If
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApplicantImportApplicantStatus", mstrModuleName)
        End Try
    End Function


    'Shani(24-Aug-2015) -- Start
    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    'Private Function InsertATLog(ByVal strApplicantIDs As String, ByVal blnErrorOccured As Boolean, ByVal StartTime As DateTime) As Boolean
    Private Function InsertATLog(ByVal strApplicantIDs As String, _
                                 ByVal blnErrorOccured As Boolean, _
                                 ByVal StartTime As DateTime, _
                                 ByVal intUserunkid As Integer, _
                                 ByVal dtCurrentDateAndTime As DateTime) As Boolean
        'Shani(24-Aug-2015) -- End

        Dim objDataOperation As New clsDataOperation
        Dim exForce As Exception
        Dim dsList As DataSet
        Dim StrQ As String


        Try
            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@applicantids", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strApplicantIDs.ToString)
            objDataOperation.AddParameter("@motherboardsrno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMBoardSrNo.ToString)
            objDataOperation.AddParameter("@issucceeded", SqlDbType.Bit, eZeeDataType.BIT_SIZE, Not blnErrorOccured)

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, User._Object._Userunkid)
            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUserunkid)
            'Shani(24-Aug-2015) -- End

            objDataOperation.AddParameter("@auditstarttime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, StartTime)
            If blnErrorOccured = False Then

                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'objDataOperation.AddParameter("@auditendtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, ConfigParameter._Object._CurrentDateAndTime)
                objDataOperation.AddParameter("@auditendtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, dtCurrentDateAndTime)
                'Shani(24-Aug-2015) -- End

            Else
                objDataOperation.AddParameter("@auditendtime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            End If
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, getIP.ToString)
            objDataOperation.AddParameter("@machine_name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMachineName.ToString)



            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            'StrQ = "INSERT INTO atapplicant_import ( " & _
            '              "  applicantids " & _
            '              ", motherboardsrno " & _
            '              ", issucceeded " & _
            '              ", audituserunkid " & _
            '              ", auditstarttime " & _
            '              ", auditendtime " & _
            '              ", ip " & _
            '              ", machine_name" & _
            '        ") VALUES (" & _
            '              "  @applicantids " & _
            '              ", @motherboardsrno " & _
            '              ", @issucceeded " & _
            '              ", @audituserunkid " & _
            '              ", @auditstarttime " & _
            '              ", @auditendtime " & _
            '              ", @ip " & _
            '              ", @machine_name" & _
            '        "); SELECT @@identity"

            StrQ = "INSERT INTO atapplicant_import ( " & _
                          "  applicantids " & _
                          ", motherboardsrno " & _
                          ", issucceeded " & _
                          ", audituserunkid " & _
                          ", auditstarttime " & _
                          ", auditendtime " & _
                          ", ip " & _
                          ", machine_name" & _
                        ", form_name " & _
                        ", module_name1 " & _
                        ", module_name2 " & _
                        ", module_name3 " & _
                        ", module_name4 " & _
                        ", module_name5 " & _
                        ", isweb " & _
                    ") VALUES (" & _
                          "  @applicantids " & _
                          ", @motherboardsrno " & _
                          ", @issucceeded " & _
                          ", @audituserunkid " & _
                          ", @auditstarttime " & _
                          ", @auditendtime " & _
                          ", @ip " & _
                          ", @machine_name" & _
                        ", @form_name " & _
                        ", @module_name1 " & _
                        ", @module_name2 " & _
                        ", @module_name3 " & _
                        ", @module_name4 " & _
                        ", @module_name5 " & _
                        ", @isweb " & _
                    "); SELECT @@identity"

            'S.SANDEEP [ 19 JULY 2012 ] -- END


            'S.SANDEEP [ 19 JULY 2012 ] -- START
            'Enhancement : TRA Changes

            If mstrWebFormName.Trim.Length <= 0 Then
                'S.SANDEEP [ 11 AUG 2012 ] -- START
                'ENHANCEMENT : TRA CHANGES
                'Dim frm As Form
                'For Each frm In Application.OpenForms
                '    If Form.ActiveForm.Name IsNot Nothing AndAlso Form.ActiveForm.Name = frm.Name Then
                '        objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, frm.Name)
                '        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                '        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                '    End If
                'Next
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrForm_Name)
                        objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, False)
                        objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, StrModuleName1)
                'S.SANDEEP [ 11 AUG 2012 ] -- END
            Else
                objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrWebFormName)
                objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
                objDataOperation.AddParameter("@module_name1", SqlDbType.NVarChar, 500, Language.getMessage(mstrModuleName, 12, "WEB"))
            End If
            objDataOperation.AddParameter("@module_name2", SqlDbType.NVarChar, 500, StrModuleName2)
            objDataOperation.AddParameter("@module_name3", SqlDbType.NVarChar, 500, StrModuleName3)
            objDataOperation.AddParameter("@module_name4", SqlDbType.NVarChar, 500, StrModuleName4)
            objDataOperation.AddParameter("@module_name5", SqlDbType.NVarChar, 500, StrModuleName5)

            'S.SANDEEP [ 19 JULY 2012 ] -- END

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            ' mintAtapplicantimportunkid = CInt(dsList.Tables(0).Rows(0).Item(0))
            Return True
        Catch ex As Exception
            'Sohail (15 Oct 2019) -- Start
            'DisplayError.Show("-1", ex.Message, "InsertATLog", mstrModuleName)
            Throw New Exception(ex.Message & "; Procedure Name: InsertATLog; Module Name: " & mstrModuleName)
            'Sohail (15 Oct 2019) -- Start
        End Try
    End Function
    'Sohail (26 Aug 2011) -- End

    'Sohail (23 Dec 2011) -- Start
    'Sohail (05 May 2012) -- Start
    'TRA - ENHANCEMENT    :   Now one applicant can apply for multiple vacancies and vacancy maping is created to save multiple vacancied for an applicant.
    'Sohail (05 Dec 2016) -- Start
    'Enhancement - 64.1 - Online Recruitment Redesign changes for tra security issues.
    'Public Function isExistForWebImport(ByVal strRefNo As String, ByRef intExistingApplicantId As Integer, ByRef strApplicantCode As String, ByRef strFirstname As String, ByRef strLastname As String) As Boolean
    '    'Public Function isExistForWebImport(ByVal strRefNo As String, ByVal intVacancyUnkid As Integer, ByRef intExistingApplicantId As Integer, ByRef strApplicantCode As String, ByRef strFirstname As String, ByRef strLastname As String) As Boolean
    '    'Sohail (05 May 2012) -- End
    '    Dim dsList As DataSet = Nothing
    '    Dim strQ As String = ""
    '    Dim exForce As Exception

    '    Dim objDataOperation As New clsDataOperation

    '    Try
    '        strQ = "SELECT " & _
    '          "  applicantunkid " & _
    '          ", applicant_code " & _
    '          ", titleunkid " & _
    '          ", firstname " & _
    '          ", surname " & _
    '          ", othername " & _
    '          ", gender " & _
    '          ", email " & _
    '          ", present_address1 " & _
    '          ", present_address2 " & _
    '          ", present_countryunkid " & _
    '          ", present_stateunkid " & _
    '          ", present_province " & _
    '          ", present_post_townunkid " & _
    '          ", present_zipcode " & _
    '          ", present_road " & _
    '          ", present_estate " & _
    '          ", present_plotno " & _
    '          ", present_mobileno " & _
    '          ", present_alternateno " & _
    '          ", present_tel_no " & _
    '          ", present_fax " & _
    '          ", perm_address1 " & _
    '          ", perm_address2 " & _
    '          ", perm_countryunkid " & _
    '          ", perm_stateunkid " & _
    '          ", perm_province " & _
    '          ", perm_post_townunkid " & _
    '          ", perm_zipcode " & _
    '          ", perm_road " & _
    '          ", perm_estate " & _
    '          ", perm_plotno " & _
    '          ", perm_mobileno " & _
    '          ", perm_alternateno " & _
    '          ", perm_tel_no " & _
    '          ", perm_fax " & _
    '          ", birth_date " & _
    '          ", marital_statusunkid " & _
    '          ", anniversary_date " & _
    '          ", language1unkid " & _
    '          ", language2unkid " & _
    '          ", language3unkid " & _
    '          ", language4unkid " & _
    '          ", nationality " & _
    '          ", userunkid " & _
    '          ", vacancyunkid " & _
    '         "FROM rcapplicant_master " & _
    '         "WHERE referenceno = @referenceno "

    '        ' "AND vacancyunkid = @vacancyunkid " 'Sohail (05 May 2012)

    '        objDataOperation.AddParameter("@referenceno", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strRefNo)
    '        'objDataOperation.AddParameter("@vacancyunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intVacancyUnkid) 'Sohail (05 May 2012)

    '        dsList = objDataOperation.ExecQuery(strQ, "List")

    '        If objDataOperation.ErrorMessage <> "" Then
    '            exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
    '            Throw exForce
    '        End If

    '        If dsList.Tables(0).Rows.Count > 0 Then
    '            intExistingApplicantId = CInt(dsList.Tables(0).Rows(0).Item("applicantunkid"))
    '            strApplicantCode = dsList.Tables(0).Rows(0).Item("applicant_code")
    '            strFirstname = dsList.Tables(0).Rows(0).Item("firstname")
    '            strLastname = dsList.Tables(0).Rows(0).Item("surname")
    '        Else
    '            intExistingApplicantId = 0
    '            strApplicantCode = ""
    '            strFirstname = ""
    '            strLastname = ""
    '        End If

    '        Return dsList.Tables(0).Rows.Count > 0
    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: isExistForWebImport; Module Name: " & mstrModuleName)
    '    Finally
    '        exForce = Nothing
    '        If dsList IsNot Nothing Then dsList.Dispose()
    '        objDataOperation = Nothing
    '    End Try
    'End Function
    Public Function isExistForWebImport(ByVal strEmail As String, ByRef intExistingApplicantId As Integer, ByRef strApplicantCode As String, ByRef strFirstname As String, ByRef strLastname As String) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT TOP 1 " & _
              "  applicantunkid " & _
              ", applicant_code " & _
              ", titleunkid " & _
              ", firstname " & _
              ", surname " & _
              ", othername " & _
              ", gender " & _
              ", email " & _
              ", present_address1 " & _
              ", present_address2 " & _
              ", present_countryunkid " & _
              ", present_stateunkid " & _
              ", present_province " & _
              ", present_post_townunkid " & _
              ", present_zipcode " & _
              ", present_road " & _
              ", present_estate " & _
              ", present_plotno " & _
              ", present_mobileno " & _
              ", present_alternateno " & _
              ", present_tel_no " & _
              ", present_fax " & _
              ", perm_address1 " & _
              ", perm_address2 " & _
              ", perm_countryunkid " & _
              ", perm_stateunkid " & _
              ", perm_province " & _
              ", perm_post_townunkid " & _
              ", perm_zipcode " & _
              ", perm_road " & _
              ", perm_estate " & _
              ", perm_plotno " & _
              ", perm_mobileno " & _
              ", perm_alternateno " & _
              ", perm_tel_no " & _
              ", perm_fax " & _
              ", birth_date " & _
              ", marital_statusunkid " & _
              ", anniversary_date " & _
              ", language1unkid " & _
              ", language2unkid " & _
              ", language3unkid " & _
              ", language4unkid " & _
              ", nationality " & _
              ", userunkid " & _
              ", vacancyunkid " & _
              ", ISNULL(memberships,'') AS memberships  " & _
              ", ISNULL(achievements,'') AS achievements  " & _
              ", ISNULL(journalsresearchpapers,'') AS journalsresearchpapers  " & _
              ", ISNULL(rcapplicant_master.mother_tongue,'') AS mother_tongue " & _
              ", rcapplicant_master.isimpaired " & _
              ", ISNULL(rcapplicant_master.impairment,'') AS impairment " & _
              ", ISNULL(rcapplicant_master.current_salary,0) AS current_salary " & _
              ", ISNULL(rcapplicant_master.expected_salary,0) AS expected_salary " & _
              ", ISNULL(rcapplicant_master.expected_benefits,'') AS expected_benefits " & _
              ", rcapplicant_master.willing_to_relocate " & _
              ", rcapplicant_master.willing_to_travel " & _
              ", ISNULL(rcapplicant_master.notice_period_days,0) AS notice_period_days " & _
             "FROM rcapplicant_master " & _
             "WHERE rcapplicant_master.isvoid = 0 AND email = @email " & _
             "ORDER BY applicantunkid DESC "
            'Nilay (13 Apr 2017) -- [mother_tongue, isimpaired, impairment, current_salary, expected_salary, expected_benefits, willing_to_relocate, willing_to_travel, notice_period_days]

            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmail)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intExistingApplicantId = CInt(dsList.Tables(0).Rows(0).Item("applicantunkid"))
                strApplicantCode = dsList.Tables(0).Rows(0).Item("applicant_code")
                strFirstname = dsList.Tables(0).Rows(0).Item("firstname")
                strLastname = dsList.Tables(0).Rows(0).Item("surname")
            Else
                intExistingApplicantId = 0
                strApplicantCode = ""
                strFirstname = ""
                strLastname = ""
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistForWebImport; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function
    'Sohail (05 Dec 2016) -- End
    'Sohail (23 Dec 2011) -- End

    'Pinkal (12-Oct-2011) -- Start
    'Enhancement : TRA Changes

    Public Function GetShortListedApplicant() As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Try
            strQ = " SELECT " & _
                      " applicant_code " & _
                      ",firstname " & _
                      ",othername " & _
                      " ,surname " & _
                      " FROM rcapplicant_master " & _
                      " WHERE rcapplicant_master.isvoid = 0 "


            '                      "LEFT JOIN rcapplicantqualification_tran ON rcapplicant_master.applicantunkid = rcapplicantqualification_tran.applicantunkid "
            'LEFT JOIN dbo.rcapplicantskill_tran ON rcapplicantqualification_tran.applicantunkid = rcapplicantskill_tran.applicantunkid
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetShortListedApplicant", mstrModuleName)
        End Try
        Return dsList
    End Function

    'Pinkal (12-Oct-2011) -- End

    'Sohail (26 May 2017) -- Start
    'Issue - 67.1 - applicants details gets removed if he changes after data is imported.
    Public Function isExistServerNullSkillUnkId(ByVal intApplicantUnkId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT TOP 1 1 " & _
                    "FROM rcapplicantskill_tran " & _
                    "WHERE isvoid = 0 " & _
                        "AND serverskilltranunkid IS NULL "

            If intApplicantUnkId > 0 Then
                strQ &= " AND applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistServerNullSkillUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExistServerNullQualificationUnkId(ByVal intApplicantUnkId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT TOP 1 1 " & _
                    "FROM rcapplicantqualification_tran " & _
                    "WHERE isvoid = 0 " & _
                        "AND serverqualificationtranunkid IS NULL "

            If intApplicantUnkId > 0 Then
                strQ &= " AND applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistServerNullQualificationUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExistServerNullJobHistoryUnkId(ByVal intApplicantUnkId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT TOP 1 1 " & _
                    "FROM rcjobhistory " & _
                    "WHERE isvoid = 0 " & _
                        "AND serverjobhistorytranunkid IS NULL "

            If intApplicantUnkId > 0 Then
                strQ &= " AND applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistForWebImport; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExistServerNullReferenceUnkId(ByVal intApplicantUnkId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT TOP 1 1 " & _
                    "FROM rcapp_reference_tran " & _
                    "WHERE ISNULL(isvoid, 0) = 0 " & _
                        "AND serverreferencetranunkid IS NULL "

            If intApplicantUnkId > 0 Then
                strQ &= " AND applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistServerNullReferenceUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExistServerNullAttachmentUnkId(ByVal intApplicantUnkId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT TOP 1 1 " & _
                    "FROM rcattachfiletran " & _
                    "WHERE isvoid = 0 " & _
                        "AND serverattachfiletranunkid IS NULL "

            If intApplicantUnkId > 0 Then
                strQ &= " AND applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistServerNullAttachmentUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExistServerSkillUnkId(ByVal intApplicantUnkId As Integer, ByVal intServerskilltranunkid As Integer, ByVal blnOnlyActive As Boolean) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = 0

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT skilltranunkid " & _
                    "FROM rcapplicantskill_tran " & _
                    "WHERE 1 = 1 "

            If blnOnlyActive = True Then
                strQ &= " AND isvoid = 0 "
            End If

            If intApplicantUnkId > 0 Then
                strQ &= " AND applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            If intServerskilltranunkid > 0 Then
                strQ &= " AND serverskilltranunkid = @serverskilltranunkid "
                objDataOperation.AddParameter("@serverskilltranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intServerskilltranunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0).Item("skilltranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistServerSkillUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return intUnkId
    End Function

    Public Function isExistServerQualificationUnkId(ByVal intApplicantUnkId As Integer, ByVal intServerqualificationtranunkid As Integer, ByVal blnOnlyActive As Boolean) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = 0

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT qualificationtranunkid " & _
                    "FROM rcapplicantqualification_tran " & _
                    "WHERE 1 = 1 "

            If blnOnlyActive = True Then
                strQ &= " AND isvoid = 0 "
            End If

            If intApplicantUnkId > 0 Then
                strQ &= " AND applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            If intServerqualificationtranunkid > 0 Then
                strQ &= " AND serverqualificationtranunkid = @serverqualificationtranunkid "
                objDataOperation.AddParameter("@serverqualificationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intServerqualificationtranunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0).Item("qualificationtranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistServerQualificationUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return intUnkId
    End Function

    Public Function isExistServerJobHistoryUnkId(ByVal intApplicantUnkId As Integer, ByVal intServerjobhistorytranunkid As Integer, ByVal blnOnlyActive As Boolean) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = 0

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT jobhistorytranunkid " & _
                    "FROM rcjobhistory " & _
                    "WHERE 1 = 1 "

            If blnOnlyActive = True Then
                strQ &= " AND isvoid = 0 "
            End If

            If intApplicantUnkId > 0 Then
                strQ &= " AND applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            If intServerjobhistorytranunkid > 0 Then
                strQ &= " AND serverjobhistorytranunkid = @serverjobhistorytranunkid "
                objDataOperation.AddParameter("@serverjobhistorytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intServerjobhistorytranunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0).Item("jobhistorytranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistServerJobHistoryUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return intUnkId
    End Function

    Public Function isExistServerReferenceUnkId(ByVal intApplicantUnkId As Integer, ByVal intServerreferencetranunkid As Integer, ByVal blnOnlyActive As Boolean) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = 0

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT referencetranunkid " & _
                    "FROM rcapp_reference_tran " & _
                    "WHERE 1 = 1 "

            If blnOnlyActive = True Then
                strQ &= " AND ISNULL(isvoid, 0) = 0 "
            End If

            If intApplicantUnkId > 0 Then
                strQ &= " AND applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            If intServerreferencetranunkid > 0 Then
                strQ &= " AND serverreferencetranunkid = @serverreferencetranunkid "
                objDataOperation.AddParameter("@serverreferencetranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intServerreferencetranunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0).Item("referencetranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistServerReferenceUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return intUnkId
    End Function

    Public Function isExistServerAttachUnkId(ByVal intApplicantUnkId As Integer, ByVal intServerattachfiletranunkid As Integer, ByVal blnOnlyActive As Boolean) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = 0

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT attachfiletranunkid " & _
                    "FROM rcattachfiletran " & _
                    "WHERE 1 = 1 "

            If blnOnlyActive = True Then
                strQ &= " AND isvoid = 0 "
            End If

            If intApplicantUnkId > 0 Then
                strQ &= " AND applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            If intServerattachfiletranunkid > 0 Then
                strQ &= " AND serverattachfiletranunkid = @serverattachfiletranunkid "
                objDataOperation.AddParameter("@serverattachfiletranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intServerattachfiletranunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0).Item("attachfiletranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistServerAttachUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return intUnkId
    End Function
    'Sohail (26 May 2017) -- End

    'Sohail (09 Oct 2018) -- Start
    'TANAPA - Support Issue Id # 2502 - linking recruitment server app_vacancy with aruti desktop app_vacancy in 75.1.
    Public Function isExistServerNullAppVacancyUnkId(ByVal intApplicantUnkId As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT TOP 1 1 " & _
                    "FROM rcapp_vacancy_mapping " & _
                    "WHERE ISNULL(isvoid, 0) = 0 " & _
                        "AND serverappvacancytranunkid IS NULL "

            If intApplicantUnkId > 0 Then
                strQ &= " AND applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistServerNullAppVacancyUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
    End Function

    Public Function isExistServerAppVacancyUnkId(ByVal intApplicantUnkId As Integer, ByVal intServerappvacancytranunkid As Integer, ByVal blnOnlyActive As Boolean) As Integer
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim intUnkId As Integer = 0

        Dim objDataOperation As New clsDataOperation

        Try
            strQ = "SELECT appvacancytranunkid " & _
                    "FROM rcapp_vacancy_mapping " & _
                    "WHERE 1 = 1 "

            If blnOnlyActive = True Then
                strQ &= " AND isvoid = 0 "
            End If

            If intApplicantUnkId > 0 Then
                strQ &= " AND applicantunkid = @applicantunkid "
                objDataOperation.AddParameter("@applicantunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intApplicantUnkId)
            End If

            If intServerappvacancytranunkid > 0 Then
                strQ &= " AND serverappvacancytranunkid = @serverappvacancytranunkid "
                objDataOperation.AddParameter("@serverappvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intServerappvacancytranunkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intUnkId = CInt(dsList.Tables(0).Rows(0).Item("appvacancytranunkid"))
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExistServerAppVacancyUnkId; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return intUnkId
    End Function
    'Sohail (09 Oct 2018) -- End


    'Varsha (03 Jan 2018) -- Start
    'Enhancement: Ref. #130 - On Applicant CV Report, allow user to apply filter based on a position. 
    'This filter should allow user to report on all cv's shortlisted for that particular position. 
    'Also on that CV report, change to the report design to match what the applicant sees on the application link after applying for a position.
    Public Function GetApplicantFromVacancy(ByVal xVacancyID As Integer, Optional ByVal strListName As String = "List", Optional ByVal mblFlag As Boolean = False _
                                            , Optional ByVal mblnOnlyShortListApplicant As Boolean = False, Optional ByVal mblnOnlyFinalShortListApplicant As Boolean = False) As DataSet
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim objDataOperation As New clsDataOperation
        Try

            If mblFlag = True Then
                strQ = "SELECT 0 as applicantunkid, ' ' as applicantcode,  ' ' +  @name  as Applicant UNION "
            End If

            strQ &= " SELECT DISTINCT " & _
                      "    rcapplicant_master.applicantunkid " & _
                      ",   ISNULL(applicant_code,'') AS applicantcode " & _
                      ",   ISNULL(rcapplicant_master.firstname,'') + ' ' + ISNULL(rcapplicant_master.othername,'') + ' ' + ISNULL(rcapplicant_master.surname,'') AS Applicant " & _
                      " FROM rcapplicant_master " & _
                      " LEFT JOIN ( " & _
                      "                 SELECT " & _
                      "                      CVac.applicantunkid " & _
                      "                     ,CVac.vacancyunkid " & _
                      "                     ,cfcommon_master.name AS vacancy " & _
                      "                     ,CONVERT(CHAR(8), openingdate, 112) AS ODate " & _
                      "                     ,CONVERT(CHAR(8), closingdate, 112) AS CDate " & _
                      "               FROM (SELECT " & _
                      "                     applicantunkid " & _
                      "                     ,vacancyunkid " & _
                      "                     ,ROW_NUMBER() OVER (PARTITION BY applicantunkid ORDER BY appvacancytranunkid DESC) AS Srno " & _
                      "                      FROM rcapp_vacancy_mapping WHERE ISNULL(isvoid, 0) = 0 "
            'Sohail (09 Oct 2018) - [isactive = 1]=[]

            If xVacancyID > 0 Then
                strQ &= "  AND rcapp_vacancy_mapping.vacancyunkid  = @VacancyId "
            End If

            strQ &= " ) AS CVac " & _
                      "  JOIN rcvacancy_master ON CVac.vacancyunkid = rcvacancy_master.vacancyunkid " & _
                      "  JOIN cfcommon_master ON cfcommon_master.masterunkid = rcvacancy_master.vacancytitle AND cfcommon_master.mastertype = " & clsCommon_Master.enCommonMaster.VACANCY_MASTER & " " & _
                      "  WHERE CVac.Srno = 1 AND rcvacancy_master.isvoid = 0 "

            If xVacancyID > 0 Then
                strQ &= "  AND rcvacancy_master.vacancyunkid  = @VacancyId "
            End If

            strQ &= " ) AS Vac ON rcapplicant_master.applicantunkid = Vac.applicantunkid "


            If mblnOnlyShortListApplicant OrElse mblnOnlyFinalShortListApplicant Then
                strQ &= " LEFT JOIN rcshortlist_master ON rcshortlist_master.vacancyunkid = Vac.vacancyunkid " & _
                            " LEFT JOIN rcshortlist_finalapplicant ON rcshortlist_finalapplicant.shortlistunkid = rcshortlist_master.shortlistunkid AND rcapplicant_master.applicantunkid = rcshortlist_finalapplicant.applicantunkid "

                If mblnOnlyShortListApplicant Then
                    strQ &= " AND rcshortlist_finalapplicant.isshortlisted = 1 AND rcshortlist_finalapplicant.isfinalshortlisted = 0 "
                ElseIf mblnOnlyFinalShortListApplicant Then
                    strQ &= " AND rcshortlist_finalapplicant.isshortlisted = 1 AND rcshortlist_finalapplicant.isfinalshortlisted = 1 "
                End If

            End If

            'Sohail (25 Sep 2020) -- Start
            'NMB Enhancement : OLD-76 #  : Process of linking employee with an applicant, bind vacancy to employee and send notification to reporting to on applying vacancy.
            strQ &= " WHERE rcapplicant_master.isvoid = 0 "
            'Sohail (25 Sep 2020) -- End

            If xVacancyID > 0 Then
                strQ &= "  AND Vac.vacancyunkid = @VacancyId  "
            End If

            If mblnOnlyShortListApplicant OrElse mblnOnlyFinalShortListApplicant Then
                strQ &= " AND rcshortlist_master.isvoid = 0 AND rcshortlist_finalapplicant.isvoid = 0 "
            End If

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@name", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, Language.getMessage(mstrModuleName, 20, "Select"))
            objDataOperation.AddParameter("@VacancyId", SqlDbType.Int, eZeeDataType.INT_SIZE, xVacancyID)
            dsList = objDataOperation.ExecQuery(strQ, strListName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicantFromVacancy; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            objDataOperation = Nothing
        End Try
        Return dsList
    End Function
    'Varsha (03 Jan 2018) -- End

    'Sohail (25 Sep 2020) -- Start
    'NMB Enhancement : OLD-75 #  : Search job page with apply button in ESS (same as MSS, only current open vacancies)..
    Public Function GetApplicantIDByEmployeeID(ByVal intEmployeeUnkId As Integer) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim intRet_ApplicantUnkID As Integer = 0

        Dim objDataOperation As New clsDataOperation

        Try
            StrQ = "SELECT rcapplicant_master.applicantunkid " & _
                    "FROM rcapplicant_master " & _
                    "WHERE rcapplicant_master.isvoid = 0 " & _
                          "AND rcapplicant_master.UserId IS NOT NULL " & _
                          "AND rcapplicant_master.employeeunkid = @employeeunkid "

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRet_ApplicantUnkID = dsList.Tables(0).Rows(0)(0)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicantIDByEmployeeID; Module Name: " & mstrModuleName)
        End Try
        Return intRet_ApplicantUnkID
    End Function

    Public Function InsertUpdateApplicantByEmployeeID(ByVal intEmployeeUnkId As Integer _
                                                      , ByVal strEmail As String _
                                                      , ByVal intBaseCountryUnkID As Integer _
                                                      , ByVal dtAsOnDate As Date _
                                                      , ByVal intCompanyId As Integer _
                                                      , ByVal intApplicantCodeNotype As Integer _
                                                      , ByVal strApplicantCodePrifix As String _
                                                      , ByRef intRet_ApplicantUnkID As Integer _
                                                      ) As Boolean
        'Sohail (04 Nov 2020) - [intCompanyId, intApplicantCodeNotype, strApplicantCodePrifix]
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        intRet_ApplicantUnkID = 0

        Dim objDataOperation As New clsDataOperation
        'Hemant (03 Feb 2023) -- Start
        'ISSUE : Error Throw - "Value was either too large or too small for an Int32". in aruti configuration
        objDataOperation.BindTransaction()
        'Hemant (03 Feb 2023) -- End  

        Try

            StrQ = "DECLARE @TranName VARCHAR " & _
                    "DECLARE @mApplicantID INT " & _
                    "SET @mApplicantID = 0 " & _
                    "DECLARE @applicantexist BIT " & _
                    "SET @applicantexist = 0 " & _
                    "SET NOCOUNT ON " & _
                    " " & _
                    "SET @TranName = 'MyTran' " & _
                    "BEGIN TRAN @TranName " & _
                    " " & _
                    "SELECT @mApplicantID = rcapplicant_master.applicantunkid " & _
                        "FROM rcapplicant_master " & _
                        "WHERE rcapplicant_master.isvoid = 0 " & _
                              "AND rcapplicant_master.UserId IS NOT NULL " & _
                              "AND rcapplicant_master.employeeunkid = @employeeunkid " & _
                    " " & _
                    "IF @mApplicantID > 0 " & _
                        "SET @applicantexist = 1 " & _
                    "ELSE " & _
                        "BEGIN " & _
                            "SELECT @mApplicantID = rcapplicant_master.applicantunkid " & _
                            "FROM rcapplicant_master " & _
                            "WHERE rcapplicant_master.isvoid = 0 " & _
                                    "AND rcapplicant_master.UserId IS NOT NULL " & _
                                    "AND rcapplicant_master.email = @email " & _
                            "IF @mApplicantID > 0 " & _
                                "SET @applicantexist = 1 " & _
                            "ELSE " & _
                                "BEGIN " & _
                                "INSERT INTO rcapplicant_master " & _
                                    "(applicant_code " & _
                                  ", titleunkid " & _
                                  ", firstname " & _
                                  ", surname " & _
                                  ", othername " & _
                                  ", gender " & _
                                  ", email " & _
                                  ", present_address1 " & _
                                  ", present_address2 " & _
                                  ", present_countryunkid " & _
                                  ", present_stateunkid " & _
                                  ", present_province " & _
                                  ", present_post_townunkid " & _
                                  ", present_zipcode " & _
                                  ", present_road " & _
                                  ", present_estate " & _
                                  ", present_plotno " & _
                                  ", present_mobileno " & _
                                  ", present_alternateno " & _
                                  ", present_tel_no " & _
                                  ", present_fax " & _
                                  ", perm_address1 " & _
                                  ", perm_address2 " & _
                                  ", perm_countryunkid " & _
                                  ", perm_stateunkid " & _
                                  ", perm_province " & _
                                  ", perm_post_townunkid " & _
                                  ", perm_zipcode " & _
                                  ", perm_road " & _
                                  ", perm_estate " & _
                                  ", perm_plotno " & _
                                  ", perm_mobileno " & _
                                  ", perm_alternateno " & _
                                  ", perm_tel_no " & _
                                  ", perm_fax " & _
                                  ", birth_date " & _
                                  ", marital_statusunkid " & _
                                  ", anniversary_date " & _
                                  ", language1unkid " & _
                                  ", language2unkid " & _
                                  ", language3unkid " & _
                                  ", language4unkid " & _
                                  ", nationality " & _
                                  ", userunkid " & _
                                  ", vacancyunkid " & _
                                  ", isimport " & _
                                  ", other_skills " & _
                                  ", other_qualification " & _
                                  ", employeecode " & _
                                  ", referenceno " & _
                                  ", memberships " & _
                                  ", achievements " & _
                                  ", journalsresearchpapers " & _
                                  ", UserId " & _
                                  ", mother_tongue " & _
                                  ", isimpaired " & _
                                  ", impairment " & _
                                  ", current_salary " & _
                                  ", expected_salary " & _
                                  ", expected_benefits " & _
                                  ", willing_to_relocate " & _
                                  ", willing_to_travel " & _
                                  ", notice_period_days " & _
                                  ", current_salary_currency_id " & _
                                  ", expected_salary_currency_id " & _
                                  ", employeeunkid " & _
                                  ", loginemployeeunkid " & _
                                  ", isvoid " & _
                                  ", voiduserunkid " & _
                                  ", voiddatetime " & _
                                  ", voidreason " & _
                                    ") " & _
                            "SELECT TOP 1 " & _
                                    "NEWID() " & _
                                  ", titleunkid " & _
                                  ", firstname " & _
                                  ", surname " & _
                                  ", othername " & _
                                  ", gender " & _
                                  ", email " & _
                                  ", present_address1 " & _
                                  ", present_address2 " & _
                                  ", present_countryunkid " & _
                                  ", present_stateunkid " & _
                                  ", hremployee_master.present_provinceunkid " & _
                                  ", present_post_townunkid " & _
                                  ", hremployee_master.present_postcodeunkid " & _
                                  ", present_road " & _
                                  ", present_estate " & _
                                  ", present_plotno " & _
                                  ", hremployee_master.present_mobile " & _
                                  ", present_alternateno " & _
                                  ", present_tel_no " & _
                                  ", present_fax " & _
                                  ", domicile_address1 " & _
                                  ", domicile_address2 " & _
                                  ", domicile_countryunkid " & _
                                  ", domicile_stateunkid " & _
                                  ", domicile_provinceunkid " & _
                                  ", domicile_post_townunkid " & _
                                  ", domicile_postcodeunkid " & _
                                  ", domicile_road " & _
                                  ", domicile_estate " & _
                                  ", domicile_plotno " & _
                                  ", domicile_mobile " & _
                                  ", domicile_alternateno " & _
                                  ", domicile_tel_no " & _
                                  ", domicile_fax " & _
                                  ", birthdate " & _
                                  ", maritalstatusunkid " & _
                                  ", anniversary_date " & _
                                  ", language1unkid " & _
                                  ", language2unkid " & _
                                  ", language3unkid " & _
                                  ", language4unkid " & _
                                  ", nationalityunkid " & _
                                  ", 1 " & _
                                  ", 0 " & _
                                  ", 0 " & _
                                  ", '' " & _
                                  ", '' " & _
                                  ", employeecode " & _
                                  ", '' " & _
                                  ", '' " & _
                                  ", '' " & _
                                  ", '' " & _
                                  ", NEWID() " & _
                                  ", '' " & _
                                  ", 0 " & _
                                  ", '' " & _
                                  ", EGRD.scale " & _
                                  ", EGRD.scale " & _
                                  ", '' " & _
                                  ", 1 " & _
                                  ", 1 " & _
                                  ", 30 " & _
                                  ", @countryunkid " & _
                                  ", @countryunkid " & _
                                  ", employeeunkid " & _
                                  ", employeeunkid " & _
                                  ", 0 " & _
                                  ", 0 " & _
                                  ", NULL " & _
                                  ", '' " & _
                                "FROM hremployee_master " & _
                                "LEFT JOIN " & _
                                  "( " & _
                                  "	SELECT " & _
                                  "     scale " & _
                                  "		,employeeunkid AS GEmpId " & _
                                  "	FROM " & _
                                  "	( " & _
                                  "		SELECT " & _
                                  "			employeeunkid " & _
                                  "         ,newscale AS scale " & _
                                  "			,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                                  "		FROM prsalaryincrement_tran " & _
                                  "		WHERE isvoid = 0 AND isapproved = 1 AND employeeunkid = @employeeunkid AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(dtAsOnDate) & "' " & _
                                  "	) AS GRD WHERE GRD.Rno = 1 " & _
                                  ") AS EGRD ON EGRD.GEmpId = hremployee_master.employeeunkid " & _
                                "WHERE hremployee_master.employeeunkid = @employeeunkid " & _
                                "SELECT @mApplicantID = @@IDENTITY " & _
                    " " & _
                                "IF @@error <> 0 " & _
                                "BEGIN " & _
                                    "ROLLBACK TRAN @TranName " & _
                                    "/*RETURN @@error*/ " & _
                                "END " & _
                        "END " & _
                        "END " & _
                        " " & _
                    "IF @applicantexist = 1 " & _
                        "BEGIN " & _
                        "UPDATE rcapplicant_master " & _
                        "SET firstname = a.firstname " & _
                          ", othername = a.othername " & _
                          ", surname = a.surname " & _
                          ", gender = a.gender " & _
                          ", present_countryunkid = A.present_countryunkid " & _
                          ", present_address1 = A.present_address1 " & _
                          ", present_address2 = A.present_address2 " & _
                          ", present_province = A.present_provinceunkid " & _
                          ", present_road = A.present_road " & _
                          ", present_estate = A.present_estate " & _
                          ", present_plotno = A.present_plotno " & _
                          ", present_mobileno = A.present_mobile " & _
                          ", present_alternateno = A.present_alternateno " & _
                          ", present_tel_no = A.present_tel_no " & _
                          ", present_fax = A.present_fax " & _
                          ", perm_countryunkid = rcapplicant_master.perm_countryunkid " & _
                          ", perm_address1 = A.domicile_address1 " & _
                          ", perm_address2 = A.domicile_address2 " & _
                          ", perm_province = A.domicile_provinceunkid " & _
                          ", perm_road = A.domicile_road " & _
                          ", perm_estate = A.domicile_estate " & _
                          ", perm_plotno = A.domicile_plotno " & _
                          ", perm_mobileno = A.domicile_mobile " & _
                          ", perm_alternateno = A.domicile_alternateno " & _
                          ", perm_tel_no = A.domicile_tel_no " & _
                          ", perm_fax = A.domicile_fax " & _
                          ", birth_date = A.birthdate " & _
                          ", anniversary_date = A.anniversary_date " & _
                          ", nationality = A.nationalityunkid " & _
                          ", isimport = 0 " & _
                          ", employeecode = A.employeecode " & _
                          "/*, applicant_code = A.employeecode*/ " & _
                          ", employeeunkid = A.employeeunkid " & _
                          ", current_salary = A.scale " & _
                          ", loginemployeeunkid = A.employeeunkid " & _
                        "FROM " & _
                        "( " & _
                            "SELECT TOP 1 " & _
                                    "employeecode " & _
                                  ", titleunkid " & _
                                  ", firstname " & _
                                  ", surname " & _
                                  ", othername " & _
                                  ", gender " & _
                                  ", email " & _
                                  ", present_address1 " & _
                                  ", present_address2 " & _
                                  ", present_countryunkid " & _
                                  ", present_stateunkid " & _
                                  ", hremployee_master.present_provinceunkid " & _
                                  ", present_post_townunkid " & _
                                  ", hremployee_master.present_postcodeunkid " & _
                                  ", present_road " & _
                                  ", present_estate " & _
                                  ", present_plotno " & _
                                  ", hremployee_master.present_mobile " & _
                                  ", present_alternateno " & _
                                  ", present_tel_no " & _
                                  ", present_fax " & _
                                  ", domicile_address1 " & _
                                  ", domicile_address2 " & _
                                  ", domicile_countryunkid " & _
                                  ", domicile_stateunkid " & _
                                  ", domicile_provinceunkid " & _
                                  ", domicile_post_townunkid " & _
                                  ", domicile_postcodeunkid " & _
                                  ", domicile_road " & _
                                  ", domicile_estate " & _
                                  ", domicile_plotno " & _
                                  ", domicile_mobile " & _
                                  ", domicile_alternateno " & _
                                  ", domicile_tel_no " & _
                                  ", domicile_fax " & _
                                  ", birthdate " & _
                                  ", maritalstatusunkid " & _
                                  ", anniversary_date " & _
                                  ", language1unkid " & _
                                  ", language2unkid " & _
                                  ", language3unkid " & _
                                  ", language4unkid " & _
                                  ", nationalityunkid " & _
                                  ", EGRD.scale " & _
                                  ", employeeunkid " & _
                                "FROM hremployee_master " & _
                                "LEFT JOIN " & _
                                  "( " & _
                                  "	SELECT " & _
                                  "     scale " & _
                                  "		,employeeunkid AS GEmpId " & _
                                  "	FROM " & _
                                  "	( " & _
                                  "		SELECT " & _
                                  "			employeeunkid " & _
                                  "         ,newscale AS scale " & _
                                  "			,ROW_NUMBER() OVER ( PARTITION BY employeeunkid ORDER BY incrementdate DESC, salaryincrementtranunkid DESC ) AS Rno " & _
                                  "		FROM prsalaryincrement_tran " & _
                                  "		WHERE isvoid = 0 AND isapproved = 1 AND employeeunkid = @employeeunkid AND CONVERT(CHAR(8), incrementdate, 112) <= '" & eZeeDate.convertDate(dtAsOnDate) & "' " & _
                                  "	) AS GRD WHERE GRD.Rno = 1 " & _
                                  ") AS EGRD ON EGRD.GEmpId = hremployee_master.employeeunkid " & _
                                "WHERE hremployee_master.employeeunkid = @employeeunkid " & _
                        ") AS A " & _
                        "WHERE rcapplicant_master.applicantunkid = @mApplicantID " & _
                        " " & _
                        "IF @@error <> 0 " & _
                        "BEGIN " & _
                            "ROLLBACK TRAN @TranName " & _
                            "/*RETURN @@error*/ " & _
                        "END " & _
                    "END " & _
                        " " & _
                        "COMMIT TRANSACTION @TranName " & _
                    " " & _
                        "SELECT  @mApplicantID AS applicantunkid, @applicantexist AS isapplicantexist "
            'Sohail (04 Nov 2020) - [isapplicantexist]

            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeUnkId)
            objDataOperation.AddParameter("@countryunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intBaseCountryUnkID)
            objDataOperation.AddParameter("@email", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, strEmail)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRet_ApplicantUnkID = dsList.Tables(0).Rows(0)(0)
                'Sohail (04 Nov 2020) -- Start
                'NMB Issue # : - Error Cannot insert duplicate key in applicant code column on import data in recruitment.
                If CBool(dsList.Tables(0).Rows(0).Item("isapplicantexist")) = False Then
                    If intApplicantCodeNotype = 1 Then
                        If Set_AutoNumber(objDataOperation, intRet_ApplicantUnkID, "rcapplicant_master", "applicant_code", "applicantunkid", "NextApplicantCodeNo", strApplicantCodePrifix, intCompanyId) = False Then
                            If objDataOperation.ErrorMessage <> "" Then
                                Throw New Exception(objDataOperation.ErrorMessage)
                            End If
                        End If
                    End If

                    clsCommonATLog._LoginEmployeeUnkid = intEmployeeUnkId
                    clsCommonATLog._WebFormName = mstrWebFormName
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", intRet_ApplicantUnkID, "", "", -1, 1, 0, ) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                Else
                    clsCommonATLog._LoginEmployeeUnkid = intEmployeeUnkId
                    clsCommonATLog._WebFormName = mstrWebFormName
                    If clsCommonATLog.Insert_TranAtLog(objDataOperation, "rcapplicant_master", "applicantunkid", intRet_ApplicantUnkID, "", "", -1, 2, 0) = False Then
                        exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                        Throw exForce
                    End If
                End If
                'Sohail (04 Nov 2020) -- End
            End If

            'Hemant (03 Feb 2023) -- Start
            'ISSUE : Error Throw - "Value was either too large or too small for an Int32". in aruti configuration
            objDataOperation.ReleaseTransaction(True)
            'Hemant (03 Feb 2023) -- End  
            Return True

        Catch ex As Exception
            'Hemant (03 Feb 2023) -- Start
            'ISSUE : Error Throw - "Value was either too large or too small for an Int32". in aruti configuration
            objDataOperation.ReleaseTransaction(False)
            'Hemant (03 Feb 2023) -- End  
            Throw New Exception(ex.Message & "; Procedure Name: InsertUpdateApplicantByEmployeeID; Module Name: " & mstrModuleName)
            Return False
        End Try

    End Function
    'Sohail (25 Sep 2020) -- End

    'Sohail (08 Oct 2018) -- Start
    'TANAPA - Support Issue Id # 2502 - Entry 2:30 Hrs less in Date Column in Tables of tran_TNP_Jul2017_Jun2018 During Import Process (Example : 1899-12-31 2:30:00 instead of 1900-01-01 0:00:000) in 75.1.
    Public Sub RemoveDateTimeZones(ByVal ds As DataSet)
        Try
            If ds IsNot Nothing Then
                For Each dt As DataTable In ds.Tables
                    For Each Col As DataColumn In dt.Columns
                        If Col.DataType Is GetType(Date) Then
                            Col.DateTimeMode = DataSetDateTime.Unspecified
                        End If
                    Next
                Next
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: RemoveDateTimeZones; Module Name: " & mstrModuleName)
        End Try
    End Sub
    'Sohail (08 Oct 2018) -- End


    'Pinkal (28-Apr-2023) -- Start
    '(A1X-865) NMB - Read aptitude test results from provided views.

    Public Function GetApplicantIDByEmail(ByVal xEmail As String) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim intRet_ApplicantUnkID As Integer = 0

        Dim objDataOperation As New clsDataOperation

        Try
            StrQ = " SELECT rcapplicant_master.applicantunkid " & _
                       " FROM rcapplicant_master " & _
                       " WHERE rcapplicant_master.isvoid = 0 " & _
                       " AND rcapplicant_master.UserId IS NOT NULL " & _
                       " AND rcapplicant_master.email = @email "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@email", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, xEmail)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRet_ApplicantUnkID = dsList.Tables(0).Rows(0)(0)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicantIDByEmail; Module Name: " & mstrModuleName)
        End Try
        Return intRet_ApplicantUnkID
    End Function

    'Pinkal (09-Sep-2023) -- Start
    'TRA Include New Screen For Importing Final ShortListing Applicant.
    Public Function GetApplicantIDByCode(ByVal xApplicantCode As String) As Integer
        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim dsList As New DataSet
        Dim intRet_ApplicantUnkID As Integer = 0

        Dim objDataOperation As New clsDataOperation

        Try
            StrQ = " SELECT rcapplicant_master.applicantunkid " & _
                       " FROM rcapplicant_master " & _
                       " WHERE rcapplicant_master.isvoid = 0 " & _
                       " AND rcapplicant_master.applicant_code = @applicant_code "

            objDataOperation.ClearParameters()
            objDataOperation.AddParameter("@applicant_code", SqlDbType.VarChar, eZeeDataType.NAME_SIZE, xApplicantCode)

            dsList = objDataOperation.ExecQuery(StrQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If dsList.Tables(0).Rows.Count > 0 Then
                intRet_ApplicantUnkID = dsList.Tables(0).Rows(0)(0)
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetApplicantIDByCode; Module Name: " & mstrModuleName)
        End Try
        Return intRet_ApplicantUnkID
    End Function


    'Pinkal (16-Oct-2023) -- Start
    '(A1X-1400) NMB - Post assigned employee assets to P2P

    Public Function ExportApplicantFeedbackToWeb(ByVal StrCode As String, _
                             ByVal intClientCode As Integer, _
                             ByVal strDatabaseServer As String, _
                             ByVal intDatabaseServerSetting As String, _
                             ByVal strAuthenticationCode As String, _
                             ByVal strConfigDatabaseName As String, _
                             ByVal strWebServiceLink As String, _
                             ByVal dsApplicantFeedBack As DataSet _
                            ) As Boolean

        Dim StrQ As String = String.Empty
        Dim exForce As Exception
        Dim mLinkDesk As String = String.Empty
        Dim objDataOp As New clsDataOperation
        Try

            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
            mLinkDesk = strConfigDatabaseName & ".."
            GintTotalApplicantToImport = 20

            Dim HttpBinding As New System.ServiceModel.BasicHttpBinding
            With HttpBinding
                .MaxBufferPoolSize = 2147483647
                .MaxReceivedMessageSize = 2147483647
                .ReaderQuotas.MaxStringContentLength = 2147483647
                .ReaderQuotas.MaxArrayLength = 2147483647
                .ReaderQuotas.MaxBytesPerRead = 2147483647
                .ReaderQuotas.MaxNameTableCharCount = 2147483647
                .OpenTimeout = New TimeSpan(0, 20, 0)
                .ReceiveTimeout = New TimeSpan(0, 20, 0)
                .SendTimeout = New TimeSpan(0, 20, 0)
                .MessageEncoding = ServiceModel.WSMessageEncoding.Text 'Sohail (11 Dec 2015) - [CCBRT Error: The content type text/html of the response message does not match the content type of the binding]
                .Security.Transport.ClientCredentialType = ServiceModel.HttpClientCredentialType.None
            End With


            Dim WebRef As New WR.ArutiRecruitmentService
            Dim UserCred As New WR.UserCredentials
            WebRef.Url = strWebServiceLink
            WebRef.Timeout = 1200000 '20 minutes 'Sohail (21 Sep 2019)
            Dim strErrorMessage As String = ""

            System.Net.ServicePointManager.Expect100Continue = False 'Sohail (25 Oct 2016) - [Issue : The remote server returned an error: (417) Expectation Failed]
            Try
                System.Net.ServicePointManager.SecurityProtocol = System.Net.ServicePointManager.SecurityProtocol Or System.Net.SecurityProtocolType.Tls Or DirectCast(240, System.Net.SecurityProtocolType) Or DirectCast(768, System.Net.SecurityProtocolType) Or DirectCast(3072, System.Net.SecurityProtocolType) 'Sohail (30 Jul 2019) - [Error : The underlying connection was closed: An unexpected error occurred on a send.]
            Catch ex As Exception

            End Try
            System.Net.ServicePointManager.ServerCertificateValidationCallback = AddressOf AcceptAllCertifications
            UserCred.Password = "CNjh9qOZUGhuqVpVp/LjZoYbJJoNgPiyC/P/vxPGfWo="
            UserCred.WebClientID = intClientCode
            UserCred.CompCode = StrCode
            UserCred.AuthenticationCode = strAuthenticationCode
            WebRef.UserCredentialsValue = UserCred

            Call RemoveDateTimeZones(dsApplicantFeedBack)

            strErrorMessage = ""
            WebRef.UploadApplicantFeedback(strErrorMessage, dsApplicantFeedBack)

            If strErrorMessage <> "" Then
                exForce = New Exception(objDataOp.ErrorNumber & " : " & strErrorMessage)
                Throw exForce
                Exit Try
            End If


            '******    - UPDATE, INSERT - END

            System.Windows.Forms.Cursor.Current = Cursors.Default
            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ExportApplicantFeedbackToWeb", mstrModuleName)
            Return False
        Finally
            StrQ = String.Empty : mLinkDesk = String.Empty
        End Try
    End Function

    'Pinkal (16-Oct-2023) -- End



    'Pinkal (09-Sep-2023) -- End

    'Pinkal (28-Apr-2023) -- End


    'Pinkal (15-Sep-2023) -- Start
    '(A1X-158) Mbeya Cement: HIK Device Integration - Automatic attendance data download [Used only for Import Web Method Not to Use any other Method]
    'Public Sub RepairImportFromWebData(ByVal dtTable As DataTable)
    '    Dim exForce As Exception
    '    Dim objDataOperation As New clsDataOperation
    '    Try

    '        If dtTable Is Nothing OrElse dtTable.Rows.Count <= 0 Then Exit Sub

    '        For Each dr As DataRow In dtTable.Rows

    '            Dim StrQ As String = ""

    '            Dim xAppVacancyTranId As Integer = CInt(dr("appvacancytranunkid"))

    '            Dim xServerAppVacancyTranId As Integer = CInt(dr("serverappvacancytranunkid"))

    '            If xServerAppVacancyTranId > 0 Then

    '                StrQ = "SELECT * FROM rcattachfiletran WHERE isvoid = 0 AND  appvacancytranunkid = @appvacancytranunkid "
    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAppVacancyTranId)
    '                Dim dsAttach As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '                Try
    '                    If objDataOperation.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                Catch ex As Exception
    '                    Continue For
    '                End Try

    '                If dsAttach IsNot Nothing AndAlso dsAttach.Tables(0).Rows.Count <= 0 Then

    '                    StrQ = "SELECT * FROM rcattachfiletran WHERE isvoid = 0 AND  appvacancytranunkid = @serverappvacancytranunkid "
    '                    objDataOperation.ClearParameters()
    '                    objDataOperation.AddParameter("@serverappvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xServerAppVacancyTranId)
    '                    Dim dsAttach1 As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '                    Try
    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If
    '                    Catch ex As Exception
    '                        Continue For
    '                    End Try

    '                    If dsAttach1 IsNot Nothing AndAlso dsAttach1.Tables(0).Rows.Count > 0 Then

    '                        StrQ = "UPDATE rcattachfiletran SET appvacancytranunkid  = @appvacancytranunkid  WHERE  isvoid = 0 AND appvacancytranunkid = @Serverappvacancytranunkid"
    '                        objDataOperation.ClearParameters()
    '                        objDataOperation.AddParameter("@Serverappvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xServerAppVacancyTranId)
    '                        objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAppVacancyTranId)
    '                        objDataOperation.ExecNonQuery(StrQ)

    '                        Try
    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If
    '                        Catch ex As Exception
    '                            Continue For
    '                        End Try

    '                    End If  'If dsAttach1 IsNot Nothing AndAlso dsAttach1.Tables(0).Rows.Count > 0 Then

    '                End If  ' If dsAttach IsNot Nothing AndAlso dsAttach.Tables(0).Rows.Count <= 0 Then


    '                StrQ = "SELECT * FROM hrdocuments_tran WHERE isactive = 1 AND  appvacancytranunkid = @appvacancytranunkid "
    '                objDataOperation.ClearParameters()
    '                objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAppVacancyTranId)
    '                Dim dsServerAppDoc As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '                Try
    '                    If objDataOperation.ErrorMessage <> "" Then
    '                        exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '                        Throw exForce
    '                    End If
    '                Catch ex As Exception
    '                    Continue For
    '                End Try

    '                If dsServerAppDoc IsNot Nothing AndAlso dsServerAppDoc.Tables(0).Rows.Count <= 0 Then

    '                    StrQ = "SELECT * FROM hrdocuments_tran WHERE  isactive = 1 AND appvacancytranunkid = @serverappvacancytranunkid "
    '                    objDataOperation.ClearParameters()
    '                    objDataOperation.AddParameter("@serverappvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xServerAppVacancyTranId)
    '                    Dim dsAppdoc As DataSet = objDataOperation.ExecQuery(StrQ, "List")

    '                    Try
    '                        If objDataOperation.ErrorMessage <> "" Then
    '                            exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '                            Throw exForce
    '                        End If
    '                    Catch ex As Exception
    '                        Continue For
    '                    End Try

    '                    If dsAppdoc IsNot Nothing AndAlso dsAppdoc.Tables(0).Rows.Count > 0 Then

    '                        StrQ = "UPDATE hrdocuments_tran SET appvacancytranunkid  = @appvacancytranunkid  WHERE  isactive = 1 AND appvacancytranunkid = @Serverappvacancytranunkid "
    '                        objDataOperation.ClearParameters()
    '                        objDataOperation.AddParameter("@Serverappvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xServerAppVacancyTranId)
    '                        objDataOperation.AddParameter("@appvacancytranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, xAppVacancyTranId)
    '                        objDataOperation.ExecNonQuery(StrQ)

    '                        Try
    '                            If objDataOperation.ErrorMessage <> "" Then
    '                                exForce = New Exception(objDataOperation.ErrorNumber & ":" & objDataOperation.ErrorMessage)
    '                                Throw exForce
    '                            End If
    '                        Catch ex As Exception
    '                            Continue For
    '                        End Try

    '                    End If  'If dsAppdoc IsNot Nothing AndAlso dsAppdoc.Tables(0).Rows.Count <= 0 Then

    '                End If  '  If dsServerAppDoc IsNot Nothing AndAlso dsServerAppDoc.Tables(0).Rows.Count <= 0 Then

    '            End If  ' If xServerAppVacancyTranId > 0 Then

    '        Next

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: RepairImportFromWebData; Module Name: " & mstrModuleName)
    '    Finally
    '        objDataOperation = Nothing
    '    End Try
    'End Sub
    'Pinkal (15-Sep-2023) -- End


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Sorry! This Process is currently running on")
			Language.setMessage(mstrModuleName, 2, "Select")
			Language.setMessage(mstrModuleName, 3, "Select")
			Language.setMessage(mstrModuleName, 4, "Sorry, you cannot delete this Applicant. Reason : This Applicant is already linked with some transaction.")
			Language.setMessage("clsEmployee_Master", 7, "Male")
			Language.setMessage("clsEmployee_Master", 8, "Female")
			Language.setMessage("clsEmployee_Master", 9, "Other")
			Language.setMessage(mstrModuleName, 12, "WEB")
			Language.setMessage(mstrModuleName, 13, "This Applicant Code already exists.Please define new Applicant Code.")
			Language.setMessage(mstrModuleName, 14, "This Email Address already exists.Please define new Email Address.")
			Language.setMessage(mstrModuleName, 15, "Internet Connection :")
			Language.setMessage(mstrModuleName, 16, "Data Import process failed.")
			Language.setMessage(mstrModuleName, 17, "Data Imported Successfully.")
			Language.setMessage(mstrModuleName, 18, "Please select Image path from configuration -> Option -> Photo Path.")
			Language.setMessage(mstrModuleName, 19, " Machine.")
            Language.setMessage(mstrModuleName, 20, "Select")
            Language.setMessage(mstrModuleName, 21, "Please select Document path from configuration -> Option -> Documents Path.")
			Language.setMessage(mstrModuleName, 22, "Your application is successfully sent to")
			Language.setMessage(mstrModuleName, 23, "Please Redo Import Data to import the rest of the applicants.")
			Language.setMessage(mstrModuleName, 8, "Data Export process failed.")
			Language.setMessage(mstrModuleName, 9, "Data Exported Successfully.")

		Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class
