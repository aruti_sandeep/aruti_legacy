﻿'************************************************************************************************************************************
'Class Name : clsEmpnondisclosure_declaration_tran.vb
'Purpose    :
'Date       :03-02-2020
'Written By :Sohail
'Modified   :
'************************************************************************************************************************************

Imports eZeeCommonLib
Imports System.Text
Imports System.Web

''' <summary>
''' Purpose: 
''' Developer: Sohail
''' </summary>
Public Class clsEmpnondisclosure_declaration_tran
    Private Shared ReadOnly mstrModuleName As String = "clsEmpnondisclosure_declaration_tran"
    Dim objDataOperation As clsDataOperation
    Dim mstrMessage As String = ""
    Private xDataOp As clsDataOperation

#Region " Private variables "
    Private mintNondisclosuredeclarationtranunkid As Integer
    Private mintEmployeeunkid As Integer
    Private mintYearunkid As Integer
    Private mdtDeclaration_Date As Date
    Private mintWitness1userunkid As Integer
    Private mdtWitness1_Date As Date
    Private mintWitness2userunkid As Integer
    Private mdtWitness2_Date As Date
    Private mblnIssubmitforapproval As Boolean
    Private mintApprovalstatusunkid As Integer
    Private mintFinalapproverunkid As Integer
    Private mintUserunkid As Integer
    Private mintLoginemployeeunkid As Integer
    Private mblnIsweb As Boolean
    Private mblnIsvoid As Boolean
    Private mintVoiduserunkid As Integer
    Private mintVoidloginemployeeunkid As Integer
    Private mdtVoiddatetime As Date
    Private mstrVoidreason As String = String.Empty
#End Region

#Region " Properties "
    ''' <summary>
    ''' Purpose: Get Message from Class 
    ''' Modify By: Sohail
    ''' </summary>
    Public ReadOnly Property _Message() As String
        Get
            Return mstrMessage
        End Get
    End Property

    Public WriteOnly Property _xDataOp() As clsDataOperation
        Set(ByVal value As clsDataOperation)
            xDataOp = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set nondisclosuredeclarationtranunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Nondisclosuredeclarationtranunkid() As Integer
        Get
            Return mintNondisclosuredeclarationtranunkid
        End Get
        Set(ByVal value As Integer)
            mintNondisclosuredeclarationtranunkid = Value
            Call getData()
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set employeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Employeeunkid() As Integer
        Get
            Return mintEmployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintEmployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set yearunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Yearunkid() As Integer
        Get
            Return mintYearunkid
        End Get
        Set(ByVal value As Integer)
            mintYearunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set declaration_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Declaration_Date() As Date
        Get
            Return mdtDeclaration_Date
        End Get
        Set(ByVal value As Date)
            mdtDeclaration_Date = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set witness1userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Witness1userunkid() As Integer
        Get
            Return mintWitness1userunkid
        End Get
        Set(ByVal value As Integer)
            mintWitness1userunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set witness1_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Witness1_Date() As Date
        Get
            Return mdtWitness1_Date
        End Get
        Set(ByVal value As Date)
            mdtWitness1_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set witness2userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Witness2userunkid() As Integer
        Get
            Return mintWitness2userunkid
        End Get
        Set(ByVal value As Integer)
            mintWitness2userunkid = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set witness2_date
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Witness2_Date() As Date
        Get
            Return mdtWitness2_Date
        End Get
        Set(ByVal value As Date)
            mdtWitness2_Date = value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set issubmitforapproval
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Issubmitforapproval() As Boolean
        Get
            Return mblnIssubmitforapproval
        End Get
        Set(ByVal value As Boolean)
            mblnIssubmitforapproval = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set approvalstatusunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Approvalstatusunkid() As Integer
        Get
            Return mintApprovalstatusunkid
        End Get
        Set(ByVal value As Integer)
            mintApprovalstatusunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set finalapproverunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Finalapproverunkid() As Integer
        Get
            Return mintFinalapproverunkid
        End Get
        Set(ByVal value As Integer)
            mintFinalapproverunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set userunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Userunkid() As Integer
        Get
            Return mintUserunkid
        End Get
        Set(ByVal value As Integer)
            mintUserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set loginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Loginemployeeunkid() As Integer
        Get
            Return mintLoginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintLoginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isweb
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isweb() As Boolean
        Get
            Return mblnIsweb
        End Get
        Set(ByVal value As Boolean)
            mblnIsweb = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set isvoid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Isvoid() As Boolean
        Get
            Return mblnIsvoid
        End Get
        Set(ByVal value As Boolean)
            mblnIsvoid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiduserunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiduserunkid() As Integer
        Get
            Return mintVoiduserunkid
        End Get
        Set(ByVal value As Integer)
            mintVoiduserunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidloginemployeeunkid
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidloginemployeeunkid() As Integer
        Get
            Return mintVoidloginemployeeunkid
        End Get
        Set(ByVal value As Integer)
            mintVoidloginemployeeunkid = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voiddatetime
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voiddatetime() As Date
        Get
            Return mdtVoiddatetime
        End Get
        Set(ByVal value As Date)
            mdtVoiddatetime = Value
        End Set
    End Property

    ''' <summary>
    ''' Purpose: Get or Set voidreason
    ''' Modify By: Sohail
    ''' </summary>
    Public Property _Voidreason() As String
        Get
            Return mstrVoidreason
        End Get
        Set(ByVal value As String)
            mstrVoidreason = Value
        End Set
    End Property

#End Region

#Region " Other Properties "
    Private mstrFormName As String = String.Empty
    Public WriteOnly Property _FormName() As String
        Set(ByVal value As String)
            mstrFormName = value
        End Set
    End Property

    Private mstrClientIP As String = ""
    Public WriteOnly Property _ClientIP() As String
        Set(ByVal value As String)
            mstrClientIP = value
        End Set
    End Property

    Private mstrHostName As String = ""
    Public WriteOnly Property _HostName() As String
        Set(ByVal value As String)
            mstrHostName = value
        End Set
    End Property

    Private mintAuditUserId As Integer = 0
    Public WriteOnly Property _AuditUserId() As Integer
        Set(ByVal value As Integer)
            mintAuditUserId = value
        End Set
    End Property

    Private mdtAuditDate As DateTime = Now
    Public WriteOnly Property _AuditDate() As DateTime
        Set(ByVal value As DateTime)
            mdtAuditDate = value
        End Set
    End Property

    Private mintCompanyUnkid As Integer = 0
    Public WriteOnly Property _CompanyUnkid() As Integer
        Set(ByVal value As Integer)
            mintCompanyUnkid = value
        End Set
    End Property
    'Gajanan [27-June-2020] -- Start
    'Enhancement:Create AT-Non Disclosure Declaration Report
    Private mstrMacAddress As String = ""
    Public WriteOnly Property _MacAddress() As String
        Set(ByVal value As String)
            mstrMacAddress = value
        End Set
    End Property
    'Gajanan [27-June-2020] -- End

    'Sohail (24 Jan 2022) -- Start
    'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee non-disclosure declaration.
    Private lstWebEmail As List(Of clsEmailCollection) = Nothing
    Public ReadOnly Property _WebEmailList() As List(Of clsEmailCollection)
        Get
            Return lstWebEmail
        End Get
    End Property
    'Sohail ((24 Jan 2022) -- End

#End Region

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Sub GetData()
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  nondisclosuredeclarationtranunkid " & _
              ", employeeunkid " & _
              ", yearunkid " & _
              ", declaration_date " & _
              ", witness1userunkid " & _
              ", witness1_date " & _
              ", witness2userunkid " & _
              ", witness2_date " & _
              ", issubmitforapproval " & _
              ", approvalstatusunkid " & _
              ", finalapproverunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM hrempnondisclosure_declaration_tran " & _
             "WHERE nondisclosuredeclarationtranunkid = @nondisclosuredeclarationtranunkid "

            objDataOperation.AddParameter("@nondisclosuredeclarationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintNondisclosuredeclarationTranUnkId.ToString)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            For Each dtRow As DataRow In dsList.Tables(0).Rows
                mintnondisclosuredeclarationtranunkid = CInt(dtRow.Item("nondisclosuredeclarationtranunkid"))
                mintEmployeeunkid = CInt(dtRow.Item("employeeunkid"))
                mintYearunkid = CInt(dtRow.Item("yearunkid"))
                mdtdeclaration_date = dtRow.Item("declaration_date")
                mintWitness1userunkid = CInt(dtRow.Item("witness1userunkid"))
                If IsDBNull(dtRow.Item("witness1_date")) = True Then
                    mdtWitness1_Date = Nothing
                Else
                    mdtWitness1_Date = dtRow.Item("witness1_date")
                End If
                mintWitness2userunkid = CInt(dtRow.Item("witness2userunkid"))
                If IsDBNull(dtRow.Item("witness2_date")) = True Then
                    mdtWitness2_Date = Nothing
                Else
                    mdtWitness2_Date = dtRow.Item("witness2_date")
                End If
                mblnissubmitforapproval = CBool(dtRow.Item("issubmitforapproval"))
                mintapprovalstatusunkid = CInt(dtRow.Item("approvalstatusunkid"))
                mintfinalapproverunkid = CInt(dtRow.Item("finalapproverunkid"))
                mintuserunkid = CInt(dtRow.Item("userunkid"))
                mintloginemployeeunkid = CInt(dtRow.Item("loginemployeeunkid"))
                mblnisweb = CBool(dtRow.Item("isweb"))
                mblnisvoid = CBool(dtRow.Item("isvoid"))
                mintvoiduserunkid = CInt(dtRow.Item("voiduserunkid"))
                mintvoidloginemployeeunkid = CInt(dtRow.Item("voidloginemployeeunkid"))
                If IsDBNull(dtRow.Item("voiddatetime")) = True Then
                    mdtVoiddatetime = Nothing
                Else
                    mdtVoiddatetime = dtRow.Item("voiddatetime")
                End If
                mstrvoidreason = dtRow.Item("voidreason").ToString
                Exit For
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetData; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Sub


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function GetList(ByVal xDataBaseName As String, _
                            ByVal xUserId As Integer, _
                            ByVal xYearId As Integer, _
                            ByVal xCompanyId As Integer, _
                            ByVal xIncludeInactiveEmp As Boolean, _
                            ByVal xPeriodStartDate As DateTime, _
                            ByVal xPeriodEndDate As DateTime, _
                            ByVal xUserAccessFilterString As String, _
                            ByVal strTableName As String, _
                            Optional ByVal blnApplyUserAccessFilter As Boolean = True, _
                            Optional ByVal intEmpUnkID As Integer = 0, _
                            Optional ByVal mstrAdvanceFilter As String = "", _
                            Optional ByVal strFilterString As String = "" _
                            ) As DataSet

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            Dim xDateJoinQry, xDateFilterQry, xUACQry, xUACFiltrQry, xAdvanceJoinQry As String
            xDateJoinQry = "" : xDateFilterQry = "" : xUACQry = "" : xUACFiltrQry = "" : xAdvanceJoinQry = ""
            Call GetDatesFilterString(xDateJoinQry, xDateFilterQry, xPeriodStartDate, xPeriodEndDate, , , xDataBaseName)
            If blnApplyUserAccessFilter = True Then
                Call NewAccessLevelFilterString(xUACQry, xUACFiltrQry, xPeriodEndDate, , xDataBaseName, xUserId, xCompanyId, xYearId, xUserAccessFilterString)
            End If
            Call GetAdvanceFilterQry(xAdvanceJoinQry, xPeriodEndDate, xDataBaseName)

            strQ = "SELECT   hremployee_master.employeeunkid " & _
                           ", employeecode  " & _
                           ", ISNULL(hremployee_master.firstname, '') + ' ' + ISNULL(hremployee_master.othername, '') + ' ' + ISNULL(hremployee_master.surname, '') AS employeename "

            strQ &= "INTO #TableEmp " & _
                     "FROM hremployee_master "

            If xDateJoinQry.Trim.Length > 0 Then
                strQ &= xDateJoinQry
            End If

            If xUACQry.Trim.Length > 0 Then
                strQ &= xUACQry
            End If

            If xAdvanceJoinQry.Trim.Length > 0 Then
                strQ &= xAdvanceJoinQry
            End If

            strQ &= "WHERE 1 = 1 "

            If intEmpUnkID > 0 Then
                strQ &= " AND hremployee_master.employeeunkid = " & intEmpUnkID
            End If

            If xDateFilterQry.Trim.Length > 0 Then
                strQ &= xDateFilterQry
            End If

            If xUACFiltrQry.Trim.Length > 0 Then
                strQ &= " AND " & xUACFiltrQry
            End If

            If mstrAdvanceFilter.Trim.Length > 0 Then
                strQ &= " AND " & mstrAdvanceFilter & " "
            End If

            strQ &= "SELECT " & _
                      "  hrempnondisclosure_declaration_tran.nondisclosuredeclarationtranunkid " & _
                      ", hrempnondisclosure_declaration_tran.employeeunkid " & _
                      ", #TableEmp.employeecode " & _
                      ", #TableEmp.employeename " & _
                      ", hrempnondisclosure_declaration_tran.yearunkid " & _
                      ", cffinancial_year_tran.financialyear_name " & _
                      ", hrempnondisclosure_declaration_tran.declaration_date " & _
                      ", CONVERT(CHAR(8), hrempnondisclosure_declaration_tran.declaration_date, 112) AS strdeclaration_date " & _
                      ", hrempnondisclosure_declaration_tran.witness1userunkid " & _
                      ", hrempnondisclosure_declaration_tran.witness1_date " & _
                      ", LTRIM(CONVERT(CHAR(8), ISNULL(hrempnondisclosure_declaration_tran.witness1_date, ''), 112)) AS strwitness1_date " & _
                      ", hrempnondisclosure_declaration_tran.witness2userunkid " & _
                      ", hrempnondisclosure_declaration_tran.witness2_date " & _
                      ", LTRIM(CONVERT(CHAR(8), ISNULL(hrempnondisclosure_declaration_tran.witness2_date, ''), 112)) AS strwitness2_date " & _
                      ", hrempnondisclosure_declaration_tran.issubmitforapproval " & _
                      ", hrempnondisclosure_declaration_tran.approvalstatusunkid " & _
                      ", hrempnondisclosure_declaration_tran.finalapproverunkid " & _
                      ", hrempnondisclosure_declaration_tran.userunkid " & _
                      ", hrempnondisclosure_declaration_tran.loginemployeeunkid " & _
                      ", hrempnondisclosure_declaration_tran.isweb " & _
                      ", hrempnondisclosure_declaration_tran.isvoid " & _
                      ", hrempnondisclosure_declaration_tran.voiduserunkid " & _
                      ", hrempnondisclosure_declaration_tran.voidloginemployeeunkid " & _
                      ", hrempnondisclosure_declaration_tran.voiddatetime " & _
                      ", hrempnondisclosure_declaration_tran.voidreason " & _
                 "FROM hrempnondisclosure_declaration_tran " & _
                      "JOIN #TableEmp ON #TableEmp.employeeunkid = hrempnondisclosure_declaration_tran.employeeunkid " & _
                      "LEFT JOIN hrmsConfiguration..cffinancial_year_tran ON cffinancial_year_tran.yearunkid = hrempnondisclosure_declaration_tran.yearunkid " & _
                 "WHERE hrempnondisclosure_declaration_tran.isvoid = 0 "

            strQ &= " DROP TABLE #TableEmp "

            dsList = objDataOperation.ExecQuery(strQ, strTableName)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: GetList; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
        Return dsList
    End Function


    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> INSERT INTO Database Table (hrempnondisclosure_declaration_tran) </purpose>
    Public Function Insert() As Boolean
        'If isExist(mstrName) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@declaration_date", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtdeclaration_date.ToString)
            objDataOperation.AddParameter("@witness1userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWitness1userunkid.ToString)
            If mdtWitness1_Date = Nothing Then
                objDataOperation.AddParameter("@witness1_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@witness1_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWitness1_Date.ToString)
            End If
            objDataOperation.AddParameter("@witness2userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWitness2userunkid.ToString)
            If mdtWitness2_Date = Nothing Then
                objDataOperation.AddParameter("@witness2_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@witness2_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWitness2_Date.ToString)
            End If
            objDataOperation.AddParameter("@issubmitforapproval", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnissubmitforapproval.ToString)
            objDataOperation.AddParameter("@approvalstatusunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintapprovalstatusunkid.ToString)
            objDataOperation.AddParameter("@finalapproverunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintfinalapproverunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            strQ = "INSERT INTO hrempnondisclosure_declaration_tran ( " & _
                      "  employeeunkid " & _
                      ", yearunkid " & _
                      ", declaration_date " & _
                      ", witness1userunkid " & _
                      ", witness1_date " & _
                      ", witness2userunkid " & _
                      ", witness2_date " & _
                      ", issubmitforapproval " & _
                      ", approvalstatusunkid " & _
                      ", finalapproverunkid " & _
                      ", userunkid " & _
                      ", loginemployeeunkid " & _
                      ", isweb " & _
                      ", isvoid " & _
                      ", voiduserunkid " & _
                      ", voidloginemployeeunkid " & _
                      ", voiddatetime " & _
                      ", voidreason" & _
                ") VALUES (" & _
                      "  @employeeunkid " & _
                      ", @yearunkid " & _
                      ", @declaration_date " & _
                      ", @witness1userunkid " & _
                      ", @witness1_date " & _
                      ", @witness2userunkid " & _
                      ", @witness2_date " & _
                      ", @issubmitforapproval " & _
                      ", @approvalstatusunkid " & _
                      ", @finalapproverunkid " & _
                      ", @userunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @isweb " & _
                      ", @isvoid " & _
                      ", @voiduserunkid " & _
                      ", @voidloginemployeeunkid " & _
                      ", @voiddatetime " & _
                      ", @voidreason" & _
                "); SELECT @@identity"

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            mintNondisclosuredeclarationTranUnkId = dsList.Tables(0).Rows(0).Item(0)

            If InsertAuditTrail(objDataOperation, enAuditType.ADD) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Insert; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Update Database Table (hrempnondisclosure_declaration_tran) </purpose>
    Public Function Update() As Boolean
        'If isExist(mstrName, mintNondisclosuredeclarationtranunkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            objDataOperation.AddParameter("@nondisclosuredeclarationtranunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintnondisclosuredeclarationtranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@declaration_date", SqlDbType.datetime, eZeeDataType.DATETIME_SIZE, mdtdeclaration_date.ToString)
            objDataOperation.AddParameter("@witness1userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWitness1userunkid.ToString)
            If mdtWitness1_Date = Nothing Then
                objDataOperation.AddParameter("@witness1_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@witness1_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWitness1_Date.ToString)
            End If
            objDataOperation.AddParameter("@witness2userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWitness2userunkid.ToString)
            If mdtWitness2_Date = Nothing Then
                objDataOperation.AddParameter("@witness2_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@witness2_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWitness2_Date.ToString)
            End If
            objDataOperation.AddParameter("@issubmitforapproval", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnissubmitforapproval.ToString)
            objDataOperation.AddParameter("@approvalstatusunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintapprovalstatusunkid.ToString)
            objDataOperation.AddParameter("@finalapproverunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintfinalapproverunkid.ToString)
            objDataOperation.AddParameter("@userunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintuserunkid.ToString)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintloginemployeeunkid.ToString)
            objDataOperation.AddParameter("@isweb", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisweb.ToString)
            objDataOperation.AddParameter("@isvoid", SqlDbType.bit, eZeeDataType.BIT_SIZE, mblnisvoid.ToString)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoiduserunkid.ToString)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.int, eZeeDataType.INT_SIZE, mintvoidloginemployeeunkid.ToString)
            If mdtVoiddatetime = Nothing Then
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime.ToString)
            End If
            objDataOperation.AddParameter("@voidreason", SqlDbType.nvarchar, eZeeDataType.NAME_SIZE, mstrvoidreason.ToString)

            strQ = "UPDATE hrempnondisclosure_declaration_tran SET " & _
                      "  employeeunkid = @employeeunkid" & _
                      ", yearunkid = @yearunkid" & _
                      ", declaration_date = @declaration_date" & _
                      ", witness1userunkid = @witness1userunkid" & _
                      ", witness1_date = @witness1_date" & _
                      ", witness2userunkid = @witness2userunkid" & _
                      ", witness2_date = @witness2_date" & _
                      ", issubmitforapproval = @issubmitforapproval" & _
                      ", approvalstatusunkid = @approvalstatusunkid" & _
                      ", finalapproverunkid = @finalapproverunkid" & _
                      ", userunkid = @userunkid" & _
                      ", loginemployeeunkid = @loginemployeeunkid" & _
                      ", isweb = @isweb" & _
                      ", isvoid = @isvoid" & _
                      ", voiduserunkid = @voiduserunkid" & _
                      ", voidloginemployeeunkid = @voidloginemployeeunkid" & _
                      ", voiddatetime = @voiddatetime" & _
                      ", voidreason = @voidreason " & _
            "WHERE nondisclosuredeclarationtranunkid = @nondisclosuredeclarationtranunkid "

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            If InsertAuditTrail(objDataOperation, enAuditType.EDIT) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If xDataOp Is Nothing Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Update; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <returns>Boolean</returns>
    ''' <purpose> Delete Database Table (hrempnondisclosure_declaration_tran) </purpose>
    Public Function Delete(ByVal intUnkid As Integer) As Boolean
        'If isUsed(intUnkid) Then
        '    mstrMessage = "<Message>"
        '    Return False
        'End If

        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        Dim blnxDataOp As Boolean = False

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
            objDataOperation.BindTransaction()
        Else
            objDataOperation = xDataOp
            blnxDataOp = True
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "UPDATE hrempnondisclosure_declaration_tran SET " & _
                     " isvoid = @isvoid" & _
                     ", voiduserunkid = @voiduserunkid " & _
                     ", voidloginemployeeunkid = @voidloginemployeeunkid " & _
                     ", voiddatetime = @voiddatetime" & _
                     ", voidreason = @voidreason " & _
           "WHERE nondisclosuredeclarationtranunkid = @nondisclosuredeclarationtranunkid "

            objDataOperation.AddParameter("@nondisclosuredeclarationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNondisclosuredeclarationtranunkid)
            objDataOperation.AddParameter("@isvoid", SqlDbType.Bit, eZeeDataType.BIT_SIZE, True)
            objDataOperation.AddParameter("@voiduserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoiduserunkid)
            objDataOperation.AddParameter("@voidloginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintVoidloginemployeeunkid)
            objDataOperation.AddParameter("@voiddatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtVoiddatetime)
            objDataOperation.AddParameter("@voidreason", SqlDbType.NVarChar, eZeeDataType.DESC_SIZE, mstrVoidreason)

            Call objDataOperation.ExecNonQuery(strQ)

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Me._xDataOp = objDataOperation
            Me._Nondisclosuredeclarationtranunkid = mintNondisclosuredeclarationtranunkid

            If InsertAuditTrail(objDataOperation, enAuditType.DELETE) = False Then
                objDataOperation.ReleaseTransaction(False)
                Return False
            End If

            If blnxDataOp = False Then objDataOperation.ReleaseTransaction(True)

            Return True
        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: Delete; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If blnxDataOp = False Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isUsed(ByVal intUnkid As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            StrQ = "<Query>"

            objDataOperation.AddParameter("@nondisclosuredeclarationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.tables(0).rows.count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isUsed; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    ''' <summary>
    ''' Modify By: Sohail
    ''' </summary>
    ''' <purpose> Assign all Property variable </purpose>
    Public Function isExist(ByVal intEmployeeID As Integer, ByVal intYearID As Integer, Optional ByVal intUnkid As Integer = -1, Optional ByVal strDBName As String = "") As Boolean
        'Sohail (05 Aug 2022) - [strDBName]
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception
        'Sohail (05 Aug 2022) -- Start
        'Issue :  : NMB - System was giving NonDisclouser declaration was not done when user tries to generate report for any previous year.
        Dim strDatabaseName As String = ""
        If strDBName.Trim <> "" Then
            strDatabaseName = strDBName & ".."
        End If
        'Sohail (05 Aug 2022) -- End

        If xDataOp Is Nothing Then
            objDataOperation = New clsDataOperation
        Else
            objDataOperation = xDataOp
        End If
        objDataOperation.ClearParameters()

        Try
            strQ = "SELECT " & _
              "  nondisclosuredeclarationtranunkid " & _
              ", employeeunkid " & _
              ", yearunkid " & _
              ", declaration_date " & _
              ", witness1userunkid " & _
              ", witness1_date " & _
              ", witness2userunkid " & _
              ", witness2_date " & _
              ", issubmitforapproval " & _
              ", approvalstatusunkid " & _
              ", finalapproverunkid " & _
              ", userunkid " & _
              ", loginemployeeunkid " & _
              ", isweb " & _
              ", isvoid " & _
              ", voiduserunkid " & _
              ", voidloginemployeeunkid " & _
              ", voiddatetime " & _
              ", voidreason " & _
             "FROM " & strDatabaseName & "hrempnondisclosure_declaration_tran " & _
             "WHERE isvoid = 0 "
            'Sohail (05 Aug 2022) - [strDatabaseName]

            If intEmployeeID > 0 Then
                strQ &= " AND employeeunkid = @employeeunkid "
                objDataOperation.AddParameter("employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intEmployeeID)
            End If

            If intYearID > 0 Then
                strQ &= " AND yearunkid = @yearunkid "
                objDataOperation.AddParameter("yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intYearID)
            End If

            If intUnkid > 0 Then
                strQ &= " AND nondisclosuredeclarationtranunkid <> @nondisclosuredeclarationtranunkid "
                objDataOperation.AddParameter("@nondisclosuredeclarationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, intUnkid)
            End If

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return dsList.Tables(0).Rows.Count > 0
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: isExist; Module Name: " & mstrModuleName)
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
            If xDataOp Is Nothing Then objDataOperation = Nothing
        End Try
    End Function

    Public Function InsertAuditTrail(ByVal objDataOperation As clsDataOperation, ByVal AuditType As Integer) As Boolean
        Dim dsList As DataSet = Nothing
        Dim strQ As String = ""
        Dim exForce As Exception

        Try
            objDataOperation.ClearParameters()

            objDataOperation.AddParameter("@nondisclosuredeclarationtranunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintNondisclosuredeclarationtranunkid.ToString)
            objDataOperation.AddParameter("@employeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintEmployeeunkid.ToString)
            objDataOperation.AddParameter("@yearunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintYearunkid.ToString)
            objDataOperation.AddParameter("@declaration_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtDeclaration_Date.ToString)
            objDataOperation.AddParameter("@witness1userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWitness1userunkid.ToString)
            If mdtWitness1_Date = Nothing Then
                objDataOperation.AddParameter("@witness1_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@witness1_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWitness1_Date.ToString)
            End If
            objDataOperation.AddParameter("@witness2userunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintWitness2userunkid.ToString)
            If mdtWitness2_Date = Nothing Then
                objDataOperation.AddParameter("@witness2_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, DBNull.Value)
            Else
                objDataOperation.AddParameter("@witness2_date", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtWitness2_Date.ToString)
            End If
            objDataOperation.AddParameter("@issubmitforapproval", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIssubmitforapproval.ToString)
            objDataOperation.AddParameter("@approvalstatusunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintApprovalstatusunkid.ToString)
            objDataOperation.AddParameter("@finalapproverunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintFinalapproverunkid.ToString)

            objDataOperation.AddParameter("@audituserunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintUserunkid)
            objDataOperation.AddParameter("@loginemployeeunkid", SqlDbType.Int, eZeeDataType.INT_SIZE, mintLoginemployeeunkid)
            objDataOperation.AddParameter("@audittype", SqlDbType.Int, eZeeDataType.INT_SIZE, AuditType.ToString)
            objDataOperation.AddParameter("@auditdatetime", SqlDbType.DateTime, eZeeDataType.DATETIME_SIZE, mdtAuditDate)
            objDataOperation.AddParameter("@isweb", SqlDbType.Bit, eZeeDataType.BIT_SIZE, mblnIsweb.ToString)
            objDataOperation.AddParameter("@ip", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrClientIP)
            objDataOperation.AddParameter("@host", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrHostName)
            objDataOperation.AddParameter("@form_name", SqlDbType.NVarChar, 500, mstrFormName)
            'Gajanan [27-June-2020] -- Start
            'Enhancement:Create AT-Non Disclosure Declaration Report
            objDataOperation.AddParameter("@mac_address", SqlDbType.NVarChar, eZeeDataType.NAME_SIZE, mstrMacAddress)
            'Gajanan [27-June-2020] -- End


            strQ = "INSERT INTO athrempnondisclosure_declaration_tran ( " & _
                      "  nondisclosuredeclarationtranunkid " & _
                      ", employeeunkid " & _
                      ", yearunkid " & _
                      ", declaration_date " & _
                      ", witness1userunkid " & _
                      ", witness1_date " & _
                      ", witness2userunkid " & _
                      ", witness2_date " & _
                      ", issubmitforapproval " & _
                      ", approvalstatusunkid " & _
                      ", finalapproverunkid " & _
                      ", audituserunkid " & _
                      ", loginemployeeunkid " & _
                      ", audittype " & _
                      ", auditdatetime " & _
                      ", isweb " & _
                      ", ip " & _
                      ", host " & _
                      ", form_name " & _
                      ", mac_address " & _
                ") VALUES (" & _
                      "  @nondisclosuredeclarationtranunkid " & _
                      ", @employeeunkid " & _
                      ", @yearunkid " & _
                      ", @declaration_date " & _
                      ", @witness1userunkid " & _
                      ", @witness1_date " & _
                      ", @witness2userunkid " & _
                      ", @witness2_date " & _
                      ", @issubmitforapproval " & _
                      ", @approvalstatusunkid " & _
                      ", @finalapproverunkid " & _
                      ", @audituserunkid " & _
                      ", @loginemployeeunkid " & _
                      ", @audittype " & _
                      ", @auditdatetime " & _
                      ", @isweb " & _
                      ", @ip " & _
                      ", @host " & _
                      ", @form_name " & _
                      ", @mac_address " & _
                "); SELECT @@identity"
            'Gajanan [27-June-2020] -- Add [mac_address]

            dsList = objDataOperation.ExecQuery(strQ, "List")

            If objDataOperation.ErrorMessage <> "" Then
                exForce = New Exception(objDataOperation.ErrorNumber & ": " & objDataOperation.ErrorMessage)
                Throw exForce
            End If

            Return True

        Catch ex As Exception
            objDataOperation.ReleaseTransaction(False)
            Throw New Exception(ex.Message & "; Procedure Name: InsertAuditTrail; Module Name: " & mstrModuleName)
            Return False
        Finally
            exForce = Nothing
            If dsList IsNot Nothing Then dsList.Dispose()
        End Try

    End Function

    'Sohail (19 Feb 2020) -- Start
    'NMB Enhancement # : Once user clicks on acknowledge button, system to trigger email to employee and to specified users mapped on configuration.
    Public Sub SendMailToEmployee_User(ByVal strEmployeeUnkId As String _
                                       , ByVal strNotifyNonDisclosureDecFinalSavedUserIDs As String _
                                       , ByVal intCompanyUnkId As Integer _
                                       , ByVal intYearUnkId As Integer _
                                       , ByVal strDatabaseName As String _
                                       , ByVal xPeriodStart As Date _
                                       , ByVal xPeriodEnd As Date _
                                       , ByVal xUserModeSetting As String _
                                       , ByVal xOnlyApproved As Boolean _
                                       , ByVal xIncludeIn_ActiveEmployee As Boolean _
                                       , Optional ByVal iLoginTypeId As Integer = -1 _
                                       , Optional ByVal iLoginEmployeeId As Integer = -1 _
                                       , Optional ByVal iUserId As Integer = -1 _
                                       , Optional ByVal strFileName As String = "" _
                                       )
        'Sohail (29 May 2020) - [strFileName]
        Dim strFinalSavedNofificationArrayIDs() As String
        Dim strEmployeeName As String
        Dim strMessage As New StringBuilder
        Dim objMaster As New clsMasterData
        Dim objUser As New clsUserAddEdit
        Dim objEmp As New clsEmployee_Master
        Dim objMail As New clsSendMail
        Dim strEmpContent As String = ""
        Dim strUserContent As String = ""
        Dim strEmployeeEmail As String
        Dim objCommon As New clsCommon_Master
        Dim strEmployeeTitle As String

        Try

            objUser = New clsUserAddEdit

            'Sohail (24 Jan 2022) -- Start
            'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee non-disclosure declaration.
            If HttpContext.Current IsNot Nothing Then
                lstWebEmail = New List(Of clsEmailCollection)
            End If
            'Sohail ((24 Jan 2022) -- End

            objEmp._Employeeunkid(xPeriodStart) = Convert.ToInt32(strEmployeeUnkId)
            objCommon._Masterunkid = objEmp._Titalunkid
            strEmployeeTitle = objCommon._Name
            strEmployeeName = strEmployeeTitle & " " & objEmp._Firstname & " " & objEmp._Surname

            strEmployeeEmail = objEmp._Email
            If strEmployeeEmail IsNot Nothing AndAlso strEmployeeEmail.Length > 0 Then
                strMessage = New StringBuilder

                objMail._Subject = Language.getMessage(mstrModuleName, 1, "Your Acknowledgement has been submitted.")

                strMessage.Append("<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")

                strEmpContent = Language.getMessage(mstrModuleName, 2, "Dear #employeename#," & _
                                                    vbCrLf & vbCrLf & _
                                                    "This is to notify that your acknowledgement has been submitted successfully." & _
                                                    vbCrLf & vbCrLf & _
                                                    "Thank you," & _
                                                    vbCrLf & _
                                                    "Compliance")

                strEmpContent = strEmpContent.Replace("#employeename#", "<B>" & strEmployeeName & "</B>")
                strEmpContent = strEmpContent.Replace(vbCrLf, "<BR>")

                strMessage.Append(strEmpContent)

                strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                strMessage.Append("</BODY></HTML>")

                objMail._Message = strMessage.ToString
                objMail._ToEmail = strEmployeeEmail

                If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                If mstrFormName.Trim.Length > 0 Then
                    objMail._Form_Name = mstrFormName
                End If
                objMail._LogEmployeeUnkid = iLoginEmployeeId
                objMail._OperationModeId = iLoginTypeId
                objMail._UserUnkid = 0
                Dim objUsr As New clsUserAddEdit
                objUsr._Userunkid = 0
                objMail._SenderAddress = IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)
                objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT

                'Sohail (29 May 2020) -- Start
                'NMB Enhancement # : After employee Acknowledges, the email sent to him will have the declaration form as an attachment.
                'gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                If strFileName.Trim <> "" Then
                    objMail._AttachedFiles = strFileName
                End If

                'Sohail (24 Jan 2022) -- Start
                'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee non-disclosure declaration.
                'gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email), strFileName))
                If HttpContext.Current Is Nothing Then
                gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email), strFileName))
                Else
                    lstWebEmail.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", 0, iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email), strFileName))
                End If
                'Sohail ((24 Jan 2022) -- End
                'Sohail (29 May 2020) -- End
                objUsr = Nothing
            End If

            strFinalSavedNofificationArrayIDs = strNotifyNonDisclosureDecFinalSavedUserIDs.Split(",")
            For Each strNofificationID As String In strFinalSavedNofificationArrayIDs
                If strNofificationID.Trim = "" Then Continue For

                Dim strAppEmpIDs As String = objMaster.GetUserAccessLevelEmployees(strDatabaseName, CInt(strNofificationID), intYearUnkId, intCompanyUnkId, xPeriodStart, xPeriodEnd, xUserModeSetting, xOnlyApproved, xIncludeIn_ActiveEmployee)
                If (strAppEmpIDs.Split(",").Contains(strEmployeeUnkId)) = True Then

                    strMessage = New StringBuilder

                    objUser._Userunkid = CInt(strNofificationID)
                    If objUser._Email.Trim = "" Then Continue For


                    objMail._Subject = Language.getMessage(mstrModuleName, 3, "Employee Acknowledgement Notification") & "-" & strEmployeeName & "(" & objEmp._Employeecode & ")"

                    strMessage.Append("<HTML> <BODY style='font-size:9.0pt; font-family:" & Chr(34) & "Verdana" & Chr(34) & "," & Chr(34) & "Sans-Serif" & Chr(34) & "; color:#333399;'>")

                    strUserContent = Language.getMessage(mstrModuleName, 4, "Dear #username#," & _
                                                         vbCrLf & vbCrLf & _
                                                         "This is to notify you that confidentiality acknowledgement for employee #employeename# has been submitted." & _
                                                         vbCrLf & vbCrLf & _
                                                         "Regards.")

                    strUserContent = strUserContent.Replace("#username#", "<B>" & objUser._Firstname & " " & objUser._Lastname & "</B>")
                    strUserContent = strUserContent.Replace("#employeename#", "<B>" & strEmployeeName & "</B>")
                    strUserContent = strUserContent.Replace(vbCrLf, "<BR>")

                    strMessage.Append(strUserContent)

                    strMessage.Append("<p><center style='padding-top:25px;font-size:10.0pt;font-family:&quot;Verdana&quot;,&quot;Sans-Serif&quot;;color:#333399;margin-left:0px;margin-right:0px;margin-top:0px;margin-bottom:10px'><b>&quot;POWERED BY ARUTI HR &amp; PAYROLL MANAGEMENT SOFTWARE.&quot;</b></center></p>")
                    strMessage.Append("</BODY></HTML>")

                    objMail._Message = strMessage.ToString
                    objMail._ToEmail = objUser._Email

                    If iLoginTypeId <= 0 Then iLoginTypeId = enLogin_Mode.DESKTOP
                    If mstrFormName.Trim.Length > 0 Then
                        objMail._Form_Name = mstrFormName
                    End If
                    objMail._LogEmployeeUnkid = iLoginEmployeeId
                    objMail._OperationModeId = iLoginTypeId
                    objMail._UserUnkid = Convert.ToInt32(strNofificationID)
                    Dim objUsr As New clsUserAddEdit
                    objUsr._Userunkid = Convert.ToInt32(strNofificationID)
                    objMail._SenderAddress = IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)
                    objMail._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT

                    'Sohail (29 May 2020) -- Start
                    'NMB Enhancement # : After employee Acknowledges, the email sent to him will have the declaration form as an attachment.
                    'gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", Convert.ToInt32(strNofificationID), iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email)))
                    If strFileName.Trim <> "" Then
                        objMail._AttachedFiles = strFileName
                    End If

                    'Sohail (24 Jan 2022) -- Start
                    'NMB Issue :  : Collection was modified; enumeration operation may not execute on Sending Notification in employee non-disclosure declaration.
                    'gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", Convert.ToInt32(strNofificationID), iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email), strFileName))
                    If HttpContext.Current Is Nothing Then
                    gobjEmailList.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", Convert.ToInt32(strNofificationID), iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email), strFileName))
                    Else
                        lstWebEmail.Add(New clsEmailCollection(objMail._ToEmail, objMail._Subject, objMail._Message, mstrFormName, iLoginEmployeeId, "", "", Convert.ToInt32(strNofificationID), iLoginTypeId, clsSendMail.enAT_VIEW_TYPE.PAYROLL_MGT, IIf(objUsr._Email = "", objUsr._Firstname & " " & objUsr._Lastname, objUsr._Email), strFileName))
                    End If
                    'Sohail ((24 Jan 2022) -- End

                    'Sohail (29 May 2020) -- End
                    objUsr = Nothing
                Else
                    Continue For
                End If


            Next

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: SendMailToEmployee_User; Module Name: " & mstrModuleName)
        Finally
            objUser = Nothing
            objMail = Nothing
        End Try
    End Sub
    'Sohail (19 Feb 2020) -- End

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Public Shared Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Your Acknowledgement has been submitted.")
			Language.setMessage(mstrModuleName, 2, "Dear #employeename#," & _
                                                    vbCrLf & vbCrLf & _
                                                    "This is to notify that your acknowledgement has been submitted successfully." & _
                                                    vbCrLf & vbCrLf & _
                                                    "Thank you," & _
                                                    vbCrLf & _
                                                    "Compliance")
			Language.setMessage(mstrModuleName, 3, "Employee Acknowledgement Notification")
			Language.setMessage(mstrModuleName, 4, "Dear #username#," & _
                                                         vbCrLf & vbCrLf & _
                                                         "This is to notify you that confidentiality acknowledgement for employee #employeename# has been submitted." & _
                                                         vbCrLf & vbCrLf & _
                                                         "Regards.")

		Catch Ex As Exception
            Throw New Exception(Ex.Message & "; Procedure Name: SetMessages; Module Name: " & mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class