﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmRemark

#Region " Private Variables "
    Private ReadOnly mstrModuleName As String = "frmRemark"
    Private mstrRemarks As String = String.Empty
    Private menApplicationType As enArutiApplicatinType
    Private mblnCancel As Boolean = True
    Private lstInvalidChar() As Char = {CChar("*"), CChar("+"), CChar("-"), CChar("/")} 'Sohail (25 Aug 2022)
#End Region

#Region " Diplay Dialog "

    Public Function displayDialog(ByRef strRemarks As String, ByVal enAppType As enArutiApplicatinType) As Boolean
        Try
            mstrRemarks = strRemarks

            'Sohail (25 Aug 2022) -- Start
            'Enhancement : AC2-793 : VFT - As a user, I want to be able to stop over deduction where net pay goes below a certain percentage of employee gross or basic.
            txtRemarks.InvalidChars = txtRemarks.InvalidChars.Where(Function(x) lstInvalidChar.Contains(x) = False).ToArray
            'Sohail (25 Aug 2022) -- End

            'Pinkal (27-Mar-2017) -- Start
            'Enhancement - Working On Import Device Attendance Data.
            If strRemarks.Trim.Length > 0 Then
                txtRemarks.Text = strRemarks
                txtRemarks.ReadOnly = True
            End If
            'Pinkal (27-Mar-2017) -- End

            menApplicationType = enAppType
            Me.ShowDialog()
            strRemarks = mstrRemarks

            Return Not mblnCancel

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmRemark_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.KeyCode = Windows.Forms.Keys.Enter Then
                If btnOk.Enabled = True Then
                    Call btnOk.PerformClick()
                End If
            ElseIf e.Control = True And e.KeyCode = Windows.Forms.Keys.O Then
                Call btnOk.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRemark_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmRemark_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "frmRemark_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

    Private Sub frmRemark_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Call Set_Logo(Me, menApplicationType)

            'Sohail (25 Aug 2022) -- Start
            'Enhancement : AC2-793 : VFT - As a user, I want to be able to stop over deduction where net pay goes below a certain percentage of employee gross or basic.
            txtRemarks.InvalidChars = txtRemarks.InvalidChars.Where(Function(x) lstInvalidChar.Contains(x) = False).ToArray
            'Sohail (25 Aug 2022) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRemark_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If txtRemarks.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry,") & " " & objgbRemarks.Text & " " & Language.getMessage(mstrModuleName, 2, "cannot be blank."), enMsgBoxStyle.Information)
                txtRemarks.Focus()
                Exit Sub
            End If

            mstrRemarks = txtRemarks.Text.Trim
            mblnCancel = False
            Me.Close()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnOk_Click", mstrModuleName)
        End Try
    End Sub

#End Region



    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            
            Call SetLanguage()

            Me.objgbRemarks.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.objgbRemarks.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnOk.GradientBackColor = GUI._ButttonBackColor
            Me.btnOk.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnOk.Text = Language._Object.getCaption(Me.btnOk.Name, Me.btnOk.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Sorry,")
            Language.setMessage(mstrModuleName, 2, "cannot be blank.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class