﻿#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.IO

#End Region

Public Class frmPreviewDocuments

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmPreviewDocuments"
    Private mdtTran As DataTable
    Private mstrPreviewIds As String = String.Empty
    Private objDocument As clsScan_Attach_Documents

    'SHANI (16 JUL 2015) -- Start
    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    'Upload Image folder to access those attachemts from Server and 
    'all client machines if ArutiSelfService is installed otherwise save then on Document path
    Private mblnIsSelfServiceInstall As Boolean
    Private mdsDoc As DataSet
    'SHANI (16 JUL 2015) -- End 

    'Nilay (02-Jun-2016) -- Start
    'ENHANCEMENT : Set Actual Image in Preview List as per Mathew's request
    Dim mstrLocalPath As String = String.Empty
    'Nilay (02-Jun-2016) -- End

    'S.SANDEEP [24 MAY 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    Private mblnIsTrasacation As Boolean = False
    'S.SANDEEP [24 MAY 2016] -- END


#End Region

#Region " Display Dialog "

    'S.SANDEEP [24 MAY 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    'Public Function displayDialog(ByVal strPreviewIds As String) As Boolean
    Public Function displayDialog(ByVal strPreviewIds As String, Optional ByVal blnTrascation As Boolean = False) As Boolean
        'S.SANDEEP [24 MAY 2016] -- END
        Try

            mstrPreviewIds = strPreviewIds
            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            mblnIsTrasacation = blnTrascation
            'S.SANDEEP [24 MAY 2016] -- END

            Me.ShowDialog()

            ''SHANI (16 JUL 2015) -- Start
            ''Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
            ''Upload Image folder to access those attachemts from Server and 
            ''all client machines if ArutiSelfService is installed otherwise save then on Document path
            'If mdsDoc IsNot Nothing AndAlso mdtTran.Rows.Count > 0 Then
            '    For Each xRow As DataRow In mdtTran.Rows
            '        If xRow("temppath").ToString.Trim <> "" Then
            '            If File.Exists(xRow("temppath").ToString) Then
            '                File.Delete(xRow("temppath").ToString)
            '            End If
            '        End If
            '    Next
            'End If
            ''SHANI (16 JUL 2015) -- End 

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region "Public Property"
    Public Property _dtTable()
        Get
            Return mdtTran
        End Get
        Set(ByVal value)
            mdtTran = value
        End Set
    End Property
#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillList()
        Try
            lvPreviewList.Items.Clear()

            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            If mdtTran Is Nothing Then Exit Sub
            'S.SANDEEP [24 MAY 2016] -- END

            For Each dtRow As DataRow In mdtTran.Rows
                Dim lvItem As New ListViewItem

                lvItem.Text = dtRow.Item("code").ToString               'Code
                lvItem.SubItems.Add(dtRow.Item("names").ToString)       'Name
                lvItem.SubItems.Add(dtRow.Item("document").ToString)    'Document
                lvItem.SubItems.Add(dtRow.Item("filename").ToString)    'FileName
                lvItem.SubItems.Add(dtRow.Item("doctype").ToString)     'DocType

                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                'lvItem.SubItems.Add(dtRow.Item("orgfilepath").ToString) 'FullPath
                lvItem.Tag = dtRow.Item("scanattachtranunkid").ToString

                'S.SANDEEP [24 MAY 2016] -- START
                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                'lvItem.SubItems.Add(dtRow.Item("filepath").ToString) 'FullPath
                If dtRow.Item("filepath").ToString.Trim.Length > 0 Then
                lvItem.SubItems.Add(dtRow.Item("filepath").ToString) 'FullPath
                Else
                    lvItem.SubItems.Add(dtRow.Item("orgfilepath").ToString) 'FullPath
                End If
                'S.SANDEEP [24 MAY 2016] -- END


                'SHANI (16 JUL 2015) -- End 



                lvPreviewList.Items.Add(lvItem)
            Next

            lvPreviewList.GroupingColumn = objcolhDocType
            lvPreviewList.DisplayGroups(True)

            If lvPreviewList.Items.Count >= 4 Then
                colhName.Width = 200 - 30
            Else
                colhName.Width = 200
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
        End Try
    End Sub

    'SHANI (16 JUL 2015) -- Start
    'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    'Upload Image folder to access those attachemts from Server and 
    'all client machines if ArutiSelfService is installed otherwise save then on Document path
    Private Function CreateImage(ByVal xrow As DataRow) As String
        Dim strError As String = ""
        Dim strLocalpath As String = ""
        Try
            Dim mstrFolderName As String = (From p In mdsDoc.Tables("Docs") Where (CInt(p.Item("Id").ToString) = CInt(xrow("scanattachrefid").ToString)) Select (p.Item("Name").ToString)).FirstOrDefault

            'Shani(24-Aug-2015) -- Start
            'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
            'Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xrow("filepath").ToString, xrow("fileuniquename"), mstrFolderName, strError)
            Dim imagebyte() As Byte = clsFileUploadDownload.DownloadFile(xrow("filepath").ToString, xrow("fileuniquename"), mstrFolderName, strError, ConfigParameter._Object._ArutiSelfServiceURL)
            'Shani(24-Aug-2015) -- End

            If imagebyte IsNot Nothing Then
                strLocalpath = Path.GetTempPath() & DateTime.Now.ToString("MMyyyyddsshhmm") & "_" & xrow("fileuniquename")
                Dim ms As New MemoryStream(imagebyte)
                Dim fs As New FileStream(strLocalpath, FileMode.Create)
                ms.WriteTo(fs)
                ms.Close()
                fs.Close()
                fs.Dispose()
                'xrow(0).Item("temppath") = strLocalpath
                'xrow(0).AcceptChanges()
                'objlblCaption.Visible = False
                'picViewImage.ImageLocation = strLocalpath
            Else

                'SHANI (27 JUL 2015) -- Start
                'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
                'mdtTran
                'objlblCaption.Visible = True
                'Dim StrMessage As String = "Unable to preview. Reson : File [ " & lvPreviewList.SelectedItems(0).SubItems(colhFileName.Index).Text & " ] has been " & _
                '"removed or renamed or moved to other location."
                'eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                If System.IO.File.Exists(xrow("filepath").ToString) Then
                    strLocalpath = xrow("filepath").ToString
                    Dim strFileUniqueName As String = Company._Object._Code & "_" & Guid.NewGuid.ToString & "_" & DateTime.Now.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(CStr(xrow("filepath").ToString))

                    'Shani(24-Aug-2015) -- Start
                    'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                    'If clsFileUploadDownload.UploadFile(xrow("filepath").ToString, mstrFolderName, strFileUniqueName, strError) = False Then
                    If clsFileUploadDownload.UploadFile(xrow("filepath").ToString, mstrFolderName, strFileUniqueName, strError, ConfigParameter._Object._ArutiSelfServiceURL) = False Then
                        'Shani(24-Aug-2015) -- End

                        eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                        Exit Try
                    Else
                        Dim strPath As String = ConfigParameter._Object._ArutiSelfServiceURL
                        If Strings.Right(strPath, 1) <> "/" AndAlso Strings.Right(strPath, 1) <> "\" Then
                            strPath += "/"
                        End If
                        strPath += "uploadimage/" & mstrFolderName & "/" + strFileUniqueName
                        xrow("AUD") = "U"
                        xrow("fileuniquename") = strFileUniqueName
                        xrow("filepath") = strPath
                        xrow("filesize") = (New System.IO.FileInfo(strLocalpath)).Length / 1024
                        xrow.AcceptChanges()
                        Dim objScanAttach As New clsScan_Attach_Documents
                        objScanAttach._Datatable = mdtTran
                        objScanAttach.InsertUpdateDelete_Documents()
                        If objDocument._Message <> "" Then
                            objScanAttach = Nothing
                            eZeeMsgBox.Show(objDocument._Message, enMsgBoxStyle.Information)
                            Exit Try
                        End If
                        objScanAttach = Nothing
                        xrow("AUD") = ""
                        xrow.AcceptChanges()

                    End If
                Else
                objlblCaption.Visible = True
                Dim StrMessage As String = "Unable to preview. Reson : File [ " & lvPreviewList.SelectedItems(0).SubItems(colhFileName.Index).Text & " ] has been " & _
                "removed or renamed or moved to other location."

                    'Pinkal (16-Oct-2023) -- Start
                    '(A1X-1400) NMB - Post assigned employee assets to P2P
                    'eZeeMsgBox.Show(strError, enMsgBoxStyle.Information)
                    eZeeMsgBox.Show(StrMessage, enMsgBoxStyle.Information)
                    'Pinkal (16-Oct-2023) -- End

            End If
            End If
            'SHANI (27 JUL 2015) -- End 
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "CreateImage :", mstrModuleName)
        End Try
        Return strLocalpath
    End Function
    'SHANI (16 JUL 2015) -- End 
    'Nilay (02-Jun-2016) -- Start
    'ENHANCEMENT : Set Actual Image in Preview List as per Mathew's request
    Private Sub ScaleImage(ByVal strLocalPath As String)
        Try

            Dim PicBoxHeight As Integer
            Dim PicBoxWidth As Integer
            Dim ImageHeight As Integer
            Dim ImageWidth As Integer
            Dim ResizeWidthScale As Single
            Dim TempImage As Image
            Dim scale_factorH As Single = 1.0
            Dim scale_factorW As Single = 1.0

            picViewImage.SizeMode = PictureBoxSizeMode.Normal
            PicBoxHeight = picViewImage.Height
            PicBoxWidth = picViewImage.Width

            TempImage = Image.FromFile(strLocalPath)
            ImageHeight = TempImage.Height
            ImageWidth = TempImage.Width

            If ImageHeight > PicBoxHeight Then
                scale_factorH = CSng(PicBoxHeight / ImageHeight)
            End If

            If ImageWidth > PicBoxWidth Then
                scale_factorW = CSng(PicBoxWidth / ImageWidth)
            End If

            If scale_factorW < 1 Then
                ResizeWidthScale = scale_factorW
            Else
                ResizeWidthScale = scale_factorH * scale_factorW
            End If

            picViewImage.Image = TempImage

            Dim bm_source As New Bitmap(picViewImage.Image)
            Dim bm_dest As New Bitmap(CInt(bm_source.Width * ResizeWidthScale), CInt(bm_source.Height * scale_factorH))
            Dim gr_dest As Graphics = Graphics.FromImage(bm_dest)

            Dim xpoint As Integer = 0
            Dim Ypoint As Integer = 0

            If bm_dest.Width < PicBoxWidth Then
                xpoint = (PicBoxWidth - bm_dest.Width) / 2
            End If

            If bm_dest.Height < PicBoxHeight Then
                Ypoint = (PicBoxHeight - bm_dest.Height) / 2
            End If

            gr_dest.DrawImage(bm_source, 0, 0, bm_dest.Width + 1, bm_dest.Height + 1)

            picViewImage.Padding = New System.Windows.Forms.Padding(xpoint, Ypoint, 0, 0)
            'picViewImage.Margin.Top = Ypoint
            picViewImage.Image = bm_dest
            
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "ScaleImage", mstrModuleName)
        End Try
    End Sub
    'Nilay (02-Jun-2016) -- End

#End Region

#Region " Form's Event(s) "

    'S.SANDEEP [24 MAY 2016] -- START
    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
    'Private Sub frmPreviewDocuments_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    objDocument = New clsScan_Attach_Documents
    '    Try
    '        Call Set_Logo(Me, gApplicationType)
    '        objlblCaption.ForeColor = GUI.ColorComp
    '        objlblCaption.Text = "Please select information from list in order to preview."
    '        lvPreviewList.GridLines = False

    '        'Shani(24-Aug-2015) -- Start
    '        'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
    '        'Call objDocument.GetList("List", mstrPreviewIds)
    '        Call objDocument.GetList(ConfigParameter._Object._Document_Path, "List", mstrPreviewIds)
    '        'Shani(24-Aug-2015) -- End

    '        mdtTran = objDocument._Datatable.Copy

    '        'SHANI (16 JUL 2015) -- Start
    '        'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
    '        'Upload Image folder to access those attachemts from Server and 
    '        'all client machines if ArutiSelfService is installed otherwise save then on Document path
    '        mdtTran.Columns.Add("temppath", Type.GetType("System.String"))
    '        mblnIsSelfServiceInstall = IsSelfServiceExist()
    '        mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")
    '        'SHANI (16 JUL 2015) -- End 
    '        Call FillList()
    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "", mstrModuleName)
    '    Finally
    '    End Try
    'End Sub
    Private Sub frmPreviewDocuments_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objDocument = New clsScan_Attach_Documents
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)
            objlblCaption.ForeColor = GUI.ColorComp
            objlblCaption.Text = "Please select information from list in order to preview."
            lvPreviewList.GridLines = False

            If mblnIsTrasacation = False AndAlso mdtTran Is Nothing Then
                'S.SANDEEP |04-SEP-2021| -- START
                'Call objDocument.GetList(ConfigParameter._Object._Document_Path, "List", mstrPreviewIds)
                Call objDocument.GetList(ConfigParameter._Object._Document_Path, "List", mstrPreviewIds, , , , , , CBool(IIf(mstrPreviewIds.Trim.Length <= 0, True, False)), , , , True)
                'S.SANDEEP |04-SEP-2021| -- END
                mdtTran = objDocument._Datatable.Copy
            End If


            'S.SANDEEP [24 MAY 2016] -- START
            'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
            'mdtTran.Columns.Add("temppath", Type.GetType("System.String"))
            If mdtTran.Columns.Contains("temppath") = False Then mdtTran.Columns.Add("temppath", Type.GetType("System.String"))
            'S.SANDEEP [24 MAY 2016] -- END

            mblnIsSelfServiceInstall = IsSelfServiceExist()
            mdsDoc = (New clsScan_Attach_Documents).GetDocFolderName("Docs")

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmPreviewDocuments_Load", mstrModuleName)
        Finally
        End Try
    End Sub
    'S.SANDEEP [24 MAY 2016] -- END
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)
            Call Language.setLanguage(Me.Name)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
#End Region

#Region " Button's Event(s) "

#End Region

#Region " Control's Event(s) "

    Private Sub lvPreviewList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvPreviewList.SelectedIndexChanged
        Try

            'SHANI (27 JUL 2015) -- Start
            'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
            Cursor.Current = Cursors.WaitCursor
            'SHANI (27 JUL 2015) -- End 

            If lvPreviewList.SelectedItems.Count > 0 Then
                objlblCaption.Text = "No Preview available."

                'SHANI (16 JUL 2015) -- Start
                'Enhancement : Save all attachments from deskto and SS to IIS Aruti Self Service 
                'Upload Image folder to access those attachemts from Server and 
                'all client machines if ArutiSelfService is installed otherwise save then on Document path
                'If IO.File.Exists(lvPreviewList.SelectedItems(0).SubItems(objcolhFullPath.Index).Text) Then
                '    If clsScan_Attach_Documents.IsValidImage(lvPreviewList.SelectedItems(0).SubItems(objcolhFullPath.Index).Text) Then
                '        objlblCaption.Visible = False
                '        Dim StrFile As String = lvPreviewList.SelectedItems(0).SubItems(objcolhFullPath.Index).Text
                '        picViewImage.ImageLocation = StrFile
                '    Else
                '        objlblCaption.Visible = True
                '        Process.Start(lvPreviewList.SelectedItems(0).SubItems(objcolhFullPath.Index).Text)
                '    End If
                'Else
                '    objlblCaption.Visible = True
                '    Dim StrMessage As String = "Unable to preview. Reson : File [ " & lvPreviewList.SelectedItems(0).SubItems(colhFileName.Index).Text & " ] has been " & _
                '    "removed or renamed or moved to other location."
                '    eZeeMsgBox.Show(StrMessage, enMsgBoxStyle.Information)
                'End If

                'S.SANDEEP [24 MAY 2016] -- START
                'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                If mblnIsSelfServiceInstall Then

                    Dim strError As String = ""
                    Dim xRow() As DataRow = mdtTran.Select("scanattachtranunkid=" & lvPreviewList.SelectedItems(0).Tag)
                    Dim strLocalPath As String = ""

                    'S.SANDEEP [24 MAY 2016] -- START
                    'ENHANCEMENT : DISCIPLINE MAKE-OVER {BY ANDREW}
                    'If xRow(0).Item("temppath").ToString.Trim <> "" Then
                    Dim strFilePath As String = ""
                    If CInt(lvPreviewList.SelectedItems(0).Tag) <= 0 Then
                        strFilePath = lvPreviewList.SelectedItems(0).SubItems(objcolhFullPath.Index).Text
                    End If

                    If xRow(0).Item("temppath").ToString.Trim <> "" OrElse strFilePath <> "" Then
                        If xRow(0).Item("temppath").ToString.Trim.Length > 0 Then
                            strFilePath = xRow(0).Item("temppath").ToString.Trim
                        End If

                        'If System.IO.File.Exists(xRow(0).Item("temppath").ToString.Trim) Then
                        '    objlblCaption.Visible = False
                        '    If clsScan_Attach_Documents.IsValidImage(xRow(0).Item("temppath").ToString.Trim) Then
                        '        'Nilay (02-Jun-2016) -- Start
                        '        'ENHANCEMENT : Set Actual Image in Preview List as per Mathew's request
                        '        mstrLocalPath = xRow(0).Item("temppath").ToString
                        '        Call ScaleImage(mstrLocalPath)
                        '        'picViewImage.ImageLocation = xRow(0).Item("temppath").ToString.Trim
                        '        'Nilay (02-Jun-2016) -- End
                        '    Else
                        '        objlblCaption.Visible = True
                        '        Process.Start(xRow(0).Item("temppath").ToString.Trim)
                        '    End If
                        If System.IO.File.Exists(strFilePath) Then
                            objlblCaption.Visible = False
                            If clsScan_Attach_Documents.IsValidImage(strFilePath) Then
                                mstrLocalPath = strFilePath
                                Call ScaleImage(mstrLocalPath)
                            Else
                                objlblCaption.Visible = True
                                Process.Start(strFilePath)
                            End If
                            'S.SANDEEP [24 MAY 2016] -- END
                        Else
                            strLocalPath = CreateImage(xRow(0))
                            If clsScan_Attach_Documents.IsValidImage(strLocalPath) Then
                                If strLocalPath <> "" Then
                                    xRow(0).Item("temppath") = strLocalPath
                                    xRow(0).AcceptChanges()
                                    objlblCaption.Visible = False
                                    'Nilay (02-Jun-2016) -- Start
                                    'ENHANCEMENT : Set Actual Image in Preview List as per Mathew's request
                                    Call ScaleImage(strLocalPath)
                                    mstrLocalPath = strLocalPath
                                    'picViewImage.ImageLocation = strLocalPath
                                    'Nilay (02-Jun-2016) -- End
                                End If
                            Else
                                objlblCaption.Visible = True
                                Process.Start(strLocalPath)
                            End If
                        End If
                    Else
                        strLocalPath = CreateImage(xRow(0))
                        If strLocalPath <> "" Then
                            If clsScan_Attach_Documents.IsValidImage(strLocalPath) Then
                                xRow(0).Item("temppath") = strLocalPath
                                xRow(0).AcceptChanges()
                                objlblCaption.Visible = False
                                'Nilay (02-Jun-2016) -- Start
                                'ENHANCEMENT : Set Actual Image in Preview List as per Mathew's request
                                Call ScaleImage(strLocalPath)
                                mstrLocalPath = strLocalPath
                                'picViewImage.ImageLocation = strLocalPath
                                'Nilay (02-Jun-2016) -- End
                            Else
                                objlblCaption.Visible = True
                                Process.Start(strLocalPath)
                            End If
                        End If
                    End If
                Else
                If IO.File.Exists(lvPreviewList.SelectedItems(0).SubItems(objcolhFullPath.Index).Text) Then
                    If clsScan_Attach_Documents.IsValidImage(lvPreviewList.SelectedItems(0).SubItems(objcolhFullPath.Index).Text) Then
                        objlblCaption.Visible = False
                        Dim StrFile As String = lvPreviewList.SelectedItems(0).SubItems(objcolhFullPath.Index).Text
                            'Nilay (02-Jun-2016) -- Start
                            'ENHANCEMENT : Set Actual Image in Preview List as per Mathew's request
                            'picViewImage.ImageLocation = StrFile
                            Call ScaleImage(StrFile)
                            'Nilay (02-Jun-2016) -- End
                    Else
                        objlblCaption.Visible = True
                        Process.Start(lvPreviewList.SelectedItems(0).SubItems(objcolhFullPath.Index).Text)
                    End If
                Else
                    objlblCaption.Visible = True
                    Dim StrMessage As String = "Unable to preview. Reson : File [ " & lvPreviewList.SelectedItems(0).SubItems(colhFileName.Index).Text & " ] has been " & _
                    "removed or renamed or moved to other location."
                    eZeeMsgBox.Show(StrMessage, enMsgBoxStyle.Information)
                End If
                End If
                'SHANI (16 JUL 2015) -- End
            Else
                objlblCaption.Text = "Please select information from list in order to preview."
                objlblCaption.Visible = True
                picViewImage.Image = Nothing
            End If
        Catch ex As Exception
            eZeeMsgBox.Show(ex.Message, enMsgBoxStyle.Information)
            'DisplayError.Show("-1", ex.Message, "lvPreviewList_SelectedIndexChanged", mstrModuleName)
        Finally

            'SHANI (27 JUL 2015) -- Start
            'Enhancement - Provide option on configuration to make Qualification and Dependant attachment mandatory
            Cursor.Current = Cursors.Default
            'SHANI (27 JUL 2015) -- End 

        End Try
    End Sub

    'Nilay (02-Jun-2016) -- Start
    'ENHANCEMENT : Set Actual Image in Preview List as per Mathew's request
    Private Sub picViewImage_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picViewImage.DoubleClick
        Try
            Process.Start(mstrLocalPath)
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "picViewImage_DoubleClick", mstrModuleName)
        End Try
    End Sub
    'Nilay (02-Jun-2016) -- End


#End Region



	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try

			Call SetLanguage()
			
			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
			Me.colhName.Text = Language._Object.getCaption(CStr(Me.colhName.Tag), Me.colhName.Text)
			Me.colhDocument.Text = Language._Object.getCaption(CStr(Me.colhDocument.Tag), Me.colhDocument.Text)
			Me.colhFileName.Text = Language._Object.getCaption(CStr(Me.colhFileName.Tag), Me.colhFileName.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class