﻿'************************************************************************************************************************************
'Class Name :frmTableCloning.vb
'Purpose    :Allow User to Copy Master Tables Data from Different Transaction Tables
'Date       :28-Nov-2017
'Written By :Sandeep J Sharma
'Modified   :
'************************************************************************************************************************************
#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmTableCloning

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmTableCloning"
    Private objTabClone As clstables_cloning = Nothing
    Dim dicDestinationDatabases As Dictionary(Of Integer, String)
    Dim dicDestinationStartDate As Dictionary(Of Integer, String)
    Dim dicItemsDetails As Dictionary(Of String, List(Of clstables_cloning.clsItemCollection))

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim dtCombo As DataTable
        Try
            dtCombo = objTabClone.GetSourceDatabase("List", True)
            With cboCompany
                .ValueMember = "id"
                .DisplayMember = "cname"
                .DataSource = dtCombo
                .SelectedValue = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillItems()
        Dim dtTable As New DataTable
        Try
            dtTable = objTabClone.GetItemsInformation("List")
            lvItemInformation.Items.Clear()
            For Each row As DataRow In dtTable.Rows
                Dim lvitem As New ListViewItem

                lvitem.Text = ""

                lvitem.SubItems.Add(row("cgid").ToString)
                lvitem.SubItems.Add(row("cgname").ToString)
                lvitem.SubItems.Add(row("tablename").ToString)
                lvitem.SubItems.Add(row("itemname").ToString)
                lvitem.SubItems(colhItemList.Index).Tag = row("itemid").ToString
                lvitem.SubItems.Add(row("mtypeid").ToString)

                lvitem.Tag = row("itemname").ToString() & "|" & row("cgid").ToString

                lvItemInformation.Items.Add(lvitem)
            Next

            lvItemInformation.GroupingColumn = objcolhIGrp
            lvItemInformation.DisplayGroups(True)

            If lvItemInformation.Groups.Count > 4 Then
                colhItemList.Width = 310 - 20
            Else
                colhItemList.Width = 310
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillItems", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub FillDatabase(ByVal intCompanyId As Integer)
        Dim dtTable As New DataTable
        Try
            dtTable = objTabClone.GetDestinationDatabases("List", intCompanyId)
            lvDestinationInfo.Items.Clear()
            For Each row As DataRow In dtTable.Rows
                Dim lvitem As New ListViewItem

                lvitem.Text = ""

                lvitem.SubItems.Add(row("cname").ToString())
                lvitem.SubItems.Add(row("DBname").ToString())
                lvitem.SubItems.Add(row("sdate").ToString())
                lvitem.Tag = row("yearunkid").ToString()

                lvDestinationInfo.Items.Add(lvitem)

            Next

            lvDestinationInfo.GroupingColumn = objcolhDGrp
            lvDestinationInfo.DisplayGroups(True)

            If lvDestinationInfo.Groups.Count > 4 Then
                colhDestination.Width = 290 - 20
            Else
                colhDestination.Width = 290
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillDatabase", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Function IsValidInfo() As Boolean
        Try
            If CInt(cboCompany.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Sorry, Company is mandatory information. Please select company to continue."), enMsgBoxStyle.Information)
                cboCompany.Focus()
                Return False
            End If
            If lvDestinationInfo.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, Destination database are mandatory information. Please check atleast one destination database continue."), enMsgBoxStyle.Information)
                lvDestinationInfo.Focus()
                Return False
            End If
            If lvItemInformation.CheckedItems.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Sorry, Items are mandatory information. Please check atleast one item to continue."), enMsgBoxStyle.Information)
                lvItemInformation.Focus()
                Return False
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidInfo", mstrModuleName)
        Finally
        End Try
        Return True
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmTableCloning_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            objTabClone = New clstables_cloning
            Call FillCombo()
            Call FillItems()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmTableCloning_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clstables_cloning.SetMessages()
            objfrm._Other_ModuleNames = "clstables_cloning"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStart.Click
        Try
            Me.SplitContainer1.Panel1.Enabled = False
            Me.Cursor = Cursors.WaitCursor
            If IsValidInfo() = False Then Exit Sub
            If lvDestinationInfo.CheckedItems.Count > 0 Then
                dicDestinationDatabases = lvDestinationInfo.CheckedItems.Cast(Of ListViewItem)().ToDictionary(Function(x) Convert.ToInt32(x.Tag), Function(x) x.SubItems(colhDestination.Index).Text)
                dicDestinationStartDate = lvDestinationInfo.CheckedItems.Cast(Of ListViewItem)().ToDictionary(Function(x) Convert.ToInt32(x.Tag), Function(x) x.SubItems(objcolhsdate.Index).Text)
            End If
            If lvItemInformation.CheckedItems.Count > 0 Then
                dicItemsDetails = New Dictionary(Of String, List(Of clstables_cloning.clsItemCollection))
                For Each item As ListViewItem In lvItemInformation.CheckedItems
                    If dicItemsDetails.ContainsKey(item.Tag) = False Then
                        Dim ic As New List(Of clstables_cloning.clsItemCollection)
                        ic.Add(New clstables_cloning.clsItemCollection(item.SubItems(objcolhtablename.Index).Text, _
                                                                                CInt(item.SubItems(objcolhMtypeId.Index).Text), _
                                                                                CInt(item.SubItems(objcolhcgid.Index).Text), _
                                                                                CInt(item.SubItems(colhItemList.Index).Tag)))
                        dicItemsDetails.Add(item.Tag.ToString, ic)
                    End If
                Next
            End If

            If objTabClone.CloneItems(txtDatabaseName.Text, dicDestinationDatabases, dicDestinationStartDate, dicItemsDetails, tspbOverallProgress, tspbCurrProgress, objOvrProgress, objCurrProgress) = False Then
                eZeeMsgBox.Show(objTabClone._InfoMessage, enMsgBoxStyle.Information)
            Else
                eZeeMsgBox.Show("Data Copied Successfully!!", enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnStart_Click", mstrModuleName)
        Finally
            Me.SplitContainer1.Panel1.Enabled = True
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " Combobox Event "

    Private Sub cboCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        Try
            If CInt(cboCompany.SelectedValue) > 0 Then
                Dim tmp() As DataRow = CType(cboCompany.DataSource, DataTable).Select("id = '" & CInt(cboCompany.SelectedValue) & "'")
                If tmp.Length > 0 Then
                    txtDatabaseName.Text = tmp(0)("DBname").ToString()
                End If
                Call FillDatabase(CInt(cboCompany.SelectedValue))
            Else
                txtDatabaseName.Text = ""
                lvDestinationInfo.Items.Clear()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompany_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " ListView Event(s) "

    Private Sub lvDestinationInfo_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvDestinationInfo.ItemChecked
        Try
            RemoveHandler objchkDCheck.CheckedChanged, AddressOf objchkDCheck_CheckedChanged
            If lvDestinationInfo.CheckedItems.Count <= 0 Then
                objchkDCheck.CheckState = CheckState.Unchecked
            ElseIf lvDestinationInfo.CheckedItems.Count < lvDestinationInfo.Items.Count Then
                objchkDCheck.CheckState = CheckState.Indeterminate
            ElseIf lvDestinationInfo.CheckedItems.Count = lvDestinationInfo.Items.Count Then
                objchkDCheck.CheckState = CheckState.Checked
            End If
            AddHandler objchkDCheck.CheckedChanged, AddressOf objchkDCheck_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvDestinationInfo_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub lvItemInformation_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lvItemInformation.ItemChecked
        Try
            RemoveHandler objChkICheck.CheckedChanged, AddressOf objChkICheck_CheckedChanged
            If lvItemInformation.CheckedItems.Count <= 0 Then
                objChkICheck.CheckState = CheckState.Unchecked
            ElseIf lvItemInformation.CheckedItems.Count < lvItemInformation.Items.Count Then
                objChkICheck.CheckState = CheckState.Indeterminate
            ElseIf lvItemInformation.CheckedItems.Count = lvItemInformation.Items.Count Then
                objChkICheck.CheckState = CheckState.Checked
            End If
            AddHandler objChkICheck.CheckedChanged, AddressOf objChkICheck_CheckedChanged
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvItemInformation_ItemChecked", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Checkbox Event(s) "

    Private Sub objChkICheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkICheck.CheckedChanged
        Try
            RemoveHandler lvItemInformation.ItemChecked, AddressOf lvItemInformation_ItemChecked
            For Each lvitem As ListViewItem In lvItemInformation.Items
                lvitem.Checked = objChkICheck.Checked
            Next
            AddHandler lvItemInformation.ItemChecked, AddressOf lvItemInformation_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkICheck_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub objchkDCheck_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objchkDCheck.CheckedChanged
        Try
            RemoveHandler lvDestinationInfo.ItemChecked, AddressOf lvDestinationInfo_ItemChecked
            For Each lvitem As ListViewItem In lvDestinationInfo.Items
                lvitem.Checked = objchkDCheck.Checked
            Next
            AddHandler lvDestinationInfo.ItemChecked, AddressOf lvDestinationInfo_ItemChecked
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkDCheck_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbCompanyInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbCompanyInfo.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbItemInformation.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbItemInformation.ForeColor = GUI._eZeeContainerHeaderForeColor

            Me.gbDestinationInfo.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbDestinationInfo.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnStart.GradientBackColor = GUI._ButttonBackColor
            Me.btnStart.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnStop.GradientBackColor = GUI._ButttonBackColor
            Me.btnStop.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnStart.Text = Language._Object.getCaption(Me.btnStart.Name, Me.btnStart.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnStop.Text = Language._Object.getCaption(Me.btnStop.Name, Me.btnStop.Text)
            Me.lblSelectCompany.Text = Language._Object.getCaption(Me.lblSelectCompany.Name, Me.lblSelectCompany.Text)
            Me.lblDatabase.Text = Language._Object.getCaption(Me.lblDatabase.Name, Me.lblDatabase.Text)
            Me.gbCompanyInfo.Text = Language._Object.getCaption(Me.gbCompanyInfo.Name, Me.gbCompanyInfo.Text)
            Me.gbItemInformation.Text = Language._Object.getCaption(Me.gbItemInformation.Name, Me.gbItemInformation.Text)
            Me.gbDestinationInfo.Text = Language._Object.getCaption(Me.gbDestinationInfo.Name, Me.gbDestinationInfo.Text)
            Me.colhDestination.Text = Language._Object.getCaption(CStr(Me.colhDestination.Tag), Me.colhDestination.Text)
            Me.colhItemList.Text = Language._Object.getCaption(CStr(Me.colhItemList.Tag), Me.colhItemList.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class

'dicItemsDetails = lvItemInformation.CheckedItems.Cast(Of ListViewItem)().ToDictionary(Function(item) item.Tag.ToString(), Function(item) item.SubItems.Cast(Of ListViewItem.ListViewSubItem)().Select(Function(si) si.Text).ToList())