﻿Option Strict On

#Region " Improts "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmErrorLog

#Region " Private Variables "
    Private Const mstrModuleName As String = "frmErrorLog"
    Private objError As clsErrorlog_Tran
    Private mstrDatabaseName As String = String.Empty

#End Region

#Region " Display Dialog "

    Public Function displayDialog(Optional ByVal strDatabaseName As String = "") As Boolean
        Try
            mstrDatabaseName = strDatabaseName

            Me.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        Finally
        End Try
    End Function

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Try
           
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillList()
        Dim mlstError As List(Of clsErrorlog_Tran)
        Try

            If User._Object.Privilege._AllowToViewSystemErrorLog = False Then Exit Try

            Cursor.Current = Cursors.WaitCursor

            Dim dtStart As Date = Nothing
            Dim dtEnd As Date = Nothing
            If dtpFromDate.Checked = True Then
                dtStart = dtpFromDate.Value
            End If
            If dtpTodate.Checked = True Then
                dtEnd = dtpTodate.Value
            End If
            If chkOnlyArchived.Checked = True Then
                colhArchivedBy.Visible = True
                colhArchivedDate.Visible = True
            Else
                colhArchivedBy.Visible = False
                colhArchivedDate.Visible = False
            End If

            RemoveHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged
            objchkSelectAll.Checked = False
            AddHandler objchkSelectAll.CheckedChanged, AddressOf objchkSelectAll_CheckedChanged

            mlstError = objError.GetListCollection(xDataOp:=Nothing _
                                                   , intUnkId:=0 _
                                                   , strDatabaseName:=mstrDatabaseName _
                                                   , blnOnlyArchived:=chkOnlyArchived.Checked _
                                                   , blnOnlySentEmail:=chkOnlyEmailSent.Checked _
                                                   , dtFromDate:=dtStart _
                                                   , dtToDate:=dtEnd _
                                                   )

            objcolhCheck.DataPropertyName = "_IsChecked"
            objcolhErrorlogunkid.DataPropertyName = "_Errorlogunkid"
            colhErrorMessage.DataPropertyName = "_Error_MessageDecrypted"
            colhError_Location.DataPropertyName = "_Error_Location"
            colhError_Date.DataPropertyName = "_Error_Date"
            colhDBVersion.DataPropertyName = "_Database_Version"
            colhIsEmailSent.DataPropertyName = "_Isemailsent"
            colhIsFromWeb.DataPropertyName = "_Isweb"
            colhUserName.DataPropertyName = "_UserName"
            colhIsESS.DataPropertyName = "_IsESS"
            colhArchivedBy.DataPropertyName = "_ArchivedByUserName"
            colhArchivedDate.DataPropertyName = "_Voiddatetime"
            colhHost.DataPropertyName = "_Host"
            colhIP.DataPropertyName = "_Ip"

            With dgvError
                .AutoGenerateColumns = False

                .DataSource = mlstError
                .Refresh()
            End With


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            Call objbtnSearch.ShowResult(CStr(dgvError.RowCount))
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub SetVisibility()
        Try
            btnArchive.Enabled = User._Object.Privilege._AllowToArchiveSystemErrorLog
            btnSendEmail.Enabled = User._Object.Privilege._AllowToSendSystemErrorLogEmail
            btnExport.Enabled = User._Object.Privilege._AllowToExportSystemErrorLog

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmErrorLog_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objError = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmErrorLog_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmErrorLog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objError = New clsErrorlog_Tran
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call FillCombo()
            Call SetVisibility()
            'FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmErrorLog_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmErrorLog_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        Try
            If Me.Width >= 1100 Then
                colhErrorMessage.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            Else
                colhErrorMessage.AutoSizeMode = DataGridViewAutoSizeColumnMode.None
                colhErrorMessage.Width = 200
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmErrorLog_SizeChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsErrorlog_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsErrorlog_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Checkbox Events "

    Private Sub chkOnlyArchived_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOnlyArchived.CheckedChanged
        Try
            Call FillList()
            If chkOnlyArchived.Checked = True Then
                btnArchive.Visible = False
            Else
                btnArchive.Visible = True
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "chkOnlyArchived_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub chkOnlyEmailSent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOnlyEmailSent.CheckedChanged
        Try
            Call FillList()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "chkOnlyEmailSent_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub objchkSelectAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles objchkSelectAll.CheckedChanged
        Try
            If dgvError.DataSource Is Nothing Then Exit Try

            Dim lstError As List(Of clsErrorlog_Tran) = (From p In CType(dgvError.DataSource, List(Of clsErrorlog_Tran)) Where (p._IsChecked = Not objchkSelectAll.Checked) Select (p)).ToList
            lstError.ForEach(Function(x) updt(x, objchkSelectAll.Checked))
            dgvError.Refresh()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objchkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub

    Private Function updt(ByVal itm As clsErrorlog_Tran, ByVal blnCheck As Boolean) As Boolean
        Try
            itm._IsChecked = blnCheck

            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "updt", mstrModuleName)
        End Try
    End Function
#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnSendEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendEmail.Click
        Try
            If dgvError.DataSource Is Nothing Then Exit Try

            Dim lstError As List(Of clsErrorlog_Tran) = (From p In CType(dgvError.DataSource, List(Of clsErrorlog_Tran)) Where (p._IsChecked = True) Select (p)).ToList
            If lstError.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please tick atleast one transaction to perform further operation on it."), enMsgBoxStyle.Information)
                dgvError.Focus()
                Exit Try
            End If

            If gobjEmailList.Count > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sending Email(s) process is in progress from other module. Please wait."), enMsgBoxStyle.Information)
                Exit Try
            End If

            objError._LoginTypeId = enLogin_Mode.DESKTOP
            objError._Form_Name = mstrModuleName
            If mstrDatabaseName.Trim.ToLower = "hrmsconfiguration" Then
                objError._ModuleRefId = 0
                If Company._Object._Companyunkid <= 0 Then
                    Company._Object._Companyunkid = lstError(0)._Companyunkid
                End If
            Else
                objError._ModuleRefId = clsSendMail.enAT_VIEW_TYPE.EMPLOYEE_MGT
            End If

            If objError.SendEmail(Nothing, lstError, mstrDatabaseName) = False Then
                If objError._Message <> "" Then
                    eZeeMsgBox.Show(objError._Message, enMsgBoxStyle.Information)
                End If
            Else
                Call FillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnSendEmail_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnArchive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnArchive.Click
        Try
            If dgvError.DataSource Is Nothing Then Exit Try

            Dim lstError As List(Of clsErrorlog_Tran) = (From p In CType(dgvError.DataSource, List(Of clsErrorlog_Tran)) Where (p._IsChecked = True AndAlso p._Isvoid = False) Select (p)).ToList
            If lstError.Count <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please tick atleast one transaction to perform further operation on it."), enMsgBoxStyle.Information)
                dgvError.Focus()
                Exit Try
            End If

            If (eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Are you sure you want to Archive selected errors?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No) Then
                Exit Try
            End If

            If objError.Delete(Nothing, lstError, User._Object._Userunkid, ConfigParameter._Object._CurrentDateAndTime, "", mstrDatabaseName) = False Then

            Else
                Call FillList()
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnArchive_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim dlgSaveFile As New SaveFileDialog
        Try
            If dgvError.DataSource Is Nothing Then Exit Try

            dlgSaveFile.InitialDirectory = ConfigParameter._Object._ExportDataPath
            dlgSaveFile.Filter = "Excel files(*.xlsx)|*.xlsx"
            dlgSaveFile.FilterIndex = 0

            If dlgSaveFile.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim lstError As List(Of clsErrorlog_Tran) = (From p In CType(dgvError.DataSource, List(Of clsErrorlog_Tran)) Select (p)).ToList

                Call ExportGenericListToExcel(lstError, "ErrorLog", dlgSaveFile.FileName)

                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Error log exported successfully."), enMsgBoxStyle.Information)

                Process.Start(dlgSaveFile.FileName)

            End If


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            Call FillList()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnReset.Click
        Try
            chkOnlyArchived.Checked = False
            chkOnlyEmailSent.Checked = False
            dtpFromDate.Value = DateAndTime.Now
            dtpTodate.Value = DateAndTime.Now
            dtpFromDate.Checked = True
            dtpTodate.Checked = True

            Call FillList()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub



#End Region

    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()

            Call SetLanguage()

            Me.gbFilterCriteria.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.gbFilterCriteria.ForeColor = GUI._eZeeContainerHeaderForeColor



            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnSendEmail.GradientBackColor = GUI._ButttonBackColor
            Me.btnSendEmail.GradientForeColor = GUI._ButttonFontColor

            Me.btnExport.GradientBackColor = GUI._ButttonBackColor
            Me.btnExport.GradientForeColor = GUI._ButttonFontColor

            Me.btnArchive.GradientBackColor = GUI._ButttonBackColor
            Me.btnArchive.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnSendEmail.Text = Language._Object.getCaption(Me.btnSendEmail.Name, Me.btnSendEmail.Text)
            Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
            Me.btnArchive.Text = Language._Object.getCaption(Me.btnArchive.Name, Me.btnArchive.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.gbFilterCriteria.Text = Language._Object.getCaption(Me.gbFilterCriteria.Name, Me.gbFilterCriteria.Text)
            Me.chkOnlyEmailSent.Text = Language._Object.getCaption(Me.chkOnlyEmailSent.Name, Me.chkOnlyEmailSent.Text)
            Me.chkOnlyArchived.Text = Language._Object.getCaption(Me.chkOnlyArchived.Name, Me.chkOnlyArchived.Text)
            Me.colhErrorMessage.HeaderText = Language._Object.getCaption(Me.colhErrorMessage.Name, Me.colhErrorMessage.HeaderText)
            Me.colhError_Location.HeaderText = Language._Object.getCaption(Me.colhError_Location.Name, Me.colhError_Location.HeaderText)
            Me.colhError_Date.HeaderText = Language._Object.getCaption(Me.colhError_Date.Name, Me.colhError_Date.HeaderText)
            Me.colhDBVersion.HeaderText = Language._Object.getCaption(Me.colhDBVersion.Name, Me.colhDBVersion.HeaderText)
            Me.colhIsFromWeb.HeaderText = Language._Object.getCaption(Me.colhIsFromWeb.Name, Me.colhIsFromWeb.HeaderText)
            Me.colhIsEmailSent.HeaderText = Language._Object.getCaption(Me.colhIsEmailSent.Name, Me.colhIsEmailSent.HeaderText)
            Me.colhHost.HeaderText = Language._Object.getCaption(Me.colhHost.Name, Me.colhHost.HeaderText)
            Me.colhIP.HeaderText = Language._Object.getCaption(Me.colhIP.Name, Me.colhIP.HeaderText)
            Me.lblTodate.Text = Language._Object.getCaption(Me.lblTodate.Name, Me.lblTodate.Text)
            Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please tick atleast one transaction to perform further operation on it.")
            Language.setMessage(mstrModuleName, 2, "Sending Email(s) process is in progress from other module. Please wait.")
            Language.setMessage(mstrModuleName, 3, "Are you sure you want to Archive selected errors?")
            Language.setMessage(mstrModuleName, 4, "Error log exported successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>


    
End Class