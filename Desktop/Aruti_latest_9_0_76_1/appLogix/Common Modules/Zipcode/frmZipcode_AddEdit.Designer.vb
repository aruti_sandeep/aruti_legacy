﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmZipcode_AddEdit
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmZipcode_AddEdit))
        Me.lblState = New System.Windows.Forms.Label
        Me.cboState = New System.Windows.Forms.ComboBox
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.lblZipcode = New System.Windows.Forms.Label
        Me.pnlZipcode = New System.Windows.Forms.Panel
        Me.gbZipcode = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnAddCity = New eZee.Common.eZeeGradientButton
        Me.objbtnAddState = New eZee.Common.eZeeGradientButton
        Me.cboCity = New System.Windows.Forms.ComboBox
        Me.lblCity = New System.Windows.Forms.Label
        Me.txtZipcodeNo = New eZee.TextBox.AlphanumericTextBox
        Me.txtZipCode = New eZee.TextBox.AlphanumericTextBox
        Me.lblCode = New System.Windows.Forms.Label
        Me.cboCountry = New System.Windows.Forms.ComboBox
        Me.lblCountry = New System.Windows.Forms.Label
        Me.objFooter.SuspendLayout()
        Me.pnlZipcode.SuspendLayout()
        Me.gbZipcode.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblState
        '
        Me.lblState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblState.Location = New System.Drawing.Point(8, 63)
        Me.lblState.Name = "lblState"
        Me.lblState.Size = New System.Drawing.Size(71, 14)
        Me.lblState.TabIndex = 209
        Me.lblState.Text = "State"
        Me.lblState.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboState
        '
        Me.cboState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboState.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboState.FormattingEnabled = True
        Me.cboState.Location = New System.Drawing.Point(86, 59)
        Me.cboState.Name = "cboState"
        Me.cboState.Size = New System.Drawing.Size(185, 21)
        Me.cboState.TabIndex = 2
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Controls.Add(Me.btnClose)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 187)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(326, 55)
        Me.objFooter.TabIndex = 11
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(128, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 6
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(224, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 7
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lblZipcode
        '
        Me.lblZipcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblZipcode.Location = New System.Drawing.Point(8, 145)
        Me.lblZipcode.Name = "lblZipcode"
        Me.lblZipcode.Size = New System.Drawing.Size(75, 14)
        Me.lblZipcode.TabIndex = 206
        Me.lblZipcode.Text = "Zipcode No"
        Me.lblZipcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pnlZipcode
        '
        Me.pnlZipcode.Controls.Add(Me.gbZipcode)
        Me.pnlZipcode.Controls.Add(Me.objFooter)
        Me.pnlZipcode.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlZipcode.Location = New System.Drawing.Point(0, 0)
        Me.pnlZipcode.Name = "pnlZipcode"
        Me.pnlZipcode.Size = New System.Drawing.Size(326, 242)
        Me.pnlZipcode.TabIndex = 2
        '
        'gbZipcode
        '
        Me.gbZipcode.BorderColor = System.Drawing.Color.Black
        Me.gbZipcode.Checked = False
        Me.gbZipcode.CollapseAllExceptThis = False
        Me.gbZipcode.CollapsedHoverImage = Nothing
        Me.gbZipcode.CollapsedNormalImage = Nothing
        Me.gbZipcode.CollapsedPressedImage = Nothing
        Me.gbZipcode.CollapseOnLoad = False
        Me.gbZipcode.Controls.Add(Me.objbtnAddCity)
        Me.gbZipcode.Controls.Add(Me.objbtnAddState)
        Me.gbZipcode.Controls.Add(Me.cboCity)
        Me.gbZipcode.Controls.Add(Me.lblCity)
        Me.gbZipcode.Controls.Add(Me.cboState)
        Me.gbZipcode.Controls.Add(Me.lblState)
        Me.gbZipcode.Controls.Add(Me.txtZipcodeNo)
        Me.gbZipcode.Controls.Add(Me.lblZipcode)
        Me.gbZipcode.Controls.Add(Me.txtZipCode)
        Me.gbZipcode.Controls.Add(Me.lblCode)
        Me.gbZipcode.Controls.Add(Me.cboCountry)
        Me.gbZipcode.Controls.Add(Me.lblCountry)
        Me.gbZipcode.ExpandedHoverImage = Nothing
        Me.gbZipcode.ExpandedNormalImage = Nothing
        Me.gbZipcode.ExpandedPressedImage = Nothing
        Me.gbZipcode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbZipcode.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbZipcode.HeaderHeight = 25
        Me.gbZipcode.HeaderMessage = ""
        Me.gbZipcode.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbZipcode.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbZipcode.HeightOnCollapse = 0
        Me.gbZipcode.LeftTextSpace = 0
        Me.gbZipcode.Location = New System.Drawing.Point(8, 8)
        Me.gbZipcode.Name = "gbZipcode"
        Me.gbZipcode.OpenHeight = 91
        Me.gbZipcode.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbZipcode.ShowBorder = True
        Me.gbZipcode.ShowCheckBox = False
        Me.gbZipcode.ShowCollapseButton = False
        Me.gbZipcode.ShowDefaultBorderColor = True
        Me.gbZipcode.ShowDownButton = False
        Me.gbZipcode.ShowHeader = True
        Me.gbZipcode.Size = New System.Drawing.Size(309, 172)
        Me.gbZipcode.TabIndex = 12
        Me.gbZipcode.Temp = 0
        Me.gbZipcode.Text = "Zipcode"
        Me.gbZipcode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnAddCity
        '
        Me.objbtnAddCity.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddCity.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddCity.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddCity.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddCity.BorderSelected = False
        Me.objbtnAddCity.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddCity.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Add
        Me.objbtnAddCity.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddCity.Location = New System.Drawing.Point(277, 87)
        Me.objbtnAddCity.Name = "objbtnAddCity"
        Me.objbtnAddCity.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddCity.TabIndex = 215
        '
        'objbtnAddState
        '
        Me.objbtnAddState.BackColor = System.Drawing.Color.Transparent
        Me.objbtnAddState.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnAddState.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnAddState.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnAddState.BorderSelected = False
        Me.objbtnAddState.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnAddState.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Add
        Me.objbtnAddState.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnAddState.Location = New System.Drawing.Point(277, 60)
        Me.objbtnAddState.Name = "objbtnAddState"
        Me.objbtnAddState.Size = New System.Drawing.Size(21, 21)
        Me.objbtnAddState.TabIndex = 214
        '
        'cboCity
        '
        Me.cboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCity.FormattingEnabled = True
        Me.cboCity.Location = New System.Drawing.Point(86, 86)
        Me.cboCity.Name = "cboCity"
        Me.cboCity.Size = New System.Drawing.Size(185, 21)
        Me.cboCity.TabIndex = 3
        '
        'lblCity
        '
        Me.lblCity.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCity.Location = New System.Drawing.Point(8, 90)
        Me.lblCity.Name = "lblCity"
        Me.lblCity.Size = New System.Drawing.Size(72, 14)
        Me.lblCity.TabIndex = 212
        Me.lblCity.Text = "City"
        Me.lblCity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtZipcodeNo
        '
        Me.txtZipcodeNo.Flags = 0
        Me.txtZipcodeNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZipcodeNo.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtZipcodeNo.Location = New System.Drawing.Point(86, 141)
        Me.txtZipcodeNo.MaxLength = 15
        Me.txtZipcodeNo.Name = "txtZipcodeNo"
        Me.txtZipcodeNo.Size = New System.Drawing.Size(212, 21)
        Me.txtZipcodeNo.TabIndex = 5
        '
        'txtZipCode
        '
        Me.txtZipCode.Flags = 0
        Me.txtZipCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZipCode.InvalidChars = New Char() {Global.Microsoft.VisualBasic.ChrW(37), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(42), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(43), Global.Microsoft.VisualBasic.ChrW(63), Global.Microsoft.VisualBasic.ChrW(62), Global.Microsoft.VisualBasic.ChrW(60), Global.Microsoft.VisualBasic.ChrW(58), Global.Microsoft.VisualBasic.ChrW(92)}
        Me.txtZipCode.Location = New System.Drawing.Point(86, 114)
        Me.txtZipCode.Name = "txtZipCode"
        Me.txtZipCode.Size = New System.Drawing.Size(111, 21)
        Me.txtZipCode.TabIndex = 4
        '
        'lblCode
        '
        Me.lblCode.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCode.Location = New System.Drawing.Point(8, 118)
        Me.lblCode.Name = "lblCode"
        Me.lblCode.Size = New System.Drawing.Size(75, 14)
        Me.lblCode.TabIndex = 88
        Me.lblCode.Text = "Code"
        '
        'cboCountry
        '
        Me.cboCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCountry.FormattingEnabled = True
        Me.cboCountry.Location = New System.Drawing.Point(86, 32)
        Me.cboCountry.Name = "cboCountry"
        Me.cboCountry.Size = New System.Drawing.Size(212, 21)
        Me.cboCountry.TabIndex = 1
        '
        'lblCountry
        '
        Me.lblCountry.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCountry.Location = New System.Drawing.Point(8, 36)
        Me.lblCountry.Name = "lblCountry"
        Me.lblCountry.Size = New System.Drawing.Size(71, 14)
        Me.lblCountry.TabIndex = 85
        Me.lblCountry.Text = "Country"
        Me.lblCountry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmZipcode_AddEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(326, 242)
        Me.Controls.Add(Me.pnlZipcode)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmZipcode_AddEdit"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Add/Edit Zipcode"
        Me.objFooter.ResumeLayout(False)
        Me.pnlZipcode.ResumeLayout(False)
        Me.gbZipcode.ResumeLayout(False)
        Me.gbZipcode.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblState As System.Windows.Forms.Label
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents cboState As System.Windows.Forms.ComboBox
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents lblZipcode As System.Windows.Forms.Label
    Friend WithEvents pnlZipcode As System.Windows.Forms.Panel
    Friend WithEvents gbZipcode As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents txtZipCode As eZee.TextBox.AlphanumericTextBox
    Friend WithEvents cboCountry As System.Windows.Forms.ComboBox
    Friend WithEvents lblCountry As System.Windows.Forms.Label
    Friend WithEvents lblCode As System.Windows.Forms.Label
    Friend WithEvents cboCity As System.Windows.Forms.ComboBox
    Friend WithEvents lblCity As System.Windows.Forms.Label
    Friend WithEvents objbtnAddCity As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnAddState As eZee.Common.eZeeGradientButton
    Friend WithEvents txtZipcodeNo As eZee.TextBox.AlphanumericTextBox
End Class
