﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data


Public Class frmJVPosting

#Region " Private Variable "

    Private ReadOnly mstrModuleName As String = "frmJVPosting"
    Private mstrSearchText As String = ""

#End Region

#Region " Private Methods "

    Private Sub FillCombo()
        Dim objPeriod As New clscommom_period_Tran
        Dim objMaster As New clsMasterData
        Dim intFisrtOpenPeriod As Integer = 0
        Dim dsCombo As DataSet
        Try
            intFisrtOpenPeriod = objMaster.getFirstPeriodID(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, enStatusType.OPEN, False, True)
            dsCombo = objPeriod.getListForCombo(enModuleReference.Payroll, FinancialYear._Object._YearUnkid, FinancialYear._Object._DatabaseName, FinancialYear._Object._Database_Start_Date, "List", True)

            With cboPeriod
                .DisplayMember = "name"
                .ValueMember = "periodunkid"
                .DataSource = dsCombo.Tables(0)
                .SelectedValue = intFisrtOpenPeriod
            End With

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        Finally
            objPeriod = Nothing
            objMaster = Nothing
        End Try
    End Sub

    Private Sub FillList()
        Dim objJVPosting As New clsJVPosting_Tran
        Dim dsList As DataSet
        Try

            If User._Object.Privilege._AllowToViewJVPosting = True Then

                dsList = objJVPosting.GetList("List", CInt(cboPeriod.SelectedValue))

                objdgcolhJVPostingtranunkid.DataPropertyName = "jvpostingtranunkid"
                dgcolhPeriodName.DataPropertyName = "period_name"
                'Sohail (10 Apr 2020) -- Start
                'NMB Enhancement # : Allow to post Flex Cube JV on any allocation.
                'dgcolhBranchCode.DataPropertyName = "BracnhCode"
                'dgcolhBranchName.DataPropertyName = "BracnhName"
                dgcolhAllocationByName.DataPropertyName = "AllocationByName"
                dgcolhAllocationTranName.DataPropertyName = "AllocationTranName"
                'Sohail (10 Apr 2020) -- End
                dgcolhBatchNo.DataPropertyName = "batchno"

                dgvJVPosting.AutoGenerateColumns = False
                dgvJVPosting.DataSource = dsList.Tables(0)
                dgvJVPosting.Refresh()
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillList", mstrModuleName)
        Finally
            objJVPosting = Nothing
        End Try
    End Sub

    Private Sub SetDefaultSearchText(ByVal cbo As ComboBox)
        Try
            mstrSearchText = Language.getMessage(mstrModuleName, 1, "Type to Search")
            With cbo
                .ForeColor = Color.Gray
                .Text = mstrSearchText
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Italic)
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetDefaultSearchText", mstrModuleName)
        End Try
    End Sub

    Private Sub SetRegularFont(ByVal cbo As ComboBox)
        Try
            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)
                .SelectionLength = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetRegularFont", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Form's Event "

    Private Sub frmJVPosting_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmJVPosting_FormClosed", mstrModuleName)
        End Try
    End Sub

    Private Sub frmJVPosting_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call FillCombo()


        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmJVPosting_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            'clsJVPosting_Tran.SetMessages()
            objfrm._Other_ModuleNames = "clsJVPosting_Tran"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Combobox Events "

    Private Sub cboPeriod_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.GotFocus
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            With cbo
                .ForeColor = Color.Black
                .Font = New Font(Me.Font.FontFamily, Me.Font.Size, FontStyle.Regular)

                If .Text = mstrSearchText Then
                    .Text = ""
                End If
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_GotFocus", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cboPeriod.KeyPress
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try

            If (CInt(AscW(e.KeyChar)) >= 65 AndAlso CInt(AscW(e.KeyChar)) <= 90) Or (CInt(AscW(e.KeyChar)) >= 97 AndAlso CInt(AscW(e.KeyChar)) <= 122) Or (CInt(AscW(e.KeyChar)) >= 47 AndAlso CInt(AscW(e.KeyChar)) <= 57) Then
                Dim frm As New frmCommonSearch
                With frm
                    .ValueMember = cbo.ValueMember
                    .DisplayMember = cbo.DisplayMember
                    .DataSource = CType(cbo.DataSource, DataTable)
                    .CodeMember = "code"

                End With
                Dim c As Char = Convert.ToChar(e.KeyChar)
                frm.TypedText = c.ToString
                If frm.DisplayDialog Then
                    cbo.SelectedValue = frm.SelectedValue
                    e.KeyChar = ChrW(Keys.ShiftKey)
                Else
                    cbo.Text = ""
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.Leave
        Dim cbo As ComboBox = CType(sender, ComboBox)

        Try

            If CInt(cbo.SelectedValue) <= 0 Then
                Call SetDefaultSearchText(cbo)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_Leave", mstrModuleName)
        End Try
    End Sub

    Private Sub cboPeriod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPeriod.SelectedIndexChanged
        Dim cbo As ComboBox = CType(sender, ComboBox)
        Try
            If CInt(cbo.SelectedValue) < 0 Then
                Call SetDefaultSearchText(cbo)
            Else
                Call SetRegularFont(cbo)
            End If

            Call FillList()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, cbo.Name & "_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

    
	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbGroupList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbGroupList.ForeColor = GUI._eZeeContainerHeaderForeColor 


			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbGroupList.Text = Language._Object.getCaption(Me.gbGroupList.Name, Me.gbGroupList.Text)
			Me.lblPeriod.Text = Language._Object.getCaption(Me.lblPeriod.Name, Me.lblPeriod.Text)
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.dgcolhPeriodName.HeaderText = Language._Object.getCaption(Me.dgcolhPeriodName.Name, Me.dgcolhPeriodName.HeaderText)
			Me.dgcolhAllocationByName.HeaderText = Language._Object.getCaption(Me.dgcolhAllocationByName.Name, Me.dgcolhAllocationByName.HeaderText)
			Me.dgcolhAllocationTranName.HeaderText = Language._Object.getCaption(Me.dgcolhAllocationTranName.Name, Me.dgcolhAllocationTranName.HeaderText)
			Me.dgcolhBatchNo.HeaderText = Language._Object.getCaption(Me.dgcolhBatchNo.Name, Me.dgcolhBatchNo.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Type to Search")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class