﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmFullBackUp
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFullBackUp))
        Me.pnlMainInfo = New System.Windows.Forms.Panel
        Me.dgvBackup = New System.Windows.Forms.DataGridView
        Me.objdgcolhCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.dgcolhName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhPath = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objdgcolhIsGrp = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objdgcolhGrpId = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.pnlMessage = New System.Windows.Forms.Panel
        Me.lblMessage = New System.Windows.Forms.Label
        Me.btnBackup = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblDatabaseName = New System.Windows.Forms.Label
        Me.pbProgress = New System.Windows.Forms.ProgressBar
        Me.tlpContainer = New System.Windows.Forms.TableLayoutPanel
        Me.pnlMainInfo.SuspendLayout()
        CType(Me.dgvBackup, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.objFooter.SuspendLayout()
        Me.pnlMessage.SuspendLayout()
        Me.tlpContainer.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMainInfo
        '
        Me.pnlMainInfo.Controls.Add(Me.dgvBackup)
        Me.pnlMainInfo.Controls.Add(Me.objFooter)
        Me.pnlMainInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMainInfo.Location = New System.Drawing.Point(0, 0)
        Me.pnlMainInfo.Name = "pnlMainInfo"
        Me.pnlMainInfo.Size = New System.Drawing.Size(493, 451)
        Me.pnlMainInfo.TabIndex = 0
        '
        'dgvBackup
        '
        Me.dgvBackup.AllowUserToAddRows = False
        Me.dgvBackup.AllowUserToDeleteRows = False
        Me.dgvBackup.AllowUserToResizeColumns = False
        Me.dgvBackup.AllowUserToResizeRows = False
        Me.dgvBackup.BackgroundColor = System.Drawing.Color.WhiteSmoke
        Me.dgvBackup.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgvBackup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvBackup.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objdgcolhCheck, Me.dgcolhName, Me.dgcolhPath, Me.objdgcolhIsGrp, Me.objdgcolhGrpId})
        Me.dgvBackup.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvBackup.Location = New System.Drawing.Point(0, 0)
        Me.dgvBackup.Name = "dgvBackup"
        Me.dgvBackup.RowHeadersVisible = False
        Me.dgvBackup.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBackup.Size = New System.Drawing.Size(493, 396)
        Me.dgvBackup.TabIndex = 1
        '
        'objdgcolhCheck
        '
        Me.objdgcolhCheck.HeaderText = ""
        Me.objdgcolhCheck.Name = "objdgcolhCheck"
        Me.objdgcolhCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.objdgcolhCheck.Width = 25
        '
        'dgcolhName
        '
        Me.dgcolhName.HeaderText = "Database Name"
        Me.dgcolhName.Name = "dgcolhName"
        Me.dgcolhName.ReadOnly = True
        Me.dgcolhName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.dgcolhName.Width = 180
        '
        'dgcolhPath
        '
        Me.dgcolhPath.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhPath.HeaderText = "Backup Path"
        Me.dgcolhPath.Name = "dgcolhPath"
        Me.dgcolhPath.ReadOnly = True
        Me.dgcolhPath.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'objdgcolhIsGrp
        '
        Me.objdgcolhIsGrp.HeaderText = "IsGrp"
        Me.objdgcolhIsGrp.Name = "objdgcolhIsGrp"
        Me.objdgcolhIsGrp.Visible = False
        '
        'objdgcolhGrpId
        '
        Me.objdgcolhGrpId.HeaderText = "GrpId"
        Me.objdgcolhGrpId.Name = "objdgcolhGrpId"
        Me.objdgcolhGrpId.Visible = False
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.pnlMessage)
        Me.objFooter.Controls.Add(Me.btnBackup)
        Me.objFooter.Controls.Add(Me.btnCancel)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 396)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(493, 55)
        Me.objFooter.TabIndex = 0
        '
        'pnlMessage
        '
        Me.pnlMessage.AutoScroll = True
        Me.pnlMessage.Controls.Add(Me.lblMessage)
        Me.pnlMessage.Location = New System.Drawing.Point(3, 6)
        Me.pnlMessage.Name = "pnlMessage"
        Me.pnlMessage.Size = New System.Drawing.Size(270, 43)
        Me.pnlMessage.TabIndex = 7
        '
        'lblMessage
        '
        Me.lblMessage.AutoSize = True
        Me.lblMessage.Location = New System.Drawing.Point(3, 3)
        Me.lblMessage.Name = "lblMessage"
        Me.lblMessage.Size = New System.Drawing.Size(0, 13)
        Me.lblMessage.TabIndex = 6
        Me.lblMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnBackup
        '
        Me.btnBackup.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBackup.BackColor = System.Drawing.Color.White
        Me.btnBackup.BackgroundImage = CType(resources.GetObject("btnBackup.BackgroundImage"), System.Drawing.Image)
        Me.btnBackup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnBackup.BorderColor = System.Drawing.Color.Empty
        Me.btnBackup.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnBackup.FlatAppearance.BorderSize = 0
        Me.btnBackup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBackup.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBackup.ForeColor = System.Drawing.Color.Black
        Me.btnBackup.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnBackup.GradientForeColor = System.Drawing.Color.Black
        Me.btnBackup.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBackup.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnBackup.Location = New System.Drawing.Point(279, 13)
        Me.btnBackup.Name = "btnBackup"
        Me.btnBackup.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnBackup.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnBackup.Size = New System.Drawing.Size(98, 30)
        Me.btnBackup.TabIndex = 5
        Me.btnBackup.Text = "&Backup"
        Me.btnBackup.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(383, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(98, 30)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "&Close"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'objlblDatabaseName
        '
        Me.objlblDatabaseName.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objlblDatabaseName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblDatabaseName.Location = New System.Drawing.Point(4, 1)
        Me.objlblDatabaseName.Name = "objlblDatabaseName"
        Me.objlblDatabaseName.Size = New System.Drawing.Size(485, 22)
        Me.objlblDatabaseName.TabIndex = 0
        Me.objlblDatabaseName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'pbProgress
        '
        Me.pbProgress.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbProgress.Location = New System.Drawing.Point(4, 27)
        Me.pbProgress.Name = "pbProgress"
        Me.pbProgress.Size = New System.Drawing.Size(485, 15)
        Me.pbProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        Me.pbProgress.TabIndex = 0
        '
        'tlpContainer
        '
        Me.tlpContainer.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.tlpContainer.ColumnCount = 1
        Me.tlpContainer.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpContainer.Controls.Add(Me.pbProgress, 0, 1)
        Me.tlpContainer.Controls.Add(Me.objlblDatabaseName, 0, 0)
        Me.tlpContainer.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.tlpContainer.Location = New System.Drawing.Point(0, 451)
        Me.tlpContainer.Name = "tlpContainer"
        Me.tlpContainer.RowCount = 2
        Me.tlpContainer.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22.0!))
        Me.tlpContainer.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpContainer.Size = New System.Drawing.Size(493, 46)
        Me.tlpContainer.TabIndex = 2
        '
        'frmFullBackUp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(493, 497)
        Me.Controls.Add(Me.pnlMainInfo)
        Me.Controls.Add(Me.tlpContainer)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmFullBackUp"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Back Up Database"
        Me.pnlMainInfo.ResumeLayout(False)
        CType(Me.dgvBackup, System.ComponentModel.ISupportInitialize).EndInit()
        Me.objFooter.ResumeLayout(False)
        Me.pnlMessage.ResumeLayout(False)
        Me.pnlMessage.PerformLayout()
        Me.tlpContainer.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMainInfo As System.Windows.Forms.Panel
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnBackup As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents dgvBackup As System.Windows.Forms.DataGridView
    Friend WithEvents objdgcolhCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents dgcolhName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhPath As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objdgcolhIsGrp As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents objdgcolhGrpId As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents lblMessage As System.Windows.Forms.Label
    Friend WithEvents pnlMessage As System.Windows.Forms.Panel
    Friend WithEvents objlblDatabaseName As System.Windows.Forms.Label
    Friend WithEvents pbProgress As System.Windows.Forms.ProgressBar
    Friend WithEvents tlpContainer As System.Windows.Forms.TableLayoutPanel
End Class
