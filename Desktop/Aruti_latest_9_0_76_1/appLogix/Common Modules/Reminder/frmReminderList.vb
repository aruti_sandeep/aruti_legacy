﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

'Last Message Index = 5

Public Class frmReminderList

#Region "Private Variable"

    Private objReminderMaster As clsreminder_master
    Private ReadOnly mstrModuleName As String = "frmReminderList"

#End Region

#Region "Form's Event"

    Private Sub frmReminderList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objReminderMaster = New clsreminder_master
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            fillList()

            'S.SANDEEP [ 29 OCT 2012 ] -- START
            'ENHANCEMENT : TRA CHANGES
            Call SetVisibility()
            'S.SANDEEP [ 29 OCT 2012 ] -- END

            If lvReminderList.Items.Count > 0 Then lvReminderList.Items(0).Selected = True
            lvReminderList.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReminderList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmReminderList_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvReminderList.Focused = True Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmReminderList_KeyUp", mstrModuleName)
        End Try
    End Sub

    Private Sub frmReminderList_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objReminderMaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsreminder_master.SetMessages()
            objfrm._Other_ModuleNames = "clsreminder_master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region "Button's Event"

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            Dim objfrmRemider_AddEdit As New frmReminder_AddEdit
            If objfrmRemider_AddEdit.displayDialog(-1, enAction.ADD_CONTINUE) Then
                fillList()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Try
            If lvReminderList.SelectedItems.Count < 1 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Reminder from the list to perform further operation."), enMsgBoxStyle.Information) '?1
                lvReminderList.Select()
                Exit Sub
            End If
            Dim objfrmRemider_AddEdit As New frmReminder_AddEdit
            Try
                Dim intSelectedIndex As Integer
                intSelectedIndex = lvReminderList.SelectedItems(0).Index
                If objfrmRemider_AddEdit.displayDialog(CInt(lvReminderList.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                    Call fillList()
                End If
                objfrmRemider_AddEdit = Nothing

                lvReminderList.Items(intSelectedIndex).Selected = True
                lvReminderList.EnsureVisible(intSelectedIndex)
                lvReminderList.Select()
            Catch ex As Exception
                Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
            Finally
                If objfrmRemider_AddEdit IsNot Nothing Then objfrmRemider_AddEdit.Dispose()
            End Try
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvReminderList.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select Reminder from the list to perform further operation."), enMsgBoxStyle.Information) '?1
            lvReminderList.Select()
            Exit Sub
        End If
        'If objReminderMaster.isUsed(CInt(lvReminderList.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this Reminder. Reason: This Reminder is in use."), enMsgBoxStyle.Information) '?2
        '    lvReminderList.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvReminderList.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this Reminder?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objReminderMaster.Delete(CInt(lvReminderList.SelectedItems(0).Tag))
                lvReminderList.SelectedItems(0).Remove()

                If lvReminderList.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvReminderList.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvReminderList.Items.Count - 1
                    lvReminderList.Items(intSelectedIndex).Selected = True
                    lvReminderList.EnsureVisible(intSelectedIndex)
                ElseIf lvReminderList.Items.Count <> 0 Then
                    lvReminderList.Items(intSelectedIndex).Selected = True
                    lvReminderList.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvReminderList.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region " Private Methods "

    Private Sub fillList()
        Dim dsReminderList As New DataSet
        Try
            dsReminderList = objReminderMaster.GetList("List")

            Dim lvItem As ListViewItem

            lvReminderList.Items.Clear()
            For Each drRow As DataRow In dsReminderList.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("start_date").ToString
                lvItem.Tag = drRow("reminderunkid")
                lvItem.SubItems.Add(drRow("title").ToString)
                lvItem.SubItems.Add(drRow("reminder_type").ToString)
                lvItem.SubItems.Add(drRow("iscomplete").ToString)
                lvReminderList.Items.Add(lvItem)
            Next

            If lvReminderList.Items.Count > 16 Then
                colhComplete.Width = 85 - 18
            Else
                colhComplete.Width = 85
            End If

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsReminderList.Dispose()
        End Try
    End Sub

    'S.SANDEEP [ 29 OCT 2012 ] -- START
    'ENHANCEMENT : TRA CHANGES
    Private Sub SetVisibility()
        Try
            btnNew.Enabled = User._Object.Privilege._AllowtoAddGeneralReminder
            btnEdit.Enabled = User._Object.Privilege._AllowtoEditGeneralReminder
            btnDelete.Enabled = User._Object.Privilege._AllowtoDeleteGeneralReminder
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "SetVisibility", mstrModuleName)
        End Try
    End Sub
    'S.SANDEEP [ 29 OCT 2012 ] -- END

#End Region


    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
           
            Call SetLanguage()

         
            Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1
            Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2
            Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor
            Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor
            Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor


            Me.btnDelete.GradientBackColor = GUI._ButttonBackColor
            Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

            Me.btnEdit.GradientBackColor = GUI._ButttonBackColor
            Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

            Me.btnNew.GradientBackColor = GUI._ButttonBackColor
            Me.btnNew.GradientForeColor = GUI._ButttonFontColor

            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title", Me.eZeeHeader.Title)
            Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message", Me.eZeeHeader.Message)
            Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
            Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
            Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.colhDateAndTime.Text = Language._Object.getCaption(CStr(Me.colhDateAndTime.Tag), Me.colhDateAndTime.Text)
            Me.colhTitle.Text = Language._Object.getCaption(CStr(Me.colhTitle.Tag), Me.colhTitle.Text)
            Me.colhType.Text = Language._Object.getCaption(CStr(Me.colhType.Tag), Me.colhType.Text)
            Me.colhComplete.Text = Language._Object.getCaption(CStr(Me.colhComplete.Tag), Me.colhComplete.Text)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "Please select Reminder from the list to perform further operation.")
            Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this Reminder?")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class