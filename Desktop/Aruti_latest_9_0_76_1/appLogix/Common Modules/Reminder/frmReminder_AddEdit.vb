﻿Option Strict On

#Region " Imports "
Imports eZeeCommonLib
Imports Aruti.Data
#End Region

'Last Message Index = 5

Public Class frmReminder_AddEdit

#Region "Private Variable"

    Private ReadOnly mstrModuleName As String = "frmRemider_AddEdit"
    Private mblnCancel As Boolean = True
    Private objRemindermaster As clsreminder_master
    Private menAction As enAction = enAction.ADD_ONE
    Private mintreminderMasterUnkid As Integer = -1

    Private mstrTitleName As String = String.Empty
    Private mdtStartDate As Date = Nothing

    Private m_Dataview As DataView
#End Region

#Region " Properties "

    Public WriteOnly Property _TitleName() As String
        Set(ByVal value As String)
            mstrTitleName = value
        End Set
    End Property

    Public WriteOnly Property _StartDate() As Date
        Set(ByVal value As Date)
            mdtStartDate = Value
        End Set
    End Property

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction) As Boolean
        Try
            mintreminderMasterUnkid = intUnkId
            menAction = eAction

            Me.ShowDialog()

            intUnkId = mintreminderMasterUnkid

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " Form's Event "

    Private Sub frmRemider_AddEdit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objRemindermaster = New clsreminder_master
        Try
            Call Set_Logo(Me, gApplicationType)
            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 

            setColor()
            If menAction = enAction.EDIT_ONE Then
                objRemindermaster._Reminderunkid = mintreminderMasterUnkid
            End If
            Call FillCombo()
            Call FillUser_List()
            GetValue()
            txtName.Select()
            If mintreminderMasterUnkid = -1 Then
                txtName.Text = mstrTitleName
                'Sandeep [ 09 Oct 2010 ] -- Start
                'Issues Reported by Vimal
                'dtpStartDate.Value = mdtStartDate
                dtpStartDate.Value = CDate(IIf(mdtStartDate = Nothing, Now, mdtStartDate))
                'Sandeep [ 09 Oct 2010 ] -- End 
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRemider_AddEdit_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmRemider_AddEdit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles MyBase.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                SendKeys.Send("{TAB}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRemider_AddEdit_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmRemider_AddEdit_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Try
            If e.Control = True And e.KeyCode = Keys.S Then
                btnSave_Click(sender, e)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmRemider_AddEdit_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmRemider_AddEdit_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        objRemindermaster = Nothing
    End Sub
    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsreminder_master.SetMessages()
            objfrm._Other_ModuleNames = "clsreminder_master"

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 
#End Region

#Region " Button's Event "

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim blnFlag As Boolean = False
        Try

            If IsValidate() = False Then Exit Sub

            Call SetValue()

            If menAction = enAction.EDIT_ONE Then
                blnFlag = objRemindermaster.Update()
            Else
                blnFlag = objRemindermaster.Insert()
            End If

            If blnFlag = False And objRemindermaster._Message <> "" Then
                eZeeMsgBox.Show(objRemindermaster._Message, enMsgBoxStyle.Information)
            End If

            If blnFlag Then
                mblnCancel = False
                If menAction = enAction.ADD_CONTINUE Then
                    objRemindermaster = Nothing
                    objRemindermaster = New clsreminder_master
                    Call GetValue()
                    txtName.Focus()
                Else
                    mintreminderMasterUnkid = objRemindermaster._Reminderunkid
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    'Sandeep [ 23 Oct 2010 ] -- Start
    Private Sub objbtnAddType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnAddType.Click
        Try
            Dim intRefId As Integer = -1
            Dim frm As New frmReminderType_AddEdit

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If
            'Anjan (02 Sep 2011)-End 

            frm.displayDialog(intRefId, enAction.ADD_ONE)
            If intRefId > -1 Then
                Dim objReminderType As New clsReminderType
                Dim dsList As New DataSet
                dsList = objReminderType.getComboList("Type", True)
                With cboType
                    .ValueMember = "Id"
                    .DisplayMember = "RType"
                    .DataSource = dsList.Tables("Type")
                    .SelectedValue = intRefId
                End With
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnAddType_Click", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 23 Oct 2010 ] -- End 

#End Region

#Region " Private Methods "

    Private Sub setColor()
        Try
            txtName.BackColor = GUI.ColorComp
            cboType.BackColor = GUI.ColorComp
            cboPriority.BackColor = GUI.ColorComp
            txtMessage.BackColor = GUI.ColorComp
            cboInterval.BackColor = GUI.ColorComp
            nudRecurr.BackColor = GUI.ColorOptional
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "setColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try
            txtName.Text = objRemindermaster._Title
            'Sandeep [ 21 Aug 2010 ] -- Start
            'cboType.Text = objRemindermaster._Reminder_Type
            cboType.SelectedValue = objRemindermaster._Reminder_Type
            'Sandeep [ 21 Aug 2010 ] -- End 
            cboPriority.SelectedIndex = objRemindermaster._Priority
            txtMessage.Text = objRemindermaster._ReminderMessage
            cboInterval.SelectedIndex = objRemindermaster._Interval_Type

            If objRemindermaster._Reminderunkid > 0 Then
                dtpStartDate.Value = objRemindermaster._Start_Date.Date
                dtpStartTime.Value = objRemindermaster._Start_Date
                If objRemindermaster._Stopreminder.HasValue Then
                    chkStopRecurring.Checked = True
                    dtRecurringDate.Value = objRemindermaster._Stopreminder.Value
                End If
                If objRemindermaster._Frequency > 0 Then
                    nudRecurr.Value = objRemindermaster._Frequency
                Else
                    nudRecurr.Value = nudRecurr.Value
                End If
            Else
                dtpStartDate.Value = Now.Date
                dtpStartTime.Value = Now
                dtRecurringDate.Value = Now
            End If

            Dim strid As String() = objRemindermaster._UserUnkIds.Replace(" ", "").Split(CChar(","))

            Dim index As Integer
            'For Each lvitem As ListViewItem In lvUserList.Items
            '    index = Array.IndexOf(strid, lvitem.Tag.ToString)

            '    If index > 0 Then
            '        lvitem.Checked = True
            '    Else
            '        lvitem.Checked = False
            '    End If
            'Next
            For Each dtRow As DataRow In m_Dataview.Table.Rows
                index = Array.IndexOf(strid, dtRow.Item("userunkid").ToString)

                If index > 0 Then
                    dtRow.Item("Checked") = True
                Else
                    dtRow.Item("Checked") = False
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try
            Dim mstrdate As String = String.Empty

            objRemindermaster._Title = txtName.Text.Trim

            mstrdate = dtpStartDate.Value.Date.ToString("dd-MMM-yyyy") & " "
            mstrdate &= dtpStartTime.Value.ToString("hh:mm:ss tt")
            objRemindermaster._Start_Date = CDate(mstrdate)
            'Sandeep [ 21 Aug 2010 ] -- Start
            'objRemindermaster._Reminder_Type = cboType.Text.Trim
            objRemindermaster._Reminder_Type = CInt(cboType.SelectedValue)
            'Sandeep [ 21 Aug 2010 ] -- End 
            objRemindermaster._Priority = CInt(cboPriority.SelectedIndex)
            objRemindermaster._ReminderMessage = txtMessage.Text.Trim
            objRemindermaster._Interval_Type = CInt(cboInterval.SelectedIndex)


            'S.SANDEEP [ 12 OCT 2011 ] -- START
            'ENHANCEMENT : AUDIT TRAIL MAINTENANCE
            objRemindermaster._Userunkid = User._Object._Userunkid
            'S.SANDEEP [ 12 OCT 2011 ] -- END 


            If chkStopRecurring.Checked Then
                objRemindermaster._Stopreminder = CDate(dtRecurringDate.Value.Date & " " & dtRecurringDate.Value.ToString("HH:mm:ss"))
            Else
                objRemindermaster._Stopreminder = Nothing
            End If

            If nudRecurr.Visible = True Then
                objRemindermaster._Frequency = CInt(nudRecurr.Value)
            Else
                objRemindermaster._Frequency = 0
            End If


            Dim strid As String = "0"

            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'For Each lvitem As ListViewItem In lvUserList.Items
            '    If lvitem.Checked Then
            '        strid &= ", " & CStr(lvitem.Tag)
            '    End If
            'Next
            For Each dtRow As DataRow In m_Dataview.Table.Rows
                If CBool(dtRow.Item("Checked")) = True Then
                    strid &= ", " & CInt(dtRow.Item("userunkid"))
                End If
            Next
            'Sohail (06 Jan 2012) -- End
            objRemindermaster._UserUnkIds = strid

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 21 Aug 2010 ] -- Start
    Private Sub FillCombo()

        'Sandeep [ 23 Oct 2010 ] -- Start
        'Dim objCommonMaster As New clsCommon_Master
        Dim objReminderType As New clsReminderType
        'Sandeep [ 23 Oct 2010 ] -- End 
        Dim dsList As New DataSet
        Try


            'Sandeep [ 23 Oct 2010 ] -- Start
            'Issue : Remider Type New Table
            'dsList = objCommonMaster.getComboList(clsCommon_Master.enCommonMaster.REMINDER_TYPE, True, "ReminderType")
            'With cboType
            '    .ValueMember = "masterunkid"
            '    .DisplayMember = "name"
            '    .DataSource = dsList.Tables("ReminderType")
            '    .SelectedValue = 0
            'End With
            dsList = objReminderType.getComboList("ReminderType", True)
            With cboType
                .ValueMember = "Id"
                .DisplayMember = "RType"
                .DataSource = dsList.Tables("ReminderType")
                .SelectedValue = 0
            End With
            'Sandeep [ 23 Oct 2010 ] -- End 

            With cboPriority
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 6, "Select Priority"))
                .Items.Add(Language.getMessage(mstrModuleName, 7, "Low"))
                .Items.Add(Language.getMessage(mstrModuleName, 8, "Medium"))
                .Items.Add(Language.getMessage(mstrModuleName, 9, "High"))
                .SelectedIndex = 0
            End With

            With cboInterval
                .Items.Clear()
                .Items.Add(Language.getMessage(mstrModuleName, 21, "Once"))
                .Items.Add(Language.getMessage(mstrModuleName, 11, "Daily"))
                .Items.Add(Language.getMessage(mstrModuleName, 12, "Weekly"))
                .Items.Add(Language.getMessage(mstrModuleName, 13, "Monthly"))
                .Items.Add(Language.getMessage(mstrModuleName, 15, "Yearly"))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillUser_List()
        Dim dsUser As New DataSet
        Dim objUser As New clsUserAddEdit
        Try
            ' dsUser = objUser.GetList("USER", True)
            'S.SANDEEP [24 SEP 2015] -- START
            'Dim objOption As New clsPassowdOptions
            'If objOption._UserLogingModeId = enAuthenticationMode.BASIC_AUTHENTICATION Then
            '    'S.SANDEEP [ 01 DEC 2012 ] -- START
            '    'ENHANCEMENT : TRA CHANGES
            '    'dsList = objUserAddEdit.getComboList("List", True, False)
            '    Dim objPswd As New clsPassowdOptions
            '    If objPswd._IsEmployeeAsUser Then
            '        dsUser = objUser.GetList("List", True, False, True)
            '    Else
            '        dsUser = objUser.GetList("List", True, False)
            '    End If
            '    objPswd = Nothing
            '    'S.SANDEEP [ 01 DEC 2012 ] -- END
            'ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_BASIC_AUTHENTICATION Then
            '    dsUser = objUser.GetList("List", True, True)
            '    'dsUser (21-Jun-2012) -- Start
            '    'Enhancement : TRA Changes
            'ElseIf objOption._UserLogingModeId = enAuthenticationMode.AD_SSO_AUTHENTICATION Then
            '    dsUser = objUser.GetList("List", True, True)
            '    'Pinkal (21-Jun-2012) -- End
            'End If

            dsUser = objUser.GetList("List")
            'S.SANDEEP [24 SEP 2015] -- END


            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'Dim lvItem As ListViewItem

            'For Each dtRow As DataRow In dsUser.Tables("USER").Rows
            '    lvItem = New ListViewItem

            '    lvItem.Text = dtRow.Item("username").ToString

            '    lvItem.Tag = dtRow.Item("userunkid")

            '    lvUserList.Items.Add(lvItem)

            '    lvItem = Nothing
            'Next
            Dim dCol As DataColumn = New DataColumn("Checked")
            dCol.DataType = System.Type.GetType("System.Boolean")
            dCol.DefaultValue = False

            dsUser.Tables("List").Columns.Add(dCol)
            dgvUserList.AutoGenerateColumns = False

            colhCheck.DataPropertyName = "Checked"
            colhUserUnkId.DataPropertyName = "userunkid"
            colhUserName.DataPropertyName = "username"
            colhFirstName.DataPropertyName = "firstname"
            colhLastName.DataPropertyName = "lastname"

            m_Dataview = New DataView(dsUser.Tables("List"))
            dgvUserList.DataSource = m_Dataview
            dgvUserList.Refresh()
            'Sohail (06 Jan 2012) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillUser_List", mstrModuleName)
        End Try
    End Sub

    Private Function IsValidate() As Boolean
        Try
            If Trim(txtName.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Reminder Name cannot be blank. Reminder Name is required information."), enMsgBoxStyle.Information)
                txtName.Focus()
                Return False
            ElseIf cboType.Text.Trim = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Reminder Type is compulsory information.Please Select Reminder Type."), enMsgBoxStyle.Information)
                cboType.Select()
                Return False
            ElseIf CInt(cboPriority.SelectedIndex) = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Reminder Priority is compulsory information.Please Select Reminder Priority."), enMsgBoxStyle.Information)
                cboPriority.Select()
                Return False
            ElseIf Trim(txtMessage.Text) = "" Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Reminder Message cannot be blank. Reminder Message is required information."), enMsgBoxStyle.Information)
                txtMessage.Focus()
                Return False
                'Sandeep [ 01 FEB 2011 ] -- START
                'ElseIf CInt(cboInterval.SelectedIndex) = 0 Then
                '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Reminder Interval is compulsory information.Please Select Reminder Interval."), enMsgBoxStyle.Information)
                '    cboInterval.Select()
                '    Return False
                'Sandeep [ 01 FEB 2011 ] -- END 
            End If

            Return True

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValidate", mstrModuleName)
        End Try
    End Function

    Private Sub Whichaction(ByVal intintervaltype As Integer)
        Try
            Select Case intintervaltype
                Case 0     'Once
                    objlblRecurring.Text = Language.getMessage(mstrModuleName, 22, "This is One time reminder. there is no reoccurence option.")
                    chkStopRecurring.Enabled = False
                    dtRecurringDate.Enabled = (chkStopRecurring.Checked And chkStopRecurring.Enabled)

                    objlblOption.Text = ""
                    objlblOption.Visible = False

                    nudRecurr.Enabled = False
                    nudRecurr.Visible = False
                Case 1     'Daily
                    objlblRecurring.Text = Language.getMessage(mstrModuleName, 23, "The daily reminder will start at the time mentioned above and will reoccur every day.")

                    chkStopRecurring.Enabled = True
                    dtRecurringDate.Enabled = (chkStopRecurring.Checked And chkStopRecurring.Enabled)

                    objlblOption.Text = Language.getMessage(mstrModuleName, 16, "Day(s)")
                    objlblOption.Visible = True

                    nudRecurr.Enabled = True
                    nudRecurr.Visible = True
                Case 2     'Weekly
                    objlblRecurring.Text = Language.getMessage(mstrModuleName, 14, "The weekly reminder will start at the time mentioned above and reoccur every week.")

                    chkStopRecurring.Enabled = True
                    dtRecurringDate.Enabled = (chkStopRecurring.Checked And chkStopRecurring.Enabled)

                    objlblOption.Text = Language.getMessage(mstrModuleName, 17, "Week(s)")
                    objlblOption.Visible = True

                    nudRecurr.Enabled = True
                    nudRecurr.Visible = True
                Case 3    'Monthly
                    objlblRecurring.Text = Language.getMessage(mstrModuleName, 20, "The monthly reminder will start at the date/time mentioned above and reoccur every month.")

                    chkStopRecurring.Enabled = True
                    dtRecurringDate.Enabled = (chkStopRecurring.Checked And chkStopRecurring.Enabled)

                    objlblOption.Text = Language.getMessage(mstrModuleName, 18, "Month(s)")
                    objlblOption.Visible = True

                    nudRecurr.Enabled = True
                    nudRecurr.Visible = True
                Case 4    'Yearly
                    objlblRecurring.Text = Language.getMessage(mstrModuleName, 10, "The yearly reminder will start at the date/time indicated above and reoccur every year.")

                    chkStopRecurring.Enabled = True
                    dtRecurringDate.Enabled = (chkStopRecurring.Checked And chkStopRecurring.Enabled)

                    objlblOption.Text = Language.getMessage(mstrModuleName, 19, "Yearly(s)")
                    objlblOption.Visible = True

                    nudRecurr.Enabled = True
                    nudRecurr.Visible = True
            End Select
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Whichaction", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 21 Aug 2010 ] -- End 

#End Region

#Region " Controls "

    Private Sub chkstoprecurring_checkedchanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStopRecurring.CheckedChanged
        Try
            If chkStopRecurring.Checked = True Then
                dtRecurringDate.Enabled = True
            Else
                dtRecurringDate.Enabled = False
                dtRecurringDate.Value = Today.Date
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "chkstoprecurring_checkedchanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboInterval_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboInterval.SelectedIndexChanged
        Try
            Call Whichaction(CInt(cboInterval.SelectedIndex))
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboInterval_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    'Sandeep [ 23 Oct 2010 ] -- Start
    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        Try
            'Sohail (06 Jan 2012) -- Start
            'TRA - ENHANCEMENT
            'If chkSelectAll.CheckState = CheckState.Checked Then
            '    For i As Integer = 0 To lvUserList.Items.Count - 1
            '        lvUserList.Items(i).Checked = True
            '    Next
            'End If
            Dim strSearch As String = ""
            If txtSearchUser.Text.Length > 0 Then
                strSearch = "username LIKE '%" & txtSearchUser.Text & "%' OR firstname LIKE '%" & txtSearchUser.Text & "%' OR lastname LIKE '%" & txtSearchUser.Text & "%'"
            End If
            For Each dtRow As DataRow In m_Dataview.Table.Select(strSearch)
                dtRow.Item("Checked") = chkSelectAll.Checked
                dtRow.AcceptChanges()
                Next
            'Sohail (06 Jan 2012) -- End
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "chkSelectAll_CheckedChanged", mstrModuleName)
        End Try
    End Sub
    'Sandeep [ 23 Oct 2010 ] -- End 

    'Sohail (06 Jan 2012) -- Start
    'TRA - ENHANCEMENT
    Private Sub txtSearchUser_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchUser.TextChanged
        Try
            If txtSearchUser.Text.Length > 0 Then
                m_Dataview.RowFilter = "username LIKE '%" & txtSearchUser.Text & "%' OR firstname LIKE '%" & txtSearchUser.Text & "%' OR lastname LIKE '%" & txtSearchUser.Text & "%'"
            Else
                m_Dataview.RowFilter = ""
            End If
            If chkSelectAll.Checked = True Then chkSelectAll.Checked = False
            For Each dtRow As DataRow In m_Dataview.Table.Rows
                dtRow.Item("Checked") = False
                dtRow.AcceptChanges()
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchUser_TextChanged", mstrModuleName)
        End Try
    End Sub
    'Sohail (06 Jan 2012) -- End

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
		
			Call SetLanguage()
			
			Me.gbReminderDetails.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbReminderDetails.ForeColor = GUI._eZeeContainerHeaderForeColor 

			Me.gbUser.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbUser.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbReminderDetails.Text = Language._Object.getCaption(Me.gbReminderDetails.Name, Me.gbReminderDetails.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblName.Text = Language._Object.getCaption(Me.lblName.Name, Me.lblName.Text)
			Me.lblStartDate.Text = Language._Object.getCaption(Me.lblStartDate.Name, Me.lblStartDate.Text)
			Me.lblPriority.Text = Language._Object.getCaption(Me.lblPriority.Name, Me.lblPriority.Text)
			Me.lblType.Text = Language._Object.getCaption(Me.lblType.Name, Me.lblType.Text)
			Me.lblMessage.Text = Language._Object.getCaption(Me.lblMessage.Name, Me.lblMessage.Text)
			Me.lblIntervatnadRecurr.Text = Language._Object.getCaption(Me.lblIntervatnadRecurr.Name, Me.lblIntervatnadRecurr.Text)
			Me.chkStopRecurring.Text = Language._Object.getCaption(Me.chkStopRecurring.Name, Me.chkStopRecurring.Text)
			Me.lblInterval.Text = Language._Object.getCaption(Me.lblInterval.Name, Me.lblInterval.Text)
			Me.colhDeskClerk.Text = Language._Object.getCaption(CStr(Me.colhDeskClerk.Tag), Me.colhDeskClerk.Text)
			Me.lblDeskClerk.Text = Language._Object.getCaption(Me.lblDeskClerk.Name, Me.lblDeskClerk.Text)
			Me.DataGridViewTextBoxColumn1.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn1.Name, Me.DataGridViewTextBoxColumn1.HeaderText)
			Me.DataGridViewTextBoxColumn2.HeaderText = Language._Object.getCaption(Me.DataGridViewTextBoxColumn2.Name, Me.DataGridViewTextBoxColumn2.HeaderText)
			Me.gbUser.Text = Language._Object.getCaption(Me.gbUser.Name, Me.gbUser.Text)
			Me.chkSelectAll.Text = Language._Object.getCaption(Me.chkSelectAll.Name, Me.chkSelectAll.Text)
			Me.colhCheck.HeaderText = Language._Object.getCaption(Me.colhCheck.Name, Me.colhCheck.HeaderText)
			Me.colhUserUnkId.HeaderText = Language._Object.getCaption(Me.colhUserUnkId.Name, Me.colhUserUnkId.HeaderText)
			Me.colhUserName.HeaderText = Language._Object.getCaption(Me.colhUserName.Name, Me.colhUserName.HeaderText)
			Me.colhFirstName.HeaderText = Language._Object.getCaption(Me.colhFirstName.Name, Me.colhFirstName.HeaderText)
			Me.colhLastName.HeaderText = Language._Object.getCaption(Me.colhLastName.Name, Me.colhLastName.HeaderText)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Reminder Name cannot be blank. Reminder Name is required information.")
			Language.setMessage(mstrModuleName, 2, "Reminder Type is compulsory information.Please Select Reminder Type.")
			Language.setMessage(mstrModuleName, 3, "Reminder Priority is compulsory information.Please Select Reminder Priority.")
			Language.setMessage(mstrModuleName, 4, "Reminder Message cannot be blank. Reminder Message is required information.")
			Language.setMessage(mstrModuleName, 6, "Select Priority")
			Language.setMessage(mstrModuleName, 7, "Low")
			Language.setMessage(mstrModuleName, 8, "Medium")
			Language.setMessage(mstrModuleName, 9, "High")
			Language.setMessage(mstrModuleName, 10, "The yearly reminder will start at the date/time indicated above and reoccur every year.")
			Language.setMessage(mstrModuleName, 11, "Daily")
			Language.setMessage(mstrModuleName, 12, "Weekly")
			Language.setMessage(mstrModuleName, 13, "Monthly")
			Language.setMessage(mstrModuleName, 14, "The weekly reminder will start at the time mentioned above and reoccur every week.")
			Language.setMessage(mstrModuleName, 15, "Yearly")
			Language.setMessage(mstrModuleName, 16, "Day(s)")
			Language.setMessage(mstrModuleName, 17, "Week(s)")
			Language.setMessage(mstrModuleName, 18, "Month(s)")
			Language.setMessage(mstrModuleName, 19, "Yearly(s)")
			Language.setMessage(mstrModuleName, 20, "The monthly reminder will start at the date/time mentioned above and reoccur every month.")
			Language.setMessage(mstrModuleName, 21, "Once")
			Language.setMessage(mstrModuleName, 22, "This is One time reminder. there is no reoccurence option.")
			Language.setMessage(mstrModuleName, 23, "The daily reminder will start at the time mentioned above and will reoccur every day.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class