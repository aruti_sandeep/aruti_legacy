﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmADUserMapping

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmADUserMapping"
    Private mdt_ImportData As DataTable
    Private mblnIsManager As Boolean
    Private mintYearUnkid As Integer = 0
    Private mdtEmpl As DataTable

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByVal mdtUser As DataTable, ByVal blnIsManager As Boolean) As Boolean
        Try
            mdt_ImportData = mdtUser
            mblnIsManager = blnIsManager
            If mblnIsManager = True Then
                objgbAdUserMapping.Text = Language.getMessage(mstrModuleName, 4, "Import Active Directory User(s)") & " " & Language.getMessage(mstrModuleName, 5, "As Manager")
            Else
                objgbAdUserMapping.Text = Language.getMessage(mstrModuleName, 4, "Import Active Directory User(s)") & " " & Language.getMessage(mstrModuleName, 6, "As Employee")
            End If
            Me.ShowDialog()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function

#End Region

#Region " From's Events "

    Private Sub frmADUserMapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()
            Call FillCombo()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmADUserMapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Function "

    Private Sub FillCombo()
        Dim objCompany As New clsCompany_Master
        Dim dsCombo As New DataSet
        Dim dTable As New DataTable
        Dim dRow As DataRow = Nothing
        Try
            dsCombo = objCompany.GetList("List", True)
            With cboCompany
                .ValueMember = "companyunkid"
                .DisplayMember = "name"
                .DataSource = dsCombo.Tables("List")
                .SelectedIndex = 0
            End With
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillGrid()
        Try
            dgData.AutoGenerateColumns = False
            dgcolhAdUserName.DataPropertyName = "username"
            dgcolhDisplayName.DataPropertyName = "edisplay"
            dgcolhfName.DataPropertyName = "efname"
            dgcolhlName.DataPropertyName = "elname"
            colhStatus.DataPropertyName = "estatus"
            objcolhStatusId.DataPropertyName = "estatusid"
            dgData.DataSource = mdt_ImportData
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        Finally
        End Try
    End Sub

    'Pinkal (28-Nov-2017) -- Start
    ''Enhancement -  issue # 0001617: Active Directory Integration, Common Active Directory Integration for (SUMATRA,PACRA,THPS).

    Private Function MapUserName(ByVal x As DataRow, ByVal dtEmpTab As DataTable) As Boolean
        Try
        If dtEmpTab.Rows.Count > 0 Then
                'Dim dtmp() As DataRow = dtEmpTab.Select("displayname = '" & x.Item("username").ToString & "' ")
                Dim dtmp() As DataRow = dtEmpTab.Select("displayname = '" & x.Item("username").ToString().Replace("'", "''") & "'")
            If dtmp.Length > 0 Then
                x.Item("employeeunkid") = dtmp(0).Item("employeeunkid")
                x.Item("companyunkid") = CInt(cboCompany.SelectedValue)
                x.Item("estatus") = Language.getMessage(mstrModuleName, 1, "Mapped")
                x.Item("estatusid") = 1
                x.Item("edisplay") = dtmp(0).Item("displayname")
                x.Item("efname") = dtmp(0).Item("firstname")
                x.Item("elname") = dtmp(0).Item("surname")
                x.Item("password") = clsSecurity.Decrypt(dtmp(0).Item("password").ToString(), "ezee").ToString()
                    'S.SANDEEP |02-MAR-2020| -- START
                    'ISSUE/ENHANCEMENT : ACTIVE DIRECTORY EMAIL
                    If x.Item("email").ToString().Trim().Length > 0 Then
                        x.Item("email") = dtmp(0).Item("email")
                    End If
                    'S.SANDEEP |02-MAR-2020| -- END
            Else
                x.Item("employeeunkid") = 0

                    'Pinkal (16-May-2019) -- Start
                    'Enhancement [0003816]- EXTERNAL USER CREATION FROM AD Ability to create an external user when Integrate users from ACTIVE DIRECTORY option is enabled. 
                    'x.Item("companyunkid") = CInt(cboCompany.SelectedValue)
                    x.Item("companyunkid") = 0
                    'Pinkal (16-May-2019) -- End

                x.Item("estatus") = Language.getMessage(mstrModuleName, 2, "Not Found")
                x.Item("estatusid") = 2
                x.Item("edisplay") = ""
                x.Item("efname") = ""
                x.Item("elname") = ""
                x.Item("password") = ""
            End If
        End If
        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: MapUserName; Module Name: " & mstrModuleName)
        End Try
        Return True
    End Function

    'Pinkal (28-Nov-2017) -- End

#End Region

#Region " Button's Events "

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Try
            Dim objUser As New clsUserAddEdit
            Cursor = Cursors.WaitCursor
            mdt_ImportData.AcceptChanges()
            objUser._CreatedById = User._Object._Userunkid
            objUser._Creation_Date = ConfigParameter._Object._CurrentDateAndTime
            objUser._IsManager = mblnIsManager

            'Pinkal (16-May-2019) -- Start
            'Enhancement [0003816]- EXTERNAL USER CREATION FROM AD Ability to create an external user when Integrate users from ACTIVE DIRECTORY option is enabled. 
            'objUser._EmployeeCompanyUnkid = CInt(cboCompany.SelectedValue)
            'Pinkal (16-May-2019) -- End

            objUser._AssignCompanyPrivilegeIDs = mintYearUnkid.ToString()
            Dim blnFlag As Boolean = objUser.ImportADUser(mdt_ImportData)
            If blnFlag = False And objUser._Message <> "" Then
                Cursor = Cursors.Default
                eZeeMsgBox.Show(objUser._Message, enMsgBoxStyle.Information)
            End If
            If blnFlag Then
                Cursor = Cursors.Default
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Data successfully Imported."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Me.Close()
            End If
            objUser = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnImport_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " DataGrid Event(s) "

    Private Sub dgData_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgData.DataBindingComplete
        Try
            For Each dgvRow As DataGridViewRow In dgData.Rows
                If CInt(dgvRow.Cells(objcolhStatusId.Index).Value) = 2 Then
                    dgvRow.DefaultCellStyle.ForeColor = Color.Red
                End If
            Next
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub dgData_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgData.DataError

    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboCompany_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCompany.SelectedIndexChanged
        Try
            If CInt(cboCompany.SelectedValue) > 0 Then
                Dim objEmp As New clsEmployee_Master
                Dim objMaster As New clsMasterData
                Dim dsYear As New DataSet
                dsYear = objMaster.Get_Database_Year_List("List", False, CInt(cboCompany.SelectedValue))
                If dsYear.Tables(0).Rows.Count > 0 Then
                    mintYearUnkid = CInt(dsYear.Tables(0).Rows(0).Item("yearunkid"))
                    Dim objConfig As New clsConfigOptions
                    objConfig._Companyunkid = CInt(cboCompany.SelectedValue)
                    mdtEmpl = objEmp.GetEmployeeForUser(CStr(dsYear.Tables(0).Rows(0).Item("database_name")), CInt(cboCompany.SelectedValue), User._Object._Userunkid, eZeeDate.convertDate(objConfig._EmployeeAsOnDate), mintYearUnkid, objConfig._UserAccessModeSetting, False).Tables(0)
                    objConfig = Nothing
                    mdt_ImportData.AsEnumerable().ToList.ForEach(Function(x) MapUserName(x, mdtEmpl))
                    Call FillGrid()
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCompany_SelectedIndexChanged", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
            Me.objgbAdUserMapping.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.objgbAdUserMapping.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnImport.GradientBackColor = GUI._ButttonBackColor
            Me.btnImport.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.btnImport.Text = Language._Object.getCaption(Me.btnImport.Name, Me.btnImport.Text)
            Me.objgbAdUserMapping.Text = Language._Object.getCaption(Me.objgbAdUserMapping.Name, Me.objgbAdUserMapping.Text)
            Me.lblCompany.Text = Language._Object.getCaption(Me.lblCompany.Name, Me.lblCompany.Text)
            Me.dgcolhAdUserName.HeaderText = Language._Object.getCaption(Me.dgcolhAdUserName.Name, Me.dgcolhAdUserName.HeaderText)
            Me.dgcolhDisplayName.HeaderText = Language._Object.getCaption(Me.dgcolhDisplayName.Name, Me.dgcolhDisplayName.HeaderText)
            Me.dgcolhfName.HeaderText = Language._Object.getCaption(Me.dgcolhfName.Name, Me.dgcolhfName.HeaderText)
            Me.dgcolhlName.HeaderText = Language._Object.getCaption(Me.dgcolhlName.Name, Me.dgcolhlName.HeaderText)
            Me.dgcolhEcode.HeaderText = Language._Object.getCaption(Me.dgcolhEcode.Name, Me.dgcolhEcode.HeaderText)
            Me.colhStatus.HeaderText = Language._Object.getCaption(Me.colhStatus.Name, Me.colhStatus.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Mapped")
			Language.setMessage(mstrModuleName, 2, "Not Found")
			Language.setMessage(mstrModuleName, 3, "Data successfully Imported.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class