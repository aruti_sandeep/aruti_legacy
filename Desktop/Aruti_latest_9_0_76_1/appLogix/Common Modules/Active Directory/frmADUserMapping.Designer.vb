﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmADUserMapping
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmADUserMapping))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objgbAdUserMapping = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objpnlData = New System.Windows.Forms.Panel
        Me.dgData = New System.Windows.Forms.DataGridView
        Me.objcolhImage = New System.Windows.Forms.DataGridViewImageColumn
        Me.dgcolhAdUserName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhDisplayName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhfName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhlName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgcolhEcode = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colhStatus = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.objcolhStatusId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cboCompany = New System.Windows.Forms.ComboBox
        Me.lblCompany = New System.Windows.Forms.Label
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnImport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.objgbAdUserMapping.SuspendLayout()
        Me.objpnlData.SuspendLayout()
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.objgbAdUserMapping)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(757, 457)
        Me.pnlMain.TabIndex = 0
        '
        'objgbAdUserMapping
        '
        Me.objgbAdUserMapping.BorderColor = System.Drawing.Color.Black
        Me.objgbAdUserMapping.Checked = False
        Me.objgbAdUserMapping.CollapseAllExceptThis = False
        Me.objgbAdUserMapping.CollapsedHoverImage = Nothing
        Me.objgbAdUserMapping.CollapsedNormalImage = Nothing
        Me.objgbAdUserMapping.CollapsedPressedImage = Nothing
        Me.objgbAdUserMapping.CollapseOnLoad = False
        Me.objgbAdUserMapping.Controls.Add(Me.objpnlData)
        Me.objgbAdUserMapping.Controls.Add(Me.cboCompany)
        Me.objgbAdUserMapping.Controls.Add(Me.lblCompany)
        Me.objgbAdUserMapping.Dock = System.Windows.Forms.DockStyle.Fill
        Me.objgbAdUserMapping.ExpandedHoverImage = Nothing
        Me.objgbAdUserMapping.ExpandedNormalImage = Nothing
        Me.objgbAdUserMapping.ExpandedPressedImage = Nothing
        Me.objgbAdUserMapping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbAdUserMapping.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.objgbAdUserMapping.HeaderHeight = 25
        Me.objgbAdUserMapping.HeaderMessage = ""
        Me.objgbAdUserMapping.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objgbAdUserMapping.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.objgbAdUserMapping.HeightOnCollapse = 0
        Me.objgbAdUserMapping.LeftTextSpace = 0
        Me.objgbAdUserMapping.Location = New System.Drawing.Point(0, 0)
        Me.objgbAdUserMapping.Name = "objgbAdUserMapping"
        Me.objgbAdUserMapping.OpenHeight = 300
        Me.objgbAdUserMapping.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.objgbAdUserMapping.ShowBorder = True
        Me.objgbAdUserMapping.ShowCheckBox = False
        Me.objgbAdUserMapping.ShowCollapseButton = False
        Me.objgbAdUserMapping.ShowDefaultBorderColor = True
        Me.objgbAdUserMapping.ShowDownButton = False
        Me.objgbAdUserMapping.ShowHeader = True
        Me.objgbAdUserMapping.Size = New System.Drawing.Size(757, 402)
        Me.objgbAdUserMapping.TabIndex = 69
        Me.objgbAdUserMapping.Temp = 0
        Me.objgbAdUserMapping.Text = "Import Active Directory User(s)"
        Me.objgbAdUserMapping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objpnlData
        '
        Me.objpnlData.Controls.Add(Me.dgData)
        Me.objpnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objpnlData.Location = New System.Drawing.Point(1, 25)
        Me.objpnlData.Name = "objpnlData"
        Me.objpnlData.Size = New System.Drawing.Size(755, 376)
        Me.objpnlData.TabIndex = 130
        '
        'dgData
        '
        Me.dgData.AllowUserToAddRows = False
        Me.dgData.AllowUserToDeleteRows = False
        Me.dgData.AllowUserToResizeColumns = False
        Me.dgData.AllowUserToResizeRows = False
        Me.dgData.BackgroundColor = System.Drawing.Color.White
        Me.dgData.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.objcolhImage, Me.dgcolhAdUserName, Me.dgcolhDisplayName, Me.dgcolhfName, Me.dgcolhlName, Me.dgcolhEcode, Me.colhStatus, Me.objcolhStatusId})
        Me.dgData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgData.Location = New System.Drawing.Point(0, 0)
        Me.dgData.MultiSelect = False
        Me.dgData.Name = "dgData"
        Me.dgData.ReadOnly = True
        Me.dgData.RowHeadersVisible = False
        Me.dgData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgData.Size = New System.Drawing.Size(755, 376)
        Me.dgData.TabIndex = 129
        '
        'objcolhImage
        '
        Me.objcolhImage.HeaderText = ""
        Me.objcolhImage.Name = "objcolhImage"
        Me.objcolhImage.ReadOnly = True
        Me.objcolhImage.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.objcolhImage.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.objcolhImage.Visible = False
        Me.objcolhImage.Width = 30
        '
        'dgcolhAdUserName
        '
        Me.dgcolhAdUserName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.dgcolhAdUserName.HeaderText = "Username"
        Me.dgcolhAdUserName.Name = "dgcolhAdUserName"
        Me.dgcolhAdUserName.ReadOnly = True
        '
        'dgcolhDisplayName
        '
        Me.dgcolhDisplayName.HeaderText = "Emp. Displayname"
        Me.dgcolhDisplayName.Name = "dgcolhDisplayName"
        Me.dgcolhDisplayName.ReadOnly = True
        Me.dgcolhDisplayName.Width = 125
        '
        'dgcolhfName
        '
        Me.dgcolhfName.HeaderText = "Emp. Firstname"
        Me.dgcolhfName.Name = "dgcolhfName"
        Me.dgcolhfName.ReadOnly = True
        Me.dgcolhfName.Width = 125
        '
        'dgcolhlName
        '
        Me.dgcolhlName.HeaderText = "Emp. Surname"
        Me.dgcolhlName.Name = "dgcolhlName"
        Me.dgcolhlName.ReadOnly = True
        Me.dgcolhlName.Width = 125
        '
        'dgcolhEcode
        '
        Me.dgcolhEcode.HeaderText = "Emp. Code"
        Me.dgcolhEcode.Name = "dgcolhEcode"
        Me.dgcolhEcode.ReadOnly = True
        Me.dgcolhEcode.Visible = False
        '
        'colhStatus
        '
        Me.colhStatus.HeaderText = "Status"
        Me.colhStatus.Name = "colhStatus"
        Me.colhStatus.ReadOnly = True
        Me.colhStatus.Width = 200
        '
        'objcolhStatusId
        '
        Me.objcolhStatusId.HeaderText = "objcolhStatusId"
        Me.objcolhStatusId.Name = "objcolhStatusId"
        Me.objcolhStatusId.ReadOnly = True
        Me.objcolhStatusId.Visible = False
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.DropDownWidth = 250
        Me.cboCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(489, 2)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(265, 21)
        Me.cboCompany.TabIndex = 128
        '
        'lblCompany
        '
        Me.lblCompany.BackColor = System.Drawing.Color.Transparent
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(416, 4)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(67, 17)
        Me.lblCompany.TabIndex = 127
        Me.lblCompany.Text = "Company"
        Me.lblCompany.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnImport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 402)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(757, 55)
        Me.EZeeFooter1.TabIndex = 1
        '
        'btnImport
        '
        Me.btnImport.BackColor = System.Drawing.Color.White
        Me.btnImport.BackgroundImage = CType(resources.GetObject("btnImport.BackgroundImage"), System.Drawing.Image)
        Me.btnImport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnImport.BorderColor = System.Drawing.Color.Empty
        Me.btnImport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnImport.FlatAppearance.BorderSize = 0
        Me.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImport.ForeColor = System.Drawing.Color.Black
        Me.btnImport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnImport.GradientForeColor = System.Drawing.Color.Black
        Me.btnImport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Location = New System.Drawing.Point(553, 13)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnImport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnImport.Size = New System.Drawing.Size(93, 30)
        Me.btnImport.TabIndex = 2
        Me.btnImport.Text = "&Import"
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(652, 13)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(93, 30)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmADUserMapping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(757, 457)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmADUserMapping"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Map Active Directory Users"
        Me.pnlMain.ResumeLayout(False)
        Me.objgbAdUserMapping.ResumeLayout(False)
        Me.objpnlData.ResumeLayout(False)
        CType(Me.dgData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents btnImport As eZee.Common.eZeeLightButton
    Friend WithEvents objgbAdUserMapping As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents objpnlData As System.Windows.Forms.Panel
    Friend WithEvents dgData As System.Windows.Forms.DataGridView
    Friend WithEvents objcolhImage As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents dgcolhAdUserName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhDisplayName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhfName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhlName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgcolhEcode As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colhStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents objcolhStatusId As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
