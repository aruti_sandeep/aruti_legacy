﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports System.DirectoryServices
Imports System.IO
Imports System.DirectoryServices.Protocols

Public Class frmEnableDisableADUsers

#Region " Private Variable(s) "

    Private ReadOnly mstrModuleName As String = "frmImportADUsers"
    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private dsUser As DataSet
    Dim objUser As clsUserAddEdit
    Private imgPlusIcon As Image = My.Resources.plus_blue
    Private imgMinusIcon As Image = My.Resources.minus_blue
    Private imgBlankIcon As Image = My.Resources.blankImage
    Dim dvUser As DataView = Nothing

#End Region

#Region " Private Function(s) & Method(s) "

    Private Sub FillGrid()
        Try
            dsUser = objUser.GetList("List", "hrmsConfiguration..cfuser_master.isaduser = 1")

            If dsUser IsNot Nothing Then
                If dsUser.Tables(0).Columns.Contains("ischeck") = False Then
                    dsUser.Tables(0).Columns.Add("ischeck", Type.GetType("System.Boolean"))
                End If
            End If
            If dsUser IsNot Nothing Then dvUser = dsUser.Tables(0).DefaultView

            dgAdUsers.AutoGenerateColumns = False
            colhUserName.DataPropertyName = "username"
            objdgcolhUserID.DataPropertyName = "userunkid"
            objdgcolhCheck.DataPropertyName = "ischeck"
            dgAdUsers.DataSource = dvUser
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillGrid", mstrModuleName)
        End Try
    End Sub

    Private Function IsValid() As Boolean
        Try
            If dgAdUsers.Rows.Count = 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "There is no data to do further operation."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                Return False
            End If

            If dsUser.Tables(0).Rows.Count > 0 Then
                Dim drRow As DataRow() = dsUser.Tables(0).Select("ischeck = true")
                If drRow.Length = 0 Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "User is compulsory information.Please select atleast one user to enable/disable from active directory."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                    Return False
                End If
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "IsValid", mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (01-Mar-2023) -- Start
    '(A1X-658) As a user, i want to have a port configuration option while configuring AD connection parameters As a user, i want to have a port configuration option while configuring AD conection parameters.

    'Private Function EnableDisableUser(ByVal blnEnable As Boolean, ByVal mstrUserName As String) As Boolean
    '    Try
    '        Dim objConfig As New clsConfigOptions
    '        Dim mstrADIPAddress As String = ""
    '        Dim mstrADDomain As String = ""
    '        Dim mstrADUserName As String = ""
    '        Dim mstrADUserPwd As String = ""

    '        objConfig.IsValue_Changed("ADIPAddress", "-999", mstrADIPAddress)
    '        objConfig.IsValue_Changed("ADDomain", "-999", mstrADDomain)
    '        objConfig.IsValue_Changed("ADDomainUser", "-999", mstrADUserName)
    '        objConfig.IsValue_Changed("ADDomainUserPwd", "-999", mstrADUserPwd)

    '        'Pinkal (01-Mar-2023) -- Start
    '        '(A1X-658) As a user, i want to have a port configuration option while configuring AD connection parameters As a user, i want to have a port configuration option while configuring AD conection parameters.
    '        Dim mstrADPortNo As String = ""
    '        objConfig.IsValue_Changed("ADPortNo", "-999", mstrADPortNo)
    '        'Pinkal (01-Mar-2023) -- End

    '        If mstrADUserPwd.Trim.Length > 0 Then
    '            mstrADUserPwd = clsSecurity.Decrypt(mstrADUserPwd, "ezee")
    '        End If

    '        Dim ar() As String = Nothing
    '        If mstrADDomain.Trim.Length > 0 AndAlso mstrADDomain.Trim.Contains(".") Then
    '            ar = mstrADDomain.Trim.Split(CChar("."))
    '            mstrADDomain = ""
    '            If ar.Length > 0 Then
    '                For i As Integer = 0 To ar.Length - 1
    '                    mstrADDomain &= ",DC=" & ar(i)
    '                Next
    '            End If
    '        End If

    '        If mstrADDomain.Trim.Length > 0 Then
    '            mstrADDomain = mstrADDomain.Trim.Substring(1)
    '        End If

    '        'Pinkal (01-Mar-2023) -- Start
    '        '(A1X-658) As a user, i want to have a port configuration option while configuring AD connection parameters As a user, i want to have a port configuration option while configuring AD conection parameters.
    '        Dim entry As DirectoryEntry = Nothing
    '        If mstrADPortNo.Trim.Length > 0 Then
    '            entry = New DirectoryEntry("LDAP://" & mstrADIPAddress.Trim & ":" & mstrADPortNo.Trim & "/" & mstrADDomain.Trim, mstrADUserName.Trim, mstrADUserPwd.Trim, AuthenticationTypes.Secure)
    '        Else
    '            entry = New DirectoryEntry("LDAP://" & mstrADIPAddress.Trim & "/" & mstrADDomain.Trim, mstrADUserName.Trim, mstrADUserPwd.Trim)
    '        End If
    '        'Pinkal (01-Mar-2023) -- End

    '        Dim obj As Object = entry.NativeObject
    '        Dim search As DirectorySearcher = New DirectorySearcher(entry)
    '        search.Filter = "(SAMAccountName=" & mstrUserName & ")"
    '        search.PropertiesToLoad.Add("cn")
    '        Dim result As SearchResult = search.FindOne()

    '        If result IsNot Nothing Then


    '            Dim objUserEntry As DirectoryEntry = result.GetDirectoryEntry()
    '            Dim iValue As Integer = Convert.ToInt32(objUserEntry.Properties("userAccountControl").Value)

    '            If blnEnable Then
    '                objUserEntry.Properties("userAccountControl").Value = iValue And Not &H2
    '            Else
    '                objUserEntry.Properties("userAccountControl").Value = iValue Or &H2
    '            End If

    '            objUserEntry.CommitChanges()
    '            objUserEntry.Close()

    '            Dim objUser As New clsUserAddEdit
    '            Dim xUserId As Integer = objUser.Return_UserId(mstrUserName, "hrmsconfiguration", enLoginMode.USER)
    '            objUser.EnableDisableADUser(blnEnable, xUserId, iValue, CInt(objUserEntry.Properties("userAccountControl").Value))
    '            objUser = Nothing

    '        End If


    '    Catch ex As Exception
    '        Throw New Exception(ex.Message & "; Procedure Name: EnableDisableUser; Module Name: " & mstrModuleName)
    '        Return False
    '    End Try
    '    Return True
    'End Function

    Private Function EnableDisableUser(ByVal blnEnable As Boolean, ByVal mstrUserName As String) As Boolean
        Try
            Dim objConfig As New clsConfigOptions
            Dim mstrADIPAddress As String = ""
            Dim mstrADDomain As String = ""
            Dim mstrADUserName As String = ""
            Dim mstrADUserPwd As String = ""
            Dim mstrADPortNo As String = ""

            clsADIntegration.GetADConnection(mstrADIPAddress, mstrADDomain, mstrADUserName, mstrADUserPwd, mstrADPortNo, Nothing)

            Dim mstrAttributes() As String = New String() {"userAccountControl", "SamAccountName", "cn"}

            Dim objLDAP As LdapConnection = clsADIntegration.SetADConnection(mstrADIPAddress.Trim, mstrADDomain.Trim, mstrADPortNo.Trim, mstrADUserName.Trim, mstrADUserPwd.Trim)
            Dim objSSLSearch As New SearchRequest(mstrADDomain, "(SAMAccountName=" & mstrUserName & ")", Protocols.SearchScope.Subtree, mstrAttributes)
            Dim objResponse As SearchResponse = DirectCast(objLDAP.SendRequest(objSSLSearch), SearchResponse)
            If objResponse.ResultCode = ResultCode.Success Then
                Dim srcResult As SearchResultEntry = objResponse.Entries(0)
                If srcResult.Attributes.Count > 0 Then
                    If srcResult.Attributes.Contains("userAccountControl") Then
                        Dim iValue As Integer = Convert.ToInt32(srcResult.Attributes("userAccountControl")(0))
                        Dim objModifyrequest As ModifyRequest = New ModifyRequest()
                        objModifyrequest.DistinguishedName = srcResult.DistinguishedName
                        Dim objModifyDir As DirectoryAttributeModification = New DirectoryAttributeModification()
                        objModifyDir.Operation = DirectoryAttributeOperation.Replace
                        objModifyDir.Name = "userAccountControl"
                        Dim mstrValue As String = ""
                        If blnEnable Then
                            mstrValue = CStr(iValue And Not 514)
                        Else
                            mstrValue = CStr(iValue Or 514)
            End If
                        objModifyDir.Add(mstrValue)
                        objModifyrequest.Modifications.Add(objModifyDir)

                        Dim objModifyResponse As ModifyResponse = CType(objLDAP.SendRequest(objModifyrequest), ModifyResponse)
                        If objModifyResponse.ResultCode <> ResultCode.Success Then
                            eZeeMsgBox.Show("Error : " & objModifyResponse.ErrorMessage)
                            Return False
                End If
            End If
                End If
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message & "; Procedure Name: EnableDisableUser; Module Name: " & mstrModuleName)
            Return False
        End Try
        Return True
    End Function

    'Pinkal (01-Mar-2023) -- End

#End Region

#Region " Form's Event(s) "

    Private Sub frmEnableDisableADUsers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objUser = New clsUserAddEdit

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            Call OtherSettings()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmEnableDisableADUsers_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmEnableDisableADUsers_LanguageClick(ByVal sender As Object, ByVal eventArgs As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try

            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()
            objfrm.displayDialog(Me)

            Language.Refresh()
            Call Language.setLanguage(Me)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "frmEnableDisableADUsers_LanguageClick", mstrModuleName)
        Finally
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearch.Click
        Try
            txtSearchUser.Text = ""
            If dsUser IsNot Nothing Then dsUser.Tables(0).Rows.Clear()
            Cursor = Cursors.WaitCursor
            FillGrid()
            Cursor = Cursors.Default
        Catch ex As Exception
            Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " DataGrid's Event(s) "

    Private Sub dgAdUsers_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgAdUsers.CellContentClick, dgAdUsers.CellContentDoubleClick
        Try

            If e.RowIndex <= -1 Then Exit Sub

            If Me.dgAdUsers.IsCurrentCellDirty Then
                Me.dgAdUsers.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

            dvUser.ToTable.AcceptChanges()

            RemoveHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged
            If e.ColumnIndex = objdgcolhCheck.Index Then
                Dim drRow As DataRow() = dvUser.ToTable.Select("ischeck = true")
                If drRow.Length <= 0 Then
                    objChkAll.CheckState = CheckState.Unchecked
                ElseIf drRow.Length < dgAdUsers.Rows.Count Then
                    objChkAll.CheckState = CheckState.Indeterminate
                ElseIf drRow.Length = dgAdUsers.Rows.Count Then
                    objChkAll.CheckState = CheckState.Checked
                End If
            End If
            AddHandler objChkAll.CheckedChanged, AddressOf objChkAll_CheckedChanged

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "dgAdUsers_CellContentClick", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region "ContextMenu Event"

    Private Sub mnuEnableADUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEnableADUser.Click, mnuDisableADUser.Click
        Try

            If IsValid() = False Then Exit Sub
            Dim dtUser As DataTable = New DataView(dvUser.Table(), "ischeck = true", "", DataViewRowState.CurrentRows).ToTable()

            Dim mblnFlag As Boolean = False

            If dtUser IsNot Nothing AndAlso dtUser.Rows.Count > 0 Then
                For Each dr As DataRow In dtUser.Rows
                    If CType(sender, ToolStripMenuItem).Name = mnuEnableADUser.Name Then
                        mblnFlag = EnableDisableUser(True, dr("username").ToString())
                    ElseIf CType(sender, ToolStripMenuItem).Name = mnuDisableADUser.Name Then
                        mblnFlag = EnableDisableUser(False, dr("username").ToString())
                    End If
                Next
            End If

            If mblnFlag Then
                If CType(sender, ToolStripMenuItem).Name = mnuEnableADUser.Name Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Selected user(s) enabled successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                ElseIf CType(sender, ToolStripMenuItem).Name = mnuDisableADUser.Name Then
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Selected user(s) disabled successfully."), CType(enMsgBoxStyle.Information + enMsgBoxStyle.OkOnly, enMsgBoxStyle))
                End If
            End If


        Catch ex As Exception
            Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "mnuEnableADUser_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region "TextChange Event"

    Private Sub txtSearchUser_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearchUser.TextChanged
        Try
            If dvUser IsNot Nothing Then
                dvUser.RowFilter = "username like '%" & txtSearchUser.Text.Trim & "%'"
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "txtSearchUser_TextChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    Private Sub objChkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objChkAll.CheckedChanged
        Try
            If dvUser Is Nothing Then Exit Sub
            RemoveHandler dgAdUsers.CellContentClick, AddressOf dgAdUsers_CellContentClick
            RemoveHandler dgAdUsers.CellContentDoubleClick, AddressOf dgAdUsers_CellContentClick
            For Each drow As DataRowView In dvUser
                drow.Item("ischeck") = objChkAll.Checked
            Next
            dvUser.Table.AcceptChanges()
            dgAdUsers.Refresh()
            AddHandler dgAdUsers.CellContentClick, AddressOf dgAdUsers_CellContentClick
            AddHandler dgAdUsers.CellContentDoubleClick, AddressOf dgAdUsers_CellContentClick
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objChkAll_CheckedChanged", mstrModuleName)
        Finally
        End Try
    End Sub





    '<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
    Private Sub OtherSettings()
        Try
            Me.SuspendLayout()
            Call SetLanguage()

            Me.EZeeCollapsibleContainer1.GradientColor = GUI._eZeeContainerHeaderBackColor
            Me.EZeeCollapsibleContainer1.ForeColor = GUI._eZeeContainerHeaderForeColor


            Me.btnClose.GradientBackColor = GUI._ButttonBackColor
            Me.btnClose.GradientForeColor = GUI._ButttonFontColor

            Me.btnOperation.GradientBackColor = GUI._ButttonBackColor
            Me.btnOperation.GradientForeColor = GUI._ButttonFontColor


            Me.ResumeLayout()
        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "OtherSettings", mstrModuleName)
        End Try
    End Sub


    Private Sub SetLanguage()
        Try
            Me.Text = Language._Object.getCaption(Me.Name, Me.Text)

            Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
            Me.EZeeCollapsibleContainer1.Text = Language._Object.getCaption(Me.EZeeCollapsibleContainer1.Name, Me.EZeeCollapsibleContainer1.Text)
            Me.btnOperation.Text = Language._Object.getCaption(Me.btnOperation.Name, Me.btnOperation.Text)
            Me.mnuEnableADUser.Text = Language._Object.getCaption(Me.mnuEnableADUser.Name, Me.mnuEnableADUser.Text)
            Me.mnuDisableADUser.Text = Language._Object.getCaption(Me.mnuDisableADUser.Name, Me.mnuDisableADUser.Text)
            Me.colhUserName.HeaderText = Language._Object.getCaption(Me.colhUserName.Name, Me.colhUserName.HeaderText)

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetLanguage", mstrModuleName)
        End Try
    End Sub


    Private Sub SetMessages()
        Try
            Language.setMessage(mstrModuleName, 1, "There is no data to do further operation.")
            Language.setMessage(mstrModuleName, 2, "User is compulsory information.Please select atleast one user to enable/disable from active directory.")
            Language.setMessage(mstrModuleName, 3, "Selected user(s) enabled successfully.")
            Language.setMessage(mstrModuleName, 4, "Selected user(s) disabled successfully.")

        Catch Ex As Exception
            DisplayError.Show("-1", Ex.Message, "SetMessages", mstrModuleName)
        End Try
    End Sub
#End Region 'Language & UI Settings
    '</Language>
End Class