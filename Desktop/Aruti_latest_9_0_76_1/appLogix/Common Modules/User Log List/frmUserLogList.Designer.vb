﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUserLogList
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserLogList))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.spcContainer = New System.Windows.Forms.SplitContainer
        Me.gbFilterCriteria = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.btnReset = New eZee.Common.eZeeGradientButton
        Me.objbtnSearch = New eZee.Common.eZeeGradientButton
        Me.cboFilter = New System.Windows.Forms.ComboBox
        Me.objlblCaption = New System.Windows.Forms.Label
        Me.cboViewBy = New System.Windows.Forms.ComboBox
        Me.lblViewBy = New System.Windows.Forms.Label
        Me.objeline3 = New eZee.Common.eZeeLine
        Me.radDescending = New System.Windows.Forms.RadioButton
        Me.radAscending = New System.Windows.Forms.RadioButton
        Me.objelSort = New eZee.Common.eZeeLine
        Me.cboGrouping = New System.Windows.Forms.ComboBox
        Me.lblGroupBy = New System.Windows.Forms.Label
        Me.objeline2 = New eZee.Common.eZeeLine
        Me.objeline1 = New eZee.Common.eZeeLine
        Me.btnSearch = New eZee.Common.eZeeGradientButton
        Me.lblLogOutDateTo = New System.Windows.Forms.Label
        Me.lblLogOutDateFrom = New System.Windows.Forms.Label
        Me.dtpLogoutToDate = New System.Windows.Forms.DateTimePicker
        Me.dtpLogoutFromDate = New System.Windows.Forms.DateTimePicker
        Me.lblLoginToDate = New System.Windows.Forms.Label
        Me.lblLoginFromDate = New System.Windows.Forms.Label
        Me.dtpLoginToDate = New System.Windows.Forms.DateTimePicker
        Me.dtpLoginFromDate = New System.Windows.Forms.DateTimePicker
        Me.gbUserLog = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.objbtnHide = New eZee.Common.eZeeGradientButton
        Me.objbtnShow = New eZee.Common.eZeeGradientButton
        Me.pnlData = New System.Windows.Forms.Panel
        Me.lvUserLog = New eZee.Common.eZeeListView(Me.components)
        Me.colhUserName = New System.Windows.Forms.ColumnHeader
        Me.colhLogindate = New System.Windows.Forms.ColumnHeader
        Me.colhLogoutDate = New System.Windows.Forms.ColumnHeader
        Me.colhIp = New System.Windows.Forms.ColumnHeader
        Me.colhMachine = New System.Windows.Forms.ColumnHeader
        Me.colhRemark = New System.Windows.Forms.ColumnHeader
        Me.objcolhLoginDate = New System.Windows.Forms.ColumnHeader
        Me.objcolhLogoutDate = New System.Windows.Forms.ColumnHeader
        Me.colhLoginFrom = New System.Windows.Forms.ColumnHeader
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnExport = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnClose = New eZee.Common.eZeeLightButton(Me.components)
        Me.pnlMain.SuspendLayout()
        Me.spcContainer.Panel1.SuspendLayout()
        Me.spcContainer.Panel2.SuspendLayout()
        Me.spcContainer.SuspendLayout()
        Me.gbFilterCriteria.SuspendLayout()
        Me.gbUserLog.SuspendLayout()
        Me.pnlData.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.spcContainer)
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(839, 444)
        Me.pnlMain.TabIndex = 0
        '
        'spcContainer
        '
        Me.spcContainer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.spcContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.spcContainer.IsSplitterFixed = True
        Me.spcContainer.Location = New System.Drawing.Point(0, 0)
        Me.spcContainer.Margin = New System.Windows.Forms.Padding(0)
        Me.spcContainer.Name = "spcContainer"
        '
        'spcContainer.Panel1
        '
        Me.spcContainer.Panel1.Controls.Add(Me.gbFilterCriteria)
        '
        'spcContainer.Panel2
        '
        Me.spcContainer.Panel2.Controls.Add(Me.gbUserLog)
        Me.spcContainer.Size = New System.Drawing.Size(839, 394)
        Me.spcContainer.SplitterDistance = 279
        Me.spcContainer.SplitterWidth = 3
        Me.spcContainer.TabIndex = 2
        '
        'gbFilterCriteria
        '
        Me.gbFilterCriteria.BorderColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.Checked = False
        Me.gbFilterCriteria.CollapseAllExceptThis = False
        Me.gbFilterCriteria.CollapsedHoverImage = Nothing
        Me.gbFilterCriteria.CollapsedNormalImage = Nothing
        Me.gbFilterCriteria.CollapsedPressedImage = Nothing
        Me.gbFilterCriteria.CollapseOnLoad = False
        Me.gbFilterCriteria.Controls.Add(Me.btnReset)
        Me.gbFilterCriteria.Controls.Add(Me.objbtnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.cboFilter)
        Me.gbFilterCriteria.Controls.Add(Me.objlblCaption)
        Me.gbFilterCriteria.Controls.Add(Me.cboViewBy)
        Me.gbFilterCriteria.Controls.Add(Me.lblViewBy)
        Me.gbFilterCriteria.Controls.Add(Me.objeline3)
        Me.gbFilterCriteria.Controls.Add(Me.radDescending)
        Me.gbFilterCriteria.Controls.Add(Me.radAscending)
        Me.gbFilterCriteria.Controls.Add(Me.objelSort)
        Me.gbFilterCriteria.Controls.Add(Me.cboGrouping)
        Me.gbFilterCriteria.Controls.Add(Me.lblGroupBy)
        Me.gbFilterCriteria.Controls.Add(Me.objeline2)
        Me.gbFilterCriteria.Controls.Add(Me.objeline1)
        Me.gbFilterCriteria.Controls.Add(Me.btnSearch)
        Me.gbFilterCriteria.Controls.Add(Me.lblLogOutDateTo)
        Me.gbFilterCriteria.Controls.Add(Me.lblLogOutDateFrom)
        Me.gbFilterCriteria.Controls.Add(Me.dtpLogoutToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpLogoutFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoginToDate)
        Me.gbFilterCriteria.Controls.Add(Me.lblLoginFromDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpLoginToDate)
        Me.gbFilterCriteria.Controls.Add(Me.dtpLoginFromDate)
        Me.gbFilterCriteria.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbFilterCriteria.ExpandedHoverImage = Nothing
        Me.gbFilterCriteria.ExpandedNormalImage = Nothing
        Me.gbFilterCriteria.ExpandedPressedImage = Nothing
        Me.gbFilterCriteria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbFilterCriteria.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbFilterCriteria.HeaderHeight = 25
        Me.gbFilterCriteria.HeaderMessage = ""
        Me.gbFilterCriteria.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbFilterCriteria.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbFilterCriteria.HeightOnCollapse = 0
        Me.gbFilterCriteria.LeftTextSpace = 0
        Me.gbFilterCriteria.Location = New System.Drawing.Point(0, 0)
        Me.gbFilterCriteria.Name = "gbFilterCriteria"
        Me.gbFilterCriteria.OpenHeight = 300
        Me.gbFilterCriteria.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbFilterCriteria.ShowBorder = True
        Me.gbFilterCriteria.ShowCheckBox = False
        Me.gbFilterCriteria.ShowCollapseButton = False
        Me.gbFilterCriteria.ShowDefaultBorderColor = True
        Me.gbFilterCriteria.ShowDownButton = False
        Me.gbFilterCriteria.ShowHeader = True
        Me.gbFilterCriteria.Size = New System.Drawing.Size(279, 394)
        Me.gbFilterCriteria.TabIndex = 0
        Me.gbFilterCriteria.Temp = 0
        Me.gbFilterCriteria.Text = "Filter Criteria"
        Me.gbFilterCriteria.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnReset.BackColor = System.Drawing.Color.Transparent
        Me.btnReset.BackColor1 = System.Drawing.Color.Transparent
        Me.btnReset.BackColor2 = System.Drawing.Color.Transparent
        Me.btnReset.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnReset.BorderSelected = False
        Me.btnReset.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnReset.Image = Global.Aruti.Data.My.Resources.Resources.reset_20
        Me.btnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnReset.Location = New System.Drawing.Point(254, 2)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(21, 21)
        Me.btnReset.TabIndex = 217
        '
        'objbtnSearch
        '
        Me.objbtnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnSearch.BackColor = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnSearch.BorderSelected = False
        Me.objbtnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnSearch.Image = Global.Aruti.Data.My.Resources.Resources.Mini_Search
        Me.objbtnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnSearch.Location = New System.Drawing.Point(247, 328)
        Me.objbtnSearch.Name = "objbtnSearch"
        Me.objbtnSearch.Size = New System.Drawing.Size(21, 21)
        Me.objbtnSearch.TabIndex = 239
        '
        'cboFilter
        '
        Me.cboFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboFilter.DropDownWidth = 200
        Me.cboFilter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboFilter.FormattingEnabled = True
        Me.cboFilter.Location = New System.Drawing.Point(108, 328)
        Me.cboFilter.Name = "cboFilter"
        Me.cboFilter.Size = New System.Drawing.Size(133, 21)
        Me.cboFilter.TabIndex = 238
        '
        'objlblCaption
        '
        Me.objlblCaption.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblCaption.Location = New System.Drawing.Point(8, 330)
        Me.objlblCaption.Name = "objlblCaption"
        Me.objlblCaption.Size = New System.Drawing.Size(94, 16)
        Me.objlblCaption.TabIndex = 237
        Me.objlblCaption.Text = "#Caption"
        Me.objlblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboViewBy
        '
        Me.cboViewBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboViewBy.DropDownWidth = 200
        Me.cboViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboViewBy.FormattingEnabled = True
        Me.cboViewBy.Location = New System.Drawing.Point(108, 301)
        Me.cboViewBy.Name = "cboViewBy"
        Me.cboViewBy.Size = New System.Drawing.Size(133, 21)
        Me.cboViewBy.TabIndex = 235
        '
        'lblViewBy
        '
        Me.lblViewBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblViewBy.Location = New System.Drawing.Point(8, 303)
        Me.lblViewBy.Name = "lblViewBy"
        Me.lblViewBy.Size = New System.Drawing.Size(94, 16)
        Me.lblViewBy.TabIndex = 234
        Me.lblViewBy.Text = "View By"
        Me.lblViewBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objeline3
        '
        Me.objeline3.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objeline3.Location = New System.Drawing.Point(8, 276)
        Me.objeline3.Name = "objeline3"
        Me.objeline3.Size = New System.Drawing.Size(233, 22)
        Me.objeline3.TabIndex = 233
        Me.objeline3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radDescending
        '
        Me.radDescending.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radDescending.Location = New System.Drawing.Point(108, 253)
        Me.radDescending.Name = "radDescending"
        Me.radDescending.Size = New System.Drawing.Size(133, 17)
        Me.radDescending.TabIndex = 232
        Me.radDescending.Text = "Descending"
        Me.radDescending.UseVisualStyleBackColor = True
        '
        'radAscending
        '
        Me.radAscending.Checked = True
        Me.radAscending.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.radAscending.Location = New System.Drawing.Point(108, 230)
        Me.radAscending.Name = "radAscending"
        Me.radAscending.Size = New System.Drawing.Size(133, 17)
        Me.radAscending.TabIndex = 232
        Me.radAscending.TabStop = True
        Me.radAscending.Text = "Ascending"
        Me.radAscending.UseVisualStyleBackColor = True
        '
        'objelSort
        '
        Me.objelSort.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objelSort.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objelSort.Location = New System.Drawing.Point(46, 210)
        Me.objelSort.Name = "objelSort"
        Me.objelSort.Size = New System.Drawing.Size(195, 17)
        Me.objelSort.TabIndex = 231
        Me.objelSort.Text = "Sort By"
        Me.objelSort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboGrouping
        '
        Me.cboGrouping.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboGrouping.DropDownWidth = 200
        Me.cboGrouping.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboGrouping.FormattingEnabled = True
        Me.cboGrouping.Location = New System.Drawing.Point(108, 186)
        Me.cboGrouping.Name = "cboGrouping"
        Me.cboGrouping.Size = New System.Drawing.Size(133, 21)
        Me.cboGrouping.TabIndex = 230
        '
        'lblGroupBy
        '
        Me.lblGroupBy.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroupBy.Location = New System.Drawing.Point(8, 188)
        Me.lblGroupBy.Name = "lblGroupBy"
        Me.lblGroupBy.Size = New System.Drawing.Size(94, 16)
        Me.lblGroupBy.TabIndex = 229
        Me.lblGroupBy.Text = "Group By"
        Me.lblGroupBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objeline2
        '
        Me.objeline2.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objeline2.Location = New System.Drawing.Point(8, 161)
        Me.objeline2.Name = "objeline2"
        Me.objeline2.Size = New System.Drawing.Size(233, 22)
        Me.objeline2.TabIndex = 228
        Me.objeline2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objeline1
        '
        Me.objeline1.LineOrientation = eZee.Common.eZeeLine.Orientation.Horizontal
        Me.objeline1.Location = New System.Drawing.Point(8, 85)
        Me.objeline1.Name = "objeline1"
        Me.objeline1.Size = New System.Drawing.Size(233, 22)
        Me.objeline1.TabIndex = 227
        Me.objeline1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'btnSearch
        '
        Me.btnSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearch.BackColor = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor1 = System.Drawing.Color.Transparent
        Me.btnSearch.BackColor2 = System.Drawing.Color.Transparent
        Me.btnSearch.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnSearch.BorderSelected = False
        Me.btnSearch.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnSearch.Image = Global.Aruti.Data.My.Resources.Resources.search_20
        Me.btnSearch.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnSearch.Location = New System.Drawing.Point(230, 2)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(21, 21)
        Me.btnSearch.TabIndex = 218
        '
        'lblLogOutDateTo
        '
        Me.lblLogOutDateTo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLogOutDateTo.Location = New System.Drawing.Point(8, 140)
        Me.lblLogOutDateTo.Name = "lblLogOutDateTo"
        Me.lblLogOutDateTo.Size = New System.Drawing.Size(94, 15)
        Me.lblLogOutDateTo.TabIndex = 226
        Me.lblLogOutDateTo.Text = "To"
        Me.lblLogOutDateTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLogOutDateFrom
        '
        Me.lblLogOutDateFrom.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLogOutDateFrom.Location = New System.Drawing.Point(8, 112)
        Me.lblLogOutDateFrom.Name = "lblLogOutDateFrom"
        Me.lblLogOutDateFrom.Size = New System.Drawing.Size(94, 16)
        Me.lblLogOutDateFrom.TabIndex = 225
        Me.lblLogOutDateFrom.Text = "Logout From Date"
        Me.lblLogOutDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpLogoutToDate
        '
        Me.dtpLogoutToDate.Checked = False
        Me.dtpLogoutToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpLogoutToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpLogoutToDate.Location = New System.Drawing.Point(108, 137)
        Me.dtpLogoutToDate.Name = "dtpLogoutToDate"
        Me.dtpLogoutToDate.ShowCheckBox = True
        Me.dtpLogoutToDate.Size = New System.Drawing.Size(133, 21)
        Me.dtpLogoutToDate.TabIndex = 223
        '
        'dtpLogoutFromDate
        '
        Me.dtpLogoutFromDate.Checked = False
        Me.dtpLogoutFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpLogoutFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpLogoutFromDate.Location = New System.Drawing.Point(108, 110)
        Me.dtpLogoutFromDate.Name = "dtpLogoutFromDate"
        Me.dtpLogoutFromDate.ShowCheckBox = True
        Me.dtpLogoutFromDate.Size = New System.Drawing.Size(133, 21)
        Me.dtpLogoutFromDate.TabIndex = 224
        '
        'lblLoginToDate
        '
        Me.lblLoginToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoginToDate.Location = New System.Drawing.Point(8, 64)
        Me.lblLoginToDate.Name = "lblLoginToDate"
        Me.lblLoginToDate.Size = New System.Drawing.Size(94, 15)
        Me.lblLoginToDate.TabIndex = 222
        Me.lblLoginToDate.Text = "To"
        Me.lblLoginToDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLoginFromDate
        '
        Me.lblLoginFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoginFromDate.Location = New System.Drawing.Point(8, 36)
        Me.lblLoginFromDate.Name = "lblLoginFromDate"
        Me.lblLoginFromDate.Size = New System.Drawing.Size(94, 16)
        Me.lblLoginFromDate.TabIndex = 221
        Me.lblLoginFromDate.Text = "Login From Date"
        Me.lblLoginFromDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'dtpLoginToDate
        '
        Me.dtpLoginToDate.Checked = False
        Me.dtpLoginToDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpLoginToDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpLoginToDate.Location = New System.Drawing.Point(108, 61)
        Me.dtpLoginToDate.Name = "dtpLoginToDate"
        Me.dtpLoginToDate.ShowCheckBox = True
        Me.dtpLoginToDate.Size = New System.Drawing.Size(133, 21)
        Me.dtpLoginToDate.TabIndex = 220
        '
        'dtpLoginFromDate
        '
        Me.dtpLoginFromDate.Checked = False
        Me.dtpLoginFromDate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpLoginFromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpLoginFromDate.Location = New System.Drawing.Point(108, 34)
        Me.dtpLoginFromDate.Name = "dtpLoginFromDate"
        Me.dtpLoginFromDate.ShowCheckBox = True
        Me.dtpLoginFromDate.Size = New System.Drawing.Size(133, 21)
        Me.dtpLoginFromDate.TabIndex = 220
        '
        'gbUserLog
        '
        Me.gbUserLog.BorderColor = System.Drawing.Color.Black
        Me.gbUserLog.Checked = False
        Me.gbUserLog.CollapseAllExceptThis = False
        Me.gbUserLog.CollapsedHoverImage = Nothing
        Me.gbUserLog.CollapsedNormalImage = Nothing
        Me.gbUserLog.CollapsedPressedImage = Nothing
        Me.gbUserLog.CollapseOnLoad = False
        Me.gbUserLog.Controls.Add(Me.objbtnHide)
        Me.gbUserLog.Controls.Add(Me.objbtnShow)
        Me.gbUserLog.Controls.Add(Me.pnlData)
        Me.gbUserLog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbUserLog.ExpandedHoverImage = Nothing
        Me.gbUserLog.ExpandedNormalImage = Nothing
        Me.gbUserLog.ExpandedPressedImage = Nothing
        Me.gbUserLog.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbUserLog.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbUserLog.HeaderHeight = 25
        Me.gbUserLog.HeaderMessage = ""
        Me.gbUserLog.HeaderMessageFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.gbUserLog.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbUserLog.HeightOnCollapse = 0
        Me.gbUserLog.LeftTextSpace = 0
        Me.gbUserLog.Location = New System.Drawing.Point(0, 0)
        Me.gbUserLog.Name = "gbUserLog"
        Me.gbUserLog.OpenHeight = 300
        Me.gbUserLog.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbUserLog.ShowBorder = True
        Me.gbUserLog.ShowCheckBox = False
        Me.gbUserLog.ShowCollapseButton = False
        Me.gbUserLog.ShowDefaultBorderColor = True
        Me.gbUserLog.ShowDownButton = False
        Me.gbUserLog.ShowHeader = True
        Me.gbUserLog.Size = New System.Drawing.Size(557, 394)
        Me.gbUserLog.TabIndex = 0
        Me.gbUserLog.Temp = 0
        Me.gbUserLog.Text = "User Log"
        Me.gbUserLog.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objbtnHide
        '
        Me.objbtnHide.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnHide.BackColor = System.Drawing.Color.Transparent
        Me.objbtnHide.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnHide.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnHide.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnHide.BorderSelected = False
        Me.objbtnHide.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnHide.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnHide.Location = New System.Drawing.Point(532, 2)
        Me.objbtnHide.Name = "objbtnHide"
        Me.objbtnHide.Size = New System.Drawing.Size(21, 21)
        Me.objbtnHide.TabIndex = 216
        '
        'objbtnShow
        '
        Me.objbtnShow.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnShow.BackColor = System.Drawing.Color.Transparent
        Me.objbtnShow.BackColor1 = System.Drawing.Color.Transparent
        Me.objbtnShow.BackColor2 = System.Drawing.Color.Transparent
        Me.objbtnShow.BorderNormalColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnShow.BorderSelected = False
        Me.objbtnShow.DialogResult = System.Windows.Forms.DialogResult.None
        Me.objbtnShow.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.objbtnShow.Location = New System.Drawing.Point(532, 2)
        Me.objbtnShow.Name = "objbtnShow"
        Me.objbtnShow.Size = New System.Drawing.Size(21, 21)
        Me.objbtnShow.TabIndex = 218
        '
        'pnlData
        '
        Me.pnlData.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlData.Controls.Add(Me.lvUserLog)
        Me.pnlData.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlData.Location = New System.Drawing.Point(2, 26)
        Me.pnlData.Name = "pnlData"
        Me.pnlData.Size = New System.Drawing.Size(553, 366)
        Me.pnlData.TabIndex = 2
        '
        'lvUserLog
        '
        Me.lvUserLog.BackColorOnChecked = False
        Me.lvUserLog.ColumnHeaders = Nothing
        Me.lvUserLog.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.colhUserName, Me.colhLogindate, Me.colhLogoutDate, Me.colhIp, Me.colhMachine, Me.colhRemark, Me.objcolhLoginDate, Me.objcolhLogoutDate, Me.colhLoginFrom})
        Me.lvUserLog.CompulsoryColumns = ""
        Me.lvUserLog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvUserLog.FullRowSelect = True
        Me.lvUserLog.GridLines = True
        Me.lvUserLog.GroupingColumn = Nothing
        Me.lvUserLog.HideSelection = False
        Me.lvUserLog.Location = New System.Drawing.Point(0, 0)
        Me.lvUserLog.MinColumnWidth = 50
        Me.lvUserLog.MultiSelect = False
        Me.lvUserLog.Name = "lvUserLog"
        Me.lvUserLog.OptionalColumns = ""
        Me.lvUserLog.ShowMoreItem = False
        Me.lvUserLog.ShowSaveItem = False
        Me.lvUserLog.ShowSelectAll = True
        Me.lvUserLog.ShowSizeAllColumnsToFit = True
        Me.lvUserLog.Size = New System.Drawing.Size(553, 366)
        Me.lvUserLog.Sortable = True
        Me.lvUserLog.TabIndex = 1
        Me.lvUserLog.UseCompatibleStateImageBehavior = False
        Me.lvUserLog.View = System.Windows.Forms.View.Details
        '
        'colhUserName
        '
        Me.colhUserName.Tag = "colhUserName"
        Me.colhUserName.Text = "User"
        Me.colhUserName.Width = 150
        '
        'colhLogindate
        '
        Me.colhLogindate.Tag = "colhLogindate"
        Me.colhLogindate.Text = "Last Login date"
        Me.colhLogindate.Width = 150
        '
        'colhLogoutDate
        '
        Me.colhLogoutDate.Tag = "colhLogoutDate"
        Me.colhLogoutDate.Text = "Logout Date"
        Me.colhLogoutDate.Width = 150
        '
        'colhIp
        '
        Me.colhIp.Tag = "colhIp"
        Me.colhIp.Text = "IP"
        Me.colhIp.Width = 120
        '
        'colhMachine
        '
        Me.colhMachine.Tag = "colhMachine"
        Me.colhMachine.Text = "Machine"
        Me.colhMachine.Width = 120
        '
        'colhRemark
        '
        Me.colhRemark.Tag = "colhRemark"
        Me.colhRemark.Text = "Status"
        Me.colhRemark.Width = 100
        '
        'objcolhLoginDate
        '
        Me.objcolhLoginDate.Tag = "objcolhLoginDate"
        Me.objcolhLoginDate.Text = ""
        Me.objcolhLoginDate.Width = 0
        '
        'objcolhLogoutDate
        '
        Me.objcolhLogoutDate.Tag = "objcolhLogoutDate"
        Me.objcolhLogoutDate.Text = ""
        Me.objcolhLogoutDate.Width = 0
        '
        'colhLoginFrom
        '
        Me.colhLoginFrom.Tag = "colhLoginFrom"
        Me.colhLoginFrom.Text = "Login From"
        Me.colhLoginFrom.Width = 120
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnExport)
        Me.EZeeFooter1.Controls.Add(Me.btnClose)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 394)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(839, 50)
        Me.EZeeFooter1.TabIndex = 0
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.BackgroundImage = CType(resources.GetObject("btnExport.BackgroundImage"), System.Drawing.Image)
        Me.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnExport.BorderColor = System.Drawing.Color.Empty
        Me.btnExport.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnExport.FlatAppearance.BorderSize = 0
        Me.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExport.ForeColor = System.Drawing.Color.Black
        Me.btnExport.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnExport.GradientForeColor = System.Drawing.Color.Black
        Me.btnExport.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Location = New System.Drawing.Point(615, 9)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnExport.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnExport.Size = New System.Drawing.Size(103, 30)
        Me.btnExport.TabIndex = 1
        Me.btnExport.Text = "&Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.BackColor = System.Drawing.Color.White
        Me.btnClose.BackgroundImage = CType(resources.GetObject("btnClose.BackgroundImage"), System.Drawing.Image)
        Me.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnClose.BorderColor = System.Drawing.Color.Empty
        Me.btnClose.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.Black
        Me.btnClose.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnClose.GradientForeColor = System.Drawing.Color.Black
        Me.btnClose.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Location = New System.Drawing.Point(724, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnClose.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnClose.Size = New System.Drawing.Size(103, 30)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "&Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmUserLogList
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(839, 444)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUserLogList"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "User Log List"
        Me.pnlMain.ResumeLayout(False)
        Me.spcContainer.Panel1.ResumeLayout(False)
        Me.spcContainer.Panel2.ResumeLayout(False)
        Me.spcContainer.ResumeLayout(False)
        Me.gbFilterCriteria.ResumeLayout(False)
        Me.gbUserLog.ResumeLayout(False)
        Me.pnlData.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents lvUserLog As eZee.Common.eZeeListView
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnClose As eZee.Common.eZeeLightButton
    Friend WithEvents colhUserName As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLogindate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhIp As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhMachine As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhRemark As System.Windows.Forms.ColumnHeader
    Friend WithEvents spcContainer As System.Windows.Forms.SplitContainer
    Friend WithEvents gbFilterCriteria As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents gbUserLog As eZee.Common.eZeeCollapsibleContainer
    Friend WithEvents pnlData As System.Windows.Forms.Panel
    Friend WithEvents objbtnHide As eZee.Common.eZeeGradientButton
    Friend WithEvents objbtnShow As eZee.Common.eZeeGradientButton
    Friend WithEvents btnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents btnReset As eZee.Common.eZeeGradientButton
    Friend WithEvents colhLogoutDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents dtpLoginFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblLoginToDate As System.Windows.Forms.Label
    Friend WithEvents lblLoginFromDate As System.Windows.Forms.Label
    Friend WithEvents dtpLoginToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblLogOutDateTo As System.Windows.Forms.Label
    Friend WithEvents lblLogOutDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpLogoutToDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpLogoutFromDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents objeline1 As eZee.Common.eZeeLine
    Friend WithEvents objeline2 As eZee.Common.eZeeLine
    Friend WithEvents cboGrouping As System.Windows.Forms.ComboBox
    Friend WithEvents lblGroupBy As System.Windows.Forms.Label
    Friend WithEvents objelSort As eZee.Common.eZeeLine
    Friend WithEvents objeline3 As eZee.Common.eZeeLine
    Friend WithEvents radDescending As System.Windows.Forms.RadioButton
    Friend WithEvents radAscending As System.Windows.Forms.RadioButton
    Friend WithEvents cboViewBy As System.Windows.Forms.ComboBox
    Friend WithEvents lblViewBy As System.Windows.Forms.Label
    Friend WithEvents cboFilter As System.Windows.Forms.ComboBox
    Friend WithEvents objlblCaption As System.Windows.Forms.Label
    Friend WithEvents objbtnSearch As eZee.Common.eZeeGradientButton
    Friend WithEvents objcolhLoginDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents objcolhLogoutDate As System.Windows.Forms.ColumnHeader
    Friend WithEvents colhLoginFrom As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnExport As eZee.Common.eZeeLightButton
End Class
