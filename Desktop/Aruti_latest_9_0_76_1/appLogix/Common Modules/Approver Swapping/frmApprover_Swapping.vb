﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data
Imports Aruti.Data.Language

Public Class frmApprover_Swapping

#Region "Private Variable"

    Private mblnCancel As Boolean = True
    Private menAction As enAction = enAction.ADD_ONE
    Private ReadOnly mstrModuleName As String = "frmApprover_Swapping"
    Private mintUserMappunkid As Integer = -1
    Private ApproverType As enSwapApproverType
    Dim dsFromApproverEmp As DataSet = Nothing
    Dim dsToApproverEmp As DataSet = Nothing
    Private objLeaveApprover As clsleaveapprover_master
    Private objExpenseApprover As clsExpenseApprover_Master
    Private objLoanApprover As clsLoanApprover_master
    Dim drFromRow() As DataRow = Nothing
    Dim drToRow() As DataRow = Nothing

#End Region

#Region " Display Dialog "

    Public Function displayDialog(ByRef intUnkId As Integer, ByVal eAction As enAction, ByVal _ApproverType As enSwapApproverType) As Boolean
        Try
            ApproverType = _ApproverType
            menAction = eAction
            Me.ShowDialog()

            Return Not mblnCancel
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "displayDialog", mstrModuleName)
        End Try
    End Function


#End Region

#Region "Form's Event"

    Private Sub frmApprover_Swapping_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            If ApproverType = enSwapApproverType.Leave Then
                objLeaveApprover = New clsleaveapprover_master
                LblCategory.Visible = False
                cboCategory.Visible = False

            ElseIf ApproverType = enSwapApproverType.Loan Then
                objLoanApprover = New clsLoanApprover_master
                LblCategory.Visible = False
                cboCategory.Visible = False

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then
                objExpenseApprover = New clsExpenseApprover_Master
                LblCategory.Visible = True
                cboCategory.Visible = True

            ElseIf ApproverType = enSwapApproverType.Performance Then
                LblCategory.Visible = True
                cboCategory.Visible = True
            End If

            Call Set_Logo(Me, gApplicationType)
            Language.setLanguage(Me.Name)
            FillCombo()
            btnSave.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmApprover_Swapping_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            If ApproverType = enSwapApproverType.Leave Then
                clsleaveapprover_master.SetMessages()
                clsleaveapprover_Tran.SetMessages()
                objfrm._Other_ModuleNames = "clsleaveapprover_master,clsleaveapprover_Tran"
            End If

            objfrm.displayDialog(Me)
            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Private Methods"

    Private Sub FillCombo()

        Dim dsFill As DataSet = Nothing
        Try

            If ApproverType = enSwapApproverType.Leave Then

                Dim objApprover As New clsleaveapprover_master


                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                'Dim dsList As DataSet = objApprover.GetList("List", True, "True")


                'Pinkal (19-Jan-2016) -- Start
                'Enhancement - Change in Employee Inactive condition.
                'Dim dsList As DataSet = objApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                '                                                                 , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                '                                                                 , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                '                                                                 , True, ConfigParameter._Object._IsIncludeInactiveEmp, True, False, -1, Nothing, "", "")
                Dim dsList As DataSet = objApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                                 , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid _
                                                                                 , ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting _
                                                                                , True, True, True, False, -1, Nothing, "", "")
                'Pinkal (19-Jan-2016) -- End
                'Pinkal (24-Aug-2015) -- End

                'S.SANDEEP [30 JAN 2016] -- START
                'Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "leaveapproverunkid", "name")
                Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "leaveapproverunkid", "name", "isexternalapprover")
                'S.SANDEEP [30 JAN 2016] -- END

                Dim dr As DataRow = dtTable.NewRow
                dr("leaveapproverunkid") = 0
                dr("name") = Language.getMessage(mstrModuleName, 1, "Select")
                'S.SANDEEP [30 JAN 2016] -- START
                dr("isexternalapprover") = False
                'S.SANDEEP [30 JAN 2016] -- END
                dtTable.Rows.InsertAt(dr, 0)

                cboFromApprover.DisplayMember = "name"
                cboFromApprover.ValueMember = "leaveapproverunkid"
                cboFromApprover.DataSource = dtTable

                objApprover = Nothing

            ElseIf ApproverType = enSwapApproverType.Loan Then

                Dim objlnApprover As New clsLoanApprover_master

                'Pinkal (11-Dec-2017) -- Start
                'Bug - Solved Loan Approver Migration Issue .
                'Dim dsList As DataSet = objlnApprover.getListForCombo("List", True, True)
                Dim dsList As DataSet = objlnApprover.getListForCombo("List", True, True, False)
                Dim dtTable As DataTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "lnempapproverunkid", "name", "isexternalapprover")
                'Pinkal (11-Dec-2017) -- End


                cboFromApprover.DisplayMember = "name"
                cboFromApprover.ValueMember = "lnempapproverunkid"

                'Pinkal (11-Dec-2017) -- Start
                'Bug - Solved Loan Approver Migration Issue .
                'cboFromApprover.DataSource = dsList.Tables(0)
                cboFromApprover.DataSource = dtTable
                'Pinkal (11-Dec-2017) -- End

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then

                Dim dtTable As DataTable = Nothing
                'Pinkal (24-Jun-2024) -- Start
                'NMB Enhancement : P2P & Expense Category Enhancements.
                'dsFill = clsExpCommonMethods.Get_ExpenseTypes(True, True, True, "List", True, True)
                'If ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                '    dtTable = New DataView(dsFill.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
                'Else
                '    dtTable = New DataView(dsFill.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                'End If

                Dim objExpenseCategory As New clsexpense_category_master
                dsFill = objExpenseCategory.GetExpenseCategory(FinancialYear._Object._DatabaseName, True, True, True, "List", True, True)
                objExpenseCategory = Nothing
                If ConfigParameter._Object._PaymentApprovalwithLeaveApproval Then
                    dtTable = New DataView(dsFill.Tables(0), "Id <> " & enExpenseType.EXP_LEAVE, "", DataViewRowState.CurrentRows).ToTable
                Else
                    dtTable = New DataView(dsFill.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable
                End If
                'Pinkal (24-Jun-2024) -- End

                With cboCategory
                    .ValueMember = "Id"
                    .DisplayMember = "Name"
                    .DataSource = dtTable
                    .SelectedValue = 0
                End With

            ElseIf ApproverType = enSwapApproverType.Performance Then

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    Private Sub FillFromApproverEmployeeList(ByVal dsFromApprvoer As DataSet)
        Try
            If dsFromApprvoer IsNot Nothing Then

                If ApproverType = enSwapApproverType.Leave Then

                    dgFromEmployee.AutoGenerateColumns = False
                    dgFromEmployee.DataSource = dsFromApprvoer.Tables(0)
                    dgcolhFromEmpcode.DataPropertyName = "employeecode"
                    dgcolhFromEmployee.DataPropertyName = "employeename"

                ElseIf ApproverType = enSwapApproverType.Loan Then

                    dgFromEmployee.AutoGenerateColumns = False
                    dgFromEmployee.DataSource = dsFromApprvoer.Tables(0)
                    dgcolhFromEmpcode.DataPropertyName = "employeecode"
                    dgcolhFromEmployee.DataPropertyName = "employeename"

                ElseIf ApproverType = enSwapApproverType.Claim_Request Then

                    dgFromEmployee.AutoGenerateColumns = False
                    dgFromEmployee.DataSource = dsFromApprvoer.Tables(0)
                    dgcolhFromEmpcode.DataPropertyName = "ecode"
                    dgcolhFromEmployee.DataPropertyName = "ename"


                ElseIf ApproverType = enSwapApproverType.Performance Then

                End If

            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "FillFromApproverEmployeeList", mstrModuleName)
        End Try
    End Sub

    Private Sub FillToApproverEmployeeList(ByVal dsToApprover As DataSet)
        Try
            If dsToApprover IsNot Nothing Then

                If ApproverType = enSwapApproverType.Leave Then

                    dgToEmployee.AutoGenerateColumns = False
                    dgToEmployee.DataSource = dsToApprover.Tables(0)
                    dgcolhToEmpCode.DataPropertyName = "employeecode"
                    dgcolhToEmployee.DataPropertyName = "employeename"

                ElseIf ApproverType = enSwapApproverType.Loan Then

                    dgToEmployee.AutoGenerateColumns = False
                    dgToEmployee.DataSource = dsToApprover.Tables(0)
                    dgcolhToEmpCode.DataPropertyName = "employeecode"
                    dgcolhToEmployee.DataPropertyName = "employeename"

                ElseIf ApproverType = enSwapApproverType.Claim_Request Then

                    dgToEmployee.AutoGenerateColumns = False
                    dgToEmployee.DataSource = dsToApprover.Tables(0)
                    dgcolhToEmpCode.DataPropertyName = "ecode"
                    dgcolhToEmployee.DataPropertyName = "ename"

                ElseIf ApproverType = enSwapApproverType.Performance Then

                End If

            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            DisplayError.Show("-1", ex.Message, "FillToApproverEmployeeList", mstrModuleName)
        End Try
    End Sub

    'S.SANDEEP [30 JAN 2016] -- START
    Private Sub GetApproverLevel(ByVal sender As Object, ByVal intApproverID As Integer, Optional ByVal blnExtarnalApprover As Boolean = False)
        'Private Sub GetApproverLevel(ByVal sender As Object, ByVal intApproverID As Integer)
        'S.SANDEEP [30 JAN 2016] -- END
        Try
            Dim dsList As DataSet = Nothing
            If ApproverType = enSwapApproverType.Leave Then

                'S.SANDEEP [30 JAN 2016] -- START
                'dsList = objLeaveApprover.GetLevelFromLeaveApprover(True, intApproverID)
                dsList = objLeaveApprover.GetLevelFromLeaveApprover(intApproverID, blnExtarnalApprover, True)
                'S.SANDEEP [30 JAN 2016] -- END

                If CType(sender, ComboBox).Name = "cboToApprover" Then
                    cboToLevel.DisplayMember = "levelname"
                    cboToLevel.ValueMember = "levelunkid"
                    cboToLevel.DataSource = dsList.Tables(0)

                ElseIf CType(sender, ComboBox).Name = "cboFromApprover" Then
                    cboFromLevel.DisplayMember = "levelname"
                    cboFromLevel.ValueMember = "levelunkid"
                    cboFromLevel.DataSource = dsList.Tables(0)
                End If

            ElseIf ApproverType = enSwapApproverType.Loan Then

                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'dsList = objLoanApprover.GetLevelFromLoanApprover(True, intApproverID)
                dsList = objLoanApprover.GetLevelFromLoanApprover(intApproverID, blnExtarnalApprover, True)
                'Nilay (01-Mar-2016) -- End

                If CType(sender, ComboBox).Name = "cboToApprover" Then
                    cboToLevel.DisplayMember = "name"
                    cboToLevel.ValueMember = "lnlevelunkid"
                    cboToLevel.DataSource = dsList.Tables(0)

                ElseIf CType(sender, ComboBox).Name = "cboFromApprover" Then
                    cboFromLevel.DisplayMember = "name"
                    cboFromLevel.ValueMember = "lnlevelunkid"
                    cboFromLevel.DataSource = dsList.Tables(0)
                End If

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then

                Dim dr As DataRowView = CType(CType(sender, ComboBox).SelectedItem, DataRowView)

                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then
                '    dsList = objExpenseApprover.GetExpApproverLevels(CInt(cboCategory.SelectedValue), intApproverID, CInt(dr("approverempunkid")), True, "List")
                'Else
                '    dsList = objExpenseApprover.GetExpApproverLevels(CInt(cboCategory.SelectedValue), intApproverID, 0, True, "List")
                'End If

                If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then
                    dsList = objExpenseApprover.GetExpApproverLevels(CInt(cboCategory.SelectedValue), 0, intApproverID, blnExtarnalApprover, True, "List")
                Else
                    dsList = objExpenseApprover.GetExpApproverLevels(CInt(cboCategory.SelectedValue), 0, intApproverID, blnExtarnalApprover, True, "List")
                End If
                'Pinkal (18-Jun-2020) -- End




                If CType(sender, ComboBox).Name = "cboToApprover" Then
                    cboToLevel.DisplayMember = "Name"
                    cboToLevel.ValueMember = "Id"
                    cboToLevel.DataSource = dsList.Tables(0)

                ElseIf CType(sender, ComboBox).Name = "cboFromApprover" Then
                    cboFromLevel.DisplayMember = "Name"
                    cboFromLevel.ValueMember = "Id"
                    cboFromLevel.DataSource = dsList.Tables(0)
                End If


            ElseIf ApproverType = enSwapApproverType.Performance Then

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetApproverLevel", mstrModuleName)
        End Try
    End Sub

    Private Function Validation() As Boolean
        Try
            If CInt(cboFromApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Please Select From Approver to swap employee(s)."), enMsgBoxStyle.Information)
                cboFromApprover.Select()
                Return False

            ElseIf CInt(cboFromLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Level to swap employee(s)."), enMsgBoxStyle.Information)
                cboFromLevel.Select()
                Return False

            ElseIf CInt(cboToApprover.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 4, "Please Select To Approver to swap employee(s)."), enMsgBoxStyle.Information)
                cboToApprover.Select()
                Return False

            ElseIf CInt(cboToLevel.SelectedValue) <= 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 3, "Please Select Level to swap employee(s)."), enMsgBoxStyle.Information)
                cboToLevel.Select()
                Return False
            End If
            Return True
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Validation", mstrModuleName)
        End Try
    End Function

#End Region

#Region "Button's Event"

    Private Sub objbtnSearchFromApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFromApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboFromApprover.DataSource, DataTable)
            With cboFromApprover
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFromApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchToApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchToApprover.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboToApprover.DataSource, DataTable)
            With cboToApprover
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchToApprover_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchToLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchToLevel.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboToLevel.DataSource, DataTable)
            With cboToLevel
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchNewLevel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchFromLevel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchFromLevel.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboFromLevel.DataSource, DataTable)
            With cboFromLevel
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchFromLevel_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnSwap_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSwap.Click
        Try

            If Validation() = False Then Exit Sub

            Me.Cursor = Cursors.WaitCursor


            If dsFromApproverEmp IsNot Nothing AndAlso dsFromApproverEmp.Tables(0).Rows.Count > 0 Then
                drFromRow = dsFromApproverEmp.Tables(0).Select("employeeunkid = " & CInt(cboToApprover.SelectedValue))
            End If

            If dsToApproverEmp IsNot Nothing AndAlso dsToApproverEmp.Tables(0).Rows.Count > 0 Then
                drToRow = dsToApproverEmp.Tables(0).Select("employeeunkid = " & CInt(cboFromApprover.SelectedValue))
            End If


            If drFromRow IsNot Nothing AndAlso drFromRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot swap approver") & " " & cboToApprover.Text & " " & _
                                             Language.getMessage(mstrModuleName, 7, ".Reason:the selected") & " " & lblToApprover.Text & " " & _
                                             Language.getMessage(mstrModuleName, 8, "is already in the list of") & " " & lblFromApprover.Text & " " & _
                                             Language.getMessage(mstrModuleName, 9, "as an employee."), enMsgBoxStyle.Information)
                Exit Sub
            ElseIf drToRow IsNot Nothing AndAlso drToRow.Length > 0 Then
                eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 6, "Sorry, You cannot swap approver") & " " & cboFromApprover.Text & " " & _
                                             Language.getMessage(mstrModuleName, 7, ".Reason:the selected") & " " & lblFromApprover.Text & " " & _
                                             Language.getMessage(mstrModuleName, 8, "is already in the list of") & " " & lblToApprover.Text & " " & _
                                             Language.getMessage(mstrModuleName, 9, "as an employee."), enMsgBoxStyle.Information)
                Exit Sub
            End If



            cboFromApprover.Enabled = False
            objbtnSearchFromApprover.Enabled = False
            cboFromLevel.Enabled = False
            objbtnSearchFromLevel.Enabled = False
            cboToApprover.Enabled = False
            objbtnSearchToApprover.Enabled = False
            cboToLevel.Enabled = False
            objbtnSearchToLevel.Enabled = False


            If ApproverType = enSwapApproverType.Leave Then


                FillFromApproverEmployeeList(dsToApproverEmp)
                FillToApproverEmployeeList(dsFromApproverEmp)

            ElseIf ApproverType = enSwapApproverType.Loan Then

                FillFromApproverEmployeeList(dsToApproverEmp)
                FillToApproverEmployeeList(dsFromApproverEmp)

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then
                cboCategory.Enabled = False
                FillFromApproverEmployeeList(dsToApproverEmp)
                FillToApproverEmployeeList(dsFromApproverEmp)

            ElseIf ApproverType = enSwapApproverType.Performance Then

            End If

            btnSave.Enabled = True
            btnSwap.Enabled = False

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSwap_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            If Validation() = False Then Exit Sub

            If ApproverType = enSwapApproverType.Leave Then

                objLeaveApprover._SwapFromApproverunkid = CInt(cboFromApprover.Tag)
                objLeaveApprover._SwapToApproverunkid = CInt(cboToApprover.Tag)
                objLeaveApprover._Userumkid = User._Object._Userunkid

                If objLeaveApprover.SwapApprover(CType(dgFromEmployee.DataSource, DataTable), CType(dgToEmployee.DataSource, DataTable), ConfigParameter._Object._IsLeaveApprover_ForLeaveType, ConfigParameter._Object._PaymentApprovalwithLeaveApproval) = False Then
                    eZeeMsgBox.Show(objLeaveApprover._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Approver swapping done successfully."), enMsgBoxStyle.Information)
                    Me.Close()
                End If

            ElseIf ApproverType = enSwapApproverType.Loan Then

                objLoanApprover._SwapFromApproverunkid = CInt(cboFromApprover.Tag)
                objLoanApprover._SwapToApproverunkid = CInt(cboToApprover.Tag)
                objLoanApprover._Userunkid = User._Object._Userunkid

                If objLoanApprover.SwapApprover(CType(dgFromEmployee.DataSource, DataTable), CType(dgToEmployee.DataSource, DataTable), _
                                                CBool(ConfigParameter._Object._IsLoanApprover_ForLoanScheme)) = False Then
                    eZeeMsgBox.Show(objLoanApprover._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Approver swapping done successfully."), enMsgBoxStyle.Information)
                    Me.Close()
                End If

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then

                objExpenseApprover._SwapFromApproverunkid = CInt(cboFromApprover.Tag)
                objExpenseApprover._SwapToApproverunkid = CInt(cboToApprover.Tag)
                objExpenseApprover._Userunkid = User._Object._Userunkid


                'Shani(24-Aug-2015) -- Start
                'ENHANCEMENT : NEW ACTIVE CONDITION WITH REMOVAL OF GLOBAL OBJECTS IN CLASS
                'If objExpenseApprover.SwapApprover(CType(dgFromEmployee.DataSource, DataTable), CType(dgToEmployee.DataSource, DataTable)) = False Then
                If objExpenseApprover.SwapApprover(CType(dgFromEmployee.DataSource, DataTable), CType(dgToEmployee.DataSource, DataTable), ConfigParameter._Object._CurrentDateAndTime) = False Then
                    'Shani(24-Aug-2015) -- End

                    eZeeMsgBox.Show(objExpenseApprover._Message, enMsgBoxStyle.Information)
                    Exit Sub
                Else
                    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 5, "Approver swapping done successfully."), enMsgBoxStyle.Information)
                    Me.Close()
                End If

            ElseIf ApproverType = enSwapApproverType.Performance Then

            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

#End Region

#Region "ComboBox Event"

    Private Sub cboCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCategory.SelectedIndexChanged
        Try
            Dim dsList As New DataSet

            If ApproverType = enSwapApproverType.Claim_Request Then

                'Gajanan [23-May-2020] -- Start
                'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.
                'dsList = objExpenseApprover.getListForCombo(CInt(cboCategory.SelectedValue), True, "List")
                dsList = objExpenseApprover.getListForCombo(CInt(cboCategory.SelectedValue), FinancialYear._Object._DatabaseName, _
                                                            User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                            ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, _
                                                            True, "List", "", True, True)
                'Gajanan [23-May-2020] -- End


                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.

                'Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "id", "ApproverEmpunkid", "name")
                'With cboFromApprover
                '    .ValueMember = "id"
                '    .DisplayMember = "name"
                '    .DataSource = dtTable
                '    .SelectedValue = 0
                'End With

                Dim dtTable As DataTable = dsList.Tables(0).DefaultView.ToTable(True, "ApproverEmpunkid", "name", "isexternalapprover")
                With cboFromApprover
                    .ValueMember = "ApproverEmpunkid"
                    .DisplayMember = "name"
                    .DataSource = dtTable
                    .SelectedValue = 0
                End With

                'Pinkal (18-Jun-2020) -- End

            ElseIf ApproverType = enSwapApproverType.Performance Then

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboCategory_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFromApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFromApprover.SelectedIndexChanged
        Try
            Dim dsList As DataSet = Nothing
            Dim dtTable As DataTable = Nothing
            Dim iApproverID As Integer = 0

            If ApproverType = enSwapApproverType.Leave Then


                'Pinkal (24-Aug-2015) -- Start
                'Enhancement - WORKING ON ACTIVE EMPLOYEE CONDITION.
                '   dsList = objLeaveApprover.GetList("List", True, "True")

                'Pinkal (06-Jan-2016) -- Start
                'Enhancement - Working on Changes in SS for Leave Module.
                'dsList = objLeaveApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                '                                                 , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate _
                '                                                 , ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, True, False _
                '                                                 , -1, Nothing, "", "")
                dsList = objLeaveApprover.GetList("List", FinancialYear._Object._DatabaseName, User._Object._Userunkid _
                                                                 , FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, ConfigParameter._Object._EmployeeAsOnDate _
                                                             , ConfigParameter._Object._UserAccessModeSetting, True, True, True, False _
                                                                 , -1, Nothing, "", "")
                'Pinkal (06-Jan-2016) -- End

                
                'Pinkal (24-Aug-2015) -- End


                If CInt(cboFromApprover.SelectedValue) <= 0 Then
                    dtTable = dsList.Tables(0)
                Else
                    'S.SANDEEP [30 JAN 2016] -- START
                    'dtTable = New DataView(dsList.Tables(0), "leaveapproverunkid <> " & CInt(cboFromApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable()
                    Dim strApproverUnkids As String = ""
                    strApproverUnkids = objLeaveApprover.GetLeaveApproverUnkid(CInt(cboFromApprover.SelectedValue), CBool(CType(cboFromApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
                    dtTable = New DataView(dsList.Tables(0), "approverunkid NOT IN(" & strApproverUnkids & ")", "", DataViewRowState.CurrentRows).ToTable()
                    'S.SANDEEP [30 JAN 2016] -- END
                End If

                'S.SANDEEP [30 JAN 2016] -- START
                'dtTable = dtTable.DefaultView.ToTable(True, "leaveapproverunkid", "name")
                dtTable = dtTable.DefaultView.ToTable(True, "leaveapproverunkid", "name", "isexternalapprover")
                'S.SANDEEP [30 JAN 2016] -- END

                Dim dr As DataRow = dtTable.NewRow
                dr("leaveapproverunkid") = 0
                dr("name") = Language.getMessage(mstrModuleName, 1, "Select")
                'S.SANDEEP [30 JAN 2016] -- START
                dr("isexternalapprover") = False
                'S.SANDEEP [30 JAN 2016] -- END
                dtTable.Rows.InsertAt(dr, 0)

                cboToApprover.DisplayMember = "name"
                cboToApprover.ValueMember = "leaveapproverunkid"
                cboToApprover.DataSource = dtTable


            ElseIf ApproverType = enSwapApproverType.Loan Then


                'Pinkal (11-Dec-2017) -- Start
                'Bug - Solved Loan Approver Migration Issue .
                'dsList = objLoanApprover.getListForCombo("List", True, True)
                dsList = objLoanApprover.getListForCombo("List", True, True, False)
                'Pinkal (11-Dec-2017) -- End

                If CInt(cboFromApprover.SelectedValue) > 0 Then
                    'Nilay (01-Mar-2016) -- Start
                    'ENHANCEMENT - Implementing External Approval changes 
                    'dtTable = New DataView(dsList.Tables(0), " lnempapproverunkid <> " & CInt(cboFromApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable()
                    Dim strApproverUnkids As String = ""
                    strApproverUnkids = objLoanApprover.GetApproverUnkid(CInt(cboFromApprover.SelectedValue), CBool(CType(cboFromApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
                    dtTable = New DataView(dsList.Tables(0), " lnapproverunkid NOT IN(" & strApproverUnkids & ") ", "", DataViewRowState.CurrentRows).ToTable()
                    'Nilay (01-Mar-2016) -- End
                Else
                    dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable()
                End If


                'Pinkal (11-Dec-2017) -- Start
                'Bug - Solved Loan Approver Migration Issue .
                dtTable = dtTable.DefaultView.ToTable(True, "lnempapproverunkid", "name", "isexternalapprover")
                'Pinkal (11-Dec-2017) -- End


                cboToApprover.DisplayMember = "name"
                cboToApprover.ValueMember = "lnempapproverunkid"
                cboToApprover.DataSource = dtTable

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then


                'Gajanan [23-May-2020] -- Start
                'Enhancement NMB Approver Migration Changes - Working on Put options like show inactive approvers & show inactive employees on Approver Migration screen.

                'dsList = objExpenseApprover.getListForCombo(CInt(cboCategory.SelectedValue), True, "List")

                dsList = objExpenseApprover.getListForCombo(CInt(cboCategory.SelectedValue), FinancialYear._Object._DatabaseName, _
                                            User._Object._Userunkid, FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                            ConfigParameter._Object._EmployeeAsOnDate, ConfigParameter._Object._UserAccessModeSetting, _
                                            True, "List", "", True, True)
                'Gajanan [23-May-2020] -- End



                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.

                'If CInt(cboFromApprover.SelectedValue) > 0 Then
                '    dtTable = New DataView(dsList.Tables(0), " id <> " & CInt(cboFromApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable(True, "id", "ApproverEmpunkid", "name")
                'Else
                '    dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "id", "ApproverEmpunkid", "name")
                'End If
                'With cboToApprover
                '    .ValueMember = "id"
                '    .DisplayMember = "name"
                '    .DataSource = dtTable
                '    .SelectedValue = 0
                'End With

                If CInt(cboFromApprover.SelectedValue) > 0 Then
                    dtTable = New DataView(dsList.Tables(0), " ApproverEmpunkid <> " & CInt(cboFromApprover.SelectedValue), "", DataViewRowState.CurrentRows).ToTable(True, "ApproverEmpunkid", "name", "isexternalapprover")
                Else
                    dtTable = New DataView(dsList.Tables(0), "", "", DataViewRowState.CurrentRows).ToTable(True, "ApproverEmpunkid", "name", "isexternalapprover")
                End If
                With cboToApprover
                    .ValueMember = "ApproverEmpunkid"
                    .DisplayMember = "name"
                    .DataSource = dtTable
                    .SelectedValue = 0
                End With

                'Pinkal (18-Jun-2020) -- End


            End If


            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            'If ApproverType = enSwapApproverType.Leave OrElse ApproverType = enSwapApproverType.Loan Then
            If ApproverType = enSwapApproverType.Leave OrElse ApproverType = enSwapApproverType.Loan OrElse ApproverType = enSwapApproverType.Claim_Request Then
                'Pinkal (18-Jun-2020) -- End
                GetApproverLevel(sender, CInt(cboFromApprover.SelectedValue), CBool(CType(cboFromApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
            Else
            GetApproverLevel(sender, CInt(cboFromApprover.SelectedValue))
            End If

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFromApprover_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboFromLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboFromLevel.SelectedIndexChanged
        Try
            dsFromApproverEmp = Nothing

            If ApproverType = enSwapApproverType.Leave Then


                'Pinkal (14-May-2020) -- Start
                'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
                'dsFromApproverEmp = objLeaveApprover.GetEmployeeFromApprover(CInt(cboFromApprover.SelectedValue), CInt(cboFromLevel.SelectedValue) _
                '                                                          , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CBool(CType(cboFromApprover.SelectedItem, DataRowView).Item("isexternalapprover")))

                dsFromApproverEmp = objLeaveApprover.GetEmployeeFromApprover(FinancialYear._Object._DatabaseName, CInt(cboFromApprover.SelectedValue), CInt(cboFromLevel.SelectedValue) _
                                                                                                                   , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
                                                                                                                   , CBool(CType(cboFromApprover.SelectedItem, DataRowView).Item("isexternalapprover")), True)
                'Pinkal (14-May-2020) -- End
                


                FillFromApproverEmployeeList(dsFromApproverEmp)

                If dsFromApproverEmp IsNot Nothing AndAlso dsFromApproverEmp.Tables(0).Rows.Count > 0 Then
                    cboFromApprover.Tag = CInt(dsFromApproverEmp.Tables(0).Rows(0)("approverunkid"))
                Else
                    cboFromApprover.Tag = Nothing
                End If

            ElseIf ApproverType = enSwapApproverType.Loan Then

                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'dsFromApproverEmp = objLoanApprover.GetEmployeeFromLoanApprover(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                '                                                             FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                '                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                                             eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                                             ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                '                                                             "List", CInt(cboFromApprover.SelectedValue), CInt(cboFromLevel.SelectedValue))

                dsFromApproverEmp = objLoanApprover.GetEmployeeFromLoanApprover(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                                                                FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                                ConfigParameter._Object._UserAccessModeSetting, True, ConfigParameter._Object._IsIncludeInactiveEmp, _
                                                                             "List", CInt(cboFromApprover.SelectedValue), CInt(cboFromLevel.SelectedValue), _
                                                                             CBool(CType(cboFromApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
                'Nilay (01-Mar-2016) -- End

             
                FillFromApproverEmployeeList(dsFromApproverEmp)

                If dsFromApproverEmp IsNot Nothing AndAlso dsFromApproverEmp.Tables(0).Rows.Count > 0 Then
                    cboFromApprover.Tag = CInt(dsFromApproverEmp.Tables(0).Rows(0)("lnapproverunkid"))
                Else
                    cboFromApprover.Tag = Nothing
                End If

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then


                Dim dsList As DataSet = Nothing


                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.

                'Dim dr As DataRowView = CType(cboFromApprover.SelectedItem, DataRowView)
                'If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then
                '    dsFromApproverEmp = objExpenseApprover.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboFromApprover.SelectedValue), CInt(dr("ApproverEmpunkid")), CInt(cboFromLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsFromApproverEmp = objExpenseApprover.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboFromApprover.SelectedValue), 0, CInt(cboFromLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'End If

                Dim dr As DataRowView = CType(cboFromApprover.SelectedItem, DataRowView)
                If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then
                    dsFromApproverEmp = objExpenseApprover.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboCategory.SelectedValue), 0, CInt(cboFromApprover.SelectedValue), CInt(cboFromLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                Else
                    dsFromApproverEmp = objExpenseApprover.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboCategory.SelectedValue), 0, CInt(cboFromApprover.SelectedValue), CInt(cboFromLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                End If

                'Pinkal (18-Jun-2020) -- End


                FillFromApproverEmployeeList(dsFromApproverEmp)

                If dsFromApproverEmp IsNot Nothing AndAlso dsFromApproverEmp.Tables(0).Rows.Count > 0 Then
                    cboFromApprover.Tag = CInt(dsFromApproverEmp.Tables(0).Rows(0)("crapproverunkid"))
                Else
                    cboFromApprover.Tag = Nothing
                End If

            ElseIf ApproverType = enSwapApproverType.Performance Then

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboFromLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboToApprover_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboToApprover.SelectedIndexChanged
        Try

            'Pinkal (18-Jun-2020) -- Start
            'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
            If ApproverType = enSwapApproverType.Leave OrElse ApproverType = enSwapApproverType.Loan OrElse ApproverType = enSwapApproverType.Claim_Request Then
                GetApproverLevel(sender, CInt(cboToApprover.SelectedValue), CBool(CType(cboToApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
            Else
                GetApproverLevel(sender, CInt(cboToApprover.SelectedValue))
            End If
            'Pinkal (18-Jun-2020) -- End

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboToApprover_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

    Private Sub cboToLevel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboToLevel.SelectedIndexChanged
        Try
            If ApproverType = enSwapApproverType.Leave Then
                dsToApproverEmp = Nothing


                'Pinkal (14-May-2020) -- Start
                'Enhancement NMB Leave Approver Migration Changes -   Working on Put options like show inactive approvers & show inactive employees on Leave Approver Migration screen.
                'dsToApproverEmp = objLeaveApprover.GetEmployeeFromApprover(CInt(cboToApprover.SelectedValue), CInt(cboToLevel.SelectedValue) _
                '                                                          , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), CBool(CType(cboToApprover.SelectedItem, DataRowView).Item("isexternalapprover")))


                dsToApproverEmp = objLeaveApprover.GetEmployeeFromApprover(FinancialYear._Object._DatabaseName, CInt(cboToApprover.SelectedValue), CInt(cboToLevel.SelectedValue) _
                                                                          , eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate).Date _
                                                                          , CBool(CType(cboToApprover.SelectedItem, DataRowView).Item("isexternalapprover")), True)
                
                'Pinkal (14-May-2020) -- End


                FillToApproverEmployeeList(dsToApproverEmp)
                If dsToApproverEmp IsNot Nothing AndAlso dsToApproverEmp.Tables(0).Rows.Count > 0 Then
                    cboToApprover.Tag = CInt(dsToApproverEmp.Tables(0).Rows(0)("approverunkid"))
                Else
                    cboToApprover.Tag = Nothing
                End If

            ElseIf ApproverType = enSwapApproverType.Loan Then

                dsToApproverEmp = Nothing
                'Nilay (01-Mar-2016) -- Start
                'ENHANCEMENT - Implementing External Approval changes 
                'dsToApproverEmp = objLoanApprover.GetEmployeeFromLoanApprover(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                '                                                              FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                '                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                '                                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                '                                                              ConfigParameter._Object._IsIncludeInactiveEmp, "List", _
                '                                                              CInt(cboToApprover.SelectedValue), CInt(cboToLevel.SelectedValue))

                dsToApproverEmp = objLoanApprover.GetEmployeeFromLoanApprover(FinancialYear._Object._DatabaseName, User._Object._Userunkid, _
                                                                              FinancialYear._Object._YearUnkid, Company._Object._Companyunkid, _
                                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                              eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate), _
                                                                              ConfigParameter._Object._UserAccessModeSetting, True, _
                                                                              ConfigParameter._Object._IsIncludeInactiveEmp, "List", _
                                                                              CInt(cboToApprover.SelectedValue), CInt(cboToLevel.SelectedValue), _
                                                                              CBool(CType(cboToApprover.SelectedItem, DataRowView).Item("isexternalapprover")))
                'Nilay (01-Mar-2016) -- End

                
                FillToApproverEmployeeList(dsToApproverEmp)
                If dsToApproverEmp IsNot Nothing AndAlso dsToApproverEmp.Tables(0).Rows.Count > 0 Then
                    cboToApprover.Tag = CInt(dsToApproverEmp.Tables(0).Rows(0)("lnapproverunkid"))
                Else
                    cboToApprover.Tag = Nothing
                End If

            ElseIf ApproverType = enSwapApproverType.Claim_Request Then

                Dim dsList As DataSet = Nothing
                Dim dr As DataRowView = CType(cboToApprover.SelectedItem, DataRowView)

                'Pinkal (18-Jun-2020) -- Start
                'ENHANCEMENT NMB:  Working on Claim Approver Migration To change the Approver Logic.
                'If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then
                '    dsToApproverEmp = objExpenseApprover.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboToApprover.SelectedValue), CInt(dr("ApproverEmpunkid")), CInt(cboToLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'Else
                '    dsToApproverEmp = objExpenseApprover.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboToApprover.SelectedValue), 0, CInt(cboToLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                'End If

                If dr IsNot Nothing AndAlso dr.DataView.Count > 0 Then
                    dsToApproverEmp = objExpenseApprover.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboCategory.SelectedValue), 0, CInt(cboToApprover.SelectedValue), CInt(cboToLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                Else
                    dsToApproverEmp = objExpenseApprover.GetApproverAccess(FinancialYear._Object._DatabaseName, CInt(cboCategory.SelectedValue), 0, CInt(cboToApprover.SelectedValue), CInt(cboToLevel.SelectedValue), eZeeDate.convertDate(ConfigParameter._Object._EmployeeAsOnDate))
                End If
                'Pinkal (18-Jun-2020) -- End

                


                FillToApproverEmployeeList(dsToApproverEmp)

                If dsToApproverEmp IsNot Nothing AndAlso dsToApproverEmp.Tables(0).Rows.Count > 0 Then
                    cboToApprover.Tag = CInt(dsToApproverEmp.Tables(0).Rows(0)("crapproverunkid"))
                Else
                    cboToApprover.Tag = Nothing
                End If

            ElseIf ApproverType = enSwapApproverType.Performance Then

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboToLevel_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
            Call SetLanguage()
			
			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnSwap.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSwap.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.LblFromLevel.Text = Language._Object.getCaption(Me.LblFromLevel.Name, Me.LblFromLevel.Text)
			Me.lblToApprover.Text = Language._Object.getCaption(Me.lblToApprover.Name, Me.lblToApprover.Text)
			Me.lblToLevel.Text = Language._Object.getCaption(Me.lblToLevel.Name, Me.lblToLevel.Text)
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.btnSwap.Text = Language._Object.getCaption(Me.btnSwap.Name, Me.btnSwap.Text)
			Me.dgcolhFromEmpcode.HeaderText = Language._Object.getCaption(Me.dgcolhFromEmpcode.Name, Me.dgcolhFromEmpcode.HeaderText)
			Me.dgcolhFromEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhFromEmployee.Name, Me.dgcolhFromEmployee.HeaderText)
			Me.dgcolhToEmpCode.HeaderText = Language._Object.getCaption(Me.dgcolhToEmpCode.Name, Me.dgcolhToEmpCode.HeaderText)
			Me.dgcolhToEmployee.HeaderText = Language._Object.getCaption(Me.dgcolhToEmployee.Name, Me.dgcolhToEmployee.HeaderText)
			Me.EZeeHeading1.Text = Language._Object.getCaption(Me.EZeeHeading1.Name, Me.EZeeHeading1.Text)
			Me.LblCategory.Text = Language._Object.getCaption(Me.LblCategory.Name, Me.LblCategory.Text)
			Me.lblFromApprover.Text = Language._Object.getCaption(Me.lblFromApprover.Name, Me.lblFromApprover.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Select")
			Language.setMessage(mstrModuleName, 2, "Please Select From Approver to swap employee(s).")
			Language.setMessage(mstrModuleName, 3, "Please Select Level to swap employee(s).")
			Language.setMessage(mstrModuleName, 4, "Please Select To Approver to swap employee(s).")
			Language.setMessage(mstrModuleName, 5, "Approver swapping done successfully.")
			Language.setMessage(mstrModuleName, 6, "Sorry, You cannot swap approver")
			Language.setMessage(mstrModuleName, 7, ".Reason:the selected")
			Language.setMessage(mstrModuleName, 8, "is already in the list of")
			Language.setMessage(mstrModuleName, 9, "as an employee.")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class