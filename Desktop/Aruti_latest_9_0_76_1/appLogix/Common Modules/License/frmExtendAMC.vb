﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib

#End Region

Public Class frmExtendAMC
    Private ReadOnly mstrModuleName As String = "frmExtendAMC"

#Region " Form Event(s) "

    Private Sub frmExtendAMC_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objlblMessage.Text = "Aruti has encountered some issues and not able to fix. " & vbCrLf & _
                                 "Please try to fix it with Auto fix. If not fixed with Auto fix option then please contact aruti support team to fix it manually by applying the patch."
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmExtendAMC_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Event(s) "

    Private Sub btnAutoFix_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAutoFix.Click
        Me.Cursor = Cursors.WaitCursor
        Me.Enabled = False
        Try
            ezWait.Active = True
            ezWait.Refresh()
            System.Threading.Thread.Sleep(6000)
            objlblMessage.Text = "Sorry, we could not fix the issue, Please try to fix the issue manually by clicking Apply fix button."
            ezWait.Active = False

            txtPatch.Enabled = True
            btnActivate.Enabled = True
            btnAutoFix.Enabled = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnAutoFix_Click", mstrModuleName)
        Finally
            Me.Cursor = Cursors.Default
            Me.Enabled = True
        End Try
    End Sub

    Private Sub btnActivate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnActivate.Click
        Me.Cursor = Cursors.WaitCursor
        Me.Enabled = False
        Try
            If txtPatch.Text.Trim().Length <= 0 Then
                eZeeMsgBox.Show("Please enter code in order to fix it manually.", enMsgBoxStyle.Information)
                Exit Sub
            End If

            ezWait.Active = True
            ezWait.Refresh()
            objlblMessage.Text = "Applying the fix, Please wait for a while."
            Dim xMsg As String = "" : Dim blnOut As Boolean = False
            blnOut = clsMasterData.ExtendAMS_Date(txtPatch.Text.Trim(), xMsg)
            If blnOut = False AndAlso xMsg <> "" Then
                objlblMessage.Text = xMsg
            End If
            System.Threading.Thread.Sleep(6000)
            If blnOut Then
                eZeeMsgBox.Show("Fix applied successfully. Please restart aruti.", enMsgBoxStyle.Information)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnActivate_Click", mstrModuleName)
        Finally
            ezWait.Active = False
            Me.Cursor = Cursors.Default
            Me.Enabled = True
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

#End Region

End Class