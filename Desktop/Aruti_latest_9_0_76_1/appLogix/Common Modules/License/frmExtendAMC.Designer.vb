﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmExtendAMC
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExtendAMC))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.objefFormFooter = New eZee.Common.eZeeFooter
        Me.btnActivate = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.ezWait = New eZee.Common.eZeeWait
        Me.btnAutoFix = New eZee.Common.eZeeLightButton(Me.components)
        Me.objlblMessage = New System.Windows.Forms.Label
        Me.txtPatch = New System.Windows.Forms.TextBox
        Me.pnlMain.SuspendLayout()
        Me.objefFormFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.txtPatch)
        Me.pnlMain.Controls.Add(Me.objlblMessage)
        Me.pnlMain.Controls.Add(Me.objefFormFooter)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(454, 194)
        Me.pnlMain.TabIndex = 0
        '
        'objefFormFooter
        '
        Me.objefFormFooter.BorderColor = System.Drawing.Color.Silver
        Me.objefFormFooter.Controls.Add(Me.btnAutoFix)
        Me.objefFormFooter.Controls.Add(Me.ezWait)
        Me.objefFormFooter.Controls.Add(Me.btnActivate)
        Me.objefFormFooter.Controls.Add(Me.btnCancel)
        Me.objefFormFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objefFormFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objefFormFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objefFormFooter.GradientColor1 = System.Drawing.Color.Transparent
        Me.objefFormFooter.GradientColor2 = System.Drawing.Color.Transparent
        Me.objefFormFooter.Location = New System.Drawing.Point(0, 139)
        Me.objefFormFooter.Name = "objefFormFooter"
        Me.objefFormFooter.Size = New System.Drawing.Size(454, 55)
        Me.objefFormFooter.TabIndex = 3
        '
        'btnActivate
        '
        Me.btnActivate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnActivate.BackColor = System.Drawing.Color.White
        Me.btnActivate.BackgroundImage = CType(resources.GetObject("btnActivate.BackgroundImage"), System.Drawing.Image)
        Me.btnActivate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnActivate.BorderColor = System.Drawing.Color.Empty
        Me.btnActivate.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnActivate.Enabled = False
        Me.btnActivate.FlatAppearance.BorderSize = 0
        Me.btnActivate.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActivate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnActivate.ForeColor = System.Drawing.Color.Black
        Me.btnActivate.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnActivate.GradientForeColor = System.Drawing.Color.Black
        Me.btnActivate.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnActivate.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnActivate.Location = New System.Drawing.Point(236, 12)
        Me.btnActivate.Name = "btnActivate"
        Me.btnActivate.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnActivate.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnActivate.Size = New System.Drawing.Size(100, 33)
        Me.btnActivate.TabIndex = 1
        Me.btnActivate.Text = "&Apply Fix"
        Me.btnActivate.UseVisualStyleBackColor = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(342, 12)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(100, 33)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = False
        '
        'ezWait
        '
        Me.ezWait.Active = False
        Me.ezWait.CircleRadius = 15
        Me.ezWait.Location = New System.Drawing.Point(12, 6)
        Me.ezWait.Name = "ezWait"
        Me.ezWait.NumberSpoke = 10
        Me.ezWait.RotationSpeed = 100
        Me.ezWait.Size = New System.Drawing.Size(45, 44)
        Me.ezWait.SpokeColor = System.Drawing.Color.SeaGreen
        Me.ezWait.SpokeHeight = 5
        Me.ezWait.SpokeThickness = 5
        Me.ezWait.TabIndex = 3
        Me.ezWait.Visible = False
        '
        'btnAutoFix
        '
        Me.btnAutoFix.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAutoFix.BackColor = System.Drawing.Color.White
        Me.btnAutoFix.BackgroundImage = CType(resources.GetObject("btnAutoFix.BackgroundImage"), System.Drawing.Image)
        Me.btnAutoFix.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnAutoFix.BorderColor = System.Drawing.Color.Empty
        Me.btnAutoFix.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnAutoFix.FlatAppearance.BorderSize = 0
        Me.btnAutoFix.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAutoFix.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAutoFix.ForeColor = System.Drawing.Color.Black
        Me.btnAutoFix.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnAutoFix.GradientForeColor = System.Drawing.Color.Black
        Me.btnAutoFix.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAutoFix.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnAutoFix.Location = New System.Drawing.Point(12, 12)
        Me.btnAutoFix.Name = "btnAutoFix"
        Me.btnAutoFix.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnAutoFix.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnAutoFix.Size = New System.Drawing.Size(100, 33)
        Me.btnAutoFix.TabIndex = 4
        Me.btnAutoFix.Text = "A&uto Fix"
        Me.btnAutoFix.UseVisualStyleBackColor = False
        '
        'objlblMessage
        '
        Me.objlblMessage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objlblMessage.ForeColor = System.Drawing.Color.Maroon
        Me.objlblMessage.Location = New System.Drawing.Point(9, 9)
        Me.objlblMessage.Name = "objlblMessage"
        Me.objlblMessage.Size = New System.Drawing.Size(433, 93)
        Me.objlblMessage.TabIndex = 1
        '
        'txtPatch
        '
        Me.txtPatch.BackColor = System.Drawing.SystemColors.Info
        Me.txtPatch.Enabled = False
        Me.txtPatch.Location = New System.Drawing.Point(12, 110)
        Me.txtPatch.Name = "txtPatch"
        Me.txtPatch.Size = New System.Drawing.Size(430, 21)
        Me.txtPatch.TabIndex = 4
        '
        'frmExtendAMC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(454, 194)
        Me.Controls.Add(Me.pnlMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmExtendAMC"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Troubleshoot Aruti"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.objefFormFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents objefFormFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnActivate As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents ezWait As eZee.Common.eZeeWait
    Friend WithEvents btnAutoFix As eZee.Common.eZeeLightButton
    Friend WithEvents txtPatch As System.Windows.Forms.TextBox
    Friend WithEvents objlblMessage As System.Windows.Forms.Label
End Class
