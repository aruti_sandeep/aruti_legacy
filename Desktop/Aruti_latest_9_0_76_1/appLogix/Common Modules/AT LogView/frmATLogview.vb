﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmATLogview

#Region " Private Variables "

    Private ReadOnly mstrModuleName As String = "frmATLogview"
    Private mblnCancel As Boolean = True
    Private objCommonATLog As clsCommonATLog
    Dim dsModuleList As DataSet = Nothing
    Dim dsScreenList As DataSet = Nothing
    Dim StrName As String = String.Empty
    Dim StrParentNode As String = String.Empty
    Dim dtChild As New DataTable
    Dim mblnConfiguration As Boolean = False
#End Region

#Region " Private Methods "

    Private Sub SetColor()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetColor", mstrModuleName)
        End Try
    End Sub

    Private Sub GetValue()
        Try


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub SetValue()
        Try

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "SetValue", mstrModuleName)
        End Try
    End Sub

    Private Sub FillCombo()
        Try
            Dim objUser As New clsUserAddEdit
            Dim dsList As DataSet = objUser.getComboList("List", True)
            With cboUser
                .DisplayMember = "name"
                .ValueMember = "userunkid"
                .DataSource = dsList.Tables(0)
            End With

            Dim objAudit As New clsMasterData
            dsList = objAudit.GetAuditTypeList("Audit", True, True, True)
            With cboEventType
                .ValueMember = "id"
                .DisplayMember = "name"
                .DataSource = dsList.Tables(0)
            End With
            objAudit = Nothing

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillCombo", mstrModuleName)
        End Try
    End Sub

    'Private Sub FillModuleList()
    '    Try
    '        If dsModuleList.Tables.Count > 0 Then

    '            For Each drModule As DataRow In dsModuleList.Tables("List").Rows
    '                Dim tvNodeModule As TreeNode
    '                If tvModuleList.Nodes.ContainsKey(drModule.Item("module_name1").ToString) = False Then
    '                    tvNodeModule = tvModuleList.Nodes.Add(drModule.Item("module_name1").ToString)
    '                    tvNodeModule.Name = drModule.Item("module_name1").ToString()
    '                End If

    '                Dim drM1Screen As DataRow() = dsScreenList.Tables(0).Select("module_name1='" & drModule.Item("module_name1").ToString & "' AND module_name2 = '' AND module_name3 = '' AND module_name4 = '' AND module_name5 = ''")

    '                If drM1Screen.Length > 0 Then
    '                    For Each drscreen As DataRow In drM1Screen
    '                        If tvModuleList.Nodes.Find(drscreen.Item("form_name").ToString, True).Length <= 0 Then
    '                            tvNodeModule.Nodes.Add(drModule.Item("module_name1").ToString(), drscreen.Item("form_name").ToString)
    '                            tvNodeModule.Nodes(drModule.Item("module_name1").ToString()).Name = drscreen.Item("form_name").ToString
    '                        End If
    '                    Next

    '                End If

    '                Dim drM2 As DataRow() = dsModuleList.Tables(0).Select("module_name1 = '" & drModule.Item("module_name1").ToString & "' AND module_name1 <>''")
    '                If drM2.Length > 0 Then

    '                    For Each drModule2 As DataRow In drM2

    '                        If drModule2.Item("module_name2").ToString() <> "" Then
    '                            If tvModuleList.Nodes.Find(drModule2.Item("module_name2").ToString(), True).Length <= 0 Then
    '                                tvNodeModule.Nodes.Add(drModule.Item("module_name1").ToString(), drModule2.Item("module_name2").ToString)
    '                                tvNodeModule.Nodes(drModule.Item("module_name1").ToString()).Name = drModule2.Item("module_name2").ToString
    '                            End If
    '                        End If

    '                        Dim drM2Screen As DataRow() = dsScreenList.Tables(0).Select("module_name1='" & drModule.Item("module_name1").ToString & "' AND module_name2 = '" & drModule2.Item("module_name2").ToString() & "' AND module_name3 = '' AND module_name4 = '' AND module_name5 = ''")

    '                        If drM2Screen.Length > 0 Then
    '                            For Each drscreen As DataRow In drM2Screen
    '                                If tvModuleList.Nodes.Find(drscreen.Item("form_name").ToString, True).Length <= 0 Then
    '                                    tvNodeModule.Nodes(drModule2.Item("module_name2").ToString()).Nodes.Add(drModule2.Item("module_name2").ToString(), drscreen.Item("form_name").ToString)
    '                                    tvNodeModule.Nodes(drModule2.Item("module_name2").ToString()).Nodes(0).Name = drscreen.Item("form_name").ToString
    '                                    dsScreenList.Tables(0).Rows.Remove(drscreen)
    '                                    dsScreenList.AcceptChanges()
    '                                End If
    '                            Next

    '                        End If


    '                        Dim drM3 As DataRow() = dsModuleList.Tables(0).Select("module_name2 = '" & drModule2.Item("module_name2").ToString & "' AND module_name2 <>''")
    '                        If drM3.Length > 0 Then

    '                            For Each drModule3 As DataRow In drM3

    '                                If drModule3.Item("module_name3").ToString() <> "" Then

    '                                    If tvModuleList.Nodes.Find(drModule3.Item("module_name3").ToString(), True).Length <= 0 Then
    '                                        tvNodeModule.Nodes(drModule2.Item("module_name2").ToString()).Nodes.Add(drModule2.Item("module_name2").ToString(), drModule3.Item("module_name3").ToString)
    '                                        tvNodeModule.Nodes(drModule2.Item("module_name2").ToString()).Name = drModule3.Item("module_name3").ToString
    '                                    End If

    '                                End If

    '                                Dim drM3Screen As DataRow() = dsScreenList.Tables(0).Select("module_name1='" & drModule.Item("module_name1").ToString & "' AND module_name2 = '" & drModule2.Item("module_name2").ToString() & "' AND module_name3 = '" & drModule3.Item("module_name3").ToString() & "' AND module_name4 = '' AND module_name5 = ''")

    '                                If drM3Screen.Length > 0 Then

    '                                    For Each drscreen As DataRow In drM3Screen

    '                                        If tvModuleList.Nodes.Find(drscreen.Item("form_name").ToString, True).Length <= 0 Then
    '                                            tvNodeModule.Nodes(drModule3.Item("module_name3").ToString()).Nodes(0).Nodes.Add(drModule3.Item("module_name3").ToString(), drscreen.Item("form_name").ToString)
    '                                            tvNodeModule.Nodes(drModule3.Item("module_name3").ToString()).Nodes(0).Nodes(0).Name = drscreen.Item("form_name").ToString
    '                                            dsScreenList.Tables(0).Rows.Remove(drscreen)
    '                                            dsScreenList.AcceptChanges()
    '                                        End If

    '                                    Next

    '                                End If

    '                            Next

    '                        End If

    '                    Next

    '                End If

    '            Next

    '            If tvModuleList.Nodes.Count > 0 Then tvModuleList.ExpandAll()

    '        End If


    '    Catch ex As Exception
    '        DisplayError.Show("-1", ex.Message, "FillModuleList", mstrModuleName)
    '    End Try
    'End Sub

    Private Sub FillModuleList()
        Try
            If dsModuleList.Tables.Count > 0 Then

                For Each drModule As DataRow In dsModuleList.Tables("List").Rows
                    Dim tvNodeModule As TreeNode
                    If tvModuleList.Nodes.ContainsKey(drModule.Item("module_name1").ToString) = False Then
                        tvNodeModule = tvModuleList.Nodes.Add(drModule.Item("module_name1").ToString)
                        tvNodeModule.Name = drModule.Item("module_name1").ToString()

                        Dim drM1Screen As DataRow() = dsScreenList.Tables(0).Select("module_name1='" & drModule.Item("module_name1").ToString() & "' AND module_name2 = '' AND module_name3 = '' AND module_name4 = '' AND module_name5 = ''")

                        If drM1Screen.Length > 0 Then
                            For Each drscreen As DataRow In drM1Screen
                                If tvModuleList.Nodes.Find(drscreen.Item("form_name").ToString, True).Length <= 0 Then
                                    'tvModuleList.Nodes(drModule.Item("module_name1").ToString()).Nodes.Add(drModule.Item("module_name1").ToString(), drscreen.Item("form_name").ToString)
                                    tvModuleList.Nodes(drModule.Item("module_name1").ToString()).Nodes.Add(drscreen.Item("Org_Name").ToString(), drscreen.Item("form_name").ToString)
                                End If
                            Next

                        End If

                    End If

                Next

                For i As Integer = 0 To tvModuleList.Nodes.Count - 1

                    Dim drM2Screen As DataRow() = dsScreenList.Tables(0).Select("module_name1='" & tvModuleList.Nodes(i).Text.ToString() & "' AND module_name2 <> ''")

                    If drM2Screen.Length > 0 Then
                        Dim tvNodeModule As TreeNode = Nothing
                        Dim mstrModule1 As String = ""
                        Dim mstrModule2 As String = ""
                        Dim intCount As Integer = -1
                        For Each drscreen As DataRow In drM2Screen
                            If drscreen.Item("module_name2").ToString() <> mstrModule2 Then
                                tvNodeModule = tvModuleList.Nodes(drscreen.Item("module_name1").ToString()).Nodes.Add(drscreen.Item("module_name1").ToString(), drscreen.Item("module_name2").ToString)
                            End If
                            If drscreen.Item("module_name3").ToString() = "" Then
                                'tvNodeModule.Nodes.Add(drscreen.Item("module_name2").ToString(), drscreen.Item("form_name").ToString)
                                tvNodeModule.Nodes.Add(drscreen.Item("Org_Name").ToString(), drscreen.Item("form_name").ToString)
                            End If
                            mstrModule2 = drscreen.Item("module_name2").ToString()
                        Next

                    End If

                Next

                For i As Integer = 0 To tvModuleList.Nodes.Count - 1

                    Dim drM3Screen As DataRow() = dsScreenList.Tables(0).Select("module_name1='" & tvModuleList.Nodes(i).Text.ToString() & "' AND module_name2 <> '' AND module_name3 <> ''")

                    If drM3Screen.Length > 0 Then
                        Dim tvNodeModule As TreeNode = tvModuleList.Nodes(0)
                        Dim mstrModule3 As String = ""
                        Dim mstrModule2 As String = ""
                        For Each drscreen As DataRow In drM3Screen


                            If mstrModule3 <> drscreen.Item("module_name3").ToString() Then
                                'If mstrModule2 = drscreen.Item("module_name2").ToString() Or mstrModule2 = "" Then
                                '    tvNodeModule = tvModuleList.Nodes(drscreen.Item("module_name1").ToString()).Nodes(0).Nodes.Add(drscreen.Item("module_name2").ToString(), drscreen.Item("module_name3").ToString)
                                'Else
                                For k As Integer = 0 To tvModuleList.Nodes(drscreen.Item("module_name1").ToString()).Nodes.Count - 1

                                    If tvModuleList.Nodes(drscreen.Item("module_name1").ToString()).Nodes(k).Text = drscreen.Item("module_name2").ToString Then
                                        tvNodeModule = tvModuleList.Nodes(drscreen.Item("module_name1").ToString()).Nodes(k).Nodes.Add(drscreen.Item("module_name2").ToString(), drscreen.Item("module_name3").ToString)
                                    End If

                                Next

                                ' End If
                                'mstrModule2 = drscreen.Item("module_name2").ToString()
                            End If
                            mstrModule3 = drscreen.Item("module_name3").ToString()

                            'tvNodeModule.Nodes.Add(drscreen.Item("module_name3").ToString(), drscreen.Item("form_name").ToString)
                            tvNodeModule.Nodes.Add(drscreen.Item("Org_Name").ToString(), drscreen.Item("form_name").ToString)
                        Next

                    End If

                Next


                ' If tvModuleList.Nodes.Count > 0 Then tvModuleList.ExpandAll()

            End If


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "FillModuleList", mstrModuleName)
        End Try
    End Sub

    Private Sub Fill_Child_Report()
        Try

            If dgvAuditData.RowCount > 0 Then
                lvData.Items.Clear()
                lvData.Columns.Clear()
            Else
                lvData.Items.Clear()
                lvData.Columns.Clear()
                Exit Sub
            End If


            Dim StrFilter As String = String.Empty
            'StrFilter = Set_Filter()

            dtChild = clsCommonATLog.Get_Child_Data(StrName, CStr(cboChildData.SelectedValue))

            dtChild = New DataView(dtChild, StrFilter, "GName", DataViewRowState.CurrentRows).ToTable

            dtChild.Columns.Remove("Uid") : dtChild.Columns.Remove("ETYId")
            dtChild.Columns.Remove("ADate")

            If dtChild.Rows.Count > 0 Then
                Dim strColumns As String = String.Empty
                Dim iCntColumns As Integer = 0

                For Each dCol As DataColumn In dtChild.Columns
                    lvData.Columns.Add(dCol.ColumnName, 100, HorizontalAlignment.Left)
                    strColumns &= "," & dCol.ColumnName
                    iCntColumns += 1
                Next
                strColumns = Mid(strColumns, 2)
                Dim strColumnName() As String = strColumns.Split(CChar(","))

                For Each dRow As DataRow In dtChild.Rows
                    Dim lvItem As New ListViewItem
                    For i As Integer = 0 To iCntColumns - 1
                        Select Case i
                            Case 0
                                lvItem.Text = dRow.Item(strColumnName(i)).ToString
                            Case Else
                                lvItem.SubItems.Add(dRow.Item(strColumnName(i)).ToString)
                        End Select
                    Next
                    lvData.Items.Add(lvItem)
                Next

                lvData.Columns(0).Width = 0
                lvData.GroupingColumn = lvData.Columns(0)
                lvData.DisplayGroups(True)
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Fill_Child_Report", mstrModuleName)
        End Try
    End Sub

    Private Function Set_Filter() As String
        Dim StrFilter As String = String.Empty
        Try

            If CInt(cboUser.SelectedValue) > 0 Then
                StrFilter = "Uid = '" & CInt(cboUser.SelectedValue) & "'"
            End If

            If CInt(cboEventType.SelectedValue) > 0 Then
                If StrFilter.Trim.Length > 0 Then
                    StrFilter &= " AND ETYId = '" & CInt(cboEventType.SelectedValue) & "'"
                Else
                    StrFilter &= "ETYId = '" & CInt(cboEventType.SelectedValue) & "'"
                End If
            End If

            If dtpFromDate.Checked = True Then
                If StrFilter.Trim.Length > 0 Then
                    StrFilter &= " AND ADate >= " & eZeeDate.convertDate(dtpFromDate.Value)
                Else
                    StrFilter &= "ADate >= " & eZeeDate.convertDate(dtpFromDate.Value)
                End If
            End If


            If dtpTodate.Checked = True Then
                If StrFilter.Trim.Length > 0 Then
                    StrFilter &= " AND ADate <= " & eZeeDate.convertDate(dtpTodate.Value)
                Else
                    StrFilter &= "ADate <= " & eZeeDate.convertDate(dtpTodate.Value)
                End If
            End If
            Return StrFilter
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "Set_Filter", mstrModuleName)
            Return ""
        End Try
    End Function

    Private Function Store_User_Log(ByVal blnIsSearch As Boolean) As Boolean
        Dim objViewAT As New clsView_AT_Logs
        Try
            objViewAT._Atviewmodeid = clsView_AT_Logs.enAT_View_Mode.APPLICANT_EVENT_LOG
            objViewAT._Userunkid = User._Object._Userunkid
            objViewAT._Viewdatetime = ConfigParameter._Object._CurrentDateAndTime
            If blnIsSearch = True Then
                objViewAT._Viewoperationid = clsView_AT_Logs.enAT_OperationId.SEARCH
            Else
                objViewAT._Viewoperationid = clsView_AT_Logs.enAT_OperationId.EXPORT
            End If

            Dim StrXML_Value As String = ""
            StrXML_Value = "<" & Me.Name & " xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"">" & vbCrLf

            For Each ctrl As Control In pnlTreeData.Controls
                If TypeOf ctrl Is TreeView Then
                    If CType(ctrl, TreeView).SelectedNode IsNot Nothing Then
                    If CType(ctrl, TreeView).SelectedNode.IsSelected = True Then
                        StrXML_Value &= "<" & CType(ctrl, TreeView).Name & ">" & CType(ctrl, TreeView).SelectedNode.Name & "</" & CType(ctrl, TreeView).Name & ">" & vbCrLf
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                End If
            Next

            For Each ctrl As Control In gbGroupList.Controls
                If TypeOf ctrl Is DateTimePicker Then
                    If CType(ctrl, DateTimePicker).Checked = True Then
                        StrXML_Value &= "<" & ctrl.Name & ">" & eZeeDate.convertDate(CType(ctrl, DateTimePicker).Value) & "</" & ctrl.Name & ">" & vbCrLf
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                ElseIf TypeOf ctrl Is ComboBox Then
                    If CType(ctrl, ComboBox).SelectedIndex >= 0 Then
                        If CType(ctrl, ComboBox).DataSource IsNot Nothing Then
                            StrXML_Value &= "<" & ctrl.Name & ">" & CInt(CType(ctrl, ComboBox).SelectedValue) & "</" & ctrl.Name & ">" & vbCrLf
                        Else
                            StrXML_Value &= "<" & ctrl.Name & ">" & CType(ctrl, ComboBox).SelectedIndex & "</" & ctrl.Name & ">" & vbCrLf
                        End If
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                ElseIf TypeOf ctrl Is RadioButton Then
                    If CType(ctrl, RadioButton).Checked = True Then
                        StrXML_Value &= "<" & ctrl.Name & ">" & CType(ctrl, RadioButton).Checked & "</" & ctrl.Name & ">" & vbCrLf
                    Else
                        StrXML_Value &= "<" & ctrl.Name & "/>" & vbCrLf
                    End If
                End If
            Next

            If cboChildData.Visible = True Then
                StrXML_Value &= "<" & cboChildData.Name & ">" & cboChildData.Text & "</" & cboChildData.Name & ">" & vbCrLf
            End If

            StrXML_Value &= "</" & Me.Name & ">"

            objViewAT._Value_View = StrXML_Value

            If objViewAT.Insert = False Then
                eZeeMsgBox.Show(objViewAT._Message, enMsgBoxStyle.Information)
                Return False
            End If

            Return True

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "Store_User_Log", mstrModuleName)
        Finally
            objViewAT = Nothing
        End Try
    End Function

#End Region

#Region " Form's Events "

    Private Sub frmATLogview_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Set_Logo(Me, gApplicationType)
            objCommonATLog = New clsCommonATLog

            tabcData.TabPages.Remove(tabpChildDetail) : cboChildData.Visible = False

            dsModuleList = objCommonATLog.GetModuleName(False, mblnConfiguration)
            dsScreenList = objCommonATLog.GetModuleName(True, mblnConfiguration)
            FillModuleList()
            FillCombo()
            lvData.GridLines = False
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmATLogview_Load", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub objbtnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If Store_User_Log(True) = False Then Exit Sub

            Dim dtDataSource As DataTable
            If StrName.Trim.Length > 0 AndAlso StrName.StartsWith("frm") Then

                If StrName = "frmEmployee_Exemption_AddEdit" AndAlso StrParentNode = Language._Object.getCaption("mnuEmployeeData", "&Employee Data").Replace("&", "") Then
                    StrName = StrName & "_1"
                ElseIf StrName = "frmSalaryGlobalAssign" AndAlso StrParentNode = Language._Object.getCaption("mnuAppraisal", "A&ppraisal").Replace("&", "") Then
                    StrName = StrName & "_1"
                End If

                'Pinkal (24-Jul-2012) -- Start
                'Enhancement : TRA Changes
                If StrName = "frmSalaryGlobalAssign" AndAlso StrParentNode = Language._Object.getCaption("mnuJobTraining", "On Job &Training").Replace("&", "") Then
                    StrName = StrName & "_1"
                End If

                If StrName = "frmEmployeeMaster" AndAlso StrParentNode = Language._Object.getCaption("mnuJobTraining", "On Job &Training").Replace("&", "") Then
                    StrName = StrName & "_1"
                End If
                'Pinkal (24-Jul-2012) -- End

                Cursor.Current = Cursors.WaitCursor 'Sohail (04 Aug 2012)

                dtDataSource = clsCommonATLog.View_Applicantion_Log(StrName)

                Dim StrFilter As String = String.Empty

                StrFilter = Set_Filter()

                dtDataSource = New DataView(dtDataSource, StrFilter, "", DataViewRowState.CurrentRows).ToTable

                dtDataSource.Columns.Remove("Uid") : dtDataSource.Columns.Remove("ETYId")
                dtDataSource.Columns.Remove("ADate")

                dgvAuditData.DataSource = dtDataSource

                If cboChildData.Visible = True Then
                    Call Fill_Child_Report()
                End If

            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnSearch_Click", mstrModuleName)
            'Sohail (04 Aug 2012) -- Start
            'TRA - ENHANCEMENT
        Finally
            Cursor.Current = Cursors.Default
            'Sohail (04 Aug 2012) -- End
        End Try
    End Sub

    Private Sub objbtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Try
            cboEventType.SelectedIndex = 0
            cboUser.SelectedValue = 0
            dtpFromDate.Checked = False
            dtpTodate.Checked = False
            dgvAuditData.DataSource = Nothing
            lvData.GridLines = False
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "objbtnReset_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub objbtnSearchUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnSearchUser.Click
        Dim objfrm As New frmCommonSearch
        Dim dtList As DataTable
        Try
            dtList = CType(cboUser.DataSource, DataTable)
            With cboUser
                objfrm.DataSource = dtList
                objfrm.ValueMember = .ValueMember
                objfrm.DisplayMember = .DisplayMember
                objfrm.CodeMember = "employeecode"
                If objfrm.DisplayDialog Then
                    .SelectedValue = objfrm.SelectedValue
                End If
                .Focus()
            End With
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "objbtnSearchUser_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If Store_User_Log(False) = False Then Exit Sub

            Dim dtReader As DataTable = CType(dgvAuditData.DataSource, DataTable)

            If dtReader IsNot Nothing Then
                Dim strBuilder As New System.Text.StringBuilder
                strBuilder.Append(" <HTML> " & vbCrLf)
                strBuilder.Append(" <BODY><FONT FACE = TAHOMA FONT SIZE=2> " & vbCrLf)
                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='50%'> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='CENTER' COLSPAN='2'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 500, "Application Log Events") & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 501, "From Date : ") & "</B></FONT></TD> " & vbCrLf)
                If dtpFromDate.Checked = True Then
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & dtpFromDate.Value.Date & "</B></FONT></TD> " & vbCrLf)
                Else
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & "&nbsp;" & "</B></FONT></TD> " & vbCrLf)
                End If
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 502, "To Date : ") & "</B></FONT></TD> " & vbCrLf)
                If dtpTodate.Checked = True Then
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & dtpTodate.Value.Date & "</B></FONT></TD> " & vbCrLf)
                Else
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & "&nbsp;" & "</B></FONT></TD> " & vbCrLf)
                End If
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 503, "User : ") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & CStr(IIf(CInt(cboUser.SelectedValue) > 0, cboUser.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='50%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 504, "Event Type : ") & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' WIDTH = '20%'><FONT SIZE=2><B>" & CStr(IIf(CInt(cboEventType.SelectedValue) > 0, cboEventType.Text, "")) & "</B></FONT></TD> " & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)


                strBuilder.Append(" </TABLE> " & vbCrLf)

                strBuilder.Append(" <BR/> " & vbCrLf)

                strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)

                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN='" & dtReader.Columns.Count & "'  BGCOLOR='LightBlue'><FONT SIZE=2><B>" & Language.getMessage(mstrModuleName, 505, "Master Detail") & "</B></FONT></TD>" & vbCrLf)
                strBuilder.Append(" </TR> " & vbCrLf)


                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                For i As Integer = 0 To dtReader.Columns.Count - 1
                    strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & dtReader.Columns(i).Caption.ToString & "</B></FONT></TD> " & vbCrLf)
                Next
                strBuilder.Append(" </TR> " & vbCrLf)


                strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                For j As Integer = 0 To dtReader.Rows.Count - 1
                    For i As Integer = 0 To dtReader.Columns.Count - 1
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dtReader.Rows(j)(i).ToString & "</FONT></TD> " & vbCrLf)
                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)
                Next

                strBuilder.Append(" </TABLE> " & vbCrLf)

                If cboChildData.Visible = True Then
                    strBuilder.Append(" <BR/> " & vbCrLf)
                    strBuilder.Append(" <TABLE BORDER = 1 BORDERCOLOR = BLACK WIDTH='100%'> " & vbCrLf)
                    strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                    strBuilder.Append(" <TD BORDER = 1 BORDERCOLOR=BLACK ALIGN='LEFT' COLSPAN='" & dtChild.Columns.Count & "' BGCOLOR='LightBlue'><FONT SIZE=2><B>" & cboChildData.Text & "</B></FONT></TD>" & vbCrLf)
                    strBuilder.Append(" </TR> " & vbCrLf)

                    strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                    For i As Integer = 0 To dtChild.Columns.Count - 1
                        strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2><B>" & dtChild.Columns(i).Caption & "</B></FONT></TD> " & vbCrLf)
                    Next
                    strBuilder.Append(" </TR> " & vbCrLf)


                    strBuilder.Append(" <TR ALIGN = LEFT VALIGN=TOP WIDTH='100%'> " & vbCrLf)
                    For j As Integer = 0 To dtChild.Rows.Count - 1
                        For i As Integer = 0 To dtChild.Columns.Count - 1
                            strBuilder.Append(" <TD BORDER=1 BORDERCOLOR=BLACK ALIGN='LEFT'><FONT SIZE=2>" & dtChild.Rows(j)(i).ToString & "</FONT></TD> " & vbCrLf)
                        Next
                        strBuilder.Append(" </TR> " & vbCrLf)
                    Next

                End If


                strBuilder.Append(" </BODY> " & vbCrLf)
                strBuilder.Append(" </HTML> " & vbCrLf)

                Dim svDialog As New SaveFileDialog
                svDialog.Filter = "Excel files (*.xls)|*.xls"
                If svDialog.ShowDialog = Windows.Forms.DialogResult.OK Then
                    Dim fsFile As New IO.FileStream(svDialog.FileName, IO.FileMode.Create, IO.FileAccess.Write)
                    Dim strWriter As New IO.StreamWriter(fsFile)
                    With strWriter
                        .BaseStream.Seek(0, IO.SeekOrigin.End)
                        .WriteLine(strBuilder)
                        .Close()
                    End With
                    Diagnostics.Process.Start(svDialog.FileName)
                End If
            End If

            


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnExport_Click", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Treeview Events "

    Private Sub tvModuleList_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvModuleList.AfterSelect
        Try
            StrName = e.Node.Name

            If e.Node.Parent IsNot Nothing Then
                StrParentNode = e.Node.Parent.Text
            End If

            If StrName.Trim.Length > 0 Then
                Dim dList As New DataSet

                If StrName.Trim.Length > 0 AndAlso StrName.StartsWith("frm") Then
                    If StrName = "frmEmployee_Exemption_AddEdit" AndAlso StrParentNode = Language._Object.getCaption("mnuEmployeeData", "&Employee Data").Replace("&", "") Then
                        StrName = StrName & "_1"
                    ElseIf StrName = "frmSalaryGlobalAssign" AndAlso StrParentNode = Language._Object.getCaption("mnuAppraisal", "A&ppraisal").Replace("&", "") Then
                        StrName = StrName & "_1"
                    End If

                    If StrName = "frmSalaryGlobalAssign" AndAlso StrParentNode = Language._Object.getCaption("mnuJobTraining", "On Job &Training").Replace("&", "") Then
                        StrName = StrName & "_1"
                    End If

                    If StrName = "frmEmployeeMaster" AndAlso StrParentNode = Language._Object.getCaption("mnuJobTraining", "On Job &Training").Replace("&", "") Then
                        StrName = StrName & "_1"
                    End If
                End If

                dList = clsCommonATLog.Get_Child_Details(StrName)
                If dList.Tables(0).Rows.Count > 0 Then
                    With cboChildData
                        .ValueMember = "Child"
                        .DisplayMember = "TableName"
                        .DataSource = dList.Tables(0)
                        .SelectedValue = dList.Tables(0).Rows(0)("Child")
                    End With
                    If tabcData.TabPages.Contains(tabpChildDetail) = False Then
                        tabcData.TabPages.Add(tabpChildDetail)
                    End If
                    cboChildData.Visible = True
                Else
                    tabcData.TabPages.Remove(tabpChildDetail) : cboChildData.Visible = False
                End If
            End If
            dgvAuditData.DataSource = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "tvModuleList_AfterSelect", mstrModuleName)
        End Try
    End Sub

#End Region

#Region " Combobox Event(s) "

    Private Sub cboChildData_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboChildData.SelectedIndexChanged
        Try
            tabpChildDetail.Text = cboChildData.Text
            lvData.Items.Clear()
            lvData.Columns.Clear()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "cboChildData_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbGroupList.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbGroupList.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor

			Me.btnExport.GradientBackColor = GUI._ButttonBackColor 
			Me.btnExport.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.gbGroupList.Text = Language._Object.getCaption(Me.gbGroupList.Name, Me.gbGroupList.Text)
			Me.lblUser.Text = Language._Object.getCaption(Me.lblUser.Name, Me.lblUser.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.lblEventType.Text = Language._Object.getCaption(Me.lblEventType.Name, Me.lblEventType.Text)
			Me.lblTodate.Text = Language._Object.getCaption(Me.lblTodate.Name, Me.lblTodate.Text)
			Me.lblFromDate.Text = Language._Object.getCaption(Me.lblFromDate.Name, Me.lblFromDate.Text)
			Me.btnExport.Text = Language._Object.getCaption(Me.btnExport.Name, Me.btnExport.Text)
			Me.btnReset.Text = Language._Object.getCaption(Me.btnReset.Name, Me.btnReset.Text)
			Me.btnSearch.Text = Language._Object.getCaption(Me.btnSearch.Name, Me.btnSearch.Text)
			Me.tabpMasterDetail.Text = Language._Object.getCaption(Me.tabpMasterDetail.Name, Me.tabpMasterDetail.Text)
			Me.tabpChildDetail.Text = Language._Object.getCaption(Me.tabpChildDetail.Name, Me.tabpChildDetail.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 500, "Application Log Events")
			Language.setMessage(mstrModuleName, 501, "From Date :")
			Language.setMessage(mstrModuleName, 502, "To Date :")
			Language.setMessage(mstrModuleName, 503, "User :")
			Language.setMessage(mstrModuleName, 504, "Event Type :")
			Language.setMessage(mstrModuleName, 505, "Master Detail")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class