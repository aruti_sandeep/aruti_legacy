﻿Option Strict On

Imports eZeeCommonLib
Imports Aruti.Data

Public Class frmUserRoleList

#Region " Private Varaibles "
    Private objRoleMaster As clsUserRole_Master
    Private ReadOnly mstrModuleName As String = "frmUserRoleList"
#End Region

#Region " Private Function "

    Private Sub fillList()
        Dim dsRole As New DataSet
        Try
            dsRole = objRoleMaster.GetList("RoleList")

            Dim lvItem As ListViewItem

            lvUserRole.Items.Clear()
            For Each drRow As DataRow In dsRole.Tables(0).Rows
                lvItem = New ListViewItem
                lvItem.Text = drRow("code").ToString
                lvItem.Tag = drRow("roleunkid")
                lvItem.SubItems.Add(drRow("name").ToString)
                lvItem.SubItems.Add(drRow("description").ToString)
                lvUserRole.Items.Add(lvItem)
            Next

            If lvUserRole.Items.Count > 16 Then
                colhDescription.Width = 358 - 18
            Else
                colhDescription.Width = 358
            End If

            'Call setAlternateColor(lvAirline)

        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "fillList", mstrModuleName)
        Finally
            dsRole.Dispose()
        End Try
    End Sub

    'Private Sub AssignContextMenuItemText()
    '    Try
    '        objtsmiNew.Text = btnNew.Text
    '        objtsmiEdit.Text = btnEdit.Text
    '        objtsmiDelete.Text = btnDelete.Text
    '    Catch ex As Exception
    '        Call DisplayError.Show(CStr(-1), ex.Message, "AssignContextMenuItemText", mstrModuleName)
    '    End Try
    'End Sub
#End Region

#Region " Form's Events "

    Private Sub frmUserRoleList_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyUp
        Try
            If e.KeyCode = Keys.Delete And lvUserRole.Focused = True Then
                Call btnDelete.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserRoleList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmUserRoleList_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 27 Then
                Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserRoleList_KeyPress", mstrModuleName)
        End Try
    End Sub

    Private Sub frmUserRoleList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        objRoleMaster = New clsUserRole_Master
        Try
            Call Set_Logo(Me, gApplicationType)
            Call Language.setLanguage(Me.Name)

            'Anjan (02 Sep 2011)-Start
            'Issue : Including Language Settings.
            Call OtherSettings()
            'Anjan (02 Sep 2011)-End 
            'Me.ShowLanguageButton = User._Object.FD._AllowChangeLanguage

            'Call OtherSettings()


            'S.SANDEEP [ 30 May 2011 ] -- START
            'ISSUE : FINCA REQ.
            btnNew.Enabled = User._Object.Privilege._AllowAddUserRole
            btnEdit.Enabled = User._Object.Privilege._AllowEditUserRole
            'S.SANDEEP [ 30 May 2011 ] -- END 

            Call fillList()

            If lvUserRole.Items.Count > 0 Then lvUserRole.Items(0).Selected = True
            lvUserRole.Select()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmUserRoleList_Load", mstrModuleName)
        End Try
    End Sub

    Private Sub frmUserRoleList_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        objRoleMaster = Nothing
    End Sub

    'Anjan (02 Sep 2011)-Start
    'Issue : Including Language Settings.
    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            clsUserRole_Master.SetMessages()
            objfrm._Other_ModuleNames = "clsUserRole_Master"
            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub
    'Anjan (02 Sep 2011)-End 

#End Region

#Region " Buttons "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lvUserRole.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select User Role from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvUserRole.Select()
            Exit Sub
        End If
        'If objRoleMaster.isUsed(CInt(lvUserRole.SelectedItems(0).Tag)) Then
        '    eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Sorry, You cannot delete this User Role. Reason: This User Role is in use."), enMsgBoxStyle.Information) '?2
        '    lvUserRole.Select()
        '    Exit Sub
        'End If
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvUserRole.SelectedItems(0).Index

            If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 2, "Are you sure you want to delete this User Role?"), CType(enMsgBoxStyle.Question + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.Yes Then
                objRoleMaster.Delete(CInt(lvUserRole.SelectedItems(0).Tag))
                lvUserRole.SelectedItems(0).Remove()

                If lvUserRole.Items.Count <= 0 Then
                    Exit Try
                End If

                If lvUserRole.Items.Count = intSelectedIndex Then
                    intSelectedIndex = lvUserRole.Items.Count - 1
                    lvUserRole.Items(intSelectedIndex).Selected = True
                    lvUserRole.EnsureVisible(intSelectedIndex)
                ElseIf lvUserRole.Items.Count <> 0 Then
                    lvUserRole.Items(intSelectedIndex).Selected = True
                    lvUserRole.EnsureVisible(intSelectedIndex)
                End If
            End If
            lvUserRole.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnDelete_Click", mstrModuleName)
        End Try
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lvUserRole.SelectedItems.Count < 1 Then
            eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Please select User Role from the list to perform further operation on it."), enMsgBoxStyle.Information) '?1
            lvUserRole.Select()
            Exit Sub
        End If
        Dim frm As New frmUserRole_AddEdit
        Try
            Dim intSelectedIndex As Integer
            intSelectedIndex = lvUserRole.SelectedItems(0).Index

            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If

            If frm.displayDialog(CInt(lvUserRole.SelectedItems(0).Tag), enAction.EDIT_ONE) Then
                Call fillList()
            End If
            frm = Nothing

            lvUserRole.Items(intSelectedIndex).Selected = True
            lvUserRole.EnsureVisible(intSelectedIndex)
            lvUserRole.Select()
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnEdit_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim frm As New frmUserRole_AddEdit
        Try
            'If User._Object._RightToLeft = True Then
            '    frm.RightToLeft = Windows.Forms.RightToLeft.Yes
            '    frm.RightToLeftLayout = True
            '    Call Language.ctlRightToLeftlayOut(frm)
            'End If
            If frm.displayDialog(-1, enAction.ADD_CONTINUE) Then
                Call fillList()
            End If
        Catch ex As Exception
            Call DisplayError.Show("-1", ex.Message, "btnNew_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub
#End Region

    'S.SANDEEP [10 AUG 2015] -- START
    'ENHANCEMENT : Aruti SaaS Changes
#Region " Context Menu Event(s) "

    Private Sub mnuRolePrivilegeMapping_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRolePrivilegeMapping.Click
        Dim frm As New frmAbilityLevel_Privilege
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRolePrivilegeMapping_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

    Private Sub mnuRoleReportMapping_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRoleReportMapping.Click
        Dim frm As New frmReportAbilityLevel
        Try
            If User._Object._Isrighttoleft = True Then
                frm.RightToLeft = Windows.Forms.RightToLeft.Yes
                frm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(frm)
            End If

            frm.ShowDialog()

        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "mnuRoleReportMapping_Click", mstrModuleName)
        Finally
            If frm IsNot Nothing Then frm.Dispose()
        End Try
    End Sub

#End Region
    'S.SANDEEP [10 AUG 2015] -- END


    'Pinkal (27-oct-2010) ------------------- START

#Region "ListView Event"

    Private Sub lvUserRole_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvUserRole.SelectedIndexChanged
        Try
            If lvUserRole.SelectedItems.Count > 0 Then

                If CInt(lvUserRole.SelectedItems(0).Tag) = 1 Then
                    btnDelete.Enabled = False
                Else
                    'S.SANDEEP [ 30 May 2011 ] -- START
                    'ISSUE : FINCA REQ.
                    'btnDelete.Enabled = True
                    btnDelete.Enabled = User._Object.Privilege._AllowDeleteUserRole
                    'S.SANDEEP [ 30 May 2011 ] -- END 
                End If

            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "lvUserRole_SelectedIndexChanged", mstrModuleName)
        End Try
    End Sub

#End Region

    'Pinkal (27-oct-2010) ------------------- END


	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			
			Me.eZeeHeader.GradientColor1 = GUI._HeaderBackColor1 
			Me.eZeeHeader.GradientColor2 = GUI._HeaderBackColor2 
			Me.eZeeHeader.BorderColor = GUI._HeaderBorderColor 
			Me.eZeeHeader.HeaderTextForeColor = GUI._HeaderTitleFontColor 
			Me.eZeeHeader.DescriptionForeColor = GUI._HeaderMessageFontColor 


			Me.btnDelete.GradientBackColor = GUI._ButttonBackColor 
			Me.btnDelete.GradientForeColor = GUI._ButttonFontColor

			Me.btnEdit.GradientBackColor = GUI._ButttonBackColor 
			Me.btnEdit.GradientForeColor = GUI._ButttonFontColor

			Me.btnNew.GradientBackColor = GUI._ButttonBackColor 
			Me.btnNew.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.eZeeHeader.Title = Language._Object.getCaption(Me.eZeeHeader.Name & "_Title" , Me.eZeeHeader.Title)
			Me.eZeeHeader.Message = Language._Object.getCaption(Me.eZeeHeader.Name & "_Message" , Me.eZeeHeader.Message)
			Me.btnDelete.Text = Language._Object.getCaption(Me.btnDelete.Name, Me.btnDelete.Text)
			Me.btnEdit.Text = Language._Object.getCaption(Me.btnEdit.Name, Me.btnEdit.Text)
			Me.btnNew.Text = Language._Object.getCaption(Me.btnNew.Name, Me.btnNew.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.colhCode.Text = Language._Object.getCaption(CStr(Me.colhCode.Tag), Me.colhCode.Text)
			Me.colhUserRole.Text = Language._Object.getCaption(CStr(Me.colhUserRole.Tag), Me.colhUserRole.Text)
			Me.colhDescription.Text = Language._Object.getCaption(CStr(Me.colhDescription.Tag), Me.colhDescription.Text)


		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Please select User Role from the list to perform further operation on it.")
			Language.setMessage(mstrModuleName, 2, "Are you sure you want to delete this User Role?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class