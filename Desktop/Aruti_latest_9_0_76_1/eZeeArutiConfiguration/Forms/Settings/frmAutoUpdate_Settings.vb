﻿Option Strict On

#Region " Imports "

Imports eZeeCommonLib
Imports Aruti.Data

#End Region

Public Class frmAutoUpdate_Settings
    Private ReadOnly mstrModuleName As String = "frmAutoUpdate_Settings"
    Private mblnCancel As Boolean = True
    Private objConfig As clsConfigOptions
    Dim blnValue As Boolean = False


    'Pinkal (24-Apr-2013) -- Start
    'Enhancement : TRA Changes
    Private mstrATAutoUpdateSetting As String = ""
    'Pinkal (24-Apr-2013) -- End


#Region " Private Methods "

    Private Sub GetValue()
        Try
            blnValue = objConfig.Get_Auto_Update_Setting(-200)
            If blnValue = True Then
                radAutoUpdate.Checked = True
            Else
                radManually.Checked = True
            End If


            'Pinkal (24-Apr-2013) -- Start
            'Enhancement : TRA Changes

            mstrATAutoUpdateSetting = ""
            If radAutoUpdate.Checked Then
                mstrATAutoUpdateSetting = radAutoUpdate.Text
            ElseIf radManually.Checked Then
                mstrATAutoUpdateSetting = radManually.Text
            End If

            'Pinkal (24-Apr-2013) -- End


        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "GetValue", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

#Region " Form's Events "

    Private Sub frmAutoUpdate_Settings_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            objConfig = Nothing
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAutoUpdate_Settings_FormClosed", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAutoUpdate_Settings_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Try
            If e.Control = True And e.KeyCode = Windows.Forms.Keys.S Then
                Call btnSave.PerformClick()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAutoUpdate_Settings_KeyDown", mstrModuleName)
        End Try
    End Sub

    Private Sub frmAutoUpdate_Settings_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Me.KeyPress
        Try
            If Asc(e.KeyChar) = 13 Then
                Windows.Forms.SendKeys.Send("{Tab}")
                e.Handled = True
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAutoUpdate_Settings_KeyPress", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub frmAutoUpdate_Settings_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        objConfig = New clsConfigOptions
        Try
            Call Set_Logo(Me, gApplicationType)

            Language.setLanguage(Me.Name)
            Call OtherSettings()

            Call GetValue()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "frmAutoUpdate_Settings_Load", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub Form_LanguageClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.LanguageClick
        Dim objfrm As New frmLanguage
        Try
            If User._Object._Isrighttoleft = True Then
                objfrm.RightToLeft = Windows.Forms.RightToLeft.Yes
                objfrm.RightToLeftLayout = True
                Call Language.ctlRightToLeftlayOut(objfrm)
            End If

            Call SetMessages()

            objfrm.displayDialog(Me)

            Call SetLanguage()

        Catch ex As System.Exception
            Call DisplayError.Show("-1", ex.Message, "Form_LanguageClick", mstrModuleName)
        Finally
            objfrm.Dispose()
            objfrm = Nothing
        End Try
    End Sub

#End Region

#Region " Button's Events "

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Me.Close()
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnClose_Click", mstrModuleName)
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If blnValue = True Then
                If radManually.Checked = True Then
                    If eZeeMsgBox.Show(Language.getMessage(mstrModuleName, 1, "Are you sure, you want to disable the auto update feature?"), CType(enMsgBoxStyle.Information + enMsgBoxStyle.YesNo, enMsgBoxStyle)) = Windows.Forms.DialogResult.No Then
                        radAutoUpdate.Checked = True
                        Me.Close()
                    End If
                End If
            End If


            If objConfig.Insert_Update_Setting(-200, CBool(IIf(radManually.Checked, False, True))) Then

                'Pinkal (24-Apr-2013) -- Start
                'Enhancement : TRA Changes

                Dim mstrAutoUpdateSetting As String = ""
                If radAutoUpdate.Checked Then
                    mstrAutoUpdateSetting = radAutoUpdate.Text
                ElseIf radManually.Checked Then
                    mstrAutoUpdateSetting = radManually.Text
                End If

                If mstrATAutoUpdateSetting.Trim <> mstrAutoUpdateSetting.Trim Then
                    Dim objatConfigOption As New clsAtconfigoption
                    objConfig.GetData(-200, "Auto_Update_Enabled")
                    objatConfigOption._Configunkid = objConfig._ConfigOptionid
                    objatConfigOption._Optiongroupid = enConfigOptionGroup.GENERAL
                    objatConfigOption._Description = Me.Text & " --> Old Value :- " & mstrATAutoUpdateSetting.Trim & " | New Value :- " & mstrAutoUpdateSetting.Trim
                    objatConfigOption.Insert()
                End If

                'Pinkal (24-Apr-2013) -- End


            Me.Close()
            End If
        Catch ex As Exception
            DisplayError.Show("-1", ex.Message, "btnSave_Click", mstrModuleName)
        Finally
        End Try
    End Sub

#End Region

	'<Language> This Auto Generated Text Please Do Not Modify it.
#Region " Language & UI Settings "
	Private Sub OtherSettings()
		Try
			Me.SuspendLayout()
			
			Call SetLanguage()
			
			Me.gbSettings.GradientColor = GUI._eZeeContainerHeaderBackColor 
			Me.gbSettings.ForeColor = GUI._eZeeContainerHeaderForeColor 


			Me.btnSave.GradientBackColor = GUI._ButttonBackColor 
			Me.btnSave.GradientForeColor = GUI._ButttonFontColor

			Me.btnClose.GradientBackColor = GUI._ButttonBackColor 
			Me.btnClose.GradientForeColor = GUI._ButttonFontColor


			Me.ResumeLayout()
		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "OtherSettings", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetLanguage()
		Try
			Me.Text = Language._Object.getCaption(Me.Name, Me.Text)
			
			Me.btnSave.Text = Language._Object.getCaption(Me.btnSave.Name, Me.btnSave.Text)
			Me.btnClose.Text = Language._Object.getCaption(Me.btnClose.Name, Me.btnClose.Text)
			Me.gbSettings.Text = Language._Object.getCaption(Me.gbSettings.Name, Me.gbSettings.Text)
			Me.radAutoUpdate.Text = Language._Object.getCaption(Me.radAutoUpdate.Name, Me.radAutoUpdate.Text)
			Me.radManually.Text = Language._Object.getCaption(Me.radManually.Name, Me.radManually.Text)

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetLanguage", mstrModuleName)
		End Try
	End Sub
			
			
	Private Sub SetMessages()
		Try
			Language.setMessage(mstrModuleName, 1, "Are you sure, you want to disable the auto update feature?")

		Catch Ex As Exception
			DisplayError.Show("-1", ex.Message, "SetMessages", mstrModuleName)
		End Try
	End Sub
#End Region 'Language & UI Settings
	'</Language>
End Class