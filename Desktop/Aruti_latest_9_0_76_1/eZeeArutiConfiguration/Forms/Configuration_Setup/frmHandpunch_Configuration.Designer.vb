﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHandpunch_Configuration
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHandpunch_Configuration))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.EZeeFooter1 = New eZee.Common.eZeeFooter
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.gbHandpunchConfig = New eZee.Common.eZeeCollapsibleContainer(Me.components)
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.dtpDownloadUserTime = New System.Windows.Forms.DateTimePicker
        Me.LblDownloadUserTime = New System.Windows.Forms.Label
        Me.LblDownloadUserDOW = New System.Windows.Forms.Label
        Me.chkDays = New System.Windows.Forms.CheckedListBox
        Me.nudLogsInterval = New System.Windows.Forms.NumericUpDown
        Me.Label1 = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.EZeeFooter1.SuspendLayout()
        Me.gbHandpunchConfig.SuspendLayout()
        CType(Me.nudLogsInterval, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.EZeeFooter1)
        Me.pnlMain.Controls.Add(Me.gbHandpunchConfig)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(323, 289)
        Me.pnlMain.TabIndex = 0
        '
        'EZeeFooter1
        '
        Me.EZeeFooter1.BorderColor = System.Drawing.Color.Silver
        Me.EZeeFooter1.Controls.Add(Me.btnCancel)
        Me.EZeeFooter1.Controls.Add(Me.btnSave)
        Me.EZeeFooter1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.EZeeFooter1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EZeeFooter1.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.EZeeFooter1.GradientColor1 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.GradientColor2 = System.Drawing.SystemColors.Control
        Me.EZeeFooter1.Location = New System.Drawing.Point(0, 234)
        Me.EZeeFooter1.Name = "EZeeFooter1"
        Me.EZeeFooter1.Size = New System.Drawing.Size(323, 55)
        Me.EZeeFooter1.TabIndex = 85
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(223, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(127, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'gbHandpunchConfig
        '
        Me.gbHandpunchConfig.BorderColor = System.Drawing.Color.Black
        Me.gbHandpunchConfig.Checked = False
        Me.gbHandpunchConfig.CollapseAllExceptThis = False
        Me.gbHandpunchConfig.CollapsedHoverImage = Nothing
        Me.gbHandpunchConfig.CollapsedNormalImage = Nothing
        Me.gbHandpunchConfig.CollapsedPressedImage = Nothing
        Me.gbHandpunchConfig.CollapseOnLoad = False
        Me.gbHandpunchConfig.Controls.Add(Me.chkSelectAll)
        Me.gbHandpunchConfig.Controls.Add(Me.dtpDownloadUserTime)
        Me.gbHandpunchConfig.Controls.Add(Me.LblDownloadUserTime)
        Me.gbHandpunchConfig.Controls.Add(Me.LblDownloadUserDOW)
        Me.gbHandpunchConfig.Controls.Add(Me.chkDays)
        Me.gbHandpunchConfig.Controls.Add(Me.nudLogsInterval)
        Me.gbHandpunchConfig.Controls.Add(Me.Label1)
        Me.gbHandpunchConfig.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbHandpunchConfig.ExpandedHoverImage = Nothing
        Me.gbHandpunchConfig.ExpandedNormalImage = Nothing
        Me.gbHandpunchConfig.ExpandedPressedImage = Nothing
        Me.gbHandpunchConfig.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbHandpunchConfig.GradientColor = System.Drawing.SystemColors.ButtonFace
        Me.gbHandpunchConfig.HeaderHeight = 25
        Me.gbHandpunchConfig.HeaderMessage = ""
        Me.gbHandpunchConfig.HeaderMessageFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gbHandpunchConfig.HeaderMessageForeColor = System.Drawing.Color.Black
        Me.gbHandpunchConfig.HeightOnCollapse = 0
        Me.gbHandpunchConfig.LeftTextSpace = 0
        Me.gbHandpunchConfig.Location = New System.Drawing.Point(0, 0)
        Me.gbHandpunchConfig.Name = "gbHandpunchConfig"
        Me.gbHandpunchConfig.OpenHeight = 300
        Me.gbHandpunchConfig.PanelBorderStyle = System.Windows.Forms.ButtonBorderStyle.Solid
        Me.gbHandpunchConfig.ShowBorder = True
        Me.gbHandpunchConfig.ShowCheckBox = False
        Me.gbHandpunchConfig.ShowCollapseButton = False
        Me.gbHandpunchConfig.ShowDefaultBorderColor = True
        Me.gbHandpunchConfig.ShowDownButton = False
        Me.gbHandpunchConfig.ShowHeader = True
        Me.gbHandpunchConfig.Size = New System.Drawing.Size(323, 289)
        Me.gbHandpunchConfig.TabIndex = 30
        Me.gbHandpunchConfig.Temp = 0
        Me.gbHandpunchConfig.Text = "Configure Handpunch"
        Me.gbHandpunchConfig.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkSelectAll
        '
        Me.chkSelectAll.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkSelectAll.Location = New System.Drawing.Point(157, 62)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(154, 17)
        Me.chkSelectAll.TabIndex = 87
        Me.chkSelectAll.Text = "Select All"
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'dtpDownloadUserTime
        '
        Me.dtpDownloadUserTime.CustomFormat = "HH:mm"
        Me.dtpDownloadUserTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDownloadUserTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDownloadUserTime.Location = New System.Drawing.Point(154, 206)
        Me.dtpDownloadUserTime.Name = "dtpDownloadUserTime"
        Me.dtpDownloadUserTime.ShowUpDown = True
        Me.dtpDownloadUserTime.Size = New System.Drawing.Size(157, 21)
        Me.dtpDownloadUserTime.TabIndex = 85
        '
        'LblDownloadUserTime
        '
        Me.LblDownloadUserTime.BackColor = System.Drawing.Color.Transparent
        Me.LblDownloadUserTime.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDownloadUserTime.ForeColor = System.Drawing.Color.Black
        Me.LblDownloadUserTime.Location = New System.Drawing.Point(8, 209)
        Me.LblDownloadUserTime.Name = "LblDownloadUserTime"
        Me.LblDownloadUserTime.Size = New System.Drawing.Size(135, 17)
        Me.LblDownloadUserTime.TabIndex = 84
        Me.LblDownloadUserTime.Text = "Download User Time"
        '
        'LblDownloadUserDOW
        '
        Me.LblDownloadUserDOW.BackColor = System.Drawing.Color.Transparent
        Me.LblDownloadUserDOW.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDownloadUserDOW.ForeColor = System.Drawing.Color.Black
        Me.LblDownloadUserDOW.Location = New System.Drawing.Point(8, 83)
        Me.LblDownloadUserDOW.Name = "LblDownloadUserDOW"
        Me.LblDownloadUserDOW.Size = New System.Drawing.Size(135, 30)
        Me.LblDownloadUserDOW.TabIndex = 82
        Me.LblDownloadUserDOW.Text = "Download Users Day of Week"
        '
        'chkDays
        '
        Me.chkDays.CheckOnClick = True
        Me.chkDays.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDays.FormattingEnabled = True
        Me.chkDays.Location = New System.Drawing.Point(154, 83)
        Me.chkDays.Name = "chkDays"
        Me.chkDays.Size = New System.Drawing.Size(157, 116)
        Me.chkDays.TabIndex = 81
        '
        'nudLogsInterval
        '
        Me.nudLogsInterval.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudLogsInterval.Location = New System.Drawing.Point(154, 32)
        Me.nudLogsInterval.Maximum = New Decimal(New Integer() {1440, 0, 0, 0})
        Me.nudLogsInterval.Minimum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudLogsInterval.Name = "nudLogsInterval"
        Me.nudLogsInterval.Size = New System.Drawing.Size(63, 21)
        Me.nudLogsInterval.TabIndex = 79
        Me.nudLogsInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.nudLogsInterval.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(8, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(135, 15)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "Download Logs Interval"
        '
        'frmHandpunch_Configuration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(323, 289)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmHandpunch_Configuration"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Configure Handpunch"
        Me.pnlMain.ResumeLayout(False)
        Me.EZeeFooter1.ResumeLayout(False)
        Me.gbHandpunchConfig.ResumeLayout(False)
        CType(Me.nudLogsInterval, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents gbHandpunchConfig As eZee.Common.eZeeCollapsibleContainer
    Private WithEvents LblDownloadUserDOW As System.Windows.Forms.Label
    Friend WithEvents chkDays As System.Windows.Forms.CheckedListBox
    Friend WithEvents nudLogsInterval As System.Windows.Forms.NumericUpDown
    Private WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents EZeeFooter1 As eZee.Common.eZeeFooter
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Private WithEvents LblDownloadUserTime As System.Windows.Forms.Label
    Friend WithEvents dtpDownloadUserTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
End Class
