﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDevice_Configuration
    Inherits eZee.Common.eZeeForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDevice_Configuration))
        Me.pnlMain = New System.Windows.Forms.Panel
        Me.cboDeviceType = New System.Windows.Forms.ComboBox
        Me.LblDeviceType = New System.Windows.Forms.Label
        Me.cboConnectionType = New System.Windows.Forms.ComboBox
        Me.LblConnectionType = New System.Windows.Forms.Label
        Me.objFooter = New eZee.Common.eZeeFooter
        Me.btnTestConnection = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnCancel = New eZee.Common.eZeeLightButton(Me.components)
        Me.btnSave = New eZee.Common.eZeeLightButton(Me.components)
        Me.txtDbServer = New System.Windows.Forms.TextBox
        Me.lblDBUserPwd = New System.Windows.Forms.Label
        Me.lblDBUserName = New System.Windows.Forms.Label
        Me.lblDBName = New System.Windows.Forms.Label
        Me.lblDbServer = New System.Windows.Forms.Label
        Me.txtDBUserPwd = New System.Windows.Forms.TextBox
        Me.txtDBUserName = New System.Windows.Forms.TextBox
        Me.txtDBName = New System.Windows.Forms.TextBox
        Me.txtPortNo = New System.Windows.Forms.TextBox
        Me.LblPortNo = New System.Windows.Forms.Label
        Me.pnlMain.SuspendLayout()
        Me.objFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMain
        '
        Me.pnlMain.Controls.Add(Me.cboDeviceType)
        Me.pnlMain.Controls.Add(Me.LblDeviceType)
        Me.pnlMain.Controls.Add(Me.cboConnectionType)
        Me.pnlMain.Controls.Add(Me.LblConnectionType)
        Me.pnlMain.Controls.Add(Me.objFooter)
        Me.pnlMain.Controls.Add(Me.txtDbServer)
        Me.pnlMain.Controls.Add(Me.lblDBUserPwd)
        Me.pnlMain.Controls.Add(Me.lblDBUserName)
        Me.pnlMain.Controls.Add(Me.lblDBName)
        Me.pnlMain.Controls.Add(Me.lblDbServer)
        Me.pnlMain.Controls.Add(Me.txtDBUserPwd)
        Me.pnlMain.Controls.Add(Me.txtDBUserName)
        Me.pnlMain.Controls.Add(Me.txtDBName)
        Me.pnlMain.Controls.Add(Me.txtPortNo)
        Me.pnlMain.Controls.Add(Me.LblPortNo)
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 0)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(355, 258)
        Me.pnlMain.TabIndex = 0
        '
        'cboDeviceType
        '
        Me.cboDeviceType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDeviceType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboDeviceType.FormattingEnabled = True
        Me.cboDeviceType.Items.AddRange(New Object() {"COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9"})
        Me.cboDeviceType.Location = New System.Drawing.Point(148, 12)
        Me.cboDeviceType.Name = "cboDeviceType"
        Me.cboDeviceType.Size = New System.Drawing.Size(199, 21)
        Me.cboDeviceType.TabIndex = 1
        '
        'LblDeviceType
        '
        Me.LblDeviceType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDeviceType.Location = New System.Drawing.Point(6, 14)
        Me.LblDeviceType.Name = "LblDeviceType"
        Me.LblDeviceType.Size = New System.Drawing.Size(136, 16)
        Me.LblDeviceType.TabIndex = 0
        Me.LblDeviceType.Text = "Device Type"
        Me.LblDeviceType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboConnectionType
        '
        Me.cboConnectionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboConnectionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboConnectionType.FormattingEnabled = True
        Me.cboConnectionType.Items.AddRange(New Object() {"COM1", "COM2", "COM3", "COM4", "COM5", "COM6", "COM7", "COM8", "COM9"})
        Me.cboConnectionType.Location = New System.Drawing.Point(148, 39)
        Me.cboConnectionType.Name = "cboConnectionType"
        Me.cboConnectionType.Size = New System.Drawing.Size(199, 21)
        Me.cboConnectionType.TabIndex = 3
        '
        'LblConnectionType
        '
        Me.LblConnectionType.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblConnectionType.Location = New System.Drawing.Point(6, 41)
        Me.LblConnectionType.Name = "LblConnectionType"
        Me.LblConnectionType.Size = New System.Drawing.Size(136, 16)
        Me.LblConnectionType.TabIndex = 2
        Me.LblConnectionType.Text = "Connection Type"
        Me.LblConnectionType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'objFooter
        '
        Me.objFooter.BorderColor = System.Drawing.Color.Silver
        Me.objFooter.Controls.Add(Me.btnTestConnection)
        Me.objFooter.Controls.Add(Me.btnCancel)
        Me.objFooter.Controls.Add(Me.btnSave)
        Me.objFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.objFooter.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.objFooter.GradiantStyle = eZee.Common.eZeeFooter.GradientStyle.Up
        Me.objFooter.GradientColor1 = System.Drawing.SystemColors.Control
        Me.objFooter.GradientColor2 = System.Drawing.SystemColors.Control
        Me.objFooter.Location = New System.Drawing.Point(0, 203)
        Me.objFooter.Name = "objFooter"
        Me.objFooter.Size = New System.Drawing.Size(355, 55)
        Me.objFooter.TabIndex = 14
        '
        'btnTestConnection
        '
        Me.btnTestConnection.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTestConnection.BackColor = System.Drawing.Color.White
        Me.btnTestConnection.BackgroundImage = CType(resources.GetObject("btnTestConnection.BackgroundImage"), System.Drawing.Image)
        Me.btnTestConnection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnTestConnection.BorderColor = System.Drawing.Color.Empty
        Me.btnTestConnection.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnTestConnection.FlatAppearance.BorderSize = 0
        Me.btnTestConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTestConnection.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTestConnection.ForeColor = System.Drawing.Color.Black
        Me.btnTestConnection.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnTestConnection.GradientForeColor = System.Drawing.Color.Black
        Me.btnTestConnection.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTestConnection.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnTestConnection.Location = New System.Drawing.Point(9, 13)
        Me.btnTestConnection.Name = "btnTestConnection"
        Me.btnTestConnection.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnTestConnection.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnTestConnection.Size = New System.Drawing.Size(113, 30)
        Me.btnTestConnection.TabIndex = 0
        Me.btnTestConnection.Text = "&Test Connection"
        Me.btnTestConnection.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.BackColor = System.Drawing.Color.White
        Me.btnCancel.BackgroundImage = CType(resources.GetObject("btnCancel.BackgroundImage"), System.Drawing.Image)
        Me.btnCancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnCancel.BorderColor = System.Drawing.Color.Empty
        Me.btnCancel.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnCancel.FlatAppearance.BorderSize = 0
        Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.ForeColor = System.Drawing.Color.Black
        Me.btnCancel.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnCancel.GradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Location = New System.Drawing.Point(258, 13)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnCancel.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnCancel.Size = New System.Drawing.Size(90, 30)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.Color.White
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnSave.BorderColor = System.Drawing.Color.Empty
        Me.btnSave.ButtonType = eZee.Common.eZeeLightButton.enButtonType.NormalButton
        Me.btnSave.FlatAppearance.BorderSize = 0
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSave.ForeColor = System.Drawing.Color.Black
        Me.btnSave.GradientBackColor = System.Drawing.SystemColors.ActiveBorder
        Me.btnSave.GradientForeColor = System.Drawing.Color.Black
        Me.btnSave.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.HoverGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Location = New System.Drawing.Point(162, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.btnSave.PressedGradientForeColor = System.Drawing.Color.Black
        Me.btnSave.Size = New System.Drawing.Size(90, 30)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "&Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'txtDbServer
        '
        Me.txtDbServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDbServer.Location = New System.Drawing.Point(148, 91)
        Me.txtDbServer.Name = "txtDbServer"
        Me.txtDbServer.Size = New System.Drawing.Size(199, 21)
        Me.txtDbServer.TabIndex = 7
        '
        'lblDBUserPwd
        '
        Me.lblDBUserPwd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBUserPwd.Location = New System.Drawing.Point(6, 148)
        Me.lblDBUserPwd.Name = "lblDBUserPwd"
        Me.lblDBUserPwd.Size = New System.Drawing.Size(136, 16)
        Me.lblDBUserPwd.TabIndex = 10
        Me.lblDBUserPwd.Text = "Database User Password"
        Me.lblDBUserPwd.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDBUserName
        '
        Me.lblDBUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBUserName.Location = New System.Drawing.Point(6, 121)
        Me.lblDBUserName.Name = "lblDBUserName"
        Me.lblDBUserName.Size = New System.Drawing.Size(136, 16)
        Me.lblDBUserName.TabIndex = 8
        Me.lblDBUserName.Text = "Database User Name"
        Me.lblDBUserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDBName
        '
        Me.lblDBName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDBName.Location = New System.Drawing.Point(6, 66)
        Me.lblDBName.Name = "lblDBName"
        Me.lblDBName.Size = New System.Drawing.Size(136, 16)
        Me.lblDBName.TabIndex = 4
        Me.lblDBName.Text = "Database Name"
        Me.lblDBName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblDbServer
        '
        Me.lblDbServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDbServer.Location = New System.Drawing.Point(6, 93)
        Me.lblDbServer.Name = "lblDbServer"
        Me.lblDbServer.Size = New System.Drawing.Size(136, 16)
        Me.lblDbServer.TabIndex = 6
        Me.lblDbServer.Text = "Database Server"
        Me.lblDbServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDBUserPwd
        '
        Me.txtDBUserPwd.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBUserPwd.Location = New System.Drawing.Point(148, 146)
        Me.txtDBUserPwd.Name = "txtDBUserPwd"
        Me.txtDBUserPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtDBUserPwd.Size = New System.Drawing.Size(199, 21)
        Me.txtDBUserPwd.TabIndex = 11
        Me.txtDBUserPwd.UseSystemPasswordChar = True
        '
        'txtDBUserName
        '
        Me.txtDBUserName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBUserName.Location = New System.Drawing.Point(148, 119)
        Me.txtDBUserName.Name = "txtDBUserName"
        Me.txtDBUserName.Size = New System.Drawing.Size(199, 21)
        Me.txtDBUserName.TabIndex = 9
        '
        'txtDBName
        '
        Me.txtDBName.BackColor = System.Drawing.Color.White
        Me.txtDBName.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDBName.Location = New System.Drawing.Point(148, 64)
        Me.txtDBName.Name = "txtDBName"
        Me.txtDBName.ReadOnly = True
        Me.txtDBName.Size = New System.Drawing.Size(199, 21)
        Me.txtDBName.TabIndex = 5
        '
        'txtPortNo
        '
        Me.txtPortNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPortNo.Location = New System.Drawing.Point(148, 174)
        Me.txtPortNo.Name = "txtPortNo"
        Me.txtPortNo.Size = New System.Drawing.Size(199, 21)
        Me.txtPortNo.TabIndex = 13
        '
        'LblPortNo
        '
        Me.LblPortNo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPortNo.Location = New System.Drawing.Point(6, 176)
        Me.LblPortNo.Name = "LblPortNo"
        Me.LblPortNo.Size = New System.Drawing.Size(136, 16)
        Me.LblPortNo.TabIndex = 12
        Me.LblPortNo.Text = "Database Port No"
        Me.LblPortNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmDevice_Configuration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(355, 258)
        Me.Controls.Add(Me.pnlMain)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDevice_Configuration"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Device Configuration"
        Me.pnlMain.ResumeLayout(False)
        Me.pnlMain.PerformLayout()
        Me.objFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMain As System.Windows.Forms.Panel
    Friend WithEvents txtDbServer As System.Windows.Forms.TextBox
    Friend WithEvents lblDBUserPwd As System.Windows.Forms.Label
    Friend WithEvents lblDBUserName As System.Windows.Forms.Label
    Friend WithEvents lblDBName As System.Windows.Forms.Label
    Friend WithEvents lblDbServer As System.Windows.Forms.Label
    Friend WithEvents txtDBUserPwd As System.Windows.Forms.TextBox
    Friend WithEvents txtDBUserName As System.Windows.Forms.TextBox
    Friend WithEvents txtDBName As System.Windows.Forms.TextBox
    Friend WithEvents txtPortNo As System.Windows.Forms.TextBox
    Friend WithEvents LblPortNo As System.Windows.Forms.Label
    Friend WithEvents objFooter As eZee.Common.eZeeFooter
    Friend WithEvents btnTestConnection As eZee.Common.eZeeLightButton
    Friend WithEvents btnCancel As eZee.Common.eZeeLightButton
    Friend WithEvents btnSave As eZee.Common.eZeeLightButton
    Friend WithEvents LblConnectionType As System.Windows.Forms.Label
    Private WithEvents cboDeviceType As System.Windows.Forms.ComboBox
    Friend WithEvents LblDeviceType As System.Windows.Forms.Label
    Private WithEvents cboConnectionType As System.Windows.Forms.ComboBox
End Class
