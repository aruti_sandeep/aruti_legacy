Imports System
Imports System.Collections.Generic
Imports System.Text


Public Class ExpandableGridViewNodeCollection
    Implements System.Collections.Generic.IList(Of ExpandableGridViewNode), System.Collections.IList

    Friend _list As System.Collections.Generic.List(Of ExpandableGridViewNode)
    Friend _owner As ExpandableGridViewNode

    Friend Sub New(ByVal owner As ExpandableGridViewNode)
        Me._owner = owner
        Me._list = New List(Of ExpandableGridViewNode)()
    End Sub

#Region "Public Members"

    Public Sub Add(ByVal item As ExpandableGridViewNode) _
        Implements System.Collections.Generic.IList(Of ExpandableGridViewNode).Add

        Try

            ' The row needs to exist in the child collection before the parent is notified. 
            item._grid = Me._owner._grid

            Dim hadChildren As Boolean = Me._owner.HasChildren
            item._owner = Me

            Me._list.Add(item)

            Me._owner.AddChildNode(item)

            ' if the owner didn't have children but now does (asserted) and it is sited update it 
            If Not hadChildren AndAlso Me._owner.IsSited Then
                Me._owner._grid.InvalidateRow(Me._owner.RowIndex)
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Function Add(ByVal text As String) As ExpandableGridViewNode
        Dim node As New ExpandableGridViewNode()
        Try
            Me.Add(node)
            node.Cells(0).Value = text
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Return node
    End Function

    Public Function Add(ByVal ParamArray values As Object()) As ExpandableGridViewNode
        Dim node As New ExpandableGridViewNode()
        Try

            Me.Add(node)

            Dim cell As Integer = 0

            If values.Length > node.Cells.Count Then
                Throw New ArgumentOutOfRangeException("values")
            End If

            For Each o As Object In values
                node.Cells(cell).Value = o
                cell += 1
            Next
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        Return node
    End Function

    Public Sub Insert(ByVal index As Integer, ByVal item As ExpandableGridViewNode) _
        Implements System.Collections.Generic.IList(Of ExpandableGridViewNode).Insert

        ' The row needs to exist in the child collection before the parent is notified. 
        item._grid = Me._owner._grid
        item._owner = Me

        Me._list.Insert(index, item)

        Me._owner.InsertChildNode(index, item)
    End Sub

    Public Function Remove(ByVal item As ExpandableGridViewNode) As Boolean _
        Implements System.Collections.Generic.IList(Of ExpandableGridViewNode).Remove

        ' The parent is notified first then the row is removed from the child collection. 
        Me._owner.RemoveChildNode(item)
        item._grid = Nothing
        Return Me._list.Remove(item)
    End Function

    Public Sub RemoveAt(ByVal index As Integer) _
        Implements System.Collections.Generic.IList(Of ExpandableGridViewNode).RemoveAt

        Dim row As ExpandableGridViewNode = Me._list(index)

        ' The parent is notified first then the row is removed from the child collection. 
        Me._owner.RemoveChildNode(row)
        row._grid = Nothing
        Me._list.RemoveAt(index)
    End Sub

    Public Sub Clear() _
        Implements System.Collections.Generic.IList(Of ExpandableGridViewNode).Clear

        ' The parent is notified first then the row is removed from the child collection. 
        Me._owner.ClearNodes()
        Me._list.Clear()
    End Sub

    Public Function IndexOf(ByVal item As ExpandableGridViewNode) As Integer _
        Implements System.Collections.Generic.IList(Of ExpandableGridViewNode).IndexOf

        Return Me._list.IndexOf(item)
    End Function

    Default Public Property Item(ByVal index As Integer) As ExpandableGridViewNode _
        Implements System.Collections.Generic.IList(Of ExpandableGridViewNode).Item

        Get
            Return Me._list(index)
        End Get
        Set(ByVal value As ExpandableGridViewNode)
            Throw New Exception("The method or operation is not implemented.")
        End Set
    End Property

    Public Function Contains(ByVal item As ExpandableGridViewNode) As Boolean _
        Implements System.Collections.Generic.IList(Of ExpandableGridViewNode).Contains

        Return Me._list.Contains(item)
    End Function

    Public Sub CopyTo(ByVal array As ExpandableGridViewNode(), ByVal arrayIndex As Integer) _
        Implements System.Collections.Generic.IList(Of ExpandableGridViewNode).CopyTo

        Throw New Exception("The method or operation is not implemented.")
    End Sub

    Public ReadOnly Property Count() As Integer _
        Implements System.Collections.Generic.IList(Of ExpandableGridViewNode).Count

        Get
            Return Me._list.Count
        End Get
    End Property

    Public ReadOnly Property IsReadOnly() As Boolean _
        Implements System.Collections.Generic.IList(Of ExpandableGridViewNode).IsReadOnly
        Get
            Return False
        End Get
    End Property
#End Region

#Region "IList Interface"
    Private Sub Remove(ByVal value As Object) Implements System.Collections.IList.Remove
        Me.Remove(TryCast(value, ExpandableGridViewNode))
    End Sub


    Private Function Add(ByVal value As Object) As Integer _
        Implements System.Collections.IList.Add

        Dim item As ExpandableGridViewNode = TryCast(value, ExpandableGridViewNode)
        Me.Add(item)
        Return item.Index
    End Function

    Private Sub IListRemoveAt(ByVal index As Integer) Implements System.Collections.IList.RemoveAt
        Me.RemoveAt(index)
    End Sub


    Private Sub IListClear() Implements System.Collections.IList.Clear
        Me.Clear()
    End Sub

    Private ReadOnly Property IListIsReadOnly() As Boolean Implements System.Collections.IList.IsReadOnly
        Get
            Return Me.IsReadOnly
        End Get
    End Property

    Private ReadOnly Property IListIsFixedSize() As Boolean Implements System.Collections.IList.IsFixedSize
        Get
            Return False
        End Get
    End Property

    Private Function IListIndexOf(ByVal item As Object) As Integer Implements System.Collections.IList.IndexOf
        Return Me.IndexOf(TryCast(item, ExpandableGridViewNode))
    End Function

    Private Sub IListInsert(ByVal index As Integer, ByVal value As Object) Implements System.Collections.IList.Insert
        Me.Insert(index, TryCast(value, ExpandableGridViewNode))
    End Sub

    Private ReadOnly Property IListCount() As Integer Implements System.Collections.ICollection.Count
        Get
            Return Me.Count
        End Get
    End Property

    Private Function IListContains(ByVal value As Object) As Boolean Implements System.Collections.IList.Contains
        Return Me.Contains(TryCast(value, ExpandableGridViewNode))
    End Function

    Private Sub IListCopyTo(ByVal array As Array, ByVal index As Integer) Implements System.Collections.ICollection.CopyTo
        Throw New Exception("The method or operation is not implemented.")
    End Sub

    Property IListItem(ByVal index As Integer) As Object Implements System.Collections.IList.Item
        Get
            Return Me(index)
        End Get
        Set(ByVal value As Object)
            Throw New Exception("The method or operation is not implemented.")
        End Set
    End Property



#Region "IEnumerable Members"

    Public Function GetEnumerator() As IEnumerator(Of ExpandableGridViewNode) _
        Implements System.Collections.Generic.IEnumerable(Of ExpandableGridViewNode).GetEnumerator

        Return Me._list.GetEnumerator()
    End Function

#End Region


#Region "IEnumerable Members"

    Private Function IListGetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        Return Me.GetEnumerator()
    End Function

#End Region
#End Region

#Region "ICollection Members"

    Private ReadOnly Property IsSynchronized() As Boolean Implements System.Collections.ICollection.IsSynchronized
        Get
            Throw New Exception("The method or operation is not implemented.")
        End Get
    End Property

    Private ReadOnly Property SyncRoot() As Object Implements System.Collections.ICollection.SyncRoot
        Get
            Throw New Exception("The method or operation is not implemented.")
        End Get
    End Property

#End Region

End Class

