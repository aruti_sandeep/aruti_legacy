Option Strict On

Imports System
Imports System.ComponentModel
Imports System.Runtime.InteropServices
Imports System.Drawing
Imports System.Drawing.Design
Imports System.Windows.Forms.Design
Imports System.Windows.Forms

', System.Diagnostics.DebuggerStepThrough()
<System.Serializable()> _
Public Class eZeeListView
    Inherits eZeeBaseListView

#Region " Private Variables "

#Region " Proeperty Variables "

    Private cpHeader As New ContextMenuStrip
    Private chkColumnList As ColumnShowHideEditor
    Private mcolGroupingColumn As ColumnHeader
    Private mintLastGroupColumnIndex As Integer = -1
    Private mstrNecessaryColumns As String = ""
    Private mstrVisibleColumnList As String = ""
    Private mstrCheckedItems As New List(Of String)
    Private mintMinColumnWidth As Integer = 50
    Private mblnShowSelectAll As Boolean = True

#End Region

#Region " Control Variables "

    Friend WithEvents objchkSelectAll As System.Windows.Forms.CheckBox

    Private _headerRect As Rectangle

    <StructLayout(LayoutKind.Sequential)> _
    Private Structure RECT
        Public Left As Integer
        Public Top As Integer
        Public Right As Integer
        Public Bottom As Integer
    End Structure

#End Region

#End Region

#Region " Constructors "

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyBase.New()

        'Required for Windows.Forms Class Composition Designer support
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

        'Call CreateHeaderItems()

        Me.View = Windows.Forms.View.Details
        Me.MultiSelect = False
        'Me.CheckBoxes = True
        Me.HideSelection = False
        Me.FullRowSelect = True
        Me.GridLines = True
        'Me.OwnerDraw = True

        'Select All Checkbox

        'Column back color code
        'Me.DoubleBuffered = True
        'Timer1.Interval = 1
        'Timer1.Start()
        '_columnBackcolor = Brushes.WhiteSmoke
        'Column back color code

        SetStyle(ControlStyles.SupportsTransparentBackColor, True)
        SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)
    End Sub

    Public Sub New()
        Me.View = Windows.Forms.View.Details
        Me.MultiSelect = False
        'Me.CheckBoxes = True
        Me.HideSelection = False
        Me.FullRowSelect = True
        Me.GridLines = True

        'Column back color code
        'Me.DoubleBuffered = True
        'Timer1.Interval = 1
        'Timer1.Start()
        '_columnBackcolor = Brushes.WhiteSmoke
        'Column back color code

        SetStyle(ControlStyles.SupportsTransparentBackColor, True)
        SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)

    End Sub

#End Region

#Region " Properties "

    <Browsable(False)> _
    Public ReadOnly Property CheckedItemIdArray() As String()
        Get
            'Dim strIds As String()
            'If mstrCheckedItems.Length > 0 Then
            '    mstrCheckedItems = mstrCheckedItems.Remove(mstrCheckedItems.LastIndexOf(","), 1)
            '    strIds = mstrCheckedItems.Split(CChar(","))
            '    Return strIds
            'Else
            '    Return Nothing
            'End If

            If mstrCheckedItems.Count > 0 Then
                Return mstrCheckedItems.ToArray
            Else
                Return Nothing
            End If
        End Get
    End Property

    <Browsable(False)> _
    Public ReadOnly Property CheckedItemIdsString() As String
        Get
            'If mstrCheckedItems.Length > 0 Then
            '    mstrCheckedItems = mstrCheckedItems.Remove(mstrCheckedItems.LastIndexOf(","), 1)
            '    Return mstrCheckedItems
            'Else
            '    Return ""
            'End If

            If mstrCheckedItems.Count > 0 Then
                Return String.Join(",", mstrCheckedItems.ToArray())
            Else
                Return ""
            End If
        End Get
    End Property

    <Browsable(False)> _
    Public Property GroupingColumn() As ColumnHeader
        Get
            Return mcolGroupingColumn
        End Get
        Set(ByVal value As ColumnHeader)
            mcolGroupingColumn = value
            If mcolGroupingColumn IsNot Nothing Then
                mintLastGroupColumnIndex = mcolGroupingColumn.Index
            End If
        End Set
    End Property

    Private mblnBackColorOnChecked As Boolean = True
    Public Property BackColorOnChecked() As Boolean
        Get
            Return mblnBackColorOnChecked
        End Get
        Set(ByVal value As Boolean)
            mblnBackColorOnChecked = value
        End Set
    End Property

    Private mblnSorted As Boolean = True
    Public Property Sortable() As Boolean
        Get
            Return mblnSorted
        End Get
        Set(ByVal value As Boolean)
            mblnSorted = value
        End Set
    End Property

    <Browsable(False)> _
    Public Property ShowSelectAll() As Boolean
        Get
            Return mblnShowSelectAll
        End Get
        Set(ByVal value As Boolean)
            mblnShowSelectAll = value
            'If value = False Then
            '    'Me.Parent.Controls.Remove(objchkSelectAll)
            'Else
            '    objchkSelectAll = New CheckBox
            '    objchkSelectAll.AutoSize = True
            '    objchkSelectAll.Name = "objchkSelectAll"
            '    objchkSelectAll.Size = New System.Drawing.Size(15, 14)
            '    objchkSelectAll.UseVisualStyleBackColor = True
            '    objchkSelectAll.Location = New Point(Me.Location.X + 8, Me.Location.Y + 5)
            '    Me.Parent.Controls.Add(objchkSelectAll)
            '    objchkSelectAll.BringToFront()
            'End If
        End Set
    End Property

#End Region

#Region " Public Methods "

    Public Sub DisplayGroups(ByVal blnShowGroup As Boolean, Optional ByVal blnByName As Boolean = False)
        If Not Me.GroupingColumn.Index < 0 Then
            mintLastGroupColumnIndex = Me.GroupingColumn.Index
        End If

        Dim lvGroup As ListViewGroup = Nothing
        If blnShowGroup Then
            Dim strGroupName As String = ""
            Me.Groups.Clear()
            For Each lvItem As ListViewItem In Me.Items
                If Not blnByName Then
                    If strGroupName <> lvItem.SubItems(mintLastGroupColumnIndex).Text Then
                        lvGroup = New ListViewGroup

                        lvGroup.Header = lvItem.SubItems(mintLastGroupColumnIndex).Text
                        lvGroup.HeaderAlignment = HorizontalAlignment.Left

                        Me.Groups.Add(lvGroup)
                    End If
                    lvItem.Group = lvGroup
                    strGroupName = lvItem.SubItems(mintLastGroupColumnIndex).Text
                Else
                    If strGroupName <> lvItem.SubItems(mintLastGroupColumnIndex).Text.Substring(0, 1) Then
                        lvGroup = New ListViewGroup
                        lvGroup.Header = lvItem.SubItems(mintLastGroupColumnIndex).Text.Substring(0, 1)
                        lvGroup.HeaderAlignment = HorizontalAlignment.Left

                        Me.Groups.Add(lvGroup)
                    End If
                    lvItem.Group = lvGroup
                    strGroupName = lvItem.SubItems(mintLastGroupColumnIndex).Text.Substring(0, 1)
                End If
            Next
        Else
            Me.Groups.Clear()
        End If
    End Sub

    Public Sub InsertHiddenColumns()
        Me.SuspendLayout()
        Dim colHeader As ColumnHeader = Nothing
        For Each mnuItem As eZeeToolStripMenuItem In cpHeader.Items
            If mnuItem.Image Is Nothing Then
                colHeader = mnuItem.BindedColumn
                colHeader.Width = 0
                Me.Columns.Insert(mnuItem.BindedColumnIndex, colHeader)
            End If
        Next
    End Sub

    Public Sub RemoveHiddenColumns()
        Dim colHeader As ColumnHeader = Nothing

        For Each mnuItem As eZeeToolStripMenuItem In cpHeader.Items
            If mnuItem.Image Is Nothing Then
                colHeader = mnuItem.BindedColumn

                colHeader.Width = mnuItem.BindedColumnWidth
                Me.Columns.Remove(colHeader)
            End If
        Next
        Me.ResumeLayout()
    End Sub

   
#End Region

#Region " Overrides Events "

    Protected Overrides Sub OnItemChecked(ByVal e As System.Windows.Forms.ItemCheckedEventArgs)
        MyBase.OnItemChecked(e)
        If Me.Items.Count < 1 Then Exit Sub

        If Not Me.CheckBoxes Then Exit Sub

        'If e.Item.Checked Then
        '    mstrCheckedItems &= e.Item.Tag.ToString + ","
        'Else
        '    If mstrCheckedItems.Length > 0 Then
        '        mstrCheckedItems = mstrCheckedItems.Replace(e.Item.Tag.ToString + ",", "")
        '    End If
        'End If

        If e.Item.Tag IsNot Nothing Then
            If e.Item.Checked Then
                If Not mstrCheckedItems.Contains(e.Item.Tag.ToString) Then
                    mstrCheckedItems.Add(e.Item.Tag.ToString)
                End If
            Else
                If mstrCheckedItems.Contains(e.Item.Tag.ToString) Then
                    mstrCheckedItems.Remove(e.Item.Tag.ToString)
                End If
            End If
        End If

        If mblnBackColorOnChecked = True Then
            'If e.Item.Checked = True Then
            '    'Krushna (FD GUI) (10 Aug 2009) -- Start
            '    e.Item.BackColor = Color.LightSteelBlue
            '    'Krushna (FD GUI) (10 Aug 2009) -- End
            'Else
            '    e.Item.BackColor = Me.BackColor
            'End If
            e.Item.Selected = True
        End If
    End Sub

    Protected Overrides Sub OnItemDrag(ByVal e As System.Windows.Forms.ItemDragEventArgs)
        MyBase.OnItemDrag(e)

        If e.Button = Windows.Forms.MouseButtons.Left Then
            Me.DoDragDrop(e.Item, CType(DragDropEffects.Move, DragDropEffects))
        End If
    End Sub

    Protected Overrides Sub Finalize()
        'Column back color code
        'Timer1.Dispose()
        'Column back color code
        MyBase.Finalize()
    End Sub

    'Protected Overrides Sub OnMove(ByVal e As System.EventArgs)
    '    MyBase.OnMove(e)
    'If Me.Parent.Controls.Contains(objchkSelectAll) Then
    '    objchkSelectAll.Location = New Point(Me.Location.X + 8, Me.Location.Y + 5)
    '    objchkSelectAll.BringToFront()
    'End If
    'End Sub

    'Protected Overrides Sub OnParentChanged(ByVal e As System.EventArgs)
    '    MyBase.OnParentChanged(e)
    '    If Me.Parent.Controls.Contains(objchkSelectAll) Then
    '        Me.ShowSelectAll = False
    '    Else
    '        Me.ShowSelectAll = True
    '    End If
    'End Sub

    'Protected Overrides Sub OnVisibleChanged(ByVal e As System.EventArgs)
    '    MyBase.OnVisibleChanged(e)
    '    If Me.ShowSelectAll Then
    '        Me.objchkSelectAll.Visible = Me.Visible
    '    End If
    'End Sub

    'Protected Overrides Sub OnEnabledChanged(ByVal e As System.EventArgs)
    '    MyBase.OnEnabledChanged(e)
    '    If Me.ShowSelectAll Then
    '        Me.objchkSelectAll.Enabled = Me.Enabled
    '    End If
    'End Sub

    'Column back color code
    'Protected Overrides Sub OnColumnWidthChanged(ByVal e As System.Windows.Forms.ColumnWidthChangedEventArgs)
    '    MyBase.OnColumnWidthChanged(e)

    '    ColumnSelect(_SelectedColumn)
    'End Sub

    'Protected Overrides Sub OnColumnWidthChanging(ByVal e As System.Windows.Forms.ColumnWidthChangingEventArgs)
    '    MyBase.OnColumnWidthChanging(e)

    '    Me.BackgroundImage = Nothing
    'End Sub
    'Column back color code

    'Column back color code
    'Protected Overrides Sub WndProc(ByRef m As Message)
    '    MyBase.WndProc(m)

    '    'If m.Msg = WM_HSCROLL Or m.Msg = WM_VSCROLL Or m.Msg = SBM_SETSCROLLINFO Then
    '    '    OnScroll()
    '    'End If
    'End Sub

    'Protected Sub OnScroll()
    '    'RaiseEvent Scroll(Me, EventArgs.Empty)
    'End Sub
    'Column back color code

#End Region

#Region " Sort By "
    Private lvwColumnSorter As ListViewColumnSorter

    Public Sub SortBy(ByVal ColumnIndex As Integer, ByVal SortOrd As SortOrder)
        Call Me.SortBy(ColumnIndex, SortOrd, ListViewColumnSorter.ComparerType.StringComparer)
    End Sub

    Public Sub SortBy(ByVal ColumnIndex As Integer, ByVal ComparerType As ListViewColumnSorter.ComparerType)
        Call Me.SortBy(ColumnIndex, SortOrder.None, ComparerType)
    End Sub

    Public Sub SortBy(ByVal ColumnIndex As Integer, ByVal Order As SortOrder, ByVal ComparerType As ListViewColumnSorter.ComparerType)
        If lvwColumnSorter Is Nothing Then
            lvwColumnSorter = New ListViewColumnSorter()
            MyBase.ListViewItemSorter = lvwColumnSorter
        End If

        lvwColumnSorter.ColumnIndex = ColumnIndex
        If Order <> Nothing Then
            lvwColumnSorter.Order = Order
        End If

        If ComparerType <> Nothing Then
            lvwColumnSorter.DataType = ComparerType
        End If

        MyBase.Sort()
        MyBase.Refresh()
    End Sub

    Public Sub SortingType(ByVal ComparerType As ListViewColumnSorter.ComparerType)
        lvwColumnSorter.DataType = ComparerType
    End Sub

    Protected Overrides Sub OnColumnClick(ByVal e As System.Windows.Forms.ColumnClickEventArgs)
        If lvwColumnSorter Is Nothing Then
            lvwColumnSorter = New ListViewColumnSorter()
            MyBase.ListViewItemSorter = lvwColumnSorter
        End If
        MyBase.OnColumnClick(e)

        lvwColumnSorter.ColumnIndex = e.Column
        MyBase.Sort()
    End Sub
#End Region

End Class

