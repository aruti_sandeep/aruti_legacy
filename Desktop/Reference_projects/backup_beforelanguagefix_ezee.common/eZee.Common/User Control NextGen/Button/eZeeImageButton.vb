Option Strict On

Imports System
Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms

Public Class eZeeImageButton
    Inherits PictureBox

#Region " Private Variables "

    Private mimgNormalImage As Image = Nothing
    Private mimgHoverImage As Image = Nothing
    Private mimgMouseDownImage As Image = Nothing
    Private mimgMouseUpImage As Image = Nothing
    Private mimgSelectedImage As Image = Nothing

#End Region

#Region " Constructor "

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.Height = 48
        Me.Width = 48
        Me.BackgroundImageLayout = ImageLayout.Center
        Me.BackColor = Color.Transparent
        'Me.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)

        'Me.FlatStyle = Windows.Forms.FlatStyle.Flat
        'Me.FlatAppearance.BorderSize = 0
        'Me.FlatAppearance.MouseDownBackColor = Color.Transparent
        'Me.FlatAppearance.MouseOverBackColor = Color.Transparent
    End Sub

#End Region

#Region " Properties "

    <Browsable(False)> _
    Public Overrides Property BackgroundImage() As Image
        Get
            Return MyBase.BackgroundImage
        End Get
        Set(ByVal value As Image)
            MyBase.BackgroundImage = value
        End Set
    End Property

    Public Property NormalImage() As Image
        Get
            Return mimgNormalImage
        End Get
        Set(ByVal value As Image)
            mimgNormalImage = value
            Me.BackgroundImage = value
            Me.Invalidate()
        End Set
    End Property

    Public Property MouseHoverImage() As Image
        Get
            Return mimgHoverImage
        End Get
        Set(ByVal value As Image)
            mimgHoverImage = value
            Me.Invalidate()
        End Set
    End Property

    Public Property MouseDownImage() As Image
        Get
            Return mimgMouseDownImage
        End Get
        Set(ByVal value As Image)
            mimgMouseDownImage = value
            Me.Invalidate()
        End Set
    End Property

    Public Property MouseUpImage() As Image
        Get
            Return mimgMouseUpImage
        End Get
        Set(ByVal value As Image)
            mimgMouseUpImage = value
            Me.Invalidate()
        End Set
    End Property

    Public Property SelectedImage() As Image
        Get
            Return mimgSelectedImage
        End Get
        Set(ByVal value As Image)
            mimgSelectedImage = value
            Me.Invalidate()
        End Set
    End Property

#End Region

#Region " Overrides Events "

    Protected Overrides Sub OnMouseEnter(ByVal e As System.EventArgs)
        MyBase.OnMouseEnter(e)

        If Me.MouseHoverImage Is Nothing Then Exit Sub

        Me.BackgroundImage = Me.MouseHoverImage
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal e As System.EventArgs)
        MyBase.OnMouseLeave(e)

        If Me.Focused Then
            If Me.SelectedImage IsNot Nothing Then
                Me.BackgroundImage = Me.SelectedImage
            Else
                Me.BackgroundImage = Me.NormalImage
            End If
        Else
            Me.BackgroundImage = Me.NormalImage
        End If
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal mevent As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseDown(mevent)

        If Me.MouseDownImage Is Nothing Then Exit Sub

        Me.BackgroundImage = Me.MouseDownImage
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal mevent As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseUp(mevent)

        If Me.MouseUpImage Is Nothing Then Exit Sub

        Me.BackgroundImage = Me.MouseUpImage
    End Sub

    Protected Overrides Sub OnClick(ByVal e As System.EventArgs)
        MyBase.OnClick(e)
        If Me.SelectedImage IsNot Nothing Then
            Me.BackgroundImage = Me.SelectedImage
        Else
            Me.BackgroundImage = Me.NormalImage
        End If
    End Sub

#End Region

End Class
