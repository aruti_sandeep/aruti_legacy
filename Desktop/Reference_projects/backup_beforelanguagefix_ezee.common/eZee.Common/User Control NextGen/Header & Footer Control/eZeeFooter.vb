Imports System
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Drawing.Drawing2D


Public Class eZeeFooter
    Inherits Panel

#Region " Private Variables "

    Public Enum GradientStyle
        Up = 1
        Down = 2
        Central = 3
    End Enum

    Private mclrBorderColor As Color = Color.Silver
    Private menGradientStyle As GradientStyle = GradientStyle.Up
    Private mclrGradientColor1 As Color = SystemColors.Control
    Private mclrGradientColor2 As Color = SystemColors.Control

#End Region

#Region " Constructor "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        'Krushna (FD GUI) (10 Aug 2009) -- Start
        ' Add any initialization after the InitializeComponent() call.
        SetStyle(ControlStyles.UserPaint, True)
        SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        SetStyle(ControlStyles.CacheText, True)
        SetStyle(ControlStyles.ResizeRedraw, True)
        'Krushna (FD GUI) (10 Aug 2009) -- End

        Me.Dock = DockStyle.Bottom
    End Sub

#End Region

#Region " Properties "

    Public Property BorderColor() As Color
        Get
            Return mclrBorderColor
        End Get
        Set(ByVal value As Color)
            mclrBorderColor = value
            Me.Invalidate()
        End Set
    End Property

    Public Property GradiantStyle() As GradientStyle
        Get
            Return menGradientStyle
        End Get
        Set(ByVal value As GradientStyle)
            menGradientStyle = value
            'Call OnPaint(New System.Windows.Forms.PaintEventArgs(Me.CreateGraphics, Me.ClientRectangle))
            Me.Invalidate()
        End Set
    End Property

    Public Property GradientColor1() As Color
        Get
            Return mclrGradientColor1
        End Get
        Set(ByVal value As Color)
            mclrGradientColor1 = value
            Me.Invalidate()
        End Set
    End Property

    Public Property GradientColor2() As Color
        Get
            Return mclrGradientColor2
        End Get
        Set(ByVal value As Color)
            mclrGradientColor2 = value
            Me.Invalidate()
        End Set
    End Property

#End Region

#Region " Overrides Events "

    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
        If Me.Height < 50 Then
            Me.Height = 50
        Else
            MyBase.OnResize(e)
        End If
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        MyBase.OnPaint(e)

        Dim g As Graphics = e.Graphics
        Dim start As New Point
        Dim gb As LinearGradientBrush

        Select Case menGradientStyle
            Case GradientStyle.Up
                start.X = Me.Width
                start.Y = 0

                gb = New LinearGradientBrush(start, New Point(start.X, Me.Height), mclrGradientColor2, mclrGradientColor1)
                g.FillRectangle(gb, New Rectangle(0, 0, Me.Width, Me.Height))
            Case GradientStyle.Down
                start.X = Me.Width
                start.Y = 0

                gb = New LinearGradientBrush(start, New Point(start.X, Me.Height), mclrGradientColor1, mclrGradientColor2)
                g.FillRectangle(gb, New Rectangle(0, 0, Me.Width, Me.Height))
            Case GradientStyle.Central
                start.X = CInt(Me.Width * 0.5)
                start.Y = 0

                gb = New LinearGradientBrush(start, New Point(start.X, CInt(Me.Height * 0.5)), mclrGradientColor1, mclrGradientColor2)
                g.FillRectangle(gb, New Rectangle(0, 0, Me.Width, CInt(Me.Height * 0.5)))
                gb = New LinearGradientBrush(New Point(start.X, CInt(Me.Height * 0.5)), New Point(start.X, Me.Height), mclrGradientColor2, mclrGradientColor1)
                g.FillRectangle(gb, New Rectangle(0, CInt(Me.Height * 0.5), Me.Width, Me.Height))
        End Select

        'If Me.BorderStyle = Windows.Forms.BorderStyle.None Then
        '    ControlPaint.DrawBorder(e.Graphics, New Rectangle(0, 0, Me.Width, Me.Height), mclrBorderColor, ButtonBorderStyle.Solid)
        'End If

        'ControlPaint.DrawBorder3D(e.Graphics, Me.ClientRectangle)

        ControlPaint.DrawBorder(e.Graphics, New Rectangle(-2, 0, Me.Width + 3, 1), mclrBorderColor, ButtonBorderStyle.Solid)
        ControlPaint.DrawBorder3D(e.Graphics, New Rectangle(-2, 0, Me.Width + 3, 1))


    End Sub

#End Region

End Class
