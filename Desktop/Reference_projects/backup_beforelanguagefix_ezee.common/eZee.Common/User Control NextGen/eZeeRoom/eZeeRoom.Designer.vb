Partial Class eZeeRoom
    Inherits System.Windows.Forms.Control

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New(ByVal Container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        Container.Add(Me)

    End Sub

    'Component overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(eZeeRoom))
        Me.imgRoomStatus = New System.Windows.Forms.ImageList(Me.components)
        Me.SuspendLayout()
        '
        'imgRoomStatus
        '
        Me.imgRoomStatus.ImageStream = CType(resources.GetObject("imgRoomStatus.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgRoomStatus.TransparentColor = System.Drawing.Color.Transparent
        Me.imgRoomStatus.Images.SetKeyName(0, "LM 04.ico")
        Me.imgRoomStatus.Images.SetKeyName(1, "GroupMember5_16.png")
        Me.imgRoomStatus.Images.SetKeyName(2, "NetLock1_16.png")
        Me.imgRoomStatus.Images.SetKeyName(3, "NoSmoking_16.png")
        Me.imgRoomStatus.Images.SetKeyName(4, "VipGuest1_16.png")
        Me.imgRoomStatus.Images.SetKeyName(5, "Zadu_16.png")
        Me.imgRoomStatus.Images.SetKeyName(6, "FollowUp_16.png")
        Me.imgRoomStatus.Images.SetKeyName(7, "LM 05.ico")
        '
        'eZeeRoom
        '
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents imgRoomStatus As System.Windows.Forms.ImageList

End Class
