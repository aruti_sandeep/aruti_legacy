Imports System.ComponentModel
Imports System.Drawing

Public Class eZeeRoom
    Inherits System.Windows.Forms.Control

    Private mintRoomUnkid As Integer
    Private mintTranUnkid As Integer
    Private mintStatusUnkid As Integer
    Private mstrRoomType As String
    Private mintXPos As Integer
    Private mintYPos As Integer
    Private mblnUsed As Boolean = False
    Private mstrGUID As Guid = Nothing
    Private mstrIP As String = ""
    Private mstrMachineName As String = ""
    Private mstrUserName As String = ""
    
    Private mintHeight As Integer
    Private mintWidth As Integer

    'Public Sub New()

    '    'mintHeight = Size.Height
    '    'mintWidth = Size.Width
    'End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        SetStyle(Windows.Forms.ControlStyles.UserPaint, True)
        SetStyle(Windows.Forms.ControlStyles.OptimizedDoubleBuffer, True)
        SetStyle(Windows.Forms.ControlStyles.AllPaintingInWmPaint, True)
        SetStyle(Windows.Forms.ControlStyles.CacheText, True)
        'SetStyle(Windows.Forms.ControlStyles.ResizeRedraw, True)
    End Sub

    <Category("Room Property"), _
    [ReadOnly](True), _
    DisplayName(" ID"), _
    Description("Room unique key")> _
    Public Property RoomUnkid() As Integer
        Get
            Return mintRoomUnkid
        End Get
        Set(ByVal intRoomUnkid As Integer)
            mintRoomUnkid = intRoomUnkid
        End Set
    End Property

    <Category("Room Property"), _
        [ReadOnly](True), _
        DisplayName(" Tran ID"), _
        Description("Room Transaction unique key")> _
        Public Property TranUnkid() As Integer
        Get
            Return mintTranUnkid
        End Get
        Set(ByVal intTranUnkid As Integer)
            mintTranUnkid = intTranUnkid
        End Set
    End Property

    <Category("Room Property"), _
        [ReadOnly](True), _
        DisplayName(" Status"), _
        Description("Room Status")> _
        Public Property StatusUnkid() As Integer
        Get
            Return mintStatusUnkid
        End Get
        Set(ByVal intStatusUnkid As Integer)
            mintStatusUnkid = intStatusUnkid
        End Set
    End Property

    <Category("Room Property"), _
        [ReadOnly](True), _
        DisplayName(" Room Type"), _
        Description("Room Type of the selected room")> _
        Public Property RoomType() As String
        Get
            Return mstrRoomType
        End Get
        Set(ByVal strRoomType As String)
            mstrRoomType = strRoomType
        End Set
    End Property

    <Category("Room Property"), _
    DisplayName("X Position"), _
    Description("X position of selected room ")> _
    Public Property xPos() As Integer
        Get
            Return Location.X
        End Get
        Set(ByVal intXPos As Integer)
            mintXPos = intXPos
            Location = New Point(mintXPos, Location.Y)
        End Set
    End Property

    <Category("Room Property"), _
    DisplayName("Y Position"), _
    Description("Y position of selected room ")> _
    Public Property yPos() As Integer
        Get
            Return Location.Y
        End Get
        Set(ByVal intYPos As Integer)
            mintYPos = intYPos
            Location = New Point(Location.X, mintYPos)
        End Set
    End Property

    <Category("Room Property"), _
    DisplayName("Height"), _
    Description("Height of selected room ")> _
    Public Property RoomHeight() As Integer
        Get
            Return Size.Height
        End Get
        Set(ByVal intHeight As Integer)
            mintHeight = intHeight
            Size = New System.Drawing.Size(Size.Width, mintHeight)
        End Set
    End Property

    <Category("Room Property"), _
    DisplayName("Width"), _
    Description("Width of selected room ")> _
    Public Property RoomWidth() As Integer
        Get
            Return Size.Width
        End Get
        Set(ByVal intWidth As Integer)
            mintWidth = intWidth
            Size = New System.Drawing.Size(mintWidth, Size.Height)
        End Set
    End Property

    Public Property Used() As Boolean
        Get
            Return mblnUsed
        End Get
        Set(ByVal blnUsed As Boolean)
            mblnUsed = blnUsed
        End Set
    End Property

    Public Property IP() As String
        Get
            Return mstrIP
        End Get
        Set(ByVal strIP As String)
            mstrIP = strIP
        End Set
    End Property

    Public Property MachineName() As String
        Get
            Return mstrMachineName
        End Get
        Set(ByVal strMachineName As String)
            mstrMachineName = strMachineName
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return mstrUserName
        End Get
        Set(ByVal strUserName As String)
            mstrUserName = strUserName
        End Set
    End Property

    Protected pointX As Integer = 0
    Protected pointY As Integer = 0
    Protected iRect As New Rectangle()
    Protected txtRect As New Rectangle()

    Protected _Color1 As Color = Color.White
    Protected _Color2 As Color = Color.White

    Private _radius As Integer = 5
    Private _borderColor As Color = Color.Black

    Private mintImageIndex As Integer = 0

    Private mblnNetLock As Boolean = False
    Public Property _NetLock() As Boolean
        Get
            Return mblnNetLock
        End Get
        Set(ByVal value As Boolean)
            mblnNetLock = value
            'Call setImageIndex(2)
            Me.Invalidate()
        End Set
    End Property

    Private mblnDirty As Boolean = False
    Public Property _Dirty() As Boolean
        Get
            Return mblnDirty
        End Get
        Set(ByVal value As Boolean)
            mblnDirty = value
            'Call setImageIndex(2)
            Me.Invalidate()
        End Set
    End Property

    Private mblnNonSmocking As Boolean = False
    Public Property _NonSmocking() As Boolean
        Get
            Return mblnNonSmocking
        End Get
        Set(ByVal value As Boolean)
            mblnNonSmocking = value
            Me.Invalidate()
        End Set
    End Property

    Private mblnGroup As Boolean = False
    Public Property _Group() As Boolean
        Get
            Return mblnGroup
        End Get
        Set(ByVal value As Boolean)
            mblnGroup = value
            Me.Invalidate()
        End Set
    End Property

    Private mblnFollowup As Boolean = False
    Public Property _Followup() As Boolean
        Get
            Return mblnFollowup
        End Get
        Set(ByVal value As Boolean)
            mblnFollowup = value
            Me.Invalidate()
        End Set
    End Property

    Private mblnVipGuest As Boolean = False
    Public Property _VipGuest() As Boolean
        Get
            Return mblnVipGuest
        End Get
        Set(ByVal value As Boolean)
            mblnVipGuest = value
            Me.Invalidate()
        End Set
    End Property

    <Category("Radius"), _
    DisplayName(" Radius"), _
    Description("Rounded border.")> _
    Public Property Radius() As Integer
        Get
            Return _radius
        End Get
        Set(ByVal Value As Integer)
            _radius = Value
            Me.Invalidate()
        End Set
    End Property

    Public Enum TextAlignment
        Left
        Center
        Right
    End Enum

    Public Property Color1() As Color
        Get
            Return _Color1
        End Get
        Set(ByVal value As Color)
            _Color1 = value
            Me.Invalidate()
        End Set
    End Property

    Public Property Color2() As Color
        Get
            Return _Color2
        End Get
        Set(ByVal value As Color)
            _Color2 = value
            Me.Invalidate()
        End Set
    End Property

    <Browsable(False)> _
    Public Shadows Property BackColor() As Color
        Get

        End Get
        Set(ByVal value As Color)

        End Set
    End Property

    Protected Overloads Overrides Sub OnPaint(ByVal pe As Windows.Forms.PaintEventArgs)
        ' TODO: Add custom paint code here 

        ' Calling the base class OnPaint 
        MyBase.OnPaint(pe)
        iRect = MyBase.ClientRectangle
        DrawBorder(pe.Graphics)
        'pe.Graphics.SmoothingMode = sm
        DrawText(pe)
        DrawStatusImage(pe)

        pe.Dispose()
    End Sub

    Protected Overloads Overrides Sub OnResize(ByVal e As EventArgs)
        MyBase.OnResize(e)
        iRect = MyBase.ClientRectangle
        Me.Invalidate()
    End Sub

    Protected Sub DrawText(ByVal pe As Windows.Forms.PaintEventArgs)
        'pe.Graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias
        Dim sz As SizeF = pe.Graphics.MeasureString(MyBase.Text, MyBase.Font)

        pointX = (Me.iRect.Width - sz.Width) / 2
        txtRect = New Rectangle(Me.pointX, Me.pointY + 2, CInt(sz.Width), CInt(sz.Height))


        'If the mouse is passing over the text it is selected and will be dimmed 
        'otherwise nothing. 
        Dim brText As Brush = New SolidBrush(MyBase.ForeColor)
        Dim sf As StringFormat = New StringFormat
        sf.Alignment = StringAlignment.Center

        pe.Graphics.DrawString(MyBase.Text, MyBase.Font, brText, New Rectangle(Me.ClientRectangle.X + 2, Me.ClientRectangle.Y + 2, Me.ClientRectangle.Width, Me.ClientRectangle.Height - 20), sf)
    End Sub

    Private Sub DrawBorder(ByVal g As Graphics)
        Dim rect As Rectangle = Me.ClientRectangle
        rect.Width -= 1
        rect.Height -= 1
        Using bp As Drawing2D.GraphicsPath = GetPath(rect, _radius)
            'Dipti (13 May 2010) - Start
            Dim Regbp As Drawing2D.GraphicsPath = GetPath(New Rectangle(rect.X - 2, rect.Y - 2, rect.Width + 4, rect.Height + 4), _radius * 2)
            Me.Region = New Region(Regbp)

            Dim clr As Color = _Color2
            If _Color2.Name = "0" Then
                clr = Color.LightGray
            ElseIf _Color2.Name = "ffffffff" Then
                clr = Color.LightGray
            Else
                clr = _Color2
            End If
            clr = Color.LightGray
            Using p As New Pen(clr)
                'Dipti (13 May 2010) - End
                Dim grBrush As Drawing2D.LinearGradientBrush = New Drawing2D.LinearGradientBrush(rect, _Color1, _Color2, Drawing2D.LinearGradientMode.Vertical)
                g.FillPath(grBrush, bp)
                Dim sm As Drawing2D.SmoothingMode = g.SmoothingMode
                g.SmoothingMode = Drawing2D.SmoothingMode.AntiAlias
                g.DrawPath(p, bp)
                g.SmoothingMode = sm
            End Using
        End Using
    End Sub

    Protected Function GetPath(ByVal rc As Rectangle, ByVal r As Integer) As Drawing2D.GraphicsPath
        ' Build the path with the round corners in the rectangle 
        ' r is the radious of rounded corner. 
        Dim x As Integer = rc.X, y As Integer = rc.Y, w As Integer = rc.Width, h As Integer = rc.Height
        r = r << 1
        Dim path As New Drawing2D.GraphicsPath()
        If r > 0 Then
            ' If the radious of rounded corner is greater than one side then 
            ' do the side rounded 
            If r > h Then
                r = h
            End If


            'Rounded 
            If r > w Then
                r = w
            End If


            ''Rounded 
            'path.AddArc(x, y, r, r, 180, 90)
            ''Upper left corner 
            'path.AddArc(x + w - r, y, r, r, 270, 90)
            ''Upper right corner 
            'path.AddArc(x + w - r, y + h - r, r, r, 0, 90)
            ''Lower right corner 
            'path.AddArc(x, y + h - r, r, r, 90, 90)
            ''Lower left corner 

            'Rounded 
            path.AddArc(x, y, r, r, 180, 90)
            'Upper left corner 
            path.AddArc(x + w - r, y, r, r, 270, 90)
            'Upper right corner 
            path.AddArc(x + w - r, y + h - r, r, r, 20, 90)
            'Lower right corner 
            path.AddArc(x, y + h - r, r, r, 90, 90)
            'Lower left corner 
            path.CloseFigure()

        Else
            ' If the radious of rounded corner is zero then the path is a rectangle 
            path.AddRectangle(rc)
        End If

        Return path
    End Function

    Protected Function GetShadowPath(ByVal rc As Rectangle, ByVal r As Integer) As Drawing2D.GraphicsPath
        ' Build the path with the round corners in the rectangle 
        ' r is the radious of rounded corner. 
        Dim x As Integer = rc.X, y As Integer = rc.Y, w As Integer = rc.Width, h As Integer = rc.Height
        r = r << 1
        Dim path As New Drawing2D.GraphicsPath()
        If r > 0 Then
            ' If the radious of rounded corner is greater than one side then 
            ' do the side rounded 
            If r > h Then
                r = h
            End If


            'Rounded 
            If r > w Then
                r = w
            End If


            'Rounded 
            path.AddArc(x, y, r, r, 180, 90)
            'Upper left corner 
            path.AddArc(x + w - r, y, r, r, 270, 90)
            'Upper right corner 
            path.AddArc(x + w - r, y + h - r, r, r, 0, 90)
            'Lower right corner 
            path.AddArc(x, y + h - r, r, r, 90, 90)
            'Lower left corner 
            path.CloseFigure()

        Else
            ' If the radious of rounded corner is zero then the path is a rectangle 
            path.AddRectangle(rc)
        End If

        Return path
    End Function

    Protected Sub DrawStatusImage(ByVal pe As Windows.Forms.PaintEventArgs)
        'pe.Graphics.DrawImage(imgRoomStatus.Images(0), New Point(6, 53))
        'pe.Graphics.DrawImage(imgRoomStatus.Images(1), New Point(30, 53))
        'pe.Graphics.DrawImage(imgRoomStatus.Images(2), New Point(54, 53))
        'pe.Graphics.DrawImage(imgRoomStatus.Images(3), New Point(6, 33))
        'pe.Graphics.DrawImage(imgRoomStatus.Images(4), New Point(30, 33))
        'pe.Graphics.DrawImage(imgRoomStatus.Images(5), New Point(54, 33))
        mintImageIndex = 0
        DrawImage(pe.Graphics)
    End Sub

    Protected Sub DrawImage(ByVal g As Graphics)
        Dim intY As Integer
        Dim intX As Integer

        Dim blnFollowup As Boolean = mblnFollowup
        Dim blnGroup As Boolean = mblnGroup
        Dim blnNetLock As Boolean = mblnNetLock
        Dim blnNonSmocking As Boolean = mblnNonSmocking
        Dim blnVipGuest As Boolean = mblnVipGuest
        Dim blnDirty As Boolean = mblnDirty


        Dim intLine As Integer = 2
        Dim intInOnelineImage As Integer = 3
        Dim intNoofIconDraw As Integer = 0
        If mblnFollowup Then intNoofIconDraw += 1
        If mblnGroup Then intNoofIconDraw += 1
        If mblnNetLock Then intNoofIconDraw += 1
        If mblnNonSmocking Then intNoofIconDraw += 1
        If mblnVipGuest Then intNoofIconDraw += 1
        If mblnDirty Then intNoofIconDraw += 1

        If Me.ClientRectangle.Width >= 148 Then
            intLine = 1
            intInOnelineImage = 6
        Else
            intLine = 2
            If Me.ClientRectangle.Width >= 99 Then
                If Me.ClientRectangle.Width < 120 Then
                    intInOnelineImage = 4
                ElseIf Me.ClientRectangle.Width < 148 Then
                    intInOnelineImage = 5
                Else
                    intInOnelineImage = 6
                End If
            Else
                intInOnelineImage = 3
            End If
        End If

        Dim j As Integer = 1
        For i As Integer = 1 To intNoofIconDraw
            If intLine = 2 Then
                If i <= intInOnelineImage Then
                    intX = (j - 1) * 21 + 6
                    intY = Me.ClientRectangle.Height - 22
                Else
                    intX = (j - 1) * 21 + 6
                    intY = Me.ClientRectangle.Height - 42
                End If
            Else
                intY = Me.ClientRectangle.Height - 22
                intX = (i - 1) * 21 + 6
            End If
            If i = intInOnelineImage Then
                j = 1
            Else
                j += 1
            End If


            If blnFollowup Then
                g.DrawImage(imgRoomStatus.Images(0), New Point(intX, intY))
                mintImageIndex += 1
                blnFollowup = False
                Continue For
            End If
            If blnGroup Then
                g.DrawImage(imgRoomStatus.Images(1), New Point(intX, intY))
                mintImageIndex += 1
                blnGroup = False
                Continue For
            End If
            If blnNetLock Then
                g.DrawImage(imgRoomStatus.Images(2), New Point(intX, intY))
                mintImageIndex += 1
                blnNetLock = False
                Continue For
            End If
            If blnNonSmocking Then
                g.DrawImage(imgRoomStatus.Images(3), New Point(intX, intY))
                mintImageIndex += 1
                blnNonSmocking = False
                Continue For
            End If
            If blnVipGuest Then
                g.DrawImage(imgRoomStatus.Images(4), New Point(intX, intY))
                mintImageIndex += 1
                blnVipGuest = False
                Continue For
            End If
            If blnDirty Then
                g.DrawImage(imgRoomStatus.Images(5), New Point(intX, intY))
                mintImageIndex += 1
                blnDirty = False
                Continue For
            End If
        Next
    End Sub

    Private Sub eZeeRoom_ControlAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.ControlEventArgs) Handles Me.ControlAdded
        mintXPos = Location.X
        mintYPos = Location.Y
    End Sub

End Class
