Public Class KeyPressedEventArgs
    Inherits EventArgs
    Public Handled As Boolean
    Private m_Key As BG_Button
    Private m_KeyValue As String

    Public Sub New(ByVal aKeyValue As String, ByVal aKey As BG_Button)
        Me.m_KeyValue = aKeyValue
        Me.m_Key = aKey
    End Sub

    Public ReadOnly Property Key() As BG_Button
        Get
            Return Me.m_Key
        End Get
    End Property

    Public ReadOnly Property KeyValue() As String
        Get
            Return m_KeyValue
        End Get
    End Property

End Class