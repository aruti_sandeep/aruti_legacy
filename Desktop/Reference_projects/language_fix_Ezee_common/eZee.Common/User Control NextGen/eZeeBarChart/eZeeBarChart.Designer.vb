<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeBarChart
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.picBarChart = New System.Windows.Forms.PictureBox
        CType(Me.picBarChart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picBarChart
        '
        Me.picBarChart.BackColor = System.Drawing.Color.Transparent
        Me.picBarChart.Dock = System.Windows.Forms.DockStyle.Fill
        Me.picBarChart.Location = New System.Drawing.Point(0, 0)
        Me.picBarChart.Name = "picBarChart"
        Me.picBarChart.Size = New System.Drawing.Size(409, 208)
        Me.picBarChart.TabIndex = 0
        Me.picBarChart.TabStop = False
        '
        'eZeeBarChart
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me.picBarChart)
        Me.Name = "eZeeBarChart"
        Me.Size = New System.Drawing.Size(409, 208)
        CType(Me.picBarChart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents picBarChart As System.Windows.Forms.PictureBox

End Class
