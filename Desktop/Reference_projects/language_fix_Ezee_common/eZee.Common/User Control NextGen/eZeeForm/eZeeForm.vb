Imports System
Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Reflection
Imports System.IO

Public Delegate Sub LanguageClickEventHandler(ByVal sender As Object, ByVal eventArgs As System.EventArgs)

'<System.Diagnostics.DebuggerStepThrough()> _
Public Class eZeeForm
    Inherits Form

#Region " Variable & API Declaration "
    ' The state of our little button
    'Private _buttState As LanguageButtonState = LanguageButtonState.Normal
    'Private _buttPosition As New Rectangle()
    Public Shared mstrForm_Name As String = ""

    <Runtime.InteropServices.DllImport("user32.dll")> _
    Private Shared Function GetWindowDC(ByVal hWnd As IntPtr) As IntPtr
    End Function

    <Runtime.InteropServices.DllImport("user32.dll")> _
    Private Shared Function GetWindowRect(ByVal hWnd As IntPtr, ByRef lpRect As Rectangle) As Integer
    End Function

    <Runtime.InteropServices.DllImport("user32.dll")> _
    Private Shared Function ReleaseDC(ByVal hWnd As IntPtr, ByVal hDC As IntPtr) As Integer
    End Function

    Public Enum LanguageButtonState
        Normal = 0
        Pressed = 1
        Hover = 2
        Selected = 3
    End Enum

#End Region

#Region " Constructor "
    <System.Diagnostics.DebuggerNonUserCode()> _
     Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        Me.LanguageNormal = My.Resources.Language_Normal_21
        Me.LanguageHover = My.Resources.Language_Hover_21
        Me.LanguagePressed = My.Resources.Language_Pressed_21
    End Sub

#End Region

#Region " Event Declaration "

    <EditorBrowsable(EditorBrowsableState.Always)> _
    Public Event LanguageClick As LanguageClickEventHandler

#End Region

#Region " Properties "
    Private mimgLanguageNormal As Image = Nothing
    <Browsable(False)> _
    Public Property LanguageNormal() As Image
        Get
            Return mimgLanguageNormal
        End Get
        Set(ByVal value As Image)
            mimgLanguageNormal = value
        End Set
    End Property

    Private mimgLanguageHover As Image = Nothing
    <Browsable(False)> _
    Public Property LanguageHover() As Image
        Get
            Return mimgLanguageHover
        End Get
        Set(ByVal value As Image)
            mimgLanguageHover = value
        End Set
    End Property

    <Browsable(False)> _
    Private mimgLanguagePressed As Image = Nothing
    Public Property LanguagePressed() As Image
        Get
            Return mimgLanguagePressed
        End Get
        Set(ByVal value As Image)
            mimgLanguagePressed = value
        End Set
    End Property

    Private mblnShowLanguage As Boolean = True
    Public Property ShowLanguageButton() As Boolean
        Get
            Return mblnShowLanguage
        End Get
        Set(ByVal value As Boolean)
            mblnShowLanguage = value
            'Me.Invalidate()
            Me.RefreshMenu()
        End Set
    End Property

    Private mstrHelpTag As String = String.Empty
    Public Property _HelpTag() As String
        Get
            Return mstrHelpTag
        End Get
        Set(ByVal value As String)
            mstrHelpTag = value
        End Set
    End Property
#End Region

#Region " Language "
    'Private Sub DrawButton(ByVal hwnd As IntPtr)
    '    Try

    '        Dim x As Integer, y As Integer

    '        ' Work out size and positioning
    '        Dim CaptionHeight As Integer = Bounds.Height - ClientRectangle.Height
    '        Dim ButtonSize As Size = New Size(SystemInformation.CaptionButtonSize.Width - 3, SystemInformation.CaptionButtonSize.Height - 3) 'New Size(21, 21)
    '        'If gobjUser._RightToLeft Then
    '        '    x = CInt(5 + ButtonSize.Width)
    '        'Else
    '        'End If
    '        Dim dblWinButtonWidth As Double = 0

    '        'Krushna (FD GUI) (10 Aug 2009) -- Start
    '        If Me.ControlBox Then
    '            If Me.MaximizeBox Or Me.MinimizeBox Then
    '                dblWinButtonWidth = 4.4
    '            Else
    '                If Me.HelpButton Then
    '                    dblWinButtonWidth = 3.3
    '                Else
    '                    dblWinButtonWidth = 2.3
    '                End If
    '            End If
    '        Else
    '            dblWinButtonWidth = 1.3
    '        End If
    '        'Krushna (FD GUI) (10 Aug 2009) -- End

    '        x = CInt(Bounds.Width - (dblWinButtonWidth * ButtonSize.Width))

    '        y = CInt((CaptionHeight - ButtonSize.Height) / 2) ' - 1 '+1 1

    '        If Me.RightToLeftLayout And Me.RightToLeft = Windows.Forms.RightToLeft.Yes Then
    '            _buttPosition.Location = New Point(CInt(5 + ButtonSize.Width), y)
    '        Else
    '            _buttPosition.Location = New Point(CInt(Bounds.Width - (dblWinButtonWidth * ButtonSize.Width)), y)
    '        End If

    '        Dim hDC As IntPtr = GetWindowDC(hwnd)
    '        Try
    '            Using g As Graphics = Graphics.FromHdc(hDC)
    '                If _buttState = LanguageButtonState.Normal Then
    '                    If Me.LanguageNormal IsNot Nothing Then
    '                        g.DrawImage(Me.LanguageNormal, x, y, ButtonSize.Width, ButtonSize.Height)
    '                    End If
    '                ElseIf _buttState = LanguageButtonState.Pressed Then
    '                    If Me.LanguagePressed IsNot Nothing Then
    '                        g.DrawImage(Me.LanguagePressed, x, y, ButtonSize.Width, ButtonSize.Height)
    '                    End If
    '                ElseIf _buttState = LanguageButtonState.Hover Then
    '                    If Me.LanguageHover IsNot Nothing Then
    '                        g.DrawImage(Me.LanguageHover, x, y, ButtonSize.Width, ButtonSize.Height)
    '                    End If
    '                ElseIf _buttState = LanguageButtonState.Selected Then
    '                    If Me.LanguageNormal IsNot Nothing Then
    '                        g.DrawImage(Me.LanguageNormal, x, y, ButtonSize.Width, ButtonSize.Height)
    '                    End If
    '                End If
    '            End Using
    '        Catch ex As Exception
    '            MsgBox("1.1" & ex.Message)
    '        End Try
    '        ReleaseDC(hwnd, hDC)
    '    Catch ex As Exception
    '        MsgBox("1" & ex.Message)
    '    End Try
    'End Sub

    'Protected Overloads Overrides Sub WndProc(ByRef m As Message)
    '    Try
    '        If Me.ShowLanguageButton = False Then
    '            MyBase.WndProc(m)
    '            Exit Sub
    '        End If

    '        Dim x As Integer, y As Integer
    '        Dim windowRect As New Rectangle()
    '        GetWindowRect(m.HWnd, windowRect)

    '        Select Case m.Msg
    '            ' WM_NCPAINT
    '            ' WM_PAINT
    '            Case &H85, &HA
    '                MyBase.WndProc(m)

    '                DrawButton(m.HWnd)

    '                m.Result = IntPtr.Zero

    '                Exit Select

    '                ' WM_ACTIVATE
    '            Case &H86
    '                MyBase.WndProc(m)
    '                DrawButton(m.HWnd)

    '                Exit Select

    '                ' WM_NCMOUSEMOVE
    '            Case &HA0
    '                ' Extract the least significant 16 bits
    '                x = (CInt(m.LParam) << 16) >> 16
    '                ' Extract the most significant 16 bits
    '                y = CInt(m.LParam) >> 16

    '                x -= windowRect.Left
    '                y -= windowRect.Top

    '                MyBase.WndProc(m)

    '                If Not _buttPosition.Contains(New Point(x, y)) AndAlso _buttState = LanguageButtonState.Pressed Then
    '                    _buttState = LanguageButtonState.Normal
    '                    DrawButton(m.HWnd)
    '                End If

    '                Exit Select

    '                ' WM_NCLBUTTONDOWN
    '            Case &HA1
    '                ' Extract the least significant 16 bits
    '                x = (CInt(m.LParam) << 16) >> 16
    '                ' Extract the most significant 16 bits
    '                y = CInt(m.LParam) >> 16

    '                x -= windowRect.Left
    '                y -= windowRect.Top

    '                If _buttPosition.Contains(New Point(x, y)) Then
    '                    _buttState = LanguageButtonState.Pressed
    '                    DrawButton(m.HWnd)
    '                Else
    '                    MyBase.WndProc(m)
    '                End If

    '                Exit Select

    '                ' WM_NCLBUTTONUP
    '            Case &HA2
    '                ' Extract the least significant 16 bits
    '                x = (CInt(m.LParam) << 16) >> 16
    '                ' Extract the most significant 16 bits
    '                y = CInt(m.LParam) >> 16

    '                x -= windowRect.Left
    '                y -= windowRect.Top

    '                If _buttPosition.Contains(New Point(x, y)) AndAlso _buttState = LanguageButtonState.Hover Then
    '                    _buttState = LanguageButtonState.Normal
    '                    Call RaiseLanguageButton_Click(New Button, New EventArgs)
    '                    DrawButton(m.HWnd)
    '                Else
    '                    MyBase.WndProc(m)
    '                End If

    '                Exit Select

    '            Case &H84
    '                ' Extract the least significant 16 bits
    '                x = (CInt(m.LParam) << 16) >> 16
    '                ' Extract the most significant 16 bits
    '                y = CInt(m.LParam) >> 16

    '                x -= windowRect.Left
    '                y -= windowRect.Top

    '                If _buttPosition.Contains(New Point(x, y)) Then
    '                    m.Result = CType(18, IntPtr)
    '                    _buttState = LanguageButtonState.Hover
    '                    DrawButton(m.HWnd)
    '                Else
    '                    ' HTBORDER
    '                    _buttState = LanguageButtonState.Normal
    '                    DrawButton(m.HWnd)
    '                    MyBase.WndProc(m)
    '                End If

    '                Exit Select

    '            Case Else
    '                MyBase.WndProc(m)
    '                Exit Select
    '        End Select
    '    Catch ex As Exception
    '        MsgBox("2" & ex.Message)
    '    End Try
    'End Sub

    'Private Sub RaiseLanguageButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    RaiseEvent LanguageClick(sender, e)
    'End Sub

    Public Const WM_SYSCOMMAND As Int32 = &H112
    Public Const MF_SEPARATOR As Int32 = &H800
    Public Const MF_BYPOSITION As Int32 = &H400
    Public Const MF_STRING As Int32 = &H0
    Public Const IDM_LANGUAGE As Int32 = 1000
    Public Const IDM_SEPARATOR As Int32 = 1001

    <Runtime.InteropServices.DllImport("user32.dll")> _
    Private Shared Function GetSystemMenu(ByVal hWnd As IntPtr, ByVal bRevert As Boolean) As IntPtr
    End Function
    <Runtime.InteropServices.DllImport("user32.dll")> _
    Private Shared Function InsertMenu(ByVal hMenu As IntPtr, ByVal wPosition As Int32, ByVal wFlags As Int32, ByVal wIDNewItem As Int32, ByVal lpNewItem As String) As Boolean
    End Function
    <Runtime.InteropServices.DllImport("user32.dll")> _
    Private Shared Function RemoveMenu(ByVal hMenu As Int32, ByVal nPosition As Int32, ByVal wFlags As Int32) As Int32
    End Function

    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = WM_SYSCOMMAND Then
            Select Case m.WParam.ToInt32()
                Case IDM_LANGUAGE
                    RaiseEvent LanguageClick(Me, New EventArgs)
                    Return
                Case Else
            End Select
        End If
        MyBase.WndProc(m)
    End Sub

    Friend Enum enmSelectMenuMethod As Integer
        MF_BYCOMMAND = &H0I
        MF_BYPOSITION = &H400I
    End Enum

    Private Sub RefreshMenu()
        Dim sysMenuHandle As IntPtr = GetSystemMenu(Me.Handle, False)
        If Me.ShowLanguageButton Then
            InsertMenu(sysMenuHandle, 5, MF_BYPOSITION Or MF_SEPARATOR, IDM_SEPARATOR, String.Empty)
            InsertMenu(sysMenuHandle, 6, MF_BYPOSITION, IDM_LANGUAGE, "Edit Language")
        Else
            RemoveMenu(sysMenuHandle, IDM_SEPARATOR, enmSelectMenuMethod.MF_BYCOMMAND)
            RemoveMenu(sysMenuHandle, IDM_LANGUAGE, enmSelectMenuMethod.MF_BYCOMMAND)
        End If
    End Sub
#End Region

#Region " Overrides Events "
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        Me.RefreshMenu()
        '_buttPosition.Size = New Size(SystemInformation.CaptionButtonSize.Width - 4, SystemInformation.CaptionButtonSize.Height - 4) 'New Size(21, 21) ' SystemInformation.CaptionButtonSize
    End Sub

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()
        MyBase.HelpButton = eZeeFrom_Configuration._ShowHelpButton
        Me.KeyPreview = True
    End Sub

    Private Sub eZeeForm_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        mstrForm_Name = Me.Name
    End Sub
#End Region

#Region " Help "
    Private Sub ShowHelp()
        If eZeeFrom_Configuration._ShowHelpButton Then
            If mstrHelpTag <> "" Then
                Help.ShowHelp(Me, eZeeFrom_Configuration._HelpFileName, mstrHelpTag & ".htm")
            Else
                Help.ShowHelp(Me, eZeeFrom_Configuration._HelpFileName, "Help_Not_Found.htm")
            End If
        End If
    End Sub

    Private Sub eZeeForm_HelpButtonClicked(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.HelpButtonClicked
        e.Cancel = True
        Call Me.ShowHelp()
    End Sub

    Private Sub eZeeForm_HelpRequested(ByVal sender As System.Object, ByVal hlpevent As System.Windows.Forms.HelpEventArgs) Handles MyBase.HelpRequested
        Call Me.ShowHelp()
    End Sub
#End Region

End Class

