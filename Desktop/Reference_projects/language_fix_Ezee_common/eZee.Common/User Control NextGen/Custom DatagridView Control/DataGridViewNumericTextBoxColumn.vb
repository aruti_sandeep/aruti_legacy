Imports System
Imports System.Drawing
Imports System.Diagnostics
Imports System.Globalization
Imports System.Windows.Forms
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.IO
Imports System.Text

<ToolboxItem(False)> _
<ToolboxBitmap(GetType(eZee.TextBox.NumericTextBox))> _
Public Class NumericTextBoxEx
    Inherits eZee.TextBox.NumericTextBox
    Private m_backColor As Color

    <DefaultValue(GetType(Color), "Window")> _
    Public Shadows Property BackColor() As Color
        Get
            Return m_backColor
        End Get
        Set(ByVal value As Color)
            If m_backColor <> value Then
                m_backColor = value
                Me.OnBackColorChanged(EventArgs.Empty)
            End If
        End Set
    End Property

    Protected Overloads Overrides Sub OnBackColorChanged(ByVal e As EventArgs)
        MyBase.OnBackColorChanged(e)

        If Me.[ReadOnly] Then
            MyBase.BackColor = m_backColor
        Else
            MyBase.BackColor = m_backColor
        End If
        Me.Invalidate()
    End Sub
End Class

Public Class DataGridViewNumericTextBoxEditingControl
    Inherits NumericTextBoxEx
    Implements IDataGridViewEditingControl

    Private dataGridViewControl As DataGridView
    ' grid owning this editing control
    Private valueIsChanged As Boolean
    ' editing control's value has changed or not
    Private rowIndexNum As Integer
    '  row index in which the editing control resides
    Public Sub New()
        Me.TabStop = False
        ' control must not be part of the tabbing loop
    End Sub

    Public Overridable Property EditingControlDataGridView() As DataGridView Implements System.Windows.Forms.IDataGridViewEditingControl.EditingControlDataGridView
        Get
            Return Me.dataGridViewControl
        End Get
        Set(ByVal value As DataGridView)
            Me.dataGridViewControl = value
        End Set
    End Property

    Public Overridable Property EditingControlFormattedValue() As Object Implements System.Windows.Forms.IDataGridViewEditingControl.EditingControlFormattedValue
        Get
            Return GetEditingControlFormattedValue(DataGridViewDataErrorContexts.Formatting)
        End Get
        Set(ByVal value As Object)
            Me.Text = DirectCast(value, String)
        End Set
    End Property

    Public Overridable Property EditingControlRowIndex() As Integer Implements System.Windows.Forms.IDataGridViewEditingControl.EditingControlRowIndex
        Get
            Return Me.rowIndexNum
        End Get
        Set(ByVal value As Integer)
            Me.rowIndexNum = value
        End Set
    End Property

    Public Overridable Property EditingControlValueChanged() As Boolean Implements System.Windows.Forms.IDataGridViewEditingControl.EditingControlValueChanged
        Get
            Return Me.valueIsChanged
        End Get
        Set(ByVal value As Boolean)
            Me.valueIsChanged = value
        End Set
    End Property

    ''' <summary>
    ''' Property which determines which cursor must be used for the editing panel,
    ''' i.e. the parent of the editing control.
    ''' </summary>
    Public Overridable ReadOnly Property EditingPanelCursor() As Cursor Implements System.Windows.Forms.IDataGridViewEditingControl.EditingPanelCursor
        Get
            Return Cursors.[Default]
        End Get
    End Property

    ''' <summary>
    ''' Property which indicates whether the editing control needs to be repositioned 
    ''' when its value changes.
    ''' </summary>
    Public Overridable ReadOnly Property RepositionEditingControlOnValueChange() As Boolean Implements System.Windows.Forms.IDataGridViewEditingControl.RepositionEditingControlOnValueChange
        Get
            Return False
        End Get
    End Property

    ''' <summary>
    ''' Method called by the grid before the editing control is shown so it can adapt to the 
    ''' provided cell style.
    ''' </summary>
    Public Overridable Sub ApplyCellStyleToEditingControl(ByVal dataGridViewCellStyle As DataGridViewCellStyle) Implements System.Windows.Forms.IDataGridViewEditingControl.ApplyCellStyleToEditingControl
        Me.Font = dataGridViewCellStyle.Font
        Me.ForeColor = dataGridViewCellStyle.ForeColor
        'Me.BorderStyle = Windows.Forms.BorderStyle.None

        If dataGridViewCellStyle.BackColor.A < 255 Then
            ' The NumericUpDown control does not support transparent back colors
            Dim opaqueBackColor As Color = Color.FromArgb(255, dataGridViewCellStyle.BackColor)
            Me.BackColor = opaqueBackColor
            Me.dataGridViewControl.EditingPanel.BackColor = opaqueBackColor
        Else
            Me.BackColor = dataGridViewCellStyle.BackColor

        End If

        Select Case dataGridViewCellStyle.Alignment
            Case DataGridViewContentAlignment.BottomCenter, DataGridViewContentAlignment.MiddleCenter, DataGridViewContentAlignment.TopCenter
                Me.TextAlign = HorizontalAlignment.Center
            Case DataGridViewContentAlignment.BottomLeft, DataGridViewContentAlignment.MiddleLeft, DataGridViewContentAlignment.TopLeft
                Me.TextAlign = HorizontalAlignment.Left
            Case DataGridViewContentAlignment.BottomRight, DataGridViewContentAlignment.MiddleRight, DataGridViewContentAlignment.TopRight
                Me.TextAlign = HorizontalAlignment.Right
        End Select

    End Sub


    ''' <summary>
    ''' Method called by the grid on keystrokes to determine if the editing control is
    ''' interested in the key or not.
    ''' </summary>
    Public Overridable Function EditingControlWantsInputKey(ByVal keyData As Keys, ByVal dataGridViewWantsInputKey As Boolean) As Boolean Implements System.Windows.Forms.IDataGridViewEditingControl.EditingControlWantsInputKey
        Select Case keyData And Keys.KeyCode
            Case Keys.Right, Keys.Left, Keys.Down, Keys.Up, Keys.Home, Keys.[End], _
             Keys.Delete
                Return True
        End Select
        Return Not dataGridViewWantsInputKey
    End Function

    ''' <summary>
    ''' Returns the current value of the editing control.
    ''' </summary>
    Public Overridable Function GetEditingControlFormattedValue(ByVal context As DataGridViewDataErrorContexts) As Object Implements System.Windows.Forms.IDataGridViewEditingControl.GetEditingControlFormattedValue
        Return Me.Text
    End Function

    ''' <summary>
    ''' Called by the grid to give the editing control a chance to prepare itself for
    ''' the editing session.
    ''' </summary>
    Public Overridable Sub PrepareEditingControlForEdit(ByVal selectAll As Boolean) Implements System.Windows.Forms.IDataGridViewEditingControl.PrepareEditingControlForEdit
        If selectAll Then
            Me.SelectAll()
        Else
            Me.SelectionStart = Me.Text.Length
        End If
    End Sub

    ' End of the IDataGridViewEditingControl interface implementation

    ''' <summary>
    ''' Small utility function that updates the local dirty state and 
    ''' notifies the grid of the value change.
    ''' </summary>
    Private Sub NotifyDataGridViewOfValueChange()
        If Not Me.valueIsChanged Then
            Me.valueIsChanged = True
            Me.dataGridViewControl.NotifyCurrentCellDirty(True)
        End If
    End Sub

    ''' <summary>
    ''' Listen to the KeyPress notification to know when the value changed, and 
    ''' notify the grid of the change.
    ''' </summary>
    Protected Overloads Overrides Sub OnKeyPress(ByVal e As KeyPressEventArgs)
        MyBase.OnKeyPress(e)

        ' The value changes when a digit, the decimal separator or the negative sign is pressed.
        Dim notifyValueChange As Boolean = False

        If Char.IsDigit(e.KeyChar) Then
            notifyValueChange = True
        Else
            Dim numberFormatInfo As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
            Dim decimalSeparatorStr As String = numberFormatInfo.NumberDecimalSeparator
            Dim groupSeparatorStr As String = numberFormatInfo.NumberGroupSeparator
            Dim negativeSignStr As String = numberFormatInfo.NegativeSign
            If Not String.IsNullOrEmpty(decimalSeparatorStr) AndAlso decimalSeparatorStr.Length = 1 Then
                notifyValueChange = decimalSeparatorStr(0) = e.KeyChar
            End If
            If Not notifyValueChange AndAlso Not String.IsNullOrEmpty(groupSeparatorStr) AndAlso groupSeparatorStr.Length = 1 Then
                notifyValueChange = groupSeparatorStr(0) = e.KeyChar
            End If
            If Not notifyValueChange AndAlso Not String.IsNullOrEmpty(negativeSignStr) AndAlso negativeSignStr.Length = 1 Then
                notifyValueChange = negativeSignStr(0) = e.KeyChar
            End If
        End If

        If notifyValueChange Then
            ' Let the DataGridView know about the value change
            NotifyDataGridViewOfValueChange()
        End If
    End Sub

    ''' <summary>
    ''' Listen to the ValueChanged notification to forward the change to the grid.
    ''' </summary>
    Protected Overloads Overrides Sub OnTextChanged(ByVal e As EventArgs)
        MyBase.OnTextChanged(e)

        If Me.Focused Then
            ' Let the DataGridView know about the value change
            NotifyDataGridViewOfValueChange()
        End If
    End Sub
End Class

Public Class DataGridViewNumericTextBoxColumn
    Inherits DataGridViewTextBoxColumn
    Private Const m_allowMaxDecimalLength As Integer = 6

    Public Sub New()
        Dim cell As New DataGridViewNumericTextBoxCell()
        MyBase.CellTemplate = cell

        MyBase.SortMode = DataGridViewColumnSortMode.Automatic
        MyBase.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        MyBase.DefaultCellStyle.Format = "F" & Me.DecimalLength.ToString()
    End Sub

    Private ReadOnly Property NumEditCellTemplate() As DataGridViewNumericTextBoxCell
        Get
            Dim cell As DataGridViewNumericTextBoxCell = TryCast(Me.CellTemplate, DataGridViewNumericTextBoxCell)
            If cell Is Nothing Then
                Throw New InvalidOperationException("DataGridViewNumericTextBoxColumn does not have a CellTemplate.")
            End If
            Return cell
        End Get
    End Property

    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)> _
    Public Overloads Overrides Property CellTemplate() As DataGridViewCell
        Get
            Return MyBase.CellTemplate
        End Get
        Set(ByVal value As DataGridViewCell)
            Dim cell As DataGridViewNumericTextBoxCell = TryCast(value, DataGridViewNumericTextBoxCell)
            If value IsNot Nothing AndAlso cell Is Nothing Then
                Throw New InvalidCastException("Value provided for CellTemplate must be of type TEditNumDataGridViewCell or derive from it.")
            End If
            MyBase.CellTemplate = value
        End Set
    End Property

    <Category("Appearance"), DefaultValue(0), Description("The decimal length of cell value.")> _
    Public Property DecimalLength() As Integer
        Get
            Return Me.NumEditCellTemplate.DecimalLength
        End Get
        Set(ByVal value As Integer)
            If value < 0 OrElse value > m_allowMaxDecimalLength Then
                Throw New ArgumentOutOfRangeException("The DecimalLength must be between 0 and " & m_allowMaxDecimalLength.ToString() & ".")
            End If

            MyBase.DefaultCellStyle.Format = "F" & value.ToString()

            Me.NumEditCellTemplate.DecimalLength = value
            If Me.DataGridView IsNot Nothing Then
                ' Update all the existing DataGridViewNumericUpDownCell cells in the column accordingly.
                Dim dataGridViewRows As DataGridViewRowCollection = Me.DataGridView.Rows
                Dim rowCount As Integer = dataGridViewRows.Count
                For rowIndex As Integer = 0 To rowCount - 1
                    ' Be careful not to unshare rows unnecessarily. 
                    ' This could have severe performance repercussions.
                    Dim dataGridViewRow As DataGridViewRow = dataGridViewRows.SharedRow(rowIndex)
                    Dim dataGridViewCell As DataGridViewNumericTextBoxCell = TryCast(dataGridViewRow.Cells(Me.Index), DataGridViewNumericTextBoxCell)
                    If dataGridViewCell IsNot Nothing Then
                        ' Call the internal SetDecimalPlaces method instead of the property to avoid invalidation 
                        ' of each cell. The whole column is invalidated later in a single operation for better performance.
                        dataGridViewCell.SetDecimalLength(rowIndex, value)
                    End If
                Next
                Me.DataGridView.InvalidateColumn(Me.Index)
                ' TODO: Call the grid's autosizing methods to autosize the column, rows, column headers / row headers as needed.
            End If
        End Set
    End Property

    <Category("Appearance"), DefaultValue(True), Description("Whether negative sign is allowed or not.")> _
    Public Property AllowNegative() As Boolean
        Get
            Return Me.NumEditCellTemplate.AllowNegative
        End Get
        Set(ByVal value As Boolean)
            Me.NumEditCellTemplate.AllowNegative = value
            If Me.DataGridView IsNot Nothing Then
                Dim dataGridViewRows As DataGridViewRowCollection = Me.DataGridView.Rows
                Dim rowCount As Integer = dataGridViewRows.Count
                For rowIndex As Integer = 0 To rowCount - 1
                    Dim dataGridViewRow As DataGridViewRow = dataGridViewRows.SharedRow(rowIndex)
                    Dim dataGridViewCell As DataGridViewNumericTextBoxCell = TryCast(dataGridViewRow.Cells(Me.Index), DataGridViewNumericTextBoxCell)
                    If dataGridViewCell IsNot Nothing Then
                        dataGridViewCell.EnableNegative(rowIndex, value)
                    End If
                Next
            End If
        End Set
    End Property

    <Category("Appearance"), DefaultValue(False), Description("Whether show null when value is zero or not.")> _
    Public Property ShowNullWhenZero() As Boolean
        Get
            Return Me.NumEditCellTemplate.ShowNullWhenZero
        End Get
        Set(ByVal value As Boolean)
            Me.NumEditCellTemplate.ShowNullWhenZero = value
            If Me.DataGridView IsNot Nothing Then
                Dim dataGridViewRows As DataGridViewRowCollection = Me.DataGridView.Rows
                Dim rowCount As Integer = dataGridViewRows.Count
                For rowIndex As Integer = 0 To rowCount - 1
                    Dim dataGridViewRow As DataGridViewRow = dataGridViewRows.SharedRow(rowIndex)
                    Dim dataGridViewCell As DataGridViewNumericTextBoxCell = TryCast(dataGridViewRow.Cells(Me.Index), DataGridViewNumericTextBoxCell)
                    If dataGridViewCell IsNot Nothing Then
                        dataGridViewCell.EnableShowNullWhenZero(rowIndex, value)
                    End If
                Next
                Me.DataGridView.InvalidateColumn(Me.Index)
            End If
        End Set
    End Property

    Public Overloads Overrides Function ToString() As String
        Dim sb As New System.Text.StringBuilder(100)
        sb.Append("DataGridViewNumericTextBoxColumn { Name=")
        sb.Append(Me.Name)
        sb.Append(", Index=")
        sb.Append(Me.Index.ToString(CultureInfo.CurrentCulture))
        sb.Append(" }")
        Return sb.ToString()
    End Function
End Class

Public Class DataGridViewNumericTextBoxCell
    Inherits DataGridViewTextBoxCell
    Private m_showNullWhenZero As Boolean = False
    Private m_allowNegative As Boolean = True
    Private m_decimalLength As Integer = 0

    Private Shared defaultEditType As Type = GetType(DataGridViewNumericTextBoxEditingControl)
    Private Shared defaultValueType As Type = GetType(Decimal)

    Public Sub New()
    End Sub

    <DefaultValue(0)> _
    Public Property DecimalLength() As Integer
        Get
            Return m_decimalLength
        End Get
        Set(ByVal value As Integer)
            If m_decimalLength <> value Then
                SetDecimalLength(Me.RowIndex, value)
                OnCommonChange()
                ' Assure that the cell/column gets repainted and autosized if needed
            End If
        End Set
    End Property

    <DefaultValue(True)> _
    Public Property AllowNegative() As Boolean
        Get
            Return m_allowNegative
        End Get
        Set(ByVal value As Boolean)
            If m_allowNegative <> value Then
                EnableNegative(Me.RowIndex, value)
            End If
        End Set
    End Property

    <DefaultValue(False)> _
    Public Property ShowNullWhenZero() As Boolean
        Get
            Return m_showNullWhenZero
        End Get
        Set(ByVal value As Boolean)
            If m_showNullWhenZero <> value Then
                EnableShowNullWhenZero(Me.RowIndex, value)
                OnCommonChange()
            End If
        End Set
    End Property

    Private ReadOnly Property NumericTextBoxEditingControl() As DataGridViewNumericTextBoxEditingControl
        Get
            Return TryCast(Me.DataGridView.EditingControl, DataGridViewNumericTextBoxEditingControl)
        End Get
    End Property

    Public Overloads Overrides ReadOnly Property EditType() As Type
        Get
            Return defaultEditType
        End Get
    End Property

    Public Overloads Overrides ReadOnly Property ValueType() As Type
        Get
            Dim tvalueType As Type = MyBase.ValueType
            If tvalueType IsNot Nothing Then
                Return tvalueType
            End If
            Return defaultValueType
        End Get
    End Property

    ''' <summary>
    ''' If set 0/1.23 to two cells, it will throw Exception when sort by clicking column header.
    ''' Override this method to ensure the type of value.
    ''' </summary>
    Protected Overloads Overrides Function SetValue(ByVal rowIndex As Integer, ByVal value As Object) As Boolean
        Dim val As Decimal = 0D
        Try
            If Not value Is Nothing Then
                val = Math.Round(System.Convert.ToDecimal(value), m_decimalLength)
                Return MyBase.SetValue(rowIndex, val)
            End If
        Catch
        End Try
        Return MyBase.SetValue(rowIndex, Nothing)
        ' if set 0 and 1.23, it will throw exception when sort
    End Function

    Public Overloads Overrides Function Clone() As Object
        Dim dataGridViewCell As DataGridViewNumericTextBoxCell = TryCast(MyBase.Clone(), DataGridViewNumericTextBoxCell)
        If dataGridViewCell IsNot Nothing Then
            dataGridViewCell.DecimalLength = Me.DecimalLength
            dataGridViewCell.AllowNegative = Me.AllowNegative
            dataGridViewCell.ShowNullWhenZero = Me.ShowNullWhenZero
        End If
        Return dataGridViewCell
    End Function

    Public Overloads Overrides Sub InitializeEditingControl(ByVal rowIndex As Integer, ByVal initialFormattedValue As Object, ByVal dataGridViewCellStyle As DataGridViewCellStyle)
        MyBase.InitializeEditingControl(rowIndex, initialFormattedValue, dataGridViewCellStyle)
        Dim numEditBox As NumericTextBoxEx = TryCast(Me.DataGridView.EditingControl, NumericTextBoxEx)

        If numEditBox IsNot Nothing Then
            numEditBox.MaxDecimalPlaces = Me.DecimalLength
            numEditBox.AllowNegative = Me.AllowNegative

            Dim initialFormattedValueStr As String = TryCast(initialFormattedValue, String)

            If String.IsNullOrEmpty(initialFormattedValueStr) Then
                numEditBox.Text = ""
            Else
                numEditBox.Text = initialFormattedValueStr
            End If
            numEditBox.Invalidate()
        End If
    End Sub

    <EditorBrowsable(EditorBrowsableState.Advanced)> _
    Public Overloads Overrides Sub DetachEditingControl()
        Dim dataGridView As DataGridView = Me.DataGridView
        If dataGridView Is Nothing OrElse dataGridView.EditingControl Is Nothing Then
            Throw New InvalidOperationException("Cell is detached or its grid has no editing control.")
        End If

        Dim numEditBox As NumericTextBoxEx = TryCast(dataGridView.EditingControl, NumericTextBoxEx)
        If numEditBox IsNot Nothing Then
            numEditBox.ClearUndo()
            ' avoid interferences between the editing sessions
        End If

        MyBase.DetachEditingControl()
    End Sub

    ''' <summary>
    ''' Consider the decimal in the formatted representation of the cell value.
    ''' </summary>
    Protected Overloads Overrides Function GetFormattedValue(ByVal value As Object, ByVal rowIndex As Integer, ByRef cellStyle As DataGridViewCellStyle, ByVal valueTypeConverter As TypeConverter, ByVal formattedValueTypeConverter As TypeConverter, ByVal context As DataGridViewDataErrorContexts) As Object
        Dim baseFormattedValue As Object = MyBase.GetFormattedValue(value, rowIndex, cellStyle, valueTypeConverter, formattedValueTypeConverter, context)
        Dim formattedText As String = TryCast(baseFormattedValue, String)

        If value Is Nothing OrElse String.IsNullOrEmpty(formattedText) Then
            Return baseFormattedValue
        End If

        Dim unformattedDecimal As Decimal
        ' 123.1 to "123.1"
        Dim formattedDecimal As Decimal
        ' 123.1 to "123.12" if DecimalLength is 2
        Try
            unformattedDecimal = System.Convert.ToDecimal(value)
            ' 123.1 to "123.1"
            formattedDecimal = System.Convert.ToDecimal(formattedText)
            ' 123.1 to "123.12" if DecimalLength is 2
        Catch ex As Exception
        End Try

        If unformattedDecimal = 0D AndAlso m_showNullWhenZero Then
            Return MyBase.GetFormattedValue(Nothing, rowIndex, cellStyle, valueTypeConverter, formattedValueTypeConverter, context)
        End If

        If unformattedDecimal = formattedDecimal Then
            Return formattedDecimal.ToString("F" & m_decimalLength.ToString())
        End If
        Return formattedText
    End Function

    Private Sub OnCommonChange()
        If Me.DataGridView IsNot Nothing AndAlso Not Me.DataGridView.IsDisposed AndAlso Not Me.DataGridView.Disposing Then
            If Me.RowIndex = -1 Then
                Me.DataGridView.InvalidateColumn(Me.ColumnIndex)
            Else
                Me.DataGridView.UpdateCellValue(Me.ColumnIndex, Me.RowIndex)
            End If
        End If
    End Sub

    Private Function OwnsEditingControl(ByVal rowIndex As Integer) As Boolean
        If rowIndex = -1 OrElse Me.DataGridView Is Nothing Then
            Return False
        End If
        Dim editingControl As DataGridViewNumericTextBoxEditingControl = TryCast(Me.DataGridView.EditingControl, DataGridViewNumericTextBoxEditingControl)
        Return editingControl IsNot Nothing AndAlso rowIndex = DirectCast(editingControl, IDataGridViewEditingControl).EditingControlRowIndex
    End Function

    Friend Sub SetDecimalLength(ByVal rowIndex As Integer, ByVal value As Integer)
        m_decimalLength = value
        If OwnsEditingControl(rowIndex) Then
            Me.NumericTextBoxEditingControl.MaxDecimalPlaces = value
        End If
    End Sub

    Friend Sub EnableNegative(ByVal rowIndex As Integer, ByVal value As Boolean)
        m_allowNegative = value
        If OwnsEditingControl(rowIndex) Then
            Me.NumericTextBoxEditingControl.AllowNegative = value
        End If
    End Sub

    Friend Sub EnableShowNullWhenZero(ByVal rowIndex As Integer, ByVal value As Boolean)
        m_showNullWhenZero = value
    End Sub

    Public Overloads Overrides Function ToString() As String
        Return ("DataGridViewNumericTextBoxCell { ColIndex=" & ColumnIndex.ToString(CultureInfo.CurrentCulture) & ", RowIndex=") + RowIndex.ToString(CultureInfo.CurrentCulture) & " }"
    End Function

End Class


'    