<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeListBox
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                For iCnt As Integer = 0 To pnlMain.Controls.Count - 1
                    RemoveHandler pnlMain.Controls(0).Click, AddressOf btn_Click
                Next

                If _HoveringTimer IsNot Nothing Then
                    _HoveringTimer.Stop()
                    _HoveringTimer.Dispose()
                End If

                If objcbodata IsNot Nothing Then
                    objcbodata.DataSource = Nothing
                    objcbodata.Dispose()
                End If

                If components IsNot Nothing Then
                    components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.btnPrevious = New eZee.Common.eZeeButton
        Me.btnNext = New eZee.Common.eZeeButton
        Me.pnlMain = New System.Windows.Forms.Panel
        Me._HoveringTimer = New System.Windows.Forms.Timer(Me.components)
        Me.objcbodata = New System.Windows.Forms.ComboBox
        Me.SuspendLayout()
        '
        'btnPrevious
        '
        Me.btnPrevious.InnerBorderColor = System.Drawing.Color.Transparent
        Me.btnPrevious.Location = New System.Drawing.Point(3, 3)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(65, 53)
        Me.btnPrevious.TabIndex = 2
        Me.btnPrevious.TabStop = False
        '
        'btnNext
        '
        Me.btnNext.InnerBorderColor = System.Drawing.Color.Transparent
        Me.btnNext.Location = New System.Drawing.Point(74, 3)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(65, 53)
        Me.btnNext.TabIndex = 1
        Me.btnNext.TabStop = False
        '
        'pnlMain
        '
        Me.pnlMain.Location = New System.Drawing.Point(3, 62)
        Me.pnlMain.Name = "pnlMain"
        Me.pnlMain.Size = New System.Drawing.Size(136, 131)
        Me.pnlMain.TabIndex = 3
        '
        '_HoveringTimer
        '
        Me._HoveringTimer.Interval = 500
        '
        'objcbodata
        '
        Me.objcbodata.FormattingEnabled = True
        Me.objcbodata.Location = New System.Drawing.Point(3, 199)
        Me.objcbodata.Name = "objcbodata"
        Me.objcbodata.Size = New System.Drawing.Size(136, 21)
        Me.objcbodata.TabIndex = 4
        Me.objcbodata.Visible = False
        '
        'eZeeListBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.objcbodata)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.btnPrevious)
        Me.Controls.Add(Me.btnNext)
        Me.DoubleBuffered = True
        Me.Name = "eZeeListBox"
        Me.Size = New System.Drawing.Size(149, 225)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents btnPrevious As eZeeButton
    Private WithEvents btnNext As eZeeButton
    Private pnlMain As System.Windows.Forms.Panel
    Private WithEvents _HoveringTimer As System.Windows.Forms.Timer
    Private objcbodata As System.Windows.Forms.ComboBox

End Class
