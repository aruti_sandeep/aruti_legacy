Imports System
Imports System.Collections
Imports System.Reflection
Imports System.Runtime.CompilerServices

Public Class BG_ButtonCollection
    Inherits CollectionBase

    Friend Event RefreshKeyRequired As RefreshKeyRequiredEventHandler

    Public Sub Add(ByVal Key As BG_Button)
        AddHandler Key.RefreshKeyRequired, AddressOf KeyDescriptor_RefreshKeyRequired
        'Dim intLst As Integer = MyBase.List.Count - 1
        'If intLst > -1 Then
        '    Key.BackColor = MyBase.List(intLst).BackColor
        '    Key.ForeColor = MyBase.List(intLst).ForeColor
        '    Key.SelectedBackColor = MyBase.List(intLst).SelectedBackColor
        '    Key.SelectedForeColor = MyBase.List(intLst).SelectedForeColor
        '    Key.GradientBackColor = MyBase.List(intLst).GradientBackColor
        '    Key.BorderColor = MyBase.List(intLst).BorderColor
        '    Key.BorderWidth = MyBase.List(intLst).BorderWidth
        '    Key.Bounds = New Rectangle(MyBase.List(intLst).Bounds.X + MyBase.List(intLst).Bounds.Width, _
        '        MyBase.List(intLst).Bounds.Y, _
        '        MyBase.List(intLst).Bounds.Width, _
        '        MyBase.List(intLst).Bounds.Height)

        '    Key.Font = MyBase.List(intLst).Font
        '    Key.TextAlignment = MyBase.List(intLst).TextAlignment

        '    Key.BackColor = MyBase.List(intLst).BackColor
        'End If
        MyBase.List.Add(Key)
    End Sub

    Public Function IndexOf(ByVal Key As BG_Button)
        Return MyBase.List.IndexOf(Key)
    End Function

    Public Sub Remove(ByVal Idx As Integer)
        If (Idx > (MyBase.Count - 1)) OrElse (Idx < 0) Then
            Throw New IndexOutOfRangeException()
        End If
        If (Not MyBase.List(Idx) Is Nothing) AndAlso (TypeOf MyBase.List(Idx) Is BG_Button) Then
            Dim descriptor As BG_Button = CType(IIf(TypeOf MyBase.List(Idx) Is BG_Button, MyBase.List(Idx), Nothing), BG_Button)
            If Not descriptor Is Nothing Then
                RemoveHandler descriptor.RefreshKeyRequired, AddressOf KeyDescriptor_RefreshKeyRequired
            End If
        End If
        MyBase.List.RemoveAt(Idx)
    End Sub

    Default Public Property Item(ByVal index As Integer) As BG_Button
        Get
            Return CType(MyBase.List(index), BG_Button)
        End Get
        Set(ByVal value As BG_Button)
            MyBase.List(index) = value
        End Set
    End Property

    Private Sub KeyDescriptor_RefreshKeyRequired(ByVal sender As Object, ByVal e As BG_ButtonEventArgs)
        Me.OnRefreshKeyRequired(e)
    End Sub

    Protected Overridable Sub OnRefreshKeyRequired(ByVal anArgs As BG_ButtonEventArgs)
        If Not Me.RefreshKeyRequiredEvent Is Nothing Then
            RaiseEvent RefreshKeyRequired(Me, anArgs)
        End If
    End Sub

    'Public Overloads ReadOnly Property Count() As Integer
    '    Get
    '        Return MyBase.List.Count
    '    End Get
    'End Property
End Class
