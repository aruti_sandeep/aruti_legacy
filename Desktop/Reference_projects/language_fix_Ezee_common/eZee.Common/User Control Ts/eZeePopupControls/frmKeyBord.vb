'************************************************************************************************************************************
'Class Name : frmKeyBord
'Purpose    : 
'Written By : 
'Modified   : Naimish
'************************************************************************************************************************************
Imports System.Windows.Forms


Public Class frmKeyBord
    Private Readonly mstrModuleName as  String  = "frmKeyBord"

    Dim mblnCancel As Boolean = True
    Dim _strText As String = ""
    Dim _blnIsPassword As Boolean = False

    Dim m_strTitle As String = "Enter Text"
    Dim m_strOkCaption As String = "Finish"
    Dim m_strCancelCaption As String = "Cancel"

#Region " Properties "
    Public Property _Text() As String
        Get
            Return _strText
        End Get
        Set(ByVal value As String)
            _strText = value
        End Set
    End Property

    Public Property _Caption() As String
        Get
            Return m_strTitle
        End Get
        Set(ByVal value As String)
            m_strTitle = value
        End Set
    End Property

    Public Property _Ok_Caption() As String
        Get
            Return m_strOkCaption
        End Get
        Set(ByVal value As String)
            m_strOkCaption = value
        End Set
    End Property

    Public Property _Cancel_Caption() As String
        Get
            Return m_strCancelCaption
        End Get
        Set(ByVal value As String)
            m_strCancelCaption = value
        End Set
    End Property

    Public Property IsPassword() As Boolean
        Get
            Return _blnIsPassword
        End Get
        Set(ByVal value As Boolean)
            _blnIsPassword = value
        End Set
    End Property
#End Region

#Region " Public Method "
    Public Overloads Function DisplayDialog() As Boolean

        Try
            Me.ShowDialog()

            Return Not mblnCancel

        Catch ex As Exception
            Throw ex
            Return Text
        End Try
    End Function
#End Region

#Region " Form "
    Private Sub frmKeyBord_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Me.Text = m_strTitle
            btnCancel.Text = m_strCancelCaption
            btnFinish.Text = m_strOkCaption

            If _blnIsPassword Then
                txtKeyBoardText.Multiline = False
                txtKeyBoardText.PasswordChar = "#"
            Else
                txtKeyBoardText.Multiline = True
                txtKeyBoardText.PasswordChar = ""
            End If

            txtKeyBoardText.Text = _strText

            txtKeyBoardText.Focus()

            txtKeyBoardText.SelectionStart = 0
            txtKeyBoardText.SelectionLength = Len(txtKeyBoardText.Text)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region " Buttons "
    Private Sub btnStandard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStandard.Click
        Try
            ezKbMain.KeyboardType = eZeeKeyBoardLayOut.KeyBoard_Standard
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnAlphabetical_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlphabetical.Click
        Try
            ezKbMain.KeyboardType = eZeeKeyBoardLayOut.KeyBoard_Alphabetical
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        Try
            _strText = txtKeyBoardText.Text
            mblnCancel = False
            Me.Close()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region " Controls "
    Private Sub ezKbMain_UserKeyPressed(ByVal sender As Object, ByVal e As eZeeTsKeyboardEventArgs) Handles ezKbMain.UserKeyPressed
        Try
            txtKeyBoardText.Focus()
            SendKeys.Send(e.KeyboardKeyPressed)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

   
End Class