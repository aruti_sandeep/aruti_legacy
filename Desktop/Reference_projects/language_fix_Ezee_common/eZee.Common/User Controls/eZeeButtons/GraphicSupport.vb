Imports System.Drawing
Imports System.Windows.Forms

Friend Class GS 'Graphic Support

#Region "Shared Method XRectangleF"
    Public Shared Function XRectangleF(ByVal x As Single, ByVal y As Single, ByVal width As Single, ByVal height As Single) As RectangleF
        If width < 1 Then width = 1
        If height < 1 Then height = 1
        Return New RectangleF(x, y, width, height)
    End Function
#End Region

#Region "Shared Method XRectangle"
    Public Shared Function XRectangle(ByVal x As Integer, ByVal y As Integer, ByVal width As Integer, ByVal height As Integer) As Rectangle
        If width < 1 Then width = 1
        If height < 1 Then height = 1
        Return New Rectangle(x, y, width, height)
    End Function
#End Region

#Region "Shared Method PaintGradientRectangle"
    Public Shared Function PaintGradientRectangle(ByVal g As Drawing.Graphics, _
                           ByVal aPointX As Integer, _
                           ByVal aPointY As Integer, _
                           ByVal aWidth As Integer, _
                           ByVal aHeight As Integer, _
                           ByVal aColor1 As Color, _
                           ByVal aColor2 As Color, _
                           ByVal aGradientMode As Drawing2D.LinearGradientMode) As RectangleF

        Dim RectanglePaint As RectangleF = GS.XRectangleF(aPointX, aPointY, aWidth, aHeight)
        Dim gradBrush As New System.Drawing.Drawing2D.LinearGradientBrush(RectanglePaint, aColor1, aColor2, aGradientMode)
        g.FillRectangle(gradBrush, RectanglePaint)
        Return RectanglePaint

    End Function
#End Region

#Region "Shared Method PaintBorder"
    Public Shared Sub PaintBorder(ByVal g As Drawing.Graphics, _
                                  ByVal aPointX As Integer, _
                                  ByVal aPointY As Integer, _
                                  ByVal aWidth As Integer, _
                                  ByVal aHeight As Integer, _
                                  ByVal aColor As Color)

        Dim BorderPen As New Pen(New SolidBrush(aColor), 1)
        g.DrawRectangle(BorderPen, GS.XRectangle(aPointX, aPointY, aWidth - 1, aHeight - 1))
    End Sub
#End Region

#Region "Shared Method PaintWatermark"
    Friend Shared Sub PaintWatermark(ByVal g As Graphics, _
                                     ByVal aImage As Image, _
                                     ByVal aWatermarkArea As Rectangle, _
                                     ByVal aWatermarkSize As Size, _
                                     ByVal aWatermarkPosition As ContentAlignment)

        If aImage Is Nothing Then Return
        If aWatermarkSize.IsEmpty Then aWatermarkSize = aImage.Size

        Dim p As New Point
        p = GS.GetPointAligned(aWatermarkSize, aWatermarkArea.Location, aWatermarkArea.Size, aWatermarkPosition)

        Dim mWatermarkRectangle As Rectangle = GS.XRectangle(p.X, p.Y, _
                                                             aWatermarkSize.Width, aWatermarkSize.Height)

        Dim imgAttributes As Imaging.ImageAttributes = GetMatrix()
        g.DrawImage(aImage, mWatermarkRectangle, _
                    0, 0, aImage.Width, aImage.Height, _
                    GraphicsUnit.Pixel, imgAttributes)

        imgAttributes.Dispose()

    End Sub
#End Region

#Region " Shared Method GetMatrix "
    Private Shared Function GetMatrix() As Imaging.ImageAttributes
        Dim mColorMatrix As Imaging.ColorMatrix = New Imaging.ColorMatrix
        mColorMatrix.Matrix33 = 0.12F
        mColorMatrix.Matrix00 = 1 / 3.0F
        mColorMatrix.Matrix01 = 1 / 3.0F
        mColorMatrix.Matrix02 = 1 / 3.0F
        mColorMatrix.Matrix10 = 1 / 3.0F
        mColorMatrix.Matrix11 = 1 / 3.0F
        mColorMatrix.Matrix12 = 1 / 3.0F
        mColorMatrix.Matrix20 = 1 / 3.0F
        mColorMatrix.Matrix21 = 1 / 3.0F
        mColorMatrix.Matrix22 = 1 / 3.0F
        Dim imgAttributes As New Imaging.ImageAttributes
        imgAttributes.SetColorMatrix(mColorMatrix, Imaging.ColorMatrixFlag.Default, Imaging.ColorAdjustType.Bitmap)
        Return imgAttributes
    End Function
#End Region

#Region "Shared Method ImageContentAlign"
    Public Overloads Shared Function GetPointAligned(ByVal aObjectSize As Size, _
                                                     ByVal aPositionInsideContainer As Point, _
                                                     ByVal aContainerSize As Size, _
                                                     ByVal aMarginLeft As Integer, _
                                                     ByVal aMarginRight As Integer, _
                                                     ByVal aMarginTop As Integer, _
                                                     ByVal aMarginBottom As Integer, _
                                                     ByVal aAlign As ContentAlignment) As Point

        If aObjectSize.Height = 0 AndAlso aObjectSize.Width = 0 Then Return New Point(0, 0)

        Dim X As Integer = 0
        Dim Y As Integer = 0

        Select Case aAlign
            Case ContentAlignment.BottomCenter
                X = Convert.ToInt32((aContainerSize.Width / 2) - (aObjectSize.Width / 2))
                Y = aContainerSize.Height - aObjectSize.Height - aMarginBottom
            Case ContentAlignment.BottomLeft
                X = 0 + aMarginLeft
                Y = aContainerSize.Height - aObjectSize.Height - aMarginBottom
            Case ContentAlignment.BottomRight
                X = aContainerSize.Width - aObjectSize.Width - aMarginRight
                Y = aContainerSize.Height - aObjectSize.Height - aMarginBottom
            Case ContentAlignment.MiddleCenter
                X = Convert.ToInt32((aContainerSize.Width / 2) - (aObjectSize.Width / 2))
                Y = Convert.ToInt32((aContainerSize.Height / 2) - (aObjectSize.Height / 2))
            Case ContentAlignment.MiddleLeft
                X = 0 + aMarginLeft
                Y = Convert.ToInt32((aContainerSize.Height / 2) - (aObjectSize.Height / 2))
            Case ContentAlignment.MiddleRight
                X = aContainerSize.Width - aObjectSize.Width - aMarginRight
                Y = Convert.ToInt32((aContainerSize.Height / 2) - (aObjectSize.Height / 2))
            Case ContentAlignment.TopCenter
                X = Convert.ToInt32((aContainerSize.Width / 2) - (aObjectSize.Width / 2))
                Y = 0 + aMarginTop
            Case ContentAlignment.TopLeft
                X = 0 + aMarginLeft
                Y = 0 + aMarginTop
            Case ContentAlignment.TopRight
                X = aContainerSize.Width - aObjectSize.Width - aMarginRight
                Y = 0 + aMarginTop
        End Select

        Return New Point(X + aPositionInsideContainer.X, Y + aPositionInsideContainer.Y)

    End Function

    Public Overloads Shared Function GetPointAligned(ByVal sObjectSize As Size, _
                                                     ByVal aPositionInsideContainer As Point, _
                                                     ByVal aContainerSize As Size, _
                                                     ByVal aAlign As ContentAlignment) As Point
        Return GetPointAligned(sObjectSize, aPositionInsideContainer, aContainerSize, 0, 0, 0, 0, aAlign)
    End Function

#End Region

#Region "Shared Method WriteText"
    Friend Shared Sub WriteText(ByVal g As Drawing.Graphics, _
                                     ByVal aText As String, _
                                     ByVal aFont As Font, _
                                     ByVal aEnabled As Boolean, _
                                     ByVal aContentAlignment As System.Drawing.ContentAlignment, _
                                     ByVal aTextQuality As eZeeGradientButtonEnums.TextRendering, _
                                     ByVal aSolidBrushColor As Color, _
                                     ByVal aPointX As Integer, _
                                     ByVal aPointY As Integer, _
                                     ByVal aWidth As Integer, _
                                     ByVal aHeight As Integer)

        Dim StrFormat As New System.Drawing.StringFormat

        Select Case aContentAlignment
            Case ContentAlignment.BottomCenter
                StrFormat.LineAlignment = StringAlignment.Far
                StrFormat.Alignment = StringAlignment.Center
            Case ContentAlignment.BottomLeft
                StrFormat.LineAlignment = StringAlignment.Far
                StrFormat.Alignment = StringAlignment.Near
            Case ContentAlignment.BottomRight
                StrFormat.LineAlignment = StringAlignment.Far
                StrFormat.Alignment = StringAlignment.Far

            Case ContentAlignment.MiddleCenter
                StrFormat.LineAlignment = StringAlignment.Center
                StrFormat.Alignment = StringAlignment.Center
            Case ContentAlignment.MiddleLeft
                StrFormat.LineAlignment = StringAlignment.Center
                StrFormat.Alignment = StringAlignment.Near
            Case ContentAlignment.MiddleRight
                StrFormat.LineAlignment = StringAlignment.Center
                StrFormat.Alignment = StringAlignment.Far

            Case ContentAlignment.TopCenter
                StrFormat.LineAlignment = StringAlignment.Near
                StrFormat.Alignment = StringAlignment.Center
            Case ContentAlignment.TopLeft
                StrFormat.LineAlignment = StringAlignment.Near
                StrFormat.Alignment = StringAlignment.Near
            Case ContentAlignment.TopRight
                StrFormat.LineAlignment = StringAlignment.Near
                StrFormat.Alignment = StringAlignment.Far
        End Select

        Select Case aTextQuality
            Case eZeeGradientButtonEnums.TextRendering.SystemDefault
                g.TextRenderingHint = Drawing.Text.TextRenderingHint.SystemDefault
            Case eZeeGradientButtonEnums.TextRendering.Antialias
                g.TextRenderingHint = Drawing.Text.TextRenderingHint.AntiAlias
            Case eZeeGradientButtonEnums.TextRendering.ClearType
                g.TextRenderingHint = Drawing.Text.TextRenderingHint.ClearTypeGridFit
        End Select

        If aEnabled Then
            g.DrawString(aText, _
                         aFont, _
                         New SolidBrush(aSolidBrushColor), _
                         GS.XRectangleF(aPointX, aPointY, aWidth, aHeight), _
                         StrFormat)
        Else
            ControlPaint.DrawStringDisabled(g, _
                                            aText, aFont, _
                                            SystemColors.Control, _
                                            GS.XRectangleF(aPointX, aPointY, aWidth, aHeight), _
                                            StrFormat)
        End If


    End Sub
#End Region

#Region " Friend Shared GetDisabledImage "
    Friend Shared Function GetDisabledImage(ByVal aImage As Image) As Image
        If (aImage Is Nothing) Then Return aImage
        Dim disabledImage As Image = New Bitmap(aImage.Width, aImage.Height, Imaging.PixelFormat.Format32bppArgb)
        Dim g As Graphics = Graphics.FromImage(disabledImage)
        ControlPaint.DrawImageDisabled(g, aImage, 0, 0, Color.White)
        g.Dispose()
        Return disabledImage
    End Function
#End Region


End Class

