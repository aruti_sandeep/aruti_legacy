using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

using Microsoft.SqlServer;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;

using System.Diagnostics;
using Microsoft.Win32;

namespace sqlExpressSetup
{
    public partial class Form1 : Form
    {
        bool blnState = true;
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {

            System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(thProcess));
            t.Start();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void thProcess()
        {
            String strAppPath;
            strAppPath = null;
            try
            {
                //this.Text = "Installing SQL Server 2005 Express Edition";

                //this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                //Process objProcess = new Process();
                //objProcess.StartInfo.FileName = "regsvr32";
                //objProcess.StartInfo.Arguments = "C:\\WINDOWS\\system32\\SQL_DMO.dll";
                //objProcess.Start( );

                //RegistryKey Key = Registry.LocalMachine.CreateSubKey("Software\\ezee\\eZeeNextGen\\");
                //Key.SetValue("apppath", "C:\\Program Files\\eZee\\eZee FrontDesk NextGen\\");
                strAppPath = Path.GetDirectoryName(Application.ExecutablePath);

                Process objProcess = new Process();
                objProcess.StartInfo.FileName = strAppPath + "\\7z.exe";
                objProcess.StartInfo.Arguments = "x -r sqlexpr.7z";
                objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                objProcess.Start();
                objProcess.WaitForExit();
                objProcess = null;

                clsSqlExpress osqlExpress = new clsSqlExpress();
                osqlExpress.InstanceName = textBox1.Text;
                //osqlExpress.InstanceName = "Express_HRD";
                osqlExpress.ReportErrors = true;

                //osqlExpress.SetupFileLocation = strAppPath + "\\sqlexpr.exe";  //Provide location for the Express setup file
                osqlExpress.SetupFileLocation = strAppPath + "\\SQLEXPR\\setup.exe";  //Provide location for the Express setup file
                //osqlExpress.SetupFileLocation = 
                osqlExpress.SqlBrowserAccountName = ""; //Blank means LocalSystem
                osqlExpress.SqlBrowserPassword = ""; // N/A
                //osqlExpress.SqlDataDirectory = "F:\\Program Files\\Microsoft SQL Server\\";
                //osqlExpress.SqlInstallDirectory = "C:\\Program Files\\";
                //osqlExpress.SqlInstallSharedDirectory = "C:\\Program Files\\";
                osqlExpress.SqlServiceAccountName = ""; //Blank means Localsystem
                osqlExpress.SqlServicePassword = ""; // N/A
                osqlExpress.SAPassword = textBox2.Text; //<<Supply a secure sysadmin password>>
                osqlExpress.UseSQLSecurityMode = true;
                osqlExpress.Collation = "SQL_Latin1_General_CP1_CI_AS";
                osqlExpress.DisableNetworkProtocols = false;

                if (osqlExpress.IsExpressInstalled() == true)
                {
                    //this.Cursor = System.Windows.Forms.Cursors.Default;
                    //MessageBox.Show("Instance Already exists");
                    //this.Close();
                    blnState = false;
                    return;
                }

                if (osqlExpress.InstallExpress())
                {
                    if (osqlExpress.IsExpressInstalled() == false)
                    {
                        //this.Cursor = System.Windows.Forms.Cursors.Default;
                        MessageBox.Show("Instance can't create please contact eZee Support Team.");
                        //this.Close();
                        blnState = false;
                        return;
                    }

                    //this.Text = "Installing Database Sript";

                    string strConnection;
                    /*strConnection = "Data Source=(Local)\\" & textBox1.Text & ";" +
                                    "Initial Catalog = MASTER;" +
                                    "Persist Security Info=True;" +
                                    "User ID = sa;" +
                                    "Password=" & textBox2.Text & "";*/
                    strConnection = "Data Source=(Local)\\" + textBox1.Text + ";" +
                                    "Persist Security Info=True;" +
                                    "User ID = sa;" +
                                    "Password=" + textBox2.Text + "";


                    //Process objProcess = new Process();
                    //objProcess.StartInfo.FileName = strAppPath + "\\SQL_SMO.dll";
                    //objProcess.Start();
                    //objProcess.WaitForExit();

                    //SqlConnection cn = new SqlConnection(strConnection);

                    //SqlConnection conn = new SqlConnection(strConnection);

                    //Server server = new Server(new ServerConnection(conn));

                    //server.ConnectionContext.ExecuteNonQuery(strScript);
                }
                else
                {
                    //this.Cursor = System.Windows.Forms.Cursors.Default;
                    MessageBox.Show("Please contact eZee Support Team.");
                }

                //this.Cursor = System.Windows.Forms.Cursors.Default;
                //this.Close();
                blnState = false;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                //this.Close();
                blnState = false;
            }
            finally
            {
                blnState = false;
                //System.IO.Directory.Delete(strAppPath+ "\\SQLEXPR");
            }
        }

        private void tmrtimer1_Tick(object sender, EventArgs e)
        {
            pgbInstall.Refresh(); 
            if (!blnState) 
                this.Close();

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = blnState; 
        }

                


    }
}