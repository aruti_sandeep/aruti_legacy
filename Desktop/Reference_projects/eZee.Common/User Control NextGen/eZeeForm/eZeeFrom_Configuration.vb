Public Class eZeeFrom_Configuration
    Private Shared mblnShowHelpButton As Boolean = False
    Public Shared ReadOnly Property _ShowHelpButton() As Boolean
        Get
            Return mblnShowHelpButton
        End Get
    End Property

    Private Shared mstrHelpFileName As String = ""
    Public Shared Property _HelpFileName() As String
        Get
            Return mstrHelpFileName
        End Get
        Set(ByVal value As String)
            mstrHelpFileName = value
            mblnShowHelpButton = System.IO.File.Exists(mstrHelpFileName)
        End Set
    End Property
End Class
