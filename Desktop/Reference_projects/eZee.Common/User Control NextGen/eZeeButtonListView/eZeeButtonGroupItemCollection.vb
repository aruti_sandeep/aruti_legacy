Public Class eZeeButtonGroupItemCollection
    Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList

    Private m_Group As eZeeButtonGroup
    Private m_Items As ArrayList

    Public Sub New(ByVal group As eZeeButtonGroup)
        Me.m_Group = group
    End Sub

    Private Sub CheckListViewItem(ByVal item As eZeeButtonItem)
        If ((Not item.Owner Is Nothing) AndAlso (Not item.Owner Is Me.m_Group.eZeeButtonListView)) Then
            Throw New ArgumentException("item")
        End If
    End Sub

    Public Function Add(ByVal Item As eZeeButtonItem) As eZeeButtonItem Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.Add
        Me.CheckListViewItem(Item)
        Me.MoveToGroup(Item, Me.m_Group)
        Me.m_Items.Add(Item)
        Return Item
    End Function

    Private Sub MoveToGroup(ByVal item As eZeeButtonItem, ByVal newGroup As eZeeButtonGroup)
        Dim group As eZeeButtonGroup = item.Group
        If (Not group Is newGroup) Then
            item.Group = newGroup
            If (Not group Is Nothing) Then
                group.Items.Remove(item)
            End If
        End If
    End Sub

    Public Sub AddRange(ByVal items() As eZeeButtonItem) Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.AddRange
        Dim i As Integer
        For i = 0 To items.Length - 1
            Me.CheckListViewItem(items(i))
        Next i
        Me.m_Items.AddRange(items)
        Dim j As Integer
        For j = 0 To items.Length - 1
            Me.MoveToGroup(items(j), Me.m_Group)
        Next j
    End Sub

    Public Sub Clear() Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.Clear
        Dim i As Integer
        For i = 0 To Me.Count - 1
            Me.MoveToGroup(Me.Item(i), Nothing)
        Next i
        Me.m_Items.Clear()
    End Sub

    Public Function Contains(ByVal item As eZeeButtonItem) As Boolean Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.Contains
        Return Me.m_Items.Contains(item)
    End Function

    <System.ComponentModel.EditorBrowsable(ComponentModel.EditorBrowsableState.Never)> _
    Public Function Contains(ByVal ItemId As Integer) As Boolean Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.Contains
        Return True
    End Function

    Public Sub CopyTo(ByVal dest As System.Array, ByVal index As Integer) Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.CopyTo
        Me.m_Items.CopyTo(dest, index)
    End Sub

    Public ReadOnly Property Count() As Integer Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.Count
        Get
            Return Me.Items.Count
        End Get
    End Property

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.GetEnumerator
        Return Me.Items.GetEnumerator
    End Function

    Public Function IndexOf(ByVal item As eZeeButtonItem) As Integer Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.IndexOf
        Return Me.Items.IndexOf(item)
    End Function

    Public Function Insert(ByVal index As Integer, ByVal item As eZeeButtonItem) As eZeeButtonItem Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.Insert
        Return item
    End Function

    Default Public Property Item(ByVal index As Integer) As eZeeButtonItem Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.Item
        Get
            Return DirectCast(Me.Items.Item(index), eZeeButtonItem)
        End Get
        Set(ByVal value As eZeeButtonItem)
            If (Not value Is Me.Items.Item(index)) Then
                Me.MoveToGroup(DirectCast(Me.Items.Item(index), eZeeButtonItem), Nothing)
                Me.Items.Item(index) = value
                Me.MoveToGroup(DirectCast(Me.Items.Item(index), eZeeButtonItem), Me.m_Group)
            End If
        End Set
    End Property

    Private ReadOnly Property Items() As ArrayList
        Get
            If (Me.m_Items Is Nothing) Then
                Me.m_Items = New ArrayList
            End If
            Return Me.m_Items
        End Get
    End Property

    Public ReadOnly Property OwnerIsDesignMode() As Boolean Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.OwnerIsDesignMode
        Get
            If (Me.m_Group.eZeeButtonListView Is Nothing) Then
                Return False
            End If
            Dim site As System.ComponentModel.ISite = Me.m_Group.eZeeButtonListView.Site
            Return ((Not site Is Nothing) AndAlso site.DesignMode)
        End Get
    End Property

    Public Sub Remove(ByVal item As eZeeButtonItem) Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.Remove
        Me.Items.Remove(item)
        If (item.Group Is Me.m_Group) Then
            item.Group = Nothing
        End If
    End Sub

    Public Sub RemoveAt(ByVal index As Integer) Implements eZeeButtonListView.eZeeButtonItemCollection.IInnerList.RemoveAt
        Me.Remove(Me.Item(index))
    End Sub

    
End Class
