
'<Serializable()> _
Public Class eZeeButtonListView
    Inherits System.Windows.Forms.FlowLayoutPanel

    Private itemcount As Integer
    Private groupcount As Integer
    Private mblnCollapseOnFill As Boolean
    Private mintSelectedValue As Integer
    Private mintSelectedCount As Integer
    Private mstrSelectedText As String
    Private m_SelectedItem As eZeeButtonItem
    Private mblnIsAddBegin As Boolean
    Private mblnShowHeader As Boolean

    'Header Variables 
    Private header As eZeeButtonListViewHeader
    Private mstrHeaderText As String = "Header Text"
    Private mstrAddNewText As String = "Add New Item"
    Private mclrHeaderBackColor As System.Drawing.Color = Drawing.Color.WhiteSmoke

    Private listItemCollection As eZeeButtonItemCollection
    Private listGroupCollection As eZeeButtonGroupCollection

    'Events
    Public Event ItemClick As ItemClickEventHandler
    Public Event GroupClick As GroupClickEventHandler
    Public Event LinkLabelClick As System.Windows.Forms.LinkLabelLinkClickedEventHandler

    Public Sub New()
        'MyBase.SetStyle(ControlStyles.UserPaint, False)
        'MyBase.SetStyle(ControlStyles.StandardClick, False)
        'MyBase.SetStyle(ControlStyles.UseTextForAccessibility, False)

        itemcount = 0
        groupcount = 0
        mblnCollapseOnFill = True
        mintSelectedValue = -1
        mintSelectedCount = 0
        mstrSelectedText = ""
        m_SelectedItem = Nothing
        mblnIsAddBegin = False
        mblnShowHeader = False

        Me.BorderStyle = Windows.Forms.BorderStyle.FixedSingle
        Me.BackColor = Drawing.Color.White
        Me.AutoScroll = True
        Me.listItemCollection = New eZeeButtonItemCollection(New eZeeButtonItemNativeCollection(Me))
    End Sub

#Region " Control Properties "

    <System.ComponentModel.Browsable(False)> _
    Public ReadOnly Property Items() As eZeeButtonItemCollection
        Get
            Return Me.listItemCollection
        End Get
    End Property

    <System.ComponentModel.Browsable(False)> _
    Public ReadOnly Property Groups() As eZeeButtonGroupCollection
        Get
            If (Me.listGroupCollection Is Nothing) Then
                Me.listGroupCollection = New eZeeButtonGroupCollection(Me)
            End If
            Return Me.listGroupCollection
        End Get
    End Property

    Public Property CollapseOnFill() As Boolean
        Get
            Return mblnCollapseOnFill
        End Get
        Set(ByVal value As Boolean)
            mblnCollapseOnFill = value
        End Set
    End Property

    <System.ComponentModel.Browsable(False)> _
    Public ReadOnly Property ExpandedGroups() As List(Of eZeeButtonGroup)
        Get
            Dim groups As New List(Of eZeeButtonGroup)
            Dim i As Integer = 0
            For Each cntrl As System.Windows.Forms.Control In MyBase.Controls
                If TypeOf cntrl Is eZeeButtonGroup Then
                    If (CType(cntrl, eZeeButtonGroup).Selected) Then
                        groups.Add(CType(cntrl, eZeeButtonGroup))
                    End If
                End If
            Next
            Return groups
        End Get
    End Property

    <System.ComponentModel.Browsable(False)> _
    Public Property SelectedValue() As Integer
        Get
            Return mintSelectedValue
        End Get
        Set(ByVal value As Integer)
            mintSelectedValue = value
            Me.SelectItem(value)
        End Set
    End Property

    <System.ComponentModel.Browsable(False)> _
    Public ReadOnly Property SelectedCount() As Integer
        Get
            Return mintSelectedCount
        End Get
    End Property

    <System.ComponentModel.Browsable(False)> _
    Public ReadOnly Property SelectedText() As String
        Get
            Return mstrSelectedText
        End Get
    End Property

    <System.ComponentModel.Browsable(False)> _
    Public Property SelectedItem() As eZeeButtonItem
        Get
            Return m_SelectedItem
        End Get
        Set(ByVal value As eZeeButtonItem)
            m_SelectedItem = value
            If (value IsNot Nothing) Then
                Me.SelectItem(value.ItemId)
            End If
        End Set
    End Property

    Public Property ShowHeader() As Boolean
        Get
            Return mblnShowHeader
        End Get
        Set(ByVal value As Boolean)
            mblnShowHeader = value

            Me.ShowHideHeader(value)
        End Set
    End Property

    Public Property HeaderText() As String
        Get
            Return mstrHeaderText
        End Get
        Set(ByVal value As String)
            mstrHeaderText = value
            Me.SetHeaderValues()
        End Set
    End Property

    Public Property HeaderBackColor() As System.Drawing.Color
        Get
            Return mclrHeaderBackColor
        End Get
        Set(ByVal value As System.Drawing.Color)
            mclrHeaderBackColor = value
        End Set
    End Property

    Public Property AddText() As String
        Get
            Return mstrAddNewText
        End Get
        Set(ByVal value As String)
            mstrAddNewText = value
            Me.SetHeaderValues()
        End Set
    End Property

#End Region

#Region " Raises Events "

    <System.ComponentModel.Description("Raises the ItemClick event.")> _
    Protected Overridable Sub OnItemClick(ByVal biItem As eZeeButtonItem, ByVal e As ItemClickEventArgs)
        RaiseEvent ItemClick(biItem, e)
    End Sub

    <System.ComponentModel.Description("Raises the GroupClick event.")> _
    Protected Overridable Sub OnGroupClick(ByVal bgGroup As eZeeButtonGroup, ByVal e As GroupClickEventArgs)
        RaiseEvent GroupClick(bgGroup, e)
    End Sub

    <System.ComponentModel.Description("Raises the LinkLabel Click event.")> _
    Protected Overridable Sub OnLinkLabelClick(ByVal lnkLabel As System.Windows.Forms.LinkLabel, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
        RaiseEvent LinkLabelClick(lnkLabel, e)
    End Sub

    Friend Function RaiseItemClickEvent(ByVal biItem As eZeeButtonItem) As Boolean

        'biItem.Selected = True

        Dim itemEventArgs As ItemClickEventArgs
        itemEventArgs = New ItemClickEventArgs(biItem, biItem.Index, biItem.ItemId, biItem.Text)
        Me.mintSelectedValue = biItem.ItemId
        Me.m_SelectedItem = biItem
        Me.mintSelectedCount = 1
        Me.mstrSelectedText = biItem.Text
        OnItemClick(biItem, itemEventArgs)

    End Function

    Friend Function RaiseGroupClickEvent(ByVal bgGroup As eZeeButtonGroup) As Boolean

        Dim GroupEventArgs As GroupClickEventArgs
        GroupEventArgs = New GroupClickEventArgs(bgGroup, bgGroup.Index, bgGroup.GroupID, bgGroup.Text)
        OnGroupClick(bgGroup, GroupEventArgs)

    End Function

    Friend Function RaiseLinkLabelClickEvent(ByVal lnkLabel As System.Windows.Forms.LinkLabel, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) As Boolean
        OnLinkLabelClick(lnkLabel, e)
    End Function

#End Region

#Region " Methods "

    Public Sub ClearView()
        Me.Controls.Clear()
        Me.itemcount = 0
        Me.mintSelectedValue = -1
        Me.mintSelectedCount = 0
        Me.m_SelectedItem = Nothing
        Me.header = Nothing
        Me.ShowHideHeader(Me.mblnShowHeader)
    End Sub

    Public Function GetItem(ByVal ItemId As Integer) As eZeeButtonItem
        Dim item As eZeeButtonItem = Nothing
        Try
            For Each cntrl As System.Windows.Forms.Control In Me.Controls
                If (TypeOf cntrl Is eZeeButtonItem) Then
                    item = CType(cntrl, eZeeButtonItem)
                    If (item.ItemId = ItemId) Then
                        Return item
                    End If
                End If
            Next
            Throw New NullReferenceException("ItemId")
        Finally
            item = Nothing
        End Try
    End Function

    Public Sub HideHorzScrollBar()
        For Each cntrl As System.Windows.Forms.Control In Me.Controls
            If (TypeOf cntrl Is eZeeButtonGroup Or TypeOf cntrl Is eZeeButtonListViewHeader) Then
                Me.SetWidth(cntrl)
            End If
        Next
    End Sub

    Public Sub BeginAdd()
        mblnIsAddBegin = True
    End Sub

    Public Sub EndAdd()
        mblnIsAddBegin = False
        Me.HideHorzScrollBar()
    End Sub

    Public Sub ShowHideHeader(ByVal blnShow As Boolean)
        If (blnShow) Then
            If (header IsNot Nothing) Then Exit Sub
            header = New eZeeButtonListViewHeader

            header.Owner = Me
            header.HeaderText = Me.HeaderText
            header.AddText = Me.AddText
            header.BackColor = Me.HeaderBackColor
            Me.SetWidth(header)

            Me.Controls.Add(header)
        Else
            If (header IsNot Nothing) Then
                Me.Controls.RemoveAt(0)
                header = Nothing
            End If
        End If
    End Sub

    Private Sub SetWidth(ByVal cntrl As System.Windows.Forms.Control)
        If (cntrl IsNot Nothing) Then
            If (Me.VerticalScroll.Visible) Then
                cntrl.Width = Me.Width - (Me.Padding.Left + Me.Padding.Right) - 2 - 17
            Else
                cntrl.Width = Me.Width - (Me.Padding.Left + Me.Padding.Right) - 2
            End If
        End If
    End Sub

    Private Sub SetHeaderValues()
        If (header IsNot Nothing) Then
            header.HeaderText = Me.HeaderText
            header.AddText = Me.AddText
        End If
    End Sub

    Private Sub SelectItem(ByVal ItemId As Integer)
        If (ItemId < 1) Then
            If (Me.m_SelectedItem IsNot Nothing) Then
                Me.m_SelectedItem.Checked = False
            End If
            Me.mintSelectedCount = 0
            Me.mstrSelectedText = ""
            Me.m_SelectedItem = Nothing
            Exit Sub
        End If
        Dim item As eZeeButtonItem = Nothing
        For Each cntrl As System.Windows.Forms.Control In Me.Controls
            If (TypeOf cntrl Is eZeeButtonItem) Then
                item = CType(cntrl, eZeeButtonItem)
                If (item.ItemId = ItemId) Then
                    item.Checked = True
                    Me.mintSelectedCount = 1
                    Me.mstrSelectedText = CType(cntrl, eZeeButtonItem).Text
                    Me.m_SelectedItem = item

                    item.Select()
                    Exit Sub
                End If
            End If
        Next
    End Sub

#End Region

#Region " Override Events "

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()

        Me.SetWidth(header)
    End Sub
#End Region

#Region " Button Item Collection "

    Public Class eZeeButtonItemCollection
        Implements IList, ICollection, IEnumerable

        Private lastAccessedIndex As Integer
        Private innerList As IInnerList

#Region " Constructor "

        Public Sub New(ByVal owner As eZeeButtonListView)
            Me.lastAccessedIndex = -1
            Me.innerList = New eZeeButtonItemNativeCollection(owner)
        End Sub

        Friend Sub New(ByVal innerList As IInnerList)
            Me.lastAccessedIndex = -1
            Me.innerList = innerList
        End Sub

#End Region

#Region " Private "

        Private Function Add(ByVal value As Object) As Integer Implements System.Collections.IList.Add
            If TypeOf value Is eZeeButtonItem Then
                Return Me.IndexOf(Me.Add(DirectCast(value, eZeeButtonItem)))
            End If
            If (Not value Is Nothing) Then
                Return Me.IndexOf(Me.Add(value.ToString))
            End If
            Return -1
        End Function

        Private Function Contains(ByVal value As Object) As Boolean Implements System.Collections.IList.Contains
            Return (TypeOf value Is eZeeButtonItem AndAlso Me.Contains(DirectCast(value, eZeeButtonItem)))
        End Function

        Private Function IndexOf(ByVal value As Object) As Integer Implements System.Collections.IList.IndexOf
            If TypeOf value Is eZeeButtonItem Then
                Return Me.IndexOf(DirectCast(value, eZeeButtonItem))
            End If
            Return -1
        End Function

        Private Sub Insert(ByVal index As Integer, ByVal value As Object) Implements System.Collections.IList.Insert
            If TypeOf value Is eZeeButtonItem Then
                Me.Insert(index, DirectCast(value, eZeeButtonItem))
            ElseIf (Not value Is Nothing) Then
                Me.Insert(index, value.ToString)
            End If
        End Sub

        Private ReadOnly Property IsFixedSize() As Boolean Implements System.Collections.IList.IsFixedSize
            Get
                Return False
            End Get
        End Property

        Private ReadOnly Property IsSynchronized() As Boolean Implements System.Collections.ICollection.IsSynchronized
            Get
                Return True
            End Get
        End Property

        Private ReadOnly Property SyncRoot() As Object Implements System.Collections.ICollection.SyncRoot
            Get
                Return Me
            End Get
        End Property

        Private Sub Remove(ByVal value As Object) Implements System.Collections.IList.Remove
            If ((Not value Is Nothing) AndAlso TypeOf value Is eZeeButtonItem) Then
                Me.Remove(DirectCast(value, eZeeButtonItem))
            End If
        End Sub

        Private Function IsValidIndex(ByVal index As Integer) As Boolean
            Return ((index >= 0) AndAlso (index < Me.Count))
        End Function

        Private ReadOnly Property IsReadOnly() As Boolean Implements System.Collections.IList.IsReadOnly
            Get
                Return False
            End Get
        End Property

#End Region

#Region " Public "

        Public Sub CopyTo(ByVal array As System.Array, ByVal index As Integer) Implements System.Collections.ICollection.CopyTo
            Me.innerList.CopyTo(array, index)
        End Sub

        Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
            Return Me.innerList.GetEnumerator
        End Function

        Public Sub Clear() Implements System.Collections.IList.Clear
            Me.innerList.Clear()
        End Sub

        Public Overridable Function Add(ByVal text As String) As eZeeButtonItem
            Return Me.Add(New eZeeButtonItem(text))
        End Function

        Public Overridable Function Add(ByVal value As eZeeButtonItem) As eZeeButtonItem
            Me.innerList.Add(value)
            Return value
        End Function

        Public Sub AddRange(ByVal items As eZeeButtonItem())
            If (items Is Nothing) Then
                Throw New ArgumentNullException("items")
            End If
            Me.innerList.AddRange(items)
        End Sub

        Public Function Contains(ByVal item As eZeeButtonItem) As Boolean
            Return Me.innerList.Contains(item)
        End Function

        Public Function Contains(ByVal ItemId As Integer) As Boolean
            Return Me.innerList.Contains(ItemId)
        End Function

        Public Overridable Function ContainsKey(ByVal key As String) As Boolean
            Return Me.IsValidIndex(Me.IndexOfKey(key))
        End Function

        Public Overridable Function IndexOfKey(ByVal key As String) As Integer
            If Not String.IsNullOrEmpty(key) Then
                If (Me.IsValidIndex(Me.lastAccessedIndex) AndAlso String.Compare(CType(Me.Item(Me.lastAccessedIndex), eZeeButtonItem).Name, key, True)) Then
                    Return Me.lastAccessedIndex
                End If
                Dim i As Integer
                For i = 0 To Me.Count - 1
                    If String.Compare(CType(Me.Item(i), eZeeButtonItem).Name, key, True) Then
                        Me.lastAccessedIndex = i
                        Return i
                    End If
                Next i
                Me.lastAccessedIndex = -1
            End If
            Return -1
        End Function

        Public Overridable Sub Remove(ByVal item As eZeeButtonItem)
            Me.innerList.Remove(item)
        End Sub

        Public Overridable Sub RemoveByKey(ByVal key As String)
            Dim index As Integer = Me.IndexOfKey(key)
            If Me.IsValidIndex(index) Then
                Me.RemoveAt(index)
            End If
        End Sub

        Public Sub RemoveAt(ByVal index As Integer) Implements System.Collections.IList.RemoveAt
            If ((index < 0) OrElse (index >= Me.Count)) Then
                Throw New ArgumentOutOfRangeException("index")
            End If
            Me.innerList.RemoveAt(index)
        End Sub

        <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
        Default Public Property Item(ByVal index As Integer) As Object Implements System.Collections.IList.Item
            Get
                If ((index < 0) OrElse (index >= Me.innerList.Count)) Then
                    Throw New ArgumentOutOfRangeException("index")
                End If
                Return Me.innerList.Item(index)
            End Get
            Set(ByVal value As Object)
                If ((index < 0) OrElse (index >= Me.innerList.Count)) Then
                    Throw New ArgumentOutOfRangeException("index")
                End If
                Me.innerList.Item(index) = CType(value, eZeeButtonItem)
            End Set
        End Property

        Default Public Overridable ReadOnly Property Item(ByVal key As String) As eZeeButtonItem
            Get
                If Not String.IsNullOrEmpty(key) Then
                    Dim index As Integer = Me.IndexOfKey(key)
                    If Me.IsValidIndex(index) Then
                        Return CType(Me.Item(index), eZeeButtonItem)
                    End If
                End If
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property Count() As Integer Implements System.Collections.ICollection.Count
            Get
                Return Me.innerList.Count
            End Get
        End Property

#End Region

#Region " Inner List "

        <Reflection.DefaultMember("Item")> _
        Friend Interface IInnerList
            ' Methods
            Function Add(ByVal item As eZeeButtonItem) As eZeeButtonItem
            Sub AddRange(ByVal items As eZeeButtonItem())
            Sub Clear()
            Function Contains(ByVal item As eZeeButtonItem) As Boolean
            Function Contains(ByVal ItemId As Integer) As Boolean
            Sub CopyTo(ByVal dest As Array, ByVal index As Integer)
            Function GetEnumerator() As IEnumerator
            Function IndexOf(ByVal item As eZeeButtonItem) As Integer
            Function Insert(ByVal index As Integer, ByVal item As eZeeButtonItem) As eZeeButtonItem
            Sub Remove(ByVal item As eZeeButtonItem)
            Sub RemoveAt(ByVal index As Integer)

            ' Properties
            ReadOnly Property Count() As Integer
            Default Property Item(ByVal index As Integer) As eZeeButtonItem
            ReadOnly Property OwnerIsDesignMode() As Boolean
        End Interface

#End Region

    End Class

#Region " Button Item Native Collection "

    Friend Class eZeeButtonItemNativeCollection
        Implements eZeeButtonItemCollection.IInnerList

        Private m_Owner As eZeeButtonListView

        Public Sub New(ByVal owner As eZeeButtonListView)
            Me.m_Owner = owner
        End Sub

        Private Function GetItem(ByVal index As Integer) As eZeeButtonItem
            Dim iCount As Integer = 0
            Dim cntrl As System.Windows.Forms.Control
            For Each cntrl In Me.m_Owner.Controls
                If (TypeOf cntrl Is eZeeButtonItem) Then
                    iCount += 1
                End If
                If ((iCount - 1) = index) Then
                    Return CType(cntrl, eZeeButtonItem)
                End If
            Next

            Return Nothing
        End Function

        Private Function GetItemMainIndex(ByVal index As Integer) As Integer
            Dim iCount As Integer = 0
            Dim cCount As Integer = 0
            Dim cntrl As System.Windows.Forms.Control
            For Each cntrl In Me.m_Owner.Controls
                cCount += 1
                If (TypeOf cntrl Is eZeeButtonItem) Then
                    iCount += 1
                End If
                If ((iCount - 1) = index) Then
                    Return (cCount - 1)
                End If
            Next

            Return Nothing
        End Function

        Public Function Add(ByVal item As eZeeButtonItem) As eZeeButtonItem Implements eZeeButtonItemCollection.IInnerList.Add
            item.Owner = Me.m_Owner
            item.TabIndex = Me.m_Owner.Controls.Count

            If (item.Image IsNot Nothing) Then
                'item.TextImageRelation = Windows.Forms.TextImageRelation.ImageAboveText
                item.ImageAlign = Drawing.ContentAlignment.TopCenter
                item.TextAlign = Drawing.ContentAlignment.BottomCenter
            End If
            item.Owner = Me.m_Owner
            item.Index = Me.m_Owner.itemcount
            Me.m_Owner.itemcount += 1
            Me.m_Owner.Controls.Add(item)

            'Me.m_Owner.HideHorzScrollBar()

            item.Visible = Not (Me.m_Owner.CollapseOnFill)

            Return item
        End Function

        Public Sub AddRange(ByVal items() As eZeeButtonItem) Implements eZeeButtonItemCollection.IInnerList.AddRange
            For Each item As eZeeButtonItem In items
                item.Owner = Me.m_Owner
                item.TabIndex = Me.m_Owner.Controls.Count

                Me.m_Owner.itemcount += 1
                Me.m_Owner.Controls.Add(item)
            Next
        End Sub

        Public Sub Clear() Implements eZeeButtonItemCollection.IInnerList.Clear
            Me.m_Owner.ClearView()
        End Sub

        Public Function Contains(ByVal item As eZeeButtonItem) As Boolean Implements eZeeButtonItemCollection.IInnerList.Contains
            Return Me.m_Owner.Contains(item)
        End Function

        Public Function Contains(ByVal ItemId As Integer) As Boolean Implements eZeeButtonItemCollection.IInnerList.Contains
            For Each cntrl As System.Windows.Forms.Control In Me.m_Owner.Controls
                If (TypeOf cntrl Is eZeeButtonItem) Then
                    If (CType(cntrl, eZeeButtonItem).ItemId = ItemId) Then
                        Return True
                    End If
                End If
            Next
            Return False
        End Function

        Public Sub CopyTo(ByVal dest As System.Array, ByVal index As Integer) Implements eZeeButtonItemCollection.IInnerList.CopyTo
            'If (Me.m_Owner.itemCount > 0) Then
            '    Dim i As Integer
            '    For i = 0 To Me.Count - 1
            '    dest.SetValue(Me.Item(i), (index += 1))
            '    Next i
            'End If
        End Sub

        Public ReadOnly Property Count() As Integer Implements eZeeButtonItemCollection.IInnerList.Count
            Get
                Return Me.m_Owner.itemcount
            End Get
        End Property

        Public Function GetEnumerator() As System.Collections.IEnumerator Implements eZeeButtonItemCollection.IInnerList.GetEnumerator
            Dim dest As eZeeButtonItem() = New eZeeButtonItem(Me.m_Owner.itemcount - 1) {}
            Me.CopyTo(dest, 0)
            Return dest.GetEnumerator
        End Function

        Public Function IndexOf(ByVal item As eZeeButtonItem) As Integer Implements eZeeButtonItemCollection.IInnerList.IndexOf
            If (item Is Nothing) Then
                Throw New NullReferenceException("item")
            End If

            Dim i As Integer
            For i = 0 To Me.Count - 1
                If (item Is Me.Item(i)) Then
                    Return i
                End If
            Next i
            Return -1
        End Function

        <System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)> _
        Public Function Insert(ByVal index As Integer, ByVal item As eZeeButtonItem) As eZeeButtonItem Implements eZeeButtonItemCollection.IInnerList.Insert
            'If (index >= (Me.m_Owner.itemcount - 1)) Then
            '    Throw New IndexOutOfRangeException()
            'End If

            'Dim iCount As Integer = 0
            'Dim gCount As Integer = 0
            'Dim cCount As Integer = 0
            'Dim cntrl As Control
            'For Each cntrl In Me.m_Owner.Controls
            '    cCount += 1
            '    If (TypeOf cntrl Is eZeeButtonGroup) Then
            '        gCount += 1
            '        Continue For
            '    End If
            '    If (TypeOf cntrl Is eZeeButtonItem) Then
            '        iCount += 1
            '    End If
            '    If ((iCount - 1) = index) Then
            '        Exit For
            '    End If
            'Next

            Return item
        End Function

        Default Public Property Item(ByVal index As Integer) As eZeeButtonItem Implements eZeeButtonItemCollection.IInnerList.Item
            Get
                If (index < 0 OrElse index >= (Me.m_Owner.itemcount - 1)) Then
                    Throw New ArgumentOutOfRangeException("index")
                End If

                Dim btnItem As eZeeButtonItem
                btnItem = Me.GetItem(index)

                If (btnItem Is Nothing) Then
                    Throw New NullReferenceException("eZeeButtonItem")
                End If

                Return btnItem
            End Get
            Set(ByVal value As eZeeButtonItem)
                If ((index < 0) OrElse (index >= Me.m_Owner.itemcount)) Then
                    Throw New ArgumentOutOfRangeException("index")
                End If

                Dim iCount As Integer = 0
                Dim cntrl As System.Windows.Forms.Control
                For Each cntrl In Me.m_Owner.Controls
                    If (TypeOf cntrl Is eZeeButtonItem) Then
                        iCount += 1
                    End If
                    If ((iCount - 1) = index) Then
                        cntrl = value
                        Exit For
                    End If
                Next
            End Set
        End Property

        Public ReadOnly Property OwnerIsDesignMode() As Boolean Implements eZeeButtonItemCollection.IInnerList.OwnerIsDesignMode
            Get
                Return Me.m_Owner.DesignMode
            End Get
        End Property

        Public Sub Remove(ByVal item As eZeeButtonItem) Implements eZeeButtonItemCollection.IInnerList.Remove
            If (item Is Nothing) Then
                Throw New NullReferenceException("item")
            End If

            Me.RemoveAt(Me.IndexOf(item))
        End Sub

        Public Sub RemoveAt(ByVal index As Integer) Implements eZeeButtonItemCollection.IInnerList.RemoveAt
            If (index < 0 OrElse index >= (Me.m_Owner.itemcount - 1)) Then
                Throw New ArgumentOutOfRangeException("index")
            End If

            Me.m_Owner.itemcount -= 1
            Me.m_Owner.Controls.RemoveAt(Me.GetItemMainIndex(index))
        End Sub

    End Class

#End Region

#End Region

End Class

#Region " Event Classes "

Public Class ItemClickEventArgs
    Inherits EventArgs

    Private m_item As eZeeButtonItem = Nothing
    Private m_intSelectedIndex As Integer = -1
    Private m_intSelectedUnkID As Integer = -1
    Private m_intSelectedText As String = ""

    <System.ComponentModel.Description("Gets the item that selection has changed to")> _
    Public ReadOnly Property Item() As eZeeButtonItem
        Get
            Item = m_item
        End Get
    End Property

    Public Property Index() As Integer
        Get
            Return m_intSelectedIndex
        End Get
        Set(ByVal value As Integer)
            m_intSelectedIndex = value
        End Set
    End Property

    Public Property Value() As Integer
        Get
            Return m_intSelectedUnkID
        End Get
        Set(ByVal value As Integer)
            m_intSelectedUnkID = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return m_intSelectedText
        End Get
        Set(ByVal value As String)
            m_intSelectedText = value
        End Set
    End Property

    Public Sub New(ByVal Item As eZeeButtonItem, ByVal Index As Integer, ByVal UnkId As Integer, ByVal Text As String)
        MyBase.New()
        m_item = Item
        m_intSelectedIndex = Index
        m_intSelectedUnkID = UnkId
        m_intSelectedText = Text
    End Sub

End Class

Public Class GroupClickEventArgs
    Inherits EventArgs

    Private m_item As eZeeButtonGroup = Nothing
    Private m_intPressedIndex As Integer = -1
    Private m_intPressedUnkID As Integer = -1
    Private m_intPressedText As String = ""

    <System.ComponentModel.Description("Gets the item that selection has changed to")> _
    Public ReadOnly Property Item() As eZeeButtonGroup
        Get
            Item = m_item
        End Get
    End Property

    Public Property Index() As Integer
        Get
            Return m_intPressedIndex
        End Get
        Set(ByVal value As Integer)
            m_intPressedIndex = value
        End Set
    End Property

    Public Property Value() As Integer
        Get
            Return m_intPressedUnkID
        End Get
        Set(ByVal value As Integer)
            m_intPressedUnkID = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return m_intPressedText
        End Get
        Set(ByVal value As String)
            m_intPressedText = value
        End Set
    End Property

    Public Sub New(ByVal Item As eZeeButtonGroup, ByVal Index As Integer, ByVal UnkId As Integer, ByVal Text As String)
        MyBase.New()
        m_item = Item
        m_intPressedIndex = Index
        m_intPressedUnkID = UnkId
        m_intPressedText = Text
    End Sub

End Class

Public Delegate Sub ItemClickEventHandler(ByVal sender As Object, ByVal eventArgs As ItemClickEventArgs)
Public Delegate Sub GroupClickEventHandler(ByVal sender As Object, ByVal eventArgs As GroupClickEventArgs)

#End Region



