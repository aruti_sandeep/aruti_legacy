Imports System
Imports System.Drawing

''' <summary> 
''' Structure with graphics utility methods. 
''' </summary> 
Public Class GraphicsUtil
    ''' <summary> 
    ''' Checks if point is contained within <c>RectangleF</c> structure 
    ''' and extends rectangle bounds if neccessary to include the point. 
    ''' </summary> 
    ''' <param name="rect"> 
    ''' Reference to <c>RectangleF</c> to check. 
    ''' </param> 
    ''' <param name="pointToInclude"> 
    ''' <c>PontF</c> object to include. 
    ''' </param> 
    Public Shared Sub IncludePoint(ByRef rect As RectangleF, ByVal pointToInclude As PointF)
        IncludePointX(rect, pointToInclude.X)
        IncludePointY(rect, pointToInclude.Y)
    End Sub

    ''' <summary> 
    ''' Checks if x-coordinate is contained within the <c>RectangleF</c> 
    ''' structure and extends rectangle bounds if neccessary to include 
    ''' the point. 
    ''' </summary> 
    ''' <param name="rect"> 
    ''' <c>RectangleF</c> to check. 
    ''' </param> 
    ''' <param name="xToInclude"> 
    ''' x-coordinate to include. 
    ''' </param> 
    Public Shared Sub IncludePointX(ByRef rect As RectangleF, ByVal xToInclude As Single)
        If xToInclude < rect.X Then
            rect.Width = rect.Right - xToInclude
            rect.X = xToInclude
        ElseIf xToInclude > rect.Right Then
            rect.Width = xToInclude - rect.X
        End If
    End Sub

    ''' <summary> 
    ''' Checks if y-coordinate is contained within the <c>RectangleF</c> 
    ''' structure and extends rectangle bounds if neccessary to include 
    ''' the point. 
    ''' </summary> 
    ''' <param name="rect"> 
    ''' <c>RectangleF</c> to check. 
    ''' </param> 
    ''' <param name="yToInclude"> 
    ''' y-coordinate to include. 
    ''' </param> 
    Public Shared Sub IncludePointY(ByRef rect As RectangleF, ByVal yToInclude As Single)
        If yToInclude < rect.Y Then
            rect.Height = rect.Bottom - yToInclude
            rect.Y = yToInclude
        ElseIf yToInclude > rect.Bottom Then
            rect.Height = yToInclude - rect.Y
        End If
    End Sub
End Class

