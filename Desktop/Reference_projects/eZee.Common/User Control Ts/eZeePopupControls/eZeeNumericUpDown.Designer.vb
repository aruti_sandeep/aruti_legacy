<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeNumericUpDown
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlBorder = New System.Windows.Forms.Panel
        Me.pnlText = New System.Windows.Forms.Panel
        Me.txt1 = New eZee.TextBox.NumericTextBox
        Me.pnlBtn = New System.Windows.Forms.Panel
        Me.btnUp = New eZeeGradientButton
        Me.btnDown = New eZeeGradientButton
        Me.pnlBorder.SuspendLayout()
        Me.pnlText.SuspendLayout()
        Me.pnlBtn.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlBorder
        '
        Me.pnlBorder.BackColor = System.Drawing.Color.White
        Me.pnlBorder.Controls.Add(Me.pnlText)
        Me.pnlBorder.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlBorder.Location = New System.Drawing.Point(0, 0)
        Me.pnlBorder.Name = "pnlBorder"
        Me.pnlBorder.Padding = New System.Windows.Forms.Padding(1)
        Me.pnlBorder.Size = New System.Drawing.Size(153, 39)
        Me.pnlBorder.TabIndex = 25
        '
        'pnlText
        '
        Me.pnlText.BackColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.pnlText.Controls.Add(Me.txt1)
        Me.pnlText.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlText.Location = New System.Drawing.Point(1, 1)
        Me.pnlText.Name = "pnlText"
        Me.pnlText.Padding = New System.Windows.Forms.Padding(6, 3, 3, 3)
        Me.pnlText.Size = New System.Drawing.Size(151, 37)
        Me.pnlText.TabIndex = 24
        '
        'txt1
        '
        Me.txt1.AllowNegative = True
        Me.txt1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt1.BackColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(80, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.txt1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txt1.DigitsInGroup = 0
        Me.txt1.Flags = 0
        Me.txt1.Font = New System.Drawing.Font("Verdana", 14.25!)
        Me.txt1.ForeColor = System.Drawing.Color.White
        Me.txt1.Location = New System.Drawing.Point(4, 4)
        Me.txt1.MaxDecimalPlaces = 4
        Me.txt1.MaxWholeDigits = 9
        Me.txt1.Multiline = True
        Me.txt1.Name = "txt1"
        Me.txt1.Prefix = ""
        Me.txt1.RangeMax = 1.7976931348623157E+308
        Me.txt1.RangeMin = -1.7976931348623157E+308
        Me.txt1.ReadOnly = True
        Me.txt1.SelectionHighlight = False
        Me.txt1.SelectionMode = False
        Me.txt1.Size = New System.Drawing.Size(141, 31)
        Me.txt1.TabIndex = 22
        Me.txt1.Text = "0"
        '
        'pnlBtn
        '
        Me.pnlBtn.BackColor = System.Drawing.Color.Transparent
        Me.pnlBtn.Controls.Add(Me.btnUp)
        Me.pnlBtn.Controls.Add(Me.btnDown)
        Me.pnlBtn.Dock = System.Windows.Forms.DockStyle.Right
        Me.pnlBtn.Location = New System.Drawing.Point(153, 0)
        Me.pnlBtn.Name = "pnlBtn"
        Me.pnlBtn.Size = New System.Drawing.Size(78, 39)
        Me.pnlBtn.TabIndex = 24
        '
        'btnUp
        '
        Me.btnUp.BackColor1 = System.Drawing.Color.Transparent
        Me.btnUp.BackColor2 = System.Drawing.Color.Transparent
        Me.btnUp.BorderNormal = True
        Me.btnUp.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnUp.Image = My.Resources.ArrowUp
        Me.btnUp.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnUp.Location = New System.Drawing.Point(2, 0)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(37, 36)
        Me.btnUp.TabIndex = 26
        Me.btnUp.TabStop = False
        '
        'btnDown
        '
        Me.btnDown.BackColor1 = System.Drawing.Color.Transparent
        Me.btnDown.BackColor2 = System.Drawing.Color.Transparent
        Me.btnDown.BorderNormal = True
        Me.btnDown.DialogResult = System.Windows.Forms.DialogResult.None
        Me.btnDown.Image = My.Resources.ArrowDown
        Me.btnDown.ImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.btnDown.Location = New System.Drawing.Point(38, 0)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(37, 36)
        Me.btnDown.TabIndex = 25
        Me.btnDown.TabStop = False
        '
        'eZeeNumericUpDown
        '
        Me.Controls.Add(Me.pnlBorder)
        Me.Controls.Add(Me.pnlBtn)
        Me.Name = "eZeeNumericUpDown"
        Me.Size = New System.Drawing.Size(231, 39)
        Me.pnlBorder.ResumeLayout(False)
        Me.pnlText.ResumeLayout(False)
        Me.pnlText.PerformLayout()
        Me.pnlBtn.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlBorder As System.Windows.Forms.Panel
    Friend WithEvents pnlText As System.Windows.Forms.Panel
    Friend WithEvents txt1 As eZee.TextBox.NumericTextBox
    Friend WithEvents pnlBtn As System.Windows.Forms.Panel
    Friend WithEvents btnUp As eZeeGradientButton
    Friend WithEvents btnDown As eZeeGradientButton

End Class
