Public Class BG_ButtonEventArgs
    Inherits EventArgs
    Private m_Key As BG_Button

    Public Sub New(ByVal aKey As BG_Button)
        Me.m_Key = aKey
    End Sub

    Public ReadOnly Property Key() As BG_Button
        Get
            Return Me.m_Key
        End Get
    End Property
End Class