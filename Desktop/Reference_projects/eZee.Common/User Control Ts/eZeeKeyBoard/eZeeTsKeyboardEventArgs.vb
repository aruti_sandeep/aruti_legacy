Public Class eZeeTsKeyboardEventArgs
    Inherits EventArgs
    Private ReadOnly pvtKeyboardKeyPressed As String

    Public Sub New(ByVal KeyboardKeyPressed As String)
        Me.pvtKeyboardKeyPressed = KeyboardKeyPressed '
    End Sub

    Public ReadOnly Property KeyboardKeyPressed() As String
        Get
            Return pvtKeyboardKeyPressed
        End Get
    End Property
End Class
