Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Windows.Forms.Design

Friend NotInheritable Class eZeeCurvedPanelDesigner
    Inherits ParentControlDesigner
    ' Methods
    Friend Sub New()
    End Sub

    Protected Overrides Sub PostFilterProperties(ByVal properties As IDictionary)
        MyBase.PostFilterProperties(properties)
    End Sub

    Protected Overrides Sub PreFilterProperties(ByVal properties As IDictionary)
        MyBase.PreFilterProperties(properties)
        Dim descriptor As PropertyDescriptor = Nothing
        descriptor = TypeDescriptor.CreateProperty(GetType(eZeeCurvedPanel), "FillTransparency", GetType(Double), New Attribute() {New DefaultValueAttribute(0), CategoryAttribute.Appearance, New TypeConverterAttribute(GetType(OpacityConverter)), New DescriptionAttribute("The percentage of transparency the FillColor is drawn with"), RefreshPropertiesAttribute.All})
        properties.Item("FillTransparency") = descriptor
        descriptor = TypeDescriptor.CreateProperty(GetType(eZeeCurvedPanel), "FillColor", GetType(Color), New Attribute() {New DefaultValueAttribute(SystemColors.InactiveCaption), CategoryAttribute.Appearance, New DescriptionAttribute("Background Color of the fill area."), RefreshPropertiesAttribute.All})
        properties.Item("FillColor") = descriptor
        descriptor = TypeDescriptor.CreateProperty(GetType(eZeeCurvedPanel), "FillHighlightColor", GetType(Color), New Attribute() {New DefaultValueAttribute(SystemColors.InactiveCaptionText), CategoryAttribute.Appearance, New DescriptionAttribute("Gradient highlight color of the fill area."), RefreshPropertiesAttribute.All})
        properties.Item("FillHighlightStyle") = descriptor
        descriptor = TypeDescriptor.CreateProperty(GetType(eZeeCurvedPanel), "FillInset", GetType(Padding), New Attribute() {New DefaultValueAttribute("{Left=1,Top=1,Right=1,Bottom=1}"), CategoryAttribute.Appearance, New DescriptionAttribute("Inset margin for fill area, in pixels."), RefreshPropertiesAttribute.All})
        properties.Item("FillInset") = descriptor
        descriptor = TypeDescriptor.CreateProperty(GetType(eZeeCurvedPanel), "DrawFillBorder", GetType(Boolean), New Attribute() {New DefaultValueAttribute(True), CategoryAttribute.Appearance, New DescriptionAttribute("Whether or not to draw a border around the fill area."), RefreshPropertiesAttribute.All})
        properties.Item("DrawFillBorder") = descriptor
        descriptor = TypeDescriptor.CreateProperty(GetType(eZeeCurvedPanel), "FillBorderColor", GetType(Color), New Attribute() {New DefaultValueAttribute(SystemColors.ControlText), CategoryAttribute.Appearance, New DescriptionAttribute("Color of the border around the fill area."), RefreshPropertiesAttribute.All})
        properties.Item("FillBorderColor") = descriptor
        descriptor = TypeDescriptor.CreateProperty(GetType(eZeeCurvedPanel), "RoundedCorners", GetType(CornerSettings), New Attribute() {CategoryAttribute.Appearance, New DescriptionAttribute("Rounded corner options for the fill area."), RefreshPropertiesAttribute.All, DesignerSerializationVisibilityAttribute.Content})
        properties.Item("RoundedCorners") = descriptor
    End Sub

End Class
