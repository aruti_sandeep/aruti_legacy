

Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.ComponentModel.Design
Imports System.Diagnostics
Imports System.Drawing
Imports System.Drawing.Design
Imports System.Windows.Forms
Imports System.Windows.Forms.Design


#Region "Enums"

''' Represents possible styles of a wizard page. 
Public Enum eZeeWizardPageStyle

    ''' Represents a standard interior wizard page with a white banner at the top. 
    Standard

    ''' Represents a welcome wizard page with white background and large logo on the left. 
    Welcome

    ''' Represents a finish wizard page with white background, 
    ''' a large logo on the left and OK button. 
    Finish

    ''' Represents a blank wizard page. 
    eZeeStyle
    [Custom]
End Enum
#End Region


''' Represents a wizard page control with basic layout functionality. 

<DefaultEvent("Click")> _
<Designer(GetType(eZeeWizardPage.WizardPageDesigner))> _
<DesignTimeVisible(False), ToolboxItem(False)> _
Public Class eZeeWizardPage
    Inherits Panel


#Region "Consts"
    Private Const HEADER_AREA_HEIGHT As Integer = 64
    Private Const HEADER_GLYPH_SIZE As Integer = 48
    Private Const HEADER_TEXT_PADDING As Integer = 8
    Private Const WELCOME_GLYPH_WIDTH As Integer = 164
#End Region

#Region "Fields"
    Private m_style As eZeeWizardPageStyle = eZeeWizardPageStyle.Standard
    Private m_title As String = [String].Empty
    Private m_description As String = [String].Empty
    Private m_index As Integer = -1
#End Region

#Region "Properties"
    <DefaultValue(eZeeWizardPageStyle.Standard)> _
    <Category("Wizard")> _
    <Description("Gets or sets the style of the wizard page.")> _
    Public Property Style() As eZeeWizardPageStyle
        Get
            Return Me.m_style
        End Get
        Set(ByVal value As eZeeWizardPageStyle)
            If Me.m_style <> value Then
                Me.m_style = value
                ' get the parent wizard control 
                If Me.Parent IsNot Nothing AndAlso TypeOf Me.Parent Is eZeeWizard Then
                    Dim parentWizard As eZeeWizard = DirectCast(Me.Parent, eZeeWizard)
                    ' check if page is selected 
                    If parentWizard.SelectedPage Is Me Then
                        ' reactivate the selected page (performs redraw too) 
                        parentWizard.SelectedPage = Me
                    End If
                Else
                    ' just redraw the page 
                    Me.Invalidate()
                End If
            End If
        End Set
    End Property

    <DefaultValue("")> _
    <Category("Wizard")> _
    <Description("Gets or sets the title of the wizard page.")> _
    Public Property Title() As String
        Get
            Return Me.m_title
        End Get
        Set(ByVal value As String)
            If value Is Nothing Then
                value = [String].Empty
            End If
            If Me.m_title <> value Then
                Me.m_title = value
                Me.Invalidate()
            End If
        End Set
    End Property

    <DefaultValue("")> _
    <Category("Wizard")> _
    <Description("Gets or sets the description of the wizard page.")> _
    Public Property Description() As String
        Get
            Return Me.m_description
        End Get
        Set(ByVal value As String)
            If value Is Nothing Then
                value = [String].Empty
            End If
            If Me.m_description <> value Then
                Me.m_description = value
                Me.Invalidate()
            End If
        End Set
    End Property
#End Region

#Region "Methods"

    ''' Provides custom drawing to the wizard page. 

    Protected Overloads Overrides Sub OnPaint(ByVal e As PaintEventArgs)
        ' raise paint event 
        MyBase.OnPaint(e)

        ' check if custom style 
        If Me.m_style = eZeeWizardPageStyle.[Custom] Then
            ' filter out 
            Return
        End If

        ' init graphic resources 
        Dim headerRect As Rectangle = Me.ClientRectangle
        Dim glyphRect As Rectangle = Rectangle.Empty
        Dim titleRect As Rectangle = Rectangle.Empty
        Dim descriptionRect As Rectangle = Rectangle.Empty

        ' determine text format 
        Dim textFormat As StringFormat = StringFormat.GenericDefault
        textFormat.LineAlignment = StringAlignment.Near
        textFormat.Alignment = StringAlignment.Near
        textFormat.Trimming = StringTrimming.EllipsisCharacter

        Select Case Me.m_style
            Case eZeeWizardPageStyle.eZeeStyle
                headerRect.Height = Me.Height
                headerRect.Width = WELCOME_GLYPH_WIDTH
                ControlPaint.DrawBorder3D(e.Graphics, headerRect, Border3DStyle.Etched, Border3DSide.Left)
                headerRect.Height -= SystemInformation.Border3DSize.Width
                e.Graphics.FillRectangle(SystemBrushes.Window, headerRect)

                'int eZeeheaderPadding = 2; 

                Dim eZeeheaderImage As Image = Nothing
                Dim eZeeheaderFont As Font = Me.Font
                Dim eZeeheaderTitleFont As Font = Me.Font
                If Me.Parent IsNot Nothing AndAlso TypeOf Me.Parent Is eZeeWizard Then
                    ' get content from parent wizard, if exists 
                    Dim parentWizard As eZeeWizard = DirectCast(Me.Parent, eZeeWizard)
                    eZeeheaderImage = parentWizard.HeaderImage
                    eZeeheaderFont = parentWizard.HeaderFont
                    eZeeheaderTitleFont = parentWizard.HeaderTitleFont
                End If

                ' check if we have an image 
                If eZeeheaderImage Is Nothing Then
                    ' display a focus rect as a place holder 
                    'ControlPaint.DrawFocusRectangle(e.Graphics, titleRect)
                    ControlPaint.DrawFocusRectangle(e.Graphics, headerRect)
                Else
                    ' draw header image 
                    'e.Graphics.DrawImage(eZeeheaderImage, titleRect)
                    e.Graphics.DrawImage(eZeeheaderImage, headerRect)
                End If

                titleRect.Location = New Point(HEADER_TEXT_PADDING, HEADER_TEXT_PADDING)

                titleRect.Width = WELCOME_GLYPH_WIDTH - HEADER_TEXT_PADDING


                ' determine title height 
                Dim eZeeheaderTitleHeight As Integer = CInt(Math.Ceiling(e.Graphics.MeasureString(Me.m_title, eZeeheaderTitleFont, titleRect.Width, textFormat).Height))

                ' calculate text sizes 
                titleRect.Size = New Size(WELCOME_GLYPH_WIDTH - HEADER_TEXT_PADDING, eZeeheaderTitleHeight)


                descriptionRect.Location = titleRect.Location
                descriptionRect.Y += eZeeheaderTitleHeight + HEADER_TEXT_PADDING / 2
                descriptionRect.Size = New Size(titleRect.Width, Me.Width - descriptionRect.Y)

                ' draw tilte text (single line, truncated with ellipsis) 
                e.Graphics.DrawString(Me.m_title, eZeeheaderTitleFont, SystemBrushes.WindowText, titleRect, textFormat)
                ' draw description text (multiple lines if needed) 
                e.Graphics.DrawString(Me.m_description, eZeeheaderFont, SystemBrushes.WindowText, descriptionRect, textFormat)

                Exit Select
            Case eZeeWizardPageStyle.Standard

                ' adjust height for header 
                headerRect.Height = HEADER_AREA_HEIGHT
                ' draw header border 
                ControlPaint.DrawBorder3D(e.Graphics, headerRect, Border3DStyle.Etched, Border3DSide.Bottom)
                ' adjust header rect not to overwrite the border 
                headerRect.Height -= SystemInformation.Border3DSize.Height
                ' fill header with window color 
                e.Graphics.FillRectangle(SystemBrushes.Window, headerRect)

                ' determine header image regtangle 
                'int headerPadding = (int)Math.Floor((HEADER_AREA_HEIGHT - HEADER_GLYPH_SIZE) / 2); 
                Dim headerPadding As Integer = 2
                glyphRect.Location = New Point(Me.Width - HEADER_GLYPH_SIZE - headerPadding, headerPadding)
                glyphRect.Size = New Size(HEADER_GLYPH_SIZE, HEADER_GLYPH_SIZE)

                ' determine the header content 
                Dim headerImage As Image = Nothing
                Dim headerFont As Font = Me.Font
                Dim headerTitleFont As Font = Me.Font
                If Me.Parent IsNot Nothing AndAlso TypeOf Me.Parent Is eZeeWizard Then
                    ' get content from parent wizard, if exists 
                    Dim parentWizard As eZeeWizard = DirectCast(Me.Parent, eZeeWizard)
                    headerImage = parentWizard.HeaderImage
                    headerFont = parentWizard.HeaderFont
                    headerTitleFont = parentWizard.HeaderTitleFont
                End If

                ' check if we have an image 
                If headerImage Is Nothing Then
                    ' display a focus rect as a place holder 
                    ControlPaint.DrawFocusRectangle(e.Graphics, glyphRect)
                Else
                    ' draw header image 
                    e.Graphics.DrawImage(headerImage, glyphRect)
                End If

                ' determine title height 
                Dim headerTitleHeight As Integer = CInt(Math.Ceiling(e.Graphics.MeasureString(Me.m_title, headerTitleFont, 0, textFormat).Height))

                ' calculate text sizes 
                titleRect.Location = New Point(HEADER_TEXT_PADDING, HEADER_TEXT_PADDING)
                'titleRect.Size = new Size(glyphRect.Left - HEADER_TEXT_PADDING, 
                ' headerTitleHeight); 

                descriptionRect.Location = titleRect.Location
                descriptionRect.Y += headerTitleHeight + HEADER_TEXT_PADDING / 2
                descriptionRect.Size = New Size(titleRect.Width, HEADER_AREA_HEIGHT - descriptionRect.Y)

                ' draw tilte text (single line, truncated with ellipsis) 
                e.Graphics.DrawString(Me.m_title, headerTitleFont, SystemBrushes.WindowText, titleRect, textFormat)
                ' draw description text (multiple lines if needed) 
                e.Graphics.DrawString(Me.m_description, headerFont, SystemBrushes.WindowText, descriptionRect, textFormat)
                Exit Select
            Case eZeeWizardPageStyle.Welcome, eZeeWizardPageStyle.Finish
                ' fill whole page with window color 
                e.Graphics.FillRectangle(SystemBrushes.Window, headerRect)

                ' determine welcome image regtangle 
                glyphRect.Location = Point.Empty
                glyphRect.Size = New Size(WELCOME_GLYPH_WIDTH, Me.Height)

                ' determine the icon that should appear on the welcome page 
                Dim welcomeImage As Image = Nothing
                Dim welcomeFont As Font = Me.Font
                Dim welcomeTitleFont As Font = Me.Font
                If Me.Parent IsNot Nothing AndAlso TypeOf Me.Parent Is eZeeWizard Then
                    ' get content from parent wizard, if exists 
                    Dim parentWizard As eZeeWizard = DirectCast(Me.Parent, eZeeWizard)
                    welcomeImage = parentWizard.WelcomeImage
                    welcomeFont = parentWizard.WelcomeFont
                    welcomeTitleFont = parentWizard.WelcomeTitleFont
                End If

                ' check if we have an image 
                If welcomeImage Is Nothing Then
                    ' display a focus rect as a place holder 
                    ControlPaint.DrawFocusRectangle(e.Graphics, glyphRect)
                Else
                    ' draw welcome page image 
                    e.Graphics.DrawImage(welcomeImage, glyphRect)
                End If

                ' calculate text sizes 
                titleRect.Location = New Point(WELCOME_GLYPH_WIDTH + HEADER_TEXT_PADDING, HEADER_TEXT_PADDING)
                titleRect.Width = Me.Width - titleRect.Left - HEADER_TEXT_PADDING
                ' determine title height 
                Dim welcomeTitleHeight As Integer = CInt(Math.Ceiling(e.Graphics.MeasureString(Me.m_title, welcomeTitleFont, titleRect.Width, textFormat).Height))
                descriptionRect.Location = titleRect.Location
                descriptionRect.Y += welcomeTitleHeight + HEADER_TEXT_PADDING
                descriptionRect.Size = New Size(Me.Width - descriptionRect.Left - HEADER_TEXT_PADDING, Me.Height - descriptionRect.Y)

                ' draw tilte text (multiple lines if needed) 
                e.Graphics.DrawString(Me.m_title, welcomeTitleFont, SystemBrushes.WindowText, titleRect, textFormat)
                ' draw description text (multiple lines if needed) 
                e.Graphics.DrawString(Me.m_description, welcomeFont, SystemBrushes.WindowText, descriptionRect, textFormat)
                Exit Select
        End Select
    End Sub
#End Region

#Region "Inner classes"

    ''' This is a designer for the Banner. 
    ''' This designer locks the control vertical sizing. 

    Friend Class WizardPageDesigner
        Inherits ParentControlDesigner

        ''' Gets the selection rules that indicate the movement capabilities of a component. 

        Public Overloads Overrides ReadOnly Property SelectionRules() As SelectionRules
            Get
                ' lock the control 
                Return SelectionRules.Visible Or SelectionRules.Locked
            End Get
        End Property
    End Class
#End Region
End Class

