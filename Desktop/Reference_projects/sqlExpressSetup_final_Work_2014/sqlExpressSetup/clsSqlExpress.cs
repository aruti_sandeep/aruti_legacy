using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace sqlExpressSetup
{
    class clsSqlExpress
    {
        Process myProcess;

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public static extern int GetShortPathName(
                 [MarshalAs(UnmanagedType.LPTStr)]
                   string path,
                 [MarshalAs(UnmanagedType.LPTStr)]
                   StringBuilder shortPath,
                 int shortPathLength
                 );

        #region Internal variables

        //Variables for setup.exe command line
        private string instanceName = "SQLEXPRESS";
        private string installSqlDir = "";
        private string installSqlSharedDir = "";
        private string installSqlDataDir = "";
        private string addLocal = "All";
        private bool sqlAutoStart = true;
        private bool sqlBrowserAutoStart = true;
        private string sqlBrowserAccount = "";
        private string sqlBrowserPassword = "";
        private string sqlAccount = "";
        private string sqlPassword = "";
        private bool sqlSecurityMode = false;
        private string saPassword = "";
        private string sqlCollation = "SQL_Latin1_General_CP1_CI_AS";
        private bool disableNetworkProtocols = true;
        private bool errorReporting = true;
        private string sqlExpressSetupFileLocation =
        System.Environment.GetEnvironmentVariable("TEMP") + "\\sqlexpr.exe";
        #endregion

        #region Properties
        public string InstanceName
        {
            get
            {
                return instanceName;
            }
            set
            {
                instanceName = value;
            }
        }

        public string SetupFileLocation
        {
            get
            {
                return sqlExpressSetupFileLocation;
            }
            set
            {
                sqlExpressSetupFileLocation = value;
            }
        }

        public string SqlInstallSharedDirectory
        {
            get
            {
                return installSqlSharedDir;
            }
            set
            {
                installSqlSharedDir = value;
            }
        }
        public string SqlDataDirectory
        {
            get
            {
                return installSqlDataDir;
            }
            set
            {
                installSqlDataDir = value;
            }
        }
        public bool AutostartSQLService
        {
            get
            {
                return sqlAutoStart;
            }
            set
            {
                sqlAutoStart = value;
            }
        }
        public bool AutostartSQLBrowserService
        {
            get
            {
                return sqlBrowserAutoStart;
            }
            set
            {
                sqlBrowserAutoStart = value;
            }
        }
        public string SqlBrowserAccountName
        {
            get
            {
                return sqlBrowserAccount;
            }
            set
            {
                sqlBrowserAccount = value;
            }
        }
        public string SqlBrowserPassword
        {
            get
            {
                return sqlBrowserPassword;
            }
            set
            {
                sqlBrowserPassword = value;
            }
        }
        //Defaults to LocalSystem
        public string SqlServiceAccountName
        {
            get
            {
                return sqlAccount;
            }
            set
            {
                sqlAccount = value;
            }
        }
        public string SqlServicePassword
        {
            get
            {
                return sqlPassword;
            }
            set
            {
                sqlPassword = value;
            }
        }
        public bool UseSQLSecurityMode
        {
            get
            {
                return sqlSecurityMode;
            }
            set
            {
                sqlSecurityMode = value;
            }
        }
        public string SAPassword
        {
            set
            {
                saPassword = value;
            }
        }
        public string Collation
        {
            get
            {
                return sqlCollation;
            }
            set
            {
                sqlCollation = value;
            }
        }
        public bool DisableNetworkProtocols
        {
            get
            {
                return disableNetworkProtocols;
            }
            set
            {
                disableNetworkProtocols = value;
            }
        }
        public bool ReportErrors
        {
            get
            {
                return errorReporting;
            }
            set
            {
                errorReporting = value;
            }
        }
        public string SqlInstallDirectory
        {
            get
            {
                return installSqlDir;
            }
            set
            {
                installSqlDir = value;
            }
        }
        #endregion

        #region Check Is SQL Express Installed or not
        public bool IsExpressInstalled()
        {

            using (RegistryKey Key = Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Microsoft SQL Server\\", false))
            {
                if (Key == null) return false;
                string[] strNames;
                strNames = Key.GetSubKeyNames();

                //If we cannot find a SQL Server registry key, we don't have SQL Server Express installed
                if (strNames.Length == 0) return false;

                foreach (string s in strNames)
                {
                    if (s.ToUpper() == "aPayroll")
                    {
                        //Check to see if the edition is "Express Edition"
                        //using (RegistryKey KeyEdition = Key.OpenSubKey(s.ToString() + "\\Setup\\", false))
                        //{
                        //    if ((string)KeyEdition.GetValue("Edition") == "Express Edition")
                        //    {
                        //        //If there is at least one instance of SQL Server Express installed, return true
                        //        return true;
                        //    }
                        //}
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion

        #region Enum SQL Instance
        public int EnumSQLInstances(ref string[] strInstanceArray, ref string[] strEditionArray, ref string[] strVersionArray)
        {
            using (RegistryKey Key = Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Microsoft SQL Server\\", false))
            {
                if (Key == null) return 0;
                string[] strNames;
                strNames = Key.GetSubKeyNames();

                //If we can not find a SQL Server registry key, we return 0 for none
                if (strNames.Length == 0) return 0;

                //How many instances do we have?
                int iNumberOfInstances = 0;

                foreach (string s in strNames)
                {
                    if (s.StartsWith("MSSQL."))
                        iNumberOfInstances++;
                }

                //Reallocate the string arrays to the new number of instances
                strInstanceArray = new string[iNumberOfInstances];
                strVersionArray = new string[iNumberOfInstances];
                strEditionArray = new string[iNumberOfInstances];
                int iCounter = 0;

                foreach (string s in strNames)
                {
                    if (s.StartsWith("MSSQL."))
                    {
                        //Get Instance name
                        using (RegistryKey KeyInstanceName = Key.OpenSubKey(s.ToString(), false))
                        {
                            strInstanceArray[iCounter] = (string)KeyInstanceName.GetValue("");
                        }

                        //Get Edition
                        using (RegistryKey KeySetup = Key.OpenSubKey(s.ToString() + "\\Setup\\", false))
                        {
                            strEditionArray[iCounter] = (string)KeySetup.GetValue("Edition");
                            strVersionArray[iCounter] = (string)KeySetup.GetValue("Version");
                        }
                        iCounter++;
                    }
                }
                return iCounter;
            }
        }
        #endregion

        #region Build Command Line String
        private string BuildCommandLine()
        {
            StringBuilder strCommandLine = new StringBuilder();

            //* SQL 2005
            //////if (!string.IsNullOrEmpty(instanceName))
            //////{
            //////    strCommandLine.Append(" INSTANCENAME=\"").Append(instanceName).Append("\"");
            //////}

            //////if (!string.IsNullOrEmpty(installSqlDir))
            //////{
            //////    strCommandLine.Append(" INSTALLSQLDIR=\"").Append(installSqlDir).Append("\"");
            //////}

            //////if (!string.IsNullOrEmpty(installSqlSharedDir))
            //////{
            //////    strCommandLine.Append(" INSTALLSQLSHAREDDIR=\"").Append(installSqlSharedDir).Append("\"");
            //////}

            //////if (!string.IsNullOrEmpty(installSqlDataDir))
            //////{
            //////    strCommandLine.Append(" INSTALLSQLDATADIR=\"").Append(installSqlDataDir).Append("\"");
            //////}

            //////if (!string.IsNullOrEmpty(addLocal))
            //////{
            //////    strCommandLine.Append(" ADDLOCAL=\"").Append(addLocal).Append("\"");
            //////}

            //////if (sqlAutoStart)
            //////{
            //////    strCommandLine.Append(" SQLAUTOSTART=1");
            //////}
            //////else
            //////{
            //////    strCommandLine.Append(" SQLAUTOSTART=0");
            //////}

            //////if (sqlBrowserAutoStart)
            //////{
            //////    strCommandLine.Append(" SQLBROWSERAUTOSTART=1");
            //////}
            //////else
            //////{
            //////    strCommandLine.Append(" SQLBROWSERAUTOSTART=0");
            //////}

            //////if (!string.IsNullOrEmpty(sqlBrowserAccount))
            //////{
            //////    strCommandLine.Append(" SQLBROWSERACCOUNT=\"").Append(sqlBrowserAccount).Append("\"");
            //////}

            //////if (!string.IsNullOrEmpty(sqlBrowserPassword))
            //////{
            //////    strCommandLine.Append(" SQLBROWSERPASSWORD=\"").Append(sqlBrowserPassword).Append("\"");
            //////}

            //////if (!string.IsNullOrEmpty(sqlAccount))
            //////{
            //////    strCommandLine.Append(" SQLACCOUNT=\"").Append(sqlAccount).Append("\"");
            //////}

            //////if (!string.IsNullOrEmpty(sqlPassword))
            //////{
            //////    strCommandLine.Append(" SQLPASSWORD=\"").Append(sqlPassword).Append("\"");
            //////}

            //////if (sqlSecurityMode == true)
            //////{
            //////    strCommandLine.Append(" SECURITYMODE=SQL");
            //////}

            //////if (!string.IsNullOrEmpty(saPassword))
            //////{
            //////    strCommandLine.Append(" SAPWD=\"").Append(saPassword).Append("\"");
            //////}

            //////if (!string.IsNullOrEmpty(sqlCollation))
            //////{
            //////    strCommandLine.Append(" SQLCOLLATION=\"").Append(sqlCollation).Append("\"");
            //////}

            //////if (disableNetworkProtocols == true)
            //////{
            //////    strCommandLine.Append(" DISABLENETWORKPROTOCOLS=1");
            //////}
            //////else
            //////{
            //////    strCommandLine.Append(" DISABLENETWORKPROTOCOLS=0");
            //////}

            //////if (errorReporting == true)
            //////{
            //////    strCommandLine.Append(" ERRORREPORTING=1");
            //////}
            //////else
            //////{
            //////    strCommandLine.Append(" ERRORREPORTING=0");
            //////}

            //* SQL 2008
            if (!string.IsNullOrEmpty(instanceName))
            {
                strCommandLine.Append(" /INSTANCENAME=\"").Append(instanceName).Append("\"");
            }

            if (sqlSecurityMode == true)
            {
                strCommandLine.Append(" /SECURITYMODE=SQL");
            }

            if (!string.IsNullOrEmpty(saPassword))
            {
                strCommandLine.Append(" /SAPWD=\"").Append(saPassword).Append("\"");
            }

            if (!string.IsNullOrEmpty(sqlCollation))
            {
                strCommandLine.Append(" /SQLCOLLATION=\"").Append(sqlCollation).Append("\"");
            }

            if (errorReporting == true)
            {
                strCommandLine.Append(" /ERRORREPORTING=1");
            }
            else
            {
                strCommandLine.Append(" /ERRORREPORTING=0");
            }

            return strCommandLine.ToString();
        }
        #endregion

        #region Install
        public bool InstallExpress()
        {


            //StringBuilder shortPath = new StringBuilder(255);
            //GetShortPathName(sqlExpressSetupFileLocation, shortPath, shortPath.Capacity);

            //sqlExpressSetupFileLocation = shortPath.ToString();



            //In both cases, we run Setup because we have the file.
            myProcess = new Process();
            this.myProcess.Exited += new System.EventHandler(this.MyProcess_Complete);

            myProcess.StartInfo.FileName = sqlExpressSetupFileLocation;
            //////myProcess.StartInfo.Arguments = "/qb " + BuildCommandLine();

            myProcess.StartInfo.Arguments = "/Action=Install /Quiet /FEATURES=SQL /InstanceName=" + instanceName + " /AGTSVCACCOUNT=\"NT AUTHORITY\\Network Service\" /AGTSVCSTARTUPTYPE=Automatic /SQLSVCACCOUNT=\"NT AUTHORITY\\Network Service\" /ASSYSADMINACCOUNTS=\"NT AUTHORITY\\Network Service\" /SQLSYSADMINACCOUNTS=\"\\Everyone\" /BROWSERSVCSTARTUPTYPE=Automatic /SECURITYMODE=SQL /SAPWD=" + saPassword + " /ADDCURRENTUSERASSQLADMIN=True /TCPENABLED=1 /NPENABLED=1 /SQLCOLLATION=SQL_Latin1_General_CP1_CI_AS ";

            //sqlAccount 2008
            //myProcess.StartInfo.Arguments = " /ACTION=Install /FEATURES=SQL /SQLSVCACCOUNT=\"NT AUTHORITY\\SYSTEM\" /NPENABLED=1 /TCPENABLED=1 /BROWSERSVCSTARTUPTYPE=Automatic /SQLSYSADMINACCOUNTS=\"\\Everyone\" /ISSVCACCOUNT=\"NT AUTHORITY\\NETWORKSERVICE\" /IACCEPTSQLSERVERLICENSETERMS=TRUE " + " " + BuildCommandLine();
            //myProcess.StartInfo.Arguments = "/ACTION=Install /QS /FEATURES=SQL /SQLSVCACCOUNT=\"NT AUTHORITY\\SYSTEM\" /NPENABLED=1 /TCPENABLED=1 /SQLSVCPASSWORD=\"pRofessionalaRuti999\" /SQLSYSADMINACCOUNTS=\"\\Everyone\" /AGTSVCACCOUNT=\"NT AUTHORITY\\Network Service\" /IACCEPTSQLSERVERLICENSETERMS=\"TRUE\\" + " " + BuildCommandLine();




            //sqlAccount 2012 "/ACTION=Install /FEATURES=SQL /INSTANCENAME=MSSQLSERVER /SQLSVCACCOUNT="<DomainName\UserName>" /SQLSVCPASSWORD="<StrongPassword>" /SQLSYSADMINACCOUNTS="<DomainName\UserName>" /AGTSVCACCOUNT="NT AUTHORITY\Network Service" /IACCEPTSQLSERVERLICENSETERMS  

            //2008 "/ACTION=Install /QS /FEATURES=SQL /SQLSVCACCOUNT=\"NT AUTHORITY\\SYSTEM\" /NPENABLED=1 /TCPENABLED=1 /SQLSYSADMINACCOUNTS=\"\\Everyone\" /ISSVCACCOUNT=\"NT AUTHORITY\\NetworkService\" " + BuildCommandLine();

            //MessageBox.Show(myProcess.StartInfo.Arguments);
            ///////*      /qn -- Specifies that setup run with no user interface.
            //////        /qb -- Specifies that setup show only the basic 
            //////        user interface. Only dialog boxes displaying progress information are 
            //////        displayed. Other dialog boxes, such as the dialog box that asks users if 
            //////        they want to restart at the end of the setup process, are not displayed.
            //////*/

            // "/Q = Specifies that Setup runs in a quiet mode without any user interface. This is used for unattended installations"  /IACCEPTSQLSERVERLICENSETERMS 
            // "/QS = Specifies that Setup runs and shows progress through the UI, but does not accept any input or show any error messages."


            myProcess.StartInfo.UseShellExecute = false;
            if (System.Environment.OSVersion.Version.Major >= 6)
            {

                myProcess.StartInfo.Verb = "runas";
            }

            //myProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            //myProcess.StartInfo.ErrorDialog = true;
            //myProcess.StartInfo.RedirectStandardError = true;

            //Anjan start
            //myProcess.StartInfo.Arguments = "";
            myProcess.StartInfo.ErrorDialog = true;
            myProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            myProcess.StartInfo.RedirectStandardError = true;
            //myProcess.StartInfo.UseShellExecute = true;


            try
            {
                myProcess.Start();
                myProcess.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                if (myProcess != null)
                {
                    myProcess.Dispose();
                }
            }

            //Anjan end

            //MessageBox.Show(myProcess.ExitCode.ToString()); 
            //return myProcess.HasExited;



            //int iExr = myProcess.ExitCode;

            //MessageBox.Show(iExr.ToString());

            //return (myProcess.ExitCode == 0);
            return true;

        }


        private void MyProcess_Complete(object sender, System.EventArgs e)
        {
            //MessageBox.Show("test");
        }

        #endregion


    }

}
