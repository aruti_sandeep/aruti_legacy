using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using Microsoft.SqlServer;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Diagnostics;
using Microsoft.Win32;

namespace sqlExpressSetup
{
    public partial class Form1 : Form
    {
        bool blnState = true;
        bool blnIsSuccess = false;
        SqlConnection xCon = null;
        public Form1()
        {
            InitializeComponent();
        }

        #region Forms Event(s)
        private void Form1_Load(object sender, EventArgs e)
        {
            //radDefaultInstallation.Checked = true;
            radNotInstallSQL.Checked = true;
        }
        #endregion

        #region Radio Button Event(s)
        private void radDefaultInstallation_CheckedChanged(object sender, EventArgs e)
        {
            if (radDefaultInstallation.Checked == true)
            {
                txtServerAddress.Enabled = false;
                txtSAPassword.Enabled = false;
                txtSAPassword.Text = txtSAPassword.Tag.ToString();
                txtServerAddress.Text = "";
            }
        }

        private void radSQLwithCustomPwd_CheckedChanged(object sender, EventArgs e)
        {
            if (radSQLwithCustomPwd.Checked == true)
            {
                txtServerAddress.Enabled = false;
                txtSAPassword.Enabled = true;
                txtSAPassword.Text = "";
                txtServerAddress.Text = "";
            }
        }

        private void radNotInstallSQL_CheckedChanged(object sender, EventArgs e)
        {
            if (radNotInstallSQL.Checked == true)
            {
                txtServerAddress.Enabled = true;
                txtSAPassword.Enabled = true;
                txtSAPassword.Text = "";
            }
        }
        #endregion

        #region Button's Event(s)
        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                if (radSQLwithCustomPwd.Checked == true)
                {
                    if (string.IsNullOrEmpty(txtSAPassword.Text))
                    {
                        MessageBox.Show("Please enter SA User Password.", this.gbConfigurationSetup.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                if (radNotInstallSQL.Checked == true)
                {
                    if (string.IsNullOrEmpty(txtServerAddress.Text))
                    {
                        MessageBox.Show("Please enter Server Address.", this.gbConfigurationSetup.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    if (string.IsNullOrEmpty(txtSAPassword.Text))
                    {
                        MessageBox.Show("Please enter SA User Password.", this.gbConfigurationSetup.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                DialogResult dresult = MessageBox.Show("Are you sure you want to go with the Selected SQL Configuration?", gbConfigurationSetup.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dresult == DialogResult.No)
                {
                    return;
                }
                gbConfigurationSetup.Enabled = false;
                pgbInstall.Style = ProgressBarStyle.Marquee;
                if ((radDefaultInstallation.Checked == true) || (radSQLwithCustomPwd.Checked == true))
                {
                    lblCaption.Text = "Please wait, Database server installation is in progress...";
                    System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(thProcess));
                    t.Start();
                }
                if (radNotInstallSQL.Checked == true)
                {
                    if (Connect() == true)
                    {
                        if (Create_Aruti_User() == true)
                        {
                            //S.SANDEEP [21 MAR 2016] -- START
                            //if (Create_INI_FILE() == true)
                            //{
                            //    this.Close();
                            //}
                            Disable_Windows_Authentication();
                            if (Create_INI_FILE() == true)
                            {
                                this.Close();
                            }
                            //S.SANDEEP [21 MAR 2016] -- END
                        }
                    }
                    else
                    {
                        gbConfigurationSetup.Enabled = true;
                        pgbInstall.Style = ProgressBarStyle.Continuous;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this.Text);
            }
            finally
            {
                blnIsSuccess = false;
            }
        }
        #endregion

        #region Private Method(s)
        private void thProcess()
        {
            String strAppPath;
            strAppPath = null;
            try
            {
                strAppPath = Path.GetDirectoryName(Application.ExecutablePath);
                Process objProcess = new Process();
                objProcess.StartInfo.FileName = strAppPath + "\\7z.exe";
                objProcess.StartInfo.Arguments = "x -r sqlexpr.7z";
                objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                objProcess.Start();
                objProcess.WaitForExit();
                objProcess = null;

                clsSqlExpress osqlExpress = new clsSqlExpress();
                osqlExpress.InstanceName = txtInstanceName.Text;

                osqlExpress.ReportErrors = true;
                osqlExpress.SetupFileLocation = strAppPath + "\\SQLEXPR\\setup.exe";  //Provide location for the Express setup file
                osqlExpress.SqlBrowserAccountName = ""; //Blank means LocalSystem
                osqlExpress.SqlBrowserPassword = ""; // N/A
                osqlExpress.SqlServiceAccountName = ""; //Blank means Localsystem
                osqlExpress.SqlServicePassword = ""; // N/A
                osqlExpress.SAPassword = txtSAPassword.Text; //<<Supply a secure sysadmin password>>
                osqlExpress.UseSQLSecurityMode = true;
                osqlExpress.Collation = "SQL_Latin1_General_CP1_CI_AS";
                osqlExpress.DisableNetworkProtocols = false;

                if (osqlExpress.IsExpressInstalled() == true)
                {
                    blnState = false;
                    return;
                }

                if (osqlExpress.InstallExpress() == true)
                {
                    
                    blnIsSuccess = true;
                    if (blnIsSuccess == true)
                    {

                        if (Connect() == true)
                        {
                            
                            if (Create_Aruti_User() == true)
                            {
                                this.Close();
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Instance can't create. Please contact Aruti Support Team.");
                }
                blnState = false;
            }
            catch (Exception ex)
            {
                blnState = false;
            }
            finally
            {
                blnState = false;
            }
        }
        private bool Connect()
        {
            try
            {
                string strConnection = null;
                string xServer = null;
                if (string.IsNullOrEmpty(txtServerAddress.Text))
                {
                    xServer = "(local)";
                }
                else
                {
                    xServer = txtServerAddress.Text;
                }
                strConnection = "Data Source=" + xServer + "\\" + txtInstanceName.Text + ";Persist Security Info=True;User ID = sa;Password=" + txtSAPassword.Text + "";
                xCon = new SqlConnection(strConnection);
                xCon.Open();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("A network-related or instance-specific error occurred while establishing a connection to SQL Server") == true)
                {
                    MessageBox.Show("Sorry, Server Not Found On Network. Please Contact Network Administrator.", gbConfigurationSetup.Text);
                }
                else
                {
                    MessageBox.Show(ex.Message, gbConfigurationSetup.Text);
                }
                return false;
            }
            return true;
        }
        private bool Create_Aruti_User()
        {
            try
            {
                string xQry = "";
                xQry = "IF NOT EXISTS(SELECT name FROM sys.syslogins WHERE name = 'aruti_sa') " +
                       "BEGIN " +
                       "    USE [master] " +
                       "    CREATE LOGIN [aruti_sa] WITH PASSWORD=N'" + txtSAPassword.Tag.ToString() + "', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF " +
                       "    EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'bulkadmin' " +
                       "    EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'dbcreator' " +
                       "    EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'diskadmin' " +
                       "    EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'processadmin' " +
                       "    EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'securityadmin' " +
                       "    EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'serveradmin' " +
                       "    EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'setupadmin' " +
                       "    EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'sysadmin' " +
                       "END ";

                SqlCommand xCmd = null;
                xCmd = new SqlCommand();
                xCmd.Connection = xCon;
                xCmd.CommandText = xQry;
                xCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, gbConfigurationSetup.Text);
                return false;
            }
            return true;
        }
        private bool Create_INI_FILE()
        {
            try
            {
                RegistryKey key = Registry.LocalMachine.OpenSubKey("SOFTWARE\\NPK\\Aruti");
                if (key != null)
                {
                    string apath = key.GetValue("apppath").ToString();
                    if (apath != null)
                    {
                        string filedata = null;
                        filedata = "[Aruti_Payroll]" + Environment.NewLine;
                        filedata += "Server=" + txtServerAddress.Text + Environment.NewLine;
                        filedata += "WorkingMode=Client" + Environment.NewLine;
                        if (System.IO.File.Exists(apath + "aruti.ini") == false)
                        {
                            System.IO.File.WriteAllText(apath + "aruti.ini", filedata);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, gbConfigurationSetup.Text);
                return false;
            }
            return true;
        }

        //S.SANDEEP [21 MAR 2016] -- START
        private void Disable_Windows_Authentication()
        {
            try
            {
                string xQry = "";

                xQry = "DECLARE @Login AS NVARCHAR(250), @SQL_QRY AS NVARCHAR(1000)  " +
                             "DECLARE WA_DISABLE CURSOR FOR  " +
                             "     SELECT server_principals.name FROM sys.server_principals WHERE server_principals.type_desc = 'WINDOWS_LOGIN' AND server_principals.is_disabled = 0  " +
                             "OPEN WA_DISABLE  " +
                             "FETCH NEXT FROM WA_DISABLE INTO @Login  " +
                             "WHILE @@FETCH_STATUS = 0  " +
                             "BEGIN  " +
                             "         SET @SQL_QRY = 'ALTER LOGIN ['+ @Login +'] DISABLE '  " +
                             "EXEC sys.sp_executesql @SQL_QRY  " +
                             "FETCH NEXT FROM WA_DISABLE INTO @Login  " +
                             "END  " +
                             "CLOSE WA_DISABLE  " +
                             "DEALLOCATE WA_DISABLE  ";

                SqlCommand xCmd = null;
                xCmd = new SqlCommand();
                xCmd.Connection = xCon;
                xCmd.CommandText = xQry;
                xCmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, gbConfigurationSetup.Text);
                return;
            }
        }
        //S.SANDEEP [21 MAR 2016] -- END
        #endregion

        #region Control's Event(s)
        private void tmrtimer1_Tick(object sender, EventArgs e)
        {
            pgbInstall.Refresh();
            if (!blnState) this.Close();
        }
        #endregion

        //private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    e.Cancel = blnState; 
        //}
    }
}

//private void Form1_Load(object sender, EventArgs e)
//{        
//    //System.Threading.Thread t = new System.Threading.Thread(new System.Threading.ThreadStart(thProcess));
//    //t.Start();                        
//}
//private void thProcess()
//{
//    String strAppPath;
//    strAppPath = null;
//    try
//    {
//        //this.Text = "Installing SQL Server 2005 Express Edition";

//        //this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

//        //Process objProcess = new Process();
//        //objProcess.StartInfo.FileName = "regsvr32";
//        //objProcess.StartInfo.Arguments = "C:\\WINDOWS\\system32\\SQL_DMO.dll";
//        //objProcess.Start( );

//        //RegistryKey Key = Registry.LocalMachine.CreateSubKey("Software\\ezee\\eZeeNextGen\\");
//        //Key.SetValue("apppath", "C:\\Program Files\\eZee\\eZee FrontDesk NextGen\\");
//        strAppPath = Path.GetDirectoryName(Application.ExecutablePath);

//        Process objProcess = new Process();
//        objProcess.StartInfo.FileName = strAppPath + "\\7z.exe";
//        objProcess.StartInfo.Arguments = "x -r sqlexpr.7z";
//        objProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
//        objProcess.Start();
//        objProcess.WaitForExit();
//        objProcess = null;

//        clsSqlExpress osqlExpress = new clsSqlExpress();
//        osqlExpress.InstanceName = txtInstanceName.Text;
//        //osqlExpress.InstanceName = "Express_HRD";
//        osqlExpress.ReportErrors = true;

//        //osqlExpress.SetupFileLocation = strAppPath + "\\sqlexpr.exe";  //Provide location for the Express setup file
//        osqlExpress.SetupFileLocation = strAppPath + "\\SQLEXPR\\setup.exe";  //Provide location for the Express setup file
//        //osqlExpress.SetupFileLocation = 
//        osqlExpress.SqlBrowserAccountName = ""; //Blank means LocalSystem
//        osqlExpress.SqlBrowserPassword = ""; // N/A
//        //osqlExpress.SqlDataDirectory = "F:\\Program Files\\Microsoft SQL Server\\";
//        //osqlExpress.SqlInstallDirectory = "C:\\Program Files\\";
//        //osqlExpress.SqlInstallSharedDirectory = "C:\\Program Files\\";
//        osqlExpress.SqlServiceAccountName = ""; //Blank means Localsystem
//        osqlExpress.SqlServicePassword = ""; // N/A
//        osqlExpress.SAPassword = txtSAPassword.Text; //<<Supply a secure sysadmin password>>
//        osqlExpress.UseSQLSecurityMode = true;
//        osqlExpress.Collation = "SQL_Latin1_General_CP1_CI_AS";
//        osqlExpress.DisableNetworkProtocols = false;

//        if (osqlExpress.IsExpressInstalled() == true)
//        {
//            //this.Cursor = System.Windows.Forms.Cursors.Default;
//            //MessageBox.Show("Instance Already exists");
//            //this.Close();
//            blnState = false;
//            return;
//        }

//        if (osqlExpress.InstallExpress())
//        {
//            if (osqlExpress.IsExpressInstalled() == false)
//            {
//                //this.Cursor = System.Windows.Forms.Cursors.Default;
//                MessageBox.Show("Instance can't create please contact eZee Support Team.");
//                //this.Close();
//                blnState = false;
//                return;
//            }

//            //this.Text = "Installing Database Sript";

//            string strConnection;
//            /*strConnection = "Data Source=(Local)\\" & textBox1.Text & ";" +
//                            "Initial Catalog = MASTER;" +
//                            "Persist Security Info=True;" +
//                            "User ID = sa;" +
//                            "Password=" & textBox2.Text & "";*/
//            strConnection = "Data Source=(Local)\\" + txtInstanceName.Text + ";" +
//                            "Persist Security Info=True;" +
//                            "User ID = sa;" +
//                            "Password=" + txtSAPassword.Text + "";


//            //Process objProcess = new Process();
//            //objProcess.StartInfo.FileName = strAppPath + "\\SQL_SMO.dll";
//            //objProcess.Start();
//            //objProcess.WaitForExit();

//            //SqlConnection cn = new SqlConnection(strConnection);

//            //SqlConnection conn = new SqlConnection(strConnection);

//            //Server server = new Server(new ServerConnection(conn));

//            //server.ConnectionContext.ExecuteNonQuery(strScript);
//        }
//        else
//        {
//            //this.Cursor = System.Windows.Forms.Cursors.Default;
//            MessageBox.Show("Please contact eZee Support Team.");
//        }

//        //this.Cursor = System.Windows.Forms.Cursors.Default;
//        //this.Close();
//        blnState = false;
//    }
//    catch (Exception ex)
//    {
//        //MessageBox.Show(ex.Message.ToString());
//        //this.Close();
//        blnState = false;
//    }
//    finally
//    {
//        blnState = false;
//        //System.IO.Directory.Delete(strAppPath+ "\\SQLEXPR");
//    }
//}

//private void tmrtimer1_Tick(object sender, EventArgs e)
//{
//    pgbInstall.Refresh(); 
//    if (!blnState) 
//        this.Close();

//}

//private void Form1_FormClosing(object sender, FormClosingEventArgs e)
//{
//    e.Cancel = blnState; 
//}

//private void radioButton3_CheckedChanged(object sender, EventArgs e)
//{

//}         