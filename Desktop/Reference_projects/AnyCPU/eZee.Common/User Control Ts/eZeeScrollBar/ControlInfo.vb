Imports System.ComponentModel

''' <summary>ControlInfo class is a storage container for fake controls made from gdi+.</summary>
''' 
Public Class ControlInfo

#Region "Private Variables"

    Private P_X As Integer
    Private P_Y As Integer
    Private P_H As Integer
    Private P_W As Integer
    Private P_Name As String
    Private P_X2 As Integer
    Private P_Y2 As Integer

#End Region

    ''' <summary>The left location of the fake control.</summary>
    <CategoryAttribute("Layout"), DescriptionAttribute("The left location of the fake control.")> _
        Public Property X() As Integer
        Get
            Return P_X
        End Get
        Set(ByVal Value As Integer)
            P_X = Value
        End Set
    End Property

    ''' <summary>The top location of the fake control.</summary>
    <CategoryAttribute("Layout"), DescriptionAttribute("The top location of the fake control.")> _
        Public Property Y() As Integer
        Get
            Return P_Y
        End Get
        Set(ByVal Value As Integer)
            P_Y = Value
        End Set
    End Property

    ''' <summary>The height of the fake control.</summary>
    <CategoryAttribute("Layout"), DescriptionAttribute("The height of the fake control.")> _
    Public Property H() As Integer
        Get
            Return P_H
        End Get
        Set(ByVal Value As Integer)
            P_H = Value
        End Set
    End Property

    ''' <summary>The width of the fake control.</summary>
    <CategoryAttribute("Layout"), DescriptionAttribute("The width of the fake control.")> _
    Public Property W() As Integer
        Get
            Return P_W
        End Get
        Set(ByVal Value As Integer)
            P_W = Value
        End Set
    End Property

    ''' <summary>The name of the fake control.</summary>
    <CategoryAttribute("Layout"), DescriptionAttribute("The name of the fake control.")> _
    Public Property Name() As String
        Get
            Return P_Name
        End Get
        Set(ByVal Value As String)
            P_Name = Value
        End Set
    End Property

    ''' <summary>The left + width of the fake control.</summary>
    <CategoryAttribute("Layout"), DescriptionAttribute("The left + width of the fake control.")> _
    Public Property X2() As Integer
        Get
            Return P_X2
        End Get
        Set(ByVal Value As Integer)
            P_X2 = Value
        End Set
    End Property

    ''' <summary>The top + height of the fake control.</summary>
    <CategoryAttribute("Layout"), DescriptionAttribute("The top + height of the fake control.")> _
    Public Property Y2() As Integer
        Get
            Return P_Y2
        End Get
        Set(ByVal Value As Integer)
            P_Y2 = Value
        End Set
    End Property

    ''' <summary>The constructor of the controlinfo class.</summary>
    <CategoryAttribute("Layout"), DescriptionAttribute("The constructor of the controlinfo class.")> _
    Public Sub New()

        P_X = 0
        P_Y = 0
        P_H = 0
        P_W = 0
        P_Name = ""
        P_X2 = 0
        P_Y2 = 0

    End Sub

End Class