Public Class eZeeListBoxEventArgs
    Inherits System.EventArgs

    Private m_intSelectedIndex As Integer = -1
    Private m_intSelectedUnkID As Integer = -1
    Private m_intSelectedText As String = ""
    Private m_Control As ItemButton

    Public Property SelecedIndex() As Integer
        Get
            Return m_intSelectedIndex
        End Get
        Set(ByVal value As Integer)
            m_intSelectedIndex = value
        End Set
    End Property

    Public Property SelecedUnkId() As Integer
        Get
            Return m_intSelectedUnkID
        End Get
        Set(ByVal value As Integer)
            m_intSelectedUnkID = value
        End Set
    End Property

    Public Property SelecedText() As String
        Get
            Return m_intSelectedText
        End Get
        Set(ByVal value As String)
            m_intSelectedText = value
        End Set
    End Property

    Public Property SelecedControl() As ItemButton
        Get
            Return m_Control
        End Get
        Set(ByVal value As ItemButton)
            m_Control = value
        End Set
    End Property

    Public Sub New(ByVal Index As Integer, ByVal UnkId As Integer, ByVal Text As String, ByVal ctrl As ItemButton)

        MyBase.New()
        m_intSelectedIndex = Index
        m_intSelectedUnkID = UnkId
        m_intSelectedText = Text
        m_Control = ctrl
    End Sub

End Class