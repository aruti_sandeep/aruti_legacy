Imports System
Imports System.Diagnostics
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System.Drawing
Imports System.Drawing.Drawing2D

<System.Serializable()> Public Class eZeeHeader
    Inherits UserControl

#Region " Private Variables "

    Public Enum GradientStyle
        Up = 1
        Down = 2
        Central = 3
    End Enum

    Private mclrBorderColor As Color = SystemColors.ControlDark
    Private menGradientStyle As GradientStyle = GradientStyle.Central
    Private mclrGradientColor1 As Color = SystemColors.Window
    Private mclrGradientColor2 As Color = SystemColors.Control

#End Region

#Region " Properties "

    Public Property Title() As String
        Get
            Return objlblHeaderText.Text
        End Get
        Set(ByVal value As String)
            objlblHeaderText.Text = value
            Me.Invalidate()
        End Set
    End Property

    Public Property Message() As String
        Get
            Return objlblDescription.Text
        End Get
        Set(ByVal value As String)
            objlblDescription.Text = value
            Me.Invalidate()
        End Set
    End Property

    Public Property HeaderTextForeColor() As Color
        Get
            Return objlblHeaderText.ForeColor
        End Get
        Set(ByVal value As Color)
            objlblHeaderText.ForeColor = value
            Me.Invalidate()
        End Set
    End Property

    Public Property DescriptionForeColor() As Color
        Get
            Return objlblDescription.ForeColor
        End Get
        Set(ByVal value As Color)
            objlblDescription.ForeColor = value
            Me.Invalidate()
        End Set
    End Property

    Public Property Icon() As Image
        Get
            Return picIcon.Image
        End Get
        Set(ByVal value As Image)
            picIcon.Image = value
            If value Is Nothing Then
                objlblDescription.Width += 20
                objlblHeaderText.Width += 20
                picIcon.Visible = False
            Else
                objlblDescription.Width -= 25
                objlblHeaderText.Width -= 25
                picIcon.Visible = True
            End If
            Me.Invalidate()
        End Set
    End Property

    Public Property BorderColor() As Color
        Get
            Return mclrBorderColor
        End Get
        Set(ByVal value As Color)
            mclrBorderColor = value
            Me.Invalidate()
        End Set
    End Property

    Public Property GradiantStyle() As GradientStyle
        Get
            Return menGradientStyle
        End Get
        Set(ByVal value As GradientStyle)
            menGradientStyle = value
            'Call OnPaint(New System.Windows.Forms.PaintEventArgs(Me.CreateGraphics, Me.ClientRectangle))
            Me.Invalidate()
        End Set
    End Property

    Public Property GradientColor1() As Color
        Get
            Return mclrGradientColor1
        End Get
        Set(ByVal value As Color)
            mclrGradientColor1 = value
            Me.Invalidate()
        End Set
    End Property

    Public Property GradientColor2() As Color
        Get
            Return mclrGradientColor2
        End Get
        Set(ByVal value As Color)
            mclrGradientColor2 = value
            Me.Invalidate()
        End Set
    End Property

#End Region

#Region " Constructor "

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        SetStyle(ControlStyles.UserPaint, True)
        SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        SetStyle(ControlStyles.AllPaintingInWmPaint, True)
        SetStyle(ControlStyles.CacheText, True)
        SetStyle(ControlStyles.ResizeRedraw, True)
    End Sub

#End Region

#Region " Overrides Events "

    Protected Overrides Sub OnResize(ByVal e As System.EventArgs)
        If Me.Height < 58 Then
            Me.Height = 58
        Else
            MyBase.OnResize(e)
        End If
    End Sub

    Protected Overrides Sub OnPaint(ByVal e As System.Windows.Forms.PaintEventArgs)
        MyBase.OnPaint(e)

        Dim g As Graphics = e.Graphics
        Dim start As New Point
        Dim gb As LinearGradientBrush

        Select Case menGradientStyle
            Case GradientStyle.Up
                start.X = Me.Width
                start.Y = 0

                gb = New LinearGradientBrush(start, New Point(start.X, Me.Height), mclrGradientColor2, mclrGradientColor1)
                g.FillRectangle(gb, New Rectangle(0, 0, Me.Width, Me.Height))
            Case GradientStyle.Down
                start.X = Me.Width
                start.Y = 0

                gb = New LinearGradientBrush(start, New Point(start.X, Me.Height), mclrGradientColor1, mclrGradientColor2)
                g.FillRectangle(gb, New Rectangle(0, 0, Me.Width, Me.Height))
            Case GradientStyle.Central
                start.X = CInt(Me.Width * 0.5)
                start.Y = 0

                gb = New LinearGradientBrush(start, New Point(start.X, CInt(Me.Height * 0.6)), mclrGradientColor1, mclrGradientColor2)
                g.FillRectangle(gb, New Rectangle(0, 0, Me.Width, CInt(Me.Height * 0.6)))

                gb = New LinearGradientBrush(New Point(start.X, CInt(Me.Height * 0.4)), New Point(start.X, Me.Height), mclrGradientColor2, mclrGradientColor1)
                g.FillRectangle(gb, New Rectangle(0, CInt(Me.Height * 0.5), Me.Width, Me.Height))
        End Select

        If Me.BorderStyle = Windows.Forms.BorderStyle.None Then
            ControlPaint.DrawBorder(e.Graphics, New Rectangle(0, 0, Me.Width, Me.Height), mclrBorderColor, ButtonBorderStyle.Solid)
        End If

    End Sub

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()

        If Me.Icon Is Nothing Then
            objlblDescription.Width += 20
            objlblHeaderText.Width += 20
            picIcon.Visible = False
        Else
            objlblDescription.Width -= 20
            objlblHeaderText.Width -= 20
            picIcon.Visible = True
        End If

        Me.Dock = DockStyle.Top
    End Sub

#End Region

End Class


