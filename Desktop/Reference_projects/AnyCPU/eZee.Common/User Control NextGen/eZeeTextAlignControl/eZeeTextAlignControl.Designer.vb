<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeTextAlignControl
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(eZeeTextAlignControl))
        Me.txtTextAlign = New System.Windows.Forms.TextBox
        Me.pnlTextAlign = New System.Windows.Forms.Panel
        Me.objbtnBottomRight = New eZeeLightButton(Me.components)
        Me.objbtnBottomLeft = New eZeeLightButton(Me.components)
        Me.objbtnBottomCenter = New eZeeLightButton(Me.components)
        Me.objbtnMiddleRight = New eZeeLightButton(Me.components)
        Me.objbtnMiddleLeft = New eZeeLightButton(Me.components)
        Me.objbtnMiddleCenter = New eZeeLightButton(Me.components)
        Me.objbtnTopRight = New eZeeLightButton(Me.components)
        Me.objbtnTopLeft = New eZeeLightButton(Me.components)
        Me.objbtnTopCenter = New eZeeLightButton(Me.components)
        Me.objbtnDropDown = New eZeeLightButton(Me.components)
        Me.pnlTextAlign.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtTextAlign
        '
        Me.txtTextAlign.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTextAlign.BackColor = System.Drawing.Color.White
        Me.txtTextAlign.Location = New System.Drawing.Point(0, 0)
        Me.txtTextAlign.Name = "txtTextAlign"
        Me.txtTextAlign.ReadOnly = True
        Me.txtTextAlign.Size = New System.Drawing.Size(125, 21)
        Me.txtTextAlign.TabIndex = 0
        '
        'pnlTextAlign
        '
        Me.pnlTextAlign.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlTextAlign.BackColor = System.Drawing.Color.White
        Me.pnlTextAlign.Controls.Add(Me.objbtnBottomRight)
        Me.pnlTextAlign.Controls.Add(Me.objbtnBottomLeft)
        Me.pnlTextAlign.Controls.Add(Me.objbtnBottomCenter)
        Me.pnlTextAlign.Controls.Add(Me.objbtnMiddleRight)
        Me.pnlTextAlign.Controls.Add(Me.objbtnMiddleLeft)
        Me.pnlTextAlign.Controls.Add(Me.objbtnMiddleCenter)
        Me.pnlTextAlign.Controls.Add(Me.objbtnTopRight)
        Me.pnlTextAlign.Controls.Add(Me.objbtnTopLeft)
        Me.pnlTextAlign.Controls.Add(Me.objbtnTopCenter)
        Me.pnlTextAlign.Location = New System.Drawing.Point(0, 20)
        Me.pnlTextAlign.Name = "pnlTextAlign"
        Me.pnlTextAlign.Size = New System.Drawing.Size(140, 93)
        Me.pnlTextAlign.TabIndex = 2
        '
        'objbtnBottomRight
        '
        Me.objbtnBottomRight.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnBottomRight.BackColor = System.Drawing.Color.White
        Me.objbtnBottomRight.BackgroundImage = CType(resources.GetObject("objbtnBottomRight.BackgroundImage"), System.Drawing.Image)
        Me.objbtnBottomRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnBottomRight.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.objbtnBottomRight.FlatAppearance.BorderSize = 0
        Me.objbtnBottomRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnBottomRight.ForeColor = System.Drawing.Color.Black
        Me.objbtnBottomRight.GradientBackColor = System.Drawing.Color.LightSteelBlue
        Me.objbtnBottomRight.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottomRight.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnBottomRight.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottomRight.Location = New System.Drawing.Point(95, 64)
        Me.objbtnBottomRight.Name = "objbtnBottomRight"
        Me.objbtnBottomRight.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnBottomRight.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottomRight.Selected = False
        Me.objbtnBottomRight.ShowDefaultBorderColor = True
        Me.objbtnBottomRight.Size = New System.Drawing.Size(38, 23)
        Me.objbtnBottomRight.TabIndex = 8
        Me.objbtnBottomRight.TabStop = False
        Me.objbtnBottomRight.Tag = "1024"
        Me.objbtnBottomRight.UseVisualStyleBackColor = True
        '
        'objbtnBottomLeft
        '
        Me.objbtnBottomLeft.BackColor = System.Drawing.Color.White
        Me.objbtnBottomLeft.BackgroundImage = CType(resources.GetObject("objbtnBottomLeft.BackgroundImage"), System.Drawing.Image)
        Me.objbtnBottomLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnBottomLeft.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.objbtnBottomLeft.FlatAppearance.BorderSize = 0
        Me.objbtnBottomLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnBottomLeft.ForeColor = System.Drawing.Color.Black
        Me.objbtnBottomLeft.GradientBackColor = System.Drawing.Color.LightSteelBlue
        Me.objbtnBottomLeft.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottomLeft.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnBottomLeft.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottomLeft.Location = New System.Drawing.Point(7, 64)
        Me.objbtnBottomLeft.Name = "objbtnBottomLeft"
        Me.objbtnBottomLeft.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnBottomLeft.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottomLeft.Selected = False
        Me.objbtnBottomLeft.ShowDefaultBorderColor = True
        Me.objbtnBottomLeft.Size = New System.Drawing.Size(38, 23)
        Me.objbtnBottomLeft.TabIndex = 7
        Me.objbtnBottomLeft.TabStop = False
        Me.objbtnBottomLeft.Tag = "256"
        Me.objbtnBottomLeft.UseVisualStyleBackColor = True
        '
        'objbtnBottomCenter
        '
        Me.objbtnBottomCenter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnBottomCenter.BackColor = System.Drawing.Color.White
        Me.objbtnBottomCenter.BackgroundImage = CType(resources.GetObject("objbtnBottomCenter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnBottomCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnBottomCenter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.objbtnBottomCenter.FlatAppearance.BorderSize = 0
        Me.objbtnBottomCenter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnBottomCenter.ForeColor = System.Drawing.Color.Black
        Me.objbtnBottomCenter.GradientBackColor = System.Drawing.Color.LightSteelBlue
        Me.objbtnBottomCenter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottomCenter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnBottomCenter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottomCenter.Location = New System.Drawing.Point(51, 64)
        Me.objbtnBottomCenter.Name = "objbtnBottomCenter"
        Me.objbtnBottomCenter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnBottomCenter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnBottomCenter.Selected = False
        Me.objbtnBottomCenter.ShowDefaultBorderColor = True
        Me.objbtnBottomCenter.Size = New System.Drawing.Size(38, 23)
        Me.objbtnBottomCenter.TabIndex = 6
        Me.objbtnBottomCenter.TabStop = False
        Me.objbtnBottomCenter.Tag = "512"
        Me.objbtnBottomCenter.UseVisualStyleBackColor = True
        '
        'objbtnMiddleRight
        '
        Me.objbtnMiddleRight.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnMiddleRight.BackColor = System.Drawing.Color.White
        Me.objbtnMiddleRight.BackgroundImage = CType(resources.GetObject("objbtnMiddleRight.BackgroundImage"), System.Drawing.Image)
        Me.objbtnMiddleRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnMiddleRight.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.objbtnMiddleRight.FlatAppearance.BorderSize = 0
        Me.objbtnMiddleRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnMiddleRight.ForeColor = System.Drawing.Color.Black
        Me.objbtnMiddleRight.GradientBackColor = System.Drawing.Color.LightSteelBlue
        Me.objbtnMiddleRight.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnMiddleRight.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnMiddleRight.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnMiddleRight.Location = New System.Drawing.Point(95, 35)
        Me.objbtnMiddleRight.Name = "objbtnMiddleRight"
        Me.objbtnMiddleRight.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnMiddleRight.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnMiddleRight.Selected = False
        Me.objbtnMiddleRight.ShowDefaultBorderColor = True
        Me.objbtnMiddleRight.Size = New System.Drawing.Size(38, 23)
        Me.objbtnMiddleRight.TabIndex = 5
        Me.objbtnMiddleRight.TabStop = False
        Me.objbtnMiddleRight.Tag = "64"
        Me.objbtnMiddleRight.UseVisualStyleBackColor = True
        '
        'objbtnMiddleLeft
        '
        Me.objbtnMiddleLeft.BackColor = System.Drawing.Color.White
        Me.objbtnMiddleLeft.BackgroundImage = CType(resources.GetObject("objbtnMiddleLeft.BackgroundImage"), System.Drawing.Image)
        Me.objbtnMiddleLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnMiddleLeft.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.objbtnMiddleLeft.FlatAppearance.BorderSize = 0
        Me.objbtnMiddleLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnMiddleLeft.ForeColor = System.Drawing.Color.Black
        Me.objbtnMiddleLeft.GradientBackColor = System.Drawing.Color.LightSteelBlue
        Me.objbtnMiddleLeft.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnMiddleLeft.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnMiddleLeft.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnMiddleLeft.Location = New System.Drawing.Point(7, 35)
        Me.objbtnMiddleLeft.Name = "objbtnMiddleLeft"
        Me.objbtnMiddleLeft.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnMiddleLeft.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnMiddleLeft.Selected = False
        Me.objbtnMiddleLeft.ShowDefaultBorderColor = True
        Me.objbtnMiddleLeft.Size = New System.Drawing.Size(38, 23)
        Me.objbtnMiddleLeft.TabIndex = 4
        Me.objbtnMiddleLeft.TabStop = False
        Me.objbtnMiddleLeft.Tag = "16"
        Me.objbtnMiddleLeft.UseVisualStyleBackColor = True
        '
        'objbtnMiddleCenter
        '
        Me.objbtnMiddleCenter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnMiddleCenter.BackColor = System.Drawing.Color.White
        Me.objbtnMiddleCenter.BackgroundImage = CType(resources.GetObject("objbtnMiddleCenter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnMiddleCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnMiddleCenter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.objbtnMiddleCenter.FlatAppearance.BorderSize = 0
        Me.objbtnMiddleCenter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnMiddleCenter.ForeColor = System.Drawing.Color.Black
        Me.objbtnMiddleCenter.GradientBackColor = System.Drawing.Color.LightSteelBlue
        Me.objbtnMiddleCenter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnMiddleCenter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnMiddleCenter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnMiddleCenter.Location = New System.Drawing.Point(51, 35)
        Me.objbtnMiddleCenter.Name = "objbtnMiddleCenter"
        Me.objbtnMiddleCenter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnMiddleCenter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnMiddleCenter.Selected = False
        Me.objbtnMiddleCenter.ShowDefaultBorderColor = True
        Me.objbtnMiddleCenter.Size = New System.Drawing.Size(38, 23)
        Me.objbtnMiddleCenter.TabIndex = 3
        Me.objbtnMiddleCenter.TabStop = False
        Me.objbtnMiddleCenter.Tag = "32"
        Me.objbtnMiddleCenter.UseVisualStyleBackColor = True
        '
        'objbtnTopRight
        '
        Me.objbtnTopRight.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnTopRight.BackColor = System.Drawing.Color.White
        Me.objbtnTopRight.BackgroundImage = CType(resources.GetObject("objbtnTopRight.BackgroundImage"), System.Drawing.Image)
        Me.objbtnTopRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnTopRight.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.objbtnTopRight.FlatAppearance.BorderSize = 0
        Me.objbtnTopRight.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnTopRight.ForeColor = System.Drawing.Color.Black
        Me.objbtnTopRight.GradientBackColor = System.Drawing.Color.LightSteelBlue
        Me.objbtnTopRight.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnTopRight.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnTopRight.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnTopRight.Location = New System.Drawing.Point(95, 6)
        Me.objbtnTopRight.Name = "objbtnTopRight"
        Me.objbtnTopRight.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnTopRight.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnTopRight.Selected = False
        Me.objbtnTopRight.ShowDefaultBorderColor = True
        Me.objbtnTopRight.Size = New System.Drawing.Size(38, 23)
        Me.objbtnTopRight.TabIndex = 2
        Me.objbtnTopRight.TabStop = False
        Me.objbtnTopRight.Tag = "4"
        Me.objbtnTopRight.UseVisualStyleBackColor = True
        '
        'objbtnTopLeft
        '
        Me.objbtnTopLeft.BackColor = System.Drawing.Color.White
        Me.objbtnTopLeft.BackgroundImage = CType(resources.GetObject("objbtnTopLeft.BackgroundImage"), System.Drawing.Image)
        Me.objbtnTopLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnTopLeft.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.objbtnTopLeft.FlatAppearance.BorderSize = 0
        Me.objbtnTopLeft.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnTopLeft.ForeColor = System.Drawing.Color.Black
        Me.objbtnTopLeft.GradientBackColor = System.Drawing.Color.LightSteelBlue
        Me.objbtnTopLeft.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnTopLeft.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnTopLeft.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnTopLeft.Location = New System.Drawing.Point(7, 6)
        Me.objbtnTopLeft.Name = "objbtnTopLeft"
        Me.objbtnTopLeft.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnTopLeft.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnTopLeft.Selected = False
        Me.objbtnTopLeft.ShowDefaultBorderColor = True
        Me.objbtnTopLeft.Size = New System.Drawing.Size(38, 23)
        Me.objbtnTopLeft.TabIndex = 1
        Me.objbtnTopLeft.TabStop = False
        Me.objbtnTopLeft.Tag = "1"
        Me.objbtnTopLeft.UseVisualStyleBackColor = True
        '
        'objbtnTopCenter
        '
        Me.objbtnTopCenter.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnTopCenter.BackColor = System.Drawing.Color.White
        Me.objbtnTopCenter.BackgroundImage = CType(resources.GetObject("objbtnTopCenter.BackgroundImage"), System.Drawing.Image)
        Me.objbtnTopCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnTopCenter.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.objbtnTopCenter.FlatAppearance.BorderSize = 0
        Me.objbtnTopCenter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnTopCenter.ForeColor = System.Drawing.Color.Black
        Me.objbtnTopCenter.GradientBackColor = System.Drawing.Color.LightSteelBlue
        Me.objbtnTopCenter.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnTopCenter.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnTopCenter.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnTopCenter.Location = New System.Drawing.Point(51, 6)
        Me.objbtnTopCenter.Name = "objbtnTopCenter"
        Me.objbtnTopCenter.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnTopCenter.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnTopCenter.Selected = False
        Me.objbtnTopCenter.ShowDefaultBorderColor = True
        Me.objbtnTopCenter.Size = New System.Drawing.Size(38, 23)
        Me.objbtnTopCenter.TabIndex = 0
        Me.objbtnTopCenter.TabStop = False
        Me.objbtnTopCenter.Tag = "2"
        Me.objbtnTopCenter.UseVisualStyleBackColor = True
        '
        'objbtnDropDown
        '
        Me.objbtnDropDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objbtnDropDown.BackColor = System.Drawing.Color.White
        Me.objbtnDropDown.BackgroundImage = CType(resources.GetObject("objbtnDropDown.BackgroundImage"), System.Drawing.Image)
        Me.objbtnDropDown.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.objbtnDropDown.BorderColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.objbtnDropDown.FlatAppearance.BorderSize = 0
        Me.objbtnDropDown.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.objbtnDropDown.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.objbtnDropDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.objbtnDropDown.Font = New System.Drawing.Font("Wingdings 3", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.objbtnDropDown.ForeColor = System.Drawing.Color.Black
        Me.objbtnDropDown.GradientBackColor = System.Drawing.Color.LightSteelBlue
        Me.objbtnDropDown.GradientForeColor = System.Drawing.Color.Black
        Me.objbtnDropDown.HoverGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDropDown.HoverGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDropDown.Location = New System.Drawing.Point(124, 0)
        Me.objbtnDropDown.Name = "objbtnDropDown"
        Me.objbtnDropDown.PressedGradientBackColor = System.Drawing.Color.Transparent
        Me.objbtnDropDown.PressedGradientForeColor = System.Drawing.Color.Black
        Me.objbtnDropDown.Selected = False
        Me.objbtnDropDown.ShowDefaultBorderColor = False
        Me.objbtnDropDown.Size = New System.Drawing.Size(16, 21)
        Me.objbtnDropDown.TabIndex = 1
        Me.objbtnDropDown.TabStop = False
        Me.objbtnDropDown.Text = "q"
        Me.objbtnDropDown.UseVisualStyleBackColor = False
        '
        'eZeeTextAlignControl
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.objbtnDropDown)
        Me.Controls.Add(Me.txtTextAlign)
        Me.Controls.Add(Me.pnlTextAlign)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "eZeeTextAlignControl"
        Me.Size = New System.Drawing.Size(140, 114)
        Me.pnlTextAlign.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtTextAlign As System.Windows.Forms.TextBox
    Friend WithEvents objbtnDropDown As eZeeLightButton
    Friend WithEvents pnlTextAlign As System.Windows.Forms.Panel
    Friend WithEvents objbtnBottomRight As eZeeLightButton
    Friend WithEvents objbtnBottomLeft As eZeeLightButton
    Friend WithEvents objbtnBottomCenter As eZeeLightButton
    Friend WithEvents objbtnMiddleRight As eZeeLightButton
    Friend WithEvents objbtnMiddleLeft As eZeeLightButton
    Friend WithEvents objbtnMiddleCenter As eZeeLightButton
    Friend WithEvents objbtnTopRight As eZeeLightButton
    Friend WithEvents objbtnTopLeft As eZeeLightButton
    Friend WithEvents objbtnTopCenter As eZeeLightButton

End Class
