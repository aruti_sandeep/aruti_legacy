Imports System
Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Reflection
Imports System.IO

Public Delegate Sub LanguageClickEventHandler(ByVal sender As Object, ByVal eventArgs As System.EventArgs)

'<System.Diagnostics.DebuggerStepThrough()> _
Public Class eZeeForm
    Inherits Form

#Region " Variable & API Declaration "

    ' Property Variables "
    Private mimgLanguageNormal As Image = Nothing
    Private mimgLanguageHover As Image = Nothing
    Private mimgLanguagePressed As Image = Nothing

    Private mblnShowLanguage As Boolean = True

    Private mintLeftLanguageButton As Integer = 0

    'Friend WithEvents objbtnLanguage As Button

    ' The state of our little button
    Private _buttState As LanguageButtonState = LanguageButtonState.Normal
    Private _buttPosition As New Rectangle()

    <Runtime.InteropServices.DllImport("user32.dll")> _
    Private Shared Function GetWindowDC(ByVal hWnd As IntPtr) As IntPtr
    End Function

    <Runtime.InteropServices.DllImport("user32.dll")> _
    Private Shared Function GetWindowRect(ByVal hWnd As IntPtr, ByRef lpRect As Rectangle) As Integer
    End Function

    <Runtime.InteropServices.DllImport("user32.dll")> _
    Private Shared Function ReleaseDC(ByVal hWnd As IntPtr, ByVal hDC As IntPtr) As Integer
    End Function

    Public Enum LanguageButtonState
        Normal = 0
        Pressed = 1
        Hover = 2
        Selected = 3
    End Enum

#End Region

#Region " Constructor "
    <System.Diagnostics.DebuggerNonUserCode()> _
     Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Required for Windows.Forms Class Composition Designer support
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        Me.LanguageNormal = My.Resources.Language_Normal_21
        Me.LanguageHover = My.Resources.Language_Hover_21
        Me.LanguagePressed = My.Resources.Language_Pressed_21
    End Sub

#End Region

#Region " Event Declaration "

    <EditorBrowsable(EditorBrowsableState.Always)> _
    Public Event LanguageClick As LanguageClickEventHandler

#End Region

#Region " Properties "

    <Browsable(False)> _
    Public Property LanguageNormal() As Image
        Get
            Return mimgLanguageNormal
        End Get
        Set(ByVal value As Image)
            mimgLanguageNormal = value
        End Set
    End Property

    <Browsable(False)> _
    Public Property LanguageHover() As Image
        Get
            Return mimgLanguageHover
        End Get
        Set(ByVal value As Image)
            mimgLanguageHover = value
        End Set
    End Property

    <Browsable(False)> _
    Public Property LanguagePressed() As Image
        Get
            Return mimgLanguagePressed
        End Get
        Set(ByVal value As Image)
            mimgLanguagePressed = value
        End Set
    End Property

    Public Property ShowLanguageButton() As Boolean
        Get
            Return mblnShowLanguage
        End Get
        Set(ByVal value As Boolean)
            mblnShowLanguage = value
            Me.Invalidate()
        End Set
    End Property

    Private mstrHelpTag As String = ""
    Public Property _HelpTag() As String
        Get
            Return mstrHelpTag
        End Get
        Set(ByVal value As String)
            mstrHelpTag = value
        End Set
    End Property

#End Region

#Region " Language "

    Private Sub DrawButton(ByVal hwnd As IntPtr)
        Dim hDC As IntPtr = GetWindowDC(hwnd)
        Dim x As Integer, y As Integer

        Using g As Graphics = Graphics.FromHdc(hDC)
            ' Work out size and positioning
            Dim CaptionHeight As Integer = Bounds.Height - ClientRectangle.Height
            Dim ButtonSize As Size = New Size(SystemInformation.CaptionButtonSize.Width - 3, SystemInformation.CaptionButtonSize.Height - 3) 'New Size(21, 21)
            'If gobjUser._RightToLeft Then
            '    x = CInt(5 + ButtonSize.Width)
            'Else
            'End If

            Dim dblWinButtonWidth As Double = 0

            'Krushna (FD GUI) (10 Aug 2009) -- Start
            If Me.ControlBox Then
                If Me.MaximizeBox Or Me.MinimizeBox Then
                    dblWinButtonWidth = 4.4
                Else
                    If Me.HelpButton Then
                        dblWinButtonWidth = 3.3
                    Else
                        dblWinButtonWidth = 2.3
                    End If
                End If
            Else
                dblWinButtonWidth = 1.3
            End If
            'Krushna (FD GUI) (10 Aug 2009) -- End

            x = CInt(Bounds.Width - (dblWinButtonWidth * ButtonSize.Width))

            y = CInt((CaptionHeight - ButtonSize.Height) / 2) ' - 1 '+1 1

            If Me.RightToLeftLayout And Me.RightToLeft = Windows.Forms.RightToLeft.Yes Then
                _buttPosition.Location = New Point(CInt(5 + ButtonSize.Width), y)
            Else
                _buttPosition.Location = New Point(CInt(Bounds.Width - (dblWinButtonWidth * ButtonSize.Width)), y)
            End If

            '_buttPosition.Location = New Point(x, y)

            'Dim start As New Point
            'start.X = ButtonSize.Width
            'start.Y = 0

            'Dim rect As Rectangle

            'rect = New Rectangle(start, New Point(start.X, ButtonSize.Height))
            'Dim gb As New LinearGradientBrush(rect, ControlPaint.Light(Drawing.Color.Green, 1.5F), Drawing.Color.Blue, LinearGradientMode.Vertical) 'Color.FromArgb(206, 223, 251)

            '' Work out color
            'Dim color As Brush
            'If _buttState = LanguageButtonState.Pushed Then
            '    color = Brushes.LightGreen
            'Else
            '    color = gb 'Brushes.Red
            'End If

            '' Draw our "button"
            ''g.FillRectangle(color, New Rectangle(x, y, ButtonSize.Width, ButtonSize.Height))

            'g.DrawLine(New Pen(Drawing.Color.White), x, y, x + ButtonSize.Width - 2, y) 'Up Border
            'g.DrawLine(New Pen(Drawing.Color.White), x, y + ButtonSize.Height - 1, x + ButtonSize.Width - 2, y + ButtonSize.Height - 1) 'Down Border
            'g.DrawLine(New Pen(Drawing.Color.White), x, y, x, y + ButtonSize.Height - 1) 'Left Border
            'g.DrawLine(New Pen(Drawing.Color.White), x + ButtonSize.Width - 1, y, x + ButtonSize.Width - 1, y + ButtonSize.Height - 1) 'Right Border

            'g.FillRectangle(color, x, y, ButtonSize.Width, ButtonSize.Height)

            'If _buttState = LanguageButtonState.Pushed Then
            '    Color = Brushes.LightGreen
            'Else
            '    Color = gb 'Brushes.Red
            'End If

            If _buttState = LanguageButtonState.Normal Then
                If Me.LanguageNormal IsNot Nothing Then
                    g.DrawImage(Me.LanguageNormal, x, y, ButtonSize.Width, ButtonSize.Height)
                End If
            ElseIf _buttState = LanguageButtonState.Pressed Then
                If Me.LanguagePressed IsNot Nothing Then
                    g.DrawImage(Me.LanguagePressed, x, y, ButtonSize.Width, ButtonSize.Height)
                End If
            ElseIf _buttState = LanguageButtonState.Hover Then
                If Me.LanguageHover IsNot Nothing Then
                    g.DrawImage(Me.LanguageHover, x, y, ButtonSize.Width, ButtonSize.Height)
                End If
            ElseIf _buttState = LanguageButtonState.Selected Then
                If Me.LanguageNormal IsNot Nothing Then
                    g.DrawImage(Me.LanguageNormal, x, y, ButtonSize.Width, ButtonSize.Height)
                End If
            End If

        End Using

        ReleaseDC(hwnd, hDC)
    End Sub

    Protected Overloads Overrides Sub WndProc(ByRef m As Message)
        If Me.ShowLanguageButton = False Then
            MyBase.WndProc(m)
            Exit Sub
        End If

        Dim x As Integer, y As Integer
        Dim windowRect As New Rectangle()
        GetWindowRect(m.HWnd, windowRect)

        Select Case m.Msg
            ' WM_NCPAINT
            ' WM_PAINT
            Case &H85, &HA
                MyBase.WndProc(m)

                DrawButton(m.HWnd)

                m.Result = IntPtr.Zero

                Exit Select

                ' WM_ACTIVATE
            Case &H86
                MyBase.WndProc(m)
                DrawButton(m.HWnd)

                Exit Select

                ' WM_NCMOUSEMOVE
            Case &HA0
                ' Extract the least significant 16 bits
                x = (CInt(m.LParam) << 16) >> 16
                ' Extract the most significant 16 bits
                y = CInt(m.LParam) >> 16

                x -= windowRect.Left
                y -= windowRect.Top

                MyBase.WndProc(m)

                If Not _buttPosition.Contains(New Point(x, y)) AndAlso _buttState = LanguageButtonState.Pressed Then
                    _buttState = LanguageButtonState.Normal
                    DrawButton(m.HWnd)
                End If

                Exit Select

                ' WM_NCLBUTTONDOWN
            Case &HA1
                ' Extract the least significant 16 bits
                x = (CInt(m.LParam) << 16) >> 16
                ' Extract the most significant 16 bits
                y = CInt(m.LParam) >> 16

                x -= windowRect.Left
                y -= windowRect.Top

                If _buttPosition.Contains(New Point(x, y)) Then
                    _buttState = LanguageButtonState.Pressed
                    DrawButton(m.HWnd)
                Else
                    MyBase.WndProc(m)
                End If

                Exit Select

                ' WM_NCLBUTTONUP
            Case &HA2
                ' Extract the least significant 16 bits
                x = (CInt(m.LParam) << 16) >> 16
                ' Extract the most significant 16 bits
                y = CInt(m.LParam) >> 16

                x -= windowRect.Left
                y -= windowRect.Top

                If _buttPosition.Contains(New Point(x, y)) AndAlso _buttState = LanguageButtonState.Hover Then
                    _buttState = LanguageButtonState.Normal
                    Call RaiseLanguageButton_Click(New Button, New EventArgs)
                    DrawButton(m.HWnd)
                Else
                    MyBase.WndProc(m)
                End If

                Exit Select

            Case &H84
                ' Extract the least significant 16 bits
                x = (CInt(m.LParam) << 16) >> 16
                ' Extract the most significant 16 bits
                y = CInt(m.LParam) >> 16

                x -= windowRect.Left
                y -= windowRect.Top

                If _buttPosition.Contains(New Point(x, y)) Then
                    m.Result = CType(18, IntPtr)
                    _buttState = LanguageButtonState.Hover
                    DrawButton(m.HWnd)
                Else
                    ' HTBORDER
                    _buttState = LanguageButtonState.Normal
                    DrawButton(m.HWnd)
                    MyBase.WndProc(m)
                End If

                Exit Select

            Case Else
                MyBase.WndProc(m)
                Exit Select
        End Select
    End Sub

    Private Sub RaiseLanguageButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        RaiseEvent LanguageClick(sender, e)
    End Sub
#End Region

#Region " Overrides Events "
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        _buttPosition.Size = New Size(SystemInformation.CaptionButtonSize.Width - 4, SystemInformation.CaptionButtonSize.Height - 4) 'New Size(21, 21) ' SystemInformation.CaptionButtonSize
    End Sub

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()
        MyBase.HelpButton = eZeeFrom_Configuration._ShowHelpButton
        Me.KeyPreview = True
    End Sub
#End Region

#Region " Help "
    Private Sub ShowHelp()
        If eZeeFrom_Configuration._ShowHelpButton Then
            If mstrHelpTag <> "" Then
                Help.ShowHelp(Me, eZeeFrom_Configuration._HelpFileName, mstrHelpTag & ".htm")
            Else
                Help.ShowHelp(Me, eZeeFrom_Configuration._HelpFileName, "Help_Not_Found.htm")
            End If
        End If
    End Sub

    Private Sub eZeeForm_HelpButtonClicked(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.HelpButtonClicked
        e.Cancel = True
        Call Me.ShowHelp()
    End Sub

    Private Sub eZeeForm_HelpRequested(ByVal sender As System.Object, ByVal hlpevent As System.Windows.Forms.HelpEventArgs) Handles MyBase.HelpRequested
        Call Me.ShowHelp()
    End Sub
#End Region

End Class

