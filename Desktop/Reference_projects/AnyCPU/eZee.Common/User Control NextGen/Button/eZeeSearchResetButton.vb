Imports System
Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms

Public Class eZeeSearchResetButton
    Inherits PictureBox

#Region " Construtor "

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        Me.Height = 24
        Me.Width = 24

        Me.BackColor = Color.Transparent
        Me.Text = ""
    End Sub

#End Region

#Region " Private Variables "

    Private strImage As String = "Search_24.png"
    Private strHoverImage As String = "Search_Hover_24.png"
    Private strPressedImage As String = "Search_Pressed_24.png"


    Private menButtonType As EnumButtonType = EnumButtonType.Search

    Public Enum EnumButtonType
        Search = 0
        Reset = 1
    End Enum

    'HRK - 19 Jul 2010 - Start
    Private mstrSearchMessage As String = ""
    Private mstrResultMessage As String = ""
    'HRK - 19 Jul 2010 - End

#End Region

#Region " Properties "
    <Browsable(False)> _
    <EditorBrowsable(EditorBrowsableState.Never)> _
    Public Shadows Property Image() As Image
        Get
            Return MyBase.Image
        End Get
        Set(ByVal value As Image)
            MyBase.Image = value
        End Set
    End Property

    Public Property ButtonType() As EnumButtonType
        Get
            Return menButtonType
        End Get
        Set(ByVal value As EnumButtonType)
            menButtonType = value

            Call SetProperty()

            Me.Invalidate()
        End Set
    End Property

    'HRK - 19 Jul 2010 - Start
    Public Property SearchMessage() As String
        Get
            Return mstrSearchMessage
        End Get
        Set(ByVal value As String)
            mstrSearchMessage = value
        End Set
    End Property

    Public Property ResultMessage() As String
        Get
            Return mstrResultMessage
        End Get
        Set(ByVal value As String)
            mstrResultMessage = value
        End Set
    End Property
    'HRK - 19 Jul 2010 - End

#End Region

#Region " Overrides Events "
    Private Sub SetProperty()
        Select Case menButtonType
            Case EnumButtonType.Reset
                strImage = "Reset_24.png"
                strHoverImage = "Reset_Hover_24.png"
                strPressedImage = "Reset_Pressed_24.png"
                Me.ToolTip1.SetToolTip(Me, Ui.Language.eZeeSearchResetButton.Caption_Reset)
            Case Else
                strImage = "Search_24.png"
                strHoverImage = "Search_Hover_24.png"
                strPressedImage = "Search_Pressed_24.png"
                Me.ToolTip1.SetToolTip(Me, Ui.Language.eZeeSearchResetButton.Caption_Search)
        End Select
        MyBase.Image = imgList.Images(strImage)
    End Sub

    Protected Overrides Sub OnCreateControl()
        MyBase.OnCreateControl()

        MyBase.Image = imgList.Images(strImage)
        Me.Text = ""
    End Sub

    Protected Overrides Sub OnMouseDown(ByVal mevent As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseDown(mevent)

        MyBase.Image = imgList.Images(strPressedImage)
    End Sub

    Protected Overrides Sub OnMouseUp(ByVal mevent As System.Windows.Forms.MouseEventArgs)
        MyBase.OnMouseUp(mevent)

        MyBase.Image = imgList.Images(strHoverImage)
    End Sub

    Protected Overrides Sub OnMouseEnter(ByVal e As System.EventArgs)
        MyBase.OnMouseEnter(e)

        MyBase.Image = imgList.Images(strHoverImage)
    End Sub

    Protected Overrides Sub OnMouseLeave(ByVal e As System.EventArgs)
        MyBase.OnMouseLeave(e)

        MyBase.Image = imgList.Images(strImage)
    End Sub

#End Region

    Public Overloads Sub PerformClick()
        If Me.Enabled AndAlso Me.Visible Then
            MyBase.OnClick(EventArgs.Empty)
        End If
    End Sub

    Private Sub eZeeSearchResetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Click
        'HRK - 19 Jul 2010 - Start
        If TypeOf Me.Parent Is eZeeCollapsibleContainer Then
            Dim parent As eZeeCollapsibleContainer = Me.Parent
            If mstrSearchMessage = "" Then
                parent.HeaderMessage = Ui.Language.eZeeSearchResetButton.SearchMessage
            Else
                parent.HeaderMessage = mstrSearchMessage
            End If
        End If
        'HRK - 19 Jul 2010 - End
    End Sub

    'HRK - 19 Jul 2010 - Start
    Public Sub ShowResult(ByVal strCount As String)
        'HRK - 19 Jul 2010 - Start
        If TypeOf Me.Parent Is eZeeCollapsibleContainer Then
            Dim parent As eZeeCollapsibleContainer = Me.Parent
            If mstrResultMessage = "" Then
                parent.HeaderMessage = Ui.Language.eZeeSearchResetButton.ResultMessage.Replace("#", strCount)
            Else
                parent.HeaderMessage = mstrResultMessage.Replace("#", strCount)
            End If
        End If
        'HRK - 19 Jul 2010 - End
    End Sub
    'HRK - 19 Jul 2010 - End

End Class
