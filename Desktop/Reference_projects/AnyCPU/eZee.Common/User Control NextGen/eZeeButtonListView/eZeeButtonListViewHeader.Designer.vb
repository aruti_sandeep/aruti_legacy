<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class eZeeButtonListViewHeader
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.objlblAddNew = New System.Windows.Forms.LinkLabel
        Me.objlblHeaderText = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'objlblAddNew
        '
        Me.objlblAddNew.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objlblAddNew.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline
        Me.objlblAddNew.Location = New System.Drawing.Point(147, 5)
        Me.objlblAddNew.Name = "objlblAddNew"
        Me.objlblAddNew.Size = New System.Drawing.Size(119, 13)
        Me.objlblAddNew.TabIndex = 0
        Me.objlblAddNew.TabStop = True
        Me.objlblAddNew.Text = "LinkLabel1"
        Me.objlblAddNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.objlblAddNew.UseCompatibleTextRendering = True
        '
        'objlblHeaderText
        '
        Me.objlblHeaderText.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.objlblHeaderText.Location = New System.Drawing.Point(5, 5)
        Me.objlblHeaderText.Name = "objlblHeaderText"
        Me.objlblHeaderText.Size = New System.Drawing.Size(136, 13)
        Me.objlblHeaderText.TabIndex = 1
        Me.objlblHeaderText.Text = "Label1"
        Me.objlblHeaderText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'eZeeButtonListViewHeader
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.AliceBlue
        Me.Controls.Add(Me.objlblHeaderText)
        Me.Controls.Add(Me.objlblAddNew)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(0)
        Me.Name = "eZeeButtonListViewHeader"
        Me.Size = New System.Drawing.Size(272, 23)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents objlblAddNew As System.Windows.Forms.LinkLabel
    Friend WithEvents objlblHeaderText As System.Windows.Forms.Label

End Class
