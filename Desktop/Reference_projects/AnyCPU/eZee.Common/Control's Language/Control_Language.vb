Namespace Ui.Language
    Public Class ListView
        Private Shared mstrShowAllCaption As String = "Show &All"

        Private Shared mstrCaption_More As String = "&More..."
        Private Shared mstrCaption_Save As String = "&Save"
        Private Shared mstrCaption_SizeAllColumnsToFit As String = "Size All Columns To &Fit"

        Public Shared Property Caption_ShowAll() As String
            Get
                Return mstrShowAllCaption
            End Get
            Set(ByVal value As String)
                mstrShowAllCaption = value
            End Set
        End Property

        Public Shared Property Caption_More() As String
            Get
                Return mstrCaption_More
            End Get
            Set(ByVal value As String)
                mstrCaption_More = value
            End Set
        End Property

        Public Shared Property Caption_Save() As String
            Get
                Return mstrCaption_Save
            End Get
            Set(ByVal value As String)
                mstrCaption_Save = value
            End Set
        End Property

        Public Shared Property Caption_SizeAllColumnsToFit() As String
            Get
                Return mstrCaption_SizeAllColumnsToFit
            End Get
            Set(ByVal value As String)
                mstrCaption_SizeAllColumnsToFit = value
            End Set
        End Property
    End Class

    Public Class eZeeSearchResetButton
        Private Shared mstrCaption_Search As String = "Search"
        Private Shared mstrCaption_Reset As String = "Reset"
        'HRK - 19 Jul 2010 - Start
        Private Shared mstrSearchMessage As String = "[ Searching... ]"
        Private Shared mstrResultMessage As String = "[ # Records Found ]"
        'HRK - 19 Jul 2010 - End

        Public Shared Property Caption_Search() As String
            Get
                Return mstrCaption_Search
            End Get
            Set(ByVal value As String)
                mstrCaption_Search = value
            End Set
        End Property

        Public Shared Property Caption_Reset() As String
            Get
                Return mstrCaption_Reset
            End Get
            Set(ByVal value As String)
                mstrCaption_Reset = value
            End Set
        End Property

        'HRK - 19 Jul 2010 - Start
        Public Shared Property SearchMessage() As String
            Get
                Return mstrSearchMessage
            End Get
            Set(ByVal value As String)
                mstrSearchMessage = value
            End Set
        End Property
        Public Shared Property ResultMessage() As String
            Get
                Return mstrResultMessage
            End Get
            Set(ByVal value As String)
                mstrResultMessage = value
            End Set
        End Property
        'HRK - 19 Jul 2010 - End

    End Class
End Namespace