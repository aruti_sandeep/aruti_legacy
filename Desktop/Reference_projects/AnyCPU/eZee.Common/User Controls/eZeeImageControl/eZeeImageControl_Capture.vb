Imports System.Runtime.InteropServices
Imports System.Drawing
Imports system.Windows.Forms


Public Class eZeeImageControl_Capture
    Private ReadOnly mstrModuleName As String = "eZeeImageControl_Capture"

#Region " Capture Image API "
    Private Const WM_CAP As Short = &H400S

    Private Const WM_CAP_DRIVER_CONNECT As Integer = WM_CAP + 10
    Private Const WM_CAP_DRIVER_DISCONNECT As Integer = WM_CAP + 11
    Private Const WM_CAP_EDIT_COPY As Integer = WM_CAP + 30

    Private Const WM_CAP_SET_PREVIEW As Integer = WM_CAP + 50
    Private Const WM_CAP_SET_PREVIEWRATE As Integer = WM_CAP + 52
    Private Const WM_CAP_SET_SCALE As Integer = WM_CAP + 53
    Private Const WS_CHILD As Integer = &H40000000
    Private Const WS_VISIBLE As Integer = &H10000000
    Private Const SWP_NOMOVE As Short = &H2S
    Private Const SWP_NOSIZE As Short = 1
    Private Const SWP_NOZORDER As Short = &H4S
    Private Const HWND_BOTTOM As Short = 1

    Dim iDevice As Integer = 0 ' Current device ID
    Dim hHwnd As Integer ' Handle to preview window

    Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
        (ByVal hwnd As Integer, ByVal wMsg As Integer, ByVal wParam As Integer, _
        <MarshalAs(UnmanagedType.AsAny)> ByVal lParam As Object) As Integer

    Declare Function SetWindowPos Lib "user32" Alias "SetWindowPos" (ByVal hwnd As Integer, _
        ByVal hWndInsertAfter As Integer, ByVal x As Integer, ByVal y As Integer, _
        ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer) As Integer

    Declare Function DestroyWindow Lib "user32" (ByVal hndw As Integer) As Boolean

    Declare Function capCreateCaptureWindowA Lib "avicap32.dll" _
        (ByVal lpszWindowName As String, ByVal dwStyle As Integer, _
        ByVal x As Integer, ByVal y As Integer, ByVal nWidth As Integer, _
        ByVal nHeight As Short, ByVal hWndParent As Integer, _
        ByVal nID As Integer) As Integer

    Declare Function capGetDriverDescriptionA Lib "avicap32.dll" (ByVal wDriver As Short, _
        ByVal lpszName As String, ByVal cbName As Integer, ByVal lpszVer As String, _
        ByVal cbVer As Integer) As Boolean
#End Region
    

    Private mblnCancel As Boolean = True

#Region " Property "
    Dim m_Image As Image = Nothing
    Public Property _Image() As Image
        Get
            Return m_Image
        End Get
        Set(ByVal value As Image)
            m_Image = value
        End Set
    End Property
#End Region

#Region " Public Method "
    Public Function DisplayDialog() As Boolean
        Me.ShowDialog()
        Return Not mblnCancel
    End Function
#End Region

#Region " Private Function "
    Private Sub LoadDeviceList()
        Dim strName As String = Space(100)
        Dim strVer As String = Space(100)
        Dim bReturn As Boolean
        Dim x As Integer = 0

        Do
            bReturn = capGetDriverDescriptionA(x, strName, 100, strVer, 100)
            If bReturn Then lstDevices.Items.Add(strName.Trim)
            x += 1
        Loop Until bReturn = False
    End Sub

    Private Sub OpenPreviewWindow()
        Dim iHeight As Integer = picCapture.Height
        Dim iWidth As Integer = picCapture.Width

        ' Open Preview window in picturebox
        hHwnd = capCreateCaptureWindowA(iDevice, WS_VISIBLE Or WS_CHILD, 0, 0, 640, _
            480, picCapture.Handle.ToInt32, 0)

        ' Connect to device
        If SendMessage(hHwnd, WM_CAP_DRIVER_CONNECT, iDevice, 0) Then
            'Set the preview scale
            SendMessage(hHwnd, WM_CAP_SET_SCALE, True, 0)

            'Set the preview rate in milliseconds
            SendMessage(hHwnd, WM_CAP_SET_PREVIEWRATE, 66, 0)

            'Start previewing the image from the camera
            SendMessage(hHwnd, WM_CAP_SET_PREVIEW, True, 0)

            ' Resize window to fit in picturebox
            SetWindowPos(hHwnd, HWND_BOTTOM, 0, 0, picCapture.Width, picCapture.Height, _
                    SWP_NOMOVE Or SWP_NOZORDER)

            objbtnCapture.Enabled = True
            objbtnStop.Enabled = True
            objbtnStart.Enabled = False

        Else
            ' Error connecting to device close window
            DestroyWindow(hHwnd)

            objbtnCapture.Enabled = False
            objbtnStop.Enabled = False
            objbtnStart.Enabled = True
        End If
    End Sub

    Private Sub ClosePreviewWindow()
        ' Disconnect from device
        SendMessage(hHwnd, WM_CAP_DRIVER_DISCONNECT, iDevice, 0)
        ' close window
        DestroyWindow(hHwnd)
        objbtnCapture.Enabled = False
        objbtnStop.Enabled = False
        objbtnStart.Enabled = True
    End Sub

    Private Sub setImage(ByVal Img As Image)

        If Img Is Nothing Then Exit Sub

        If Img.Size.Height > 100 Or Img.Size.Width > 100 Then
            picCapture.SizeMode = PictureBoxSizeMode.StretchImage
        Else
            picCapture.SizeMode = PictureBoxSizeMode.CenterImage
        End If

        picCapture.Image = Img
    End Sub
#End Region

#Region " Form "
    Private Sub frmVideo_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs)
        If objbtnStop.Enabled Then
            ClosePreviewWindow()
        End If
    End Sub

    Private Sub frmVideo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not m_Image Is Nothing Then
            picCapture.Image = m_Image
        End If

        Call LoadDeviceList()

        If lstDevices.Items.Count > 0 Then
            objbtnStart.Enabled = True
            lstDevices.SelectedIndex = 0
        Else
            lstDevices.Items.Add("Web Camera is not attached with this computer.")
        End If

        Call setImage(m_Image)
    End Sub
#End Region

#Region " Buttons "
    Private Sub objbtnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnStart.Click
        iDevice = lstDevices.SelectedIndex
        Call OpenPreviewWindow()
    End Sub

    Private Sub objbtnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnStop.Click
        ClosePreviewWindow()
    End Sub

    Private Sub objbtnCapture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnCapture.Click
        Dim data As IDataObject
        Dim bmap As Image

        ' Copy image to clipboard
        SendMessage(hHwnd, WM_CAP_EDIT_COPY, 0, 0)

        ' Get image from clipboard and convert it to a bitmap
        data = Clipboard.GetDataObject()
        If data.GetDataPresent(GetType(System.Drawing.Bitmap)) Then
            bmap = CType(data.GetData(GetType(System.Drawing.Bitmap)), Image)
            Call ClosePreviewWindow()

            Call setImage(bmap)
        End If
    End Sub

    Private Sub objbtnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnCancel.Click
        Me.Close()
    End Sub

    Private Sub objbtnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles objbtnOk.Click
        mblnCancel = False
        m_Image = picCapture.Image
        Me.Close()
    End Sub
#End Region

    
End Class