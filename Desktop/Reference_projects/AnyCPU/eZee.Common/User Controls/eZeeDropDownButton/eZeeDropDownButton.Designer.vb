Imports Microsoft.VisualBasic
Imports System

Partial Public Class eZeeDropDownButton
    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private components As System.ComponentModel.IContainer = Nothing

    ''' <summary>
    ''' Clean up any resources being used.
    ''' </summary>
    ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not imageButton Is Nothing Then
                imageButton.Parent.Dispose()
                imageButton.Parent = Nothing
                imageButton.Dispose()
                imageButton = Nothing
            End If
            If Not x_timer Is Nothing Then
                x_timer.Stop()
            End If
            DestroyFrames()
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region "Component Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify 
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.x_timer = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        ' 
        ' timer
        ' 
        '			Me.timer.Tick += New System.EventHandler(Me.timer_Tick);
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private WithEvents x_timer As System.Windows.Forms.Timer
End Class
