'************************************************************************************************************************************
'Class Name : eZeeImageCombo.vb
'Purpose    : 
'Date       : 18 July 2007
'Written By : Naimish
'Modified   :	
'************************************************************************************************************************************

Imports System
Imports System.Drawing
Imports System.ComponentModel

Public Class eZeeImageCombo
    Inherits Windows.Forms.ComboBox

    Public Sub New()
        Me.DrawMode = Windows.Forms.DrawMode.OwnerDrawFixed
    End Sub

    <Browsable(False)> _
    Public Property SelectedTag() As Object
        Get
            If Me.SelectedItem = Nothing Then
                Return Nothing
            Else
                Return Me.SelectedItem.tag
            End If
        End Get
        Set(ByVal value As Object)
            If value = Nothing Then Exit Property
            Me.SelectedIndex = 0
            For Each obj As ComboBoxItem In Me.Items
                If obj.Tag.ToString = value.ToString Then
                    Me.SelectedItem = obj
                    Exit For
                End If
            Next
        End Set
    End Property

    Protected Overrides Sub OnDrawItem(ByVal e As System.Windows.Forms.DrawItemEventArgs)
        e.DrawBackground()
        e.DrawFocusRectangle()

        Dim cboItem As New ComboBoxItem
        Dim imageSize As New Size(e.Bounds.Height, e.Bounds.Height)

        Dim bounds As New Rectangle
        bounds = e.Bounds

        Try
            If e.Index < 0 Then
                e.Graphics.DrawString(cboItem.Text, e.Font, New SolidBrush(e.ForeColor), bounds.Left, bounds.Top)
                Exit Sub
            End If

            If Not Me.Items(e.Index).GetType() Is GetType(ComboBoxItem) Then
                e.Graphics.DrawString(Me.Items(e.Index).ToString(), e.Font, New SolidBrush(e.ForeColor), e.Bounds.Left + imageSize.Width, e.Bounds.Top)
                Exit Sub
            End If

            'Draw custom item
            cboItem = CType(Me.Items(e.Index), ComboBoxItem)

            Dim forecolor As Color
            If (cboItem.ForeColor = Color.FromKnownColor(KnownColor.Transparent)) Then
                forecolor = e.ForeColor
            Else
                forecolor = cboItem.ForeColor
            End If

            Dim font As Font
            If cboItem.Mark Then
                font = New Font(e.Font, FontStyle.Bold)
            Else
                font = e.Font
            End If

            If Not cboItem.Image Is Nothing Then
                e.Graphics.DrawImage(cboItem.Image, New Rectangle(e.Bounds.Left, e.Bounds.Top, 16, 16))
            End If

            'Draw Text
            e.Graphics.DrawString(cboItem.Text, font, New SolidBrush(forecolor), e.Bounds.Left + imageSize.Width, e.Bounds.Top)

        Catch ex As Exception
            If (e.Index <> -1) Then
                e.Graphics.DrawString(Items(e.Index).ToString(), e.Font, New SolidBrush(e.ForeColor), bounds.Left, bounds.Top)
            Else
                e.Graphics.DrawString(Text, e.Font, New SolidBrush(e.ForeColor), bounds.Left, bounds.Top)
            End If
        End Try

        MyBase.OnDrawItem(e)
    End Sub
End Class

Public Class ComboBoxItem

#Region " Property Variables "
    Private _text As String
    Private _Image As Image = Nothing

    Private _ForeColor_Renamed As Color = Color.FromKnownColor(KnownColor.Transparent)
    Private _Mark_Renamed As Boolean = False

    Private _Tag As Object = Nothing
#End Region

#Region " Properties "
    Property Text() As String
        Get
            Return _text
        End Get
        Set(ByVal Value As String)
            _text = Value
        End Set
    End Property

    Public Property Image() As Image
        Get
            Return _Image
        End Get
        Set(ByVal value As Image)
            _Image = value
        End Set
    End Property

    ' forecolor
    Public Property ForeColor() As Color
        Get
            Return _ForeColor_Renamed
        End Get
        Set(ByVal value As Color)
            _ForeColor_Renamed = value
        End Set
    End Property

    ' mark (bold)
    Public Property Mark() As Boolean
        Get
            Return _Mark_Renamed
        End Get
        Set(ByVal value As Boolean)
            _Mark_Renamed = value
        End Set
    End Property

    ' tag
    Public Property Tag() As Object
        Get
            Return _Tag
        End Get
        Set(ByVal value As Object)
            _Tag = value
        End Set
    End Property

#End Region

#Region " Constructors "

    Public Sub New()
    End Sub

    Public Sub New(ByVal Text As String)
        Me._text = Text
    End Sub

    Public Sub New(ByVal Text As String, ByVal Image As Image)
        Me._text = Text
        Me._Image = Image
    End Sub

    Public Sub New(ByVal Text As String, ByVal Image As Image, ByVal Tag As Object)
        Me._text = Text
        Me._Image = Image
        Me._Tag = Tag
    End Sub

    Public Sub New(ByVal Text As String, ByVal Image As Image, ByVal Mark As Boolean)
        Me._text = Text
        Me._Image = Image
        Me._Mark_Renamed = Mark
    End Sub

    Public Sub New(ByVal Text As String, ByVal Image As Image, ByVal Mark As Boolean, ByVal ForeColor As Color)
        Me._text = Text
        Me._Image = Image
        Me._Mark_Renamed = Mark
        Me._ForeColor_Renamed = ForeColor
    End Sub

    Public Sub New(ByVal Text As String, ByVal Image As Image, ByVal Mark As Boolean, ByVal ForeColor As Color, ByVal Tag As Object)
        Me._text = Text
        Me._Image = Image
        Me._Mark_Renamed = Mark
        Me._ForeColor_Renamed = ForeColor
        Me._Tag = Tag
    End Sub

#End Region

    Public Overrides Function ToString() As String
        Return _text
    End Function

End Class
