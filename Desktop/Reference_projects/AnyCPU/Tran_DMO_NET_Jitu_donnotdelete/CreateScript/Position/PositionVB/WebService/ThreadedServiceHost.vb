Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Threading
Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.ServiceModel.Description

Namespace PositionTrackerPC.WebService
	''' <summary>
	''' Class to house the thread for the webservice.
	''' Part of this code is borrowed from CodeProject and slighty modified
	''' to make singleton service classes possible
	''' </summary>
	''' <typeparam name="TService">implementing class of the webservice</typeparam>
	''' <typeparam name="TContract">contract interface of the webservice</typeparam>
    'Friend Class ThreadedServiceHost(Of TService, TContract)
    '	Private Const SleepTime As Integer = 100

    '	Private _serviceHost As ServiceHost = Nothing
    '	Private _thread As Thread
    '	Private _serviceAddress As String
    '	Private _endpointAddress As String
    '	Private _running As Boolean
    '	Private _singletonInstance As Object = Nothing

    '	''' <summary>
    '	''' Constructor without singleton
    '	''' </summary>
    '	''' <param name="serviceAddress">the address of the service</param>
    '	''' <param name="endpointAddress">the address of the endpoint</param>
    '	Public Sub New(ByVal serviceAddress As String, ByVal endpointAddress As String)
    '		_serviceAddress = serviceAddress
    '		_endpointAddress = endpointAddress

    '		' start a new thread to host the service
    '		_thread = New Thread(New ThreadStart(AddressOf ThreadMethod))
    '		_thread.Start()
    '	End Sub

    '	''' <summary>
    '	''' Constructor with singleton
    '	''' </summary>
    '	''' <param name="singletonInstance">singleton class instance</param>
    '	''' <param name="serviceAddress">the address of the service</param>
    '	''' <param name="endpointAddress">the address of the endpoint</param>
    '	Public Sub New(ByVal singletonInstance As Object, ByVal serviceAddress As String, ByVal endpointAddress As String)
    '		_singletonInstance = singletonInstance
    '		_serviceAddress = serviceAddress
    '		_endpointAddress = endpointAddress

    '		' start a new thread to house the service
    '		_thread = New Thread(New ThreadStart(AddressOf ThreadMethod))
    '		_thread.Start()
    '	End Sub

    '	''' <summary>
    '	''' The method that implements the thread
    '	''' </summary>
    '	Private Sub ThreadMethod()
    '		Try
    '			' Indicate we are running
    '			_running = True

    '			' Construct the host, with either the singleton or the type
    '			If _singletonInstance IsNot Nothing Then
    '				_serviceHost = New ServiceHost(_singletonInstance, New Uri(_serviceAddress))
    '			Else
    '				_serviceHost = New ServiceHost(GetType(TService), New Uri(_serviceAddress))
    '			End If

    '			' Add service endpoint, and publish metadata (easy to consume ...)
    '			_serviceHost.AddServiceEndpoint(GetType(TContract), New BasicHttpBinding(), _endpointAddress)
    '			Dim smb As New ServiceMetadataBehavior()
    '			smb.HttpGetEnabled = True

    '			_serviceHost.Description.Behaviors.Add(smb)
    '			_serviceHost.Open()

    '			Do While _running
    '				' Wait until thread is stopped
    '				Thread.Sleep(SleepTime)
    '			Loop

    '			' Stop the host
    '			_serviceHost.Close()
    '		Catch e1 As Exception
    '			If _serviceHost IsNot Nothing Then
    '				_serviceHost.Close()
    '			End If
    '		End Try
    '	End Sub

    '	''' <summary>
    '	''' Request the end of the thread method.
    '	''' </summary>
    '	Public Sub [Stop]()
    '		SyncLock Me
    '			_running = False
    '		End SyncLock
    '	End Sub
    'End Class
End Namespace
