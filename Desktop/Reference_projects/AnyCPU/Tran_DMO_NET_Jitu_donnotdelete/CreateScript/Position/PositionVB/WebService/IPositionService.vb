Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
'using System.Linq;
Imports System.Text
Imports System.ServiceModel

Namespace PositionTrackerPC.WebService
	'[ServiceContract(Namespace="http://bruins/positionservice")]
	Public Interface IPositionService
		'[OperationContract]
		Function SendPosition(ByVal latitude As Double, ByVal longitude As Double, ByVal remarks As String) As Boolean
	End Interface
End Namespace
