Imports Microsoft.VisualBasic
Imports System.IO
Imports System.Reflection
Imports System.Xml.Serialization

Namespace PositionTrackerPC.Configuration
	''' <summary>
	''' Class to hold application configuration information
	''' It is able to serialize itself in the same location as
	''' the executing assembly
	''' </summary>
	Public Class AppConfiguration
		Private _webServiceHostName As String = String.Empty
		Private _portNumber As Integer = 8081
		Private _GoogleAPIKey As String = ""

		''' <summary>
		''' Holds Google static maps API key
		''' </summary>
		Public Property GoogleMapAPIKey() As String
			Get
				Return _GoogleAPIKey
			End Get
			Set(ByVal value As String)
				_GoogleAPIKey = value
			End Set
		End Property

		''' <summary>
		''' Hostname of the machine where the webservice is ran
		''' </summary>
		Public Property WebServiceHostName() As String
			Get
				Return _webServiceHostName
			End Get
			Set(ByVal value As String)
				_webServiceHostName = value
			End Set
		End Property

		''' <summary>
		''' Portnumber at which the webservice is listening
		''' </summary>
		Public Property PortNumber() As Integer
			Get
				Return _portNumber
			End Get
			Set(ByVal value As Integer)
				_portNumber = value
			End Set
		End Property

		''' <summary>
		''' Gets the application configuration from disk
		''' </summary>
		''' <returns>AppConfiguration instance</returns>
		Public Shared Function ApplicationConfiguration() As AppConfiguration
			Dim cfg As AppConfiguration = Nothing

			' assemble filename
			Dim rootPath As String = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
			rootPath &= Path.DirectorySeparatorChar & "AppConfig.xml"

			If rootPath.StartsWith("file:\") Then
				rootPath = rootPath.Substring("file:\".Length)
			End If

			Try
				' serialize from XML
				Dim serializer As New XmlSerializer(GetType(AppConfiguration))
				Dim reader As New StreamReader(rootPath)
				cfg = CType(serializer.Deserialize(reader), AppConfiguration)
				reader.Close()
			Catch
				' if not found, assembly one with default values
				cfg = New AppConfiguration()
				cfg.PortNumber = 8081
				cfg.WebServiceHostName = ""
				cfg.GoogleMapAPIKey = ""
			End Try
			Return cfg
		End Function

		''' <summary>
		''' Saves the configuration to disk
		''' </summary>
		Public Sub Save()
			' assmble the filename
			Dim rootPath As String = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
			rootPath &= Path.DirectorySeparatorChar & "AppConfig.xml"

			If rootPath.StartsWith("file:\") Then
				rootPath = rootPath.Substring("file:\".Length)
			End If

			Try
				' serialize using  XML
				Dim serializer As New XmlSerializer(GetType(AppConfiguration))
				Dim writer As New StreamWriter(rootPath)
				serializer.Serialize(writer, Me)
				writer.Close()
			Catch
			End Try
		End Sub
	End Class
End Namespace
