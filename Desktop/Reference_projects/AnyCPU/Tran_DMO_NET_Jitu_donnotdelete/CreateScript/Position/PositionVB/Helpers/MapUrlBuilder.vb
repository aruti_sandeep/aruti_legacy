Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
'using System.Linq;
Imports System.Text
Imports System.Globalization

Namespace PositionTrackerPC.Helpers
	Public Class MapUrlBuilder
		Private _coordinate As New Coordinate()
		Private _zoomLevel As Integer = 17
		Private _mapType As String = "roadmap"
		Private _apiKey As String = String.Empty

		#Region "public properties"
		''' <summary>
		''' Coordinate to use in url
		''' </summary>
		Public Property CenterCoordinate() As Coordinate
			Get
				Return _coordinate
			End Get
			Set(ByVal value As Coordinate)
				_coordinate = value
			End Set
		End Property

		''' <summary>
		''' Zoomlevel to use in url
		''' </summary>
		Public Property ZoomLevel() As Integer
			Get
				Return _zoomLevel
			End Get
			Set(ByVal value As Integer)
				_zoomLevel = value
			End Set
		End Property

		''' <summary>
		''' Maptype to use in url
		''' </summary>
		Public Property MapType() As String
			Get
				Return _mapType
			End Get
			Set(ByVal value As String)
				_mapType = value
			End Set
		End Property

		''' <summary>
		'''  API key to use in url
		''' </summary>
		Public Property GoogleMapsAPIKey() As String
			Get
				Return _apiKey
			End Get
			Set(ByVal value As String)
				_apiKey = value
			End Set
		End Property
		#End Region

		''' <summary>
		''' The resulting url
		''' </summary>
		Public ReadOnly Property MapUrl() As String
			Get
				Return String.Format(CultureInfo.InvariantCulture, "http://maps.google.com/staticmap?center={0},{1}&size=512x512&markers={0},{1},greenc&zoom={2}&maptype={3}&key={4}", _coordinate.Latitude, _coordinate.Longitude, _zoomLevel, _mapType, _apiKey)
			End Get
		End Property

		''' <summary>
		'''  Default string representation
		''' </summary>
		''' <returns></returns>
		Public Overrides Function ToString() As String
			Return MapUrl
		End Function
	End Class
End Namespace
