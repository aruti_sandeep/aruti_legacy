Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
'using System.Linq;
Imports System.Text
Imports PositionTrackerPC.Helpers

Namespace PositionTrackerPC.Helpers
	''' <summary>
	''' Helper class to display and remember locations visited in one session
	''' </summary>
	Public Class ShowLocation
		Private _location As Coordinate = Nothing
		Private _remarks As String = String.Empty
		Private _timing As DateTime = DateTime.Now

		''' <summary>
		''' Location coordinates
		''' </summary>
		Public Property Location() As Coordinate
			Get
				Return _location
			End Get
			Set(ByVal value As Coordinate)
				_location = value
			End Set
		End Property

		''' <summary>
		''' Remarks for this location
		''' </summary>
		Public Property Remarks() As String
			Get
				Return _remarks
			End Get
			Set(ByVal value As String)
				_remarks = value
			End Set
		End Property

		''' <summary>
		''' Time this location was visited
		''' </summary>
		Public Property Time() As DateTime
			Get
				Return _timing
			End Get
			Set(ByVal value As DateTime)
				_timing = value
			End Set
		End Property

		' Override to show time in combobox
		Public Overrides Function ToString() As String
			Return Time.ToShortTimeString()
		End Function
	End Class
End Namespace
