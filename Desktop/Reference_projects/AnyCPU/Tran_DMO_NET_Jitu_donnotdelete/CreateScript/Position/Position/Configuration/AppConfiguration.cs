﻿using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace PositionTrackerPC.Configuration
{
    /// <summary>
    /// Class to hold application configuration information
    /// It is able to serialize itself in the same location as
    /// the executing assembly
    /// </summary>
    public class AppConfiguration
    {
        private string _webServiceHostName = string.Empty;
        private int _portNumber = 8081;
        private string _GoogleAPIKey = "";

        /// <summary>
        /// Holds Google static maps API key
        /// </summary>
        public string GoogleMapAPIKey
        {
            get
            {
                return _GoogleAPIKey;
            }
            set
            {
                _GoogleAPIKey = value;
            }
        }

        /// <summary>
        /// Hostname of the machine where the webservice is ran
        /// </summary>
        public string WebServiceHostName
        {
            get
            {
                return _webServiceHostName;
            }
            set
            {
                _webServiceHostName = value;
            }
        }

        /// <summary>
        /// Portnumber at which the webservice is listening
        /// </summary>
        public int PortNumber
        {
            get
            {
                return _portNumber;
            }
            set
            {
                _portNumber = value;
            }
        }

        /// <summary>
        /// Gets the application configuration from disk
        /// </summary>
        /// <returns>AppConfiguration instance</returns>
        public static AppConfiguration ApplicationConfiguration()
        {
            AppConfiguration cfg = null;

            // assemble filename
            string rootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            rootPath += Path.DirectorySeparatorChar + "AppConfig.xml";

            if (rootPath.StartsWith("file:\\"))
            {
                rootPath = rootPath.Substring("file:\\".Length);
            }

            try
            {
                // serialize from XML
                XmlSerializer serializer = new XmlSerializer(typeof(AppConfiguration));
                StreamReader reader = new StreamReader(rootPath);
                cfg = (AppConfiguration)serializer.Deserialize(reader);
                reader.Close();
            }
            catch
            {
                // if not found, assembly one with default values
                cfg = new AppConfiguration();
                cfg.PortNumber = 8081;
                cfg.WebServiceHostName = "";
                cfg.GoogleMapAPIKey = "";
            }
            return cfg;
        }

        /// <summary>
        /// Saves the configuration to disk
        /// </summary>
        public void Save()
        {
            // assmble the filename
            string rootPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            rootPath += Path.DirectorySeparatorChar + "AppConfig.xml";

            if (rootPath.StartsWith("file:\\"))
            {
                rootPath = rootPath.Substring("file:\\".Length);
            }

            try
            {
                // serialize using  XML
                XmlSerializer serializer = new XmlSerializer(typeof(AppConfiguration));
                StreamWriter writer = new StreamWriter(rootPath);
                serializer.Serialize(writer, this);
                writer.Close();
            }
            catch
            {
            }
        }
    }
}
