﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.ServiceModel;
using PositionTrackerPC.Helpers;

namespace PositionTrackerPC.WebService
{
    /// <summary>
    /// Implementing class of the webservice
    /// This one is used as a singleton, to make it possible
    /// to communicate with the "outside" code
    /// </summary>
    //[ServiceBehaviorAttribute(InstanceContextMode=InstanceContextMode.Single)]
    public class PositionService : IPositionService
    {
        /// <summary>
        /// Event which is fired when the service was called (from the outside world)
        /// </summary>
        public event EventHandler<PositionChangedEventArgs> PositionChanged;

        #region IPositionService Members
        /// <summary>
        /// Implementing member of this webservice
        /// </summary>
        /// <param name="latitude">latitude component of position</param>
        /// <param name="longitude">longitude component of position</param>
        /// <param name="remarks">remarks for this position</param>
        /// <returns>whether it succeeded</returns>
        public bool SendPosition(double latitude, double longitude, string remarks)
        {
            // Fire the event to signify subscribers
            Coordinate position = new Coordinate(latitude, longitude);
            OnPositionChanged(position, remarks);
            return true;
        }
        #endregion

        /// <summary>
        /// Event fire method
        /// </summary>
        /// <param name="Position">position</param>
        /// <param name="remarks">remarks</param>
        private void OnPositionChanged(Coordinate Position, string remarks)
        {
            // only fire when there are subscribers
            if (PositionChanged != null)
            {
                // construct arguments
                PositionChangedEventArgs args = new PositionChangedEventArgs(Position, remarks);
                // fire
                PositionChanged(this, args);
            }
        }
    }

    /// <summary>
    /// Class holding arguments for the event
    /// </summary>
    public class PositionChangedEventArgs : EventArgs
    {
        private Coordinate _position = null;
        private string _remarks = string.Empty;

        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="position">position</param>
        /// <param name="remarks">remarks</param>
        public PositionChangedEventArgs(Coordinate position, string remarks)
        {
            _position = position;
            _remarks = remarks;
        }

        /// <summary>
        ///  Position
        /// </summary>
        public Coordinate ChangedPosition
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
            }
        }
        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks
        {
            get
            {
                return _remarks;
            }
            set
            {
                _remarks = value;
            }
        }
    }
}
