<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditObject
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtScript = New SqlBuilder.Controls.RichTextBoxParser
        Me.lblVersion = New System.Windows.Forms.Label
        Me.txtObject = New System.Windows.Forms.TextBox
        Me.txtVersion = New System.Windows.Forms.TextBox
        Me.lblObjectName = New System.Windows.Forms.Label
        Me.txtCreatedOn = New System.Windows.Forms.TextBox
        Me.lblCreatedOn = New System.Windows.Forms.Label
        Me.txtModifyOn = New System.Windows.Forms.TextBox
        Me.lblModifyOn = New System.Windows.Forms.Label
        Me.txtID = New System.Windows.Forms.TextBox
        Me.lblId = New System.Windows.Forms.Label
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.StatusInfo1 = New SqlBuilder.Controls.StatusInfo
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtScript
        '
        Me.txtScript.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtScript.BackColor = System.Drawing.Color.White
        Me.txtScript.DetectUrls = False
        Me.txtScript.FirstVisibleLine = 1
        Me.txtScript.Font = New System.Drawing.Font("Courier New", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScript.HideSelection = False
        Me.txtScript.Location = New System.Drawing.Point(199, 12)
        Me.txtScript.Name = "txtScript"
        Me.txtScript.Size = New System.Drawing.Size(706, 414)
        Me.txtScript.StatusBar = Me.StatusInfo1
        Me.txtScript.TabIndex = 10
        Me.txtScript.Text = ""
        Me.txtScript.TextModified = False
        '
        'lblVersion
        '
        Me.lblVersion.BackColor = System.Drawing.Color.Gray
        Me.lblVersion.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblVersion.ForeColor = System.Drawing.Color.White
        Me.lblVersion.Location = New System.Drawing.Point(3, 54)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(178, 20)
        Me.lblVersion.TabIndex = 10
        Me.lblVersion.Text = "Version"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtObject
        '
        Me.txtObject.BackColor = System.Drawing.Color.White
        Me.txtObject.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtObject.Location = New System.Drawing.Point(3, 138)
        Me.txtObject.Name = "txtObject"
        Me.txtObject.Size = New System.Drawing.Size(178, 20)
        Me.txtObject.TabIndex = 10
        '
        'txtVersion
        '
        Me.txtVersion.BackColor = System.Drawing.Color.White
        Me.txtVersion.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtVersion.Location = New System.Drawing.Point(3, 84)
        Me.txtVersion.Name = "txtVersion"
        Me.txtVersion.Size = New System.Drawing.Size(178, 20)
        Me.txtVersion.TabIndex = 12
        '
        'lblObjectName
        '
        Me.lblObjectName.BackColor = System.Drawing.Color.Gray
        Me.lblObjectName.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblObjectName.ForeColor = System.Drawing.Color.White
        Me.lblObjectName.Location = New System.Drawing.Point(3, 108)
        Me.lblObjectName.Name = "lblObjectName"
        Me.lblObjectName.Size = New System.Drawing.Size(178, 20)
        Me.lblObjectName.TabIndex = 11
        Me.lblObjectName.Text = "Object Name"
        Me.lblObjectName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCreatedOn
        '
        Me.txtCreatedOn.BackColor = System.Drawing.Color.LightYellow
        Me.txtCreatedOn.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtCreatedOn.Location = New System.Drawing.Point(3, 192)
        Me.txtCreatedOn.Name = "txtCreatedOn"
        Me.txtCreatedOn.ReadOnly = True
        Me.txtCreatedOn.Size = New System.Drawing.Size(178, 20)
        Me.txtCreatedOn.TabIndex = 14
        '
        'lblCreatedOn
        '
        Me.lblCreatedOn.BackColor = System.Drawing.Color.Gray
        Me.lblCreatedOn.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblCreatedOn.ForeColor = System.Drawing.Color.White
        Me.lblCreatedOn.Location = New System.Drawing.Point(3, 162)
        Me.lblCreatedOn.Name = "lblCreatedOn"
        Me.lblCreatedOn.Size = New System.Drawing.Size(178, 20)
        Me.lblCreatedOn.TabIndex = 13
        Me.lblCreatedOn.Text = "Created On"
        Me.lblCreatedOn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtModifyOn
        '
        Me.txtModifyOn.BackColor = System.Drawing.Color.LightYellow
        Me.txtModifyOn.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtModifyOn.Location = New System.Drawing.Point(3, 246)
        Me.txtModifyOn.Name = "txtModifyOn"
        Me.txtModifyOn.ReadOnly = True
        Me.txtModifyOn.Size = New System.Drawing.Size(178, 20)
        Me.txtModifyOn.TabIndex = 16
        '
        'lblModifyOn
        '
        Me.lblModifyOn.BackColor = System.Drawing.Color.Gray
        Me.lblModifyOn.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblModifyOn.ForeColor = System.Drawing.Color.White
        Me.lblModifyOn.Location = New System.Drawing.Point(3, 216)
        Me.lblModifyOn.Name = "lblModifyOn"
        Me.lblModifyOn.Size = New System.Drawing.Size(178, 20)
        Me.lblModifyOn.TabIndex = 15
        Me.lblModifyOn.Text = "Modify On"
        Me.lblModifyOn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtID
        '
        Me.txtID.BackColor = System.Drawing.Color.LightYellow
        Me.txtID.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtID.Location = New System.Drawing.Point(3, 30)
        Me.txtID.Name = "txtID"
        Me.txtID.ReadOnly = True
        Me.txtID.Size = New System.Drawing.Size(178, 20)
        Me.txtID.TabIndex = 18
        '
        'lblId
        '
        Me.lblId.BackColor = System.Drawing.Color.Gray
        Me.lblId.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblId.ForeColor = System.Drawing.Color.White
        Me.lblId.Location = New System.Drawing.Point(3, 0)
        Me.lblId.Name = "lblId"
        Me.lblId.Size = New System.Drawing.Size(178, 20)
        Me.lblId.TabIndex = 17
        Me.lblId.Text = "Id"
        Me.lblId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.txtCreatedOn, 0, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.txtModifyOn, 0, 9)
        Me.TableLayoutPanel1.Controls.Add(Me.txtID, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.lblModifyOn, 0, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.lblCreatedOn, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.lblId, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblVersion, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtVersion, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtObject, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.lblObjectName, 0, 4)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(12, 12)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 10
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(184, 275)
        Me.TableLayoutPanel1.TabIndex = 10
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.DialogResult = System.Windows.Forms.DialogResult.No
        Me.Button2.Location = New System.Drawing.Point(830, 432)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 25)
        Me.Button2.TabIndex = 12
        Me.Button2.Text = "Cancel"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.DialogResult = System.Windows.Forms.DialogResult.Yes
        Me.Button1.Location = New System.Drawing.Point(749, 432)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 25)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "Save"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'StatusInfo1
        '
        Me.StatusInfo1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.StatusInfo1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.StatusInfo1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusInfo1.ForeColor = System.Drawing.Color.Blue
        Me.StatusInfo1.Location = New System.Drawing.Point(0, 460)
        Me.StatusInfo1.Name = "StatusInfo1"
        Me.StatusInfo1.Size = New System.Drawing.Size(917, 18)
        Me.StatusInfo1.TabIndex = 13
        Me.StatusInfo1.Text = "StatusInfo1"
        Me.StatusInfo1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EditObject
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(917, 478)
        Me.Controls.Add(Me.StatusInfo1)
        Me.Controls.Add(Me.txtScript)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "EditObject"
        Me.Text = "EditObject"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtScript As SqlBuilder.Controls.RichTextBoxParser
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents lblId As System.Windows.Forms.Label
    Friend WithEvents txtModifyOn As System.Windows.Forms.TextBox
    Friend WithEvents lblModifyOn As System.Windows.Forms.Label
    Friend WithEvents txtCreatedOn As System.Windows.Forms.TextBox
    Friend WithEvents lblCreatedOn As System.Windows.Forms.Label
    Friend WithEvents txtVersion As System.Windows.Forms.TextBox
    Friend WithEvents lblObjectName As System.Windows.Forms.Label
    Friend WithEvents txtObject As System.Windows.Forms.TextBox
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents StatusInfo1 As SqlBuilder.Controls.StatusInfo
End Class
