Imports CreateScript.SQLSMO

Public Class XMLScript
    Public mstrScriptFile As String = ""
    Public isNew As Boolean
    Public m_Dataset As DataSet


    Public Sub NewXML()
        If mstrScriptFile <> "" Then
            Select Case MsgBox(String.Format("Do you want to save {0} file.", mstrScriptFile), MsgBoxStyle.Question + MsgBoxStyle.YesNoCancel)
                Case MsgBoxResult.Yes
                    Me.SaveXML()
                Case MsgBoxResult.No

                Case MsgBoxResult.Cancel
                    Exit Sub
            End Select
        End If

        If Me.SetDataset("") Then
            mstrScriptFile = "New Script File"
            isNew = True
        ElseIf Not isNew Then
            Me.SetDataset(mstrScriptFile)
            'txtFileName.Text = mstrScriptFile
        End If
    End Sub

    Public Sub LoadXML()
        Using obj As New OpenFileDialog
            obj.Title = "Select Database Script File"
            obj.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*"
            If obj.ShowDialog = Windows.Forms.DialogResult.OK Then
                If Me.SetDataset(obj.FileName) Then
                    mstrScriptFile = obj.FileName
                    isNew = False
                Else
                    Me.SetDataset(mstrScriptFile)
                End If
            Else
                Exit Sub
            End If
        End Using
    End Sub

    Public Sub SaveXML()
        If isNew Then
            Using obj As New SaveFileDialog
                obj.Title = "Select Database Script File"
                obj.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*"
                If obj.ShowDialog = Windows.Forms.DialogResult.OK Then
                    mstrScriptFile = obj.FileName
                Else
                    Exit Sub
                End If
            End Using
        End If
        m_Dataset.WriteXml(mstrScriptFile, XmlWriteMode.WriteSchema)
        isNew = False
    End Sub

    Private Sub AddTable(ByVal strTableName)
        Dim aTable As New DataTable(strTableName)
        With aTable
            .Columns.Add("Id", System.Type.GetType("System.Int32"))
            .Columns.Add("Version", System.Type.GetType("System.String"))
            .Columns.Add("CheckType", System.Type.GetType("System.Int32"))
            .Columns.Add("ConditionValue", System.Type.GetType("System.String"))
            .Columns.Add("Script", System.Type.GetType("System.String"))
            .Columns.Add("CreatedOn", System.Type.GetType("System.String"))
            .Columns.Add("UpdateOn", System.Type.GetType("System.String"))
        End With
        m_Dataset.Tables.Add(aTable)
    End Sub

    Private Function SetDataset(ByVal strFile As String) As Boolean
        Try
            If m_Dataset IsNot Nothing Then
                m_Dataset.Dispose()
                m_Dataset = Nothing
            End If

            m_Dataset = New DataSet("Script")
            If System.IO.File.Exists(strFile) Then
                m_Dataset.ReadXml(strFile)
                If m_Dataset.DataSetName <> "Script" Then
                    MsgBox("Invalid Script File", MsgBoxStyle.Information)
                    Exit Function
                End If
            End If

            If Not m_Dataset.Tables.Contains(XMLHelper.ConTable) Then Me.AddTable(XMLHelper.ConTable)
            If Not m_Dataset.Tables.Contains(XMLHelper.ConTrigger) Then Me.AddTable(XMLHelper.ConTrigger)
            If Not m_Dataset.Tables.Contains(XMLHelper.ConStoreProcedure) Then Me.AddTable(XMLHelper.ConStoreProcedure)
            If Not m_Dataset.Tables.Contains(XMLHelper.ConView) Then Me.AddTable(XMLHelper.ConView)
            If Not m_Dataset.Tables.Contains(XMLHelper.ConUpdateScript) Then Me.AddTable(XMLHelper.ConUpdateScript)
            If Not m_Dataset.Tables.Contains(XMLHelper.ConType) Then Me.AddTable(XMLHelper.ConType)
            If Not m_Dataset.Tables.Contains(XMLHelper.ConIndex) Then Me.AddTable(XMLHelper.ConIndex)

            Return True
        Catch ex As Exception
            MsgBox(ex.Message)
            Return True
        End Try
    End Function

End Class
