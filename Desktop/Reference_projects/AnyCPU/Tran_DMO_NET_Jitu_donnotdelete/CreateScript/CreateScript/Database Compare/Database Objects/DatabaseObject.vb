Imports Microsoft.VisualBasic
Imports System
Imports System.Collections
Imports System.Data.SqlClient

Namespace DatabaseCompare.Domain
	''' <summary>
	''' Summary description for DatabaseObject.
	''' </summary>
	Public MustInherit Class DatabaseObject
		Private name_Renamed As String
		Private id_Renamed As Integer
		Private columns_Renamed As Hashtable

		Public Sub New(ByVal name As String, ByVal id As Integer)
            Me.name_Renamed = name
			Me.id_Renamed = id
			columns_Renamed = New Hashtable()
		End Sub

		Public Property Name() As String
			Get
				Return name_Renamed
			End Get
			Set(ByVal value As String)
				name_Renamed = value
			End Set
		End Property

		Public Property Id() As Integer
			Get
				Return id_Renamed
			End Get
			Set(ByVal value As Integer)
				id_Renamed = value
			End Set
		End Property

		Public Property Columns() As Hashtable
			Get
				Return columns_Renamed
			End Get
			Set(ByVal value As Hashtable)
				columns_Renamed = value
			End Set
		End Property

        Public Overridable Sub GatherData(ByRef conn As SqlConnection)
            GetColumnData(conn)
        End Sub

        Private Sub GetColumnData(ByRef conn As SqlConnection)
            Using command As SqlCommand = conn.CreateCommand()
                command.CommandText = "select name, xtype, length, xscale from syscolumns where id=@id order by colorder"
                'command.Parameters.Add("@id", Me.id_Renamed)
                command.Parameters.Add(New SqlParameter("@id", Me.id_Renamed))
                Using reader As SqlDataReader = command.ExecuteReader()
                    Do While reader.Read()
                        columns_Renamed(reader.GetString(0)) = New Column(reader.GetString(0), reader.GetByte(1), reader.GetInt16(2), reader.GetByte(3))
                    Loop
                End Using
            End Using
        End Sub

        Public MustOverride Property TextDefinition() As String

        Public Function CompareTo(ByVal obj As DatabaseObject) As Boolean
            Return CompareColumns(obj) AndAlso LocalCompare(obj)
        End Function

        Protected Overridable Function LocalCompare(ByVal obj As DatabaseObject) As Boolean
            Return True
        End Function

        Private Function CompareColumns(ByVal obj As DatabaseObject) As Boolean
            If Me.Columns.Values.Count <> obj.Columns.Values.Count Then
                Return False
            End If
            For Each c As Column In Me.Columns.Values
                Dim oc As Column = TryCast(obj.Columns(c.Name), Column)
                If oc Is Nothing Then
                    Return False
                End If
                If (Not c.CompareTo(oc)) Then
                    Return False
                End If
            Next c
            Return True
        End Function
    End Class
End Namespace
