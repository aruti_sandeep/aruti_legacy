
Public Class frmCompare
    Public Db1 As DatabaseCompare.Domain.Database
    Public Db2 As DatabaseCompare.Domain.Database

    Private Sub btnLiveServerConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLiveServerConnect.Click
        Try
            Using obj As New SelectServer
                If obj.ShowDialog = Windows.Forms.DialogResult.Yes Then
                    Db2 = New DatabaseCompare.Domain.Database(obj._ConnectionString)
                    If Db2.TestConnection Then
                        lblLiveServer.Text = obj._DatabaseString
                    End If
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnTestServerConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTestServerConnect.Click
        Try
            Using obj As New SelectServer
                If obj.ShowDialog = Windows.Forms.DialogResult.Yes Then
                    Db1 = New DatabaseCompare.Domain.Database(obj._ConnectionString)
                    If Db1.TestConnection Then
                        lblTestServer.Text = obj._DatabaseString
                    End If
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Delegate Sub UIHelperDelegate(ByVal working As Boolean)
    Private Delegate Sub StatusDelegate(ByVal statusMessage As String)
    Private Delegate Sub ListDelegate(ByVal statusMessage As DatabaseCompare.Domain.DBDifference)

    Private Sub SetDoingLengthyOperation(ByVal working As Boolean)
        If Me.InvokeRequired Then
            Dim setDoingLengthyOperation As New UIHelperDelegate(AddressOf Me.SetDoingLengthyOperation)
            Dim arguments() As Object = {working}
            Me.Invoke(setDoingLengthyOperation, arguments)
            Return
        End If
        btnCompareDatabases.Enabled = Not working

        gbTestServer.Enabled = Not working
        gbLiveServer.Enabled = Not working

        If working Then
            Me.Cursor = Cursors.WaitCursor
        Else
            Me.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub UpdateList(ByVal message As DatabaseCompare.Domain.DBDifference)
        If Me.InvokeRequired Then
            Dim del As New ListDelegate(AddressOf Me.UpdateList)
            Dim arguments() As Object = {message}
            Me.Invoke(del, arguments)
            Return
        End If

        If Not tvTestServer.Nodes.ContainsKey(message.Type) Then
            tvTestServer.Nodes.Add(message.Type, message.Type)
        End If
        Dim nod As New TreeNode(message.Name)
        nod.Name = message.Name
        Select Case message.StatusId
            Case 0
            Case 1
                nod.ForeColor = Color.Gray
            Case 2
                nod.ForeColor = Color.Red
            Case 3
                nod.ForeColor = Color.Blue
        End Select
        tvTestServer.Nodes(message.Type).Nodes.Add(nod)

        If Not tvLiveServer.Nodes.ContainsKey(message.Type) Then
            tvLiveServer.Nodes.Add(message.Type, message.Type)
        End If
        Dim nod2 As New TreeNode(message.Name)
        nod2.Name = message.Name
        Select Case message.StatusId
            Case 0
            Case 1
                nod2.ForeColor = Color.Red
            Case 2
                nod2.ForeColor = Color.Gray
            Case 3
                nod2.ForeColor = Color.Blue
        End Select

        tvLiveServer.Nodes(message.Type).Nodes.Add(nod2)
    End Sub

    Private Sub UpdateStatusBar(ByVal message As String)
        If Me.InvokeRequired Then
            Dim del As New StatusDelegate(AddressOf Me.UpdateStatusBar)
            Dim arguments() As Object = {message}
            Me.Invoke(del, arguments)
            Return
        End If
        statusBar.Text = message
    End Sub

    Private differences As ArrayList

    Private Sub DoGatherDatabaseData(ByVal param As Object)
        Try
            Try
                UpdateStatusBar("Loading Database 1 objects")
                Db1.LoadObjects()
            Catch ex As Exception
                MessageBox.Show("Error loading Database 1 objects:" & Constants.vbCrLf & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End Try
            Try
                UpdateStatusBar("Loading Database 2 objects")
                Db2.LoadObjects()
            Catch ex As Exception
                MessageBox.Show("Error loading Database 2 objects:" & Constants.vbCrLf & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End Try
            UpdateStatusBar("Comparing Database objects")
            differences = Db1.CompareTo(Db2)
            differences.Sort()
            For Each d As DatabaseCompare.Domain.DBDifference In differences
                UpdateList(d)
            Next d

            UpdateStatusBar("")

        Catch ex As Exception
            MessageBox.Show("Error comparing Database objects:" & Constants.vbCrLf & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        Finally
            SetDoingLengthyOperation(False)
        End Try
    End Sub

    Private Sub btnCompareDatabases_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCompareDatabases.Click
        tvTestServer.Nodes.Clear()
        tvLiveServer.Nodes.Clear()

        SetDoingLengthyOperation(True)
        Dim doWork As New Threading.WaitCallback(AddressOf Me.DoGatherDatabaseData)
        Threading.ThreadPool.QueueUserWorkItem(doWork)
    End Sub

#Region " tvTestServer "
    Private Sub tvTestServer_AfterCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvTestServer.AfterCollapse
        tvLiveServer.Nodes(e.Node.Name).Collapse(False)
    End Sub

    Private Sub tvTestServer_AfterExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvTestServer.AfterExpand
        tvLiveServer.Nodes(e.Node.Name).Expand()
    End Sub

    Private Sub tvTestServer_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvTestServer.AfterSelect
        Try

            Dim node As TreeNode = tvLiveServer.Nodes(e.Node.Name)
            If node Is Nothing Then
                tvLiveServer.SelectedNode = tvLiveServer.Nodes(e.Node.Parent.Name).Nodes(e.Node.Name)
            Else
                tvLiveServer.SelectedNode = node
            End If

            Dim Topnode As TreeNode = tvLiveServer.Nodes(tvTestServer.TopNode.Name)
            If Topnode Is Nothing Then
                tvLiveServer.TopNode = tvLiveServer.Nodes(tvTestServer.TopNode.Parent.Name).Nodes(tvTestServer.TopNode.Name)
            Else
                tvLiveServer.TopNode = Topnode
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub tvTestServer_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tvTestServer.MouseWheel
        Try
            Dim Topnode As TreeNode = tvLiveServer.Nodes(tvTestServer.TopNode.Name)
            If Topnode Is Nothing Then
                tvLiveServer.TopNode = tvLiveServer.Nodes(tvTestServer.TopNode.Parent.Name).Nodes(tvTestServer.TopNode.Name)
            Else
                tvLiveServer.TopNode = Topnode
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub tvTestServer_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles tvTestServer.Scroll
        Try
            Dim Topnode As TreeNode = tvLiveServer.Nodes(tvTestServer.TopNode.Name)
            If Topnode Is Nothing Then
                tvLiveServer.TopNode = tvLiveServer.Nodes(tvTestServer.TopNode.Parent.Name).Nodes(tvTestServer.TopNode.Name)
            Else
                tvLiveServer.TopNode = Topnode
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region " tvLiveServer "
    Private Sub tvLiveServer_AfterCollapse(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvLiveServer.AfterCollapse
        tvTestServer.Nodes(e.Node.Name).Collapse(False)
    End Sub

    Private Sub tvLiveServer_AfterExpand(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvLiveServer.AfterExpand
        tvTestServer.Nodes(e.Node.Name).Expand()
    End Sub

    Private Sub tvLiveServer_AfterSelect(ByVal sender As Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvLiveServer.AfterSelect
        Try
            Dim node As TreeNode = tvTestServer.Nodes(e.Node.Name)
            If node Is Nothing Then
                tvTestServer.SelectedNode = tvTestServer.Nodes(e.Node.Parent.Name).Nodes(e.Node.Name)
            Else
                tvTestServer.SelectedNode = node
            End If

            Dim Topnode As TreeNode = tvTestServer.Nodes(tvLiveServer.TopNode.Name)
            If Topnode Is Nothing Then
                tvTestServer.TopNode = tvTestServer.Nodes(tvLiveServer.TopNode.Parent.Name).Nodes(tvLiveServer.TopNode.Name)
            Else
                tvTestServer.TopNode = Topnode
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub tvLiveServer_MouseWheel(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tvLiveServer.MouseWheel
        Try

            Dim Topnode As TreeNode = tvTestServer.Nodes(tvLiveServer.TopNode.Name)
            If Topnode Is Nothing Then
                tvTestServer.TopNode = tvTestServer.Nodes(tvLiveServer.TopNode.Parent.Name).Nodes(tvLiveServer.TopNode.Name)
            Else
                tvTestServer.TopNode = Topnode
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub tvLiveServer_Scroll(ByVal sender As Object, ByVal e As System.Windows.Forms.ScrollEventArgs) Handles tvLiveServer.Scroll
        Try

            Dim Topnode As TreeNode = tvTestServer.Nodes(tvLiveServer.TopNode.Name)
            If Topnode Is Nothing Then
                tvTestServer.TopNode = tvTestServer.Nodes(tvLiveServer.TopNode.Parent.Name).Nodes(tvLiveServer.TopNode.Name)
            Else
                tvTestServer.TopNode = Topnode
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region
End Class
