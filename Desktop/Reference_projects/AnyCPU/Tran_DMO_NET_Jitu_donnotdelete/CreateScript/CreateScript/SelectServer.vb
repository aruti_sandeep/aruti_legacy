Public Class SelectServer
    Public _ConnectionString As String
    Public _DatabaseString As String

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        _ConnectionString = DatabaseCompare.Domain.Connection.ConnectionString(txtServer1.Text, txtUN1.Text, txtP1.Text, chkSSPI1.Checked, cboTestDatabase.Text)
        _DatabaseString = cboTestDatabase.Text
    End Sub

    Public Function GetDatabaseName() As String()
        Dim connString As String = ""
        Dim str As New List(Of String)
        Try
            connString = DatabaseCompare.Domain.Connection.ConnectionString(txtServer1.Text, txtUN1.Text, txtP1.Text, chkSSPI1.Checked)
            Using conn As New SqlClient.SqlConnection(connString)
                conn.Open()
                Using command As SqlClient.SqlCommand = conn.CreateCommand()
                    command.CommandText = "select * from sys.databases"

                    Using reader As SqlClient.SqlDataReader = command.ExecuteReader()
                        Do While reader.Read()
                            str.Add(reader.GetString(0).Trim())
                        Loop
                    End Using
                End Using
                conn.Close()
                conn.Dispose()
            End Using
            '            catch ( Exception ex )
        Catch ex As Exception
            Throw ex
        End Try
        Return str.ToArray
    End Function

    Private Sub btnTestServerConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTestServerConnect.Click
        Try
            cboTestDatabase.DataSource = Me.GetDatabaseName()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally

        End Try
    End Sub
End Class