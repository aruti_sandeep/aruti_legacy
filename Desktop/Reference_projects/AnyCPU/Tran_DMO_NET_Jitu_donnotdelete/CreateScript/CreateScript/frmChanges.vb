Public Class frmChanges
    Private ScriptDatabase_renamed As String = ""
    Public Property ScriptDatabase() As String
        Get
            Return ScriptDatabase_renamed
        End Get
        Set(ByVal value As String)
            ScriptDatabase_renamed = value
        End Set
    End Property

    Private TestDatabase_renamed As String = ""
    Public Property TestDatabase() As String
        Get
            Return TestDatabase_renamed
        End Get
        Set(ByVal value As String)
            TestDatabase_renamed = value
        End Set
    End Property

    Private Sub frmChanges_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtScriptDatabase.AppendText(ScriptDatabase_renamed)
        txtTestDatabase.AppendText(TestDatabase_renamed)
    End Sub
End Class