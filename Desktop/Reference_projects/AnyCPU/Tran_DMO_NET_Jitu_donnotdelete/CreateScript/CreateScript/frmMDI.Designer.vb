<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMDI
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMDI))
        Me.ToolStrip = New System.Windows.Forms.ToolStrip
        Me.tsbNew = New System.Windows.Forms.ToolStripButton
        Me.tsbOpen = New System.Windows.Forms.ToolStripButton
        Me.tsbSave = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.btnOpenServer = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.tsbSelectCompareDatabase = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.tsbCompareChanges = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton4 = New System.Windows.Forms.ToolStripButton
        Me.StatusStrip = New System.Windows.Forms.StatusStrip
        Me.lblServer = New System.Windows.Forms.ToolStripStatusLabel
        Me.txtServer = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblTestDatabase = New System.Windows.Forms.ToolStripStatusLabel
        Me.txtTestDatabase = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblScriptDatabase = New System.Windows.Forms.ToolStripStatusLabel
        Me.txtScriptDatabase = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblStatus = New System.Windows.Forms.ToolStripStatusLabel
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.mnuScript = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNewXML = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuOpenXml = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSaveXML = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuLiveDatabase = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuConnectLiveDatabase = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSelectCompareDatabase = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCreateScriptDatabase = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuCompareChanges = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip = New System.Windows.Forms.MenuStrip
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.tvXmlObject = New System.Windows.Forms.TreeView
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.tsbCreateScriptDatabase = New System.Windows.Forms.ToolStripButton
        Me.tsbUpdateScriptDatabase = New System.Windows.Forms.ToolStripButton
        Me.ToolStripSeparator9 = New System.Windows.Forms.ToolStripSeparator
        Me.dgvData = New System.Windows.Forms.DataGridView
        Me.colSelect = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.colId = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colVersion = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colObject = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colScript = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colCreatedOn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colModifyOn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pnlMain = New System.Windows.Forms.SplitContainer
        Me.pnlResult = New System.Windows.Forms.SplitContainer
        Me.lstMissing = New System.Windows.Forms.ListBox
        Me.mnuScriptCreation = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuScript_ViewChanges = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuScript_CreateEntry = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuScript_UpdateEntry = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStrip2 = New System.Windows.Forms.ToolStrip
        Me.lblObject = New System.Windows.Forms.ToolStripLabel
        Me.ToolStripButton3 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker
        Me.ToolStrip.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        Me.MenuStrip.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMain.Panel1.SuspendLayout()
        Me.pnlMain.Panel2.SuspendLayout()
        Me.pnlMain.SuspendLayout()
        Me.pnlResult.Panel1.SuspendLayout()
        Me.pnlResult.Panel2.SuspendLayout()
        Me.pnlResult.SuspendLayout()
        Me.mnuScriptCreation.SuspendLayout()
        Me.ToolStrip2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip
        '
        Me.ToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbNew, Me.tsbOpen, Me.tsbSave, Me.ToolStripSeparator1, Me.btnOpenServer, Me.ToolStripSeparator2, Me.tsbSelectCompareDatabase, Me.ToolStripSeparator3, Me.tsbCompareChanges, Me.ToolStripButton4})
        Me.ToolStrip.Location = New System.Drawing.Point(0, 24)
        Me.ToolStrip.Name = "ToolStrip"
        Me.ToolStrip.Size = New System.Drawing.Size(792, 25)
        Me.ToolStrip.TabIndex = 6
        Me.ToolStrip.Text = "ToolStrip"
        '
        'tsbNew
        '
        Me.tsbNew.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbNew.Image = CType(resources.GetObject("tsbNew.Image"), System.Drawing.Image)
        Me.tsbNew.ImageTransparentColor = System.Drawing.Color.Black
        Me.tsbNew.Name = "tsbNew"
        Me.tsbNew.Size = New System.Drawing.Size(23, 22)
        Me.tsbNew.Text = "New"
        '
        'tsbOpen
        '
        Me.tsbOpen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbOpen.Image = CType(resources.GetObject("tsbOpen.Image"), System.Drawing.Image)
        Me.tsbOpen.ImageTransparentColor = System.Drawing.Color.Black
        Me.tsbOpen.Name = "tsbOpen"
        Me.tsbOpen.Size = New System.Drawing.Size(23, 22)
        Me.tsbOpen.Text = "Open"
        '
        'tsbSave
        '
        Me.tsbSave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbSave.Image = CType(resources.GetObject("tsbSave.Image"), System.Drawing.Image)
        Me.tsbSave.ImageTransparentColor = System.Drawing.Color.Black
        Me.tsbSave.Name = "tsbSave"
        Me.tsbSave.Size = New System.Drawing.Size(23, 22)
        Me.tsbSave.Text = "Save"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'btnOpenServer
        '
        Me.btnOpenServer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnOpenServer.Image = CType(resources.GetObject("btnOpenServer.Image"), System.Drawing.Image)
        Me.btnOpenServer.ImageTransparentColor = System.Drawing.Color.Black
        Me.btnOpenServer.Name = "btnOpenServer"
        Me.btnOpenServer.Size = New System.Drawing.Size(23, 22)
        Me.btnOpenServer.Text = "Print Preview"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'tsbSelectCompareDatabase
        '
        Me.tsbSelectCompareDatabase.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbSelectCompareDatabase.Image = CType(resources.GetObject("tsbSelectCompareDatabase.Image"), System.Drawing.Image)
        Me.tsbSelectCompareDatabase.ImageTransparentColor = System.Drawing.Color.Black
        Me.tsbSelectCompareDatabase.Name = "tsbSelectCompareDatabase"
        Me.tsbSelectCompareDatabase.Size = New System.Drawing.Size(23, 22)
        Me.tsbSelectCompareDatabase.Text = "Open"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(6, 25)
        '
        'tsbCompareChanges
        '
        Me.tsbCompareChanges.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbCompareChanges.Image = CType(resources.GetObject("tsbCompareChanges.Image"), System.Drawing.Image)
        Me.tsbCompareChanges.ImageTransparentColor = System.Drawing.Color.Black
        Me.tsbCompareChanges.Name = "tsbCompareChanges"
        Me.tsbCompareChanges.Size = New System.Drawing.Size(23, 22)
        Me.tsbCompareChanges.Text = "Help"
        '
        'ToolStripButton4
        '
        Me.ToolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton4.Image = CType(resources.GetObject("ToolStripButton4.Image"), System.Drawing.Image)
        Me.ToolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton4.Name = "ToolStripButton4"
        Me.ToolStripButton4.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton4.Text = "ToolStripButton4"
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblServer, Me.txtServer, Me.lblTestDatabase, Me.txtTestDatabase, Me.lblScriptDatabase, Me.txtScriptDatabase, Me.lblStatus})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 544)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(792, 22)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'lblServer
        '
        Me.lblServer.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left
        Me.lblServer.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblServer.ForeColor = System.Drawing.Color.Black
        Me.lblServer.Name = "lblServer"
        Me.lblServer.Size = New System.Drawing.Size(52, 17)
        Me.lblServer.Text = "Server:"
        '
        'txtServer
        '
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(79, 17)
        Me.txtServer.Text = "Not Connected"
        '
        'lblTestDatabase
        '
        Me.lblTestDatabase.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left
        Me.lblTestDatabase.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTestDatabase.ForeColor = System.Drawing.Color.Black
        Me.lblTestDatabase.Name = "lblTestDatabase"
        Me.lblTestDatabase.Size = New System.Drawing.Size(68, 17)
        Me.lblTestDatabase.Text = "Database:"
        '
        'txtTestDatabase
        '
        Me.txtTestDatabase.Name = "txtTestDatabase"
        Me.txtTestDatabase.Size = New System.Drawing.Size(79, 17)
        Me.txtTestDatabase.Text = "Not Connected"
        '
        'lblScriptDatabase
        '
        Me.lblScriptDatabase.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left
        Me.lblScriptDatabase.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScriptDatabase.ForeColor = System.Drawing.Color.Black
        Me.lblScriptDatabase.Name = "lblScriptDatabase"
        Me.lblScriptDatabase.Size = New System.Drawing.Size(104, 17)
        Me.lblScriptDatabase.Text = "Script Database:"
        '
        'txtScriptDatabase
        '
        Me.txtScriptDatabase.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right
        Me.txtScriptDatabase.Name = "txtScriptDatabase"
        Me.txtScriptDatabase.Size = New System.Drawing.Size(83, 17)
        Me.txtScriptDatabase.Text = "Not Connected"
        '
        'lblStatus
        '
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(312, 17)
        Me.lblStatus.Spring = True
        Me.lblStatus.Text = "Ready"
        Me.lblStatus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'mnuScript
        '
        Me.mnuScript.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNewXML, Me.mnuOpenXml, Me.mnuSaveXML})
        Me.mnuScript.Name = "mnuScript"
        Me.mnuScript.Size = New System.Drawing.Size(46, 20)
        Me.mnuScript.Text = "Script"
        '
        'mnuNewXML
        '
        Me.mnuNewXML.Name = "mnuNewXML"
        Me.mnuNewXML.Size = New System.Drawing.Size(122, 22)
        Me.mnuNewXML.Text = "New XML"
        '
        'mnuOpenXml
        '
        Me.mnuOpenXml.Name = "mnuOpenXml"
        Me.mnuOpenXml.Size = New System.Drawing.Size(122, 22)
        Me.mnuOpenXml.Text = "Open XML"
        '
        'mnuSaveXML
        '
        Me.mnuSaveXML.Name = "mnuSaveXML"
        Me.mnuSaveXML.Size = New System.Drawing.Size(122, 22)
        Me.mnuSaveXML.Text = "Save XML"
        '
        'mnuLiveDatabase
        '
        Me.mnuLiveDatabase.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuConnectLiveDatabase, Me.mnuSelectCompareDatabase, Me.mnuCreateScriptDatabase, Me.ToolStripSeparator4, Me.mnuCompareChanges})
        Me.mnuLiveDatabase.Name = "mnuLiveDatabase"
        Me.mnuLiveDatabase.Size = New System.Drawing.Size(65, 20)
        Me.mnuLiveDatabase.Text = "Database"
        '
        'mnuConnectLiveDatabase
        '
        Me.mnuConnectLiveDatabase.Name = "mnuConnectLiveDatabase"
        Me.mnuConnectLiveDatabase.Size = New System.Drawing.Size(198, 22)
        Me.mnuConnectLiveDatabase.Text = "Connect Database"
        '
        'mnuSelectCompareDatabase
        '
        Me.mnuSelectCompareDatabase.Name = "mnuSelectCompareDatabase"
        Me.mnuSelectCompareDatabase.Size = New System.Drawing.Size(198, 22)
        Me.mnuSelectCompareDatabase.Text = "Select Compare Database"
        '
        'mnuCreateScriptDatabase
        '
        Me.mnuCreateScriptDatabase.Name = "mnuCreateScriptDatabase"
        Me.mnuCreateScriptDatabase.Size = New System.Drawing.Size(198, 22)
        Me.mnuCreateScriptDatabase.Text = "Create Script Database"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(195, 6)
        '
        'mnuCompareChanges
        '
        Me.mnuCompareChanges.Name = "mnuCompareChanges"
        Me.mnuCompareChanges.Size = New System.Drawing.Size(198, 22)
        Me.mnuCompareChanges.Text = "Compare Changes"
        '
        'MenuStrip
        '
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuScript, Me.mnuLiveDatabase})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(792, 24)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.tvXmlObject)
        Me.Panel1.Controls.Add(Me.ToolStrip1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(190, 495)
        Me.Panel1.TabIndex = 10
        '
        'tvXmlObject
        '
        Me.tvXmlObject.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvXmlObject.Location = New System.Drawing.Point(0, 0)
        Me.tvXmlObject.Name = "tvXmlObject"
        Me.tvXmlObject.Size = New System.Drawing.Size(186, 466)
        Me.tvXmlObject.TabIndex = 8
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbCreateScriptDatabase, Me.tsbUpdateScriptDatabase, Me.ToolStripSeparator9})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 466)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(186, 25)
        Me.ToolStrip1.TabIndex = 7
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'tsbCreateScriptDatabase
        '
        Me.tsbCreateScriptDatabase.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbCreateScriptDatabase.Image = CType(resources.GetObject("tsbCreateScriptDatabase.Image"), System.Drawing.Image)
        Me.tsbCreateScriptDatabase.ImageTransparentColor = System.Drawing.Color.Black
        Me.tsbCreateScriptDatabase.Name = "tsbCreateScriptDatabase"
        Me.tsbCreateScriptDatabase.Size = New System.Drawing.Size(23, 22)
        Me.tsbCreateScriptDatabase.Text = "New"
        '
        'tsbUpdateScriptDatabase
        '
        Me.tsbUpdateScriptDatabase.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbUpdateScriptDatabase.Image = CType(resources.GetObject("tsbUpdateScriptDatabase.Image"), System.Drawing.Image)
        Me.tsbUpdateScriptDatabase.ImageTransparentColor = System.Drawing.Color.Black
        Me.tsbUpdateScriptDatabase.Name = "tsbUpdateScriptDatabase"
        Me.tsbUpdateScriptDatabase.Size = New System.Drawing.Size(23, 22)
        Me.tsbUpdateScriptDatabase.Text = "Open"
        '
        'ToolStripSeparator9
        '
        Me.ToolStripSeparator9.Name = "ToolStripSeparator9"
        Me.ToolStripSeparator9.Size = New System.Drawing.Size(6, 25)
        '
        'dgvData
        '
        Me.dgvData.AllowUserToAddRows = False
        Me.dgvData.AllowUserToDeleteRows = False
        Me.dgvData.AllowUserToOrderColumns = True
        Me.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvData.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colSelect, Me.colId, Me.colVersion, Me.colObject, Me.colScript, Me.colCreatedOn, Me.colModifyOn})
        Me.dgvData.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvData.Location = New System.Drawing.Point(0, 0)
        Me.dgvData.Name = "dgvData"
        Me.dgvData.RowHeadersVisible = False
        Me.dgvData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvData.Size = New System.Drawing.Size(598, 322)
        Me.dgvData.TabIndex = 12
        '
        'colSelect
        '
        Me.colSelect.FillWeight = 20.0!
        Me.colSelect.HeaderText = ""
        Me.colSelect.Name = "colSelect"
        Me.colSelect.Width = 20
        '
        'colId
        '
        Me.colId.FillWeight = 50.0!
        Me.colId.HeaderText = "ID"
        Me.colId.Name = "colId"
        Me.colId.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colId.Width = 50
        '
        'colVersion
        '
        Me.colVersion.FillWeight = 80.0!
        Me.colVersion.HeaderText = "Version"
        Me.colVersion.Name = "colVersion"
        Me.colVersion.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colVersion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colVersion.Width = 80
        '
        'colObject
        '
        Me.colObject.HeaderText = "Check"
        Me.colObject.Name = "colObject"
        Me.colObject.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colObject.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colScript
        '
        Me.colScript.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colScript.HeaderText = "Script"
        Me.colScript.Name = "colScript"
        Me.colScript.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colScript.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colCreatedOn
        '
        Me.colCreatedOn.HeaderText = "Created On"
        Me.colCreatedOn.Name = "colCreatedOn"
        Me.colCreatedOn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colCreatedOn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'colModifyOn
        '
        Me.colModifyOn.HeaderText = "Modify On"
        Me.colModifyOn.Name = "colModifyOn"
        Me.colModifyOn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.colModifyOn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'pnlMain
        '
        Me.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlMain.Location = New System.Drawing.Point(0, 49)
        Me.pnlMain.Name = "pnlMain"
        '
        'pnlMain.Panel1
        '
        Me.pnlMain.Panel1.Controls.Add(Me.Panel1)
        '
        'pnlMain.Panel2
        '
        Me.pnlMain.Panel2.Controls.Add(Me.pnlResult)
        Me.pnlMain.Panel2.Controls.Add(Me.ToolStrip2)
        Me.pnlMain.Size = New System.Drawing.Size(792, 495)
        Me.pnlMain.SplitterDistance = 190
        Me.pnlMain.TabIndex = 13
        '
        'pnlResult
        '
        Me.pnlResult.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlResult.Location = New System.Drawing.Point(0, 25)
        Me.pnlResult.Name = "pnlResult"
        Me.pnlResult.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'pnlResult.Panel1
        '
        Me.pnlResult.Panel1.Controls.Add(Me.dgvData)
        '
        'pnlResult.Panel2
        '
        Me.pnlResult.Panel2.Controls.Add(Me.lstMissing)
        Me.pnlResult.Size = New System.Drawing.Size(598, 470)
        Me.pnlResult.SplitterDistance = 322
        Me.pnlResult.TabIndex = 14
        '
        'lstMissing
        '
        Me.lstMissing.ContextMenuStrip = Me.mnuScriptCreation
        Me.lstMissing.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstMissing.FormattingEnabled = True
        Me.lstMissing.Location = New System.Drawing.Point(0, 0)
        Me.lstMissing.Name = "lstMissing"
        Me.lstMissing.Size = New System.Drawing.Size(598, 134)
        Me.lstMissing.TabIndex = 0
        '
        'mnuScriptCreation
        '
        Me.mnuScriptCreation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuScript_ViewChanges, Me.mnuScript_CreateEntry, Me.mnuScript_UpdateEntry})
        Me.mnuScriptCreation.Name = "ContextMenuStrip1"
        Me.mnuScriptCreation.Size = New System.Drawing.Size(142, 70)
        '
        'mnuScript_ViewChanges
        '
        Me.mnuScript_ViewChanges.Name = "mnuScript_ViewChanges"
        Me.mnuScript_ViewChanges.Size = New System.Drawing.Size(141, 22)
        Me.mnuScript_ViewChanges.Text = "View Changes"
        '
        'mnuScript_CreateEntry
        '
        Me.mnuScript_CreateEntry.Name = "mnuScript_CreateEntry"
        Me.mnuScript_CreateEntry.Size = New System.Drawing.Size(141, 22)
        Me.mnuScript_CreateEntry.Text = "Create Entry"
        '
        'mnuScript_UpdateEntry
        '
        Me.mnuScript_UpdateEntry.Name = "mnuScript_UpdateEntry"
        Me.mnuScript_UpdateEntry.Size = New System.Drawing.Size(141, 22)
        Me.mnuScript_UpdateEntry.Text = "Update Entry"
        '
        'ToolStrip2
        '
        Me.ToolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblObject, Me.ToolStripButton3, Me.ToolStripButton2, Me.ToolStripButton1})
        Me.ToolStrip2.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip2.Name = "ToolStrip2"
        Me.ToolStrip2.Size = New System.Drawing.Size(598, 25)
        Me.ToolStrip2.TabIndex = 13
        Me.ToolStrip2.Text = "ToolStrip2"
        '
        'lblObject
        '
        Me.lblObject.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.lblObject.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObject.ForeColor = System.Drawing.Color.Navy
        Me.lblObject.Name = "lblObject"
        Me.lblObject.Size = New System.Drawing.Size(23, 22)
        Me.lblObject.Text = "--"
        '
        'ToolStripButton3
        '
        Me.ToolStripButton3.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton3.Image = CType(resources.GetObject("ToolStripButton3.Image"), System.Drawing.Image)
        Me.ToolStripButton3.ImageTransparentColor = System.Drawing.Color.Black
        Me.ToolStripButton3.Name = "ToolStripButton3"
        Me.ToolStripButton3.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton3.Text = "Save"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Black
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton2.Text = "Open"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Black
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 22)
        Me.ToolStripButton1.Text = "New"
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        '
        'frmMDI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 566)
        Me.Controls.Add(Me.pnlMain)
        Me.Controls.Add(Me.ToolStrip)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.StatusStrip)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip
        Me.MinimumSize = New System.Drawing.Size(800, 600)
        Me.Name = "frmMDI"
        Me.Text = "Script Maker"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ToolStrip.ResumeLayout(False)
        Me.ToolStrip.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        CType(Me.dgvData, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMain.Panel1.ResumeLayout(False)
        Me.pnlMain.Panel2.ResumeLayout(False)
        Me.pnlMain.Panel2.PerformLayout()
        Me.pnlMain.ResumeLayout(False)
        Me.pnlResult.Panel1.ResumeLayout(False)
        Me.pnlResult.Panel2.ResumeLayout(False)
        Me.pnlResult.ResumeLayout(False)
        Me.mnuScriptCreation.ResumeLayout(False)
        Me.ToolStrip2.ResumeLayout(False)
        Me.ToolStrip2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnOpenServer As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStrip As System.Windows.Forms.ToolStrip
    Friend WithEvents tsbOpen As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbSave As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuScript As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOpenXml As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuLiveDatabase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuConnectLiveDatabase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents lblServer As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtServer As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents mnuSaveXML As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsbNew As System.Windows.Forms.ToolStripButton
    Friend WithEvents mnuNewXML As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents tvXmlObject As System.Windows.Forms.TreeView
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents tsbCreateScriptDatabase As System.Windows.Forms.ToolStripButton
    Friend WithEvents tsbUpdateScriptDatabase As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator9 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuSelectCompareDatabase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgvData As System.Windows.Forms.DataGridView
    Friend WithEvents pnlMain As System.Windows.Forms.SplitContainer
    Friend WithEvents ToolStrip2 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripButton3 As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblObject As System.Windows.Forms.ToolStripLabel
    Friend WithEvents lblTestDatabase As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtTestDatabase As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblScriptDatabase As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents txtScriptDatabase As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents mnuCreateScriptDatabase As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsbSelectCompareDatabase As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsbCompareChanges As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuCompareChanges As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblStatus As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents pnlResult As System.Windows.Forms.SplitContainer
    Friend WithEvents lstMissing As System.Windows.Forms.ListBox
    Friend WithEvents mnuScriptCreation As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuScript_ViewChanges As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuScript_CreateEntry As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuScript_UpdateEntry As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents colSelect As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents colId As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colVersion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colObject As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colScript As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCreatedOn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colModifyOn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToolStripButton4 As System.Windows.Forms.ToolStripButton

End Class
