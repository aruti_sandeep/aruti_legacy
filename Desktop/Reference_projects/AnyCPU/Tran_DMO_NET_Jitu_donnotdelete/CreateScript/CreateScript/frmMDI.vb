Imports System.Windows.Forms
Imports CreateScript.SQLSMO

Public Class frmMDI
    Private ReadOnly m_strTitle As String = "Script Maker - {0}"
    Private m_dsv As New DatabaseServer
    Private m_XML As New XMLScript

#Region " Menu "
    Private Sub mnuNewXML_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewXML.Click, tsbNew.Click
        Try
            Me.m_XML.NewXML()
            Call Me.SetStatus()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub mnuLoadXml_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOpenXml.Click, tsbOpen.Click
        Try
            Me.m_XML.LoadXML()
            Call Me.SetStatus()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub mnuSaveXML_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSaveXML.Click, tsbSave.Click
        Try
            Me.m_XML.SaveXML()
            Call Me.SetStatus()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region " Form "
    Private Sub frmMDI_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call Me.m_XML.NewXML()
            Call Me.SetStatus()
            colId.DataPropertyName = "Id"
            colVersion.DataPropertyName = "Version"
            colObject.DataPropertyName = "ConditionValue"
            colScript.DataPropertyName = "Script"
            colCreatedOn.DataPropertyName = "CreatedOn"
            colModifyOn.DataPropertyName = "UpdateOn"
            dgvData.AutoGenerateColumns = False
            dgvData.DataSource = m_XML.m_Dataset
            dgvData.DataMember = XMLHelper.ConType
            lblObject.Text = XMLHelper.ConType
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
#End Region

#Region " Controls "
    Private Sub tvXmlObject_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvXmlObject.AfterSelect
        If e.Node.Name <> "" Then
            dgvData.DataSource = m_XML.m_Dataset
            dgvData.DataMember = e.Node.Name
            lblObject.Text = e.Node.Name
        Else
            dgvData.DataSource = m_XML.m_Dataset
            dgvData.DataMember = XMLHelper.ConType
            lblObject.Text = XMLHelper.ConType
        End If

        'Select Case e.Node.Name
        '    Case m_XML.ConType

        '    Case m_XML.ConTable

        '    Case m_XML.ConIndex

        '    Case m_XML.ConStoreProcedure

        '    Case m_XML.ConView

        '    Case m_XML.ConTrigger

        '    Case m_XML.ConUpdateScript

        'End Select
    End Sub
#End Region

#Region " Database "
    Private Sub SetStatus()
        If m_dsv.IsConnected Then
            txtServer.Text = m_dsv.Server
        Else
            txtServer.Text = "Not Connected"
        End If

        If m_dsv.IsTestDatabase Then
            txtTestDatabase.Text = m_dsv.TestDatabase
        Else
            txtTestDatabase.Text = "Not Connected"
        End If

        If m_dsv.IsScriptDatabse Then
            txtTestDatabase.Text = m_dsv.ScriptDatabase
        Else
            txtTestDatabase.Text = "Not Connected"
        End If

        mnuSelectCompareDatabase.Enabled = m_dsv.IsConnected
        tsbSelectCompareDatabase.Enabled = m_dsv.IsConnected

        mnuCreateScriptDatabase.Enabled = m_dsv.IsConnected
        tsbCreateScriptDatabase.Enabled = m_dsv.IsConnected

        tsbUpdateScriptDatabase.Enabled = m_dsv.IsScriptDatabse

        mnuCompareChanges.Enabled = m_dsv.IsScriptDatabse And m_dsv.IsTestDatabase
        tsbCompareChanges.Enabled = m_dsv.IsScriptDatabse And m_dsv.IsTestDatabase

        tvXmlObject.Nodes.Clear()

        With tvXmlObject.Nodes.Add("Full Script")
            .Nodes.Add(XMLHelper.ConType, XMLHelper.ConType & " (" & m_XML.m_Dataset.Tables(XMLHelper.ConType).Rows.Count & ")")
            .Nodes.Add(XMLHelper.ConTable, XMLHelper.ConTable & " (" & m_XML.m_Dataset.Tables(XMLHelper.ConTable).Rows.Count & ")")
            .Nodes.Add(XMLHelper.ConIndex, XMLHelper.ConIndex & " (" & m_XML.m_Dataset.Tables(XMLHelper.ConIndex).Rows.Count & ")")
            .Nodes.Add(XMLHelper.ConStoreProcedure, XMLHelper.ConStoreProcedure & " (" & m_XML.m_Dataset.Tables(XMLHelper.ConStoreProcedure).Rows.Count & ")")
            .Nodes.Add(XMLHelper.ConView, XMLHelper.ConView & " (" & m_XML.m_Dataset.Tables(XMLHelper.ConView).Rows.Count & ")")
            .Nodes.Add(XMLHelper.ConTrigger, XMLHelper.ConTrigger & " (" & m_XML.m_Dataset.Tables(XMLHelper.ConTrigger).Rows.Count & ")")
        End With

        With tvXmlObject.Nodes.Add("Update Script")
            .Nodes.Add(XMLHelper.ConUpdateScript, XMLHelper.ConUpdateScript & " (" & m_XML.m_Dataset.Tables(XMLHelper.ConUpdateScript).Rows.Count & ")")
        End With
        tvXmlObject.ExpandAll()

        Me.Text = String.Format(m_strTitle, m_XML.mstrScriptFile)

    End Sub

    Private Sub btnOpenServer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpenServer.Click, mnuConnectLiveDatabase.Click
        Using obj As New frmSelectServer
            If obj.ShowDialog = Windows.Forms.DialogResult.Yes Then
                m_dsv = obj.DatabaseServer
                Dim str As String = DatabaseCompare.Domain.Connection.ConnectionString(m_dsv.Server, m_dsv.User, m_dsv.Password, m_dsv.Integrated)
                Dim ds As New DatabaseCompare.Domain.Database(str)
                If ds.TestConnection() Then
                    m_dsv.IsConnected = True
                Else
                    m_dsv.IsConnected = False
                    MsgBox("Database Server Conenction Fail", MsgBoxStyle.Critical)
                End If
            End If
            Call Me.SetStatus()
        End Using
    End Sub

    Private Sub mnuSelectCompareDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectCompareDatabase.Click, tsbSelectCompareDatabase.Click
        Try
            Using obj As New frmSelectDatabase
                obj._DatabaseServer = m_dsv
                If obj.ShowDialog = Windows.Forms.DialogResult.Yes Then

                    Dim str As String = DatabaseCompare.Domain.Connection.ConnectionString(m_dsv.Server, m_dsv.User, m_dsv.Password, m_dsv.Integrated, obj._DatabaseName)
                    Dim ds As New DatabaseCompare.Domain.Database(str)
                    If ds.TestConnection() Then
                        m_dsv.IsTestDatabase = True
                        m_dsv.TestDatabase = obj._DatabaseName
                    Else
                        m_dsv.IsTestDatabase = False
                        m_dsv.TestDatabase = ""
                        MsgBox("Database Conenction Fail", MsgBoxStyle.Critical)
                    End If
                End If
            End Using
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            Call Me.SetStatus()
        End Try
    End Sub

    Private Sub mnuCreateScriptDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCreateScriptDatabase.Click, tsbCreateScriptDatabase.Click
        Try
            Dim str As String = DatabaseCompare.Domain.Connection.ConnectionString(m_dsv.Server, m_dsv.User, m_dsv.Password, m_dsv.Integrated, "Master")
            Dim ds As New DatabaseCompare.Domain.Database(str)

            ds.ExecNonQuery("DECLARE @SQL VARCHAR(8000) " & _
                    "SELECT @SQL=COALESCE(@SQL,'')+'Kill '+CAST(spid AS VARCHAR(10))+ '; ' " & _
                    "FROM sys.sysprocesses " & _
                    "WHERE DBID=DB_ID('ScriptDatabase') " & _
                    "EXEC(@SQL) ")

            ds.ExecNonQuery("IF EXISTS (SELECT name FROM sys.databases WHERE name = N'ScriptDatabase') DROP DATABASE [ScriptDatabase]; ")
            ds.ExecNonQuery("CREATE DATABASE [ScriptDatabase];  ")

            m_dsv.IsScriptDatabse = True
            m_dsv.ScriptDatabase = "ScriptDatabase"

            Using obj As New SQLSMO.Wait
                obj._DatabaseServer = m_dsv
                obj._DataSet = m_XML.m_Dataset
                obj.displayDialog()
            End Using


        Catch ex As Exception
            m_dsv.IsScriptDatabse = False
            m_dsv.ScriptDatabase = ""
            MsgBox(ex.Message)
        Finally
            Call Me.SetStatus()
        End Try
    End Sub

    Private Sub tsbUpdateScriptDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbUpdateScriptDatabase.Click
        Try
            If m_dsv.IsScriptDatabse Then
                m_dsv.ScriptDatabase = "ScriptDatabase"

                Using obj As New SQLSMO.Wait
                    obj._DatabaseServer = m_dsv
                    obj._DataSet = m_XML.m_Dataset
                    obj.displayDialog()
                End Using
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            Call Me.SetStatus()
        End Try
    End Sub

    Private Sub mnuCompareChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCompareChanges.Click, tsbCompareChanges.Click
        Try
            lstMissing.Items.Clear()
            BackgroundWorker1.RunWorkerAsync()
        Catch ex As Exception

        End Try
    End Sub

    Dim differences As ArrayList

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        m_dsv.ScriptDatabase = "PMS_Rayaburi"
        Dim str1 As String = DatabaseCompare.Domain.Connection.ConnectionString(m_dsv.Server, m_dsv.User, m_dsv.Password, m_dsv.Integrated, m_dsv.ScriptDatabase)
        Dim Db1 As New DatabaseCompare.Domain.Database(str1)
        Dim str2 As String = DatabaseCompare.Domain.Connection.ConnectionString(m_dsv.Server, m_dsv.User, m_dsv.Password, m_dsv.Integrated, m_dsv.TestDatabase)
        Dim Db2 As New DatabaseCompare.Domain.Database(str2)
        Db1.TestConnection()

        Try
            Db1.ConnectDMO(m_dsv, m_dsv.ScriptDatabase)
            BackgroundWorker1.ReportProgress(1, "Loading Script Database objects")
            Db1.LoadObjects()

        Catch ex As Exception
            MessageBox.Show("Error loading Database Script objects:" & Constants.vbCrLf & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End Try
        Db2.TestConnection()
        Try
            Db2.ConnectDMO(m_dsv, m_dsv.TestDatabase)
            BackgroundWorker1.ReportProgress(1, "Loading Test Database objects")
            Db2.LoadObjects()
        Catch ex As Exception
            MessageBox.Show("Error loading Database Test objects:" & Constants.vbCrLf & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End Try
        BackgroundWorker1.ReportProgress(1, "Comparing Database objects")
        If differences IsNot Nothing Then
            differences.Clear()
            differences = Nothing
        End If
        differences = New ArrayList
        differences.AddRange(Db1.CompareTo(Db2))
        differences.Sort()


        For Each d As DatabaseCompare.Domain.DBDifference In differences
            UpdateList(d)
        Next d

        BackgroundWorker1.ReportProgress(1, "Ready")
    End Sub

    Private Delegate Sub ListDelegate(ByVal statusMessage As DatabaseCompare.Domain.DBDifference)
    Private Sub UpdateList(ByVal message As DatabaseCompare.Domain.DBDifference)
        If Me.InvokeRequired Then
            Dim del As New ListDelegate(AddressOf Me.UpdateList)
            Dim arguments() As Object = {message}
            Me.Invoke(del, arguments)
            Return
        End If

        If message.StatusId = 1 Then
            lstMissing.Items.Add(message)
        Else

        End If

    End Sub

    Private Sub BackgroundWorker1_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles BackgroundWorker1.ProgressChanged
        lblStatus.Text = CStr(e.UserState)
    End Sub

#End Region


#Region " XML Upgarade "
    Private Sub mnuScript_ViewChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScript_ViewChanges.Click
        If lstMissing.SelectedItems.Count <= 0 Then Exit Sub
        Using obj As New frmChanges
            Dim a As DatabaseCompare.Domain.DBDifference = CType(lstMissing.SelectedItem, DatabaseCompare.Domain.DBDifference)
            obj.ScriptDatabase = a.DB1_TextDefinition
            obj.TestDatabase = a.DB2_TextDefinition
            obj.ShowDialog()
        End Using
    End Sub

    Private Sub mnuScript_CreateEntry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScript_CreateEntry.Click
        If lstMissing.SelectedItems.Count <= 0 Then Exit Sub
        Using obj As New EditObject
            If dgvData.RowCount > 0 Then
                EditObject.txtID.Text = dgvData.Rows(dgvData.RowCount - 1).Cells(colId.Index).Value + 1
                EditObject.txtVersion.Text = dgvData.Rows(dgvData.RowCount - 1).Cells(colVersion.Index).Value
            Else
                EditObject.txtID.Text = 1
                EditObject.txtVersion.Text = ""
            End If

            EditObject.txtObject.Text = ""
            Dim a As DatabaseCompare.Domain.DBDifference = CType(lstMissing.SelectedItem, DatabaseCompare.Domain.DBDifference)

            EditObject.txtObject.Text = a.Name
            EditObject.txtScript.Text = a.DB2_TextDefinition

            EditObject.txtCreatedOn.Text = Now
            EditObject.txtModifyOn.Text = Now

            If EditObject.ShowDialog() = Windows.Forms.DialogResult.Yes Then
                m_XML.m_Dataset.Tables(dgvData.DataMember).Rows.Add(New Object() { _
                            CInt(EditObject.txtID.Text) _
                            , EditObject.txtVersion.Text _
                            , 1 _
                            , EditObject.txtObject.Text _
                            , EditObject.txtScript.Text _
                            , EditObject.txtCreatedOn.Text _
                            , EditObject.txtModifyOn.Text _
                            })
            End If

        End Using
    End Sub

    Private Sub mnuScript_UpdateEntry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuScript_UpdateEntry.Click
        If lstMissing.SelectedItems.Count <= 0 Then Exit Sub

    End Sub
#End Region

   
    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        Dim d As New DatabaseServer
        d.Server = "(Local)\EzeeNExtGEN"
        d.User = "sa"
        d.Password = "gReenmAgic113"
        d.Integrated = False
        Dim obj As New DMO_Value(d, "PMS_NextGen")
        obj.LoadObject()

    End Sub
End Class
