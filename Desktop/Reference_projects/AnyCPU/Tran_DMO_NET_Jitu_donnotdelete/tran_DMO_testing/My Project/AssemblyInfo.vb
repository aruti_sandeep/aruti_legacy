﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("tran_DMO_testing")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("eZee Technosys Pvt. Ltd.")> 
<Assembly: AssemblyProduct("tran_DMO_testing")> 
<Assembly: AssemblyCopyright("Copyright © eZee Technosys Pvt. Ltd. 2011")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("c5c690a6-db58-4ac1-8c34-63903c5f3d5b")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
