using System;
public class oString
{
#region  POS Function 
	public enum oAlignment: int
	{
		Left = 0,
		Right = 1,
		Center = 2
	}


	public static string PadRight(string Text, int Length)
	{
		return PadRight(Text, Length, ' ');
	}

    //ORIGINAL LINE: Public Shared Function PadRight(ByVal Text As String, ByVal Length As Integer, Optional ByVal PadChar As Char = " ") As String
	public static string PadRight(string Text, int Length, char PadChar)
	{

		return Padding(Text, Length, oAlignment.Right, PadChar);

	}


	public static string PadLeft(string Text, int Length)
	{
		return PadLeft(Text, Length, ' ');
	}

    //ORIGINAL LINE: Public Shared Function PadLeft(ByVal Text As String, ByVal Length As Integer, Optional ByVal PadChar As Char = " ") As String
	public static string PadLeft(string Text, int Length, char PadChar)
	{

		return Padding(Text, Length, oAlignment.Left, PadChar);

	}


	public static string PadCenter(string Text, int Length)
	{
		return PadCenter(Text, Length, ' ');
	}

    //ORIGINAL LINE: Public Shared Function PadCenter(ByVal Text As String, ByVal Length As Integer, Optional ByVal PadChar As Char = " ") As String
	public static string PadCenter(string Text, int Length, char PadChar)
	{

		return Padding(Text, Length, oAlignment.Center, PadChar);

	}


	public static string Padding(string Text, int Length, oAlignment Alignment)
	{
		return Padding(Text, Length, Alignment, ' ');
	}

	public static string Padding(string Text, int Length)
	{
		return Padding(Text, Length, oAlignment.Left, ' ');
	}

    //ORIGINAL LINE: Public Shared Function Padding(ByVal Text As String, ByVal Length As Integer, Optional ByVal Alignment As oAlignment = oAlignment.Left, Optional ByVal PadChar As Char = " ") As String
	public static string Padding(string Text, int Length, oAlignment Alignment, char PadChar)
	{

		string Result = "";
		try
		{
			if (Length <= 0)
			{
				return "";
			}

			switch (Alignment)
			{
				case oAlignment.Left:
					if (Text.Length > Length)
					{
						Result = Text.Substring(Text.Length - Length);
					}
					else
					{
						Result = Text.PadLeft(Length, PadChar);
					}
					break;
				case oAlignment.Right:
					if (Text.Length > Length)
					{
						Result = Text.Substring(0, Length);
					}
					else
					{
						Result = Text.PadRight(Length, PadChar);
					}
					break;
				case oAlignment.Center:
					Text = new String(PadChar, Length) + Text + new String(PadChar, Length);
					Result = Text.Substring((Text.Length - Length) / 2, Length);
					break;
			}

			return Result;

		}
		catch (Exception ex)
		{
            System.Windows.Forms.MessageBox.Show(ex.Message + "[Padding-oString]");
			return "";
		}
	}


	public static string WordWrap(string Text, int Length, oString.oAlignment Alignment)
	{
		return WordWrap(Text, Length, Alignment, ' ');
	}

	public static string WordWrap(string Text, int Length)
	{
		return WordWrap(Text, Length, oString.oAlignment.Right, ' ');
	}

    //ORIGINAL LINE: Public Shared Function WordWrap(ByVal Text As String, ByVal Length As Integer, Optional ByVal Alignment As oString.oAlignment = oString.oAlignment.Right, Optional ByVal PadChar As Char = " ") As String
    public static string WordWrap(string Text, int Length, oString.oAlignment Alignment, char PadChar)
    {
        string strLine = "'";
        string PrintText = "";
        int intEnter = 0;
        try
        {
            if (Text.Length == 0 | Length == 0)
            {
                return "";
            }

            for (int iCnt = 0; iCnt <= Text.Length; iCnt += Length)
            {
                if (Text.Substring(iCnt).Length > Length)
                {
                    strLine = Text.Substring(iCnt, Length);
                    intEnter = (strLine.IndexOf(System.Environment.NewLine, 0) + 1);
                    if (intEnter > 0)
                    {
                        strLine = Text.Substring(iCnt, intEnter + 1);
                        strLine = strLine.Replace(System.Environment.NewLine, "");
                        iCnt -= (Length - (intEnter + 1));
                    }
                    PrintText += strLine + System.Environment.NewLine;
                }
                else
                {
                    strLine = Text.Substring(iCnt);
                    strLine = Padding(strLine, Length, Alignment, PadChar);
                    PrintText += strLine;
                }
            }
            return PrintText;
        }
        catch (Exception ex)
        {
            System.Windows.Forms.MessageBox.Show(ex.Message + "[WordWrap-oString]");
            return "";
        }
    }

    public static string WordWrapText(string Text, int Length, oString.oAlignment Alignment, char PadChar)
    {
        string strLine = "";
        string PrintText = "";

        try
        {
            if (Text.Length == 0 || Length == 0)
            {
                return "";
            }

            Text = Text.Replace("\n", "");

            foreach (string Line in Text.Split(System.Environment.NewLine.ToCharArray()))
            {
                string strL = Line;

                while (strL.Length > 0)
                {
                    strLine = string.Empty;
                    if (strL.Length <= Length)
                    {
                        strLine = strL;
                        strL = strL.Substring(strLine.Length);
                        strLine = Padding(strLine, Length, Alignment, PadChar);
                        PrintText += strLine + System.Environment.NewLine;
                    }
                    else
                    {
                        foreach (string sWord in strL.Replace(" ", "| ").Split('|'))
                        {
                            if (sWord.Length == 0)
                            {
                                continue;
                            }
                            else if (sWord.Length > Length && strLine.Length == 0)
                            {
                                strLine = sWord.Substring(0, Length);
                                break;
                            }
                            else if (sWord.Length + strLine.Length <= Length)
                            {
                                strLine += sWord;
                            }
                            else
                            {
                                break;
                            }
                        }

                        strL = strL.Substring(strLine.Length);
                        strLine = Padding(strLine, Length, Alignment, PadChar);
                        PrintText += strLine + System.Environment.NewLine;
                    }
                }
            }

            return PrintText.Trim(System.Environment.NewLine.ToCharArray());
        }
        catch (Exception ex)
        {
            System.Windows.Forms.MessageBox.Show(ex.Message + "[WordWrapText-oString]");
            return "";
        }
    }
#endregion
}
