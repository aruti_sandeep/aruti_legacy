﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Xml;

    public sealed class WorksheetPageSetup : IWriter, IReader, ICodeWriter
    {
        private WorksheetPageFooter _footer;
        private WorksheetPageHeader _header;
        private WorksheetPageLayout _layout;
        private WorksheetPageMargins _pageMargins;

        internal WorksheetPageSetup()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._layout != null)
            {
                Util.Traverse(type, this._layout, method, targetObject, "Layout");
            }
            if (this._header != null)
            {
                Util.Traverse(type, this._header, method, targetObject, "Header");
            }
            if (this._footer != null)
            {
                Util.Traverse(type, this._footer, method, targetObject, "Footer");
            }
            if (this._pageMargins != null)
            {
                Util.Traverse(type, this._pageMargins, method, targetObject, "PageMargins");
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if (element2 != null)
                {
                    if (WorksheetPageLayout.IsElement(element2))
                    {
                        ((IReader) this.Layout).ReadXml(element2);
                    }
                    else
                    {
                        if (WorksheetPageHeader.IsElement(element2))
                        {
                            ((IReader) this.Header).ReadXml(element2);
                            continue;
                        }
                        if (WorksheetPageFooter.IsElement(element2))
                        {
                            ((IReader) this.Footer).ReadXml(element2);
                            continue;
                        }
                        if (WorksheetPageMargins.IsElement(element2))
                        {
                            ((IReader) this.PageMargins).ReadXml(element2);
                        }
                    }
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "PageSetup", "urn:schemas-microsoft-com:office:excel");
            if (this._layout != null)
            {
                ((IWriter) this._layout).WriteXml(writer);
            }
            if (this._header != null)
            {
                ((IWriter) this._header).WriteXml(writer);
            }
            if (this._footer != null)
            {
                ((IWriter) this._footer).WriteXml(writer);
            }
            if (this._pageMargins != null)
            {
                ((IWriter) this._pageMargins).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "PageSetup", "urn:schemas-microsoft-com:office:excel");
        }

        public WorksheetPageFooter Footer
        {
            get
            {
                if (this._footer == null)
                {
                    this._footer = new WorksheetPageFooter();
                }
                return this._footer;
            }
        }

        public WorksheetPageHeader Header
        {
            get
            {
                if (this._header == null)
                {
                    this._header = new WorksheetPageHeader();
                }
                return this._header;
            }
        }

        public WorksheetPageLayout Layout
        {
            get
            {
                if (this._layout == null)
                {
                    this._layout = new WorksheetPageLayout();
                }
                return this._layout;
            }
        }

        public WorksheetPageMargins PageMargins
        {
            get
            {
                if (this._pageMargins == null)
                {
                    this._pageMargins = new WorksheetPageMargins();
                }
                return this._pageMargins;
            }
        }
    }
}

