﻿namespace ExcelWriter
{
    using System;
    using System.Collections;
    using System.Reflection;

    public sealed class WorksheetNamedCellCollection : CollectionBase
    {
        internal WorksheetNamedCellCollection()
        {
        }

        public int Add(string name)
        {
            if (name == null)
            {
                throw new ArgumentNullException("name");
            }
            return base.InnerList.Add(name);
        }

        public bool Contains(string item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(string[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(string item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, string item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(string item)
        {
            base.InnerList.Remove(item);
        }

        public string this[int index]
        {
            get
            {
                return (string) base.InnerList[index];
            }
        }
    }
}

