﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class Crn : IWriter, IReader, ICodeWriter
    {
        private int _colFirst = -2147483648;
        private int _colLast = -2147483648;
        private NumberCollection _numbers;
        private string _row;
        private string _text;

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._row != null)
            {
                Util.AddAssignment(method, targetObject, "Row", this._row);
            }
            if (this._colFirst != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "ColFirst", this._colFirst);
            }
            if (this._colLast != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "ColLast", this._colLast);
            }
            if (this._numbers != null)
            {
                ((ICodeWriter) this._numbers).WriteTo(type, method, targetObject);
            }
            if (this._text != null)
            {
                Util.AddAssignment(method, targetObject, "Text", this._text);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if (element2 != null)
                {
                    if (Util.IsElement(element2, "Row", "urn:schemas-microsoft-com:office:excel"))
                    {
                        this._row = element2.InnerText;
                    }
                    else
                    {
                        if (Util.IsElement(element2, "ColFirst", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._colFirst = int.Parse(element2.InnerText);
                            continue;
                        }
                        if (Util.IsElement(element2, "ColLast", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._colLast = int.Parse(element2.InnerText);
                            continue;
                        }
                        if (Util.IsElement(element2, "Number", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this.Numbers.Add(element2.InnerText);
                            continue;
                        }
                        if (Util.IsElement(element2, "Text", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._text = element2.InnerText;
                        }
                    }
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "Crn", "urn:schemas-microsoft-com:office:excel");
            if (this._row != null)
            {
                writer.WriteElementString("Row", "urn:schemas-microsoft-com:office:excel", this._row);
            }
            if (this._colFirst != -2147483648)
            {
                writer.WriteElementString("ColFirst", "urn:schemas-microsoft-com:office:excel", this._colFirst.ToString());
            }
            if (this._colLast != -2147483648)
            {
                writer.WriteElementString("ColLast", "urn:schemas-microsoft-com:office:excel", this._colLast.ToString());
            }
            if (this._numbers != null)
            {
                ((IWriter) this._numbers).WriteXml(writer);
            }
            if (this._text != null)
            {
                writer.WriteElementString("Text", "urn:schemas-microsoft-com:office:excel", this._text);
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Crn", "urn:schemas-microsoft-com:office:excel");
        }

        public int ColFirst
        {
            get
            {
                if (this._colFirst == -2147483648)
                {
                    return 0;
                }
                return this._colFirst;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Invalid range, > 0");
                }
                this._colFirst = value;
            }
        }

        public int ColLast
        {
            get
            {
                if (this._colLast == -2147483648)
                {
                    return 0;
                }
                return this._colLast;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException("Invalid range, > 0");
                }
                this._colLast = value;
            }
        }

        public NumberCollection Numbers
        {
            get
            {
                if (this._numbers == null)
                {
                    this._numbers = new NumberCollection();
                }
                return this._numbers;
            }
        }

        public string Row
        {
            get
            {
                return this._row;
            }
            set
            {
                this._row = value;
            }
        }

        public string Text
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
            }
        }
    }
}

