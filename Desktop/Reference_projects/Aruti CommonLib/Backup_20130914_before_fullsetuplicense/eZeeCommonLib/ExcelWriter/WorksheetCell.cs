﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetCell : IWriter, IReader, ICodeWriter
    {
        private WorksheetComment _comment;
        private WorksheetCellData _data;
        private string _formula;
        private string _href;
        private int _index;
        private int _mergeAcross;
        private int _mergeDown;
        private WorksheetNamedCellCollection _namedCell;
        internal WorksheetRow _row;
        private string _styleID;

        public WorksheetCell()
        {
            this._mergeAcross = -1;
            this._mergeDown = -1;
        }

        public WorksheetCell(string text)
        {
            this._mergeAcross = -1;
            this._mergeDown = -1;
            this.Data.Text = text;
            this.Data.Type = DataType.String;
        }

        public WorksheetCell(string text, DataType type)
        {
            this._mergeAcross = -1;
            this._mergeDown = -1;
            this.Data.Text = text;
            this.Data.Type = type;
        }

        public WorksheetCell(string text, string styleID)
        {
            this._mergeAcross = -1;
            this._mergeDown = -1;
            this.Data.Text = text;
            this._styleID = styleID;
            this.Data.Type = DataType.String;
        }

        public WorksheetCell(string text, DataType type, string styleID)
        {
            this._mergeAcross = -1;
            this._mergeDown = -1;
            this.Data.Text = text;
            this.Data.Type = type;
            this._styleID = styleID;
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._styleID != null)
            {
                Util.AddAssignment(method, targetObject, "StyleID", this._styleID);
            }
            if (this._data != null)
            {
                Util.Traverse(type, this._data, method, targetObject, "Data");
            }
            if (this._index != 0)
            {
                Util.AddAssignment(method, targetObject, "Index", this._index);
            }
            if (this._mergeAcross > 0)
            {
                Util.AddAssignment(method, targetObject, "MergeAcross", this._mergeAcross);
            }
            if (this._mergeDown >= 0)
            {
                Util.AddAssignment(method, targetObject, "MergeDown", this._mergeDown);
            }
            if (this._formula != null)
            {
                Util.AddAssignment(method, targetObject, "Formula", this._formula);
            }
            if (this._href != null)
            {
                Util.AddAssignment(method, targetObject, "HRef", this._href);
            }
            if (this._comment != null)
            {
                Util.Traverse(type, this._comment, method, targetObject, "Comment");
            }
            if (this._namedCell != null)
            {
                foreach (string str in this._namedCell)
                {
                    method.Statements.Add(new CodeMethodInvokeExpression(new CodePropertyReferenceExpression(targetObject, "NamedCell"), "Add", new CodeExpression[] { new CodePrimitiveExpression(str) }));
                }
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            this._index = Util.GetAttribute(element, "Index", "urn:schemas-microsoft-com:office:spreadsheet", 0);
            this._mergeAcross = Util.GetAttribute(element, "MergeAcross", "urn:schemas-microsoft-com:office:spreadsheet", -1);
            this._mergeDown = Util.GetAttribute(element, "MergeDown", "urn:schemas-microsoft-com:office:spreadsheet", -1);
            this._styleID = Util.GetAttribute(element, "StyleID", "urn:schemas-microsoft-com:office:spreadsheet");
            this._formula = Util.GetAttribute(element, "Formula", "urn:schemas-microsoft-com:office:spreadsheet");
            this._href = Util.GetAttribute(element, "HRef", "urn:schemas-microsoft-com:office:spreadsheet");
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if (element2 != null)
                {
                    if (WorksheetCellData.IsElement(element2))
                    {
                        ((IReader) this.Data).ReadXml(element2);
                    }
                    else
                    {
                        if (WorksheetComment.IsElement(element2))
                        {
                            ((IReader) this.Comment).ReadXml(element2);
                            continue;
                        }
                        if (element2.LocalName == "NamedCell")
                        {
                            string name = Util.GetAttribute(element2, "Name", "urn:schemas-microsoft-com:office:spreadsheet");
                            if ((name != null) && (name.Length > 0))
                            {
                                this.NamedCell.Add(name);
                            }
                        }
                    }
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("s", "Cell", "urn:schemas-microsoft-com:office:spreadsheet");
            if (this._index != 0)
            {
                writer.WriteAttributeString("s", "Index", "urn:schemas-microsoft-com:office:spreadsheet", this._index.ToString());
            }
            if (this._mergeAcross > 0)
            {
                writer.WriteAttributeString("s", "MergeAcross", "urn:schemas-microsoft-com:office:spreadsheet", this._mergeAcross.ToString());
            }
            if (this._mergeDown >= 0)
            {
                writer.WriteAttributeString("s", "MergeDown", "urn:schemas-microsoft-com:office:spreadsheet", this._mergeDown.ToString());
            }
            if (this._styleID != null)
            {
                writer.WriteAttributeString("s", "StyleID", "urn:schemas-microsoft-com:office:spreadsheet", this._styleID);
            }
            if (this._formula != null)
            {
                writer.WriteAttributeString("s", "Formula", "urn:schemas-microsoft-com:office:spreadsheet", this._formula);
            }
            if (this._href != null)
            {
                writer.WriteAttributeString("s", "HRef", "urn:schemas-microsoft-com:office:spreadsheet", this._href);
            }
            if (this._comment != null)
            {
                ((IWriter) this._comment).WriteXml(writer);
            }
            if (this._data != null)
            {
                ((IWriter) this._data).WriteXml(writer);
            }
            if (this._namedCell != null)
            {
                foreach (string str in this._namedCell)
                {
                    writer.WriteStartElement("s", "NamedCell", "urn:schemas-microsoft-com:office:spreadsheet");
                    writer.WriteAttributeString("s", "Name", "urn:schemas-microsoft-com:office:spreadsheet", str);
                    writer.WriteEndElement();
                }
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Cell", "urn:schemas-microsoft-com:office:spreadsheet");
        }

        public WorksheetComment Comment
        {
            get
            {
                if (this._comment == null)
                {
                    this._comment = new WorksheetComment();
                }
                return this._comment;
            }
        }

        public WorksheetCellData Data
        {
            get
            {
                if (this._data == null)
                {
                    this._data = new WorksheetCellData(this);
                }
                return this._data;
            }
        }

        public string Formula
        {
            get
            {
                return this._formula;
            }
            set
            {
                this._formula = value;
            }
        }

        public string HRef
        {
            get
            {
                return this._href;
            }
            set
            {
                this._href = value;
            }
        }

        public int Index
        {
            get
            {
                return this._index;
            }
            set
            {
                this._index = value;
            }
        }

        internal bool IsSimple
        {
            get
            {
                return (((((this._styleID != null) && (this._data != null)) && (this._data.IsSimple && (this._index == 0))) && (((this._mergeAcross <= 0) && (this._mergeDown < 0)) && ((this._formula == null) && (this._href == null)))) && ((this._comment == null) && (this._namedCell == null)));
            }
        }

        public int MergeAcross
        {
            get
            {
                if (this._mergeAcross == -1)
                {
                    return 0;
                }
                return this._mergeAcross;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                this._mergeAcross = value;
            }
        }

        public int MergeDown
        {
            get
            {
                if (this._mergeDown == -1)
                {
                    return 0;
                }
                return this._mergeDown;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("value");
                }
                this._mergeDown = value;
            }
        }

        public WorksheetNamedCellCollection NamedCell
        {
            get
            {
                if (this._namedCell == null)
                {
                    this._namedCell = new WorksheetNamedCellCollection();
                }
                return this._namedCell;
            }
        }

        public WorksheetRow Row
        {
            get
            {
                return this._row;
            }
        }

        public string StyleID
        {
            get
            {
                return this._styleID;
            }
            set
            {
                this._styleID = value;
            }
        }
    }
}

