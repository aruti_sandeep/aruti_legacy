﻿namespace ExcelWriter
{
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Xml;

    public sealed class PivotItemCollection : CollectionBase, IWriter
    {
        internal PivotItemCollection()
        {
        }

        public int Add(PivotItem pivotItem)
        {
            return base.InnerList.Add(pivotItem);
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(PivotItem item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(PivotItem[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(PivotItem item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, PivotItem item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(PivotItem item)
        {
            base.InnerList.Remove(item);
        }

        public PivotItem this[int index]
        {
            get
            {
                return (PivotItem) base.InnerList[index];
            }
        }
    }
}

