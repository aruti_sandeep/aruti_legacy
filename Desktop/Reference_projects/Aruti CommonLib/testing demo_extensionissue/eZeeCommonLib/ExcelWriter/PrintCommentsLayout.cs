﻿namespace ExcelWriter
{
    using System;

    public enum PrintCommentsLayout
    {
        None,
        SheetEnd,
        InPlace
    }
}

