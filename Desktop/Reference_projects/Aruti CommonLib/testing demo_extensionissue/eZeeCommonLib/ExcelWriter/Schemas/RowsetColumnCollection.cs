﻿namespace ExcelWriter.Schemas
{
    using ExcelWriter;
    using System;
    using System.Collections;
    using System.Reflection;
    using System.Xml;

    public sealed class RowsetColumnCollection : CollectionBase, IWriter
    {
        internal RowsetColumnCollection()
        {
        }

        public int Add(RowsetColumn column)
        {
            return base.InnerList.Add(column);
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(RowsetColumn item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(RowsetColumn[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(RowsetColumn item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, RowsetColumn item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(RowsetColumn item)
        {
            base.InnerList.Remove(item);
        }

        public RowsetColumn this[int index]
        {
            get
            {
                return (RowsetColumn) base.InnerList[index];
            }
        }
    }
}

