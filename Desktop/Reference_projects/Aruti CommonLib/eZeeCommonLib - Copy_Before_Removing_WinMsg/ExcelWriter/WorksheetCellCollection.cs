﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Collections;
    using System.Reflection;
    using System.Xml;

    public sealed class WorksheetCellCollection : CollectionBase, IWriter, ICodeWriter
    {
        private WorksheetRow _row;

        internal WorksheetCellCollection(WorksheetRow row)
        {
            if (row == null)
            {
                throw new ArgumentNullException("row");
            }
            this._row = row;
        }

        public WorksheetCell Add()
        {
            WorksheetCell cell = new WorksheetCell();
            this.Add(cell);
            return cell;
        }

        public int Add(WorksheetCell cell)
        {
            if (cell == null)
            {
                throw new ArgumentNullException("cell");
            }
            cell._row = this._row;
            return base.InnerList.Add(cell);
        }

        public WorksheetCell Add(string text)
        {
            WorksheetCell cell = new WorksheetCell(text);
            this.Add(cell);
            return cell;
        }

        public WorksheetCell Add(string text, DataType dataType, string styleID)
        {
            WorksheetCell cell = new WorksheetCell(text, dataType, styleID);
            this.Add(cell);
            return cell;
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                WorksheetCell cell = this[i];
                if (cell.IsSimple)
                {
                    method.Statements.Add(new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[] { new CodePrimitiveExpression(cell.Data.Text), Util.GetRightExpressionForValue(cell.Data.Type, typeof(DataType)), new CodePrimitiveExpression(cell.StyleID) }));
                }
                else
                {
                    string variableName = "cell";
                    if (Worksheet.cellDeclaration == null)
                    {
                        Worksheet.cellDeclaration = new CodeVariableDeclarationStatement(typeof(WorksheetCell), "cell");
                        method.Statements.Add(Worksheet.cellDeclaration);
                    }
                    CodeAssignStatement statement = new CodeAssignStatement(new CodeVariableReferenceExpression("cell"), new CodeMethodInvokeExpression(targetObject, "Add", new CodeExpression[0]));
                    method.Statements.Add(statement);
                    ((ICodeWriter) cell).WriteTo(type, method, new CodeVariableReferenceExpression(variableName));
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            for (int i = 0; i < base.InnerList.Count; i++)
            {
                ((IWriter) base.InnerList[i]).WriteXml(writer);
            }
        }

        public bool Contains(WorksheetCell item)
        {
            return base.InnerList.Contains(item);
        }

        public void CopyTo(WorksheetCell[] array, int index)
        {
            base.InnerList.CopyTo(array, index);
        }

        public int IndexOf(WorksheetCell item)
        {
            return base.InnerList.IndexOf(item);
        }

        public void Insert(int index, WorksheetCell item)
        {
            base.InnerList.Insert(index, item);
        }

        public void Remove(WorksheetCell item)
        {
            base.InnerList.Remove(item);
        }

        public object[] ToArray()
        {
            return base.InnerList.ToArray();
        }

        public WorksheetCell this[int index]
        {
            get
            {
                return (WorksheetCell) base.InnerList[index];
            }
            set
            {
                if (value != null)
                {
                    value._row = this._row;
                }
                base.InnerList[index] = value;
            }
        }
    }
}

