//************************************************************************************************************************************
//Class Name : clsINI.vb
//Purpose    : Read Write from INI File.
//Date       : 
//Written By : Jitu
//Modified   : Naimish
//Note       : MergeJitu
//************************************************************************************************************************************

namespace eZeeCommonLib
{
    /// <summary>
    /// Read Write from INI File.
    /// </summary>
    public class clsINI
    {
        private string strFilename;

        #region  API Functions
        [System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileStringA", ExactSpelling = true, CharSet = System.Runtime.InteropServices.CharSet.Ansi, SetLastError = true)]
        private static extern int GetPrivateProfileString(string lpApplicationName, string lpKeyName, string lpDefault, System.Text.StringBuilder lpReturnedString, int nSize, string lpFileName);

        [System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint = "WritePrivateProfileStringA", ExactSpelling = true, CharSet = System.Runtime.InteropServices.CharSet.Ansi, SetLastError = true)]
        private static extern int WritePrivateProfileString(string lpApplicationName, string lpKeyName, string lpString, string lpFileName);

        [System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileIntA", ExactSpelling = true, CharSet = System.Runtime.InteropServices.CharSet.Ansi, SetLastError = true)]
        private static extern int GetPrivateProfileInt(string lpApplicationName, string lpKeyName, int nDefault, string lpFileName);

        [System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint = "WritePrivateProfileStringA", ExactSpelling = true, CharSet = System.Runtime.InteropServices.CharSet.Ansi, SetLastError = true)]
        private static extern int FlushPrivateProfileString(int lpApplicationName, int lpKeyName, int lpString, string lpFileName);
        #endregion

        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="Filename">INI File Name with full path.</param> 
        public clsINI(string Filename)
        {
            strFilename = Filename;
        }

        #region  Properties
        /// <summary>
        /// Determine the INI file name which link with class object.
        /// </summary>
        public string FileName
        {
            get
            {
                return strFilename;
            }
        }
        #endregion

        /// <summary>
        /// Read <b>String</b> value from INI file.
        /// </summary>
        /// <param name="Section">INI Section Name.</param> 
        /// <param name="Key">Section Key Name.</param> 
        /// <param name="Default">Optional Para, Default Value of Key.</param> 
        /// <returns>String</returns> 

        public string GetString(string Section, string Key)
        {
            return GetString(Section, Key, "");
        }

        //ORIGINAL LINE: Public Function GetString(ByVal Section As String, ByVal Key As String, Optional ByVal @Default As String = "") As String
        public string GetString(string Section, string Key, string Default)
        {

            // Returns a string from your INI file
            int intCharCount = 0;
            string strResult = "";
            System.Text.StringBuilder objResult = new System.Text.StringBuilder(256);

            //intCharCount = GetPrivateProfileString(Section, Key, _
            //   [Default], objResult, objResult.Capacity, strFilename)
            intCharCount = GetPrivateProfileString(Section, Key, "", objResult, objResult.Capacity, strFilename);

            if (intCharCount > 0)
            {
                strResult = objResult.ToString().Substring(0, intCharCount);
            }
            else
            {
                WriteString(Section, Key, Default);
                strResult = Default;
            }

            return strResult;

        }

        /// <summary>
        /// Read <b>Integer</b> value from INI file.
        /// </summary>
        /// <param name="Section">INI Section Name.</param> 
        /// <param name="Key">Section Key Name.</param> 
        /// <param name="Default">Optional Para, Default Value of Key.</param> 
        /// <returns>Integer</returns> 

        public int GetInteger(string Section, string Key)
        {
            return GetInteger(Section, Key, 0);
        }

        //ORIGINAL LINE: Public Function GetInteger(ByVal Section As String, ByVal Key As String, Optional ByVal @Default As Integer = 0) As Integer
        public int GetInteger(string Section, string Key, int Default)
        {

            // Returns an integer from your INI file
            int intResult = 0;
            intResult = GetPrivateProfileInt(Section, Key, 0, strFilename);

            if (intResult == 0)
            {
                WriteInteger(Section, Key, Default);
                intResult = Default;
            }

            return intResult;
        }

        /// <summary>
        /// Read <b>Boolean</b> value from INI file.
        /// </summary>
        /// <param name="Section">INI Section Name.</param> 
        /// <param name="Key">Section Key Name.</param> 
        /// <param name="Default">Optional Para, Default Value of Key.</param> 
        /// <returns>Boolean</returns> 

        public bool GetBoolean(string Section, string Key)
        {
            return GetBoolean(Section, Key, false);
        }

        //ORIGINAL LINE: Public Function GetBoolean(ByVal Section As String, ByVal Key As String, Optional ByVal @Default As Boolean = false) As Boolean
        public bool GetBoolean(string Section, string Key, bool Default)
        {

            // Returns a boolean from your INI file
            bool blnFlag = false;

            blnFlag = (GetPrivateProfileInt(Section, Key, 0, strFilename) == 1);

            if (blnFlag == false)
            {
                WriteBoolean(Section, Key, Default);
                blnFlag = Default;
            }

            return blnFlag;
        }

        /// <summary>
        /// Write <b>String</b> value in INI file.
        /// </summary>
        /// <param name="Section">INI Section Name.</param> 
        /// <param name="Key">Section Key Name.</param> 
        /// <param name="Value">String value of key.</param> 
        public void WriteString(string Section, string Key, string Value)
        {

            // Writes a string to your INI file
            WritePrivateProfileString(Section, Key, Value, strFilename);
            Flush();

        }

        /// <summary>
        /// Write <b>Integer</b> value in INI file.
        /// </summary>
        /// <param name="Section">INI Section Name.</param> 
        /// <param name="Key">Section Key Name.</param> 
        /// <param name="Value">Integer value of key.</param> 
        public void WriteInteger(string Section, string Key, int Value)
        {
            // Writes an integer to your INI file
            WriteString(Section, Key, System.Convert.ToString(Value));
            Flush();
        }

        /// <summary>
        /// Write <b>Boolean</b> value in INI file.
        /// </summary>
        /// <param name="Section">INI Section Name.</param> 
        /// <param name="Key">Section Key Name.</param> 
        /// <param name="Value">Boolean value of key.</param> 
        public void WriteBoolean(string Section, string Key, bool Value)
        {
            // Writes a boolean to your INI file
            WriteString(Section, Key, System.Convert.ToString(System.Convert.ToInt32(Value)));
            Flush();
        }

        /// <summary>
        /// Stores all the cached changes to INI file.
        /// </summary>
        private void Flush()
        {
            FlushPrivateProfileString(0, 0, 0, strFilename);
        }

    }
}