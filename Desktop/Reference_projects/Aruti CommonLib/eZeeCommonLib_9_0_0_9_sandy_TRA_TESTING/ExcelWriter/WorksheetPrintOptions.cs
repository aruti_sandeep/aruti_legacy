﻿namespace ExcelWriter
{
    using System;
    using System.CodeDom;
    using System.Globalization;
    using System.Xml;

    public sealed class WorksheetPrintOptions : IWriter, IReader, ICodeWriter
    {
        private bool _blackAndWhite;
        private PrintCommentsLayout _commentsLayout;
        private bool _draftQuality;
        private int _fitHeight = 1;
        private int _fitWidth = 1;
        private bool _gridLines;
        private int _horizontalResolution = 600;
        private bool _leftToRight;
        private int _paperSizeIndex = -2147483648;
        private PrintErrorsOption _printErrors;
        private bool _rowColHeadings;
        private int _scale = 100;
        private bool _validPrinterInfo;
        private int _verticalResolution = 600;

        internal WorksheetPrintOptions()
        {
        }

        void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
        {
            if (this._paperSizeIndex != -2147483648)
            {
                Util.AddAssignment(method, targetObject, "PaperSizeIndex", this._paperSizeIndex);
            }
            if (this._horizontalResolution != 600)
            {
                Util.AddAssignment(method, targetObject, "HorizontalResolution", this._horizontalResolution);
            }
            if (this._verticalResolution != 600)
            {
                Util.AddAssignment(method, targetObject, "VerticalResolution", this._verticalResolution);
            }
            if (this._blackAndWhite)
            {
                Util.AddAssignment(method, targetObject, "BlackAndWhite", true);
            }
            if (this._draftQuality)
            {
                Util.AddAssignment(method, targetObject, "DraftQuality", true);
            }
            if (this._gridLines)
            {
                Util.AddAssignment(method, targetObject, "Gridlines", true);
            }
            if (this._scale != 100)
            {
                Util.AddAssignment(method, targetObject, "Scale", this._scale);
            }
            if (this._fitWidth != 1)
            {
                Util.AddAssignment(method, targetObject, "FitWidth", this._fitWidth);
            }
            if (this._fitHeight != 1)
            {
                Util.AddAssignment(method, targetObject, "FitHeight", this._fitHeight);
            }
            if (this._leftToRight)
            {
                Util.AddAssignment(method, targetObject, "LeftToRight", true);
            }
            if (this._rowColHeadings)
            {
                Util.AddAssignment(method, targetObject, "RowColHeadings", true);
            }
            if (this._printErrors != PrintErrorsOption.Displayed)
            {
                Util.AddAssignment(method, targetObject, "PrintErrors", this._printErrors);
            }
            if (this._commentsLayout != PrintCommentsLayout.None)
            {
                Util.AddAssignment(method, targetObject, "CommentsLayout", this._commentsLayout);
            }
            if (this._validPrinterInfo)
            {
                Util.AddAssignment(method, targetObject, "ValidPrinterInfo", true);
            }
        }

        void IReader.ReadXml(XmlElement element)
        {
            if (!IsElement(element))
            {
                throw new ArgumentException("Invalid element", "element");
            }
            foreach (XmlNode node in element.ChildNodes)
            {
                XmlElement element2 = node as XmlElement;
                if (element2 != null)
                {
                    if (Util.IsElement(element2, "Gridlines", "urn:schemas-microsoft-com:office:excel"))
                    {
                        this._gridLines = true;
                    }
                    else
                    {
                        if (Util.IsElement(element2, "BlackAndWhite", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._blackAndWhite = true;
                            continue;
                        }
                        if (Util.IsElement(element2, "DraftQuality", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._draftQuality = true;
                            continue;
                        }
                        if (Util.IsElement(element2, "ValidPrinterInfo", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._validPrinterInfo = true;
                            continue;
                        }
                        if (Util.IsElement(element2, "PaperSizeIndex", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._paperSizeIndex = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "HorizontalResolution", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._horizontalResolution = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "VerticalResolution", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._verticalResolution = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "RowColHeadings", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._rowColHeadings = true;
                            continue;
                        }
                        if (Util.IsElement(element2, "LeftToRight", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._leftToRight = true;
                            continue;
                        }
                        if (Util.IsElement(element2, "Scale", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._scale = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "FitWidth", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._fitWidth = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "FitHeight", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._fitHeight = int.Parse(element2.InnerText, CultureInfo.InvariantCulture);
                            continue;
                        }
                        if (Util.IsElement(element2, "PrintErrors", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._printErrors = (PrintErrorsOption)Enum.Parse(typeof(PrintErrorsOption), element2.InnerText);
                            continue;
                        }
                        if (Util.IsElement(element2, "CommentsLayout", "urn:schemas-microsoft-com:office:excel"))
                        {
                            this._commentsLayout = (PrintCommentsLayout)Enum.Parse(typeof(PrintCommentsLayout), element2.InnerText);
                        }
                    }
                }
            }
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "Print", "urn:schemas-microsoft-com:office:excel");
            if (this._paperSizeIndex != -2147483648)
            {
                writer.WriteElementString("PaperSizeIndex", "urn:schemas-microsoft-com:office:excel", this._paperSizeIndex.ToString());
            }
            writer.WriteElementString("HorizontalResolution", "urn:schemas-microsoft-com:office:excel", this._horizontalResolution.ToString());
            writer.WriteElementString("VerticalResolution", "urn:schemas-microsoft-com:office:excel", this._verticalResolution.ToString());
            if (this._blackAndWhite)
            {
                writer.WriteElementString("BlackAndWhite", "urn:schemas-microsoft-com:office:excel", "");
            }
            if (this._draftQuality)
            {
                writer.WriteElementString("DraftQuality", "urn:schemas-microsoft-com:office:excel", "");
            }
            if (this._gridLines)
            {
                writer.WriteElementString("Gridlines", "urn:schemas-microsoft-com:office:excel", "");
            }
            if (this._scale != 100)
            {
                writer.WriteElementString("Scale", "urn:schemas-microsoft-com:office:excel", this._scale.ToString());
            }
            if (this._fitWidth != 1)
            {
                writer.WriteElementString("FitWidth", "urn:schemas-microsoft-com:office:excel", this._fitWidth.ToString());
            }
            if (this._fitHeight != 1)
            {
                writer.WriteElementString("FitHeight", "urn:schemas-microsoft-com:office:excel", this._fitHeight.ToString());
            }
            if (this._leftToRight)
            {
                writer.WriteElementString("LeftToRight", "urn:schemas-microsoft-com:office:excel", "");
            }
            if (this._rowColHeadings)
            {
                writer.WriteElementString("RowColHeadings", "urn:schemas-microsoft-com:office:excel", "");
            }
            if (this._printErrors != PrintErrorsOption.Displayed)
            {
                writer.WriteElementString("PrintErrors", "urn:schemas-microsoft-com:office:excel", this._printErrors.ToString());
            }
            if (this._commentsLayout != PrintCommentsLayout.None)
            {
                writer.WriteElementString("CommentsLayout", "urn:schemas-microsoft-com:office:excel", this._commentsLayout.ToString());
            }
            if (this._validPrinterInfo)
            {
                writer.WriteElementString("ValidPrinterInfo", "urn:schemas-microsoft-com:office:excel", "");
            }
            writer.WriteEndElement();
        }

        internal static bool IsElement(XmlElement element)
        {
            return Util.IsElement(element, "Print", "urn:schemas-microsoft-com:office:excel");
        }

        public bool BlackAndWhite
        {
            get
            {
                return this._blackAndWhite;
            }
            set
            {
                this._blackAndWhite = value;
            }
        }

        public PrintCommentsLayout CommentsLayout
        {
            get
            {
                return this._commentsLayout;
            }
            set
            {
                this._commentsLayout = value;
            }
        }

        public bool DraftQuality
        {
            get
            {
                return this._draftQuality;
            }
            set
            {
                this._draftQuality = value;
            }
        }

        public int FitHeight
        {
            get
            {
                return this._fitHeight;
            }
            set
            {
                this._fitHeight = value;
            }
        }

        public int FitWidth
        {
            get
            {
                return this._fitWidth;
            }
            set
            {
                this._fitWidth = value;
            }
        }

        public bool GridLines
        {
            get
            {
                return this._gridLines;
            }
            set
            {
                this._gridLines = value;
            }
        }

        public int HorizontalResolution
        {
            get
            {
                return this._horizontalResolution;
            }
            set
            {
                this._horizontalResolution = value;
            }
        }

        public bool LeftToRight
        {
            get
            {
                return this._leftToRight;
            }
            set
            {
                this._leftToRight = value;
            }
        }

        public int PaperSizeIndex
        {
            get
            {
                if (this._paperSizeIndex == -2147483648)
                {
                    return 0;
                }
                return this._paperSizeIndex;
            }
            set
            {
                this._paperSizeIndex = value;
            }
        }

        public PrintErrorsOption PrintErrors
        {
            get
            {
                return this._printErrors;
            }
            set
            {
                this._printErrors = value;
            }
        }

        public bool RowColHeadings
        {
            get
            {
                return this._rowColHeadings;
            }
            set
            {
                this._rowColHeadings = value;
            }
        }

        public int Scale
        {
            get
            {
                return this._scale;
            }
            set
            {
                this._scale = value;
            }
        }

        public bool ValidPrinterInfo
        {
            get
            {
                return this._validPrinterInfo;
            }
            set
            {
                this._validPrinterInfo = value;
            }
        }

        public int VerticalResolution
        {
            get
            {
                return this._verticalResolution;
            }
            set
            {
                this._verticalResolution = value;
            }
        }
    }
}

