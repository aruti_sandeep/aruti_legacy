﻿namespace ExcelWriter.Schemas
{
    using ExcelWriter;
    using System;
    using System.Xml;

    public sealed class RowsetData : IWriter
    {
        private RowsetRowCollection _rows;

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("rs", "data", "urn:schemas-microsoft-com:rowset");
            if (this._rows != null)
            {
                ((IWriter) this._rows).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public RowsetRowCollection Rows
        {
            get
            {
                if (this._rows == null)
                {
                    this._rows = new RowsetRowCollection();
                }
                return this._rows;
            }
        }
    }
}

