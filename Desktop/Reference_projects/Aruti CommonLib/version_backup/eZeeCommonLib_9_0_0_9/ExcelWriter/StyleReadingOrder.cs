﻿namespace ExcelWriter
{
    using System;

    public enum StyleReadingOrder
    {
        NotSet,
        Context,
        RightToLeft,
        LeftToRight
    }
}

