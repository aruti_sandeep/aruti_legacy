﻿namespace ExcelWriter
{
    using ExcelWriter.Schemas;
    using System;
    using System.Globalization;
    using System.Xml;

    public sealed class PivotCache : IWriter
    {
        private int _cacheIndex = -2147483648;
        private RowsetData _rowsetData;
        private ExcelWriter.Schemas.Schema _schema;

        internal PivotCache()
        {
        }

        void IWriter.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("x", "PivotCache", "urn:schemas-microsoft-com:office:excel");
            if (this._cacheIndex != -2147483648)
            {
                writer.WriteElementString("CacheIndex", "urn:schemas-microsoft-com:office:excel", this._cacheIndex.ToString());
            }
            if (this._schema != null)
            {
                ((IWriter) this._schema).WriteXml(writer);
            }
            if (this._rowsetData != null)
            {
                ((IWriter) this._rowsetData).WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        public int CacheIndex
        {
            get
            {
                return this._cacheIndex;
            }
            set
            {
                this._cacheIndex = value;
            }
        }

        public RowsetData Data
        {
            get
            {
                if (this._rowsetData == null)
                {
                    this._rowsetData = new RowsetData();
                }
                return this._rowsetData;
            }
        }

        public ExcelWriter.Schemas.Schema Schema
        {
            get
            {
                if (this._schema == null)
                {
                    this._schema = new ExcelWriter.Schemas.Schema();
                }
                return this._schema;
            }
        }
    }
}

