using System;

public class eZeeMsgBox
{
    private static bool mblnLastCheckResult = true;
    public static bool LastCheckResult
    {
        get
        {
            return mblnLastCheckResult;
        }
    }

    private static bool mblnRightToLeft = false;
    public static bool RightToLeft
    {
        get
        {
            return mblnRightToLeft;
        }
        set
        {
            mblnRightToLeft = value;
        }
    }
    public static System.Windows.Forms.DialogResult Show(string Message, enMsgBoxStyle oenMsgBoxStyle, string Title, bool showCheckBox)
    {
        return Show(Message, oenMsgBoxStyle, Title, showCheckBox, 0);
    }

    public static System.Windows.Forms.DialogResult Show(string Message, enMsgBoxStyle oenMsgBoxStyle, string Title)
    {
        return Show(Message, oenMsgBoxStyle, Title, false, 0);
    }

    public static System.Windows.Forms.DialogResult Show(string Message, enMsgBoxStyle oenMsgBoxStyle)
    {
        return Show(Message, oenMsgBoxStyle, null, false, 0);
    }

    public static System.Windows.Forms.DialogResult Show(string Message)
    {
        return Show(Message, enMsgBoxStyle.Information, null, false, 0);
    }

    public static System.Windows.Forms.DialogResult Show(string Message, enMsgBoxStyle oenMsgBoxStyle, string Title, bool showCheckBox, int AutoCloseAfter)
    {
        eZeeMessageBox frm = new eZeeMessageBox();
        try
        {
            mblnLastCheckResult = true;

            System.Windows.Forms.DialogResult oDialogResult = new System.Windows.Forms.DialogResult();

            if (mblnRightToLeft == true)
            {
                frm.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
                frm.RightToLeftLayout = true;
                modGlobal.ctlRightToLeftlayOut( frm);
            }

            oDialogResult = frm.Display(Message, oenMsgBoxStyle, Title, showCheckBox, AutoCloseAfter);
            mblnLastCheckResult = frm.Chekced;
            return oDialogResult;
        }
        catch (Exception ex)
        {
            ex.ToString();
            return 0;
        }
        finally
        {
            frm = null;
        }
    }

	public class MessageBoxButton
	{
        private static System.Drawing.Font mfntMsgFont = new System.Drawing.Font("Tahoma", 8 );
 
        public static System.Drawing.Font Message_Font
        {
            get
            {
                return mfntMsgFont;
            }
            set
            {
                mfntMsgFont = value;
            }
        }

		private static string mstrDefaultTitle = "Aruti";
		public static string DefaultTitle
		{
			get
			{
				return mstrDefaultTitle;
			}
			set
			{
				mstrDefaultTitle = value;
			}
		}

		private static string mstrAbort = "Abort";
		public static string Abort
		{
			get
			{
				return mstrAbort;
			}
			set
			{
				mstrAbort = value;
			}
		}

		private static string mstrCancel = "Cancel";
		public static string Cancel
		{
			get
			{
				return mstrCancel;
			}
			set
			{
				mstrCancel = value;
			}
		}

		private static string mstrIgnore = "Ignore";
		public static string Ignore
		{
			get
			{
				return mstrIgnore;
			}
			set
			{
				mstrIgnore = value;
			}
		}

		private static string mstrNo = "No";
		public static string No
		{
			get
			{
				return mstrNo;
			}
			set
			{
				mstrNo = value;
			}
		}

		private static string mstrOk = "OK";
		public static string Ok
		{
			get
			{
				return mstrOk;
			}
			set
			{
				mstrOk = value;
			}
		}

		private static string mstrRetry = "Retry";
		public static string Retry
		{
			get
			{
				return mstrRetry;
			}
			set
			{
				mstrRetry = value;
			}
		}

		private static string mstrYes = "Yes";
		public static string Yes
		{
			get
			{
				return mstrYes;
			}
			set
			{
				mstrYes = value;
			}
		}

        private static string mstrDont_Show_Again = "Don't Show Again";
        public static string Dont_Show_Again
		{
			get
			{
                return mstrDont_Show_Again;
			}
			set
			{
                mstrDont_Show_Again = value;
			}
		}
	}
}
