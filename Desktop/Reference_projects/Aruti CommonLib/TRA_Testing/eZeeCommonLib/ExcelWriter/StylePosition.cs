﻿namespace ExcelWriter
{
    using System;

    public enum StylePosition
    {
        NotSet,
        Left,
        Top,
        Right,
        Bottom
    }
}

