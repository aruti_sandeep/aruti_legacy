IF EXISTS(select * from sys.procedures where name='CreateCalendar') DROP PROC CreateCalendar

CREATE PROC CreateCalendar
    (
      @tablename AS NVARCHAR(20) ,
      @fdt AS SMALLDATETIME ,
      @tdt AS SMALLDATETIME ,
      @dept AS VARCHAR(5) ,
      @sect AS VARCHAR(5) ,
      @job AS VARCHAR(5) ,
      @UserId AS NVARCHAR(5) ,
      @Employeeid AS NVARCHAR(5) ,
      @datefmtno AS INT
    )
AS 
    BEGIN
        DECLARE @dt AS SMALLDATETIME ,
            @temp VARCHAR(100) ,
            @sql VARCHAR(4000) ,
            @new VARCHAR(4000) ,
            @subqry VARCHAR(4000)
        IF EXISTS ( SELECT  name
                    FROM    sys.tables
                    WHERE   name = @tablename ) 
            SELECT  @temp = 'drop table ' + @tablename + ''
        EXEC (@temp)
        SELECT  @dt = @fdt
        SELECT  @sql = ' create table ' + @tablename
                + '( CalendarTableID int identity(1,1), leaveplannerunkid varchar(500) default('
                + CHAR(39) + ' ' + CHAR(39)
                + '), leavetypeunkid varchar(500) default(' + CHAR(39) + ' '
                + CHAR(39)
                + '), EmployeeUnkid int, Ecode varchar(255), DisplayName varchar(100),   '
        WHILE @dt <= @tdt 
            BEGIN
                SELECT  @sql = @sql + '[' + CONVERT(VARCHAR(10), @dt, @datefmtno)
                        + '] varchar(100) default(' + CHAR(39) + ' ' + CHAR(39)
                        + ') , '
                SELECT  @dt = DATEADD(D, 1, @dt)
            END
        SELECT  @sql = LEFT(@sql, LEN(@sql) - 2) + ')'
        PRINT @sql
        EXEC (@sql)
        SELECT  @new = ' SELECT l.EmployeeUnkID as EmployeeUnkID, e.employeecode as Ecode, (e.firstname+space(1)+e.surname) as  Displayname   into '
                + @tablename + '1 '
        SELECT  @new = @new
                + ' FROM lvleaveplanner l left outer join hremployee_master e on e.EmployeeUnkID = l.EmployeeUnkID '
        SELECT  @new = @new + ' WHERE l.isvoid = 0 '
        SELECT  @new = @new + ' AND ( l.startdate between ' + CHAR(39)
                + CONVERT(VARCHAR(8), @fdt, 112) + CHAR(39) + ' and '
                + CHAR(39) + CONVERT(VARCHAR(8), @tdt, 112) + CHAR(39)
        SELECT  @new = @new + '       OR l.stopdate between ' + CHAR(39)
                + CONVERT(VARCHAR(8), @fdt, 112) + CHAR(39) + ' and '
                + CHAR(39) + CONVERT(VARCHAR(8), @tdt, 112) + CHAR(39) + ' ) '
        PRINT @new
        IF @Employeeid > 0 
            SELECT  @new = @new + '    AND  l.employeeunkid =' + @Employeeid
                    + ''
        IF @UserId > 0 
            SELECT  @new = @new + '    AND l.userunkid IN(-1,' + @UserId
                    + ' )'
        IF @dept > 0 
            SELECT  @new = @new + '    AND e.departmentunkid = ' + @dept
        IF @sect > 0 
            SELECT  @new = @new + '    AND e.sectionunkid =' + @sect
        IF @job > 0 
            SELECT  @new = @new + '    AND e.jobunkid =' + @job
        PRINT @new
        SELECT  @new = @new
                + ' group by l.employeeunkid,e.firstname , e.surname,e.employeecode  '
        PRINT ( @new )
        SELECT  @subqry = SUBSTRING(@new, CHARINDEX('FROM ', @new), LEN(@new))
        SELECT  @subqry = REPLACE(@subqry,
                                  'group by l.employeeunkid,e.firstname , e.surname',
                                  'group by l.employeeunkid,l.leaveplannerunkid,l.leavetypeunkid,e.firstname,e.surname')
        PRINT @new
        EXEC (@new)
        PRINT @subqry
        SELECT  @new = 'insert into ' + @tablename
                + '(EmployeeUnkID, Ecode, DisplayName)  select EmployeeUnkID, Ecode, DisplayName  from '
                + @tablename + '1  '
        SELECT  @new = @new + ' '
        EXEC (@new)
        IF EXISTS ( SELECT  name
                    FROM    sys.tables
                    WHERE   name = 'mynew' ) 
            SELECT  @temp = 'drop table mynew'
        EXEC (@temp)
        SELECT  @new = 'select  l.EmployeeUnkID , l.leaveplannerunkid, l.leavetypeunkid  into mynew '
                + @subqry + ' '
        PRINT @new
        EXEC (@new)
        SELECT  @new = 'drop table ' + @tablename + '1'
        EXEC (@new)
        DECLARE @EmployeeUnkID AS NVARCHAR(10) ,
            @leaveplannerunkid AS NVARCHAR(10) ,
            @leavetypeunkid AS NVARCHAR(10)
        DECLARE updateorddet CURSOR
        FOR
            SELECT  EmployeeUnkID ,
                    leaveplannerunkid ,
                    leavetypeunkid
            FROM    mynew
        OPEN updateorddet
        FETCH NEXT FROM updateorddet INTO @EmployeeUnkID, @leaveplannerunkid,
            @leavetypeunkid
        WHILE @@fetch_status = 0 
            BEGIN
                PRINT @EmployeeUnkID
                PRINT @leaveplannerunkid
                PRINT @leavetypeunkid
                SELECT  @new = 'update ' + @tablename
                        + ' set  leaveplannerunkid  = leaveplannerunkid  + '
                        + CHAR(39) + @leaveplannerunkid + ',' + CHAR(39)
                        + '  , leavetypeunkid  = leavetypeunkid + ' + CHAR(39)
                        + @leavetypeunkid + ',' + CHAR(39)
                        + ' where EmployeeUnkID = ' + @EmployeeUnkID + ' '
                PRINT @new
                EXEC (@new)
                FETCH NEXT FROM updateorddet INTO @EmployeeUnkID,
                    @leaveplannerunkid, @leavetypeunkid
            END
        CLOSE updateorddet
        DEALLOCATE updateorddet
    END