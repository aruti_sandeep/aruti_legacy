﻿#Region " Imports "

Imports System
Imports eZeeCommonLib
Imports System.Security

#End Region

'* NOTE : IF THERE IS ANY CHANGES IN FOLLOWING TABLE PLEASE MAKE SURE YOU DO THE CHANGES IN IDM_ALTER_9 IF APPLICABLE
'(1). hremployee_transfer_tran
'(2). hremployee_dates_tran
'(3). hremployee_cctranhead_tran
'(4). hremployee_categorization_tran
'(5). hremployee_work_permit_tran
'(6). hremployee_rehire_tran
'(7). hremployee_master

Public Class frmUpdateDatabase

#Region " Private Variables "

    Private dtConfigScript As DataTable = Nothing
    Private dtTranScript As DataTable = Nothing
    Private dtImageScript As DataTable = Nothing
    Private dsList As New DataSet
    Private mstrDatabaseName As String = String.Empty
    Private mstrArgs As String = String.Empty '/N -> New Installation, /S -> Service Pack, /T -> Only Transaction Database Create Script 
    Private dtTemp() As DataRow = Nothing
    Private strResource As String = String.Empty
    Private mIntTotalDatabases As Integer = 0
    Private arrDatabase As ArrayList
    Private iDBCnt As Integer = 0
    Private iReportCnt As Integer = 1
    Private mblnArutiImg As Boolean = False
    Private StrDB_Version As String = String.Empty
    Private Org_DbVersion As String = String.Empty
    Private iRet_Value As Integer = 0
    Private Const conREG_NODE As String = "Software\NPK\Aruti"
    Private blnIsSameVersion As Boolean = False
    Private strCurrentDbName As String = String.Empty
    Private mintTotalScriptCount As Integer = 0
    'S.SANDEEP |24-APR-2019| -- START
    Private mblnArutiIdm As Boolean = False
    Private dtIDMScript As DataTable = Nothing
    'S.SANDEEP |24-APR-2019| -- END


    '<FOR DISABLING CLOSE BUTTON OF FORM PLEASE DO NOT DELETE THIS>
    Private Const CP_NOCLOSE_BUTTON As Integer = &H200
    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim myCp As CreateParams = MyBase.CreateParams
            myCp.ClassStyle = myCp.ClassStyle Or CP_NOCLOSE_BUTTON
            Return myCp
        End Get
    End Property
    '<FOR DISABLING CLOSE BUTTON OF FORM PLEASE DO NOT DELETE THIS>
    Private blnCopyError As Boolean = False
#End Region

#Region " API Functions "

    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="GetPrivateProfileStringA", ExactSpelling:=True, CharSet:=System.Runtime.InteropServices.CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function GetPrivateProfileString(ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As System.Text.StringBuilder, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="WritePrivateProfileStringA", ExactSpelling:=True, CharSet:=System.Runtime.InteropServices.CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function WritePrivateProfileString(ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="GetPrivateProfileIntA", ExactSpelling:=True, CharSet:=System.Runtime.InteropServices.CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function GetPrivateProfileInt(ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Integer, ByVal lpFileName As String) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("kernel32.dll", EntryPoint:="WritePrivateProfileStringA", ExactSpelling:=True, CharSet:=System.Runtime.InteropServices.CharSet.Ansi, SetLastError:=True)> _
    Private Shared Function FlushPrivateProfileString(ByVal lpApplicationName As Integer, ByVal lpKeyName As Integer, ByVal lpString As Integer, ByVal lpFileName As String) As Integer
    End Function

#End Region

#Region " Forms Event(s) "

    Private Sub frmUpdateDatabase_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            CheckForIllegalCrossThreadCalls = False
            Dim objDatabase As New eZeeDatabase
            'objDatabase.ServerName = "(local)"
            'objDatabase.Connect()
            Dim mstrServerName As String = ""
            Dim xAppPath As String = getValue()

            If xAppPath.Trim.Length > 0 Then
                mstrServerName = GetString("Aruti_Payroll", "Server", xAppPath)
                If mstrServerName = "" Then mstrServerName = "(local)"
            Else
                mstrServerName = "(local)"
            End If

            Dim aCore32_Ver As Integer = 0
            If IO.File.Exists(xAppPath & "acore32.dll") Then
                Dim vr As FileVersionInfo = FileVersionInfo.GetVersionInfo(xAppPath & "acore32.dll")
                aCore32_Ver = CInt(vr.FileVersion.Replace(".", ""))
            End If

            objDatabase.ServerName = mstrServerName
            objDatabase.Connect()

            Create_Aruti_User()

            If My.Application.CommandLineArgs.Count > 0 Then
                mstrArgs = My.Application.CommandLineArgs(0).ToString
                Try
                    mstrDatabaseName = My.Application.CommandLineArgs(1).ToString
                Catch ex As Exception
                    mstrDatabaseName = ""
                End Try
            End If

            'Me.pbProgress.ForeColor = Color.Orange

            Using objD As New clsDataOperation
                If objD.RecordCount("SELECT 1 FROM sys.databases WHERE UPPER(name) = 'ARUTIIMAGES'") > 0 Then mblnArutiImg = True
                'S.SANDEEP |24-APR-2019| -- START
                If objD.RecordCount("SELECT 1 FROM sys.databases WHERE UPPER(name) = 'IDM_DATA'") > 0 Then mblnArutiIdm = True
                'S.SANDEEP |24-APR-2019| -- END
            End Using

            'iDBCnt += 1

            Select Case mstrArgs.ToString.ToUpper

                Case "/N" 'THIS IS FOR NEW INSTALLTION
                    Call Generate_Config_Script()
                    mIntTotalDatabases = 1
                    pbProgress.Maximum = dtConfigScript.Rows.Count
                    lblDataCount.Text = "Creating Database. Please Wait."

                Case "/S" 'THIS IS FOR SERVICE PACK
                    eZeeDatabase.change_database("hrmsConfiguration")
                    objDataOperation = New clsDataOperation
                    StrDB_Version = objDataOperation.getVersion
                    Org_DbVersion = StrDB_Version
                    Dim sOrgVersion As String = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name & " : " & StrDB_Version
                    Dim StrVer() As String = StrDB_Version.Split(CChar("."))
                    StrDB_Version = ""
                    For i As Integer = 0 To StrVer.Length - 1
                        StrDB_Version &= CInt(StrVer(i))
                    Next

                    Call Generate_Config_Script(sOrgVersion)
                    Dim dCView As DataView = dtConfigScript.DefaultView
                    dCView.RowFilter = "CheckType >=" & StrDB_Version
                    dtConfigScript = dCView.ToTable

                    Call Generate_Trans_Script(sOrgVersion)
                    Dim dTView As DataView = dtTranScript.DefaultView
                    dTView.RowFilter = "CheckType >=" & StrDB_Version
                    dtTranScript = dTView.ToTable

                    If mblnArutiImg = True Then
                        Call Generate_Images_Script(sOrgVersion)
                        Dim dIView As DataView = dtImageScript.DefaultView
                        dIView.RowFilter = "CheckType >=" & StrDB_Version
                        dtImageScript = dIView.ToTable
                    End If

                    'S.SANDEEP |24-APR-2019| -- START
                    If mblnArutiIdm Then
                        Call Generate_IDM_Script(sOrgVersion)
                        Dim dIView As DataView = dtIDMScript.DefaultView
                        dIView.RowFilter = "CheckType >=" & StrDB_Version
                        dtIDMScript = dIView.ToTable
                    End If
                    'S.SANDEEP |24-APR-2019| -- END

                    arrDatabase = New ArrayList

                    If mstrDatabaseName = "" Then

                        arrDatabase = objDatabase.fillTranDatabaseList
                    Else

                        arrDatabase = New ArrayList
                        arrDatabase.Add(mstrDatabaseName)
                    End If

                    If mblnArutiImg = True Then
                        mIntTotalDatabases = arrDatabase.Count + 2
                    Else
                        mIntTotalDatabases = arrDatabase.Count + 1
                    End If

                    'S.SANDEEP |24-APR-2019| -- START
                    If mblnArutiIdm Then
                        mIntTotalDatabases = mIntTotalDatabases + 1
                    End If
                    'S.SANDEEP |24-APR-2019| -- END

                    pbProgress.Maximum = mIntTotalDatabases
                    objDataOperation = Nothing

                    'lblDataCount.Text = "Total Database Found : " & mIntTotalDatabases.ToString & ". Updating Database : " & (iDBCnt + 1).ToString
                    lblDataCount.Text = "Total Database Found : " & mIntTotalDatabases.ToString & ". Updating Database(s)...."

                Case "/T" 'THIS IS FOR NEW COMPANY MADE FROM ARUTI CONFIGURATION
                    Call Generate_Trans_Script()
                    mIntTotalDatabases = 1
                    pbProgress.Maximum = dtTranScript.Rows.Count
                    arrDatabase = New ArrayList
                    arrDatabase.Add(mstrDatabaseName)
                    lblDataCount.Text = "Creating Database. Please Wait."

                Case Else 'RUNNING FULL SCRIPT IN CASE OF ANY SCRIPT LEFT OUT 

                    Using objD As New clsDataOperation
                        If objD.RecordCount("SELECT 1 FROM sys.databases WHERE UPPER(name) = 'HRMSCONFIGURATION'") <= 0 Then Me.Close()
                    End Using

                    Call Generate_Config_Script()
                    Call Generate_Trans_Script()


                    If mblnArutiImg = True Then
                        Call Generate_Images_Script()
                    End If

                    'S.SANDEEP |24-APR-2019| -- START
                    If mblnArutiIdm Then
                        Call Generate_IDM_Script()
                    End If
                    'S.SANDEEP |24-APR-2019| -- END

                    arrDatabase = New ArrayList
                    If mstrDatabaseName = "" Then
                        arrDatabase = objDatabase.fillTranDatabaseList
                    Else
                        arrDatabase = New ArrayList
                        arrDatabase.Add(mstrDatabaseName)
                    End If


                    If arrDatabase.Count > 0 Then
                        If mblnArutiImg = True Then
                            mIntTotalDatabases = arrDatabase.Count + 2
                        Else
                            mIntTotalDatabases = arrDatabase.Count + 1
                        End If
                    Else
                        mIntTotalDatabases = 1
                    End If

                    'S.SANDEEP |24-APR-2019| -- START
                    If mblnArutiIdm Then
                        mIntTotalDatabases = mIntTotalDatabases + 1
                    End If
                    'S.SANDEEP |24-APR-2019| -- END

                    pbProgress.Maximum = mIntTotalDatabases

                    'lblDataCount.Text = "Total Database Found : " & mIntTotalDatabases.ToString & ". Updating Database : " & (iDBCnt + 1).ToString
                    lblDataCount.Text = "Total Database Found : " & mIntTotalDatabases.ToString & ". Updating Database(s)...."
            End Select

            Using ObjDO As New eZeeCommonLib.clsDataOperation
                dsList = New DataSet
                dsList = ObjDO.ExecQuery("SELECT DISTINCT  " & _
                                         " REVERSE(RIGHT(REVERSE(physical_name),(LEN(physical_name)-CHARINDEX('\', REVERSE(physical_name),1))+1)) As PhyPath  " & _
                                         ",sys.databases.name as dbName " & _
                                         "FROM sys.master_files " & _
                                         "JOIN sys.databases ON sys.master_files.database_id = sys.databases.database_id ", "List")
            End Using
            'objlblError.Text = "Processing Script...."
            txtError.Text = "Processing Script...."
            bgWorker.RunWorkerAsync()
        Catch ex As Exception
            Select Case mstrArgs.ToString.ToUpper
                Case "/N"
                    Using objDa As New clsDataOperation
                        objDa.ExecNonQuery("Use [master] ")
                        objDa.ExecNonQuery("DROP DATABASE hrmsConfiguration")
                    End Using
            End Select
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Me.Close()
        End Try
    End Sub

#End Region

#Region " Private Function(s) "

    Public Function GetString(ByVal Section As String, ByVal Key As String, ByVal AppPath As String) As String
        Return GetString(Section, Key, "", AppPath)
    End Function

    Public Function GetString(ByVal Section As String, ByVal Key As String, ByVal [Default] As String, ByVal mstrAppPath As String) As String
        Dim intCharCount As Integer = 0
        Dim strResult As String = ""
        Dim objResult As New System.Text.StringBuilder(256)
        intCharCount = GetPrivateProfileString(Section, Key, "", objResult, objResult.Capacity, mstrAppPath & "aruti.ini")

        If intCharCount > 0 Then
            strResult = objResult.ToString().Substring(0, intCharCount)
        End If
        Return strResult
    End Function

    Public Function getValue() As String
        Dim Key As Microsoft.Win32.RegistryKey = Nothing
        Dim Value As String = String.Empty
        Try
            Key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(conREG_NODE, True)
            If Key Is Nothing Then
                Value = ""
            End If
            Value = CStr(Key.GetValue("AppPath"))
        Catch e As Exception
            Value = ""
        End Try
        Return Value
    End Function

    Private Sub Generate_Config_Script(Optional ByVal sVersion As String = "")
        Dim sMsg As String = String.Empty
        Try
            dtConfigScript = Nothing
            '/****************Data Type Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.DataType_1
            Try
                dsList.ReadXml(New IO.StringReader(strResource)) : dtConfigScript = dsList.Tables(0)
            Catch ex As Exception
                sMsg = "File : [DATA TYPE] "
                Throw ex
            End Try
            '/****************Data Type Script********************/ END

            '/****************Configuration Main Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Config_Main_2
            Try
                dsList.ReadXml(New IO.StringReader(strResource)) : dtConfigScript.Merge(dsList.Tables(0), True)
            Catch ex As Exception
                sMsg = "File : Configuration [MAIN] "
                Throw ex
            End Try
            '/****************Configuration Main Script********************/ END

            '/****************Configuration Alter Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Config_Alter_3
            If strResource.Trim.Length > 0 Then
                Try
                    dsList.ReadXml(New IO.StringReader(strResource)) : dtConfigScript.Merge(dsList.Tables(0), True)
                Catch ex As Exception
                    sMsg = "File : Configuration [ALTER] "
                    Throw ex
                End Try
            End If
            '/****************Configuration Alter Script********************/ END

            '/****************Configuration UDF Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Config_UDF_4
            If strResource.Trim.Length > 0 Then
                Try
                    dsList.ReadXml(New IO.StringReader(strResource)) : dtConfigScript.Merge(dsList.Tables(0), True)
                Catch ex As Exception
                    sMsg = "File : Configuration [USER DEFINE]"
                    Throw ex
                End Try
            End If
            '/****************Configuration UDF Script********************/ END
            If dtConfigScript.Rows.Count <= 0 Then
                MsgBox("Sorry, Error in creating database script. Please contact Aruti Support Team.", MsgBoxStyle.Critical)
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox("Generate_Config_Script : " & sMsg & " ;" & vbCrLf & ex.Message, MsgBoxStyle.Critical, sVersion)
            Me.Close()
        End Try
    End Sub

    Private Sub Generate_Trans_Script(Optional ByVal sVersion As String = "")
        Dim sMsg As String = String.Empty
        Try
            dtTranScript = Nothing
            '/****************Data Type Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.DataType_1
            Try
                dsList.ReadXml(New IO.StringReader(strResource)) : dtTranScript = dsList.Tables(0)
            Catch ex As Exception
                sMsg = "File : [DATA TYPE]"
                Throw ex
            End Try
            '/****************Data Type Script********************/ END

            '/****************Transaction Main Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Tran_Main_5
            Try
                dsList.ReadXml(New IO.StringReader(strResource))
                'Dim duplicates = dsList.Tables(0).AsEnumerable().GroupBy(Function(r) r("Id")).Where(Function(gr) gr.Count() > 1).[Select](Function(g) g.Key)
                'If duplicates.Count > 0 Then
                '    sMsg = "File : Transaction [MAIN]"
                '    Throw New Exception("Same Number Found In Main.")
                'End If
                dtTranScript.Merge(dsList.Tables(0), True)
            Catch ex As Exception
                sMsg = "File : Transaction [MAIN]"
                Throw ex
            End Try
            '/****************Transaction Main Script********************/ END

            '/****************Transaction Alter Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Tran_Alter_6
            If strResource.Trim.Length > 0 Then
                Try
                    dsList.ReadXml(New IO.StringReader(strResource)) : dtTranScript.Merge(dsList.Tables(0), True)
                Catch ex As Exception
                    sMsg = "File : Transaction [ALTER]"
                    Throw ex
                End Try
            End If
            '/****************Transaction Alter Script********************/ END

            '/****************Transaction UDF Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Tran_UDF_7
            If strResource.Trim.Length > 0 Then
                Try
                    dsList.ReadXml(New IO.StringReader(strResource)) : dtTranScript.Merge(dsList.Tables(0), True)
                Catch ex As Exception
                    sMsg = "File : Transaction [USER DEFINE]"
                    Throw ex
                End Try
            End If
            '/****************Transaction UDF Script********************/ END
            If dtTranScript.Rows.Count <= 0 Then
                MsgBox("Sorry, Error in creating database script. Please contact Aruti Support Team.", MsgBoxStyle.Critical)
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox("Generate_Trans_Script : " & sMsg & " ;" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
            Me.Close()
        End Try
    End Sub

    Private Sub Generate_Images_Script(Optional ByVal sVersion As String = "")
        Try
            dtImageScript = Nothing
            '/****************Aruti Images Alter Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Img_Alter_8
            If strResource.Trim.Length > 0 Then
                dsList.ReadXml(New IO.StringReader(strResource)) : dtImageScript = dsList.Tables(0)
            End If
            '/****************Aruti Images Alter Script********************/ END
        Catch ex As Exception
            MsgBox("Generate_Images_Script : " & ex.Message, MsgBoxStyle.Critical, sVersion)
        End Try
    End Sub

    'S.SANDEEP |24-APR-2019| -- START
    Private Sub Generate_IDM_Script(Optional ByVal sVersion As String = "")
        Try
            dtIDMScript = Nothing
            '/****************Aruti IDM Alter Script********************/ START
            dsList = New DataSet : strResource = ""
            strResource = My.Resources.Idm_Alter_9
            If strResource.Trim.Length > 0 Then
                dsList.ReadXml(New IO.StringReader(strResource)) : dtIDMScript = dsList.Tables(0)
            End If
            '/****************Aruti IDM Alter Script********************/ END
        Catch ex As Exception
            MsgBox("Generate_IDM_Script : " & ex.Message, MsgBoxStyle.Critical, sVersion)
        End Try
    End Sub
    'S.SANDEEP |24-APR-2019| -- END

    Private Function Create_Aruti_User() As Boolean
        Try
            Dim xQry As String = ""
            xQry = "IF NOT EXISTS(SELECT name FROM sys.syslogins WHERE name = 'aruti_sa') " & _
                       "BEGIN " & _
                           "USE [master] " & _
                           "CREATE LOGIN [aruti_sa] WITH PASSWORD=N'" & lblDataCount.Tag.ToString() & "', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'bulkadmin' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'dbcreator' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'diskadmin' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'processadmin' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'securityadmin' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'serveradmin' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'setupadmin' " & _
                           "EXEC master..sp_addsrvrolemember @loginame = N'aruti_sa', @rolename = N'sysadmin' " & _
                       "END "
            Using objdo As New clsDataOperation

                objdo.ExecNonQuery(xQry)

                If objdo.ErrorMessage <> "" Then
                    Throw New Exception(objdo.ErrorNumber & ":" & objdo.ErrorMessage)
                End If

            End Using

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Controls Event(s) "

    Private Sub bgWorker_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker.DoWork
        Dim StrQ As String = String.Empty
        Dim sMsg As String = String.Empty
        'S.SANDEEP |11-APR-2019| -- START
        Dim intId As Integer = 0
        Dim intBlockId As Integer = 0
        Dim strChckVersion As String = ""
        'S.SANDEEP |11-APR-2019| -- END
        Try
            iDBCnt = 0


            Using objDataOperation As New clsDataOperation
                '/**************** RUNNING CONFIGURATION SCRIPT ****************/ START
                If dtConfigScript IsNot Nothing Then
                    StrQ = "USE hrmsConfiguration "
                    sMsg = "hrmsConfiguration"
                    objDataOperation.ExecNonQuery(StrQ.Trim)
                    strCurrentDbName = sMsg
                    mintTotalScriptCount = dtConfigScript.Rows.Count - 1

                    If Not objDataOperation.ErrorMessage = "" Then
                        MsgBox(objDataOperation.ErrorMessage)
                        Exit Sub
                    End If

                    For iCnt As Integer = 0 To dtConfigScript.Rows.Count - 1
                        StrQ = CStr(dtConfigScript.Rows(iCnt).Item("Script"))
                        'S.SANDEEP |11-APR-2019| -- START
                        intId = CInt(dtConfigScript.Rows(iCnt).Item("Id"))
                        intBlockId = CInt(dtConfigScript.Rows(iCnt).Item("ConditionValue1"))
                        strChckVersion = CStr(dtConfigScript.Rows(iCnt).Item("Version"))
                        'S.SANDEEP |11-APR-2019| -- END
                        objDataOperation.ExecNonQuery(StrQ.Trim)
                        If Not objDataOperation.ErrorMessage = "" Then
                            Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                        End If
                        'Select Case mstrArgs.ToString.ToUpper
                        'Case "/N", "/T", "/S"
                        bgWorker.ReportProgress(iCnt)
                        'End Select
                        System.Threading.Thread.Sleep(100)
                    Next
                    Select Case mstrArgs.ToString.ToUpper
                        Case "/N", "/T"
                        Case Else
                            iDBCnt += 1
                            bgWorker.ReportProgress(iDBCnt)
                    End Select
                End If
                '/**************** RUNNING CONFIGURATION SCRIPT ****************/ END


                '/**************** RUNNING TRANSACTION SCRIPT ****************/ START
                If dtTranScript IsNot Nothing Then
                    Dim StrPhysicalPath As String = String.Empty
                    For Each objItem As Object In arrDatabase
                        If objItem.ToString.ToUpper().StartsWith("TRAN_") Then
                            If dsList.Tables(0).Rows.Count > 0 Then  ' if database is not present in physical path then it will ignore.
                                dtTemp = dsList.Tables(0).Select("dbName='" & objItem.ToString & "'")
                                If dtTemp.Length > 0 Then
                                    StrPhysicalPath = CStr(dtTemp(0)("PhyPath"))
                                End If
                            End If

                            If StrPhysicalPath.Length > 0 Then
                                'If System.IO.File.Exists(StrPhysicalPath & objItem.ToString & ".mdf") = True And System.IO.File.Exists(StrPhysicalPath & objItem.ToString & "_log.LDF") = True Then
                                'objDataOperation.ExecNonQuery("USE " & objItem.ToString)
                                eZeeDatabase.change_database(objItem.ToString)
                                sMsg = objItem.ToString
                                strCurrentDbName = sMsg
                                mintTotalScriptCount = dtTranScript.Rows.Count - 1

                                For iCnt As Integer = 0 To dtTranScript.Rows.Count - 1
                                    StrQ = CStr(dtTranScript.Rows(iCnt).Item("Script"))

                                    'S.SANDEEP |11-APR-2019| -- START
                                    intId = CInt(dtTranScript.Rows(iCnt).Item("Id"))
                                    intBlockId = CInt(dtTranScript.Rows(iCnt).Item("ConditionValue1"))
                                    strChckVersion = CStr(dtTranScript.Rows(iCnt).Item("Version"))
                                    'S.SANDEEP |11-APR-2019| -- END
                                    objDataOperation.ExecNonQuery(StrQ.Trim)
                                    If Not objDataOperation.ErrorMessage = "" Then
                                        Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                                    End If
                                    'Select Case mstrArgs.ToString.ToUpper
                                    '   Case "/N", "/T", "/S"
                                    bgWorker.ReportProgress(iCnt)
                                    'End Select
                                    System.Threading.Thread.Sleep(100)
                                Next
                                Select Case mstrArgs.ToString.ToUpper
                                    Case "/N", "/T"
                                    Case Else
                                        iDBCnt += 1
                                        bgWorker.ReportProgress(iDBCnt)
                                End Select
                                'End If
                            End If

                        End If
                    Next
                End If
                '/**************** RUNNING TRANSACTION SCRIPT ****************/ END

                '/**************** RUNNING IMAGES DATABASE SCRIPT ****************/ START
                If mblnArutiImg = True Then
                    If dtImageScript IsNot Nothing Then
                        objDataOperation.ExecNonQuery("USE arutiimages")
                        sMsg = "arutiimages"
                        strCurrentDbName = sMsg
                        mintTotalScriptCount = dtImageScript.Rows.Count - 1
                        If Not objDataOperation.ErrorMessage = "" Then
                            MsgBox(objDataOperation.ErrorMessage)
                            Exit Sub
                        End If

                        'lblDBName.Text = "Current Database : arutiimages "

                        For iCnt As Integer = 0 To dtImageScript.Rows.Count - 1
                            StrQ = CStr(dtImageScript.Rows(iCnt).Item("Script"))

                            'S.SANDEEP |11-APR-2019| -- START
                            intId = CInt(dtImageScript.Rows(iCnt).Item("Id"))
                            intBlockId = CInt(dtImageScript.Rows(iCnt).Item("ConditionValue1"))
                            strChckVersion = CStr(dtImageScript.Rows(iCnt).Item("Version"))
                            'S.SANDEEP |11-APR-2019| -- END

                            objDataOperation.ExecNonQuery(StrQ.Trim)
                            If Not objDataOperation.ErrorMessage = "" Then
                                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            End If
                            'Select Case mstrArgs.ToString.ToUpper
                            '   Case "/N", "/T", "/S"
                            bgWorker.ReportProgress(iCnt)
                            'End Select
                            System.Threading.Thread.Sleep(100)
                        Next
                        Select Case mstrArgs.ToString.ToUpper
                            Case "/N", "/T"
                            Case Else
                                iDBCnt += 1
                                bgWorker.ReportProgress(iDBCnt)
                        End Select
                    End If
                End If
                '/**************** RUNNING IMAGES DATABASE SCRIPT ****************/ END

                'S.SANDEEP |24-APR-2019| -- START
                If mblnArutiIdm Then
                    If dtIDMScript IsNot Nothing Then
                        objDataOperation.ExecNonQuery("USE IDM_DATA")
                        sMsg = "IDM_DATA"
                        strCurrentDbName = sMsg
                        mintTotalScriptCount = dtIDMScript.Rows.Count - 1

                        If Not objDataOperation.ErrorMessage = "" Then
                            MsgBox(objDataOperation.ErrorMessage)
                            Exit Sub
                        End If

                        For iCnt As Integer = 0 To dtIDMScript.Rows.Count - 1
                            StrQ = CStr(dtIDMScript.Rows(iCnt).Item("Script"))

                            intId = CInt(dtIDMScript.Rows(iCnt).Item("Id"))
                            intBlockId = CInt(dtIDMScript.Rows(iCnt).Item("ConditionValue1"))
                            strChckVersion = CStr(dtIDMScript.Rows(iCnt).Item("Version"))

                            objDataOperation.ExecNonQuery(StrQ.Trim)

                            If Not objDataOperation.ErrorMessage = "" Then
                                Throw New Exception(objDataOperation.ErrorNumber & " : " & objDataOperation.ErrorMessage)
                            End If

                            bgWorker.ReportProgress(iCnt)

                            System.Threading.Thread.Sleep(100)
                        Next
                        Select Case mstrArgs.ToString.ToUpper
                            Case "/N", "/T"
                            Case Else
                                iDBCnt += 1
                                bgWorker.ReportProgress(iDBCnt)
                        End Select
                    End If
                End If
                'S.SANDEEP |24-APR-2019| -- END
            End Using
        Catch ex As Exception
            'S.SANDEEP |11-APR-2019| -- START
            Dim byt As Byte() = System.Text.Encoding.UTF8.GetBytes(ex.Message)
            Dim strError As String = "Error Id          : 0x000000" & intId.ToString() & "E" & vbCrLf & _
                                     "Error Block       : 0x000000" & intBlockId.ToString() & "E" & vbCrLf & _
                                     "Error Decription  : 0x00" & strChckVersion.Replace(".", "00") & "00C" & vbCrLf & _
                                     "Error Content     : " & Convert.ToBase64String(byt)
            'S.SANDEEP |11-APR-2019| -- END
            If Org_DbVersion.Trim.Length > 0 Then
                Using objDataOp As New clsDataOperation

                    StrQ = "Use hrmsConfiguration"

                    objDataOp.ExecNonQuery(StrQ)

                    If objDataOp.ErrorMessage <> "" Then
                        MsgBox("Updating Script Issue : " & sMsg & ";" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
                        iRet_Value = -1
                    End If

                    StrQ = "IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[version]') AND type in (N'P', N'PC')) " & _
                           "DROP PROCEDURE [dbo].[version] "

                    objDataOp.ExecNonQuery(StrQ)

                    If objDataOp.ErrorMessage <> "" Then
                        MsgBox("Updating Script Issue : " & sMsg & ";" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
                        iRet_Value = -1
                    End If

                    StrQ = "CREATE PROCEDURE [dbo].[version] AS SELECT '" & Org_DbVersion & "' AS version "

                    objDataOp.ExecNonQuery(StrQ)

                    If objDataOp.ErrorMessage <> "" Then
                        MsgBox("Updating Script Issue : " & sMsg & ";" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
                        iRet_Value = -1
                    End If

                    System.Threading.Thread.Sleep(1000)

                End Using
            End If
            'S.SANDEEP |11-APR-2019| -- START
            'MsgBox("Updating Script Issue : " & sMsg & ";" & vbCrLf & ex.Message, MsgBoxStyle.Critical)
            txtError.Text = strError
            If MsgBox("Updating Script failed due to below error. Please press yes to copy exception and send it to aruti support team.", CType(MsgBoxStyle.Critical + MsgBoxStyle.YesNo, MsgBoxStyle)) = MsgBoxResult.Yes Then
                blnCopyError = True
            End If
            'S.SANDEEP |11-APR-2019| -- END
            iRet_Value = -1
        End Try
    End Sub

    Private Sub bgWorker_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgWorker.ProgressChanged
        Try
            lblDBName.Text = "Database : " & strCurrentDbName & ", Progress : " & CDbl((e.ProgressPercentage / mintTotalScriptCount) * 100).ToString("##0.#0") & "%"
            Select Case mstrArgs.ToString.ToUpper
                Case "/N", "/T"
                    pbProgress.Value = e.ProgressPercentage
                Case Else
                    pbProgress.Value = iDBCnt
                    lblDataCount.Text = "Total Database Found : " & mIntTotalDatabases.ToString & ". Database Updated : " & (iDBCnt).ToString
                    'lblDataCount.Text = "Total Database Found : " & mIntTotalDatabases.ToString & ". Updating Database(s)...."
                    lblPercentage.Text = "Overall Update Progress : " & CDbl((iDBCnt / mIntTotalDatabases) * 100).ToString("##0.#0") & "%"
            End Select
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub bgWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgWorker.RunWorkerCompleted
        Try
            If iRet_Value = -1 Then
                My.Computer.Clipboard.SetText(txtError.Text)
                MsgBox("Fail to update database(s).", CType(MsgBoxStyle.OkOnly + MsgBoxStyle.Information, MsgBoxStyle))
            Else
                lblDBName.Text = "" : lblPercentage.Text = ""
                MsgBox("Database(s) Updated Successfully.", CType(MsgBoxStyle.OkOnly + MsgBoxStyle.Information, MsgBoxStyle))
            End If
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

#End Region

End Class